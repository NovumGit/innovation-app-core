<?php
namespace _Default;

use Core\Customer;
use Core\Logger;
use Core\MainController;
use Core\Setting;
use Core\Translate;
use Core\Utils;
use Exception\LogicException;
use Model\Category\CustomerWishlistQuery;
use Model\Cms\Site;
use Model\Cms\SitePageQuery;
use Model\Cms\SiteQuery;
use Model\Logging\Except_log;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderQuery;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;
use Model\Setting\MasterTable\SaleOrderStatusQuery;

abstract class _DefaultController extends MainController  {

    protected $oSite;

    function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);

	    $oSiteQuery = SiteQuery::create();
	    $oSiteQuery->setQueryKey('SiteQuery-findOneByDomain');
	    $this->oSite = $oSiteQuery->findOneByDomain(getSiteSettings()['live_domain']);
    }

    function getSite():Site
    {
        return $this->oSite;
    }
    function getCurrentLanguage():Language
    {
        return Utils::detectWebshopUserLanguage();
    }
    abstract function runShop();

    /**
     * This method returns the currently open sale order if there is one.
     * @return SaleOrder
     */
    function getCurrentSaleOrder():SaleOrder
    {
        try
        {
        if(isset($_SESSION['CURRENT_SALE_ORDER']) && is_numeric($_SESSION['CURRENT_SALE_ORDER']))
        {
	        $SaleOrderQuery = SaleOrderQuery::create();
	        $SaleOrderQuery->setQueryKey('SaleOrderQuery-findOneById');
            $oSaleOrder = $SaleOrderQuery->findOneById($_SESSION['CURRENT_SALE_ORDER']);

            if($oSaleOrder instanceof SaleOrder && !$oSaleOrder->getIsClosed())
            {
                if($oSaleOrder->getCreatedOn()->diff(new \DateTime())->days < 7)
                {
                    // Order is van de afgelopen week, die kun je nog oppakken
                    return $oSaleOrder;
                }
            }
            Logger::warning('The system tried to re-open a customer sale order based on the CURRENT_SALE_ORDER setting but that order no longer existed.');
            unset($_SESSION['CURRENT_SALE_ORDER']);
        }
        }
        catch (\Exception $e)
        {
            Except_log::register($e, false);
            $this->redirect('/' . $this->getCurrentLanguage()->getShopUrlPrefix());
        }
        if(Customer::isSignedIn())
        {
	        // The customer was not yet already working on an order but maybe he still had a half completed order?
	        $oCustomer = Customer::getMember();

	        // Close old unfinished Sale_orders.
        	$iUpdatedRows = SaleOrderQuery::create()
		        ->filterByCreatedOn(['max' => time() - 24 * 60 * 60 * 5])
		        ->filterByCustomerId($oCustomer->getId())
		        ->update(['IsClosed' => true]);

        	Logger::info("Closed $iUpdatedRows sale order(s) for customer {$oCustomer->getId()} because they have been created more then 5 days ago.");

            $oSaleOrder = SaleOrderQuery::create()
                ->filterByIsClosed(false)
                ->filterByIsFullyPaid(false)
                ->filterByCustomerId($oCustomer->getId())
                ->findOne();
            if($oSaleOrder instanceof SaleOrder)
            {
                $_SESSION['CURRENT_SALE_ORDER'] = $oSaleOrder->getId();
                return $oSaleOrder;
            }
        }
        // The customer did not have an order yet and he/she also did not have a previous one.
        // So let's create a new order then..

        if(!isset($_SESSION['CURRENT_SALE_ORDER']))
        {
            $oSaleOrderStatusQuery = new SaleOrderStatusQuery();
            $oSaleOrderStatus = $oSaleOrderStatusQuery->findOneByCode('new');
            $oSaleOrder = new SaleOrder();
            $oSaleOrder->setEnvelopeWeightGrams(Setting::get('envelope_weight_grams'));
            $oSaleOrder->setWebshopUserSessionId(session_id());
            $oSaleOrder->setSaleOrderStatusId($oSaleOrderStatus->getId());
            $oSaleOrder->setWorkInProgress(true);
            $oLanguage = $this->getCurrentLanguage();
            $oSaleOrder->setLanguageId($oLanguage->getId());
            $oSaleOrder->save();
            $_SESSION['CURRENT_SALE_ORDER'] = $oSaleOrder->getId();
            return $oSaleOrder;
        }
        throw new LogicException("For some reason the script passed all possiblities for creating a sale order, this indicates a bug.");
    }
    function parse($sTemplate, $aData, Language $oLanguageOverwrite = null)
    {

        $oCurrentLanguage = $this->getCurrentLanguage();

	    $oLanguageQuery = LanguageQuery::create();
	    $oLanguageQuery->setQueryKey('LanguageQuery-filterByIsEnabledWebshop');

        $aData['languages'] = $oLanguageQuery->filterByIsEnabledWebshop(true)->find();
        $aData['settings'] = getSiteSettings();

        $aData['site'] = $this->getSite();
        $aData['own_company'] = $this->getSite()->getCompany();
        if(isset($_SESSION['CURRENT_SALE_ORDER']))
        {
            $aData['current_order'] = $this->getCurrentSaleOrder();
        }

        $aData['show_cookie_warning'] = isset($_SESSION['hide_cookie_warning']) ? false : true;

        $aData['customer'] = [];
        $aData['customer']['is_signed_in'] = Customer::isSignedIn();
        $aData['current_language'] = $oCurrentLanguage;
        $aData['url_prefix'] = '/'.$oCurrentLanguage->getShopUrlPrefix();
        return parent::parse($sTemplate, $aData);
    }

    function run()
    {
        $oCurrentLanguage = $this->getCurrentLanguage();

	    $oSiteQuery = SiteQuery::create();
	    $oSiteQuery->setQueryKey('SiteQuery-findOneByDomain');

        $oSite = $oSiteQuery->findOneByDomain(getSiteSettings()['db_domain']);


        $aFooter = FooterHelper::getFooter($oSite, $oCurrentLanguage);
        $aOpeningHours = OpeningHoursHelper::getOpeningHours();
        if($this->getRequestUri() == '/' && $oCurrentLanguage instanceof Language)
        {
            if(!trim($oCurrentLanguage->getShopUrlPrefix()))
            {
                throw new LogicException("ShopUrlPrefix is not configured for the default webshop language.");
            }
            $this->redirect('/'.$oCurrentLanguage->getShopUrlPrefix(), 301);
        }
        $aResult = $this->runShop();
        if(Customer::isSignedIn())
        {
            $oCustomer = Customer::getMember();
            $iCustomerWishlistCount = CustomerWishlistQuery::create()->filterByCustomerId($oCustomer->getId())->count();
            $aResult['wishlist_count'] = $iCustomerWishlistCount;
        }
        $aResult['top_nav_pages'] = SitePageQuery::create()->filterByLanguageId($oCurrentLanguage->getId())->filterByTag('top_nav')->find();
        $aSiteSettings = getSiteSettings();
        $sMenuHelperClassName = '\\'.$aSiteSettings['namespace'].'\\MenuHelper';
        if(class_exists($sMenuHelperClassName))
        {
            $oMenuHelper = new $sMenuHelperClassName();
        }
        if(isset($oMenuHelper) && $oMenuHelper instanceof AbstractMenuHelper)
        {
            $aResult['categories'] = $oMenuHelper->getTranslatedMainMenu($oCurrentLanguage->getId());
        }


        $aResult['footer_items'] = $aFooter;
        $aResult['opening_hours'] = $aOpeningHours;

        if(!isset($aResult['meta_description']))
        {
            $aResult['meta_description'] = Translate::fromCode("meta description");
        }
        if(!isset($aResult['keywords']))
        {
            $aResult['keywords'] = Translate::fromCode("meta keywords");
        }

        $aResult['author'] = Translate::fromCode("NuiCart - http://nuicart.nl");
        return $aResult;
    }
}
