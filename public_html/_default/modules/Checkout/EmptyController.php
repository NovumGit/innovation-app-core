<?php
namespace _Default\Checkout;


use _Default\_DefaultController;
use Core\Translate;

class EmptyController extends _DefaultController  {

    function runShop()
    {
        $aResult['content'] = $this->parse('Checkout/empty.twig', []);
        $aResult['title'] =  Translate::fromCode("Checkout | Your basket is empty");
        return $aResult;
    }

}