<?php
namespace _Default\Checkout;

use _Default\_DefaultController;
use Core\Customer;
use Core\DeferredAction;
use Core\Location;
use Core\Logger;
use Core\Mailer;
use Core\Setting;
use Core\StatusMessage;
use Core\Translate;
use Exception\LogicException;
use Model\Crm\Customer as CustomerModel;
use Model\Crm\CustomerQuery;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderPayment;
use Model\Setting\MasterTable\Base\Sale_order_notification_type;
use Model\Setting\MasterTable\Base\ShippingMethodQuery;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\CountryShippingMethodQuery;
use Model\Setting\MasterTable\PaymentMethod;
use Model\Setting\MasterTable\PaymentMethodQuery;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;
use Model\Setting\MasterTable\ShippingMethod;
use Model\Setting\MasterTable\UsaStateQuery;
use Psp\AbstractPsp;
use Psp\Paymethods;

class FinalController extends _DefaultController
{
	protected function saveShippingDetails(SaleOrder $oSaleOrder, $aData)
	{
		// Does nothing but may be used in subclasses.
	}
	protected function savePaymentDetails(SaleOrder $oSaleOrder, PaymentMethod $oPaymentMethod, array $aData):string
	{
		$oSaleOrderPayment = new SaleOrderPayment();
		$oSaleOrderPayment->setSaleOrderId($oSaleOrder->getId());
		$oSaleOrderPayment->setPaymentMethodId($oPaymentMethod->getId());
		$oSaleOrderPayment->setTotalPrice($oSaleOrder->getTotalPrice());
		$oSaleOrderPayment->setStartedOn(time());
		$sSecretHash = sha1(time().$_SERVER['REMOTE_ADDR'].'x');
		$oSaleOrderPayment->setOurSecretHash($sSecretHash);
		$oSaleOrderPayment->setIsPaid(false);
		$oSaleOrderPayment->save();

		$sPaymentObjectName = '\\Psp\\Psp'.$oPaymentMethod->getPsp();
		$oPaymentObject = new $sPaymentObjectName();

		if(!$oPaymentObject instanceof AbstractPsp)
		{
			throw new LogicException("Could not find Payment service provider object.");
		}
		$sUrl = $oPaymentObject->getPayUrl($oSaleOrder, $oSaleOrderPayment, $oPaymentMethod->getCode(), $aData['issuer_'.$aData['paymethod']]);
		return $sUrl;
	}
    function doComplete()
    {
        $oSaleOrder = $this->getCurrentSaleOrder();
        $oLanguage = $this->getCurrentLanguage();
        if($oSaleOrder->getSaleOrderItems()->count() == 0)
        {
            StatusMessage::warning("Your basket is empty.");
            $this->redirect('/'.$oLanguage->getShopUrlPrefix().'/checkout/empty');
        }

        $aData = $this->post('data');
        $aErrors = Helper::validate($aData, $oSaleOrder);
        $oPaymentMethod = PaymentMethodQuery::create()->findOneById($aData['paymethod']);

        if(empty($aErrors))
        {
            if(Customer::isSignedIn())
            {
                $oCustomer = Customer::getMember();
            }
            else
            {
                // The user is currently not signed in
                $oCustomer = CustomerQuery::create()->findOneByEmail($aData['customer_email']);
                if (!$oCustomer instanceof CustomerModel)
                {
                    // The emailaddress does not belong to a customer already
                    $oCustomer = Helper::createCustomer($aData, $this->getCurrentLanguage());
                }
            }
            $oCustomer->setLanguageId($oLanguage->getId());
            $oCustomer->save();
            Customer::setMember($oCustomer);

            Helper::updateAddressbook($oCustomer, $aData);
            $oSaleOrder = $this->getCurrentSaleOrder();
            Helper::fillInSaleOrder($oSaleOrder, $oCustomer, $aData);

	        $this->saveShippingDetails($oSaleOrder, $aData);
	        $sPaymentUrl = $this->savePaymentDetails($oSaleOrder, $oPaymentMethod, $aData);

            $oSale_order_notification_type = Sale_order_notification_typeQuery::create()->findOneByCode('thanks_for_order');
            if($oSale_order_notification_type instanceof Sale_order_notification_type)
            {
                Logger::info("Sending thanks for your order e-mail.");
                Mailer::sendSaleOrderStatusMail($oSaleOrder->getId(), $oSale_order_notification_type->getId());
            }
            else
            {
                Logger::info("Not sending thanks for your order e-mail, there is no notification type with code thanks_for_order.");
            }

            if(empty($sPaymentUrl))
            {
                throw new LogicException("Unable to fetch Pay url");
            }
            $this->redirect($sPaymentUrl);
        }
        else
        {
            $sErrorMessage  = Helper::makeErrorHtml($aErrors);
            StatusMessage::warning($sErrorMessage, Translate::fromCode("Could not process order"));
        }
    }
    function doChangeWrapping()
    {
        $aData = $this->post('data');
        $bSpecialWrap = (isset($aData['special_wrap']) && $aData['special_wrap'] == '1');
        $oSaleOrder = $this->getCurrentSaleOrder();
        $oSaleOrder->setHasWrap($bSpecialWrap);
        if($bSpecialWrap)
        {
            $oSaleOrder->setWrappingPrice(Setting::get('special_wrap_fee'));
        }
        else
        {
            $oSaleOrder->setWrappingPrice(0);
        }
        $oSaleOrder->save();
        $this->redirect($this->getRequestUri());
    }
    function doChangeCountry()
    {
        $aData = $this->post('data');
        $oSaleOrder = $this->getCurrentSaleOrder();
        Helper::changeShipping($oSaleOrder, $aData);
    }
    function doChangeShipping()
    {
        $aData = $this->post('data');
        $oSaleOrder = $this->getCurrentSaleOrder();
        Helper::changeShipping($oSaleOrder, $aData);
    }
    function runShop()
    {
        $aSiteSettings = getSiteSettings();
        $oLanguage = $this->getCurrentLanguage();
        // $_GET['email'] is filled in aftger a failed login attempt
        $sGetEmail = $this->get('email');
        $oSaleOrder = $this->getCurrentSaleOrder();

        if(in_array($this->post('_do'), ['ChangeCountry', 'ChangeShipping', 'Complete']))
        {
            $aData = $this->post('data');

            if(isset($aData['different_address']))
            {
                $oSaleOrder->setCustomerDeliveryCountry($aData['customer_delivery_country']);
            }
            else
            {
                $oSaleOrder->setCustomerDeliveryCountry($aData['customer_invoice_country']);
            }

            $oCurrentUserCountry = CountryQuery::create()->findOneByName($oSaleOrder->getCustomerDeliveryCountry());
            $aShippingMethods = CountryShippingMethodQuery::getShippingMethodsByCountryIdAndWeight($oCurrentUserCountry->getId(), $oSaleOrder->calcWeight());
        }
        else
        {
            if(!Customer::isSignedIn())
            {
                $oCurrentUserCountry = Location::detectCountryByIp($_SERVER['REMOTE_ADDR']);
                $aData = Helper::defaultFormSettings(new CustomerModel(), $oCurrentUserCountry);
            }
            else
            {
                $oCurrentCustomer = Customer::getMember();

                if($oSaleOrder->getCustomerDeliveryCountry())
                {
                    $aData = Helper::formFromSaleOrderObject($oSaleOrder);
                }
                else
                {
                    $aData = Helper::defaultFormSettings(Customer::getMember(), $oCurrentCustomer->detectCountry());
                }

                if(isset($aData['customer_delivery_country_object']))
                {
                    $oCurrentUserCountry = $aData['customer_delivery_country_object'];
                }
                else
                {
                    $oCurrentUserCountry = CountryQuery::create()->findOneByIso2('NL');
                    // throw new LogicException("Could not detect a country for the customer, this is an issue that must be resolved.");
                }
            }
            $aShippingMethods = CountryShippingMethodQuery::getShippingMethodsByCountryIdAndWeight($oCurrentUserCountry->getId(), $oSaleOrder->calcWeight());


            Logger::custom('---- Before shippingmethod filter', Logger::getLogdir().'/shipping-methods');
            Logger::custom(print_r($aShippingMethods, true), Logger::getLogdir().'/shipping-methods');

            $sFullShippingMethodFilterClass  = '\\'.$aSiteSettings['namespace'].'\\Checkout\\ShippingMethodFilter';

            if(class_exists($sFullShippingMethodFilterClass))
            {
                $oFullShippingMethodFilterClass = new $sFullShippingMethodFilterClass();
                if($oFullShippingMethodFilterClass instanceof AbstractShippingMethodFilter)
                {
                    $aShippingMethods = $oFullShippingMethodFilterClass->filter($oSaleOrder, $aShippingMethods);
                }
            }
            Logger::custom('---- After shippingmethod filter', Logger::getLogdir().'/shipping-methods');

            Logger::custom(print_r($aShippingMethods, true), Logger::getLogdir().'/shipping-methods');
            Logger::custom(print_r($oSaleOrder, true), Logger::getLogdir().'/shipping-methods');

            $aDefaultShippingmethod = current($aShippingMethods);
            $oDefaultShippingMethod = $aDefaultShippingmethod['method'];

            if( !$oDefaultShippingMethod instanceof ShippingMethod)
            {
            	$sMsg = 'No shippingmethod available for order/country/weight :'.$oSaleOrder->getId().'/'.$oSaleOrder->getCustomerDeliveryCountry().'/thickness-is-calculated-laster';
                Logger::custom($sMsg, Logger::getLogdir().'/shipping-methods');
                mail('anton@nui-boutkam.nl', $sMsg, $sMsg);
                // throw new LogicException("No shippingmethod available for this order/country/weight.");
            }

            if($oDefaultShippingMethod instanceof ShippingMethod)
            {
                $oCountryShippingMethod = CountryShippingMethodQuery::create()
                    ->filterByCountryId($oCurrentUserCountry->getId())
                    ->filterByShippingMethodId($oDefaultShippingMethod->getId())
                    ->findOne();
                $aData['shipping'] = $oDefaultShippingMethod->getId();

                $oSaleOrder->setShippingMethodId($oDefaultShippingMethod->getId());
                if($oSaleOrder->calcHasFreeShipping())
                {
                    $oSaleOrder->setShippingPrice(0);
                }
                else
                {
                    $oSaleOrder->setShippingPrice($oCountryShippingMethod->getPrice());
                }
                $oSaleOrder->setShippingCarrier($oDefaultShippingMethod->getName());
                $oSaleOrder->save();
            }
            if($oPaymentMethod = PaymentMethodQuery::create()->findOneByCode('creditcard'))
            {
                $aData['paymethod'] = $oPaymentMethod->getId();
            }
        }
        if($oCurrentUserCountry->getStealthMandatory())
        {
            $oSaleOrder->setHasWrap(true);
            $oSaleOrder->setWrappingPrice(Setting::get('special_wrap_fee'));
            $oSaleOrder->save();
        }
        if($oSaleOrder->getSaleOrderItems()->count() == 0)
        {
            StatusMessage::warning("Your basket is empty.");
            $this->redirect('/'.$oLanguage->getShopUrlPrefix().'/checkout/empty');
        }
        $aPaymentMethods = Paymethods::getPaymethods($oCurrentUserCountry->getIso2(), 'EUR', $oSaleOrder->getTotalPrice());

        DeferredAction::register('checkout', $this->getRequestUri(false));
        $sFullShippingMethodFilterClass  = '\\'.$aSiteSettings['namespace'].'\\Checkout\\ShippingMethodFilter';
        if(class_exists($sFullShippingMethodFilterClass))
        {
            $oFullShippingMethodFilterClass = new $sFullShippingMethodFilterClass();
            if($oFullShippingMethodFilterClass instanceof AbstractShippingMethodFilter)
            {
                $aShippingMethods = $oFullShippingMethodFilterClass->filter($oSaleOrder, $aShippingMethods);
            }
        }

        if(isset($aData['shipping']))
        {
	        $aData['current_shipping_method'] =	ShippingMethodQuery::create()->findOneById($aData['shipping']);
        }



	    $aViewData = [
            'r' => 'checkout',
            'email' => $sGetEmail,
            'is_signed_in' => Customer::isSignedIn(),
            'failed_login_attempt' => $sGetEmail ? true : false,
            'sale_order' => $oSaleOrder,
            'countries' => CountryQuery::create()->orderByName()->find(),
            'states' => UsaStateQuery::create()->orderByName()->find(),
            'current_shipping_country' => $oCurrentUserCountry,
            'special_wrap_fee' => Setting::get('special_wrap_fee'),
            'shipping_methods' => $aShippingMethods,
            'payment_methods' => $aPaymentMethods,
            'data' => $aData
        ];
        $aResult['content'] = $this->parse('Checkout/final.twig', $aViewData);
        $aResult['title'] =  Translate::fromCode("Checkout").' '.$oSaleOrder->getId();
        return $aResult;
    }
}
