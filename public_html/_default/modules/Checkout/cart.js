var oShippingMethod = $('.shipping_method');
oShippingMethod.change(function()
{
    $('#fld_do').val('ChangeShipping');
    $('#frm_checkout').submit();

});


// This code is used in more then one occasion (checkout, home)
$('.delete_cart_order_item').click(function(e){
    $(this).parent().parent().fadeOut(200);
    e.preventDefault();

    $.ajax($(this).attr('href'),
    {
        dataType: "json",
        data : {order_item_id : $(this).data('order_item_id')},
        error : function(e) {
            console.log(e);
        },
        success : function(data) {
            window.location = window.location;
        }
    });
});

$('#fld_update_cart').click(function(e){
    e.preventDefault();
    $('#fld_do').val('UpdateCart');
    $('#frm_checkout').submit();
});


$('#fld_apply_coupon').click(function(e)
{
    $('#fld_do').val('ApplyCoupon');
    e.preventDefault();
    $('#frm_checkout').submit();
});