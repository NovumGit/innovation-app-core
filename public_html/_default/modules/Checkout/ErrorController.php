<?php
namespace _Default\Checkout;

use _Default\_DefaultController;
use Core\StatusMessage;
use Core\Translate;

class ErrorController extends _DefaultController  {

    function runShop()
    {
        $oLanguage = $this->getCurrentLanguage();
        $sMessage = "You came back from website of the payment service provider, we have redirected you to the payment page because apparently something went wrong when you tried to make a payment. Please contact us if you are experiencing problems with our system";
        StatusMessage::warning(Translate::fromCode($sMessage));
        $this->redirect('/'.$oLanguage->getShopUrlPrefix().'/checkout/final');
        exit();
    }
}
