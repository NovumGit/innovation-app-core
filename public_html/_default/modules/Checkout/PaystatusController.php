<?php
namespace _Default\Checkout;

use _Default\_DefaultController;
use AdminModules\Order\Event\Send_status_email;
use Model\Sale\SaleOrderPayment;
use Model\Sale\SaleOrderPaymentQuery;
use Model\Sale\SaleOrderQuery;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;

class PaystatusController extends _DefaultController
{
    function runShop()
    {
        $fAmount = $this->get('Amount');
        // Filteren op alleen id is ook mogelijk maar die postback key is alleen bij ons en bij Icepay bekend.
        // Extra veiligheidje dus.
        $oSaleOrderPaymentQuery = SaleOrderPaymentQuery::create();
        $oSaleOrderPaymentQuery->filterById($this->get('OrderID'));
        $oSaleOrderPaymentQuery->filterByOurSecretHash($this->get('postback_key'));
        $oSaleOrderPayment = $oSaleOrderPaymentQuery->findOne();

        if($oSaleOrderPayment instanceof SaleOrderPayment)
        {
            $aAmount = round($oSaleOrderPayment->getTotalPrice(),2);
            $aAmount = $aAmount * 100;
            if($aAmount == $fAmount)
            {
                $oSale_order_notification_type = Sale_order_notification_typeQuery::create()->findOneByCode('invoice_email');
                $oSaleOrderPayment->setIsPaid(true);
                $oSaleOrderPayment->save();

                $iOrderId = $oSaleOrderPayment->getSaleOrderId();

                $oSendStatusEmail = new Send_status_email(
                    [
                        'order_id' => $iOrderId,
                        'notification_type_id' => $oSale_order_notification_type->getId()
                    ]
                    ,[]);
                $oSendStatusEmail->trigger('overview');

                $oSaleOrder = SaleOrderQuery::create()->findOneById($oSaleOrderPayment->getSaleOrderId());
                $oSaleOrder->setIsFullyPaid(true);
                $oSaleOrder->save();
            }
        }
        exit('done');
    }
}
