<?php
namespace _Default\Checkout;

use _Default\_DefaultController;
use Core\ButtonEventDispatcher;
use Core\Translate;
use Model\Cms\SitePage;
use Model\Cms\SitePageQuery;
use Model\Cms\SiteQuery;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderPayment;
use Model\Sale\SaleOrderPaymentQuery;
use Model\Setting\MasterTable\PaymentMethodQuery;
use Psp\CheckPaymentInterface;

class ThanksController extends _DefaultController
{

    function runShop()
    {
        $oCurrentSaleOrder = $this->getCurrentSaleOrder();

        if(isset($_SESSION['CURRENT_SALE_ORDER']))
        {
            unset($_SESSION['CURRENT_SALE_ORDER']);
        }

        if(!$oCurrentSaleOrder instanceof SaleOrder)
        {
            $this->redirect('/');
        }

        $iPaymethodId = $oCurrentSaleOrder->getPaymethodId();

        if($iPaymethodId)
        {
            $oPaymentMethod = PaymentMethodQuery::create()->findOneById($iPaymethodId);
            $sPaymentObjectName = '\\Psp\\Psp'.$oPaymentMethod->getPsp();
            $oPaymentObject = new $sPaymentObjectName();

            if($oPaymentObject instanceof CheckPaymentInterface)
            {
                $iTrxId = $this->get('trxid');
                $oSaleOrderPayment = new SaleOrderPayment();
                if($iTrxId)
                {
                    $oSaleOrderPaymentQuery = SaleOrderPaymentQuery::create();
                    $oSaleOrderPaymentQuery->filterBySaleOrderId($oCurrentSaleOrder->getId());
                    $oSaleOrderPaymentQuery->filterByTrxid($this->get('trxid'));
                    $oSaleOrderPayment = $oSaleOrderPaymentQuery->findOne();

                }

                // PsPPaycheckout
                $iPayCheckoutReference = $this->get('PayCheckoutReference');
                $iPayCheckoutTrace = $this->get('PayCheckoutTrace');

                if($iTrxId)
                {
                    $oSaleOrderPaymentQuery = SaleOrderPaymentQuery::create();
                    $oSaleOrderPaymentQuery->filterBySaleOrderId($oCurrentSaleOrder->getId());
                    $oSaleOrderPaymentQuery->filterByPaymentReference($iPayCheckoutReference);
                    $oSaleOrderPaymentQuery->filterByTransactionReference($iPayCheckoutTrace);
                    $oSaleOrderPayment = $oSaleOrderPaymentQuery->findOne();
                }

                $bOrderPaid = $oPaymentObject->checkPayment($oCurrentSaleOrder, $oSaleOrderPayment);

                if($bOrderPaid)
                {
                    // @todo, dit moet op een mooiere manier geregeld worden.
                    $iOrderPaidButtonId = 10;
                    $bIsLast = true;
                    ButtonEventDispatcher::Dispatch($iOrderPaidButtonId, ['order_id' => $oCurrentSaleOrder->getId()], $bIsLast);
                }
            }
        }

        $oCurrentSaleOrder->setIsClosed(true);
        $oCurrentSaleOrder->save();

        $_SESSION['CURRENT_SALE_ORDER'] = null;
        unset($_SESSION['CURRENT_SALE_ORDER']);

        $oLanguage = $this->getCurrentLanguage();
        $aSiteSettings = getSiteSettings();
        $oSite = SiteQuery::create()->findOneByDomain($aSiteSettings['live_domain']);

        $oSitePage = SitePageQuery::create()
                        ->filterBySite($oSite)
                        ->filterByLanguage($oLanguage)
                        ->filterByTag('thank-you')
                        ->findOne();

        if(!$oSitePage instanceof SitePage)
        {
            $oSitePage = new SitePage();
            $oSitePage->setSiteId($oSite->getId());
            $oSitePage->setTag('thank-you');
            $oSitePage->setLanguageId($oLanguage->getId());
            $oSitePage->setContent('You can now safely light a spliff, sit back, relax and for instance watch this Youtube video!');
            $oSitePage->setTitle('Thank you!');
            $oSitePage->save();
        }

        $aViewData =
        [
            'site_page' => $oSitePage
        ];
        $aResult['content'] = $this->parse('Checkout/thanks.twig', $aViewData);
        $aResult['title'] =  Translate::fromCode("Checkout | Thanks");
        return $aResult;
    }
}
