<?php
namespace _Default\Checkout;

use Model\Sale\SaleOrder;

abstract class AbstractShippingMethodFilter {
    abstract function filter(SaleOrder $oSaleOrder, $aShippingMethods):array;
}
