<?php
namespace _Default\Checkout;

use _Default\_DefaultController;
use Core\Customer;
use Core\Location;
use Core\StatusMessage;
use Core\Translate;
use Exception\LogicException;
use Helper\CouponHelper;
use Model\Marketing\CouponQuery;
use Model\Product;
use Model\Setting\MasterTable\CountryShippingMethodQuery;
use Model\Setting\MasterTable\ShippingMethodQuery;

class CartController extends _DefaultController  {

    private $aRedeemCodeMessages = [];

    function __construct($aGet, $aPost)
    {
        // Ivm vertalen hier al vastgelegd (vertaalbestanden worden direct aangemaakt).
        $this->aRedeemCodeMessages = [
            'redeem_code_no_start_date' => Translate::fromCode('That redeem code does not have a starting date defined, this is why we cannot validate it.'),
            'redeem_code_no_end_date' => Translate::fromCode('That redeem code does not have an ending date, this is why it is currently invalid.'),
            'redeem_code_not_found' => Translate::fromCode('That redeem code is not found in our system.'),
            'redeem_code_not_valid_yet' => Translate::fromCode('That redeem code is not valid yet but it is available in the system, please check your voucher.'),
            'redeem_code_not_valid_anymore' => Translate::fromCode('Sorry but that redeem code is expired.'),
            'redeem_code_already_redeemed' =>  Translate::fromCode('Sorry that coupon code has been used already.'),
        ];
        parent::__construct($aGet, $aPost);
    }
    function doApplyCoupon()
    {
        $sCoupon = $this->post('coupon');

        if(empty($sCoupon))
        {
            StatusMessage::warning(Translate::fromCode("Please specify a coupon code before redeeming it."));
            $this->redirect($this->getRequestUri());
        }
        else if($sInvalidReasonCode = CouponHelper::isInvalid($sCoupon))
        {

            if(!isset($this->aRedeemCodeMessages[$sInvalidReasonCode]))
            {
                throw new LogicException(Translate::fromCode("Could not validate your redeem code, something must have gone wrong."));
            }
            else
            {
                StatusMessage::warning($this->aRedeemCodeMessages[$sInvalidReasonCode]);
            }
        }
        else
        {
            $oSaleOrder = $this->getCurrentSaleOrder();
            $oCoupon = CouponQuery::create()->findOneByRedeemCode($sCoupon);
            $oCouponType = $oCoupon->getCouponType();

            $oSaleOrder->setCouponCode($sCoupon);
            $oSaleOrder->setCouponDiscountAmount($oCouponType->getDiscountAmount());
            $oSaleOrder->setCouponDiscountPercentage($oCouponType->getDiscountPercentage());
            $oSaleOrder->save();

            $oCoupon->setGivenOut(true);
            $oCoupon->save();
            StatusMessage::success(Translate::fromCode('Yay, you have discount!'));
        }
        $this->redirect($this->getRequestUri());
    }
    function doUpdateCart()
    {
        $oSaleOrder = $this->getCurrentSaleOrder();
        $aOrderItems = $oSaleOrder->getSaleOrderItems();
        $aQuantities = $this->post('new_quantities');

        if(!$aOrderItems->isEmpty())
        {
            foreach($aOrderItems as $oOrderItem)
            {
                if(isset($aQuantities[$oOrderItem->getId()]))
                {
                    if($aQuantities[$oOrderItem->getId()] <= 0)
                    {
                        $oOrderItem->delete();
                    }
                    else
                    {
                        $oProduct = $oOrderItem->getFirstProduct();

                        if($oProduct instanceof Product && $oProduct->getUnitCode() == 'cm')
                        {
                            $oOrderItem->setQuantity($aQuantities[$oOrderItem->getId()] / 100);
                            $oOrderItem->save();
                        }
                        else
                        {
                            $oOrderItem->setQuantity($aQuantities[$oOrderItem->getId()]);
                            $oOrderItem->save();
                        }
                    }
                }
            }
        }
        StatusMessage::success("Quantities changed", "Change quantities");
        $this->redirect($this->getRequestUri());
    }
    function doChangeShipping()
    {
        $aData = $this->post('data');
        if(!Customer::isSignedIn())
        {
            $oCurrentUserCountry = Location::detectCountryByIp($_SERVER['REMOTE_ADDR']);
        }
        else
        {
            $oCurrentCustomer = Customer::getMember();
            $oCurrentUserCountry = $oCurrentCustomer->detectCountry();
        }

        $oCountryShipping = CountryShippingMethodQuery::create()
            ->filterByCountryId($oCurrentUserCountry->getId())
            ->filterByShippingMethodId($aData['shipping'])
            ->findOne();

        $oShippingMethod = ShippingMethodQuery::create()->findOneById($aData['shipping']);
        $oSaleOrder = $this->getCurrentSaleOrder();
        $oSaleOrder->setShippingMethodId($oShippingMethod->getId());
        $oSaleOrder->setShippingHasTrackTrace($oShippingMethod->getIsTrackTrace());
        $oSaleOrder->setShippingPrice($oCountryShipping->getPrice());
        $oSaleOrder->setShippingCarrier($oShippingMethod->getName());
        $oSaleOrder->save();
    }
    function runShop()
    {
	    $aSiteSettings = getSiteSettings();

	    $aData = $this->post('data');
        $oLanguage = $this->getCurrentLanguage();
        $oSaleOrder = $this->getCurrentSaleOrder();


        if($oSaleOrder->getSaleOrderItems()->count() == 0 )
        {
            $this->redirect('/'.$oLanguage->getShopUrlPrefix().'/checkout/empty');
        }

        $aShippingMethods = null;
        if(!Customer::isSignedIn())
        {
            $oCurrentUserCountry = Location::detectCountryByIp($_SERVER['REMOTE_ADDR']);
            $aShippingMethods = CountryShippingMethodQuery::getShippingMethodsByCountryIdAndWeight($oCurrentUserCountry->getId(), $oSaleOrder->calcWeight());
        }
        else
        {
            $oCurrentCustomer = Customer::getMember();
            $oCurrentUserCountry = $oCurrentCustomer->detectCountry();
            $aShippingMethods = CountryShippingMethodQuery::getShippingMethodsByCountryIdAndWeight($oCurrentUserCountry->getId(), $oSaleOrder->calcWeight());
        }

	    $sFullShippingMethodFilterClass  = '\\'.$aSiteSettings['namespace'].'\\Checkout\\ShippingMethodFilter';

	    if(class_exists($sFullShippingMethodFilterClass))
	    {
		    $oFullShippingMethodFilterClass = new $sFullShippingMethodFilterClass();
		    if($oFullShippingMethodFilterClass instanceof AbstractShippingMethodFilter)
		    {
			    $aShippingMethods = $oFullShippingMethodFilterClass->filter($oSaleOrder, $aShippingMethods);
		    }
	    }


        $bHasFreeShipping = $oSaleOrder->calcHasFreeShipping();

        $aViewData = [
            'shipping_methods' =>  $aShippingMethods,
            'data' => $aData,
            'has_free_shipping' => $bHasFreeShipping,
            'sale_order' => $oSaleOrder
        ];

        $aResult['content'] = $this->parse('Checkout/cart.twig', $aViewData);

        $aResult['title'] =  Translate::fromCode("Checkout");
        return $aResult;
    }
}
