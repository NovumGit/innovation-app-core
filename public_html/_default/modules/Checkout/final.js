// alert('x');
var oTogglePaymethod = $('.toggle_paymethod');

oTogglePaymethod.click(function()
{
    var sItemID = '#' + $(this).attr('for');
    $(sItemID).prop('checked', true);
});

var oTogglePaymethodRadio =$('.toggle_paymethod_radio');
var oShippingMethod = $('.shipping_method');
oShippingMethod.change(function()
{
    $('#fld_do').val('ChangeShipping');
    $('#frm_checkout').submit();

});
var oShipBox = $('#ship-box');
oShipBox.change(function()
{
    $('#fld_do').val('ChangeShipping');
    $('#frm_checkout').submit();
});

oTogglePaymethodRadio.change(function()
{
    $('.toggle_paymethod a', $(this).parent()).click();
});


if($('a', $('.toggle_paymethod_radio:checked').parent()).length == 1)
{
    setTimeout(function()
    {
        $('a', $('.toggle_paymethod_radio:checked').parent()).click();
    }, 2000);

}

if($('.shipping_method').length == 1)
{
    $('.shipping_method:first').prop("checked", true);
}


var oInvoiceCountry = $('#fld_invoice_country');
oInvoiceUsaStateContainer = $('#fld_invoice_usa_state_container');

oDeliveryCountry = $('#fld_delivery_country');
oDeliveryUsaStateContainer = $('#fld_delivery_usa_state_container');

oInvoiceCountry.change(function()
{
    $('#fld_do').val('ChangeCountry');
    $('#frm_checkout').submit();
    /*
    var sNewDisplay = (oInvoiceCountry.val() == 'United States of America') ? 'block' : 'none';
    oInvoiceUsaStateContainer.css('display', sNewDisplay);
    */
});

oDeliveryCountry.change(function()
{
    $('#fld_do').val('ChangeCountry');
    $('#frm_checkout').submit();

    var sNewDisplay = (oDeliveryCountry.val() === 'United States of America') ? 'block' : 'none';
    oDeliveryUsaStateContainer.css('display', sNewDisplay);
});


$('#fld_special_wrap').on('change', function()
{
    $('#fld_do').val('ChangeWrapping');
    $('#frm_checkout').submit();
});


