<?php
namespace _Default\Checkout\Psp;

use Model\Setting\MasterTable\PaymentMethodQuery;

class Paymethods
{
    /*
    static function getIcepayPaymethods($sCountryCode, $sCurrency, $fAmount)
    {
        $fAmountUsStyle = str_replace(',', '.', $fAmount);
        $fAmountRounded = round($fAmountUsStyle, 2);
        $fAmountInCents = (int) str_replace('.', '', $fAmountRounded);

        $aGet = [];
        $aGet['country_code'] = $sCountryCode;
        $aGet['amount'] = $fAmountInCents;
        $aGet['currency'] = $sCurrency;

        $sQuery = http_build_query($aGet);
        $sPaymentMethods = Curl::callUrl('getpaymentmethods.php?'.$sQuery);
        $aPaymentMethods = json_decode($sPaymentMethods, true);

        $aOut = null;
        if(!empty($aPaymentMethods))
        {
            $aOut = [];
            foreach($aPaymentMethods as $aPaymentMethod)
            {
                $aOut[$aPaymentMethod['PaymentMethodCode']] = $aPaymentMethod;
            }
        }
        return $aOut;
    }
    */
    static function getPaymethods($sCountryCode, $sCurrency, $fAmount)
    {
        $aOut = [];
        $aPaymentMethods = PaymentMethodQuery::create()->filterByIsEnabledWebshop(true)->find();
        $aIcepayPaymentMethods = self::getIcepayPaymethods($sCountryCode, $sCurrency, $fAmount);

        foreach($aPaymentMethods as $oPaymentMethod)
        {
            if($oPaymentMethod->getPsp() == 'IcePay' && !isset($aIcepayPaymentMethods[$oPaymentMethod->getCode()]))
            {
                // Deze betaalmethod is niet beschikbaar voor deze land+currency+amount combo.
                continue;
            }
            $aItem['id'] = $oPaymentMethod->getId();
            $aItem['description'] = $oPaymentMethod->getName();
            $aItem['code'] = $oPaymentMethod->getCode();

            if ($oPaymentMethod->getPsp() == 'IcePay')
            {
                $aItem['issuers'] = $aIcepayPaymentMethods[$oPaymentMethod->getCode()]['Issuers'];
            }
            $aOut[] = $aItem;
        }
        return $aOut;
    }
/*
    public static function getPayUrl(SaleOrder $oSaleOrder, SaleOrderPayment $oSaleOrderPayment, $sPaymentMethodCode, $sIssuerId)
    {
        $sDeliveryCountry = $oSaleOrder->getCustomerDeliveryCountry();
        $oDeliveryCountry = CountryQuery::create()->findOneByName($sDeliveryCountry);

        if(!$oDeliveryCountry instanceof Country)
        {
            throw new LogicException("Delivery Country not set.");
        }

        $fAmountUsStyle = str_replace(',', '.', $oSaleOrder->getTotalPrice());
        $fAmountRounded = round($fAmountUsStyle, 2);
        $fAmountInCents = (int) str_replace('.', '', $fAmountRounded);

        $iLanguageId = $oSaleOrder->getLanguageId();
        $oLanguage = LanguageQuery::create()->findOneById($iLanguageId);
        $sLanguage = strtoupper($oLanguage->getShopUrlPrefix());

        $aVars['country_code'] = $oDeliveryCountry->getIso2();
        $aVars['amount'] = $fAmountInCents;
        $aVars['language'] = strtoupper($sLanguage);
        $aVars['currency'] = 'EUR';
        $aVars['payment_method'] = $sPaymentMethodCode;
        $aVars['issuer_id'] = $sIssuerId;
        $aVars['our_ref'] = "Payment for order ".$oSaleOrder->getOrderNumber();
        $aVars['order_id'] = $oSaleOrderPayment->getId();
        $sQuery = http_build_query($aVars);

        $sResult = Curl::callUrl('transactionrequest.php?'.$sQuery);
        $aResult = json_decode($sResult, true);
        if(isset($aResult['url']))
        {
            return $aResult['url'];
        }
        throw new LogicException("Expected an url as response from the payment service provider but got $sResult.");
    }
*/
}
