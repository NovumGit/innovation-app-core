<?php
namespace _Default\Checkout;

use Core\Translate;
use Exception\LogicException;
use Model\Cms\SiteQuery;
use Model\Company\CompanyQuery;
use Model\Crm\Customer;
use Model\Crm\CustomerAddress;
use Model\Crm\CustomerAddressQuery;
use Model\Crm\CustomerAddressType;
use Model\Crm\CustomerAddressTypeQuery;
use Model\Crm\CustomerCompany;
use Model\Crm\CustomerTypeQuery;
use Model\Sale\SaleOrder;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\CountryShippingMethod;
use Model\Setting\MasterTable\CountryShippingMethodQuery;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\PaymentMethodQuery;
use Model\Setting\MasterTable\PayTerm;
use Model\Setting\MasterTable\PayTermQuery;
use Model\Setting\MasterTable\SaleOrderStatus;
use Model\Setting\MasterTable\SaleOrderStatusQuery;
use Model\Setting\MasterTable\ShippingMethodQuery;
use Model\Setting\MasterTable\UsaStateQuery;

class Helper
{
    static function getRedirectUrl(SaleOrder $oSaleOrder, Language $oLanguage)
    {
        $sUrl = null;
        $sPrefix = '/'.$oLanguage->getShopUrlPrefix().'/';

        if($oSaleOrder->getPaymethodCode() == 'cash')
        {
            $sUrl = $sPrefix.'payment_instructions/cash?id='.$oSaleOrder->getId();
        }
        else if($oSaleOrder->getPaymethodCode() == 'bank')
        {
            $sUrl = $sPrefix.'payment_instructions/bank?id='.$oSaleOrder->getId();

        }
        else if($oSaleOrder->getPaymethodCode() == 'mrcash')
        {
            $sUrl = $sPrefix.'checkout/thanks?id='.$oSaleOrder->getId();
        }
        else if($oSaleOrder->getPaymethodCode() == 'ideal')
        {
            $sUrl = $sPrefix.'checkout/thanks?id='.$oSaleOrder->getId();
        }
        else if($oSaleOrder->getPaymethodCode() == 'paypal')
        {
            $sUrl = $sPrefix.'checkout/thanks?id='.$oSaleOrder->getId();
        }
        else if($oSaleOrder->getPaymethodCode() == 'creditcard')
        {
            $sUrl = $sPrefix.'checkout/thanks?id='.$oSaleOrder->getId();
        }
        else if($oSaleOrder->getPaymethodCode() == 'sofort')
        {
            $sUrl = $sPrefix.'checkout/thanks?id='.$oSaleOrder->getId();
        }
        else if($oSaleOrder->getPaymethodCode() == 'bitcoin')
        {
            $sUrl = $sPrefix.'payment_instructions/bitcoin?id='.$oSaleOrder->getId();
        }
        else
        {
            throw new LogicException("Encountered an unimplemented payment method ".$oSaleOrder->getPaymethodCode());
        }
        return $sUrl;

    }
    static function fillInSaleOrder(SaleOrder $oSaleOrder, Customer $oCustomer, $aData)
    {
		// Some shops have deliver / invoice attn names as separate fields while with others they are combinmed.
	    // This was required for shipping with (DHL), here we unify them.
    	foreach(['invoice', 'deliver'] as $sFieldtype)
	    {
		    if(!isset($aData['customer_' . $sFieldtype . '_attn_first_name']))
		    {
			    $aNameParts = [];
			    if(isset($aData['customer_' . $sFieldtype . '_attn_first_name']))
			    {
				    $aNameParts[] = $aData['customer_' . $sFieldtype . '_attn_first_name'];
			    }
			    if(isset($aData['customer_' . $sFieldtype . '_attn_last_name']))
			    {
				    $aNameParts[] = $aData['customer_' . $sFieldtype . '_attn_last_name'];
			    }
			    $aData['customer_' . $sFieldtype . '_attn_name'] = join(' ', $aNameParts);
		    }
	    }


        $iNextOrderNumber = SaleOrder::generateNextOrderNumber();

        $oSaleOrder->setOrderNumber($iNextOrderNumber);
        $oPaymentMethod = PaymentMethodQuery::create()->findOneById($aData['paymethod']);

        $oSaleOrder->setPaymethodCode($oPaymentMethod->getCode());
        $oSaleOrder->setPaymethodName($oPaymentMethod->getName());
        $oSaleOrder->setPaymethodId($oPaymentMethod->getId());

        if(isset($aData['customer_vat_number']))
        {
            $oSaleOrder->setCustomerVatNumber($aData['customer_vat_number']);
        }

        $oPayTerm = $oCustomer->getPayTerm();
        if($oPayTerm instanceof PayTerm)
        {
            $oSaleOrder->setPaytermDays($oPayTerm->getTotalDays());
        }
        else
        {
            $oPayTerm = PayTermQuery::create()->findOneByIsDefault(true);
            if(!$oPayTerm instanceof PayTerm)
            {
                $oPayTerm = PayTermQuery::create()->findOne();
            }
            $oSaleOrder->setPaytermDays($oPayTerm->getTotalDays());
        }

        $aSiteSettings = getSiteSettings();
        $oSite = SiteQuery::create()->findOneByDomain($aSiteSettings['live_domain']);
        $oOurCompany = CompanyQuery::create()->findOneById($oSite->getInvoicingCompanyId());
        $oSaleOrder->setSite($oSite);
        $oSaleOrder->setSaleOrderStatus(SaleOrderStatusQuery::create()->findOneByCode('wait_payment'));
        $oSaleOrder->setSource('webshop');
        $oSaleOrder->setIsCompleted(false);
        $oSaleOrder->setCustomerId($oCustomer->getId());
        if($oCustomer->getPayTerm())
        {
            $oPayTerm = $oCustomer->getPayTerm();
            $oSaleOrder->setPaytermDays($oPayTerm->getTotalDays());
            $oSaleOrder->setPaytermOriginalId($oPayTerm->getId());
            $oSaleOrder->setPaytermString($oPayTerm->getName());
        }
        if($oCustomerCompany = $oCustomer->getCompany())
        {
            $oSaleOrder->setCompanyName($oCustomerCompany->getCompanyName());
        }
        $oSaleOrder->setDebitor($oCustomer->getDebitor());
        $oSaleOrder->setOurCustomFolder($oSite->getCustomFolder());
        $oSaleOrder->setOurSlogan($oOurCompany->getSlogan());
        $oSaleOrder->setOurPhone($oOurCompany->getPhone());
        $oSaleOrder->setOurFax($oOurCompany->getFax());
        $oSaleOrder->setOurWebsite($oOurCompany->getWebsite());
        $oSaleOrder->setOurEmail($oOurCompany->getEmail());
        $oSaleOrder->setOurVatNumber($oOurCompany->getVatNumber());
        $oSaleOrder->setOurChamberOfCommerce($oOurCompany->getChamberOfCommerce());
        $oSaleOrder->setOurIbanNumber($oOurCompany->getIban());
        $oSaleOrder->setOurBicNumber($oOurCompany->getBic());
        $oSaleOrder->setOurGeneralCompanyName($oOurCompany->getCompanyName());
        $oSaleOrder->setOurGeneralStreet($oOurCompany->getGeneralStreet());
        $oSaleOrder->setOurGeneralNumber($oOurCompany->getGeneralNumber());
        $oSaleOrder->setOurGeneralNumberAdd($oOurCompany->getGeneralNumberAdd());
        $oSaleOrder->setOurGeneralPostal($oOurCompany->getGeneralPostal());
        $oSaleOrder->setOurGeneralPoBox($oOurCompany->getGeneralPoBox());
        $oSaleOrder->setOurGeneralCity($oOurCompany->getGeneralCity());
        $oSaleOrder->setOurGeneralCountry($oOurCompany->getGeneralCountry()->getName());
        $oSaleOrder->setCustomerPhone($aData['customer_phone']);
        $oSaleOrder->setCustomerEmail($aData['customer_email']);

        $oSaleOrder->setCustomerInvoiceCompanyName($aData['customer_invoice_company_name']);

        if(isset($aData['customer_invoice_attn_name']))
        {
	        $oSaleOrder->setCustomerInvoiceAttnName($aData['customer_invoice_attn_name']);
        }
        else if(isset($aData['customer_invoice_attn_first_name']))
        {
	        $oSaleOrder->setCustomerInvoiceAttnName($aData['customer_invoice_attn_first_name'] . ' ' . $aData['customer_invoice_attn_last_name']);
        }


        $oSaleOrder->setCustomerInvoicePostal($aData['customer_invoice_postal']);
        $oSaleOrder->setCustomerInvoiceCity($aData['customer_invoice_city']);
        $oSaleOrder->setCustomerInvoiceCountry($aData['customer_delivery_country']);
        if($aData['customer_invoice_country'] == 'United States of America')
        {
            $oSaleOrder->setCustomerInvoiceUsaState($aData['customer_invoice_usa_state']);
        }

	    $oSaleOrder->setCustomerInvoiceStreet($aData['customer_invoice_street'] ?? null);
        $oSaleOrder->setCustomerInvoiceNumber($aData['customer_invoice_number'] ?? null);
        $oSaleOrder->setCustomerInvoiceNumberAdd($aData['customer_invoice_number_add'] ?? null);
        $oSaleOrder->setCustomerInvoiceAddressL1($aData['customer_invoice_address_l1'] ?? null);
        $oSaleOrder->setCustomerInvoiceAddressL2($aData['customer_invoice_address_l2'] ?? null);

        if(isset($aData['different_address']) && $aData['different_address'] == '1')
        {
            $oSaleOrder->setCustomerDeliveryCompanyName($aData['customer_delivery_company_name']);

	        if(isset($aData['customer_delivery_attn_name']))
	        {
		        $oSaleOrder->setCustomerDeliveryAttnName($aData['customer_delivery_attn_name']);
	        }
	        else if(isset($aData['customer_delivery_attn_first_name']))
	        {
		        $oSaleOrder->setCustomerDeliveryAttnName($aData['customer_delivery_attn_first_name'] . ' ' . $aData['customer_delivery_attn_last_name']);
	        }


            $oSaleOrder->setCustomerDeliveryPostal($aData['customer_delivery_postal']);
            $oSaleOrder->setCustomerDeliveryCity($aData['customer_delivery_city']);
            $oSaleOrder->setCustomerDeliveryCountry($aData['customer_delivery_country']);
            if($aData['customer_delivery_country'] == 'United States of America')
            {
                $oSaleOrder->setCustomerDeliveryUsaState($aData['customer_delivery_usa_state']);
            }

	        $oSaleOrder->setCustomerDeliveryStreet($aData['customer_delivery_street'] ?? null);
	        $oSaleOrder->setCustomerDeliveryNumber($aData['customer_delivery_number'] ?? null);
	        $oSaleOrder->setCustomerDeliveryNumberAdd($aData['customer_delivery_number_add'] ?? null);
            $oSaleOrder->setCustomerDeliveryAddressL1($aData['customer_delivery_address_l1'] ?? null);
            $oSaleOrder->setCustomerDeliveryAddressL2($aData['customer_delivery_address_l2'] ?? null);
        }
        else
        {
            $oSaleOrder->setCustomerDeliveryCompanyName($aData['customer_invoice_company_name']);

	        if(isset($aData['customer_invoice_attn_name']))
	        {
		        $oSaleOrder->setCustomerDeliveryAttnName($aData['customer_invoice_attn_name']);
	        }
	        else if(isset($aData['customer_invoice_attn_first_name']))
	        {
		        $oSaleOrder->setCustomerDeliveryAttnName($aData['customer_invoice_attn_first_name'] . ' ' . $aData['customer_invoice_attn_last_name']);
	        }

            $oSaleOrder->setCustomerDeliveryPostal($aData['customer_invoice_postal']);
            $oSaleOrder->setCustomerDeliveryCity($aData['customer_invoice_city']);
            $oSaleOrder->setCustomerDeliveryCountry($aData['customer_invoice_country']);
            if($aData['customer_invoice_country'] == 'United States of America')
            {
                $oSaleOrder->setCustomerDeliveryUsaState($aData['customer_invoice_usa_state']);
            }

	        $oSaleOrder->setCustomerDeliveryStreet($aData['customer_invoice_street'] ?? null);
	        $oSaleOrder->setCustomerDeliveryNumber($aData['customer_invoice_number'] ?? null);
	        $oSaleOrder->setCustomerDeliveryNumberAdd($aData['customer_invoice_number_add'] ?? null);
	        $oSaleOrder->setCustomerDeliveryAddressL1($aData['customer_invoice_address_l1'] ?? null);
	        $oSaleOrder->setCustomerDeliveryAddressL2($aData['customer_invoice_address_l2'] ?? null);
        }

        if(isset($aData['customer_invoice_attn_first_name']))
        {
	        $oSaleOrder->setCpFirstName($aData['customer_invoice_attn_first_name']);
	        $oSaleOrder->setCpLastName($aData['customer_invoice_attn_last_name']);
        }
	    else if(isset($aData['customer_invoice_attn_name']) && !empty($aData['customer_invoice_attn_name']))
        {
        	$aParts = explode(' ', $aData['customer_invoice_attn_name']);

            $oSaleOrder->setCpFirstName($aParts[0]);
            $oSaleOrder->setCpLastName(array_reverse($aParts)[0]);
        }
        else if($oCustomer->getFirstName() && $oCustomer->getLastName())
        {
            $oSaleOrder->setCpFirstName($oCustomer->getFirstName());
            $oSaleOrder->setCpLastName($oCustomer->getLastName());
        }
        else
        {
            $oSaleOrder->setCpFirstName('unspecified');
            $oSaleOrder->setCpLastName('unspecified');
        }

        $sGender = $oCustomer->getGender();
        if(!empty($sGender))
        {
        	if($sGender == 'male')
	        {
		        $oSaleOrder->setCpSallutation('Mr.');
	        }
	        else if($sGender == 'female')
	        {
		        $oSaleOrder->setCpSallutation('Mrs.');
	        }
        }

        $oSaleOrderStatus = SaleOrderStatusQuery::create()->findOneByCode('new');

        if($oSaleOrderStatus instanceof SaleOrderStatus)
        {
            $oSaleOrder->setSaleOrderStatusId($oSaleOrderStatus->getId());
        }
        $oSaleOrder->setCustomerOrderNote($aData['notes']);
        $oSaleOrder->setWorkInProgress(false);
        $oSaleOrder->setItemDeleted(false);
        $oSaleOrder->save();
        self::changeShipping($oSaleOrder, $aData);
        $oSaleOrder->save();
    }
    static function changeShipping(SaleOrder $oSaleOrder, $aData)
    {
        if($oSaleOrder->calcHasFreeShipping())
        {
            $oShippingMethod = ShippingMethodQuery::create()->findOneByIsDefaultFree(true);
            $oSaleOrder->setShippingPrice(0);
        }
        else
        {
            if(isset($aData['different_address']) && $aData['different_address'] == '1')
            {
                $sCountry = $aData['customer_delivery_country'];
            }
            else
            {
                $sCountry = $aData['customer_invoice_country'];
            }

            $oCountry = CountryQuery::create()->findOneByName($sCountry);

            $oCountryShippingMethodQuery = CountryShippingMethodQuery::create();
            $oCountryShippingMethodQuery->filterByCountryId($oCountry->getId());
            $oCountryShippingMethodQuery->filterByIsEnabledWebsite(true);

            $oShippingMethodQuery =  ShippingMethodQuery::create();

            if(isset($aData['shipping']))
            {
                $oCountryShippingMethodQuery->filterByShippingMethodId($aData['shipping']);
                $oShippingMethod = $oShippingMethodQuery->findOneById($aData['shipping']);
            }
            else
            {
                $oShippingMethod = $oShippingMethodQuery->findOne();
            }
            $oCountryShippingMethod = $oCountryShippingMethodQuery->findOne();

            if(!$oCountryShippingMethod instanceof CountryShippingMethod)
            {
            	// No shipping method chosen, fallback to picking a random one.
	            $oCountryChangedRandomCountryShipping = $oCountryShippingMethodQuery->findOne();
                $oCountryShippingMethod = $oCountryChangedRandomCountryShipping;
            }

            $oSaleOrder->setShippingPrice($oCountryShippingMethod->getPrice());
        }
        $oSaleOrder->setShippingHasTrackTrace($oShippingMethod->getIsTrackTrace());
        $oSaleOrder->setShippingCarrier($oShippingMethod->getName());
        $oSaleOrder->setShippingMethodId($oShippingMethod->getId());
        $oSaleOrder->save();
    }
    static function createCustomer($aData, Language $oLanguage):Customer
    {
        $oCustomer = new Customer();
        $oCustomerTypeQuery = CustomerTypeQuery::create();
        if(isset($aData['customer_invoice_company_name']) && $aData['customer_invoice_company_name'])
        {
            $oCustomerType = $oCustomerTypeQuery->findOneByCode('company');
            $oCustomerCompany = new CustomerCompany();
            $oCustomerCompany->setCompanyName($aData['customer_invoice_company_name']);
            if(isset($aData['block_customer_vat_number']))
            {
                $oCustomerCompany->setVatNumber($aData['block_customer_vat_number']);
            }
        }
        else
        {
            $oCustomerType = $oCustomerTypeQuery->findOneByCode('person');
        }

	    if(isset($aData['customer_invoice_attn_first_name']))
	    {
		    $oCustomer->setFirstName($aData['customer_invoice_attn_first_name']);
	    }
	    if(isset($aData['customer_invoice_attn_last_name']))
	    {
		    $oCustomer->setLastName($aData['customer_invoice_attn_last_name']);
	    }

        $oCustomer->setLanguage($oLanguage);
        $oCustomer->setCustomerType($oCustomerType);
        $oCustomer->setPayTerm(PayTermQuery::create()->findOneByCode('14DAYS'));
        $oCustomer->setDebitor(Customer::generateNexDebitorNumber());
        $oCustomer->setEmail($aData['customer_email']);
        $oCustomer->setItemDeleted(0);
        $oCustomer->setItemDeleted(0);
        if(isset($aData['create_account']) && $aData['create_account'] == '1')
        {
            $sSalt = rand(0, 9999).'x';
            $sEncryptedPass = \Core\Customer::makeEncryptedPass($aData['password'], $sSalt);
            $oCustomer->setPassword($sEncryptedPass);
            $oCustomer->setSalt($sSalt);
        }
        else
        {
            $oCustomer->setOncePasswordReset(1);
        }
        $oCustomer->setIsWholesale(false);
        $oCustomer->setCanBuyOnCredit(false);
        $oCustomer->setPhone($aData['customer_phone']);
        $oCustomer->save();
        if(isset($oCustomerCompany))
        {
            $oCustomerCompany->setCustomerId($oCustomer->getId());
            $oCustomerCompany->save();
        }
        return $oCustomer;
    }
    static function formFromSaleOrderObject(SaleOrder $oSaleOrder)
    {
        $aData = [];
        $aData['customer_email'] = $oSaleOrder->getCustomerEmail();
        $aData['customer_phone'] = $oSaleOrder->getCustomerPhone();
        $aData['different_address'] = '0';
        if( $oSaleOrder->getCustomerDeliveryAddressL1() && $oSaleOrder->getCustomerInvoiceAddressL1() &&
            ($oSaleOrder->getCustomerDeliveryAddressL1() != $oSaleOrder->getCustomerInvoiceAddressL1()))
        {
            $aData['different_address'] = '1';
        }
        $aData['create_account'] = '0';
        $aData['customer_delivery_country_object'] = CountryQuery::create()->findOneByName($oSaleOrder->getCustomerDeliveryCountry());
        $aData['customer_invoice_country'] = $oSaleOrder->getCustomerInvoiceCountry();
        $aData['customer_invoice_usa_state'] = $oSaleOrder->getCustomerInvoiceUsaState();
        $aData['customer_invoice_attn_name'] = $oSaleOrder->getCustomerInvoiceAttnName();



        $aData['customer_invoice_company_name'] = $oSaleOrder->getCustomerInvoiceCompanyName();
        $aData['customer_invoice_address_l1'] = $oSaleOrder->getCustomerInvoiceAddressL1();
        $aData['customer_invoice_address_l2'] = $oSaleOrder->getCustomerInvoiceAddressL2();
        $aData['customer_invoice_city'] = $oSaleOrder->getCustomerInvoiceCity();
        $aData['customer_invoice_postal'] = $oSaleOrder->getCustomerInvoicePostal();
        $aData['customer_invoice_country'] = $oSaleOrder->getCustomerInvoiceCountry();
        $aData['customer_delivery_country'] = $oSaleOrder->getCustomerDeliveryCountry();
        $aData['customer_delivery_usa_state'] = $oSaleOrder->getCustomerDeliveryUsaState();
        $aData['customer_delivery_attn_name'] = $oSaleOrder->getCustomerDeliveryAttnName();
        $aData['customer_delivery_company_name'] = $oSaleOrder->getCustomerDeliveryCompanyName();
        $aData['customer_delivery_address_l1'] = $oSaleOrder->getCustomerDeliveryAddressL1();
        $aData['customer_delivery_address_l2'] = $oSaleOrder->getCustomerDeliveryAddressL2();
        $aData['customer_delivery_city'] = $oSaleOrder->getCustomerDeliveryCity();
        $aData['customer_delivery_postal'] = $oSaleOrder->getCustomerDeliveryPostal();
        $aData['customer_delivery_country_object'] = CountryQuery::create()->findOneByName($oSaleOrder->getCustomerDeliveryCountry());
        $aData['customer_delivery_country'] = $oSaleOrder->getCustomerDeliveryCountry();
        $aData['notes'] = '';
        return $aData;
    }
    static function defaultFormSettings(Customer $oCustomer, Country $oDefaultCountry):array
    {
        $aData = [];
        $aData['customer_email'] = $oCustomer->getEmail();
        $aData['customer_phone'] = $oCustomer->getPhone();
        $aData['different_address'] = $oCustomer->hasDeliveryAddress() ? '1' : '0';
        $aData['create_account'] = '0'; // togggle create account form part

        if($oCustomer->hasInvoiceAddress())
        {
            $oAddress =  $oCustomer->getInvoiceAddress();
        }
        else if($oCustomer->hasGeneralAddress())
        {
            $oAddress =  $oCustomer->getGeneralAddress();
        }
        if(isset($oAddress) && $oAddress instanceof CustomerAddress)
        {
            if($oAddress->getCountry())
            {
                $oCountry = $oAddress->getCountry();
                $aData['customer_delivery_country_object'] = $oCountry;
                $aData['customer_invoice_country'] = $oCountry->getName();
            }
            if($oAddress->getUsaState())
            {
                $oState = $oAddress->getUsaState();
                $aData['customer_invoice_usa_state'] = $oState->getName();
            }

            $aData['customer_invoice_attn_name'] = $oAddress->getAttnName();
            $aData['customer_invoice_company_name'] = $oAddress->getCompanyName();
            $aData['customer_invoice_address_l1'] = $oAddress->getAddressL1();
            $aData['customer_invoice_address_l2'] = $oAddress->getAddressL2();
            $aData['customer_invoice_city'] = $oAddress->getCity();
            $aData['customer_invoice_postal'] = $oAddress->getPostal();
        }
        if(!isset($aData['customer_invoice_country']))
        {
            $aData['customer_invoice_country'] = $oDefaultCountry->getName();
        }


        if($oCustomer->hasDeliveryAddress())
        {
            $oAddress = $oCustomer->getDeliveryAddress();
            $oCountry = $oAddress->getCountry();
            $aData['customer_delivery_country_object'] = $oCountry;
            if($oCountry)
            {
                $aData['customer_delivery_country'] = $oCountry->getName();
            }
            $oState = $oAddress->getUsaState();
            if($oState)
            {
                $aData['customer_delivery_usa_state'] = $oState->getName();
            }
            $aData['customer_delivery_attn_name'] = $oAddress->getAttnName();
            $aData['customer_delivery_company_name'] = $oAddress->getCompanyName();
            $aData['customer_delivery_address_l1'] = $oAddress->getAddressL1();
            $aData['customer_delivery_address_l2'] = $oAddress->getAddressL2();
            $aData['customer_delivery_city'] = $oAddress->getCity();
            $aData['customer_delivery_postal'] = $oAddress->getPostal();
        }
        if(!isset($aData['customer_delivery_country']))
        {
            $aData['customer_delivery_country_object'] = $oDefaultCountry;
            $aData['customer_delivery_country'] = $oDefaultCountry->getName();
        }
        $aData['notes'] = '';
        return $aData;
    }

    public static function validate($aData, SaleOrder $oSaleOrder)
    {
        $aErrors = [];


        if(!isset($aData['paymethod']) || empty($aData['paymethod']))
        {
            $aErrors[] = Translate::fromCode("Geef alstublieft op hoe u wenst te betalen.");
        }

        if(!isset($aData['agree_terms']) || $aData['agree_terms'] !== 1)
        {
            $aErrors[] = Translate::fromCode("U dient akkoord te gaan met de algemene voorwaarden.");
        }
		if(isset($aData['customer_invoice_attn_first_name']))
		{
	        if(isset($aData['customer_invoice_attn_first_name']) && empty($aData['customer_invoice_attn_first_name']))
	        {
		        $aErrors[] = Translate::fromCode("Please specify your first name.");
	        }
	        if(isset($aData['customer_invoice_attn_last_name']) && empty($aData['customer_invoice_attn_last_name']))
	        {
		        $aErrors[] = Translate::fromCode("Please specify your last name.");
	        }
		}
		else
		{
	        if(isset($aData['customer_invoice_attn_name']) && empty($aData['customer_invoice_attn_name']))
	        {
		        $aErrors[] = Translate::fromCode("Please specify your name.");
	        }
		}

        if(empty($aData['customer_email']))
        {
            $aErrors[] = Translate::fromCode("Geef alstublieft uw e-mailadres op.");
        }
        if(empty($aData['customer_invoice_country']))
        {
            $aErrors[] = Translate::fromCode("Het veld land is verplicht.");
        }
        if(isset($aData['customer_invoice_country']) && $aData['customer_invoice_country'] == 'United States of America' && empty($aData['customer_invoice_usa_state']))
        {
            $aErrors[] = Translate::fromCode("Customers from the United States must specify their state.");
        }


		if(isset($aData['customer_invoice_address_l1']) && empty($aData['customer_invoice_address_l1']))
        {
            $aErrors[] = Translate::fromCode("Geef alstublieft uw adres op.");
        }
		else if(isset($aData['customer_invoice_street']) && empty($aData['customer_invoice_street']))
        {
	        $aErrors[] = Translate::fromCode("Geef alstublieft u adres op.");
        }
		else if(isset($aData['customer_invoice_number']) && empty($aData['customer_invoice_number']))
		{
			$aErrors[] = Translate::fromCode("Geef alstublieft uw huisnummer op.");
		}

        if(empty($aData['customer_invoice_city']))
        {
            $aErrors[] = Translate::fromCode("Geef alstublieft uw stad op.");
        }
        if(empty($aData['customer_invoice_postal']))
        {
            $aErrors[] = Translate::fromCode("Geef alstublieft uw postcode op.");
        }
        if(isset($aData['different_address']) && $aData['different_address'] == '1')
        {
	        if(isset($aData['customer_delivery_attn_first_name']) && empty($aData['customer_delivery_attn_first_name']))
	        {
		        $aErrors[] = Translate::fromCode("Geef alstublieft een naam op bij het afleveradres.");
	        }
	        else if(isset($aData['customer_delivery_attn_name']) && empty($aData['customer_delivery_attn_name']))
            {
                $aErrors[] = Translate::fromCode("Geef alstublieft een naam op bij het afleveradres.");
            }

            if(empty($aData['customer_delivery_country']))
            {
                $aErrors[] = Translate::fromCode("Geef alstublieft het land op.");
            }
            if(isset($aData['customer_delivery_country']) && $aData['customer_delivery_country'] == 'United States of America' && empty($aData['customer_delivery_usa_state']))
            {
                $aErrors[] = Translate::fromCode("Please specify the delivery state.");
            }


	        if(isset($aData['customer_delivery_address_l1']) && empty($aData['customer_delivery_address_l1']))
	        {
		        $aErrors[] = Translate::fromCode("Please specify your address.");
	        }
	        else if(isset($aData['customer_delivery_street']) && empty($aData['customer_delivery_street']))
	        {
		        $aErrors[] = Translate::fromCode("Please specify your address.");
	        }
	        else if(isset($aData['customer_delivery_number']) && empty($aData['customer_delivery_number']))
	        {
		        $aErrors[] = Translate::fromCode("Please specify your housenumber.");
	        }

	        if(empty($aData['customer_delivery_city']))
            {
                $aErrors[] = Translate::fromCode("Please specify a delivery city.");
            }
            if(empty($aData['customer_delivery_postal']))
            {
                $aErrors[] = Translate::fromCode("Please specify a delivery postal code.");
            }
        }
        if(isset($aData['create_account']) && $aData['create_account'] == '1')
        {
            if(empty($aData['password']))
            {
                $aErrors[] = Translate::fromCode("Please specify a password.");
            }
            else if(strlen($aData['password']) < 8)
            {
                $aErrors[] = Translate::fromCode("In order to protect your privacy and security we require you to provide a password that is at least 8 characters long.");
            }
        }

        if(!isset($aData['paymethod']) || !is_numeric($aData['paymethod']))
        {
            $aErrors[] = Translate::fromCode("Geef alstublieft op hoe u wenst te betalen.");
        }

        else if(!isset($aData['issuer_'.$aData['paymethod']]))
        {
            $aErrors[] = Translate::fromCode("Please select a bank / type below your selected payment method.");

        }

        return $aErrors;
    }

    public static function makeErrorHtml($aErrors)
    {
        $aOut = [];
        $aOut[] = '<ul>';
        foreach ($aErrors as $sError)
        {
            $aOut[] = '<li>' . $sError. '</li>';
        }
        $aOut[] = '<ul>';
        return join(PHP_EOL, $aOut);
    }

    public static function updateAddressbook(Customer $oCustomer, $aData)
    {
        if(isset($aData['different_address']))
        {

            // Customer provided two addresses, we are creating a delivery and an invoice address
            // first the invoice address
            $oAtDelivery = CustomerAddressTypeQuery::create()->findOneByCode('delivery');
            $oCustomerAddress = CustomerAddressQuery::create()->filterByCustomer($oCustomer)->filterByCustomerAddressType($oAtDelivery);
            if(!$oCustomerAddress instanceof CustomerAddressType)
            {
                $oCustomerAddress = new CustomerAddress();
                $oCustomerAddress->setCustomerAddressType($oAtDelivery);
                $oCustomerAddress->setCustomerId($oCustomer->getId());
                $oCustomerAddress->setCountry(CountryQuery::create()->findOneByName($aData['customer_delivery_country']));
                if($aData['customer_delivery_country'] == 'United States of America')
                {
                    $oCustomerAddress->setUsaState(UsaStateQuery::create()->findOneByName($aData['customer_delivery_usa_state']));
                }
                $oCustomerAddress->setCompanyName($aData['customer_delivery_company_name']);

                if(!isset($aData['customer_delivery_attn_name']))
                {
	                $aNameParts = [];
					if(isset($aData['customer_delivery_attn_first_name']))
					{
						$aNameParts[] = $aData['customer_delivery_attn_first_name'];
					}
					if(isset($aData['customer_delivery_attn_last_name']))
					{
						$aNameParts[] = $aData['customer_delivery_attn_last_name'];
					}
	                $aData['customer_delivery_attn_name'] = join(' ', $aNameParts);
				}
                $oCustomerAddress->setAttnName($aData['customer_delivery_attn_name']);

                // International shopw usually have 2 address lines
                $oCustomerAddress->setAddressL1($aData['customer_delivery_address_l1'] ?? null);
                $oCustomerAddress->setAddressL2($aData['customer_delivery_address_l2'] ?? null);

	            // Dutch / Belgian shops use street, number etc
	            $oCustomerAddress->setStreet($aData['customer_delivery_street'] ?? null);
	            $oCustomerAddress->setNumber($aData['customer_delivery_number'] ?? null);
	            $oCustomerAddress->setNumberAdd($aData['customer_delivery_number_add'] ?? null);

                $oCustomerAddress->setCity($aData['customer_delivery_city']);
                $oCustomerAddress->setPostal($aData['customer_delivery_postal']);
                $oCustomerAddress->setIsDefault(true);
                $oCustomerAddress->save();
            }
            // now the invoice addresss
            $oAtInvoice = CustomerAddressTypeQuery::create()->findOneByCode('invoice');
            $oCustomerAddress = CustomerAddressQuery::create()->filterByCustomer($oCustomer)->filterByCustomerAddressType($oAtInvoice);
            if(!$oCustomerAddress instanceof CustomerAddressType)
            {
                $oCustomerAddress = new CustomerAddress();
                $oCustomerAddress->setCustomerAddressType($oAtInvoice);
                $oCustomerAddress->setCustomerId($oCustomer->getId());
                $oCustomerAddress->setCountry(CountryQuery::create()->findOneByName($aData['customer_invoice_country']));
                if ($aData['customer_invoice_country'] == 'United States of America') {
                    $oCustomerAddress->setUsaState(UsaStateQuery::create()->findOneByName($aData['customer_invoice_usa_state']));
                }
                $oCustomerAddress->setCompanyName($aData['customer_invoice_company_name']);


	            if(!isset($aData['customer_invoice_attn_name']))
	            {
		            $aNameParts = [];
		            if(isset($aData['customer_invoice_attn_first_name']))
		            {
			            $aNameParts[] = $aData['customer_invoice_attn_first_name'];
		            }
		            if(isset($aData['customer_invoice_attn_last_name']))
		            {
			            $aNameParts[] = $aData['customer_invoice_attn_last_name'];
		            }
		            $aData['customer_invoice_attn_name'] = join(' ', $aNameParts);
	            }
	            $oCustomerAddress->setAttnName($aData['customer_invoice_attn_name']);

	            // International shopw usually have 2 address lines
                $oCustomerAddress->setAddressL1($aData['customer_invoice_address_l1'] ?? null);
                $oCustomerAddress->setAddressL2($aData['customer_invoice_address_l2'] ?? null);

	            // Dutch / Belgian shops use street, number etc
	            $oCustomerAddress->setStreet($aData['customer_invoice_street'] ?? null);
	            $oCustomerAddress->setNumber($aData['customer_invoice_number'] ?? null);
	            $oCustomerAddress->setNumberAdd($aData['customer_invoice_number_add'] ?? null);

                $oCustomerAddress->setCity($aData['customer_invoice_city']);
                $oCustomerAddress->setPostal($aData['customer_invoice_postal']);
                $oCustomerAddress->setIsDefault(true);
                $oCustomerAddress->save();
            }
        }
        else
        {
            // only a general address
            $oAtGeneral = CustomerAddressTypeQuery::create()->findOneByCode('general');
            $oCustomerAddress = CustomerAddressQuery::create()->filterByCustomer($oCustomer)->filterByCustomerAddressType($oAtGeneral);
            if(!$oCustomerAddress instanceof CustomerAddressType)
            {
                $oCustomerAddress = new CustomerAddress();
                $oCustomerAddress->setCustomerId($oCustomer->getId());
                $oCustomerAddress->setCustomerAddressType($oAtGeneral);
                $oCustomerAddress->setCountry(CountryQuery::create()->findOneByName($aData['customer_invoice_country']));
                if ($aData['customer_invoice_country'] == 'United States of America') {
                    $oCustomerAddress->setUsaState(UsaStateQuery::create()->findOneByName($aData['customer_delivery_usa_state']));
                }
                $oCustomerAddress->setCompanyName($aData['customer_invoice_company_name']);

                if(isset($aData['customer_invoice_attn_name']))
                {
	                $oCustomerAddress->setAttnName($aData['customer_invoice_attn_name']);
                }


	            // International shopw usually have 2 address lines
                $oCustomerAddress->setAddressL1($aData['customer_invoice_address_l1'] ?? null);
                $oCustomerAddress->setAddressL2($aData['customer_invoice_address_l2'] ?? null);

	            // Dutch / Belgian shops use street, number etc
	            $oCustomerAddress->setStreet($aData['customer_invoice_street'] ?? null);
	            $oCustomerAddress->setNumber($aData['customer_invoice_number'] ?? null);
	            $oCustomerAddress->setNumberAdd($aData['customer_invoice_number_add'] ?? null);

                $oCustomerAddress->setCity($aData['customer_invoice_city']);
                $oCustomerAddress->setPostal($aData['customer_invoice_postal']);
                $oCustomerAddress->setIsDefault(true);
                $oCustomerAddress->save();
            }
        }
    }
}
