<?php
namespace _Default\Checkout;

use _Default\_DefaultController;
use Core\Customer;
use Core\InlineTemplate;
use Exception\LogicException;
use Model\Cms\SitePage;
use Model\Cms\SitePageQuery;
use Model\Cms\SiteQuery;
use Model\Sale\SaleOrderQuery;

class Wiretransfer_pay_instructionsController extends _DefaultController
{
    function getTag()
    {
        return 'wiretransfer_pay_instruction';
    }

    function runShop()
    {
        $oLanguage = $this->getCurrentLanguage();
        if(!Customer::isSignedIn())
        {
            $this->redirect('/'.$oLanguage->getShopUrlPrefix());
        }
        $oCurrentCustomer = Customer::getMember();

        $oSaleOrder = $this->getCurrentSaleOrder();
        if(isset($_SESSION['CURRENT_SALE_ORDER']))
        {
            unset($_SESSION['CURRENT_SALE_ORDER']);
        }
        $iSaleOrderId = $this->get('sale_order_id', null, true, 'numeric');

        /*
            30-5-2017 vooor vangool uitgezet
            $oSaleOrder->setIsClosed(true);
            $oSaleOrder->save();
        */

        // De prijs en het ordernummer moeten wel onderin de pagina weergegeven worden.
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iSaleOrderId);

        if($oSaleOrder->getCustomerId() != $oCurrentCustomer->getId())
        {
            throw new LogicException("This is not your order.");
        }

        $aSiteSettings = getSiteSettings();
        $oSite = SiteQuery::create()->findOneByDomain($aSiteSettings['live_domain']);

        $sTag = $this->getTag();
        $oSitePageQuery = SitePageQuery::create();
        $oSitePageQuery->filterByLanguageId($oLanguage->getId());
        $oSitePageQuery->filterBySiteId($oSite->getId());
        $oSitePageQuery->filterByTag($sTag);

        $oSitePage = $oSitePageQuery->findOne();

        if(!$oSitePage instanceof SitePage)
        {
            throw new LogicException("Could not find a SitePage with pay instructions, this indicates a bug.");
        }

        $aViewData['Sale_order'] = $oSaleOrder;
        // $aViewData['Sale_order_item'] = $aSaleOrderItem;

        $sTitle = InlineTemplate::parse($oSitePage->getTitle(), $aViewData);
        $sContents  = InlineTemplate::parse($oSitePage->getContent(), $aViewData);

        $aMainContent = [
            'contents' => nl2br($sContents),
            'title' => $sTitle
        ];

        $aResult = [
            'content' => $this->parse('Checkout/pay_instructions.twig', $aMainContent),
            'title' => $sTitle
        ];
        return $aResult;
    }
}