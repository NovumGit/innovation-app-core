<?php
namespace _Default\Faq;

use _Default\_DefaultController;
use Model\Cms\Base\SiteFAQCategoryQuery;
use Model\Cms\SiteFAQCategoryTranslationQuery;
use Model\Cms\SiteFAQQuery;

class OverviewController extends _DefaultController {

    function runShop()
    {
        $oSite = $this->getSite();
        $oLanguage = $this->getCurrentLanguage();


        $aFaqCategories = SiteFAQCategoryQuery::create()->findBySiteId($oSite->getId());

        $aCategories = [];
        foreach($aFaqCategories as $oFaqCategory)
        {
            $oFaqTransQuery = SiteFAQCategoryTranslationQuery::create();

            $oFaqCategoryTranslation = $oFaqTransQuery
                                        ->filterBySiteFaqCategoryId($oFaqCategory->getId())
                                        ->filterByLanguageId($oLanguage->getId())
                                        ->findOne();

            $aItems = SiteFAQQuery::create()->filterByLanguageId($oLanguage->getId())->filterBySiteFaqCategoryId($oFaqCategory->getId())->find();

            $aCategories[] = [
                'category_id' => $oFaqCategory->getId(),
                'translation' => $oFaqCategoryTranslation->getName(),
                'image' => $oFaqCategory->getImagePath(),
                'items' => $aItems
            ];
        }

        $aMainView = [
            'categories' => $aCategories
        ];
        $aView = [
            'content' => $this->parse('Faq/overview.twig', $aMainView)
        ];
        return $aView;
    }
}
