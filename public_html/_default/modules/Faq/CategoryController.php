<?php
namespace _Default\Faq;

use _Default\_DefaultController;
use Model\Cms\SiteFAQCategoryTranslationQuery;
use Model\Cms\SiteFAQQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class CategoryController extends _DefaultController {

    function runShop()
    {

        $sUrl = $this->getRequestUri();
        preg_match('/-([0-9]+)/', $sUrl, $aMatches);
        $iFaqCategoryId = $aMatches[1];
        $oSite = $this->getSite();
        $oLanguage = $this->getCurrentLanguage();


        $aOtherCategories = SiteFAQCategoryTranslationQuery::create()
                            ->filterByLanguageId($oLanguage->getId())
                            ->filterBySiteFaqCategoryId($iFaqCategoryId, Criteria::NOT_EQUAL)
                            ->find();

        $aAllOtherCategories = [];
        foreach($aOtherCategories as $oOtherCategory)
        {
            $aAllOtherCategories[] = [
                'object' => $oOtherCategory,
                'item_count' => SiteFAQQuery::create()->filterByLanguageId($oLanguage->getId())
                                ->filterBySiteFaqCategoryId($oOtherCategory->getSiteFaqCategoryId())
                                ->count()
            ];
        }

        $oSiteFAQCategoryTranslation = SiteFAQCategoryTranslationQuery::create()
                                        ->filterBySiteFaqCategoryId($iFaqCategoryId)
                                        ->filterByLanguageId($oLanguage->getId())
                                        ->findOne();


        $aItems = SiteFAQQuery::create()->filterByLanguageId($oLanguage->getId())
                    ->filterBySiteFaqCategoryId($iFaqCategoryId)
                    ->find();

        $aMainView = [
            'faq_items' => $aItems,
            'other_categories' => $aAllOtherCategories,
            'all_faq_item_count' => SiteFAQQuery::create()->filterByLanguageId($oLanguage->getId())->count(),
            'category_name' => $oSiteFAQCategoryTranslation->getName()
        ];
        $aView = [
            'content' => $this->parse('Faq/category.twig', $aMainView)
        ];
        return $aView;
    }
}