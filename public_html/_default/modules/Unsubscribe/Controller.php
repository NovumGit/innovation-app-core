<?php
namespace _Default\Unsubscribe;

use Core\MainController;
use Core\StatusMessage;
use Core\Translate;
use Model\Crm\Customer;
use Model\Crm\CustomerQuery;

class Controller extends MainController {

    function run()
    {
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $sEmail = $this->get('email', null, true);

        $oCustomerQuery = CustomerQuery::create();
        $oCustomerQuery->filterByEmail($sEmail);
        $oCustomerQuery->filterById($iCustomerId);
        $oCustomer = $oCustomerQuery->findOne();

        if(!$oCustomer instanceof Customer)
        {
            throw new \LogicException("Could not find you");
        }

        $oCustomer->setIsNewsletterSubscriber(false);
        $oCustomer->save();

        StatusMessage::success(Translate::fromCode("You have been unsubscribed from the newsletter."));
        $this->redirect('/');
    }
}