<?php
namespace _Default\Cron;
use Core\MainController;
use Model\CustomerQuery;
use Model\Product;
use Model\Sale\OrderItemProduct;
use Model\Sale\OrderItemProductQuery;
use Model\Sale\SaleOrderItemQuery;

class Fix_descriptionsController extends MainController
{

    function run()
    {
        $aAllSaleOrderItems = SaleOrderItemQuery::create()->find();

        foreach($aAllSaleOrderItems as $oSaleOrderItem)
        {
            $sDescription = $oSaleOrderItem->getDescription();

            if(preg_match('/^[0-9]+$/', trim($sDescription)))
            {
                $oOrderItemProduct = OrderItemProductQuery::create()->findOneBySaleOrderItemId($oSaleOrderItem->getId());
                if(!$oOrderItemProduct instanceof OrderItemProduct)
                {
                    continue;
                }
                $iProductId = $oOrderItemProduct->getProductId();
                $oProduct = CustomerQuery::create()->findOneById($iProductId);

                if($oProduct instanceof Product)
                {
                    echo $oProduct->getTitle();
                }

                $iDerrivedFromProduct = $oProduct->getDerrivedFromId();
                $oDerrivedFromProduct = CustomerQuery::create()->findOneById($iDerrivedFromProduct);

                if($oDerrivedFromProduct instanceof Product)
                {
                    $oSecondSaleOrderItem = SaleOrderItemQuery::create()->findOneById($oSaleOrderItem->getId());
                    $sNewDescription = $oDerrivedFromProduct->getTitle().' '.trim($oSecondSaleOrderItem->getDescription());
                    if(strlen($sNewDescription) > 200)
                    {
                        continue;
                    }
                    $oSecondSaleOrderItem->setDescription($sNewDescription);
                    echo "Saved $sNewDescription <br>";
                    $oSaleOrderItem->save();
                }

            }

        }

        echo "Done";
        exit();
    }
}
