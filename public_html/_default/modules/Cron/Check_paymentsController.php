<?php
namespace _Default\Cron;

use Core\ButtonEventDispatcher;
use Core\MainController;
use Model\Sale\SaleOrderPaymentQuery;
use Model\Sale\SaleOrderQuery;
use Model\Setting\MasterTable\Base\PaymentMethodQuery;
use Psp\CheckPaymentInterface;

class Check_paymentsController extends MainController
{
    function run()
    {
        $oSaleOrderQuery = SaleOrderQuery::create();
        $oSaleOrderQuery->filterByWorkInProgress(false);
        $oSaleOrderQuery->filterByItemDeleted(false);
        $oSaleOrderQuery->filterByIsFullyPaid(false);
        $aSaleOrders = $oSaleOrderQuery->find();

        foreach($aSaleOrders as $oSaleOrder)
        {
            echo $oSaleOrder->getOrderNumber() . "<br>";

            $iPaymethodId = $oSaleOrder->getPaymethodId();

            if($iPaymethodId)
            {
                $oPaymentMethod = PaymentMethodQuery::create()->findOneById($iPaymethodId);
                $sPaymentObjectName = '\\Psp\\Psp'.$oPaymentMethod->getPsp();

                $oPaymentObject = new $sPaymentObjectName();

                if($oPaymentObject instanceof CheckPaymentInterface)
                {
                    $oSaleOrderPaymentQuery = SaleOrderPaymentQuery::create();
                    $oSaleOrderPaymentQuery->filterBySaleOrderId($oSaleOrder->getId());
                    $oSaleOrderPayment = $oSaleOrderPaymentQuery->findOne();

                    $bOrderPaid = $oPaymentObject->checkPayment($oSaleOrder, $oSaleOrderPayment);

                    echo get_class($oPaymentObject) . ' ' . $oSaleOrder->getId() . ' is paid ' . $bOrderPaid . '<br>';
                    if($bOrderPaid)
                    {
                        // @todo, dit moet op een mooiere manier geregeld worden.
                        $iOrderPaidButtonId = 10;
                        $bIsLast = true;
                        ButtonEventDispatcher::Dispatch($iOrderPaidButtonId, ['order_id' => $oSaleOrder->getId()], $bIsLast);
                    }
                }
            }

        }
        exit('all checked');
    }
}
