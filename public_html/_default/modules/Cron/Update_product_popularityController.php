<?php
namespace _Default\Cron;
use Core\MainController;
use Core\QueryMapper;

class Update_product_popularityController extends MainController
{

    function run()
    {
        QueryMapper::query("DROP TABLE IF EXISTS product_popularity");
        QueryMapper::query("CREATE TEMPORARY TABLE product_popularity
                                SELECT
                                product_id,
                                COUNT(id) popularity
                                FROM sale_order_item_product
                                GROUP BY product_id
                                ORDER BY count(id) DESC;"
        );
        QueryMapper::query("UPDATE 
                                product p, 
                                product_popularity pp 
                            SET 
                                p.popularity = pp.popularity 
                            WHERE 
                                p.id = pp.product_id");

        echo "Done";
        exit();
    }
}