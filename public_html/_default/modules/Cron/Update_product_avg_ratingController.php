<?php
namespace _Default\Cron;
use Core\MainController;
use Core\QueryMapper;

class Update_product_avg_ratingController extends MainController
{

    function run()
    {
        QueryMapper::query("DROP TABLE IF EXISTS product_avg_rating");
        QueryMapper::query("CREATE TEMPORARY TABLE product_avg_rating
                                SELECT 
                                  product_id, 
                                  AVG(rating) avg_rating 
                                FROM 
                                  product_review 
                                GROUP BY product_id;"
        );
        QueryMapper::query("UPDATE 
                                product p, 
                                product_avg_rating par 
                            SET 
                                p.avg_rating = par.avg_rating 
                            WHERE 
                                p.id = par.product_id");

        echo "Done";
        exit();
    }
}