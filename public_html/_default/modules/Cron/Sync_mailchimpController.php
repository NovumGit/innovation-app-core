<?php
namespace _Default\Cron;
use Core\MainController;
use Core\QueryMapper;
use Core\Setting;
use DrewM\MailChimp\MailChimp;
use Helper\MailChimpHelper;
use Model\Category\CategoryQuery;
use Model\Crm\Customer;
use Model\Crm\CustomerAddress;
use Model\Crm\CustomerQuery;
use Model\Sale\SaleOrderQuery;
use Model\Setting\MasterTable\Base\LanguageQuery;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\Language;

class Sync_mailchimpController extends MainController
{
    function run()
    {
        /*
        if(Setting::get('mailchimp_sync_busy'))
        {
            exit('Already running';)
        }
        */
        Setting::store('mailchimp_sync_busy', true);

        $sMailchimpApiKey = Setting::get('mailchimp_api_key');
        $oMailChimp = new MailChimp($sMailchimpApiKey);
        $aLists = $oMailChimp->get('lists');

        if(isset($aLists['lists']) && !empty($aLists['lists']))
        {
            foreach($aLists['lists'] as $aList)
            {
                if(!MailChimpHelper::syncsWithNuiCart($aList['id']))
                {
                    continue;
                }
                $aInterrests = $this->createInterrests($oMailChimp, $aList['id']);

                $sTableName = MailChimpHelper::getMailChimpListId($aList['id']);

                // Maak een tijdelijke tabel aan als die nog niet bestond.
                $sQuery = "CREATE TABLE IF NOT EXISTS `$sTableName` (
                            `id` int(11) NOT NULL AUTO_INCREMENT,
                            `customer_id` int(11) NOT NULL,
                            `is_send` tinyint(1) NOT NULL DEFAULT 0,
                             PRIMARY KEY (`id`)
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8";

                QueryMapper::query($sQuery);

                // Een iterator om over de resultaten te loopen, filters zijn al toegepast.
                $oCustomerQuery = MailChimpHelper::getQueryObjectByForeignMailchimpId($aList['id']);

                // Niet de records syncen die al in MailChimp voorkomen.


                $sCustomerQuery = "SELECT id FROM customer WHERE id NOT IN(SELECT customer_id FROM `$sTableName`)";

                /*
                $sQuery = "SELECT customer_id FROM `$sTableName`";
                $aRows = QueryMapper::fetchArray($sQuery);
                $aCustomerIds = Utils::flattenArray($aRows, 'customer_id');

                $oCustomerQuery->filterById($aCustomerIds, Criteria::NOT_IN);

                $aCustomers = $oCustomerQuery->find();
                */
                $aCustomerIds = QueryMapper::fetchArray($sCustomerQuery);

                $oMailChimpBatch = $oMailChimp->new_batch();
                $i=0;
                foreach($aCustomerIds as $aCustomer)
                {
                    $oCustomer = CustomerQuery::create()->findOneById($aCustomer['id']);
                    if($i > 1000)
                    {
                        break;
                    }
                    $oCustomerAddress = $oCustomer->getAnyCustomerAddress();
                    $aLatLong = null;
                    if($oCustomerAddress instanceof CustomerAddress)
                    {
                        $oCountry = $oCustomerAddress->getCountry();
                        if($oCountry instanceof Country)
                        {
                            $aLatLong = ['latitude' => $oCountry->getLatitude(), 'longitude' => $oCountry->getLongitude()];
                        }
                    }
                    $sQuery = "INSERT INTO `$sTableName` (customer_id, is_send) VALUE ({$oCustomer->getId()}, 0)";
                    QueryMapper::query($sQuery);

                    $aSend = [];
                    $aSend['email_type'] = 'html';

                    if($aInterrestIds = $this->getCustomerInterrests($oCustomer, $aInterrests))
                    {
                        $aSend['interests'] = $aInterrestIds;
                    }

                    $aSend['language'] = $this->getSubscriberLanguage($oCustomer->getLanguageId());
                    $aSend['email_address'] = $oCustomer->getEmail();
                    $aSend['status'] = 'subscribed';

                    if(!empty($aLatLong) && $aLatLong['latitude'] && $aLatLong['longitude'])
                    {
                        $aSend['location'] = $aLatLong;
                    }
                    $aSend['merge_fields'] = [
                        'FNAME' => $oCustomer->getFirstName(),
                        'LNAME'=>$oCustomer->getLastName()
                    ];
                    if($oCustomer->getIp())
                    {
                        $aSend['ip_signup'] = $oCustomer->getIp();
                    }
                    $aSend['timestamp_signup'] = $oCustomer->getCreatedOn('U');


                    $aItem = $oMailChimp->post("lists/{$aList['id']}/members", $aSend);

                    // var_dump($aItem);
                    // echo "aaaaaaaaaa";
                    // exit();
                    // $oMailChimpBatch->post('ml_'.$oCustomer->getId(), "lists/{$aList['id']}/members", $aSend);
                    $i++;
                }
                // $result = $oMailChimpBatch->execute();
                $sQuery = "UPDATE`$sTableName` SET is_send = 1";
                QueryMapper::query($sQuery);
                // var_dump($result);
            }
        }
        Setting::store('mailchimp_sync_busy', false);
        exit();
    }
    function getCustomerInterrests(Customer $oCustomer, $aInterrests)
    {
        $aCustomerInterrests = [];
        $aSaleOrders = SaleOrderQuery::create()->filterById($oCustomer->getId())->find();
        if($aSaleOrders->isEmpty())
        {
            return null;
        }
        foreach($aSaleOrders as $oSaleOrder)
        {
            $aSaleOrderItems = $oSaleOrder->getSaleOrderItems();

            if($aSaleOrderItems->isEmpty())
            {
                continue;
            }

            foreach($aSaleOrderItems as $oSaleOrderItem)
            {
                $iCategoryId = $oSaleOrderItem->getFirstProduct()->getCategoryId();
                $aCustomerInterrests[] = $aInterrests[$iCategoryId]['mailchimp_id'];
            }
        }
        return join(', ', $aCustomerInterrests);

    }
    function createInterrests(MailChimp $oMailChimp, $iListId)
    {
        $aCategories = CategoryQuery::create()->find();
        if($aCategories->isEmpty())
        {
            return null;
        }
        $i=0;
        $aInterrests = [];
        $aAlreadyInstalledInterrestsRaw[] = $oMailChimp->get("/lists/$iListId/interest-categories");
        $aAlreadyInstalledInterrests = [];
        if(isset($aAlreadyInstalledInterrestsRaw[0]) && isset($aAlreadyInstalledInterrestsRaw[0]['categories']))
        {
            foreach($aAlreadyInstalledInterrestsRaw[0]['categories'] as $aAlreadyInstalledInterrest)
            {
                $aAlreadyInstalledInterrests[$aAlreadyInstalledInterrest['title']] = $aAlreadyInstalledInterrest['id'];
            }
        }
        foreach($aCategories as $oCategory)
        {
            if(!isset($aAlreadyInstalledInterrests[$oCategory->getName()]))
            {
                $aInterrests[$oCategory->getId()] = [
                    'type' => 'checkboxes',
                    'display_order' => $i,
                    'title' => $oCategory->getName()
                ];

                $aResponse = $oMailChimp->post("/lists/$iListId/interest-categories", $aInterrests[$oCategory->getId()]);
                $aInterrests[$oCategory->getId()]['mailchimp_id'] = $aResponse['id'];
            }
            $i++;
        }
        return $aInterrests;
    }
    function getSubscriberLanguage($iLocalLanguageId)
    {
        $oLanguage = LanguageQuery::create()->findOneById($iLocalLanguageId);
        if(!$oLanguage instanceof Language)
        {
            return 'en';
        }
        $aMailChimpSupporedLanguage = ['en', 'ar','af', 'be','bg', 'ca', 'zh', 'hr', 'cs', 'da', 'nl', 'et','fa', 'fi', 'fr', 'de', 'el', 'he', 'hi', 'hu', 'is', 'id', 'ga', 'it', 'ja', 'km', 'ko', 'lv','lt', 'mt', 'ms', 'mk','no', 'pl', 'pt', 'ro', 'ru', 'sr', 'sk', 'sl', 'es', 'sw', 'sv', 'ta', 'th', 'tr', 'uk', 'vi'];

        if(in_array($oLanguage->getShopUrlPrefix(), $aMailChimpSupporedLanguage))
        {
            return $oLanguage->getShopUrlPrefix();
        }
    }
}
