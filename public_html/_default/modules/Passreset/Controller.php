<?php
namespace _Default\Passreset;

use _Default\_DefaultController;
use Core\Customer;
use Core\StatusMessage;
use Core\Translate;
use Model\Crm\CustomerQuery;

class Controller extends _DefaultController {
    function doResetPass()
    {

        $bFormIsValid = true;
        $oLanguage = $this->getCurrentLanguage();
        $oLanguage->getShopUrlPrefix();
        $aData = $this->post('reset');
        $sEmailAddress = $aData['email'];

        if (!filter_var($sEmailAddress, FILTER_VALIDATE_EMAIL))
        {
            StatusMessage::warning(Translate::fromCode("Please specify a valid e-mail address."));
            $bFormIsValid = false;
        }
        if($bFormIsValid)
        {
            $oCustomer = CustomerQuery::create()->findOneByEmail($sEmailAddress);
            if(!$oCustomer)
            {
                StatusMessage::danger(Translate::fromCode("E-mailadres of wachtwoord is incorrect."));
                $this->redirect($this->getRequestUri(false));
            }
            Customer::sendPassResetMail($oCustomer, $oLanguage->getId());
            StatusMessage::success(Translate::fromCode("If your e-mailaddress is present in our system, a new password has been send to you."));
        }
        else
        {
            StatusMessage::success(Translate::fromCode("Please specify a valid e-mailaddress."));
        }
        $this->redirect('/'.$oLanguage->getShopUrlPrefix()."/customer/login?email=$sEmailAddress&origin=passreset");
        exit();
    }
    function runShop()
    {
        $aData = $this->post('reset');

        if (empty($aData) && Customer::isSignedIn())
        {
            $aData['email'] = Customer::getMember()->getEmail();
        }
        $aViewData = [
            'data' => $aData,
            'rand_mail' => $_SESSION['rand_mail']
        ];
        $aView = [
            'content' => $this->parse('Passreset/reset.twig', $aViewData),
            'title' => Translate::fromCode("Wachtwoord resetten")
        ];
        return $aView;
    }
}
