<?php
namespace _Default;

class CategoryItem
{
    private $sUrl;
    private $sTitle;
    private $sType;

    function __construct($sTitle, $sUrl, $sType = 'menu-item')
    {
        $this->sUrl = $sUrl;
        $this->sTitle = $sTitle;
        $this->sType = $sType;
    }
    
    function getType()
    {
        return $this->sType;
    }

    function getUrl()
    {
        return $this->sUrl;
    }

    function getTitle()
    {
        return $this->sTitle;
    }
}