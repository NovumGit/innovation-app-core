<?php
namespace _Default\Cart;

use _Default\_DefaultController;
use Core\Customer;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderQuery;

class Open_orderController extends _DefaultController  {

    function __construct($aGet, $aPost)
    {
        if(!Customer::isSignedIn())
        {
            $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
            DeferredAction::register('open_order', $this->getRequestUri());
            $this->redirect('/'.$sPrefix.'/account/login?r=open_order');
        }
        parent::__construct($aGet, $aPost);
    }

    function runShop()
    {

        $iOrderId = $this->get('order_id');
        $oCustomer = Customer::getMember();
        $sRFail = $this->get('r_fail');

        $sFailUrl = DeferredAction::get($sRFail);


        $oSaleOrder = SaleOrderQuery::create()
                        ->filterByCustomerId($oCustomer->getId())
                        ->filterById($iOrderId)
                        ->findOne();

        if(!$oSaleOrder instanceof SaleOrder)
        {
            StatusMessage::info(Translate::fromCode("That order does not seem to belong to you, sorry but you are not allowed to open it."));
            $this->redirect($sFailUrl);
        }
        else if($oSaleOrder->getIsFullyPaid())
        {
            StatusMessage::warning(Translate::fromCode("Based on our information, you already paid for this order. You may pay more then once but that would not be fair. (-:"));
            $this->redirect($sFailUrl);
        }
        else
        {
            $_SESSION['CURRENT_SALE_ORDER'] =  $oSaleOrder->getId();
        }
        StatusMessage::info(Translate::fromCode("You can now complete this order."));
        $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
        $this->redirect('/'.$sPrefix.'/checkout/final');

    }
}
