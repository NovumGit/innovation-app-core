<?php
namespace _Default\Cart;

use _Default\_DefaultController;
use Core\Currency;
use Core\DeferredAction;
use Core\Logger;
use Core\Setting;
use Core\StatusMessage;
use Core\StatusMessageButton;
use Core\Translate;
use Core\Utils;
use Exception\LogicException;
use Helper\SaleHelper;
use Model\CustomerQuery;
use Model\Product;
use Model\Sale\OrderItemProductQuery;
use Model\Setting\MasterTable\ProductUnit;

class AddController extends _DefaultController  {

    function runShop(){

        if(strstr(strtolower($_SERVER['HTTP_USER_AGENT']), "bot"))
        {
            Utils::jsonExit(['status' => 'no bots allowed here']);
        }

        $iProductId = $this->get('product_id', null, true, 'numeric');
        $iQuantity = $this->get('quantity', 1, true, 'numeric');
        $oProduct = CustomerQuery::create()->findOneById($iProductId);

        if(!$oProduct instanceof Product)
        {
            throw new LogicException("Could not find the product that you where adding.");
        }


        $oSaleOrder = $this->getCurrentSaleOrder();
        Logger::info("Product added to basket {$oSaleOrder->getId()} - $iProductId $iQuantity");

        $bModifiedQuantity = false;
        if(!$oSaleOrder->getSaleOrderItems()->isEmpty())
        {
            foreach($oSaleOrder->getSaleOrderItems() as $oSaleOrderItem)
            {
                $oOrderItemProduct = OrderItemProductQuery::create()->findOneBySaleOrderItemId($oSaleOrderItem->getId());

                $oProduct = $oOrderItemProduct->getProduct();
                $oUnit = $oProduct->getProductUnit();

                if($oUnit instanceof ProductUnit && $oUnit->getCode() == 'cm')
                {
                    $iRealQuantity = $iQuantity / 100;
                }
                else
                {
                    $iRealQuantity = $iQuantity;
                }

                if($oOrderItemProduct->getProductId() == $iProductId)
                {
                    $iNewQuantity = $oSaleOrderItem->getQuantity() + $iRealQuantity;
                    $oSaleOrderItem->setQuantity($iNewQuantity);
                    $oSaleOrderItem->save();
                    $bModifiedQuantity = true;
                }
            }
        }

        if(!$bModifiedQuantity)
        {
            $oLanguage = $this->getCurrentLanguage();
            $oProduct = CustomerQuery::create()->findOneById($iProductId);
            $oUnit = $oProduct->getProductUnit();

            if($oUnit instanceof ProductUnit && $oUnit->getCode() == 'cm')
            {
                $iRealQuantity = $iQuantity / 100;
            }
            else
            {
                $iRealQuantity = $iQuantity;
            }

            SaleHelper::addProductToOrder($iProductId, $oSaleOrder->getId(), $oProduct->getSalePrice(), $iRealQuantity, $oLanguage);
        }
        $sDeferredAction = DeferredAction::get('after_add');
        $oLanguage = $this->getCurrentLanguage();

        $aButtons = [
            'stay_button' => new StatusMessageButton(
                Translate::fromCode('Continue shopping'),
                $sDeferredAction,
                Translate::fromCode('Click here to continue shopping'),
                'success'
            ),
            'checkout' => new StatusMessageButton(
                Translate::fromCode('Checkout'),
                '/'.$oLanguage->getShopUrlPrefix().'/checkout/cart',
                Translate::fromCode('Click here to proceed to checkout'),
                'success'
            )
        ];

        $sTitle = Translate::fromCode("The product has been added to your basket.");

        if(Setting::get('free_shipping_above'))
        {
            if($oSaleOrder->calcHasFreeShipping())
            {
                $sContents = Translate::fromCode("You have free shipping on this order");
            }
            else
            {
                $fFreeShippingFrom = Currency::formatForPresentation($oSaleOrder->calcFreeShippingAfter());
                $sContents = Translate::fromCode("Order for [amount] euro's extra and get ")."<b>".Translate::fromCode('free shipping')."</b>.";
                $sContents = str_replace('[amount]', $fFreeShippingFrom, $sContents);
            }
            StatusMessage::success($sContents, $sTitle, $aButtons);
        }
        else
        {
            $sTitle = Translate::fromCode("Item in winkemandje");
            $sContents = Translate::fromCode("Uw bestelling is toegevoegd aan uw winkelmandje.");
            StatusMessage::success($sContents, $sTitle, $aButtons);
        }

        if($sDeferredAction)
        {
            $this->redirect($sDeferredAction);
        }
        $this->redirect('/checkout/cart');
    }
}
