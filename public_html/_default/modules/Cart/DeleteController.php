<?php
namespace _Default\Cart;

use _Default\_DefaultController;
use Core\Currency;
use Core\Utils;
use Model\Sale\SaleOrderItemQuery;

class DeleteController extends _DefaultController {

    function runShop()
    {
        $iSaleOrderItemId = $this->get('order_item_id', null, true, 'numeric');
        $oSaleOrder = $this->getCurrentSaleOrder();

        SaleOrderItemQuery::create()
            ->filterBySaleOrderId($oSaleOrder->getId())
            ->filterById($iSaleOrderItemId)
            ->delete();

        $aResult['total_product_price'] = Currency::priceFormat($oSaleOrder->getTotalProductPrice());
        $aResult['product_type_count'] = $oSaleOrder->getSaleOrderItems()->count();
        Utils::jsonExit($aResult);
    }
}
