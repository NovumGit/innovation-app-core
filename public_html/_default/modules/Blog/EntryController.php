<?php
namespace _Default\Blog;

use _Default\_DefaultController;
use _Default\LastViewed;
use Model\Cms\SiteBlogQuery;

class EntryController extends _DefaultController
{
    function runShop()
    {

        $sCurrentUrl = $this->getRequestUri();
        $aUrlParts = explode('-', $sCurrentUrl);
        $iBlogId = end($aUrlParts);

        $sAuthor = $this->get('author');

        if($sAuthor)
        {
            $aBlogPosts = BlogHelper::getPosts($sAuthor);
        }
        else
        {
            $aBlogPosts = BlogHelper::getPosts();
        }




        $oBlog = SiteBlogQuery::create()->findOneById($iBlogId);
        $aViewData = [
            'entry' => $oBlog,
            'recent_products' => LastViewed::getList(),
            'posting_users' => BlogHelper::getBlogPosterNames(),
            'blog_posts' => $aBlogPosts,
            'total_blog_count' => SiteBlogQuery::create()->count()
        ];

        $aResult['content'] = $this->parse('Blog/entry.twig', $aViewData);
        $aResult['title'] =  $oBlog->getTitle();

        return $aResult;
    }

}
