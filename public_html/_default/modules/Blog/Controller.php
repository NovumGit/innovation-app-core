<?php
namespace _Default\Blog;

use _Default\_DefaultController;
use _Default\LastViewed;
use Core\Translate;
use Model\Cms\SiteBlogQuery;

class Controller extends _DefaultController {

    function runShop()
    {
        $sAuthor = $this->get('author');

        if($sAuthor)
        {
            $aBlogPosts = BlogHelper::getPosts($sAuthor);
        }
        else
        {
            $aBlogPosts = BlogHelper::getPosts();
        }




        $aViewData = [
            'recent_products' => LastViewed::getList(),
            'posting_users' => BlogHelper::getBlogPosterNames(),
            'blog_posts' => $aBlogPosts,
            'total_blog_count' => SiteBlogQuery::create()->count()
        ];


        $aResult['content'] = $this->parse('Blog/overview.twig', $aViewData);
        $aResult['title'] = Translate::fromCode("Blog");

        return $aResult;
    }
}
