<?php
namespace _Default\Blog;

use Core\QueryMapper;
use Model\Account\UserQuery;
use Model\Cms\SiteBlogQuery;
use Model\Cms\SiteQuery;

class BlogHelper{

    static function getPosts($sAuthor = null)
    {
        $aSiteSettings = getSiteSettings();
        $oSite = SiteQuery::create()->findOneByDomain($aSiteSettings['live_domain']);
        $oSiteBlogQuery = SiteBlogQuery::create()->filterBySite($oSite);

        if($sAuthor)
        {
            $aUserIds = [];
            $oUserQuery = UserQuery::create();
            $aUsers = $oUserQuery->findByFirstName($sAuthor);

            foreach ($aUsers as $oUser)
            {
                $aUserIds[] = $oUser->getId();
            }
            $oSiteBlogQuery->filterByCreatedByUserId($aUserIds);
        }
        return $oSiteBlogQuery->find();
    }
    static function getBlogPosterNames()
    {
        $aSiteSettings = getSiteSettings();

        $sQuery = "SELECT 
                      u.first_name,
                       count(sb.id) num_posts
                    FROM 
                      `user` u,
                      site_blog sb,
                      site s
                    WHERE 
                      s.id = sb.site_id
                    AND s.domain = '{$aSiteSettings['live_domain']}'
                    AND u.id = sb.created_by_user_id
                    GROUP BY u.first_name";

        return QueryMapper::fetchArray($sQuery);
    }
}