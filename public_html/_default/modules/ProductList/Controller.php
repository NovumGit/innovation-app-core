<?php
namespace _Default\ProductList;

use _Default\_DefaultController;
use Core\DeferredAction;
use Model\Category\CategoryQuery;
use Model\Category\CategoryTranslationQuery;
use Model\CustomerQuery;
use Model\Product;
use Propel\Runtime\ActiveQuery\Criteria;

class Controller extends _DefaultController  {

    function runShop()
    {

        $sSort = $this->get('sort', 'popularity');

        $oCurrentLanguage = $this->getCurrentLanguage();
        $sUrl = $this->getRequestUri();
        $aUrlParts = explode('-', $sUrl);
        $iCategoryId = array_pop($aUrlParts);

        $oCategory = CategoryQuery::create()->findOneById($iCategoryId);


        $oCategoryTranslation = CategoryTranslationQuery::create()
                                    ->filterByCategoryId($iCategoryId)
                                    ->filterByLanguageId($oCurrentLanguage->getId())
                                    ->findOne();

        $oProductQuery = CustomerQuery::create();
        $oProductQuery->filterByDerrivedFromId(null);
        $oProductQuery->filterByCategoryId($iCategoryId);

        if($sSort == 'popularity')
        {
            $oProductQuery->orderByPopularity(Criteria::ASC);
        }
        else if($sSort == 'rating')
        {
            $oProductQuery->orderByAvgRating(Criteria::ASC);
        }

        $aProducts  = CustomerQuery::create()
                        ->filterByDerrivedFromId(null)
                        ->filterByCategoryId($iCategoryId)
                        ->find();

        $aBannerAreaVars = [
            'category' => $oCategory,
            'category_translation' => $oCategoryTranslation,
        ];

        $sTranslatedUnitName  = '';
        $oProduct = CustomerQuery::create()->filterByDerrivedFromId(null)->findOneByCategoryId($oCategory->getId());

        if($oProduct instanceof Product)
        {
            $sTranslatedUnitName = $oProduct->getTranslatedUnitName($oCurrentLanguage->getId());
        }

        $sBannerArea = $this->parse('ProductList/banner_area.twig', $aBannerAreaVars);
        DeferredAction::register('after_add_wishlist', $this->getRequestUri());
        DeferredAction::register('after_add', $this->getRequestUri());



        $aViewData = [
            'sort' => $sSort,
            'view' => $this->get('view', 'tiles'),
            'category' => $oCategory,
            'products' => $aProducts,
            'banner_area' => $sBannerArea,
            'unit_name' => $sTranslatedUnitName
        ];

        $aResult['content'] = $this->parse('ProductList/all_products.twig', $aViewData);
        $aResult['title'] =  $oCategoryTranslation->getName();
        return $aResult;
    }
}
