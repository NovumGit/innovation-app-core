<?php
namespace _Default;

class MainMenuItem
{
    private $sUrl;
    private $sName;

    function __construct($sName, $sWebshopUrlNoLang)
    {
        $this->sUrl = $sWebshopUrlNoLang;
        $this->sName = $sName;
    }
    function getName()
    {
        return $this->sName;
    }

    function getWebshopUrl()
    {
        return $this->sUrl;
    }
}