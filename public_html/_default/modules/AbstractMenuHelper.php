<?php
namespace _Default;

abstract class AbstractMenuHelper{
    abstract function getTranslatedMainMenu(int $iLanguageId);
}