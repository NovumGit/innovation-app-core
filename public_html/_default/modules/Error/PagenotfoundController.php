<?php
namespace _Default\Error;

use Core\Logger;
use Core\MainController;

class PagenotfoundController extends MainController
{
    function run()
    {

        $sReferer = 'not set';
        if(isset($_SERVER['HTTP_REFERER']))
        {
            $sReferer = $_SERVER['HTTP_REFERER'];
        }

        Logger::pageNotFound("Pagina niet gevonden, url ".$_SERVER['REQUEST_URI'].' referrer: '.$sReferer);

        $this->redirect('/');
        $aView['content'] = $this->parse('Error/404.twig', []);

        return $aView;
    }
}
