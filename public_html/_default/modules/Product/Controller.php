<?php
namespace _Default\Product;

use _Default\_DefaultController;
use _Default\LastViewed;
use Core\Customer;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use emberlabs\GravatarLib\Gravatar;
use Exception\LogicException;
use Model\Category\Base\CategoryQuery;
use Model\Category\Category;
use Model\Category\CategoryTranslationQuery;
use Model\Category\ProductMultiCategoryQuery;
use Model\CustomerQuery;
use Model\Product;
use Model\Product\Base\RelatedProductQuery;
use Model\Product\Image\Base\ProductImageQuery;
use Model\ProductReview;
use Model\ProductReviewQuery;
use Model\ProductTranslation;
use Model\ProductTranslationQuery;
use Model\Setting\MasterTable\Language;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Collection\ObjectCollection;

class Controller extends _DefaultController  {

    function doAddReview()
    {
        if(Customer::isSignedIn())
        {
            $aData = $this->post('review');
            $oReview = new ProductReview();
            $oReview->setCustomerId(Customer::getMember()->getId());

            $oReview->setProductId($this->getProductIdFromUrl());
            $oReview->setDescription($aData['description']); // in $_POOST
            $oReview->setRating($aData['rating']);
            $oReview->save();

            StatusMessage::success(Translate::fromCode('Your review has been registered, to prevent spam we moderate all reviews before publication. Thank you for your info on this product!.')) ; // So this actually shows somethignon screen
            $this->redirect($this->getRequestUri(false));
        }
        else
        {
            DeferredAction::register('review_store', $this->getRequestUri().'?_do=AddReview');
            StatusMessage::warning(Translate::fromCode('Only signed in uses can leave reviews, please sign in'));
            $_SESSION['review'] = $this->post('review');  // this should have the whole form ebcaues i used an array over there
            $this->redirect('/'.$this->getCurrentLanguage()->getShopUrlPrefix().'/customer/login?r=review_store');
        }
    }
    protected function getProductIdFromUrl()
    {
        $sCurrentUrl =  $this->getRequestUri();
        $iProductId = explode('-',$sCurrentUrl);
        $iProductId = end($iProductId);
        return $iProductId;
    }
    function runShop()
    {
        $sBackUrl = DeferredAction::get('previous_page');
        $iProductId = $this->getProductIdFromUrl();
        $aLastViewedProducts = LastViewed::getList();
        LastViewed::register($iProductId);

        $oProductQuery = CustomerQuery::create();
        $oProduct = $oProductQuery->findOneById($iProductId);
        $oProduct->getProductReviews();

        if($oProduct->getItemDeleted())
        {
            StatusMessage::warning(Translate::fromCode('Het product waar u naar op zoek was kunnen wij momenteel helaas niet meer leveren.'));
            $this->redirect('/', 301);
        }

        DeferredAction::register('after_add_wishlist', $this->getRequestUri());
        DeferredAction::register('review_store', $this->getRequestUri().'?_do=AddReview');
        DeferredAction::register('after_add', $this->getRequestUri());

        $aReviews = ProductReviewQuery::create()->orderByCreatedDate(Criteria::DESC)
                        ->filterByProductId($oProduct->getId())
                        ->filterByApproved(true)
                        ->find();

        include __DIR__ . '/Gravatar.php';
        $gravatar = new Gravatar();

        $aProductImages = ProductImageQuery::create()->orderBySorting(Criteria::DESC)
                        ->filterByProductId($oProduct->getId())
                        ->filterByIsOnline(true)
                        ->find();

        $aRelatedProductLinks = RelatedProductQuery::create()
            ->filterByProductId($oProduct->getId())
            ->orderBySorting()
            ->find();

        $aRelatedProducts = null;
        if(!$aRelatedProductLinks->isEmpty()) {
            foreach ($aRelatedProductLinks as $oProductLink) {
                $oRelatedProduct =  CustomerQuery::create()->findOneById($oProductLink->getOtherProductId());
                if(!$oRelatedProduct->getDeletedByUserId())
                {
                    $aRelatedProducts[] = $oRelatedProduct;
                }

            }
        }
        $oCurrentLanguage = $this->getCurrentLanguage();
        if($this->getRequestUri() == '/' && $oCurrentLanguage instanceof Language)
        {
            if(!trim($oCurrentLanguage->getShopUrlPrefix()))
            {
                throw new LogicException("ShopUrlPrefix is not configured for the default webshop language.");
            }
            $this->redirect('/'.$oCurrentLanguage->getShopUrlPrefix(), 301);
        }
        $iMainCategory = $oProduct->getCategoryId();
        $oCategory = CategoryQuery::create()->filterByParentId($iMainCategory)->find();
        $aCategoryProducts = null;
        $aOtherCategories = [];
        if(!$oCategory->isEmpty())
        {
            foreach ($oCategory as $oCategories)
            {
                $oCategoryTranslation = CategoryTranslationQuery::create()
                    ->filterByLanguageId($oCurrentLanguage->getId())
                    ->filterByCategoryId($oCategories->getId())
                    ->findOne();
                $aOtherCategories[]= [
                    'category_name' => $oCategories->getName(),
                    'cat_products' => CustomerQuery::create()->filterByDerrivedFromId(null)->findByCategoryId($oCategories->getId()),
                    'category_translation' => $oCategoryTranslation
                ];
            }
        }
        $aSimilarProducts = null;
        $aSimilarProducts = new ObjectCollection();
        if((10 - $aLastViewedProducts->count()) > 1)
        {
            $aSimilarProducts = CustomerQuery::create()
                ->filterByDerrivedFromId(null)
                ->join('Product.ProductImage')
                ->filterById($oProduct->getId(), Criteria::NOT_IN)
                ->limit(10 - $aLastViewedProducts->count())
                ->filterByCategoryId($oProduct->getCategoryId())
                ->find();
        }
        $sTranslatedUnitName = $oProduct->getTranslatedUnitName($oCurrentLanguage->getId());
        $sBannerImage = $this->getBannerImage($oProduct);

        $sReturnUrl = DeferredAction::get('origin_category_page');
        if($sReturnUrl)
        {
            $sReturnUrl = '/'.$oCurrentLanguage->getShopUrlPrefix().$oProduct->getCategory()->getUrl();
        }

        $oProductTranslation = ProductTranslationQuery::create()
                                ->filterByProductId($oProduct->getId())
                                ->filterByLanguageId($oCurrentLanguage->getId())
                                ->findOne();

        $aViewData = [
            'back_url' => $sBackUrl,
            'product'   => $oProduct,
            'banner_image' => $sBannerImage,
            'origin_category_page' => $sReturnUrl,
            'unit_name' => $sTranslatedUnitName,
            'category'   => $oProduct->getCategory(),
            'reviews'   => $aReviews,
            'gravatar'  => $gravatar,
            'product_images' => $aProductImages,
            'related_products' => $aRelatedProducts,
            'last_viewed_products' => $aLastViewedProducts,
            'other_categories' => $aOtherCategories,
            'similar_products' => $aSimilarProducts,
            'product_category_name' => CategoryTranslationQuery::create()->filterByLanguageId($oCurrentLanguage->getId())->findOneByCategoryId($oProduct->getCategoryId())
        ];

        // Here a template is parsed, product.twig and you pass data to that template $aViewData
        $aResult['content'] = $this->parse('Product/product.twig', $aViewData);



        $aResult['title'] =  $oProduct->getTitle();
        if($oProductTranslation instanceof ProductTranslation)
        {
            $aResult['meta_description'] = $oProductTranslation->getMetaDescription();
            $aResult['keywords']  = $oProductTranslation->getMetaKeywords();
        }

        return $aResult;
    }
    function getBannerImage(Product $oProduct)
    {
        $aProductMultiCategory = ProductMultiCategoryQuery::create()->findByProductId($oProduct->getId());
        if(!$aProductMultiCategory->isEmpty())
        {
            foreach($aProductMultiCategory as $oProductMultiCategory)
            {
                $oCategory = $oProductMultiCategory->getCategory();
                if($oCategory instanceof Category && $oCategory->getImagePath())
                {
                    return $oCategory->getImagePath();
                }
            }
        }
        $oCategory = $oProduct->getCategory();
        if($oCategory instanceof Category)
        {
            return $oCategory->getImagePath();
        }
        return null;
    }
}
