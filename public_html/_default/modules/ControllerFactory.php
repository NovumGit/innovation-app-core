<?php
namespace _Default;

use _Default\Checkout\ErrorController;
use _Default\Customer\LoginController;
use _Default\Mail\CustomerController;
use _Default\Mail\NewsletterconfirmController;
use _Default\Mail\SupplierController;
use _Default\Product\Controller;
use Core\ControllerFactory as CoreControllerFactory;
use Core\IControllerFactory;
use Core\MainController;
use Vangoolstoffenonline\Checkout\FinalController;

class ControllerFactory extends CoreControllerFactory implements IControllerFactory {

    public function getController(array $aGet, array $aPost, string $sNamespace = null):MainController
    {
        $sRequestUri = $_SERVER['REQUEST_URI'];
        $sRequestUriNoLang = preg_replace('/^\/[a-z]{2}\//', '', $sRequestUri);

        if(preg_match('/^p\//', $sRequestUriNoLang))
        {
            $oController  = new Controller($aGet, $aPost);
        }
        else if(strpos($sRequestUriNoLang, 'checkout/error') === 0)
        {
	        $oController  = new ErrorController($aGet, $aPost);
        }
        else if(strpos($sRequestUriNoLang, 'checkout/final') === 0)
        {
	        $oController  = new FinalController($aGet, $aPost);
        }
        else if(strpos($sRequestUriNoLang, 'mail/customer') === 0)
        {
            $oController  = new CustomerController($aGet, $aPost);
        }
        else if(strpos($sRequestUriNoLang, 'mail/supplier') === 0)
        {
            $oController  = new SupplierController($aGet, $aPost);
        }
        else if(strpos($sRequestUriNoLang, 'mail/newseletterconfirm') === 0)
        {
            $oController  = new NewsletterconfirmController($aGet, $aPost);
        }
        else if(strpos($sRequestUriNoLang, 'mail') === 0)
        {
	        $oController  = new Mail\Controller($aGet, $aPost);
        }
        else if(strpos($sRequestUriNoLang, 'customer/login') === 0)
        {
            $sFullQualifiedCustomClassname = "\\$sNamespace\\Customer\\LoginController";

            if(class_exists($sFullQualifiedCustomClassname))
            {
                $oController  = new $sFullQualifiedCustomClassname($aGet, $aPost);
            }
            else
            {
                $oController  = new LoginController($aGet, $aPost);
            }
        }
        else if(strpos($sRequestUriNoLang, 'passreset') === 0)
        {
            $oController  = new \_Default\Passreset\Controller($aGet, $aPost);
        }
        else if($sRequestUriNoLang == '/ubsubscribe')
        {
            $oController  = new \_Default\Robots\Controller($aGet, $aPost);
        }
        else if($sRequestUriNoLang == '/robots.txt')
        {
            $oController  = new \_Default\Robots\Controller($aGet, $aPost);
        }
        else if(preg_match('/^c\//', $sRequestUriNoLang))
        {
            $sFullQualifiedCustomClassname = "\\$sNamespace\\ProductList\\Controller";

            if(class_exists($sFullQualifiedCustomClassname))
            {
                $oController  = new $sFullQualifiedCustomClassname($aGet, $aPost);
            }
            else
            {
                $oController  = new \_Default\ProductList\Controller($aGet, $aPost);
            }
        }
        else if(preg_match('/^faq$/', $sRequestUriNoLang))
        {
            $sFullQualifiedCustomClassname = "\\$sNamespace\\Faq\\OverviewController";

            if(class_exists($sFullQualifiedCustomClassname))
            {
                $oController  = new $sFullQualifiedCustomClassname($aGet, $aPost);
            }
            else
            {
                $oController  = new \_Default\Faq\OverviewController($aGet, $aPost);
            }
        }
        else if(preg_match('/^s\/[a-z0-9-]+/', $sRequestUriNoLang))
        {
            $oController  = new \_Default\Page\Controller($aGet, $aPost);
        }
        else if(preg_match('/^b\/[a-zA-Z0-9-]+/', $sRequestUriNoLang))
        {
            $oController  = new \_Default\Blog\EntryController($aGet, $aPost);
        }
        else if(preg_match('/^b\/blog/', $sRequestUriNoLang))
        {
            $oController  = new \_Default\Blog\Controller($aGet, $aPost);
        }

        if(!isset($oController))
        {
            $oController  = parent::getController($aGet, $aPost, '_Default');
        }
        return $oController;
    }
}
