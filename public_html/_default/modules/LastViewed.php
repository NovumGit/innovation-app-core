<?php
namespace _Default;

use Model\CustomerQuery;
use Propel\Runtime\Collection\ObjectCollection;

class LastViewed{

    static function register($iProductId)
    {
        if(!isset($_SESSION['last_viewed']))
        {
            $_SESSION['last_viewed'] = [];
        }
        // Add the current product to the beginning of the array.
        array_unshift($_SESSION['last_viewed'], $iProductId);

        if(count($_SESSION['last_viewed']) > 10)
        {
            // Pop the oldest item from the end of the array
            array_pop($_SESSION['last_viewed']);
        }
    }
    static function getList():ObjectCollection
    {
        $aItems = new ObjectCollection();
        if(!empty($_SESSION['last_viewed']))
        {
            $aItems = CustomerQuery::create()->filterById($_SESSION['last_viewed'])->find();
        }
        return $aItems;
    }
}