<?php
namespace _Default\Account;

use _Default\_DefaultController;
use Core\Cfg;
use Core\Config;
use Core\Customer;
use Core\DeferredAction;

class InvoiceController extends _DefaultController {

    function __construct($aGet, $aPost)
    {
        if(!Customer::isSignedIn())
        {
            $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
            DeferredAction::register('invoice', $this->getRequestUri());
            $this->redirect('/'.$sPrefix.'/customer/login?r=invoice');
        }
        parent::__construct($aGet, $aPost);
    }

    function runShop()
    {

        $aArguments = [
            'order_id' => $this->get('sale_order_id', null, true, 'numeric'),
            'secret_key' => Config::getSecretKey(),
            '_do' => 'NoLoginPrintInvoice',
            'format' => 'pdf'
        ];



        $sUrl = Cfg::get('PROTOCOL') . '://admin.'.$_SERVER['HTTP_HOST'].'/order/invoice/invoice';


        // https://admin.vangoolstoffenonline.nl/order/invoice/invoice?order_id=574527&secret_key=12312387123798b21eifweifcvlkjwqn&_do=NoLoginPrintInvoice&format=pdf

        $sQueryString = http_build_query($aArguments);
        $oCurl = curl_init();
        curl_setopt($oCurl, CURLOPT_URL, $sUrl.'?'.$sQueryString);
        curl_setopt($oCurl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($oCurl, CURLOPT_RETURNTRANSFER, true);
        $sResponse = curl_exec($oCurl);

        header('Content-type: application/pdf');
        echo $sResponse;
        exit();
    }
}
