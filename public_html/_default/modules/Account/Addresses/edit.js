function toggleStateBlock()
{
    sSelectedItemHtml = $('#fld_country option:selected').html();

    sStateBlockVisibility = (sSelectedItemHtml == 'United States of America') ? 'block' : 'none';

    $('#state_block').css('display', sStateBlockVisibility);
}

toggleStateBlock();

$('#fld_country').change(function(e){
    toggleStateBlock();
});