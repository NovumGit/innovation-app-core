<?php
namespace _Default\Account\Addresses;

use _Default\Account\GeneralAccountController;
use Core\Customer;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Model\Crm\Base\CustomerAddressQuery;
use Model\Crm\CustomerAddress;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class Confirm_deleteController extends GeneralAccountController  {

    function __construct($aGet, $aPost)
    {
        if(!Customer::isSignedIn())
        {
            $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
            DeferredAction::register('addresses', $this->getRequestUri());
            $this->redirect('/'.$sPrefix.'/customer/login?r=addresses');
        }
        parent::__construct($aGet, $aPost);
    }
    function getQueryObject():ModelCriteria
    {
        new ModelCriteria();
    }
    function doEraseAddress()
    {
        $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
        $iAddressId =  $this->get('address_id');
        $oCustomer = Customer::getMember();
        CustomerAddressQuery::create()->filterByCustomerId($oCustomer->getId())->findOneById($iAddressId)->delete();
        StatusMessage::info(Translate::fromCode("The address is deleted."));
        $this->redirect('/'.$sPrefix.'/account/addresses/overview');
    }
    function runShop()
    {
        $iAddressId =  $this->get('address_id');
        $oCustomer = Customer::getMember();
        $oCustomerAddress = CustomerAddressQuery::create()->filterByCustomerId($oCustomer->getId())->findOneById($iAddressId);
        $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
        DeferredAction::register('addresses', $this->getRequestUri());

        if(!$oCustomerAddress instanceof CustomerAddress)
        {
            StatusMessage::warning(Translate::fromCode("I could not find that address, is it already deleted?"));
            $this->redirect('/'.$sPrefix.'/account/addresses/overview');
        }


        $aViewData = [
            'address' => $oCustomerAddress,
            'left_menu' => $this->getLeftMenu()
        ];

        $aOut['content'] = $this->parse('Account/Addresses/confirm_delete.twig', $aViewData);
        $aOut['title'] =  Translate::fromCode("My Account | My Address");
        return $aOut;
    }
}
