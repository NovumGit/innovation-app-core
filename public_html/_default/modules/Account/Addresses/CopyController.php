<?php
namespace _Default\Account\Addresses;

use _Default\Account\GeneralAccountController;
use Core\Customer;
use Core\DeferredAction;
use Core\StatusMessage;
use Exception\LogicException;
use Model\Crm\CustomerAddress;
use Model\Crm\CustomerAddressQuery;
use Model\Crm\CustomerAddressTypeQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class CopyController extends GeneralAccountController {

    function getQueryObject():ModelCriteria
    {
        return new ModelCriteria();
    }

    function __construct($aGet, $aPost)
    {
        if(!Customer::isSignedIn())
        {
            $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
            DeferredAction::register('addresses', $this->getRequestUri());
            $this->redirect('/'.$sPrefix.'/customer/login?r=addresses');
        }
        parent::__construct($aGet, $aPost);
    }

    function runShop()
    {
        $iAddressId = $this->get('address_id');
        $sTo = $this->get('to');

        $oCustomer = Customer::getMember();
        $oCustomerAddress = CustomerAddressQuery::create()
                                ->filterByCustomerId($oCustomer->getId())
                                ->filterById($iAddressId)
                                ->findOne();

        if($oCustomerAddress instanceof CustomerAddress)
        {
            $oCustomerAddressType = CustomerAddressTypeQuery::create()->findOneByCode($sTo);
            $oNewCustomerAddress = $oCustomerAddress->copy();
            $oNewCustomerAddress->setCustomerAddressTypeId($oCustomerAddressType->getId());
            $oNewCustomerAddress->setIsDefault(false);
            $oNewCustomerAddress->save();
            StatusMessage::info("Address copied.");
        }
        else
        {
            StatusMessage::warning("Could not find the specified address, is it yours?");
        }
        $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
        $this->redirect('/'.$sPrefix.'/account/addresses/overview');
        throw new LogicException('Nothing to run here.');
    }
}
