<?php
namespace _Default\Account\Addresses;

use _Default\Account\GeneralAccountController;
use Core\Customer;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Model\Crm\Base\CustomerAddressQuery;
use Model\Crm\CustomerAddress;
use Model\Crm\CustomerAddressTypeQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class OverviewController extends GeneralAccountController {

    function __construct($aGet, $aPost)
    {
        if(!Customer::isSignedIn())
        {
            $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
            DeferredAction::register('addresses', $this->getRequestUri());
            $this->redirect('/'.$sPrefix.'/customer/login?r=addresses');
        }
        parent::__construct($aGet, $aPost);
    }
    private function setDefaultAddress($iAddressId, $sType)
    {
        $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
        $oCustomerAddressType = CustomerAddressTypeQuery::create()->findOneByCode($sType);
        $oCustomer = Customer::getMember();
        CustomerAddressQuery::create()
            ->filterByCustomerId($oCustomer->getId())
            ->filterByCustomerAddressTypeId($oCustomerAddressType->getId())
            ->update(['IsDefault' => false]);

        $oCustomerAddress = CustomerAddressQuery::create()
            ->filterByCustomerId($oCustomer->getId())
            ->findOneById($iAddressId);

        if($oCustomerAddress instanceof CustomerAddress)
        {
            $oCustomerAddress->setIsDefault(true);
            $oCustomerAddress->setCustomerAddressTypeId($oCustomerAddressType->getId());
            $oCustomerAddress->save();
            StatusMessage::info(Translate::fromCode("This address is now set to be the default."));
        }
        else
        {
            StatusMessage::warning(Translate::fromCode("Could not update customer address, is this your address?"));
        }

        $this->redirect('/'.$sPrefix.'/account/addresses/overview');
        exit();
    }
    function doSetGeneral()
    {
        $iAddressId = $this->get('address_id', true, null, 'numeric');
        $this->setDefaultAddress($iAddressId, 'general');
    }
    function doSetDefaultDelivery()
    {
        $iAddressId = $this->get('address_id', true, null, 'numeric');
        $this->setDefaultAddress($iAddressId, 'delivery');
    }
    function doSetDefaultInvoice()
    {
        $iAddressId = $this->get('address_id', true, null, 'numeric');
        $this->setDefaultAddress($iAddressId, 'invoice');
    }
    function getQueryObject():ModelCriteria
    {
        $oCustomer = Customer::getMember();
        $oCustomerAddressQuery = CustomerAddressQuery::create()->filterByCustomerId($oCustomer->getId());
        return $oCustomerAddressQuery;
    }

    function runShop()
    {
        $oCustomerAddressQuery = $this->getQueryObject();
        $oDefaultGeneralCustomerAddress = null;
        if($oCustomerAddressQuery instanceof CustomerAddressQuery)
        {
            $oCustomerAddressType = CustomerAddressTypeQuery::create()->findOneByCode('general');
            $oDefaultGeneralCustomerAddressQuery = clone $oCustomerAddressQuery;
            $oDefaultGeneralCustomerAddressQuery->filterByIsDefault(true);
            $oDefaultGeneralCustomerAddressQuery->filterByCustomerAddressTypeId($oCustomerAddressType->getId());
            $oDefaultGeneralCustomerAddress = $oDefaultGeneralCustomerAddressQuery->findOne();
            if($oDefaultGeneralCustomerAddress instanceof CustomerAddress)
            {
                $oCustomerAddressQuery->filterById($oDefaultGeneralCustomerAddress->getId(), Criteria::NOT_EQUAL);
            }
        }
        $oCustomerAddressQuery->orderByIsDefault(Criteria::DESC);

        $aViewData = [
            'other_addresses' => $oCustomerAddressQuery->find(),
            'default_general_customer_address' => $oDefaultGeneralCustomerAddress,
            'left_menu' => $this->getLeftMenu()
        ];

        $aOut['content'] = $this->parse('Account/Addresses/overview.twig', $aViewData);
        $aOut['title'] =  Translate::fromCode("My Account | My Address");
        return $aOut;
    }
}
