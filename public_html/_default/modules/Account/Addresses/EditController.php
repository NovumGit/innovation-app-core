<?php
namespace _Default\Account\Addresses;

use _Default\Account\GeneralAccountController;
use Core\Customer;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Crud\CustomerAddress\CrudCustomerAddressManager;
use Model\Crm\Base\CustomerAddressQuery;
use Model\Crm\CustomerAddress;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\UsaStateQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class EditController extends GeneralAccountController {

    function getQueryObject():ModelCriteria
    {
        return new ModelCriteria();
    }

    function __construct($aGet, $aPost)
    {
        if(!Customer::isSignedIn())
        {
            $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
            DeferredAction::register('addresses', $this->getRequestUri());
            $this->redirect('/'.$sPrefix.'/customer/login?r=addresses');
        }
        parent::__construct($aGet, $aPost);
    }
    function doStoreAddress()
    {
        $aAddress = $this->post('address');
        $oCrudCustomerAddressManager = new CrudCustomerAddressManager();
        $oCrudCustomerAddressManager->save($aAddress);

        StatusMessage::success(Translate::fromCode("Wijzigingen opgeslagen"));
        $this->redirect($this->getRequestUri());
    }
    function runShop()
    {
        $oCustomer = Customer::getMember();

        $aCountries = CountryQuery::create()->orderByName()->find();
        $aStates = UsaStateQuery::create()->orderByName()->find();

        $iAddresssId = $this->get('address_id');
        $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
        $oCustomerAddress = CustomerAddressQuery::create()
                                ->filterByCustomerId($oCustomer->getId())
                                ->filterById($iAddresssId)
                                ->findOne();

        if(!$oCustomerAddress instanceof CustomerAddress)
        {
            StatusMessage::info(Translate::fromCode("Could not find this customer address."));
            $this->redirect('/'.$sPrefix.'/account/addresses/overview');
        }

        $aViewData = [
            'countries' => $aCountries,
            'states' => $aStates,
            'address' => $oCustomerAddress,
            'left_menu' => $this->getLeftMenu()
        ];

        $aOut['content'] = $this->parse('Account/Addresses/edit.twig', $aViewData);
        $aOut['title'] =  Translate::fromCode("My Account | My Address");
        return $aOut;
    }
}
