<?php
namespace _Default\Account;

use Model\Sale\Base\SaleOrder;

class OrderStatusHelper
{
    public static function addOrderStatus($aSaleOrders)
    {

        if($aSaleOrders)
        {
            foreach($aSaleOrders as $oSaleOrder)
            {
                if(!$oSaleOrder instanceof SaleOrder)
                {
                    throw new LogicException("Expected an instance of SaleOrder here...");
                }
                $sStatus = 'Nieuw';

                if($oSaleOrder->getWorkInProgress() == false && $oSaleOrder->getItemDeleted() == false && $oSaleOrder->isFullyPaid() == false)
                {
                    $sStatus = 'Wacht op betaling';
                }
                else if(
                    $oSaleOrder->getIsFullyPaid() == true &&
                    $oSaleOrder->getIsFullyShipped() == false &&
                    $oSaleOrder->getItemDeleted() == false &&
                    $oSaleOrder->getIsReadyForShipping() == false) {
                    $sStatus = 'Wordt verzameld';
                }
                else if(
                    $oSaleOrder->getIsFullyPaid() == true &&
                    $oSaleOrder->getIsFullyShipped() == false &&
                    $oSaleOrder->getItemDeleted() == false &&
                    $oSaleOrder->getIsReadyForShipping() == true) {
                    $sStatus = 'Wacht op koerier';
                }
                else if(
                    $oSaleOrder->getIsFullyPaid() == true &&
                    $oSaleOrder->getItemDeleted() == false &&
                    $oSaleOrder->getIsReadyForShipping() == true &&
                    $oSaleOrder->getSaleOrderStatus()->getCode() == 'on_hold') {
                    $sStatus = 'Nalevering';
                }
                else if(
                    $oSaleOrder->getItemDeleted() == false &&
                    $oSaleOrder->getIsFullyShipped() == true &&
                    $oSaleOrder->getIsFullyPaid() == true
                )
                {
                    $sStatus = 'Verzonden';
                }

                $oSaleOrder->status_label = $sStatus;
            }
        }
        return $aSaleOrders;

    }
}