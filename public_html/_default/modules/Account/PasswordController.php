<?php
namespace _Default\Account;

use _Default\_DefaultController;
use Core\Customer;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Model\Crm\Base\CustomerQuery;

class PasswordController extends _DefaultController {



    function __construct($aGet, $aPost)
    {
        if(!Customer::isSignedIn())
        {
            $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();

            DeferredAction::register('password', $this->getRequestUri());
            $this->redirect('/'.$sPrefix.'/customer/login?r=password');
        }
        parent::__construct($aGet, $aPost);
    }


    function doResetPassword()
    {

        $bFormIsValid = true;
        $oLanguage = $this->getCurrentLanguage();
        $oLanguage->getShopUrlPrefix();

        $aData = $this->post('password');
        $oldPassword = $aData['old'];
        $newPassword = $aData['new'];

        $oCustomer = CustomerQuery::create()->findOneById(Customer::getMember()->getId());

        $sGeneratedEncryptedPass = Customer::makeEncryptedPass($oldPassword, $oCustomer->getSalt());
        if ($sGeneratedEncryptedPass != $oCustomer->getPassword()) {

            StatusMessage::warning(Translate::fromCode("Your password is incorrect."));
            $bFormIsValid = false;

        }else{

            $sGeneratedEncryptedPass = Customer::makeEncryptedPass($newPassword, $oCustomer->getSalt());
            $oCustomer->setPassword($sGeneratedEncryptedPass);
            //$oCustomer->setSalt();
            $oCustomer->save();

            //Customer::sendWelcomeMail($oCustomer, $oLanguage->getId());
            StatusMessage::success(Translate::fromCode("Password updated successfully!"));
        }



    }


    function runShop()
    {
        $aViewData = [];

        $aOut['content'] = $this->parse('Account/password.twig', $aViewData);
        $aOut['title'] =  Translate::fromCode("My Account | Password Change");
        return $aOut;

    }
}
