<?php
namespace _Default\Account;

use Core\Customer as CoreCustomer;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Crud\Customer\CrudCustomerManager;
use Model\Crm\Base\CustomerQuery;
use Model\Crm\Customer;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class EditController extends GeneralAccountController   {

    function getQueryObject():ModelCriteria
    {
        return new ModelCriteria();
    }

    function __construct($aGet, $aPost)
    {
        if(!CoreCustomer::isSignedIn())
        {
            $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
            DeferredAction::register('account_edit', $this->getRequestUri());
            $this->redirect('/'.$sPrefix.'/customer/login?r=account_edit');
            StatusMessage::warning(Translate::fromCode("To view that page you have to be signed in."));
        }
        parent::__construct($aGet, $aPost);
    }

    function doStore()
    {
        $aData = $this->post('edit');
        $aData['id'] = CoreCustomer::getMember()->getId();

        $oCustomer = CustomerQuery::create()->findOneById($aData['id']);

        if(!$oCustomer instanceof Customer)
        {
            $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
            $this->redirect('/'.$sPrefix.'/customer/login?r=account_edit');
        }
        (new CrudCustomerManager())->save($aData);
        /*
        $oCustomer->save();

        var_dump($aData);
        if(isset($aData['customer_company_company_name']) && $aData['customer_company_company_name'])
        {
            $oCustomerCompany = $oCustomer->getCompany();
            if(!$oCustomerCompany instanceof CustomerCompany)
            {
                $oCustomerCompany = new CustomerCompany();
                $oCustomerCompany->setCustomerId($oCustomer->getId());
            }
            $oCustomerCompany->setCompanyName($aData['customer_company_company_name']);
            $oCustomerCompany->save();

        }
        */
        /*
        if($aData)
*/
    }

    function runShop()
    {
        $oCustomer = CoreCustomer::getMember();
        $aViewData = [
            'current_customer' => $oCustomer,
            'left_menu' => $this->getLeftMenu()
        ];

        $aOut['content'] = $this->parse('Account/edit.twig', $aViewData);
        $aOut['title'] =  Translate::fromCode("My Account | Edit Profile");
        return $aOut;
    }
}
