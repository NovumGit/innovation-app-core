<?php
namespace _Default\Account;

use Core\Customer;
use Core\DeferredAction;
use Core\Translate;
use Model\Sale\SaleOrderQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class OrdersController extends GeneralAccountController  {

    function __construct($aGet, $aPost)
    {
        if(!Customer::isSignedIn())
        {
            $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
            DeferredAction::register('orders', $this->getRequestUri());
            $this->redirect('/'.$sPrefix.'/customer/login?r=orders');
        }
        parent::__construct($aGet, $aPost);
    }

    function getQueryObject():ModelCriteria
    {
        $oCurrentCustomer = Customer::getMember();
        $oSaleOrderQuery = SaleOrderQuery::create();
        $aSaleOrders = $oSaleOrderQuery->filterByCustomerId($oCurrentCustomer->getId());
        $oSaleOrderQuery->orderByCreatedOn(Criteria::DESC);
        return $aSaleOrders;
    }

    function runShop()
    {
        $aSaleOrders = $this->getQueryObject();
        $aSaleOrders = OrderStatusHelper::addOrderStatus($aSaleOrders->find());

        $aViewData = [
            'orders'=> $aSaleOrders,
            'left_menu' => $this->getLeftMenu()
        ];

        DeferredAction::register('order_overview', $this->getRequestUri());

        $aOut['content'] = $this->parse('Account/orders.twig', $aViewData);
        $aOut['title'] =  Translate::fromCode("My Account | Orders");
        return $aOut;

    }
}
