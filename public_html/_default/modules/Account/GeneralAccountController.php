<?php
namespace _Default\Account;

use _Default\_DefaultController;
use _Default\Account\Addresses\OverviewController;
use Propel\Runtime\ActiveQuery\ModelCriteria;

abstract class GeneralAccountController extends _DefaultController {

    abstract function getQueryObject():ModelCriteria;

    function getLeftMenu()
    {
        $aGet = $_GET;
        $aPost = $_POST;
        $aLeftMenuVars = [
            'order_count' => (new OrdersController($aGet, $aPost))->getQueryObject()->count(),
            'wishlist_count' => (new WishlistController($aGet, $aPost))->getQueryObject()->count(),
            'invoice_count' => (new InvoicesController($aGet, $aPost))->getQueryObject()->count(),
            'address_count' => (new OverviewController($aGet, $aPost))->getQueryObject()->count()
        ];


        return $this->parse('Account/left.twig', $aLeftMenuVars);

    }
}
