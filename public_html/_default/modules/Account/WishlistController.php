<?php
namespace _Default\Account;

use Core\Customer;
use Core\DeferredAction;
use Core\Translate;
use Model\Category\CustomerWishlistQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class WishlistController extends GeneralAccountController  {

    function getQueryObject():ModelCriteria
    {
        $oCustomer = Customer::getMember();
        $oCustomerWishlistQuery = CustomerWishlistQuery::create();
        $oCustomerWishlistQuery->orderBySort(Criteria::DESC);
        $oCustomerWishlistQuery->filterByCustomerId($oCustomer->getId());
        return $oCustomerWishlistQuery;
    }

    function __construct($aGet, $aPost)
    {
        if(!Customer::isSignedIn())
        {
            $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
            DeferredAction::register('wishlist', $this->getRequestUri());
            $this->redirect('/'.$sPrefix.'/customer/login?r=wishlist');
        }
        parent::__construct($aGet, $aPost);
    }

    function runShop()
    {
        $aCustomerWishlist = $this->getQueryObject();

        DeferredAction::register('wishlist', $this->getRequestUri());

        $aViewData = [
            'wislist_products' => $aCustomerWishlist
        ];

        $aOut['content'] = $this->parse('Account/wishlist.twig', $aViewData);
        $aOut['title'] =  Translate::fromCode("My Account | Wishlist Product");
        return $aOut;

    }
}