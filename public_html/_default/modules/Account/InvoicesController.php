<?php
namespace _Default\Account;

use Core\Customer;
use Core\DeferredAction;
use Core\Translate;
use Model\Sale\Base\SaleOrderQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;

class InvoicesController extends GeneralAccountController  {


    function getQueryObject():ModelCriteria
    {
        $oCustomer = Customer::getMember();
        $oSaleOrderQuery = SaleOrderQuery::create();
        $oSaleOrderQuery->filterByCustomerId($oCustomer->getId());
        $oSaleOrderQuery->filterByWorkInProgress(false);
        $oSaleOrderQuery->filterByIsFullyPaid(true);
        return $oSaleOrderQuery;
    }

    function __construct($aGet, $aPost)
    {
        if(!Customer::isSignedIn())
        {
            $sPrefix = $this->getCurrentLanguage()->getShopUrlPrefix();
            DeferredAction::register('invoices', $this->getRequestUri());
            $this->redirect('/'.$sPrefix.'/customer/login?r=invoices');
        }
        parent::__construct($aGet, $aPost);
    }

    function runShop()
    {

        $oSaleOrders = $this->getQueryObject();

        $aOrderObjects = $oSaleOrders->find();
        $aOrderObjects = OrderStatusHelper::addOrderStatus($aOrderObjects);


        $aViewData = [
            'orders'=> $aOrderObjects,
            'left_menu' => $this->getLeftMenu()
        ];

        $aOut['content'] = $this->parse('Account/invoices.twig', $aViewData);
        $aOut['title'] =  Translate::fromCode("My Account | Invoices");
        return $aOut;

    }
}
