<?php
namespace _Default\Store_locator;

use _Default\_DefaultController;
use Core\Translate;
use Model\Reseller\ResellerQuery;

class ShopsController extends _DefaultController {

    function runShop()
    {
        $aResellers = ResellerQuery::create()->find();


        if(!$aResellers->isEmpty())
        {
            foreach($aResellers as $oReseller)
            {
                if(!$oReseller->getLatLongChecked() && $oReseller->getCountry() && $oReseller->getCity() && $oReseller->getAddressL1())
                {
                    $sAddress = urlencode($oReseller->getAddressL1().','.$oReseller->getAddressL2().', '.$oReseller->getCity().', '.$oReseller->getCountry()->getName());
                    $sUrl = 'https://maps.googleapis.com/maps/api/geocode/json?address='.$sAddress.'&key=AIzaSyDMD3RZ5sKH1umfXcDVBniPjd2cLQPonkI';

                    $ch = curl_init();
                    $iTimeout = 30;
                    curl_setopt($ch,CURLOPT_URL,$sUrl);
                    curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, $iTimeout);

                    $sData = curl_exec($ch);
                    curl_close($ch);

                    if($sData)
                    {
                        $aData = json_decode($sData, true);
                    }

                    if(isset($aData['results']) && isset($aData['results'][0]) && isset($aData['results'][0]['geometry']))
                    {
                        $oReseller->setLatitude($aData['results'][0]['geometry']['location']['lat']);
                        $oReseller->setLongtitude($aData['results'][0]['geometry']['location']['lng']);

                    }
                    $oReseller->setLatLongChecked(true);
                    $oReseller->save();
                }
            }
        }

        $aViewData = [
            'resellers' => ResellerQuery::create()->find()
        ];




        $aResult['content'] = $this->parse('Store_locator/shops.twig', $aViewData);
        $aResult['title'] = Translate::fromCode('Store locator');
        return $aResult;
    }
}
