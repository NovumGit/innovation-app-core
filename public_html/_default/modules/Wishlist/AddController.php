<?php
namespace _Default\Wishlist;

use _Default\_DefaultController;
use Core\Customer;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Model\Category\CustomerWishlistQuery;

class AddController extends _DefaultController {

    function runShop()
    {
        $oLanguage = $this->getCurrentLanguage();
        $iProductId = $this->get('product_id');
        if(Customer::isSignedIn())
        {
            $oCustomer = Customer::getMember();
            CustomerWishlistQuery::addItem($iProductId, $oCustomer->getId());
            StatusMessage::success(Translate::fromCode("The product has been added to your wishlist."));

            $sReturnUrl = DeferredAction::get('after_add_wishlist');

            $this->redirect($sReturnUrl);
        }
        else
        {
            StatusMessage::warning(Translate::fromCode("To add products to your wishlist you have to be registered and signed in."));
            DeferredAction::register('add_wishlist', $this->getRequestUri());
            $this->redirect('/'.$oLanguage->getShopUrlPrefix().'/customer/login?r=add_wishlist');
        }
        //exit('yay');
    }
}