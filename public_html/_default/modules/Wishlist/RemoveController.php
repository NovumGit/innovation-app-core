<?php
namespace _Default\Wishlist;

use _Default\_DefaultController;
use Core\Customer;
use Core\DeferredAction;
use Core\StatusMessage;
use Core\Translate;
use Model\Category\CustomerWishlistQuery;

class RemoveController extends _DefaultController {

    function runShop()
    {
        $iProductId = $this->get('product_id');
        if(Customer::isSignedIn())
        {
            $oCustomer = Customer::getMember();

            CustomerWishlistQuery::removeItem($iProductId, $oCustomer->getId());
            StatusMessage::success(Translate::fromCode("The product has been removed to your wishlist."));

            $sReturnUrl = DeferredAction::get('wishlist');

            $this->redirect($sReturnUrl);
        }
        else
        {
            $oLanguage = $this->getCurrentLanguage();
            StatusMessage::warning(Translate::fromCode("To remove products from your wishlist you have to be signed in."));
            DeferredAction::register('remove_wishlist', $this->getRequestUri());
            $this->redirect('/'.$oLanguage->getShopUrlPrefix().'/customer/login?r=remove_wishlist');
        }
    }
}