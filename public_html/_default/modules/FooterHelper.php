<?php
namespace _Default;

use Exception\LogicException;
use Model\Cms\Site;
use Model\Cms\SiteFooterBlock;
use Model\Cms\SiteFooterBlockQuery;
use Model\Cms\SiteFooterBlockTranslationQuery;
use Model\Cms\SiteFooterBlockTranslationSitePageQuery;
use Model\Setting\MasterTable\Language;

class FooterHelper
{
    static function getFooter(Site $oSite, Language $oCurrentLanguage)
    {
        $iNumFootBlock = $oSite->getNumFooterBlocks();

        $oSiteFooterBlock = SiteFooterBlockQuery::create();
	    $oSiteFooterBlock->setQueryKey('FooterHelper-getFooter');
        $aSiteFooterBlocksObjects = $oSiteFooterBlock
            ->filterBySiteId($oSite->getId())
            ->orderBySorting()
            ->limit($iNumFootBlock ? $iNumFootBlock : 4)->find();

        if($aSiteFooterBlocksObjects->isEmpty())
        {
            $aFooter = null;
        }
        else
        {
            $aFooter = [];
            foreach($aSiteFooterBlocksObjects as $oSiteFooterBlock)
            {
                if(!$oSiteFooterBlock instanceof SiteFooterBlock)
                {
                    throw new LogicException("Expected an instancen of SiteFooterBlock");
                }

	            $oSiteFooterBlockTranslationQuery = SiteFooterBlockTranslationQuery::create();
	            $oSiteFooterBlockTranslationQuery->setQueryKey('SiteFooterBlockTranslationQuery-footer');

	            $oSiteFooterBlockTranslationQuery->filterByLanguageId($oCurrentLanguage->getId());
	            $oSiteFooterBlockTranslationQuery->filterBySiteFooterBlockId($oSiteFooterBlock->getId());
                $oSiteFooterBlockTranslation = $oSiteFooterBlockTranslationQuery->findOne();

                $aSitePages = [];
                if($oSiteFooterBlock->getType() == 'cms')
                {
                    $oSiteFooterBlockTranslationSitePageQuery = SiteFooterBlockTranslationSitePageQuery::create();
	               /// $oSiteFooterBlockTranslationSitePageQuery->setQueryKey('SiteFooterBlockTranslationSitePageQuery-footer');

                    $aSiteFooterBlockTranslationSitePage = $oSiteFooterBlockTranslationSitePageQuery->findBySiteFooterBlockTranslationId($oSiteFooterBlockTranslation->getId());

                    foreach($aSiteFooterBlockTranslationSitePage as $oSiteFooterBlockTranslationSitePage)
                    {
                        $aSitePages[] = $oSiteFooterBlockTranslationSitePage->getSitePage();
                    }
                }
                $aFooter[] = [
                    'type' => $oSiteFooterBlock->getType(),
                    'width' => $oSiteFooterBlock->getWidth(),
                    'title' => $oSiteFooterBlockTranslation->getTitle(),
                    'html_contents' => $oSiteFooterBlockTranslation->getHtmlContents(),
                    'site_pages' => $aSitePages
                ];
            }
        }
        return $aFooter;
    }
}
