<?php
namespace _Default\Mail;

use _Default\_DefaultController;
use _Default\AbstractMenuHelper;
use Core\InlineTemplate;
use Model\Crm\NewsletterSubscriberQuery;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;
use Model\System\SystemRegistryQuery;

class NewsletterconfirmController extends _DefaultController  {


    function runShop()
    {
        $oLanguage = $this->getCurrentLanguage();
        $iNewsletterSubscriberId = $this->get('newsletter_subscriber_id', null, true, 'numeric');
        $iNotificationTypeId = $this->get('notification_type_id', null, true, 'numeric');
        $oSale_order_notification = Sale_order_notification_typeQuery::create()->findOneById($iNotificationTypeId);

        $oNewsletterSubscriber = NewsletterSubscriberQuery::create()->findOneById($iNewsletterSubscriberId);

        $sContents = SystemRegistryQuery::getVal($oSale_order_notification->getCode().'_contents_'.$oLanguage->getId());
        $sSender = SystemRegistryQuery::getVal($oSale_order_notification->getCode().'_sender_'.$oLanguage->getId());
        $sSubject = SystemRegistryQuery::getVal($oSale_order_notification->getCode().'_subject_'.$oLanguage->getId());
        $aViewData['NewsletterSubscriber'] = $oNewsletterSubscriber;

        $sSubject = InlineTemplate::parse($sSubject, $aViewData);
        $sContents = InlineTemplate::parse($sContents, $aViewData);
        $aSiteSettings = getSiteSettings();

        $sMenuHelperClass = '\\'.$aSiteSettings['namespace'].'\\MenuHelper';

        $aViewData['categories'] = [];
        if(class_exists($sMenuHelperClass))
        {
            $oMenuHelper = new $sMenuHelperClass;
            if($oMenuHelper instanceof AbstractMenuHelper)
            {
                $aViewData['categories'] = $oMenuHelper->getTranslatedMainMenu($oLanguage->getId());
            }
        }


        $aViewData['sender'] = InlineTemplate::parse($sSender, $aViewData);
        $aViewData['to'] = $oNewsletterSubscriber->getEmail();
        $aViewData['subject'] = $sSubject;
        $aViewData['contents'] = nl2br($sContents);

        $aViewData['copyright'] = $aSiteSettings['copyright'];


        $aViewData['site_url'] = $aSiteSettings['protocol'].'://'.$aSiteSettings['domain'];
        $aViewData['sale_order_notification'] = $oSale_order_notification;
        $sFullTemplate = $this->parse('Mail/main.twig', $aViewData);

        exit($sFullTemplate);
    }
}

