<?php
namespace _Default\Mail;

use Core\InlineTemplate;
use Core\MainController;
use Model\ContactMessageQuery;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;
use Model\System\SystemRegistryQuery;

class AutoreplyController extends MainController
{
    function run()
    {

        $iContactMessageId = $this->get('contact_message_id');
        $oContactMessage = ContactMessageQuery::create()->findOneById($iContactMessageId);
        $iLanguageId = $oContactMessage->getLanguageId();

        $iNotificationTypeId = $this->get('notification_type_id', null, true, 'numeric');

        $oSale_order_notification = Sale_order_notification_typeQuery::create()->findOneById($iNotificationTypeId);

        $sContents = SystemRegistryQuery::getVal($oSale_order_notification->getCode().'_contents_'.$iLanguageId);
        $sSender = SystemRegistryQuery::getVal($oSale_order_notification->getCode().'_sender_'.$iLanguageId);
        $sSubject = SystemRegistryQuery::getVal($oSale_order_notification->getCode().'_subject_'.$iLanguageId);

        $aViewData['ContactMessage'] = $oContactMessage;
        $aViewData['sender'] = $sSender;

        $sSubject = InlineTemplate::parse($sSubject, $aViewData);
        $sContents = InlineTemplate::parse($sContents, $aViewData);

        $aViewData['subject'] = $sSubject;
        $aViewData['contents'] = $sContents;

        $aSiteSettings = getSiteSettings();

        $aViewData['copyright'] = $aSiteSettings['copyright'];

        $sDomain = $aSiteSettings['domain'];

        // live.domeinnaam admin.live.domeinnaam
        if(strpos($_SERVER['HTTP_HOST'],'live.') === 0 || strpos($_SERVER['HTTP_HOST'],'live.') > 0)
        {
            $sDomain = $_SERVER['HTTP_HOST'];
        }
        // test.domeinnaam admin.test.domeinnaam
        if(strpos($_SERVER['HTTP_HOST'],'test.') === 0 || strpos($_SERVER['HTTP_HOST'],'test.') > 0)
        {
            $sDomain = $_SERVER['HTTP_HOST'];
        }

        $aViewData['site_url'] = $aSiteSettings['protocol'].'://'.$sDomain;
        $aViewData['sale_order_notification'] = $oSale_order_notification;

        $sFullTemplate = $this->parse('Mail/main.twig', $aViewData);

        exit($sFullTemplate);

    }
}