<?php
namespace _Default\Mail;

use _Default\_DefaultController;
use _Default\AbstractMenuHelper;
use Core\Customer;
use Core\InlineTemplate;
use Model\Company\CompanyQuery;
use Model\Crm\CustomerQuery;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;
use Model\System\SystemRegistryQuery;

class CustomerController extends _DefaultController  {


    function runShop()
    {
        // De taal wordt bepaald door de url van de pagina.
        // Als je de mail dus in een andere taal wilt hebben dan roep je een andere url aan.
        // Anders zou je hier allerlei trucen toe moeten passen.

        $oLanguage = $this->getCurrentLanguage();

        // $sPassword = $this->get('new_pass', null);
        $iCustomerId = $this->get('customer_id', null, true, 'numeric');
        $iNotificationTypeId = $this->get('notification_type_id', null, true, 'numeric');
        $oCustomer = CustomerQuery::create()->findOneById($iCustomerId);
        $oSale_order_notification = Sale_order_notification_typeQuery::create()->findOneById($iNotificationTypeId);

        $sContents = SystemRegistryQuery::getVal($oSale_order_notification->getCode().'_contents_'.$oLanguage->getId());
        $sSender = SystemRegistryQuery::getVal($oSale_order_notification->getCode().'_sender_'.$oLanguage->getId());
        $sSubject = SystemRegistryQuery::getVal($oSale_order_notification->getCode().'_subject_'.$oLanguage->getId());


        if($sContents)
        {
            if(strpos($sContents, 'getVerificationKey') && trim($oCustomer->getVerificationKey()) == '')
            {
                $oCustomer->setVerificationKey(sha1('time'.time()));
                $oCustomer->save();
            }
        }

        $aViewData['Customer'] = $oCustomer;
        $aViewData['Own_company'] = CompanyQuery::getDefaultCompany();

        $sSubject = InlineTemplate::parse($sSubject, $aViewData);
        $sContents = InlineTemplate::parse($sContents, $aViewData);


        if(strpos($sContents, '[[new_pass]]') !== false)
        {
            // generateNewPass
            $sUnencryptedPass = Customer::generateNewPass($oCustomer);
            $sContents = str_replace('[[new_pass]]', $sUnencryptedPass, $sContents);
        }


        $aViewData['sender'] = InlineTemplate::parse($sSender, $aViewData);
        $aViewData['to'] = $oCustomer->getEmail();

        $aSettings = getSiteSettings();
        $sMenuHelperClass = '\\'.$aSettings['namespace'].'\\MenuHelper';

        $aViewData['categories'] = [];
        if(class_exists($sMenuHelperClass))
        {
            $oMenuHelper = new $sMenuHelperClass;
            if($oMenuHelper instanceof AbstractMenuHelper)
            {
                $aViewData['categories'] = $oMenuHelper->getTranslatedMainMenu($oLanguage->getId());
            }
        }

        $aViewData['subject'] = $sSubject;
        $aViewData['contents'] = nl2br($sContents);
        $aSiteSettings = getSiteSettings();

        $aViewData['copyright'] = $aSiteSettings['copyright'];

        $sDomain = $aSiteSettings['domain'];

        // live.domeinnaam admin.live.domeinnaam
        if(strpos($_SERVER['HTTP_HOST'],'live.') === 0 || strpos($_SERVER['HTTP_HOST'],'live.') > 0)
        {
            $sDomain = $_SERVER['HTTP_HOST'];
        }
        // test.domeinnaam admin.test.domeinnaam
        if(strpos($_SERVER['HTTP_HOST'],'test.') === 0 || strpos($_SERVER['HTTP_HOST'],'test.') > 0)
        {
            $sDomain = $_SERVER['HTTP_HOST'];
        }

        $aViewData['site_url'] = $aSiteSettings['protocol'].'://'.$sDomain;
        $aViewData['sale_order_notification'] = $oSale_order_notification;

        /* {{sale_order.getCompanyN}} */
        $sFullTemplate = $this->parse('Mail/main.twig', $aViewData);

        exit($sFullTemplate);
    }
}

