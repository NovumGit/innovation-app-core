<?php
namespace _Default\Mail;

use _Default\_DefaultController;
use _Default\AbstractMenuHelper;
use Core\InlineTemplate;
use Core\User;
use Model\Company\CompanyQuery;
use Model\Crm\CustomerQuery;
use Model\Sale\SaleOrderItemQuery;
use Model\Sale\SaleOrderQuery;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;
use Model\System\SystemRegistryQuery;

class Controller extends _DefaultController  {


    function runShop()
    {
        // De taal wordt bepaald door de url van de pagina.
        // Als je de mail dus in een andere taal wilt hebben dan roep je een andere url aan.
        // Anders zou je hier allerlei trucen toe moeten passen.

        $oLanguage = $this->getCurrentLanguage();

        $iOrderId = $this->get('order_id', null, false, 'numeric');
        $iNotificationTypeId = $this->get('notification_type_id', null, true, 'numeric');
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);
        $oCustomer = CustomerQuery::create()->findOneById($oSaleOrder->getCustomerId());
        $aSaleOrderItem = SaleOrderItemQuery::create()->findBySaleOrderId($oSaleOrder->getId());

        $oSale_order_notification = Sale_order_notification_typeQuery::create()->findOneById($iNotificationTypeId);

        $sContents = SystemRegistryQuery::getVal($oSale_order_notification->getCode().'_contents_'.$oLanguage->getId());
        $sSender = SystemRegistryQuery::getVal($oSale_order_notification->getCode().'_sender_'.$oLanguage->getId());
        $sSubject = SystemRegistryQuery::getVal($oSale_order_notification->getCode().'_subject_'.$oLanguage->getId());

        $oCurrentUser = User::getMember();

        $aViewData['Customer'] = $oCustomer;
        $aViewData['Own_company'] = CompanyQuery::getDefaultCompany();
        $aViewData['User'] = $oCurrentUser;
        $aViewData['Sale_order'] = $oSaleOrder;
        $aViewData['Sale_order_item'] = $aSaleOrderItem;

        $sSubject = InlineTemplate::parse($sSubject, $aViewData);
        $sContents  = InlineTemplate::parse($sContents, $aViewData);

        $aViewData['sender'] = InlineTemplate::parse($sSender, $aViewData);
        $aViewData['to'] = $oSaleOrder->getCustomerEmail();

        $aViewData['is_gmail'] = (strpos($aViewData['to'], 'gmail.com'));


        $aSettings = getSiteSettings();
        $sMenuHelperClass = '\\'.$aSettings['namespace'].'\\MenuHelper';

        $aViewData['categories'] = [];
        if(class_exists($sMenuHelperClass))
        {
            $oMenuHelper = new $sMenuHelperClass;
            if($oMenuHelper instanceof AbstractMenuHelper)
            {
                $aViewData['categories'] = $oMenuHelper->getTranslatedMainMenu($oLanguage->getId());
            }
        }

        $aViewData['sale_order'] = SaleOrderItemQuery::create()->findOneById($iOrderId);
        $aViewData['subject'] = $sSubject;
        $aViewData['contents'] = nl2br($sContents);
        $aSiteSettings = getSiteSettings();

        $aViewData['copyright'] = $aSiteSettings['copyright'];

        $sDomain = $aSiteSettings['domain'];

        // live.domeinnaam admin.live.domeinnaam
        if(strpos($_SERVER['HTTP_HOST'],'live.') === 0 || strpos($_SERVER['HTTP_HOST'],'live.') > 0)
        {
            $sDomain = $_SERVER['HTTP_HOST'];
        }
        // test.domeinnaam admin.test.domeinnaam
        if(strpos($_SERVER['HTTP_HOST'],'test.') === 0 || strpos($_SERVER['HTTP_HOST'],'test.') > 0)
        {
            $sDomain = $_SERVER['HTTP_HOST'];
        }

        $aViewData['site_url'] = $aSiteSettings['protocol'].'://'.$sDomain;
        $aViewData['sale_order_notification'] = $oSale_order_notification;

        /* {{sale_order.getCompanyN}} */
        $sFullTemplate = $this->parse('Mail/main.twig', $aViewData);

        //$sFullTemplate = $this->parse('Mail/incomplete.twig', []);
        // $sFullTemplate = $this->parse('Mail/invoice.twig', []);
        //$sFullTemplate = $this->parse('Mail/order.twig', []);
        //$sFullTemplate = $this->parse('Mail/shipped.twig', []);

        //$sFullTemplate = $this->parse('Mail/tracking.twig', []);
        //$sFullTemplate = $this->parse('Mail/subscribe.twig', []);
        //$sFullTemplate = $this->parse('Mail/unsubscribe.twig', []);
        //$sFullTemplate = $this->parse('Mail/wishlist.twig', []);

        exit($sFullTemplate);
    }
}

