<?php
namespace _Default;

use Model\System\SystemRegistryQuery;

class OpeningHoursHelper{

    static function getOpeningHours()
    {
        $iDayOfWeek = date('N');

        return [
            1 => [
                'is_today' => ($iDayOfWeek == 1),
                'label' => 'Monday',
                'is_open' => SystemRegistryQuery::getVal('is_open_1'),
                'open' => SystemRegistryQuery::getVal('1_open'),
                'close' => SystemRegistryQuery::getVal('1_close'),
            ],
            2 => [
                'is_today' => ($iDayOfWeek == 2),
                'label' => 'Tuesday',
                'is_open' => SystemRegistryQuery::getVal('is_open_2'),
                'open' => SystemRegistryQuery::getVal('2_open'),
                'close' => SystemRegistryQuery::getVal('2_close'),
            ],
            3 => [
                'is_today' => ($iDayOfWeek == 3),
                'label' => 'Wednesday',
                'is_open' => SystemRegistryQuery::getVal('is_open_3'),
                'open' => SystemRegistryQuery::getVal('3_open'),
                'close' => SystemRegistryQuery::getVal('3_close'),
            ],
            4 => [
                'is_today' => ($iDayOfWeek == 4),
                'label' => 'Thursday',
                'is_open' => SystemRegistryQuery::getVal('is_open_4'),
                'open' => SystemRegistryQuery::getVal('4_open'),
                'close' => SystemRegistryQuery::getVal('4_close'),
            ],
            5 => [
                'is_today' => ($iDayOfWeek == 5),
                'label' => 'Friday',
                'is_open' => SystemRegistryQuery::getVal('is_open_5'),
                'open' => SystemRegistryQuery::getVal('5_open'),
                'close' => SystemRegistryQuery::getVal('5_close'),
            ],
            6 => [
                'is_today' => ($iDayOfWeek == 6),
                'label' => 'Saturday',
                'is_open' => SystemRegistryQuery::getVal('is_open_6'),
                'open' => SystemRegistryQuery::getVal('6_open'),
                'close' => SystemRegistryQuery::getVal('6_close'),
            ],
            7 => [
                'is_today' => ($iDayOfWeek == 7),
                'label' => 'Sunday',
                'is_open' => SystemRegistryQuery::getVal('is_open_7'),
                'open' => SystemRegistryQuery::getVal('7_open'),
                'close' => SystemRegistryQuery::getVal('7_close'),
            ],
        ];

    }
}