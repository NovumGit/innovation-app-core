<?php
namespace _Default;

use Core\Cfg;
use Core\Customer;
use Core\DeferredAction;
use Core\Mailer;
use Core\MailMessage;
use Core\StatusMessage;
use Core\Translate;
use Model\Base\PromoProductTranslationQuery;
use Model\Cms\SiteBannerQuery;
use Model\Cms\SiteBlogQuery;
use Model\Cms\SiteQuery;
use Model\Crm\Base\CustomerQuery;
use Model\Crm\Base\NewsletterSubscriberQuery;
use Model\Crm\NewsletterSubscriber;
use Model\PromoProductQuery;
use Model\PromoProductTranslation;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;
use Model\System\SystemRegistryQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class HomeController extends _DefaultController  {

    function doSetCurrency()
    {
        if(!isset($_SESSION['currency']))
        {
            $_SESSION['currency'] = [];
        }
        $_SESSION['currency']['current'] = $this->get('currency');

        $sReturnUrl = DeferredAction::get('previous_url');
        $this->redirect($sReturnUrl);
        exit();
    }
    function doConfirmSubscription()
    {
        $sConfirmHash = $this->get('confirm_hash', null, true);
        $oNewsletterSubscriber = NewsletterSubscriberQuery::create()->findOneByConfirmHash($sConfirmHash);

        if($oNewsletterSubscriber instanceof NewsletterSubscriber)
        {
            $oNewsletterSubscriber->setConfirmHash(null);
            $oNewsletterSubscriber->setConfirmed(true);
            $oNewsletterSubscriber->save();
            StatusMessage::success(Translate::fromCode('Thank you for your subscription to our newsletter!'), 'Thanks!');
        }
        else
        {
            StatusMessage::warning(Translate::fromCode('Could not confirm, did you confirm already before maybe.'), 'Newsletter subscription');
        }
        $this->redirect('/');
    }

    function doSubscribe()
    {

        $bFormIsValid = true;
        $aData = $this->post('message');

        if( $_SESSION['rand_mail'] != $aData["rand_mail"] )
        {
            // The random string did not match, we are not informing the client, let them think the message was send.
            $this->redirect($this->getRequestUri(false));
        }
        $aWarnings = [];
        if (!filter_var($aData['email'], FILTER_VALIDATE_EMAIL))
        {
            $aWarnings[] = Translate::fromCode("Please specify a valid e-mail address.");
            $bFormIsValid = false;
        }
        if (empty($aData['email']))
        {
            $aWarnings[] = Translate::fromCode("Please specify an e-mail address.");
            $bFormIsValid = false;
        }
        $sSubscribed = NewsletterSubscriberQuery::create()->findOneByEmail($aData['email']);
        if (!empty($sSubscribed))
        {
            $aWarnings[] = Translate::fromCode("This e-mail address already subscribed!");
            $bFormIsValid = false;
        }
        if($bFormIsValid)
        {
            $sFirstName = $aData['first_name'];
            $sLastName = $aData['last_name'];
            $sEmailAddress = $aData['email'];
            $oCustomer = CustomerQuery::create()->findOneByEmail($sEmailAddress);
            $oSubscribe = new NewsletterSubscriber();
            if (!$oCustomer)
            {
                $oSubscribe->setLastName($sLastName);
                $oSubscribe->setFirstName($sFirstName);
                $oSubscribe->setEmail($sEmailAddress);
                $oSubscribe->save();
            }
            else
            {
                $oSubscribe->setEmail($sEmailAddress);
                $oSubscribe->setConfirmed(false);
                $sHash = sha1(time()+rand(0,99));
                $oSubscribe->setConfirmHash($sHash);
                $oSubscribe->setCustomerId($oCustomer->getId());
                $oSubscribe->setCompanyName($oCustomer->getCompanyName());
                $oSubscribe->setFirstName($oCustomer->getFirstName());
                $oSubscribe->setLastName($oCustomer->getLastName());
                $oSubscribe->save();
            }
            $oSaleOrderNotificationType = Sale_order_notification_typeQuery::create();
            $oSaleOrderNotification = $oSaleOrderNotificationType->findOneByCode('confirm_subscription');

            $oLanguage  = $this->getCurrentLanguage();
            $sSender = SystemRegistryQuery::getVal('confirm_subscription_sender_'.$oLanguage->getId());
            $sSubject = SystemRegistryQuery::getVal('confirm_subscription_subject_'.$oLanguage->getId());
            $sDomain = Cfg::get('PROTOCOL').'://'.Cfg::get('DOMAIN');

            $sUrl = $sDomain."/".$oLanguage->getShopUrlPrefix().'/mail/newsletterconfirm?newsletter_subscriber_id='.$oSubscribe->getId().'&notification_type_id='.$oSaleOrderNotification->getId();
            $sContents = file_get_contents($sUrl);

            ini_set('display_errors', true);
            error_reporting(E_ALL);

            $oMailMessage = new MailMessage();
            $oMailMessage->setFrom($sSender);
            $oMailMessage->setTo($oSubscribe->getEmail());
            $oMailMessage->setSubject($sSubject);
            $oMailMessage->setBody($sContents);
            Mailer::send($oMailMessage);

            StatusMessage::success(Translate::fromCode("An e-mail has been send to").' '.$oSubscribe->getEmail().' '.Translate::fromCode("please click on the link to confirm the newsletter subscription."), Translate::fromCode('Thank you!'));
            $this->redirect($this->getRequestUri(false));
        }
        else
        {
            StatusMessage::warning(join(PHP_EOL.'<br>- ', $aWarnings));
        }
    }

    function runShop()
    {
        $aSettings = getSiteSettings();
        $oSite = SiteQuery::create()->findOneByDomain($aSettings['live_domain']);
        DeferredAction::register('after_add', $this->getRequestUri());
        $oLanguage = $this->getCurrentLanguage();
        $oSiteBanners = SiteBannerQuery::create()->findBySiteId($this->getSite()->getId());
        $aPromoProducts = PromoProductQuery::create()->findBySiteId($this->getSite()->getId());
        $aPromoTranslations = [];
        if(!$aPromoProducts->isEmpty())
        {
            foreach($aPromoProducts as $oPromoProduct)
            {
                $oPromoProductTranslation = PromoProductTranslationQuery::create()
                    ->filterByPromoProduct($oPromoProduct)
                    ->filterByLanguage($this->getCurrentLanguage())
                    ->findOne();

                if($oPromoProductTranslation instanceof PromoProductTranslation)
                {
                    $aPromoTranslations[] = [
                        'trans' => $oPromoProductTranslation,
                        'product' => $oPromoProductTranslation->getPromoProduct()->getProduct()
                    ];
                }
            }
        }

        $_SESSION['rand_mail'] = md5(rand(0, 9999));
        $aData = $this->post('message');
        if (empty($aData) && Customer::isSignedIn())
        {
            $aData['email'] = Customer::getMember()->getEmail();
        }

        $aSiteSettings = getSiteSettings();

        if(class_exists($aSiteSettings['namespace'].'\\HomeHelper'))
        {
            $sClassName = $aSiteSettings['namespace'].'\\HomeHelper';
            $aBestSellers = $sClassName::getBestSellers($oLanguage);
            $aTopBestSellers = $sClassName::getTopBestSellers();
            $aActions = $sClassName::getActions();
            $aAlltimeToppers = $sClassName::getAlltimeToppers();
        }
        else
        {
            $aBestSellers = HomeHelper::getBestSellers($oLanguage);
            $aTopBestSellers = HomeHelper::getTopBestSellers();
            $aActions = HomeHelper::getActions();
            $aAlltimeToppers = HomeHelper::getAlltimeToppers();
        }



        $aUsps = HomeHelper::getUsps($oSite, $oLanguage);
        $aInSale = HomeHelper::getInSale(10);

        $aBlogPosts = SiteBlogQuery::create()
                            ->filterBySite($oSite)
                            ->filterByLanguage($oLanguage)
                            ->orderByCreatedOn(Criteria::DESC)->limit(3);

        $aViewData = [
            'data' => $aData,
            'rand_mail' => $_SESSION['rand_mail'],
            'site_banners' => $oSiteBanners,
            'actions' => $aActions, /*Deals in femaleseeds*/
            'bestsellers' => $aBestSellers,
            'blog_posts' => $aBlogPosts,
            'top_bestsellers' => $aTopBestSellers,
            'alltime_toppers' => $aAlltimeToppers,
            'in_sale' => $aInSale,
            'usps' => $aUsps,
            'promo_translations' => $aPromoTranslations
        ];

        $aResult['content'] = $this->parse('home.twig', $aViewData);
        $aResult['title'] = Translate::fromCode("Home page title");

        return $aResult;
    }
}
