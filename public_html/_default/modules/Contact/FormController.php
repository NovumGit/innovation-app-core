<?php
namespace _Default\Contact;

use _Default\_DefaultController;
use Core\Customer;
use Core\Mailer;
use Core\MailMessage;
use Core\StatusMessage;
use Core\Translate;
use Exception\LogicException;
use Model\Cms\Site;
use Model\Cms\SiteQuery;
use Model\ContactMessage;

class FormController extends _DefaultController  {

    function doAddMessage()
    {

        $bFormIsValid = true;

        $aData = $this->post('message');
        /*
        if( $_SESSION['rand_mail'] != $aData["rand_mail"] )
        {
            // The random string did not match, we are not informing the client, let them think the message was send.
            $this->redirect($this->getRequestUri(false));
        }
        */
        if (!filter_var($aData['email'], FILTER_VALIDATE_EMAIL)) {
            //echo "This ($aData['email']) email address is consideredvalid.";
            StatusMessage::warning(Translate::fromCode("Please specify a valid e-mail address."));

            $bFormIsValid = false;
        }

        if (empty($aData['message'])) {
            //echo "This ($aData['email']) email address is considered valid.";
            StatusMessage::warning(Translate::fromCode("Your message has no content."));

            $bFormIsValid = false;
        }


        if($bFormIsValid) {
            $oMessage = new ContactMessage();
            if (Customer::isSignedIn()) {
                $oMessage->setCustomerId(Customer::getMember()->getId());
            }
            // This way the user can modify his name and email even when they are signed in.
            // Sometimes resellers will place an order and they have more email addresses and customers
            // After this shop we are also going to use this sourcecode for a wholesale shop for the same customer.
            $oMessage->setName($aData['name']);
            $oMessage->setSubject($aData['subject']);
            $oMessage->setMessage($aData['message']);
            $oMessage->setEmail($aData['email']);
            $oMessage->save();

            $oMailMessage = new MailMessage();
            $oMailMessage->setFrom($aData['email']);
            $sShopUrl = \Core\Config::getBaseUrl();

            $oSite = SiteQuery::create()->findOneByDomain($sShopUrl);

            if(!$oSite instanceof Site)
            {
                throw new LogicException("Something went wrong when sending a contact email, could not find the Site related to $sShopUrl. This indicates a bug, the developer has been informed.");
            }
            $oMailMessage->setTo($oSite->getContactFormToEmail());
            $oMailMessage->setSubject($aData['subject']);
            $oMailMessage->setBody(nl2br($aData['message']));

            Mailer::send($oMailMessage);

            StatusMessage::success(Translate::fromCode("Your message has been send."));
            // So if there are errors the user is not redirected
            $this->redirect($this->getRequestUri(false));
        }
    }

    function runShop()
    {

        $_SESSION['rand_mail'] = md5(rand(0, 9999));
        $aData = $this->post('message');

        if (empty($aData) && Customer::isSignedIn())
        {
            $aData['email'] = Customer::getMember()->getEmail();
            $aData['name'] = Customer::getMember()->getFullName();
        }


        $aViewData =
            [
                'data' => $aData,
                'rand_mail' => $_SESSION['rand_mail']
            ];

        $aResult['content'] = $this->parse('Contact/form.twig', $aViewData);
        $aResult['title'] = Translate::fromCode("Contact");

        return $aResult;
    }
}
