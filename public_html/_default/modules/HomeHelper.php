<?php
namespace _Default;

use Core\QueryMapper;
use Core\Utils;
use Model\Category\Base\CategoryTranslationQuery;
use Model\Category\CategoryQuery;
use Model\Cms\Site;
use Model\Cms\SiteUspQuery;
use Model\CustomerQuery;
use Model\Setting\MasterTable\Language;

class HomeHelper
{

    static function getActions($iLimit = 4)
    {
        $sQuery = "SELECT 
                      p.id product_id
                    FROM 
                      product p,
                      product_image pi
                    WHERE 
                      p.is_offer = 1
                    AND p.in_webshop = 1
                    AND pi.product_id = p.id
                    AND p.derrived_from_id IS NULL
                    GROUP BY p.id
                    LIMIT $iLimit";
        $aProductIds = QueryMapper::fetchArray($sQuery);
        $aids = Utils::flattenArray($aProductIds, 'product_id');
        $aItems = CustomerQuery::create()->filterById($aids)->find();
        if($aItems->isEmpty())
        {
            return null;
        }
        return $aItems;

    }
    static function getAlltimeToppers()
    {
        $sQuery = "SELECT 
                      AVG(pr.rating), 
                      COUNT(pr.rating), 
                      pr.product_id 
                    FROM
                      product_review pr,
                      product p
                    WHERE
                      p.id = pr.product_id
                    AND p.in_webshop = 1
                    GROUP BY
                     product_id 
                    
                    HAVING COUNT(pr.rating) > 4 
                    ORDER BY AVG(pr.rating) DESC
                    LIMIT 2";

        $aProductIds = QueryMapper::fetchArray($sQuery);
        $aids = Utils::flattenArray($aProductIds, 'product_id');
        $aItems = CustomerQuery::create()->filterById($aids)->find();

        if($aItems->isEmpty())
        {
            return null;
        }
        return $aItems;

    }
    static function getUsps(Site $oSite, Language $oLanguage)
    {
        return SiteUspQuery::create()->filterBySite($oSite)->filterByLanguage($oLanguage);
    }
    static function getInSale($iLimit = 2)
    {
        $sQuery = "SELECT 
                      p.id product_id
                    FROM 
                      product p,
                      product_image pi
                    WHERE 
                      p.in_sale = 1
                    AND p.in_webshop = 1
                    AND pi.product_id = p.id
                    AND p.derrived_from_id IS NULL
                    LIMIT $iLimit";

        $aProductIds = QueryMapper::fetchArray($sQuery);
        $aids = Utils::flattenArray($aProductIds, 'product_id');
        $aItems = CustomerQuery::create()->filterById($aids)->find();

        if($aItems->isEmpty())
        {
            return null;
        }
        return $aItems;
    }
    static function getTopBestSellers()
    {

        $sQuery = "SELECT 
                      pp.id product_id                                                    
                    FROM 
                      product pp, 
                      product p,
                      sale_order_item_product soip,
                      sale_order_item soi,
                      sale_order so
                    WHERE
                        p.id = soip.product_id
                    AND p.stock_quantity > 0
                    AND soi.id = soip.sale_order_item_id
                    AND so.id = soi.sale_order_id     
                    AND DATE(so.created_on) BETWEEN CURDATE() - INTERVAL 60 DAY AND CURDATE()
                    AND pp.id = p.derrived_from_id
                    AND p.in_webshop = 1
                    GROUP BY 
                      soip.product_id
                    ORDER BY COUNT(soip.product_id) DESC
                    LIMIT 2";

        $aProductIds = QueryMapper::fetchArray($sQuery);

        if(!empty($aProductIds))
        {
            $aids = Utils::flattenArray($aProductIds, 'product_id');
            $aItems = CustomerQuery::create()->filterById($aids)->find();
            return $aItems;
        }
        return null;

    }
    static function getBestSellers(Language $oLanguage)
    {
        $aCategories = CategoryQuery::create()->findByInBestsellers(true);
        if(!$aCategories->isEmpty())
        {

            $aCategoryBestsellers = [];
            foreach($aCategories as $oCategory)
            {
                /* We find the most sold products and then get the parent product of that, so not the actual variation */
                $sQuery = "SELECT 
                              pp.id product_id                                                    
                        FROM 
                          product pp, -- parent product 
                          product p,
                          sale_order_item_product soip,
                          sale_order_item soi,
                          sale_order so
                        WHERE
                            p.id = soip.product_id
                        AND p.category_id = ".$oCategory->getId()."
                        AND p.stock_quantity > 0
                        AND soi.id = soip.sale_order_item_id
                        AND so.id = soi.sale_order_id     
                        AND DATE(so.created_on) BETWEEN CURDATE() - INTERVAL 30 DAY AND CURDATE()
                        AND pp.id = p.derrived_from_id
                        AND p.in_webshop = 1
                        GROUP BY 
                          soip.product_id
                        ORDER BY COUNT(soip.product_id) DESC
                        LIMIT 10";

                $aProductIds = QueryMapper::fetchArray($sQuery);
                $aProductIds = null;
                if(!empty($aProductIds))
                {
                    $aIds = Utils::flattenArray($aProductIds, 'product_id');
                    $aItems = CustomerQuery::create()->filterById($aIds)->find();

                    $oCategoryTranslation = CategoryTranslationQuery::create()
                                            ->filterByLanguageId($oLanguage->getId())
                                            ->filterByCategoryId($oCategory->getId())
                                            ->findOne();

                    $aCategoryBestsellers[] = [
                        'category_translation' => $oCategoryTranslation,
                        'category' => $oCategory,
                        'has_products' => !$aItems->isEmpty(),
                        'products' => $aItems
                    ];
                }

            }
            return $aCategoryBestsellers;
        }
        return null;
    }
}
