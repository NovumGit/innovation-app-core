<?php
namespace _Default\Search;

use _Default\_DefaultController;
use Core\DeferredAction;
use Core\QueryMapper;
use Core\Translate;
use Exception\LogicException;
use Model\CustomerQuery;
use voku\helper\AntiXSS;

class Controller extends _DefaultController {

    function runShop()
    {
        $sSort = $this->get('sort', 'popularity');
        $aAllowedSortingMethods = ['popularity', 'newness', 'rating'];
        if(!in_array($sSort, $aAllowedSortingMethods))
        {
            throw new LogicException("Only one of these: ".join(',',  $aAllowedSortingMethods)." is allowed here.");
        }
        $sQueryInput = $this->get('query');

        $sOrderBy = '';
        if($sSort == 'popularity')
        {
            $sOrderBy = 'ORDER BY p.popularity';
        }
        else if($sSort == 'rating')
        {
            $sOrderBy = 'ORDER BY p.avg_rating';
        }

        $sQuery = "SELECT 
                      id 
                    FROM 
                      product p 
                    WHERE
                      p.derrived_from_id IS NULL
                    AND p.deleted_by_user_id IS NULL 
                    AND (p.number LIKE ".QueryMapper::quote('%'.$sQueryInput.'%')." OR p.title  LIKE ".QueryMapper::quote('%'.$sQueryInput.'%').")
                    ".$sOrderBy.' LIMIT 96';

        $aResults = QueryMapper::fetchArray($sQuery);

        $aProducts = null;
        if(!empty($aResults))
        {
            $aIds = [];
            foreach($aResults as $aResult)
            {
                $aIds[] = $aResult['id'];
            }
            $oProductQuery = CustomerQuery::create();
            $aProducts = $oProductQuery->findById($aIds);
        }

        $oAntiXss = new AntiXSS();
        $sCleanQuery = $oAntiXss->xss_clean($sQueryInput);

        // $sBannerArea = $this->parse('ProductList/banner_area.twig', $aBannerAreaVars);
        DeferredAction::register('after_add_wishlist', $this->getRequestUri());
        DeferredAction::register('after_add', $this->getRequestUri());
        $aViewData = [
            'sort' => $sSort,
            'query' => $sCleanQuery,
            'view' => $this->get('view', 'tiles'),
            'products' => $aProducts,
            'banner_area' => '',
           /* 'unit_name' => $sTranslatedUnitName*/
        ];
        $aResult['query'] = $sCleanQuery;
        $aResult['content'] = $this->parse('Search/all_products.twig', $aViewData);
        $aResult['title'] =  Translate::fromCode("Search results");
        return $aResult;
    }
}
