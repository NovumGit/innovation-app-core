<?php
namespace _Default\Customer;


use _Default\_DefaultController;
use Core\StatusMessage;
use Core\Translate;

class LogoutController extends _DefaultController
{
    function runShop()
    {
        unset($_SESSION);
        session_destroy();

        $oCurrentLanguage = $this->getCurrentLanguage();
        $sRedirectUrl = "/".$oCurrentLanguage->getShopUrlPrefix().'/customer/login';

        StatusMessage::success(Translate::fromCode("Logout Successfully!"));
        $this->redirect($sRedirectUrl);

    }
}
