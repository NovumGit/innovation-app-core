<?php
namespace _Default\Page;

use _Default\_DefaultController;
use Model\Cms\SitePageQuery;

class Controller extends _DefaultController
{
    function runShop()
    {
        $sUrl = $this->getRequestUri();
        $aParts = explode('/', $sUrl);

        $mSitePageId = $aParts[3];
        if(preg_match('/[0-9]+/', $mSitePageId))
        {
            $iSitePageId = $mSitePageId;
            $oSitePage = SitePageQuery::create()->findOneById($iSitePageId);
        }
        else
        {
            $oLanguage = $this->getCurrentLanguage();

           // var_dump($oLanguage);
            //exit();

            $sSitePageTag = $mSitePageId;

            $oSitePageQuery = SitePageQuery::create();
            $oSitePageQuery->filterByLanguageId($oLanguage->getId());
            $oSitePageQuery->filterByTag($sSitePageTag);

            $oSitePage = $oSitePageQuery->findOne();

            /*
            if(!$oSitePage instanceof SitePage)
            {

       //         $this->redirect('/'.$oLanguage->getShopUrlPrefix().'/error/pagenotfound');
                // Stukje hieronder is te gebruiken om eventueel wat pagina's te genereren.
                $aSettings = getSiteSettings();
                $oSite = SiteQuery::create()->findOneByDomain($aSettings['live_domain']);

                StatusMessage::info("Pagina gegenereerd");
                $oSitePage = new SitePage();
                $oSitePage->setSiteId($oSite->getId());
                $oSitePage->setLanguageId($oLanguage->getId());
                $oSitePage->setTitle($sUrl);
                $oSitePage->setContent("Schrijf deze conten in het cms.");
                $oSitePage->setTag($sSitePageTag);
                $oSitePage->save();
            }
            */
        }


        $aViewData = [
            'site_page' => $oSitePage
        ];

        $aView = [
            'meta_description' => $oSitePage->getMetaDescription(),
            'meta_keywords' => $oSitePage->getMetaKeywords(),
            'content' => $this->parse('Page/page.twig', $aViewData),
            'title' => $oSitePage->getTitle(),

        ];
        return $aView;
        exit($sUrl);
    }
}
