<?php

namespace _DefaultApi;

use Crud\BaseManager;
use Crud\IActionDefinition;
use Exception\LogicException;

class CrudActionHelper
{
    public static function trigger(
        BaseManager $oManager,
        string $sRequestUri,
        array $aGet,
        array $aPost,
        int $iItemId
    ): void {
        $oAction = self::getAction($oManager, $sRequestUri, $aGet, $aPost, $iItemId);
        $oAction->trigger('item');
    }

    private static function getAction(
        BaseManager $oManager,
        string $sUrlPath,
        array $aGet,
        array $aPost,
        int $iItemId
    ): ?IActionDefinition {
        $sPossiblyActionName = array_reverse(explode('/', $sUrlPath))[0];
        if (is_numeric($sPossiblyActionName)) {
            return null;
        }
        $aEvents = $oManager->getAllAvailableActions();
        $aEventObjects = $oManager->loadCrudActions($aEvents);

        foreach ($aEventObjects as $aEventObject) {
            if ($aEventObject['base_name'] == $sPossiblyActionName) {
                $aGet['item_id'] = $iItemId;

                $sActionFqn = $aEventObject['fqn'];
                return new $sActionFqn($aGet, $aPost);
            }
        }
        throw new LogicException("Action $sPossiblyActionName not found.");
    }

    public static function isActionRequest(BaseManager $oManager, string $sUrlPath): bool
    {
        $sPossiblyActionName = array_reverse(explode('/', $sUrlPath))[0];
        if (is_numeric($sPossiblyActionName)) {
            return false;
        }
        $aEvents = $oManager->getAllAvailableActions();
        $aEventObjects = $oManager->loadCrudActions($aEvents);

        foreach ($aEventObjects as $aEventObject) {
            if ($aEventObject['base_name'] == $sPossiblyActionName) {
                return true;
            }
        }
        return false;
    }
}
