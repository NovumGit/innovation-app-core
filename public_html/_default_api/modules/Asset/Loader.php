<?php
namespace _DefaultApi\Asset;

use Core\Header;
use Core\MainController;

class Loader extends MainController
{
    function run()
    {
        /**
         * @todo Load images from the data folder and not from the admin document root.
         */
        $sRequestedFile = $this->getRequestUri();
        $sAdminFileDir = '../../../admin_public_html/custom/' . $_ENV['SYSTEM_ID'];

        $sLogoFile = $sAdminFileDir . '/' . basename($sRequestedFile);


        if(file_exists($sLogoFile))
        {
            Header::showHeader($sLogoFile);

            readfile($sLogoFile);
        }
        else
        {

            $sLogoFile = '../../../admin_public_html/placeholders/no-image-square.png';
            readfile($sLogoFile);
        }
    }

}
