<?php
namespace _DefaultApi\Doc;

use Core\MainController;
use Crud\BaseManager;
use Crud\CrudFactory;
use Crud\FormManager;
use Crud\Generic\Field\GenericDelete;
use Crud\Generic\Field\GenericEdit;
use Crud\IEventField;

class ExplainCrud extends MainController
{
    function getRegularCrudFields(BaseManager $oManager)
    {
        $aFields = $oManager->getAllAvailableFields();
        $aFieldObjects = $oManager->loadCrudFields($aFields);
        $aOut = [];
        foreach ($aFieldObjects as $oFieldObject)
        {
            if(is_subclass_of($oFieldObject, GenericEdit::class))
            {
                continue;
            }
            else if(is_subclass_of($oFieldObject, GenericDelete::class))
            {
                continue;
            }
            else if($oFieldObject instanceof IEventField)
            {
                continue;
            }
            $aOut[] = $oFieldObject;
        }
        return $aOut;
    }
    function getCrudActions(BaseManager $oManager)
    {
        $aEvents = $oManager->getAllAvailableActions();
        $aEventObjects = $oManager->loadCrudActions($aEvents);
        return $aEventObjects;
    }
    function getCallbacks()
    {

    }
    function run()
    {
        $sResource = preg_replace('/\/v[0-9]+\/doc\//','', $this->getRequestUri());
        $oManager = CrudFactory::getByResourceKey($sResource);

        if($oManager instanceof FormManager)
        {
            $oCrudConfig =  $oManager->getCrudConfig();
            $aEditors = $oCrudConfig->getCrudEditors();
        }

        $aRegularCrudFields = $this->getRegularCrudFields($oManager);
        $aCrudActions = $this->getCrudActions($oManager);
        $aSiteSettings = getSiteSettings();


        $aData = [
            'edit_layouts' => $aEditors,
            'api_version' => $aSiteSettings['api_version'],
            'manager' => $oManager,
            'module' => $sResource,
            'host' => $_SERVER['HTTP_HOST'],
            'protocol' => getSiteSettings()['protocol'],
            'regular_fields' => $aRegularCrudFields,
            'crud_actions' => $aCrudActions,
        ];

        return [
            'active' => 'resource_index',
            'content' => $this->parse('Doc/explain.twig', $aData)
        ];
    }
}
