<?php
namespace _DefaultApi\Home;

use _DefaultApi\EndpointFilter;
use _DefaultApi\GenericDatatypesHelper;
use _DefaultApi\InvertedEndpointFilter;
use _DefaultApi\Schema\UrlHelper;
use Core\MainController;
use Crud\CrudFactory;
use Crud\IApiExposable;

class Controller extends MainController
{
    function run()
    {
        $aGenericDataTypes = GenericDatatypesHelper::getDatatypes();

        $iApiVersion = getSiteSettings()['api_version'];

        $aActiveMap = [
            '/' => ['active' => 'home', 'template' => 'home'],
            '/rest' => ['active' => 'rest', 'template' => 'rest'],
            '/v' . $iApiVersion . '/doc/datatypes' => ['active' => 'datatypes', 'template' => 'datatypes'],
            '/endpoints' => ['active' => 'endpoints', 'template' => 'endpoints'],
            '/schema' => ['active' => 'schema', 'template' => 'schema'],
        ];

        $aData = [
            'host' => $_SERVER['HTTP_HOST'],
            'protocol' => getSiteSettings()['protocol'],
            'schema_urls' => new UrlHelper(getSiteSettings()['protocol'], $_SERVER['HTTP_HOST'], $iApiVersion),
            'api_version' => $iApiVersion,
            'datatypes' => $aGenericDataTypes,
            'relevant_cruds' => CrudFactory::getAll([IApiExposable::class], CrudFactory::getEndpointFilter()),
            'system_cruds' => CrudFactory::getAll([IApiExposable::class], new InvertedEndpointFilter(CrudFactory::getEndpointFilter())),
            'all_cruds' => CrudFactory::getAll([IApiExposable::class], new EndpointFilter())

        ];


        $aOut = [
            'active' => $aActiveMap[$this->getRequestUri()]['active'],
            'content' => $this->parse('Home/' . $aActiveMap[$this->getRequestUri()]['template'] . '.twig', $aData)
        ];

        return $aOut;
    }
}
