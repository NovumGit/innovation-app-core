<?php

namespace _DefaultApi;

use Core\Customer;
use Core\MainController;
use Exception\LogicException;
use Model\Cms\Site;
use Model\Setting\MasterTable\Language;

abstract class ApiController extends MainController
{
    protected Site $oSite;

    public function __construct($aGet, $aPost)
    {
        parent::__construct($aGet, $aPost);
    }

    public function parse($sTemplate, $aData, Language $oLanguageOverwrite = null)
    {
        if (!function_exists('getSiteSettings')) {
            throw new LogicException("Function getSiteSettings missing");
        }
        $aData['settings'] = getSiteSettings();

        $aData['customer'] = [];
        $aData['customer']['is_signed_in'] = Customer::isSignedIn();

        return parent::parse($sTemplate, $aData);
    }
}
