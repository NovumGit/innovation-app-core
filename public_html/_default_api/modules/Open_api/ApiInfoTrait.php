<?php
namespace _DefaultApi\Open_api;

use Api\Info\IApiInfo;

trait ApiInfoTrait
{

    /**
     * @return IApiInfo
     * @throws \Exception
     */
    function getApiInfo():IApiInfo
    {
        $sNamespace = getSiteSettings()['namespace'];

        $sFqn = '\\' . $sNamespace . '\\Generated\\ApiInfo';

        if(class_exists($sFqn))
        {
            return new $sFqn();
        }
        throw new \Exception("Class missing $sFqn");
    }
}
