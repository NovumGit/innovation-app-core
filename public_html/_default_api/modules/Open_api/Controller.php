<?php
namespace _DefaultApi\Open_api;

use _DefaultApi\Open_api\Document\Element\Tags\Collection;
use _DefaultApi\Open_api\Document\Element\Tags\Create;
use _DefaultApi\Open_api\Document\Element\Tags\Delete;
use _DefaultApi\Open_api\Document\Element\Tags\Get;
use _DefaultApi\Open_api\Document\Element\Tags\Item;
use _DefaultApi\Open_api\Document\Element\Tags\Search;
use _DefaultApi\Open_api\Document\Element\Tags\Update;
use _DefaultApi\Open_api\Document\Paths;
use _DefaultApi\Open_api\Document\Schemas;
use Core\MainController;
use Crud\CrudFactory;
use Crud\IApiExposable;

class Controller extends MainController
{
    use ApiInfoTrait;

    /**
     * @return array|void
     * @throws \Exception
     */
    function run()
    {
        header('Content-type: application/json');

        $oApiInfo = $this->getApiInfo();
        $oApiInfo->getDescription();

        // openapi.json
        header('Content-type: application/json');

        $oEndpointFilter = CrudFactory::getEndpointFilter();
        if($this->getRequestUri(false) === '/openapi-full.json')
        {
            $oEndpointFilter = null;
        }

        $aAllCruds = CrudFactory::getAll([IApiExposable::class], $oEndpointFilter);

        $aData = [
            "components" => [
                "schemas" => Schemas::get($aAllCruds)
            ],
            "info" => [
                "title" => $oApiInfo->getTitle(),
                "description" => preg_replace('/\s+/S', " ", $oApiInfo->getDescription()),
                "contact" => [
                    "name" => $oApiInfo->getOrganisation(),
                    "email" => $oApiInfo->getEmail()
                ],
                "version" => "1.0.0"
            ],
            "openapi" => "3.0.2",
            "paths" => Paths::get($aAllCruds),
            "schemes" => [
                "https"
            ],
            "tags" => [
                Collection::get(),
                Search::get(),
                Create::get(),
                Item::get(),
                Get::get(),
                Delete::get(),
                Update::get()
            ],
            "servers" => [
                [
                    "url" => getSiteSettings()['protocol'] . '://' . getSiteSettings()['live_domain'],
                    "description" => "Production environment"
                ],
                [
                    "url" => getSiteSettings()['protocol'] . '://' . getSiteSettings()['test_domain'],
                    "description" => "Test environment"
                ]
            ]
        ];

        echo json_encode($aData, JSON_UNESCAPED_SLASHES);
        exit();



    }

}
