<?php
namespace _DefaultApi\Open_api\Document;

use _DefaultApi\Open_api\Document\Element\Parameters\Integer;
use _DefaultApi\Open_api\Document\Element\Response;
use _DefaultApi\Open_api\Document\Element\Tags\Collection;
use _DefaultApi\Open_api\Document\Element\Tags\Create;
use _DefaultApi\Open_api\Document\Element\Tags\Delete;
use _DefaultApi\Open_api\Document\Element\Tags\Get;
use _DefaultApi\Open_api\Document\Element\Tags\Item;
use _DefaultApi\Open_api\Document\Element\Tags\Search;
use _DefaultApi\Open_api\Document\Element\Tags\Update;
use Crud\FormManager;
use Crud\IApiExposable;

final class Paths
{
    /**
     * @param FormManager[] $aCruds
     * @return array|null
     */
    static final function get(array $aCruds):?array
    {
        if(empty($aCruds))
        {
            return null;
        }
        $aOut = [];
        foreach ($aCruds as $aCrud)
        {
            $oManagerObject = $aCrud['crud_manager'];
            if($oManagerObject instanceof IApiExposable)
            {
                $sModuleOriginal = $oManagerObject->getModuleName();;

                $sModuleLc = strtolower($sModuleOriginal);

                $aOut["/v" . getSiteSettings()["api_version"] . "/rest/" . $sModuleLc ."/"] = [
                    "get" => [
                        "tags" => [Collection::name, Search::name],
                        "description" => "Retrieves the items from the $sModuleLc. You may specify filters in the URL to limit the items received.",
                         "operationId" =>  'find' . $sModuleOriginal,
                        "summary" => "Retrieves the items from the $sModuleLc. You may specify filters in the URL to limit the items received.",

                        "responses" => [
                            200 => Response::get("The collection of $sModuleLc items.", "#/components/schemas/$sModuleLc"),
                            401 => Response::get("Returned when you are not allowed to perform this action")
                        ]
                    ],
                    "post" => [
                        "tags" => [Item::name, Create::name],
                        "description" => 'Create a new item in the collection',
                        "summary" => 'Create new ' . $sModuleLc . ' record',
                        "operationId" =>  'create' . $sModuleOriginal,
                        "responses" => [
                            201 => Response::get("The item was created successfully.", "#/components/schemas/$sModuleLc"),
                            401 => Response::get("Returned when you are not allowed to perform this action")
                        ]
                    ]
                ];

                $aOut["/v" . getSiteSettings()["api_version"] . "/rest/" . $sModuleLc . "/{id}"] =  [
                    "get" => [
                        "tags" => [Item::name, Get::name],
                        "description" => 'Retrieve the item with the given key.',
                        "summary" => 'Fetch an existing ' . $sModuleLc . ' record',
                        "operationId" =>  'get' . $sModuleOriginal,

                        "parameters" => [
                            Integer::get("id", "The id of the " . $sModuleLc . " record, always called id", "path", true)
                        ],
                        "responses" => [
                            200 => Response::get("A $sModuleLc object", "#/components/schemas/$sModuleLc"),
                            401 => Response::get("Returned when you are not allowed to perform this action"),
                            404 => Response::get("Returned when the requested item could not be found.")
                        ]
                    ],

                    "delete" => [
                        "tags" => [Item::name, Delete::name],
                        "description" => 'Remove the item with the given key from the resource.',
                        "summary" => 'Remove a ' . $sModuleLc . ' record',
                        "operationId" =>  'delete' . $sModuleOriginal,
                        "parameters" => [
                            Integer::get("id", "The id of the " . $sModuleLc . " record, always called id", "path", true)
                        ],
                        "responses" => [
                            401 => Response::get("You are unauthorized to perform this action"),
                            404 => Response::get("Returned when the requested item could not be found.")
                        ]
                    ],
                    "patch" => [
                        "tags" => [Item::name, Update::name],
                        "description" => 'Update the item with the given key with the given fields. Fields not given will remain unchanged.',
                        "summary" => 'Change a ' . $sModuleLc . ' record',
                        "operationId" =>  'update' . $sModuleOriginal,
                        "parameters" => [
                            Integer::get("id", "The id of the " . $sModuleLc . " record, always called id", "path", true)
                        ],
                        "responses" => [
                            401 => Response::get("You are unauthorized to perform this action"),
                            404 => Response::get("Returned when the item could not be located")
                        ]
                    ],
                    "put" => [
                        "tags" => [Item::name, Update::name],
                        "description" => 'Replace / overwrite the item with the given key with the given fields. Fields not given will be set to null.',
                        "summary" => 'Replace a ' . $sModuleLc . ' record',
                        "operationId" =>  'replace' . $sModuleOriginal,
                        "parameters" => [
                            Integer::get("id", "The id of the " . $sModuleLc . " record, always called id", "path", true)
                        ],
                        "responses" => [
                            401 => Response::get("You are unauthorized to perform this action"),
                            404 => Response::get("Returned when the item could not be located")
                        ]
                    ]
                ];
            }
        }
        return $aOut;
    }

}

