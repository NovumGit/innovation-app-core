<?php
namespace _DefaultApi\Open_api\Document;

use Crud\CrudFieldFilter;
use Crud\IApiExposable;

final class Schemas
{
    static final function get(array $aCruds):?array
    {
        if(empty($aCruds))
        {
            return null;
        }
        $sSiteUrl = getSiteSettings()['protocol'] . '://' . getSiteSettings()['live_domain'];
        $sCurrentApiVersion = getSiteSettings()['api_version'];
            $aOut = [];
        foreach ($aCruds as $aCrud)
        {
            $aModuleInfo = ["properties" => null, "type" => "object"];
            $oManagerObject = $aCrud["crud_manager"];
            if($oManagerObject instanceof IApiExposable)
            {
                $oCrudFieldFilter = new CrudFieldFilter($oManagerObject);

                if(count($oCrudFieldFilter->getEditableFields()) === 0 && count($oCrudFieldFilter->getRequiredFields()) === 0)
                {
                    continue;
                }
                foreach ($oCrudFieldFilter->getEditableFields() as $oField)
                {
                    $aModuleInfo["properties"][$oField->getFieldName()] = [
                        "title" => $oField->getFieldLabel(),
                        "type" => $oField->getPrimitive()->getLabel(),
                        "format" => $oField->getDataType(),
                    ];
                }

                foreach ($oCrudFieldFilter->getRequiredFields() as $oField)
                {
                    $aModuleInfo["required"][] = $oField->getFieldName();
                }

                $aModuleInfo["externalDocs"] = [
                    "url" => $sSiteUrl . '/v' . $sCurrentApiVersion . '/doc/' . strtolower($oManagerObject->getModuleName()),
                    "description" => "Meer informatie over dit endpoint"

                ];
            }
            $aOut[strtolower($oManagerObject->getModuleName())] = $aModuleInfo;
        }

        return $aOut;
    }

}
