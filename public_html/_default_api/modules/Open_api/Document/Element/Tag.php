<?php

namespace _DefaultApi\Open_api\Document\Element;

use _DefaultApi\Open_api\ApiInfoTrait;

abstract class Tag
{
    use ApiInfoTrait;

    abstract static function get();
}