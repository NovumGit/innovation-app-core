<?php

namespace _DefaultApi\Open_api\Document\Element;

use _DefaultApi\Open_api\ApiInfoTrait;

abstract class Parameter
{
    use ApiInfoTrait;

    abstract static function get(string $sName, string $sDescription, string $sIn, bool $bRequired);
}