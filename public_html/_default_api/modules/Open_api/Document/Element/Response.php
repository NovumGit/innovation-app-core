<?php

namespace _DefaultApi\Open_api\Document\Element;

final class Response
{
    static final function get(string $sDescription, string $sRef = null, $bResultIsCollection = false): array
    {
        $aOut = ["description" => $sDescription];

        if($bResultIsCollection)
        {
            $aSchemaContent = [
                "type" => "array",
                "items" => [
                    "\$ref" => $sRef
                ]
            ];
        }
        else
        {
            $aSchemaContent = [
                "\$ref" => $sRef
            ];
        }

        if ($sRef) {
            $aExtra =[
                "content" => [
                    "application/json" => [
                        "schema" => [
                            $aSchemaContent
                        ]
                    ]
                ]

            ];
            $aOut = array_merge($aOut, $aExtra);
        }

        return $aOut;
    }
}
