<?php
namespace _DefaultApi\Open_api\Document\Element\Tags;

use _DefaultApi\Open_api\Document\Element\ExternalDoc;
use _DefaultApi\Open_api\Document\Element\Tag;

final class Search extends Tag
{
    const name = "search";

    /**
     * @return array
     * @throws \Exception
     */
    final static function get(): array
    {
        $sDocumentationUrl = (new self)->getApiInfo()->getDocumentationUrl();

        return [
            "name" => self::name,
            "description" => "Allows you to run queries on this collection",
            "externalDocs" => ExternalDoc::get("For instructions see here", $sDocumentationUrl)
        ];
    }
}