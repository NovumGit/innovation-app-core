<?php
namespace _DefaultApi\Open_api\Document\Element\Tags;

use _DefaultApi\Open_api\Document\Element\ExternalDoc;
use _DefaultApi\Open_api\Document\Element\Tag;

final class Get extends Tag
{
    const name = "get";

    /**
     * @return array
     * @throws \Exception
     */
    final static function get(): array
    {
        $sDocumentationUrl = (new self)->getApiInfo()->getDocumentationUrl();
        return [
            "name" => self::name,
            "description" => "Fetch a single item from the collection",
            "externalDocs" => ExternalDoc::get("See this page for more info", $sDocumentationUrl)
        ];
    }
}