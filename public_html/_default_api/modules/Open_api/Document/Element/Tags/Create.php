<?php
namespace _DefaultApi\Open_api\Document\Element\Tags;

use _DefaultApi\Open_api\Document\Element\ExternalDoc;
use _DefaultApi\Open_api\Document\Element\Tag;

final class Create extends Tag
{
    const name = "create";

    /**
     * @return array
     * @throws \Exception
     */
    final static function get(): array
    {
        $sDocumentationUrl = (new self)->getApiInfo()->getDocumentationUrl();


        return [
            "name" => self::name,
            "description" => "Request multiple items at once",
            "externalDocs" => ExternalDoc::get("See this page for more info", $sDocumentationUrl)
        ];
    }
}