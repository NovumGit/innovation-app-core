<?php
namespace _DefaultApi\Open_api\Document\Element\Tags;

use _DefaultApi\Open_api\Document\Element\ExternalDoc;
use _DefaultApi\Open_api\Document\Element\Tag;

final class Update extends Tag
{
    const name = "update";

    /**
     * @return array
     * @throws \Exception
     */
    final static function get(): array
    {
        $sDocumentationUrl = (new self)->getApiInfo()->getDocumentationUrl();

        return [
            "name" => self::name,
            "description" => "Change a single item",
            "externalDocs" => ExternalDoc::get("See this page for more info", $sDocumentationUrl)
        ];
    }
}