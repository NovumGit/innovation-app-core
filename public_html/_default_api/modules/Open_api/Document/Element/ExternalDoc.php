<?php

namespace _DefaultApi\Open_api\Document\Element;

final class ExternalDoc
{
    static final function get(string $sDescription, string $sUrl): array
    {
        return [
            "description" => $sDescription,
            "url" => $sUrl
        ];
    }
}