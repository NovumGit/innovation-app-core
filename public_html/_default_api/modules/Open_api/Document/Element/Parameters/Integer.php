<?php
namespace _DefaultApi\Open_api\Document\Element\Parameters;

use _DefaultApi\Open_api\Document\Element\Parameter;

final class Integer extends Parameter
{
    const name = "collection";

    /**
     * @param string $sName
     * @param string $sDescription
     * @param string $sIn "query", "header", "path" or "cookie".
     * @param bool $bRequired
     * @return array
     */
    final static function get(string $sName, string $sDescription, string $sIn, bool $bRequired): array
    {
        return [
            "name" => $sName,
            "in" => $sIn,
            "description" => $sDescription,
            "required" => $bRequired
        ];
    }
}
