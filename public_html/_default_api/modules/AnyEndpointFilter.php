<?php

namespace _DefaultApi;

use Crud\FormManager;
use Crud\IApiEndpointFilter;

class AnyEndpointFilter implements IApiEndpointFilter
{
    public function getAllowedEndpoints(): array
    {
        return [];
    }
    public function getDisallowedEndpoints(): array
    {
        return [];
    }
    public function filter(FormManager $oManager): bool
    {
        return true;
    }
}
