<?php
namespace _DefaultApi\Rest\V1;

use _DefaultApi\ApiController;
use Core\Utils;
use Crud\CrudFactory;
use Crud\IApiExposable;
use Exception;

class Item extends ApiController
{
    /**
     * @return array|void
     * @throws Exception
     */
    function run()
    {
        $iItemId = array_reverse(explode('/', $this->getRequestUri()))[0];
        $oManager = $this->getCrudManager();
        $oQueryObject = $oManager->getQueryObject();


        // print_r($oQueryObject);
        if(method_exists($oQueryObject, 'findOneBy'))
        {
            $oRow = $oQueryObject->findOneBy('id', $iItemId);

            if(method_exists($oRow, 'toArray'))
            {
                $aRow = $oRow->toArray();
                echo json_encode($aRow);
                exit();
            }
            Utils::jsonOk();
        }
        else
        {
            throw new \LogicException("Could nog find item.");
        }
        exit();

    }
    private function getCrudManager():IApiExposable
    {
        $sRequestUri = $this->getRequestUri();
        $sRequestUriNoVars = explode('?', $sRequestUri)[0];
        $sUrl = str_replace('/v1/rest/', '', $sRequestUriNoVars);
        $sEndpoint = preg_replace('/\/[0-9]+$/', '', $sUrl);
        $aCruds = CrudFactory::getAll([IApiExposable::class]);

        return $aCruds[$sEndpoint]['crud_manager'];
    }
}
