<?php
namespace _DefaultApi\Rest\V1;

use _DefaultApi\ApiController;
use Core\Utils;
use Crud\CrudFactory;
use Crud\FormManager;
use Crud\IApiExposable;
use Exception;
use Model\Logging\Except_log;

class Collection extends ApiController
{
    /**
     * @return array|void
     * @throws Exception
     */
    function run()
    {
        $oManager = $this->getCrudManager();
        if(strtolower($_SERVER['REQUEST_METHOD']) == 'post')
        {
            $this->add();
        }
        else
        {
            if(!$oManager instanceof IApiExposable)
            {
                throw new Exception("Manager needs to implement IApiExposable for this to work.");
            }

            $oQueryObject = $oManager->getQueryObject();

            $aRows = $oQueryObject->find();
            echo $aRows->toJSON();
            exit();
        }
    }

    private function add()
    {
        try
        {
            $oManager = $this->getCrudManager();
            if($_SERVER['CONTENT_TYPE'] != 'application/json')
            {
                throw new \Exception("Unsupported content-type");
            }
            $aData = json_decode(file_get_contents('php://input'), true);

            if($oManager->isValid($aData))
            {
                $oData = $oManager->save($aData);
                Utils::jsonExit(json_encode($oData->toArray()));
            }
            Utils::jsonExit(['errors' => $oManager->getValidationErrors($aData)]);
        }
        catch (Exception $e)
        {
            Except_log::register($e, false);
            Utils::jsonExit(['errors' => [$e->getMessage()]]);

        }
    }

    private function getCrudManager():FormManager
    {
        $sUrl = str_replace('/v1/rest/', '', $this->getRequestUri());
        $sEndpoint = preg_replace('/\/$/', '', $sUrl);
        $aCruds = CrudFactory::getAll([IApiExposable::class]);

        return $aCruds[$sEndpoint]['crud_manager'];
    }
}
