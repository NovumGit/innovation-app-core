<?php
namespace _DefaultApi\Rest\V1;

use _DefaultApi\ApiController;
use Crud\CrudFactory;
use Crud\FormManager;
use Crud\IApiExposable;
use Exception;

class Definition extends ApiController
{
    /**
     * @return array|void
     * @throws Exception
     */
    function run()
    {
        $oManager = $this->getCrudManager();


        if(!$oManager instanceof IApiExposable)
        {
            throw new Exception("Manager needs to implement IApiExposable for this to work.");
        }

        $aFields = $oManager->getAllFieldsAsArray();


        echo json_encode($aFields);

        exit();

    }

    private function getCrudManager():FormManager
    {
        $sUrl = str_replace(['/v1/rest/', '/definition'], '', $this->getRequestUri());

        $sEndpoint = preg_replace('/\/$/', '', $sUrl);
        $aCruds = CrudFactory::getAll([IApiExposable::class]);

        return $aCruds[$sEndpoint]['crud_manager'];
    }
}
