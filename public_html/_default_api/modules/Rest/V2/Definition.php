<?php

namespace _DefaultApi\Rest\V2;

use _DefaultApi\ApiController;
use _DefaultApi\CrudManagerTrait;
use Crud\Field;
use Crud\IApiExposable;
use Crud\IEventField;
use Exception;

class Definition extends ApiController {
    use CrudManagerTrait;

    /**
     * @return array|void
     * @throws Exception
     */
    public function run() {
        $oManager = $this->getCrudManager();

        $iLayout = $this->get('layout');

        if (!$oManager instanceof IApiExposable) {
            throw new Exception("Manager needs to implement " . IApiExposable::class . " for this to work.");
        }

        if ($iLayout) {
            $aFields = $oManager->getRenderedFormAsArray($iLayout);
        } else {
            $aFields = $oManager->getAllFieldsAsArray(function (Field $oField) {

                if ($oField instanceof IEventField) {
                    return false;
                }
                return true;
            });
        }

        echo json_encode($aFields);

        exit();
    }
}
