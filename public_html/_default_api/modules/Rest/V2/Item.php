<?php

namespace _DefaultApi\Rest\V2;

use _DefaultApi\ApiController;
use _DefaultApi\CrudActionHelper;
use _DefaultApi\CrudManagerTrait;
use _DefaultApi\Rest\V2\Helper\FieldStyle;
use _DefaultApi\Rest\V2\Helper\Payload;
use Core\Header;
use Core\HttpStatusCode;
use Core\Json\Error;
use Core\Utils;
use Exception;
use Propel\Runtime\Map\TableMap;

class Item extends ApiController
{
    use CrudManagerTrait;

    private function delete(int $iItemId): void
    {
        try {
            $oManager = $this->getCrudManager();
            $oManager->delete($iItemId);
            Utils::jsonOk();
        } catch (Exception\ClassNotFoundException $e) {
            Header::sendStatusCode(new HttpStatusCode(HttpStatusCode::HTTP_NOT_FOUND));
            $oError = new Error("Could not locate the item that needs to be deleted. ");
            echo $oError->toJson();
            exit();
        } catch (Exception\InvalidArgumentException $e) {
            Header::sendStatusCode(new HttpStatusCode(HttpStatusCode::HTTP_BAD_REQUEST));
            $oError = new Error(<<<TAG
Could not delete that item because this action is unavailable in the API  for this type of data
TAG);
            echo $oError->toJson();
            exit();
        }
    }

    private function put($iItemId)
    {
        $aData = Payload::getPostPutPatch();
        $aData['id'] = $iItemId;
        $this->getCrudManager()->save($aData);
    }

    private function patch($iItemId)
    {
        $aData = Payload::getPostPutPatch();
        $aData['id'] = $iItemId;
        $this->getCrudManager()->save($aData);
    }

    private function getKeyFromPath()
    {
        $aParts = explode('/', $this->getRequestUri());
        $iCountParts = count($aParts);
        $sAltITemId = array_reverse($aParts)[1];
        if ($iCountParts <= 5 || is_numeric($sAltITemId)) {
            return 'id';
        }

        return array_reverse($aParts)[1];
    }
    private function getItemId(): string
    {
        $aUrlParts = array_reverse(explode('/', $this->getRequestUri()));

        if (!is_numeric($aUrlParts[0]) && is_numeric($aUrlParts[1])) {
            // Action url
            return $aUrlParts[1];
        }
        // Normal url
        return $aUrlParts[0];
    }

    /**
     * @return array|void
     * @throws Exception
     */
    function run()
    {
        header('Access-Control-Allow-Methods: HEAD,GET,POST,PUT,PATCH,DELETE');
        header('Access-Control-Allow-Headers: *');

        $oManager = $this->getCrudManager();
        $oQueryObject = $oManager->getQueryObject();
        $sFieldCase = strtolower(
            $this->get(
                'field_style',
                'snake_case',
                false,
                Collection::fieldStyles
            )
        );
        $sFieldLayout = FieldStyle::get($sFieldCase);

        $sFormat = strtolower($this->get('layout_format', 'associative', false, ['associative', 'list']));
        $iItemId = $this->getItemId($this->getRequestUri());

        $sFieldName = $this->getKeyFromPath();
        $sFieldPhpName = $oManager->getTableMap()::translateFieldnameForClass(
            $oQueryObject->getModelName(),
            $sFieldName,
            TableMap::TYPE_FIELDNAME,
            TableMap::TYPE_PHPNAME
        );

        if (!method_exists($oQueryObject, 'findOneBy')) {
            throw new Exception\LogicException("Invalid QueryObject");
        }

        $oSubject = $oQueryObject->findOneBy($sFieldPhpName, $iItemId);

        if (CrudActionHelper::isActionRequest($oManager, $this->getRequestUri())) {
            CrudActionHelper::trigger(
                $oManager,
                $this->getRequestUri(),
                $this->get(),
                $this->post(),
                $oSubject->getId()
            );
        }

        if (strtolower($_SERVER['REQUEST_METHOD']) === 'delete') {
            $this->delete($oSubject->getId());
        }
        if (strtolower($_SERVER['REQUEST_METHOD']) === 'put') {
            $this->put($oSubject->getId());
        }
        if (strtolower($_SERVER['REQUEST_METHOD']) === 'patch') {
            $this->patch($oSubject->getId());
        }

        $oRow = $oQueryObject->findOneBy($sFieldPhpName, $iItemId);

        if (method_exists($oRow, 'toArray')) {
            if ($sFormat === 'list') {
                $aRow = $oRow->toArray(FieldStyle::get('snake_case'));
                $aFields = $oManager->getAllFieldsAsArray();
                foreach ($aFields as &$aField) {
                    unset($aField['interfaces']);
                    foreach ($aRow as $sKey => $sValue) {
                        if ($sKey == $aField['name']) {
                            $aField['value'] = $sValue;
                        }
                    }
                }
                echo json_encode($aFields);
            } else {
                $aRow = $oRow->toArray($sFieldLayout);
                echo json_encode($aRow);
            }
            exit();
        }
        Utils::jsonOk();
        exit();
    }
}
