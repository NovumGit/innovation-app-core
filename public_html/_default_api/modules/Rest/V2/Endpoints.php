<?php

namespace _DefaultApi\Rest\V2;

use _DefaultApi\ApiController;
use _DefaultApi\CrudManagerTrait;
use Crud\CrudFactory;
use Crud\FormManager;
use Crud\IApiExposable;
use Crud\IEventField;
use Crud\IField;
use Exception;

class Endpoints extends ApiController
{
    use CrudManagerTrait;

    /**
     * @return array|void
     * @throws Exception
     */
    public function run()
    {

        $aEndpoints = CrudFactory::getAll([IApiExposable::class], CrudFactory::getEndpointFilter());

        $iApiVersion = getSiteSettings()['api_version'];
        $aFieldInfo = [];
        $sBaseUrl  = getSiteSettings()['protocol'] . '://' . getSiteSettings()['live_domain'];

        foreach ($aEndpoints as &$aEndpoint) {
            $sManagerName = $aEndpoint['fqn'];
            $oManager = new $sManagerName();
            if ($oManager instanceof FormManager) {
                $aFieldInfo = [];
                $aFields = $oManager->getFieldIterator();
                foreach ($aFields as $oField) {
                    if ($oField instanceof IField && !$oField instanceof IEventField) {
                        $aFieldInfo[] = [
                            'name' => $oField->getFieldName(),
                            'title' => $oField->getFieldTitle(),
                            'primitive' => (string) $oField->getPrimitive(),
                        ];
                    }
                }
            }

            $aEndpoint['field'] = $aFieldInfo;
            $aEndpoint['urls'] = [
                'path' => [
                    'documentation' => "/v$iApiVersion/doc/{$aEndpoint['module']}",
                    'definition' => "/v$iApiVersion/rest/{$aEndpoint['module']}/definition",
                    'collection' => "/v$iApiVersion/rest/{$aEndpoint['module']}/",
                    'item' => "/v$iApiVersion/rest/{$aEndpoint['module']}/{id}"
                ],
                'full' => [
                    'documentation' => $sBaseUrl . "/v$iApiVersion/doc/{$aEndpoint['module']}",
                    'definition' => $sBaseUrl . "/v$iApiVersion/rest/{$aEndpoint['module']}/definition",
                    'collection' => $sBaseUrl . "/v$iApiVersion/rest/{$aEndpoint['module']}/",
                    'item' => $sBaseUrl . "/v$iApiVersion/rest/{$aEndpoint['module']}/{id}"
                ]
            ];
        }
        echo json_encode($aEndpoints);
        exit();
    }
}
