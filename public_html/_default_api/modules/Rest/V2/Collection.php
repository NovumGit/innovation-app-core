<?php

namespace _DefaultApi\Rest\V2;

use _DefaultApi\ApiController;
use _DefaultApi\CrudApiManagerTrait;
use _DefaultApi\CrudManagerTrait;
use _DefaultApi\Rest\V2\Helper\FieldStyle;
use _DefaultApi\Rest\V2\Helper\Payload;
use Core\Header;
use Core\HttpStatusCode;
use Core\Mime\JsonMime;
use Core\Utils;
use Crud\IApiExposable;
use Exception;
use Model\Logging\Except_log;
use Model\Setting\CrudManager\FilterOperatorQuery;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Map\TableMap;

class Collection extends ApiController
{
    use CrudManagerTrait;
    use CrudApiManagerTrait;

    public const fieldStyles = [
        'snake_case',
        'camelcase',
        'numeric',
        'uppercamel',
    ];

    /**
     * @return array|void
     * @throws Exception
     */
    public function run()
    {

        header('Access-Control-Allow-Methods: HEAD,GET,POST,DELETE');
        header('Access-Control-Allow-Headers: *');

        $sFieldCase = strtolower(
            $this->get('field_style', 'snake_case', false, self::fieldStyles)
        );
        $oManager = $this->getCrudManager();
        if (strtolower($_SERVER['REQUEST_METHOD']) == 'post') {
            $this->add();
        } else {
            if (!$oManager instanceof IApiExposable) {
                throw new Exception("Manager needs to implement IApiExposable for this to work.");
            }

            $iItemsPP = $this->get('items_pp', 10, false, 'numeric');
            $iCurrentPage = $this->get('current_page', 1, false, 'numeric');

            $oQueryObject = $oManager->getQueryObject();

            $aFilters = $this->get('filter');

            if (!empty($aFilters) && is_array($aFilters)) {
                foreach ($aFilters as $sFieldName => $aFilter) {
                    $sFilterOperator = $aFilter['operator'] ?? 'equals';
                    $oFilterOperator = FilterOperatorQuery::create()->findOneByName($sFilterOperator);

                    $sFieldPhpName = $oManager->getTableMap()::translateFieldnameForClass(
                        $oQueryObject->getModelName(),
                        $sFieldName,
                        TableMap::TYPE_FIELDNAME,
                        TableMap::TYPE_PHPNAME
                    );

                    $oQueryObject->filterBy($sFieldPhpName, $aFilter['value'], $oFilterOperator->toCriteria());
                }
            }

            $pPagedData = $oQueryObject->paginate($iCurrentPage, $iItemsPP);
            $sFieldLayout = FieldStyle::get($sFieldCase);

            $aRows = [];
            foreach ($pPagedData as $oObject) {
                if (!$oObject instanceof ActiveRecordInterface) {
                    throw new Exception\LogicException("Expected an instance of ActiveRecordInterface");
                }

                $aRows[] = $oObject->toArray($sFieldLayout);
            }

            $aPaginate = [];
            $iPageCount = ceil($pPagedData->getNbResults() / $iItemsPP);

            if ($iCurrentPage > 1) {
                $aPaginate['previous'] = Utils::getRequestUri(true, [
                    'current_page' => ($iCurrentPage--),
                    'items_pp'     => $iItemsPP,
                ]);
            }
            if ($iCurrentPage < $iPageCount) {
                $aPaginate['next'] = Utils::getRequestUri(true, [
                    'current_page' => (++$iCurrentPage),
                    'items_pp'     => $iItemsPP,
                ]);
            }

            $aOut = [
                'count' => $pPagedData->getNbResults(),
                "pages" => $aPaginate,
                'results' => $aRows,

            ];
            echo json_encode($aOut);
            exit();
        }
    }

    private function add()
    {
        try {
            $oManager = $this->getCrudManager();
            $aData = Payload::getPostPutPatch();

            if ($oManager->isValid($aData)) {
                $oData = $oManager->save($aData);
                Utils::jsonExit(json_encode($oData->toArray()));
            }

            Header::contentType(new JsonMime());

            $aErrors = $oManager->getValidationErrors($aData);

            Utils::jsonExit(['errors' => $aErrors]);
        } catch (Exception $e) {
            Header::sendStatusCode(new HttpStatusCode(HttpStatusCode::HTTP_INTERNAL_SERVER_ERROR));
            Except_log::register($e, false);
            Utils::jsonExit(['errors' => [$e->getMessage()]]);
        }
    }
}
