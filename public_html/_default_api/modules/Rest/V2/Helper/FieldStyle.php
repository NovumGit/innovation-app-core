<?php
namespace _DefaultApi\Rest\V2\Helper;

use Propel\Runtime\Map\TableMap;

class FieldStyle
{

    static function get($sFieldCase)
    {
        $sFieldLayout = TableMap::TYPE_FIELDNAME;

        if($sFieldCase == 'camelcase')
        {
            $sFieldLayout = TableMap::TYPE_CAMELNAME;
        }
        else if($sFieldCase == 'numeric')
        {
            $sFieldLayout = TableMap::TYPE_NUM;
        }
        else if($sFieldCase == 'uppercamel')
        {
            $sFieldLayout = TableMap::TYPE_PHPNAME;
        }
        return $sFieldLayout;

    }

}