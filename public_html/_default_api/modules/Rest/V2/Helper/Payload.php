<?php
namespace _DefaultApi\Rest\V2\Helper;


use Core\Header;
use Core\HttpRequest;
use Core\HttpStatusCode;
use Core\Json\Error;
use Core\Json\JsonUtils;

class Payload
{
    /**
     * @return array|null
     */
    static function getPostPutPatch(): ?array
    {
        try
        {
            if (HttpRequest::getContentType() == 'application/json')
            {

                $sJson = file_get_contents('php://input');
                $aData = JsonUtils::decode($sJson);

                if(isset($aData['id']) && empty($aData['id']))
                {
                    unset($aData['id']);
                }
                return $aData;
            }

            throw new \Exception("Unsupported content-type");
        }
        catch (\InvalidArgumentException $e)
        {
            $oError = new Error('Json parse error');
        }
        catch (\Exception $e)
        {
            $oError = new Error('Only content-type application/json is supported, we expect json in the body.');
        }

        $oStatusCode = new HttpStatusCode(HttpStatusCode::HTTP_BAD_REQUEST);
        Header::sendStatusCode($oStatusCode);
        $oError->addError($oStatusCode->getMessage());
        echo $oError->toJson();
        exit();

    }
}
