<?php

namespace _DefaultApi;

use Crud\FormManager;
use Crud\IApiEndpointFilter;

class InvertedEndpointFilter implements IApiEndpointFilter
{
    private IApiEndpointFilter $oOriginalApiEndpointFilter;

    public function __construct(IApiEndpointFilter $oOriginalApiEndpointFilter)
    {
        $this->oOriginalApiEndpointFilter = $oOriginalApiEndpointFilter;
    }

    public function getAllowedEndpoints(): array
    {
        return [];
    }

    public function getDisallowedEndpoints(): array
    {
        return [];
    }

    public function filter(FormManager $oManager): bool
    {
        return !$this->oOriginalApiEndpointFilter->filter($oManager);
    }
}
