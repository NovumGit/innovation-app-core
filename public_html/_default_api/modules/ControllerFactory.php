<?php

namespace _DefaultApi;

use _Default\ControllerFactory as _DefaultControllerFactory;
use _DefaultApi\Asset\Loader;
use _DefaultApi\Doc\ExplainCrud;
use _DefaultApi\Home\Controller;
use _DefaultApi\Open_api\Controller as OpenApiController;
use _DefaultApi\Schema\Generator;
use Core\Header;
use Core\HttpStatusCode;
use Core\IControllerFactory;
use Core\Logger;
use Core\MainController;

class ControllerFactory extends _DefaultControllerFactory implements IControllerFactory {
    public function getController(array $aGet, array $aPost, string $sNamespace = null): MainController {
        $sRequestUri = $_SERVER['REQUEST_URI'];

        $iApiVersion = 2;
        if(function_exists('getSiteSettings'))
        {
            $iApiVersion = getSiteSettings()['api_version'];
        }

        preg_match('/^\/v([0-9]+)/', $sRequestUri, $aMatches);
        $sVersion = isset($aMatches[1]) ? strtoupper($aMatches[1]) : $iApiVersion;
        $sRequestUri = preg_replace('/^\/(v[0-9]+)/', '', $sRequestUri);

        $aParts = explode('?', $sRequestUri);
        $sRequestUriNoVars = $aParts[0];

        if (in_array($sRequestUri, [
            '/',
            '/rest',
            '/doc/datatypes',
            '/endpoints',
            '/schema',
        ])) {
            $oController = new Controller($aGet, $aPost);
        } else if (preg_match('/^\/rest\/[a-z_]+\/definition$/', $sRequestUriNoVars)) {
            $sClassNamespace = "\\_DefaultApi\\Rest\\V$sVersion\\Definition";
            $oController = new $sClassNamespace($aGet, $aPost);
        } else if (preg_match('/^\/openapi.json$/', $sRequestUri)) {
            $oController = new OpenApiController($aGet, $aPost);
        } else if (preg_match('/^\/openapi-full.json$/', $sRequestUri)) {
            $oController = new OpenApiController($aGet, $aPost);
        } else if (preg_match('/^\/doc\/[a-z_]+$/', $sRequestUriNoVars)) {
            $oController = new ExplainCrud($aGet, $aPost);
        } else if (preg_match('/schema\.(xml|json)$/', $sRequestUriNoVars)) {
            $oController = new Generator($aGet, $aPost);
        } else if (preg_match('/^\/rest\/endpoints\/$/', $sRequestUriNoVars)) {
            $sClassNamespace = "\\_DefaultApi\\Rest\\V$sVersion\\Endpoints";
            $oController = new $sClassNamespace($aGet, $aPost);
        } else if (preg_match('/^\/rest\/[a-z_]+\/$/', $sRequestUriNoVars)) {
            $sClassNamespace = "\\_DefaultApi\\Rest\\V$sVersion\\Collection";
            $oController = new $sClassNamespace($aGet, $aPost);
        } else if (preg_match('/^\/rest\/[a-z_]+\/[a-zA-Z0-9\/]+$/', $sRequestUriNoVars)) {
            $sClassNamespace = "\\_DefaultApi\\Rest\\V$sVersion\\Item";
            $oController = new $sClassNamespace($aGet, $aPost);
        } else if (preg_match('/(logo-.+)/', $sRequestUri)) {
            $oController = new Loader($aGet, $aPost);
        }

        if (!isset($oController) || !$oController instanceof MainController) {
            Header::sendStatusCode(new HttpStatusCode(HttpStatusCode::HTTP_NOT_FOUND));
            echo "Page not found";
            Logger::info('Could not find controller for url ' . $_SERVER['REQUEST_URI']);
            exit();
        }

        return $oController;
    }
}
