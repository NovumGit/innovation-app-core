<?php

namespace _DefaultApi;

use Core\Utils;
use Crud\CrudFactory;
use Crud\FormManager;
use Crud\IApiExposable;

trait CrudManagerTrait
{

    protected function getCrudManager(): FormManager
    {
        $aMatches = [];
        preg_match('/\/v[0-9]+\/rest\/([a-z_]+)/', Utils::getRequestUri(), $aMatches);
        $sEndpoint = $aMatches[1];

        $aCruds = CrudFactory::getAll([IApiExposable::class], new AnyEndpointFilter());

        return $aCruds[$sEndpoint]['crud_manager'];
    }
}
