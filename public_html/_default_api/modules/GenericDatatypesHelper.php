<?php

namespace _DefaultApi;

use Core\Cfg;
use Crud\Generic\InterfaceGenericFieldType;

class GenericDatatypesHelper
{
    public static function getDatatypes()
    {
        $sAbsRoot = Cfg::get('ABSOLUTE_ROOT') . DIRECTORY_SEPARATOR . 'classes';
        $sGenericCrudFieldPath = Cfg::get('ABSOLUTE_ROOT') . '/classes/Crud/Generic/Field';
        $aGenericCrudFields = glob($sGenericCrudFieldPath . '/*');

        $aFields = [];
        foreach ($aGenericCrudFields as $sGenericCrudField) {
            $sBaseName = str_replace(['Generic', '.php'], '', basename($sGenericCrudField));

            $sTmpname = str_replace($sAbsRoot, '', $sGenericCrudField);
            $sTmpname = str_replace('.php', '', $sTmpname);

            $sFqClassName = str_replace(DIRECTORY_SEPARATOR, '\\', $sTmpname);

            if (in_array(InterfaceGenericFieldType::class, class_implements($sFqClassName))) {
                $aFields[] = [
                    'name' => $sBaseName,
                    'fq_class' => $sFqClassName,
                    'description' => $sFqClassName::getApiDescription()
                ];
            }
        }
        return $aFields;
    }
}
