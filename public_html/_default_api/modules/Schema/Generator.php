<?php

namespace _DefaultApi\Schema;

use Core\Cfg;
use Core\Header;
use Core\MainController;
use Core\Mime\JsonMime;
use Core\Mime\XmlMime;
use Generator\Schema\Converter;
use Model\Module\ModuleQuery;
use Model\System\DataModel\Model\DataModelQuery;

class Generator extends MainController
{


    /**
     * @return array|void
     * @throws \Exception
     */
    function run()
    {
        $iApiVersion = getSiteSettings()['api_version'];
        $sProtocol = getSiteSettings()['protocol'];
        $sHostname = $_SERVER['HTTP_HOST'];

        $oUrlHelper = new UrlHelper($sProtocol, $sHostname, $iApiVersion);

        // @todo make this a factory / object oriented.
        if ($this->getRequestUri() == '/v2/schema.json') {
            $sVersion = 'json-schema';
            $sXsdLocation = null;
        } elseif ($this->getRequestUri() === '/v2/schema.xml') {
            $sVersion = 'complete';
            $sXsdLocation = $oUrlHelper->getXsdLocation();
        } elseif ($this->getRequestUri() == '/v2/propel-schema.xml') {
            $sVersion = 'propel';
            $sXsdLocation = $oUrlHelper->getPropelXsdLocation();
        }


        $oModuleQuery = ModuleQuery::create();

        $oDataModelQuery = DataModelQuery::create();
        $sCustom = Cfg::get('CUSTOM_NAMESPACE');

        $oConverter = new Converter($oModuleQuery, $oDataModelQuery, $sCustom);

        if ($sVersion === 'propel') {
            Header::contentType(new XmlMime());
            echo $oConverter->asPropelXml();
        } elseif ($sVersion == 'complete') {
            Header::contentType(new XmlMime());
            echo $oConverter->asXml();
        } else {
            Header::contentType(new JsonMime());
            echo $oConverter->asJson();
        }
        exit();
    }
}
