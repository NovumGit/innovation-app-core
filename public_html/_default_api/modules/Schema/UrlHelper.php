<?php

namespace _DefaultApi\Schema;

class UrlHelper
{
    private $protocol;
    private $host;
    private $api_version;

    public function __construct($protocol, $host, $api_version)
    {
        $this->protocol = $protocol;
        $this->host = $host;
        $this->api_version = $api_version;
    }
    private function getBaseUrl(): string
    {
        return $this->protocol . '://' . $this->host . '/v' . $this->api_version;
    }
    public function getXsdLocation(): string
    {
        return $this->getBaseUrl() . '/schema.xsd';
    }
    public function getXmlLocation(): string
    {
        return $this->getBaseUrl() . '/schema.xml';
    }
    public function getPropelXsdLocation(): string
    {
        return $this->getBaseUrl() . '/propel-schema.xsd';
    }
    public function getPropelXmlLocation(): string
    {
        return $this->getBaseUrl() . '/propel-schema.xml';
    }
    public function getErdLocation(): string
    {
        return $this->getBaseUrl() . '/schema.json';
    }
}
