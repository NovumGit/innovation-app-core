<?php

namespace _DefaultApi;

use Api\Info\IApiInfo;
use Crud\FormManager;

class ApiInfo implements IApiInfo
{

    public function getTitle(): string
    {
        return 'This is the Demo API system, this class should only act as a placeholder';
    }

    public function getDescription(): string
    {
        return 'This is the Demo API system, this class should only act as a placeholder';
    }

    public function getEmail(): string
    {
        return 'sample@xxx.xx';
    }

    public function getOrganisation(): string
    {
        return 'This is the Demo API system, this class should only act as a placeholder';
    }

    public function getPaths(FormManager $oManager)
    {
    }
}
