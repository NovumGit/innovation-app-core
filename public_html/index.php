<?php

use Core\Cfg;
use Core\Csrf;
use Core\Environment;
use Core\IControllerFactory;
use Core\Json\JsonUtils;
use Core\MainController;
use Core\StatusMessage;
use Core\StatusModal;
use Core\Utils;
use Exception\NuiCartException;
use Exception\ClassNotFoundException;
use Model\Logging\Except_log;
use Model\Setting\MasterTable\Language;
use Exception\FileNotFoundException;

/** @noinspection PhpIncludeInspection */
$sVirtualRoot = dirname($_SERVER['SCRIPT_FILENAME'], 5);
$sVirtualSysDir = $sVirtualRoot . '/.system';
$oLoader = require $sVirtualRoot . '/vendor/autoload.php';

header('Access-Control-Allow-Origin: *');

ini_set('zend.enable_gc', 0);
error_reporting(E_ALL);
ini_set('log_errors', 1);
ini_set('error_log', "$sVirtualRoot/data/log/php-errors.log");
ini_set('display_errors', Environment::isDevel());
ini_set('display_errors', true);

if (file_exists("../site-settings.php"))
{
    /** @noinspection PhpIncludeInspection */
    require "../site-settings.php";
} else
{
    throw new LogicException("Missing site-settings.php");
}
$aSiteSettings = JsonUtils::decode(file_get_contents('../site.json'));

$sEnvFileLocation = "$sVirtualSysDir/env/";
if (!file_exists($sEnvFileLocation))
{
    exit("Env file missing " . $sEnvFileLocation);
}
$dotenv = Dotenv\Dotenv::createImmutable($sEnvFileLocation, ".{$aSiteSettings['config_dir']}");
$dotenv->load();

try
{
    $sUrl = $_SERVER['REQUEST_URI'];
    if (strpos($_SERVER['REQUEST_URI'], '?'))
    {
        $sUrl = substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?'));
    }
    if (preg_match('#\/modules(.+)\.js$#', $sUrl))
    {
        header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', 60)); // 60 seconds

        if (file_exists('..' . $sUrl))
        {
            readfile('..' . $sUrl);
        } else
        {
            if (file_exists("$sVirtualRoot/_default/$sUrl"))
            {
                readfile("$sVirtualRoot/_default/$sUrl");
            }
        }
        exit();
    }

    // For some reason _GET variables where gone, perhaps overwritten by htaccess?
    $aGetVars = [];
    if (strpos($_SERVER['REQUEST_URI'], '?'))
    {
        $sGetVarString = substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '?') + 1, strlen($_SERVER['REQUEST_URI']) - strpos($_SERVER['REQUEST_URI'], '?'));
        parse_str($sGetVarString, $aGetVars);
    }
    $_GET = array_merge($aGetVars, $_GET);
    $_REQUEST = array_merge($aGetVars, $_REQUEST);

    session_start();
    $_SESSION['QUERY_COUNT'] = 1;

    $sConfighost = str_replace('.nuidev.nl', '.nl', $_SERVER['HTTP_HOST']);

    $aConfigFiles = [];
    $aSiteSettigs = getSiteSettings();

    if (isset($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == 'http' && $aSiteSettigs['protocol'] == 'https')
    {
        header("HTTP/1.1 301 Moved Permanently");
        header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
        exit();
    }

    MainController::addGlobalTemplateVars(['site_settings' => $aSiteSettigs]);

    $sPropelConfigFile = Utils::makePath($sVirtualSysDir, "config", "{$aSiteSettigs['config_dir']}", "propel", "config.php");
    $aConfig = require $sVirtualSysDir . DIRECTORY_SEPARATOR . "config/{$aSiteSettigs['config_dir']}" . DIRECTORY_SEPARATOR . "config.php";
    Cfg::set($aConfig);

    if (file_exists($sPropelConfigFile))
    {
        require_once $sPropelConfigFile;
    } 
    else
    {
        throw new FileNotFoundException("Missing ORM configuration file, did you install the database? <code>./novum db:make {$aSiteSettigs['config_dir']}</code>");
    }


    $sControllerFactoryClass = '\\' . $aSiteSettigs['namespace'] . '\\ControllerFactory';

    $oControllerFactory = new $sControllerFactoryClass();

    if (!$oControllerFactory instanceof IControllerFactory)
    {
        throw new \Exception\ClassNotFoundException("Controllerfactory missing");
    }

    $oController = $oControllerFactory->getController($_GET, $_POST, $aSiteSettigs['namespace']);

    if (isset($_REQUEST['_do']) && !empty($_REQUEST['_do']))
    {
        $sDoBeforeMethod = 'do' . $_REQUEST['_do'];
        if (method_exists($oController, $sDoBeforeMethod))
        {
            $oController->$sDoBeforeMethod();
        } else
        {
            throw new LogicException("Attempt to do _do method . " . get_class($oController) . " -> $sDoBeforeMethod but does not exist.");
        }
    }
    if (!$oController instanceof MainController)
    {
        Except_log::registerError(E_USER_NOTICE, "Tried to call run on a non existent controller " . $_SERVER['REQUEST_URI'] . " redirecting the user to the home page", __FILE__, __LINE__);
        $oLanguage = Utils::detectWebshopUserLanguage();
        if ($oLanguage instanceof Language)
        {
            Utils::redirect('/' . $oLanguage->getShopUrlPrefix(), 301);
        } else
        {
            Utils::redirect('/', 301);
        }
    }

    // Injecteer extra variablen
    $sControllerInjector = '\\' . $aSiteSettigs['namespace'] . '\\ControllerInjector';
    if (class_exists($sControllerInjector))
    {
        $oInjector = new $sControllerInjector();
        $aViewData = $oInjector->run($oController->run());
    } else
    {
        $aViewData = $oController->run();
    }

    $aViewData['status_messages'] = null;
    if (Core\StatusMessage::hasStatusMessages())
    {
        $aViewData['status_messages'] = StatusMessage::getClearStatusMessages();
    }
    $aViewData['status_modals'] = null;
    if (StatusModal::hasStatusMessages())
    {
        $aViewData['status_modals'] = StatusModal::getClearStatusMessages();
    }
    $aViewData['is_signed_in'] = Core\Customer::isSignedIn();
    if ($aViewData['is_signed_in'])
    {
        $aViewData['signed_in_customer'] = Core\Customer::getMember();
    }

    $aViewData['site_settings'] = getSiteSettings();
    $aViewData['controller'] = $oController;



    $sOut = $oController->parse($oController->getLayout(), $aViewData);

    $sCsrfField = Csrf::getField();
    $sOut = str_replace('</form', $sCsrfField . '</form', $sOut);

    echo $sOut;
}
catch (\Exception $e)
{
    echo $e->getMessage();
    // Except_log::register($e, true);
}



//UniqueVisitorQuery::registerVisitor();
