echo "Starting on tcp port: 73910"
docker run --rm \
--name outway_apinovumdigid \
--volume /home/novum/platform/system/public_html/api.overheid.demo.novum.nu/nlx/root.crt:/certs/root.crt:ro \
--volume /home/novum/platform/system/public_html/api.overheid.demo.novum.nu/nlx/org.crt:/certs/org.crt:ro \
--volume /home/novum/platform/system/public_html/api.overheid.demo.novum.nu/nlx/org.key:/certs/org.key:ro \
--env DIRECTORY_INSPECTION_ADDRESS=directory-inspection-api.demo.nlx.io:443 \
--env TLS_NLX_ROOT_CERT=/certs/root.crt \
--env TLS_ORG_CERT=/certs/org.crt \
--env TLS_ORG_KEY=/certs/org.key \
--env DISABLE_LOGDB=1 \
--publish 74020:8080 \
nlxio/outway:latest