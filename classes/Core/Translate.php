<?php
namespace Core;

use Crud\CrudElement;
use Exception\FileException;
use Exception\InvalidArgumentException;
use Exception\LogicException;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;
use Model\Account\User as UserModel;
use Core\User as CoreUser;

class Translate
{
	static $aAllLanguages = false;
	static $sDefaultLanguage = false;
	static $sCookieAlreadyStored = false;

	public static function storeLanguageIdInCookie($iLanguageId)
	{
		if (!self::$sCookieAlreadyStored) {
			// This check prevents ERR_RESPONSE_HEADERS_TOO_BIG error in Chrome.
			setcookie("prefered_language_id", $iLanguageId, time() + 2628000); // 1 maand, te gebruiken in het inlog venster.
			self::$sCookieAlreadyStored = true;
		}
	}

	public static function getCurrentLocale($sReturn = 'LocaleCode')
	{
		$oCurrentUser = CoreUser::getMember();

		if (!isset($_SESSION['cached_default_language']) || empty($_SESSION['cached_default_language'])) {
			if (Environment::getCurrent() == 'admin') {
				$_SESSION['cached_default_language'] = LanguageQuery::create()->findOneByIsDefaultCms(true);
			}
			else {
				if (Environment::getCurrent() == 'frontend') {
					$_SESSION['cached_default_language'] = LanguageQuery::create()->findOneByIsDefaultWebshop(true);
				}
				else {
					if (Environment::getCurrent() == 'commandline') {
						$_SESSION['cached_default_language'] = LanguageQuery::create()->findOneByShopUrlPrefix('nl');
					}
					else {
						throw new LogicException("Could not detect environment");
					}
				}
			}
		}
		if ($oCurrentUser instanceof UserModel && $oCurrentUser->getPreferedLanguageId()) {
			$iLanguageId = $oCurrentUser->getPreferedLanguageId();
			self::storeLanguageIdInCookie($iLanguageId);
		}
		else
		{
			if (isset($_COOKIE['prefered_language_id'])) {

				$iLanguageId = $_COOKIE['prefered_language_id'];
			}
			else {
				if (isset($_SESSION['cached_default_language']) && $_SESSION['cached_default_language'] instanceof Language) {
					$iLanguageId = $_SESSION['cached_default_language']->getId();
				}
				else {
					if ($oEnglishLangauge = LanguageQuery::create()->findOneByLocaleCode('en_US')) {
						$iLanguageId = $oEnglishLangauge->getId();
					}
					else {
						if ($oDutchLangauge = LanguageQuery::create()->findOneByLocaleCode('nl_NL')) {
							$iLanguageId = $oDutchLangauge->getId();
						}
						else {
							throw new LogicException("No default langauge and no fallback language is found. Are there any languages installed?");
						}
					}
				}
			}
		}


		$oLanguage = LanguageQuery::getByIdCached($iLanguageId);

		if (!$oLanguage instanceof Language) {
			unset($_COOKIE['prefered_language_id']);

			if(!self::$sDefaultLanguage instanceof Language)
			{
				self::$sDefaultLanguage = LanguageQuery::create()->findOneByLocaleCode('en_US');
			}
			$oLanguage = self::$sDefaultLanguage;
		}
		if ($sReturn == 'LocaleCode') {
			return $oLanguage->getLocaleCode();
		}
		else {
			if ($sReturn == 'Id') {
				return $oLanguage->getId();
			}
			else {
				if ($sReturn == 'FlagIconPath') {
					return $oLanguage->getFlagIconPath();
				}
				else {
					throw new InvalidArgumentException("Expected LocaleCode or Id as argument in " . __METHOD__);
				}
			}
		}
	}

	public static function setCurrentLocale($sLocaleCode)
	{
		$_SESSION['CURRENT_LOCALE'] = $sLocaleCode;
	}

	public static function crudField($sString, CrudElement $oCrudField)
	{
		$sLocaleCode = self::getCurrentLocale();
		$oReflector = new \ReflectionClass($oCrudField);
		$aDirParts = explode(DIRECTORY_SEPARATOR, $oReflector->getFileName());
		$aLocaleDir = [];
		foreach ($aDirParts as $sDirParth) {
			if ($sDirParth == 'Field') {
				break;
			}
			else {
				$aLocaleDir[] = $sDirParth;
			}
		}
		$sTranslationDir = join(DIRECTORY_SEPARATOR, $aLocaleDir) . '/Locales';
		$sTranslationFile = $sTranslationDir . DIRECTORY_SEPARATOR . $sLocaleCode . '.json';
		if (!file_exists($sTranslationFile)) {
			return $sString;
		}
		$sTranslationJson = file_get_contents($sTranslationFile);
		$aTranslation = json_decode($sTranslationJson, true);
		// Als de vertaling niet bestaat dan controleren we direct alle vertaalbestanden en voegen overal het woord toe,
		if (!isset($aTranslation[$sString])) {
			$aOtherTranslationFiles = glob($sTranslationDir . '.*');
			if (!empty($aOtherTranslationFiles)) {
				foreach ($aOtherTranslationFiles as $sOtherTranslationFile) {
					if (basename($sOtherTranslationFile) == '.' || basename($sOtherTranslationFile) == '..') {
						continue;
					}
					if (preg_match('/\.json$/', $sOtherTranslationFile)) {
						$sOtherLanguageTranslationjson = file_get_contents($sOtherTranslationFile);
						$aOtherLanguageTranslation = json_decode($sOtherLanguageTranslationjson, true);
						if (!isset($aOtherLanguageTranslation[$sString])) {
							$aOtherLanguageTranslation[$sString] = $sString;
							$aOtherLanguageTranslation['all_translated'] = false;
						}
						$sOtherLanguageTranslationjson = json_encode($aOtherLanguageTranslation);
						file_put_contents($sOtherTranslationFile . '.tmp', $sOtherLanguageTranslationjson);
						unlink($sOtherTranslationFile);
						rename($sOtherTranslationFile . '.tmp', $sOtherTranslationFile);
					}
				}
			}

			return $sString;
		}

		return $aTranslation[$sString];
	}

	public static function fromCode($sString)
	{
		$aBacktrace = debug_backtrace();
		$sPhpFile = dirname($aBacktrace[0]['file']);
		$sTranslationFile = null;

		if(strpos($sPhpFile, '/Crud/') && preg_match('/Field$/', $sPhpFile))
        {
            $sTranslationFile = dirname($sPhpFile) . '/Locales/' . Translate::getCurrentLocale() . '.json';
        }
		else if (strpos($sPhpFile, '/classes/'))
		{
			$bFoundTranslationFolder = false;
			foreach ($aBacktrace as $aBacktraceItem) {
				if (isset($aBacktraceItem['file']) && strpos($aBacktraceItem['file'], 'admin_modules')) {
					$bFoundTranslationFolder = true;
					$sAdminModulesFolder = substr($aBacktraceItem['file'], 0, strpos($aBacktraceItem['file'], 'admin_modules/') + strlen('admin_modules/'));
					$sTranslationFile = $sAdminModulesFolder . 'Locales/' . Translate::getCurrentLocale() . '.json';
				}
			}
			if (!$bFoundTranslationFolder) {
				throw new LogicException("Translate function seems to be called from the classes folder and now we don't know where to look for the translation file. Please fix this issue.");
			}
		}
		else {
			$sTranslationFile = $sPhpFile . '/Locales/' . Translate::getCurrentLocale() . '.json';

			if (Environment::getCurrent() == 'frontend') {
				$oReflector = new \ReflectionFunction('getSiteSettings');
				$sSiteFolder = basename(dirname($oReflector->getFileName()));
				$sTranslationFile = str_replace('/_default/', '/' . $sSiteFolder . '/', $sTranslationFile);
			}
		}
		if ($sTranslationFile == null) {
			throw new LogicException("No translationfile found");
		}

		$sString = self::getOrCreateTranslation($sString, $sTranslationFile);

		return $sString;
	}

	private static function getOrCreateTranslation($sString, $sTranslationFile)
	{
		/*
		 * Dit stukje zorgt ervoor dat module submappen geen eigen Locales folder krijgen.
		 * De vertalingen worden per module map opgeslagen. (bijvoorbeeld bij admin_modules/Setting zijn veel submappen
		 */
		$aTranslationFileParts = explode('/', $sTranslationFile);
		$aBeforeAdminModules = [];
		$aAfterAdminModules = [];
		$bKeepFromNow = false;
		$bLocalesStarted = false;
		$iCurrDepthAfterAdminModules = 0;
		foreach ($aTranslationFileParts as $sTranslationFilePart) {
			if ($sTranslationFilePart == 'admin_modules' || $bKeepFromNow) {
				$bKeepFromNow = true;
				if ($sTranslationFilePart == 'Locales') {
					$bLocalesStarted = true;
				}
				if ($iCurrDepthAfterAdminModules <= 1 || $bLocalesStarted) {
					$aAfterAdminModules[] = $sTranslationFilePart;
					$iCurrDepthAfterAdminModules++;
				}
			}
			else {
				$aBeforeAdminModules[] = $sTranslationFilePart;
			}
		}
		$aPathParts = array_merge($aBeforeAdminModules, $aAfterAdminModules);
		$sTranslationFile = join('/', $aPathParts);
		if (!is_dir(dirname($sTranslationFile))) {
			mkdir(dirname($sTranslationFile), 0777, true);
		}
		if (!file_exists($sTranslationFile)) {
			touch($sTranslationFile);
		}
		if (!is_writable($sTranslationFile)) {
			throw new FileException("File not writable: $sTranslationFile");
		}
		$sTranslation = file_get_contents($sTranslationFile);
		$aTranslation = [];
		if (!empty($sTranslation)) {
			$aTranslation = json_decode($sTranslation, true);
		}
		if (isset($aTranslation[$sString])) {
			$sString = $aTranslation[$sString];
		}
		else {
			// Als de vertaling niet bestaat voor deze taal, dan controleren we gelijk alle andere talen en voegen hem ook daar toe.
			$aPossibleOtherLanguageTranslationFiles = glob(dirname($sTranslationFile) . '/*');
			if (!empty($aPossibleOtherLanguageTranslationFiles)) {
				foreach ($aPossibleOtherLanguageTranslationFiles as $aPossibleOtherLanguageTranslationFile) {
					if (preg_match('/\.json$/', $aPossibleOtherLanguageTranslationFile)) {
						$sOtherTranslationJson = file_get_contents($aPossibleOtherLanguageTranslationFile);
						$aOtherTranslationJson = json_decode($sOtherTranslationJson, true);
						if (!isset($aOtherTranslationJson[$sString])) {
							$aOtherTranslationJson['all_translated'] = false;
							$aOtherTranslationJson[$sString] = $sString;
							file_put_contents($aPossibleOtherLanguageTranslationFile . '.tmp', json_encode($aOtherTranslationJson));
							rename($aPossibleOtherLanguageTranslationFile, $aPossibleOtherLanguageTranslationFile . '.prev');
							rename($aPossibleOtherLanguageTranslationFile . '.tmp', $aPossibleOtherLanguageTranslationFile);
							unlink($aPossibleOtherLanguageTranslationFile . '.prev');
						}
					}
				}
			}
			// We voegen een woord toe aan het vertaal bestand, daarom markeren we hem ook gelijk weer als niet volledig vertaald.
			$aTranslation['all_translated'] = false;
			$aTranslation[$sString] = $sString;
			file_put_contents($sTranslationFile, json_encode($aTranslation));
		}

		return $sString;
	}

	public static function string($sString, $sRootPath)
	{
		if (!isset($_SESSION['TRANSLATION'][$sRootPath][$sString])) {
			$sTranslationFile = $sRootPath . '/Locales/' . self::getCurrentLocale() . '.json';
			$_SESSION['TRANSLATION'][$sRootPath][$sString] = self::getOrCreateTranslation($sString, $sTranslationFile);
		}

		return $_SESSION['TRANSLATION'][$sRootPath][$sString];
	}
}
