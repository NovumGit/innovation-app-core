<?php
namespace Core;

class Layout
{
    private static $show_navbar_top = null;
    private static $show_sidebar_left = null;

    static function setNavTopVisibility(bool $bVisible = false)
    {
        self::$show_navbar_top = $bVisible;
    }
    static function setSidebarLeftVisibility(bool $bVisible = false)
    {
        self::$show_sidebar_left = $bVisible;
    }

    static function isNavTopVisible():bool
    {
        if(self::$show_navbar_top !== null)
        {
            return self::$show_navbar_top;
        }
        return Setting::get('show_navbar_top', true);
    }
    static function isSidebarLeftVisible():bool
    {
        if(self::$show_sidebar_left !== null)
        {
            return self::$show_sidebar_left;
        }
        return Setting::get('show_sidebar_left', true);

    }
}
