<?php

namespace Core;

use Bluerhinos\phpMQTT;
use Core\Asynchronous\IActionHandler;
use Exception\LogicException;
use Ramsey\Uuid\Uuid;

class Mqtt
{
    private static $sServer = null;
    private static $sUser = null;
    private static $sPass = null;
    private static $sPort = null;

    static function setConfig(string $sServer, string $sUser,string $sPass, int $sPort = 1883):void
    {
        self::$sServer = $sServer;
        self::$sUser = $sUser;
        self::$sPass = $sPass;
    }

    /**
     * @param string $sTopic
     * @param $callback
     * @throws \Exception
     */
    static function subscribe(string $sTopic, $callback)
    {
        $iClientId = Uuid::uuid4(); // make sure this is unique for connecting to sever - you could use uniqid()

        $oMqtt = new phpMQTT(self::$sServer ?? Cfg::get('MQTT_SERVER'), self::$sPort ?? Cfg::get('MQTT_PORT'), $iClientId);
        if (!$oMqtt->connect(true, NULL, self::$sUser ?? Cfg::get('MQTT_USERNAME'), self::$sPass ?? Cfg::get('MQTT_PASSWORD')))
        {
            exit(1);
        }

        $topics[$sTopic] = array("qos" => 0, "function" => $callback);
        $oMqtt->subscribe($topics, 0);
        while ($oMqtt->proc()) {

        }
        $oMqtt->close();
    }

    /**
     * @param string $sTopic
     * @param string $sMessage
     * @throws \Exception
     */
    static function publish(string $sTopic, string $sMessage)
    {
        // cloudmanager
        // Tsmakosrss2019! (This system manages all kinds of server related stuff since 2019!)
        $iClientId = Uuid::uuid4();

        $oMqtt = new phpMQTT(self::$sServer ?? Cfg::get('MQTT_SERVER'), self::$sPort ?? Cfg::get('MQTT_PORT'), $iClientId);

        if ($oMqtt->connect(true, NULL, self::$sUser ?? Cfg::get('MQTT_USERNAME'), self::$sPass ?? Cfg::get('MQTT_PASSWORD'))) {
            $oMqtt->publish($sTopic, $sMessage, 0);
            $oMqtt->close();
        } else {
            throw new LogicException('Connection error');
        }
    }

}
