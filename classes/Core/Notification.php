<?php
namespace Core;

use Model\System\NotificationAction;
use Model\System\NotificationLevel;
use Model\System\NotificationLevelQuery;
use Model\System\NotificationType;
use Model\System\NotificationTypeQuery;

class Notification
{
    /**
     * @param string $sMessage
     * @param string $sLevel [success|danger|warning|info]
     * @param string $sType - Make this a string that you chose your self, such as "Sale order created".
     * @param StatusMessageButton[] $aActions = null
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \LogicException
     */
    static function register($sLevel, $sType, $sMessage, $aActions = null)
    {

        $oNotification = new \Model\System\Notification();

        $oNotificationTypeQuery = NotificationTypeQuery::create();
        $oNotificationType = $oNotificationTypeQuery->findOneByName($sType);

        if(!$oNotificationType instanceof NotificationType)
        {
            $oNotificationType = new NotificationType();
            $oNotificationType->setName($sType);
            $oNotificationType->save();
        }
        $oNotification->setNotificationTypeId($sLevel);


        $oNotificationLevelQuery = NotificationLevelQuery::create();
        $oNotificationLevel = $oNotificationLevelQuery->findOneByName($sLevel);

        if(!$oNotificationLevel instanceof NotificationLevel)
        {
           throw new \LogicException("Notification level must be one of success, danger, warning or info.");
        }
        $oNotification->setMessage($sMessage);
        $oNotification->setNotificationLevelId($oNotificationLevel->getId());
        $oNotification->setCreatedOn(time());

        $oNotification->save();

        if(!empty($aActions))
        {
            foreach ($aActions as $oAction)
            {
                $oNotificationAction = new NotificationAction();
                $oNotificationAction->setLabel($oAction->getLabel());
                $oNotificationAction->setUrl($oAction->getAction());
                $oNotificationAction->setNotificationId($oNotification->getId());
                $oNotificationAction->save();
            }
        }
    }
}