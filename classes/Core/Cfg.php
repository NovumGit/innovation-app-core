<?php
namespace Core;

use Hi\Helpers\DirectoryStructure;

class Cfg{

    static $config;

    /**
     * Stel de configuratie array in.
     * @param $config array, een array met alle instellingen voor de applicatie.
     */
    static function set($config){
        if(is_array(self::$config)){
            self::$config = array_merge(self::$config,$config);
        }else{
            self::$config = $config;
        }
    }
    /**
     * Haal een of meerdere configuratie instellingen op.
     * @param $param 1, key uit de (multidimentionele) array
     * @param $param 2, key uit de (multidimentionele) array
     * @param $etc gaat onbeperkt door.
     */
    static function get(){
        $args 		= func_get_args();
        $num_args 	= func_num_args();

        if($num_args === 1 && $args[0] === 'ABSOLUTE_ROOT')
        {
            $oDirectoryStructure = new DirectoryStructure();
            return $oDirectoryStructure->getSystemDir(true);
        }

        if($num_args>3){
            echo "Teveel argumenten CFG klasse";
            return false;
        }
        else if($num_args==1)
        {
            if(isset(self::$config[$args[0]]))
            {
                return self::$config[$args[0]];
            }
            else if(isset($_SERVER['HURAH_' . $args[0]]))
            {
                return $_SERVER['HURAH_' . $args[0]];
            }
            return null;

        }
        else if($num_args==2)
        {
            return self::$config[$args[0]][$args[1]] ?? $_SERVER['HURAH_' . $args[0]]['HURAH_' . $args[1]];
        }
        else if($num_args==3)
        {
            return self::$config[$args[0]][$args[1]][$args[2]] ?? $_SERVER['HURAH_' . $args[0]]['HURAH_' . $args[1]]['HURAH_' . $args[2]];
        }
    }
}
