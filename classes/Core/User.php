<?php
namespace Core;

use Exception\LogicException;
use Model\Account\User as UserModel;
use Model\Account\UserQuery;
use Model\Setting\MasterTable\Role;

class User{

    private static $prefetchedUser;
    static function logout()
    {
        unset($_SESSION['member']);
        unset($_SESSION['password_decrypt']);
        unset($_SESSION);

    }
    static function setMember(UserModel $oUser = null)
    {
        if($oUser)
        {
            $_SESSION['member'] = $oUser->toArray();
        }
        else
        {
            $_SESSION['member'] = null;
        }
    }
    static function getOriginalRole():?Role
    {
        if(!isset($_SESSION['member']))
        {
            return null;
        }
        if(isset($_SESSION['member']['original_role']))
        {
            if(!$_SESSION['member']['original_role'] instanceof Role)
            {
                throw new LogicException("Original role is set but it is not an instance of Role, this indicates a bug.");
            }
            return $_SESSION['member']['original_role'];
        }
        return self::getRole();
    }
    static function getRole():Role
    {
        if(isset($_SESSION['member']['temp_role']))
        {
            return $_SESSION['member']['temp_role'];
        }
        if(self::getMember() instanceof \Model\Account\User)
        {
            return self::getMember()->getRole();
        }
        return new Role();
    }
    static function switchRole(Role $oNewRole)
    {
        if(!isset($_SESSION['member']))
        {
            throw new LogicException("User is not signed in, please check this first, this indicates a bug.");
        }

        if(!isset($_SESSION['member']['original_role']))
        {
            $_SESSION['member']['original_role'] = self::getRole();
        }
        $_SESSION['member']['temp_role'] = $oNewRole;
    }

    static function getMember()
    {
        if(!isset($_SESSION['member']) || empty($_SESSION['member']))
        {
            return null;
        }

        if(!self::$prefetchedUser)
        {
            self::$prefetchedUser = UserQuery::create()->findOneById($_SESSION['member']['Id']);;
        }

        return self::$prefetchedUser;
    }
    /**
     * @return mixed
     */
    function getCurrentSignedInUser()
    {
        if(!isset($_SESSION['member']) || empty($_SESSION['member']))
        {
            return false;
        }
        return $_SESSION['member'];
    }

    static function isSignedIn()
    {
        if(!isset($_SESSION['member']) || empty($_SESSION['member']))
        {
            return false;
        }
        return true;
    }
    static function makeSalt(){
        return substr(md5(time().'#edf23'), 0, 25);
    }
    static function makeEncryptedPass($sUnEncryptedPassword, $sSalt){
        return hash('sha256', $sSalt.$sUnEncryptedPassword);
    }
}
