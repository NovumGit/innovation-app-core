<?php
namespace Core;

use Exception\LogicException;
use ZipArchive;

class ZipDir
{
    private static $aZipFiles = null;
    private static $aZipStrings = null;

    static function addFile($sLocalPath, $sInZipPath)
    {
        if(!file_exists($sLocalPath))
        {
            throw new LogicException("File does not exist $sLocalPath");
        }
        self::$aZipFiles[$sLocalPath] = $sInZipPath;
    }

    static function addFromString($sInZipPath, $sString)
    {
        self::$aZipStrings[$sInZipPath] = $sString;
    }
    static function reset()
    {
        self::$aZipFiles = null;
        self::$aZipStrings = null;
    }
    static function download($sOutputFile = 'file.zip')
    {
        $sDataDir = Config::getDataDir().'/tmp/download';

        if(!is_dir($sDataDir))
        {
            mkdir($sDataDir);
        }
        $sTmpFileLocation  = $sDataDir.'/'.$sOutputFile;

        $oZipFile = new ZipArchive();
        $oZipFile->open($sTmpFileLocation, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        if(empty(self::$aZipFiles))
        {
            throw new LogicException("Cannot create zip archive as there are no files to zip.");
        }

        if(!empty(self::$aZipFiles))
        {
            foreach(self::$aZipFiles as $sRealLocation => $sInZipLocation)
            {
                $oZipFile->addFile($sRealLocation, $sInZipLocation);
            }
        }
        if(!empty(self::$aZipStrings))
        {
            foreach(self::$aZipStrings as $sInZipLocation => $sString)
            {
                $oZipFile->addFromString($sInZipLocation, $sString);
            }
        }

        $oZipFile->close();
        //then send the headers to force download the zip file

        header("Content-type: application/zip");
        header("Content-Disposition: attachment; filename=$sOutputFile");
        header("Pragma: no-cache");
        header("Expires: 0");

        sleep(1);
        readfile($sTmpFileLocation);
        unlink($sTmpFileLocation);
    }
}