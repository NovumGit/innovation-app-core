<?php
namespace Core;

use Cassandra\Uuid;
use Exception\LogicException;
use Twig_Loader_Filesystem;
use Twig_Environment;
use Twig_Extension_Debug;


class TemplateFactory{

    /**
     * @param string $sTwigHtml
     * @param $aData
     * @return false|string
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    public static function parseFromString(string $sTwigHtml, $aData)
    {
        $oTwig = self::create();
        $oTemplate = $oTwig->createTemplate($sTwigHtml);
        return $oTemplate->render($aData);
    }
    /**
     * @param Twig_Environment $oTwig
     * @param $sTemplate
     * @param $aData
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public static function parse($sTemplate, array $aData = null):string
    {
        $oTwig = self::create();

        $aData['magic_arguments'] = [];
        $aData['magic_arguments']['current_template'] = $sTemplate;
        $aData['magic_arguments']['current_url'] = Utils::getRequestUri();
        $aData['magic_arguments']['request_uri'] = Utils::getRequestUri(false);

        $sTemplateFolder = '/'.dirname($sTemplate);

        if($sTemplateFolder == '/.')
        {
            $sTemplateFolder = '';
        }

        $sModuleDir = Environment::getCurrent() == 'frontend' ? 'Modules' : 'admin_modules';
        $aData['magic_arguments']['is_devel'] = (isset($_SERVER['IS_DEVEL']));
        $aData['magic_arguments']['current_template_dir'] = "../$sModuleDir".$sTemplateFolder;
        $aData['magic_arguments']['custom'] = Config::getCustom();
        $aData['magic_arguments']['assets'] = Config::getAssetsDir();
        $aData['magic_arguments']['rand'] = \Ramsey\Uuid\Uuid::uuid4();

        $oTwig = TemplateFactory::addTranslateFilter($oTwig);
        $oTwig = TemplateFactory::addBase64Embed($oTwig);
        $oTwig = TemplateFactory::addSlugify($oTwig);

        return $oTwig->render($sTemplate, $aData);
    }
	public static function createFromDirs(array $aDirectories):Twig_Environment
	{
		$oTwigLoader = new Twig_Loader_Filesystem($aDirectories);
		$aTwigConfig = array('cache' => Config::getDataDir(true).'/tmp/twig', 'debug' => true);

		$oTwig = new Twig_Environment($oTwigLoader, $aTwigConfig);
		$oTwig->addExtension(new Twig_Extension_Debug());
		return $oTwig;
	}
    public static function create():Twig_Environment
    {
        $aTemplatesDirs = [];
        $aTemplatesDirs[] = '../'. Environment::getModuleDir();
        $aTemplatesDirs[] = '../';


        if(Environment::getModuleDir() ==  'admin_modules')
        {
            $aTemplatesDirs[] = '../classes';
        }
        else
        {
            $sAbsolutePublicRoot = dirname($_SERVER['SCRIPT_FILENAME'], 3);
            $aTemplatesDirs[] = "{$sAbsolutePublicRoot}/_default/modules";
            $aTemplatesDirs[] = "{$sAbsolutePublicRoot}/_default/";
            $aTemplatesDirs[] = "{$sAbsolutePublicRoot}/_default_api";
            $aTemplatesDirs[] = "{$sAbsolutePublicRoot}/_default_api/modules";
        }
        return self::createFromDirs($aTemplatesDirs);
    }

    static function addTranslateFilter(Twig_Environment $oTwig)
    {
        $oFilter = new \Twig_SimpleFilter('translate', function (Twig_Environment $oTwig, $oContext, $sString) {
            // $oTwigLoader = $oTwig->getLoader();
            $sModuleName = null;
            if(!isset($oContext['magic_arguments']))
            {
                throw new LogicException(__METHOD__.' needs "magic_arguments", to figure out the Locale dir');
            }
            $sCurrentTemplateDir = $oContext['magic_arguments']['current_template_dir'];

            if(Environment::getCurrent() == 'frontend')
            {
                $oReflector = new \ReflectionFunction('getSiteSettings');
                $sSiteFolder = basename(dirname($oReflector->getFileName()));
                $sCurrentTemplateDir = str_replace('../modules', '../../'.$sSiteFolder.'/modules', $sCurrentTemplateDir);
            }
            return Translate::string($sString, $sCurrentTemplateDir);
        },['needs_context' => true, 'needs_environment' => true]);
        $oTwig->addFilter($oFilter);
        return $oTwig;
    }

    public static function addSlugify(Twig_Environment $oTwig)
    {
        $oFilter = new \Twig_SimpleFilter('slugify', function (Twig_Environment $oTwig, $oContext, $sString) {

            return Utils::slugify($sString);

            /*
            // $oTwigLoader = $oTwig->getLoader();

            $sModuleName = null;
            if(!isset($oContext['magic_arguments']))
            {
                throw new LogicException(__METHOD__.' needs "magic_arguments", to figure out the Locale dir');
            }

            return Translate::string(, $oContext['magic_arguments']['current_template_dir']);
            */
        },['needs_context' => true, 'needs_environment' => true]);

        $oTwig->addFilter($oFilter);
        return $oTwig;
    }
    public static function addBase64Embed(Twig_Environment $oTwig)
    {
        $oFilter = new \Twig_SimpleFilter('base_64_embed', function (Twig_Environment $oTwig, $oContext, $sString) {

            // return $sString;
            $sType = pathinfo($sString, PATHINFO_EXTENSION);

            $aContextOptions = [
                "ssl" => [
                    "verify_peer"=>false,
                    "verify_peer_name"=>false
                ],
            ];
            $rContext = stream_context_create($aContextOptions);


            $sBinary = file_get_contents($sString, false, $rContext);

            if($sBinary)
            {
                $sBase64 = 'data:image/' . $sType . ';base64,' . base64_encode($sBinary);
                return $sBase64;
            }
            return $sString;
            /*
            // $oTwigLoader = $oTwig->getLoader();

            $sModuleName = null;
            if(!isset($oContext['magic_arguments']))
            {
                throw new LogicException(__METHOD__.' needs "magic_arguments", to figure out the Locale dir');
            }

            return Translate::string(, $oContext['magic_arguments']['current_template_dir']);
            */
        },['needs_context' => true, 'needs_environment' => true]);

        $oTwig->addFilter($oFilter);
        return $oTwig;
    }
}
