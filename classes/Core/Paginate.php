<?php
namespace Core;

use Exception\LogicException;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Util\PropelModelPager;

class Paginate{
    private $oPager = null;
    private $oQueryObject = null;
    private $iCurrentPage;
    private $iItemsPP;

    function __construct(ModelCriteria $oQueryObject, $iCurrentPage, $iItemsPP)
    {
        if(!$oQueryObject instanceof ModelCriteria)
        {
            throw new LogicException("QueryObject must implement ModelCriteria because we need paginate over here...");
        }
        $this->oQueryObject = $oQueryObject;
        $this->oPager = $oQueryObject->paginate($iCurrentPage, $iItemsPP);
        $this->iCurrentPage = $iCurrentPage;
        $this->iItemsPP = $iItemsPP;
    }

    function getPaginateHtml($iMaxPages = 10, $sBaseUrl = 'current_url')
    {
        if($sBaseUrl == 'current_url')
        {
            $aParsed = parse_url($_SERVER['REQUEST_URI']);
            if(isset($aParsed['query']))
            {
                parse_str($aParsed['query'], $aParts);
                if(isset($aParts['p']))
                {
                    unset($aParts['p']);
                }
            }

            if(isset($aParts) && !empty($aParts))
            {
                $sQuery = '?'.http_build_query($aParts);
            }
            else
            {
                $sQuery = '';
            }

            $sNewUrl = $aParsed['path'].$sQuery;
        }
        if(!$this->oPager instanceof PropelModelPager)
        {
            throw new LogicException("QueryObject must implement ModelCriteria because we need paginate over here...");
        }
        $aPages = $this->oPager->getLinks($iMaxPages);

        if(empty($aPages))
        {
            return '';
        }

        $aPaginateHtml = [];
        $aPaginateHtml[] = '    <div class="dt-panelfooter clearfix">';

        $sConcat = strpos($sNewUrl, '?') ? '&' : '?';
        $aPaginateHtml[] = '        <div class="dataTables_info" id="datatable_info" role="status" aria-live="polite">Showing '.$this->oPager->getFirstIndex().' to '.$this->oPager->getLastIndex().' of '.$this->oPager->getNbResults().' entries</div>';

        if(count($aPages) > 1)
        {
            $aPaginateHtml[] = '            <div class="dataTables_paginate paging_simple_numbers" id="datatable_paginate">';
            $aPaginateHtml[] = '                <ul class="pagination">';

            if($this->oPager->getPage() == 1 )
            {
                $aPaginateHtml[] = '                    <li class="paginate_button previous disabled" aria-controls="datatable" tabindex="0" id="datatable_previous">';
                $aPaginateHtml[] = '                        <a href="#">First</a>';
                $aPaginateHtml[] = '                    </li>';

                $aPaginateHtml[] = '                    <li class="paginate_button previous disabled" aria-controls="datatable" tabindex="0" id="datatable_previous">';
                $aPaginateHtml[] = '                        <a href="#">Previous</a>';
                $aPaginateHtml[] = '                    </li>';
            }
            else
            {

                $aPaginateHtml[] = '                    <li class="paginate_button previous" aria-controls="datatable" tabindex="0" id="datatable_previous">';
                $aPaginateHtml[] = '                        <a href="'.$sNewUrl.$sConcat.'p=1">First</a>';
                $aPaginateHtml[] = '                    </li>';

                $aPaginateHtml[] = '                    <li class="paginate_button previous" aria-controls="datatable" tabindex="0" id="datatable_previous">';
                $aPaginateHtml[] = '                        <a href="'.$sNewUrl.$sConcat.'p='.$this->oPager->getPreviousPage().'">Previous</a>';
                $aPaginateHtml[] = '                    </li>';
            }
            foreach($aPages as $iPage)
            {
                $sActiveClass = $this->oPager->getPage() == $iPage ? 'active' : '';
                $aPaginateHtml[] = '                    <li class="paginate_button '.$sActiveClass.'" aria-controls="datatable" tabindex="0">';


                $aPaginateHtml[] = '                        <a href="'.$sNewUrl.$sConcat.'p='.$iPage.'">'.$iPage.'</a>';
                $aPaginateHtml[] = '                    </li>';
            }

            $sDisabled = $this->oPager->getLastPage() == $iPage ? ' disabled' : '';
            $aPaginateHtml[] = '                    <li class="paginate_button next' . $sDisabled . '" aria-controls="datatable" tabindex="0" id="datatable_next">';
            $aPaginateHtml[] = '                        <a href="'.$sNewUrl.$sConcat.'p='.$this->oPager->getNextPage().'">Next</a>';
            $aPaginateHtml[] = '                    </li>';

            $aPaginateHtml[] = '                    <li class="paginate_button previous' . $sDisabled . '" aria-controls="datatable" tabindex="0" id="datatable_previous">';
            $aPaginateHtml[] = '                        <a href="'.$sNewUrl.$sConcat.'p='.$this->oPager->getLastPage().'">Last</a>';
            $aPaginateHtml[] = '                    </li>';

            $aPaginateHtml[] = '                </ul>';
            $aPaginateHtml[] = '            </div>';
        }
        $aPaginateHtml[] = '        </div>';

        $aPaginateHtml[] = '    </div>';
        return join(PHP_EOL, $aPaginateHtml);
    }

    function getDataIterator()
    {
        return $this->oPager;
    }
}