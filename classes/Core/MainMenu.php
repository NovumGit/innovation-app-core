<?php
namespace Core;

use AdminModules\ModuleConfig;

class MainMenu{

    static $allRegistered = false;

    static function get()
    {
        if(!self::$allRegistered)
        {
            // MainMenu::get is called several times during pageload.
            Module::registerAll();
            self::$allRegistered = true;
        }


        $aMenuBlocks = [];
        if(User::isSignedIn())
        {
            $oCurrentUser = User::getMember();

            $aModules = Module::getEnabledModules(User::getRole()->getId());

            $oTwig = TemplateFactory::create();
            $oTwig = TemplateFactory::addTranslateFilter($oTwig);

            if(!empty($aModules))
            {
                $i = 1;
                foreach($aModules as $oModule)
                {
                    $aExtraVars = [];
                    if($oModule instanceof ModuleConfig)
                    {
                        $aExtraVars = $oModule->getMenuExtraVars();
                    }

                    $aMenuData = [];
                    if(method_exists($oModule, 'getMenuData'))
                    {
                        $aMenuData = $oModule->getMenuData();
                    }

                    $aModulePathParts  = explode("\\", get_class($oModule));
                    $sModuleName = $aModulePathParts[1];


                    if($sModuleName == 'Custom')
                    {
                        $sModuleName = $aModulePathParts[1]."/".$aModulePathParts[2]."/".$aModulePathParts[3];
                    }

                    foreach(['custom-menu.twig', 'menu.twig'] as $sTryFile)
                    {
                        $sPossibleMenuFileName = $sModuleName . '/' . $sTryFile;
                        $sRelativePossibleModuleDirName = '../admin_modules/'.$sModuleName;

                        if(is_dir($sRelativePossibleModuleDirName) && file_exists($sRelativePossibleModuleDirName . '/' . $sTryFile))
                        {
                            $sMenuFileName = $sPossibleMenuFileName;
                            $aTemplateArgs =
                                [
                                    'current_user' => User::getMember(),
                                    'extra_vars' => $aExtraVars,
                                    'module_name' => $sModuleName,
                                    'menu_state' => UserRegistry::get('menu_state_'.$sModuleName) == 'open' ? 'menu-open' : '',
                                    'passed_data' => $aMenuData,
                                    'magic_arguments' => [
                                        'current_template' => $sMenuFileName,
                                        'current_template_dir' => '../admin_modules/'.$sModuleName
                                    ]
                                ];
                            $aMenuBlocks[$i] = ['html' => $oTwig->render($sMenuFileName, $aTemplateArgs)];
                            $i++;
                            break;
                        }
                    }
                }
            }
            return $aMenuBlocks;
        }
        return null;
    }
}
