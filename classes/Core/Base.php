<?php
namespace Core;
use Exception\LogicException;
use Core\User as CurrentUser;

abstract class Base
{
    protected static $aAutoloadJsFiles = [];
    protected static $aAutoloadCssFiles = [];
    protected $aGet;
    protected $aPost;
    private static $aGlobalTemplateVars = [];

    function getRequestUri($bKeepGetVars = true, $aAddVars = null, $bMergeAddVarsRecursive = false){
        return Utils::getRequestUri($bKeepGetVars, $aAddVars, $bMergeAddVarsRecursive);
    }

    /**
     * @param array $aVars
     * @return null
     */
    static function addGlobalTemplateVars(array $aVars)
    {
        self::$aGlobalTemplateVars = array_merge(self::$aGlobalTemplateVars, $aVars);
        return null;
    }
    function __construct($aGet, $aPost)
    {
        $this->aGet = $aGet;
        $this->aPost = $aPost;
    }
    function redirect($url, $iRedirectHttpCode = null){
        Utils::redirect($url, $iRedirectHttpCode);
    }
    /**
     * @param string $sTemplate - template file name
     * @param mixed $aData - data to be parsed into the template
     * @return string - resulting html
     */
    function parse($sTemplate, $aData){

        if(!empty(self::$aGlobalTemplateVars))
        {
            $aData = array_merge(self::$aGlobalTemplateVars, $aData);
        }

        if(CurrentUser::isSignedIn()){
            $aData['current_user'] = CurrentUser::getMember();
            $aData['current_role'] = User::getRole();

        }

        // Hoofdmenu inladen
        if(Environment::getCurrent() == 'admin')
        {
            $aData['menu_blocks'] =  MainMenu::get();
        }
        else
        {
            $aData['is_signed_in'] = Customer::isSignedIn();
            if($aData['is_signed_in'])
            {
                $aData['signed_in_customer'] = Customer::getMember();
            }
        }
        $sJsFilename = str_replace('.twig', '.js', $sTemplate);
        $sCssFilename = str_replace('.twig', '.css', $sTemplate);

        if(Environment::getCurrent() == 'frontend')
        {
            $sModuleRoot = dirname(dirname($_SERVER['SCRIPT_FILENAME'])).'/';
        }
        else
        {
            $sModuleRoot = '../';
        }


        if(file_exists($sModuleRoot.Environment::getModuleDir().'/'.$sCssFilename))
        {
            self::$aAutoloadCssFiles[] = '/'.Environment::getModuleDir().'/'.$sCssFilename;
        }

        if(file_exists($sModuleRoot.Environment::getModuleDir().'/'.$sJsFilename))
        {
            self::$aAutoloadJsFiles[] = '/'.Environment::getModuleDir().'/'.$sJsFilename;
        }

        if(Environment::getCurrent() == 'frontend')
        {
            $sAltRoot = dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME']))).'/_default/modules/';

            if(file_exists($sAltRoot.$sJsFilename))
            {
                self::$aAutoloadJsFiles[] = '/'.Environment::getModuleDir().'/'.$sJsFilename;
            }
        }
        $aData['current_session'] = $_SESSION;
        $aData['autoload_js_files'] = array_unique(self::$aAutoloadJsFiles);
        $aData['autoload_css_files'] = array_unique(self::$aAutoloadCssFiles);

        $sHtml = TemplateFactory::parse($sTemplate, $aData);
        return $sHtml;
    }
    /**
     * @param null $sKey
     * @param null $sDefaultValue
     * @param bool $bIsMandatory
     * @param string $sMustBe [numeric|array|numeric]
     * @return null
     */
    protected function get($sKey = null, $sDefaultValue = null, $bIsMandatory = false, $sMustBe = null)
    {
        if ($sKey == null) {
            return $this->aGet;
        } else if (!isset($this->aGet[$sKey])) {
            $aBacktrace = debug_backtrace();


            if ($bIsMandatory && $sDefaultValue == null) {
                throw new LogicException("Get $sKey is a mandatory get variable for this controller " . $aBacktrace[0]['file'] . ':' . $aBacktrace[0]['line']);
            }
            $mOut = $sDefaultValue;
        } else {
            $mOut = $this->aGet[$sKey];
        }

        if ($bIsMandatory || $mOut) {
            if ($sMustBe == 'numeric' && !is_numeric($mOut)) {
                throw new LogicException("Get $sKey must contain a numerical value.");
            } else if ($sMustBe == 'array' && !is_array($mOut)) {
                throw new LogicException("Get $sKey moet be of type array.");
            }
        }
        return $mOut;
    }


    /**
     * @param null $sKey
     * @param null $sDefaultValue
     * @param bool $bIsMandatory
     * @param string $sMustBe [numeric|array]
     * @return null
     */
    protected function post($sKey = null, $sDefaultValue = null, $bIsMandatory = false, $sMustBe = null)
    {
        if ($sKey == null) {
            return $this->aPost;
        } else if (!isset($this->aPost[$sKey])) {
            if ($bIsMandatory) {
                throw new LogicException("Post $sKey is bestaat niet terwijl dit argument verplicht is.");
            }
            $mOut = $sDefaultValue;
        } else {

            $mOut = $this->aPost[$sKey];
        }

        if ($bIsMandatory || $mOut) {
            if ($sMustBe == 'numeric' && !is_numeric($mOut)) {
                throw new LogicException("Post $sKey moet een nummerieke waarde bevatten.");
            } else if ($sMustBe == 'array' && !is_array($mOut)) {
                throw new LogicException("Post $sKey moet een array waarde bevatten.");
            }
        }

        return $mOut;
    }
}
