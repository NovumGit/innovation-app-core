<?php
namespace Core\Json;

use Hurah\Types\Type\Json;

final class Error
{
    private $aErrors = [];
    public function __construct($sMessage)
    {
        $this->aErrors[] = $sMessage;
    }

    public function addError(string $sMessage)
    {
        $this->aErrors[] = $sMessage;
    }
    public function toJson():Json
    {
        return new Json(JsonUtils::encode(['errors' => $this->aErrors]));
    }
}
