<?php
namespace Core;


use Exception\LogicException;

abstract class AbstractEvent extends Base implements IAbstractEvent
{

    private $iButtonEventId;
    private $iCrudEditorButtonEvent;

    function __construct($aGet, $aPost)
    {
        if($this->isConfigurable() && !$this instanceof AbstractConfigurableEvent)
        {
            throw new LogicException("Als een event isConfigurable = true terug geeft dan moet dat event ook AbstractConfigurableEvent implementeren ".get_class($this)." doet dat niet.");

        }
        parent::__construct($aGet, $aPost);
    }
    public function setButtonEventId($iButtonEventId)
    {
        $this->iButtonEventId = $iButtonEventId;
    }
    public function getButtonEventId()
    {
        return $this->iButtonEventId;
    }
    public function setCrudEditorButtonEvent($iCrudEditorButtonEvent)
    {
        $this->iCrudEditorButtonEvent = $iCrudEditorButtonEvent;
    }
    protected function getCrudEditorButtonEvent($iCrudEditorButtonEvent)
    {
        $this->iCrudEditorButtonEvent = $iCrudEditorButtonEvent;
    }

    abstract function getDescription(): string;
    abstract function trigger(string $sEnvironment): void;
    abstract function isConfigurable(): bool;

    /**
     * Deze methode is nodig om te bepalen wat voor data er geretouneerd wordt.
     * Twee pdf documenten kunnen bijvoorbeeld gemerged worden maar er kan maar een csv bestand terug gegeven worden.
     * @return \core\Mime\Mime;
     */
    abstract function outputType(): \core\Mime\Mime;

    function __destruct()
    {
        // TODO: Implement __destruct() method.
    }
}
