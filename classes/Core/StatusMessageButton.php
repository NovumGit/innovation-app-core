<?php
namespace Core;
use Exception\LogicException;

class StatusMessageButton extends StatusMessage {

    private $sButtonLabel = null;
    private $sButtonAction = null;
    private $sButtonTitle;
    private $sButtonType;

    /**
     * StatusMessageButton constructor.
     * @param string $sButtonLabel The text shown on the button
     * @param string $sButtonAction a url, where should we send the user?
     * @param string $sButtonTitle For the title="" attribute, usually used in tooltips
     * @param string $sType what color shall we give the button? Options: [danger|warning|alert|success|info|default]
     */
    function __construct($sButtonLabel, $sButtonAction, $sButtonTitle, $sType)
    {
        $this->typeAllowed($sType);
        $this->sButtonLabel = $sButtonLabel;
        $this->sButtonAction = $sButtonAction;
        $this->sButtonTitle = $sButtonTitle;
        $this->sButtonType = $sType;
    }

    private function typeAllowed(string $sType):bool
    {
        $aAllowedTypes =  ['danger', 'warning', 'alert', 'success', 'info', 'default'];
        if(!in_array($sType, $aAllowedTypes))
        {
            throw new LogicException("Button type must be of one of these types: ".join(', ', $aAllowedTypes));
        }
        return true;
    }
    function getLabel()
    {
        return $this->sButtonLabel;
    }
    function getAction()
    {
        return $this->sButtonAction;
    }
    function getTitle()
    {
        return $this->sButtonTitle;
    }
    function getType()
    {
        return $this->sButtonType;
    }

    /**
     * @param string $sType
     */
    function setType(string $sType)
    {
        $this->typeAllowed($sType);
        $this->sButtonType = $sType;
    }
}
