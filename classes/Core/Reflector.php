<?php
namespace Core;

use Hurah\Types\Type\PhpNamespace;
use Exception\LogicException;
use Model\Logging\Except_log;

class Reflector
{
    private $oReflector;
    private $oObject;

    function __construct($oObject)
    {
        try
        {
            $this->oObject = $oObject;
            $this->oReflector = new \ReflectionClass($oObject);
        }
        catch (\Exception $e)
        {
            Except_log::register($e, false);
        }
    }

    function getNamespaceName():PhpNamespace
    {
        return new PhpNamespace($this->getReflector()->getNamespaceName());
    }
    function getFileName():string
    {
        return $this->getReflector()->getFileName();
    }
    function implementsInterface(string $sInterfaceName):bool
    {
        if(!$this->oReflector)
        {
            $sClassName = '';
            if(is_string($this->oObject))
            {
                $sClassName = $this->oObject;
            }
            else
            {
                $sClassName = get_class($this->oObject);

            }
            throw new LogicException("Class $sClassName does not contain a method implementsInterface.");
        }
        return $this->oReflector->implementsInterface($sInterfaceName);
    }

    function getReflector():\ReflectionClass
    {
        return $this->oReflector;
    }

    function getShortName():string
    {
        return $this->getReflector()->getShortName();
    }

    /**
     * Returns the full name of the class, so with namespace
     * @return string
     */
    function getName():string
    {
        return $this->getReflector()->getName();
    }
    function getClassDir():string
    {
        return dirname($this->getReflector()->getFileName());
    }
}
