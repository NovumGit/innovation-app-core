<?php
namespace Core;

class HttpRequest
{
    static function getContentType():?string
    {
        $aHeaders = getallheaders();

        foreach ($aHeaders as $sKey => $sValue)
        {
           if(trim(strtolower($sKey)) == 'content-type')
           {
               return $sValue;
           }
        }
        return null;
    }
}
