<?php
namespace Core;


class InlineTemplate
{
    /**
     * @param string $sTemplateHtml
     * @param array $aData
     * @return false|string
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    static function parse(string $sTemplateHtml, array $aData)
    {
        $oTwigEnvironment = TemplateFactory::createFromDirs([]);
        $oTemplate = $oTwigEnvironment->createTemplate($sTemplateHtml);
        return $oTemplate->render($aData);
    }
}
