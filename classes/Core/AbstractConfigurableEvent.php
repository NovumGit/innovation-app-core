<?php
namespace Core;

interface AbstractConfigurableEvent
{
    function isConfigurable();

    /**
     * @param $iCrudEditorButtonEventId
     * @param $sEnvironment [overview|edit]
     * @return mixed
     */
    function getConfiguratorHtml($iCrudEditorButtonEventId, $sEnvironment);
    function saveConfiguration($sEnvironment);

}