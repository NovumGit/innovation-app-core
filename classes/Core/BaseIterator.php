<?php
namespace Core;

use Iterator as PhpIterator;

abstract class BaseIterator implements PhpIterator {
    protected int $position;
    protected array $array;

    abstract public function current();

    public function rewind() {
        $this->position = 0;
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return isset($this->array[$this->position]);
    }

}
