<?php

namespace Core;

use Exception\LogicException;
use Model\Company\CompanyQuery;
use Model\Crm\Customer as CustomerModel;
use Model\Crm\CustomerQuery;
use Model\Setting\MasterTable\Base\Sale_order_notification_typeQuery;
use Model\Setting\MasterTable\LanguageQuery;
use Model\System\SystemRegistryQuery;

class Customer {

    static function logout() {
        unset($_SESSION['customer']);
        unset($_SESSION['password_decrypt']);
    }

    static function setMember(CustomerModel $oCustomer = null) {
        if ($oCustomer) {
            $_SESSION['customer'] = $oCustomer->toArray();
        } else {
            $_SESSION['customer'] = null;
        }
    }

    static function getMember(): CustomerModel {
        if (!isset($_SESSION['customer']) || empty($_SESSION['customer'])) {
            throw new LogicException(<<<EOT
The developer made a mistake. he should have checked of the customer is signed in first.
EOT
            );
        }

        $oCustomerQuery = CustomerQuery::create();
        $oCustomerQuery->setQueryKey('CustomerQuery-findOneById');
        $oCustomer = $oCustomerQuery->findOneById($_SESSION['customer']['Id']);

        return $oCustomer;
    }

    /*
     * Creates a new password for the customer, stores the salt+encryted pass and returns the unencrypted none
     */
    static function generateNewPass(CustomerModel $oCustomer): string {
        $sSalt = substr(sha1('xef' . time()), 12, 25);
        $sUnencryptedPass = substr(sha1('3xef' . time()), 4, 8);

        $oCustomer->setSalt($sSalt);
        $oCustomer->setPassword(Customer::makeEncryptedPass($sUnencryptedPass, $sSalt));
        $oCustomer->save();
        return $sUnencryptedPass;
    }

    /**
     * @param CustomerModel $oCustomer
     * @param $iLanguageId
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    public static function sendPassResetMail(CustomerModel $oCustomer, $iLanguageId) {
        $oLanguage = LanguageQuery::create()->findOneById($iLanguageId);
        $oSale_order_notification_type = Sale_order_notification_typeQuery::create()->findOneByCode('password_reset');

        if (!isset($oSale_order_notification_type)) {
            throw new LogicException("No notification e-mail defined for the password reset mail.");
        }

        $iSale_order_notification_typeId = $oSale_order_notification_type->getId();

        $sFrom = SystemRegistryQuery::getVal('password_reset_sender_' . $iLanguageId);
        $sSubject = SystemRegistryQuery::getVal('password_reset_subject_' . $iLanguageId);

        $iCustomerId = $oCustomer->getId();
        $aViewData['Customer'] = $oCustomer;
        $aViewData['Own_company'] = CompanyQuery::getDefaultCompany();
        $aViewData['User'] = User::getMember();

        $sUnencryptedPass = self::generateNewPass($oCustomer);
        $sFrom = InlineTemplate::parse($sFrom, $aViewData);
        $sTo = $oCustomer->getEmail();
        $sSubject = InlineTemplate::parse($sSubject, $aViewData);

        $sUrl = Cfg::get('PROTOCOL') . '://' . $_SERVER['HTTP_HOST'];

        $sQuery = http_build_query([
            'notification_type_id' => $iSale_order_notification_typeId,
            'customer_id' => $iCustomerId,
            'new_pass' => $sUnencryptedPass
        ]);
        $sPath = "/" . $oLanguage->getShopUrlPrefix() . "/mail/customer?$sQuery";

        $sContent = file_get_contents($sUrl . $sPath, false, stream_context_create([
            "ssl" => [
                "verify_peer" => false,
                "verify_peer_name" => false,
            ],
        ]));

        $oMailMessage = new MailMessage();
        $oMailMessage->setFrom($sFrom);
        $oMailMessage->setTo($sTo);
        $oMailMessage->setSubject($sSubject);
        $oMailMessage->setBody($sContent);

        Mailer::send($oMailMessage);
    }

    /**
     * @return mixed
     */
    function getCurrentSignedInCustomer() {
        if (!isset($_SESSION['customer']) || empty($_SESSION['customer'])) {
            return false;
        }
        return $_SESSION['customer'];
    }

    static function isSignedIn() {
        if (!isset($_SESSION['customer']) || empty($_SESSION['customer'])) {
            return false;
        }
        return true;
    }

    static function makeSalt() {
        return substr(md5(time() . '#e22df23'), 0, 25);
    }

    static function makeEncryptedPass($sUnEncryptedPassword, $sSalt) {
        return hash('sha256', $sSalt . $sUnEncryptedPassword);
    }
}
