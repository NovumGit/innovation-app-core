<?php
/**
 * Created by PhpStorm.
 * User: anton
 * Date: 19-1-20
 * Time: 20:27
 */

namespace Core;

interface IAbstractEvent
{
    public function setButtonEventId($iButtonEventId);

    public function getButtonEventId();

    public function setCrudEditorButtonEvent($iCrudEditorButtonEvent);

    function getDescription(): string;

    function trigger(string $sEnvironment): void;

    function isConfigurable(): bool;

    /**
     * Deze methode is nodig om te bepalen wat voor data er geretouneerd wordt.
     * Twee pdf documenten kunnen bijvoorbeeld gemerged worden maar er kan maar een csv bestand terug gegeven worden.
     * @return \core\Mime\Mime;
     */
    function outputType(): \core\Mime\Mime;
}
