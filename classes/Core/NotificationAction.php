<?php
namespace Core;

class NotificationAction
{

    private $sButtonLabel;
    private $sButtonAction;
    private $sType;

    /**
     * StatusMessageButton constructor.
     * @param string $sButtonLabel The text shown on the button
     * @param string $sButtonAction a url, where should we send the user?
     * @param string $sType what color shall we give the button? Options: [danger|warning|alert|success|info|default]
     */
    function __construct($sButtonLabel, $sButtonAction, $sType)
    {
        $this->sButtonLabel = $sButtonLabel;
        $this->sButtonAction = $sButtonAction;
        $this->sType = $sType;
    }
}