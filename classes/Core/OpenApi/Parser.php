<?php
namespace Core\OpenApi;

class Parser
{

    private $aOpenApiData;

    function fromUrl(string $sUrl)
    {
        $sOpenApiData = file_get_contents($sUrl);
        $this->aOpenApiData = json_decode($sOpenApiData, true);
    }
    function toArray():array
    {
        return $this->aOpenApiData;
    }
    function getPaths():array
    {
        return $this->aOpenApiData['paths'];
    }
}
