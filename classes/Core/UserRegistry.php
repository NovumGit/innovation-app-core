<?php
namespace Core;

use Model\User\UserRegistry as UserRegistryModel;
use Model\User\UserRegistryQuery as UserRegistryModelQuery;
use LogicException;

class UserRegistry{

    static $preLoadedUserRegistry ;

    /**
     * @param string $sKey
     * @param string $sValue
     * @param User $oUserModel when left to null the currently signed in user is updated.
     */
    static function register($sKey, $sValue, $oUserModel = null)
    {
        $oUserRegistry = self::getObject($sKey, $oUserModel);

        if(!$oUserRegistry instanceof UserRegistryModel)
        {
            if($oUserModel instanceof \Model\Account\User)
            {
                $oUser = $oUserModel;
            }
            else
            {
                $oUser = User::getMember();
            }
            $oUserRegistry = new UserRegistryModel();
            $oUserRegistry->setItemKey($sKey);
            $oUserRegistry->setUserId($oUser->getId());
        }

        $oUserRegistry->setItemValue($sValue);
        $oUserRegistry->save();
    }
    /**
     * @param string $sKey
     * @param User $oUserModel when left to null the currently signed in user is updated.
     */
    static function get($sKey, $oUserModel = null)
    {

        $oUserRegistry = self::getObject($sKey, $oUserModel);

        if($oUserRegistry instanceof UserRegistryModel)
        {
            return $oUserRegistry->getItemValue();
        }
        return null;
    }
    /**
     * @param string $sKey
     */
    static function unregister($sKey)
    {
        $oUserRegistry = self::getObject($sKey);
        if($oUserRegistry instanceof UserRegistryModel)
        {
            $oUserRegistry->delete();
        }
    }

    private static function getObject($sKey, \Model\Account\User $oUserModel = null)
    {
        if($oUserModel == null)
        {
            $oUserModel = User::getMember();
        }
        if($oUserModel == null)
        {
            return null;
        }

        if(!$oUserModel instanceof \Model\Account\User)
        {
            throw new \Exception\LogicException("Expected an instance of User.");
        }

        $aFullRegistry = UserRegistryModelQuery::create()->findByUserId($oUserModel->getId());

        if(!self::$preLoadedUserRegistry && !$aFullRegistry->isEmpty())
        {
            foreach ($aFullRegistry as $oRegistryItem)
            {
                self::$preLoadedUserRegistry[$oRegistryItem->getItemKey()] = $oRegistryItem;
            }
        }

        if(isset(self::$preLoadedUserRegistry[$sKey]))
        {
            return self::$preLoadedUserRegistry[$sKey];
        }
        return null;
    }
}
