<?php
namespace Core;
use Model\Setting\MasterTable\Base\CountryQuery;
use Model\Setting\MasterTable\Country;

class Location{

    static function detectCountryByIp($sIp)
    {
        if(isset($_SERVER['DETECTED_COUNTRY']))
        {
            return CountryQuery::create()->findOneByIso2($_SERVER['DETECTED_COUNTRY']);
        }
        if($sIp == '127.0.0.1')
        {
            $sIp = '83.128.167.222';
        }

        $ch = curl_init("http://www.geoplugin.net/php.gp?ip=$sIp");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS, 100);
        $sData = curl_exec($ch);
        $curl_errno = curl_errno($ch);


        if($curl_errno > 0 || $sData == false)
        {
            $aData['geoplugin_countryCode'] = 'NL';
        }
        else
        {
            $aData = unserialize($sData);

            if(!isset($aData['geoplugin_countryCode']))
            {
                $aData['geoplugin_countryCode'] = 'NL';
            }
            else
            {
                $_SERVER['DETECTED_COUNTRY'] = $aData['geoplugin_countryCode'];
            }
        }
        $oCountry = CountryQuery::create()->findOneByIso2($aData['geoplugin_countryCode']);

        if($oCountry instanceof Country)
        {
            return $oCountry;
        }
        return null;
    }
}