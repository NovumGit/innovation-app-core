<?php

namespace Core;

use AdminModules\ModuleConfig;
use Exception;
use Exception\FileNotFoundException;
use Exception\LogicException;
use Model\Logging\Except_log;
use Model\Module\ModuleQuery;

class Module
{

    static function getEnabledModules($iFkRole)
    {
        $iFkRole = (int)$iFkRole;
        $sQuery = "SELECT m.id FROM module m, module_role mr WHERE m.id  = mr.module_id AND mr.role_id = $iFkRole ORDER BY mr.sorting";

        return self::getModuleObjects($sQuery);
    }

    static function getDisabledModules($iFkRole)
    {
        $iFkRole = (int)$iFkRole;
        $sQuery = "SELECT id FROM module WHERE id NOT IN (SELECT module_id FROM module_role WHERE role_id = $iFkRole) ORDER BY name";

        return self::getModuleObjects($sQuery);
    }

    static function getAllModules()
    {
        return self::getModuleObjects('SELECT id FROM module ORDER BY name');
    }

    private static function getModuleObjects($sQuery)
    {

        $aItems = QueryMapper::fetchArray($sQuery);

        $aModuleRecords = [];
        if (empty($aItems))
        {
            return false;
        }

        foreach ($aItems as $aItem)
        {
            $aModuleRecords[] = ModuleQuery::create()->findOneById($aItem['id']);
        }

        $aModuleObjects = [];
        foreach ($aModuleRecords as $oModule)
        {
            $sNamespacedName = $oModule->getName();

            if (class_exists($sNamespacedName))
            {
                $oModuleConfigObject = new $sNamespacedName;
                $oModuleConfigObject->setDatabaseId($oModule->getId());

                $aModuleObjects[] = $oModuleConfigObject;
            }
        }

        return $aModuleObjects;
    }

    /*
     * @return ModuleConfig[]
     */
    static function registerAll()
    {
        $aModuleDirPatterns = [
            '../admin_modules/Custom' .
            Cfg::get('CUSTOM_NAMESPACE'),
            '../admin_modules'];

        foreach ($aModuleDirPatterns as $sPattern)
        {
            $aModuleDirContents = glob($sPattern . '/*');

            $aModules = [];
            try
            {
                foreach ($aModuleDirContents as $sModuleDir)
                {
                    if (is_dir($sModuleDir))
                    {
                        $aSkip = [
                            '../admin_modules/Login',
                            '../admin_modules/Error',
                            '../admin_modules/Custom',
                            '../admin_modules/Locales'
                        ];

                        if (in_array($sModuleDir, $aSkip))
                        {
                            continue;
                        }
                        if(preg_match('/Custom\/[a-zA-Z]+/', $sModuleDir))
                        {
                            continue;
                        }

                        if (file_exists($sModuleDir . '/.skip'))
                        {
                            continue;
                        }
                        if (!file_exists($sModuleDir . '/Config.php'))
                        {
                            throw new FileNotFoundException('Config ontbreekt ' . $sModuleDir . '/Config.php');
                        }

                        $sNameSpacedClassname = str_replace('/', '\\', $sModuleDir) . '\\Config';
                        $sNameSpacedClassname = str_replace('.\\', '\\', $sNameSpacedClassname);
                        $sNameSpacedClassname = str_replace('.\admin_modules', 'AdminModules', $sNameSpacedClassname);

                        $oModuleQuery = ModuleQuery::create();

                        $oModule = $oModuleQuery->findOneByName($sNameSpacedClassname);

                        $oModuleConfig = new $sNameSpacedClassname();

                        if (!$oModuleConfig instanceof ModuleConfig)
                        {
                            throw new LogicException("Module class not an instance of ModuleConfig.");
                        }

                        if (!$oModule instanceof \Model\Module\Module)
                        {
                            $oModule = new \Model\Module\Module();
                            $oModule->setName($sNameSpacedClassname);
                        }

                        $oModule->setTitle($oModuleConfig->getModuleTitle());
                        $oModule->save();
                        $aModules[] = new $sNameSpacedClassname;
                    }
                }
            }
            catch (Exception $e)
            {
                if (Environment::isDevel())
                {
                    Logger::console($e->getMessage());
                }
                Except_log::register($e, false);
            }
        }
        return $aModules;
    }
}
