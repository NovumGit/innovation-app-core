<?php
namespace Core;


class Environment
{
    static function isDevel()
    {
        return isset($_SERVER['PWD']) && strpos($_SERVER['PWD'], 'anton') || isset($_SERVER['IS_DEVEL']);
    }
    static function isTest()
    {
        if(isset($_SERVER['PWD']) && strpos($_SERVER['PWD'], 'test'))
        {
            return true;
        }
        if(isset($_SERVER['HTTP_HOST']))
        {
            return (strpos($_SERVER['HTTP_HOST'], 'test') === 0 || strpos($_SERVER['HTTP_HOST'], 'admin.test') === 0);
        }
        return false;
    }

    static function isProduction()
    {
        return (!self::isDevel() && !self::isTest());
    }
    static function getCurrent()
    {

        if(isset($_SERVER['PWD']) || isset($_SERVER['TERM']))
        {
            return 'commandline';
        }
        if(isset($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'], 'dealers.') === 0)
        {
            return 'admin';
        }
        if(isset($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'], 'cockpit.') === 0)
        {
            return 'admin';
        }
        if(isset($_SERVER['HTTP_HOST']) && Cfg::get('CUSTOM_ADMIN_DOMAIN') == $_SERVER['HTTP_HOST'])
        {
            return 'admin';
        }
	    if(isset($_SERVER['HTTP_HOST']) && $_SERVER['HTTP_HOST'] == Cfg::get('ADMIN_URL'))
	    {
		    return 'admin';
	    }
        return (isset($_SERVER['HTTP_HOST']) && preg_match('/^admin\.[a-z]+/', $_SERVER['HTTP_HOST'])) ? 'admin' : 'frontend';
    }

    static function getModuleDir()
    {
        $sNamespaceRoot = self::getCurrent()=='admin' ? 'admin_modules' : 'modules';
        return $sNamespaceRoot;
    }
    static function getModuleRootNamespace($sNamespace = null)
    {
        $sNamespaceRoot = self::getCurrent() == 'admin' ? 'AdminModules' : (($sNamespace) ? $sNamespace : 'Modules');
        return $sNamespaceRoot;
    }
}
