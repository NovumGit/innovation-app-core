<?php

namespace Core;

class Currency
{
    public static function priceFormat($fPrice)
    {
        return '€ ' . number_format($fPrice, 2, ',', '.');
    }

    public static function formatForDb($fPrice)
    {
        $fPrice = str_replace(',', '.', $fPrice);
        return $fPrice;
    }

    public static function formatForPresentation($fPrice)
    {
        if ($fPrice) {
            $fPrice = number_format($fPrice, 2, ',', '.');
        }

        return $fPrice;
    }
}
