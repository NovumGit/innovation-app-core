<?php

namespace Core;

use Exception\LogicException;

abstract class AbstractPopup
{
    protected static $aSupportedTypes = [
        'warning' => 'Warning!',
        'info'    => 'Information',
        'success' => 'Success (-:',
        'alert'   => 'Alert!',
        'danger'  => 'Danger!',
    ];
    /**
     * @param StatusMessageButton[] $aStatusMessageButtons
     * @param string $sMessageType
     * @return array
     */
    protected static function buttonArray(array $aStatusMessageButtons = null, string $sMessageType = 'info'):?array
    {
        $aButtons = null;
        if ($aStatusMessageButtons)
        {
            foreach ($aStatusMessageButtons as $oStatusMessageButton)
            {
                if (!$oStatusMessageButton instanceof StatusMessageButton)
                {
                    throw new LogicException("StatusModals's third parameter must be an array of StatusMessageButtons.");
                }
                $aButtons[] = [
                    'label' => $oStatusMessageButton->getLabel(),
                    'action' => $oStatusMessageButton->getAction(),
                    'type' => $oStatusMessageButton->getType(),
                    'title' => $oStatusMessageButton->getTitle(),
                ];
            }
        }
        if (!isset(self::$aSupportedTypes[$sMessageType]))
        {
            throw new LogicException("StatusId message should be of any of these types: " . join(PHP_EOL, array_keys($aSupportedTypes)));
        }
        return $aButtons;
    }
}
