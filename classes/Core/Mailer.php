<?php
namespace Core;

use Model\ContactMessage;
use Model\Logging\Except_log;
use Model\Sale\SaleOrderQuery;
use Model\SaleOrderNotification\SaleOrderNotification;
use Model\Setting\MasterTable\LanguageQuery;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery;
use Model\System\SystemRegistryQuery;
use Swift_Message;
use Swift_Attachment;
use Swift_SmtpTransport;
use Swift_Mailer;

class Mailer{

    static $sOverrideForEver = null;
    static function sendContactMessageAutoreply(ContactMessage $oContactMessage, $iNoticationTypeId, $sTemplate = '/mail/autoreply')
    {
        $oSale_order_notification_type = Sale_order_notification_typeQuery::create()->findOneById($iNoticationTypeId);
        $oLanguage = LanguageQuery::create()->findOneById($oContactMessage->getLanguageId());

        $sSender = SystemRegistryQuery::getVal($oSale_order_notification_type->getCode().'_sender_'.$oLanguage->getId());
        $sSubject = SystemRegistryQuery::getVal($oSale_order_notification_type->getCode().'_subject_'.$oLanguage->getId());
        $sDomain = Cfg::get('PROTOCOL').'://'.Cfg::get('DOMAIN');

        $sUrl = $sDomain . "/" . $oLanguage->getShopUrlPrefix() . $sTemplate . "?contact_message_id=" . $oContactMessage->getId() . '&notification_type_id=' . $iNoticationTypeId;
        $sContents = file_get_contents($sUrl);

        $oMailMessage = new MailMessage();
        $oMailMessage->setFrom($sSender);
        $oMailMessage->setTo($oContactMessage->getEmail());
        $oMailMessage->setSubject($sSubject);
        $oMailMessage->setBody($sContents);

        Mailer::send($oMailMessage, null);
    }
    static function sendSaleOrderStatusMail($iOrderId, $iNoticationTypeId)
    {
        $oSaleOrder = SaleOrderQuery::create()->findOneById($iOrderId);
        $oSale_order_notification_type = Sale_order_notification_typeQuery::create()->findOneById($iNoticationTypeId);

        $oLanguage = LanguageQuery::create()->findOneById($oSaleOrder->getLanguageId());
        $sSender = SystemRegistryQuery::getVal($oSale_order_notification_type->getCode().'_sender_'.$oLanguage->getId());
        $sSubject = SystemRegistryQuery::getVal($oSale_order_notification_type->getCode().'_subject_'.$oLanguage->getId());
        $sDomain = Cfg::get('PROTOCOL').'://'.Cfg::get('DOMAIN');
        $sUrl = $sDomain."/".$oLanguage->getShopUrlPrefix().'/mail?order_id='.$iOrderId.'&notification_type_id='.$iNoticationTypeId;
        $sContents = file_get_contents($sUrl);

        $twig = new \Twig_Environment(new \Twig_Loader_Array([]));
        $template = $twig->createTemplate($sSubject);
        $sSubject = $template->render(['Sale_order' => $oSaleOrder]);

        $oSaleOrderNotification = new SaleOrderNotification();
        $oSaleOrderNotification->setSaleOrderId($iOrderId);
        $oSaleOrderNotification->setSaleOrderNotificationTypeId($oSale_order_notification_type->getId());
        $oSaleOrderNotification->setFromEmail($sSender);
        $oSaleOrderNotification->setToEmail($oSaleOrder->getCustomerEmail());

        $oSaleOrderNotification->setContent(strip_tags($sContents));
        $oSaleOrderNotification->setTitle($sSubject);
        $oSaleOrderNotification->setDateSent(time());
        $oSaleOrderNotification->setSenderIp($_SERVER['REMOTE_ADDR']);
        $oSaleOrderNotification->save();

        $oMailMessage = new MailMessage();
        $oMailMessage->setFrom($sSender);
        $oMailMessage->setTo($oSaleOrder->getCustomerEmail());
        $oMailMessage->setSubject($sSubject);
        $oMailMessage->setBody($sContents);

        $aSwift_Attachments = null;
        if($oSale_order_notification_type->getCode() == 'invoice_email')
        {
            $sPdfUrlDomain = 'http://admin.'.Cfg::get('DOMAIN').'/order/invoice/invoice?order_id='.$iOrderId.'&format=pdf&_do=NoLoginPrintInvoice&secret_key=12312387123798b21eifweifcvlkjwqn';

            // connect via SSL, but don't check cert
            $handle=curl_init($sPdfUrlDomain);
            curl_setopt($handle, CURLOPT_VERBOSE, true);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
            $sPdfFile = curl_exec($handle);

            // $sPdfFile = file_get_contents($sPdfUrlDomain);
            $aSwift_Attachments[] = new Swift_Attachment($sPdfFile, 'invoice.pdf', 'application/pdf');
        }

        Mailer::send($oMailMessage, $aSwift_Attachments);
    }
    /*
     * Leave access to this method public please.
     */
    static function getDevTestToAddress()
    {
        return 'anton@nui-boutkam.nl';
    }
    /**
     * @param MailMessage $oMailMessage
     * @param Swift_Attachment $aAttachments[]
     */
    static function send(MailMessage $oMailMessage, $aAttachments = null)
    {
        if(Environment::isDevel() /* || Environment::isTest()*/)
        {
            // $oTransport = Swift_SmtpTransport::newInstance('smtp.caiway.net', 25);
            $oTransport = Swift_SmtpTransport::newInstance('nui-boutkam.nl', 25)
            ->setUsername('anton@nui-boutkam.nl')
            ->setPassword('Krxvmt77');
         //   $oTransport = Swift_MailTransport::newInstance();
            $oMailMessage->setTo(self::getDevTestToAddress());
        }
        else if(Environment::isTest())
        {
            // Bij test.fashionfemme.com werden de mails anders niet goed verzonden, later ff smtp server checken of accounts aanmaken.
            $oTransport = \Swift_MailTransport::newInstance(null);
        }
        else if(Cfg::get('CUSTOM_MAIL_TRANSPORT'))
        {

            // noreply@femaleseeds.nl
            $oTransport = Cfg::get('CUSTOM_MAIL_TRANSPORT');
        }
        else
        {
            // vangool en dtc hebben dit in ieder geval zo nodig
            // mail is depricated in php7.
            $oTransport = Swift_SmtpTransport::newInstance('localhost', 25);
        }

        $oMailer = Swift_Mailer::newInstance($oTransport);

        $oMessage = Swift_Message::newInstance();


        if($oMailMessage->getFromName())
        {
            $oMessage->setFrom($oMailMessage->getFrom(), $oMailMessage->getFromName());
        }
        else
        {
            $oMessage->setFrom($oMailMessage->getFrom());
        }


        if(Cfg::get('CUSTOM_OVERRIDE_FROM'))
        {

            $oMessage->setSubject($oMailMessage->getFrom() . ', ' . $oMailMessage->getSubject());
            $oMailMessage->setFrom(Cfg::get('CUSTOM_OVERRIDE_FROM'));
        }
        else
        {
            $oMessage->setSubject($oMailMessage->getSubject());
        }


        if(self::$sOverrideForEver)
        {
            $oMessage->setTo(self::$sOverrideForEver);
        }
        else
        {

            $oMessage->setTo($oMailMessage->getTo());
        }



        // De regel hieronder heeft nooit gewerkt,  Swift_Encoder_Base64Encoder is mogelijk het verkeerde argument, juiste klasse zoeken als je dit nodig hebt.
        // $oMessage->setEncoder(new \Swift_Encoder_Base64Encoder());
        // $oMessage->setContentType('multipart/mixed');
        $oMessage->setBody(strip_tags($oMailMessage->getBody()));
        $oMessage->addPart($oMailMessage->getBody(), 'text/html');

        if(!empty($aAttachments))
        {
            foreach($aAttachments as $oAttachment)
            {
                $oMessage->attach($oAttachment);
            }
        }
        if(!$oMailer->send($oMessage, $aFailures))
        {
            if($aFailures)
            {
                foreach($aFailures as $sFailure)
                {
                    Except_log::registerError(E_WARNING, 'Could not send email to '.$sFailure.' with subject '.$oMessage->getSubject(), __FILE__, __LINE__);
                }
            }
        }
    }
}
