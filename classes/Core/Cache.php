<?php
namespace Core;
use phpFastCache\CacheManager;

class Cache
{
    private static $oCacher;
    private static function getConfig(){
        $sCacheDir = Config::getDataDir(true).'/tmp/cache';

        if(!is_dir($sCacheDir))
        {
            mkdir($sCacheDir, true);
        }
        return [
            "path" => $sCacheDir,
        ];
    }
    /**
     * @param string $sKey
     * @return mixed
     */
    static function get($sKey)
    {
        if(!self::$oCacher)
        {
            self::$oCacher = CacheManager::getInstance('files', self::getConfig());
        }

        return self::$oCacher->getItem($sKey)->get();
    }
    /**
     * @param string $sKey
     * @return mixed
     */
    static function isHit($sKey)
    {
        if(!self::$oCacher)
        {
            self::$oCacher = CacheManager::getInstance('files', self::getConfig());
        }

        return self::$oCacher->getItem($sKey)->isHit();
    }

    static function set($sKey, $aData, $iExpiresSeconds)
    {
        if(!self::$oCacher)
        {
            self::$oCacher = CacheManager::getInstance('files', self::getConfig());
        }

        $oCacheHolder = self::$oCacher->getItem($sKey);


        $oCacheHolder->set($aData)->expiresAfter($iExpiresSeconds);
        self::$oCacher->save($oCacheHolder);

        return $aData;
    }
}
