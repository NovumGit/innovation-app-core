<?php
namespace Core;

class Debug {

    static function backtrace()
    {

        $aTrace = debug_backtrace();
        Logger::console('Debug backtrace');

        foreach ($aTrace as $iLine => $aLine)
        {
            Logger::console($aLine['file'] . ':' . $aLine['line']);
        }

        Logger::console('Called from:' . $aTrace[0]['file'] . ':' . $aTrace[0]['line']);

    }
    static function pre_r($aData)
    {
        echo  debug_backtrace()[0]['file'] . ':' .debug_backtrace()[0]['line']."<br>";
        echo "<pre>" . print_r($aData, true) . "</pre>";
    }

}
