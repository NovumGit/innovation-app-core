<?php

namespace Core;

use Hurah\Types\Type\IGenericDataType;
use Hurah\Types\Type\Path;
use Hurah\Types\Type\PhpNamespace;
use Core\Json\JsonUtils;
use LogicException;
use Model\Setting\MasterTable\Base\LanguageQuery;
use Model\Setting\MasterTable\Language;
use phpDocumentor\Reflection\Types\Self_;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Symfony\Component\Console\Output\OutputInterface;

class Utils
{
    /**
     * Recursively removes a directory and it's content
     * @param Path $oPath
     * @param bool $removeOnlyChildren
     * @return bool
     */
    public static function rmDir($mPath, bool $removeOnlyChildren = false, ?OutputInterface $output = null): bool
    {
        if (is_string($mPath)) {
            $source = $mPath;
        } else {
            $source = (string) $mPath;
        }
        if (empty($source) || file_exists($source) === false) {
            $output->writeln("Directory <info>$source</info> does not exist");
            return false;
        }

        if (is_file($source)) {
            $output->writeln("Unlink file <info>$source</info>");
            return unlink($source);
        } elseif (is_link($source)) {
            $output->writeln("Unlink symbolic link <info>$source</info>");
            return unlink($source);
        }

        $files = new RecursiveIteratorIterator(
            new RecursiveDirectoryIterator($source, RecursiveDirectoryIterator::SKIP_DOTS),
            RecursiveIteratorIterator::CHILD_FIRST
        );

        //$fileinfo as SplFileInfo
        foreach ($files as $fileinfo) {
            if ($fileinfo->isDir()) {
                $output->writeln("Rmdir <info>{$fileinfo->getRealPath()}</info>");
                if (self::rmDir($fileinfo->getRealPath(), $removeOnlyChildren, $output) === false) {
                    return false;
                }
            } else {
                $output->writeln("Unlink <info>{$fileinfo->getRealPath()}</info>");
                if (unlink($fileinfo->getRealPath()) === false) {
                    return false;
                }
            }
        }

        if ($removeOnlyChildren === false) {
            $output->writeln("Rmdir <info>{$source}</info>");
            return rmdir($source);
        }

        return true;
    }
    static function removeNamespace(string $className): string
    {
        if (preg_match('@\\\\([\w]+)$@', $className, $matches)) {
            $className = $matches[1];
        }

        return $className;
    }

    static function makeNamespace(...$aParts): string
    {
        $aUseParts = [];
        foreach ($aParts as $mPart) {
            if (is_null($mPart)) {
                continue;
            }
            if (is_array($mPart)) {
                $aUseParts[] =  join('\\', $mPart);
            } elseif (is_string($mPart)) {
                $aUseParts[] =  $mPart;
            } elseif (is_object($mPart) && $mPart instanceof PhpNamespace) {
                $aUseParts[] =  (string) $mPart;
            } elseif (is_object($mPart)) {
                print_r($mPart);
                $reflector = new Reflector($mPart);
                $aUseParts[] = (string) $reflector->getNamespaceName();
            }
        }
        return join('\\', $aUseParts);
    }

    /**
     * Stores data in a directory inside the data dir, if the dir does not exist it will be created.
     * @param $sDirName the dir relative to the datadir
     * @param $sFileName the name of the file
     * @param mixed $sData anything
     */
    static function store(string $sDirName, string $sFileName, $sData)
    {
        echo "tt";
        if ((is_object($sData) || is_array($sData)) && !$sData instanceof IGenericDataType) {
            $sData = JsonUtils::encode($sData, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        }

        $sPath = self::makePath(Config::getDataDir(true), $sDirName, $sFileName);

        if (!is_dir(dirname($sPath))) {
            self::makeDir(dirname($sPath));
        }
        Logger::debug("Writing file to $sPath");
        self::filePutContents($sPath, $sData);
    }
    static function makePath(...$aParts): Path
    {
        $aUseParts = [];
        foreach ($aParts as &$mPart) {
            if (is_null($mPart) || empty($mPart)) {
                continue;
            }
            if (is_array($mPart)) {
                $aUseParts[] = self::makePath(...$mPart);
                continue;
            }
            $aUseParts[] = $mPart;
        }
        return new Path(join(DIRECTORY_SEPARATOR, $aUseParts));
    }
    static function makeDir(string $sPath): void
    {
        if (!file_exists($sPath)) {
            mkdir($sPath, 0777, true);
        }
    }
    static function filePutContents($sFilename, $sfileContents, $ocMode = 0777): int
    {
        $ifileContentsResult = file_put_contents($sFilename, $sfileContents);
        chmod($sFilename, $ocMode);
        return $ifileContentsResult;
    }

    static function urlHasQuery(string $url): bool
    {
        return (parse_url($url, PHP_URL_QUERY));
    }
    static function pathIsUrl(string $sFilePath): bool
    {
        return strpos('://', $sFilePath);
    }

    static function generatePassword($length = 9, $add_dashes = false, $available_sets = 'luds')
    {
        $sets = array();
        if (strpos($available_sets, 'l') !== false) {
            $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        }
        if (strpos($available_sets, 'u') !== false) {
            $sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
        }
        if (strpos($available_sets, 'd') !== false) {
            $sets[] = '23456789';
        }
        if (strpos($available_sets, 's') !== false) {
            $sets[] = '!@#$%&*?';
        }
        $all = '';
        $password = '';
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
            $all .= $set;
        }
        $all = str_split($all);
        for ($i = 0; $i < $length - count($sets); $i++) {
            $password .= $all[array_rand($all)];
        }
        $password = str_shuffle($password);
        if (!$add_dashes) {
            return $password;
        }
        $dash_len = floor(sqrt($length));
        $dash_str = '';
        while (strlen($password) > $dash_len) {
            $dash_str .= substr($password, 0, $dash_len) . '-';
            $password = substr($password, $dash_len);
        }
        $dash_str .= $password;
        return $dash_str;
    }

    static function shortenByWord($sString, $iLength)
    {
        $parts = preg_split('/([\s\n\r]+)/', $sString, null, PREG_SPLIT_DELIM_CAPTURE);
        $parts_count = count($parts);
        $length = 0;
        $last_part = 0;
        for (; $last_part < $parts_count; ++$last_part) {
            $length += strlen($parts[$last_part]);
            if ($length > $iLength) {
                break;
            }
        }

        return implode(array_slice($parts, 0, $last_part));
    }

    static function requestIsSSL()
    {
        return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;
    }

    static function verifyGoogleCaptha($sSecret, $sResponse)
    {
        $aArguments = ['secret' => $sSecret, 'response' => $sResponse, 'remoteip' => $_SERVER['REMOTE_ADDR']];
        $sUrl = 'https://www.google.com/recaptcha/api/siteverify';
        $aQuery = http_build_query($aArguments);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sUrl);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $aQuery);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $sServerOutput = curl_exec($ch);
        $aServerOutput = json_decode($sServerOutput, true);
        if ($aServerOutput['success'] == true) {
            return true;
        }

        return false;
    }
    static function snake_case($sString)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $sString, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }
    static function camelCase($sString)
    {
        $sString = str_replace('_', ' ', $sString);
        $sString = ucwords($sString);
        $sString = str_replace(' ', '', $sString);

        return $sString;
    }

    static function redirect($url, $iRedirectHttpCode = null)
    {
        if ($iRedirectHttpCode == 301) {
            header("StatusId: 301 Moved Permanently");
        }
        header(sprintf('Location: %s', $url));
        exit();
    }

    static function detectWebshopUserLanguage(): Language
    {

        if (preg_match('/^\/([a-z]{2})\/?/', Utils::getRequestUri(), $aParts)) {
            $oLanguageQuery = LanguageQuery::create();
            $oLanguageQuery->setQueryKey('LanguageQuery-findOneByShopUrlPrefix');
            $oLanguage = $oLanguageQuery->findOneByShopUrlPrefix($aParts[1]);
        } else {
            $oLanguageQuery = LanguageQuery::create();
            $oLanguageQuery->setQueryKey('LanguageQuery-findOneByIsDefaultWebshop');
            $oLanguage = LanguageQuery::create()->findOneByIsDefaultWebshop(true);
        }

        if (!$oLanguage instanceof Language) {
            $oLanguageQuery = LanguageQuery::create();
            $oLanguageQuery->setQueryKey('LanguageQuery-findOneByLocaleCode');
            $oLanguage = LanguageQuery::create()->findOneByLocaleCode('en_US');
        }
        $_COOKIE['prefered_language_id'] = $oLanguage->getId();

        return $oLanguage;
    }

    static function cleanPhoneNumber($sNumber)
    {
        return $sNumber;
    }

    static function getSnappyBinaryPath()
    {
        $sPath = Cfg::get('WKHTML_TO_PDF_PATH');
        if ($sPath) {
            return $sPath;
        }
        // ln -s /usr/bin/wkhtmltopdf /usr/local/bin/wkhtmltopdf

        return (isset($_SERVER['WINDIR'])) ? 'D:/programs/wkhtmltopdf/bin/wkhtmltopdf' : '/usr/local/bin/wkhtmltopdf';
    }

    private static function paginateUrl($sUrl, $iPage)
    {
        $aQuery = [];
        $sBaseUrl = $sUrl;
        if (strpos($sUrl, '?')) {
            $sBaseUrl = substr($sUrl, 0, strpos($sUrl, '?'));
            $sQueryPart = substr($sUrl, strpos($sUrl, '?') + 1, strlen($sUrl));
            parse_str($sQueryPart, $aQuery);
        }
        $aQuery['p'] = $iPage;
        $sUrl = $sBaseUrl . '?' . http_build_query($aQuery);

        return $sUrl;
    }

    static function paginate($sCurrentUrl, $iCurrentPage, $iResultCount, $iItemsPP)
    {
        $sNewUrl = $sCurrentUrl;
        $aOutput = [];
        $aOutput[] = '<ul class="pagination">';
        $iPagesNum = 0;
        if ($iItemsPP < $iResultCount) {
            $iPagesNum = ceil($iResultCount / $iItemsPP);
        }
        if ($iPagesNum > 11 && $iCurrentPage > 1) {
            $aOutput[] = "<li><a href=\"" . self::paginateUrl($sNewUrl, 1) . "\">&lt;&lt;</a></li>";
        }
        if ($iPagesNum > 11 && $iCurrentPage > 1) {
            $iFastForwardPageNum = (($iCurrentPage - 1) < 1) ? 1 : ($iCurrentPage - 1);
            $aOutput[] = "<li><a href=\"" . self::paginateUrl($sNewUrl, $iFastForwardPageNum) . "\">&lt;</a></li>";
        }
        for ($iC = 1; $iC <= $iPagesNum; $iC++) {
            if ($iC > ($iCurrentPage - 5) && $iC < ($iCurrentPage + 5)) {
                $sActive = ($iC == $iCurrentPage) ? 'active' : '';
                $aOutput[] = "<li class=\"$sActive\"><a href=\"" . self::paginateUrl($sNewUrl, $iC) . "\" class=\"$sActive\">$iC</a></li>";
            }
        }
        $iFastForward = (($iCurrentPage + 1) >= $iC) ? $iC : ($iCurrentPage + 1);
        if ($iPagesNum > 11 && $iFastForward != $iC) {
            $aOutput[] = "<li><a href=\"" . self::paginateUrl($sNewUrl, $iFastForward) . "\">&gt;</a></li>";
        }
        if ($iPagesNum > 11 && $iCurrentPage < ($iC - 1)) {
            $aOutput[] = "<li><a href=\"" . self::paginateUrl($sNewUrl, ($iC - 1)) . "\">&gt;&gt;</a></li>";
        }
        $aOutput[] = '</ul>';
        if (isset($aOutput) && is_array($aOutput)) {
            return join(' ', $aOutput);
        }

        return null;
    }

    static function readCachedFile($sFileName, $logger = null)
    {
        //get the last-modified-date of this very file
        $lastModified = filemtime($sFileName);
        //get a unique hash of this file (etag)
        $etagFile = md5_file(__FILE__);
        //get the HTTP_IF_MODIFIED_SINCE header if set
        // $ifModifiedSince = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
        //get the HTTP_IF_NONE_MATCH header if set (etag: unique file hash)
        $etagHeader = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);
        header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', (60 * 5))); // 5 minutes
        //set last-modified header
        header("Last-Modified: " . gmdate("D, d M Y H:i:s", $lastModified) . " GMT");
        //set etag-header
        header("Etag: $etagFile");
        //make sure caching is turned on
        header('Cache-Control: public');
        //check if page has changed. If not, send 304 and exit
        if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $lastModified || $etagHeader == $etagFile) {
            if ($logger && method_exists($logger, 'logAction')) {
                $logger->logAction('No need to load from disk, image cached.');
            }
            header("HTTP/1.1 304 Not Modified");
            exit;
        }
        if ($logger && method_exists($logger, 'logAction')) {
            $logger->logAction('Fetching requested file from disk. ' . $_GET['requested_file']);
        }
        readfile($sFileName);
    }

    /**
     * Use Header::contentType
     * @todo remove this
     * @deprecated
     * @param $sFileName
     */
    static function showHeader($sFileName)
    {
        $sExt = pathinfo($sFileName, PATHINFO_EXTENSION);
        if ($sExt == 'jpg' || $sExt == 'jpeg') {
            header('Content-type: image/jpeg');
        } else {
            if ($sExt == 'png') {
                header('Content-type: image/png');
            } else {
                throw new \Exception\LogicException("Unsupported file type");
            }
        }
    }

    static function tail($sFile, $iNumberOfLines)
    {
        $handle = fopen($sFile, "r");
        $linecounter = $iNumberOfLines;
        $pos = -2;
        $beginning = false;
        $text = [];
        while ($linecounter > 0) {
            $t = " ";
            while ($t != "\n") {
                if (fseek($handle, $pos, SEEK_END) == -1) {
                    $beginning = true;
                    break;
                }
                $t = fgetc($handle);
                $pos--;
            }
            $linecounter--;
            if ($beginning) {
                rewind($handle);
            }
            $text[$iNumberOfLines - $linecounter - 1] = fgets($handle);
            if ($beginning) {
                break;
            }
        }
        fclose($handle);

        return array_reverse($text);
    }

    static function flattenArray($aArray, $sKeyToDistill)
    {
        if (empty($aArray)) {
            return [];
        }
        $aOut = [];
        foreach ($aArray as $aItem) {
            if (isset($aItem[$sKeyToDistill]) && $aItem[$sKeyToDistill]) {
                $aOut[] = $aItem[$sKeyToDistill];
            }
        }

        return $aOut;
    }

    static function getDutchHolidayDays()
    {
        return ['new_year_day' => 'Nieuwjaar', 'good_friday' => 'Goede vrijdag', 'first_easterday' => '1e Paasdag', 'second_easterday' => '2e Paasdag', 'kingsday' => 'Koningsdag', 'ascension_day' => 'Hemelvaart', 'liberation_day' => 'Bevrijdingsdag', '1st_pentecost' => '1e Pinksterdag', '2nd_pentecost' => '2e Pinksterdag', '1st_chrismas_day' => '1e Kerstdag', '1nd_chrismas_day' => '2e Kerstdag', 'new_years_eve' => 'Oudejaarsdag',];
    }

    static function generateDropdownHourRanges()
    {
        $aOut = [];
        foreach (range(0, 24) as $h) {
            foreach (range(0, 45, 15) as $m) {
                $aOut[] = str_pad($h, 2, '0', STR_PAD_LEFT) . ':' . str_pad($m, 2, '0', STR_PAD_LEFT);
            }
        }

        return $aOut;
    }

    static function getRequestUri($bKeepGetVars = true, $aAddVars = null, $bMergeAddvarsRecursive = false)
    {
        if (!isset($_SERVER['REQUEST_URI'])) {
            return null;
        }
        $sRequestUri = $_SERVER['REQUEST_URI'];
        if (strpos($sRequestUri, '?') && $bKeepGetVars == false) {
            $sRequestUri = explode('?', $sRequestUri)[0];
        }
        if (is_array($aAddVars)) {
            $aData = [];
            if (strpos($sRequestUri, '?')) {
                $sRequestUriNoVars = explode('?', $sRequestUri)[0];
                parse_str(explode('?', $sRequestUri)[1], $aData);
            } else {
                $sRequestUriNoVars = $sRequestUri;
            }
            if ($bMergeAddvarsRecursive) {
                $aNewData = array_merge_recursive($aData, $aAddVars);
            } else {
                $aNewData = array_merge($aData, $aAddVars);
            }
            $sAddVars = http_build_query($aNewData);
            $sRequestUri = $sRequestUriNoVars . '?' . $sAddVars;
        }

        return $sRequestUri;
    }

    /**
     * @param \DateTime $oDateTime
     * @param bool $full
     * @return string
     * @throws \Exception
     */
    static function timeElapsed(\DateTime $oDateTime, $full = false)
    {
        $now = new \DateTime();
        $diff = $now->diff($oDateTime);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'jaar',
            'm' => 'maand',
            'w' => 'week',
            'd' => 'dag',
            'h' => 'uur',
            'i' => 'minuut',
            's' => 'seconde'
        );

        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v;

                if ($diff->$k > 1) {
                    $v = str_replace('jaar', 'jaren', $v);
                    $v = str_replace('maand', 'maanden', $v);
                    $v = str_replace('week', 'weken', $v);
                    $v = str_replace('dag', 'dagen', $v);
                    $v = str_replace('uur', 'uren', $v);
                    $v = str_replace('minuut', 'minuten', $v);
                    $v = str_replace('seconde', 'seconden', $v);
                }
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) {
            $string = array_slice($string, 0, 1);
        }
        $return = $string ? implode(', ', $string) . ' geleden' : 'nu online';

        if (trim($return) == '') {
            $return = '-';
        }

        return $return;
    }

    public static function slugify($sText, $sConnectChar = '-')
    {
        // replace non letter or digits by -
        $sText = preg_replace('~[^\\pL\d]+~u', $sConnectChar, $sText);
        // trim
        $sText = trim($sText, '-');
        // transliterate
        $sText = iconv('utf-8', 'us-ascii//TRANSLIT', $sText);
        // lowercase
        $sText = strtolower($sText);
        // remove unwanted characters
        $sText = preg_replace('~[^-\w]+~', '', $sText);
        if (empty($sText)) {
            return 'n-a';
        }

        return $sText;
    }

    static function priceDisplay($fPrice)
    {
        return number_format($fPrice, 2, ',', '.');
    }

    static function priceEuToUs($fPrice)
    {
        $fPrice = str_replace('.', '', $fPrice);
        $fPrice = str_replace(',', '.', $fPrice);

        return $fPrice;
    }

    static function makeUrl($sUrl, $aGetVars = [])
    {
        $aUrlParts = parse_url($sUrl);
        $aUrlConstruct = [];
        if (isset($aUrlParts['scheme'])) {
            $aUrlConstruct[] = $aUrlParts['scheme'] . '://';
        }
        if (isset($aUrlParts['host'])) {
            $aUrlConstruct[] = $aUrlParts['host'];
        }
        if (isset($aUrlParts['path'])) {
            $aUrlConstruct[] = $aUrlParts['path'];
        }
        $aOldQueryParts = [];
        if (isset($aUrlParts['query'])) {
            parse_str($aUrlParts['query'], $aOldQueryParts);
        }
        $aNewVars = $aOldQueryParts;
        if (!empty($aGetVars)) {
            $aNewVars = array_replace_recursive($aNewVars, $aGetVars);
        }
        if (!empty($aNewVars)) {
            $sQueryString = '?' . http_build_query($aNewVars);
            $aUrlConstruct[] = $sQueryString;
        }
        $sCompleteUrl = join('', $aUrlConstruct);

        return $sCompleteUrl;
    }

    /**
     * @param $aObjectArray
     * @param string $sLabelMethod
     * @param string|null $mDefaultValue
     * @param string $sKeyColumnOrMethod - defaults to "id" or "getId" depending on the type of $aObjectArray/
     * @return array
     */
    static function makeSelectOptions($aObjectArray, $sLabelMethod, string $mDefaultValue = null, string $sKeyColumnOrMethod = 'auto')
    {
        $aDropdownOptions = [];
        if (!empty($aObjectArray)) {
            foreach ($aObjectArray as $oObjectOrArray) {
                $sKeyColumn = null;
                if ($sKeyColumnOrMethod === 'auto') {
                    if (is_array($oObjectOrArray)) {
                        $sKeyColumn = 'id';
                    } elseif (is_object($oObjectOrArray)) {
                        $sKeyColumn = 'getId';
                    }
                } else {
                    $sKeyColumn = $sKeyColumnOrMethod;
                }

                $bSelected = false;
                $aValues = null;
                if (is_object($oObjectOrArray)) {
                    if (!method_exists($oObjectOrArray, $sLabelMethod)) {
                        throw new LogicException("When attempting to make dropdown options of an array of objects the system made me call $sLabelMethod on " . get_class($oObjectOrArray) . " but that method does not exist." . __METHOD__);
                    }
                    if (!method_exists($oObjectOrArray, 'getId')) {
                        throw new LogicException("When attempting to make dropdown options of an array of objects the system made me call getId on " . get_class($oObjectOrArray) . " but that method does not exist." . __METHOD__);
                    }
                    $bSelected = ($mDefaultValue == $oObjectOrArray->$sKeyColumn());

                    $aValues = [
                        'id' => $oObjectOrArray->getId(),
                        'label' => $oObjectOrArray->$sLabelMethod()
                    ];
                } elseif (is_array($oObjectOrArray)) {
                    $bSelected = ($mDefaultValue == $oObjectOrArray[$sKeyColumn]);

                    $aValues = [
                        'id' => $oObjectOrArray[$sKeyColumn],
                        'label' => $oObjectOrArray[$sLabelMethod]
                    ];
                }
                if ($bSelected) {
                    $aValues['selected'] = 'selected';
                }
                $aDropdownOptions[] = $aValues;
            }
        }
        return $aDropdownOptions;
    }

    static function jsonExit($aData)
    {
        echo json_encode($aData);
        exit();
    }

    static function jsonOk()
    {
        echo json_encode(['ok' => true]);
        exit();
    }

    static function usort_strcmp(&$aArray, $sKey)
    {
        usort($aArray, function ($a, $b) use ($sKey) {
            return strcmp($a[$sKey], $b[$sKey]);
        });
    }

    public static function getSupportedImageMimeTypes()
    {
        // Meer mime types nodig?
        // http://php.net/manual/en/function.image-type-to-mime-type.php
        return ['image/gif', 'image/jpeg', 'image/png',];
    }

    /*
     * Checks for bad byte sequence(s),
     */
    static function checkEncoding($string, $string_encoding)
    {
        $fs = $string_encoding == 'UTF-8' ? 'UTF-32' : $string_encoding;
        $ts = $string_encoding == 'UTF-32' ? 'UTF-8' : $string_encoding;

        return $string === mb_convert_encoding(mb_convert_encoding($string, $fs, $ts), $ts, $fs);
    }
}
