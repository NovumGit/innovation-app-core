<?php
namespace Core;

abstract class MainController extends BaseController{

    private $sLayout = 'layout.twig';

    function isPublic()
    {
        return false;
    }
    function getLayout()
    {
        return $this->sLayout;
    }
    function setLayout($sLayout)
    {
        $this->sLayout = $sLayout;
    }

    function addCssFile($sFileName = '/Modules/Blabla/blabla.js')
    {
        self::$aAutoloadCssFiles[] = $sFileName;
    }
    function addjsFiles(array $aFiles):void
    {
        foreach($aFiles as $sFile)
        {
            $this->addJsFile($sFile);
        }
    }
    function addJsFile($sFileName = '/Modules/Blabla/blabla.js /admin_modules/Blabla/blabla.js')
    {
        self::$aAutoloadJsFiles[] = $sFileName;
    }

    /**
     * @return array
     */
    abstract function run();
}