<?php
namespace Core\Asynchronous;

class Server
{

    /**
     * On it's own this is not asynchronous the way nodejs is. There is no event loop.
     * The port remains closed untill the request is handled. Use a queue to make something like an event loop.
     *
     * Currently not asynchronous.
     * @param int $iPort
     * @param IActionHandler|null $oHandler
     */
    static function listen($iPort = 6789, IActionHandler $oHandler = null)
    {
        $sock = socket_create_listen($iPort);
        socket_getsockname($sock, $sAddress, $iPort);
        // $sAddress = '127.0.0.1',

        print "Server Listening on $sAddress:$iPort\n";

        while($c = socket_accept($sock)) {
            /* do something useful */
            // socket_getpeername($c, $raddr, $rport);
           // $sMessage = socket_read($sock, 102);

            // accept incoming connections
            // spawn another socket to handle communication
            // read client input
            $sMessage = socket_read($c, 1024) or die("Could not read input\n");

            if($oHandler instanceof IActionHandler)
            {
                $oHandler->handle($sMessage);
            }
        }
        socket_close($sock);
    }
}