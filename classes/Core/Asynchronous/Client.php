<?php
namespace Core\Asynchronous;

class Client
{

    static function send($iPort, $sMessage)
    {
        $host = "127.0.0.1";
        $port = $iPort;
        set_time_limit(0);
        $socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Could not create socket\n");
        $result = socket_connect($socket, $host, $port) or die("Could not connect toserver\n");
        socket_write($socket, $sMessage, strlen($sMessage)) or die("Could not send data to server\n");
        socket_close($socket);
    }
}