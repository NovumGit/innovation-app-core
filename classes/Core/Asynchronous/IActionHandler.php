<?php
namespace Core\Asynchronous;

interface IActionHandler
{
    function handle(string $sMessage):void;
}
