<?php
namespace Core\ModelDecorator;

use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Connection\ConnectionInterface;

interface IPostSave
{
    function postSave(ActiveRecordInterface $oModel, ConnectionInterface $con = null);
}