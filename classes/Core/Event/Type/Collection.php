<?php
namespace Core\Event\Type;

use Model\System\Event\Event;

class Collection implements \Iterator
{
    private $aEvents = [];

    /**
     * @param Event[] $aEvents
     */
    public function __construct(array $aEvents = null)
    {
        $this->aEvents = $aEvents;
    }

    public function add(Event $oField)
    {
        $this->aEvents[] = $oField;
    }

    function next(): void
    {
        next($this->aEvents);
    }

    function valid(): bool
    {
        $key = key($this->aEvents);
        return ($key !== null && $key !== false);
    }

    function current(): Event
    {
        return current($this->aEvents);
    }

    function rewind()
    {
        reset($this->aEvents);
    }

    function key(): int
    {
        return key($this->aEvents);
    }
}
