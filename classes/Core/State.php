<?php
namespace Core;

use LowCode\State\IState;

/**
 * Simple wrapper function to store state, uses Setting under the hood but to keep things separated..
 * Class State
 * @package Core
 */
class State {

    static function store($sKey, IState $sVal)
    {
        $sVal = serialize($sVal);
        Setting::store('state_' . $sKey, $sVal);
    }

    static function get($sKey, $sDefault = 'not_set'):?IState
    {
        $sSate = Setting::get('state_' . $sKey);
        if(!$sSate)
        {
            return null;
        }
        return unserialize($sSate);
    }
}
