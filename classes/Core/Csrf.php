<?php
namespace Core;

use Exception\SecurityException;

class Csrf
{

    static function ok()
    {
        if(isset($_GET['csrf']))
        {
            $sToken = $_GET['csrf'];
        }

        if(isset($_POST['csrf']))
        {
            $sToken = $_POST['csrf'];
        }

        if(!isset($sToken))
        {
            throw new SecurityException('CSRF missing');
        }
        if($sToken == null)
        {
            throw new SecurityException('Empty CSRF');
        }


        return ($sToken == $_SESSION['csrf_token']);
    }
    static function getField()
    {
        if(!isset($_SESSION['csrf_token']) || $_SESSION['csrf_token'] == null)
        {
            $_SESSION['csrf_token'] = uniqid();
        }
        return '<input type="hidden" name="csrf" value="'.$_SESSION['csrf_token'].'" />';
    }
}

