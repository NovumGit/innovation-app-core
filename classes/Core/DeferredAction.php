<?php
namespace Core;

use Exception\InvalidArgumentException;

class DeferredAction {

    /**
     * Deze functie kun je gebruiken om een flow te definieren tussen, waar moet de gebruiker heen na het opslaan bijvoorbeeld
     * @param $sKey
     * @param string $sUrl
     */
    static function register($sKey, $sUrl = null)
    {

        if(!is_string($sUrl))
        {
            throw new InvalidArgumentException('Deferred action is expecting an url as argument');
        }

        $_SESSION['DEFERRED_ACTIONS'][$sKey] = $sUrl;
    }

    static function get($sKey)
    {
        if(isset($_SESSION['DEFERRED_ACTIONS'][$sKey]))
        {
            return $_SESSION['DEFERRED_ACTIONS'][$sKey];
        }
        return null;
    }
}
