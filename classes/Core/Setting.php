<?php
namespace Core;

use Model\System\SystemRegistry;
use Model\System\SystemRegistryQuery;

class Setting{

    static function store($sKey, $sVal)
    {
        $oSystemRegistryQuery = SystemRegistryQuery::create();
        $oSystemRegistry = $oSystemRegistryQuery->findOneByItemKey($sKey);

        if(!$oSystemRegistry instanceof SystemRegistry)
        {
            $oSystemRegistry = new SystemRegistry();
            $oSystemRegistry->setItemKey($sKey);
        }
        $oSystemRegistry->setItemValue($sVal);
        $oSystemRegistry->save();
    }
    static function clear(string $sKey)
    {
        $oSystemRegistryQuery = SystemRegistryQuery::create();
        $oSystemRegistry = $oSystemRegistryQuery->findOneByItemKey($sKey);
        if($oSystemRegistry instanceof SystemRegistry)
        {
            $oSystemRegistry->delete();
        }
    }
    static function get($sKey, $sDefault = 'not_set')
    {
        $oSystemRegistryQuery = SystemRegistryQuery::create();
        $oSystemRegistry = $oSystemRegistryQuery->findOneByItemKey($sKey);
        if(!$oSystemRegistry instanceof SystemRegistry && $sDefault != 'not_set')
        {
            self::store($sKey, $sDefault);
            return self::get($sKey);
        }
        else if($oSystemRegistry instanceof SystemRegistry)
        {

            return $oSystemRegistry->getItemValue();
        }

        return null;
    }
}
