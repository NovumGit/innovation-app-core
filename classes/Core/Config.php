<?php
namespace Core;

use Cli\Tools\CommandUtils;

class Config{


    static function getDataDir($bAbsolute = false)
    {
        if(isset($_SERVER['DATA_DIR']))
        {
            return $_SERVER['DATA_DIR'];
        }
        else if(isset($_SERVER['SYSTEM_ROOT']))
        {
            return $_SERVER['SYSTEM_ROOT'] .'/data';
        }
        else if(isset($_SERVER['PWD']) && strpos($_SERVER['SCRIPT_NAME'], 'build/tools/hurah'))
        {
            return CommandUtils::getRoot() .'/data';
        }
        else if($bAbsolute)
        {
            return Cfg::get('ABSOLUTE_ROOT') . '/data';
        }

        if(isset($_SERVER['PWD']) && strpos($_SERVER['PWD'], '/test/'))
        {
            // Running from phpunit
            $sDir = substr($_SERVER['PWD'], 0, strpos($_SERVER['PWD'], '/test/'));
            $sDir = $sDir.'/data';

            return $sDir;
        }

        if(isset($_SERVER['PWD']) && strpos($_SERVER['PWD'], '/cron') && strpos($_SERVER['PWD'], '/public_html/'))
        {
            // Running from cron folder in public_html
            return '../../../data';
        }

        return Environment::getCurrent() == 'frontend' ? '../../../data' : '../data';
    }
    static function getSecretKey()
    {
        return '12312387123798b21eifweifcvlkjwqn';
    }
    static function getBaseUrl()
    {
        $sBaseUrl = str_replace('admin.', '', $_SERVER['HTTP_HOST']);
        $sBaseUrl = str_replace('.nuidev', '', $sBaseUrl);
        $sBaseUrl = str_replace('www.', '', $sBaseUrl);
        if(strpos($sBaseUrl, ':')) { // evt poortnummer ook verwijderen.
            $sBaseUrl = explode(':', $sBaseUrl)[0];
        }
        return $sBaseUrl;
    }
    static function getSnappyBinaryPath(){
        return (isset($_SERVER['WINDIR']))
            ? 'D:/programs/wkhtmltopdf/bin/wkhtmltopdf'
            :
            '/usr/local/bin/wkhtmltopdf';
    }

    static function getAssetsDir()
    {
        if(isset($_SERVER['ASSETS_DIR']))
        {
            return $_SERVER['ASSETS_DIR'];
        }
        return self::getCustom();
    }
    static function getCustom()
    {
        if(isset($_SERVER['SYSTEM_ID']))
        {
            return $_SERVER['SYSTEM_ID'];
        }
        if(!isset($_SERVER['HTTP_HOST']))
        {
            return null;
        }
        $sCustom = str_replace('admin.live.', '', $_SERVER['HTTP_HOST']);
        $sCustom = str_replace('admin.test.', '', $sCustom);
        $sCustom = str_replace('dealers.test.', '', $sCustom);
        $sCustom = str_replace('admin.', '', $sCustom);
        $sCustom = str_replace('dealers.', '', $sCustom);
        $sCustom = str_replace('.nuidev', '', $sCustom);
        $sCustom = str_replace('.nuicart', '', $sCustom);
        $sCustom = preg_replace('/\.nl$/', '', $sCustom);
        $sCustom = preg_replace('/\.com$/', '', $sCustom);

        if(strpos($sCustom, ':')) {
            $sCustom = explode(':', $sCustom)[0];
        }
        return $sCustom;
    }
    static function getDateTimeFormat($bAddSeconds = false)
    {
        if($bAddSeconds)
        {
            return 'd-m-Y H:i:s';
        }
        return 'd-m-Y H:i';
    }
    static function deliveryDaysMap()
    {
        // Let op, labels hier allemaal in lowercase ongeacht wat er in de database staat.
        $aDeliveryDaysMap = [
            'same day' => 0,
            'next day' => 1,
            '3 werkdagen' => 3,
            '5 werkdagen' => 5,
            '7 werkdagen' => 7,
            '8 werkdagen' => 8,
        ];
        return $aDeliveryDaysMap;
    }
    static function getDateFormat()
    {
        // DD-MM-YYYY hh:mm -> dit is de js variant in custom.js, deze uiteraard gelijk houden!
        return 'd-m-Y';
    }
}
