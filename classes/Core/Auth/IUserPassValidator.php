<?php
namespace Core\Auth;

interface IUserPassValidator
{
    function validate(string $sUser, string $sPass):bool;
}
