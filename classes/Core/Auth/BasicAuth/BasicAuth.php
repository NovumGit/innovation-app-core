<?php
namespace Core\Auth\BasicAuth;

class BasicAuth
{
    const realm = 'Please login';
    private static function showLogin()
    {
        header('HTTP/1.1 401 Unauthorized');
        header('WWW-Authenticate: Digest realm="' . self::realm . '",qop="auth",nonce="' . uniqid() . '",opaque="' . md5(self::realm) . '"');
    }
    private static function httpDigestParse($txt)
    {
        // protect against missing data
        $needed_parts = ['nonce'    => 1,
                         'nc'       => 1,
                         'cnonce'   => 1,
                         'qop'      => 1,
                         'username' => 1,
                         'uri'      => 1,
                         'response' => 1,
        ];
        $data = [];
        $keys = implode('|', array_keys($needed_parts));

        preg_match_all('@(' . $keys . ')=(?:([\'"])([^\2]+?)\2|([^\s,]+))@', $txt, $matches, PREG_SET_ORDER);

        foreach ($matches as $m)
        {
            $data[$m[1]] = $m[3] ? $m[3] : $m[4];
            unset($needed_parts[$m[1]]);
        }

        return $needed_parts ? false : $data;
    }

    /**
     * Expects an array of users where the username is the key and the plain password is the value
     * @param array $aUsers
     * @return bool
     */
    static function authenticate(array $aUsers)
    {
        if (empty($_SERVER['PHP_AUTH_DIGEST']))
        {
            self::showLogin();
            die('Sorry we cannot let you in.');
        }

        // analyze the PHP_AUTH_DIGEST variable
        if (!($data = self::httpDigestParse($_SERVER['PHP_AUTH_DIGEST'])) || !isset($aUsers[$data['username']]))
        {
            self::showLogin();
            $_SERVER['PHP_AUTH_DIGEST'] = null;
            header('HTTP/1.1 401 Unauthorized');

            die('Sorry, wrong Credentials!');
        }

        // generate the validation response

        $A1 = md5($data['username'] . ':' . self::realm . ':' . $aUsers[$data['username']]);
        $A2 = md5($_SERVER['REQUEST_METHOD'] . ':' . $data['uri']);
        $valid_response = md5($A1 . ':' . $data['nonce'] . ':' . $data['nc'] . ':' . $data['cnonce'] . ':' . $data['qop'] . ':' . $A2);

        if ($data['response'] != $valid_response)
        {
            header('HTTP/1.1 401 Unauthorized');

            die('Wrong Credentials, sorry!');
        }

        // ok, valid username & password
        $_SESSION['HAS_ACCESS'] = true;
        $_SESSION['USER'] = [
            'username' => $data['username'],
            'password' => $aUsers[$data['username']],
        ];
        return true;
    }
}
