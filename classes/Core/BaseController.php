<?php
namespace Core;

abstract class BaseController extends Base{

    function makeUrl($sUrl, $aGetVars = [])
    {
        return Utils::makeUrl($sUrl, $aGetVars);
    }

}