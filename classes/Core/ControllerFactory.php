<?php
namespace Core;

use AdminModules\System\Installer\InstallationWizard;
use Exception\LogicException;
use AdminModules\Dashboard\HomeController;

class ControllerFactory implements IControllerFactory{

    /**
     * @param array $aGet
     * @param array $aPost
     * @param string $sNamespace
     * @throws
     * @return MainController
     */
    public function getController(array $aGet, array $aPost, string $sNamespace = null) : MainController{

        $sNamespaceRoot = Environment::getModuleRootNamespace($sNamespace);

        $sRequestUri = $_SERVER['REQUEST_URI'];
        if(Environment::getCurrent() == 'frontend')  {
            // Language code is in the url so languagecode logic here.

            // /en, /en/, /de, /de/, /es etc (alleen landcode of landcode met trailing slash)
            if(preg_match('/^\/[a-z]{2}\/?$/', $sRequestUri))
            {
                $sRequestUri = '/';
            }

            // /en/asdfasdf, , /de/asdfasf, etc (landcode en van alles daar achter)
            if(preg_match('/^\/[a-z]{2}\/(.+)/', $sRequestUri, $aMatches))
            {
                $sRequestUri = '/'.$aMatches[1];
            }
        }

        if(Setting::get('start_installer'))
        {
            return new InstallationWizard($_GET, $_POST);
        }
        if(strpos($sRequestUri, '?')){
            $sRequestUri = explode('?', $sRequestUri)[0];
        }

        $aParts = array_filter(explode('/', $sRequestUri));

        if(count($aParts) == 0)
        {
            $sFullQualifiedClassname = "\\$sNamespaceRoot\\Dashboard\\HomeController";
        }

        else if(count($aParts) == 2 && $aParts[1] == 'c')
        {
            $sModule = 'Category';
            $sController = 'OverviewController';
            $sFullQualifiedClassname = "\\$sNamespaceRoot\\$sModule\\$sController";

        }else if(count($aParts) == 1)
        {
            $sModule = ucfirst($aParts[1]);
            // $sController = ucfirst($aParts[1]);
            $sFullQualifiedClassname = "\\$sNamespaceRoot\\$sModule\\Controller";
        }
        else if(count($aParts) == 2)
        {
            $sModule = ucfirst($aParts[1]);
            $sController = ucfirst($aParts[2]);
            $sFullQualifiedClassname = "\\$sNamespaceRoot\\$sModule\\$sController".'Controller';
        }
        else if(count($aParts) == 3)
        {
            $sModule = ucfirst($aParts[1]);
            $sSubModule = ucfirst($aParts[2]);
            $sController = ucfirst($aParts[3]);
            $sFullQualifiedClassname = "\\$sNamespaceRoot\\$sModule\\$sSubModule\\$sController".'Controller';
        }
        else if(count($aParts) == 4)
        {
            $sModule = ucfirst($aParts[1]);
            $sSubModule = ucfirst($aParts[2]);
            $sSubSubModule = ucfirst($aParts[3]);
            $sController = ucfirst($aParts[4]);

            if($sModule == 'Custom')
            {
                $sSubModule = Cfg::get('CUSTOM_NAMESPACE');
            }

            $sFullQualifiedClassname = "\\$sNamespaceRoot\\$sModule\\$sSubModule\\$sSubSubModule\\$sController".'Controller';
        }
        else if(count($aParts) == 5)
        {
            $sModule = ucfirst($aParts[1]);
            $sSubModule = ucfirst($aParts[2]);

            if($sModule == 'Custom')
            {
                $sSubModule = Cfg::get('CUSTOM_NAMESPACE');
            }

            $sSubSubModule = ucfirst($aParts[3]);
            $sSubSubSubModule = ucfirst($aParts[4]);
            $sController = ucfirst($aParts[5]);
            $sFullQualifiedClassname = "\\$sNamespaceRoot\\$sModule\\$sSubModule\\$sSubSubModule\\$sSubSubSubModule\\$sController".'Controller';
            // exit($sFullQualifiedClassname);
        }
        else
        {
            throw new LogicException("I don't understand that url.");
        }

        if(Environment::getCurrent() == 'admin' && !User::isSignedIn())
        {
            if(isset($_GET['_do']) && preg_match('/^NoLogin/', $_GET['_do']))
            {
                // The _do method starts with NoLogin and then it may be called without login in.
            }
            else
            {
                $sFullQualifiedClassname = "\\$sNamespaceRoot\\Login\\Controller";
            }
        }
        else if(Environment::getCurrent() == 'frontend' && $sRequestUri == '/')
        {
            $sFullQualifiedClassname = "\\$sNamespaceRoot\\HomeController";
        }
        else if($sRequestUri == '/')
        {
            $sFullQualifiedClassname = "\\$sNamespaceRoot\\Dashboard\\HomeController";
        }

        if(!class_exists($sFullQualifiedClassname))
        {
            $_SESSION['CONTROLLER_WANTED'] = $sFullQualifiedClassname;
            $sFullQualifiedClassname = "\\$sNamespaceRoot\\Error\\PagenotFoundController";

            if(!class_exists($sFullQualifiedClassname))
            {
                throw new LogicException("Controller PagenotFoundController not found");
            }
        }
/*
        if(Environment::getCurrent() == 'admin')
        {

            $sFullModuleName = 'AdminModules\\'.ucfirst($sModule).'\\Config';
            $oModule = ModuleQuery::create()->findOneByName($sFullModuleName);

            if($oModule instanceof Module)
            {
                $oUser = User::getMember();

                if($oUser instanceof User)
                {
                    $oRole = User::getMember()->getRole();

                    $oModuleRoleQuery = ModuleRoleQuery::create();
                    $oModuleRoleQuery->filterByModuleId($oModule->getId());
                    $oModuleRoleQuery->filterByRoleId($oRole->getId());
                    $oModuleRole = $oModuleRoleQuery->findOne();

                    if(!$oModuleRole instanceof ModuleRole)
                    {
                        // guilty as charge
                        $bUserRoleEnabledModule = false;

                    }
                }
            }
        }
*/
        if(!class_exists($sFullQualifiedClassname))
        {
            throw new LogicException("Class not found $sFullQualifiedClassname ");
        }
        else if(class_exists($sFullQualifiedClassname))
        {
            $oController = new $sFullQualifiedClassname($aGet, $aPost);
        }
        else
        {
            $oController = new HomeController($aGet, $aPost);
        }
        return $oController ;

/*
 * Dit brak in de frontend, oplossing voor zoeken morgen!
        if($oController->isPublic() || $bUserRoleEnabledModule)
        {
            return $oController;
        }
        else
        {
            $sNoAccessController = "\\$sNamespaceRoot\\Error\\NoAccessController";
            return new $sNoAccessController;
        }
*/
    }
}
