<?php
namespace Core\Mime;

class VoidMime implements Mime{

    function getCode()
    {
        return 'void';
    }
}
