<?php
namespace Core\Mime;

class JpgMime implements Mime, IContentType {

    final function getCode()
    {
        return 'image';
    }

    /**
     * @return string
     */
    final function getContentType():string
    {
        return 'image/jpeg';
    }
}
