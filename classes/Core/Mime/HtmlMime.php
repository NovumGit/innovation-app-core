<?php
namespace Core\Mime;

class HtmlMime implements Mime{

    function getCode()
    {
        return 'html';
    }
}
