<?php
namespace Core\Mime;

interface Mime{

    public function getCode();
}
