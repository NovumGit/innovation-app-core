<?php
namespace Core\Mime;

class XmlMime implements Mime, IContentType {

    final function getCode()
    {
        return 'xml';
    }

    /**
     * @return string
     */
    final function getContentType():string
    {
        return 'text/xml';
    }
}
