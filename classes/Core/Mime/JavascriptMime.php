<?php
namespace Core\Mime;

class JavascriptMime implements Mime, IContentType {

    public static function getContentType(): string
    {
        return 'application/javascript';
    }

    function getCode()
    {
        return 'js';
    }
}
