<?php
namespace Core\Mime;

interface IContentType
{
    public function getContentType():string;
}
