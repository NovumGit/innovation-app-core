<?php
namespace Core\Mime;

class FileMime implements Mime{

    function getCode()
    {
        return 'file';
    }
}
