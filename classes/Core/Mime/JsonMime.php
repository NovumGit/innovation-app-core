<?php
namespace Core\Mime;

class JsonMime implements Mime, IContentType {

    public function getContentType(): string
    {
        return 'application/json';
    }

    function getCode()
    {
        return 'json';
    }
}
