<?php
namespace Core\Mime;

class ExcelMime implements Mime {

    function getCode()
    {
        return 'xls';
    }
}
