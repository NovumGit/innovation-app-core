<?php
namespace Core\Mime;

class PdfMime implements Mime, IContentType {

    public static function getContentType(): string
    {
        return 'application/pdf"';
    }

    function getCode()
    {
        return 'pdf';
    }
}
