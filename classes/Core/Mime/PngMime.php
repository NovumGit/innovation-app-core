<?php
namespace Core\Mime;

class PngMime implements Mime, IContentType {

    final function getCode()
    {
        return 'image';
    }
    /**
     * @return string
     */
    final function getContentType():string
    {
        return 'image/png';
    }
}
