<?php
namespace Core;

use Model\User\UserActivity;
use Model\User\UserActivityQuery;
use Model\Setting\MasterTable\ActivityType;
use Model\Setting\MasterTable\ActivityTypeQuery;
use Core\User;

class LogActivity{

    static function register($sModule, $sEvent, $sDescription, $mData = [])
    {
        $oUser = User::getMember();


        $oActivityTypeQuery = ActivityTypeQuery::create();
        $oActivityType = $oActivityTypeQuery->findOneByModuleNameAndEventName($sModule, $sEvent);

        if(!$oActivityType instanceof ActivityType)
        {
            $oActivityType = new ActivityType();
            $oActivityType->setModuleName($sModule);
            $oActivityType->setEventName($sEvent);
            $oActivityType->save();
        }

        $oUserActivity = new UserActivity();
        $oUserActivity->setUserId($oUser->getId());
        $oUserActivity->setActivityTypeId($oActivityType->getId());
        $oUserActivity->setTitle($sDescription);

        if(!empty($mData))
        {
            $oUserActivity->setActivityData(json_encode($mData));
        }
        $oUserActivity->save();



    }
}

