<?php
namespace Core;


use Exception\LogicException;

class StatusMessage extends AbstractPopup {

    /**
     * Gebruik deze methode om de gebruiker algemene informatie te geven.
     * @param $sMessage
     * @param $sTitleOverride
     * @param $aStatusMessageButtons
     */
    static function info(string $sMessage, string $sTitleOverride = null, array $aStatusMessageButtons = null){
        self::register($sMessage, 'info', $sTitleOverride, $aStatusMessageButtons);
    }

    /**
     * Gebruik deze methode na het opslaan van een formulier of als er iets goed is verlopen.
     * @param $sMessage
     * @param $sTitleOverride
     * @param $aStatusMessageButtons
     */
    static function success(string $sMessage, string $sTitleOverride = null, array $aStatusMessageButtons = null){
        self::register($sMessage, 'success', $sTitleOverride, $aStatusMessageButtons);
    }

    /**
     * Gebruik deze methode wanneer de gebruiker ergens voor op moet passen, hij dreigt wat te vergeten bijvoorbeeld
     * @param $sMessage
     * @param $sTitleOverride
     * @param $aStatusMessageButtons
     */
    static function alert(string $sMessage, string $sTitleOverride = null, array $aStatusMessageButtons = null){
        self::register($sMessage, 'alert', $sTitleOverride, $aStatusMessageButtons);
    }
    /**
     * Gebruik deze methode als een formulier validatie bijvoorbeeld niet goed is gegaan.
     * @param $sMessage
     * @param $sTitleOverride
     * @param $aStatusMessageButtons
     */
    static function warning(string $sMessage, string $sTitleOverride = null, array $aStatusMessageButtons = null){
        self::register($sMessage, 'warning', $sTitleOverride, $aStatusMessageButtons);
    }

    /**
     * Gebruik deze methode wanneer er serieus iets fout is gegaan
     * @param $sMessage
     * @param $sTitleOverride
     * @param $aStatusMessageButtons
     */
    static function danger(string $sMessage, string $sTitleOverride = null, array $aStatusMessageButtons = null){
        self::register($sMessage, 'danger', $sTitleOverride, $aStatusMessageButtons);
    }

    private static function register(string $sMessageContents, string $sMessageType, string $sTitleOverride = null, array $aStatusMessageButtons = null)
    {
        $aButtons = self::buttonArray($aStatusMessageButtons, $sMessageType);

        if(!isset(self::$aSupportedTypes[$sMessageType])){
            throw new LogicException("StatusId message should be of any of these types: ".join(PHP_EOL, array_keys(self::$aSupportedTypes)));
        }

        if(!isset($_SESSION['status_messages']))
        {
            $_SESSION['status_messages'] = null;
        }

        $sMessageTypeColor = $sMessageType;
        if($sMessageType == 'success')
        {
            $sMessageTypeColor = 'success';
        }
        if($sTitleOverride)
        {
            $sLabel = $sTitleOverride;
        }
        else
        {
            $sLabel = self::$aSupportedTypes[$sMessageType];
        }


        $_SESSION['status_messages'][] =
            [
                'type' => $sMessageTypeColor,
                'contents' => $sMessageContents,
                'label' => $sLabel,
                'buttons' => $aButtons
            ];
    }
    static function hasStatusMessages()
    {
        return !empty($_SESSION['status_messages']);
    }
    static function getClearStatusMessages()
    {
        $aStatusMessages = $_SESSION['status_messages'];
        $_SESSION['status_messages'] = null;
        return $aStatusMessages;
    }
}
