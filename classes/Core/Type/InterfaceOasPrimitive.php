<?php
namespace Core\Type;

/**
 * The primitive types that implement this interface are the types that are supported in the open api specification.
 * https://github.com/OAI/OpenAPI-Specification/blob/master/versions/3.0.2.md
 *
 * Interface InterfaceOasPrimitive
 *
 * @package Core\Type
 */
interface InterfaceOasPrimitive {

    function getLabel():string;
}
