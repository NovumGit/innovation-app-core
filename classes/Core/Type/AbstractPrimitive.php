<?php
namespace Core\Type;

abstract class AbstractPrimitive
{
    abstract function getLabel():string;

    public function __toString() {
        return $this->getLabel();
    }
}

