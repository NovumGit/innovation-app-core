<?php
namespace Core\Type\Primitive;

use Core\Type\AbstractPrimitive;
use Core\Type\InterfaceOasPrimitive;

class Boolean extends AbstractPrimitive implements InterfaceOasPrimitive
{
    function getLabel(): string
    {
        return 'boolean';
    }
}

