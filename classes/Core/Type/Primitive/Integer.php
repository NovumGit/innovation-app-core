<?php
namespace Core\Type\Primitive;

use Core\Type\AbstractPrimitive;
use Core\Type\InterfaceOasPrimitive;

class Integer extends AbstractPrimitive implements InterfaceOasPrimitive
{
    function getLabel(): string
    {
        return 'integer';
    }
}


