<?php
namespace Core\Type\Primitive;

use Core\Type\AbstractPrimitive;
use Core\Type\InterfaceOasPrimitive;

/**
 * String is a reserved word in php7 hence the name StringType.
 *
 * Class StringType
 * @package Core\Type\Primitive
 */
class StringType extends AbstractPrimitive implements InterfaceOasPrimitive{

    function getLabel(): string
    {
        return 'string';
    }
}

