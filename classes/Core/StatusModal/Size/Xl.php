<?php

namespace Core\StatusModal\Size;

use Core\StatusModal\ISize;

class Xl implements ISize
{

    public function getClass(): string
    {
        return 'xl';
    }
}
