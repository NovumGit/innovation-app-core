<?php
namespace Core\StatusModal\Size;

use Core\StatusModal\ISize;

class Sm implements ISize
{

    public function getClass(): string
    {
        return 'sm';
    }
}
