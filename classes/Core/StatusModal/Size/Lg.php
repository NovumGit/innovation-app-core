<?php
namespace Core\StatusModal\Size;

use Core\StatusModal\ISize;

class Lg implements ISize
{
    public function getClass(): string
    {
        return 'lg';
    }

}
