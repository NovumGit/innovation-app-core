<?php
namespace Core\StatusModal\Size;

use Core\StatusModal\ISize;

class Full implements ISize
{
    public function getClass(): string
    {
        return 'full';
    }

}
