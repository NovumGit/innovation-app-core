<?php
namespace Core\StatusModal\Size;

use Core\StatusModal\ISize;

class Basic implements ISize
{
    public function getClass(): string
    {
        return 'basic';
    }
}
