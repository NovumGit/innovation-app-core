<?php
namespace Core\StatusModal;

use Core\StatusModal\Size\Basic;


class StatusModalArguments
{
    private $size;
    private $bButtonsEnabled = true;

    function __construct()
    {
        $this->size = new Basic();
    }

    /**
     * Turns off the button bar so also removes any default buttons.
     */
    public function disableButtons()
    {
        $this->bButtonsEnabled = false;
    }
    public function buttonsEnabled():bool
    {
        return $this->bButtonsEnabled;
    }
    public function setSize(ISize $size)
    {
        $this->size = $size;
    }

    public function getSize()
    {
        return $this->size;
    }

}
