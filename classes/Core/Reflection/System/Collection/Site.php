<?php

namespace Core\Reflection\System\Collection;

use ArrayIterator;
use Core\Reflection\System\Item\Site as ApiItem;

class Site extends ArrayIterator
{
    function __construct($array = [], $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    function current(): ApiItem
    {
        return parent::current();
    }

}
