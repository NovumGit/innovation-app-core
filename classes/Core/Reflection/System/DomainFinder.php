<?php
namespace Core\Reflection\System;

use Hurah\Types\Type\SystemId;
use Core\Reflection\ISystem;
use DirectoryIterator;
use Hi\Helpers\DirectoryStructure;
use Core\Reflection\System\Item\Domain as DomainItem;
use Core\Reflection\System\Collection\Domain as DomainCollection;

class DomainFinder implements ISystem
{

    function find():DomainCollection
    {
        $oDirectoryStructure = new DirectoryStructure();
        $sDomainDir = $oDirectoryStructure->getDomainDir(true);
        $oDirectoryIterator = new DirectoryIterator($sDomainDir);
        $aDomains = [];
        foreach ($oDirectoryIterator as $oDirectory)
        {
            if(!$oDirectory->isDir() || $oDirectory->isDot())
            {
                continue;
            }

            $aDomains[] = new DomainItem(new SystemId($oDirectory->getFilename()));
        }

        return new DomainCollection($aDomains);

    }
}
