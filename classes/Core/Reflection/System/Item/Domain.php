<?php

namespace Core\Reflection\System\Item;

use Hurah\Types\Type\Composer;
use Hurah\Types\Type\Composer\RepositoryType;
use Hurah\Types\Type\Path;
use Hurah\Types\Type\SystemId;
use Core\Utils;
use Exception\InvalidArgumentException;
use Hi\Helpers\DirectoryStructure;

class Domain
{
    private SystemId $oSystemId;

    function __construct(SystemId $oSystemID)
    {
        $this->oSystemId = $oSystemID;
    }

    function getDomainRoot(): Path
    {
        $oDirectoryStructure = new DirectoryStructure();
        return new Path(Utils::makePath($oDirectoryStructure->getDomainDir(true), $this->oSystemId));
    }

    /**
     * Returns the origin of the plugin, possible options: [local, git, packagist]
     * @return string
     */
    function getRepositoryType(): Composer\Repository {
        $oDirectoryStructure = new DirectoryStructure();
        $sMainComposerFile = Utils::makePath($oDirectoryStructure->getSystemRoot(), 'composer.json');
        $sMainComposerContents = file_get_contents($sMainComposerFile);
        $oComposer = new Composer($sMainComposerContents);

        foreach ($oComposer->getRepositoryList()->getValue() as $repository)
        {
            if (!$repository instanceof Composer\Repository)
            {
                throw new InvalidArgumentException("Expected an instance of Repository");
            }
            $sRepositoryLocation  = Utils::makePath($oDirectoryStructure->getSystemRoot(), $repository->getLocation());
            $sPossibleComposerFile = Utils::makePath($sRepositoryLocation, 'composer.json');

            if (file_exists($sPossibleComposerFile))
            {
                $sComposerFileContents = file_get_contents($sPossibleComposerFile);
                $oComposer = new Composer($sComposerFileContents);

                if ((string)$oComposer->getName() == (string)$this->getComposer()->getName())
                {
                    return new Composer\Repository([
                        'type' => new RepositoryType(Composer\RepositoryType::TYPE_PATH),
                        'url'  => $sRepositoryLocation
                    ]);
                }
            }
        }
        return new Composer\Repository([
                'type' => new RepositoryType(Composer\RepositoryType::TYPE_COMPOSER),
                'url'  => "https://www.packagist.org/packages/{$this->getComposer()->getName()}",
            ]);
    }

    function getComposerPath(): Path
    {
        return new Path(Utils::makePath($this->getDomainRoot(), "composer.json"));
    }
    function getComposer():Composer
    {
        return Composer::fromPath($this->getComposerPath());
    }

}
