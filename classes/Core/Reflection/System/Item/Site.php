<?php

namespace Core\Reflection\System\Item;

use Hurah\Types\Type\Composer;
use Hurah\Types\Type\Composer\RepositoryType;
use Hurah\Types\Type\Path;
use Hurah\Types\Type\SiteJson;
use Core\Utils;
use Exception\InvalidArgumentException;
use Hi\Helpers\DirectoryStructure;

class Site
{
    private Path $sSiteDir;

    function __construct(Path $sSiteDir)
    {
        $this->sSiteDir = $sSiteDir;
    }

    function getPublicRoot(): Path
    {
        $oDirectoryStructure = new DirectoryStructure();
        return new Path(Utils::makePath($oDirectoryStructure->getPublicDir(true), $this->sSiteDir));
    }

    function getSite(): SiteJson
    {
        return new SiteJson(Utils::makePath($this->getPublicRoot(), 'site.json'));
    }

    function getRepositoryType(): Composer\Repository {
        $oDirectoryStructure = new DirectoryStructure();
        $sMainComposerFile = Utils::makePath($oDirectoryStructure->getSystemRoot(), 'composer.json');
        $sMainComposerContents = file_get_contents($sMainComposerFile);
        $oComposer = new Composer($sMainComposerContents);

        foreach ($oComposer->getRepositoryList()->getValue() as $repository)
        {
            if (!$repository instanceof Composer\Repository)
            {
                throw new InvalidArgumentException("Expected an instance of Repository");
            }
            $sRepositoryLocation  = Utils::makePath($oDirectoryStructure->getSystemRoot(), $repository->getLocation());
            $sPossibleComposerFile = Utils::makePath($sRepositoryLocation, 'composer.json');

            if (file_exists($sPossibleComposerFile))
            {
                $sComposerFileContents = file_get_contents($sPossibleComposerFile);
                $oComposer = new Composer($sComposerFileContents);

                if ((string)$oComposer->getName() == (string)$this->getComposer()->getName())
                {
                    return new Composer\Repository([
                        'type' => new RepositoryType(Composer\RepositoryType::TYPE_PATH),
                        'url'  => $sRepositoryLocation
                    ]);
                }
            }
        }
        return new Composer\Repository([
            'type' => new RepositoryType(Composer\RepositoryType::TYPE_COMPOSER),
            'url'  => "https://www.packagist.org/packages/{$this->getComposer()->getName()}",
        ]);
    }
    function getComposerPath(): Path
    {
        return new Path(Utils::makePath($this->getPublicRoot(), 'composer.json'));
    }

    function getComposer(): Composer
    {
        return Composer::fromPath($this->getComposerPath());
    }

}
