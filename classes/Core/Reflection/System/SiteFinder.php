<?php
namespace Core\Reflection\System;

use Hurah\Types\Type\Path;
use Core\Reflection\ISystem;
use Core\Reflection\System\Item\Site;
use DirectoryIterator;
use Hi\Helpers\DirectoryStructure;
use Core\Reflection\System\Collection\Site as ApiCollection;

class SiteFinder implements ISystem
{

    function find():ApiCollection
    {
        $oDirectoryStructure = new DirectoryStructure();
        $sPublicDir = $oDirectoryStructure->getPublicDir(true);

        $oDirectoryIterator = new DirectoryIterator($sPublicDir);
        $aApis = [];
        foreach ($oDirectoryIterator as $oDirectory)
        {
            if(!$oDirectory->isDir() || $oDirectory->isDot())
            {
                continue;
            }

            $aApis[] = new Site(new Path($oDirectory->getFilename()));
        }

        return new ApiCollection($aApis);

    }
}
