<?php
namespace Core;


use Core\Mime\IContentType;
use Core\Mime\JpgMime;
use Core\Mime\JsonMime;
use Core\Mime\Mime;
use Core\Mime\PngMime;

class Header
{
    /**
     * @param $sFileName
     */
    static function showHeader($sFileName):void
    {
        $sExt = pathinfo($sFileName, PATHINFO_EXTENSION);

        if ($sExt == 'jpg' || $sExt == 'jpeg')
        {
            self::contentType(new JpgMime());
            return;
        }
        else if ($sExt == 'png')
        {

            self::contentType(new PngMime());
            return;
        }

        throw new \Exception\LogicException("Unsupported file type $sExt");
    }

    public static function sendStatusCode(HttpStatusCode $oStatusCode)
    {
        header($oStatusCode->getHeader());
    }


    public static function contentType(IContentType $oMime):void
    {
        header('Content-Type: ' . $oMime->getContentType());
    }
}
