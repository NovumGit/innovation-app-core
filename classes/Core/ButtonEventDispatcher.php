<?php
namespace Core;

use Clegginabox\PDFMerger\PDFMerger;
use Core\Mime\ExcelMime;
use Core\Mime\HtmlMime;
use Core\Mime\JavascriptMime;
use Core\Mime\PdfMime;
use Core\Mime\VoidMime;
use Core\Mime\ZipDirMime;
use Exception\InvalidArgumentException;
use Exception\LogicException;
use Model\Setting\CrudManager\Base\CrudEditorButtonEventQuery;
use Model\Setting\CrudManager\CrudViewButtonEventQuery;
use Propel\Runtime\ActiveQuery\Criteria;

class ButtonEventDispatcher{

    private static $aPdfFilesToMerge = [];
    private static $sJavascriptToRun = null;
    /**
     * @param int $iButtonId
     * @param array $aOverwriteRequest
     * @param string $sEnvironment Should be overview or edit, depends from where we call this event.
     * @param bool $bIsLast when calling Dispatch for multiple orders we need to know if this is the last item so we can output the pdf. (we need to merge all pdfs first)
     * @return bool
     * @throws \Exception
     */
    static function Dispatch($iButtonId, $aOverwriteRequest = [], $sEnvironment = 'edit', $bIsLast = true)
    {
        if(!in_array($sEnvironment, ['edit', 'overview']))
        {
            throw new InvalidArgumentException("Only edit and overview are supported here as argument.");
        }

        // ini_set('display_errors', 0);

        if (ob_get_contents())
        {
            ob_end_clean();
        }

        ob_start();

        if($sEnvironment == 'edit')
        {
            $CrudEditorButtonEvent = CrudEditorButtonEventQuery::create();
            $CrudEditorButtonEvent->filterByCrudEditorButtonId($iButtonId);
            $CrudEditorButtonEvent->orderBySorting(Criteria::DESC);
            $aEvents = $CrudEditorButtonEvent->find();
        }
        else if($sEnvironment == 'overview')
        {
            $oCrudViewButtonEventQuery = CrudViewButtonEventQuery::create();
            $oCrudViewButtonEventQuery->filterByCrudViewButtonId($iButtonId);
            $oCrudViewButtonEventQuery->orderBySorting(Criteria::DESC);
            $aEvents = $oCrudViewButtonEventQuery->find();
        }
        else
        {
            throw new LogicException("Not implemented");
        }
        $sJavascript = null;
        $bHasOutput = false;

        if(!empty($aEvents))
        {
            foreach($aEvents as $oEventRecord)
            {
                $sEventClass = $oEventRecord->getEventClass();
                $oEvent = new  $sEventClass(array_merge($_GET, $aOverwriteRequest), array_merge($_POST, $aOverwriteRequest));

                if($oEvent instanceof AbstractEvent)
                {
                    $oEvent->setButtonEventId($oEventRecord->getId());
                    $oEventOutput = $oEvent->trigger($sEnvironment);

                    if($oEvent->outputType() instanceof ExcelMime)
                    {
                        $bHasOutput = true;
                    }
                    else if($oEvent->outputType() instanceof PdfMime)
                    {
                        $bHasOutput = true;

                        $sTmpFileName = Config::getDataDir().'/tmp/'.uniqid().'-'.rand(0, 1000).'.pdf';
                        Logger::info($sTmpFileName);
                        self::$aPdfFilesToMerge[] = $sTmpFileName;
                        file_put_contents($sTmpFileName, $oEventOutput);
                    }
                    else if($oEvent->outputType() instanceof JavascriptMime)
                    {
                        self::$sJavascriptToRun .= ob_get_clean();
                    }
                    else if($oEvent->outputType() instanceof ZipDirMime)
                    {
                        $bHasOutput = true;
                        // Doen hier nog even niets.
                    }
                    else if($oEvent->outputType() instanceof HtmlMime)
                    {
                        $bHasOutput = true;
                    }
                    else if(!$oEvent->outputType() instanceof VoidMime)
                    {
                        throw new LogicException("Event type is not supported.");
                    }
                }
			}
        }

        if(!isset($oEvent))
        {
            return false;
        }

        if($bIsLast)
        {
            if($oEvent->outputType() instanceof ExcelMime)
            {
                if($oEventOutput instanceof \Excel)
                {
                    $oEventOutput->download('shippinglist');
                    exit();
                }
            }
            else if($oEvent->outputType() instanceof ZipDirMime)
            {
                ZipDir::download('download.zip');
                ZipDir::reset();
                exit();
            }
            if(self::$sJavascriptToRun)
            {
                // This javascript is executed on returning to the previous page.
                // Not always a good solution but when there is no output on the button it will work.
                $_SESSION['event_javascript_to_run'] = self::$sJavascriptToRun;
            }

            /*
             *  Voorbeelden in gebruik
             *      $pdf = new \Clegginabox\PDFMerger\PDFMerger;

                    $pdf->addPDF('samplepdfs/one.pdf', '1, 3, 4');
                    $pdf->addPDF('samplepdfs/two.pdf', '1-2');
                    $pdf->addPDF('samplepdfs/three.pdf', 'all');

                    //You can optionally specify a different orientation for each PDF
                    $pdf->addPDF('samplepdfs/one.pdf', '1, 3, 4', 'L');
                    $pdf->addPDF('samplepdfs/two.pdf', '1-2', 'P);

                    $pdf->merge('file', 'samplepdfs/TEST2.pdf', 'P');

                    // REPLACE 'file' WITH 'browser', 'download', 'string', or 'file' for output options
                    // Last parameter is for orientation (P for protrait, L for Landscape).
                    // This will be used for every PDF that doesn't have an orientation specified
            */
            if(!empty(self::$aPdfFilesToMerge))
            {
                self::$aPdfFilesToMerge = array_reverse(self::$aPdfFilesToMerge);
                $oPdfMerger = new \Jurosh\PDFMerge\PDFMerger();
                foreach(self::$aPdfFilesToMerge as $sTmpFile)
                {
                    $oPdfMerger->addPDF($sTmpFile);
                }
                $oPdfMerger->merge('browser', 'samengevoegd.pdf', 'P');
                // $oPdfMerger->merge('file', '../tmp/samengevoegd.pdf', 'P');
                foreach(self::$aPdfFilesToMerge as $sTmpFile)
                {
                   unlink($sTmpFile);
                }
                // Re-initialize
                self::$aPdfFilesToMerge = [];
            }
        }
        return $bHasOutput;
    }
}
