<?php

namespace Core;

use Core\StatusModal\StatusModalArguments;
use Throwable;

class StatusModal extends AbstractPopup
{

    /**
     * Gebruik deze methode om de gebruiker algemene informatie te geven.
     * @param string $sMessage
     * @param string $sTitleOverride
     * @param array $aStatusMessageButtons
     * @param StatusModalArguments|null $oStatusModalArguments
     * @return null
     */
    static function info(string $sMessage, string $sTitleOverride = null, array $aStatusMessageButtons = null, StatusModalArguments $oStatusModalArguments = null)
    {
        self::register($sMessage, 'info', $sTitleOverride, $aStatusMessageButtons, $oStatusModalArguments);
        return null;
    }

    /**
     * Gebruik deze methode na het opslaan van een formulier of als er iets goed is verlopen.
     * @param string $sMessage
     * @param string $sTitleOverride
     * @param array $aStatusMessageButtons
     * @param StatusModalArguments|null $oStatusModalArguments
     * @return null
     */
    static function success(string $sMessage, string $sTitleOverride = null, array $aStatusMessageButtons = null, StatusModalArguments $oStatusModalArguments = null)
    {
        self::register($sMessage, 'success', $sTitleOverride, $aStatusMessageButtons, $oStatusModalArguments);
        return null;
    }

    /**
     * Gebruik deze methode wanneer de gebruiker ergens voor op moet passen, hij dreigt wat te vergeten bijvoorbeeld
     * @param string $sMessage
     * @param string $sTitleOverride
     * @param array $aStatusMessageButtons
     * @param StatusModalArguments|null $oStatusModalArguments
     * @return null
     */
    static function alert(string $sMessage, string $sTitleOverride = null, array $aStatusMessageButtons = null, StatusModalArguments $oStatusModalArguments = null)
    {
        self::register($sMessage, 'alert', $sTitleOverride, $aStatusMessageButtons, $oStatusModalArguments);
        return null;
    }

    /**
     * Gebruik deze methode als een formulier validatie bijvoorbeeld niet goed is gegaan.
     * @param string $sMessage
     * @param string $sTitleOverride
     * @param array $aStatusMessageButtons
     * @param StatusModalArguments|null $oStatusModalArguments
     * @return null
     */
    static function warning(string $sMessage, string $sTitleOverride = null, array $aStatusMessageButtons = null, StatusModalArguments $oStatusModalArguments = null)
    {
        self::register($sMessage, 'warning', $sTitleOverride, $aStatusMessageButtons, $oStatusModalArguments);
        return null;
    }

    /**
     * Gebruik deze methode wanneer er serieus iets fout is gegaan
     * @param string $sMessage
     * @param string $sTitleOverride
     * @param array $aStatusMessageButtons
     * @param StatusModalArguments|null $oStatusModalArguments
     * @return null
     */
    static function danger(string $sMessage, string $sTitleOverride = null, array $aStatusMessageButtons = null, StatusModalArguments $oStatusModalArguments = null)
    {
        self::register($sMessage, 'danger', $sTitleOverride, $aStatusMessageButtons, $oStatusModalArguments);
        return null;
    }

    /**
     * @param string $sMessageContents
     * @param string $sMessageType
     * @param string|null $sTitleOverride
     * @param array|null $aStatusMessageButtons
     * @return string
     * @throws Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    public static function asString(string $sMessageContents, string $sMessageType, string $sTitleOverride = null, array $aStatusMessageButtons = null, StatusModalArguments $oStatusModalArguments = null): string
    {
        $aModal = self::asArray($sMessageContents, $sMessageType, $sTitleOverride, $aStatusMessageButtons);
        return TemplateFactory::parse('Generic/status_modals.twig', ['status_modals' => [$aModal]]);
    }

    private static function asArray(string $sMessageContents, string $sMessageType, string $sTitleOverride = null, array $aStatusMessageButtons = null, StatusModalArguments $oStatusModalArguments = null): array
    {
        if($oStatusModalArguments === null)
        {
            $oStatusModalArguments = new StatusModalArguments();
        }
        $aButtons = self::buttonArray($aStatusMessageButtons, $sMessageType);

        $sMessageTypeColor = $sMessageType;
        if ($sMessageType == 'success')
        {
            $sMessageTypeColor = 'info';
        }
        if ($sTitleOverride)
        {
            $sLabel = $sTitleOverride;
        } else
        {
            $sLabel = self::$aSupportedTypes[$sMessageType];
        }

        if (!isset($_SESSION['status_modals']))
        {
            $_SESSION['status_modals'] = null;
        }
        return [
            'type'            => $sMessageTypeColor,
            'contents'        => $sMessageContents,
            'label'           => $sLabel,
            'buttons'         => $aButtons,
            'arguments'       => $oStatusModalArguments,
        ];
    }

    private static function register(string $sMessageContents, string $sMessageType, string $sTitleOverride = null, array $aStatusMessageButtons = null, StatusModalArguments $oStatusModalArguments = null)
    {
        $_SESSION['status_modals'][] = self::asArray($sMessageContents, $sMessageType, $sTitleOverride, $aStatusMessageButtons, $oStatusModalArguments);
    }

    static function hasStatusMessages()
    {
        return !empty($_SESSION['status_modals']);
    }

    static function getClearStatusMessages()
    {
        $aStatusMessages = $_SESSION['status_modals'];
        $_SESSION['status_modals'] = null;
        return $aStatusMessages;
    }
}
