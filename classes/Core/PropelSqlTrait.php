<?php
namespace Core;

trait PropelSqlTrait
{
	public function cacheContains($key)
	{
		return Cache::isHit($key);
	}

	public function cacheFetch($key)
	{
		return Cache::get($key);
	}

	public function cacheStore($key, $value, $lifetime = 3600)
	{
		return Cache::set($key, $value, $lifetime);
	}
}