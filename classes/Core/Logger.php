<?php
namespace Core;

use Core\Json\JsonUtils;
use Exception\LogicException;
use Monolog\Handler\PHPConsoleHandler;
use Monolog\Logger as MonoLogger;
use Monolog\Handler\StreamHandler;
use Psr\Log\LoggerInterface;

class Logger {

	private static $iMinLogLevel = MonoLogger::INFO;

	/**
	 * Overwrites te minium Loglevel
	 * @param int $iLogLevel \Monolog\Logger
	 */
	public static function setMinLogLevel($iLogLevel = MonoLogger::INFO)
	{
		self::$iMinLogLevel = $iLogLevel;
	}

    private static function getMinLogLevel(){
		return self::$iMinLogLevel;
    }
    static function custom($sMessage, $sLogFilename, $iLogLevel = MonoLogger::DEBUG)
    {
        $log = new MonoLogger('custom');
        $log->pushHandler(new StreamHandler($sLogFilename, $iLogLevel));
        $log->addInfo($sMessage);
    }
    static function getLogdir($bAbsolute = false)
    {
        $sLogDir = Config::getDataDir($bAbsolute).'/log';

        if(!is_writable(Config::getDataDir($bAbsolute)))
        {
            throw new LogicException(Config::getDataDir($bAbsolute).' not writable.');
        }
        if(!is_dir($sLogDir))
        {
            mkdir($sLogDir);
        }
        return Config::getDataDir().'/log';
    }
    static function pageNotFound($sMessage)
    {
        $log = new MonoLogger('404');

        $log->pushHandler(new StreamHandler(self::getLogdir().'/hurah-page-not-found.log', self::getMinLogLevel()));
        $log->pushHandler(new PHPConsoleHandler(['enabled' => true]));
        $log->addError($sMessage);
        self::error($sMessage);
    }
    static function error($sMessage)
    {
        $log = new MonoLogger('error');
        $log->pushHandler(new StreamHandler(self::getLogdir().'/hurah-error.log', self::getMinLogLevel()));
        $log->pushHandler(new StreamHandler(self::getLogdir().'/hurah-verbose.log', self::getMinLogLevel()));
        $log->pushHandler(new PHPConsoleHandler(['enabled' => true]));

        $log->addError($sMessage);
    }
    static function warning($sMessage)
    {
        $log = new MonoLogger('warning');
        $log->pushHandler(new StreamHandler(self::getLogdir().'/hurah-verbose.log', self::getMinLogLevel()));
        $log->pushHandler(new StreamHandler(self::getLogdir().'/hurah-warning.log', self::getMinLogLevel()));
        $log->pushHandler(new PHPConsoleHandler(['enabled' => true]));

        $log->addWarning($sMessage);
    }
    static function info($sMessage)
    {
        $log = new MonoLogger('info');
        $log->pushHandler(new StreamHandler(self::getLogdir().'/hurah-verbose.log', self::getMinLogLevel()));
        $log->pushHandler(new StreamHandler(self::getLogdir().'/hurah-info.log', self::getMinLogLevel()));
        $log->pushHandler(new PHPConsoleHandler(['enabled' => true]));

        $log->addInfo($sMessage);
    }
    static function debug($mixedMessage)
    {
        $log = new MonoLogger('debug');
        $log->pushHandler(new StreamHandler(self::getLogdir().'/hurah-verbose.log', self::getMinLogLevel()));
        $log->pushHandler(new StreamHandler(self::getLogdir().'/hurah-debug.log', self::getMinLogLevel()));
        // $log->pushHandler(new PHPConsoleHandler(['enabled' => true]));

        if(!is_string($mixedMessage)){
            $log->addWarning(print_r($mixedMessage, true));
        }else{
            $log->addWarning($mixedMessage);
        }
    }
    static function console($mMessage, string $sLevel = 'info')
    {
        if(is_array($mMessage) || is_object($mMessage))
        {
            $mMessage = JsonUtils::encode($mMessage);
        }
        $log = new MonoLogger('debug');
        $log->pushHandler(new PHPConsoleHandler(['enabled' => true]));

        if($sLevel == 'info')
        {
            $log->addInfo($mMessage);
        }
        else if($sLevel == 'warning')
        {
            $log->addWarning($mMessage);

        }
        else
        {
            $log->addDebug($mMessage);

        }

    }
}
