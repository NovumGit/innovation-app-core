<?php
namespace Core;

use Propel\Runtime\Propel;

class QueryMapper
{
    static function getRowCount()
    {
        $aRow = QueryMapper::fetchRow('SELECT FOUND_ROWS() as row_count;');
        return $aRow['row_count'];
    }
    /**
     * @param $sQuery
     * @return boolean
     */
    static function query($sQuery, array $aOptions = null)
    {
        $oConnection = Propel::getWriteConnection('hurah');
        $oStatement = $oConnection->prepare($sQuery);
        return $oStatement->execute();
    }

    /**
     * @param string $sQuery
     * @return Object[]
     */
    static function fetchArray($sQuery)
    {
        $oConnection = Propel::getWriteConnection('hurah');
        $oStatement = $oConnection->prepare($sQuery);
        $oStatement->execute();

        $aRows = $oStatement->fetchAll(\PDO::FETCH_ASSOC);
        return $aRows;
    }

    /**
     * @param string $sQuery
     * @return mixed
     */
    static function fetchVal($sQuery)
    {
        $aData = self::fetchRow($sQuery);

        if(!empty($aData))
        {
            return current($aData);
        }
        return null;
    }
    static function fetchRow($sQuery)
    {
        $aData = self::fetchArray($sQuery);

        if(isset($aData[0]))
        {
            return $aData[0];
        }
        return null;
    }
    static function quote($sQuery)
    {
        $oConnection = Propel::getWriteConnection('hurah');
        return $oConnection->quote($sQuery);
    }

    public static function makeLimit($iCurrentPage, $iItemsPP)
    {
        $iFrom = ($iCurrentPage * $iItemsPP) - $iItemsPP;
        $iTo = $iItemsPP;
        $sLimit = "LIMIT $iFrom, $iTo";
        return $sLimit;
    }
}