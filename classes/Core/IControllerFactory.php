<?php
namespace Core;

interface IControllerFactory
{
    function getController(array $aGet, array $aPost, string $sNamespace = null):MainController;
}
