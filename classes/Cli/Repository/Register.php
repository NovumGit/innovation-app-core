<?php

namespace Cli\Repository;

use Cli\BaseCommand;
use Hurah\Types\Type\Composer\Repository;
use Hurah\Types\Type\Composer\RepositoryList;
use Hurah\Types\Type\Composer\RepositoryType;
use Hurah\Types\Type\Path;
use Hurah\Types\Type\PluginType;
use Core\Utils;
use DirectoryIterator;
use Hi\Helpers\DirectoryStructure;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use  Generator\Composer\Utils as ComposerUtils;

class Register extends BaseCommand
{
    protected function configure()
    {
        $this->setName("repository:register");
        $this->setDescription("Register al local repository directories as a repository");
        $sSystemName = function_exists('getSiteSettings') ? getSiteSettings()['system_name'] : 'Innovation App';
        $this->setHelp(<<<EOT
$sSystemName is based on installable packages. Normaly this is done trough a system called Packagist or by specifying 
a custom package repository location such as a remote git repository. When you are creating a new system, for instance
by invoking <info>system:create</info> you actually are creating a new package. The structure if this package is for
100% identical to what you would publish on Packagist if you where to make your new system available via open source.

To make your new system behave like a package a local folder inside <info>data/local-repositories</info> is created and
then we tell composer to look inside that folder first when installing packages. In the background your new package is 
installed for you, making this process transparent but when you finally decide to publish your package there is not much
to do anymore. Just push to git and tell Packagist to fetch your package from there.  

Normally there is no need to call this command manually. It is done for you when creating a new system with 
<info>system:create</info> but it is good to know. When you uninstall a local package with 
<info>package:uninstall</info> it is actually uninstalled but remains in the local repository allowing you to 
<info>package:install</info> it again at any given time. 

EOT
        );
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $oDirectoryStructure = new DirectoryStructure();
        $aRepositoryList = [];
        $aRepositoryCount = [];
        foreach (PluginType::getAll() as $oPluginType) {
            $aRepositoryCount[(string) $oPluginType] = $aRepositoryCount[(string) $oPluginType] ?? 0;
            $oPluginRepositoryPath = $oDirectoryStructure->getPluginRespositoryDir($oPluginType, false);
            $oDirectoryIterator = new DirectoryIterator($oPluginRepositoryPath);
            foreach ($oDirectoryIterator as $oDirectory) {
                if ($oDirectory->isFile()) {
                    continue;
                }
                if ($oDirectory->isDot()) {
                    continue;
                }
                $sPluginComposerPath = Utils::makePath($oDirectory->getPathname(), 'composer.json');

                if (file_exists($sPluginComposerPath)) {
                    $aRepositoryCount[(string) $oPluginType] ++;

                    $oRepository = new Repository([
                        'type' => new RepositoryType(RepositoryType::TYPE_PATH),
                        'path' => new Path($oDirectory->getPathname())
                    ]);
                    $aRepositoryList[] = $oRepository;

                    $sPluginDir = basename($oDirectory->getPathname());
                    $output->writeln("<comment>Found</comment> <info>$oPluginType</info> <comment>plugin</comment> <info>$sPluginDir</info>");
                }
            }
        }


        $oComposerUtils = new ComposerUtils(new Path(Utils::makePath('composer.json')));
        $oComposerUtils->add(new RepositoryList($aRepositoryList));
        $oComposerUtils->save();
        $iRepositoryCount = count($aRepositoryList);
        foreach ($aRepositoryCount as $sType => $ICount) {
            $output->writeln("<comment>Registered</comment> <info>{$ICount} $sType</info> <comment>repositories</comment>");
        }
        $output->writeln("Total repositories registered: <info>{$iRepositoryCount}</info>");

        return Command::SUCCESS;
    }
}
