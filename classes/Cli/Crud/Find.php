<?php

namespace Cli\Crud;

use Cli\BaseCommand;
use Cli\Crud\Find\Main;
use Cli\Helper\Questions\Domain;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Find extends BaseCommand
{
    protected function configure()
    {
        $this->setName("crud:find");
        $this->setDescription('Find available cruds');
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, Domain::getInfo(true));
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->domainCollector($input, $output);
        $aSelectedDomains = $input->getArgument('domain');

        $helper = $this->getHelper('question');
        $oMain = new Main($input, $output, $helper, $aSelectedDomains === 'all' ? [] : $aSelectedDomains, $this);
        $oMain->run();
        return Command::SUCCESS;
    }
}
