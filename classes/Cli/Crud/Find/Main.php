<?php

namespace Cli\Crud\Find;

use Cli\BaseCommand;
use Cli\Database\Helper\Propel;
use Crud\CrudFactory;
use Crud\IApiExposable;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Main
{
    private InputInterface $input;
    private OutputInterface $output;
    private QuestionHelper $helper;
    private array $aDomains;
    private BaseCommand $command;

    public function __construct(InputInterface $input, OutputInterface $output, QuestionHelper $helper, array $aDomains, BaseCommand $command)
    {
        $this->input = $input;
        $this->output = $output;
        $this->helper = $helper;
        $this->aDomains = $aDomains;
        $this->command = $command;
    }

    public function run()
    {

        foreach ($this->aDomains as $sDomainKey) {
            Propel::includeConfigs($sDomainKey, $this->output);

            $aAllCruds = CrudFactory::getAll([]);

            $aCrudTable = [];
            $i = 0;
            foreach ($aAllCruds as $aCrudInfo) {
                $sTitle = '';
                $oManager = $aCrudInfo['crud_manager'];
                if ($oManager instanceof IApiExposable) {
                    $sTitle = $oManager->getShortDescription();
                }

                ++$i;
                $aCrudTable[$i] = [
                    'id' => "<info>$i</info>",
                    'module' => $aCrudInfo['module'],
                    'class_name' => $aCrudInfo['class_name'],
                    'info' => $sTitle,
                ];
            }

            $oTable = new Table($this->output);
            $oTable->setHeaders([
                '#',
                'Module',
                'Manager',
                'Info',
            ])->setRows($aCrudTable);

            $oTable->render();
        }
    }
}
