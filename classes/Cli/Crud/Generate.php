<?php

namespace Cli\Crud;

use Cli\BaseCommand;
use Cli\Crud\Generate\Main;
use Cli\Helper\Questions\Domain;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Generate extends BaseCommand
{
    protected function configure()
    {
        $this->setName("crud:generate");
        $this->setDescription('Generates crud classes based on schema.xml');
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, Domain::getInfo(true));
        $this->setHelp(<<<EOT
Crud's provide an abstraction layer above the ORM and serve several purposes. Please refer to the Components/Crud
section of the documentation to get more details but a summary: 

1. Allow you to dynamically configure UI's.
2. Enrich the data types that are commonly found in databases. Instead of VARCHAR we have EMAIL or BSN etc.
3. Alter the way data is presented to a user.
4. Give fine grained control over how a field can be changed (write-once, read-may, only visible for role x etc)
5. Give you the option to bind events to field changes.
etc    

This command generates all the crud code. Usually you will invoke this command via another command such as 
<info>package:migrate</info>
EOT
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->domainCollector($input, $output);
        $aSelectedDomains = $input->getArgument('domain');

        $helper = $this->getHelper('question');
        $oMain = new Main($input, $output, $helper, $aSelectedDomains === 'all' ? [] : $aSelectedDomains);
        $oMain->run();
        return Command::SUCCESS;
    }
}
