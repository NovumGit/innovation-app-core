<?php

namespace Cli\Crud\Generate;

use Generator\Generators\Crud\CrudsFromSchema;
use Core\Cfg;
use Core\Config;
use Core\Utils;
use Generator\Schema\Converter;
use Hi\Helpers\DirectoryStructure;
use Model\Module\ModuleQuery;
use Model\System\DataModel\Model\DataModelQuery;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Main
{
    private InputInterface $input;
    private OutputInterface $output;
    private QuestionHelper $helper;
    private array $aDomains;

    public function __construct(InputInterface $input, OutputInterface $output, QuestionHelper $helper, array $aDomains)
    {
        $this->input = $input;
        $this->output = $output;
        $this->helper = $helper;
        $this->aDomains = $aDomains;
    }

    public function run()
    {

        foreach ($this->aDomains as $sDomainKey) {
            $this->output->writeln("Running Crud generator for domain <info>{$sDomainKey}</info>");
            $oDirectoryStructure = new DirectoryStructure();

            $sBuildRoot = Utils::makePath($oDirectoryStructure->getSystemDir(true), 'build');

            $aSchemaLocations = [
                Utils::makePath($sBuildRoot, 'database', $sDomainKey, 'schema.xml'),
                Utils::makePath($sBuildRoot, 'schema', 'core-schema-extra.xml'),
                Utils::makePath($sBuildRoot, 'schema', 'novum-schema.xml'),
            ];

            foreach ($aSchemaLocations as $sSchemaLocation) {
                $oCrudFromSchema = new CrudsFromSchema($sSchemaLocation, $this->output);
                $oCrudFromSchema->run();
            }

            $this->output->writeln("<comment>Fetching user schema from: </comment><info>database</info>");
            $oDirectoryStructure = new DirectoryStructure();
            // Module query wordt niet gebruikt....
            $oModuleQuery = ModuleQuery::create();
            // $sXsdLocation = 'https://novumgit.gitlab.io/innovation-app-schema-xsd/v1/schema.xsd';
            $sConfigPath = Utils::makePath($oDirectoryStructure->getConfigRoot(true), $sDomainKey, 'config.php');
            $sPropelPath = Utils::makePath($oDirectoryStructure->getConfigRoot(true), $sDomainKey, 'propel', 'config.php');
            require $sPropelPath;
            $aDomainConfig = require $sConfigPath;
            Cfg::set($aDomainConfig);
            $oDataModelQuery = DataModelQuery::create();
            $sCustom = Cfg::get('CUSTOM_NAMESPACE');

            $oConverter = new Converter($oModuleQuery, $oDataModelQuery, $sCustom);
            $sSchemaXml = $oConverter->asXml();
            $sTmpSchemaLocation = Utils::makePath(Config::getDataDir(true), 'user-generated-schema.xml');
            $this->output->writeln("<comment>Storing schema in</comment> <info>$sTmpSchemaLocation</info>");
            file_put_contents($sTmpSchemaLocation, $sSchemaXml);
            $oCrudsFromSchema = new CrudsFromSchema($sTmpSchemaLocation, $this->output);
            $oCrudsFromSchema->run();
        }
    }
}
