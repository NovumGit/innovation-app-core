<?php

namespace Cli\System;

use Cli\System\Init\Extender\Factory as SystemTypeFactory;
use Composer\Command\BaseCommand;
use Composer\IO\ConsoleIO;
use Composer\Repository\CompositeRepository;
use Composer\Util\ProcessExecutor;
use Hurah\Types\Type\Composer\License;
use Hurah\Types\Type\PluginType;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\ExecutableFinder;
use Symfony\Component\Process\Process;
use Throwable;
use Twig_Error_Loader;
use Twig_Error_Syntax;

class Test extends BaseCommand
{
    /** @var CompositeRepository */
    protected $repos;

    /** @var array */
    private $gitConfig;


    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setHidden(true);
        $this->setName('system:test')->setDescription('Test create boilerplate system.')->setDefinition([
            new InputOption('system_type', null, InputOption::VALUE_OPTIONAL, 'Type of system (e.g. ' . join(', ', SystemTypeFactory::systemTypes) . ')'),
        ])->setHelp(<<<EOT
The <info>test</info> command creates a basic Innovation app system.

<info>./novum system:test</info>

Read more at https://docs.demo.novum.nu/
EOT
        );
        $this->setHelp("For testing purposes, should not be invoked.");
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Throwable
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Syntax
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {


        $input->setOption('vendor', 'novum');
        $input->setOption('service_name', 'test');
        $input->setOption('domain_name', 'test.demo.novum.nu');
        $input->setOption('homepage', 'https://docs.demo.novum.nu');
        $input->setOption('description', 'Dit is een testomgeving van Novum');
        $input->setOption('license', License::MIT);
        $input->setOption('stability', 'dev');
        $input->setOption('authors', [['name' => 'Anton Boutkam', 'email' => 'anton@novum.nu']]);
        $input->setOption('keywords', ['novum', 'innovatie', 'lab']);
        $input->setOption('prefer_stable', true);
        $input->setOption('add_domain', true);

        $oFactory = new SystemTypeFactory();

        $oApiBuilder = $oFactory->getBuilder(new PluginType(PluginType::API), $input, $output);
        $oApiBuilder->build();
        $oApiBuilder->makeRepository();

        $oSiteBuilder = $oFactory->getBuilder(new PluginType(PluginType::SITE), $input, $output);
        $oSiteBuilder->build();
        $oSiteBuilder->makeRepository();

        $oDomainBuilder = $oFactory->getBuilder(new PluginType(PluginType::DOMAIN), $input, $output);
        $oDomainBuilder->build();
        $oDomainBuilder->makeRepository();

        return 0;
    }

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {

        $this->setIO(new ConsoleIO($input, $output, $this->getHelperSet()));

        $git = $this->getGitConfig();
        $io = $this->getIO();

        $formatter = $this->getHelperSet()->get('formatter');

        $io->writeError([
            '',
            $formatter->formatBlock('Welcome to the Novum Innovation app initializer', 'bg=blue;fg=white', true),
            '',
        ]);

        // namespace
        $io->writeError([
            '',
            'This command will guide you through creating a new base system.',
            '',
        ]);

        $system_type = $input->getOption('system_type');

        $system_type = $io->askAndValidate('Package Type (e.g. ' . join(', ', SystemTypeFactory::systemTypes) . ') [<comment>' . $system_type . '</comment>]: ', function ($value) use ($system_type) {

            if (!in_array($value, SystemTypeFactory::systemTypes)) {
                throw new InvalidArgumentException('Invalid system type "' . $value . '". Must be empty or one of: ' . join(', ', SystemTypeFactory::systemTypes));
            }

            return $value;
        }, null, $system_type);
        $input->setOption('system_type', $system_type);
    }

    protected function getGitConfig()
    {
        if (null !== $this->gitConfig) {
            return $this->gitConfig;
        }

        $finder = new ExecutableFinder();
        $gitBin = $finder->find('git');

        // TODO in v3 always call with an array
        if (method_exists('Symfony\Component\Process\Process', 'fromShellCommandline')) {
            $cmd = new Process([
                $gitBin,
                'config',
                '-l',
            ]);
        } else {
            $cmd = new Process([ProcessExecutor::escape($gitBin), 'config', '-l']);
        }
        $cmd->run();

        if ($cmd->isSuccessful()) {
            $this->gitConfig = [];
            preg_match_all('{^([^=]+)=(.*)$}m', $cmd->getOutput(), $matches, PREG_SET_ORDER);
            foreach ($matches as $match) {
                $this->gitConfig[$match[1]] = $match[2];
            }

            return $this->gitConfig;
        }

        return $this->gitConfig = [];
    }
}
