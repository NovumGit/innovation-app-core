<?php

namespace Cli\System;

use Cli\BaseCommand;
use Cli\System\Init\Extender\Factory as SystemTypeFactory;
use Cli\Helper\Questions\AddDomain;
use Cli\Helper\Questions\Author;
use Cli\Helper\Questions\Description;
use Cli\Helper\Questions\DomainName;
use Cli\Helper\Questions\HomePage;
use Cli\Helper\Questions\Keywords;
use Cli\Helper\Questions\License;
use Cli\Helper\Questions\MinStability;
use Cli\Helper\Questions\PreferStable;
use Cli\Helper\Questions\ServiceName;
use Cli\Helper\Questions\SystemType;
use Cli\Helper\Questions\Vendor;
use Composer\IO\ConsoleIO;
use Composer\Package\BasePackage;
use Composer\Repository\CompositeRepository;
use Hurah\Types\Type\PluginType;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Init extends BaseCommand
{
    /** @var CompositeRepository */
    protected $repos;

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('system:init')
            ->setDescription('Initializes a boilerplate system for you to work on')
            ->setDefinition([
                new InputOption('system_type', null, InputOption::VALUE_OPTIONAL, 'Type of system (one of: ' . join(', ', SystemTypeFactory::systemTypes) . ')'),
                new InputOption('service_name', null, InputOption::VALUE_REQUIRED, 'Name of the service (e.g. "belastingdienst" or "todo")'),
                new InputOption('vendor', null, InputOption::VALUE_REQUIRED, 'Unique identifier of your company or you (e.g. "novum" or "monolog")'),
                new InputOption('domain_name', null, InputOption::VALUE_REQUIRED, 'Base domain / dns name of new system (e.g. novum.nu)'),
                new InputOption('homepage', null, InputOption::VALUE_REQUIRED, 'Homepage / info pase (e.g. https://docs.demo.novum.nu)'),
                new InputOption('description', null, InputOption::VALUE_REQUIRED, 'Description'),
                new InputOption('license', 'l', InputOption::VALUE_REQUIRED, 'License (empty or one of: ' . implode(', ', array_keys(\Hurah\Types\Type\Composer\License::availableLicenses())) . ')'),
                new InputOption('stability', 's', InputOption::VALUE_REQUIRED, 'Minimum stability (empty or one of: ' . implode(', ', array_keys(BasePackage::$stabilities)) . ')', 'dev'),
                new InputOption('author', null, InputOption::VALUE_REQUIRED, 'Author name of system'),
                new InputOption('authors', null, InputOption::VALUE_REQUIRED, 'Authors of system'),
                new InputOption('keywords', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'Keywords that describe the system', ["fake api, belastingdienst"]),
                new InputOption('prefer_stable', null, InputOption::VALUE_REQUIRED, 'Prefer stable', true),
                new InputOption('add_domain', null, InputOption::VALUE_OPTIONAL, 'Add domain (only applicable when choosing site or api as system_type)', true)
            ])
            ->setHelp(
                <<<EOT
The <info>init</info> command creates a basic boilerplate system and stores it as a repository that you can install.
This command allows you to create domains, api's and sites and is interactive. You can specify all the options and 
create a new system in a single command or omit options and make the system ask you what you want.

Normally it makes more sense to use <info>system:create</info> as it does not only create the boilerplate system but 
also installs it for you so it is available.

Read more at https://docs.demo.novum.nu/
EOT
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $system_type = $input->getOption('system_type');


        $allowList = ['system_type', 'service_name', 'vendor', 'domain_name', 'homepage', 'description', 'license', 'stability', 'author', 'keywords', 'prefer_stable', 'add_domain'];

        $options = array_filter(array_intersect_key($input->getOptions(), array_flip($allowList)));
        $options['add_domain'] = $options['add_domain'] ?? false;

        if (isset($options['author'])) {
            if (is_array($options['author'])) {
                $options['authors'] = [$options['author']];
            } else {
                $options['authors'] = $this->formatAuthors($options['author']);
            }

            unset($options['author']);
        }

        if (isset($options['stability'])) {
            $options['minimum-stability'] = $options['stability'];
            unset($options['stability']);
        }

        $input->setOption('authors', $options['authors']);


        /**
         * Get the object that will actually build the system.
         */
        $aPackagesToInstall = [];
        $oSystemTypeFactory = new SystemTypeFactory();
        $oBuilder = $oSystemTypeFactory->getBuilder(new PluginType($system_type), $input, $output);

        $oBuilder->build();
        $oBuilder->makeRepository();
        $aPackagesToInstall[] = $oBuilder->getPackageName();
        $output->writeln("<comment>$system_type created</comment>");

        if (PluginType::hasDomainDependency(new PluginType($system_type)) && $options['add_domain']) {
            $output->writeln("<comment>Finished creating $system_type, now creating domain</comment>");
            /**
             * Get the object that will actually build the system.
             */
            $oBuilder = $oSystemTypeFactory->getBuilder(new PluginType(PluginType::DOMAIN), $input, $output);
            $oBuilder->build();
            $aPackagesToInstall[] = $oBuilder->getPackageName();
            $oBuilder->makeRepository();
            $output->writeln("<comment>Domain created</comment>");
        }

        $output->writeln("<info>Initialization done</info>");
        return 0;
    }

    protected function formatAuthors($author)
    {
        return array($this->parseAuthorString($author));
    }

    /**
     * @private
     * @param string $author
     * @return array
     */
    public function parseAuthorString($author)
    {
        if (preg_match('/^(?P<name>[- .,\p{L}\p{N}\p{Mn}\'’"()]+) <(?P<email>.+?)>$/u', $author, $match)) {
            if ($this->isValidEmail($match['email'])) {
                return array(
                    'name' => trim($match['name']),
                    'email' => $match['email'],
                );
            }
        }

        throw new InvalidArgumentException(
            'Invalid author string.  Must be in the format: ' .
            'John Smith <john@example.com>'
        );
    }

    protected function isValidEmail($email)
    {
        // assume it's valid if we can't validate it
        if (!function_exists('filter_var')) {
            return true;
        }

        // php <5.3.3 has a very broken email validator, so bypass checks
        if (PHP_VERSION_ID < 50303) {
            return true;
        }

        return false !== filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {


        $this->setIO(new ConsoleIO($input, $output, $this->getHelperSet()));
        $io = $this->getIO();


        $formatter = $this->getHelperSet()->get('formatter');

        $io->writeError(array(
            '',
            $formatter->formatBlock('Welcome to the Novum Innovation app initializer', 'bg=blue;fg=white', true),
            '',
        ));


        $io->writeError(array(
            '',
            'This command will guide you through creating a new base system.',
            '',
        ));


        (new Vendor())->ask($io, $input);
        (new ServiceName())->ask($io, $input);
        (new Keywords())->ask($io, $input);
        (new PreferStable())->ask($io, $input);
        (new Description())->ask($io, $input);
        (new Author())->ask($io, $input);
        (new MinStability())->ask($io, $input);
        (new SystemType())->ask($io, $input);
        (new AddDomain())->ask($io, $input);
        (new License())->ask($io, $input);
        (new DomainName())->ask($io, $input);
        (new HomePage())->ask($io, $input);
        (new SystemType())->ask($io, $input);
    }
}
