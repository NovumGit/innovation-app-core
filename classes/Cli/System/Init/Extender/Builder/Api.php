<?php

namespace Cli\System\Init\Extender\Builder;

use Cli\System\Init\Extender\BaseBuilder;
use Cli\System\Init\Extender\IBuilder;
use Generator\Vo\ApiBuildConfig;
use Hurah\Types\Type\PluginType;
use Generator\Api\Builder\Structure;
use Generator\Composer\Builder as ComposerBuilder;
use Generator\IApiStructureConfig;
use Generator\ICompleteApi;
use Throwable;
use Twig_Error_Loader;
use Twig_Error_Syntax;

final class Api extends BaseBuilder implements IBuilder
{
    public function getType(): PluginType
    {
        return new PluginType(PluginType::API);
    }

    public function toBuildVo(): ICompleteApi
    {
        return new ApiBuildConfig($this->getOption('vendor'), $this->getOption('service_name'), $this->getOption('domain_name'), $this->getOption('homepage'), $this->getOption('description'), $this->getOption('license'), $this->getOption('stability'), $this->getOption('authors'), $this->getOption('keywords'), $this->getOption('prefer_stable'));
    }

    /**
     * @param IApiStructureConfig $oApiStructureVo
     * @throws Throwable
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Syntax
     */
    public function build(): void
    {
        $oApIConfig = $this->toBuildVo();

        $oApiStructure = new Structure();
        $oApiStructure->createStructure($oApIConfig, 'live');

        $oComposerBuilder = new ComposerBuilder($oApIConfig);
        $oComposerBuilder->save();

        exec('chmod 777 -R ./*');

        // $oDomainBuilder = new Domain($this->input, $this->output);
        // $oDomainBuilder->build($oApiStructureVo);
    }
}
