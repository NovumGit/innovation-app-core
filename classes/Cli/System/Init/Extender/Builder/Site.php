<?php

namespace Cli\System\Init\Extender\Builder;

use Cli\System\Init\Extender\BaseBuilder;
use Cli\System\Init\Extender\IBuilder;
use Generator\Vo\SiteBuildConfig;
use Hurah\Types\Type\PluginType;
use Generator\Composer\Builder as ComposerBuilder;
use Generator\ICompleteSite;
use Generator\Site\Builder\Structure;
use Throwable;

final class Site extends BaseBuilder implements IBuilder
{

    public function getType(): PluginType
    {
        return new PluginType(PluginType::SITE);
    }

    public function toBuildVo(): ICompleteSite
    {
        return new SiteBuildConfig($this->getOption('vendor'), $this->getOption('service_name'), $this->getOption('domain_name'), $this->getOption('homepage'), $this->getOption('description'), $this->getOption('license'), $this->getOption('stability'), $this->getOption('authors'), $this->getOption('keywords'), $this->getOption('prefer_stable'));
    }

    /**
     * @throws Throwable
     */
    public function build(): void
    {
        $oSiteConfig = $this->toBuildVo();

        $oSiteStructure = new Structure();
        $oSiteStructure->createEnvironment($oSiteConfig, 'live');

        $oComposerBuilder = new ComposerBuilder($oSiteConfig);
        $oComposerBuilder->save();

        exec('chmod 777 -R ./*');
        /*
        $oSystemBuildVo = new SystemBuildVo();

        $this->
        $this->input->getOptions();
        // TODO: Implement build() method.
        */
    }
}
