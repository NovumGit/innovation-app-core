<?php

namespace Cli\System\Init\Extender\Builder;

use Cli\System\Init\Extender\BaseBuilder;
use Cli\System\Init\Extender\IBuilder;
use Generator\Vo\DomainBuildVo;
use Hurah\Types\Type\PluginType;
use Generator\Composer\Builder as ComposerBuilder;
use Generator\Config\Builder as ConfigBuilder;
use Generator\Domain\DirectoryStructure;
use Generator\Env\Builder;
use Generator\ICompleteDomain;
use Generator\Schema\Builder\SchemaBuilder;

final class Domain extends BaseBuilder implements IBuilder
{
    public function getType(): PluginType
    {
        return new PluginType(PluginType::DOMAIN);
    }

    public function toBuildVo(): ICompleteDomain
    {
        return new DomainBuildVo($this->getOption('vendor'), $this->getOption('service_name'), $this->getOption('domain_name'), $this->getOption('homepage'), $this->getOption('description'), $this->getOption('license'), $this->getOption('stability'), $this->getOption('authors'), $this->getOption('keywords'), $this->getOption('prefer_stable'));
    }

    public function build(): void
    {
        $oDomainConfig = $this->toBuildVo();

        $oDirectoryStructure = new DirectoryStructure($oDomainConfig);
        $oDirectoryStructure->create();

        $oConfigs = new ConfigBuilder();
        $oConfigs->makeConfig($oDomainConfig);

        $oEnvBuilder = new Builder();
        $oEnvBuilder->create($oDomainConfig);

        echo "System id " . $oDomainConfig->getSystemId() . PHP_EOL;

        $oDomainComposer = new ComposerBuilder($oDomainConfig);
        $oDomainComposer->save();

        $oSchema = new SchemaBuilder($oDomainConfig);
        $oSchema->save();

        exec('chmod 777 -R ./*');
    }
}
