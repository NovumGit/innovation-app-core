<?php

namespace Cli\System\Init\Extender;

use Hurah\Types\Type\PluginType;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class Factory
{
    public const systemTypes = [
        'site',
        'api',
        'domain',
    ];

    public function getBuilder(PluginType $type, InputInterface $input, OutputInterface $output): IBuilder
    {
        $sClassName = '\\Cli\\System\\Init\\Extender\\Builder\\' . ucfirst($type);
        return new $sClassName($input, $output);
    }
}
