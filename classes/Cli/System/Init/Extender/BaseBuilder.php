<?php

namespace Cli\System\Init\Extender;

use Hurah\Types\Type\Path;
use Hurah\Types\Type\PluginType;
use Core\Utils;
use Generator\ICompleteComponent;
use Hi\Helpers\DirectoryStructure;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

abstract class BaseBuilder implements IBuilder
{
    protected $input;
    protected $output;

    abstract public function toBuildVo(): ICompleteComponent;

    abstract public function getType(): PluginType;

    public function __construct(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;
    }

    public function getInstallDir(): Path
    {
        return $this->toBuildVo()->getInstallDir('live');
    }

    public function makeRepository(): void
    {
        $sInstallDirName = $this->toBuildVo()->getInstallDir('live');

        $oDirectoryStructure = new DirectoryStructure();
        $sRepositoryDir = $oDirectoryStructure->getPluginRespositoryDir($this->getType());
        $sRepositoryDirName = Utils::makePath($sRepositoryDir, basename($sInstallDirName));
        $this->output->writeln("<error>Repository location $sRepositoryDirName</error>");

        $sRepositoryArchiveDir = Utils::makePath($oDirectoryStructure->getDataDir(true), 'repository-archive');
        $this->output->writeln("<error>Repository archive location $sRepositoryArchiveDir</error>");
        Utils::makeDir($sRepositoryArchiveDir);
        $sArchiveRepositoryDirName = Utils::makePath($sRepositoryArchiveDir, date('YmdHis') . basename($sInstallDirName));

        $this->output->writeln("<comment>Moving code to repository location $sInstallDirName, $sRepositoryDirName</comment>");

        if (file_exists($sRepositoryDirName)) {
            rename($sRepositoryDirName, $sArchiveRepositoryDirName);
            chmod($sArchiveRepositoryDirName, 0777);
        }
        rename($sInstallDirName, $sRepositoryDirName);
        chmod($sRepositoryDirName, 0777);
    }

    public function getOption($sName)
    {
        return $this->input->getOption($sName);
    }

    public function getPackageName(): string
    {
        return $this->toBuildVo()->getPackageName();
    }
}
