<?php

namespace Cli\System\Init\Extender;

use Hurah\Types\Type\Path;

interface IBuilder
{
    public function build(): void;

    public function makeRepository(): void;

    public function getInstallDir(): Path;

    public function getPackageName(): string;
}
