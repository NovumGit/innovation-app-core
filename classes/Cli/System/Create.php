<?php

namespace Cli\System;

use Cli\BaseCommand;
use Cli\System\Create\Main;
use Cli\System\Init\Extender\Factory as SystemTypeFactory;
use Composer\Package\BasePackage;
use Hurah\Types\Type\Composer\License;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Create extends BaseCommand
{

    protected function configure()
    {
        $this->setName("system:create");

        $this->setDefinition([
            new InputOption('service_name', null, InputOption::VALUE_OPTIONAL, 'Name of the service (e.g. "belastingdienst" or "todo")'),
            new InputOption('vendor', null, InputOption::VALUE_OPTIONAL, 'Unique identifier of your company or you (e.g. "novum" or "monolog")'),
            new InputOption('domain_name', null, InputOption::VALUE_REQUIRED, 'Base domain / dns name of new system (e.g. novum.nu)'),
            new InputOption('system_type', null, InputOption::VALUE_OPTIONAL, 'Type of system (one of: ' . join(', ', SystemTypeFactory::systemTypes) . ')'),
            new InputOption('package_name', null, InputOption::VALUE_OPTIONAL, 'Package name can be used instead of all the other options. (vendor/package)'),
            // From system:init
            new InputOption('homepage', null, InputOption::VALUE_OPTIONAL, 'Homepage / info pase (e.g. https://docs.demo.novum.nu)'),
            new InputOption('system_type', null, InputOption::VALUE_OPTIONAL, 'Type of system (one of: ' . join(', ', SystemTypeFactory::systemTypes) . ')'),
            new InputOption('homepage', null, InputOption::VALUE_OPTIONAL, 'Homepage / info pase (e.g. https://docs.demo.novum.nu)'),
            new InputOption('description', null, InputOption::VALUE_OPTIONAL, 'Description'),
            new InputOption('license', 'l', InputOption::VALUE_OPTIONAL, 'License (empty or one of: ' . implode(', ', array_keys(License::availableLicenses())) . ')'),
            new InputOption('stability', 's', InputOption::VALUE_OPTIONAL, 'Minimum stability (empty or one of: ' . implode(', ', array_keys(BasePackage::$stabilities)) . ')', 'dev'),
            new InputOption('author', null, InputOption::VALUE_OPTIONAL, 'Author name of system'),
            new InputOption('authors', null, InputOption::VALUE_OPTIONAL, 'Authors of system'),
            new InputOption('keywords', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Keywords that describe the system', ["fake api, belastingdienst"]),
            new InputOption('prefer_stable', null, InputOption::VALUE_OPTIONAL, 'Prefer stable', true),
            new InputOption('add_domain', null, InputOption::VALUE_OPTIONAL, 'Add domain (only applicable when choosing site or api as system_type)', true)
        ]);
        $this->setDescription("Creates a new boilerplate system with database + installation");
        $this->setHelp(<<<EOT
Does the same thing as <info>system:init</info> but runs <info>repository:register</info> afterwards and then 
<info>package:install</info> so it makes the new system available immediately.
EOT
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $helper = $this->getHelper('question');
        $oMain = new Main($input, $output, $helper, $this);
        $oMain->run($input, $output);
        return Command::SUCCESS;
    }
}
