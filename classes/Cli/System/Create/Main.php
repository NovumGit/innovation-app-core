<?php

namespace Cli\System\Create;

use Cli\BaseCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Main
{
    private InputInterface $input;
    private OutputInterface $output;
    private QuestionHelper $helper;
    private BaseCommand $oCommand;

    private array $aAllOptions = [];

    public function __construct(InputInterface $input, OutputInterface $output, QuestionHelper $helper, BaseCommand $oCommand)
    {
        $this->input = $input;
        $this->output = $output;
        $this->helper = $helper;
        $this->oCommand = $oCommand;
    }

    public function run()
    {
        $aCommands = [
            [
                'command' => 'system:init',

                'filter' => function(array $aArguments, InputInterface $input)
                {
                    return $input;
                }
            ],
            [
                'command' => 'repository:register',
                'filter'  => function (array $aArguments) {
                    return [];
                },
            ],

            [
                /* install the domain package  */
                'command' => 'package:install',

                'filter' => function(array $aOptions)
                {
                    $sPackageName = "{$aOptions['vendor']}/domain-{$aOptions['service_name']}";
                    $aPassOptions["--package_name"] = [$sPackageName];
                    $aPassOptions["--full"] = 'no';

                    return $aPassOptions;
                },
            ],
            [
                /* install the site/api itself */
                'command' => 'package:install',

                'filter' => function(array $aOptions)
                {
                    $sPackageName = "{$aOptions['vendor']}/{$aOptions['system_type']}-{$aOptions['service_name']}";
                    $aPassOptions["--package_name"] = [$sPackageName];

                    // Generate database
                    $aPassOptions["--full"] = 'yes';
                    return $aPassOptions;
                },
            ],
            [
                'command' => 'db:make',
                'filter'  => function (array $aOptions) {
                    return ['domain' => [$aOptions['vendor'] . '.' . $aOptions['service_name']]];
                },
            ],
            [
                'command' => 'package:migrate',
                'filter'  => function (array $aOptions) {
                    return ['domain' => [$aOptions['vendor'] . '.' . $aOptions['service_name']]];
                },
            ],
        ];
        $this->output->writeln("<comment>Started</comment> <info>{$this->oCommand->getName()}</info>");

        foreach ($aCommands as $aCommand) {
            $this->output->writeln("<comment>Executing <info>{$aCommand['command']}</info>");
            $this->call($aCommand);
        }
    }

    private function call(array $aCommand)
    {
        // $this->output->writeln("Executing <info>{$sCommand}</info>");
        $command = $this->oCommand->getApplication()->find($aCommand['command']);
        // $aInput = new NullIn(['domain' => [$sDomain]]);

        $aInput = [];


        if(is_callable($aCommand['filter']))
        {
            $aInput = $aCommand['filter']($this->aAllOptions, $this->input);
        }

        if ($aInput === 'skip') {
            $this->output("<error>Skipping step, this is normal but shown as an error anyway.</error>");
            return;
        }
        if($aInput instanceof ArrayInput)
        {
            $oCommandInput = $aInput;
        }
        else
        {
            $oCommandInput = new ArrayInput($aInput);
        }

        $returnCode = $command->run($oCommandInput, $this->output);

        $this->aAllOptions = array_merge($this->aAllOptions, array_filter($oCommandInput->getOptions()));

        if ($returnCode === Command::SUCCESS) {
            $this->output->writeln("<info>{$aCommand['command']}</info> executed <info>successfully</info>");
        } else {
            $this->output->writeln("<info>{$aCommand['command']}</info> execution <warning>failed</warning>");
            sleep(2);
        }
    }
}
