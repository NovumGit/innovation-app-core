<?php

namespace Cli\Package\Uninstall;

use Core\Json\JsonUtils;
use Core\Utils;
use Hi\Helpers\DirectoryStructure;
use Hurah\Types\Type\Composer;
use Hurah\Types\Type\Path;
use Symfony\Component\Console\Output\OutputInterface;

class Domain
{
    public static function uninstall(Composer $oComposer, array $aConfig, OutputInterface $output): void
    {
        $sSystemId = $oComposer->getExtra()['system_id'];
        $sNameSpace = $aConfig['CUSTOM_NAMESPACE'];
        $oDirectoryStructure = new DirectoryStructure();
        $sDomainDir = Utils::makePath($oDirectoryStructure->getDomainDir(true), $sSystemId);
        $sBuildDir = Utils::makePath($oDirectoryStructure->getSystemDir(true), 'build', 'database', $sSystemId);
        $sCustomDir = Utils::makePath($oDirectoryStructure->getSystemDir(true), 'admin_modules', 'Custom', $sNameSpace);
        $sAssetsDir = Utils::makePath($oDirectoryStructure->getSystemDir(true), 'admin_public_html', 'custom', $sSystemId);
        $sCrudDir = Utils::makePath($oDirectoryStructure->getSystemDir(true), 'classes', 'Crud', 'Custom', $sNameSpace);
        $sModelDir = Utils::makePath($oDirectoryStructure->getSystemDir(true), 'classes', 'Model', 'Custom', $sNameSpace);
        $sVendorDir = Utils::makePath($oDirectoryStructure->getVendorDir(), $oComposer->getName());

        if (is_dir($sBuildDir)) {
            echo "Removing build dir $sBuildDir" . PHP_EOL;
            Utils::rmDir(new Path($sBuildDir), false, $output);
        }
        if (is_dir($sModelDir)) {
            echo "Removing $sModelDir" . PHP_EOL;
            Utils::rmDir(new Path($sModelDir), false, $output);
        }
        if (is_dir($sCrudDir)) {
            echo "Removing $sCrudDir" . PHP_EOL;
            Utils::rmDir(new Path($sCrudDir), false, $output);
        }
        if (is_dir($sAssetsDir)) {
            echo "Removing $sAssetsDir" . PHP_EOL;
            Utils::rmDir(new Path($sAssetsDir), false, $output);
        }
        if (is_dir($sCustomDir)) {
            echo "Removing $sCustomDir" . PHP_EOL;
            Utils::rmDir(new Path($sCustomDir), false, $output);
        }
        if (is_dir($sDomainDir)) {
            echo "Removing $sDomainDir" . PHP_EOL;
            Utils::rmDir(new Path($sDomainDir), false, $output);
        }
        if (is_dir($sVendorDir)) {
            echo "Removing $sVendorDir" . PHP_EOL;
            Utils::rmDir(new Path($sVendorDir), false, $output);
        }

        $sMainComposerFile = file_get_contents('./composer.json');
        $aMainComposerFile = JsonUtils::decode($sMainComposerFile);

        if (isset($aMainComposerFile['require'][(string)$oComposer->getName()])) {
            echo "Removing {$oComposer->getName()} from dependency list" . PHP_EOL;
            unset($aMainComposerFile['require'][(string)$oComposer->getName()]);
        }

        $sNewJsonFile = JsonUtils::encode($aMainComposerFile, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        file_put_contents('./composer.json', $sNewJsonFile);

        return;
    }
}
