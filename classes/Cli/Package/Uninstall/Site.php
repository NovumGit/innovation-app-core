<?php

namespace Cli\Package\Uninstall;

use Hurah\Types\Type\Composer;
use Hurah\Types\Type\Path;
use Hurah\Types\Type\SiteJson;
use Core\Json\JsonUtils;
use Core\Utils;
use Hi\Helpers\DirectoryStructure;
use Symfony\Component\Console\Output\OutputInterface;

class Site
{
    public static function uninstall(Composer $oComposer, SiteJson $oSiteJson, OutputInterface $output): void
    {
        $oDirectoryStructure = new DirectoryStructure();
        $sPublicDir = new Path(Utils::makePath($oDirectoryStructure->getSystemRoot(), $oDirectoryStructure->getPublicSitePath($oComposer->getExtra()['install_dir'])));

        if (is_dir($sPublicDir)) {
            echo "Removing $sPublicDir " . PHP_EOL;
            Utils::rmDir($sPublicDir, false, $output);
        }

        $sApacheConfigDir = Utils::makePath($oDirectoryStructure->getSystemRoot(), 'assets', 'server', 'http');
        if (is_dir($sApacheConfigDir)) {
            foreach ($oSiteJson->getEnvironments() as $sEnvironment) {
                $sDomain = $oSiteJson->getDomain($sEnvironment)['domain'];
                $sApacheEnvironmentConfigFile = Utils::makePath($sApacheConfigDir, $sEnvironment, "$sDomain.conf");
                if (file_exists($sApacheEnvironmentConfigFile)) {
                    echo "Removing $sApacheEnvironmentConfigFile" . PHP_EOL;
                    unlink($sApacheEnvironmentConfigFile);
                }
            }
        }

        $sSystemPublicDir = new Path(Utils::makePath($oDirectoryStructure->getSystemDir(true), 'public_html', $oComposer->getExtra()['install_dir']));

        if (is_link($sSystemPublicDir)) {
            echo "Unlinking $sSystemPublicDir" . PHP_EOL;
            unlink($sSystemPublicDir);
        }

        $sMainComposerFile = file_get_contents('./composer.json');
        $aMainComposerFile = JsonUtils::decode($sMainComposerFile);

        if (isset($aMainComposerFile['require'][(string)$oComposer->getName()])) {
            echo "Removing {$oComposer->getName()} from dependency list" . PHP_EOL;
            unset($aMainComposerFile['require'][(string)$oComposer->getName()]);
        }

        $sVendorDir = Utils::makePath($oDirectoryStructure->getVendorDir(), $oComposer->getName());
        Utils::rmDir($sVendorDir, false, $output);

        $sNewJsonFile = JsonUtils::encode($aMainComposerFile, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        file_put_contents('./composer.json', $sNewJsonFile);

        return;
    }
}
