<?php

namespace Cli\Package;

use Cli\BaseCommand;
use Helper\Package\Components\PackageInfoFactory;
use Hurah\Types\Type\Composer;
use Composer\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class Install extends BaseCommand
{
    protected function configure()
    {
        $this->setName("package:install");
        $this->addOption('package_name', null, InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY, 'Package name(s) (name property inside compoer.json)');
        $this->addOption('full', null, InputOption::VALUE_REQUIRED, 'Should we run package:migrate after the installation is completed? (yes/no/ask), default=ask', 'ask');

        $this->setDescription("Install a package from a (local) repository.");
        $help = <<<EOT
This command behaves like <info>./composer require</info> but decorated with additional functionality. Don't worry if 
you are not familiar with composer, this script will handle that part for you. Besides installing packages for you it 
will populate the database so installing a new system can be done with a single command. 
EOT;
        $this->setHelp($help);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $sFull = $input->getOption('full');
        if (!in_array($sFull, ['yes', 'no', 'ask'])) {
            $sFull = 'ask';
        }

        if ($sFull === 'ask') {
            while (true) {
                $helper = $this->getHelper('question');
                $question = new Question("<question>Install the database also?</question> (<info>y</info>/<info>n</info>)[<comment>y</comment>]", 'y');
                $sInstallDb = strtolower($helper->ask($input, $output, $question));

                $sInstallDb = str_replace('yes', 'y', $sInstallDb);
                $sInstallDb = str_replace('no', 'n', $sInstallDb);

                if (!in_array($sInstallDb, ['y', 'n'])) {
                    $output->writeln("<error>Please answer with y or n</error>");
                    continue;
                }
                break;
            }
        }
        $sFull = str_replace('yes', 'y', $sFull);
        $sFull = str_replace('no', 'n', $sFull);
        $input->setOption('full', $sFull);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Executing package:install");

        putenv('COMPOSER_HOME=' . __DIR__ . '/vendor/bin/composer');
        $aPackages = $input->getOption('package_name');


        foreach ($aPackages as $sPackageName) {
            $output->writeln("Installing <info>$sPackageName</info>");
            $this->installPackage($input, $output, $sPackageName);
        }
        return Command::SUCCESS;
    }

    private function installPackage(InputInterface $input, OutputInterface $output, string $sPackageName): void
    {
        $output->writeln("Running composer install for package <info>$sPackageName</info>");
        $this->composerInstall($output, $sPackageName);

        $sFull = $input->getOption('full');

        if ($sFull === 'y') {
            $this->dbInstall($output, $sPackageName);
        }
    }

    private function composerInstall(OutputInterface $output, string $sPackageName): void
    {
        $output->writeln("<comment>installing</comment> <info>$sPackageName</info>");
        // call `composer install` command programmatically


        $_ENV['COMPOSER_DISCARD_CHANGES'] = true;
        $input = new ArrayInput([
            'command' => 'require',
            'packages' => [$sPackageName],
            '--no-interaction'
        ]);
        $application = new Application();
        $application->setAutoExit(false); // prevent `$application->run` method from exitting the script
        $application->run($input);
        $output->writeln("<info>$sPackageName</info> installed");
    }

    private function dbInstall(OutputInterface $output, string $sPackageName): void
    {
        $oComposer = Composer::fromPackageName(new Composer\Name($sPackageName));
        $aBacktrace = debug_backtrace();
        foreach ($aBacktrace as $aItem) {
            echo $aItem['file'] . ':' . $aItem['line'] . PHP_EOL;
        }
        $oPackageInfo = PackageInfoFactory::get($oComposer);

        $oMigrateCommand = $this->getApplication()->find('package:migrate');
        $oInput = new ArrayInput($aInput = [
            'domain' => [$oPackageInfo->getSystemId()]
        ]);
        $oMigrateCommand->run($oInput, $output);
    }
}
