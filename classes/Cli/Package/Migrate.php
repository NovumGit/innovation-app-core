<?php

namespace Cli\Package;

use Cli\BaseCommand;
use Cli\Helper\Questions\Domain;
use Cli\Package\Migrate\Main;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Migrate extends BaseCommand
{
    private $aCommands = [
        ['command' => 'db:make', 'arguments' => []],
        ['command' => 'crud:generate', 'arguments' => []],
        ['command' => 'controller:generate', 'arguments' => []],
        ['command' => 'package:migrate', 'arguments' => []]
    ];

    protected function configure()
    {
        $this->setName("package:migrate");

        $aCommandNames = [];
        array_walk($this->aCommands, function ($mCommand) use ($aCommandNames) {
            $aCommandNames[] = "<info>{$mCommand['command']}</info>";
        });

        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, Domain::getInfo(true));

        $this->setDescription("Wrapper that executes " . join(', ', $aCommandNames) . " in a single go");
        $this->setHelp(<<<EOT
Based on your schema.xml file a lot of code is generated. Several individual commands need to be executed to generate
all the code that will make up the system. The package migrate command runs all the commands required to bring the 
system up to date with your schema.xml file. This includes, view, models, controllers and crud elements / classes.
EOT
        );
        // $this->addOption('verbose', 'v', InputOption::VALUE_NONE);
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $aDomainsStr = $input->getArgument('domain');
        $aDomains = [];
        foreach ($aDomainsStr as $sDomain) {
            $aDomains[] = $sDomain;
        }

        $oMain = new Main($this, $input, $output, $aDomains, $this->aCommands);
        $oMain->run();

        $output->writeln('Executing');
        return Command::SUCCESS;
    }
}
