<?php

namespace Cli\Package;

use Api\Packagist\Api;
use Cli\BaseCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class Find extends BaseCommand
{
    protected function configure()
    {
        $this->setName("package:find");
        $this->addOption('type');
        $this->addOption('name');
        $this->addArgument('tokens');
        $this->setDescription("Find / find installable packages");
        $sSystemName = function_exists('getSiteSettings') ? getSiteSettings()['system_name'] : 'Innovation App';
        $this->setHelp(<<<EOT
Packagist is the default way of sharing components, libraries or pieces of code in the PHP landscape. This command 
allows you to search for installable packages from the Packagist API that are specifically written for installation in 
{$sSystemName}. For more info refer to the Components or Plugins section of the documentation.  
EOT
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $oApi = new Api();
        $aInstallablePackages = $oApi->getInstallablePackages();

        if (!is_iterable($aInstallablePackages)) {
            $output->writeln("<error>No installable packages found</error>");
            return Command::FAILURE;
        }

        $aRows = [];
        $sPrevType = null;
        $i = 0;
        $aPackageIndex = [];

        $output->writeln("<info>Seeking installable packages in packagist</info>");
        foreach ($aInstallablePackages as $aInstallablePackage) {
            if ($sPrevType && $aInstallablePackage['type'] !== $sPrevType) {
                $aRows[] = new TableSeparator();
            }
            $sPrevType = $aInstallablePackage['type'];
            $i++;

            $bInstalled = (bool) $aInstallablePackage['installed'];

            $color = function ($value) use ($bInstalled) {
                if ($bInstalled) {
                    return "<fg=white;bg=green>{$value}</>";
                }
                return $value;
            };
            $aPackageIndex[$i] = $aInstallablePackage;
            $aRows[] = [
                $color($aInstallablePackage['installed'] ? '-' : $i),
                $color($aInstallablePackage['name']),
                $color($aInstallablePackage['type']),
                $color($aInstallablePackage['url']),
                $color($bInstalled ? 'installed' : '')
            ];
        }

        $table = new Table($output);
        $table->setHeaders(['#', 'Package name', 'Package type', 'Package url', 'Installed']);
        $table->setRows($aRows);
        $table->render();

        $helper = $this->getHelper('question');
        if ($helper instanceof QuestionHelper) {
            while (true) {
                $installSomethingQuestion = new Question(
                    '<question>Would you like to install one of these packages?</question> (<info>y</info>/<info>n</info>)[<comment>n</comment>]:  ',
                    'n'
                );
                $installSomething = strtolower($helper->ask($input, $output, $installSomethingQuestion));

                if (!$installSomething || $installSomething === 'n' || $installSomething === 'no') {
                    return Command::SUCCESS;
                } elseif ($installSomething === 'y' || $installSomething === 'yes') {
                    break;
                }
                $output->writeln('<error>Invalid answer, please answer with y or n.</error>');
            }


            $output->writeln('Please select the package that you would like to install');
            $question = new Question(
                '<question>Enter the package <info>id</info> or <info>name</info> to install or enter to quit</question>: ',
                0
            );

            $package = $helper->ask($input, $output, $question);

            if ($package) {
                $output->writeln("Installing: <info>{$aPackageIndex[$package]['name']}</info>");

                $oCommand = $this->getApplication()->find('package:install');

                if (is_numeric($package)) {
                    $package = $aPackageIndex[$package]['name'];
                }

                // $oPackageInfoObject  = $oApi->getPackageDetails($package);


                $output->writeln("Initializing database + generating code for: <info>{$package}</info>");
                $aInput = new ArrayInput([
                    '--package_name' => [$package],
                    '--full' => 'yes'
                ]);
                $oCommand->run($aInput, $output);

/*
                $oCommand = $this->getApplication()->find('package:migrate');
                $aInput = new ArrayInput([
                    'domain' => $oPackageInfoObject->getSystemId()
                ]);
                $oCommand->run($aInput, $output);
*/
            } else {
                $output->writeln("Nothing to install");

            }
            return Command::SUCCESS;
        }
        return Command::SUCCESS;
    }
}
