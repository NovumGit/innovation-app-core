<?php

namespace Cli\Package;

use Api\Packagist\Api;
use Cli\BaseCommand;
use Cli\Package\Uninstall\Domain;
use Cli\Package\Uninstall\Site;
use Cli\System\Init\Extender\Factory as SystemTypeFactory;
use Hurah\Types\Type\Composer;
use Hurah\Types\Type\PluginType;
use Hurah\Types\Type\SiteJson;
use Core\Utils;
use Exception;
use Exception\FileNotFoundException;
use Helper\Package\Components\PackageInfoFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class Uninstall extends BaseCommand
{
    public function __construct()
    {
        if (!isset($_SERVER['SYSTEM_ROOT'])) {
            exit("Environment variable SYSTEM_ROOT is missing, this is required");
        }
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName("package:uninstall");

        $this->setDefinition([
            new InputOption('service_name', null, InputOption::VALUE_OPTIONAL, 'Name of the service (e.g. "belastingdienst" or "todo")'),
            new InputOption('vendor', null, InputOption::VALUE_OPTIONAL, 'Unique identifier of your company or you (e.g. "novum" or "monolog")'),
            new InputOption('domain_name', null, InputOption::VALUE_REQUIRED, 'Base domain / dns name of new system (e.g. novum.nu)'),
            new InputOption('system_type', null, InputOption::VALUE_OPTIONAL, 'Type of system (one of: ' . join(', ', SystemTypeFactory::systemTypes) . ')'),
            new InputOption('package_name', null, InputOption::VALUE_OPTIONAL, 'Package name can be used instead of all the other options. (vendor/package)'),
            new InputOption('remove_db', null, InputOption::VALUE_OPTIONAL, 'When set to yes the database is also dropped and the user is deleted'),

        ]);

        $this->setDescription("Uninstall a package and all the symlinks that reference it");
        $sSystemName = function_exists('getSiteSettings') ? getSiteSettings()['system_name'] : 'Innovation App';

        $help = <<<EOT
To make working and sharing code written in $sSystemName as easy as possible we have centralized all the code and 
components that are, under normal circumstances, relevant for you as a developer. In reality the code that you write is 
spread and distributed over many folders, mostly done with symbolic links. This makes uninstalling a system a bit more
complex as you might have to remove 10 or so symbolic links. This is why it is better to use the uninstall feature.

If you are uninstalling a package that you created based on <info>system:create</info> or <info>system:init</info>, 
nothing is actually deleted. Your package will be still there inside the data folder at 
<info>data/local-repository/[site|domain|api]/[your-name]</info>. You can re-install your system by running 
<info>package:install [package-name]</info>

Databases are never touched by this utility.

EOT;
        $this->setHelp($help);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {

        if (!$sPackageName = $input->getOption('package_name')) {
            while (true) {
                $aInstalledPackages = self::viewInstalledPackageList($output);
                $helper = $this->getHelper('question');
                $sPackageToRemove = new Question('<question>Which package would you like to uninstall? (ctrl+c to cancel)</question>:  ', null);
                $sPackageName = strtolower($helper->ask($input, $output, $sPackageToRemove));

                if (is_numeric($sPackageName) && isset($aInstalledPackages[$sPackageName])) {
                    $sPackageName = $aInstalledPackages[$sPackageName]['name'];
                }

                try {
                    Composer::fromPackageName(new Composer\Name($sPackageName));
                    $input->setOption('package_name', $sPackageName);
                    break;
                } catch (FileNotFoundException $e) {
                    $output->writeln("<error>No package found that goes by the name $sPackageName</error>");
                    continue;
                }
            }
        }

        $sRemoveDb = $input->getOption('remove_db');
        $aRemoveDbOptions = [
            'yes',
            'no',
            'y',
            'n',
        ];
        while (true) {
            if (in_array($sRemoveDb, $aRemoveDbOptions)) {
                break;
            }

            $helper = $this->getHelper('question');
            $sPackageToRemove = new Question('<question>Remove the database also?</question> (<info>y</info>/<info>n</info>)[<comment>n</comment>]:  ', null);
            $sRemoveDb = strtolower($helper->ask($input, $output, $sPackageToRemove));
        }

        $sRemoveDb = str_replace('yes', 'y', $sRemoveDb);
        $sRemoveDb = str_replace('no', 'n', $sRemoveDb);
        $input->setOption('remove_db', $sRemoveDb);
    }

    private static function viewInstalledPackageList(OutputInterface $output): array
    {
        $oApi = new Api();
        $aInstallablePackages = $oApi->getInstallablePackages();

        if (!is_iterable($aInstallablePackages)) {
            $output->writeln("<error>No installable packages found</error>");
            return [];
        }

        $aRows = [];
        $sPrevType = null;
        $i = 0;
        $aPackageIndex = [];

        $output->writeln("<info>Installed packages:</info>");
        foreach ($aInstallablePackages as $aInstallablePackage) {
            if (!$aInstallablePackage['installed']) {
                continue;
            }
            if ($sPrevType && $aInstallablePackage['type'] !== $sPrevType) {
                $aRows[] = new TableSeparator();
            }
            $sPrevType = $aInstallablePackage['type'];
            $i++;

            $bInstalled = (bool)$aInstallablePackage['installed'];

            $color = function ($value) use ($bInstalled) {
                if ($bInstalled) {
                    return "<fg=white>{$value}</>";
                }
                return $value;
            };
            $aPackageIndex[$i] = $aInstallablePackage;
            $aRows[] = [
                $i,
                $color($aInstallablePackage['name']),
                $color($aInstallablePackage['type']),
                $color($aInstallablePackage['url']),
                $color($bInstalled ? 'installed' : ''),
            ];
        }
        $table = new Table($output);
        $table->setHeaders([
            '#',
            'Package name',
            'Package type',
            'Package url',
        ]);
        $table->setRows($aRows);
        $table->render();

        return $aPackageIndex;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            if ($input->getOption('remove_db') === 'y') {
                $sPackageName = $input->getOption('package_name');
                $output->writeln("Removing database that belongs to <info>$sPackageName</info> ");
                $this->removeDb($input, $output);
            }

            $this->removePackage($input, $output);
        } catch (Exception $e) {
            $output->writeln("<error>Could not remove package</error>");
            $output->writeln("<error>{$e->getMessage()}</error>");
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }

    private function removePackage(InputInterface $input, OutputInterface $output): void
    {
        if (!$sPackageName = $input->getOption('package_name')) {
            $sPackageName = $input->getOption('vendor') . "/" . $input->getOption('system_type') . "-" . $input->getOption('service_name');
        }

        $sPackageRoot = Utils::makePath($_SERVER['SYSTEM_ROOT'], 'vendor', $sPackageName);

        if (!is_dir($sPackageRoot)) {
            $sMessage = 'Package $sPackageName not found, are you sure it was installed?';
            $output->writeln("<error>$sMessage</error>");
            throw new FileNotFoundException($sMessage);
        }

        $oPackageComposer = Composer::fromPath(Utils::makePath($sPackageRoot, 'composer.json'));

        if (
            in_array((string)$oPackageComposer->getType(), [
            PluginType::API,
            PluginType::SITE,
            ])
        ) {
            $output->writeln("<comment>Removing:</comment> <info>{$oPackageComposer->getName()}</info>");

            $sSitePath = Utils::makePath($sPackageRoot, 'site.json');
            $oSiteJson = new SiteJson($sSitePath);
            Site::uninstall($oPackageComposer, $oSiteJson, $output);
            $output->writeln("<comment>Removed:</comment> <info>{$oPackageComposer->getName()}</info>");
        } else {
            if ((string)$oPackageComposer->getType() == PluginType::DOMAIN) {
                $sDomainConfigFile = Utils::makePath($sPackageRoot, 'config.php');
                $aDomainConfig = require_once $sDomainConfigFile;

                $output->writeln("<comment>Removing:</comment> <info>{$oPackageComposer->getName()}</info>");
                Domain::uninstall($oPackageComposer, $aDomainConfig, $output);
                $output->writeln("<comment>Removed:</comment> <info>{$oPackageComposer->getName()}</info>");
            }
        }
    }

    private function removeDb(InputInterface $input, OutputInterface $output): void
    {

        $sPackageName = $input->getOption('package_name');
        $oComposer = Composer::fromPackageName(new Composer\Name($sPackageName));
        $oPackageInfo = PackageInfoFactory::get($oComposer);

        $output->writeln("Removing database <info>{$oPackageInfo->getSystemId()}</info>");

        $oCommand = $this->getApplication()->find('db:uninstall');

        $oInput = new ArrayInput([
            'domain' => [(string)$oPackageInfo->getSystemId()],
        ]);
        $oCommand->run($oInput, $output);
    }
}
