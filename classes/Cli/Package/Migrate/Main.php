<?php

namespace Cli\Package\Migrate;

use Cli\BaseCommand;
use Composer\IO\ConsoleIO;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Main
{
    private InputInterface $input;
    private OutputInterface $output;
    private array $aCommands;
    private array $aDomains;
    private BaseCommand $oCommand;

    /**
     * Main constructor.
     * @param BaseCommand $oCommand
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param array $aDomains
     * @param array $aCommands
     */
    public function __construct(BaseCommand $oCommand, InputInterface $input, OutputInterface $output, array $aDomains, array $aCommands)
    {
        $this->oCommand = $oCommand;
        $this->input = $input;
        $this->output = $output;
        $this->aCommands = $aCommands;
        $this->aDomains = $aDomains;
    }

    public function run()
    {

        $this->oCommand->setIO(new ConsoleIO($this->input, $this->output, $this->oCommand->getHelperSet()));

        $io = $this->oCommand->getIO();

        $formatter = $this->oCommand->getHelperSet()->get('formatter');

        $this->output->writeln("Running full migration");
        $this->output->writeln("<error>domains</error>");
        foreach ($this->aDomains as $sDomain) {
            foreach ($this->aCommands as $aCommand) {
                $io->writeError([
                    '',
                    $formatter->formatBlock('Executing ' . $aCommand['command'] . ' - ' . json_encode($aCommand['arguments']), 'bg=blue;fg=white', true),
                    '',
                ]);

                $this->command($aCommand['command'], $sDomain, $aCommand['arguments']);
            }
        }
    }

    private function command(string $sCommand, string $sDomain, array $aExtraInput = [])
    {
        $this->output->writeln("Executing <info>{$sCommand}</info> on <info>$sDomain</info>");
        $command = $this->oCommand->getApplication()->find($sCommand);

        $aCommand = array_merge(['domain' => [$sDomain]], $aExtraInput);
        $aInput = new ArrayInput($aCommand);

        echo $sCommand . ' domain -> ' . join(' ', $aCommand['domain']) . PHP_EOL;

        $returnCode = $command->run($aInput, $this->output);
        if ($returnCode === Command::SUCCESS) {
            $this->output->writeln("Command {$sCommand} executed <info>succesfully</info>");
        } else {
            $this->output->writeln("<warning>Command {$sCommand} on {$sDomain} execution failed</warning>");
        }
    }
}
