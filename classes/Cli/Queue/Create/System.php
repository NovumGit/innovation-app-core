<?php

namespace Cli\Queue\Create;

use Cli\BaseCommand;
use Core\Json\JsonUtils;
use Crud\User\CrudUserManager;
use DirectoryIterator;
use Hi\Helpers\DirectoryStructure;
use Hurah\Types\Type\Path;
use Hurah\Types\Type\PathCollection;
use Hurah\Types\Util\FileSystem;
use Exception\InvalidArgumentException;
use Model\Account\Base\UserQuery;
use Model\Setting\MasterTable\RoleQuery;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class System extends BaseCommand
{

    protected function configure()
    {
        $this->setName("queue:system:create");
        $this->addOption('test', 't', InputOption::VALUE_NONE, 'Will not move the file away to a processing folder');
        /*
                $this->setDefinition([
                    new InputOption('service_name', null, InputOption::VALUE_OPTIONAL, 'Name of the service (e.g. "belastingdienst" or "todo")'),
                    new InputOption('vendor', null, InputOption::VALUE_OPTIONAL, 'Unique identifier of your company or you (e.g. "novum" or "monolog")'),
                    new InputOption('domain_name', null, InputOption::VALUE_REQUIRED, 'Base domain / dns name of new system (e.g. novum.nu)'),
                    new InputOption('system_type', null, InputOption::VALUE_OPTIONAL, 'Type of system (one of: ' . join(', ', SystemTypeFactory::systemTypes) . ')'),
                    new InputOption('package_name', null, InputOption::VALUE_OPTIONAL, 'Package name can be used instead of all the other options. (vendor/package)'),
                ]);
        */
        $this->setDescription("Checks inside data folder for json files that are generated from the UI to create new systems");
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $oPendingSystemCreateTasks = $this->makeJsonTaskList();
        $this->createSystems($oPendingSystemCreateTasks, $input, $output);
        return Command::SUCCESS;
    }

    /**
     * @param OutputInterface $output
     * @param InputInterface $input
     * @param Path $oSystemCreateJsonTaskFilePath
     * @throws \Hurah\Types\Exception\InvalidArgumentException
     */
    private function createSystem(OutputInterface $output, InputInterface $input, Path $oSystemCreateJsonTaskFilePath): void
    {
        $oProcessDir = $this->getQueueRoot()->extend('processing')->makeDir();
        $oArchiveDir = $this->getQueueRoot()->extend('archive')->makeDir();

        $output->writeln("Found file to process: <info>{$oSystemCreateJsonTaskFilePath->basename()}</info>");

        if (!$input->getOption('test')) {
            $output->writeln("Moving <info>{$oSystemCreateJsonTaskFilePath->basename()}</info> to <info>processing</info> directory");
            $oSystemCreateJsonTaskFilePath->move(FileSystem::makePath($oProcessDir, $oSystemCreateJsonTaskFilePath->basename()));
        }

        $aJsonContents = JsonUtils::decode($oSystemCreateJsonTaskFilePath->contents());
        $this->processCreateSystemSteps($aJsonContents, $input, $output);

        if (!$input->getOption('test')) {
            $output->writeln("Moving <info>{$oSystemCreateJsonTaskFilePath->basename()}</info> to <info>archive</info> directory");
            $this->archiveTask($oSystemCreateJsonTaskFilePath, $oArchiveDir);
        }
    }
    private function processCreateSystemSteps(array $aJob, InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Running system:create command for {$aJob['vendor']}/{$aJob['service_name']}");

        $commandInput = $this->createInput($aJob);
        $oCommand = $this->getApplication()->find('system:create');
        $oCommand->run($commandInput, $output);

        $output->writeln("Creating user <info>{$aJob['email']}</info>");
        $this->addUser($aJob);

        $output->writeln("Creating SSL cert job for domain <info>{$aJob['domain']}</info>");
        $this->addCertJob($aJob);

        $output->writeln("Registering  SSL cert job for domain <info>{$aJob['domain']}</info>");

    }


    private function makeJsonTaskList(): PathCollection
    {
        $oTaskListDirectoryIterator = $this->getTaskListDirectoryIterator();
        $oPathCollection = $this->collectTasks($oTaskListDirectoryIterator);
        return $oPathCollection;
    }

    /**
     * @return DirectoryIterator
     */
    private function getTaskListDirectoryIterator(): DirectoryIterator
    {
        $oSystemCreateSource = $this->getQueueRoot()->extend('create');
        $oDirectoryIterator = $oSystemCreateSource->getDirectoryIterator();
        return $oDirectoryIterator;
    }

    private function getQueueRoot(): Path
    {
        $oDirectoryStructure = new DirectoryStructure();
        return FileSystem::makePath($oDirectoryStructure->getDataDir(), 'queue', 'system');
    }

    /**
     * @param DirectoryIterator $oTaskListDirectoryIterator
     * @return PathCollection
     */
    private function collectTasks(DirectoryIterator $oTaskListDirectoryIterator): PathCollection
    {
        $oPathCollection = new PathCollection();
        foreach ($oTaskListDirectoryIterator as $oCandidate) {
            if ($oCandidate->getExtension() === 'json') {
                try
                {
                    $oPathCollection->add($oCandidate->getPathname());
                }
                catch (InvalidArgumentException $e)
                {
                    $this->getIO()->error("{$oCandidate->getPathname()} is not a valid Path");
                }

            }
        }
        return $oPathCollection;
    }

    private function createSystems(PathCollection $oWorkLoad, InputInterface $input, OutputInterface $output)
    {
        if (is_iterable($oWorkLoad)) {
            foreach ($oWorkLoad as $oPath) {
                $this->createSystem($output, $input, $oPath);
            }
            $oWorkLoad->rewind();
        }
    }


    private function createInput(array $aJob): InputInterface
    {
        return new ArrayInput([
            '--system_type' => $aJob['type'],
            '--service_name' => $aJob['service_name'],
            '--vendor' => $aJob['vendor'],
            '--domain_name' => $aJob['domain'],
            '--homepage' => $aJob['homepage'],
            '--description' => $aJob['description'],
            '--license' => $aJob['license'],
            '--stability' => 'dev',
            '--author' => $aJob['first_name'] . ' ' . $aJob['last_name'] . ' <' . $aJob['email'] . '>',
            '--authors' => [$aJob['first_name'] . ' ' . $aJob['last_name'] . ' <' . $aJob['email'] . '>'],
            '--keywords' => explode(',', $aJob['keywords']),
            '--prefer_stable' => true,
            '--add_domain' => true
        ]);
    }

    private function addUser(array $aJob)
    {
        $oUserQuery = UserQuery::create();
        $oUser = $oUserQuery->findOneByEmail($aJob['email']);

        $aUser = [
            'id' => $oUser ? $oUser->getId() : null,
            'is_deleted' => false,
            'can_change_roles' => true,
            'role_id' => RoleQuery::create()->findOneByName('Admin')->getId(),
            'first_name' => $aJob['first_name'],
            'last_name' => $aJob['last_name'],
            'email' => $aJob['email'],
            'password' => $aJob['password']

        ];

        $oCrudUserManager = new CrudUserManager();
        $oCrudUserManager->save($aUser);
    }

    private function addCertJob(array $aJob)
    {
        $oDirectoryStructure = new DirectoryStructure();
        $oCertDir = FileSystem::makePath($oDirectoryStructure->getDataDir(), 'queue', 'certs');
        $oCertDir->extend($aJob['domain'])->write(JsonUtils::encode($aJob, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT));
    }

    /**
     * @param Path $oSystemCreateJsonTaskFilePath
     * @param Path $oArchiveDir
     * @throws \Hurah\Types\Exception\InvalidArgumentException
     */
    private function archiveTask(Path $oSystemCreateJsonTaskFilePath, Path $oArchiveDir): void
    {
        $oSystemCreateJsonTaskFilePath->move(FileSystem::makePath($oArchiveDir, $oSystemCreateJsonTaskFilePath->basename()));
        $aContents = JsonUtils::decode($oSystemCreateJsonTaskFilePath->contents());
        $aContents['password'] = '***********';
        $oSystemCreateJsonTaskFilePath->write(JsonUtils::encode($aContents, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
    }

}

