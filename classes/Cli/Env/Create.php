<?php

namespace Cli\Env;

use Cli\BaseCommand;
use Composer\IO\ConsoleIO;
use Hurah\Types\Type\Composer\ServiceName;
use Hurah\Types\Type\Composer\Vendor;
use Hurah\Types\Type\SystemId;
use Generator\Env\Builder;
use Generator\Env\EnvConfig;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Create extends BaseCommand
{

    protected function configure()
    {
        $this->setName("env:create");

        $this->setDefinition([
            new InputOption('service_name', null, InputOption::VALUE_REQUIRED, 'Name of the service (e.g. "belastingdienst" or "todo")'),
            new InputOption('vendor', null, InputOption::VALUE_REQUIRED, 'Unique identifier of your company or you (e.g. "novum" or "monolog")'),
        ]);
        $this->setDescription("Generate an environment file based on the system id.");
        $this->setHelp(<<<EOT
This command is normally called in the context of another command such as <info>system:create</info>. When called on it's
own it will create an environment configuration file with a newly generated mysql password combined with a fixed database
name and username so it might lock you out of your database. To regain access to your database <info>db:pass-reset</info>
can be invoked which generates a new password again but also re-configures all the configurations and grants you access
to the database. If no database exists you can populate a new database by calling <info>db:make</info>.  
EOT
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->setIO(new ConsoleIO($input, $output, $this->getHelperSet()));

        $io = $this->getIO();

        (new \Cli\Helper\Questions\Vendor())->ask($io, $input);
        (new \Cli\Helper\Questions\ServiceName())->ask($io, $input);

        parent::initialize($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $sVendor = $input->getOption('vendor');
        $sServiceName = $input->getOption('service_name');

        $oSystemId = SystemId::make(new Vendor($sVendor), new ServiceName($sServiceName));
        $output->writeln("<comment>Creating .env file for</comment> <info>{$oSystemId}</info>");

        $oEnvBuilder = new Builder();
        $oEnvBuilder->create(new EnvConfig($oSystemId));
        return Command::SUCCESS;
    }
}
