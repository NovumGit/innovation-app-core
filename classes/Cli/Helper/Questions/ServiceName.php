<?php

namespace Cli\Helper\Questions;

use InvalidArgumentException;

class ServiceName
{

    public function ask($io, $input)
    {

        if($service_name = $input->getOption('service_name'))
        {
            //@todo the validation below is disabled now when argument is passed in command, need to fix asap.
            return;
        }

        $cwd = realpath(".");
        if (!$service_name = $input->getOption('service_name')) {
            $service_name = basename($cwd);
            $service_name = preg_replace('{(?:([a-z])([A-Z])|([A-Z])([A-Z][a-z]))}', '\\1\\3-\\2\\4', $service_name);
            $service_name = strtolower($service_name);
        } else {
            if (!preg_match('/[a-z]{1}[a-z0-9-]+/', $service_name)) {
                throw new InvalidArgumentException('The system name ' . $service_name . ' is invalid, it should be lowercase and only contain characters, numbers and dashes');
            }
        }

        $service_name = $io->askAndValidate('System name (<service_name>) [<comment>' . $service_name . '</comment>]: ', function ($value) use ($service_name) {
            if (null === $value) {
                return $service_name;
            }
            if (!preg_match('/[a-z]{1}[a-z0-9-]+/', $value)) {
                throw new InvalidArgumentException('The system name ' . $service_name . ' is invalid, it should be lowercase and only contain characters, numbers and dashes');
            }
            return $value;
        }, null, $service_name);
        $input->setOption('service_name', $service_name);
    }
}
