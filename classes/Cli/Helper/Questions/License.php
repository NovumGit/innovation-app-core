<?php

namespace Cli\Helper\Questions;

use Symfony\Component\Console\Input\InputInterface;

class License
{


    function ask($io, InputInterface $input)
    {

        $license = $input->getOption('license');
        if ($license) {
            //@todo the validation below is disabled now when argument is passed in command, need to fix asap.
            return;
        }
        // <license>
        if (null === $license) {
            $license = 'MIT';
        }

        $license = $io->ask(
            'License [<comment>' . $license . '</comment>]: ',
            $license
        );
        $input->setOption('license', $license);
        // </license>

    }
}
