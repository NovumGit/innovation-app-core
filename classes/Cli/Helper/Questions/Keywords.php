<?php
namespace Cli\Helper\Questions;

class Keywords
{

    function ask($io, $input) {

        $keywords = $input->getOption('keywords');

        if($keywords)
        {
            //@todo the validation below is disabled now when argument is passed in command, need to fix asap.
            return;
        }

        // <keywords>

        $keywords = $io->askAndValidate(
            'Keywords (comma separated list) (<keywords>) [<comment>' . join(', ', $keywords) . '</comment>]: ',
            function ($value) use ($keywords) {
                if (null === $value) {
                    return $keywords;
                }
                return $keywords;
            },
            null,
            $keywords
        );
        $input->setOption('keywords', $keywords);

    }
}
