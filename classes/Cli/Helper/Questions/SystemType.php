<?php

namespace Cli\Helper\Questions;

use Cli\System\Init\Extender\Factory as SystemTypeFactory;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;


class SystemType
{


    function ask($io, InputInterface $input)
    {

        $system_type = $input->getOption('system_type');

        if ($system_type) {
            //@todo the validation below is disabled now when argument is passed in command, need to fix asap.
            return;
        }


        // <system_type>


        $system_type = $io->askAndValidate(
            'Package Type (e.g. ' . join(', ', SystemTypeFactory::systemTypes) . ') [<comment>' . $system_type . '</comment>]: ',
            function ($value) use ($system_type) {

                if (!in_array($value, SystemTypeFactory::systemTypes)) {
                    throw new InvalidArgumentException(
                        'Invalid system type "' . $value . '". Must be empty or one of: ' .
                        join(', ', SystemTypeFactory::systemTypes)
                    );
                }

                return $value;
            },
            null,
            $system_type
        );
        $input->setOption('system_type', $system_type);
        // </system_type>

    }
}
