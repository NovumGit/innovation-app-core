<?php
namespace Cli\Helper\Questions;

class Description
{

    function ask($io, $input) {

        $description = $input->getOption('description') ?: false;


        if($description)
        {
            //@todo the validation below is disabled now when argument is passed in command, need to fix asap.
            return;
        }

        // <description>

        $description = $io->ask(
            'Description [<comment>' . $description . '</comment>]: ',
            $description
        );
        $input->setOption('description', $description);
        // </description>

    }
}
