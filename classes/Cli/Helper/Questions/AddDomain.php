<?php

namespace Cli\Helper\Questions;

use Hurah\Types\Type\PluginType;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;

class AddDomain
{


    function ask($io, InputInterface $input)
    {

        $add_domain = $input->getOption('add_domain');
        if ($add_domain) {
            return;
        }
        $system_type = $input->getOption('system_type');

        if (PluginType::hasDomainDependency($system_type)) {
            $add_domain = $input->getOption('add_domain');
            $add_domain = $io->askAndValidate(
                'Would you like to generate the domain files also if not present? [<comment>' . ($add_domain ? 'yes' : 'no') . '</comment>]: ',
                function ($value) use ($add_domain) {

                    if (!in_array($add_domain, ['yes', 'no'])) {
                        throw new InvalidArgumentException(
                            'Please answer with yes or no'
                        );
                    }
                    return $value;
                },
                null,
                $add_domain
            );
            $input->setOption('add_domain', $add_domain === 'yes' || $add_domain === true);
        } else {
            $output = new ConsoleOutput();
            $output->writeln("<error>$system_type has no domain dependency</error>");
        }

    }
}
