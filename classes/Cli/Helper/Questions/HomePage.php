<?php

namespace Cli\Helper\Questions;

use Symfony\Component\Console\Input\InputInterface;

class HomePage
{


    function ask($io, InputInterface $input)
    {

        $homepage = $input->getOption('homepage');

        if ($homepage) {
            //@todo the validation below is disabled now when argument is passed in command, need to fix asap.
            return;
        }

        if (null === $homepage) {
            $homepage = 'https://docs.demo.novum.nu/';
        }

        $homepage = $io->ask(
            'Homepage [<comment>' . $homepage . '</comment>]: ',
            $homepage
        );
        $input->setOption('homepage', $homepage);


    }
}
