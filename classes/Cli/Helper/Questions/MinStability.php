<?php

namespace Cli\Helper\Questions;

use Composer\Package\BasePackage;
use InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;

class MinStability
{


    function ask($io, InputInterface $input)
    {

        $minimumStability = $input->getOption('stability') ?: null;

        if ($minimumStability) {
            //@todo the validation below is disabled now when argument is passed in command, need to fix asap.
            return;
        }


        // <stability>

        $minimumStability = $io->askAndValidate(
            'Minimum Stability [<comment>' . $minimumStability . '</comment>]: ',
            function ($value) use ($minimumStability) {

                if (!isset(BasePackage::$stabilities[$value])) {
                    throw new InvalidArgumentException(
                        'Invalid minimum stability "' . $value . '". Must be empty or one of: ' .
                        implode(', ', array_keys(BasePackage::$stabilities))
                    );
                }

                return $value;
            },
            null,
            $minimumStability
        );
        $input->setOption('stability', $minimumStability);

    }
}
