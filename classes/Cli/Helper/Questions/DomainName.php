<?php

namespace Cli\Helper\Questions;

use Symfony\Component\Console\Input\InputInterface;

class DomainName
{


    function ask($io, InputInterface $input)
    {

        $domain_name = $input->getOption('domain_name');

        if ($domain_name) {//@todo the validation below is disabled now when argument is passed in command, need to fix asap.
            return;
        }


        $service_name = $input->getOption('service_name');

        // <domain_name>
        $domain_name = $service_name . '.demo.novum.nu';

        $domain_name = $io->ask(
            'Domain name [<comment>' . $domain_name . '</comment>]: ',
            $domain_name
        );
        $input->setOption('domain_name', $domain_name);
        // </domain_name>


    }
}
