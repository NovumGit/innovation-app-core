<?php

namespace Cli\Helper\Questions;

use InvalidArgumentException;

class PreferStable
{

    function ask($io, $input)
    {

        $prefer_stable = $input->getOption('prefer_stable');

        if ($prefer_stable) {
            //@todo the validation below is disabled now when argument is passed in command, need to fix asap.
            return;
        }

        $prefer_stable = $io->askAndValidate(
            'Prefer stable (<prefer_stable>) [<comment>' . ($prefer_stable ? 'yes' : 'no') . '</comment>]: ',
            function ($value) use ($prefer_stable) {
                if (null === $value) {
                    return $prefer_stable;
                }

                if (!in_array($prefer_stable, ['yes', 'no'])) {
                    throw new InvalidArgumentException(
                        'Please answer with yes or no'
                    );
                }

                return $prefer_stable;
            },
            null,
            $prefer_stable
        );
        $input->setOption('prefer_stable', trim($prefer_stable) === 'yes');

    }
}
