<?php

namespace Cli\Helper\Questions;

use InvalidArgumentException;

class Vendor
{

    public function ask($io, $input)
    {

        if($vendor = $input->getOption('vendor'))
        {
            //@todo the validation below is disabled now when argument is passed in command, need to fix asap.
            return;
        }

        if (!$vendor = $input->getOption('vendor')) {
            $aExistingDomains = glob('./domain/*');
            if (isset($aExistingDomains[0])) {
                $aParts = explode('.', basename($aExistingDomains[0]));
                $vendor = $aParts[0];
            } else {
                if (!empty($_SERVER['COMPOSER_DEFAULT_VENDOR'])) {
                    $vendor = $_SERVER['COMPOSER_DEFAULT_VENDOR'];
                } elseif (isset($git['github.user'])) {
                    $vendor = $git['github.user'];
                } elseif (!empty($_SERVER['USERNAME'])) {
                    $vendor = $_SERVER['USERNAME'];
                } elseif (!empty($_SERVER['USER'])) {
                    $vendor = $_SERVER['USER'];
                } elseif (get_current_user()) {
                    $vendor = get_current_user();
                } else {
                    // package names must be in the format foo/bar
                    $vendor .= '/' . $vendor;
                }
            }
            $vendor = strtolower($vendor);
        } else {
            if (!preg_match('/[a-z]{1}[a-z0-9-]+/', $vendor)) {
                throw new InvalidArgumentException('The vendor name ' . $vendor . ' is invalid, it should be lowercase, only contain characters, numbers and dashes and start with a character');
            }
        }


        $vendor = $io->askAndValidate(
            'System vendor (<vendor>) [<comment>' . $vendor . '</comment>]: ',
            function ($value) use ($vendor) {
                if (null === $value) {
                    return $vendor;
                }
            if (!preg_match('/[a-z]{1}[a-z0-9-]+/', $vendor)) {
                throw new InvalidArgumentException('The vendor name ' . $value . ' is invalid,  lowercase, only contain characters, numbers and dashes and start with a character');
            }

            return $value;
        }, null, $vendor);

        $input->setOption('vendor', $vendor);
    }
}
