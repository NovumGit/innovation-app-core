<?php

namespace Cli\Helper\Questions;

use Cli\Helper\IInfo;
use Cli\BaseCommand;
use InvalidArgumentException;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Domain implements IInfo
{
    protected static $aDomainMap = [];

    public static function getInfo(bool $bPlural = false): string
    {
        if ($bPlural) {
            return 'Domain and system id are the same thing. It is a string made up out of a vendor name and a service name separated by a dot. For example: novum.belastingdienst. This command accepts multiple domains separated by a comma.';
        }
        return 'Domain and system id are the same thing. It is a string made up out of a vendor name and a service name separated by a dot. For example: novum.belastingdienst';
    }

    protected function domainExists(?string $domain, BaseCommand $command): bool
    {
        if ($domain) {
            foreach ($command->getAllDomains() as $oSeekDomain) {
                if ($domain === (string)$oSeekDomain->getSystemID()) {
                    return true;
                }
            }
        }
        return false;
    }

    public function ask($io, OutputInterface $output, InputInterface $input, BaseCommand $command)
    {

        $domain = $input->getArgument('domain');
        $bDomainValid = $this->domainExists($domain, $command);

        if (!$bDomainValid) {
            $output->writeln('');
            if ($domain) {
                $output->writeln('<comment>The specified domain is invalid or not installed. Please choose one of the domains below</comment>');
            } else {
                $output->writeln('<comment>Please pick a domain from the list below</comment>');
            }
            $aRows = [];

            foreach ($command->getAllDomains() as $i => $oDomain) {
                $iItem = $i + 1;
                $aRows[] = [
                    $iItem,
                    $oDomain->getSystemID(),
                ];
                self::$aDomainMap[$iItem] = (string)$oDomain->getSystemID();
            }
            $table = new Table($output);
            $table->setHeaders([
                'Id',
                'Domain',
            ])->setRows($aRows);

            $table->render();

            $domain = $io->askAndValidate('Domain (<vendor.package>) [<comment>' . $domain . '</comment>]: ', function ($value) use ($domain) {

                if (isset(self::$aDomainMap[$value])) {
                    return self::$aDomainMap[$value];
                } else {
                    if (in_array($value, self::$aDomainMap)) {
                        return $value;
                    }
                }
                if (null === $value) {
                    return $domain;
                }
                if (!preg_match('/[a-zA-Z0-9]+\.[a-zA-Z0-9]+/', $value)) {
                    throw new InvalidArgumentException('The domain name ' . $domain . ' is invalid, it must be one of ' . join(', ', self::$aDomainMap));
                }
                return $value;
            }, null, $domain);
            $input->setArgument('domain', $domain);
        }
    }
}
