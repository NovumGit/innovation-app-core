<?php

namespace Cli\Helper\Application;

use Hurah\Types\Type\PhpNamespace;
use Hurah\Types\Type\Path;
use Hi\Helpers\DirectoryStructure;
use ReflectionClass;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Finder\Finder;

class ApplicationLoader
{
    private Application $oApplication;
    private array $aDirectories;

    function __construct(string $sApplicationTitle = 'Novum Innovation App')
    {
        $this->oApplication = new Application($sApplicationTitle);
        $this->aDirectories = $this->getDefaultDirectories();
    }

    function addDirectory(Path $sPath, PhpNamespace $namespace, string $sCommandSuffix)
    {
        $this->aDirectories[] = [
            'ns'     => (string)$namespace,
            'path'   => (string)$sPath,
            'suffix' => $sCommandSuffix,
        ];
    }

    private function getDefaultDirectories(): array
    {
        $aDirectories = [];
        $oDirectoryStructure = new DirectoryStructure();
        if (!defined('SEP')) {
            define('SEP', DIRECTORY_SEPARATOR);
        }

        $oCommandDirectory = Path::make(
            $oDirectoryStructure->getSystemRoot(),
            $oDirectoryStructure->getSystemDir(),
            'classes',
            'Cli'
        );

        $oFinder = new Finder();
        $oFinder->directories()->in("{$oCommandDirectory}")->depth('< 3');

        foreach ($oFinder as $oFile) {

            $sSubPath = str_replace("{$oCommandDirectory}", '', $oFile);
            $iDepth = substr_count($sSubPath, DIRECTORY_SEPARATOR);

            $sFileCopy = (string)$oFile;

            $aNsComponents = [];
            for ($i = 0; $i < $iDepth; $i++) {
                $aNsComponents[] = basename($sFileCopy);
                $sFileCopy = dirname($sFileCopy);
            }

            $aDirectories[] = [
                'path' => $oFile->getPathname(),
                'ns'   => "\\Cli\\" . join("\\", array_reverse($aNsComponents)) . "\\",
            ];
        }
        return $aDirectories;
    }

    public function load(): Application
    {
        $aDirectories = $this->aDirectories;
        foreach ($aDirectories as $aDirectory) {
            $oFinder = new Finder();
            $oFinder->files()->name('*.php')->in($aDirectory['path'])->depth(0);

            $ns = $aDirectory['ns'];

            foreach ($oFinder as $file) {
                $r = new ReflectionClass($ns . $file->getBasename('.php'));
                if ($r->isSubclassOf(Command::class) && !$r->isAbstract()) {
                    $oCommand = $r->newInstance();
                    if (isset($aDirectory['suffix'])) {
                        $oCommand->setName($aDirectory['suffix'] . ':' . $oCommand->getName());
                    }

                    if ($oCommand instanceof Command) {
                        $this->oApplication->add($oCommand);
                    }
                }
            }
        }
        return $this->oApplication;
    }
}
