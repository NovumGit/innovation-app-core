<?php

namespace Cli\Helper;

interface IInfo
{
    public static function getInfo(): string;
}
