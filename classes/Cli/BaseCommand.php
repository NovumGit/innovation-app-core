<?php

namespace Cli;

use Exception;
use Hi\Helpers\DirectoryStructure;
use Hi\Helpers\Domain;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

abstract class BaseCommand extends \Composer\Command\BaseCommand
{
    public function getAllDomains()
    {
        $oDirectoryStructure = new DirectoryStructure();
        $aDomains = $oDirectoryStructure->getDomainCollection();
        return $aDomains;
    }
    protected function domainCollector(InputInterface $input, OutputInterface $output):int
    {
        $aDomains = $this->getAllDomains();

        $aSelectedDomains = $input->getArgument('domain');

        if (empty($aDomains)) {
            $this->noDomainsMessage($output);
            return Command::FAILURE;
        } elseif (empty($aSelectedDomains)) {
            try {
                $this->selectDomainMessage($input, $output, $aDomains);
            } catch (Exception $e) {
                $output->writeln("<error>{$e->getMessage()}</error>");
                return Command::FAILURE;
            }
        }
        return Command::SUCCESS;
    }

    protected function noDomainsMessage(OutputInterface $output): int
    {
        $output->writeln("<question>No databases / domains available</question>");
        $output->writeln("Databases are bound to a domain, you must first install or create a domain");
        $output->writeln("Here are some domain packages you could install:");
        $sHr = "---------------------------------------";
        $output->writeln($sHr);
        $command = $this->getApplication()->find('package:find');
        $arguments = [
            '--type'    =>  'novum-domain',
            'tokens'  => ['novum']
        ];

        $searchInput = new ArrayInput($arguments);
        $returnCode = $command->run($searchInput, $output);
        $output->writeln($sHr);
        //  composer search --type='novum-domain' novum
        $output->writeln("<info>Installing is as easy as typing \"composer require <vendor/package>\"</info>");
        $output->writeln("<info>For example \"composer require novum/domain-svb\"</info>");
        return $returnCode;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @param Domain[] $aDomain
     * @throws Exception
     */
    protected function selectDomainMessage(InputInterface $input, OutputInterface $output, array $aDomain): void
    {
        $output->writeln("<question>This command requires a domain, please choose one</question>");
        $this->hr($output);

        $i = 0;
        $aDomainIndex = [];
        foreach ($aDomain as $domain) {
            $i++;
            $aDomainIndex[$i] = $domain->getSystemID();
            $output->writeln("{$i}: {$domain->getSystemID()}");
        }
        $this->hr($output);

        $helper = $this->getHelper('question');

        $question = new Question("<question>Select the appropriate numbers separated by commas and/or spaces, or leave input" . PHP_EOL . "blank to select all (Enter 'c' to cancel): </question>", 'all');
        $sDomainInput = $helper->ask($input, $output, $question);

        $aDomainInput = explode(' ', str_replace(',', ' ', $sDomainInput));
        $aValues = [];
        foreach ($aDomainInput as $sDomainItem) {
            $sDomainItem = trim($sDomainItem);
            if (trim($sDomainItem) === 'all') {
                $input->setArgument('domain', 'all');
            } elseif (is_numeric($sDomainItem) && isset($aDomainIndex[$sDomainItem])) {
                $output->writeln("Executing task on <info>{$aDomainIndex[$sDomainItem]}</info>");
                $aValues[] = $aDomainIndex[$sDomainItem];
            } else {
                $output->writeln("<warning>$sDomainItem is not a valid option, skipping</warning>");
            }
        }

        if (!empty($aValues)) {
            $input->setArgument('domain', $aValues);
        } else {
            $input->setArgument('domain', $aDomainIndex);
        }
    }

    protected function hr(OutputInterface $output)
    {
        $output->writeln('- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -');
    }
}
