<?php

namespace Cli\Controller\Generate;

use Generator\Generators\Admin\Module\Controller\FromSchema;
use Hi\Helpers\DirectoryStructure;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Main
{
    private InputInterface $input;
    private OutputInterface $output;
    private QuestionHelper $helper;
    private array $aDomains;

    public function __construct(InputInterface $input, OutputInterface $output, QuestionHelper $helper, array $aDomains)
    {
        $this->input = $input;
        $this->output = $output;
        $this->helper = $helper;
        $this->aDomains = $aDomains;
    }

    public function run()
    {

        foreach ($this->aDomains as $sDomainKey) {
            $this->output->writeln("Running Controller generator for domain <info>{$sDomainKey}</info>");
            $oDirectoryStructure = new DirectoryStructure();
            $sSchemaLocation = $oDirectoryStructure->getSystemRoot() . DIRECTORY_SEPARATOR . $oDirectoryStructure->getSystemDir() . DIRECTORY_SEPARATOR . 'build' . DIRECTORY_SEPARATOR . 'database' . DIRECTORY_SEPARATOR . $sDomainKey . DIRECTORY_SEPARATOR . 'schema.xml';

            $oCrudFromSchema = new FromSchema($sSchemaLocation, null, $this->output);
            $oCrudFromSchema->run();
        }
    }
}
