<?php

namespace Cli\Controller;

use Cli\BaseCommand;
use Cli\Helper\Questions\Domain;
use Cli\Controller\Generate\Main;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;

class Generate extends BaseCommand
{
    protected function configure()
    {
        $this->setName("controller:generate");
        $this->setDescription('Generates controller classes based on schema.xml');
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, Domain::getInfo(true));
        $this->setHelp(<<<EOT
This script generates controller files, twig templates, module configuration files, filter functionality, code to handle
collections of data as well as functionality to generate forms and creates the required code needed to expose a model
to the outside world via a REST api. Under normal circumstances this script is executed as part of another command. 
It usually makes more sense to execute <info>package:migrate</info> which this is part of.

EOT
        );
    }
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->domainCollector($input, $output);
        $aSelectedDomains = $input->getArgument('domain');

        $helper = $this->getHelper('question');
        $oMain = new Main($input, $output, $helper, $aSelectedDomains === 'all' ? [] : $aSelectedDomains);
        $oMain->run();
        return Command::SUCCESS;
    }
}
