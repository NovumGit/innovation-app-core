<?php

namespace Cli\Service;

use Cli\BaseCommand;
use Hurah\Types\Type\Composer\Repository;
use Hurah\Types\Type\Composer\RepositoryList;
use Hurah\Types\Type\Composer\RepositoryType;
use Hurah\Types\Type\Path;
use Hurah\Types\Type\PluginType;
use Core\Utils;
use DirectoryIterator;
use Hi\Helpers\DirectoryStructure;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use  Generator\Composer\Utils as ComposerUtils;

class Anounce extends BaseCommand
{
    protected function configure()
    {
        $this->setName("service:anounce");
        /// $this->addArgument('system_id',  )
        $this->setDescription("Register your api in the repository so other api's can find your service");
        $sSystemName = function_exists('getSiteSettings') ? getSiteSettings()['system_name'] : 'Innovation App';
        $this->setHelp(<<<EOT
$sSystemName will add your API to the repository so it is available to other API's for querying.
EOT
        );
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

    }
}
