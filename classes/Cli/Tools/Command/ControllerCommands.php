<?php

namespace Cli\Tools\Command;

use Cli\Tools\AbstractCommand;
use Cli\Tools\AbstractDialog;
use Cli\Tools\Dialog\ControllerOverview;
use Cli\Tools\Dialog\ControllersFromSchema;
use ReflectionException;

class ControllerCommands extends AbstractCommand
{

    function getName(): string
    {
        return 'controller';
    }

    public function getShortInfoLine(): string
    {
        return 'Controller related commands';
    }

    /**
     * @throws ReflectionException
     */
    public function run()
    {
        AbstractDialog::$sCurrentScript = self::$sCurrentScript;

        if (empty($this->getArgument(3))) {
            $this->h1("These are your options");
        }
        if (empty($this->getArgument(2))) {
            echo "create - Creates a new controler" . PHP_EOL;
            echo "generate - Generate a new controler" . PHP_EOL;
            exit();
        } else {
            if (empty($this->getArgument(3))) {
                if ($this->getArgument(2) == 'create') {
                    echo "overview - Creates a new controler prepared to show a list of things" . PHP_EOL;
                    echo "generic_edit - Creates a new controler prepared to edit a record" . PHP_EOL;
                } else {
                    if ($this->getArgument(2) == 'generate') {
                        $this->option('from-propel-schema', 'Use the modified Propel schema');
                    }
                }
            }
        }

        if ($this->getArgument(2) == 'create' && $this->getArgument(3) == 'overview') {
            (new ControllerOverview($this))->start(3);
        } else {
            if ($this->getArgument(2) == 'create' && $this->getArgument(3) == 'generic_edit') {
            } else {
                if ($this->getArgument(2) == 'generate' && $this->getArgument(3) == 'from-propel-schema') {
                    (new ControllersFromSchema($this))->start(3);
                }
            }
        }
        /*
        if(!in_array($this->getArgument(2), ['create']))
        {
            echo "Only the option create is supported as the second argument, so \"crud create\" " . PHP_EOL;
        }

        if(!in_array($this->getArgument(3), ['manager', 'fields']))
        {
            echo "Only the options manager and fields are supported as the third argument, like \"crud create manager\" " . PHP_EOL;
        }

        if($this->getArgument(2) == 'create' && $this->getArgument(3) == 'manager')
        {

            (new \Cli\Tools\Dialog\CrudManager())->start();
        }
        else if($this->getArgument(2) == 'create' && $this->getArgument(3) == 'fields')
        {

            (new \Cli\Tools\Dialog\CrudFields())->start();
        }
        */
        echo PHP_EOL . PHP_EOL;
        exit();
    }
}
