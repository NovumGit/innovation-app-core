<?php

namespace Cli\Tools\Command;

use Cli\Tools\AbstractCommand;
use Cli\Tools\AbstractDialog;
use Cli\Tools\Dialog\ApiCreator;
use Cli\Tools\Dialog\NLXCreator;
use Cli\Tools\Dialog\TomlCreator;

class ApiCommands extends AbstractCommand
{

    function getName(): string
    {
        return 'api';
    }
    function getShortInfoLine(): string
    {
        return 'API related commands';
    }

    /**
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    function run()
    {
        AbstractDialog::$sCurrentScript = self::$sCurrentScript;


        if ($this->getArgument(2) == 'regenerate' && !in_array($this->getArgument(3), ['from-api-xml'])) {
            $this->option('from-api-xml', 'Use the api xml file, usually under database/{project folder}/api.xml');
            exit();
        } elseif ($this->getArgument(2) == 'regenerate') {
            (new ApiCreator($this))->start(2);
            echo PHP_EOL . PHP_EOL;
            exit();
        } elseif ($this->getArgument(2) == 'toml') {
            (new TomlCreator($this))->start(2);
            echo PHP_EOL . PHP_EOL;
            exit();
        } elseif ($this->getArgument(2) == 'nlx') {
            (new NLXCreator($this))->start(2);
            echo PHP_EOL . PHP_EOL;
            exit();
        } elseif ($this->getArgument(2) == 'info') {
            (new ApiCreator($this))->start(2);
            echo PHP_EOL . PHP_EOL;
            exit();
        }

        if (empty($this->getArgument(3))) {
            $this->h1("These are your options");
        }

        if (empty($this->getArgument(2))) {
            echo "regenerate - Generate or update an Api class + documentation based on the api.xml file in the build folder" . PHP_EOL;
            echo "toml - Generate toml file based on api.xml" . PHP_EOL;
            echo "nlx - Generate nlx gateway based on api.xml" . PHP_EOL;
            echo "info - Generate ApiInfo class based on api.xml" . PHP_EOL;

            exit();
        }
        /*
        if(!in_array($this->getArgument(2), ['create']))
        {
            echo "Only the option create is supported as the second argument, so \"crud create\" " . PHP_EOL;
        }

        if(!in_array($this->getArgument(3), ['manager', 'fields']))
        {
            echo "Only the options manager and fields are supported as the third argument, like " \"crud create manager\" "
            . PHP_EOL;
        }

        if($this->getArgument(2) == 'create' && $this->getArgument(3) == 'manager')
        {

            (new \Cli\Tools\Dialog\CrudManager())->start();
        }
        else if($this->getArgument(2) == 'create' && $this->getArgument(3) == 'fields')
        {

            (new \Cli\Tools\Dialog\CrudFields())->start();
        }
        */
        echo PHP_EOL . PHP_EOL;
        exit();
    }
}
