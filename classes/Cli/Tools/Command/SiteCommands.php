<?php

namespace Cli\Tools\Command;

use Cli\Tools\AbstractCommand;
use Cli\Tools\Dialog\Site;

class SiteCommands extends AbstractCommand
{

    public function getName(): string
    {
        return 'site';
    }

    public function getShortInfoLine(): string
    {
        return 'Create new site';
    }

    public function run()
    {
        if (!in_array($this->getArgument(2), ['create'])) {
            $this->h1("Your options are:");
            $this->option('create', 'Starts a dialog that allows you to create a site + development environment.');
            $this->newline();
        }
        if ($this->getArgument(2) == 'create') {
            (new Site($this))->start(2);
        }
    }
}
