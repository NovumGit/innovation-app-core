<?php

namespace Cli\Tools\Command;

use Cli\Tools\AbstractCommand;
use Cli\Tools\AbstractDialog;
use Throwable;
use Twig_Error_Loader;

class UiComponentCommands extends AbstractCommand
{

    public function getName(): string
    {
        return 'ui-components';
    }

    public function getShortInfoLine(): string
    {
        return 'Install UI component types';
    }

    /**
     * @throws Throwable
     * @throws Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    function run()
    {
        AbstractDialog::$sCurrentScript = self::$sCurrentScript;

        if (empty($this->getArgument(2))) {
            echo "reload - Loops over the component directory and creates the database schema's" . PHP_EOL;
            echo "lookup install - Loops over the component directory and creates the database schema's" . PHP_EOL;

            exit();
        } else {
            if ($this->getArgument(2) == 'reload') {
                (new \Cli\Tools\Dialog\CrudUiComponentInstaller($this))->start(2);
                exit();
            } else {
                if ($this->getArgument(2) == 'lookup' && $this->getArgument(3) == 'install') {
                    (new \Cli\Tools\Dialog\CrudUiComponentLookupInstaller($this))->start(3);
                    exit();
                }
            }
        }

        echo PHP_EOL . PHP_EOL;
        exit();
    }
}
