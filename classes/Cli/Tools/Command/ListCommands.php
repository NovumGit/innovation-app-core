<?php

namespace Cli\Tools\Command;

use Cli\Tools\AbstractCommand;
use Cli\Tools\CommandUtils;

class ListCommands extends AbstractCommand
{
    public function getName(): string
    {
        return 'list-commands';
    }

    public function getShortInfoLine(): string
    {
        return 'Displays a list of available commands';
    }

    public function run()
    {
        echo PHP_EOL;
        echo "These are the commands that are available to you: " . PHP_EOL;
        echo "-------------------------------------------------" . PHP_EOL;

        foreach (CommandUtils::getCommands(self::$aArguments) as $oClass) {
            if ($oClass instanceof AbstractCommand) {
                echo $oClass->getName() . ' - ' . $oClass->getShortInfoLine() . PHP_EOL;
            }
        }
        echo PHP_EOL . PHP_EOL;
    }
}
