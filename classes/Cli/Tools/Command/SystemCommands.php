<?php

namespace Cli\Tools\Command;

use Cli\Tools\AbstractCommand;
use Cli\Tools\AbstractDialog;
use Cli\Tools\Dialog\SystemCreator;
use Throwable;
use Twig_Error_Loader;
use Twig_Error_Syntax;

class SystemCommands extends AbstractCommand
{

    public function getName(): string
    {
        return 'system';
    }

    public function getShortInfoLine(): string
    {
        return 'Manage systems';
    }

    /**
     * @throws Throwable
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Syntax
     */
    public function run()
    {
        AbstractDialog::$sCurrentScript = self::$sCurrentScript;

        if (empty($this->getArgument(2))) {
            echo "create - Create or initialize a new system" . PHP_EOL;
            exit();
        } else {
            if ($this->getArgument(2) == 'create') {
                (new SystemCreator($this))->start(2);
                exit();
            }
        }

        echo PHP_EOL . PHP_EOL;
        exit();
    }
}
