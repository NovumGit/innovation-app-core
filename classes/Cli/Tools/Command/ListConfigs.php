<?php

namespace Cli\Tools\Command;

use Cli\Tools\AbstractCommand;

class ListConfigs extends AbstractCommand
{
    public function getName(): string
    {
        return 'list-configs';
    }

    public function getShortInfoLine(): string
    {
        return 'Lists all available configuration folders';
    }

    public function run()
    {
        print_r($_SERVER);
    }
}
