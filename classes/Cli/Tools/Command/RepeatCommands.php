<?php

namespace Cli\Tools\Command;

use Cli\Tools\AbstractCommand;
use Cli\Tools\Runner;
use Exception;

class RepeatCommands extends AbstractCommand
{

    public function getName(): string
    {
        return 'repeat';
    }

    public function getShortInfoLine(): string
    {
        return 'Repeat previously entered commands by providing their filename';
    }

    /**
     * @throws Exception
     */
    public function run()
    {
        if (!in_array($this->getArgument(2), ['from-file'])) {
            $this->h1("These are your options");
            $this->option('from-file', 'Specify an absolute path to the file to be repeated');
            exit();
        }

        if ($this->getArgument(2) == 'from-file' && empty($this->getArgument(3))) {
            $this->h1("These are your options");
            $this->option('<the-actual-filename>', 'Fill in the actual filename that contains the answers');
        }
        if ($this->getArgument(2) == 'from-file' && !empty($this->getArgument(3)) && !preg_match('/\.json$/', $this->getArgument(3))) {
            $this->h1("JSON file error");
            $this->option('ERROR:', 'The file is supposed to have the .json extention. You are probably feeding this script the xml file.');
        } else {
            if ($this->getArgument(2) == 'from-file' && !empty($this->getArgument(3))) {
                $sConfigFile = $this->getArgument(3);
                $sConfigJson = file_get_contents($sConfigFile);

                $aConfig = json_decode($sConfigJson, true);

                $oRunner = new Runner();
                $oRunner->setPrefilledAnswers($aConfig['answers']);
                $oRunner->run($aConfig['original_arguments']);
            }
        }

        echo "All done" . PHP_EOL;
        exit();
    }
}
