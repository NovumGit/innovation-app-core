<?php

namespace Cli\Tools\Command;

use Cli\Tools\AbstractCommand;
use Cli\Tools\AbstractDialog;
use Cli\Tools\Dialog\NodeRedGenerate;
use Throwable;
use Twig_Error_Loader;
use Twig_Error_Syntax;

class NodeRedCommands extends AbstractCommand
{

    public function getName(): string
    {
        return 'node-red';
    }

    public function getShortInfoLine(): string
    {
        return 'Node red related commands';
    }

    /**
     * @throws Throwable
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Syntax
     */
    public function run()
    {
        AbstractDialog::$sCurrentScript = self::$sCurrentScript;

        (new NodeRedGenerate($this))->start(0);

        if ($this->getArgument(2) == 'generate') {
            echo PHP_EOL . PHP_EOL;
            exit();
        }

        if (empty($this->getArgument(2))) {
            echo "generate - Generate node red nodes" . PHP_EOL;
            exit();
        }
        /*
        if(!in_array($this->getArgument(2), ['create']))
        {
            echo "Only the option create is supported as the second argument, so \"crud create\" " . PHP_EOL;
        }

        if(!in_array($this->getArgument(3), ['manager', 'fields']))
        {
            echo "Only the options manager and fields are supported as the third argument, like \"crud create manager\" " . PHP_EOL;
        }

        if($this->getArgument(2) == 'create' && $this->getArgument(3) == 'manager')
        {

            (new \Cli\Tools\Dialog\CrudManager())->start();
        }
        else if($this->getArgument(2) == 'create' && $this->getArgument(3) == 'fields')
        {

            (new \Cli\Tools\Dialog\CrudFields())->start();
        }
        */
        echo PHP_EOL . PHP_EOL;
        exit();
    }
}
