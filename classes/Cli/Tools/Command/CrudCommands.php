<?php

namespace Cli\Tools\Command;

use Cli\Tools\AbstractCommand;
use Cli\Tools\Dialog\CrudFields;
use Cli\Tools\Dialog\CrudFromSchema;
use Cli\Tools\Dialog\CrudManager;
use ReflectionException;

class CrudCommands extends AbstractCommand
{

    public function getName(): string
    {
        return 'crud';
    }

    public function getShortInfoLine(): string
    {
        return 'Crud related commands';
    }

    /**
     * @throws ReflectionException
     */
    public function run()
    {
        if (
            !in_array($this->getArgument(2), [
            'create',
            'generate',
            ])
        ) {
            $this->h1("These are your options");
            $this->option('create', 'Creates new crud managers or crud fields based on your input');
            $this->option('generate', 'Automatically generates crud\'s based on a schema.xml');
            exit();
        } else {
            if (
                $this->getArgument(2) == 'create' && !in_array($this->getArgument(3), [
                    'manager',
                    'fields',
                ])
            ) {
                $this->h1("These are your options");
                $this->option('mananger', 'Create a new manager');
                $this->option('fields', 'Create crud fields');
                exit();
            } else {
                if ($this->getArgument(2) == 'generate' && !in_array($this->getArgument(3), ['from-propel-schema'])) {
                    $this->h1("These are your options");
                    $this->option('from-propel-schema', 'Use the modified Propel schema');
                    exit();
                } else {
                    if ($this->getArgument(2) == 'create' && $this->getArgument(3) == 'manager') {
                        (new CrudManager($this))->start(3);
                    } else {
                        if ($this->getArgument(2) == 'generate' && $this->getArgument(3) == 'from-propel-schema') {
                            (new CrudFromSchema($this))->start(3);
                        } else {
                            if ($this->getArgument(2) == 'create' && $this->getArgument(3) == 'fields') {
                                (new CrudFields($this))->start(3);
                            }
                        }
                    }
                }
            }
        }
        echo "All done" . PHP_EOL;
        exit();
    }
}
