<?php

namespace Cli\Tools;

use Exception\LogicException;

/**
 * Class AbstractCommand
 * @package Cli\Tools
 * @deprecated
 */
abstract class AbstractCommand
{
    protected static $aArguments = [];
    protected static $aPrefillAnswers = [];
    protected static $sCurrentScript = [];

    public function __construct($aArguments)
    {
        if (!isset($aArguments[0])) {
            throw new LogicException("Undefined index \$aArguments[0] in arguments: " . print_r($aArguments, true));
        }
        self::$sCurrentScript = $aArguments[0];
        self::$aArguments = $aArguments;
    }

    function setPrefilledAnswers(array $aPrefillAnswers)
    {
        self::$aPrefillAnswers = $aPrefillAnswers;
    }

    function getPrefilledAnswers()
    {
        return self::$aPrefillAnswers;
    }

    function getPrefilledAnswer($sKey, $i = null, $sSubKey = null): ?string
    {
        if ($i !== null && $sSubKey !== null) {
            if (!isset(self::$aPrefillAnswers[$sKey])) {
                self::$aPrefillAnswers[$sKey] = null;
            }
            if (!isset(self::$aPrefillAnswers[$sKey][$i])) {
                self::$aPrefillAnswers[$sKey][$i] = null;
            }
            return self::$aPrefillAnswers[$sKey][$i][$sSubKey];
        } else {
            if (isset(self::$aPrefillAnswers[$sKey])) {
                return self::$aPrefillAnswers[$sKey];
            }
        }
        return null;
    }

    function countArguments(): int
    {
        return count(self::$aArguments);
    }

    protected function h1($sMessage)
    {
        echo $sMessage . PHP_EOL;
        echo "-------------------------------------------------" . PHP_EOL;
    }

    protected function newline()
    {
        echo PHP_EOL;
    }

    protected function option($sKeyword, $sInfo)
    {
        echo "$sKeyword - $sInfo" . PHP_EOL;
    }

    function getArguments()
    {
        return self::$aArguments;
    }

    public function hasArguments()
    {
        return (count(self::$aArguments) > 0);
    }

    public function getArgument($iNum)
    {
        if (isset(self::$aArguments[$iNum])) {
            return self::$aArguments[$iNum];
        }
        return null;
    }

    abstract public function getName(): string;

    abstract public function getShortInfoLine(): string;

    abstract public function run();
}
