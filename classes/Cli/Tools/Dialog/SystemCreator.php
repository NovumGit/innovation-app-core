<?php

namespace Cli\Tools\Dialog;

use Generator\Generators\Admin\Module\Structure\Structure as AdminStructure;
use Generator\Generators\Build\BuildStructure;
use Generator\Generators\System\Databases;
use Generator\Generators\System\Hosts;
use Generator\Generators\System\RegisterNamespace;
use Generator\Generators\System\Vhosts;
use Cli\Tools\AbstractDialog;
use Generator\Vo\SystemBuildConfig;
use Generator\Api\Builder\Structure;
use Generator\Config\Builder;
use Symfony\Component\Console\Output\ConsoleOutput;


/**
 * Class SystemCreator
 * @package Cli\Tools\Dialog
 * @deprecated
 */
class SystemCreator extends AbstractDialog
{

    /**
     * @param int $iCommandPartCount
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    function start($iCommandPartCount = 0)
    {
        if (!isset($_ENV['SYSTEM_ROOT'])) {
            exit('To run this command the environment variable SYSTEM_ROOT must be set.');
        }

        $aAnswers = $this->questionsManager();
        $oSystemBuildVo = new SystemBuildConfig($aAnswers);

        echo "Create build structure" . PHP_EOL;
        (new BuildStructure())->create($oSystemBuildVo);

        echo "Create public site " . PHP_EOL;
        (new \Generator\Site\Builder\Structure())->create($oSystemBuildVo);

        if ($oSystemBuildVo->getHasApi()) {
            echo "Create API" . PHP_EOL;
            (new Structure())->create($oSystemBuildVo);
        }

        if ($oSystemBuildVo->getHasAdmin()) {
            echo "Create admin structure" . PHP_EOL;
            (new AdminStructure($oSystemBuildVo, new ConsoleOutput()))->create($oSystemBuildVo);

        }

        echo "Create configuration files" . PHP_EOL;
        (new Builder())->create($oSystemBuildVo);

        echo "Registering namespaces to the autoloader" . PHP_EOL;
        (new RegisterNamespace())->create($oSystemBuildVo);

        echo "Create databases" . PHP_EOL;
        (new Databases())->create($oSystemBuildVo);

        echo "Create vhosts" . PHP_EOL;
        (new Vhosts())->create($oSystemBuildVo);

        echo "Add hosts records" . PHP_EOL;
        (new Hosts())->create($oSystemBuildVo);

        exit("all done" . PHP_EOL);
    }

    public function askQuestions(): array
    {
        $aQuestions = [

            'title'           => 'Please give your system a title, this title may be publicly visble',
            'api_title'       => 'Please give your API a descriptive title, this will be a publicly visible title',
            'api_description' => 'Please write in 1 line what the API provides or will be providing. If you are not using any API you can leave this blank',
            'service_name'    => 'Please give a servicename, this is what comes after the @ in generated logins for this system. Example: "belastingdienst"',
            'organisation'    => 'Please give the name of your organisation',

            'technical_name'  => 'Please specify the name of the person or department that answers technical questions',
            'technical_email' => 'Please specify an email address for technical issues',
            'support_email'   => 'Please specify an email address for support questions',
            'support_name'    => 'Please specify the name of the person or department that answers support questions',
            'has_nlx'         => 'Should this system be published on the https://nlx.io/ directory? (yes / no)',

            'system_url' => 'Please specify the base url, for instance (https://belastingdienst.demo.novum.nu)',

            'config_folder' => "Please specify a config folder, this can be anything but should only contain chars and dots.\n Valid examples are (novum.bri, cockpit)\n You can point to an existing config folder",
            'build_folder'  => 'Please specify a build folder, usually the same as the config folder (novum.bri, cockpit)',
            'namespace'     => 'Each system has it\'s own namespace, should be a camel cased string, for instance (NovumBri, Cockpit)',

            'has_admin' => 'Does the system have an admin panel (yes / no)',
            'has_api'   => 'Does the system have an api (yes / no)',

            'create_db_test'       => "We only create databases if they do not exist yet\nShould we create a test database for you? (yes / no)",
            'create_db_production' => "We only create databases if they do not exist yet\nShould we create a production database for you? (yes / no)",
            'create_db_local'      => "We only create databases if they do not exist yet\nShould we create a local development database for you? (yes / no)",

            'create_hosts_local'      => "Shall we create a local hosts entry for you? (yes / no)",
            'create_vhost_local'      => 'Should we create a local vhost for you? (yes / no)',
            'create_vhost_test'       => 'Should we create a test vhost for you? (yes / no)',
            'create_vhost_production' => 'Should we create a production vhost for you? (yes / no)',

            'ssh_keys_installed' => 'Did you install ssh keys to both test and production server (yes / no)',

            'linux_user_production' => 'Please specify the linux user on the production environment',
            'linux_user_test'       => 'Please specify the linux user on the test / staging environment',

            'production_server' => 'Please specify the IP or hostname of the production server',
            'test_server'       => 'Please specify the IP or hostname of the test server',

            'production_mysql_root_user' => 'Please specify a mysql user for the production server with CREATE DATABASE privileges' . PHP_EOL . "username",
            'production_mysql_root_pass' => 'Please specify a mysql password for the production server with CREATE DATABASE privileges' . PHP_EOL . "password",

            'test_mysql_root_user' => 'Please specify a mysql user for the test server with CREATE DATABASE privileges' . PHP_EOL . "username",
            'test_mysql_root_pass' => 'Please specify a mysql password for the test server with CREATE DATABASE privileges' . PHP_EOL . "password",
        ];

        $aAnwsers = [];
        foreach ($aQuestions as $sVariable => $mQuestion) {
            $bAsked = false;
            if ('local_virtualhost_docroot' == $sVariable && $aAnwsers['create_local_virtualhost'] !== 'yes') {
                continue;
            } else {
                if ('local_virtualhost_docroot' == $sVariable) {
                    $mQuestion = $mQuestion . '(example /home/anton/Documents/sites/hurah/public_html/' . $aAnwsers['online_domain'] . '/public_html?)';
                } else {
                    if ($sVariable == 'route_loop' || $sVariable == 'alias_loop') {
                        $bAsked = true;
                        $i = 0;
                        while (true) {
                            echo "Route number " . ($i + 1) . PHP_EOL;
                            foreach ($mQuestion as $sSubVariable => $sSubQuestion) {
                                $sAnswer = $this->askQuestion($sSubQuestion, $sVariable, $i, $sSubVariable);
                                if (empty($sAnswer)) {
                                    break 2;
                                }
                                $aAnwsers[$sVariable][$i][$sSubVariable] = $sAnswer;
                            }
                            echo PHP_EOL;
                            $i++;
                        }
                    }
                }
            }

            if (!$bAsked) {
                $sAnswer = $this->askQuestion($mQuestion, $sVariable);

                $aAnwsers[$sVariable] = $sAnswer;
            }
        }

        return $aAnwsers;
    }
}
