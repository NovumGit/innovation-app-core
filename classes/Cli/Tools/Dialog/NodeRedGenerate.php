<?php

namespace Cli\Tools\Dialog;

use Generator\Generators\Nodered\GenerateDatasources;
use Cli\Tools\AbstractDialog;
use Cli\Tools\CommandUtils;

/**
 * Class NodeRedGenerate
 * @package Cli\Tools\Dialog
 * @deprecated
 */
class NodeRedGenerate extends AbstractDialog
{
    /**
     * @param int $iCommandPartCount
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    function start($iCommandPartCount = 0)
    {
        $aAnswers = $this->questionsManager(1);

        $sDataSourcesUrl = $aAnswers['api_url'] . '/v2/rest/datasource/';
        $sDataSourcesJson = file_get_contents($sDataSourcesUrl);
        $aDataSources = json_decode($sDataSourcesJson, true);

        $oGenerateDatasources = new GenerateDatasources();

        $oGenerateDatasources->create($aDataSources['results'], 'in');
        $oGenerateDatasources->create($aDataSources['results'], 'out');

        $oGenerateDatasources->restartSupervisor();

        print_r($aAnswers);

        exit();
    }

    function askQuestions(): array
    {


        $aDirs = glob(CommandUtils::getRoot() . '/public_html/api.*');
        $aConfigDirs = [];
        foreach ($aDirs as $sDir) {
            $aConfigDirs[] = basename($sDir);
        }

        $aQuestions = [
            'api_url' => 'Please give the url of the api: ' . PHP_EOL . join(',' . PHP_EOL, $aConfigDirs) . '?',
        ];


        $aAnwsers = [];
        foreach ($aQuestions as $sVariable => $sQuestion) {
            $sAnswer = $this->askQuestion($sQuestion, $sVariable);
            $aAnwsers[$sVariable] = $sAnswer;
        }
        return $aAnwsers;
    }
}
