<?php

namespace Cli\Tools\Dialog;

use Cli\Tools\AbstractDialog;
use Cli\Tools\CommandUtils;
use Generator\Vo\CrudFieldVo;
use Exception\LogicException;
use ReflectionClass;
use ReflectionException;

/**
 * Class CrudFields
 * @package Cli\Tools\Dialog
 * @deprecated
 */
class CrudFields extends AbstractDialog
{

    /**
     * @param int $iCommandPartCount
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    function start($iCommandPartCount = 0)
    {
        $aAnswers = $this->questionsManager();

        $oCrudFieldVo = new CrudFieldVo($aAnswers);

        echo "Ok lets go and create some fields, all fields that where already there are skipped! " . PHP_EOL;

        $this->createEmptyCrudFieldFiles($oCrudFieldVo, $aAnswers);

        $sFieldsDir = $this->getFieldsDir($oCrudFieldVo);

        if (empty($sFieldsDir)) {
            throw new LogicException("No fieldsdir defined.");
        }

        $aCrudFields = glob($sFieldsDir . '/*');
        $handle = fopen("php://stdin", "r");

        foreach ($aCrudFields as $sCrudField) {
            $oCrudFieldVo = new CrudFieldVo($aAnswers);

            $sBaseName = basename($sCrudField);
            echo "$sBaseName configureren" . PHP_EOL;

            $sClassName = str_replace('.php', '', $sBaseName);
            $sFieldName = $this->fromCamelCase($sClassName);

            if (!isset($aAnswers['create_edit_button']) && $sBaseName == 'Edit.php') {
                continue;
            }

            if (!isset($aAnswers['create_delete_button']) && $sBaseName == 'Delete.php') {
                continue;
            }

            if ($sBaseName == 'Id.php') {
                echo "Skipped $sBaseName" . PHP_EOL;
                continue;
            }

            if ($this->crudFieldAlreadyExists($sCrudField, $sBaseName)) {
                continue;
            }

            $oCrudFieldVo->setClassName($sClassName);

            if (
                !in_array($sBaseName, [
                'Delete.php',
                'Edit.php',
                ])
            ) {
                echo "Geef veld label: ";
                $sResponse = trim(fgets($handle));
                $oCrudFieldVo->setFieldlabel($sResponse);
            }

            $aFieldTypes = [
                'boolean'  => 'GenericBoolean',
                'date'     => 'GenericDate',
                'datetime' => 'GenericDateTime',
                'delete'   => 'GenericDelete',
                'edit'     => 'GenericEdit',
                'email'    => 'GenericEmail',
                'float'    => 'GenericFloat',
                'integer'  => 'GenericInteger',
                'lookup'   => 'GenericLookup',
                'string'   => 'GenericString',
                'textarea' => 'GenericTextarea',
                'valuta'   => 'GenericValutaEuro',
            ];

            $sResponse = null;
            if ($sBaseName == 'Delete.php') {
                $oCrudFieldVo->setDatatype('delete');
            } else {
                if ($sBaseName == 'Edit.php') {
                    $oCrudFieldVo->setDatatype('edit');
                }
            }

            while (!in_array($oCrudFieldVo->getDatatype(), array_keys($aFieldTypes))) {
                echo "Geef veld datatype (" . join(', ', array_keys($aFieldTypes)) . "): ";
                $sResponse = trim(fgets($handle));

                if (isset($aFieldTypes[$sResponse])) {
                    $oCrudFieldVo->setDatatype($sResponse);
                    $oCrudFieldVo->setGenericType($aFieldTypes[$sResponse]);
                } else {
                    echo $sResponse . " is not in our list, sorry" . PHP_EOL;
                }
            }

            if ($sResponse == 'lookup') {
                $oCrudFieldVo->setCrudinterface('InterfaceFilterableLookupField');
            } else {
                $oCrudFieldVo->setCrudinterface('IFilterableField');
            }

            if ($oCrudFieldVo->getDatatype() == 'delete') {
                $sTemplateHtml = file_get_contents($this->getTemplateDir() . '/deleteCrudField.twig');
                $sTemplate = CommandUtils::parse($sTemplateHtml, ['gen' => $oCrudFieldVo]);
            } else {
                if ($oCrudFieldVo->getDatatype() == 'edit') {
                    $sTemplateHtml = file_get_contents($this->getTemplateDir() . '/editCrudField.twig');
                    $sTemplate = CommandUtils::parse($sTemplateHtml, ['gen' => $oCrudFieldVo]);
                } else {
                    echo "Geef de database veldnaam: $sFieldName";
                    $sResponse = trim(fgets($handle));

                    if (empty($sResponse)) {
                        $sResponse = $sFieldName;
                    }
                    $oCrudFieldVo->setFieldname($sResponse);

                    $sGetter = str_replace('.php', '', 'get' . $sClassName);
                    echo "Geef veld getter, enter voor: $sGetter";
                    $sResponse = trim(fgets($handle));

                    if (!empty($sResponse)) {
                        $sGetter = $sResponse;
                    }
                    $oCrudFieldVo->setGetter($sGetter);

                    if ($oCrudFieldVo->getDatatype() != 'boolean') {
                        echo "Geef veld icon: ";
                        $sResponse = trim(fgets($handle));
                        $oCrudFieldVo->setFieldicon($sResponse);

                        echo "Geef veld placeholder: ";
                        $sResponse = trim(fgets($handle));
                        $oCrudFieldVo->setFieldplaceholder($sResponse);
                    }

                    $sTemplateFile = file_get_contents($this->getTemplateDir() . '/crudField.twig');

                    $sTemplate = CommandUtils::parse($sTemplateFile, ['gen' => $oCrudFieldVo]);
                }
            }

            file_put_contents($sCrudField, $sTemplate);
            echo "Crudveld weg geschreven naar " . basename($sCrudField) . PHP_EOL;
        }
    }

    public function askQuestions(): array
    {
        $aQuestions = [
            'model_object'         => 'Where do we find your Propel model? (example: Model\Cockpit\Foodbox\Recipe)',
            'crud_folder_path'     => 'Where should we store the fields (example: Crud/YourThing/Field)',
            'create_edit_button'   => 'Do you want an edit button? [yes|no]',
            'create_delete_button' => 'Do you want a delete button? [yes|no]',
            'edit_url'             => 'What is url path to the edit page (example: /product/edit)',
        ];

        $aAnwsers = [];
        foreach ($aQuestions as $sVariable => $sQuestion) {
            if ($sVariable == 'create_edit_button' || $sVariable == 'edit_url') {
                $oCrudFieldVo = new CrudFieldVo($aAnwsers);
                $sFieldsDir = $this->getFieldsDir($oCrudFieldVo);

                if (file_exists($sFieldsDir . '/Edit.php')) {
                    // Edit.php already exists, not asking if the user wants an edit field
                    continue;
                }
            } elseif ($sVariable == 'create_delete_button') {
                $oCrudFieldVo = new CrudFieldVo($aAnwsers);
                $sFieldsDir = $this->getFieldsDir($oCrudFieldVo);

                if (file_exists($sFieldsDir . '/Delete.php')) {
                    // Delete.php already exists, not asking if the user wants a delete field
                    continue;
                }
            }
            $sAnswer = $this->askQuestion($sQuestion, $sVariable);
            $aAnwsers[$sVariable] = $sAnswer;
        }
        return $aAnwsers;
    }

    private function crudFieldAlreadyExists($sCrudField, $sBaseName): bool
    {
        $sCurrentFileContents = file_get_contents($sCrudField);

        if (strpos($sCurrentFileContents, 'class ' . str_replace('.php', '', $sBaseName))) {
            echo "Er zit al een klassedefinitie in bestand $sBaseName, we slaan hem over." . PHP_EOL;
            return true;
        }
        return false;
    }

    private function getClassDir()
    {
        return dirname(dirname(dirname(dirname(__FILE__))));
    }

    private function getFieldsDir(CrudFieldVo $oCrudFieldVo)
    {
        $sFieldsDir = $this->getClassDir() . '/' . $oCrudFieldVo->getCrudFolderPath();

        echo "Class dir " . $this->getClassDir() . PHP_EOL;
        echo "Folder path " . $oCrudFieldVo->getCrudFolderPath() . PHP_EOL;

        if (!is_dir($sFieldsDir)) {
            echo "Make directory $sFieldsDir " . PHP_EOL;
            mkdir($sFieldsDir);
        }

        return $sFieldsDir;
    }

    /**
     * @param CrudFieldVo $oCrudFieldVo
     * @param array $aAnswers
     * @return void
     * @throws ReflectionException
     */
    private function createEmptyCrudFieldFiles(CrudFieldVo $oCrudFieldVo, array $aAnswers): void
    {
        $oReflector = new ReflectionClass($oCrudFieldVo->getModelObject());
        $aMethods = $oReflector->getMethods();
        print_r($aMethods);
        $sFieldsDir = $this->getFieldsDir($oCrudFieldVo);

        if (isset($aAnswers['create_edit_button']) && !file_exists($sFieldsDir . '/Edit.php')) {
            echo "Create field Edit.php" . PHP_EOL;
            touch($sFieldsDir . '/Edit.php');
        }

        if (isset($aAnswers['create_delete_button']) && !file_exists($sFieldsDir . '/Delete.php')) {
            echo "Create field Delete.php" . PHP_EOL;
            touch($sFieldsDir . '/Delete.php');
        }

        foreach ($aMethods as $oMethod) {
            $aValidMethodComments = [
                'Set the value of',
                'Sets the value of',
            ];

            $bInclude = false;

            foreach ($aValidMethodComments as $sComment) {
                if (strpos($oMethod->getDocComment(), $sComment)) {
                    $bInclude = true;
                }
            }

            if ($bInclude) {
                $aField = [];
                preg_match_all('/\[([a-zA-Z_]+)\]/', $oMethod->getDocComment(), $aField);

                if (!isset($aField[1][0]) || $aField[1][0] == 'id') {
                    continue;
                }

                $sCrudfieldName = $sFieldsDir . '/' . str_replace('set', '', $oMethod->getName()) . '.php';

                if (!file_exists($sCrudfieldName)) {
                    echo "Create field " . $sCrudfieldName . PHP_EOL;
                    touch($sCrudfieldName);
                }
                echo $aField[1][0] . ' - ' . $oMethod->getName() . PHP_EOL;
            }
        }
    }

    private function fromCamelCase($input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }
}
