<?php

namespace Cli\Tools\Dialog;

use Generator\Generators\Nlx\ServiceConfigToml;
use Cli\Tools\AbstractDialog;
use DOMDocument;
use Helper\ApiXsd\Schema\Api;
use Helper\Schema\Database;
use LogicException;
use Throwable;
use Twig_Error_Loader;
use Twig_Error_Syntax;

/**
 * Class TomlCreator
 * @package Cli\Tools\Dialog
 * @deprecated
 */
class TomlCreator extends AbstractDialog
{
    /**
     * @param int $iCommandPartCount
     * @throws Throwable
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Syntax
     */
    public function start($iCommandPartCount = 0)
    {
        $aAnswers = $this->questionsManager($iCommandPartCount);

        $oDom = new DOMDocument();
        $sXml = file_get_contents($aAnswers['schema_location']);
        $oDom->loadXML($sXml);

        $oApi = simplexml_load_file($aAnswers['schema_location'], Api::class, LIBXML_NOCDATA);
        $oDatabase = simplexml_load_file(dirname($aAnswers['schema_location']) . '/schema.xml', Database::class, LIBXML_NOCDATA);

        if (!$oApi instanceof Api) {
            throw new LogicException("Could not parse api.xml");
        }
        if (!$oDatabase instanceof Database) {
            throw new LogicException("Could not parse database.xml");
        }

        (new ServiceConfigToml())->create($oApi, $oDatabase);
    }

    public function askQuestions(): array
    {
        $aQuestions = [
            'schema_location' => 'Please give the full absolute path to the api.xml file',
        ];

        $aAnwsers = [];
        foreach ($aQuestions as $sVariable => $sQuestion) {
            $sAnswer = $this->askQuestion($sQuestion, $sVariable);
            $aAnwsers[$sVariable] = $sAnswer;
        }
        return $aAnwsers;
    }
}
