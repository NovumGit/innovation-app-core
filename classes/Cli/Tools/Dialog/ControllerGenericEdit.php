<?php

namespace Cli\Tools\Dialog;

use Cli\Tools\AbstractDialog;
use Cli\Tools\CommandUtils;
use LogicException;
use Throwable;
use Twig_Error_Loader;
use Twig_Error_Syntax;

/**
 * Class ControllerGenericEdit
 * @package Cli\Tools\Dialog
 * @deprecated
 */
class ControllerGenericEdit extends AbstractDialog
{
    private function enrichAnswers(array $aAnswers, string $sSystemRoot): array
    {
        $aAnswers['controller_name'] = explode('.', $aAnswers['controller_file_name'])[0];
//PWD
        if ($aAnswers['destination_dir'] === 'PWD') {
            $aAnswers['abs_destination_dir'] = $_SERVER['PWD'];
            $aAnswers['destination_dir'] = '.';
        } else {
            if (strpos($aAnswers['destination_dir'], $sSystemRoot) === false) {
                $aAnswers['abs_destination_dir'] = $sSystemRoot . '/admin_modules/' . $aAnswers['destination_dir'];
            } else {
                $aAnswers['abs_destination_dir'] = $aAnswers['controller_name'];
                // the field destination_dir already has an absolute path so we make that path.
                $aAnswers['destination_dir'] = str_replace($sSystemRoot, './', $aAnswers['destination_dir']);
            }
        }

        $aAnswers['main_template_path'] = $aAnswers['abs_destination_dir'] . '/overview.twig';
        $aAnswers['top_nav_template_path'] = $aAnswers['abs_destination_dir'] . '/top_nav_overview.twig';
        $aAnswers['controller_file_path'] = $aAnswers['destination_dir'] . '/' . $aAnswers['controller_file_name'];
        return $aAnswers;
    }

    private function makeControllerCode(array $aAnswers, string $sSystemRoot): string
    {
        $sControllerTemplateFile = $sSystemRoot . '/classes/Cli/Tools/Template/controllerGenericOverview.twig';
        $sControllerTemplateHtml = file_get_contents($sControllerTemplateFile);
        $sControllerParsedTemplate = CommandUtils::parse($sControllerTemplateHtml, $aAnswers);
        return $sControllerParsedTemplate;
    }

    /**
     * @param int $iCommandPartCount
     * @throws Throwable
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Syntax
     */
    public function start($iCommandPartCount = 0)
    {
        /*
        $aAnswers = [
            'controller_file_name' => 'OverviewController.php',
            'destination_dir' => 'Custom/Greenhouse/Actuator',
            'crud_manager' => 'Crud\Custom\Greenhouse\Actuator\CrudActuatorManager',
            'module' => 'Actuator',
            'query_object' => 'Model\Greenhouse\ActuatorQuery',
            'overview_title' => 'Actuators',
            'copy_top_nav_template' => 'copy',
            'copy_main_template' => 'copy',
        ];
        */
        $aAnswers = [
            'controller_file_name'  => 'ListController.php',
            'destination_dir'       => 'PWD',
            'crud_manager'          => 'Crud/Rule/CrudRuleManager.php',
            'module'                => 'Rule',
            'query_object'          => 'Model\Rule\RuleQuery',
            'overview_title'        => 'Rules',
            'copy_top_nav_template' => 'copy',
            'copy_main_template'    => 'copy',

        ];

        $sSystemRoot = self::getSystemRoot();
        $aAnswers = $this->enrichAnswers($aAnswers, $sSystemRoot);

        if (!is_dir($aAnswers['destination_dir'])) {
            throw new LogicException('Destination dir does not exist ' . $aAnswers['destination_dir']);
        }

        $sControllerHtml = $this->makeControllerCode($aAnswers, $sSystemRoot);

        if (!file_exists($aAnswers['controller_file_path'])) {
            echo "Controller file created: " . $aAnswers['controller_file_path'] . PHP_EOL;
            file_put_contents($aAnswers['controller_file_path'], $sControllerHtml);
        } else {
            echo "Controller file already existed. " . $aAnswers['controller_file_path'] . PHP_EOL;
        }
        echo $aAnswers['main_template_path'] . PHP_EOL;
        if ($aAnswers['copy_main_template'] == 'copy' && !file_exists($aAnswers['main_template_path'])) {
            echo "Main template copied to: " . $aAnswers['main_template_path'] . PHP_EOL;
            copy($sSystemRoot . '/admin_modules/generic_overview.twig', $aAnswers['main_template_path']);
        }
        if ($aAnswers['copy_top_nav_template'] == 'copy' && !file_exists($aAnswers['top_nav_template_path'])) {
            echo "Nav template copied " . $aAnswers['top_nav_template_path'] . PHP_EOL;
            copy($sSystemRoot . '/admin_modules/top_nav_overview.twig', $aAnswers['top_nav_template_path']);
        }
    }

    public function askQuestions(): array
    {
        $sSampleDir = $_SERVER['PWD'];
        $sSampleDir = preg_replace('/.+admin_modules\//', '', $sSampleDir);

        $aQuestions = [
            'controller_file_name'  => 'How do you want to call your controller? (example: OverviewController.php)',
            'destination_dir'       => "(Where do you want the controller to be created? (example: $sSampleDir or PWD)",
            'crud_manager'          => 'What is the crud manager? (example: Crud/Custom/Greenhouse/Sensor/CrudSensorManager)',
            'module'                => 'What is the crud module name? (example: Sensor)',
            'query_object'          => 'What is the name of the Propel query object? (example: Model\Greenhouse\SensorQuery)',
            'overview_title'        => 'What is the overview title? (example: Sensors)',
            'copy_top_nav_template' => 'Copy the template for the top nav or use generic template? [copy|generic] ',
            'copy_main_template'    => 'Copy template for main nav or use generic templates? [copy|generic] ',
        ];

        $aAnwsers = [];
        foreach ($aQuestions as $sVariable => $sQuestion) {
            $sAnswer = $this->questionsManager($sQuestion);

            if ($sVariable == 'copy_templates') {
                while (
                    !in_array($sAnswer, [
                    'copy',
                    'generic',
                    ])
                ) {
                    echo "Sorry, only copy and generic are valid answers. " . PHP_EOL;
                    $sAnswer = $this->askQuestion($sQuestion);
                }
            }
            $aAnwsers[$sVariable] = $sAnswer;
        }
        return $aAnwsers;
    }
}
