<?php

namespace Cli\Tools\Dialog;

use Cli\Tools\AbstractDialog;
use Cli\Tools\CommandUtils;
use Throwable;

/**
 * Class CrudManager
 * @package Cli\Tools\Dialog
 * @deprecated
 */
class CrudManager extends AbstractDialog
{


    /**
     * @param int $iCommandPartCount
     * @throws Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    function start($iCommandPartCount = 0)
    {
        $aAnswers = $this->questionsManager();

        $oCrudManager = new \Generator\Vo\CrudManager($aAnswers);

        $sCrudManagerTemplate = $this->getTemplateDir() . '/crudmanager.twig';
        $sManager  = CommandUtils::parse(file_get_contents($sCrudManagerTemplate), ['gen' => $oCrudManager]);
        file_put_contents('Crud' . $oCrudManager->getBareManagerName() . 'Manager.php', $sManager);
        echo "Crud manager created, now put him in the right place " . PHP_EOL;
    }

    function askQuestions(): array
    {
        $aQuestions = [
            'crud_fields_created' => 'Did you alreay create the Crud fields? [yes|no]',
            'crud_folder_path' => 'Where can we find them? (example: Crud/YourThing)',
            'model_object' => 'Please give the fully qualified classname of your Propel model (example: Model\Cockpit\Foodbox\Recipe)',
            'overview_title' => 'Please specify a title for your overview',
            'new_title' => 'Please specify a title for your create new button',
            'edit_title' => 'Please specify a title for your edit button',
            'overview_url' => 'Please specify the path/url to the overview editor',
            'edit_url' => 'Please specify the path to the edit class',
            'expose_api' => 'Do you want to expose this Crud in the API? [yes|no]',
            'expose_api_sort_desc' => 'Please specify a short description for this endpoint/crud'
        ];

        $aAnwsers = [];
        foreach ($aQuestions as $sVariable => $sQuestion) {
            if ($sVariable == 'expose_api_sort_desc' && $aAnwsers['expose_api'] == 'no') {
                // Api is not exposed so no need to register a description.
                continue;
            }


            $sAnswer = $this->askQuestion($sQuestion, $sVariable);


            if ($sVariable == 'crud_fields_created' && $sAnswer != 'yes') {
                echo "First create the fields" . PHP_EOL;
                exit();
            }

            if ($sVariable == 'crud_folder_path' && is_dir($sAnswer)) {
                echo "The dir $sAnswer does not exist" . PHP_EOL;
                exit();
            }

            $aAnwsers[$sVariable] = $sAnswer;
        }

        return $aAnwsers;
    }
}
