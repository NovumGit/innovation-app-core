<?php

namespace Cli\Tools\Dialog;

use Generator\Ui\Component\Installer;
use Cli\Tools\AbstractDialog;
use Symfony\Component\Console\Output\ConsoleOutput;
use Throwable;
use Twig_Error_Loader;
use Twig_Error_Syntax;

/**
 * Class CrudUiComponentInstaller
 * @package Cli\Tools\Dialog
 * @deprecated
 */
class CrudUiComponentInstaller extends AbstractDialog
{
    /**
     * @param int $iCommandPartCount
     * @throws Throwable
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Syntax
     */
    public function start($iCommandPartCount = 0)
    {
        $aAnswers = $this->questionsManager($iCommandPartCount);
        Installer::create($aAnswers, new ConsoleOutput());

        exit('run');
    }

    public function askQuestions(): array
    {
        $aQuestions = [
            'config_dir' => 'Please give the name of the configdir',
        ];

        $aAnwsers = [];
        foreach ($aQuestions as $sVariable => $sQuestion) {
            $sAnswer = $this->askQuestion($sQuestion, $sVariable);
            $aAnwsers[$sVariable] = $sAnswer;
        }

        return $aAnwsers;
    }
}
