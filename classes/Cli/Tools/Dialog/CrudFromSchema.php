<?php

namespace Cli\Tools\Dialog;

use Generator\Generators\Crud\CrudsFromSchema;
use Cli\Tools\AbstractDialog;

/**
 * Class CrudFromSchema
 * @package Cli\Tools\Dialog
 * @deprecated
 */
class CrudFromSchema extends AbstractDialog
{

    /**
     * @param int $iCommandPartCount
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    function start($iCommandPartCount = 0)
    {
        $aAnswers = $this->questionsManager(3);

        $oCrudsFromSchema = new CrudsFromSchema($aAnswers['schema_location']);
        $oCrudsFromSchema->run();
    }

    function askQuestions(): array
    {
        $aQuestions = [
            'schema_location' => 'Please give the full absolute path to the schema.xml file'
        ];
        if (isset($_SERVER['argv'][4])) {
            $aAnswers['schema_location'] = $_SERVER['argv'][4];
        } else {
            $aAnswers = [];
            foreach ($aQuestions as $sVariable => $sQuestion) {
                $sAnswer = $this->askQuestion($sQuestion, $sVariable);
                $aAnswers[$sVariable] = $sAnswer;
            }
        }
        return $aAnswers;
    }
}
