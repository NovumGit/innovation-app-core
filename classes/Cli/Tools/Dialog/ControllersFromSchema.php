<?php

namespace Cli\Tools\Dialog;

use Cli\Tools\AbstractDialog;

use Generator\Generators\Admin\Module\Controller\FromSchema;

/**
 * Class ControllersFromSchema
 * @package Cli\Tools\Dialog
 * @deprecated
 */
class ControllersFromSchema extends AbstractDialog
{
    /**
     * @param int $iCommandPartCount
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    function start($iCommandPartCount = 0)
    {

        $aAnswers = $this->questionsManager(3);

        $oControllersFromSchema = new FromSchema($aAnswers['schema_location'], $aAnswers['schema_location_2'] ?? null);
        $oControllersFromSchema->run();
    }

    function askQuestions(): array
    {
        $aQuestions = [
            'schema_location' => 'Please give the full absolute path to the schema.xml file',
            'schema_location_2' => 'Do you have another schema?'

        ];
        if (isset($_SERVER['argv'][4])) {
            $aAnwsers['schema_location'] = $_SERVER['argv'][4];
            if (isset($_SERVER['argv'][5])) {
                $aAnwsers['schema_location_2'] = $_SERVER['argv'][5];
            }
        } else {
            $aAnwsers = [];
            foreach ($aQuestions as $sVariable => $sQuestion) {
                $sAnswer = $this->askQuestion($sQuestion, $sVariable);
                $aAnwsers[$sVariable] = $sAnswer;
            }
        }
        return $aAnwsers;
    }
}
