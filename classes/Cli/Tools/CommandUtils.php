<?php

namespace Cli\Tools;

use Core\InlineTemplate;
use Exception;
use Hi\Helpers\DirectoryStructure;
use ReflectionClass;
use ReflectionException;
use Throwable;
use Twig_Error_Loader;
use Twig_Error_Syntax;

/**
 * Class CommandUtils
 * @package Cli\Tools
 * @deprecated
 */
class CommandUtils
{
    public static function getRoot(): string
    {
        if (isset($_ENV['SYSTEM_ROOT'])) {
            $oDirectoryStructure = new DirectoryStructure();
            return $oDirectoryStructure->getSystemDir(true);
        }
        $sRoot = str_replace('/build/tools/hurah', '', $_SERVER['SCRIPT_NAME']);
        $sRoot = str_replace('vendor/phpunit/phpunit/phpunit', '', $sRoot);
        return $sRoot;
    }

    /**
     * @param string $sTemplate
     * @param array $aArguments
     * @return false|string
     * @throws Throwable
     * @throws Twig_Error_Loader
     * @throws Twig_Error_Syntax
     */
    public static function parse(string $sTemplate, array $aArguments)
    {
        return InlineTemplate::parse($sTemplate, $aArguments);
    }

    /**
     * @param string $sName
     * @param array $aArgv - commandline arguments
     * @return AbstractCommand
     * @throws Exception
     */
    public static function getByName(string $sName, array $aArgv = null): AbstractCommand
    {
        $aCommands = self::getCommands($aArgv, function (AbstractCommand $oClass, array $argv) {
            if ($argv['name'] == $oClass->getName()) {
                return true;
            }
            return false;
        }, ['name' => $sName]);


        if (isset($aCommands[0]) && $aCommands[0] instanceof AbstractCommand) {
            return $aCommands[0];
        }
        throw new Exception("Command $sName not found.");
    }


    /**
     * @param array|null $argv
     * @param callable|null $filter
     * @param array|null $filterArgv - passed back as 2nd argument to filter
     * @return AbstractCommand[]
     * @throws ReflectionException
     */
    public static function getCommands(array $argv = null, callable $filter = null, array $filterArgv = null): array
    {
        $oReflector = new ReflectionClass(self::class);
        $sCommandDir = dirname($oReflector->getFileName()) . DIRECTORY_SEPARATOR . 'Command';

        $aCommandsFilenames = glob($sCommandDir . DIRECTORY_SEPARATOR . '*');

        $aCommands = [];
        foreach ($aCommandsFilenames as $sCommandFile) {
            $sClassWPhp = str_replace($sCommandDir . DIRECTORY_SEPARATOR, '', $sCommandFile);
            $sClassName = str_replace('.php', '', $sClassWPhp);
            $sFqClass = '\\Cli\\Tools\\Command\\' . $sClassName;
            $oAbstractCommand = new $sFqClass($argv);

            if ($filter && $filter($oAbstractCommand, $filterArgv)) {
                // echo 'new ' . $sFqClass . '(' . join(', ', $argv) .')' . PHP_EOL;
                $aCommands[] = $oAbstractCommand;
            } elseif ($filter == null) {
                $aCommands[] = $oAbstractCommand;
            }
        }
        return $aCommands;
    }
}
