<?php

namespace Cli\Tools;

use Cli\Tools\Command\ListCommands;
use Exception;

/**
 * Class Runner
 * @package Cli\Tools
 * @deprecated
 */

class Runner
{
    private $sDefaultCommand = ListCommands::class;
    private $aPrefilledAnswers = [];

    public function setPrefilledAnswers($aPrefilledAnswers)
    {
        $this->aPrefilledAnswers = $aPrefilledAnswers;
    }

    /**
     * @param $aArguments
     * @throws Exception
     */
    public function run($aArguments)
    {
        $sCommand = null;

        // 1st argument is the script it self
        if (count($aArguments) == 1) {
            $oCommand = new $this->sDefaultCommand($aArguments);
        } else {
            if (isset($aArguments[1])) {
                $oCommand = CommandUtils::getByName($aArguments[1], $aArguments);
            } else {
                echo 'Sorry, no command found with those arguments' . PHP_EOL;
                exit();
            }
        }

        if ($oCommand instanceof AbstractCommand) {
            // echo get_class($oCommand) . '->setPrefilledAnswers' . '(' . join(', ', $this->aPrefilledAnswers) . ')'  . PHP_EOL;
            $oCommand->setPrefilledAnswers($this->aPrefilledAnswers);
            $oCommand->run();
        }
    }
}
