<?php

namespace Cli\Tools;

use Core\Config;

/**
 * Class AbstractDialog
 * @package Cli\Tools
 * @deprecated
 */
abstract class AbstractDialog
{
    public static $sCurrentScript;
    public $oCommand;
    private $aInlineAnswers = [];
    public $aLoopsAlreadyAnswered = [];

    /**
     * Je kunt na het commando direct vragen gaan beantwoorden op dezelfde regel. Daarvoor moet het systeem alleen wel
     * weten vanaf welke positie op de commandline het antwoorden van vragen start. Dit is om oneliners te supporten.
     *
     * @param int $iCommandPartCount
     *
     * @return mixed
     */
    abstract function start($iCommandPartCount = 0);

    abstract function askQuestions(): array;

    function getCommand()
    {
        return $this->oCommand;
    }

    function __construct(AbstractCommand $oCommand)
    {
        $this->oCommand = $oCommand;
    }

    static function getSystemRoot()
    {
        return dirname(dirname(dirname(self::$sCurrentScript)));
    }

    public function askQuestion($sQuestion, $sKey = null, $i = null, $sSubKey = null): ?string
    {
        if ($i !== null && $sSubKey !== null) {
            $sPreFilledResponse = $this->getCommand()->getPrefilledAnswer($sKey, $i, $sSubKey);
            if ($sPreFilledResponse) {
                // This breaks the question loop if questions in the loop where already answered.
                $this->aLoopsAlreadyAnswered[$sKey] = true;
            }
        } else {
            if ($i !== null) {
                $sPreFilledResponse = $this->getCommand()->getPrefilledAnswer($sKey);
            } else {
                if (is_array($this->aInlineAnswers) && !empty($this->aInlineAnswers)) {
                    $sPreFilledResponse = array_pop($this->aInlineAnswers);
                } else {
                    if ($sKey !== null) {
                        $sPreFilledResponse = $this->getCommand()->getPrefilledAnswer($sKey);
                    }
                }
            }
        }

        if (!empty($sPreFilledResponse)) {
            echo $sQuestion . ': ' . $sPreFilledResponse . PHP_EOL;
            return $sPreFilledResponse;
        } else {
            if (empty($sPreFilledResponse) && isset($this->aLoopsAlreadyAnswered[$sKey])) {
                return null;
            }
        }

        $handle = fopen("php://stdin", "r");
        echo $sQuestion . ': ';
        $sResponse = trim(fgets($handle));
        return $sResponse;
    }

    protected function questionsManager($iUnsetArgumentsAfter = null)
    {
        $aArgumentsWoPath = $_SERVER['argv'];

        if ($iUnsetArgumentsAfter) {
            $iSliceFrom = $iUnsetArgumentsAfter + 1;

            $this->aInlineAnswers = array_slice($_SERVER['argv'], $iSliceFrom, count($_SERVER['argv']));

            $aArgumentsWoPath = array_slice($aArgumentsWoPath, 0, $iSliceFrom);
        }

        unset($aArgumentsWoPath[0]);
        $aAnswers = $this->askQuestions();

        $aSave['answers'] = $aAnswers;
        $aSave['original_arguments'] = $_SERVER['argv'];
        $sParts = join('-', $aArgumentsWoPath);

        if ($aArgumentsWoPath[1] == 'repeat') {
            // Original answers overchrijven
            $sOriginalFile = $aArgumentsWoPath[3];
            $sOriginalData = file_get_contents($sOriginalFile);
            $aOriginalRequest = json_decode($sOriginalData, true);

            $aOriginalAnswersMinScript = $aOriginalRequest['original_arguments'];
            unset($aOriginalAnswersMinScript[0]);
            $sParts = join('-', $aOriginalAnswersMinScript);
        }
        $sCliToolsDir = Config::getDataDir(true) . '/cli-tools';
        if (!is_dir($sCliToolsDir)) {
            mkdir($sCliToolsDir, 0777, true);
        }

        $sFile = Config::getDataDir(true) . '/cli-tools/' . $sParts . '-' . time() . '.json';

        if ($_SERVER['argv'][1] != 'repeat') {
            if (!is_writable(dirname(dirname($sFile)))) {
                echo "File not writable!! $sFile" . PHP_EOL;
                exit();
            } else {
                if (!is_dir(dirname($sFile))) {
                    mkdir(dirname($sFile), 0777, true);
                }
            }

            echo "Your answers have been save to:" . PHP_EOL;
            echo "File: " . $sFile . PHP_EOL;
            echo "You need this file if you wish to repeat (hurah repeat from-file) this command. " . PHP_EOL;
            file_put_contents($sFile, json_encode($aSave));
        }
        return $aSave['answers'];
    }

    protected function getTemplateDir(): string
    {
        $sTemplateDir = CommandUtils::getRoot() . '/classes/Cli/Tools/Template';
        return $sTemplateDir;
    }
}
