<?php

namespace Cli\Component\Structure;

use Generator\Ui\Component\Installer;
use Cli\BaseCommand;
use Cli\Helper\Questions\Domain;
use Exception;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Structure extends BaseCommand
{
    protected function configure()
    {
        $this->setName("component:structure");
        $this->setDescription('Creates a virtual data model for each component based on it\'s component.xml file.');
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::REQUIRED, Domain::getInfo());
        $this->setHelp(<<<EOT
A set of generic ui components are available for you to create applications with. The architecture of these components
is inspired / based on React / React Native and a bit inspired on Mendix. Each component has state and property values
where state for instance can contain form data and properties say something about the context of a component, for 
instance on what dataset should a form be based. These properties are stored in the database and the structure of the
tables that store information on the context of a component is dynamically generated based on an XML / XSD file that is 
distributed with the component it self. This command parses these XML files and creates a representation of these files
inside the database. Of course the next step is to "materialize" the representation of the table structure so actual
tables get generated. These tables can then be used to store context information on instances of each component. This 
job is done by the <info>db:migrate</info> command. 

Each property of a component may have constraints on what are valid options for a specific property. These constraints
are also stored in the database in separate tables linked via foreign key constraints. The actual data that fills 
these lookup tables can be populated with the <info>component:data</info> command.
  
EOT
        );
    }
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $aSystemIds = $input->getArgument('domain');
        foreach ($aSystemIds as $sSystemId) {
            $aAnswers = [
                'config_dir' => $sSystemId
            ];

            try {
                Installer::create($aAnswers, $output);
            } catch (Exception $e) {
                $output->writeln("<error>Could not connect to the database</error>");
                $output->writeln("<error>Did you initialize a database with db:init or db::make?</error>");
                $output->writeln($e->getMessage());
            }
        }

        return Command::SUCCESS;
    }
}
