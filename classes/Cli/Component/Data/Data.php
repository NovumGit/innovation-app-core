<?php

namespace Cli\Component\Data;

use Generator\Ui\Component\LookupInstaller;
use Cli\BaseCommand;
use Cli\Helper\Questions\Domain;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Data extends BaseCommand
{
    protected function configure()
    {
        $this->setName("component:data");
        $this->setDescription('Populates the data that is required to configure UI components.');
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::REQUIRED, Domain::getInfo(true));
        $this->setHelp(<<<EOT
A set of generic ui components are available for you to create applications with. The architecture of these components
is inspired / based on React / React Native and a bit inspired on Mendix. Each component has state and property values
where state for instance can contain form data and properties say something about the context of a component, for 
instance on what dataset should a form be based. These properties are stored in the database and the structure of the
tables that store information on the context of a component is dynamically generated based on an XML / XSD file that is 
distributed with  the component it self. Sometimes enumeration values are required or the result of an API call is what 
defines possible values for a specific property. This command will store all the possible values or the routines that 
are required to obtain possible values inside a table structure that is tailored for a specific component, making this 
whole structure strongly typed and hopefully robust.  

The table structure that will hold the output of this script first needs to be in place. By invoking the command 
<info>component:structure</info> all information that is required to populate the tables is collected and stored in
a virtal representation of the data model, located in the database. This virtual representation can be viewed by going
to the admin panel and then click on Modeller (module can be disabled) and then on Schema. 

After creating the virtual representation, the structure needs to be "materialized". This is done with the 
<info>db:migrate</info> command.    
  
 
EOT);
    }
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $aSystemIds = $input->getArgument('domain');
        foreach ($aSystemIds as $sSystemId) {
            $aAnswers = [
                'config_dir' => $sSystemId
            ];

            LookupInstaller::install($aAnswers, $output);
        }

        return Command::SUCCESS;
    }
}
