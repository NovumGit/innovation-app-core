<?php

namespace Cli\Form;

use Cli\BaseCommand;
use Cli\Database\Helper\Propel;
use Cli\Helper\Questions\Domain;
use Composer\IO\ConsoleIO;
use Crud\FormManager;
use Crud\Generic\Field\GenericBoolean;
use Crud\Generic\Field\GenericPassword;
use Crud\IEditableField;
use Crud\IFilterableLookupField;
use Crud\IFormManager;
use LogicException;
use Model\Setting\CrudManager\CrudEditor;
use Model\Setting\CrudManager\CrudEditorQuery;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class Manage extends BaseCommand
{

    protected function configure()
    {
        $this->setName("form:manage");
        $this->addArgument('domain', InputArgument::REQUIRED, 'The system id or system id\'s that you want to install the ui components for');
        $this->addArgument('action', InputArgument::REQUIRED, 'Action that you would like to perform, currently list and edit are your options');
        $this->addArgument('form', InputArgument::OPTIONAL, 'The id or unique identifier of the form');
        $this->setDescription("Find forms and insert data into the system");
        $this->setHelp(<<<EOT
Based on Cruds forms can be defined. This utility allows you to (interactively) insert data via these forms. This is or
can be usefull when importing data. The utility also allows you to view all the configured forms that are available in 
the system.
EOT
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->setIO(new ConsoleIO($input, $output, $this->getHelperSet()));

        $io = $this->getIO();

        (new Domain())->ask($io, $output, $input, $this);
        $domain = $input->getArgument('domain');

        $output->writeln("<comment>Working on the <info>$domain</info> database</comment>");
        Propel::includeConfigs($domain, $output);

        if (!in_array($input->getArgument('action'), ['list', 'insert'])) {
            $this->askAction($input, $output);
        }

        if ($input->getArgument('action') === 'insert') {
            $this->askForm($input, $output);
        }
        parent::initialize($input, $output);
    }

    protected function askAction(InputInterface $input, OutputInterface $output)
    {
        $aCommands = [
            1 => [1, 'list', 'Generates a list of available forms'],
            2 => [2, 'insert', 'Start an interactive session to create a new record in the database, must be followed by a form argument']
        ];
        $table = new Table($output);
        $table
            ->setHeaders(['Id', 'Command', 'Info'])
            ->setRows($aCommands);
        $table->render();

        $helper = $this->getHelper('question');

        $question = new Question("<question>Please choose an action: </question>");
        $action = $helper->ask($input, $output, $question);

        if (isset($aCommands[$action])) {
            $action = $aCommands[$action][1];
        }
        $input->setArgument('action', $action);
    }

    protected function askForm(InputInterface $input, OutputInterface $output)
    {

        $form = $input->getArgument('form');
        if (!$form) {
            $this->listForms($output);
        }
        $helper = $this->getHelper('question');
        $question = new Question("<question>Please choose a form to insert data into: </question>");
        $form = $helper->ask($input, $output, $question);

        while (true) {
            $oCrudEditor = null;
            if (is_numeric($form)) {
                $oCrudEditor = CrudEditorQuery::create()->findOneById($form);
            } elseif (is_string($form)) {
                $oCrudEditor = CrudEditorQuery::create()->findOneByName($form);
            }

            if ($oCrudEditor instanceof CrudEditor) {
                $input->setArgument('form', $oCrudEditor->getId());
                return true;
            }
            $question = new Question("<error>Form \"{$form}\" not found, please choose the id or \"Code\" of the form: </error>");
            $form = $helper->ask($input, $output, $question);
        }
    }

    private function listForms($output)
    {
        $oCrudEditorQuery = CrudEditorQuery::create();
        $aCrudEditors = $oCrudEditorQuery->find();

        $aTableData = [];
        foreach ($aCrudEditors as $oCrudEditor) {
            $oCrudConfig = $oCrudEditor->getCrudConfig();
            $sManagername = $oCrudConfig->getManagerName();
            $oManager = new $sManagername();
            if (!$oManager instanceof IFormManager) {
                throw new LogicException("$sManagername does not implement IFormManager");
            }

            $aTableData[] = [$oCrudEditor->getId(), $oCrudEditor->getName(), $oCrudEditor->getTitle(), $sManagername];
        }

        $oTable = new Table($output);
        $oTable
            ->setHeaders(['Id', 'Code', 'Formulier titel', 'Manager'])
            ->setRows($aTableData);
        $oTable->render();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $sDomain = $input->getArgument('domain');
        $sAction = $input->getArgument('action');
        $iForm = $input->getArgument('form');
        Propel::includeConfigs($sDomain, $output);

        if ($sAction === 'list') {
            $this->listForms($output);
        } elseif ($sAction === 'insert') {
            $this->startDialog($input, $output, $iForm);
        }


        return Command::SUCCESS;
        // <info>($aCrudEditors);
        /*
        $sVendor = $input->getOption('vendor');
        $sServiceName = $input->getOption('service_name');

        $oSystemId = SystemId::make(new Vendor($sVendor), new ServiceName($sServiceName));
        $output->writeln("<comment>Creating .env file for</comment> <info>{$oSystemId}</info>");

        $oEnvBuilder = new Builder();
        $oEnvBuilder->create(new EnvConfig($oSystemId));
        return Command::SUCCESS;
        */
    }

    protected function startDialog(InputInterface $input, OutputInterface $output, $iForm)
    {
        $oCrudEditor = CrudEditorQuery::create()->findOneById($iForm);
        $helper = $this->getHelper('question');

        foreach ($oCrudEditor->getCrudEditorBlocksSorted() as $oCrudEditorBlock) {
            $formatter = $this->getHelperSet()->get('formatter');
            $io = $this->getIO();
            $io->writeError(array(
                '',
                $formatter->formatBlock('Form section: ' . $oCrudEditorBlock->getTitle(), 'bg=blue;fg=white', true),
                '',
            ));

            $aFormData = [];
            foreach ($oCrudEditorBlock->getFieldsSorted() as $oBlockField) {
                while (true) {
                    $sField = $oBlockField->getField();
                    $oField = new $sField();

                    if ($oField instanceof IEditableField) {
                        $oField->setArguments(['crud_editor_block_field' => $oBlockField]);

                        if ($oField instanceof IFilterableLookupField) {
                            $output->writeln('<comment>Valid options for this <info>multiplechoice</info> question are:</comment>');
                            $table = new Table($output);
                            $table
                                ->setHeaders(['Id', 'Label'])
                                ->setRows($oField->getLookups());
                            $table->render();
                        }
                        if ($oField instanceof GenericBoolean) {
                            $question = new Question("<question>" . $oField->getFieldTitle() . "</question> [y/n]: ");
                        } else {
                            $question = new Question("<question>" . $oField->getFieldTitle() . "</question>: ");
                        }
                        if ($oField instanceof GenericPassword) {
                            $question->setHidden(true);
                            $question->setHiddenFallback(false);
                        }
                        $answer = $helper->ask($input, $output, $question);
                        if ($oField instanceof GenericBoolean && !in_array($answer, ['y', 'n'])) {
                            $output->writeln("<error>Answer must be y or n</error>");
                            continue;
                        }
                        if (method_exists($oField, 'hasValidations') && $oField->hasValidations()) {
                            $tmpFormData = array_merge($aFormData, [$oField->getFieldName() => $answer]);

                            if ($aErrors = $oField->validate($tmpFormData)) {
                                foreach ($aErrors as $sError) {
                                    $output->writeln("<error>$sError</error>");
                                }
                            } else {
                                $aFormData[$oField->getFieldName()] = $answer;
                                break;
                            }
                        } else {
                            $aFormData[$oField->getFieldName()] = $answer;
                            break;
                        }
                    }
                }
            }
            $sManagerName = $oCrudEditor->getCrudConfig()->getManagerName();
            $oManager = new $sManagerName();
            if ($oManager instanceof FormManager) {
                if ($oModel = $oManager->save($aFormData)) {
                    $output->writeln("<info>Record created {$oModel->getId()}</info>");
                    return Command::SUCCESS;
                }
                $output->writeln("<error>Could not save record, an error occured</error>");
            }
        }
        return Command::FAILURE;
    }
}
