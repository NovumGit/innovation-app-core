<?php

namespace Cli\Git\Helper;

use Composer\IO\BaseIO;
use Helper\Package\Finder;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Interactive
{

    public function start(InputInterface $input, OutputInterface $output, BaseIO $io): void
    {
        $oPackageFinder = new Finder();
        $aPackageInfo = $oPackageFinder->getAll();
        $i = 0;
        $aOptions = [];
        $aTableData = [];
        foreach ($aPackageInfo as $oPackageInfo) {
            ++$i;
            $aTableRow = [
                'Id' => $i,
                'Name' => $oPackageInfo->getComposerName(),
                'System' => $oPackageInfo->getSystemId(),
                'Type' => $oPackageInfo->getPackageType(),
                'Installed' => $this->boolValue($oPackageInfo->isInstalled()),
                'Git' => $this->boolValue($oPackageInfo->hasGit()),
                'git-remote' => '<comment>-</comment>',
            ];

            if (!$oPackageInfo->hasGit()) {
                $aOptions[$i] = $oPackageInfo;
            }
            if ($oPackageInfo->hasGit() && $config = $oPackageInfo->getGitConfig()) {
                $sRemoteOriginUrl = $config['remote.origin.url'] ?? 'local';
                $aTableRow['git-remote'] = $sRemoteOriginUrl;
            }

            $aTableData[] = $aTableRow;
        }

        $oTable = new Table($output);
        $oTable->setHeaders([
            'Id',
            'Name',
            'System / domain',
            'Type',
            'Installed',
            'Git',
            'Remote',
        ])->setRows($aTableData);
        $oTable->render();

        $aDisplayOptions = [];
        foreach ($aOptions as $sKey => $oInfo) {
            $aDisplayOptions[$sKey] = $oInfo->getComposerName();
        }

        $aAnswer = $io->select("<question>For which package would you like to initialize a git repository?</question> ", $aDisplayOptions, 1, 2, 'Invalid option');

        $aAuthorParts = $aOptions[$aAnswer]->getComposer()->getAuthor()->toArray();
        $input->setOption('your_email', $aAuthorParts['email'] ?? 'example@example.com');
        $input->setOption('your_name', $aAuthorParts['name'] ?? 'Anton Boutkam');

        $input->setOption('plugin_directory', $aOptions[$aAnswer]->getInstallDir());
    }

    private function boolValue($sValue): string
    {
        return $sValue ? "<comment>yes</comment>" : "<fg=red>no</>";
    }
}
