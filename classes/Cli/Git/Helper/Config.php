<?php

namespace Cli\Git\Helper;

use Composer\Util\ProcessExecutor;
use Symfony\Component\Process\ExecutableFinder;
use Symfony\Component\Process\Process;

class Config
{
    /** @var array */
    private $gitConfig;

    public function getGitConfig()
    {
        if (null !== $this->gitConfig) {
            return $this->gitConfig;
        }

        $finder = new ExecutableFinder();
        $gitBin = $finder->find('git');

        if (method_exists('Symfony\Component\Process\Process', 'fromShellCommandline')) {
            $cmd = new Process([
                $gitBin,
                'config',
                '-l',
            ]);
        } else {
            $cmd = new Process([sprintf('%s config -l', ProcessExecutor::escape($gitBin))]);
        }
        $cmd->run();

        if ($cmd->isSuccessful()) {
            $this->gitConfig = [];
            preg_match_all('{^([^=]+)=(.*)$}m', $cmd->getOutput(), $matches, PREG_SET_ORDER);
            foreach ($matches as $match) {
                $this->gitConfig[$match[1]] = $match[2];
            }

            return $this->gitConfig;
        }

        return $this->gitConfig = [];
    }
}
