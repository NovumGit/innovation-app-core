<?php

namespace Cli\Git;

use Cli\BaseCommand;
use Cli\Git\Helper\Interactive;
use Composer\IO\ConsoleIO;
use Helper\Git\Git;
use Hurah\Types\Type\GitUri;
use Hurah\Types\Type\Path;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Init extends BaseCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('git:init')->setDescription('Initialize a git repository for a system or component')->setDefinition([
            new InputOption('plugin_directory', null, InputOption::VALUE_REQUIRED, 'The location of the plugin'),
            new InputOption('git_remote_url', null, InputOption::VALUE_OPTIONAL, 'Remote repository url.'),
            new InputOption('your_name', null, InputOption::VALUE_OPTIONAL, 'Your name, required for Git configuration.'),
            new InputOption('your_email', null, InputOption::VALUE_OPTIONAL, 'Your email address, required for Git configuration.'),

        ])->setHelp(<<<EOT
The <info>git:init</info> initializes a git repository for a system. When a remote URL is specified the contents of the
local repository is pushed to that url. The local repository is then removed and the git repository is added to 
composer.json so whenever composer is updated this package is fetched from git.
- 
Read more at https://docs.demo.novum.nu/
EOT
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {


        chdir($input->getOption('plugin_directory'));
    }

    /**
     * {@inheritdoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->setIO(new ConsoleIO($input, $output, $this->getHelperSet()));


        $sPluginDirectory = $input->getOption('plugin_directory');

        if (empty($sPluginDirectory)) {
            $oInteractive = new Interactive();
            $oInteractive->start($input, $output, $this->getIO());
        }
        $sPluginDirectory = $input->getOption('plugin_directory');

        $output->writeln("<comment>Your git repository will be created here</comment>: <info>{$sPluginDirectory}</info>");
        $output->writeln('');
        $output->writeln(<<<EOT
It is a good idea to directly create a git repository for your code. If you have a git (Github, Gitlab, BitBucket etc) 
account, now is a good moment to create a new repository. Just create a blank repository as all the required / 
additional files are already created for you.
EOT
        );
        $output->writeln('');

        $sConfigureRemote = $this->getIO()->ask("<question>Would you like to configure a remote URL for this repository?</question> (<info>y</info>/<info>n</info>)[<comment>n</comment>]: ");


        if ($sConfigureRemote == 'y') {
            $sRemoteUrl = $this->getIO()->ask("<question>Git remote url</question>: ");
            $input->setOption('git_remote_url', $sRemoteUrl);
        }

        $oRemoteUrl = null;
        if (isset($sRemoteUrl)) {
            $oRemoteUrl = new GitUri($sRemoteUrl);
        }

        $sName = $input->getOption('your_name');
        $sEmail = $input->getOption('your_email');
        Git::init(new Path($sPluginDirectory), $oRemoteUrl, $sName, $sEmail, $output);

        exit();
    }
}
