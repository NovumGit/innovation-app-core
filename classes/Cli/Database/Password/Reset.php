<?php

namespace Cli\Database\Password;

use Generator\Generators\Build\BuildStructure;
use Cli\BaseCommand;
use Cli\Database\Helper\Login;
use Cli\Database\Helper\Propel;
use Cli\Helper\Questions\Domain;
use Generator\Vo\PropelConfigBuildVo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Reset extends BaseCommand
{
    protected function configure()
    {
        $this->setName("db:pass-reset");
        $this->setDescription('Generates a new database password for the provided domains and change all configurations accordingly.');
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::REQUIRED, Domain::getInfo(true));
        $this->setHelp(<<<EOT
Useful when installing the database on a new server or just whenever you wish to change the database password.  

EOT
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $helper = $this->getHelper('question');

        $aSystemIds = $input->getArgument('domain');
        foreach ($aSystemIds as $sSystemId) {
            $oLogin = new Login($input, $output, $helper);
            $output->writeln("<comment>Updating <info>$sSystemId</info> password</comment>");

            $oDomainLogin = $oLogin->getDomainLogin($sSystemId);

            $oLogin->getRootLogin()->updatePassword($oLogin->getDomainLogin($sSystemId));

            $oPropelBuildVo = new PropelConfigBuildVo($sSystemId, $oDomainLogin->getUser(), $oDomainLogin->getPass(), $oDomainLogin->getDbName(), $oDomainLogin->getHost(), $sSystemId);
            $oBuildStructure = new BuildStructure();
            $output->writeln("<comment>Updating <info>propel</info> build file</comment>");
            $oBuildStructure->makePropel($oPropelBuildVo, 'dev');

            $output->writeln("<comment>Updating <info>propel</info> config file</comment>");
            Propel::runCommand($sSystemId, 'config:convert', $output, $this);

            $output->writeln("<comment>Password of <info>$sSystemId</info> has been updated</comment>");
        }

        return Command::SUCCESS;
    }
}
