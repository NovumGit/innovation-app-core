# db:init
1. Checks if the user exists.
2. Creates the user if needed.
3. Checks if the database exists.
4. Creates a new database if needed.
5. Checks if the user has access to the database.
6. Grants access to the database if needed.
7. Creates a new [propel](http://propelorm.org/documentation/02-buildtime.html) build config file.  
8. Runs ```db:migrate``` to create an initial data model.
