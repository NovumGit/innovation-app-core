<?php

namespace Cli\Database\Initializer;

use Generator\Generators\Build\BuildStructure;
use Cli\BaseCommand;
use Cli\Database\Helper\Login;
use Cli\Database\Helper\Propel;
use Generator\Vo\PropelConfigBuildVo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Main extends Login
{

    private BaseCommand $command;

    public function __construct(InputInterface $input, OutputInterface $output, QuestionHelper $helper, array $aDomains = [], BaseCommand $command)
    {
        $this->input = $input;
        $this->output = $output;
        $this->helper = $helper;
        $this->aDomains = $aDomains;
        $this->command = $command;
    }

    public function run()
    {
        $oRootLogin = $this->getRootLogin();
        $this->output->writeln("Got working root login, now start initializing");
        foreach ($this->aDomains as $sDomain) {
            $this->output->writeln("Initializing $sDomain");
            $oDomainLogin = $this->getDomainLogin($sDomain);

            $oRootLogin->flushPrivileges();
            $this->output->writeln("Checking if user <info>{$oDomainLogin->getUser()}</info> exists");
            if (!$oDomainLogin->canConnect()) {
                $this->output->writeln("Creating user <info>{$oDomainLogin->getUser()}</info>");
                $oRootLogin->createUser($oDomainLogin);
                $this->output->writeln("User <info>{$oDomainLogin->getUser()}</info> created");
            } else {
                $this->output->writeln("User <info>{$oDomainLogin->getUser()}</info> exists");
                $oRootLogin->updatePassword($oDomainLogin);
            }

            $bDbInitialized = false;
            $this->output->writeln("Checking if database <info>{$oDomainLogin->getDbName()}</info> exists");
            if (!$oRootLogin->dbExists($oDomainLogin->getDbName())) {
                $this->output->writeln("Database <info>{$oDomainLogin->getDbName()}</info> does not exist, creating");
                $oRootLogin->dbCreate($oDomainLogin->getDbName());
                $bDbInitialized = true;
            } else {
                $this->output->writeln("Database <info>{$oDomainLogin->getDbName()}</info> already existed so skipping creation");
            }

            $this->output->writeln("Checking if user <info>{$oDomainLogin->getUser()}</info> has access to database <info>{$oDomainLogin->getDbName()}</info>");
            $oRootLogin->grantUserAccess($oDomainLogin, $oDomainLogin->getDbName());
            if (!$oDomainLogin->canSelectDb($oDomainLogin->getDbName())) {
                $this->output->writeln("User <info>{$oDomainLogin->getUser()}</info> has no acces yet, granting now");
                $oRootLogin->grantUserAccess($oDomainLogin, $oDomainLogin->getDbName());
            } else {
                $this->output->writeln("<info>{$oDomainLogin->getUser()}</info> has access to database <info>{$oDomainLogin->getDbName()}</info>");
            }

            $this->output->writeln("Creating new propel.php source config for: <info>{$oDomainLogin->getUser()}</info>");

            $oPropelBuildVo = new PropelConfigBuildVo($sDomain, $oDomainLogin->getUser(), $oDomainLogin->getPass(), $oDomainLogin->getDbName(), $oDomainLogin->getHost(), $sDomain);
            $oBuildStructure = new BuildStructure();
            $oBuildStructure->makePropel($oPropelBuildVo, 'dev');

            $this->output->writeln("Executing config:convert to create initial config file.");
            Propel::runCommand($sDomain, 'config:convert', $this->output, $this->command);

            /**
             * If this is a new database, we will now create an initial data model, this will contain the required
             * tables for other steps to do their job but not the tables created by the user via the ERD editor.
             */
            if ($bDbInitialized) {
                $this->output->writeln("Running <info>db:migrate</info> to bootstrap an initial data model.");

                $oCommand = $this->command->getApplication()->find($sCommand = 'db:migrate');

                $aInput = new ArrayInput([
                    'domain'   => [$sDomain],
                    '--source' => 'schema',
                ]);

                $returnCode = $oCommand->run($aInput, $this->output);
                if ($returnCode === Command::SUCCESS) {
                    $this->output->writeln("{$sCommand} executed <info>succesfully</info>");
                } else {
                    $this->output->writeln("{$sCommand} execution <warning>failed</warning>");
                }
            }
        }

        // echo $sEnvFile;
        /*
        foreach ($aDomains as $sDomain)
        {

        }
        */
    }
}
