<?php

namespace Cli\Database\Migrate\Migrate;

use Cli\BaseCommand;
use Cli\Database\Helper\Propel;
use Core\Cfg;
use Core\QueryMapper;
use Core\Utils;
use DirectoryIterator;
use Exception\InvalidArgumentException;
use Generator\Schema\Converter;
use Generator\Schema\Merger;
use Hi\Helpers\DirectoryStructure;
use Hurah\Types\Type\Path;
use Model\Module\ModuleQuery;
use Model\System\DataModel\Model\DataModelQuery;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Main
{
    private InputInterface $input;
    private OutputInterface $output;
    private QuestionHelper $helper;
    private array $aDomains;
    private BaseCommand $oCommand;

    public function __construct(InputInterface $input, OutputInterface $output, QuestionHelper $helper, array $aDomains, BaseCommand $oCommand)
    {
        $this->input = $input;
        $this->output = $output;
        $this->helper = $helper;
        $this->aDomains = $aDomains;
        $this->oCommand = $oCommand;
    }

    public function run()
    {

        foreach ($this->aDomains as $sDomain) {
            $sSource = $this->input->getOption('source');

            $this->output->writeln("<info>db:migrate</info> running with <info>source:$sSource</info>");

            // Defining paths to look for stuff
            $oDirectoryStructure = new DirectoryStructure();
            $sDatabaseDir = Utils::makePath($oDirectoryStructure->getSystemDir(true), 'build', 'database', $sDomain);
            $sMigrationsDir = Utils::makePath($sDatabaseDir, 'migrations');
            $sMergedSchemaDir = Utils::makePath($sDatabaseDir, 'generated-schema');

            $this->output->writeln("Clearing contents of {$sMergedSchemaDir}");
            // Utils::rmDir($sMergedSchemaDir, true, $this->output);

            // Generating the actual database configuration file
            $this->output->writeln("(re) generating database configuration files");
            Propel::runCommand($sDomain, 'config:convert', $this->output, $this->oCommand);

            // Include the actual propel config, so the one that was just created above.
            $this->output->writeln("Including configuration files and loading system settings");
            Propel::includeConfigs($sDomain, $this->output);

            /**
             * The schema files may contain relative paths so moving to the source directory of the schema file
             */
            $this->output->writeln("Changing current working directory into system build folder <info>$sDatabaseDir</info>");
            $sPrevDir = getcwd();
            chdir($sDatabaseDir);

            Utils::makeDir($sMigrationsDir);
            Utils::makeDir($sMergedSchemaDir);
            $this->removeDanglingMigrations($sMigrationsDir);

            $this->output->writeln("<error>$sSource</error>");
            if (
            in_array($sSource, [
                'both',
                'schema',
            ])
            ) {
                $oConverter = new Merger('schema.xml', null, $this->output);
                $oConverter->merge();

                if ($sSource === 'both') {
                    // Adding virtual data model to the existing data model stored in schema.xml
                    $sUserSchema = $this->makeUserSchema();
                    $sUserSchemaFile = Utils::makePath($sMergedSchemaDir, 'import', 'user-schema.xml');
                    $sRelativeUserSchemaFile = Utils::makePath('.', 'generated-schema', 'import', 'user-schema.xml');
                    $sUserSchemaFile->write($sUserSchema);
                    $this->addUserSchema($oConverter, $sUserSchema, $sUserSchemaFile, $sRelativeUserSchemaFile);
                }
            } else {
                if ($sSource === 'virtual') {
                    $this->output->writeln("Generating <info>schema.xml</info> file based on <info>virtual</info> table structure");
                    $sUserSchema = $this->makeUserSchema();

                    // Generating a schema file based on the virtual data model
                    $sSchemaFile = Utils::makePath($sMergedSchemaDir, "schema.xml");

                    // Storing the virtual schema as the main schema file to use for this migration
                    $sSchemaFile->write($sUserSchema);
                } else {
                    if ($sSource === 'url') {
                        // Pulling the main schema file from a remote URL
                        $sUrl = $this->input->getOption('url');

                        $this->output->writeln("Pulling <info>schema.xml</info> from url: <info>$sUrl</info>");
                        $sSchemaFile = Utils::makePath($sMergedSchemaDir, "schema.xml");

                        $sDownloadedSchema = file_get_contents($sUrl);
                        $sSchemaFile->write($sDownloadedSchema);
                    } else {
                        throw new InvalidArgumentException("Invalid value for source option");
                    }
                }
            }

            Propel::runCommand($sDomain, 'diff', $this->output, $this->oCommand);
            Propel::runCommand($sDomain, 'migrate', $this->output, $this->oCommand);
            Propel::runCommand($sDomain, 'model:build', $this->output, $this->oCommand);

            $this->output->writeln("Changing back to original directory <info>$sPrevDir</info>");
            chdir($sPrevDir);
        }
    }

    private function removeDanglingMigrations(string $sMigrationsDir): void
    {
        $this->output->writeln("<comment>Checking for dangling migration files</comment> <info>$sMigrationsDir</info>");
        $oDirectoryIterator = new DirectoryIterator($sMigrationsDir);

        foreach ($oDirectoryIterator as $oFile) {
            if ($oFile->isDot()) {
                continue;
            }
            if ($oFile->isDir()) {
                continue;
            }
            $this->output->writeln("Removing dangling migration file <info>{$oFile->getPathname()}</info>.");
            unlink($oFile->getPathname());
        }
    }

    private function makeUserSchema(): ?string
    {
        $this->output->writeln("Creating schema file from: <info>database</info>");
        $aRow = QueryMapper::fetchRow('SHOW TABLES LIKE "data_model";');
        if (empty($aRow)) {
            $this->output->writeln("<error>The data_model table is missing, cannot create schema.xml</error>");
            return null;
        }

        $oDataModelQuery = DataModelQuery::create();
        $sCustom = Cfg::get('CUSTOM_NAMESPACE');

        $oModuleQuery = ModuleQuery::create();

        $oConverter = new Converter($oModuleQuery, $oDataModelQuery, $sCustom);
        return $oConverter->asPropelXml();
    }

    public function addUserSchema($oConverter, string $sUserSchema, Path $sUserSchemaFile, $sRelativeUserSchemaFile)
    {
        $this->output->writeln("<comment>Writing: </comment><info>$sUserSchemaFile</info>");
        $sUserSchemaFile->write($sUserSchema);

        if (file_exists($sUserSchemaFile)) {
            $this->output->writeln("Adding: <info>user-schema.xml</info> as import to <info>schema.xml</info>");
            $oConverter->addImports([$sRelativeUserSchemaFile]);
        }
    }
}
