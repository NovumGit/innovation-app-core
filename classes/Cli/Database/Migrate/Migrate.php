<?php

namespace Cli\Database\Migrate;

use Cli\BaseCommand;
use Cli\Database\Migrate\Migrate\Main;
use Cli\Helper\Questions\Domain;
use Exception;
use Hi\Helpers\DirectoryStructure;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Migrate extends BaseCommand
{
    private array $aModes  = [
        'virtual', 'disk', 'both', 'url'
    ];
    protected function configure()
    {
        $this->setName("db:migrate");
        $this->setDescription('Creates or modifies the database table schema based on what is stored inside schema.xml or inside the virtual table structure located in the database itself');
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, Domain::getInfo(true));
        $this->addOption('source', 's', InputOption::VALUE_REQUIRED, 'Define what to base the scheme generation on. Can be one of ' . join(', ', $this->aModes)  . '.', 'both');
        $this->addOption('url', 'u', InputOption::VALUE_REQUIRED, 'Only applicable when <info>source</info> is set to <info>url</info>, defines the url to pull the schema from', 'both')
            ->setHelp('During the initialization of the database no virtual schema exists so the source should be schema or url. Once the database is initialized the source is usually virtual, meaning the definition comes from a virtual representation of the datamodel which is actually stored in the database it self.');

        $this->setHelp(<<<EOT
Inside the domain directory you will find a schema.xml file. This file defines the data structure / schema of your 
system. This will be used to generate all the tables inside the database and all the code that comes with it. If you 
wish to make modifications to the schema you can modify the schema.xml file and run db:migrate again. The system also 
comes with an ERD editor, the contents of the schema.xml file can be imported using the <info>db::schema-to-db</info> 
command. Once this is done running <info>db::migrate</info> without the <info>source</info> option will cause duplicate 
table definitions as the schema will be imported from both the virtual data model and from schema.xml so if you decide 
to import all the system tables, you should set <info>source=virtual</info> when migrating.
EOT
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $sSource = $input->getOption('source');
        $output->writeln('<error> ' . $sSource .  ' </error>');
        sleep(5);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $oDirectoryStructure = new DirectoryStructure();
        $aDomains = $oDirectoryStructure->getDomainCollection();

        $aSelectedDomains = $input->getArgument('domain');

        if (empty($aDomains)) {
            $this->noDomainsMessage($output);
            return Command::FAILURE;
        } elseif (empty($aSelectedDomains)) {
            $this->selectDomainMessage($input, $output, $aDomains);
        }
        $aSelectedDomains = $input->getArgument('domain');

        $helper = $this->getHelper('question');
        $oMain = new Main($input, $output, $helper, $aSelectedDomains === 'all' ? [] : $aSelectedDomains, $this);
        $oMain->run();
        return Command::SUCCESS;
    }
}
