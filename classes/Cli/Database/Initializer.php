<?php

namespace Cli\Database;

use Cli\BaseCommand;
use Cli\Helper\Questions\Domain;
use Exception;
use Hi\Helpers\DirectoryStructure;
use Cli\Database\Initializer\Main;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;

class Initializer extends BaseCommand
{
    protected function configure()
    {
        $this->setName("db:init");
        $this->setDescription('Creates a new database, user and initializes schema\'s based on your domains/[domain]/schema.xml');
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, Domain::getInfo());
        $this->setHelp(<<<EOT
<comment>db::init performs the following steps</comment>
1. Checks if the user exists.
2. Creates the user if needed.
3. Checks if the database exists.
4. Creates a new database if needed.
5. Checks if the user has access to the database.
6. Grants access to the database if needed.
7. Creates a new <a href="http://propelorm.org/documentation/02-buildtime.html">propel</a> build config file.  
8. Runs <info>db:migrate</info> to create an initial data model.
EOT
        );
    }
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $oDirectoryStructure = new DirectoryStructure();
        $aDomains = $oDirectoryStructure->getDomainCollection();

        $aSelectedDomains = $input->getArgument('domain');

        if (empty($aDomains)) {
            $this->noDomainsMessage($output);
            return Command::FAILURE;
        } elseif (empty($aSelectedDomains)) {
            try {
                $this->selectDomainMessage($input, $output, $aDomains);
            } catch (Exception $e) {
                $output->writeln("<error>{$e->getMessage()}</error>");
                return Command::FAILURE;
            }
        }
        $aSelectedDomains = $input->getArgument('domain');

        $helper = $this->getHelper('question');
        $oMain = new Main($input, $output, $helper, $aSelectedDomains === 'all' ? [] : $aSelectedDomains, $this);
        $oMain->run();
        return Command::SUCCESS;
    }
}
