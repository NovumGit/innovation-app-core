<?php

namespace Cli\Database\DataLoader;

use Hurah\Types\Type\Path;
use Core\Utils;
use Hi\Helpers\DirectoryStructure;

class Essentials extends AbstractLoader
{
    /**
     * @param DirectoryStructure $oDirectoryStructure
     * @param string $sDomain
     * @return Path[]
     */
    function getCrudDirs(DirectoryStructure $oDirectoryStructure, string $sDomain): array
    {
        return [new Path(Utils::makePath($oDirectoryStructure->databaseDir(), '_default', 'novum', '*.php'))];
    }

    function getInfoName(): string
    {
        return 'db:load-essentials';
    }

    function getInfoDescription(): string
    {
        return 'Loads core data that is considered required for each installation. Core data consists of stuff like data types, icons, languages, countries, genders etc';
    }

    function getHelp()
    {
        return <<<EOT
Inside .system/build/database/_default a series of scripts are stored that are there to populate the initial database.
The data that is installed from here is considered mandatory for the system to work. This script should normally be 
runned only during the installation and is invoked by <info>db::make</info>. You can run this script as often as you like
but each individual script is only runned once after installation. If you wish to restore the contents of the system
tables you could truncate the migration_scripts table which functions as the long term memory of this script. All the
scripts that are runned have some form of protection against duplicates so (most of the time) you will end up with a 
(more or less) identical dataset. 
EOT;
    }
}
