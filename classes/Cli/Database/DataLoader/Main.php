<?php

namespace Cli\Database\DataLoader;

use Cli\Database\Helper\Propel;
use Core\Utils;
use Generator\MigrationRunner;
use Hi\Helpers\DirectoryStructure;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Main
{
    private InputInterface $input;
    private OutputInterface $output;
    private QuestionHelper $helper;
    private array $aDomains;
    private DirectoryStructure $oDirectoryStructure;

    public function __construct(InputInterface $input, OutputInterface $output, QuestionHelper $helper, array $aDomains)
    {
        $this->input = $input;
        $this->output = $output;
        $this->helper = $helper;
        $this->aDomains = $aDomains;
        $this->oDirectoryStructure = new DirectoryStructure();
    }

    public function run()
    {
        if (!is_iterable($this->aDomains)) {
            $this->output->writeln("Received no domains to load data for");
        }

        foreach ($this->aDomains as $sDomain) {
            Propel::includeConfigs($sDomain, $this->output);

            $sDefaultMigrationDir = Utils::makePath($this->oDirectoryStructure->databaseDir(), '_default', 'novum', '*.php');
            $this->output->writeln("Running migration scripts from <info>$sDefaultMigrationDir</info>");
            MigrationRunner::runOnce($sDefaultMigrationDir, ['0_always_runs.php']);

            $sCustomMigrationDir = Utils::makePath($this->oDirectoryStructure->databaseDir(), $sDomain, 'crud_queries', '*.php');
            MigrationRunner::runOnce($sCustomMigrationDir, ['0_always_runs.php']);
        }
    }
}
