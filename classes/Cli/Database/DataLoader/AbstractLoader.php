<?php

namespace Cli\Database\DataLoader;

use Cli\BaseCommand;
use Cli\Database\Helper\Propel;
use Cli\Helper\Questions\Domain;
use Exception;
use Generator\MigrationRunner;
use Hi\Helpers\DirectoryStructure;
use Hurah\Types\Type\Path;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

abstract class AbstractLoader extends BaseCommand
{
    protected function configure()
    {
        $this->setName($this->getInfoName());
        $this->setDescription($this->getInfoDescription());
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::REQUIRED, Domain::getInfo(true));
        $this->addOption('all', 'a', InputOption::VALUE_NONE, 'Re-runs all the scripts, so also the ones that where executed before');
    }

    abstract public function getInfoName();

    abstract public function getInfoDescription();

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $aSelectedDomains = $input->getArgument('domain');

        if (empty($aSelectedDomains)) {
            if (empty($aAllDomains = $this->getAllDomains())) {
                $this->noDomainsMessage($output);
                return Command::FAILURE;
            } else {
                if (empty($aSelectedDomains)) {
                    try {
                        $this->selectDomainMessage($input, $output, $aAllDomains);
                    } catch (Exception $e) {
                        $output->writeln("<error>{$e->getMessage()}</error>");
                        return Command::FAILURE;
                    }
                }
            }
        }
        return Command::SUCCESS;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $oDirectoryStructure = new DirectoryStructure();
        $aSelectedDomains = $input->getArgument('domain');
        if (!is_iterable($aSelectedDomains)) {
            $output->writeln("Received no domains to load data for");
        }

        foreach ($aSelectedDomains as $sDomain) {
            Propel::includeConfigs($sDomain, $output);
            $aPaths = $this->getCrudDirs($oDirectoryStructure, $sDomain);

            foreach ($aPaths as $oPath) {
                if ($input->getOption('all')) {
                    $output->writeln("Re-running <info>all</info> migration scripts from <info>$oPath</info>");
                    MigrationRunner::runAll($oPath, ['0_always_runs.php']);
                } else {
                    $output->writeln("Running migration scripts from <info>$oPath</info>");
                    MigrationRunner::runOnce($oPath, ['0_always_runs.php']);
                }
            }
        }
        return Command::SUCCESS;
    }

    /**
     * @param DirectoryStructure $oDirectoryStructure
     * @param string $sDomain
     * @return Path[]
     */
    abstract public function getCrudDirs(DirectoryStructure $oDirectoryStructure, string $sDomain);
}
