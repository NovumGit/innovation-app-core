<?php

namespace Cli\Database\DataLoader;

use Hurah\Types\Type\Path;
use Core\Utils;
use Hi\Helpers\DirectoryStructure;

class Custom extends AbstractLoader
{
    /**
     * @param DirectoryStructure $oDirectoryStructure
     * @param string $sDomain
     * @return Path[]
     */
    public function getCrudDirs(DirectoryStructure $oDirectoryStructure, string $sDomain): array
    {
        return [Utils::makePath($oDirectoryStructure->databaseDir(), $sDomain, 'crud_queries', '*.php')];
    }

    public function getInfoName(): string
    {
        return 'db:load-custom';
    }

    public function getInfoDescription(): string
    {
        return 'Imports data that is specific to this system from the crud_queries folder.';
    }

    public function getHelp()
    {
        return <<<EOT
After installing or creating a new system a folder is added to your domains root directory. This folder contains some
sub folders, one of them is domains/[domain]/database/init. This directory should contain scripts that populate the data
that required for your specific system. This script will loop over each file in numerical order and attempts to run 
the code inside it once. Each script will get an entry in the migration_scripts table. Once a script is in there it 
is considered done and will nog be executed again.
EOT;
    }
}
