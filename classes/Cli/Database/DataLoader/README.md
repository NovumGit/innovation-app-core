# db:load
-- *Responsible for running any scripts that contain actual data required for the system to operate*
1. Loops over all php files inside ```.system/build/database/_default/novum/*``` and runs them.
2. Loops over all php files inside ```.system/build/database/<system.id>/crud_queries/*``` and runs them.
3. Each script that was executed is recorded inside the ``migration_scripts`` table. If the script is already there it won't be executed again.
