<?php

namespace Cli\Database\DataLoader;

use Cli\BaseCommand;
use Cli\Helper\Questions\Domain;
use Exception;
use Hi\Helpers\DirectoryStructure;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;

class Load extends BaseCommand
{
    protected function configure()
    {
        $this->setName("db:load");
        $this->setDescription('Inserts and updates the data inside your database based on what is found inside your <info>domains/[domain]/database/init</info> folder and inside the .system/build/database/_default folder.');
        $this->setHelp('After the database structure is created some data needs to be present. For instance icon sets or some default user roles such as "admin" or "user". This kind of data is inserted via the db:load command.');
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, Domain::getInfo(true));
    }
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $oDirectoryStructure = new DirectoryStructure();
        $aDomains = $oDirectoryStructure->getDomainCollection();

        $aSelectedDomains = $input->getArgument('domain');

        if (empty($aDomains)) {
            $this->noDomainsMessage($output);
            return Command::FAILURE;
        } elseif (empty($aSelectedDomains)) {
            try {
                $this->selectDomainMessage($input, $output, $aDomains);
            } catch (Exception $e) {
                $output->writeln("<error>{$e->getMessage()}</error>");
                return Command::FAILURE;
            }
        }
        $aSelectedDomains = $input->getArgument('domain');

        $helper = $this->getHelper('question');
        $oMain = new Main($input, $output, $helper, $aSelectedDomains === 'all' ? [] : $aSelectedDomains);
        $oMain->run();
        return Command::SUCCESS;
    }
}
