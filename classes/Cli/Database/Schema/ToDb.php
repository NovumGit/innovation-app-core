<?php

namespace Cli\Database\Schema;

use Cli\BaseCommand;
use Cli\Database\Helper\Propel;
use Generator\Schema\Importer\SchemaToDb;
use Generator\Vo\SchemaToDbVo;
use Hurah\Types\Type\SystemId;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ToDb extends BaseCommand
{
    protected function configure()
    {
        $this->setName("db:schema-to-db");
        $this->setDescription('Imports schema.xml files into the database');
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'The system id or system id\'s that you want to install the ui components for');
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $aSystemIds = $input->getArgument('domain');
        foreach ($aSystemIds as $sSystemId) {
            Propel::includeConfigs($sSystemId, $output);

            $oSchemaToDbVo = new SchemaToDbVo(new SystemId($sSystemId));

            $oSchemaToDb = new SchemaToDb();
            $oSchemaToDb->run($oSchemaToDbVo, $output);
        }

        return Command::SUCCESS;
    }
}
