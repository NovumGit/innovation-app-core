<?php

namespace Cli\Database\Schema;

use Cli\Helper\Questions\Domain;
use Generator\Schema\Importer\SchemaToDb;
use Cli\BaseCommand;
use Cli\Database\Helper\Propel;
use Generator\Vo\SchemaToDbVo;
use Hurah\Types\Type\SystemId;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Materialize extends BaseCommand
{
    protected function configure()
    {
        $this->setName("db:materialize");
        $this->setDescription('Exports the virtal table structure to a schema.xml file that you can use when distributing your system');
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::REQUIRED, Domain::getInfo());
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $aSystemIds = $input->getArgument('domain');
        foreach ($aSystemIds as $sSystemId) {
            Propel::includeConfigs($sSystemId, $output);

            $oSchemaToDbVo = new SchemaToDbVo(new SystemId($sSystemId));

            $oSchemaToDb = new SchemaToDb();
            $oSchemaToDb->run($oSchemaToDbVo, $output);
        }

        return Command::SUCCESS;
    }
}
