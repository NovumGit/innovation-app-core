<?php

namespace Cli\Database\Uninstall;

use Cli\BaseCommand;
use Cli\Database\Helper\Login;
use Cli\Helper\Questions\Domain;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class Uninstall extends BaseCommand
{
    protected function configure()
    {
        $this->setName("db:uninstall");
        $this->setDescription('Drops the domain database + user, <error>no questions asked so be careful</error>');
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, Domain::getInfo());
        $this->setHelp(<<<EOT
<comment>db::init performs the following steps</comment>
EOT
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $aDomains = $input->getArgument('domain');
        if (empty($aDomains)) {
            $oQuestionHelper = $this->getHelper('question');

            $oQuestion = new Question("<question>Please provide a system-id / domain name </question>: ");
            $sDomain = $oQuestionHelper->ask($input, $output, $oQuestion);
            $input->setArgument('domain', [$sDomain]);
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $aDomains = $input->getArgument('domain');

        if (empty($aDomains)) {
            $output->writeln("<error>No domains passed, nothing to drop or delete.</error>");
            return Command::SUCCESS;
        }

        foreach ($aDomains as $sDomain) {
            $oLogin = new Login($input, $output, $this->getHelper('question'), $aDomains);
            $oRootUser = $oLogin->getRootLogin();
            $oDomainUser = $oLogin->getDomainLogin($sDomain);

            $output->writeln("Deleting user {$oDomainUser->getUser()}");
            $oRootUser->userDrop($oDomainUser->getUser());
            $output->writeln("Dropping database {$oDomainUser->getDbName()}");
            $oRootUser->dbDrop($oDomainUser->getDbName());
        }
        return Command::SUCCESS;
    }
}
