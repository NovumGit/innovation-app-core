<?php

namespace Cli\Database;

use Cli\BaseCommand;
use Cli\Database\Make\Main;
use Hi\Helpers\DirectoryStructure;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Make extends BaseCommand
{
    protected function configure()
    {
        $aCommands = array_keys(Main::commands());
        array_walk($aCommands, function (&$sCommand) {
            $sCommand = "<info>{$sCommand}</info>";
        });
        $this->setName("db:make");
        $this->setDescription('Runs ' . join(', ', $aCommands) . ' one after another in this sequence.');
        $this->addArgument('domain', InputArgument::IS_ARRAY | InputArgument::OPTIONAL, 'domain to migrate');

        $this->setHelp(<<<EOT
When a new system is installed a lot of things need to get done in a specific order for
everyting to work. For instance, a virtual representation of the datamodel is created inside the database it self. This 
virtual representation is used by the ERD modeller to change the data model. But for this to work some  basic tables 
tructure needs to be in place. Another part of the system, that is handling the UI generation has a hard table structure
for each component so all properties that describe the component are stored in a structured and organised way. This 
table structure is generated based on xml files that are stored within each components folder. These are converted into
physical tables lateron but based on the virtual table structure that requires tables to be in place and so on. If you
would delete a database, this script would re-generate it into it's initial state.
EOT);
    }
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $oDirectoryStructure = new DirectoryStructure();
        $aDomains = $oDirectoryStructure->getDomainCollection();

        $aSelectedDomains = $input->getArgument('domain');

        if (empty($aDomains)) {
            $this->noDomainsMessage($output);
            return Command::FAILURE;
        } elseif (empty($aSelectedDomains)) {
            $this->selectDomainMessage($input, $output, $aDomains);
        }
        $aSelectedDomains = $input->getArgument('domain');
        $helper = $this->getHelper('question');
        $oMain = new Main($input, $output, $helper, $aSelectedDomains === 'all' ? [] : $aSelectedDomains, $this);
        $oMain->run();
        return Command::SUCCESS;
    }
}
