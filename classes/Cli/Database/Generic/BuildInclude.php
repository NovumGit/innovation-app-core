<?php

namespace Cli\Database\Generic;

use Cli\BaseCommand;
use Cli\Database\Helper\Propel;
use Cli\Helper\Questions\Domain;
use Exception;
use Hi\Helpers\DirectoryStructure;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BuildInclude extends BaseCommand
{
    protected function configure()
    {
        $this->setName("db:propel-build-helper");
        $this->setDescription('Helps importing the required files');
        $this
            ->addOption('schema-dir', null, InputOption::VALUE_REQUIRED, 'The directory where the schema files are placed')
            ->addOption('output-dir', null, InputOption::VALUE_REQUIRED, 'The output directory where the migration files are located')
            ->addOption('connection', null, InputOption::VALUE_IS_ARRAY | InputOption::VALUE_REQUIRED, 'Connection to use. Example: \'bookstore=mysql:host=127.0.0.1;dbname=test;user=root;password=foobar\' where "bookstore" is your propel database name (used in your schema.xml)', []);
        $this->addArgument('domain', InputArgument::OPTIONAL, Domain::getInfo());
        $this->setHelp(<<<EOT

EOT
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $oDirectoryStructure = new DirectoryStructure();
        $aDomains = $oDirectoryStructure->getDomainCollection();

        $aSelectedDomains = $input->getArgument('domain');

        if (empty($aDomains)) {
            $this->noDomainsMessage($output);
            return Command::FAILURE;
        } elseif (empty($aSelectedDomains)) {
            try {
                $this->selectDomainMessage($input, $output, $aDomains);
            } catch (Exception $e) {
                $output->writeln("<error>{$e->getMessage()}</error>");
                return Command::FAILURE;
            }
        }
        $aSelectedDomain = $input->getArgument('domain');
        $output->writeln('<info>Including propel build config (propel.php)</info>');
        $sBuildDir = dirname(Propel::getBuildFilePathName($aSelectedDomain[0]));
        chdir($sBuildDir);

        /*
        $aConnectionParams =  $aPropel['propel']['database']['connections']['hurah'];

        $sDsn = $aConnectionParams['dsn']. ';user=' . $aConnectionParams['user'] . ';password=' . $aConnectionParams['password'];

        $output->writeln("schema-dir {$aPropel['propel']['paths']['schemaDir']}");
        $input->setOption('schema-dir', $aPropel['propel']['paths']['schemaDir']);

        $output->writeln("output-dir './crud_queries'");
        $input->setOption('output-dir', './crud_queries');

        $output->writeln("connection {$sDsn}");
        $input->setOption('connection', $sDsn);

        */

        return Command::SUCCESS;
    }
}
