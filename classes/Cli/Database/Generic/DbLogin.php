<?php

namespace Cli\Database\Generic;

use mysqli;
use mysqli_sql_exception;
use Symfony\Component\Console\Output\OutputInterface;

class DbLogin
{
    private string $user;
    private string $db_name;
    private string $pass;
    private string $host;
    private OutputInterface $output;

    public function __construct(OutputInterface $output, string $user, string $pass, string $host = 'localhost', string $sDbName = 'same_as_db_user')
    {
        if ($sDbName === 'same_as_db_user') {
            $this->db_name = $user;
        }
        $this->user = $user;
        $this->pass = $pass;
        $this->host = $host;
        $this->output = $output;
    }

    /**
     * @return string
     */
    public function getUser(): string
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getDbName(): string
    {
        return $this->db_name;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @return string|string
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @param string|string $pass
     */
    public function setPass($pass)
    {
        $this->pass = $pass;
    }

    public function updatePassword(DbLogin $oDbLogin)
    {
        $sQuery = "ALTER USER '{$oDbLogin->getUser()}'@'%' IDENTIFIED BY '{$oDbLogin->getPass()}';";
        $this->query($sQuery);
        $this->flushPrivileges();
    }

    public function createUser(DbLogin $oDbLogin)
    {
        $sQuery = "DROP USER IF EXISTS '{$oDbLogin->getUser()}'@'%';";
        $this->query($sQuery);
        $this->flushPrivileges();

        $sQuery = "CREATE USER '{$oDbLogin->getUser()}'@'%' IDENTIFIED BY '{$oDbLogin->getPass()}';";
        $this->query($sQuery);
        $this->flushPrivileges();
    }

    public function flushPrivileges()
    {
        $this->output->writeln('FlUSH PRIVILEGES;', OutputInterface::VERBOSITY_VERBOSE);
        $this->query('FlUSH PRIVILEGES;');
    }

    private function query(string $sQuery)
    {
        $this->output->writeln($sQuery, OutputInterface::VERBOSITY_VERBOSE);
        $mysqli = $this->connect();
        $result = $mysqli->query($sQuery);

        if ($mysqli->errno) {
            $this->output->writeln('<error>' . $mysqli->errno . ' - ' . $mysqli->error . '</error>');
            $this->output->writeln($sQuery);
        } else {
            if (is_bool($result)) {
                // $sQuery = preg_replace('/ IDENTIFIED BY [\'"]{1}[a-zA-Z0-9]+[\'"]{1}/', ' IDENTIFIED BY \'***********\'', $sQuery);
                $this->output->writeln("<info>RESULT OF:</info> $sQuery <info>{$result}</info>");
            }
        }
        $mysqli->close();
        return $result;
    }

    private function connect(): mysqli
    {
        mysqli_report(MYSQLI_REPORT_STRICT);
        return mysqli_connect($this->host, $this->user, $this->pass);
    }

    public function grantUserAccess(DbLogin $oDbLogin, string $sDbName)
    {
        $sQuery = "GRANT ALL ON $sDbName.* TO '$oDbLogin->user'@'%';";
        $this->query($sQuery);
        $this->flushPrivileges();
    }

    public function canSelectDb(string $sDbName): bool
    {
        try {
            $mysqli = $this->connect();

            $mysqli->select_db($sDbName);
            return true;
        } catch (mysqli_sql_exception $e) {
            return false;
        }
    }

    public function userDrop(string $sUserName)
    {
        $aQueries = [
            "DROP USER IF EXISTS '{$sUserName}'@'localhost'",
            "DROP USER IF EXISTS '{$sUserName}'@'%'",
        ];
        foreach ($aQueries as $sQuery) {
            self::query($sQuery);
        }
    }

    public function dbDrop(string $sDbName)
    {
        $sQuery = "DROP DATABASE IF EXISTS {$sDbName}";
        $this->query($sQuery);
    }

    public function dbCreate(string $sDbName)
    {
        $sQuery = "CREATE DATABASE {$sDbName}";
        $this->query($sQuery);
    }

    public function dbExists(string $sDbName): bool
    {
        $mysqli = $this->connect();
        $sQuery = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '{$sDbName}'";
        $result = $mysqli->query($sQuery);

        return $result->num_rows > 0;
    }

    public function canConnect(): bool
    {
        try {
            mysqli_report(MYSQLI_REPORT_STRICT);
            $this->output->writeln("Connecting to <info>{$this->host}</info>");
            $mysqli = mysqli_connect($this->host, $this->user, $this->pass);

            if ($mysqli) {
                return true;
            }
            return false;
        } catch (mysqli_sql_exception $e) {
            return false;
        }
    }
}
