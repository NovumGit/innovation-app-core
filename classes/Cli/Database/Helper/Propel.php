<?php

namespace Cli\Database\Helper;

use Cli\BaseCommand;
use Core\Cfg;
use Core\Utils;
use Exception\FileNotFoundException;
use Hi\Helpers\DirectoryStructure;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\NamespaceNotFoundException;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\OutputInterface;

class Propel
{

    public static function getBuildFilePathName(string $sDomain): string
    {
        $oDirectoryStructure = new DirectoryStructure();
        return Utils::makePath($oDirectoryStructure->getSystemDir(true), 'build', 'database', $sDomain, 'propel.php');
    }

    public static function getBuildFileContents(string $sDomain): array
    {

        $sConfigBuildFile = self::getBuildFilePathName($sDomain);
        return require $sConfigBuildFile;
    }

    public static function includeConfigs(string $sDomain, OutputInterface $output):void
    {
        $oDirectoryStructure = new DirectoryStructure();
        $sPropelConfig = Utils::makePath($oDirectoryStructure->getConfigRoot(true), $sDomain, 'propel', 'config.php');
        $sSystemConfigPath = Utils::makePath($oDirectoryStructure->getConfigRoot(true), $sDomain, 'config.php');

        if (!file_exists($sPropelConfig)) {
            throw new FileNotFoundException("Propel config file missing");
        }
        require_once $sPropelConfig;

        if (!file_exists($sSystemConfigPath)) {
            $output->writeln("Config file missing  <error>$sSystemConfigPath</error>");
            return;
        }
        $aSystemConfig = require $sSystemConfigPath;
        Cfg::set($aSystemConfig);
    }

    /**
     * @param string $sDomain
     * @param string $sCommand
     * @param OutputInterface $output
     * @param BaseCommand $oCommand
     * @param array $aExtra
     * @throws \Exception
     */
    public static function runCommand(string $sDomain, string $sCommand, OutputInterface $output, BaseCommand $oCommand, array $aExtra = [])
    {
        $sPrevDir = getcwd();
        $oDirectoryStructure = new DirectoryStructure();
        $sDatabaseDir = Utils::makePath($oDirectoryStructure->getSystemDir(true), 'build', 'database', $sDomain);
        chdir($sDatabaseDir);

        $output->writeln("Executing propel <info>{$sCommand}</info>");
        try {
            $command = $oCommand->getApplication()->find($sCommand);
        } catch (NamespaceNotFoundException $e) {
            $command = $oCommand->getApplication()->find('propel:' . $sCommand);
        }

        $aInput = new ArrayInput($aExtra);
        $returnCode = $command->run($aInput, $output);
        if ($returnCode === Command::SUCCESS) {
            $output->writeln("Propel {$sCommand} executed <info>succesfully</info>");
        } else {
            $output->writeln("Propel {$sCommand} execution <warning>failed</warning>");
        }
        chdir($sPrevDir);
    }
}
