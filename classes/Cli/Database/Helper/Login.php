<?php

namespace Cli\Database\Helper;

use Cli\Database\Generic\DbLogin;
use Hi\Helpers\DirectoryStructure;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class Login
{

    protected InputInterface $input;
    protected OutputInterface $output;
    protected QuestionHelper $helper;
    protected array $aDomains;

    public function __construct(InputInterface $input, OutputInterface $output, QuestionHelper $helper, array $aDomains = [])
    {
        $this->input = $input;
        $this->output = $output;
        $this->helper = $helper;
        $this->aDomains = $aDomains;
    }

    public function getRootLogin(): DbLogin
    {
        $sRootEnvFile = './.env';
        $sDbServer = 'localhost';
        if (file_exists($sRootEnvFile)) {
            $aEnvFile = parse_ini_file($sRootEnvFile);

            if (isset($aEnvFile['DATABASE_IP'])) {
                $sDbServer = $aEnvFile['DATABASE_IP'];
            }

            if (isset($aEnvFile['DATABASE_ROOT_PASSWORD'])) {
                $sRootUser = 'root';
                $sRootPassword = $aEnvFile['DATABASE_ROOT_PASSWORD'];
            }
            if (isset($aEnvFile['DATABASE_ROOT_USER'])) {
                $sRootUser = $aEnvFile['DATABASE_ROOT_USER'];
            }
        }

        if (!isset($sRootUser)) {
            $sRootUser = $this->askUsername();
        }
        if (!isset($sRootPassword)) {
            $sRootPassword = $this->askPassword();
        }
        $oDbLogin = new DbLogin($this->output, $sRootUser, $sRootPassword, $sDbServer);

        while (!$oDbLogin->canConnect()) {
            $user = $this->askUsername();
            $password = $this->askPassword();
            $oDbLogin = new DbLogin($this->output, $user, $password, $sDbServer);
        }

        return $oDbLogin;
    }

    public function getDomainLogin($sDomain): DbLogin
    {
        $oDirectoryStructure = new DirectoryStructure();
        $sDomainEnvSrc = $oDirectoryStructure->getDomainDir() . '/' . $sDomain . '/.env';

        $aEnvFile = [];
        if (file_exists($sDomainEnvSrc)) {
            $this->output->writeln("Reading database settings from " . $sDomainEnvSrc);
            $aEnvFile = parse_ini_file($sDomainEnvSrc);
        } else {
            $this->output->writeln("<error>Env file missing at " . $sDomainEnvSrc . "</error>");
        }

        if (isset($aEnvFile['DB_USER'])) {
            $sDbUser = $aEnvFile['DB_USER'];
        } else {
            $sDbUser = $this->askUsername('domain');
        }

        if (isset($aEnvFile['DB_PASS'])) {
            $sDbPass = $aEnvFile['DB_PASS'];
        } else {
            $sDbPass = $this->askPassword('domain');
        }

        if (isset($aEnvFile['DB_HOST'])) {
            $sDbServer = $aEnvFile['DB_HOST'];
        } else {
            $sDbServer = $this->askServer();
        }
        return new DbLogin($this->output, $sDbUser, $sDbPass, $sDbServer);
    }

    private function askUsername($sType = 'root'): string
    {
        $question = new Question("<question>Please provide the mysql $sType username: </question>");
        return $this->helper->ask($this->input, $this->output, $question);
    }

    private function askPassword($sType = 'root'): string
    {
        $question = new Question("<question>Please provide the mysql $sType password: </question>");
        return $this->helper->ask($this->input, $this->output, $question);
    }

    private function askServer(): string
    {
        $question = new Question("<question>Please provide the ip address or hostname of the database server: </question>");
        return $this->helper->ask($this->input, $this->output, $question);
    }
}
