<?php

namespace Cli\Database\Make;

use Cli\BaseCommand;
use Composer\IO\ConsoleIO;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Main
{
    private InputInterface $input;
    private OutputInterface $output;
    private QuestionHelper $helper;
    private array $aDomains;
    private BaseCommand $oCommand;

    public function __construct(InputInterface $input, OutputInterface $output, QuestionHelper $helper, array $aDomains, BaseCommand $oCommand)
    {
        $this->input = $input;
        $this->output = $output;
        $this->helper = $helper;
        $this->aDomains = $aDomains;
        $this->oCommand = $oCommand;
    }

    public static function commands(): array
    {
        return [
            [
                'command'   => 'db:init',
                'arguments' => [],
            ],
            [
                'command'   => 'db:migrate',
                'arguments' => ['--source' => 'schema'],
            ],
            [
                'command'   => 'db:load-essentials',
                'arguments' => [],
            ],
            [
                'command'   => 'db:schema-to-db',
                'arguments' => [],
            ],
            [
                'command'   => 'component:structure',
                'arguments' => [],
            ],
            [
                'command'   => 'db:migrate',
                'arguments' => ['--source' => 'virtual'],
            ],
            [
                'command'   => 'component:data',
                'arguments' => [],
            ],
            [
                'command'   => 'db:load-custom',
                'arguments' => [],
            ],
        ];
    }

    public function run()
    {
        $outputStyle = new OutputFormatterStyle('white', 'blue');
        $this->output->getFormatter()->setStyle('section', $outputStyle);
        $outputStyleBold = new OutputFormatterStyle('white', 'blue', ['bold']);
        $this->output->getFormatter()->setStyle('bold-section', $outputStyleBold);

        foreach ($this->aDomains as $sDomain) {
            foreach (self::commands() as $aCommand) {
                $this->output->writeln("<section>Executing <bold-section>{$aCommand['command']}</bold-section> on <bold-section>$sDomain</bold-section></section>");
                $this->call($sDomain, $aCommand['command'], $aCommand['arguments']);
            }
        }
    }

    private function call(string $sDomain, string $sCommand, array $aExtraInput = []): void
    {

        $this->oCommand->setIO(new ConsoleIO($this->input, $this->output, $this->oCommand->getHelperSet()));

        $io = $this->oCommand->getIO();

        $formatter = $this->oCommand->getHelperSet()->get('formatter');

        $io->writeError([
            '',
            $formatter->formatBlock('Executing ' . $sCommand . ' - ' . json_encode($aExtraInput), 'bg=magenta;fg=white', true),
            '',
        ]);

        $this->output->writeln("Executing <info>{$sCommand}</info>");
        $command = $this->oCommand->getApplication()->find($sCommand);
        $aCommandInput = array_merge(['domain' => [$sDomain]], $aExtraInput);

        $this->output->writeln("$sCommand input " . json_encode($aCommandInput));

        $aInput = new ArrayInput($aCommandInput);
        $returnCode = $command->run($aInput, $this->output);
        if ($returnCode === Command::SUCCESS) {
            $this->output->writeln("<info>{$sCommand}</info> on <info>$sDomain</info> executed <info>successfully</info>");
        } else {
            $this->output->writeln("<info>{$sCommand}</info> on <info>$sDomain</info> executed <warning>failed</warning>");
            sleep(2);
        }
    }
}
