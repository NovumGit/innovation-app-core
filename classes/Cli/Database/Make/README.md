# Command db:make
Performs all the steps required to create a new database and installs initial data. This command
performs the following steps.


## db:init
-- *Responsible for creating the database + user*

1. Checks if the user exists.
2. Creates the user if needed.
3. Checks if the database exists.
4. Creates a new database if needed.
5. Checks if the user has access to the database.
6. Grants access to the database if needed.
7. Creates a new [propel](http://propelorm.org/documentation/02-buildtime.html) build config file.  
8. Runs ```db:migrate``` to create an initial data model.

## db:migrate 
-- *Responsible for setting up the database schema*

1. Runs ```config:convert``` to create a new database configuration file.
2. Reads the file ```.system/build/database/<system.id>/schema.xml```
3. Creates a ```migrations``` directory to hold propel migration files.
4. Creates a ```generated-schema```, this will hold converted schema files that are compatible with Propel ORM.
5. Check to see if the database has already been set up by looking for a specific table ```data_model``` that should always be there.
6. Creates an additional schema file called ```user-schema.xml```. All the stuff that has been created via the [ERD](https://gitlab.com/NovumGit/novum-erdhttps://gitlab.com/NovumGit/novum-erd) tool will be stored in here.
7. Runs Propel ```diff``` command to generated migration scripts. 
8. Runs Propel ```migrate``` command to apply changes to the database.
9. If in step 5 the database dit not exist, will now try again to generate ```user-schema.xml``` based on the ```data_model``` table.
10. Runs Propel  ```model:build``` to generate model classes.  

## db:load
-- *Responsible for running any scripts that contain actual data required for the system to operate*
1. Loops over all php files inside ```.system/build/database/_default/novum/*``` and runs them.
2. Loops over all php files inside ```.system/build/database/<system.id>/crud_queries/*``` and runs them.
3. Each script that was executed is recorded inside the ``migration_scripts`` table. If the script is already there it won't be executed again.

## db:load-ui-components
-- *Responsible for generating table structure to hold LowCode state information*
1. Loops over all ```components```, which are located in ```classes/LowCode/Component```.
2. Parses the ```component.xml``` file to get all the configurable options available.
3. Defines a new table for each component inside the ```data_model``` table and its sub tables (```data_field```,```model_constraint```)
4. Inserts data into the newly ui component tables. ```which may not exist yet```


