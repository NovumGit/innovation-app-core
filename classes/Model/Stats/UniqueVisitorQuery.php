<?php

namespace Model\Stats;

use Core\QueryMapper;
use Model\Stats\Base\UniqueVisitorQuery as BaseUniqueVisitorQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'unique_visitor' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UniqueVisitorQuery extends BaseUniqueVisitorQuery
{

    public static function registerVisitor()
    {
        $sRemoteAddr = $_SERVER['REMOTE_ADDR'];
        $sDate = date('Y-m-d');

        $sQuery = "INSERT IGNORE INTO unique_visitor
                  (
                    `ip`,
                    `visit_date`,
                    `count`
                  )
                  VALUE 
                  (
                    '$sRemoteAddr',                    
                    '$sDate',
                    1
                  )
                  ON DUPLICATE KEY UPDATE 
                  `count` = `count` + 1";
        QueryMapper::query($sQuery);
    }
}
