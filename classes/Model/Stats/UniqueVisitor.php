<?php

namespace Model\Stats;

use Model\Stats\Base\UniqueVisitor as BaseUniqueVisitor;

/**
 * Skeleton subclass for representing a row from the 'unique_visitor' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UniqueVisitor extends BaseUniqueVisitor
{

}
