<?php

namespace Model\Company\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Cms\Site;
use Model\Cms\SiteQuery;
use Model\Cms\Base\Site as BaseSite;
use Model\Cms\Map\SiteTableMap;
use Model\Company\Company as ChildCompany;
use Model\Company\CompanyLogo as ChildCompanyLogo;
use Model\Company\CompanyLogoQuery as ChildCompanyLogoQuery;
use Model\Company\CompanyQuery as ChildCompanyQuery;
use Model\Company\CompanySetting as ChildCompanySetting;
use Model\Company\CompanySettingQuery as ChildCompanySettingQuery;
use Model\Company\Map\CompanyLogoTableMap;
use Model\Company\Map\CompanySettingTableMap;
use Model\Company\Map\CompanyTableMap;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\CountryQuery;
use Model\Setting\MasterTable\LegalForm;
use Model\Setting\MasterTable\LegalFormQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'own_company' table.
 *
 *
 *
 * @package    propel.generator.Model.Company.Base
 */
abstract class Company implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Company\\Map\\CompanyTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the is_default_supplier field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_default_supplier;

    /**
     * The value for the created_on field.
     *
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        DateTime|null
     */
    protected $created_on;

    /**
     * The value for the is_deleted field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_deleted;

    /**
     * The value for the company_name field.
     *
     * @var        string|null
     */
    protected $company_name;

    /**
     * The value for the slogan field.
     *
     * @var        string|null
     */
    protected $slogan;

    /**
     * The value for the chamber_of_commerce field.
     *
     * @var        string|null
     */
    protected $chamber_of_commerce;

    /**
     * The value for the vat_number field.
     *
     * @var        string|null
     */
    protected $vat_number;

    /**
     * The value for the legal_form_id field.
     *
     * @var        int|null
     */
    protected $legal_form_id;

    /**
     * The value for the iban field.
     *
     * @var        string|null
     */
    protected $iban;

    /**
     * The value for the bic field.
     *
     * @var        string|null
     */
    protected $bic;

    /**
     * The value for the phone field.
     *
     * @var        string|null
     */
    protected $phone;

    /**
     * The value for the fax field.
     *
     * @var        string|null
     */
    protected $fax;

    /**
     * The value for the website field.
     *
     * @var        string|null
     */
    protected $website;

    /**
     * The value for the email field.
     *
     * @var        string|null
     */
    protected $email;

    /**
     * The value for the general_street field.
     *
     * @var        string|null
     */
    protected $general_street;

    /**
     * The value for the general_number field.
     *
     * @var        string|null
     */
    protected $general_number;

    /**
     * The value for the general_number_add field.
     *
     * @var        string|null
     */
    protected $general_number_add;

    /**
     * The value for the general_postal field.
     *
     * @var        string|null
     */
    protected $general_postal;

    /**
     * The value for the general_city field.
     *
     * @var        string|null
     */
    protected $general_city;

    /**
     * The value for the general_po_box field.
     *
     * @var        string|null
     */
    protected $general_po_box;

    /**
     * The value for the general_country_id field.
     *
     * @var        int|null
     */
    protected $general_country_id;

    /**
     * The value for the has_invoice_address field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $has_invoice_address;

    /**
     * The value for the invoice_company_name field.
     *
     * @var        string|null
     */
    protected $invoice_company_name;

    /**
     * The value for the invoice_street field.
     *
     * @var        string|null
     */
    protected $invoice_street;

    /**
     * The value for the invoice_number field.
     *
     * @var        string|null
     */
    protected $invoice_number;

    /**
     * The value for the invoice_number_add field.
     *
     * @var        string|null
     */
    protected $invoice_number_add;

    /**
     * The value for the invoice_postal field.
     *
     * @var        string|null
     */
    protected $invoice_postal;

    /**
     * The value for the invoice_city field.
     *
     * @var        string|null
     */
    protected $invoice_city;

    /**
     * The value for the invoice_country_id field.
     *
     * @var        int|null
     */
    protected $invoice_country_id;

    /**
     * The value for the has_delivery_address field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $has_delivery_address;

    /**
     * The value for the delivery_company_name field.
     *
     * @var        string|null
     */
    protected $delivery_company_name;

    /**
     * The value for the delivery_street field.
     *
     * @var        string|null
     */
    protected $delivery_street;

    /**
     * The value for the delivery_number field.
     *
     * @var        string|null
     */
    protected $delivery_number;

    /**
     * The value for the delivery_number_add field.
     *
     * @var        string|null
     */
    protected $delivery_number_add;

    /**
     * The value for the delivery_postal field.
     *
     * @var        string|null
     */
    protected $delivery_postal;

    /**
     * The value for the delivery_city field.
     *
     * @var        string|null
     */
    protected $delivery_city;

    /**
     * The value for the delivery_country_id field.
     *
     * @var        int|null
     */
    protected $delivery_country_id;

    /**
     * The value for the company_logo_id field.
     *
     * @var        int|null
     */
    protected $company_logo_id;

    /**
     * The value for the max_shipping_costs field.
     *
     * @var        int|null
     */
    protected $max_shipping_costs;

    /**
     * @var        Country
     */
    protected $aGeneralCountry;

    /**
     * @var        Country
     */
    protected $aInvoiceCountry;

    /**
     * @var        Country
     */
    protected $aDeliveryCountry;

    /**
     * @var        LegalForm
     */
    protected $aLegalForm;

    /**
     * @var        ChildCompanyLogo
     */
    protected $aOwnCompanyLogo;

    /**
     * @var        ObjectCollection|ChildCompanyLogo[] Collection to store aggregation of ChildCompanyLogo objects.
     */
    protected $collCompanyLogosRelatedByOwnCompanyId;
    protected $collCompanyLogosRelatedByOwnCompanyIdPartial;

    /**
     * @var        ObjectCollection|Site[] Collection to store aggregation of Site objects.
     */
    protected $collSites;
    protected $collSitesPartial;

    /**
     * @var        ObjectCollection|ChildCompanySetting[] Collection to store aggregation of ChildCompanySetting objects.
     */
    protected $collCompanySettings;
    protected $collCompanySettingsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCompanyLogo[]
     */
    protected $companyLogosRelatedByOwnCompanyIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Site[]
     */
    protected $sitesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCompanySetting[]
     */
    protected $companySettingsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_default_supplier = false;
        $this->is_deleted = false;
        $this->has_invoice_address = false;
        $this->has_delivery_address = false;
    }

    /**
     * Initializes internal state of Model\Company\Base\Company object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Company</code> instance.  If
     * <code>obj</code> is an instance of <code>Company</code>, delegates to
     * <code>equals(Company)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [is_default_supplier] column value.
     *
     * @return boolean|null
     */
    public function getIsDefaultSupplier()
    {
        return $this->is_default_supplier;
    }

    /**
     * Get the [is_default_supplier] column value.
     *
     * @return boolean|null
     */
    public function isDefaultSupplier()
    {
        return $this->getIsDefaultSupplier();
    }

    /**
     * Get the [optionally formatted] temporal [created_on] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedOn($format = null)
    {
        if ($format === null) {
            return $this->created_on;
        } else {
            return $this->created_on instanceof \DateTimeInterface ? $this->created_on->format($format) : null;
        }
    }

    /**
     * Get the [is_deleted] column value.
     *
     * @return boolean|null
     */
    public function getItemDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Get the [is_deleted] column value.
     *
     * @return boolean|null
     */
    public function isItemDeleted()
    {
        return $this->getItemDeleted();
    }

    /**
     * Get the [company_name] column value.
     *
     * @return string|null
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Get the [slogan] column value.
     *
     * @return string|null
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * Get the [chamber_of_commerce] column value.
     *
     * @return string|null
     */
    public function getChamberOfCommerce()
    {
        return $this->chamber_of_commerce;
    }

    /**
     * Get the [vat_number] column value.
     *
     * @return string|null
     */
    public function getVatNumber()
    {
        return $this->vat_number;
    }

    /**
     * Get the [legal_form_id] column value.
     *
     * @return int|null
     */
    public function getLegalFormId()
    {
        return $this->legal_form_id;
    }

    /**
     * Get the [iban] column value.
     *
     * @return string|null
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Get the [bic] column value.
     *
     * @return string|null
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Get the [phone] column value.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get the [fax] column value.
     *
     * @return string|null
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Get the [website] column value.
     *
     * @return string|null
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Get the [email] column value.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [general_street] column value.
     *
     * @return string|null
     */
    public function getGeneralStreet()
    {
        return $this->general_street;
    }

    /**
     * Get the [general_number] column value.
     *
     * @return string|null
     */
    public function getGeneralNumber()
    {
        return $this->general_number;
    }

    /**
     * Get the [general_number_add] column value.
     *
     * @return string|null
     */
    public function getGeneralNumberAdd()
    {
        return $this->general_number_add;
    }

    /**
     * Get the [general_postal] column value.
     *
     * @return string|null
     */
    public function getGeneralPostal()
    {
        return $this->general_postal;
    }

    /**
     * Get the [general_city] column value.
     *
     * @return string|null
     */
    public function getGeneralCity()
    {
        return $this->general_city;
    }

    /**
     * Get the [general_po_box] column value.
     *
     * @return string|null
     */
    public function getGeneralPoBox()
    {
        return $this->general_po_box;
    }

    /**
     * Get the [general_country_id] column value.
     *
     * @return int|null
     */
    public function getGeneralCountryId()
    {
        return $this->general_country_id;
    }

    /**
     * Get the [has_invoice_address] column value.
     *
     * @return boolean|null
     */
    public function getHasInvoiceAddress()
    {
        return $this->has_invoice_address;
    }

    /**
     * Get the [has_invoice_address] column value.
     *
     * @return boolean|null
     */
    public function hasInvoiceAddress()
    {
        return $this->getHasInvoiceAddress();
    }

    /**
     * Get the [invoice_company_name] column value.
     *
     * @return string|null
     */
    public function getInvoiceCompanyName()
    {
        return $this->invoice_company_name;
    }

    /**
     * Get the [invoice_street] column value.
     *
     * @return string|null
     */
    public function getInvoiceStreet()
    {
        return $this->invoice_street;
    }

    /**
     * Get the [invoice_number] column value.
     *
     * @return string|null
     */
    public function getInvoiceNumber()
    {
        return $this->invoice_number;
    }

    /**
     * Get the [invoice_number_add] column value.
     *
     * @return string|null
     */
    public function getInvoiceNumberAdd()
    {
        return $this->invoice_number_add;
    }

    /**
     * Get the [invoice_postal] column value.
     *
     * @return string|null
     */
    public function getInvoicePostal()
    {
        return $this->invoice_postal;
    }

    /**
     * Get the [invoice_city] column value.
     *
     * @return string|null
     */
    public function getInvoiceCity()
    {
        return $this->invoice_city;
    }

    /**
     * Get the [invoice_country_id] column value.
     *
     * @return int|null
     */
    public function getInvoiceCountryId()
    {
        return $this->invoice_country_id;
    }

    /**
     * Get the [has_delivery_address] column value.
     *
     * @return boolean|null
     */
    public function getHasDeliveryAddress()
    {
        return $this->has_delivery_address;
    }

    /**
     * Get the [has_delivery_address] column value.
     *
     * @return boolean|null
     */
    public function hasDeliveryAddress()
    {
        return $this->getHasDeliveryAddress();
    }

    /**
     * Get the [delivery_company_name] column value.
     *
     * @return string|null
     */
    public function getDeliveryCompanyName()
    {
        return $this->delivery_company_name;
    }

    /**
     * Get the [delivery_street] column value.
     *
     * @return string|null
     */
    public function getDeliveryStreet()
    {
        return $this->delivery_street;
    }

    /**
     * Get the [delivery_number] column value.
     *
     * @return string|null
     */
    public function getDeliveryNumber()
    {
        return $this->delivery_number;
    }

    /**
     * Get the [delivery_number_add] column value.
     *
     * @return string|null
     */
    public function getDeliveryNumberAdd()
    {
        return $this->delivery_number_add;
    }

    /**
     * Get the [delivery_postal] column value.
     *
     * @return string|null
     */
    public function getDeliveryPostal()
    {
        return $this->delivery_postal;
    }

    /**
     * Get the [delivery_city] column value.
     *
     * @return string|null
     */
    public function getDeliveryCity()
    {
        return $this->delivery_city;
    }

    /**
     * Get the [delivery_country_id] column value.
     *
     * @return int|null
     */
    public function getDeliveryCountryId()
    {
        return $this->delivery_country_id;
    }

    /**
     * Get the [company_logo_id] column value.
     *
     * @return int|null
     */
    public function getCompanyLogoId()
    {
        return $this->company_logo_id;
    }

    /**
     * Get the [max_shipping_costs] column value.
     *
     * @return int|null
     */
    public function getMaxShippingCosts()
    {
        return $this->max_shipping_costs;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[CompanyTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Sets the value of the [is_default_supplier] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setIsDefaultSupplier($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_default_supplier !== $v) {
            $this->is_default_supplier = $v;
            $this->modifiedColumns[CompanyTableMap::COL_IS_DEFAULT_SUPPLIER] = true;
        }

        return $this;
    } // setIsDefaultSupplier()

    /**
     * Sets the value of [created_on] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setCreatedOn($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_on !== null || $dt !== null) {
            if ($this->created_on === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_on->format("Y-m-d H:i:s.u")) {
                $this->created_on = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CompanyTableMap::COL_CREATED_ON] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedOn()

    /**
     * Sets the value of the [is_deleted] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setItemDeleted($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_deleted !== $v) {
            $this->is_deleted = $v;
            $this->modifiedColumns[CompanyTableMap::COL_IS_DELETED] = true;
        }

        return $this;
    } // setItemDeleted()

    /**
     * Set the value of [company_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setCompanyName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->company_name !== $v) {
            $this->company_name = $v;
            $this->modifiedColumns[CompanyTableMap::COL_COMPANY_NAME] = true;
        }

        return $this;
    } // setCompanyName()

    /**
     * Set the value of [slogan] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setSlogan($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->slogan !== $v) {
            $this->slogan = $v;
            $this->modifiedColumns[CompanyTableMap::COL_SLOGAN] = true;
        }

        return $this;
    } // setSlogan()

    /**
     * Set the value of [chamber_of_commerce] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setChamberOfCommerce($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->chamber_of_commerce !== $v) {
            $this->chamber_of_commerce = $v;
            $this->modifiedColumns[CompanyTableMap::COL_CHAMBER_OF_COMMERCE] = true;
        }

        return $this;
    } // setChamberOfCommerce()

    /**
     * Set the value of [vat_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setVatNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->vat_number !== $v) {
            $this->vat_number = $v;
            $this->modifiedColumns[CompanyTableMap::COL_VAT_NUMBER] = true;
        }

        return $this;
    } // setVatNumber()

    /**
     * Set the value of [legal_form_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setLegalFormId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->legal_form_id !== $v) {
            $this->legal_form_id = $v;
            $this->modifiedColumns[CompanyTableMap::COL_LEGAL_FORM_ID] = true;
        }

        if ($this->aLegalForm !== null && $this->aLegalForm->getId() !== $v) {
            $this->aLegalForm = null;
        }

        return $this;
    } // setLegalFormId()

    /**
     * Set the value of [iban] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setIban($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->iban !== $v) {
            $this->iban = $v;
            $this->modifiedColumns[CompanyTableMap::COL_IBAN] = true;
        }

        return $this;
    } // setIban()

    /**
     * Set the value of [bic] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setBic($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->bic !== $v) {
            $this->bic = $v;
            $this->modifiedColumns[CompanyTableMap::COL_BIC] = true;
        }

        return $this;
    } // setBic()

    /**
     * Set the value of [phone] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[CompanyTableMap::COL_PHONE] = true;
        }

        return $this;
    } // setPhone()

    /**
     * Set the value of [fax] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setFax($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fax !== $v) {
            $this->fax = $v;
            $this->modifiedColumns[CompanyTableMap::COL_FAX] = true;
        }

        return $this;
    } // setFax()

    /**
     * Set the value of [website] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setWebsite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->website !== $v) {
            $this->website = $v;
            $this->modifiedColumns[CompanyTableMap::COL_WEBSITE] = true;
        }

        return $this;
    } // setWebsite()

    /**
     * Set the value of [email] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[CompanyTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [general_street] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setGeneralStreet($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->general_street !== $v) {
            $this->general_street = $v;
            $this->modifiedColumns[CompanyTableMap::COL_GENERAL_STREET] = true;
        }

        return $this;
    } // setGeneralStreet()

    /**
     * Set the value of [general_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setGeneralNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->general_number !== $v) {
            $this->general_number = $v;
            $this->modifiedColumns[CompanyTableMap::COL_GENERAL_NUMBER] = true;
        }

        return $this;
    } // setGeneralNumber()

    /**
     * Set the value of [general_number_add] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setGeneralNumberAdd($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->general_number_add !== $v) {
            $this->general_number_add = $v;
            $this->modifiedColumns[CompanyTableMap::COL_GENERAL_NUMBER_ADD] = true;
        }

        return $this;
    } // setGeneralNumberAdd()

    /**
     * Set the value of [general_postal] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setGeneralPostal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->general_postal !== $v) {
            $this->general_postal = $v;
            $this->modifiedColumns[CompanyTableMap::COL_GENERAL_POSTAL] = true;
        }

        return $this;
    } // setGeneralPostal()

    /**
     * Set the value of [general_city] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setGeneralCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->general_city !== $v) {
            $this->general_city = $v;
            $this->modifiedColumns[CompanyTableMap::COL_GENERAL_CITY] = true;
        }

        return $this;
    } // setGeneralCity()

    /**
     * Set the value of [general_po_box] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setGeneralPoBox($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->general_po_box !== $v) {
            $this->general_po_box = $v;
            $this->modifiedColumns[CompanyTableMap::COL_GENERAL_PO_BOX] = true;
        }

        return $this;
    } // setGeneralPoBox()

    /**
     * Set the value of [general_country_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setGeneralCountryId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->general_country_id !== $v) {
            $this->general_country_id = $v;
            $this->modifiedColumns[CompanyTableMap::COL_GENERAL_COUNTRY_ID] = true;
        }

        if ($this->aGeneralCountry !== null && $this->aGeneralCountry->getId() !== $v) {
            $this->aGeneralCountry = null;
        }

        return $this;
    } // setGeneralCountryId()

    /**
     * Sets the value of the [has_invoice_address] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setHasInvoiceAddress($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_invoice_address !== $v) {
            $this->has_invoice_address = $v;
            $this->modifiedColumns[CompanyTableMap::COL_HAS_INVOICE_ADDRESS] = true;
        }

        return $this;
    } // setHasInvoiceAddress()

    /**
     * Set the value of [invoice_company_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setInvoiceCompanyName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->invoice_company_name !== $v) {
            $this->invoice_company_name = $v;
            $this->modifiedColumns[CompanyTableMap::COL_INVOICE_COMPANY_NAME] = true;
        }

        return $this;
    } // setInvoiceCompanyName()

    /**
     * Set the value of [invoice_street] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setInvoiceStreet($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->invoice_street !== $v) {
            $this->invoice_street = $v;
            $this->modifiedColumns[CompanyTableMap::COL_INVOICE_STREET] = true;
        }

        return $this;
    } // setInvoiceStreet()

    /**
     * Set the value of [invoice_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setInvoiceNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->invoice_number !== $v) {
            $this->invoice_number = $v;
            $this->modifiedColumns[CompanyTableMap::COL_INVOICE_NUMBER] = true;
        }

        return $this;
    } // setInvoiceNumber()

    /**
     * Set the value of [invoice_number_add] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setInvoiceNumberAdd($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->invoice_number_add !== $v) {
            $this->invoice_number_add = $v;
            $this->modifiedColumns[CompanyTableMap::COL_INVOICE_NUMBER_ADD] = true;
        }

        return $this;
    } // setInvoiceNumberAdd()

    /**
     * Set the value of [invoice_postal] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setInvoicePostal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->invoice_postal !== $v) {
            $this->invoice_postal = $v;
            $this->modifiedColumns[CompanyTableMap::COL_INVOICE_POSTAL] = true;
        }

        return $this;
    } // setInvoicePostal()

    /**
     * Set the value of [invoice_city] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setInvoiceCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->invoice_city !== $v) {
            $this->invoice_city = $v;
            $this->modifiedColumns[CompanyTableMap::COL_INVOICE_CITY] = true;
        }

        return $this;
    } // setInvoiceCity()

    /**
     * Set the value of [invoice_country_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setInvoiceCountryId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->invoice_country_id !== $v) {
            $this->invoice_country_id = $v;
            $this->modifiedColumns[CompanyTableMap::COL_INVOICE_COUNTRY_ID] = true;
        }

        if ($this->aInvoiceCountry !== null && $this->aInvoiceCountry->getId() !== $v) {
            $this->aInvoiceCountry = null;
        }

        return $this;
    } // setInvoiceCountryId()

    /**
     * Sets the value of the [has_delivery_address] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setHasDeliveryAddress($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_delivery_address !== $v) {
            $this->has_delivery_address = $v;
            $this->modifiedColumns[CompanyTableMap::COL_HAS_DELIVERY_ADDRESS] = true;
        }

        return $this;
    } // setHasDeliveryAddress()

    /**
     * Set the value of [delivery_company_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setDeliveryCompanyName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->delivery_company_name !== $v) {
            $this->delivery_company_name = $v;
            $this->modifiedColumns[CompanyTableMap::COL_DELIVERY_COMPANY_NAME] = true;
        }

        return $this;
    } // setDeliveryCompanyName()

    /**
     * Set the value of [delivery_street] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setDeliveryStreet($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->delivery_street !== $v) {
            $this->delivery_street = $v;
            $this->modifiedColumns[CompanyTableMap::COL_DELIVERY_STREET] = true;
        }

        return $this;
    } // setDeliveryStreet()

    /**
     * Set the value of [delivery_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setDeliveryNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->delivery_number !== $v) {
            $this->delivery_number = $v;
            $this->modifiedColumns[CompanyTableMap::COL_DELIVERY_NUMBER] = true;
        }

        return $this;
    } // setDeliveryNumber()

    /**
     * Set the value of [delivery_number_add] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setDeliveryNumberAdd($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->delivery_number_add !== $v) {
            $this->delivery_number_add = $v;
            $this->modifiedColumns[CompanyTableMap::COL_DELIVERY_NUMBER_ADD] = true;
        }

        return $this;
    } // setDeliveryNumberAdd()

    /**
     * Set the value of [delivery_postal] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setDeliveryPostal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->delivery_postal !== $v) {
            $this->delivery_postal = $v;
            $this->modifiedColumns[CompanyTableMap::COL_DELIVERY_POSTAL] = true;
        }

        return $this;
    } // setDeliveryPostal()

    /**
     * Set the value of [delivery_city] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setDeliveryCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->delivery_city !== $v) {
            $this->delivery_city = $v;
            $this->modifiedColumns[CompanyTableMap::COL_DELIVERY_CITY] = true;
        }

        return $this;
    } // setDeliveryCity()

    /**
     * Set the value of [delivery_country_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setDeliveryCountryId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->delivery_country_id !== $v) {
            $this->delivery_country_id = $v;
            $this->modifiedColumns[CompanyTableMap::COL_DELIVERY_COUNTRY_ID] = true;
        }

        if ($this->aDeliveryCountry !== null && $this->aDeliveryCountry->getId() !== $v) {
            $this->aDeliveryCountry = null;
        }

        return $this;
    } // setDeliveryCountryId()

    /**
     * Set the value of [company_logo_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setCompanyLogoId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->company_logo_id !== $v) {
            $this->company_logo_id = $v;
            $this->modifiedColumns[CompanyTableMap::COL_COMPANY_LOGO_ID] = true;
        }

        if ($this->aOwnCompanyLogo !== null && $this->aOwnCompanyLogo->getId() !== $v) {
            $this->aOwnCompanyLogo = null;
        }

        return $this;
    } // setCompanyLogoId()

    /**
     * Set the value of [max_shipping_costs] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function setMaxShippingCosts($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->max_shipping_costs !== $v) {
            $this->max_shipping_costs = $v;
            $this->modifiedColumns[CompanyTableMap::COL_MAX_SHIPPING_COSTS] = true;
        }

        return $this;
    } // setMaxShippingCosts()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_default_supplier !== false) {
                return false;
            }

            if ($this->is_deleted !== false) {
                return false;
            }

            if ($this->has_invoice_address !== false) {
                return false;
            }

            if ($this->has_delivery_address !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CompanyTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CompanyTableMap::translateFieldName('IsDefaultSupplier', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_default_supplier = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CompanyTableMap::translateFieldName('CreatedOn', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_on = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CompanyTableMap::translateFieldName('ItemDeleted', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_deleted = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CompanyTableMap::translateFieldName('CompanyName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->company_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CompanyTableMap::translateFieldName('Slogan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->slogan = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CompanyTableMap::translateFieldName('ChamberOfCommerce', TableMap::TYPE_PHPNAME, $indexType)];
            $this->chamber_of_commerce = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CompanyTableMap::translateFieldName('VatNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vat_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CompanyTableMap::translateFieldName('LegalFormId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->legal_form_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CompanyTableMap::translateFieldName('Iban', TableMap::TYPE_PHPNAME, $indexType)];
            $this->iban = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : CompanyTableMap::translateFieldName('Bic', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bic = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : CompanyTableMap::translateFieldName('Phone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->phone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : CompanyTableMap::translateFieldName('Fax', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fax = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : CompanyTableMap::translateFieldName('Website', TableMap::TYPE_PHPNAME, $indexType)];
            $this->website = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : CompanyTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : CompanyTableMap::translateFieldName('GeneralStreet', TableMap::TYPE_PHPNAME, $indexType)];
            $this->general_street = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : CompanyTableMap::translateFieldName('GeneralNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->general_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : CompanyTableMap::translateFieldName('GeneralNumberAdd', TableMap::TYPE_PHPNAME, $indexType)];
            $this->general_number_add = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : CompanyTableMap::translateFieldName('GeneralPostal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->general_postal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : CompanyTableMap::translateFieldName('GeneralCity', TableMap::TYPE_PHPNAME, $indexType)];
            $this->general_city = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : CompanyTableMap::translateFieldName('GeneralPoBox', TableMap::TYPE_PHPNAME, $indexType)];
            $this->general_po_box = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : CompanyTableMap::translateFieldName('GeneralCountryId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->general_country_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : CompanyTableMap::translateFieldName('HasInvoiceAddress', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_invoice_address = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : CompanyTableMap::translateFieldName('InvoiceCompanyName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->invoice_company_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : CompanyTableMap::translateFieldName('InvoiceStreet', TableMap::TYPE_PHPNAME, $indexType)];
            $this->invoice_street = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : CompanyTableMap::translateFieldName('InvoiceNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->invoice_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : CompanyTableMap::translateFieldName('InvoiceNumberAdd', TableMap::TYPE_PHPNAME, $indexType)];
            $this->invoice_number_add = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : CompanyTableMap::translateFieldName('InvoicePostal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->invoice_postal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : CompanyTableMap::translateFieldName('InvoiceCity', TableMap::TYPE_PHPNAME, $indexType)];
            $this->invoice_city = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : CompanyTableMap::translateFieldName('InvoiceCountryId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->invoice_country_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : CompanyTableMap::translateFieldName('HasDeliveryAddress', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_delivery_address = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 31 + $startcol : CompanyTableMap::translateFieldName('DeliveryCompanyName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->delivery_company_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 32 + $startcol : CompanyTableMap::translateFieldName('DeliveryStreet', TableMap::TYPE_PHPNAME, $indexType)];
            $this->delivery_street = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 33 + $startcol : CompanyTableMap::translateFieldName('DeliveryNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->delivery_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 34 + $startcol : CompanyTableMap::translateFieldName('DeliveryNumberAdd', TableMap::TYPE_PHPNAME, $indexType)];
            $this->delivery_number_add = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 35 + $startcol : CompanyTableMap::translateFieldName('DeliveryPostal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->delivery_postal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 36 + $startcol : CompanyTableMap::translateFieldName('DeliveryCity', TableMap::TYPE_PHPNAME, $indexType)];
            $this->delivery_city = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 37 + $startcol : CompanyTableMap::translateFieldName('DeliveryCountryId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->delivery_country_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 38 + $startcol : CompanyTableMap::translateFieldName('CompanyLogoId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->company_logo_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 39 + $startcol : CompanyTableMap::translateFieldName('MaxShippingCosts', TableMap::TYPE_PHPNAME, $indexType)];
            $this->max_shipping_costs = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 40; // 40 = CompanyTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Company\\Company'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aLegalForm !== null && $this->legal_form_id !== $this->aLegalForm->getId()) {
            $this->aLegalForm = null;
        }
        if ($this->aGeneralCountry !== null && $this->general_country_id !== $this->aGeneralCountry->getId()) {
            $this->aGeneralCountry = null;
        }
        if ($this->aInvoiceCountry !== null && $this->invoice_country_id !== $this->aInvoiceCountry->getId()) {
            $this->aInvoiceCountry = null;
        }
        if ($this->aDeliveryCountry !== null && $this->delivery_country_id !== $this->aDeliveryCountry->getId()) {
            $this->aDeliveryCountry = null;
        }
        if ($this->aOwnCompanyLogo !== null && $this->company_logo_id !== $this->aOwnCompanyLogo->getId()) {
            $this->aOwnCompanyLogo = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CompanyTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCompanyQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aGeneralCountry = null;
            $this->aInvoiceCountry = null;
            $this->aDeliveryCountry = null;
            $this->aLegalForm = null;
            $this->aOwnCompanyLogo = null;
            $this->collCompanyLogosRelatedByOwnCompanyId = null;

            $this->collSites = null;

            $this->collCompanySettings = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Company::setDeleted()
     * @see Company::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompanyTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCompanyQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompanyTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CompanyTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aGeneralCountry !== null) {
                if ($this->aGeneralCountry->isModified() || $this->aGeneralCountry->isNew()) {
                    $affectedRows += $this->aGeneralCountry->save($con);
                }
                $this->setGeneralCountry($this->aGeneralCountry);
            }

            if ($this->aInvoiceCountry !== null) {
                if ($this->aInvoiceCountry->isModified() || $this->aInvoiceCountry->isNew()) {
                    $affectedRows += $this->aInvoiceCountry->save($con);
                }
                $this->setInvoiceCountry($this->aInvoiceCountry);
            }

            if ($this->aDeliveryCountry !== null) {
                if ($this->aDeliveryCountry->isModified() || $this->aDeliveryCountry->isNew()) {
                    $affectedRows += $this->aDeliveryCountry->save($con);
                }
                $this->setDeliveryCountry($this->aDeliveryCountry);
            }

            if ($this->aLegalForm !== null) {
                if ($this->aLegalForm->isModified() || $this->aLegalForm->isNew()) {
                    $affectedRows += $this->aLegalForm->save($con);
                }
                $this->setLegalForm($this->aLegalForm);
            }

            if ($this->aOwnCompanyLogo !== null) {
                if ($this->aOwnCompanyLogo->isModified() || $this->aOwnCompanyLogo->isNew()) {
                    $affectedRows += $this->aOwnCompanyLogo->save($con);
                }
                $this->setOwnCompanyLogo($this->aOwnCompanyLogo);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->companyLogosRelatedByOwnCompanyIdScheduledForDeletion !== null) {
                if (!$this->companyLogosRelatedByOwnCompanyIdScheduledForDeletion->isEmpty()) {
                    \Model\Company\CompanyLogoQuery::create()
                        ->filterByPrimaryKeys($this->companyLogosRelatedByOwnCompanyIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->companyLogosRelatedByOwnCompanyIdScheduledForDeletion = null;
                }
            }

            if ($this->collCompanyLogosRelatedByOwnCompanyId !== null) {
                foreach ($this->collCompanyLogosRelatedByOwnCompanyId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sitesScheduledForDeletion !== null) {
                if (!$this->sitesScheduledForDeletion->isEmpty()) {
                    foreach ($this->sitesScheduledForDeletion as $site) {
                        // need to save related object because we set the relation to null
                        $site->save($con);
                    }
                    $this->sitesScheduledForDeletion = null;
                }
            }

            if ($this->collSites !== null) {
                foreach ($this->collSites as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->companySettingsScheduledForDeletion !== null) {
                if (!$this->companySettingsScheduledForDeletion->isEmpty()) {
                    \Model\Company\CompanySettingQuery::create()
                        ->filterByPrimaryKeys($this->companySettingsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->companySettingsScheduledForDeletion = null;
                }
            }

            if ($this->collCompanySettings !== null) {
                foreach ($this->collCompanySettings as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CompanyTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CompanyTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CompanyTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_IS_DEFAULT_SUPPLIER)) {
            $modifiedColumns[':p' . $index++]  = 'is_default_supplier';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_CREATED_ON)) {
            $modifiedColumns[':p' . $index++]  = 'created_on';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_IS_DELETED)) {
            $modifiedColumns[':p' . $index++]  = 'is_deleted';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_COMPANY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'company_name';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_SLOGAN)) {
            $modifiedColumns[':p' . $index++]  = 'slogan';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_CHAMBER_OF_COMMERCE)) {
            $modifiedColumns[':p' . $index++]  = 'chamber_of_commerce';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_VAT_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'vat_number';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_LEGAL_FORM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'legal_form_id';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_IBAN)) {
            $modifiedColumns[':p' . $index++]  = 'iban';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_BIC)) {
            $modifiedColumns[':p' . $index++]  = 'bic';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_PHONE)) {
            $modifiedColumns[':p' . $index++]  = 'phone';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_FAX)) {
            $modifiedColumns[':p' . $index++]  = 'fax';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_WEBSITE)) {
            $modifiedColumns[':p' . $index++]  = 'website';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'email';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_STREET)) {
            $modifiedColumns[':p' . $index++]  = 'general_street';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'general_number';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_NUMBER_ADD)) {
            $modifiedColumns[':p' . $index++]  = 'general_number_add';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_POSTAL)) {
            $modifiedColumns[':p' . $index++]  = 'general_postal';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_CITY)) {
            $modifiedColumns[':p' . $index++]  = 'general_city';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_PO_BOX)) {
            $modifiedColumns[':p' . $index++]  = 'general_po_box';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_COUNTRY_ID)) {
            $modifiedColumns[':p' . $index++]  = 'general_country_id';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_HAS_INVOICE_ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = 'has_invoice_address';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_COMPANY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'invoice_company_name';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_STREET)) {
            $modifiedColumns[':p' . $index++]  = 'invoice_street';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'invoice_number';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_NUMBER_ADD)) {
            $modifiedColumns[':p' . $index++]  = 'invoice_number_add';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_POSTAL)) {
            $modifiedColumns[':p' . $index++]  = 'invoice_postal';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_CITY)) {
            $modifiedColumns[':p' . $index++]  = 'invoice_city';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_COUNTRY_ID)) {
            $modifiedColumns[':p' . $index++]  = 'invoice_country_id';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_HAS_DELIVERY_ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = 'has_delivery_address';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_COMPANY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'delivery_company_name';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_STREET)) {
            $modifiedColumns[':p' . $index++]  = 'delivery_street';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'delivery_number';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_NUMBER_ADD)) {
            $modifiedColumns[':p' . $index++]  = 'delivery_number_add';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_POSTAL)) {
            $modifiedColumns[':p' . $index++]  = 'delivery_postal';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_CITY)) {
            $modifiedColumns[':p' . $index++]  = 'delivery_city';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_COUNTRY_ID)) {
            $modifiedColumns[':p' . $index++]  = 'delivery_country_id';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_COMPANY_LOGO_ID)) {
            $modifiedColumns[':p' . $index++]  = 'company_logo_id';
        }
        if ($this->isColumnModified(CompanyTableMap::COL_MAX_SHIPPING_COSTS)) {
            $modifiedColumns[':p' . $index++]  = 'max_shipping_costs';
        }

        $sql = sprintf(
            'INSERT INTO own_company (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'is_default_supplier':
                        $stmt->bindValue($identifier, (int) $this->is_default_supplier, PDO::PARAM_INT);
                        break;
                    case 'created_on':
                        $stmt->bindValue($identifier, $this->created_on ? $this->created_on->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'is_deleted':
                        $stmt->bindValue($identifier, (int) $this->is_deleted, PDO::PARAM_INT);
                        break;
                    case 'company_name':
                        $stmt->bindValue($identifier, $this->company_name, PDO::PARAM_STR);
                        break;
                    case 'slogan':
                        $stmt->bindValue($identifier, $this->slogan, PDO::PARAM_STR);
                        break;
                    case 'chamber_of_commerce':
                        $stmt->bindValue($identifier, $this->chamber_of_commerce, PDO::PARAM_STR);
                        break;
                    case 'vat_number':
                        $stmt->bindValue($identifier, $this->vat_number, PDO::PARAM_STR);
                        break;
                    case 'legal_form_id':
                        $stmt->bindValue($identifier, $this->legal_form_id, PDO::PARAM_INT);
                        break;
                    case 'iban':
                        $stmt->bindValue($identifier, $this->iban, PDO::PARAM_STR);
                        break;
                    case 'bic':
                        $stmt->bindValue($identifier, $this->bic, PDO::PARAM_STR);
                        break;
                    case 'phone':
                        $stmt->bindValue($identifier, $this->phone, PDO::PARAM_STR);
                        break;
                    case 'fax':
                        $stmt->bindValue($identifier, $this->fax, PDO::PARAM_STR);
                        break;
                    case 'website':
                        $stmt->bindValue($identifier, $this->website, PDO::PARAM_STR);
                        break;
                    case 'email':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case 'general_street':
                        $stmt->bindValue($identifier, $this->general_street, PDO::PARAM_STR);
                        break;
                    case 'general_number':
                        $stmt->bindValue($identifier, $this->general_number, PDO::PARAM_STR);
                        break;
                    case 'general_number_add':
                        $stmt->bindValue($identifier, $this->general_number_add, PDO::PARAM_STR);
                        break;
                    case 'general_postal':
                        $stmt->bindValue($identifier, $this->general_postal, PDO::PARAM_STR);
                        break;
                    case 'general_city':
                        $stmt->bindValue($identifier, $this->general_city, PDO::PARAM_STR);
                        break;
                    case 'general_po_box':
                        $stmt->bindValue($identifier, $this->general_po_box, PDO::PARAM_STR);
                        break;
                    case 'general_country_id':
                        $stmt->bindValue($identifier, $this->general_country_id, PDO::PARAM_INT);
                        break;
                    case 'has_invoice_address':
                        $stmt->bindValue($identifier, (int) $this->has_invoice_address, PDO::PARAM_INT);
                        break;
                    case 'invoice_company_name':
                        $stmt->bindValue($identifier, $this->invoice_company_name, PDO::PARAM_STR);
                        break;
                    case 'invoice_street':
                        $stmt->bindValue($identifier, $this->invoice_street, PDO::PARAM_STR);
                        break;
                    case 'invoice_number':
                        $stmt->bindValue($identifier, $this->invoice_number, PDO::PARAM_STR);
                        break;
                    case 'invoice_number_add':
                        $stmt->bindValue($identifier, $this->invoice_number_add, PDO::PARAM_STR);
                        break;
                    case 'invoice_postal':
                        $stmt->bindValue($identifier, $this->invoice_postal, PDO::PARAM_STR);
                        break;
                    case 'invoice_city':
                        $stmt->bindValue($identifier, $this->invoice_city, PDO::PARAM_STR);
                        break;
                    case 'invoice_country_id':
                        $stmt->bindValue($identifier, $this->invoice_country_id, PDO::PARAM_INT);
                        break;
                    case 'has_delivery_address':
                        $stmt->bindValue($identifier, (int) $this->has_delivery_address, PDO::PARAM_INT);
                        break;
                    case 'delivery_company_name':
                        $stmt->bindValue($identifier, $this->delivery_company_name, PDO::PARAM_STR);
                        break;
                    case 'delivery_street':
                        $stmt->bindValue($identifier, $this->delivery_street, PDO::PARAM_STR);
                        break;
                    case 'delivery_number':
                        $stmt->bindValue($identifier, $this->delivery_number, PDO::PARAM_STR);
                        break;
                    case 'delivery_number_add':
                        $stmt->bindValue($identifier, $this->delivery_number_add, PDO::PARAM_STR);
                        break;
                    case 'delivery_postal':
                        $stmt->bindValue($identifier, $this->delivery_postal, PDO::PARAM_STR);
                        break;
                    case 'delivery_city':
                        $stmt->bindValue($identifier, $this->delivery_city, PDO::PARAM_STR);
                        break;
                    case 'delivery_country_id':
                        $stmt->bindValue($identifier, $this->delivery_country_id, PDO::PARAM_INT);
                        break;
                    case 'company_logo_id':
                        $stmt->bindValue($identifier, $this->company_logo_id, PDO::PARAM_INT);
                        break;
                    case 'max_shipping_costs':
                        $stmt->bindValue($identifier, $this->max_shipping_costs, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CompanyTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getIsDefaultSupplier();
                break;
            case 2:
                return $this->getCreatedOn();
                break;
            case 3:
                return $this->getItemDeleted();
                break;
            case 4:
                return $this->getCompanyName();
                break;
            case 5:
                return $this->getSlogan();
                break;
            case 6:
                return $this->getChamberOfCommerce();
                break;
            case 7:
                return $this->getVatNumber();
                break;
            case 8:
                return $this->getLegalFormId();
                break;
            case 9:
                return $this->getIban();
                break;
            case 10:
                return $this->getBic();
                break;
            case 11:
                return $this->getPhone();
                break;
            case 12:
                return $this->getFax();
                break;
            case 13:
                return $this->getWebsite();
                break;
            case 14:
                return $this->getEmail();
                break;
            case 15:
                return $this->getGeneralStreet();
                break;
            case 16:
                return $this->getGeneralNumber();
                break;
            case 17:
                return $this->getGeneralNumberAdd();
                break;
            case 18:
                return $this->getGeneralPostal();
                break;
            case 19:
                return $this->getGeneralCity();
                break;
            case 20:
                return $this->getGeneralPoBox();
                break;
            case 21:
                return $this->getGeneralCountryId();
                break;
            case 22:
                return $this->getHasInvoiceAddress();
                break;
            case 23:
                return $this->getInvoiceCompanyName();
                break;
            case 24:
                return $this->getInvoiceStreet();
                break;
            case 25:
                return $this->getInvoiceNumber();
                break;
            case 26:
                return $this->getInvoiceNumberAdd();
                break;
            case 27:
                return $this->getInvoicePostal();
                break;
            case 28:
                return $this->getInvoiceCity();
                break;
            case 29:
                return $this->getInvoiceCountryId();
                break;
            case 30:
                return $this->getHasDeliveryAddress();
                break;
            case 31:
                return $this->getDeliveryCompanyName();
                break;
            case 32:
                return $this->getDeliveryStreet();
                break;
            case 33:
                return $this->getDeliveryNumber();
                break;
            case 34:
                return $this->getDeliveryNumberAdd();
                break;
            case 35:
                return $this->getDeliveryPostal();
                break;
            case 36:
                return $this->getDeliveryCity();
                break;
            case 37:
                return $this->getDeliveryCountryId();
                break;
            case 38:
                return $this->getCompanyLogoId();
                break;
            case 39:
                return $this->getMaxShippingCosts();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Company'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Company'][$this->hashCode()] = true;
        $keys = CompanyTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getIsDefaultSupplier(),
            $keys[2] => $this->getCreatedOn(),
            $keys[3] => $this->getItemDeleted(),
            $keys[4] => $this->getCompanyName(),
            $keys[5] => $this->getSlogan(),
            $keys[6] => $this->getChamberOfCommerce(),
            $keys[7] => $this->getVatNumber(),
            $keys[8] => $this->getLegalFormId(),
            $keys[9] => $this->getIban(),
            $keys[10] => $this->getBic(),
            $keys[11] => $this->getPhone(),
            $keys[12] => $this->getFax(),
            $keys[13] => $this->getWebsite(),
            $keys[14] => $this->getEmail(),
            $keys[15] => $this->getGeneralStreet(),
            $keys[16] => $this->getGeneralNumber(),
            $keys[17] => $this->getGeneralNumberAdd(),
            $keys[18] => $this->getGeneralPostal(),
            $keys[19] => $this->getGeneralCity(),
            $keys[20] => $this->getGeneralPoBox(),
            $keys[21] => $this->getGeneralCountryId(),
            $keys[22] => $this->getHasInvoiceAddress(),
            $keys[23] => $this->getInvoiceCompanyName(),
            $keys[24] => $this->getInvoiceStreet(),
            $keys[25] => $this->getInvoiceNumber(),
            $keys[26] => $this->getInvoiceNumberAdd(),
            $keys[27] => $this->getInvoicePostal(),
            $keys[28] => $this->getInvoiceCity(),
            $keys[29] => $this->getInvoiceCountryId(),
            $keys[30] => $this->getHasDeliveryAddress(),
            $keys[31] => $this->getDeliveryCompanyName(),
            $keys[32] => $this->getDeliveryStreet(),
            $keys[33] => $this->getDeliveryNumber(),
            $keys[34] => $this->getDeliveryNumberAdd(),
            $keys[35] => $this->getDeliveryPostal(),
            $keys[36] => $this->getDeliveryCity(),
            $keys[37] => $this->getDeliveryCountryId(),
            $keys[38] => $this->getCompanyLogoId(),
            $keys[39] => $this->getMaxShippingCosts(),
        );
        if ($result[$keys[2]] instanceof \DateTimeInterface) {
            $result[$keys[2]] = $result[$keys[2]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aGeneralCountry) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'country';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_country';
                        break;
                    default:
                        $key = 'GeneralCountry';
                }

                $result[$key] = $this->aGeneralCountry->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aInvoiceCountry) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'country';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_country';
                        break;
                    default:
                        $key = 'InvoiceCountry';
                }

                $result[$key] = $this->aInvoiceCountry->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aDeliveryCountry) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'country';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_country';
                        break;
                    default:
                        $key = 'DeliveryCountry';
                }

                $result[$key] = $this->aDeliveryCountry->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aLegalForm) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'legalForm';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_legal_form';
                        break;
                    default:
                        $key = 'LegalForm';
                }

                $result[$key] = $this->aLegalForm->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aOwnCompanyLogo) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'companyLogo';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'own_company_logo';
                        break;
                    default:
                        $key = 'OwnCompanyLogo';
                }

                $result[$key] = $this->aOwnCompanyLogo->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCompanyLogosRelatedByOwnCompanyId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'companyLogos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'own_company_logos';
                        break;
                    default:
                        $key = 'CompanyLogos';
                }

                $result[$key] = $this->collCompanyLogosRelatedByOwnCompanyId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSites) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sites';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sites';
                        break;
                    default:
                        $key = 'Sites';
                }

                $result[$key] = $this->collSites->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCompanySettings) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'companySettings';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'own_company_settings';
                        break;
                    default:
                        $key = 'CompanySettings';
                }

                $result[$key] = $this->collCompanySettings->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Company\Company
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CompanyTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Company\Company
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setIsDefaultSupplier($value);
                break;
            case 2:
                $this->setCreatedOn($value);
                break;
            case 3:
                $this->setItemDeleted($value);
                break;
            case 4:
                $this->setCompanyName($value);
                break;
            case 5:
                $this->setSlogan($value);
                break;
            case 6:
                $this->setChamberOfCommerce($value);
                break;
            case 7:
                $this->setVatNumber($value);
                break;
            case 8:
                $this->setLegalFormId($value);
                break;
            case 9:
                $this->setIban($value);
                break;
            case 10:
                $this->setBic($value);
                break;
            case 11:
                $this->setPhone($value);
                break;
            case 12:
                $this->setFax($value);
                break;
            case 13:
                $this->setWebsite($value);
                break;
            case 14:
                $this->setEmail($value);
                break;
            case 15:
                $this->setGeneralStreet($value);
                break;
            case 16:
                $this->setGeneralNumber($value);
                break;
            case 17:
                $this->setGeneralNumberAdd($value);
                break;
            case 18:
                $this->setGeneralPostal($value);
                break;
            case 19:
                $this->setGeneralCity($value);
                break;
            case 20:
                $this->setGeneralPoBox($value);
                break;
            case 21:
                $this->setGeneralCountryId($value);
                break;
            case 22:
                $this->setHasInvoiceAddress($value);
                break;
            case 23:
                $this->setInvoiceCompanyName($value);
                break;
            case 24:
                $this->setInvoiceStreet($value);
                break;
            case 25:
                $this->setInvoiceNumber($value);
                break;
            case 26:
                $this->setInvoiceNumberAdd($value);
                break;
            case 27:
                $this->setInvoicePostal($value);
                break;
            case 28:
                $this->setInvoiceCity($value);
                break;
            case 29:
                $this->setInvoiceCountryId($value);
                break;
            case 30:
                $this->setHasDeliveryAddress($value);
                break;
            case 31:
                $this->setDeliveryCompanyName($value);
                break;
            case 32:
                $this->setDeliveryStreet($value);
                break;
            case 33:
                $this->setDeliveryNumber($value);
                break;
            case 34:
                $this->setDeliveryNumberAdd($value);
                break;
            case 35:
                $this->setDeliveryPostal($value);
                break;
            case 36:
                $this->setDeliveryCity($value);
                break;
            case 37:
                $this->setDeliveryCountryId($value);
                break;
            case 38:
                $this->setCompanyLogoId($value);
                break;
            case 39:
                $this->setMaxShippingCosts($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CompanyTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setIsDefaultSupplier($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCreatedOn($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setItemDeleted($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setCompanyName($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setSlogan($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setChamberOfCommerce($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setVatNumber($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setLegalFormId($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setIban($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setBic($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setPhone($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setFax($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setWebsite($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setEmail($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setGeneralStreet($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setGeneralNumber($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setGeneralNumberAdd($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setGeneralPostal($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setGeneralCity($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setGeneralPoBox($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setGeneralCountryId($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setHasInvoiceAddress($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setInvoiceCompanyName($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setInvoiceStreet($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setInvoiceNumber($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setInvoiceNumberAdd($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setInvoicePostal($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setInvoiceCity($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setInvoiceCountryId($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setHasDeliveryAddress($arr[$keys[30]]);
        }
        if (array_key_exists($keys[31], $arr)) {
            $this->setDeliveryCompanyName($arr[$keys[31]]);
        }
        if (array_key_exists($keys[32], $arr)) {
            $this->setDeliveryStreet($arr[$keys[32]]);
        }
        if (array_key_exists($keys[33], $arr)) {
            $this->setDeliveryNumber($arr[$keys[33]]);
        }
        if (array_key_exists($keys[34], $arr)) {
            $this->setDeliveryNumberAdd($arr[$keys[34]]);
        }
        if (array_key_exists($keys[35], $arr)) {
            $this->setDeliveryPostal($arr[$keys[35]]);
        }
        if (array_key_exists($keys[36], $arr)) {
            $this->setDeliveryCity($arr[$keys[36]]);
        }
        if (array_key_exists($keys[37], $arr)) {
            $this->setDeliveryCountryId($arr[$keys[37]]);
        }
        if (array_key_exists($keys[38], $arr)) {
            $this->setCompanyLogoId($arr[$keys[38]]);
        }
        if (array_key_exists($keys[39], $arr)) {
            $this->setMaxShippingCosts($arr[$keys[39]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Company\Company The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CompanyTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CompanyTableMap::COL_ID)) {
            $criteria->add(CompanyTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_IS_DEFAULT_SUPPLIER)) {
            $criteria->add(CompanyTableMap::COL_IS_DEFAULT_SUPPLIER, $this->is_default_supplier);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_CREATED_ON)) {
            $criteria->add(CompanyTableMap::COL_CREATED_ON, $this->created_on);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_IS_DELETED)) {
            $criteria->add(CompanyTableMap::COL_IS_DELETED, $this->is_deleted);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_COMPANY_NAME)) {
            $criteria->add(CompanyTableMap::COL_COMPANY_NAME, $this->company_name);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_SLOGAN)) {
            $criteria->add(CompanyTableMap::COL_SLOGAN, $this->slogan);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_CHAMBER_OF_COMMERCE)) {
            $criteria->add(CompanyTableMap::COL_CHAMBER_OF_COMMERCE, $this->chamber_of_commerce);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_VAT_NUMBER)) {
            $criteria->add(CompanyTableMap::COL_VAT_NUMBER, $this->vat_number);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_LEGAL_FORM_ID)) {
            $criteria->add(CompanyTableMap::COL_LEGAL_FORM_ID, $this->legal_form_id);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_IBAN)) {
            $criteria->add(CompanyTableMap::COL_IBAN, $this->iban);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_BIC)) {
            $criteria->add(CompanyTableMap::COL_BIC, $this->bic);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_PHONE)) {
            $criteria->add(CompanyTableMap::COL_PHONE, $this->phone);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_FAX)) {
            $criteria->add(CompanyTableMap::COL_FAX, $this->fax);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_WEBSITE)) {
            $criteria->add(CompanyTableMap::COL_WEBSITE, $this->website);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_EMAIL)) {
            $criteria->add(CompanyTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_STREET)) {
            $criteria->add(CompanyTableMap::COL_GENERAL_STREET, $this->general_street);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_NUMBER)) {
            $criteria->add(CompanyTableMap::COL_GENERAL_NUMBER, $this->general_number);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_NUMBER_ADD)) {
            $criteria->add(CompanyTableMap::COL_GENERAL_NUMBER_ADD, $this->general_number_add);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_POSTAL)) {
            $criteria->add(CompanyTableMap::COL_GENERAL_POSTAL, $this->general_postal);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_CITY)) {
            $criteria->add(CompanyTableMap::COL_GENERAL_CITY, $this->general_city);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_PO_BOX)) {
            $criteria->add(CompanyTableMap::COL_GENERAL_PO_BOX, $this->general_po_box);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_GENERAL_COUNTRY_ID)) {
            $criteria->add(CompanyTableMap::COL_GENERAL_COUNTRY_ID, $this->general_country_id);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_HAS_INVOICE_ADDRESS)) {
            $criteria->add(CompanyTableMap::COL_HAS_INVOICE_ADDRESS, $this->has_invoice_address);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_COMPANY_NAME)) {
            $criteria->add(CompanyTableMap::COL_INVOICE_COMPANY_NAME, $this->invoice_company_name);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_STREET)) {
            $criteria->add(CompanyTableMap::COL_INVOICE_STREET, $this->invoice_street);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_NUMBER)) {
            $criteria->add(CompanyTableMap::COL_INVOICE_NUMBER, $this->invoice_number);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_NUMBER_ADD)) {
            $criteria->add(CompanyTableMap::COL_INVOICE_NUMBER_ADD, $this->invoice_number_add);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_POSTAL)) {
            $criteria->add(CompanyTableMap::COL_INVOICE_POSTAL, $this->invoice_postal);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_CITY)) {
            $criteria->add(CompanyTableMap::COL_INVOICE_CITY, $this->invoice_city);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_INVOICE_COUNTRY_ID)) {
            $criteria->add(CompanyTableMap::COL_INVOICE_COUNTRY_ID, $this->invoice_country_id);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_HAS_DELIVERY_ADDRESS)) {
            $criteria->add(CompanyTableMap::COL_HAS_DELIVERY_ADDRESS, $this->has_delivery_address);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_COMPANY_NAME)) {
            $criteria->add(CompanyTableMap::COL_DELIVERY_COMPANY_NAME, $this->delivery_company_name);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_STREET)) {
            $criteria->add(CompanyTableMap::COL_DELIVERY_STREET, $this->delivery_street);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_NUMBER)) {
            $criteria->add(CompanyTableMap::COL_DELIVERY_NUMBER, $this->delivery_number);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_NUMBER_ADD)) {
            $criteria->add(CompanyTableMap::COL_DELIVERY_NUMBER_ADD, $this->delivery_number_add);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_POSTAL)) {
            $criteria->add(CompanyTableMap::COL_DELIVERY_POSTAL, $this->delivery_postal);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_CITY)) {
            $criteria->add(CompanyTableMap::COL_DELIVERY_CITY, $this->delivery_city);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_DELIVERY_COUNTRY_ID)) {
            $criteria->add(CompanyTableMap::COL_DELIVERY_COUNTRY_ID, $this->delivery_country_id);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_COMPANY_LOGO_ID)) {
            $criteria->add(CompanyTableMap::COL_COMPANY_LOGO_ID, $this->company_logo_id);
        }
        if ($this->isColumnModified(CompanyTableMap::COL_MAX_SHIPPING_COSTS)) {
            $criteria->add(CompanyTableMap::COL_MAX_SHIPPING_COSTS, $this->max_shipping_costs);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCompanyQuery::create();
        $criteria->add(CompanyTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Company\Company (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIsDefaultSupplier($this->getIsDefaultSupplier());
        $copyObj->setCreatedOn($this->getCreatedOn());
        $copyObj->setItemDeleted($this->getItemDeleted());
        $copyObj->setCompanyName($this->getCompanyName());
        $copyObj->setSlogan($this->getSlogan());
        $copyObj->setChamberOfCommerce($this->getChamberOfCommerce());
        $copyObj->setVatNumber($this->getVatNumber());
        $copyObj->setLegalFormId($this->getLegalFormId());
        $copyObj->setIban($this->getIban());
        $copyObj->setBic($this->getBic());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setFax($this->getFax());
        $copyObj->setWebsite($this->getWebsite());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setGeneralStreet($this->getGeneralStreet());
        $copyObj->setGeneralNumber($this->getGeneralNumber());
        $copyObj->setGeneralNumberAdd($this->getGeneralNumberAdd());
        $copyObj->setGeneralPostal($this->getGeneralPostal());
        $copyObj->setGeneralCity($this->getGeneralCity());
        $copyObj->setGeneralPoBox($this->getGeneralPoBox());
        $copyObj->setGeneralCountryId($this->getGeneralCountryId());
        $copyObj->setHasInvoiceAddress($this->getHasInvoiceAddress());
        $copyObj->setInvoiceCompanyName($this->getInvoiceCompanyName());
        $copyObj->setInvoiceStreet($this->getInvoiceStreet());
        $copyObj->setInvoiceNumber($this->getInvoiceNumber());
        $copyObj->setInvoiceNumberAdd($this->getInvoiceNumberAdd());
        $copyObj->setInvoicePostal($this->getInvoicePostal());
        $copyObj->setInvoiceCity($this->getInvoiceCity());
        $copyObj->setInvoiceCountryId($this->getInvoiceCountryId());
        $copyObj->setHasDeliveryAddress($this->getHasDeliveryAddress());
        $copyObj->setDeliveryCompanyName($this->getDeliveryCompanyName());
        $copyObj->setDeliveryStreet($this->getDeliveryStreet());
        $copyObj->setDeliveryNumber($this->getDeliveryNumber());
        $copyObj->setDeliveryNumberAdd($this->getDeliveryNumberAdd());
        $copyObj->setDeliveryPostal($this->getDeliveryPostal());
        $copyObj->setDeliveryCity($this->getDeliveryCity());
        $copyObj->setDeliveryCountryId($this->getDeliveryCountryId());
        $copyObj->setCompanyLogoId($this->getCompanyLogoId());
        $copyObj->setMaxShippingCosts($this->getMaxShippingCosts());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCompanyLogosRelatedByOwnCompanyId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCompanyLogoRelatedByOwnCompanyId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSites() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSite($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCompanySettings() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCompanySetting($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Company\Company Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a Country object.
     *
     * @param  Country|null $v
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     * @throws PropelException
     */
    public function setGeneralCountry(Country $v = null)
    {
        if ($v === null) {
            $this->setGeneralCountryId(NULL);
        } else {
            $this->setGeneralCountryId($v->getId());
        }

        $this->aGeneralCountry = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Country object, it will not be re-added.
        if ($v !== null) {
            $v->addCompanyRelatedByGeneralCountryId($this);
        }


        return $this;
    }


    /**
     * Get the associated Country object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Country|null The associated Country object.
     * @throws PropelException
     */
    public function getGeneralCountry(ConnectionInterface $con = null)
    {
        if ($this->aGeneralCountry === null && ($this->general_country_id != 0)) {
            $this->aGeneralCountry = CountryQuery::create()->findPk($this->general_country_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aGeneralCountry->addCompaniesRelatedByGeneralCountryId($this);
             */
        }

        return $this->aGeneralCountry;
    }

    /**
     * Declares an association between this object and a Country object.
     *
     * @param  Country|null $v
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     * @throws PropelException
     */
    public function setInvoiceCountry(Country $v = null)
    {
        if ($v === null) {
            $this->setInvoiceCountryId(NULL);
        } else {
            $this->setInvoiceCountryId($v->getId());
        }

        $this->aInvoiceCountry = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Country object, it will not be re-added.
        if ($v !== null) {
            $v->addCompanyRelatedByInvoiceCountryId($this);
        }


        return $this;
    }


    /**
     * Get the associated Country object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Country|null The associated Country object.
     * @throws PropelException
     */
    public function getInvoiceCountry(ConnectionInterface $con = null)
    {
        if ($this->aInvoiceCountry === null && ($this->invoice_country_id != 0)) {
            $this->aInvoiceCountry = CountryQuery::create()->findPk($this->invoice_country_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aInvoiceCountry->addCompaniesRelatedByInvoiceCountryId($this);
             */
        }

        return $this->aInvoiceCountry;
    }

    /**
     * Declares an association between this object and a Country object.
     *
     * @param  Country|null $v
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     * @throws PropelException
     */
    public function setDeliveryCountry(Country $v = null)
    {
        if ($v === null) {
            $this->setDeliveryCountryId(NULL);
        } else {
            $this->setDeliveryCountryId($v->getId());
        }

        $this->aDeliveryCountry = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Country object, it will not be re-added.
        if ($v !== null) {
            $v->addCompanyRelatedByDeliveryCountryId($this);
        }


        return $this;
    }


    /**
     * Get the associated Country object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Country|null The associated Country object.
     * @throws PropelException
     */
    public function getDeliveryCountry(ConnectionInterface $con = null)
    {
        if ($this->aDeliveryCountry === null && ($this->delivery_country_id != 0)) {
            $this->aDeliveryCountry = CountryQuery::create()->findPk($this->delivery_country_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aDeliveryCountry->addCompaniesRelatedByDeliveryCountryId($this);
             */
        }

        return $this->aDeliveryCountry;
    }

    /**
     * Declares an association between this object and a LegalForm object.
     *
     * @param  LegalForm|null $v
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLegalForm(LegalForm $v = null)
    {
        if ($v === null) {
            $this->setLegalFormId(NULL);
        } else {
            $this->setLegalFormId($v->getId());
        }

        $this->aLegalForm = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the LegalForm object, it will not be re-added.
        if ($v !== null) {
            $v->addCompany($this);
        }


        return $this;
    }


    /**
     * Get the associated LegalForm object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return LegalForm|null The associated LegalForm object.
     * @throws PropelException
     */
    public function getLegalForm(ConnectionInterface $con = null)
    {
        if ($this->aLegalForm === null && ($this->legal_form_id != 0)) {
            $this->aLegalForm = LegalFormQuery::create()->findPk($this->legal_form_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLegalForm->addCompanies($this);
             */
        }

        return $this->aLegalForm;
    }

    /**
     * Declares an association between this object and a ChildCompanyLogo object.
     *
     * @param  ChildCompanyLogo|null $v
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     * @throws PropelException
     */
    public function setOwnCompanyLogo(ChildCompanyLogo $v = null)
    {
        if ($v === null) {
            $this->setCompanyLogoId(NULL);
        } else {
            $this->setCompanyLogoId($v->getId());
        }

        $this->aOwnCompanyLogo = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCompanyLogo object, it will not be re-added.
        if ($v !== null) {
            $v->addCompanyRelatedByCompanyLogoId($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCompanyLogo object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCompanyLogo|null The associated ChildCompanyLogo object.
     * @throws PropelException
     */
    public function getOwnCompanyLogo(ConnectionInterface $con = null)
    {
        if ($this->aOwnCompanyLogo === null && ($this->company_logo_id != 0)) {
            $this->aOwnCompanyLogo = ChildCompanyLogoQuery::create()->findPk($this->company_logo_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aOwnCompanyLogo->addCompaniesRelatedByCompanyLogoId($this);
             */
        }

        return $this->aOwnCompanyLogo;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CompanyLogoRelatedByOwnCompanyId' === $relationName) {
            $this->initCompanyLogosRelatedByOwnCompanyId();
            return;
        }
        if ('Site' === $relationName) {
            $this->initSites();
            return;
        }
        if ('CompanySetting' === $relationName) {
            $this->initCompanySettings();
            return;
        }
    }

    /**
     * Clears out the collCompanyLogosRelatedByOwnCompanyId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCompanyLogosRelatedByOwnCompanyId()
     */
    public function clearCompanyLogosRelatedByOwnCompanyId()
    {
        $this->collCompanyLogosRelatedByOwnCompanyId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCompanyLogosRelatedByOwnCompanyId collection loaded partially.
     */
    public function resetPartialCompanyLogosRelatedByOwnCompanyId($v = true)
    {
        $this->collCompanyLogosRelatedByOwnCompanyIdPartial = $v;
    }

    /**
     * Initializes the collCompanyLogosRelatedByOwnCompanyId collection.
     *
     * By default this just sets the collCompanyLogosRelatedByOwnCompanyId collection to an empty array (like clearcollCompanyLogosRelatedByOwnCompanyId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCompanyLogosRelatedByOwnCompanyId($overrideExisting = true)
    {
        if (null !== $this->collCompanyLogosRelatedByOwnCompanyId && !$overrideExisting) {
            return;
        }

        $collectionClassName = CompanyLogoTableMap::getTableMap()->getCollectionClassName();

        $this->collCompanyLogosRelatedByOwnCompanyId = new $collectionClassName;
        $this->collCompanyLogosRelatedByOwnCompanyId->setModel('\Model\Company\CompanyLogo');
    }

    /**
     * Gets an array of ChildCompanyLogo objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompany is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCompanyLogo[] List of ChildCompanyLogo objects
     * @throws PropelException
     */
    public function getCompanyLogosRelatedByOwnCompanyId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCompanyLogosRelatedByOwnCompanyIdPartial && !$this->isNew();
        if (null === $this->collCompanyLogosRelatedByOwnCompanyId || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCompanyLogosRelatedByOwnCompanyId) {
                    $this->initCompanyLogosRelatedByOwnCompanyId();
                } else {
                    $collectionClassName = CompanyLogoTableMap::getTableMap()->getCollectionClassName();

                    $collCompanyLogosRelatedByOwnCompanyId = new $collectionClassName;
                    $collCompanyLogosRelatedByOwnCompanyId->setModel('\Model\Company\CompanyLogo');

                    return $collCompanyLogosRelatedByOwnCompanyId;
                }
            } else {
                $collCompanyLogosRelatedByOwnCompanyId = ChildCompanyLogoQuery::create(null, $criteria)
                    ->filterByOwnCompany($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCompanyLogosRelatedByOwnCompanyIdPartial && count($collCompanyLogosRelatedByOwnCompanyId)) {
                        $this->initCompanyLogosRelatedByOwnCompanyId(false);

                        foreach ($collCompanyLogosRelatedByOwnCompanyId as $obj) {
                            if (false == $this->collCompanyLogosRelatedByOwnCompanyId->contains($obj)) {
                                $this->collCompanyLogosRelatedByOwnCompanyId->append($obj);
                            }
                        }

                        $this->collCompanyLogosRelatedByOwnCompanyIdPartial = true;
                    }

                    return $collCompanyLogosRelatedByOwnCompanyId;
                }

                if ($partial && $this->collCompanyLogosRelatedByOwnCompanyId) {
                    foreach ($this->collCompanyLogosRelatedByOwnCompanyId as $obj) {
                        if ($obj->isNew()) {
                            $collCompanyLogosRelatedByOwnCompanyId[] = $obj;
                        }
                    }
                }

                $this->collCompanyLogosRelatedByOwnCompanyId = $collCompanyLogosRelatedByOwnCompanyId;
                $this->collCompanyLogosRelatedByOwnCompanyIdPartial = false;
            }
        }

        return $this->collCompanyLogosRelatedByOwnCompanyId;
    }

    /**
     * Sets a collection of ChildCompanyLogo objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $companyLogosRelatedByOwnCompanyId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompany The current object (for fluent API support)
     */
    public function setCompanyLogosRelatedByOwnCompanyId(Collection $companyLogosRelatedByOwnCompanyId, ConnectionInterface $con = null)
    {
        /** @var ChildCompanyLogo[] $companyLogosRelatedByOwnCompanyIdToDelete */
        $companyLogosRelatedByOwnCompanyIdToDelete = $this->getCompanyLogosRelatedByOwnCompanyId(new Criteria(), $con)->diff($companyLogosRelatedByOwnCompanyId);


        $this->companyLogosRelatedByOwnCompanyIdScheduledForDeletion = $companyLogosRelatedByOwnCompanyIdToDelete;

        foreach ($companyLogosRelatedByOwnCompanyIdToDelete as $companyLogoRelatedByOwnCompanyIdRemoved) {
            $companyLogoRelatedByOwnCompanyIdRemoved->setOwnCompany(null);
        }

        $this->collCompanyLogosRelatedByOwnCompanyId = null;
        foreach ($companyLogosRelatedByOwnCompanyId as $companyLogoRelatedByOwnCompanyId) {
            $this->addCompanyLogoRelatedByOwnCompanyId($companyLogoRelatedByOwnCompanyId);
        }

        $this->collCompanyLogosRelatedByOwnCompanyId = $companyLogosRelatedByOwnCompanyId;
        $this->collCompanyLogosRelatedByOwnCompanyIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CompanyLogo objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CompanyLogo objects.
     * @throws PropelException
     */
    public function countCompanyLogosRelatedByOwnCompanyId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCompanyLogosRelatedByOwnCompanyIdPartial && !$this->isNew();
        if (null === $this->collCompanyLogosRelatedByOwnCompanyId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCompanyLogosRelatedByOwnCompanyId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCompanyLogosRelatedByOwnCompanyId());
            }

            $query = ChildCompanyLogoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOwnCompany($this)
                ->count($con);
        }

        return count($this->collCompanyLogosRelatedByOwnCompanyId);
    }

    /**
     * Method called to associate a ChildCompanyLogo object to this object
     * through the ChildCompanyLogo foreign key attribute.
     *
     * @param  ChildCompanyLogo $l ChildCompanyLogo
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function addCompanyLogoRelatedByOwnCompanyId(ChildCompanyLogo $l)
    {
        if ($this->collCompanyLogosRelatedByOwnCompanyId === null) {
            $this->initCompanyLogosRelatedByOwnCompanyId();
            $this->collCompanyLogosRelatedByOwnCompanyIdPartial = true;
        }

        if (!$this->collCompanyLogosRelatedByOwnCompanyId->contains($l)) {
            $this->doAddCompanyLogoRelatedByOwnCompanyId($l);

            if ($this->companyLogosRelatedByOwnCompanyIdScheduledForDeletion and $this->companyLogosRelatedByOwnCompanyIdScheduledForDeletion->contains($l)) {
                $this->companyLogosRelatedByOwnCompanyIdScheduledForDeletion->remove($this->companyLogosRelatedByOwnCompanyIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCompanyLogo $companyLogoRelatedByOwnCompanyId The ChildCompanyLogo object to add.
     */
    protected function doAddCompanyLogoRelatedByOwnCompanyId(ChildCompanyLogo $companyLogoRelatedByOwnCompanyId)
    {
        $this->collCompanyLogosRelatedByOwnCompanyId[]= $companyLogoRelatedByOwnCompanyId;
        $companyLogoRelatedByOwnCompanyId->setOwnCompany($this);
    }

    /**
     * @param  ChildCompanyLogo $companyLogoRelatedByOwnCompanyId The ChildCompanyLogo object to remove.
     * @return $this|ChildCompany The current object (for fluent API support)
     */
    public function removeCompanyLogoRelatedByOwnCompanyId(ChildCompanyLogo $companyLogoRelatedByOwnCompanyId)
    {
        if ($this->getCompanyLogosRelatedByOwnCompanyId()->contains($companyLogoRelatedByOwnCompanyId)) {
            $pos = $this->collCompanyLogosRelatedByOwnCompanyId->search($companyLogoRelatedByOwnCompanyId);
            $this->collCompanyLogosRelatedByOwnCompanyId->remove($pos);
            if (null === $this->companyLogosRelatedByOwnCompanyIdScheduledForDeletion) {
                $this->companyLogosRelatedByOwnCompanyIdScheduledForDeletion = clone $this->collCompanyLogosRelatedByOwnCompanyId;
                $this->companyLogosRelatedByOwnCompanyIdScheduledForDeletion->clear();
            }
            $this->companyLogosRelatedByOwnCompanyIdScheduledForDeletion[]= clone $companyLogoRelatedByOwnCompanyId;
            $companyLogoRelatedByOwnCompanyId->setOwnCompany(null);
        }

        return $this;
    }

    /**
     * Clears out the collSites collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSites()
     */
    public function clearSites()
    {
        $this->collSites = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSites collection loaded partially.
     */
    public function resetPartialSites($v = true)
    {
        $this->collSitesPartial = $v;
    }

    /**
     * Initializes the collSites collection.
     *
     * By default this just sets the collSites collection to an empty array (like clearcollSites());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSites($overrideExisting = true)
    {
        if (null !== $this->collSites && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteTableMap::getTableMap()->getCollectionClassName();

        $this->collSites = new $collectionClassName;
        $this->collSites->setModel('\Model\Cms\Site');
    }

    /**
     * Gets an array of Site objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompany is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Site[] List of Site objects
     * @throws PropelException
     */
    public function getSites(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSitesPartial && !$this->isNew();
        if (null === $this->collSites || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSites) {
                    $this->initSites();
                } else {
                    $collectionClassName = SiteTableMap::getTableMap()->getCollectionClassName();

                    $collSites = new $collectionClassName;
                    $collSites->setModel('\Model\Cms\Site');

                    return $collSites;
                }
            } else {
                $collSites = SiteQuery::create(null, $criteria)
                    ->filterByOwnCompany($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSitesPartial && count($collSites)) {
                        $this->initSites(false);

                        foreach ($collSites as $obj) {
                            if (false == $this->collSites->contains($obj)) {
                                $this->collSites->append($obj);
                            }
                        }

                        $this->collSitesPartial = true;
                    }

                    return $collSites;
                }

                if ($partial && $this->collSites) {
                    foreach ($this->collSites as $obj) {
                        if ($obj->isNew()) {
                            $collSites[] = $obj;
                        }
                    }
                }

                $this->collSites = $collSites;
                $this->collSitesPartial = false;
            }
        }

        return $this->collSites;
    }

    /**
     * Sets a collection of Site objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sites A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompany The current object (for fluent API support)
     */
    public function setSites(Collection $sites, ConnectionInterface $con = null)
    {
        /** @var Site[] $sitesToDelete */
        $sitesToDelete = $this->getSites(new Criteria(), $con)->diff($sites);


        $this->sitesScheduledForDeletion = $sitesToDelete;

        foreach ($sitesToDelete as $siteRemoved) {
            $siteRemoved->setOwnCompany(null);
        }

        $this->collSites = null;
        foreach ($sites as $site) {
            $this->addSite($site);
        }

        $this->collSites = $sites;
        $this->collSitesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSite objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSite objects.
     * @throws PropelException
     */
    public function countSites(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSitesPartial && !$this->isNew();
        if (null === $this->collSites || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSites) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSites());
            }

            $query = SiteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOwnCompany($this)
                ->count($con);
        }

        return count($this->collSites);
    }

    /**
     * Method called to associate a Site object to this object
     * through the Site foreign key attribute.
     *
     * @param  Site $l Site
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function addSite(Site $l)
    {
        if ($this->collSites === null) {
            $this->initSites();
            $this->collSitesPartial = true;
        }

        if (!$this->collSites->contains($l)) {
            $this->doAddSite($l);

            if ($this->sitesScheduledForDeletion and $this->sitesScheduledForDeletion->contains($l)) {
                $this->sitesScheduledForDeletion->remove($this->sitesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Site $site The Site object to add.
     */
    protected function doAddSite(Site $site)
    {
        $this->collSites[]= $site;
        $site->setOwnCompany($this);
    }

    /**
     * @param  Site $site The Site object to remove.
     * @return $this|ChildCompany The current object (for fluent API support)
     */
    public function removeSite(Site $site)
    {
        if ($this->getSites()->contains($site)) {
            $pos = $this->collSites->search($site);
            $this->collSites->remove($pos);
            if (null === $this->sitesScheduledForDeletion) {
                $this->sitesScheduledForDeletion = clone $this->collSites;
                $this->sitesScheduledForDeletion->clear();
            }
            $this->sitesScheduledForDeletion[]= $site;
            $site->setOwnCompany(null);
        }

        return $this;
    }

    /**
     * Clears out the collCompanySettings collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCompanySettings()
     */
    public function clearCompanySettings()
    {
        $this->collCompanySettings = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCompanySettings collection loaded partially.
     */
    public function resetPartialCompanySettings($v = true)
    {
        $this->collCompanySettingsPartial = $v;
    }

    /**
     * Initializes the collCompanySettings collection.
     *
     * By default this just sets the collCompanySettings collection to an empty array (like clearcollCompanySettings());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCompanySettings($overrideExisting = true)
    {
        if (null !== $this->collCompanySettings && !$overrideExisting) {
            return;
        }

        $collectionClassName = CompanySettingTableMap::getTableMap()->getCollectionClassName();

        $this->collCompanySettings = new $collectionClassName;
        $this->collCompanySettings->setModel('\Model\Company\CompanySetting');
    }

    /**
     * Gets an array of ChildCompanySetting objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCompany is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCompanySetting[] List of ChildCompanySetting objects
     * @throws PropelException
     */
    public function getCompanySettings(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCompanySettingsPartial && !$this->isNew();
        if (null === $this->collCompanySettings || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCompanySettings) {
                    $this->initCompanySettings();
                } else {
                    $collectionClassName = CompanySettingTableMap::getTableMap()->getCollectionClassName();

                    $collCompanySettings = new $collectionClassName;
                    $collCompanySettings->setModel('\Model\Company\CompanySetting');

                    return $collCompanySettings;
                }
            } else {
                $collCompanySettings = ChildCompanySettingQuery::create(null, $criteria)
                    ->filterByCompanySetting($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCompanySettingsPartial && count($collCompanySettings)) {
                        $this->initCompanySettings(false);

                        foreach ($collCompanySettings as $obj) {
                            if (false == $this->collCompanySettings->contains($obj)) {
                                $this->collCompanySettings->append($obj);
                            }
                        }

                        $this->collCompanySettingsPartial = true;
                    }

                    return $collCompanySettings;
                }

                if ($partial && $this->collCompanySettings) {
                    foreach ($this->collCompanySettings as $obj) {
                        if ($obj->isNew()) {
                            $collCompanySettings[] = $obj;
                        }
                    }
                }

                $this->collCompanySettings = $collCompanySettings;
                $this->collCompanySettingsPartial = false;
            }
        }

        return $this->collCompanySettings;
    }

    /**
     * Sets a collection of ChildCompanySetting objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $companySettings A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCompany The current object (for fluent API support)
     */
    public function setCompanySettings(Collection $companySettings, ConnectionInterface $con = null)
    {
        /** @var ChildCompanySetting[] $companySettingsToDelete */
        $companySettingsToDelete = $this->getCompanySettings(new Criteria(), $con)->diff($companySettings);


        $this->companySettingsScheduledForDeletion = $companySettingsToDelete;

        foreach ($companySettingsToDelete as $companySettingRemoved) {
            $companySettingRemoved->setCompanySetting(null);
        }

        $this->collCompanySettings = null;
        foreach ($companySettings as $companySetting) {
            $this->addCompanySetting($companySetting);
        }

        $this->collCompanySettings = $companySettings;
        $this->collCompanySettingsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CompanySetting objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CompanySetting objects.
     * @throws PropelException
     */
    public function countCompanySettings(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCompanySettingsPartial && !$this->isNew();
        if (null === $this->collCompanySettings || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCompanySettings) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCompanySettings());
            }

            $query = ChildCompanySettingQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCompanySetting($this)
                ->count($con);
        }

        return count($this->collCompanySettings);
    }

    /**
     * Method called to associate a ChildCompanySetting object to this object
     * through the ChildCompanySetting foreign key attribute.
     *
     * @param  ChildCompanySetting $l ChildCompanySetting
     * @return $this|\Model\Company\Company The current object (for fluent API support)
     */
    public function addCompanySetting(ChildCompanySetting $l)
    {
        if ($this->collCompanySettings === null) {
            $this->initCompanySettings();
            $this->collCompanySettingsPartial = true;
        }

        if (!$this->collCompanySettings->contains($l)) {
            $this->doAddCompanySetting($l);

            if ($this->companySettingsScheduledForDeletion and $this->companySettingsScheduledForDeletion->contains($l)) {
                $this->companySettingsScheduledForDeletion->remove($this->companySettingsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCompanySetting $companySetting The ChildCompanySetting object to add.
     */
    protected function doAddCompanySetting(ChildCompanySetting $companySetting)
    {
        $this->collCompanySettings[]= $companySetting;
        $companySetting->setCompanySetting($this);
    }

    /**
     * @param  ChildCompanySetting $companySetting The ChildCompanySetting object to remove.
     * @return $this|ChildCompany The current object (for fluent API support)
     */
    public function removeCompanySetting(ChildCompanySetting $companySetting)
    {
        if ($this->getCompanySettings()->contains($companySetting)) {
            $pos = $this->collCompanySettings->search($companySetting);
            $this->collCompanySettings->remove($pos);
            if (null === $this->companySettingsScheduledForDeletion) {
                $this->companySettingsScheduledForDeletion = clone $this->collCompanySettings;
                $this->companySettingsScheduledForDeletion->clear();
            }
            $this->companySettingsScheduledForDeletion[]= clone $companySetting;
            $companySetting->setCompanySetting(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aGeneralCountry) {
            $this->aGeneralCountry->removeCompanyRelatedByGeneralCountryId($this);
        }
        if (null !== $this->aInvoiceCountry) {
            $this->aInvoiceCountry->removeCompanyRelatedByInvoiceCountryId($this);
        }
        if (null !== $this->aDeliveryCountry) {
            $this->aDeliveryCountry->removeCompanyRelatedByDeliveryCountryId($this);
        }
        if (null !== $this->aLegalForm) {
            $this->aLegalForm->removeCompany($this);
        }
        if (null !== $this->aOwnCompanyLogo) {
            $this->aOwnCompanyLogo->removeCompanyRelatedByCompanyLogoId($this);
        }
        $this->id = null;
        $this->is_default_supplier = null;
        $this->created_on = null;
        $this->is_deleted = null;
        $this->company_name = null;
        $this->slogan = null;
        $this->chamber_of_commerce = null;
        $this->vat_number = null;
        $this->legal_form_id = null;
        $this->iban = null;
        $this->bic = null;
        $this->phone = null;
        $this->fax = null;
        $this->website = null;
        $this->email = null;
        $this->general_street = null;
        $this->general_number = null;
        $this->general_number_add = null;
        $this->general_postal = null;
        $this->general_city = null;
        $this->general_po_box = null;
        $this->general_country_id = null;
        $this->has_invoice_address = null;
        $this->invoice_company_name = null;
        $this->invoice_street = null;
        $this->invoice_number = null;
        $this->invoice_number_add = null;
        $this->invoice_postal = null;
        $this->invoice_city = null;
        $this->invoice_country_id = null;
        $this->has_delivery_address = null;
        $this->delivery_company_name = null;
        $this->delivery_street = null;
        $this->delivery_number = null;
        $this->delivery_number_add = null;
        $this->delivery_postal = null;
        $this->delivery_city = null;
        $this->delivery_country_id = null;
        $this->company_logo_id = null;
        $this->max_shipping_costs = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCompanyLogosRelatedByOwnCompanyId) {
                foreach ($this->collCompanyLogosRelatedByOwnCompanyId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSites) {
                foreach ($this->collSites as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCompanySettings) {
                foreach ($this->collCompanySettings as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCompanyLogosRelatedByOwnCompanyId = null;
        $this->collSites = null;
        $this->collCompanySettings = null;
        $this->aGeneralCountry = null;
        $this->aInvoiceCountry = null;
        $this->aDeliveryCountry = null;
        $this->aLegalForm = null;
        $this->aOwnCompanyLogo = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CompanyTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
