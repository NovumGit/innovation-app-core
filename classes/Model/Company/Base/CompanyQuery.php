<?php

namespace Model\Company\Base;

use \Exception;
use \PDO;
use Model\Cms\Site;
use Model\Company\Company as ChildCompany;
use Model\Company\CompanyQuery as ChildCompanyQuery;
use Model\Company\Map\CompanyTableMap;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\LegalForm;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'own_company' table.
 *
 *
 *
 * @method     ChildCompanyQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCompanyQuery orderByIsDefaultSupplier($order = Criteria::ASC) Order by the is_default_supplier column
 * @method     ChildCompanyQuery orderByCreatedOn($order = Criteria::ASC) Order by the created_on column
 * @method     ChildCompanyQuery orderByItemDeleted($order = Criteria::ASC) Order by the is_deleted column
 * @method     ChildCompanyQuery orderByCompanyName($order = Criteria::ASC) Order by the company_name column
 * @method     ChildCompanyQuery orderBySlogan($order = Criteria::ASC) Order by the slogan column
 * @method     ChildCompanyQuery orderByChamberOfCommerce($order = Criteria::ASC) Order by the chamber_of_commerce column
 * @method     ChildCompanyQuery orderByVatNumber($order = Criteria::ASC) Order by the vat_number column
 * @method     ChildCompanyQuery orderByLegalFormId($order = Criteria::ASC) Order by the legal_form_id column
 * @method     ChildCompanyQuery orderByIban($order = Criteria::ASC) Order by the iban column
 * @method     ChildCompanyQuery orderByBic($order = Criteria::ASC) Order by the bic column
 * @method     ChildCompanyQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildCompanyQuery orderByFax($order = Criteria::ASC) Order by the fax column
 * @method     ChildCompanyQuery orderByWebsite($order = Criteria::ASC) Order by the website column
 * @method     ChildCompanyQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildCompanyQuery orderByGeneralStreet($order = Criteria::ASC) Order by the general_street column
 * @method     ChildCompanyQuery orderByGeneralNumber($order = Criteria::ASC) Order by the general_number column
 * @method     ChildCompanyQuery orderByGeneralNumberAdd($order = Criteria::ASC) Order by the general_number_add column
 * @method     ChildCompanyQuery orderByGeneralPostal($order = Criteria::ASC) Order by the general_postal column
 * @method     ChildCompanyQuery orderByGeneralCity($order = Criteria::ASC) Order by the general_city column
 * @method     ChildCompanyQuery orderByGeneralPoBox($order = Criteria::ASC) Order by the general_po_box column
 * @method     ChildCompanyQuery orderByGeneralCountryId($order = Criteria::ASC) Order by the general_country_id column
 * @method     ChildCompanyQuery orderByHasInvoiceAddress($order = Criteria::ASC) Order by the has_invoice_address column
 * @method     ChildCompanyQuery orderByInvoiceCompanyName($order = Criteria::ASC) Order by the invoice_company_name column
 * @method     ChildCompanyQuery orderByInvoiceStreet($order = Criteria::ASC) Order by the invoice_street column
 * @method     ChildCompanyQuery orderByInvoiceNumber($order = Criteria::ASC) Order by the invoice_number column
 * @method     ChildCompanyQuery orderByInvoiceNumberAdd($order = Criteria::ASC) Order by the invoice_number_add column
 * @method     ChildCompanyQuery orderByInvoicePostal($order = Criteria::ASC) Order by the invoice_postal column
 * @method     ChildCompanyQuery orderByInvoiceCity($order = Criteria::ASC) Order by the invoice_city column
 * @method     ChildCompanyQuery orderByInvoiceCountryId($order = Criteria::ASC) Order by the invoice_country_id column
 * @method     ChildCompanyQuery orderByHasDeliveryAddress($order = Criteria::ASC) Order by the has_delivery_address column
 * @method     ChildCompanyQuery orderByDeliveryCompanyName($order = Criteria::ASC) Order by the delivery_company_name column
 * @method     ChildCompanyQuery orderByDeliveryStreet($order = Criteria::ASC) Order by the delivery_street column
 * @method     ChildCompanyQuery orderByDeliveryNumber($order = Criteria::ASC) Order by the delivery_number column
 * @method     ChildCompanyQuery orderByDeliveryNumberAdd($order = Criteria::ASC) Order by the delivery_number_add column
 * @method     ChildCompanyQuery orderByDeliveryPostal($order = Criteria::ASC) Order by the delivery_postal column
 * @method     ChildCompanyQuery orderByDeliveryCity($order = Criteria::ASC) Order by the delivery_city column
 * @method     ChildCompanyQuery orderByDeliveryCountryId($order = Criteria::ASC) Order by the delivery_country_id column
 * @method     ChildCompanyQuery orderByCompanyLogoId($order = Criteria::ASC) Order by the company_logo_id column
 * @method     ChildCompanyQuery orderByMaxShippingCosts($order = Criteria::ASC) Order by the max_shipping_costs column
 *
 * @method     ChildCompanyQuery groupById() Group by the id column
 * @method     ChildCompanyQuery groupByIsDefaultSupplier() Group by the is_default_supplier column
 * @method     ChildCompanyQuery groupByCreatedOn() Group by the created_on column
 * @method     ChildCompanyQuery groupByItemDeleted() Group by the is_deleted column
 * @method     ChildCompanyQuery groupByCompanyName() Group by the company_name column
 * @method     ChildCompanyQuery groupBySlogan() Group by the slogan column
 * @method     ChildCompanyQuery groupByChamberOfCommerce() Group by the chamber_of_commerce column
 * @method     ChildCompanyQuery groupByVatNumber() Group by the vat_number column
 * @method     ChildCompanyQuery groupByLegalFormId() Group by the legal_form_id column
 * @method     ChildCompanyQuery groupByIban() Group by the iban column
 * @method     ChildCompanyQuery groupByBic() Group by the bic column
 * @method     ChildCompanyQuery groupByPhone() Group by the phone column
 * @method     ChildCompanyQuery groupByFax() Group by the fax column
 * @method     ChildCompanyQuery groupByWebsite() Group by the website column
 * @method     ChildCompanyQuery groupByEmail() Group by the email column
 * @method     ChildCompanyQuery groupByGeneralStreet() Group by the general_street column
 * @method     ChildCompanyQuery groupByGeneralNumber() Group by the general_number column
 * @method     ChildCompanyQuery groupByGeneralNumberAdd() Group by the general_number_add column
 * @method     ChildCompanyQuery groupByGeneralPostal() Group by the general_postal column
 * @method     ChildCompanyQuery groupByGeneralCity() Group by the general_city column
 * @method     ChildCompanyQuery groupByGeneralPoBox() Group by the general_po_box column
 * @method     ChildCompanyQuery groupByGeneralCountryId() Group by the general_country_id column
 * @method     ChildCompanyQuery groupByHasInvoiceAddress() Group by the has_invoice_address column
 * @method     ChildCompanyQuery groupByInvoiceCompanyName() Group by the invoice_company_name column
 * @method     ChildCompanyQuery groupByInvoiceStreet() Group by the invoice_street column
 * @method     ChildCompanyQuery groupByInvoiceNumber() Group by the invoice_number column
 * @method     ChildCompanyQuery groupByInvoiceNumberAdd() Group by the invoice_number_add column
 * @method     ChildCompanyQuery groupByInvoicePostal() Group by the invoice_postal column
 * @method     ChildCompanyQuery groupByInvoiceCity() Group by the invoice_city column
 * @method     ChildCompanyQuery groupByInvoiceCountryId() Group by the invoice_country_id column
 * @method     ChildCompanyQuery groupByHasDeliveryAddress() Group by the has_delivery_address column
 * @method     ChildCompanyQuery groupByDeliveryCompanyName() Group by the delivery_company_name column
 * @method     ChildCompanyQuery groupByDeliveryStreet() Group by the delivery_street column
 * @method     ChildCompanyQuery groupByDeliveryNumber() Group by the delivery_number column
 * @method     ChildCompanyQuery groupByDeliveryNumberAdd() Group by the delivery_number_add column
 * @method     ChildCompanyQuery groupByDeliveryPostal() Group by the delivery_postal column
 * @method     ChildCompanyQuery groupByDeliveryCity() Group by the delivery_city column
 * @method     ChildCompanyQuery groupByDeliveryCountryId() Group by the delivery_country_id column
 * @method     ChildCompanyQuery groupByCompanyLogoId() Group by the company_logo_id column
 * @method     ChildCompanyQuery groupByMaxShippingCosts() Group by the max_shipping_costs column
 *
 * @method     ChildCompanyQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCompanyQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCompanyQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCompanyQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCompanyQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCompanyQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCompanyQuery leftJoinGeneralCountry($relationAlias = null) Adds a LEFT JOIN clause to the query using the GeneralCountry relation
 * @method     ChildCompanyQuery rightJoinGeneralCountry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the GeneralCountry relation
 * @method     ChildCompanyQuery innerJoinGeneralCountry($relationAlias = null) Adds a INNER JOIN clause to the query using the GeneralCountry relation
 *
 * @method     ChildCompanyQuery joinWithGeneralCountry($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the GeneralCountry relation
 *
 * @method     ChildCompanyQuery leftJoinWithGeneralCountry() Adds a LEFT JOIN clause and with to the query using the GeneralCountry relation
 * @method     ChildCompanyQuery rightJoinWithGeneralCountry() Adds a RIGHT JOIN clause and with to the query using the GeneralCountry relation
 * @method     ChildCompanyQuery innerJoinWithGeneralCountry() Adds a INNER JOIN clause and with to the query using the GeneralCountry relation
 *
 * @method     ChildCompanyQuery leftJoinInvoiceCountry($relationAlias = null) Adds a LEFT JOIN clause to the query using the InvoiceCountry relation
 * @method     ChildCompanyQuery rightJoinInvoiceCountry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the InvoiceCountry relation
 * @method     ChildCompanyQuery innerJoinInvoiceCountry($relationAlias = null) Adds a INNER JOIN clause to the query using the InvoiceCountry relation
 *
 * @method     ChildCompanyQuery joinWithInvoiceCountry($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the InvoiceCountry relation
 *
 * @method     ChildCompanyQuery leftJoinWithInvoiceCountry() Adds a LEFT JOIN clause and with to the query using the InvoiceCountry relation
 * @method     ChildCompanyQuery rightJoinWithInvoiceCountry() Adds a RIGHT JOIN clause and with to the query using the InvoiceCountry relation
 * @method     ChildCompanyQuery innerJoinWithInvoiceCountry() Adds a INNER JOIN clause and with to the query using the InvoiceCountry relation
 *
 * @method     ChildCompanyQuery leftJoinDeliveryCountry($relationAlias = null) Adds a LEFT JOIN clause to the query using the DeliveryCountry relation
 * @method     ChildCompanyQuery rightJoinDeliveryCountry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DeliveryCountry relation
 * @method     ChildCompanyQuery innerJoinDeliveryCountry($relationAlias = null) Adds a INNER JOIN clause to the query using the DeliveryCountry relation
 *
 * @method     ChildCompanyQuery joinWithDeliveryCountry($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DeliveryCountry relation
 *
 * @method     ChildCompanyQuery leftJoinWithDeliveryCountry() Adds a LEFT JOIN clause and with to the query using the DeliveryCountry relation
 * @method     ChildCompanyQuery rightJoinWithDeliveryCountry() Adds a RIGHT JOIN clause and with to the query using the DeliveryCountry relation
 * @method     ChildCompanyQuery innerJoinWithDeliveryCountry() Adds a INNER JOIN clause and with to the query using the DeliveryCountry relation
 *
 * @method     ChildCompanyQuery leftJoinLegalForm($relationAlias = null) Adds a LEFT JOIN clause to the query using the LegalForm relation
 * @method     ChildCompanyQuery rightJoinLegalForm($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LegalForm relation
 * @method     ChildCompanyQuery innerJoinLegalForm($relationAlias = null) Adds a INNER JOIN clause to the query using the LegalForm relation
 *
 * @method     ChildCompanyQuery joinWithLegalForm($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the LegalForm relation
 *
 * @method     ChildCompanyQuery leftJoinWithLegalForm() Adds a LEFT JOIN clause and with to the query using the LegalForm relation
 * @method     ChildCompanyQuery rightJoinWithLegalForm() Adds a RIGHT JOIN clause and with to the query using the LegalForm relation
 * @method     ChildCompanyQuery innerJoinWithLegalForm() Adds a INNER JOIN clause and with to the query using the LegalForm relation
 *
 * @method     ChildCompanyQuery leftJoinOwnCompanyLogo($relationAlias = null) Adds a LEFT JOIN clause to the query using the OwnCompanyLogo relation
 * @method     ChildCompanyQuery rightJoinOwnCompanyLogo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OwnCompanyLogo relation
 * @method     ChildCompanyQuery innerJoinOwnCompanyLogo($relationAlias = null) Adds a INNER JOIN clause to the query using the OwnCompanyLogo relation
 *
 * @method     ChildCompanyQuery joinWithOwnCompanyLogo($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OwnCompanyLogo relation
 *
 * @method     ChildCompanyQuery leftJoinWithOwnCompanyLogo() Adds a LEFT JOIN clause and with to the query using the OwnCompanyLogo relation
 * @method     ChildCompanyQuery rightJoinWithOwnCompanyLogo() Adds a RIGHT JOIN clause and with to the query using the OwnCompanyLogo relation
 * @method     ChildCompanyQuery innerJoinWithOwnCompanyLogo() Adds a INNER JOIN clause and with to the query using the OwnCompanyLogo relation
 *
 * @method     ChildCompanyQuery leftJoinCompanyLogoRelatedByOwnCompanyId($relationAlias = null) Adds a LEFT JOIN clause to the query using the CompanyLogoRelatedByOwnCompanyId relation
 * @method     ChildCompanyQuery rightJoinCompanyLogoRelatedByOwnCompanyId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CompanyLogoRelatedByOwnCompanyId relation
 * @method     ChildCompanyQuery innerJoinCompanyLogoRelatedByOwnCompanyId($relationAlias = null) Adds a INNER JOIN clause to the query using the CompanyLogoRelatedByOwnCompanyId relation
 *
 * @method     ChildCompanyQuery joinWithCompanyLogoRelatedByOwnCompanyId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CompanyLogoRelatedByOwnCompanyId relation
 *
 * @method     ChildCompanyQuery leftJoinWithCompanyLogoRelatedByOwnCompanyId() Adds a LEFT JOIN clause and with to the query using the CompanyLogoRelatedByOwnCompanyId relation
 * @method     ChildCompanyQuery rightJoinWithCompanyLogoRelatedByOwnCompanyId() Adds a RIGHT JOIN clause and with to the query using the CompanyLogoRelatedByOwnCompanyId relation
 * @method     ChildCompanyQuery innerJoinWithCompanyLogoRelatedByOwnCompanyId() Adds a INNER JOIN clause and with to the query using the CompanyLogoRelatedByOwnCompanyId relation
 *
 * @method     ChildCompanyQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildCompanyQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildCompanyQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildCompanyQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildCompanyQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildCompanyQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildCompanyQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildCompanyQuery leftJoinCompanySetting($relationAlias = null) Adds a LEFT JOIN clause to the query using the CompanySetting relation
 * @method     ChildCompanyQuery rightJoinCompanySetting($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CompanySetting relation
 * @method     ChildCompanyQuery innerJoinCompanySetting($relationAlias = null) Adds a INNER JOIN clause to the query using the CompanySetting relation
 *
 * @method     ChildCompanyQuery joinWithCompanySetting($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CompanySetting relation
 *
 * @method     ChildCompanyQuery leftJoinWithCompanySetting() Adds a LEFT JOIN clause and with to the query using the CompanySetting relation
 * @method     ChildCompanyQuery rightJoinWithCompanySetting() Adds a RIGHT JOIN clause and with to the query using the CompanySetting relation
 * @method     ChildCompanyQuery innerJoinWithCompanySetting() Adds a INNER JOIN clause and with to the query using the CompanySetting relation
 *
 * @method     \Model\Setting\MasterTable\CountryQuery|\Model\Setting\MasterTable\LegalFormQuery|\Model\Company\CompanyLogoQuery|\Model\Cms\SiteQuery|\Model\Company\CompanySettingQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCompany findOne(ConnectionInterface $con = null) Return the first ChildCompany matching the query
 * @method     ChildCompany findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCompany matching the query, or a new ChildCompany object populated from the query conditions when no match is found
 *
 * @method     ChildCompany findOneById(int $id) Return the first ChildCompany filtered by the id column
 * @method     ChildCompany findOneByIsDefaultSupplier(boolean $is_default_supplier) Return the first ChildCompany filtered by the is_default_supplier column
 * @method     ChildCompany findOneByCreatedOn(string $created_on) Return the first ChildCompany filtered by the created_on column
 * @method     ChildCompany findOneByItemDeleted(boolean $is_deleted) Return the first ChildCompany filtered by the is_deleted column
 * @method     ChildCompany findOneByCompanyName(string $company_name) Return the first ChildCompany filtered by the company_name column
 * @method     ChildCompany findOneBySlogan(string $slogan) Return the first ChildCompany filtered by the slogan column
 * @method     ChildCompany findOneByChamberOfCommerce(string $chamber_of_commerce) Return the first ChildCompany filtered by the chamber_of_commerce column
 * @method     ChildCompany findOneByVatNumber(string $vat_number) Return the first ChildCompany filtered by the vat_number column
 * @method     ChildCompany findOneByLegalFormId(int $legal_form_id) Return the first ChildCompany filtered by the legal_form_id column
 * @method     ChildCompany findOneByIban(string $iban) Return the first ChildCompany filtered by the iban column
 * @method     ChildCompany findOneByBic(string $bic) Return the first ChildCompany filtered by the bic column
 * @method     ChildCompany findOneByPhone(string $phone) Return the first ChildCompany filtered by the phone column
 * @method     ChildCompany findOneByFax(string $fax) Return the first ChildCompany filtered by the fax column
 * @method     ChildCompany findOneByWebsite(string $website) Return the first ChildCompany filtered by the website column
 * @method     ChildCompany findOneByEmail(string $email) Return the first ChildCompany filtered by the email column
 * @method     ChildCompany findOneByGeneralStreet(string $general_street) Return the first ChildCompany filtered by the general_street column
 * @method     ChildCompany findOneByGeneralNumber(string $general_number) Return the first ChildCompany filtered by the general_number column
 * @method     ChildCompany findOneByGeneralNumberAdd(string $general_number_add) Return the first ChildCompany filtered by the general_number_add column
 * @method     ChildCompany findOneByGeneralPostal(string $general_postal) Return the first ChildCompany filtered by the general_postal column
 * @method     ChildCompany findOneByGeneralCity(string $general_city) Return the first ChildCompany filtered by the general_city column
 * @method     ChildCompany findOneByGeneralPoBox(string $general_po_box) Return the first ChildCompany filtered by the general_po_box column
 * @method     ChildCompany findOneByGeneralCountryId(int $general_country_id) Return the first ChildCompany filtered by the general_country_id column
 * @method     ChildCompany findOneByHasInvoiceAddress(boolean $has_invoice_address) Return the first ChildCompany filtered by the has_invoice_address column
 * @method     ChildCompany findOneByInvoiceCompanyName(string $invoice_company_name) Return the first ChildCompany filtered by the invoice_company_name column
 * @method     ChildCompany findOneByInvoiceStreet(string $invoice_street) Return the first ChildCompany filtered by the invoice_street column
 * @method     ChildCompany findOneByInvoiceNumber(string $invoice_number) Return the first ChildCompany filtered by the invoice_number column
 * @method     ChildCompany findOneByInvoiceNumberAdd(string $invoice_number_add) Return the first ChildCompany filtered by the invoice_number_add column
 * @method     ChildCompany findOneByInvoicePostal(string $invoice_postal) Return the first ChildCompany filtered by the invoice_postal column
 * @method     ChildCompany findOneByInvoiceCity(string $invoice_city) Return the first ChildCompany filtered by the invoice_city column
 * @method     ChildCompany findOneByInvoiceCountryId(int $invoice_country_id) Return the first ChildCompany filtered by the invoice_country_id column
 * @method     ChildCompany findOneByHasDeliveryAddress(boolean $has_delivery_address) Return the first ChildCompany filtered by the has_delivery_address column
 * @method     ChildCompany findOneByDeliveryCompanyName(string $delivery_company_name) Return the first ChildCompany filtered by the delivery_company_name column
 * @method     ChildCompany findOneByDeliveryStreet(string $delivery_street) Return the first ChildCompany filtered by the delivery_street column
 * @method     ChildCompany findOneByDeliveryNumber(string $delivery_number) Return the first ChildCompany filtered by the delivery_number column
 * @method     ChildCompany findOneByDeliveryNumberAdd(string $delivery_number_add) Return the first ChildCompany filtered by the delivery_number_add column
 * @method     ChildCompany findOneByDeliveryPostal(string $delivery_postal) Return the first ChildCompany filtered by the delivery_postal column
 * @method     ChildCompany findOneByDeliveryCity(string $delivery_city) Return the first ChildCompany filtered by the delivery_city column
 * @method     ChildCompany findOneByDeliveryCountryId(int $delivery_country_id) Return the first ChildCompany filtered by the delivery_country_id column
 * @method     ChildCompany findOneByCompanyLogoId(int $company_logo_id) Return the first ChildCompany filtered by the company_logo_id column
 * @method     ChildCompany findOneByMaxShippingCosts(int $max_shipping_costs) Return the first ChildCompany filtered by the max_shipping_costs column *

 * @method     ChildCompany requirePk($key, ConnectionInterface $con = null) Return the ChildCompany by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOne(ConnectionInterface $con = null) Return the first ChildCompany matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCompany requireOneById(int $id) Return the first ChildCompany filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByIsDefaultSupplier(boolean $is_default_supplier) Return the first ChildCompany filtered by the is_default_supplier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByCreatedOn(string $created_on) Return the first ChildCompany filtered by the created_on column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByItemDeleted(boolean $is_deleted) Return the first ChildCompany filtered by the is_deleted column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByCompanyName(string $company_name) Return the first ChildCompany filtered by the company_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneBySlogan(string $slogan) Return the first ChildCompany filtered by the slogan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByChamberOfCommerce(string $chamber_of_commerce) Return the first ChildCompany filtered by the chamber_of_commerce column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByVatNumber(string $vat_number) Return the first ChildCompany filtered by the vat_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByLegalFormId(int $legal_form_id) Return the first ChildCompany filtered by the legal_form_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByIban(string $iban) Return the first ChildCompany filtered by the iban column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByBic(string $bic) Return the first ChildCompany filtered by the bic column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByPhone(string $phone) Return the first ChildCompany filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByFax(string $fax) Return the first ChildCompany filtered by the fax column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByWebsite(string $website) Return the first ChildCompany filtered by the website column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByEmail(string $email) Return the first ChildCompany filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByGeneralStreet(string $general_street) Return the first ChildCompany filtered by the general_street column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByGeneralNumber(string $general_number) Return the first ChildCompany filtered by the general_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByGeneralNumberAdd(string $general_number_add) Return the first ChildCompany filtered by the general_number_add column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByGeneralPostal(string $general_postal) Return the first ChildCompany filtered by the general_postal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByGeneralCity(string $general_city) Return the first ChildCompany filtered by the general_city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByGeneralPoBox(string $general_po_box) Return the first ChildCompany filtered by the general_po_box column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByGeneralCountryId(int $general_country_id) Return the first ChildCompany filtered by the general_country_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByHasInvoiceAddress(boolean $has_invoice_address) Return the first ChildCompany filtered by the has_invoice_address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByInvoiceCompanyName(string $invoice_company_name) Return the first ChildCompany filtered by the invoice_company_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByInvoiceStreet(string $invoice_street) Return the first ChildCompany filtered by the invoice_street column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByInvoiceNumber(string $invoice_number) Return the first ChildCompany filtered by the invoice_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByInvoiceNumberAdd(string $invoice_number_add) Return the first ChildCompany filtered by the invoice_number_add column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByInvoicePostal(string $invoice_postal) Return the first ChildCompany filtered by the invoice_postal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByInvoiceCity(string $invoice_city) Return the first ChildCompany filtered by the invoice_city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByInvoiceCountryId(int $invoice_country_id) Return the first ChildCompany filtered by the invoice_country_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByHasDeliveryAddress(boolean $has_delivery_address) Return the first ChildCompany filtered by the has_delivery_address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByDeliveryCompanyName(string $delivery_company_name) Return the first ChildCompany filtered by the delivery_company_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByDeliveryStreet(string $delivery_street) Return the first ChildCompany filtered by the delivery_street column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByDeliveryNumber(string $delivery_number) Return the first ChildCompany filtered by the delivery_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByDeliveryNumberAdd(string $delivery_number_add) Return the first ChildCompany filtered by the delivery_number_add column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByDeliveryPostal(string $delivery_postal) Return the first ChildCompany filtered by the delivery_postal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByDeliveryCity(string $delivery_city) Return the first ChildCompany filtered by the delivery_city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByDeliveryCountryId(int $delivery_country_id) Return the first ChildCompany filtered by the delivery_country_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByCompanyLogoId(int $company_logo_id) Return the first ChildCompany filtered by the company_logo_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompany requireOneByMaxShippingCosts(int $max_shipping_costs) Return the first ChildCompany filtered by the max_shipping_costs column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCompany[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCompany objects based on current ModelCriteria
 * @method     ChildCompany[]|ObjectCollection findById(int $id) Return ChildCompany objects filtered by the id column
 * @method     ChildCompany[]|ObjectCollection findByIsDefaultSupplier(boolean $is_default_supplier) Return ChildCompany objects filtered by the is_default_supplier column
 * @method     ChildCompany[]|ObjectCollection findByCreatedOn(string $created_on) Return ChildCompany objects filtered by the created_on column
 * @method     ChildCompany[]|ObjectCollection findByItemDeleted(boolean $is_deleted) Return ChildCompany objects filtered by the is_deleted column
 * @method     ChildCompany[]|ObjectCollection findByCompanyName(string $company_name) Return ChildCompany objects filtered by the company_name column
 * @method     ChildCompany[]|ObjectCollection findBySlogan(string $slogan) Return ChildCompany objects filtered by the slogan column
 * @method     ChildCompany[]|ObjectCollection findByChamberOfCommerce(string $chamber_of_commerce) Return ChildCompany objects filtered by the chamber_of_commerce column
 * @method     ChildCompany[]|ObjectCollection findByVatNumber(string $vat_number) Return ChildCompany objects filtered by the vat_number column
 * @method     ChildCompany[]|ObjectCollection findByLegalFormId(int $legal_form_id) Return ChildCompany objects filtered by the legal_form_id column
 * @method     ChildCompany[]|ObjectCollection findByIban(string $iban) Return ChildCompany objects filtered by the iban column
 * @method     ChildCompany[]|ObjectCollection findByBic(string $bic) Return ChildCompany objects filtered by the bic column
 * @method     ChildCompany[]|ObjectCollection findByPhone(string $phone) Return ChildCompany objects filtered by the phone column
 * @method     ChildCompany[]|ObjectCollection findByFax(string $fax) Return ChildCompany objects filtered by the fax column
 * @method     ChildCompany[]|ObjectCollection findByWebsite(string $website) Return ChildCompany objects filtered by the website column
 * @method     ChildCompany[]|ObjectCollection findByEmail(string $email) Return ChildCompany objects filtered by the email column
 * @method     ChildCompany[]|ObjectCollection findByGeneralStreet(string $general_street) Return ChildCompany objects filtered by the general_street column
 * @method     ChildCompany[]|ObjectCollection findByGeneralNumber(string $general_number) Return ChildCompany objects filtered by the general_number column
 * @method     ChildCompany[]|ObjectCollection findByGeneralNumberAdd(string $general_number_add) Return ChildCompany objects filtered by the general_number_add column
 * @method     ChildCompany[]|ObjectCollection findByGeneralPostal(string $general_postal) Return ChildCompany objects filtered by the general_postal column
 * @method     ChildCompany[]|ObjectCollection findByGeneralCity(string $general_city) Return ChildCompany objects filtered by the general_city column
 * @method     ChildCompany[]|ObjectCollection findByGeneralPoBox(string $general_po_box) Return ChildCompany objects filtered by the general_po_box column
 * @method     ChildCompany[]|ObjectCollection findByGeneralCountryId(int $general_country_id) Return ChildCompany objects filtered by the general_country_id column
 * @method     ChildCompany[]|ObjectCollection findByHasInvoiceAddress(boolean $has_invoice_address) Return ChildCompany objects filtered by the has_invoice_address column
 * @method     ChildCompany[]|ObjectCollection findByInvoiceCompanyName(string $invoice_company_name) Return ChildCompany objects filtered by the invoice_company_name column
 * @method     ChildCompany[]|ObjectCollection findByInvoiceStreet(string $invoice_street) Return ChildCompany objects filtered by the invoice_street column
 * @method     ChildCompany[]|ObjectCollection findByInvoiceNumber(string $invoice_number) Return ChildCompany objects filtered by the invoice_number column
 * @method     ChildCompany[]|ObjectCollection findByInvoiceNumberAdd(string $invoice_number_add) Return ChildCompany objects filtered by the invoice_number_add column
 * @method     ChildCompany[]|ObjectCollection findByInvoicePostal(string $invoice_postal) Return ChildCompany objects filtered by the invoice_postal column
 * @method     ChildCompany[]|ObjectCollection findByInvoiceCity(string $invoice_city) Return ChildCompany objects filtered by the invoice_city column
 * @method     ChildCompany[]|ObjectCollection findByInvoiceCountryId(int $invoice_country_id) Return ChildCompany objects filtered by the invoice_country_id column
 * @method     ChildCompany[]|ObjectCollection findByHasDeliveryAddress(boolean $has_delivery_address) Return ChildCompany objects filtered by the has_delivery_address column
 * @method     ChildCompany[]|ObjectCollection findByDeliveryCompanyName(string $delivery_company_name) Return ChildCompany objects filtered by the delivery_company_name column
 * @method     ChildCompany[]|ObjectCollection findByDeliveryStreet(string $delivery_street) Return ChildCompany objects filtered by the delivery_street column
 * @method     ChildCompany[]|ObjectCollection findByDeliveryNumber(string $delivery_number) Return ChildCompany objects filtered by the delivery_number column
 * @method     ChildCompany[]|ObjectCollection findByDeliveryNumberAdd(string $delivery_number_add) Return ChildCompany objects filtered by the delivery_number_add column
 * @method     ChildCompany[]|ObjectCollection findByDeliveryPostal(string $delivery_postal) Return ChildCompany objects filtered by the delivery_postal column
 * @method     ChildCompany[]|ObjectCollection findByDeliveryCity(string $delivery_city) Return ChildCompany objects filtered by the delivery_city column
 * @method     ChildCompany[]|ObjectCollection findByDeliveryCountryId(int $delivery_country_id) Return ChildCompany objects filtered by the delivery_country_id column
 * @method     ChildCompany[]|ObjectCollection findByCompanyLogoId(int $company_logo_id) Return ChildCompany objects filtered by the company_logo_id column
 * @method     ChildCompany[]|ObjectCollection findByMaxShippingCosts(int $max_shipping_costs) Return ChildCompany objects filtered by the max_shipping_costs column
 * @method     ChildCompany[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CompanyQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Company\Base\CompanyQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Company\\Company', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCompanyQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCompanyQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCompanyQuery) {
            return $criteria;
        }
        $query = new ChildCompanyQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCompany|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CompanyTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CompanyTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompany A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, is_default_supplier, created_on, is_deleted, company_name, slogan, chamber_of_commerce, vat_number, legal_form_id, iban, bic, phone, fax, website, email, general_street, general_number, general_number_add, general_postal, general_city, general_po_box, general_country_id, has_invoice_address, invoice_company_name, invoice_street, invoice_number, invoice_number_add, invoice_postal, invoice_city, invoice_country_id, has_delivery_address, delivery_company_name, delivery_street, delivery_number, delivery_number_add, delivery_postal, delivery_city, delivery_country_id, company_logo_id, max_shipping_costs FROM own_company WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCompany $obj */
            $obj = new ChildCompany();
            $obj->hydrate($row);
            CompanyTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCompany|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CompanyTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CompanyTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CompanyTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CompanyTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the is_default_supplier column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDefaultSupplier(true); // WHERE is_default_supplier = true
     * $query->filterByIsDefaultSupplier('yes'); // WHERE is_default_supplier = true
     * </code>
     *
     * @param     boolean|string $isDefaultSupplier The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByIsDefaultSupplier($isDefaultSupplier = null, $comparison = null)
    {
        if (is_string($isDefaultSupplier)) {
            $isDefaultSupplier = in_array(strtolower($isDefaultSupplier), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CompanyTableMap::COL_IS_DEFAULT_SUPPLIER, $isDefaultSupplier, $comparison);
    }

    /**
     * Filter the query on the created_on column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedOn('2011-03-14'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn('now'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn(array('max' => 'yesterday')); // WHERE created_on > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdOn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByCreatedOn($createdOn = null, $comparison = null)
    {
        if (is_array($createdOn)) {
            $useMinMax = false;
            if (isset($createdOn['min'])) {
                $this->addUsingAlias(CompanyTableMap::COL_CREATED_ON, $createdOn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdOn['max'])) {
                $this->addUsingAlias(CompanyTableMap::COL_CREATED_ON, $createdOn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_CREATED_ON, $createdOn, $comparison);
    }

    /**
     * Filter the query on the is_deleted column
     *
     * Example usage:
     * <code>
     * $query->filterByItemDeleted(true); // WHERE is_deleted = true
     * $query->filterByItemDeleted('yes'); // WHERE is_deleted = true
     * </code>
     *
     * @param     boolean|string $itemDeleted The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByItemDeleted($itemDeleted = null, $comparison = null)
    {
        if (is_string($itemDeleted)) {
            $itemDeleted = in_array(strtolower($itemDeleted), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CompanyTableMap::COL_IS_DELETED, $itemDeleted, $comparison);
    }

    /**
     * Filter the query on the company_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCompanyName('fooValue');   // WHERE company_name = 'fooValue'
     * $query->filterByCompanyName('%fooValue%', Criteria::LIKE); // WHERE company_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $companyName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByCompanyName($companyName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($companyName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_COMPANY_NAME, $companyName, $comparison);
    }

    /**
     * Filter the query on the slogan column
     *
     * Example usage:
     * <code>
     * $query->filterBySlogan('fooValue');   // WHERE slogan = 'fooValue'
     * $query->filterBySlogan('%fooValue%', Criteria::LIKE); // WHERE slogan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slogan The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterBySlogan($slogan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slogan)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_SLOGAN, $slogan, $comparison);
    }

    /**
     * Filter the query on the chamber_of_commerce column
     *
     * Example usage:
     * <code>
     * $query->filterByChamberOfCommerce('fooValue');   // WHERE chamber_of_commerce = 'fooValue'
     * $query->filterByChamberOfCommerce('%fooValue%', Criteria::LIKE); // WHERE chamber_of_commerce LIKE '%fooValue%'
     * </code>
     *
     * @param     string $chamberOfCommerce The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByChamberOfCommerce($chamberOfCommerce = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($chamberOfCommerce)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_CHAMBER_OF_COMMERCE, $chamberOfCommerce, $comparison);
    }

    /**
     * Filter the query on the vat_number column
     *
     * Example usage:
     * <code>
     * $query->filterByVatNumber('fooValue');   // WHERE vat_number = 'fooValue'
     * $query->filterByVatNumber('%fooValue%', Criteria::LIKE); // WHERE vat_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $vatNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByVatNumber($vatNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($vatNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_VAT_NUMBER, $vatNumber, $comparison);
    }

    /**
     * Filter the query on the legal_form_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLegalFormId(1234); // WHERE legal_form_id = 1234
     * $query->filterByLegalFormId(array(12, 34)); // WHERE legal_form_id IN (12, 34)
     * $query->filterByLegalFormId(array('min' => 12)); // WHERE legal_form_id > 12
     * </code>
     *
     * @see       filterByLegalForm()
     *
     * @param     mixed $legalFormId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByLegalFormId($legalFormId = null, $comparison = null)
    {
        if (is_array($legalFormId)) {
            $useMinMax = false;
            if (isset($legalFormId['min'])) {
                $this->addUsingAlias(CompanyTableMap::COL_LEGAL_FORM_ID, $legalFormId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($legalFormId['max'])) {
                $this->addUsingAlias(CompanyTableMap::COL_LEGAL_FORM_ID, $legalFormId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_LEGAL_FORM_ID, $legalFormId, $comparison);
    }

    /**
     * Filter the query on the iban column
     *
     * Example usage:
     * <code>
     * $query->filterByIban('fooValue');   // WHERE iban = 'fooValue'
     * $query->filterByIban('%fooValue%', Criteria::LIKE); // WHERE iban LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iban The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByIban($iban = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iban)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_IBAN, $iban, $comparison);
    }

    /**
     * Filter the query on the bic column
     *
     * Example usage:
     * <code>
     * $query->filterByBic('fooValue');   // WHERE bic = 'fooValue'
     * $query->filterByBic('%fooValue%', Criteria::LIKE); // WHERE bic LIKE '%fooValue%'
     * </code>
     *
     * @param     string $bic The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByBic($bic = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($bic)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_BIC, $bic, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%', Criteria::LIKE); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the fax column
     *
     * Example usage:
     * <code>
     * $query->filterByFax('fooValue');   // WHERE fax = 'fooValue'
     * $query->filterByFax('%fooValue%', Criteria::LIKE); // WHERE fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fax The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByFax($fax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fax)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_FAX, $fax, $comparison);
    }

    /**
     * Filter the query on the website column
     *
     * Example usage:
     * <code>
     * $query->filterByWebsite('fooValue');   // WHERE website = 'fooValue'
     * $query->filterByWebsite('%fooValue%', Criteria::LIKE); // WHERE website LIKE '%fooValue%'
     * </code>
     *
     * @param     string $website The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByWebsite($website = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($website)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_WEBSITE, $website, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the general_street column
     *
     * Example usage:
     * <code>
     * $query->filterByGeneralStreet('fooValue');   // WHERE general_street = 'fooValue'
     * $query->filterByGeneralStreet('%fooValue%', Criteria::LIKE); // WHERE general_street LIKE '%fooValue%'
     * </code>
     *
     * @param     string $generalStreet The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByGeneralStreet($generalStreet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($generalStreet)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_GENERAL_STREET, $generalStreet, $comparison);
    }

    /**
     * Filter the query on the general_number column
     *
     * Example usage:
     * <code>
     * $query->filterByGeneralNumber('fooValue');   // WHERE general_number = 'fooValue'
     * $query->filterByGeneralNumber('%fooValue%', Criteria::LIKE); // WHERE general_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $generalNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByGeneralNumber($generalNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($generalNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_GENERAL_NUMBER, $generalNumber, $comparison);
    }

    /**
     * Filter the query on the general_number_add column
     *
     * Example usage:
     * <code>
     * $query->filterByGeneralNumberAdd('fooValue');   // WHERE general_number_add = 'fooValue'
     * $query->filterByGeneralNumberAdd('%fooValue%', Criteria::LIKE); // WHERE general_number_add LIKE '%fooValue%'
     * </code>
     *
     * @param     string $generalNumberAdd The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByGeneralNumberAdd($generalNumberAdd = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($generalNumberAdd)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_GENERAL_NUMBER_ADD, $generalNumberAdd, $comparison);
    }

    /**
     * Filter the query on the general_postal column
     *
     * Example usage:
     * <code>
     * $query->filterByGeneralPostal('fooValue');   // WHERE general_postal = 'fooValue'
     * $query->filterByGeneralPostal('%fooValue%', Criteria::LIKE); // WHERE general_postal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $generalPostal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByGeneralPostal($generalPostal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($generalPostal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_GENERAL_POSTAL, $generalPostal, $comparison);
    }

    /**
     * Filter the query on the general_city column
     *
     * Example usage:
     * <code>
     * $query->filterByGeneralCity('fooValue');   // WHERE general_city = 'fooValue'
     * $query->filterByGeneralCity('%fooValue%', Criteria::LIKE); // WHERE general_city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $generalCity The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByGeneralCity($generalCity = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($generalCity)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_GENERAL_CITY, $generalCity, $comparison);
    }

    /**
     * Filter the query on the general_po_box column
     *
     * Example usage:
     * <code>
     * $query->filterByGeneralPoBox('fooValue');   // WHERE general_po_box = 'fooValue'
     * $query->filterByGeneralPoBox('%fooValue%', Criteria::LIKE); // WHERE general_po_box LIKE '%fooValue%'
     * </code>
     *
     * @param     string $generalPoBox The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByGeneralPoBox($generalPoBox = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($generalPoBox)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_GENERAL_PO_BOX, $generalPoBox, $comparison);
    }

    /**
     * Filter the query on the general_country_id column
     *
     * Example usage:
     * <code>
     * $query->filterByGeneralCountryId(1234); // WHERE general_country_id = 1234
     * $query->filterByGeneralCountryId(array(12, 34)); // WHERE general_country_id IN (12, 34)
     * $query->filterByGeneralCountryId(array('min' => 12)); // WHERE general_country_id > 12
     * </code>
     *
     * @see       filterByGeneralCountry()
     *
     * @param     mixed $generalCountryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByGeneralCountryId($generalCountryId = null, $comparison = null)
    {
        if (is_array($generalCountryId)) {
            $useMinMax = false;
            if (isset($generalCountryId['min'])) {
                $this->addUsingAlias(CompanyTableMap::COL_GENERAL_COUNTRY_ID, $generalCountryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($generalCountryId['max'])) {
                $this->addUsingAlias(CompanyTableMap::COL_GENERAL_COUNTRY_ID, $generalCountryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_GENERAL_COUNTRY_ID, $generalCountryId, $comparison);
    }

    /**
     * Filter the query on the has_invoice_address column
     *
     * Example usage:
     * <code>
     * $query->filterByHasInvoiceAddress(true); // WHERE has_invoice_address = true
     * $query->filterByHasInvoiceAddress('yes'); // WHERE has_invoice_address = true
     * </code>
     *
     * @param     boolean|string $hasInvoiceAddress The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByHasInvoiceAddress($hasInvoiceAddress = null, $comparison = null)
    {
        if (is_string($hasInvoiceAddress)) {
            $hasInvoiceAddress = in_array(strtolower($hasInvoiceAddress), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CompanyTableMap::COL_HAS_INVOICE_ADDRESS, $hasInvoiceAddress, $comparison);
    }

    /**
     * Filter the query on the invoice_company_name column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceCompanyName('fooValue');   // WHERE invoice_company_name = 'fooValue'
     * $query->filterByInvoiceCompanyName('%fooValue%', Criteria::LIKE); // WHERE invoice_company_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $invoiceCompanyName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByInvoiceCompanyName($invoiceCompanyName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($invoiceCompanyName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_INVOICE_COMPANY_NAME, $invoiceCompanyName, $comparison);
    }

    /**
     * Filter the query on the invoice_street column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceStreet('fooValue');   // WHERE invoice_street = 'fooValue'
     * $query->filterByInvoiceStreet('%fooValue%', Criteria::LIKE); // WHERE invoice_street LIKE '%fooValue%'
     * </code>
     *
     * @param     string $invoiceStreet The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByInvoiceStreet($invoiceStreet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($invoiceStreet)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_INVOICE_STREET, $invoiceStreet, $comparison);
    }

    /**
     * Filter the query on the invoice_number column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceNumber('fooValue');   // WHERE invoice_number = 'fooValue'
     * $query->filterByInvoiceNumber('%fooValue%', Criteria::LIKE); // WHERE invoice_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $invoiceNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByInvoiceNumber($invoiceNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($invoiceNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_INVOICE_NUMBER, $invoiceNumber, $comparison);
    }

    /**
     * Filter the query on the invoice_number_add column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceNumberAdd('fooValue');   // WHERE invoice_number_add = 'fooValue'
     * $query->filterByInvoiceNumberAdd('%fooValue%', Criteria::LIKE); // WHERE invoice_number_add LIKE '%fooValue%'
     * </code>
     *
     * @param     string $invoiceNumberAdd The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByInvoiceNumberAdd($invoiceNumberAdd = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($invoiceNumberAdd)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_INVOICE_NUMBER_ADD, $invoiceNumberAdd, $comparison);
    }

    /**
     * Filter the query on the invoice_postal column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoicePostal('fooValue');   // WHERE invoice_postal = 'fooValue'
     * $query->filterByInvoicePostal('%fooValue%', Criteria::LIKE); // WHERE invoice_postal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $invoicePostal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByInvoicePostal($invoicePostal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($invoicePostal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_INVOICE_POSTAL, $invoicePostal, $comparison);
    }

    /**
     * Filter the query on the invoice_city column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceCity('fooValue');   // WHERE invoice_city = 'fooValue'
     * $query->filterByInvoiceCity('%fooValue%', Criteria::LIKE); // WHERE invoice_city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $invoiceCity The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByInvoiceCity($invoiceCity = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($invoiceCity)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_INVOICE_CITY, $invoiceCity, $comparison);
    }

    /**
     * Filter the query on the invoice_country_id column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceCountryId(1234); // WHERE invoice_country_id = 1234
     * $query->filterByInvoiceCountryId(array(12, 34)); // WHERE invoice_country_id IN (12, 34)
     * $query->filterByInvoiceCountryId(array('min' => 12)); // WHERE invoice_country_id > 12
     * </code>
     *
     * @see       filterByInvoiceCountry()
     *
     * @param     mixed $invoiceCountryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByInvoiceCountryId($invoiceCountryId = null, $comparison = null)
    {
        if (is_array($invoiceCountryId)) {
            $useMinMax = false;
            if (isset($invoiceCountryId['min'])) {
                $this->addUsingAlias(CompanyTableMap::COL_INVOICE_COUNTRY_ID, $invoiceCountryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($invoiceCountryId['max'])) {
                $this->addUsingAlias(CompanyTableMap::COL_INVOICE_COUNTRY_ID, $invoiceCountryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_INVOICE_COUNTRY_ID, $invoiceCountryId, $comparison);
    }

    /**
     * Filter the query on the has_delivery_address column
     *
     * Example usage:
     * <code>
     * $query->filterByHasDeliveryAddress(true); // WHERE has_delivery_address = true
     * $query->filterByHasDeliveryAddress('yes'); // WHERE has_delivery_address = true
     * </code>
     *
     * @param     boolean|string $hasDeliveryAddress The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByHasDeliveryAddress($hasDeliveryAddress = null, $comparison = null)
    {
        if (is_string($hasDeliveryAddress)) {
            $hasDeliveryAddress = in_array(strtolower($hasDeliveryAddress), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CompanyTableMap::COL_HAS_DELIVERY_ADDRESS, $hasDeliveryAddress, $comparison);
    }

    /**
     * Filter the query on the delivery_company_name column
     *
     * Example usage:
     * <code>
     * $query->filterByDeliveryCompanyName('fooValue');   // WHERE delivery_company_name = 'fooValue'
     * $query->filterByDeliveryCompanyName('%fooValue%', Criteria::LIKE); // WHERE delivery_company_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deliveryCompanyName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByDeliveryCompanyName($deliveryCompanyName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deliveryCompanyName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_DELIVERY_COMPANY_NAME, $deliveryCompanyName, $comparison);
    }

    /**
     * Filter the query on the delivery_street column
     *
     * Example usage:
     * <code>
     * $query->filterByDeliveryStreet('fooValue');   // WHERE delivery_street = 'fooValue'
     * $query->filterByDeliveryStreet('%fooValue%', Criteria::LIKE); // WHERE delivery_street LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deliveryStreet The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByDeliveryStreet($deliveryStreet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deliveryStreet)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_DELIVERY_STREET, $deliveryStreet, $comparison);
    }

    /**
     * Filter the query on the delivery_number column
     *
     * Example usage:
     * <code>
     * $query->filterByDeliveryNumber('fooValue');   // WHERE delivery_number = 'fooValue'
     * $query->filterByDeliveryNumber('%fooValue%', Criteria::LIKE); // WHERE delivery_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deliveryNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByDeliveryNumber($deliveryNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deliveryNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_DELIVERY_NUMBER, $deliveryNumber, $comparison);
    }

    /**
     * Filter the query on the delivery_number_add column
     *
     * Example usage:
     * <code>
     * $query->filterByDeliveryNumberAdd('fooValue');   // WHERE delivery_number_add = 'fooValue'
     * $query->filterByDeliveryNumberAdd('%fooValue%', Criteria::LIKE); // WHERE delivery_number_add LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deliveryNumberAdd The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByDeliveryNumberAdd($deliveryNumberAdd = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deliveryNumberAdd)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_DELIVERY_NUMBER_ADD, $deliveryNumberAdd, $comparison);
    }

    /**
     * Filter the query on the delivery_postal column
     *
     * Example usage:
     * <code>
     * $query->filterByDeliveryPostal('fooValue');   // WHERE delivery_postal = 'fooValue'
     * $query->filterByDeliveryPostal('%fooValue%', Criteria::LIKE); // WHERE delivery_postal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deliveryPostal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByDeliveryPostal($deliveryPostal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deliveryPostal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_DELIVERY_POSTAL, $deliveryPostal, $comparison);
    }

    /**
     * Filter the query on the delivery_city column
     *
     * Example usage:
     * <code>
     * $query->filterByDeliveryCity('fooValue');   // WHERE delivery_city = 'fooValue'
     * $query->filterByDeliveryCity('%fooValue%', Criteria::LIKE); // WHERE delivery_city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $deliveryCity The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByDeliveryCity($deliveryCity = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($deliveryCity)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_DELIVERY_CITY, $deliveryCity, $comparison);
    }

    /**
     * Filter the query on the delivery_country_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDeliveryCountryId(1234); // WHERE delivery_country_id = 1234
     * $query->filterByDeliveryCountryId(array(12, 34)); // WHERE delivery_country_id IN (12, 34)
     * $query->filterByDeliveryCountryId(array('min' => 12)); // WHERE delivery_country_id > 12
     * </code>
     *
     * @see       filterByDeliveryCountry()
     *
     * @param     mixed $deliveryCountryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByDeliveryCountryId($deliveryCountryId = null, $comparison = null)
    {
        if (is_array($deliveryCountryId)) {
            $useMinMax = false;
            if (isset($deliveryCountryId['min'])) {
                $this->addUsingAlias(CompanyTableMap::COL_DELIVERY_COUNTRY_ID, $deliveryCountryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deliveryCountryId['max'])) {
                $this->addUsingAlias(CompanyTableMap::COL_DELIVERY_COUNTRY_ID, $deliveryCountryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_DELIVERY_COUNTRY_ID, $deliveryCountryId, $comparison);
    }

    /**
     * Filter the query on the company_logo_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompanyLogoId(1234); // WHERE company_logo_id = 1234
     * $query->filterByCompanyLogoId(array(12, 34)); // WHERE company_logo_id IN (12, 34)
     * $query->filterByCompanyLogoId(array('min' => 12)); // WHERE company_logo_id > 12
     * </code>
     *
     * @see       filterByOwnCompanyLogo()
     *
     * @param     mixed $companyLogoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByCompanyLogoId($companyLogoId = null, $comparison = null)
    {
        if (is_array($companyLogoId)) {
            $useMinMax = false;
            if (isset($companyLogoId['min'])) {
                $this->addUsingAlias(CompanyTableMap::COL_COMPANY_LOGO_ID, $companyLogoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($companyLogoId['max'])) {
                $this->addUsingAlias(CompanyTableMap::COL_COMPANY_LOGO_ID, $companyLogoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_COMPANY_LOGO_ID, $companyLogoId, $comparison);
    }

    /**
     * Filter the query on the max_shipping_costs column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxShippingCosts(1234); // WHERE max_shipping_costs = 1234
     * $query->filterByMaxShippingCosts(array(12, 34)); // WHERE max_shipping_costs IN (12, 34)
     * $query->filterByMaxShippingCosts(array('min' => 12)); // WHERE max_shipping_costs > 12
     * </code>
     *
     * @param     mixed $maxShippingCosts The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByMaxShippingCosts($maxShippingCosts = null, $comparison = null)
    {
        if (is_array($maxShippingCosts)) {
            $useMinMax = false;
            if (isset($maxShippingCosts['min'])) {
                $this->addUsingAlias(CompanyTableMap::COL_MAX_SHIPPING_COSTS, $maxShippingCosts['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxShippingCosts['max'])) {
                $this->addUsingAlias(CompanyTableMap::COL_MAX_SHIPPING_COSTS, $maxShippingCosts['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyTableMap::COL_MAX_SHIPPING_COSTS, $maxShippingCosts, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Country object
     *
     * @param \Model\Setting\MasterTable\Country|ObjectCollection $country The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByGeneralCountry($country, $comparison = null)
    {
        if ($country instanceof \Model\Setting\MasterTable\Country) {
            return $this
                ->addUsingAlias(CompanyTableMap::COL_GENERAL_COUNTRY_ID, $country->getId(), $comparison);
        } elseif ($country instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CompanyTableMap::COL_GENERAL_COUNTRY_ID, $country->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByGeneralCountry() only accepts arguments of type \Model\Setting\MasterTable\Country or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the GeneralCountry relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function joinGeneralCountry($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('GeneralCountry');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'GeneralCountry');
        }

        return $this;
    }

    /**
     * Use the GeneralCountry relation Country object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\CountryQuery A secondary query class using the current class as primary query
     */
    public function useGeneralCountryQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinGeneralCountry($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'GeneralCountry', '\Model\Setting\MasterTable\CountryQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Country object
     *
     * @param \Model\Setting\MasterTable\Country|ObjectCollection $country The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByInvoiceCountry($country, $comparison = null)
    {
        if ($country instanceof \Model\Setting\MasterTable\Country) {
            return $this
                ->addUsingAlias(CompanyTableMap::COL_INVOICE_COUNTRY_ID, $country->getId(), $comparison);
        } elseif ($country instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CompanyTableMap::COL_INVOICE_COUNTRY_ID, $country->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByInvoiceCountry() only accepts arguments of type \Model\Setting\MasterTable\Country or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the InvoiceCountry relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function joinInvoiceCountry($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('InvoiceCountry');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'InvoiceCountry');
        }

        return $this;
    }

    /**
     * Use the InvoiceCountry relation Country object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\CountryQuery A secondary query class using the current class as primary query
     */
    public function useInvoiceCountryQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinInvoiceCountry($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'InvoiceCountry', '\Model\Setting\MasterTable\CountryQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Country object
     *
     * @param \Model\Setting\MasterTable\Country|ObjectCollection $country The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByDeliveryCountry($country, $comparison = null)
    {
        if ($country instanceof \Model\Setting\MasterTable\Country) {
            return $this
                ->addUsingAlias(CompanyTableMap::COL_DELIVERY_COUNTRY_ID, $country->getId(), $comparison);
        } elseif ($country instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CompanyTableMap::COL_DELIVERY_COUNTRY_ID, $country->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDeliveryCountry() only accepts arguments of type \Model\Setting\MasterTable\Country or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DeliveryCountry relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function joinDeliveryCountry($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DeliveryCountry');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DeliveryCountry');
        }

        return $this;
    }

    /**
     * Use the DeliveryCountry relation Country object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\CountryQuery A secondary query class using the current class as primary query
     */
    public function useDeliveryCountryQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinDeliveryCountry($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DeliveryCountry', '\Model\Setting\MasterTable\CountryQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\LegalForm object
     *
     * @param \Model\Setting\MasterTable\LegalForm|ObjectCollection $legalForm The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByLegalForm($legalForm, $comparison = null)
    {
        if ($legalForm instanceof \Model\Setting\MasterTable\LegalForm) {
            return $this
                ->addUsingAlias(CompanyTableMap::COL_LEGAL_FORM_ID, $legalForm->getId(), $comparison);
        } elseif ($legalForm instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CompanyTableMap::COL_LEGAL_FORM_ID, $legalForm->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLegalForm() only accepts arguments of type \Model\Setting\MasterTable\LegalForm or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LegalForm relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function joinLegalForm($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LegalForm');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LegalForm');
        }

        return $this;
    }

    /**
     * Use the LegalForm relation LegalForm object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\LegalFormQuery A secondary query class using the current class as primary query
     */
    public function useLegalFormQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLegalForm($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LegalForm', '\Model\Setting\MasterTable\LegalFormQuery');
    }

    /**
     * Filter the query by a related \Model\Company\CompanyLogo object
     *
     * @param \Model\Company\CompanyLogo|ObjectCollection $companyLogo The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByOwnCompanyLogo($companyLogo, $comparison = null)
    {
        if ($companyLogo instanceof \Model\Company\CompanyLogo) {
            return $this
                ->addUsingAlias(CompanyTableMap::COL_COMPANY_LOGO_ID, $companyLogo->getId(), $comparison);
        } elseif ($companyLogo instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CompanyTableMap::COL_COMPANY_LOGO_ID, $companyLogo->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByOwnCompanyLogo() only accepts arguments of type \Model\Company\CompanyLogo or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OwnCompanyLogo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function joinOwnCompanyLogo($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OwnCompanyLogo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OwnCompanyLogo');
        }

        return $this;
    }

    /**
     * Use the OwnCompanyLogo relation CompanyLogo object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Company\CompanyLogoQuery A secondary query class using the current class as primary query
     */
    public function useOwnCompanyLogoQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOwnCompanyLogo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OwnCompanyLogo', '\Model\Company\CompanyLogoQuery');
    }

    /**
     * Filter the query by a related \Model\Company\CompanyLogo object
     *
     * @param \Model\Company\CompanyLogo|ObjectCollection $companyLogo the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByCompanyLogoRelatedByOwnCompanyId($companyLogo, $comparison = null)
    {
        if ($companyLogo instanceof \Model\Company\CompanyLogo) {
            return $this
                ->addUsingAlias(CompanyTableMap::COL_ID, $companyLogo->getOwnCompanyId(), $comparison);
        } elseif ($companyLogo instanceof ObjectCollection) {
            return $this
                ->useCompanyLogoRelatedByOwnCompanyIdQuery()
                ->filterByPrimaryKeys($companyLogo->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCompanyLogoRelatedByOwnCompanyId() only accepts arguments of type \Model\Company\CompanyLogo or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CompanyLogoRelatedByOwnCompanyId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function joinCompanyLogoRelatedByOwnCompanyId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CompanyLogoRelatedByOwnCompanyId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CompanyLogoRelatedByOwnCompanyId');
        }

        return $this;
    }

    /**
     * Use the CompanyLogoRelatedByOwnCompanyId relation CompanyLogo object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Company\CompanyLogoQuery A secondary query class using the current class as primary query
     */
    public function useCompanyLogoRelatedByOwnCompanyIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompanyLogoRelatedByOwnCompanyId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CompanyLogoRelatedByOwnCompanyId', '\Model\Company\CompanyLogoQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\Site object
     *
     * @param \Model\Cms\Site|ObjectCollection $site the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompanyQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \Model\Cms\Site) {
            return $this
                ->addUsingAlias(CompanyTableMap::COL_ID, $site->getInvoicingCompanyId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            return $this
                ->useSiteQuery()
                ->filterByPrimaryKeys($site->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \Model\Cms\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\Model\Cms\SiteQuery');
    }

    /**
     * Filter the query by a related \Model\Company\CompanySetting object
     *
     * @param \Model\Company\CompanySetting|ObjectCollection $companySetting the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompanyQuery The current query, for fluid interface
     */
    public function filterByCompanySetting($companySetting, $comparison = null)
    {
        if ($companySetting instanceof \Model\Company\CompanySetting) {
            return $this
                ->addUsingAlias(CompanyTableMap::COL_ID, $companySetting->getOwnCompanyId(), $comparison);
        } elseif ($companySetting instanceof ObjectCollection) {
            return $this
                ->useCompanySettingQuery()
                ->filterByPrimaryKeys($companySetting->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCompanySetting() only accepts arguments of type \Model\Company\CompanySetting or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CompanySetting relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function joinCompanySetting($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CompanySetting');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CompanySetting');
        }

        return $this;
    }

    /**
     * Use the CompanySetting relation CompanySetting object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Company\CompanySettingQuery A secondary query class using the current class as primary query
     */
    public function useCompanySettingQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCompanySetting($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CompanySetting', '\Model\Company\CompanySettingQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCompany $company Object to remove from the list of results
     *
     * @return $this|ChildCompanyQuery The current query, for fluid interface
     */
    public function prune($company = null)
    {
        if ($company) {
            $this->addUsingAlias(CompanyTableMap::COL_ID, $company->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the own_company table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompanyTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CompanyTableMap::clearInstancePool();
            CompanyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompanyTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CompanyTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CompanyTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CompanyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CompanyQuery
