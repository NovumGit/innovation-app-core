<?php

namespace Model\Company\Base;

use \Exception;
use \PDO;
use Model\Company\CompanyLogo as ChildCompanyLogo;
use Model\Company\CompanyLogoQuery as ChildCompanyLogoQuery;
use Model\Company\Map\CompanyLogoTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'own_company_logo' table.
 *
 *
 *
 * @method     ChildCompanyLogoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCompanyLogoQuery orderByOwnCompanyId($order = Criteria::ASC) Order by the own_company_id column
 * @method     ChildCompanyLogoQuery orderByOriginalName($order = Criteria::ASC) Order by the original_name column
 * @method     ChildCompanyLogoQuery orderByExt($order = Criteria::ASC) Order by the ext column
 * @method     ChildCompanyLogoQuery orderBySize($order = Criteria::ASC) Order by the size column
 * @method     ChildCompanyLogoQuery orderByMime($order = Criteria::ASC) Order by the mime column
 *
 * @method     ChildCompanyLogoQuery groupById() Group by the id column
 * @method     ChildCompanyLogoQuery groupByOwnCompanyId() Group by the own_company_id column
 * @method     ChildCompanyLogoQuery groupByOriginalName() Group by the original_name column
 * @method     ChildCompanyLogoQuery groupByExt() Group by the ext column
 * @method     ChildCompanyLogoQuery groupBySize() Group by the size column
 * @method     ChildCompanyLogoQuery groupByMime() Group by the mime column
 *
 * @method     ChildCompanyLogoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCompanyLogoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCompanyLogoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCompanyLogoQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCompanyLogoQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCompanyLogoQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCompanyLogoQuery leftJoinOwnCompany($relationAlias = null) Adds a LEFT JOIN clause to the query using the OwnCompany relation
 * @method     ChildCompanyLogoQuery rightJoinOwnCompany($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OwnCompany relation
 * @method     ChildCompanyLogoQuery innerJoinOwnCompany($relationAlias = null) Adds a INNER JOIN clause to the query using the OwnCompany relation
 *
 * @method     ChildCompanyLogoQuery joinWithOwnCompany($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OwnCompany relation
 *
 * @method     ChildCompanyLogoQuery leftJoinWithOwnCompany() Adds a LEFT JOIN clause and with to the query using the OwnCompany relation
 * @method     ChildCompanyLogoQuery rightJoinWithOwnCompany() Adds a RIGHT JOIN clause and with to the query using the OwnCompany relation
 * @method     ChildCompanyLogoQuery innerJoinWithOwnCompany() Adds a INNER JOIN clause and with to the query using the OwnCompany relation
 *
 * @method     ChildCompanyLogoQuery leftJoinCompanyRelatedByCompanyLogoId($relationAlias = null) Adds a LEFT JOIN clause to the query using the CompanyRelatedByCompanyLogoId relation
 * @method     ChildCompanyLogoQuery rightJoinCompanyRelatedByCompanyLogoId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CompanyRelatedByCompanyLogoId relation
 * @method     ChildCompanyLogoQuery innerJoinCompanyRelatedByCompanyLogoId($relationAlias = null) Adds a INNER JOIN clause to the query using the CompanyRelatedByCompanyLogoId relation
 *
 * @method     ChildCompanyLogoQuery joinWithCompanyRelatedByCompanyLogoId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CompanyRelatedByCompanyLogoId relation
 *
 * @method     ChildCompanyLogoQuery leftJoinWithCompanyRelatedByCompanyLogoId() Adds a LEFT JOIN clause and with to the query using the CompanyRelatedByCompanyLogoId relation
 * @method     ChildCompanyLogoQuery rightJoinWithCompanyRelatedByCompanyLogoId() Adds a RIGHT JOIN clause and with to the query using the CompanyRelatedByCompanyLogoId relation
 * @method     ChildCompanyLogoQuery innerJoinWithCompanyRelatedByCompanyLogoId() Adds a INNER JOIN clause and with to the query using the CompanyRelatedByCompanyLogoId relation
 *
 * @method     \Model\Company\CompanyQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCompanyLogo findOne(ConnectionInterface $con = null) Return the first ChildCompanyLogo matching the query
 * @method     ChildCompanyLogo findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCompanyLogo matching the query, or a new ChildCompanyLogo object populated from the query conditions when no match is found
 *
 * @method     ChildCompanyLogo findOneById(int $id) Return the first ChildCompanyLogo filtered by the id column
 * @method     ChildCompanyLogo findOneByOwnCompanyId(int $own_company_id) Return the first ChildCompanyLogo filtered by the own_company_id column
 * @method     ChildCompanyLogo findOneByOriginalName(string $original_name) Return the first ChildCompanyLogo filtered by the original_name column
 * @method     ChildCompanyLogo findOneByExt(string $ext) Return the first ChildCompanyLogo filtered by the ext column
 * @method     ChildCompanyLogo findOneBySize(int $size) Return the first ChildCompanyLogo filtered by the size column
 * @method     ChildCompanyLogo findOneByMime(string $mime) Return the first ChildCompanyLogo filtered by the mime column *

 * @method     ChildCompanyLogo requirePk($key, ConnectionInterface $con = null) Return the ChildCompanyLogo by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompanyLogo requireOne(ConnectionInterface $con = null) Return the first ChildCompanyLogo matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCompanyLogo requireOneById(int $id) Return the first ChildCompanyLogo filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompanyLogo requireOneByOwnCompanyId(int $own_company_id) Return the first ChildCompanyLogo filtered by the own_company_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompanyLogo requireOneByOriginalName(string $original_name) Return the first ChildCompanyLogo filtered by the original_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompanyLogo requireOneByExt(string $ext) Return the first ChildCompanyLogo filtered by the ext column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompanyLogo requireOneBySize(int $size) Return the first ChildCompanyLogo filtered by the size column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCompanyLogo requireOneByMime(string $mime) Return the first ChildCompanyLogo filtered by the mime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCompanyLogo[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCompanyLogo objects based on current ModelCriteria
 * @method     ChildCompanyLogo[]|ObjectCollection findById(int $id) Return ChildCompanyLogo objects filtered by the id column
 * @method     ChildCompanyLogo[]|ObjectCollection findByOwnCompanyId(int $own_company_id) Return ChildCompanyLogo objects filtered by the own_company_id column
 * @method     ChildCompanyLogo[]|ObjectCollection findByOriginalName(string $original_name) Return ChildCompanyLogo objects filtered by the original_name column
 * @method     ChildCompanyLogo[]|ObjectCollection findByExt(string $ext) Return ChildCompanyLogo objects filtered by the ext column
 * @method     ChildCompanyLogo[]|ObjectCollection findBySize(int $size) Return ChildCompanyLogo objects filtered by the size column
 * @method     ChildCompanyLogo[]|ObjectCollection findByMime(string $mime) Return ChildCompanyLogo objects filtered by the mime column
 * @method     ChildCompanyLogo[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CompanyLogoQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Company\Base\CompanyLogoQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Company\\CompanyLogo', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCompanyLogoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCompanyLogoQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCompanyLogoQuery) {
            return $criteria;
        }
        $query = new ChildCompanyLogoQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCompanyLogo|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CompanyLogoTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CompanyLogoTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompanyLogo A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, own_company_id, original_name, ext, size, mime FROM own_company_logo WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCompanyLogo $obj */
            $obj = new ChildCompanyLogo();
            $obj->hydrate($row);
            CompanyLogoTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCompanyLogo|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCompanyLogoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CompanyLogoTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCompanyLogoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CompanyLogoTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyLogoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CompanyLogoTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CompanyLogoTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyLogoTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the own_company_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOwnCompanyId(1234); // WHERE own_company_id = 1234
     * $query->filterByOwnCompanyId(array(12, 34)); // WHERE own_company_id IN (12, 34)
     * $query->filterByOwnCompanyId(array('min' => 12)); // WHERE own_company_id > 12
     * </code>
     *
     * @see       filterByOwnCompany()
     *
     * @param     mixed $ownCompanyId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyLogoQuery The current query, for fluid interface
     */
    public function filterByOwnCompanyId($ownCompanyId = null, $comparison = null)
    {
        if (is_array($ownCompanyId)) {
            $useMinMax = false;
            if (isset($ownCompanyId['min'])) {
                $this->addUsingAlias(CompanyLogoTableMap::COL_OWN_COMPANY_ID, $ownCompanyId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($ownCompanyId['max'])) {
                $this->addUsingAlias(CompanyLogoTableMap::COL_OWN_COMPANY_ID, $ownCompanyId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyLogoTableMap::COL_OWN_COMPANY_ID, $ownCompanyId, $comparison);
    }

    /**
     * Filter the query on the original_name column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginalName('fooValue');   // WHERE original_name = 'fooValue'
     * $query->filterByOriginalName('%fooValue%', Criteria::LIKE); // WHERE original_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $originalName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyLogoQuery The current query, for fluid interface
     */
    public function filterByOriginalName($originalName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($originalName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyLogoTableMap::COL_ORIGINAL_NAME, $originalName, $comparison);
    }

    /**
     * Filter the query on the ext column
     *
     * Example usage:
     * <code>
     * $query->filterByExt('fooValue');   // WHERE ext = 'fooValue'
     * $query->filterByExt('%fooValue%', Criteria::LIKE); // WHERE ext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ext The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyLogoQuery The current query, for fluid interface
     */
    public function filterByExt($ext = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ext)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyLogoTableMap::COL_EXT, $ext, $comparison);
    }

    /**
     * Filter the query on the size column
     *
     * Example usage:
     * <code>
     * $query->filterBySize(1234); // WHERE size = 1234
     * $query->filterBySize(array(12, 34)); // WHERE size IN (12, 34)
     * $query->filterBySize(array('min' => 12)); // WHERE size > 12
     * </code>
     *
     * @param     mixed $size The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyLogoQuery The current query, for fluid interface
     */
    public function filterBySize($size = null, $comparison = null)
    {
        if (is_array($size)) {
            $useMinMax = false;
            if (isset($size['min'])) {
                $this->addUsingAlias(CompanyLogoTableMap::COL_SIZE, $size['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($size['max'])) {
                $this->addUsingAlias(CompanyLogoTableMap::COL_SIZE, $size['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyLogoTableMap::COL_SIZE, $size, $comparison);
    }

    /**
     * Filter the query on the mime column
     *
     * Example usage:
     * <code>
     * $query->filterByMime('fooValue');   // WHERE mime = 'fooValue'
     * $query->filterByMime('%fooValue%', Criteria::LIKE); // WHERE mime LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mime The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCompanyLogoQuery The current query, for fluid interface
     */
    public function filterByMime($mime = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mime)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CompanyLogoTableMap::COL_MIME, $mime, $comparison);
    }

    /**
     * Filter the query by a related \Model\Company\Company object
     *
     * @param \Model\Company\Company|ObjectCollection $company The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCompanyLogoQuery The current query, for fluid interface
     */
    public function filterByOwnCompany($company, $comparison = null)
    {
        if ($company instanceof \Model\Company\Company) {
            return $this
                ->addUsingAlias(CompanyLogoTableMap::COL_OWN_COMPANY_ID, $company->getId(), $comparison);
        } elseif ($company instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CompanyLogoTableMap::COL_OWN_COMPANY_ID, $company->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByOwnCompany() only accepts arguments of type \Model\Company\Company or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OwnCompany relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompanyLogoQuery The current query, for fluid interface
     */
    public function joinOwnCompany($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OwnCompany');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OwnCompany');
        }

        return $this;
    }

    /**
     * Use the OwnCompany relation Company object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Company\CompanyQuery A secondary query class using the current class as primary query
     */
    public function useOwnCompanyQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOwnCompany($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OwnCompany', '\Model\Company\CompanyQuery');
    }

    /**
     * Filter the query by a related \Model\Company\Company object
     *
     * @param \Model\Company\Company|ObjectCollection $company the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCompanyLogoQuery The current query, for fluid interface
     */
    public function filterByCompanyRelatedByCompanyLogoId($company, $comparison = null)
    {
        if ($company instanceof \Model\Company\Company) {
            return $this
                ->addUsingAlias(CompanyLogoTableMap::COL_ID, $company->getCompanyLogoId(), $comparison);
        } elseif ($company instanceof ObjectCollection) {
            return $this
                ->useCompanyRelatedByCompanyLogoIdQuery()
                ->filterByPrimaryKeys($company->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCompanyRelatedByCompanyLogoId() only accepts arguments of type \Model\Company\Company or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CompanyRelatedByCompanyLogoId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCompanyLogoQuery The current query, for fluid interface
     */
    public function joinCompanyRelatedByCompanyLogoId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CompanyRelatedByCompanyLogoId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CompanyRelatedByCompanyLogoId');
        }

        return $this;
    }

    /**
     * Use the CompanyRelatedByCompanyLogoId relation Company object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Company\CompanyQuery A secondary query class using the current class as primary query
     */
    public function useCompanyRelatedByCompanyLogoIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCompanyRelatedByCompanyLogoId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CompanyRelatedByCompanyLogoId', '\Model\Company\CompanyQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCompanyLogo $companyLogo Object to remove from the list of results
     *
     * @return $this|ChildCompanyLogoQuery The current query, for fluid interface
     */
    public function prune($companyLogo = null)
    {
        if ($companyLogo) {
            $this->addUsingAlias(CompanyLogoTableMap::COL_ID, $companyLogo->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the own_company_logo table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompanyLogoTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CompanyLogoTableMap::clearInstancePool();
            CompanyLogoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompanyLogoTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CompanyLogoTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CompanyLogoTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CompanyLogoTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CompanyLogoQuery
