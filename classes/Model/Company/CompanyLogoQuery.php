<?php

namespace Model\Company;

use Model\Company\Base\CompanyLogoQuery as BaseCompanyLogoQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'own_company_logo' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CompanyLogoQuery extends BaseCompanyLogoQuery
{

}
