<?php

namespace Model\Company;

use Core\StatusMessage;
use Model\Company\Base\CompanyQuery as BaseCompanyQuery;
use Model\Crm\CustomerCompanyQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'own_company' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CompanyQuery extends BaseCompanyQuery
{

    static function getDefaultCompany()
    {
        $oCompany = CompanyQuery::create()->findOneByIsDefaultSupplier(true);

        if(!$oCompany instanceof Company)
        {
            $oCompany = new Company();
            StatusMessage::warning("Er is geen default own company gedefinieerd, maak een company aan.");
        }
        return $oCompany;
    }
}
