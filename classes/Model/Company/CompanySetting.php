<?php

namespace Model\Company;

use Model\Company\Base\CompanySetting as BaseCompanySetting;

/**
 * Skeleton subclass for representing a row from the 'own_company_setting' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CompanySetting extends BaseCompanySetting
{

}
