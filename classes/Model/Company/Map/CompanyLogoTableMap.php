<?php

namespace Model\Company\Map;

use Model\Company\CompanyLogo;
use Model\Company\CompanyLogoQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'own_company_logo' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CompanyLogoTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Company.Map.CompanyLogoTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'own_company_logo';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Company\\CompanyLogo';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Company.CompanyLogo';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 6;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 6;

    /**
     * the column name for the id field
     */
    const COL_ID = 'own_company_logo.id';

    /**
     * the column name for the own_company_id field
     */
    const COL_OWN_COMPANY_ID = 'own_company_logo.own_company_id';

    /**
     * the column name for the original_name field
     */
    const COL_ORIGINAL_NAME = 'own_company_logo.original_name';

    /**
     * the column name for the ext field
     */
    const COL_EXT = 'own_company_logo.ext';

    /**
     * the column name for the size field
     */
    const COL_SIZE = 'own_company_logo.size';

    /**
     * the column name for the mime field
     */
    const COL_MIME = 'own_company_logo.mime';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'OwnCompanyId', 'OriginalName', 'Ext', 'Size', 'Mime', ),
        self::TYPE_CAMELNAME     => array('id', 'ownCompanyId', 'originalName', 'ext', 'size', 'mime', ),
        self::TYPE_COLNAME       => array(CompanyLogoTableMap::COL_ID, CompanyLogoTableMap::COL_OWN_COMPANY_ID, CompanyLogoTableMap::COL_ORIGINAL_NAME, CompanyLogoTableMap::COL_EXT, CompanyLogoTableMap::COL_SIZE, CompanyLogoTableMap::COL_MIME, ),
        self::TYPE_FIELDNAME     => array('id', 'own_company_id', 'original_name', 'ext', 'size', 'mime', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'OwnCompanyId' => 1, 'OriginalName' => 2, 'Ext' => 3, 'Size' => 4, 'Mime' => 5, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'ownCompanyId' => 1, 'originalName' => 2, 'ext' => 3, 'size' => 4, 'mime' => 5, ),
        self::TYPE_COLNAME       => array(CompanyLogoTableMap::COL_ID => 0, CompanyLogoTableMap::COL_OWN_COMPANY_ID => 1, CompanyLogoTableMap::COL_ORIGINAL_NAME => 2, CompanyLogoTableMap::COL_EXT => 3, CompanyLogoTableMap::COL_SIZE => 4, CompanyLogoTableMap::COL_MIME => 5, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'own_company_id' => 1, 'original_name' => 2, 'ext' => 3, 'size' => 4, 'mime' => 5, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('own_company_logo');
        $this->setPhpName('CompanyLogo');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Company\\CompanyLogo');
        $this->setPackage('Model.Company');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('own_company_id', 'OwnCompanyId', 'INTEGER', 'own_company', 'id', true, null, null);
        $this->addColumn('original_name', 'OriginalName', 'VARCHAR', true, 255, null);
        $this->addColumn('ext', 'Ext', 'VARCHAR', true, 255, null);
        $this->addColumn('size', 'Size', 'INTEGER', true, null, null);
        $this->addColumn('mime', 'Mime', 'VARCHAR', true, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('OwnCompany', '\\Model\\Company\\Company', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':own_company_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('CompanyRelatedByCompanyLogoId', '\\Model\\Company\\Company', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':company_logo_id',
    1 => ':id',
  ),
), 'SET NULL', null, 'CompaniesRelatedByCompanyLogoId', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to own_company_logo     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CompanyTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CompanyLogoTableMap::CLASS_DEFAULT : CompanyLogoTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CompanyLogo object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CompanyLogoTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CompanyLogoTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CompanyLogoTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CompanyLogoTableMap::OM_CLASS;
            /** @var CompanyLogo $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CompanyLogoTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CompanyLogoTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CompanyLogoTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CompanyLogo $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CompanyLogoTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CompanyLogoTableMap::COL_ID);
            $criteria->addSelectColumn(CompanyLogoTableMap::COL_OWN_COMPANY_ID);
            $criteria->addSelectColumn(CompanyLogoTableMap::COL_ORIGINAL_NAME);
            $criteria->addSelectColumn(CompanyLogoTableMap::COL_EXT);
            $criteria->addSelectColumn(CompanyLogoTableMap::COL_SIZE);
            $criteria->addSelectColumn(CompanyLogoTableMap::COL_MIME);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.own_company_id');
            $criteria->addSelectColumn($alias . '.original_name');
            $criteria->addSelectColumn($alias . '.ext');
            $criteria->addSelectColumn($alias . '.size');
            $criteria->addSelectColumn($alias . '.mime');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CompanyLogoTableMap::DATABASE_NAME)->getTable(CompanyLogoTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CompanyLogoTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CompanyLogoTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CompanyLogoTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CompanyLogo or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CompanyLogo object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompanyLogoTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Company\CompanyLogo) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CompanyLogoTableMap::DATABASE_NAME);
            $criteria->add(CompanyLogoTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CompanyLogoQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CompanyLogoTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CompanyLogoTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the own_company_logo table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CompanyLogoQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CompanyLogo or Criteria object.
     *
     * @param mixed               $criteria Criteria or CompanyLogo object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompanyLogoTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CompanyLogo object
        }

        if ($criteria->containsKey(CompanyLogoTableMap::COL_ID) && $criteria->keyContainsValue(CompanyLogoTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CompanyLogoTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CompanyLogoQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CompanyLogoTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CompanyLogoTableMap::buildTableMap();
