<?php

namespace Model\Company\Map;

use Model\Company\Company;
use Model\Company\CompanyQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'own_company' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CompanyTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Company.Map.CompanyTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'own_company';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Company\\Company';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Company.Company';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 40;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 40;

    /**
     * the column name for the id field
     */
    const COL_ID = 'own_company.id';

    /**
     * the column name for the is_default_supplier field
     */
    const COL_IS_DEFAULT_SUPPLIER = 'own_company.is_default_supplier';

    /**
     * the column name for the created_on field
     */
    const COL_CREATED_ON = 'own_company.created_on';

    /**
     * the column name for the is_deleted field
     */
    const COL_IS_DELETED = 'own_company.is_deleted';

    /**
     * the column name for the company_name field
     */
    const COL_COMPANY_NAME = 'own_company.company_name';

    /**
     * the column name for the slogan field
     */
    const COL_SLOGAN = 'own_company.slogan';

    /**
     * the column name for the chamber_of_commerce field
     */
    const COL_CHAMBER_OF_COMMERCE = 'own_company.chamber_of_commerce';

    /**
     * the column name for the vat_number field
     */
    const COL_VAT_NUMBER = 'own_company.vat_number';

    /**
     * the column name for the legal_form_id field
     */
    const COL_LEGAL_FORM_ID = 'own_company.legal_form_id';

    /**
     * the column name for the iban field
     */
    const COL_IBAN = 'own_company.iban';

    /**
     * the column name for the bic field
     */
    const COL_BIC = 'own_company.bic';

    /**
     * the column name for the phone field
     */
    const COL_PHONE = 'own_company.phone';

    /**
     * the column name for the fax field
     */
    const COL_FAX = 'own_company.fax';

    /**
     * the column name for the website field
     */
    const COL_WEBSITE = 'own_company.website';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'own_company.email';

    /**
     * the column name for the general_street field
     */
    const COL_GENERAL_STREET = 'own_company.general_street';

    /**
     * the column name for the general_number field
     */
    const COL_GENERAL_NUMBER = 'own_company.general_number';

    /**
     * the column name for the general_number_add field
     */
    const COL_GENERAL_NUMBER_ADD = 'own_company.general_number_add';

    /**
     * the column name for the general_postal field
     */
    const COL_GENERAL_POSTAL = 'own_company.general_postal';

    /**
     * the column name for the general_city field
     */
    const COL_GENERAL_CITY = 'own_company.general_city';

    /**
     * the column name for the general_po_box field
     */
    const COL_GENERAL_PO_BOX = 'own_company.general_po_box';

    /**
     * the column name for the general_country_id field
     */
    const COL_GENERAL_COUNTRY_ID = 'own_company.general_country_id';

    /**
     * the column name for the has_invoice_address field
     */
    const COL_HAS_INVOICE_ADDRESS = 'own_company.has_invoice_address';

    /**
     * the column name for the invoice_company_name field
     */
    const COL_INVOICE_COMPANY_NAME = 'own_company.invoice_company_name';

    /**
     * the column name for the invoice_street field
     */
    const COL_INVOICE_STREET = 'own_company.invoice_street';

    /**
     * the column name for the invoice_number field
     */
    const COL_INVOICE_NUMBER = 'own_company.invoice_number';

    /**
     * the column name for the invoice_number_add field
     */
    const COL_INVOICE_NUMBER_ADD = 'own_company.invoice_number_add';

    /**
     * the column name for the invoice_postal field
     */
    const COL_INVOICE_POSTAL = 'own_company.invoice_postal';

    /**
     * the column name for the invoice_city field
     */
    const COL_INVOICE_CITY = 'own_company.invoice_city';

    /**
     * the column name for the invoice_country_id field
     */
    const COL_INVOICE_COUNTRY_ID = 'own_company.invoice_country_id';

    /**
     * the column name for the has_delivery_address field
     */
    const COL_HAS_DELIVERY_ADDRESS = 'own_company.has_delivery_address';

    /**
     * the column name for the delivery_company_name field
     */
    const COL_DELIVERY_COMPANY_NAME = 'own_company.delivery_company_name';

    /**
     * the column name for the delivery_street field
     */
    const COL_DELIVERY_STREET = 'own_company.delivery_street';

    /**
     * the column name for the delivery_number field
     */
    const COL_DELIVERY_NUMBER = 'own_company.delivery_number';

    /**
     * the column name for the delivery_number_add field
     */
    const COL_DELIVERY_NUMBER_ADD = 'own_company.delivery_number_add';

    /**
     * the column name for the delivery_postal field
     */
    const COL_DELIVERY_POSTAL = 'own_company.delivery_postal';

    /**
     * the column name for the delivery_city field
     */
    const COL_DELIVERY_CITY = 'own_company.delivery_city';

    /**
     * the column name for the delivery_country_id field
     */
    const COL_DELIVERY_COUNTRY_ID = 'own_company.delivery_country_id';

    /**
     * the column name for the company_logo_id field
     */
    const COL_COMPANY_LOGO_ID = 'own_company.company_logo_id';

    /**
     * the column name for the max_shipping_costs field
     */
    const COL_MAX_SHIPPING_COSTS = 'own_company.max_shipping_costs';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'IsDefaultSupplier', 'CreatedOn', 'ItemDeleted', 'CompanyName', 'Slogan', 'ChamberOfCommerce', 'VatNumber', 'LegalFormId', 'Iban', 'Bic', 'Phone', 'Fax', 'Website', 'Email', 'GeneralStreet', 'GeneralNumber', 'GeneralNumberAdd', 'GeneralPostal', 'GeneralCity', 'GeneralPoBox', 'GeneralCountryId', 'HasInvoiceAddress', 'InvoiceCompanyName', 'InvoiceStreet', 'InvoiceNumber', 'InvoiceNumberAdd', 'InvoicePostal', 'InvoiceCity', 'InvoiceCountryId', 'HasDeliveryAddress', 'DeliveryCompanyName', 'DeliveryStreet', 'DeliveryNumber', 'DeliveryNumberAdd', 'DeliveryPostal', 'DeliveryCity', 'DeliveryCountryId', 'CompanyLogoId', 'MaxShippingCosts', ),
        self::TYPE_CAMELNAME     => array('id', 'isDefaultSupplier', 'createdOn', 'itemDeleted', 'companyName', 'slogan', 'chamberOfCommerce', 'vatNumber', 'legalFormId', 'iban', 'bic', 'phone', 'fax', 'website', 'email', 'generalStreet', 'generalNumber', 'generalNumberAdd', 'generalPostal', 'generalCity', 'generalPoBox', 'generalCountryId', 'hasInvoiceAddress', 'invoiceCompanyName', 'invoiceStreet', 'invoiceNumber', 'invoiceNumberAdd', 'invoicePostal', 'invoiceCity', 'invoiceCountryId', 'hasDeliveryAddress', 'deliveryCompanyName', 'deliveryStreet', 'deliveryNumber', 'deliveryNumberAdd', 'deliveryPostal', 'deliveryCity', 'deliveryCountryId', 'companyLogoId', 'maxShippingCosts', ),
        self::TYPE_COLNAME       => array(CompanyTableMap::COL_ID, CompanyTableMap::COL_IS_DEFAULT_SUPPLIER, CompanyTableMap::COL_CREATED_ON, CompanyTableMap::COL_IS_DELETED, CompanyTableMap::COL_COMPANY_NAME, CompanyTableMap::COL_SLOGAN, CompanyTableMap::COL_CHAMBER_OF_COMMERCE, CompanyTableMap::COL_VAT_NUMBER, CompanyTableMap::COL_LEGAL_FORM_ID, CompanyTableMap::COL_IBAN, CompanyTableMap::COL_BIC, CompanyTableMap::COL_PHONE, CompanyTableMap::COL_FAX, CompanyTableMap::COL_WEBSITE, CompanyTableMap::COL_EMAIL, CompanyTableMap::COL_GENERAL_STREET, CompanyTableMap::COL_GENERAL_NUMBER, CompanyTableMap::COL_GENERAL_NUMBER_ADD, CompanyTableMap::COL_GENERAL_POSTAL, CompanyTableMap::COL_GENERAL_CITY, CompanyTableMap::COL_GENERAL_PO_BOX, CompanyTableMap::COL_GENERAL_COUNTRY_ID, CompanyTableMap::COL_HAS_INVOICE_ADDRESS, CompanyTableMap::COL_INVOICE_COMPANY_NAME, CompanyTableMap::COL_INVOICE_STREET, CompanyTableMap::COL_INVOICE_NUMBER, CompanyTableMap::COL_INVOICE_NUMBER_ADD, CompanyTableMap::COL_INVOICE_POSTAL, CompanyTableMap::COL_INVOICE_CITY, CompanyTableMap::COL_INVOICE_COUNTRY_ID, CompanyTableMap::COL_HAS_DELIVERY_ADDRESS, CompanyTableMap::COL_DELIVERY_COMPANY_NAME, CompanyTableMap::COL_DELIVERY_STREET, CompanyTableMap::COL_DELIVERY_NUMBER, CompanyTableMap::COL_DELIVERY_NUMBER_ADD, CompanyTableMap::COL_DELIVERY_POSTAL, CompanyTableMap::COL_DELIVERY_CITY, CompanyTableMap::COL_DELIVERY_COUNTRY_ID, CompanyTableMap::COL_COMPANY_LOGO_ID, CompanyTableMap::COL_MAX_SHIPPING_COSTS, ),
        self::TYPE_FIELDNAME     => array('id', 'is_default_supplier', 'created_on', 'is_deleted', 'company_name', 'slogan', 'chamber_of_commerce', 'vat_number', 'legal_form_id', 'iban', 'bic', 'phone', 'fax', 'website', 'email', 'general_street', 'general_number', 'general_number_add', 'general_postal', 'general_city', 'general_po_box', 'general_country_id', 'has_invoice_address', 'invoice_company_name', 'invoice_street', 'invoice_number', 'invoice_number_add', 'invoice_postal', 'invoice_city', 'invoice_country_id', 'has_delivery_address', 'delivery_company_name', 'delivery_street', 'delivery_number', 'delivery_number_add', 'delivery_postal', 'delivery_city', 'delivery_country_id', 'company_logo_id', 'max_shipping_costs', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'IsDefaultSupplier' => 1, 'CreatedOn' => 2, 'ItemDeleted' => 3, 'CompanyName' => 4, 'Slogan' => 5, 'ChamberOfCommerce' => 6, 'VatNumber' => 7, 'LegalFormId' => 8, 'Iban' => 9, 'Bic' => 10, 'Phone' => 11, 'Fax' => 12, 'Website' => 13, 'Email' => 14, 'GeneralStreet' => 15, 'GeneralNumber' => 16, 'GeneralNumberAdd' => 17, 'GeneralPostal' => 18, 'GeneralCity' => 19, 'GeneralPoBox' => 20, 'GeneralCountryId' => 21, 'HasInvoiceAddress' => 22, 'InvoiceCompanyName' => 23, 'InvoiceStreet' => 24, 'InvoiceNumber' => 25, 'InvoiceNumberAdd' => 26, 'InvoicePostal' => 27, 'InvoiceCity' => 28, 'InvoiceCountryId' => 29, 'HasDeliveryAddress' => 30, 'DeliveryCompanyName' => 31, 'DeliveryStreet' => 32, 'DeliveryNumber' => 33, 'DeliveryNumberAdd' => 34, 'DeliveryPostal' => 35, 'DeliveryCity' => 36, 'DeliveryCountryId' => 37, 'CompanyLogoId' => 38, 'MaxShippingCosts' => 39, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'isDefaultSupplier' => 1, 'createdOn' => 2, 'itemDeleted' => 3, 'companyName' => 4, 'slogan' => 5, 'chamberOfCommerce' => 6, 'vatNumber' => 7, 'legalFormId' => 8, 'iban' => 9, 'bic' => 10, 'phone' => 11, 'fax' => 12, 'website' => 13, 'email' => 14, 'generalStreet' => 15, 'generalNumber' => 16, 'generalNumberAdd' => 17, 'generalPostal' => 18, 'generalCity' => 19, 'generalPoBox' => 20, 'generalCountryId' => 21, 'hasInvoiceAddress' => 22, 'invoiceCompanyName' => 23, 'invoiceStreet' => 24, 'invoiceNumber' => 25, 'invoiceNumberAdd' => 26, 'invoicePostal' => 27, 'invoiceCity' => 28, 'invoiceCountryId' => 29, 'hasDeliveryAddress' => 30, 'deliveryCompanyName' => 31, 'deliveryStreet' => 32, 'deliveryNumber' => 33, 'deliveryNumberAdd' => 34, 'deliveryPostal' => 35, 'deliveryCity' => 36, 'deliveryCountryId' => 37, 'companyLogoId' => 38, 'maxShippingCosts' => 39, ),
        self::TYPE_COLNAME       => array(CompanyTableMap::COL_ID => 0, CompanyTableMap::COL_IS_DEFAULT_SUPPLIER => 1, CompanyTableMap::COL_CREATED_ON => 2, CompanyTableMap::COL_IS_DELETED => 3, CompanyTableMap::COL_COMPANY_NAME => 4, CompanyTableMap::COL_SLOGAN => 5, CompanyTableMap::COL_CHAMBER_OF_COMMERCE => 6, CompanyTableMap::COL_VAT_NUMBER => 7, CompanyTableMap::COL_LEGAL_FORM_ID => 8, CompanyTableMap::COL_IBAN => 9, CompanyTableMap::COL_BIC => 10, CompanyTableMap::COL_PHONE => 11, CompanyTableMap::COL_FAX => 12, CompanyTableMap::COL_WEBSITE => 13, CompanyTableMap::COL_EMAIL => 14, CompanyTableMap::COL_GENERAL_STREET => 15, CompanyTableMap::COL_GENERAL_NUMBER => 16, CompanyTableMap::COL_GENERAL_NUMBER_ADD => 17, CompanyTableMap::COL_GENERAL_POSTAL => 18, CompanyTableMap::COL_GENERAL_CITY => 19, CompanyTableMap::COL_GENERAL_PO_BOX => 20, CompanyTableMap::COL_GENERAL_COUNTRY_ID => 21, CompanyTableMap::COL_HAS_INVOICE_ADDRESS => 22, CompanyTableMap::COL_INVOICE_COMPANY_NAME => 23, CompanyTableMap::COL_INVOICE_STREET => 24, CompanyTableMap::COL_INVOICE_NUMBER => 25, CompanyTableMap::COL_INVOICE_NUMBER_ADD => 26, CompanyTableMap::COL_INVOICE_POSTAL => 27, CompanyTableMap::COL_INVOICE_CITY => 28, CompanyTableMap::COL_INVOICE_COUNTRY_ID => 29, CompanyTableMap::COL_HAS_DELIVERY_ADDRESS => 30, CompanyTableMap::COL_DELIVERY_COMPANY_NAME => 31, CompanyTableMap::COL_DELIVERY_STREET => 32, CompanyTableMap::COL_DELIVERY_NUMBER => 33, CompanyTableMap::COL_DELIVERY_NUMBER_ADD => 34, CompanyTableMap::COL_DELIVERY_POSTAL => 35, CompanyTableMap::COL_DELIVERY_CITY => 36, CompanyTableMap::COL_DELIVERY_COUNTRY_ID => 37, CompanyTableMap::COL_COMPANY_LOGO_ID => 38, CompanyTableMap::COL_MAX_SHIPPING_COSTS => 39, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'is_default_supplier' => 1, 'created_on' => 2, 'is_deleted' => 3, 'company_name' => 4, 'slogan' => 5, 'chamber_of_commerce' => 6, 'vat_number' => 7, 'legal_form_id' => 8, 'iban' => 9, 'bic' => 10, 'phone' => 11, 'fax' => 12, 'website' => 13, 'email' => 14, 'general_street' => 15, 'general_number' => 16, 'general_number_add' => 17, 'general_postal' => 18, 'general_city' => 19, 'general_po_box' => 20, 'general_country_id' => 21, 'has_invoice_address' => 22, 'invoice_company_name' => 23, 'invoice_street' => 24, 'invoice_number' => 25, 'invoice_number_add' => 26, 'invoice_postal' => 27, 'invoice_city' => 28, 'invoice_country_id' => 29, 'has_delivery_address' => 30, 'delivery_company_name' => 31, 'delivery_street' => 32, 'delivery_number' => 33, 'delivery_number_add' => 34, 'delivery_postal' => 35, 'delivery_city' => 36, 'delivery_country_id' => 37, 'company_logo_id' => 38, 'max_shipping_costs' => 39, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('own_company');
        $this->setPhpName('Company');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Company\\Company');
        $this->setPackage('Model.Company');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('is_default_supplier', 'IsDefaultSupplier', 'BOOLEAN', false, 1, false);
        $this->addColumn('created_on', 'CreatedOn', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('is_deleted', 'ItemDeleted', 'BOOLEAN', false, 1, false);
        $this->addColumn('company_name', 'CompanyName', 'VARCHAR', false, 120, null);
        $this->addColumn('slogan', 'Slogan', 'VARCHAR', false, 255, null);
        $this->addColumn('chamber_of_commerce', 'ChamberOfCommerce', 'VARCHAR', false, 120, null);
        $this->addColumn('vat_number', 'VatNumber', 'VARCHAR', false, 120, null);
        $this->addForeignKey('legal_form_id', 'LegalFormId', 'INTEGER', 'mt_legal_form', 'id', false, null, null);
        $this->addColumn('iban', 'Iban', 'VARCHAR', false, 25, null);
        $this->addColumn('bic', 'Bic', 'VARCHAR', false, 25, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', false, 255, null);
        $this->addColumn('fax', 'Fax', 'VARCHAR', false, 255, null);
        $this->addColumn('website', 'Website', 'VARCHAR', false, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 150, null);
        $this->addColumn('general_street', 'GeneralStreet', 'VARCHAR', false, 255, null);
        $this->addColumn('general_number', 'GeneralNumber', 'VARCHAR', false, 250, null);
        $this->addColumn('general_number_add', 'GeneralNumberAdd', 'VARCHAR', false, 250, null);
        $this->addColumn('general_postal', 'GeneralPostal', 'VARCHAR', false, 250, null);
        $this->addColumn('general_city', 'GeneralCity', 'VARCHAR', false, 250, null);
        $this->addColumn('general_po_box', 'GeneralPoBox', 'VARCHAR', false, 250, null);
        $this->addForeignKey('general_country_id', 'GeneralCountryId', 'INTEGER', 'mt_country', 'id', false, null, null);
        $this->addColumn('has_invoice_address', 'HasInvoiceAddress', 'BOOLEAN', false, 1, false);
        $this->addColumn('invoice_company_name', 'InvoiceCompanyName', 'VARCHAR', false, 120, null);
        $this->addColumn('invoice_street', 'InvoiceStreet', 'VARCHAR', false, 255, null);
        $this->addColumn('invoice_number', 'InvoiceNumber', 'VARCHAR', false, 250, null);
        $this->addColumn('invoice_number_add', 'InvoiceNumberAdd', 'VARCHAR', false, 250, null);
        $this->addColumn('invoice_postal', 'InvoicePostal', 'VARCHAR', false, 250, null);
        $this->addColumn('invoice_city', 'InvoiceCity', 'VARCHAR', false, 250, null);
        $this->addForeignKey('invoice_country_id', 'InvoiceCountryId', 'INTEGER', 'mt_country', 'id', false, null, null);
        $this->addColumn('has_delivery_address', 'HasDeliveryAddress', 'BOOLEAN', false, 1, false);
        $this->addColumn('delivery_company_name', 'DeliveryCompanyName', 'VARCHAR', false, 120, null);
        $this->addColumn('delivery_street', 'DeliveryStreet', 'VARCHAR', false, 255, null);
        $this->addColumn('delivery_number', 'DeliveryNumber', 'VARCHAR', false, 25, null);
        $this->addColumn('delivery_number_add', 'DeliveryNumberAdd', 'VARCHAR', false, 25, null);
        $this->addColumn('delivery_postal', 'DeliveryPostal', 'VARCHAR', false, 25, null);
        $this->addColumn('delivery_city', 'DeliveryCity', 'VARCHAR', false, 25, null);
        $this->addForeignKey('delivery_country_id', 'DeliveryCountryId', 'INTEGER', 'mt_country', 'id', false, null, null);
        $this->addForeignKey('company_logo_id', 'CompanyLogoId', 'INTEGER', 'own_company_logo', 'id', false, null, null);
        $this->addColumn('max_shipping_costs', 'MaxShippingCosts', 'INTEGER', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('GeneralCountry', '\\Model\\Setting\\MasterTable\\Country', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':general_country_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('InvoiceCountry', '\\Model\\Setting\\MasterTable\\Country', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':invoice_country_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('DeliveryCountry', '\\Model\\Setting\\MasterTable\\Country', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':delivery_country_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('LegalForm', '\\Model\\Setting\\MasterTable\\LegalForm', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':legal_form_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('OwnCompanyLogo', '\\Model\\Company\\CompanyLogo', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':company_logo_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('CompanyLogoRelatedByOwnCompanyId', '\\Model\\Company\\CompanyLogo', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':own_company_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'CompanyLogosRelatedByOwnCompanyId', false);
        $this->addRelation('Site', '\\Model\\Cms\\Site', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':invoicing_company_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'Sites', false);
        $this->addRelation('CompanySetting', '\\Model\\Company\\CompanySetting', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':own_company_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'CompanySettings', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to own_company     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CompanyLogoTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CompanyTableMap::CLASS_DEFAULT : CompanyTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Company object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CompanyTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CompanyTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CompanyTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CompanyTableMap::OM_CLASS;
            /** @var Company $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CompanyTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CompanyTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CompanyTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Company $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CompanyTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CompanyTableMap::COL_ID);
            $criteria->addSelectColumn(CompanyTableMap::COL_IS_DEFAULT_SUPPLIER);
            $criteria->addSelectColumn(CompanyTableMap::COL_CREATED_ON);
            $criteria->addSelectColumn(CompanyTableMap::COL_IS_DELETED);
            $criteria->addSelectColumn(CompanyTableMap::COL_COMPANY_NAME);
            $criteria->addSelectColumn(CompanyTableMap::COL_SLOGAN);
            $criteria->addSelectColumn(CompanyTableMap::COL_CHAMBER_OF_COMMERCE);
            $criteria->addSelectColumn(CompanyTableMap::COL_VAT_NUMBER);
            $criteria->addSelectColumn(CompanyTableMap::COL_LEGAL_FORM_ID);
            $criteria->addSelectColumn(CompanyTableMap::COL_IBAN);
            $criteria->addSelectColumn(CompanyTableMap::COL_BIC);
            $criteria->addSelectColumn(CompanyTableMap::COL_PHONE);
            $criteria->addSelectColumn(CompanyTableMap::COL_FAX);
            $criteria->addSelectColumn(CompanyTableMap::COL_WEBSITE);
            $criteria->addSelectColumn(CompanyTableMap::COL_EMAIL);
            $criteria->addSelectColumn(CompanyTableMap::COL_GENERAL_STREET);
            $criteria->addSelectColumn(CompanyTableMap::COL_GENERAL_NUMBER);
            $criteria->addSelectColumn(CompanyTableMap::COL_GENERAL_NUMBER_ADD);
            $criteria->addSelectColumn(CompanyTableMap::COL_GENERAL_POSTAL);
            $criteria->addSelectColumn(CompanyTableMap::COL_GENERAL_CITY);
            $criteria->addSelectColumn(CompanyTableMap::COL_GENERAL_PO_BOX);
            $criteria->addSelectColumn(CompanyTableMap::COL_GENERAL_COUNTRY_ID);
            $criteria->addSelectColumn(CompanyTableMap::COL_HAS_INVOICE_ADDRESS);
            $criteria->addSelectColumn(CompanyTableMap::COL_INVOICE_COMPANY_NAME);
            $criteria->addSelectColumn(CompanyTableMap::COL_INVOICE_STREET);
            $criteria->addSelectColumn(CompanyTableMap::COL_INVOICE_NUMBER);
            $criteria->addSelectColumn(CompanyTableMap::COL_INVOICE_NUMBER_ADD);
            $criteria->addSelectColumn(CompanyTableMap::COL_INVOICE_POSTAL);
            $criteria->addSelectColumn(CompanyTableMap::COL_INVOICE_CITY);
            $criteria->addSelectColumn(CompanyTableMap::COL_INVOICE_COUNTRY_ID);
            $criteria->addSelectColumn(CompanyTableMap::COL_HAS_DELIVERY_ADDRESS);
            $criteria->addSelectColumn(CompanyTableMap::COL_DELIVERY_COMPANY_NAME);
            $criteria->addSelectColumn(CompanyTableMap::COL_DELIVERY_STREET);
            $criteria->addSelectColumn(CompanyTableMap::COL_DELIVERY_NUMBER);
            $criteria->addSelectColumn(CompanyTableMap::COL_DELIVERY_NUMBER_ADD);
            $criteria->addSelectColumn(CompanyTableMap::COL_DELIVERY_POSTAL);
            $criteria->addSelectColumn(CompanyTableMap::COL_DELIVERY_CITY);
            $criteria->addSelectColumn(CompanyTableMap::COL_DELIVERY_COUNTRY_ID);
            $criteria->addSelectColumn(CompanyTableMap::COL_COMPANY_LOGO_ID);
            $criteria->addSelectColumn(CompanyTableMap::COL_MAX_SHIPPING_COSTS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.is_default_supplier');
            $criteria->addSelectColumn($alias . '.created_on');
            $criteria->addSelectColumn($alias . '.is_deleted');
            $criteria->addSelectColumn($alias . '.company_name');
            $criteria->addSelectColumn($alias . '.slogan');
            $criteria->addSelectColumn($alias . '.chamber_of_commerce');
            $criteria->addSelectColumn($alias . '.vat_number');
            $criteria->addSelectColumn($alias . '.legal_form_id');
            $criteria->addSelectColumn($alias . '.iban');
            $criteria->addSelectColumn($alias . '.bic');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.fax');
            $criteria->addSelectColumn($alias . '.website');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.general_street');
            $criteria->addSelectColumn($alias . '.general_number');
            $criteria->addSelectColumn($alias . '.general_number_add');
            $criteria->addSelectColumn($alias . '.general_postal');
            $criteria->addSelectColumn($alias . '.general_city');
            $criteria->addSelectColumn($alias . '.general_po_box');
            $criteria->addSelectColumn($alias . '.general_country_id');
            $criteria->addSelectColumn($alias . '.has_invoice_address');
            $criteria->addSelectColumn($alias . '.invoice_company_name');
            $criteria->addSelectColumn($alias . '.invoice_street');
            $criteria->addSelectColumn($alias . '.invoice_number');
            $criteria->addSelectColumn($alias . '.invoice_number_add');
            $criteria->addSelectColumn($alias . '.invoice_postal');
            $criteria->addSelectColumn($alias . '.invoice_city');
            $criteria->addSelectColumn($alias . '.invoice_country_id');
            $criteria->addSelectColumn($alias . '.has_delivery_address');
            $criteria->addSelectColumn($alias . '.delivery_company_name');
            $criteria->addSelectColumn($alias . '.delivery_street');
            $criteria->addSelectColumn($alias . '.delivery_number');
            $criteria->addSelectColumn($alias . '.delivery_number_add');
            $criteria->addSelectColumn($alias . '.delivery_postal');
            $criteria->addSelectColumn($alias . '.delivery_city');
            $criteria->addSelectColumn($alias . '.delivery_country_id');
            $criteria->addSelectColumn($alias . '.company_logo_id');
            $criteria->addSelectColumn($alias . '.max_shipping_costs');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CompanyTableMap::DATABASE_NAME)->getTable(CompanyTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CompanyTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CompanyTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CompanyTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Company or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Company object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompanyTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Company\Company) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CompanyTableMap::DATABASE_NAME);
            $criteria->add(CompanyTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CompanyQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CompanyTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CompanyTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the own_company table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CompanyQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Company or Criteria object.
     *
     * @param mixed               $criteria Criteria or Company object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CompanyTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Company object
        }

        if ($criteria->containsKey(CompanyTableMap::COL_ID) && $criteria->keyContainsValue(CompanyTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CompanyTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CompanyQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CompanyTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CompanyTableMap::buildTableMap();
