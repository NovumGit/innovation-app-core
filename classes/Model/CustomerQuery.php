<?php

namespace Model;

use Core\Cache;
use Core\QueryMapper;
use Model\Base\ProductQuery as BaseProductQuery;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * Skeleton subclass for performing query and update operations on the 'product' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerQuery extends BaseProductQuery
{

}
