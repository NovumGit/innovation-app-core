<?php

namespace Model;

use Core\Translate;
use Model\Base\ContactMessage as BaseContactMessage;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * Skeleton subclass for representing a row from the 'contact_message' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ContactMessage extends BaseContactMessage
{
    function save(ConnectionInterface $con = null)
    {
        if(!$this->getId())
        {
            $httpReferer = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : null;

            if(!$this->getFormUrl() && isset($_SERVER['REQUEST_URI']) && isset($_SERVER['HTTP_HOST']))
            {
                $this->setFormUrl($_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
            }
            if(!$this->getReferrerSite())
            {
                $this->setReferrerSite(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : null);
            }
            if(!$this->getHttpReferrer())
            {
                $this->setHttpReferrer($httpReferer);
            }
            if(!$this->getRemoteAddr())
            {
                $this->setRemoteAddr(isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null);
            }
            if(!$this->getHttpUserAgent())
            {
                $this->setHttpUserAgent(isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : null);
            }
            if(!$this->getCreatedDate())
            {
                $this->setCreatedDate(time());
            }


            if(!$this->getLanguage())
            {
                $this->setLanguageId(Translate::getCurrentLocale('Id'));
            }

        }

        parent::save($con);
    }

}
