<?php

namespace Model;

use Model\Base\PromoProductTranslation as BasePromoProductTranslation;

/**
 * Skeleton subclass for representing a row from the 'promo_product_translation' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PromoProductTranslation extends BasePromoProductTranslation
{
    function getImagePath()
    {
        if($this->getHasImage())
        {
            return '/img/promo/'.$this->getPromoProductId().'/'.$this->getId().'.'.$this->getImageExt();
        }
        else
        {

        }

    }


}
