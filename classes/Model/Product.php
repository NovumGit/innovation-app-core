<?php

namespace Model;

use Core\Cfg;
use Core\ModelDecorator\IPostSave;
use Core\Setting;
use Core\Utils;
use Core\Customer;
use Exception\LogicException;
use Model\Base\Product as BaseProduct;
use Model\Category\CategoryTranslationQuery;
use Model\Category\ProductMultiCategory;
use Model\Category\ProductMultiCategoryQuery;
use Model\Product\Image\ProductImage;
use Model\Product\Image\ProductImageQuery;
use Model\Product\RelatedProductQuery;
use Model\Setting\MasterTable\ColorTranslation;
use Model\Setting\MasterTable\ColorTranslationQuery;
use Model\Setting\MasterTable\ProductUnitQuery;
use Model\Setting\MasterTable\ProductUnitTranslation;
use Model\Setting\MasterTable\ProductUnitTranslationQuery;
use Model\Setting\MasterTable\UsageTranslationQuery;
use Model\Setting\MasterTable\VatQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * Skeleton subclass for representing a row from the 'product' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Product extends BaseProduct
{

    private static $lastOwnImage;
    private $aArguments = [];

    function postSave(ConnectionInterface $con = null)
    {
        $oPostSave = Cfg::get('POST_PRODUCT_SAVE');
        if($oPostSave instanceof IPostSave)
        {
            $oPostSave->postSave($this, $con);
        }
    }

    function postDelete(ConnectionInterface $con = null)
    {

    }

    public function setArgument($sName, $sValue)
    {
        $this->aArguments[$sName] = $sValue;
    }
    public function getArgument($sName)
    {
        if(isset($this->aArguments[$sName]))
        {
            return $this->aArguments[$sName];
        }
        return null;
    }


    function getRelatedProducts()
    {
        if(!$this->getId())
        {
            return null;
        }
        $aRelatedProducts = RelatedProductQuery::create()->findByProductId($this->getId());


        if($aRelatedProducts->isEmpty())
        {
            return null;
        }

        $aOut = [];
        foreach($aRelatedProducts as $oRelatedProduct)
        {
            $aOut[] = CustomerQuery::create()->findOneById($oRelatedProduct->getOtherProductId());

        }
        return $aOut;

    }
    function save(ConnectionInterface $con = null)
    {
        if(!$this->getId())
        {
            $this->setHasImages(false);
        }
        else
        {
            $oProductImage = ProductImageQuery::create()->findOneByProductId($this->getId());

            if($oProductImage instanceof ProductImage)
            {
                $this->setHasImages(true);
            }

        }
        $oProduct = parent::save($con);
        return $oProduct;
    }

    function getItemDeleted()
    {
        return ($this->getDeletedByUserId() ? true : false);
    }
    function getParentProductUrl()
    {

        $oProduct = CustomerQuery::create()->findOneById($this->getDerrivedFromId());

        if($oProduct instanceof Product)
        {
            return '/p/' . Utils::slugify($oProduct->getTitle()) . '-' . $oProduct->getId();
        }
        return null;
    }
    function setPropertyByKey($sKey, $sValue)
    {
        $oProductPropertyQuery = ProductPropertyQuery::create();
        $oProductPropertyQuery->filterByField($sKey);
        $oProductPropertyQuery->filterByProductId($this->getId());
        $oProductProperty = $oProductPropertyQuery->findOne();

        if(!$oProductProperty instanceof ProductProperty)
        {
            $oProductProperty = new ProductProperty();
            $oProductProperty->setField($sKey);
            $oProductProperty->setProductId($this->getId());
        }
        $oProductProperty->setValue($sValue);
        $oProductProperty->save();
    }
    function getPropertyByKey($sKey)
    {
        if(!$this->getId())
        {
            return null;
        }
        $oProductPropertyQuery = ProductPropertyQuery::create();
        $oProductPropertyQuery->filterByField($sKey);
        $oProductPropertyQuery->filterByProductId($this->getId());
        $oProductProperty = $oProductPropertyQuery->findOne();

        if($oProductProperty instanceof ProductProperty)
        {
            return $oProductProperty->getValue();
        }
        return null;

    }

    /**
     * @param boolean|integer $mShortenUrlMaxChars
     * @return string
     */
    function getProductUrl($mShortenUrlMaxChars = false)
    {
        $funcCustomslugFunction = Cfg::get('PRODUCT_SLUG_FUNCTION');

        if(is_callable($funcCustomslugFunction))
        {
            return $funcCustomslugFunction($this);
        }

        $sSluggedTitle = Utils::slugify($this->getTitle());
        if($mShortenUrlMaxChars !== false)
        {
            $sSluggedTitle = substr($sSluggedTitle, 0, $mShortenUrlMaxChars);
        }

        return '/p/' . $sSluggedTitle . '-' . $this->getId();
    }

    function hasProductProperties()
    {
        return $this->getProductProperties()->count() > 0;
    }
    function getTranslatedUsageList($iLanguageId)
    {
        $aProductUsages = $this->getProductUsages();


        $aOut = [];
        foreach($aProductUsages as $oProductUsage)
        {
            $oUsage = $oProductUsage->getUsage();

            $oUsageTranslationQuery = UsageTranslationQuery::create();
            $oUsageTranslationQuery->filterByUsageId($oUsage->getId());
            $oUsageTranslationQuery->filterByLanguageId($iLanguageId);
            $oUsageTranslation =  $oUsageTranslationQuery->findOne();

            $aOut[] = $oUsageTranslation->getName();
        }

        if(!empty($aOut))
        {
            return join(', ', $aOut);
        }

        return '';
    }
    function getFirstTranslatedMultiCategory($iLanguageId)
    {
        $oProductMultiCategoryQuery = ProductMultiCategoryQuery::create();
        $oProductMultiCategoryQuery->filterByProductId($this->getId());
        $oProductMultiCategory = $oProductMultiCategoryQuery->findOne();

        if($oProductMultiCategory instanceof ProductMultiCategory)
        {
            $oCategoryTranslationQuery = CategoryTranslationQuery::create();
            $oCategoryTranslationQuery->filterByCategoryId($oProductMultiCategory->getCategoryId());
            $oCategoryTranslationQuery->filterByLanguageId($iLanguageId);
            $oCategoryTranslation = $oCategoryTranslationQuery->findOne();
            return $oCategoryTranslation->getName();
        }
        return null;
    }
    function getTranslatedMainCategory($iLanguageId)
    {
        $oCategory = $this->getCategory();
        $oCategoryTranslationQuery = CategoryTranslationQuery::create();
        $oCategoryTranslationQuery->filterByCategoryId($oCategory->getId());
        $oCategoryTranslationQuery->filterByLanguageId($iLanguageId);
        $oCategoryTranslation = $oCategoryTranslationQuery->findOne();

        return $oCategoryTranslation->getName();
    }
    function getTranslatedColorList($iLanguageId)
    {
        $aProductColors = $this->getProductColors();
        $aOut = [];
        foreach($aProductColors as $oProductColor)
        {
            $oColor = $oProductColor->getColor();

            $oColorTranslationQuery = ColorTranslationQuery::create();
            $oColorTranslationQuery->filterByColorId($oColor->getId());
            $oColorTranslationQuery->filterByLanguageId($iLanguageId);
            $oColorTranslation = $oColorTranslationQuery->findOne();

            if($oColorTranslation instanceof ColorTranslation)
            {
                $aOut[] = $oColorTranslation->getName();
            }
            else
            {
                $aOut[] = $oColor->getName();
            }
        }
        if(!empty($aOut))
        {
            return join(', ', $aOut);
        }
        return '';
    }
    static function generateNexProductNumber()
    {
        $bCustomerAutoProductNumbering = Setting::get('customer_auto_product_numbering') == '1' ? true : false;


        if(!$bCustomerAutoProductNumbering)
        {
            return null;
        }

        $sDbNextProductNumberWithPrefix = Setting::get('customer_next_product_number');
        $iDbNextProductNumberOriginal = 0;
        if (preg_match('/[0-9]+$/', $sDbNextProductNumberWithPrefix, $aParts))
        {
            $iDbNextProductNumberOriginal = $aParts[0];
        }
        $iNewNextProductNumber = $iDbNextProductNumberOriginal;

        $i=0;
        while(true)
        {
            $i++;

            $sRegex = '/'.$iDbNextProductNumberOriginal.'$/';

            $iNewNextProductNumber++;
            $sNewProductNumberWithPrefix = preg_replace($sRegex, $iNewNextProductNumber, $sDbNextProductNumberWithPrefix);

            $oProduct = CustomerQuery::create()->findOneByNumber($sNewProductNumberWithPrefix);


            if(!$oProduct instanceof Product)
            {
                break;
            }
        }
        if(!isset($sNewProductNumberWithPrefix))
        {
            throw new LogicException("The variable \$sNewProductNumberWithPrefix is not set which is a situation that should never occur and this implicates a bug.");
        }
        Setting::store('customer_next_debitor_number', $sNewProductNumberWithPrefix);
        return $sNewProductNumberWithPrefix;
    }
    function getUnitCode()
    {
        if($this->getUnitId())
        {
            $oProductUnit = ProductUnitQuery::create()->findOneById($this->getUnitId());
            return $oProductUnit->getCode();
        }
        else
        {
            return null;
        }

    }
    function getStockQuantity()
    {
        if($this->getVirtualStock())
        {
            return 1;
        }
        return parent::getStockQuantity(); // TODO: Change the autogenerated stub
    }

    /*
     * Geeft de sale_price weer zoals hij is, dus zonder discount percentage
     */
    public function getOriginalSalePrice()
    {
        $fSalePrice = parent::getSalePrice();

        if(Customer::isSignedIn())
        {
            $oCustomer = Customer::getMember();

            if($oCustomer->getIsIntracommunautair())
            {
                $oVat = parent::getVat();
                $fSalePrice = ($fSalePrice / (100 + $oVat->getPercentage())) * 100;
            }
        }
        return $fSalePrice;
    }


    /**
     * Get the [sale_price] column value.
     *
     * @return double
     */
    public function getSalePrice()
    {
        $fSalePrice = $this->getOriginalSalePrice();

        $sModifierFunction = Cfg::get('SALE_PRICE_MODIFIER');
        if($sModifierFunction && is_callable($sModifierFunction))
        {
            $fSalePrice = $sModifierFunction($fSalePrice);
        }

        if($this->getDiscountPercentage())
        {
            $fSalePrice = ($fSalePrice / 100) * (100 - $this->getDiscountPercentage());
        }
        return $fSalePrice;
    }

    function getVat(ConnectionInterface $con = null)
    {
        $oVat = parent::getVat();
        if(Customer::isSignedIn())
        {
            $oCustomer = Customer::getMember();

            if($oCustomer->getIsIntracommunautair())
            {
                $oVatQuery = VatQuery::create();
                $oVat = $oVatQuery->findOneByPercentage(0);
            }
        }
        return $oVat;
    }
    function deleteWithImages()
    {
        $aImages = $this->getImages();

        if(!is_null($aImages) && !$aImages->isEmpty())
        {
            foreach($aImages as $oImage)
            {
                $oImage->deleteWithFile();
            }
        }
        $this->delete();
    }
    function hasDerrrivedProducts()
    {
        return (CustomerQuery::create()->findByDerrivedFromId($this->getId())->count() > 0);
    }
    function hasImages()
    {
    	return $this->getHasImages();
    }

    function getImages()
    {
        if($this->getDerrivedFromId())
        {
            $iProductId = $this->getDerrivedFromId();
        }
        else
        {
            $iProductId = $this->getId();
        }
        $aImages = ProductImageQuery::create()->orderBySorting()->findByProductId($iProductId);

        if($aImages->isEmpty())
        {
            return null;
        }

        return $aImages;
    }
    function getTranslatedUnitName($iLanguageId)
    {

        $sTranslatedUnit = '';
        if($this->getUnitId())
        {

            $oProductUnitTranslation = ProductUnitTranslationQuery::create()->filterByLanguageId($iLanguageId)->filterByUnitId($this->getUnitId())->findOne();

            if($oProductUnitTranslation instanceof ProductUnitTranslation)
            {
                $sTranslatedUnit = $oProductUnitTranslation->getName();
            }
        }
        return $sTranslatedUnit;
    }

    function getPrimaryImageThumbPath($iWidth = 300, $iHeight = 300)
    {

        $oPrimaryImage = $this->getPrimaryImage();

        if ($oPrimaryImage instanceof ProductImage)
        {
            return '/img/product/'.$iWidth.'x'.$iHeight.'/'.$oPrimaryImage->getId().'.'.$oPrimaryImage->getFileExt();
        }
        return null;

    }
    function getLastOwnImageThumbPath($iWidth = 300, $iHeight = 300)
    {

        $oLastOwnImage = $this->getLastOwnImage();

        if ($oLastOwnImage instanceof ProductImage)
        {
            return '/img/product/'.$iWidth.'x'.$iHeight.'/'.$oLastOwnImage->getId().'.'.$oLastOwnImage->getFileExt();
        }
        return null;
    }
    /**
     * Wanneer derrived products een eigen afbeelding hebben (kleuren bijvoorbeeld)
     */
    function getLastOwnImage()
    {
    	if(isset(self::$lastOwnImage[$this->getId()]))
	    {
	    	return self::$lastOwnImage[$this->getId()];
	    };

	    $oProductImageQuery = ProductImageQuery::create();
	    $oProductImageQuery->setQueryKey('ProductImageQuery-orderBySorting-findOneByProductId');
		$oProductImageQuery->orderBySorting(Criteria::DESC);
	    $oImage = $oProductImageQuery->findOneByProductId($this->getId());

        if($oImage instanceof ProductImage)
        {
	        self::$lastOwnImage[$this->getId()] = $oImage;
            return $oImage;
        }
        return null;
    }
    function getPrimaryImage()
    {

        if($this->getDerrivedFromId())
        {
            $iProductId = $this->getDerrivedFromId();
        }
        else
        {
            $iProductId = $this->getId();
        }
        $oImage = ProductImageQuery::create()->orderBySorting()->findOneByProductId($iProductId);
        if($oImage instanceof ProductImage)
        {
            return $oImage;
        }
        return null;
    }
    function getDerrivedProducts()
    {
        $oProductQuery = CustomerQuery::create();
        $oProductQuery->orderByNumber();
        $aProducts = $oProductQuery->findByDerrivedFromId($this->getId());
        return $aProducts;
    }
    function getTranslatedTitleShortened($iLanguageId, $iMaxLength = 50)
    {
        $sTranslatedTitle = $this->getTranslatedTitle($iLanguageId);

        return Utils::shortenByWord($sTranslatedTitle, $iMaxLength);
    }
    function getTranslatedTitle($iLanguageId)
    {
        $oTranslation = $this->getTranslation($iLanguageId);
        $sTitle = $oTranslation->getTitle();

        if(empty($sTitle))
        {
            $sTitle = $this->getTitle();
        }

        return $sTitle;
    }
    function getTranslatedShortDescription($iLanguageId)
    {
        return $this->getTranslation($iLanguageId)->getShortDescription();
    }
    function getTranslatedDescription($iLanguageId)
    {
        return $this->getTranslation($iLanguageId)->getDescription();
    }
    function getAverageRatingStars()
    {
        if($this->getCalculatedAverageRating() == 0)
        {
            return null;
        }
        return range(1, $this->getCalculatedAverageRating());
    }
    private function getTranslation($iLanguageId):ProductTranslation
    {
        if($this->getDerrivedFromId())
        {
            $iProductId = $this->getDerrivedFromId();
        }
        else
        {
            $iProductId = $this->getId();
        }
        $oProductTranslation =  ProductTranslationQuery::create()
                    ->filterByLanguageId($iLanguageId)
                    ->filterByProductId($iProductId)
                    ->findOne();
        if($oProductTranslation instanceof ProductTranslation)
        {
            return $oProductTranslation;
        }
        return new ProductTranslation();
    }
}
