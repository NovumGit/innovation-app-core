<?php

namespace Model\Supplier;

use Model\Supplier\Base\SupplierPropertyQuery as BaseSupplierPropertyQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'supplier_property' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SupplierPropertyQuery extends BaseSupplierPropertyQuery
{

}
