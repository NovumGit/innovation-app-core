<?php

namespace Model\Supplier\Base;

use \Exception;
use \PDO;
use Model\Product;
use Model\Account\User;
use Model\Finance\PurchaseInvoice;
use Model\Supplier\Supplier as ChildSupplier;
use Model\Supplier\SupplierQuery as ChildSupplierQuery;
use Model\Supplier\Map\SupplierTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'supplier' table.
 *
 *
 *
 * @method     ChildSupplierQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSupplierQuery orderByOldId($order = Criteria::ASC) Order by the old_id column
 * @method     ChildSupplierQuery orderByNumber($order = Criteria::ASC) Order by the number column
 * @method     ChildSupplierQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildSupplierQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildSupplierQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildSupplierQuery orderByCity($order = Criteria::ASC) Order by the city column
 * @method     ChildSupplierQuery orderByLatitude($order = Criteria::ASC) Order by the latitude column
 * @method     ChildSupplierQuery orderByLongitude($order = Criteria::ASC) Order by the longitude column
 * @method     ChildSupplierQuery orderByAccountManagerId($order = Criteria::ASC) Order by the account_manager_id column
 *
 * @method     ChildSupplierQuery groupById() Group by the id column
 * @method     ChildSupplierQuery groupByOldId() Group by the old_id column
 * @method     ChildSupplierQuery groupByNumber() Group by the number column
 * @method     ChildSupplierQuery groupByName() Group by the name column
 * @method     ChildSupplierQuery groupByEmail() Group by the email column
 * @method     ChildSupplierQuery groupByPhone() Group by the phone column
 * @method     ChildSupplierQuery groupByCity() Group by the city column
 * @method     ChildSupplierQuery groupByLatitude() Group by the latitude column
 * @method     ChildSupplierQuery groupByLongitude() Group by the longitude column
 * @method     ChildSupplierQuery groupByAccountManagerId() Group by the account_manager_id column
 *
 * @method     ChildSupplierQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSupplierQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSupplierQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSupplierQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSupplierQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSupplierQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSupplierQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildSupplierQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildSupplierQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildSupplierQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildSupplierQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildSupplierQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildSupplierQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     ChildSupplierQuery leftJoinSupplierProperty($relationAlias = null) Adds a LEFT JOIN clause to the query using the SupplierProperty relation
 * @method     ChildSupplierQuery rightJoinSupplierProperty($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SupplierProperty relation
 * @method     ChildSupplierQuery innerJoinSupplierProperty($relationAlias = null) Adds a INNER JOIN clause to the query using the SupplierProperty relation
 *
 * @method     ChildSupplierQuery joinWithSupplierProperty($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SupplierProperty relation
 *
 * @method     ChildSupplierQuery leftJoinWithSupplierProperty() Adds a LEFT JOIN clause and with to the query using the SupplierProperty relation
 * @method     ChildSupplierQuery rightJoinWithSupplierProperty() Adds a RIGHT JOIN clause and with to the query using the SupplierProperty relation
 * @method     ChildSupplierQuery innerJoinWithSupplierProperty() Adds a INNER JOIN clause and with to the query using the SupplierProperty relation
 *
 * @method     ChildSupplierQuery leftJoinProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the Product relation
 * @method     ChildSupplierQuery rightJoinProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Product relation
 * @method     ChildSupplierQuery innerJoinProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the Product relation
 *
 * @method     ChildSupplierQuery joinWithProduct($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Product relation
 *
 * @method     ChildSupplierQuery leftJoinWithProduct() Adds a LEFT JOIN clause and with to the query using the Product relation
 * @method     ChildSupplierQuery rightJoinWithProduct() Adds a RIGHT JOIN clause and with to the query using the Product relation
 * @method     ChildSupplierQuery innerJoinWithProduct() Adds a INNER JOIN clause and with to the query using the Product relation
 *
 * @method     ChildSupplierQuery leftJoinPurchaseInvoice($relationAlias = null) Adds a LEFT JOIN clause to the query using the PurchaseInvoice relation
 * @method     ChildSupplierQuery rightJoinPurchaseInvoice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PurchaseInvoice relation
 * @method     ChildSupplierQuery innerJoinPurchaseInvoice($relationAlias = null) Adds a INNER JOIN clause to the query using the PurchaseInvoice relation
 *
 * @method     ChildSupplierQuery joinWithPurchaseInvoice($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PurchaseInvoice relation
 *
 * @method     ChildSupplierQuery leftJoinWithPurchaseInvoice() Adds a LEFT JOIN clause and with to the query using the PurchaseInvoice relation
 * @method     ChildSupplierQuery rightJoinWithPurchaseInvoice() Adds a RIGHT JOIN clause and with to the query using the PurchaseInvoice relation
 * @method     ChildSupplierQuery innerJoinWithPurchaseInvoice() Adds a INNER JOIN clause and with to the query using the PurchaseInvoice relation
 *
 * @method     \Model\Account\UserQuery|\Model\Supplier\SupplierPropertyQuery|\Model\ProductQuery|\Model\Finance\PurchaseInvoiceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSupplier findOne(ConnectionInterface $con = null) Return the first ChildSupplier matching the query
 * @method     ChildSupplier findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSupplier matching the query, or a new ChildSupplier object populated from the query conditions when no match is found
 *
 * @method     ChildSupplier findOneById(int $id) Return the first ChildSupplier filtered by the id column
 * @method     ChildSupplier findOneByOldId(int $old_id) Return the first ChildSupplier filtered by the old_id column
 * @method     ChildSupplier findOneByNumber(string $number) Return the first ChildSupplier filtered by the number column
 * @method     ChildSupplier findOneByName(string $name) Return the first ChildSupplier filtered by the name column
 * @method     ChildSupplier findOneByEmail(string $email) Return the first ChildSupplier filtered by the email column
 * @method     ChildSupplier findOneByPhone(string $phone) Return the first ChildSupplier filtered by the phone column
 * @method     ChildSupplier findOneByCity(string $city) Return the first ChildSupplier filtered by the city column
 * @method     ChildSupplier findOneByLatitude(string $latitude) Return the first ChildSupplier filtered by the latitude column
 * @method     ChildSupplier findOneByLongitude(string $longitude) Return the first ChildSupplier filtered by the longitude column
 * @method     ChildSupplier findOneByAccountManagerId(int $account_manager_id) Return the first ChildSupplier filtered by the account_manager_id column *

 * @method     ChildSupplier requirePk($key, ConnectionInterface $con = null) Return the ChildSupplier by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSupplier requireOne(ConnectionInterface $con = null) Return the first ChildSupplier matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSupplier requireOneById(int $id) Return the first ChildSupplier filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSupplier requireOneByOldId(int $old_id) Return the first ChildSupplier filtered by the old_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSupplier requireOneByNumber(string $number) Return the first ChildSupplier filtered by the number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSupplier requireOneByName(string $name) Return the first ChildSupplier filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSupplier requireOneByEmail(string $email) Return the first ChildSupplier filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSupplier requireOneByPhone(string $phone) Return the first ChildSupplier filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSupplier requireOneByCity(string $city) Return the first ChildSupplier filtered by the city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSupplier requireOneByLatitude(string $latitude) Return the first ChildSupplier filtered by the latitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSupplier requireOneByLongitude(string $longitude) Return the first ChildSupplier filtered by the longitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSupplier requireOneByAccountManagerId(int $account_manager_id) Return the first ChildSupplier filtered by the account_manager_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSupplier[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSupplier objects based on current ModelCriteria
 * @method     ChildSupplier[]|ObjectCollection findById(int $id) Return ChildSupplier objects filtered by the id column
 * @method     ChildSupplier[]|ObjectCollection findByOldId(int $old_id) Return ChildSupplier objects filtered by the old_id column
 * @method     ChildSupplier[]|ObjectCollection findByNumber(string $number) Return ChildSupplier objects filtered by the number column
 * @method     ChildSupplier[]|ObjectCollection findByName(string $name) Return ChildSupplier objects filtered by the name column
 * @method     ChildSupplier[]|ObjectCollection findByEmail(string $email) Return ChildSupplier objects filtered by the email column
 * @method     ChildSupplier[]|ObjectCollection findByPhone(string $phone) Return ChildSupplier objects filtered by the phone column
 * @method     ChildSupplier[]|ObjectCollection findByCity(string $city) Return ChildSupplier objects filtered by the city column
 * @method     ChildSupplier[]|ObjectCollection findByLatitude(string $latitude) Return ChildSupplier objects filtered by the latitude column
 * @method     ChildSupplier[]|ObjectCollection findByLongitude(string $longitude) Return ChildSupplier objects filtered by the longitude column
 * @method     ChildSupplier[]|ObjectCollection findByAccountManagerId(int $account_manager_id) Return ChildSupplier objects filtered by the account_manager_id column
 * @method     ChildSupplier[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SupplierQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Supplier\Base\SupplierQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Supplier\\Supplier', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSupplierQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSupplierQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSupplierQuery) {
            return $criteria;
        }
        $query = new ChildSupplierQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSupplier|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SupplierTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SupplierTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSupplier A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, old_id, number, name, email, phone, city, latitude, longitude, account_manager_id FROM supplier WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSupplier $obj */
            $obj = new ChildSupplier();
            $obj->hydrate($row);
            SupplierTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSupplier|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SupplierTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SupplierTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SupplierTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SupplierTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SupplierTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the old_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldId(1234); // WHERE old_id = 1234
     * $query->filterByOldId(array(12, 34)); // WHERE old_id IN (12, 34)
     * $query->filterByOldId(array('min' => 12)); // WHERE old_id > 12
     * </code>
     *
     * @param     mixed $oldId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByOldId($oldId = null, $comparison = null)
    {
        if (is_array($oldId)) {
            $useMinMax = false;
            if (isset($oldId['min'])) {
                $this->addUsingAlias(SupplierTableMap::COL_OLD_ID, $oldId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($oldId['max'])) {
                $this->addUsingAlias(SupplierTableMap::COL_OLD_ID, $oldId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SupplierTableMap::COL_OLD_ID, $oldId, $comparison);
    }

    /**
     * Filter the query on the number column
     *
     * Example usage:
     * <code>
     * $query->filterByNumber('fooValue');   // WHERE number = 'fooValue'
     * $query->filterByNumber('%fooValue%', Criteria::LIKE); // WHERE number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $number The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByNumber($number = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($number)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SupplierTableMap::COL_NUMBER, $number, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SupplierTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SupplierTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%', Criteria::LIKE); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SupplierTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the city column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE city = 'fooValue'
     * $query->filterByCity('%fooValue%', Criteria::LIKE); // WHERE city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SupplierTableMap::COL_CITY, $city, $comparison);
    }

    /**
     * Filter the query on the latitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLatitude('fooValue');   // WHERE latitude = 'fooValue'
     * $query->filterByLatitude('%fooValue%', Criteria::LIKE); // WHERE latitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $latitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByLatitude($latitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($latitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SupplierTableMap::COL_LATITUDE, $latitude, $comparison);
    }

    /**
     * Filter the query on the longitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLongitude('fooValue');   // WHERE longitude = 'fooValue'
     * $query->filterByLongitude('%fooValue%', Criteria::LIKE); // WHERE longitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $longitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByLongitude($longitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($longitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SupplierTableMap::COL_LONGITUDE, $longitude, $comparison);
    }

    /**
     * Filter the query on the account_manager_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAccountManagerId(1234); // WHERE account_manager_id = 1234
     * $query->filterByAccountManagerId(array(12, 34)); // WHERE account_manager_id IN (12, 34)
     * $query->filterByAccountManagerId(array('min' => 12)); // WHERE account_manager_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $accountManagerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByAccountManagerId($accountManagerId = null, $comparison = null)
    {
        if (is_array($accountManagerId)) {
            $useMinMax = false;
            if (isset($accountManagerId['min'])) {
                $this->addUsingAlias(SupplierTableMap::COL_ACCOUNT_MANAGER_ID, $accountManagerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($accountManagerId['max'])) {
                $this->addUsingAlias(SupplierTableMap::COL_ACCOUNT_MANAGER_ID, $accountManagerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SupplierTableMap::COL_ACCOUNT_MANAGER_ID, $accountManagerId, $comparison);
    }

    /**
     * Filter the query by a related \Model\Account\User object
     *
     * @param \Model\Account\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof \Model\Account\User) {
            return $this
                ->addUsingAlias(SupplierTableMap::COL_ACCOUNT_MANAGER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SupplierTableMap::COL_ACCOUNT_MANAGER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \Model\Account\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Account\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\Model\Account\UserQuery');
    }

    /**
     * Filter the query by a related \Model\Supplier\SupplierProperty object
     *
     * @param \Model\Supplier\SupplierProperty|ObjectCollection $supplierProperty the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSupplierQuery The current query, for fluid interface
     */
    public function filterBySupplierProperty($supplierProperty, $comparison = null)
    {
        if ($supplierProperty instanceof \Model\Supplier\SupplierProperty) {
            return $this
                ->addUsingAlias(SupplierTableMap::COL_ID, $supplierProperty->getSupplierId(), $comparison);
        } elseif ($supplierProperty instanceof ObjectCollection) {
            return $this
                ->useSupplierPropertyQuery()
                ->filterByPrimaryKeys($supplierProperty->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySupplierProperty() only accepts arguments of type \Model\Supplier\SupplierProperty or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SupplierProperty relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function joinSupplierProperty($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SupplierProperty');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SupplierProperty');
        }

        return $this;
    }

    /**
     * Use the SupplierProperty relation SupplierProperty object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Supplier\SupplierPropertyQuery A secondary query class using the current class as primary query
     */
    public function useSupplierPropertyQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSupplierProperty($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SupplierProperty', '\Model\Supplier\SupplierPropertyQuery');
    }

    /**
     * Filter the query by a related \Model\Product object
     *
     * @param \Model\Product|ObjectCollection $product the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByProduct($product, $comparison = null)
    {
        if ($product instanceof \Model\Product) {
            return $this
                ->addUsingAlias(SupplierTableMap::COL_ID, $product->getSupplierId(), $comparison);
        } elseif ($product instanceof ObjectCollection) {
            return $this
                ->useProductQuery()
                ->filterByPrimaryKeys($product->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProduct() only accepts arguments of type \Model\Product or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Product relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function joinProduct($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Product');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Product');
        }

        return $this;
    }

    /**
     * Use the Product relation Product object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ProductQuery A secondary query class using the current class as primary query
     */
    public function useProductQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Product', '\Model\ProductQuery');
    }

    /**
     * Filter the query by a related \Model\Finance\PurchaseInvoice object
     *
     * @param \Model\Finance\PurchaseInvoice|ObjectCollection $purchaseInvoice the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSupplierQuery The current query, for fluid interface
     */
    public function filterByPurchaseInvoice($purchaseInvoice, $comparison = null)
    {
        if ($purchaseInvoice instanceof \Model\Finance\PurchaseInvoice) {
            return $this
                ->addUsingAlias(SupplierTableMap::COL_ID, $purchaseInvoice->getSupplierId(), $comparison);
        } elseif ($purchaseInvoice instanceof ObjectCollection) {
            return $this
                ->usePurchaseInvoiceQuery()
                ->filterByPrimaryKeys($purchaseInvoice->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPurchaseInvoice() only accepts arguments of type \Model\Finance\PurchaseInvoice or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PurchaseInvoice relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function joinPurchaseInvoice($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PurchaseInvoice');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PurchaseInvoice');
        }

        return $this;
    }

    /**
     * Use the PurchaseInvoice relation PurchaseInvoice object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Finance\PurchaseInvoiceQuery A secondary query class using the current class as primary query
     */
    public function usePurchaseInvoiceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPurchaseInvoice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PurchaseInvoice', '\Model\Finance\PurchaseInvoiceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSupplier $supplier Object to remove from the list of results
     *
     * @return $this|ChildSupplierQuery The current query, for fluid interface
     */
    public function prune($supplier = null)
    {
        if ($supplier) {
            $this->addUsingAlias(SupplierTableMap::COL_ID, $supplier->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the supplier table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SupplierTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SupplierTableMap::clearInstancePool();
            SupplierTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SupplierTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SupplierTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SupplierTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SupplierTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SupplierQuery
