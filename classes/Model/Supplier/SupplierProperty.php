<?php

namespace Model\Supplier;

use Model\Supplier\Base\SupplierProperty as BaseSupplierProperty;

/**
 * Skeleton subclass for representing a row from the 'supplier_property' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SupplierProperty extends BaseSupplierProperty
{

}
