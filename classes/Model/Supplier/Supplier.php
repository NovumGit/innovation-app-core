<?php

namespace Model\Supplier;

use Core\Utils;
use Model\Supplier\Base\Supplier as BaseSupplier;

/**
 * Skeleton subclass for representing a row from the 'supplier' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Supplier extends BaseSupplier
{
    function setProperty($sKey, $sValue)
    {
        $oSupplierProperty = $this->getProperty($sKey);

        if($oSupplierProperty instanceof SupplierProperty)
        {
            $oSupplierProperty->setPropertyValue($sValue);
        }
        else
        {
            $oSupplierProperty = new SupplierProperty();
            $oSupplierProperty->setSupplierId($this->getId());
            $oSupplierProperty->setPropertyKey($sKey);
            $oSupplierProperty->setPropertyValue($sValue);
        }
        $oSupplierProperty->save();
    }
    function getPropertyValue($sKey)
    {
        $oSupplierProperty = $this->getProperty($sKey);

        if($oSupplierProperty instanceof SupplierProperty)
        {
            return $oSupplierProperty->getPropertyValue();
        }
        return null;
    }
    function getProperty($sKey)
    {
        $oSupplierPropertyQuery = SupplierPropertyQuery::create();
        $oSupplierPropertyQuery->filterBySupplierId($this->getId());
        $oSupplierPropertyQuery->filterByPropertyKey($sKey);
        $oSupplierProperty = $oSupplierPropertyQuery->findOne();
        if($oSupplierProperty instanceof SupplierProperty)
        {
            return $oSupplierProperty;
        }
        return null;
    }
    function getUrl()
    {
        if($this->getCity())
        {
            return '/dealers/'.$this->getId().'/'.Utils::slugify($this->getCity()).'/'.Utils::slugify($this->getName());
        }
        return '/dealers/'.$this->getId().'/'.Utils::slugify($this->getName());
    }
}
