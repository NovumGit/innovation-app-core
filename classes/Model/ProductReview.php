<?php

namespace Model;

use Core\QueryMapper;
use Model\Base\ProductReview as BaseProductReview;

/**
 * Skeleton subclass for representing a row from the 'product_review' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProductReview extends BaseProductReview
{
    function save(\Propel\Runtime\Connection\ConnectionInterface $con = NULL)
    {
        parent::save();

        $iProductId = $this->getProductId();
        $sQuery = "SELECT 
                      CEIL(AVG(rating)) average_rating
                    FROM 
                      product_review 
                    WHERE 
                      approved = 1 
                    AND product_id = $iProductId";

        $aRow = QueryMapper::fetchRow($sQuery);

        CustomerQuery::create()
            ->findOneById($iProductId)
            ->setCalculatedAverageRating($aRow == null ? null : $aRow['average_rating'])
            ->save();
    }

    function getStars()
    {
        $iRating = $this->getRating();
        // Afronden naar de dichstbijzijnde halve integer (0.5, 1 etc)
        $iRating = floor($iRating * 2) / 2;

        $aStars = [];
        for($i=1; $i<=5; $i++)
        {
            if($i < $iRating)
            {
                $aStars[$i] = 'full';
            }
            else if($i <= ($iRating + 0.5))
            {
                $aStars[$i] = 'half';
            }
            else
            {
                $aStars[$i] = 'empty';
            }
        }
        return $aStars;
    }

    function getRatingStars()
    {

        if($this->getRating() == 0)
        {
            return null;
        }
        return range(1, $this->getRating());
    }

}
