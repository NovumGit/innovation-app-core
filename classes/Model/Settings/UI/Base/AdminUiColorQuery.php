<?php

namespace Model\Settings\UI\Base;

use \Exception;
use \PDO;
use Model\Settings\UI\AdminUiColor as ChildAdminUiColor;
use Model\Settings\UI\AdminUiColorQuery as ChildAdminUiColorQuery;
use Model\Settings\UI\Map\AdminUiColorTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'admin_ui' table.
 *
 *
 *
 * @method     ChildAdminUiColorQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildAdminUiColorQuery orderByMainColor($order = Criteria::ASC) Order by the main_color column
 * @method     ChildAdminUiColorQuery orderByButtonColor($order = Criteria::ASC) Order by the button_color column
 * @method     ChildAdminUiColorQuery orderByBackgroundColor($order = Criteria::ASC) Order by the background_color column
 * @method     ChildAdminUiColorQuery orderByLoginLogo($order = Criteria::ASC) Order by the login_logo column
 * @method     ChildAdminUiColorQuery orderBySystemLogo($order = Criteria::ASC) Order by the system_logo column
 * @method     ChildAdminUiColorQuery orderByAppIcon($order = Criteria::ASC) Order by the app_icon column
 *
 * @method     ChildAdminUiColorQuery groupById() Group by the id column
 * @method     ChildAdminUiColorQuery groupByMainColor() Group by the main_color column
 * @method     ChildAdminUiColorQuery groupByButtonColor() Group by the button_color column
 * @method     ChildAdminUiColorQuery groupByBackgroundColor() Group by the background_color column
 * @method     ChildAdminUiColorQuery groupByLoginLogo() Group by the login_logo column
 * @method     ChildAdminUiColorQuery groupBySystemLogo() Group by the system_logo column
 * @method     ChildAdminUiColorQuery groupByAppIcon() Group by the app_icon column
 *
 * @method     ChildAdminUiColorQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAdminUiColorQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAdminUiColorQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAdminUiColorQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAdminUiColorQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAdminUiColorQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAdminUiColor findOne(ConnectionInterface $con = null) Return the first ChildAdminUiColor matching the query
 * @method     ChildAdminUiColor findOneOrCreate(ConnectionInterface $con = null) Return the first ChildAdminUiColor matching the query, or a new ChildAdminUiColor object populated from the query conditions when no match is found
 *
 * @method     ChildAdminUiColor findOneById(int $id) Return the first ChildAdminUiColor filtered by the id column
 * @method     ChildAdminUiColor findOneByMainColor(string $main_color) Return the first ChildAdminUiColor filtered by the main_color column
 * @method     ChildAdminUiColor findOneByButtonColor(string $button_color) Return the first ChildAdminUiColor filtered by the button_color column
 * @method     ChildAdminUiColor findOneByBackgroundColor(string $background_color) Return the first ChildAdminUiColor filtered by the background_color column
 * @method     ChildAdminUiColor findOneByLoginLogo(string $login_logo) Return the first ChildAdminUiColor filtered by the login_logo column
 * @method     ChildAdminUiColor findOneBySystemLogo(string $system_logo) Return the first ChildAdminUiColor filtered by the system_logo column
 * @method     ChildAdminUiColor findOneByAppIcon(string $app_icon) Return the first ChildAdminUiColor filtered by the app_icon column *

 * @method     ChildAdminUiColor requirePk($key, ConnectionInterface $con = null) Return the ChildAdminUiColor by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAdminUiColor requireOne(ConnectionInterface $con = null) Return the first ChildAdminUiColor matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAdminUiColor requireOneById(int $id) Return the first ChildAdminUiColor filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAdminUiColor requireOneByMainColor(string $main_color) Return the first ChildAdminUiColor filtered by the main_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAdminUiColor requireOneByButtonColor(string $button_color) Return the first ChildAdminUiColor filtered by the button_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAdminUiColor requireOneByBackgroundColor(string $background_color) Return the first ChildAdminUiColor filtered by the background_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAdminUiColor requireOneByLoginLogo(string $login_logo) Return the first ChildAdminUiColor filtered by the login_logo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAdminUiColor requireOneBySystemLogo(string $system_logo) Return the first ChildAdminUiColor filtered by the system_logo column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildAdminUiColor requireOneByAppIcon(string $app_icon) Return the first ChildAdminUiColor filtered by the app_icon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildAdminUiColor[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildAdminUiColor objects based on current ModelCriteria
 * @method     ChildAdminUiColor[]|ObjectCollection findById(int $id) Return ChildAdminUiColor objects filtered by the id column
 * @method     ChildAdminUiColor[]|ObjectCollection findByMainColor(string $main_color) Return ChildAdminUiColor objects filtered by the main_color column
 * @method     ChildAdminUiColor[]|ObjectCollection findByButtonColor(string $button_color) Return ChildAdminUiColor objects filtered by the button_color column
 * @method     ChildAdminUiColor[]|ObjectCollection findByBackgroundColor(string $background_color) Return ChildAdminUiColor objects filtered by the background_color column
 * @method     ChildAdminUiColor[]|ObjectCollection findByLoginLogo(string $login_logo) Return ChildAdminUiColor objects filtered by the login_logo column
 * @method     ChildAdminUiColor[]|ObjectCollection findBySystemLogo(string $system_logo) Return ChildAdminUiColor objects filtered by the system_logo column
 * @method     ChildAdminUiColor[]|ObjectCollection findByAppIcon(string $app_icon) Return ChildAdminUiColor objects filtered by the app_icon column
 * @method     ChildAdminUiColor[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AdminUiColorQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Settings\UI\Base\AdminUiColorQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Settings\\UI\\AdminUiColor', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAdminUiColorQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAdminUiColorQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAdminUiColorQuery) {
            return $criteria;
        }
        $query = new ChildAdminUiColorQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildAdminUiColor|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AdminUiColorTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AdminUiColorTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAdminUiColor A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, main_color, button_color, background_color, login_logo, system_logo, app_icon FROM admin_ui WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildAdminUiColor $obj */
            $obj = new ChildAdminUiColor();
            $obj->hydrate($row);
            AdminUiColorTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildAdminUiColor|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAdminUiColorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AdminUiColorTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAdminUiColorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AdminUiColorTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAdminUiColorQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(AdminUiColorTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(AdminUiColorTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AdminUiColorTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the main_color column
     *
     * Example usage:
     * <code>
     * $query->filterByMainColor('fooValue');   // WHERE main_color = 'fooValue'
     * $query->filterByMainColor('%fooValue%', Criteria::LIKE); // WHERE main_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mainColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAdminUiColorQuery The current query, for fluid interface
     */
    public function filterByMainColor($mainColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mainColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AdminUiColorTableMap::COL_MAIN_COLOR, $mainColor, $comparison);
    }

    /**
     * Filter the query on the button_color column
     *
     * Example usage:
     * <code>
     * $query->filterByButtonColor('fooValue');   // WHERE button_color = 'fooValue'
     * $query->filterByButtonColor('%fooValue%', Criteria::LIKE); // WHERE button_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $buttonColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAdminUiColorQuery The current query, for fluid interface
     */
    public function filterByButtonColor($buttonColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($buttonColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AdminUiColorTableMap::COL_BUTTON_COLOR, $buttonColor, $comparison);
    }

    /**
     * Filter the query on the background_color column
     *
     * Example usage:
     * <code>
     * $query->filterByBackgroundColor('fooValue');   // WHERE background_color = 'fooValue'
     * $query->filterByBackgroundColor('%fooValue%', Criteria::LIKE); // WHERE background_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $backgroundColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAdminUiColorQuery The current query, for fluid interface
     */
    public function filterByBackgroundColor($backgroundColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($backgroundColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AdminUiColorTableMap::COL_BACKGROUND_COLOR, $backgroundColor, $comparison);
    }

    /**
     * Filter the query on the login_logo column
     *
     * Example usage:
     * <code>
     * $query->filterByLoginLogo('fooValue');   // WHERE login_logo = 'fooValue'
     * $query->filterByLoginLogo('%fooValue%', Criteria::LIKE); // WHERE login_logo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $loginLogo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAdminUiColorQuery The current query, for fluid interface
     */
    public function filterByLoginLogo($loginLogo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($loginLogo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AdminUiColorTableMap::COL_LOGIN_LOGO, $loginLogo, $comparison);
    }

    /**
     * Filter the query on the system_logo column
     *
     * Example usage:
     * <code>
     * $query->filterBySystemLogo('fooValue');   // WHERE system_logo = 'fooValue'
     * $query->filterBySystemLogo('%fooValue%', Criteria::LIKE); // WHERE system_logo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $systemLogo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAdminUiColorQuery The current query, for fluid interface
     */
    public function filterBySystemLogo($systemLogo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($systemLogo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AdminUiColorTableMap::COL_SYSTEM_LOGO, $systemLogo, $comparison);
    }

    /**
     * Filter the query on the app_icon column
     *
     * Example usage:
     * <code>
     * $query->filterByAppIcon('fooValue');   // WHERE app_icon = 'fooValue'
     * $query->filterByAppIcon('%fooValue%', Criteria::LIKE); // WHERE app_icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appIcon The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAdminUiColorQuery The current query, for fluid interface
     */
    public function filterByAppIcon($appIcon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appIcon)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AdminUiColorTableMap::COL_APP_ICON, $appIcon, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildAdminUiColor $adminUiColor Object to remove from the list of results
     *
     * @return $this|ChildAdminUiColorQuery The current query, for fluid interface
     */
    public function prune($adminUiColor = null)
    {
        if ($adminUiColor) {
            $this->addUsingAlias(AdminUiColorTableMap::COL_ID, $adminUiColor->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the admin_ui table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AdminUiColorTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AdminUiColorTableMap::clearInstancePool();
            AdminUiColorTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AdminUiColorTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AdminUiColorTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AdminUiColorTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AdminUiColorTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // AdminUiColorQuery
