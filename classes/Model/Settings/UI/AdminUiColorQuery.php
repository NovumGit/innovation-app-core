<?php

namespace Model\Settings\UI;

use Model\Settings\UI\Base\AdminUiColorQuery as BaseAdminUiColorQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'admin_ui' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class AdminUiColorQuery extends BaseAdminUiColorQuery
{

}
