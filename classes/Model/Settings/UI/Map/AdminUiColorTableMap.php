<?php

namespace Model\Settings\UI\Map;

use Model\Settings\UI\AdminUiColor;
use Model\Settings\UI\AdminUiColorQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'admin_ui' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class AdminUiColorTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Settings.UI.Map.AdminUiColorTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'admin_ui';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Settings\\UI\\AdminUiColor';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Settings.UI.AdminUiColor';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the id field
     */
    const COL_ID = 'admin_ui.id';

    /**
     * the column name for the main_color field
     */
    const COL_MAIN_COLOR = 'admin_ui.main_color';

    /**
     * the column name for the button_color field
     */
    const COL_BUTTON_COLOR = 'admin_ui.button_color';

    /**
     * the column name for the background_color field
     */
    const COL_BACKGROUND_COLOR = 'admin_ui.background_color';

    /**
     * the column name for the login_logo field
     */
    const COL_LOGIN_LOGO = 'admin_ui.login_logo';

    /**
     * the column name for the system_logo field
     */
    const COL_SYSTEM_LOGO = 'admin_ui.system_logo';

    /**
     * the column name for the app_icon field
     */
    const COL_APP_ICON = 'admin_ui.app_icon';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'MainColor', 'ButtonColor', 'BackgroundColor', 'LoginLogo', 'SystemLogo', 'AppIcon', ),
        self::TYPE_CAMELNAME     => array('id', 'mainColor', 'buttonColor', 'backgroundColor', 'loginLogo', 'systemLogo', 'appIcon', ),
        self::TYPE_COLNAME       => array(AdminUiColorTableMap::COL_ID, AdminUiColorTableMap::COL_MAIN_COLOR, AdminUiColorTableMap::COL_BUTTON_COLOR, AdminUiColorTableMap::COL_BACKGROUND_COLOR, AdminUiColorTableMap::COL_LOGIN_LOGO, AdminUiColorTableMap::COL_SYSTEM_LOGO, AdminUiColorTableMap::COL_APP_ICON, ),
        self::TYPE_FIELDNAME     => array('id', 'main_color', 'button_color', 'background_color', 'login_logo', 'system_logo', 'app_icon', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'MainColor' => 1, 'ButtonColor' => 2, 'BackgroundColor' => 3, 'LoginLogo' => 4, 'SystemLogo' => 5, 'AppIcon' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'mainColor' => 1, 'buttonColor' => 2, 'backgroundColor' => 3, 'loginLogo' => 4, 'systemLogo' => 5, 'appIcon' => 6, ),
        self::TYPE_COLNAME       => array(AdminUiColorTableMap::COL_ID => 0, AdminUiColorTableMap::COL_MAIN_COLOR => 1, AdminUiColorTableMap::COL_BUTTON_COLOR => 2, AdminUiColorTableMap::COL_BACKGROUND_COLOR => 3, AdminUiColorTableMap::COL_LOGIN_LOGO => 4, AdminUiColorTableMap::COL_SYSTEM_LOGO => 5, AdminUiColorTableMap::COL_APP_ICON => 6, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'main_color' => 1, 'button_color' => 2, 'background_color' => 3, 'login_logo' => 4, 'system_logo' => 5, 'app_icon' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('admin_ui');
        $this->setPhpName('AdminUiColor');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Settings\\UI\\AdminUiColor');
        $this->setPackage('Model.Settings.UI');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('main_color', 'MainColor', 'VARCHAR', true, 255, '#114036');
        $this->addColumn('button_color', 'ButtonColor', 'VARCHAR', true, 255, '#114036');
        $this->addColumn('background_color', 'BackgroundColor', 'VARCHAR', true, 255, '#008f37');
        $this->addColumn('login_logo', 'LoginLogo', 'VARCHAR', true, 255, null);
        $this->addColumn('system_logo', 'SystemLogo', 'VARCHAR', true, 255, null);
        $this->addColumn('app_icon', 'AppIcon', 'VARCHAR', true, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? AdminUiColorTableMap::CLASS_DEFAULT : AdminUiColorTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (AdminUiColor object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = AdminUiColorTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = AdminUiColorTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + AdminUiColorTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = AdminUiColorTableMap::OM_CLASS;
            /** @var AdminUiColor $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            AdminUiColorTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = AdminUiColorTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = AdminUiColorTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var AdminUiColor $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                AdminUiColorTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(AdminUiColorTableMap::COL_ID);
            $criteria->addSelectColumn(AdminUiColorTableMap::COL_MAIN_COLOR);
            $criteria->addSelectColumn(AdminUiColorTableMap::COL_BUTTON_COLOR);
            $criteria->addSelectColumn(AdminUiColorTableMap::COL_BACKGROUND_COLOR);
            $criteria->addSelectColumn(AdminUiColorTableMap::COL_LOGIN_LOGO);
            $criteria->addSelectColumn(AdminUiColorTableMap::COL_SYSTEM_LOGO);
            $criteria->addSelectColumn(AdminUiColorTableMap::COL_APP_ICON);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.main_color');
            $criteria->addSelectColumn($alias . '.button_color');
            $criteria->addSelectColumn($alias . '.background_color');
            $criteria->addSelectColumn($alias . '.login_logo');
            $criteria->addSelectColumn($alias . '.system_logo');
            $criteria->addSelectColumn($alias . '.app_icon');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(AdminUiColorTableMap::DATABASE_NAME)->getTable(AdminUiColorTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(AdminUiColorTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(AdminUiColorTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new AdminUiColorTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a AdminUiColor or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or AdminUiColor object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AdminUiColorTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Settings\UI\AdminUiColor) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(AdminUiColorTableMap::DATABASE_NAME);
            $criteria->add(AdminUiColorTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = AdminUiColorQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            AdminUiColorTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                AdminUiColorTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the admin_ui table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return AdminUiColorQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a AdminUiColor or Criteria object.
     *
     * @param mixed               $criteria Criteria or AdminUiColor object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AdminUiColorTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from AdminUiColor object
        }

        if ($criteria->containsKey(AdminUiColorTableMap::COL_ID) && $criteria->keyContainsValue(AdminUiColorTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.AdminUiColorTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = AdminUiColorQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // AdminUiColorTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
AdminUiColorTableMap::buildTableMap();
