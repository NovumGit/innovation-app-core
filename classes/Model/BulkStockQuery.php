<?php

namespace Model;

use Model\Base\BulkStockQuery as BaseBulkStockQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'bulk_stock' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BulkStockQuery extends BaseBulkStockQuery
{

}
