<?php

namespace Model\Reseller\Map;

use Model\Reseller\Reseller;
use Model\Reseller\ResellerQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'reseller' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class ResellerTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Reseller.Map.ResellerTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'reseller';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Reseller\\Reseller';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Reseller.Reseller';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the id field
     */
    const COL_ID = 'reseller.id';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'reseller.name';

    /**
     * the column name for the address_l1 field
     */
    const COL_ADDRESS_L1 = 'reseller.address_l1';

    /**
     * the column name for the address_l2 field
     */
    const COL_ADDRESS_L2 = 'reseller.address_l2';

    /**
     * the column name for the city field
     */
    const COL_CITY = 'reseller.city';

    /**
     * the column name for the postal field
     */
    const COL_POSTAL = 'reseller.postal';

    /**
     * the column name for the phone field
     */
    const COL_PHONE = 'reseller.phone';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'reseller.email';

    /**
     * the column name for the url field
     */
    const COL_URL = 'reseller.url';

    /**
     * the column name for the latitude field
     */
    const COL_LATITUDE = 'reseller.latitude';

    /**
     * the column name for the longtitude field
     */
    const COL_LONGTITUDE = 'reseller.longtitude';

    /**
     * the column name for the lat_long_checked field
     */
    const COL_LAT_LONG_CHECKED = 'reseller.lat_long_checked';

    /**
     * the column name for the country_id field
     */
    const COL_COUNTRY_ID = 'reseller.country_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'AddressL1', 'AddressL2', 'City', 'Postal', 'Phone', 'Email', 'Url', 'Latitude', 'Longtitude', 'LatLongChecked', 'CountryId', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'addressL1', 'addressL2', 'city', 'postal', 'phone', 'email', 'url', 'latitude', 'longtitude', 'latLongChecked', 'countryId', ),
        self::TYPE_COLNAME       => array(ResellerTableMap::COL_ID, ResellerTableMap::COL_NAME, ResellerTableMap::COL_ADDRESS_L1, ResellerTableMap::COL_ADDRESS_L2, ResellerTableMap::COL_CITY, ResellerTableMap::COL_POSTAL, ResellerTableMap::COL_PHONE, ResellerTableMap::COL_EMAIL, ResellerTableMap::COL_URL, ResellerTableMap::COL_LATITUDE, ResellerTableMap::COL_LONGTITUDE, ResellerTableMap::COL_LAT_LONG_CHECKED, ResellerTableMap::COL_COUNTRY_ID, ),
        self::TYPE_FIELDNAME     => array('id', 'name', 'address_l1', 'address_l2', 'city', 'postal', 'phone', 'email', 'url', 'latitude', 'longtitude', 'lat_long_checked', 'country_id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'AddressL1' => 2, 'AddressL2' => 3, 'City' => 4, 'Postal' => 5, 'Phone' => 6, 'Email' => 7, 'Url' => 8, 'Latitude' => 9, 'Longtitude' => 10, 'LatLongChecked' => 11, 'CountryId' => 12, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'addressL1' => 2, 'addressL2' => 3, 'city' => 4, 'postal' => 5, 'phone' => 6, 'email' => 7, 'url' => 8, 'latitude' => 9, 'longtitude' => 10, 'latLongChecked' => 11, 'countryId' => 12, ),
        self::TYPE_COLNAME       => array(ResellerTableMap::COL_ID => 0, ResellerTableMap::COL_NAME => 1, ResellerTableMap::COL_ADDRESS_L1 => 2, ResellerTableMap::COL_ADDRESS_L2 => 3, ResellerTableMap::COL_CITY => 4, ResellerTableMap::COL_POSTAL => 5, ResellerTableMap::COL_PHONE => 6, ResellerTableMap::COL_EMAIL => 7, ResellerTableMap::COL_URL => 8, ResellerTableMap::COL_LATITUDE => 9, ResellerTableMap::COL_LONGTITUDE => 10, ResellerTableMap::COL_LAT_LONG_CHECKED => 11, ResellerTableMap::COL_COUNTRY_ID => 12, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'name' => 1, 'address_l1' => 2, 'address_l2' => 3, 'city' => 4, 'postal' => 5, 'phone' => 6, 'email' => 7, 'url' => 8, 'latitude' => 9, 'longtitude' => 10, 'lat_long_checked' => 11, 'country_id' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('reseller');
        $this->setPhpName('Reseller');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Reseller\\Reseller');
        $this->setPackage('Model.Reseller');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 120, null);
        $this->addColumn('address_l1', 'AddressL1', 'VARCHAR', false, 255, null);
        $this->addColumn('address_l2', 'AddressL2', 'VARCHAR', false, 255, null);
        $this->addColumn('city', 'City', 'VARCHAR', false, 255, null);
        $this->addColumn('postal', 'Postal', 'VARCHAR', false, 255, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', false, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 255, null);
        $this->addColumn('url', 'Url', 'VARCHAR', false, 255, null);
        $this->addColumn('latitude', 'Latitude', 'VARCHAR', false, 255, null);
        $this->addColumn('longtitude', 'Longtitude', 'VARCHAR', false, 255, null);
        $this->addColumn('lat_long_checked', 'LatLongChecked', 'INTEGER', true, null, false);
        $this->addForeignKey('country_id', 'CountryId', 'INTEGER', 'mt_country', 'id', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Country', '\\Model\\Setting\\MasterTable\\Country', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':country_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ResellerTableMap::CLASS_DEFAULT : ResellerTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Reseller object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ResellerTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ResellerTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ResellerTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ResellerTableMap::OM_CLASS;
            /** @var Reseller $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ResellerTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ResellerTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ResellerTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Reseller $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ResellerTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ResellerTableMap::COL_ID);
            $criteria->addSelectColumn(ResellerTableMap::COL_NAME);
            $criteria->addSelectColumn(ResellerTableMap::COL_ADDRESS_L1);
            $criteria->addSelectColumn(ResellerTableMap::COL_ADDRESS_L2);
            $criteria->addSelectColumn(ResellerTableMap::COL_CITY);
            $criteria->addSelectColumn(ResellerTableMap::COL_POSTAL);
            $criteria->addSelectColumn(ResellerTableMap::COL_PHONE);
            $criteria->addSelectColumn(ResellerTableMap::COL_EMAIL);
            $criteria->addSelectColumn(ResellerTableMap::COL_URL);
            $criteria->addSelectColumn(ResellerTableMap::COL_LATITUDE);
            $criteria->addSelectColumn(ResellerTableMap::COL_LONGTITUDE);
            $criteria->addSelectColumn(ResellerTableMap::COL_LAT_LONG_CHECKED);
            $criteria->addSelectColumn(ResellerTableMap::COL_COUNTRY_ID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.address_l1');
            $criteria->addSelectColumn($alias . '.address_l2');
            $criteria->addSelectColumn($alias . '.city');
            $criteria->addSelectColumn($alias . '.postal');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.url');
            $criteria->addSelectColumn($alias . '.latitude');
            $criteria->addSelectColumn($alias . '.longtitude');
            $criteria->addSelectColumn($alias . '.lat_long_checked');
            $criteria->addSelectColumn($alias . '.country_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ResellerTableMap::DATABASE_NAME)->getTable(ResellerTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ResellerTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ResellerTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ResellerTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Reseller or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Reseller object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ResellerTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Reseller\Reseller) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ResellerTableMap::DATABASE_NAME);
            $criteria->add(ResellerTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ResellerQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ResellerTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ResellerTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the reseller table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ResellerQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Reseller or Criteria object.
     *
     * @param mixed               $criteria Criteria or Reseller object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ResellerTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Reseller object
        }

        if ($criteria->containsKey(ResellerTableMap::COL_ID) && $criteria->keyContainsValue(ResellerTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ResellerTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ResellerQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ResellerTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ResellerTableMap::buildTableMap();
