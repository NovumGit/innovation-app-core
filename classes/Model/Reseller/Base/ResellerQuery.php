<?php

namespace Model\Reseller\Base;

use \Exception;
use \PDO;
use Model\Reseller\Reseller as ChildReseller;
use Model\Reseller\ResellerQuery as ChildResellerQuery;
use Model\Reseller\Map\ResellerTableMap;
use Model\Setting\MasterTable\Country;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'reseller' table.
 *
 *
 *
 * @method     ChildResellerQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildResellerQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildResellerQuery orderByAddressL1($order = Criteria::ASC) Order by the address_l1 column
 * @method     ChildResellerQuery orderByAddressL2($order = Criteria::ASC) Order by the address_l2 column
 * @method     ChildResellerQuery orderByCity($order = Criteria::ASC) Order by the city column
 * @method     ChildResellerQuery orderByPostal($order = Criteria::ASC) Order by the postal column
 * @method     ChildResellerQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildResellerQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildResellerQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method     ChildResellerQuery orderByLatitude($order = Criteria::ASC) Order by the latitude column
 * @method     ChildResellerQuery orderByLongtitude($order = Criteria::ASC) Order by the longtitude column
 * @method     ChildResellerQuery orderByLatLongChecked($order = Criteria::ASC) Order by the lat_long_checked column
 * @method     ChildResellerQuery orderByCountryId($order = Criteria::ASC) Order by the country_id column
 *
 * @method     ChildResellerQuery groupById() Group by the id column
 * @method     ChildResellerQuery groupByName() Group by the name column
 * @method     ChildResellerQuery groupByAddressL1() Group by the address_l1 column
 * @method     ChildResellerQuery groupByAddressL2() Group by the address_l2 column
 * @method     ChildResellerQuery groupByCity() Group by the city column
 * @method     ChildResellerQuery groupByPostal() Group by the postal column
 * @method     ChildResellerQuery groupByPhone() Group by the phone column
 * @method     ChildResellerQuery groupByEmail() Group by the email column
 * @method     ChildResellerQuery groupByUrl() Group by the url column
 * @method     ChildResellerQuery groupByLatitude() Group by the latitude column
 * @method     ChildResellerQuery groupByLongtitude() Group by the longtitude column
 * @method     ChildResellerQuery groupByLatLongChecked() Group by the lat_long_checked column
 * @method     ChildResellerQuery groupByCountryId() Group by the country_id column
 *
 * @method     ChildResellerQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildResellerQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildResellerQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildResellerQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildResellerQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildResellerQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildResellerQuery leftJoinCountry($relationAlias = null) Adds a LEFT JOIN clause to the query using the Country relation
 * @method     ChildResellerQuery rightJoinCountry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Country relation
 * @method     ChildResellerQuery innerJoinCountry($relationAlias = null) Adds a INNER JOIN clause to the query using the Country relation
 *
 * @method     ChildResellerQuery joinWithCountry($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Country relation
 *
 * @method     ChildResellerQuery leftJoinWithCountry() Adds a LEFT JOIN clause and with to the query using the Country relation
 * @method     ChildResellerQuery rightJoinWithCountry() Adds a RIGHT JOIN clause and with to the query using the Country relation
 * @method     ChildResellerQuery innerJoinWithCountry() Adds a INNER JOIN clause and with to the query using the Country relation
 *
 * @method     \Model\Setting\MasterTable\CountryQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildReseller findOne(ConnectionInterface $con = null) Return the first ChildReseller matching the query
 * @method     ChildReseller findOneOrCreate(ConnectionInterface $con = null) Return the first ChildReseller matching the query, or a new ChildReseller object populated from the query conditions when no match is found
 *
 * @method     ChildReseller findOneById(int $id) Return the first ChildReseller filtered by the id column
 * @method     ChildReseller findOneByName(string $name) Return the first ChildReseller filtered by the name column
 * @method     ChildReseller findOneByAddressL1(string $address_l1) Return the first ChildReseller filtered by the address_l1 column
 * @method     ChildReseller findOneByAddressL2(string $address_l2) Return the first ChildReseller filtered by the address_l2 column
 * @method     ChildReseller findOneByCity(string $city) Return the first ChildReseller filtered by the city column
 * @method     ChildReseller findOneByPostal(string $postal) Return the first ChildReseller filtered by the postal column
 * @method     ChildReseller findOneByPhone(string $phone) Return the first ChildReseller filtered by the phone column
 * @method     ChildReseller findOneByEmail(string $email) Return the first ChildReseller filtered by the email column
 * @method     ChildReseller findOneByUrl(string $url) Return the first ChildReseller filtered by the url column
 * @method     ChildReseller findOneByLatitude(string $latitude) Return the first ChildReseller filtered by the latitude column
 * @method     ChildReseller findOneByLongtitude(string $longtitude) Return the first ChildReseller filtered by the longtitude column
 * @method     ChildReseller findOneByLatLongChecked(int $lat_long_checked) Return the first ChildReseller filtered by the lat_long_checked column
 * @method     ChildReseller findOneByCountryId(int $country_id) Return the first ChildReseller filtered by the country_id column *

 * @method     ChildReseller requirePk($key, ConnectionInterface $con = null) Return the ChildReseller by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildReseller requireOne(ConnectionInterface $con = null) Return the first ChildReseller matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildReseller requireOneById(int $id) Return the first ChildReseller filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildReseller requireOneByName(string $name) Return the first ChildReseller filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildReseller requireOneByAddressL1(string $address_l1) Return the first ChildReseller filtered by the address_l1 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildReseller requireOneByAddressL2(string $address_l2) Return the first ChildReseller filtered by the address_l2 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildReseller requireOneByCity(string $city) Return the first ChildReseller filtered by the city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildReseller requireOneByPostal(string $postal) Return the first ChildReseller filtered by the postal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildReseller requireOneByPhone(string $phone) Return the first ChildReseller filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildReseller requireOneByEmail(string $email) Return the first ChildReseller filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildReseller requireOneByUrl(string $url) Return the first ChildReseller filtered by the url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildReseller requireOneByLatitude(string $latitude) Return the first ChildReseller filtered by the latitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildReseller requireOneByLongtitude(string $longtitude) Return the first ChildReseller filtered by the longtitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildReseller requireOneByLatLongChecked(int $lat_long_checked) Return the first ChildReseller filtered by the lat_long_checked column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildReseller requireOneByCountryId(int $country_id) Return the first ChildReseller filtered by the country_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildReseller[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildReseller objects based on current ModelCriteria
 * @method     ChildReseller[]|ObjectCollection findById(int $id) Return ChildReseller objects filtered by the id column
 * @method     ChildReseller[]|ObjectCollection findByName(string $name) Return ChildReseller objects filtered by the name column
 * @method     ChildReseller[]|ObjectCollection findByAddressL1(string $address_l1) Return ChildReseller objects filtered by the address_l1 column
 * @method     ChildReseller[]|ObjectCollection findByAddressL2(string $address_l2) Return ChildReseller objects filtered by the address_l2 column
 * @method     ChildReseller[]|ObjectCollection findByCity(string $city) Return ChildReseller objects filtered by the city column
 * @method     ChildReseller[]|ObjectCollection findByPostal(string $postal) Return ChildReseller objects filtered by the postal column
 * @method     ChildReseller[]|ObjectCollection findByPhone(string $phone) Return ChildReseller objects filtered by the phone column
 * @method     ChildReseller[]|ObjectCollection findByEmail(string $email) Return ChildReseller objects filtered by the email column
 * @method     ChildReseller[]|ObjectCollection findByUrl(string $url) Return ChildReseller objects filtered by the url column
 * @method     ChildReseller[]|ObjectCollection findByLatitude(string $latitude) Return ChildReseller objects filtered by the latitude column
 * @method     ChildReseller[]|ObjectCollection findByLongtitude(string $longtitude) Return ChildReseller objects filtered by the longtitude column
 * @method     ChildReseller[]|ObjectCollection findByLatLongChecked(int $lat_long_checked) Return ChildReseller objects filtered by the lat_long_checked column
 * @method     ChildReseller[]|ObjectCollection findByCountryId(int $country_id) Return ChildReseller objects filtered by the country_id column
 * @method     ChildReseller[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ResellerQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Reseller\Base\ResellerQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Reseller\\Reseller', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildResellerQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildResellerQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildResellerQuery) {
            return $criteria;
        }
        $query = new ChildResellerQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildReseller|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ResellerTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ResellerTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildReseller A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, address_l1, address_l2, city, postal, phone, email, url, latitude, longtitude, lat_long_checked, country_id FROM reseller WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildReseller $obj */
            $obj = new ChildReseller();
            $obj->hydrate($row);
            ResellerTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildReseller|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ResellerTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ResellerTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ResellerTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ResellerTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResellerTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResellerTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the address_l1 column
     *
     * Example usage:
     * <code>
     * $query->filterByAddressL1('fooValue');   // WHERE address_l1 = 'fooValue'
     * $query->filterByAddressL1('%fooValue%', Criteria::LIKE); // WHERE address_l1 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $addressL1 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByAddressL1($addressL1 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($addressL1)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResellerTableMap::COL_ADDRESS_L1, $addressL1, $comparison);
    }

    /**
     * Filter the query on the address_l2 column
     *
     * Example usage:
     * <code>
     * $query->filterByAddressL2('fooValue');   // WHERE address_l2 = 'fooValue'
     * $query->filterByAddressL2('%fooValue%', Criteria::LIKE); // WHERE address_l2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $addressL2 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByAddressL2($addressL2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($addressL2)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResellerTableMap::COL_ADDRESS_L2, $addressL2, $comparison);
    }

    /**
     * Filter the query on the city column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE city = 'fooValue'
     * $query->filterByCity('%fooValue%', Criteria::LIKE); // WHERE city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResellerTableMap::COL_CITY, $city, $comparison);
    }

    /**
     * Filter the query on the postal column
     *
     * Example usage:
     * <code>
     * $query->filterByPostal('fooValue');   // WHERE postal = 'fooValue'
     * $query->filterByPostal('%fooValue%', Criteria::LIKE); // WHERE postal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $postal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByPostal($postal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($postal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResellerTableMap::COL_POSTAL, $postal, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%', Criteria::LIKE); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResellerTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResellerTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%', Criteria::LIKE); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResellerTableMap::COL_URL, $url, $comparison);
    }

    /**
     * Filter the query on the latitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLatitude('fooValue');   // WHERE latitude = 'fooValue'
     * $query->filterByLatitude('%fooValue%', Criteria::LIKE); // WHERE latitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $latitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByLatitude($latitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($latitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResellerTableMap::COL_LATITUDE, $latitude, $comparison);
    }

    /**
     * Filter the query on the longtitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLongtitude('fooValue');   // WHERE longtitude = 'fooValue'
     * $query->filterByLongtitude('%fooValue%', Criteria::LIKE); // WHERE longtitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $longtitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByLongtitude($longtitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($longtitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResellerTableMap::COL_LONGTITUDE, $longtitude, $comparison);
    }

    /**
     * Filter the query on the lat_long_checked column
     *
     * Example usage:
     * <code>
     * $query->filterByLatLongChecked(1234); // WHERE lat_long_checked = 1234
     * $query->filterByLatLongChecked(array(12, 34)); // WHERE lat_long_checked IN (12, 34)
     * $query->filterByLatLongChecked(array('min' => 12)); // WHERE lat_long_checked > 12
     * </code>
     *
     * @param     mixed $latLongChecked The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByLatLongChecked($latLongChecked = null, $comparison = null)
    {
        if (is_array($latLongChecked)) {
            $useMinMax = false;
            if (isset($latLongChecked['min'])) {
                $this->addUsingAlias(ResellerTableMap::COL_LAT_LONG_CHECKED, $latLongChecked['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($latLongChecked['max'])) {
                $this->addUsingAlias(ResellerTableMap::COL_LAT_LONG_CHECKED, $latLongChecked['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResellerTableMap::COL_LAT_LONG_CHECKED, $latLongChecked, $comparison);
    }

    /**
     * Filter the query on the country_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCountryId(1234); // WHERE country_id = 1234
     * $query->filterByCountryId(array(12, 34)); // WHERE country_id IN (12, 34)
     * $query->filterByCountryId(array('min' => 12)); // WHERE country_id > 12
     * </code>
     *
     * @see       filterByCountry()
     *
     * @param     mixed $countryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function filterByCountryId($countryId = null, $comparison = null)
    {
        if (is_array($countryId)) {
            $useMinMax = false;
            if (isset($countryId['min'])) {
                $this->addUsingAlias(ResellerTableMap::COL_COUNTRY_ID, $countryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countryId['max'])) {
                $this->addUsingAlias(ResellerTableMap::COL_COUNTRY_ID, $countryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ResellerTableMap::COL_COUNTRY_ID, $countryId, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Country object
     *
     * @param \Model\Setting\MasterTable\Country|ObjectCollection $country The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildResellerQuery The current query, for fluid interface
     */
    public function filterByCountry($country, $comparison = null)
    {
        if ($country instanceof \Model\Setting\MasterTable\Country) {
            return $this
                ->addUsingAlias(ResellerTableMap::COL_COUNTRY_ID, $country->getId(), $comparison);
        } elseif ($country instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ResellerTableMap::COL_COUNTRY_ID, $country->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCountry() only accepts arguments of type \Model\Setting\MasterTable\Country or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Country relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function joinCountry($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Country');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Country');
        }

        return $this;
    }

    /**
     * Use the Country relation Country object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\CountryQuery A secondary query class using the current class as primary query
     */
    public function useCountryQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCountry($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Country', '\Model\Setting\MasterTable\CountryQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildReseller $reseller Object to remove from the list of results
     *
     * @return $this|ChildResellerQuery The current query, for fluid interface
     */
    public function prune($reseller = null)
    {
        if ($reseller) {
            $this->addUsingAlias(ResellerTableMap::COL_ID, $reseller->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the reseller table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ResellerTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ResellerTableMap::clearInstancePool();
            ResellerTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ResellerTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ResellerTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ResellerTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ResellerTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ResellerQuery
