<?php

namespace Model\MailLog;

use Model\MailLog\Base\MailLog as BaseMailLog;

/**
 * Skeleton subclass for representing a row from the 'mail_log' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MailLog extends BaseMailLog
{

}
