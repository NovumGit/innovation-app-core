<?php

namespace Model\MailLog\Map;

use Model\MailLog\MailLog;
use Model\MailLog\MailLogQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'mail_log' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class MailLogTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.MailLog.Map.MailLogTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'mail_log';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\MailLog\\MailLog';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.MailLog.MailLog';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'mail_log.id';

    /**
     * the column name for the sale_order_id field
     */
    const COL_SALE_ORDER_ID = 'mail_log.sale_order_id';

    /**
     * the column name for the from_email field
     */
    const COL_FROM_EMAIL = 'mail_log.from_email';

    /**
     * the column name for the to field
     */
    const COL_TO = 'mail_log.to';

    /**
     * the column name for the cc field
     */
    const COL_CC = 'mail_log.cc';

    /**
     * the column name for the bcc field
     */
    const COL_BCC = 'mail_log.bcc';

    /**
     * the column name for the content field
     */
    const COL_CONTENT = 'mail_log.content';

    /**
     * the column name for the subject field
     */
    const COL_SUBJECT = 'mail_log.subject';

    /**
     * the column name for the date_sent field
     */
    const COL_DATE_SENT = 'mail_log.date_sent';

    /**
     * the column name for the sender_ip field
     */
    const COL_SENDER_IP = 'mail_log.sender_ip';

    /**
     * the column name for the customer_id field
     */
    const COL_CUSTOMER_ID = 'mail_log.customer_id';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'mail_log.user_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'SaleOrderId', 'FromEmail', 'To', 'Cc', 'Bcc', 'Content', 'Subject', 'DateSent', 'SenderIp', 'CustomerId', 'UserId', ),
        self::TYPE_CAMELNAME     => array('id', 'saleOrderId', 'fromEmail', 'to', 'cc', 'bcc', 'content', 'subject', 'dateSent', 'senderIp', 'customerId', 'userId', ),
        self::TYPE_COLNAME       => array(MailLogTableMap::COL_ID, MailLogTableMap::COL_SALE_ORDER_ID, MailLogTableMap::COL_FROM_EMAIL, MailLogTableMap::COL_TO, MailLogTableMap::COL_CC, MailLogTableMap::COL_BCC, MailLogTableMap::COL_CONTENT, MailLogTableMap::COL_SUBJECT, MailLogTableMap::COL_DATE_SENT, MailLogTableMap::COL_SENDER_IP, MailLogTableMap::COL_CUSTOMER_ID, MailLogTableMap::COL_USER_ID, ),
        self::TYPE_FIELDNAME     => array('id', 'sale_order_id', 'from_email', 'to', 'cc', 'bcc', 'content', 'subject', 'date_sent', 'sender_ip', 'customer_id', 'user_id', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'SaleOrderId' => 1, 'FromEmail' => 2, 'To' => 3, 'Cc' => 4, 'Bcc' => 5, 'Content' => 6, 'Subject' => 7, 'DateSent' => 8, 'SenderIp' => 9, 'CustomerId' => 10, 'UserId' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'saleOrderId' => 1, 'fromEmail' => 2, 'to' => 3, 'cc' => 4, 'bcc' => 5, 'content' => 6, 'subject' => 7, 'dateSent' => 8, 'senderIp' => 9, 'customerId' => 10, 'userId' => 11, ),
        self::TYPE_COLNAME       => array(MailLogTableMap::COL_ID => 0, MailLogTableMap::COL_SALE_ORDER_ID => 1, MailLogTableMap::COL_FROM_EMAIL => 2, MailLogTableMap::COL_TO => 3, MailLogTableMap::COL_CC => 4, MailLogTableMap::COL_BCC => 5, MailLogTableMap::COL_CONTENT => 6, MailLogTableMap::COL_SUBJECT => 7, MailLogTableMap::COL_DATE_SENT => 8, MailLogTableMap::COL_SENDER_IP => 9, MailLogTableMap::COL_CUSTOMER_ID => 10, MailLogTableMap::COL_USER_ID => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'sale_order_id' => 1, 'from_email' => 2, 'to' => 3, 'cc' => 4, 'bcc' => 5, 'content' => 6, 'subject' => 7, 'date_sent' => 8, 'sender_ip' => 9, 'customer_id' => 10, 'user_id' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('mail_log');
        $this->setPhpName('MailLog');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\MailLog\\MailLog');
        $this->setPackage('Model.MailLog');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('sale_order_id', 'SaleOrderId', 'INTEGER', 'sale_order', 'id', false, null, null);
        $this->addColumn('from_email', 'FromEmail', 'VARCHAR', false, 255, null);
        $this->addColumn('to', 'To', 'VARCHAR', false, 255, null);
        $this->addColumn('cc', 'Cc', 'VARCHAR', false, 255, null);
        $this->addColumn('bcc', 'Bcc', 'VARCHAR', false, 255, null);
        $this->addColumn('content', 'Content', 'VARCHAR', false, 255, null);
        $this->addColumn('subject', 'Subject', 'VARCHAR', false, 255, null);
        $this->addColumn('date_sent', 'DateSent', 'TIMESTAMP', false, null, null);
        $this->addColumn('sender_ip', 'SenderIp', 'VARCHAR', false, 255, null);
        $this->addForeignKey('customer_id', 'CustomerId', 'INTEGER', 'customer', 'id', false, null, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'user', 'id', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SaleOrder', '\\Model\\Sale\\SaleOrder', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':sale_order_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('Customer', '\\Model\\Crm\\Customer', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('User', '\\Model\\Account\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? MailLogTableMap::CLASS_DEFAULT : MailLogTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (MailLog object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = MailLogTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = MailLogTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + MailLogTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = MailLogTableMap::OM_CLASS;
            /** @var MailLog $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            MailLogTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = MailLogTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = MailLogTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var MailLog $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                MailLogTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(MailLogTableMap::COL_ID);
            $criteria->addSelectColumn(MailLogTableMap::COL_SALE_ORDER_ID);
            $criteria->addSelectColumn(MailLogTableMap::COL_FROM_EMAIL);
            $criteria->addSelectColumn(MailLogTableMap::COL_TO);
            $criteria->addSelectColumn(MailLogTableMap::COL_CC);
            $criteria->addSelectColumn(MailLogTableMap::COL_BCC);
            $criteria->addSelectColumn(MailLogTableMap::COL_CONTENT);
            $criteria->addSelectColumn(MailLogTableMap::COL_SUBJECT);
            $criteria->addSelectColumn(MailLogTableMap::COL_DATE_SENT);
            $criteria->addSelectColumn(MailLogTableMap::COL_SENDER_IP);
            $criteria->addSelectColumn(MailLogTableMap::COL_CUSTOMER_ID);
            $criteria->addSelectColumn(MailLogTableMap::COL_USER_ID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.sale_order_id');
            $criteria->addSelectColumn($alias . '.from_email');
            $criteria->addSelectColumn($alias . '.to');
            $criteria->addSelectColumn($alias . '.cc');
            $criteria->addSelectColumn($alias . '.bcc');
            $criteria->addSelectColumn($alias . '.content');
            $criteria->addSelectColumn($alias . '.subject');
            $criteria->addSelectColumn($alias . '.date_sent');
            $criteria->addSelectColumn($alias . '.sender_ip');
            $criteria->addSelectColumn($alias . '.customer_id');
            $criteria->addSelectColumn($alias . '.user_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(MailLogTableMap::DATABASE_NAME)->getTable(MailLogTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(MailLogTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(MailLogTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new MailLogTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a MailLog or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or MailLog object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MailLogTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\MailLog\MailLog) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(MailLogTableMap::DATABASE_NAME);
            $criteria->add(MailLogTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = MailLogQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            MailLogTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                MailLogTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the mail_log table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return MailLogQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a MailLog or Criteria object.
     *
     * @param mixed               $criteria Criteria or MailLog object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MailLogTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from MailLog object
        }

        if ($criteria->containsKey(MailLogTableMap::COL_ID) && $criteria->keyContainsValue(MailLogTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.MailLogTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = MailLogQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // MailLogTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
MailLogTableMap::buildTableMap();
