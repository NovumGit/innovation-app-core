<?php

namespace Model\Setting\CrudManager;

use Model\Setting\CrudManager\Base\CrudView as BaseCrudView;

/**
 * Skeleton subclass for representing a row from the 'crud_view' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CrudView extends BaseCrudView
{
    /**
     * @return CrudViewField[]
     */
    final function getFields():array
    {
        $aFields = [];
        $aCrudViewFields = CrudViewFieldQuery::create()->orderBySorting()->findByCrudViewId($this->getId());

        if(!$aCrudViewFields->isEmpty())
        {
            foreach($aCrudViewFields as $oCrudViewField)
            {
                $aFields[] = $oCrudViewField->getFieldName();
            }
        }
        return $aFields;
    }
    /**
     *
     */
    final function deleteAllFields()
    {
        // Eerst alle velden uit de view verwijderen.
        $oQuery = CrudViewFieldQuery::create();
        $aResults = $oQuery->findByCrudViewId($this->getId());
        $aResults->delete();
    }

    /**
     * @param int $iSorting
     * @param string $sFieldName
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final function addField(int $iSorting, string $sFieldName)
    {
        $oCrudViewField = CrudViewFieldQuery::create()
            ->filterByCrudViewId($this->getId())
            ->filterByFieldName($sFieldName)
            ->findOneOrCreate();

        $oCrudViewField->setSorting($iSorting);
        $oCrudViewField->save();
    }

    /**
     * @param array $aFields
     * @return mixed
     * @throws \Propel\Runtime\Exception\PropelException
     */
    final function setFields(array $aFields):void
    {

        $this->deleteAllFields();

        // Dan alle velden opnieuw aanmaken.
        $iSorting = 0;
        foreach ($aFields as $sFieldName)
        {
            $this->addField($iSorting, $sFieldName);
            $iSorting++;
        }

        return;
    }
}
