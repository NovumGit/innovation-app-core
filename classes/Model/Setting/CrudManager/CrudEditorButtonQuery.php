<?php

namespace Model\Setting\CrudManager;

use Model\Setting\CrudManager\Base\CrudEditorButtonQuery as BaseCrudEditorButtonQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'crud_editor_button' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CrudEditorButtonQuery extends BaseCrudEditorButtonQuery
{

}
