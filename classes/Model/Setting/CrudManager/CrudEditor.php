<?php

namespace Model\Setting\CrudManager;

use Model\Setting\CrudManager\Base\CrudEditor as BaseCrudEditor;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Exception\PropelException;

/**
 * Skeleton subclass for representing a row from the 'crud_editor' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CrudEditor extends BaseCrudEditor
{

    /**
     * @return CrudEditorBlock[]
     * @throws PropelException
     */
    public function getCrudEditorBlocksSorted()
    {

        if (!$this->getId()) {
            // This CrudEditor is created on the fly / not available in the database.
            return $this->getCrudEditorBlocks();
        }

        $oCriteria = new Criteria();
        $oCriteria->addAscendingOrderByColumn('sorting');

        return $this->getCrudEditorBlocks($oCriteria);
    }
}
