<?php

namespace Model\Setting\CrudManager;

use Model\Setting\CrudManager\Base\CrudViewButton as BaseCrudViewButton;

/**
 * Skeleton subclass for representing a row from the 'crud_view_button' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CrudViewButton extends BaseCrudViewButton
{

}
