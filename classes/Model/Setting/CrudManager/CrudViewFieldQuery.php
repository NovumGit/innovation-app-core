<?php

namespace Model\Setting\CrudManager;

use Model\Setting\CrudManager\Base\CrudViewFieldQuery as BaseCrudViewFieldQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'crud_view_field' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CrudViewFieldQuery extends BaseCrudViewFieldQuery
{

}
