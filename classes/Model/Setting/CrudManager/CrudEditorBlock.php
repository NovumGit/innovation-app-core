<?php

namespace Model\Setting\CrudManager;

use Crud\CrudElement;
use Crud\IInlineEditor;
use Model\Setting\CrudManager\Base\CrudEditorBlock as BaseCrudEditorBlock;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Exception\LogicException;

/**
 * Skeleton subclass for representing a row from the 'crud_editor_block' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CrudEditorBlock extends BaseCrudEditorBlock
{


    /**
     * @return CrudEditorBlockField[]
     * @throws \Propel\Runtime\Exception\PropelException
     * @throws \Exception\LogicException
     */
    function getFieldsSorted(): ObjectCollection
    {
        if(!$this->getId())
        {
            return $this->getCrudEditorBlockFields();
        }


        $oQuery = CrudEditorBlockFieldQuery::create();
        $oQuery->filterByCrudEditorBlockId($this->getId());
        $oQuery->orderBySorting();
        $aFields = $oQuery->find();
        $aOut = [];
        foreach ($aFields as $oField) {
            if (!class_exists($oField->getField())) {
                $oField->delete();
                throw new LogicException("We konden de klasse " . $oField->getField() . ' niet vinden, het record is automatisch uit het overzicht gehaald, je kan op F5 drukken.');
            }
            $aOut[] = $oField;
        }
        $oObjectCollection = new ObjectCollection();
        $oObjectCollection->setData($aOut);
        return $oObjectCollection;
    }

    /**
     * Get the [width] column value.
     *
     * @return int
     */
    public function getWidth()
    {
        if ($this->width == 0) {
            return 6;
        }
        return $this->width;
    }

    function getFieldObjects()
    {

        $aOut = [];
        $iCrudEditorId = $this->getId();

        $oCrudEditorBlockFieldQuery = CrudEditorBlockFieldQuery::create();
        $oCrudEditorBlockFieldQuery->filterByCrudEditorBlockId($iCrudEditorId);
        $oCrudEditorBlockFieldQuery->orderBySorting();
        $oCrudEditorBlockFields = $oCrudEditorBlockFieldQuery->find();

        if (!$oCrudEditorBlockFields->isEmpty()) {
            foreach ($oCrudEditorBlockFields as $oCrudEditorBlockField) {
                $sFieldObjectName = $oCrudEditorBlockField->getField();
                $oField = new $sFieldObjectName();

                $aClassname = array_reverse(explode('\\', $sFieldObjectName))[0];

                if (!$oField instanceof CrudElement) {
                    throw new LogicException("Expected an instance of Crud\\Field but got " . get_class($oField));
                }

                $oField->setArguments(['crud_editor_block_field' => $oCrudEditorBlockField]);
                $aOut[] = [
                    'crud_editor_block_field' => $oCrudEditorBlockField,
                    'inline_editor' => ($oField instanceof IInlineEditor),
                    'crud_field' => $oField,
                    'classname' => $aClassname
                ];
            }
        }

        return $aOut;
    }

}
