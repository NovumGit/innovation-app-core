<?php

namespace Model\Setting\CrudManager;

use Exception;
use Model\Setting\CrudManager\Base\FilterOperator as BaseFilterOperator;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for representing a row from the 'filter_operator' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class FilterOperator extends BaseFilterOperator
{
    /**
     * Tries to convert this FilterOperator to a Criteria object
     * @return string
     * @throws Exception
     */
    function toCriteria():string
    {
        if($this->getName() == 'equals' || $this->getName() == 'yes_no')
        {
            return Criteria::EQUAL;
        }
        else if($this->getName() == 'not_equals')
        {
            return Criteria::NOT_EQUAL;
        }
        else if($this->getName() == 'larger_then')
        {
            return Criteria::GREATER_THAN;
        }
        else if($this->getName() == 'larger_then_or_equal')
        {
            return Criteria::GREATER_EQUAL;
        }
        else if($this->getName() == 'smaller_then')
        {
            return Criteria::LESS_THAN;
        }
        else if($this->getName() == 'smaller_then_or_equal')
        {
            return Criteria::LESS_EQUAL;
        }
        else if($this->getName() == 'is_not_null')
        {
            return Criteria::ISNOTNULL;
        }
        else if($this->getName() == 'is_null')
        {
            return Criteria::ISNULL;
        }
        else if($this->getName() == 'in')
        {
            return Criteria::IN;
        }
        else if($this->getName() == 'not_in')
        {
            return Criteria::NOT_IN;
        }
        else if($this->getName() == 'starts_with')
        {
            return Criteria::LIKE;
        }
        else if($this->getName() == 'ends_on')
        {
            return Criteria::LIKE;
        }
        else if($this->getName() == 'contains_text')
        {
            return Criteria::LIKE;
        }
        else if($this->getName() == 'not_contains_text')
        {
            return Criteria::NOT_ILIKE;
        }

        throw new Exception("Unknown operator {$this->getName()}");
    }

    /**
     * Formats a given value into a query value that matches the operator. Adds wildcards etc.
     *
     * @param $sFilterValue
     * @return string|null
     */
    function formatValue($sFilterValue):?string
    {
        if(in_array($this->getName(), ['is_not_null', 'is_null']))
        {
            $sValue = null;
        }
        else if($this->getName() == 'ends_on')
        {
            $sValue = $sFilterValue.'%';
        }
        else if(in_array($this->getName(), ['contains_text', 'not_contains_text']))
        {
            $sValue = '%'.$sFilterValue.'%';
        }
        else if(in_array($this->getName(), ['in', 'not_in']))
        {
            $sValue = explode(',', $sFilterValue);
        }
        else
        {
            $sValue = $sFilterValue;
        }
        return $sValue;
    }

}


