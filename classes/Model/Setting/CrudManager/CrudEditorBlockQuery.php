<?php

namespace Model\Setting\CrudManager;

use Model\Setting\CrudManager\Base\CrudEditorBlockQuery as BaseCrudEditorBlockQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'crud_editor_block' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CrudEditorBlockQuery extends BaseCrudEditorBlockQuery
{

}
