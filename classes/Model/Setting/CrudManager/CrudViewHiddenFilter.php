<?php

namespace Model\Setting\CrudManager;

use Crud\ICrudFilterModel;
use Model\Setting\CrudManager\Base\CrudViewHiddenFilter as BaseCrudViewHiddenFilter;

/**
 * Skeleton subclass for representing a row from the 'crud_view_hidden_filter' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CrudViewHiddenFilter extends BaseCrudViewHiddenFilter implements ICrudFilterModel
{

}
