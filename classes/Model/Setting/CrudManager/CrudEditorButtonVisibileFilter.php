<?php

namespace Model\Setting\CrudManager;

use Model\Setting\CrudManager\Base\CrudEditorButtonVisibileFilter as BaseCrudEditorButtonVisibileFilter;

/**
 * Skeleton subclass for representing a row from the 'crud_editor_button_visibile_filter' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CrudEditorButtonVisibileFilter extends BaseCrudEditorButtonVisibileFilter
{

}
