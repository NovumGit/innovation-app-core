<?php

namespace Model\Setting\CrudManager;

use Crud\Field;
use Exception\LogicException;
use Model\Setting\CrudManager\Base\CrudEditorBlockField as BaseCrudEditorBlockField;

/**
 * Skeleton subclass for representing a row from the 'crud_editor_block_field' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CrudEditorBlockField extends BaseCrudEditorBlockField
{

    /**
     * @param array $aArguments are runtime variables / settings from the crud manager.
     * @return Field
     */
    function getFieldObject(array $aArguments = null):Field
    {
        $sFqn = $this->getField();
        $oField = new $sFqn();

        if(!$oField instanceof Field)
        {
            throw new LogicException("Expected an instance of Field");
        }
        if(is_array($aArguments))
        {
            $oField->setArguments($aArguments);
        }
        return $oField;
    }
    /**
     * @return string
     * @throws \ReflectionException
     */
    function getShortName()
    {
        $oReflectionClass = new \ReflectionClass($this->getField());
        return $oReflectionClass->getShortName();

    }

}
