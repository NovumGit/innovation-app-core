<?php

namespace Model\Setting\CrudManager\Base;

use \Exception;
use \PDO;
use Model\Setting\CrudManager\CrudView as ChildCrudView;
use Model\Setting\CrudManager\CrudViewQuery as ChildCrudViewQuery;
use Model\Setting\CrudManager\Map\CrudViewTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'crud_view' table.
 *
 *
 *
 * @method     ChildCrudViewQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCrudViewQuery orderByCrudConfigId($order = Criteria::ASC) Order by the crud_config_id column
 * @method     ChildCrudViewQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildCrudViewQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method     ChildCrudViewQuery orderBySorting($order = Criteria::ASC) Order by the sorting column
 * @method     ChildCrudViewQuery orderByIsHidden($order = Criteria::ASC) Order by the is_hidden column
 * @method     ChildCrudViewQuery orderByShowQuantity($order = Criteria::ASC) Order by the show_quantity column
 * @method     ChildCrudViewQuery orderByDefaultOrderBy($order = Criteria::ASC) Order by the default_order_by column
 * @method     ChildCrudViewQuery orderByDefaultOrderDir($order = Criteria::ASC) Order by the default_order_dir column
 * @method     ChildCrudViewQuery orderByTabColorLogic($order = Criteria::ASC) Order by the tab_color_logic column
 * @method     ChildCrudViewQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCrudViewQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCrudViewQuery groupById() Group by the id column
 * @method     ChildCrudViewQuery groupByCrudConfigId() Group by the crud_config_id column
 * @method     ChildCrudViewQuery groupByName() Group by the name column
 * @method     ChildCrudViewQuery groupByCode() Group by the code column
 * @method     ChildCrudViewQuery groupBySorting() Group by the sorting column
 * @method     ChildCrudViewQuery groupByIsHidden() Group by the is_hidden column
 * @method     ChildCrudViewQuery groupByShowQuantity() Group by the show_quantity column
 * @method     ChildCrudViewQuery groupByDefaultOrderBy() Group by the default_order_by column
 * @method     ChildCrudViewQuery groupByDefaultOrderDir() Group by the default_order_dir column
 * @method     ChildCrudViewQuery groupByTabColorLogic() Group by the tab_color_logic column
 * @method     ChildCrudViewQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCrudViewQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCrudViewQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCrudViewQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCrudViewQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCrudViewQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCrudViewQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCrudViewQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCrudViewQuery leftJoinCrudConfig($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudConfig relation
 * @method     ChildCrudViewQuery rightJoinCrudConfig($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudConfig relation
 * @method     ChildCrudViewQuery innerJoinCrudConfig($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudConfig relation
 *
 * @method     ChildCrudViewQuery joinWithCrudConfig($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudConfig relation
 *
 * @method     ChildCrudViewQuery leftJoinWithCrudConfig() Adds a LEFT JOIN clause and with to the query using the CrudConfig relation
 * @method     ChildCrudViewQuery rightJoinWithCrudConfig() Adds a RIGHT JOIN clause and with to the query using the CrudConfig relation
 * @method     ChildCrudViewQuery innerJoinWithCrudConfig() Adds a INNER JOIN clause and with to the query using the CrudConfig relation
 *
 * @method     ChildCrudViewQuery leftJoinCrudView($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudView relation
 * @method     ChildCrudViewQuery rightJoinCrudView($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudView relation
 * @method     ChildCrudViewQuery innerJoinCrudView($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudView relation
 *
 * @method     ChildCrudViewQuery joinWithCrudView($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudView relation
 *
 * @method     ChildCrudViewQuery leftJoinWithCrudView() Adds a LEFT JOIN clause and with to the query using the CrudView relation
 * @method     ChildCrudViewQuery rightJoinWithCrudView() Adds a RIGHT JOIN clause and with to the query using the CrudView relation
 * @method     ChildCrudViewQuery innerJoinWithCrudView() Adds a INNER JOIN clause and with to the query using the CrudView relation
 *
 * @method     ChildCrudViewQuery leftJoinCrudViewButton($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudViewButton relation
 * @method     ChildCrudViewQuery rightJoinCrudViewButton($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudViewButton relation
 * @method     ChildCrudViewQuery innerJoinCrudViewButton($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudViewButton relation
 *
 * @method     ChildCrudViewQuery joinWithCrudViewButton($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudViewButton relation
 *
 * @method     ChildCrudViewQuery leftJoinWithCrudViewButton() Adds a LEFT JOIN clause and with to the query using the CrudViewButton relation
 * @method     ChildCrudViewQuery rightJoinWithCrudViewButton() Adds a RIGHT JOIN clause and with to the query using the CrudViewButton relation
 * @method     ChildCrudViewQuery innerJoinWithCrudViewButton() Adds a INNER JOIN clause and with to the query using the CrudViewButton relation
 *
 * @method     ChildCrudViewQuery leftJoinCrudViewVisibleFilter($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudViewVisibleFilter relation
 * @method     ChildCrudViewQuery rightJoinCrudViewVisibleFilter($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudViewVisibleFilter relation
 * @method     ChildCrudViewQuery innerJoinCrudViewVisibleFilter($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudViewVisibleFilter relation
 *
 * @method     ChildCrudViewQuery joinWithCrudViewVisibleFilter($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudViewVisibleFilter relation
 *
 * @method     ChildCrudViewQuery leftJoinWithCrudViewVisibleFilter() Adds a LEFT JOIN clause and with to the query using the CrudViewVisibleFilter relation
 * @method     ChildCrudViewQuery rightJoinWithCrudViewVisibleFilter() Adds a RIGHT JOIN clause and with to the query using the CrudViewVisibleFilter relation
 * @method     ChildCrudViewQuery innerJoinWithCrudViewVisibleFilter() Adds a INNER JOIN clause and with to the query using the CrudViewVisibleFilter relation
 *
 * @method     ChildCrudViewQuery leftJoinCrudViewHiddenFilter($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudViewHiddenFilter relation
 * @method     ChildCrudViewQuery rightJoinCrudViewHiddenFilter($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudViewHiddenFilter relation
 * @method     ChildCrudViewQuery innerJoinCrudViewHiddenFilter($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudViewHiddenFilter relation
 *
 * @method     ChildCrudViewQuery joinWithCrudViewHiddenFilter($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudViewHiddenFilter relation
 *
 * @method     ChildCrudViewQuery leftJoinWithCrudViewHiddenFilter() Adds a LEFT JOIN clause and with to the query using the CrudViewHiddenFilter relation
 * @method     ChildCrudViewQuery rightJoinWithCrudViewHiddenFilter() Adds a RIGHT JOIN clause and with to the query using the CrudViewHiddenFilter relation
 * @method     ChildCrudViewQuery innerJoinWithCrudViewHiddenFilter() Adds a INNER JOIN clause and with to the query using the CrudViewHiddenFilter relation
 *
 * @method     \Model\Setting\CrudManager\CrudConfigQuery|\Model\Setting\CrudManager\CrudViewFieldQuery|\Model\Setting\CrudManager\CrudViewButtonQuery|\Model\Setting\CrudManager\CrudViewVisibleFilterQuery|\Model\Setting\CrudManager\CrudViewHiddenFilterQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCrudView findOne(ConnectionInterface $con = null) Return the first ChildCrudView matching the query
 * @method     ChildCrudView findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCrudView matching the query, or a new ChildCrudView object populated from the query conditions when no match is found
 *
 * @method     ChildCrudView findOneById(int $id) Return the first ChildCrudView filtered by the id column
 * @method     ChildCrudView findOneByCrudConfigId(int $crud_config_id) Return the first ChildCrudView filtered by the crud_config_id column
 * @method     ChildCrudView findOneByName(string $name) Return the first ChildCrudView filtered by the name column
 * @method     ChildCrudView findOneByCode(string $code) Return the first ChildCrudView filtered by the code column
 * @method     ChildCrudView findOneBySorting(int $sorting) Return the first ChildCrudView filtered by the sorting column
 * @method     ChildCrudView findOneByIsHidden(boolean $is_hidden) Return the first ChildCrudView filtered by the is_hidden column
 * @method     ChildCrudView findOneByShowQuantity(boolean $show_quantity) Return the first ChildCrudView filtered by the show_quantity column
 * @method     ChildCrudView findOneByDefaultOrderBy(string $default_order_by) Return the first ChildCrudView filtered by the default_order_by column
 * @method     ChildCrudView findOneByDefaultOrderDir(string $default_order_dir) Return the first ChildCrudView filtered by the default_order_dir column
 * @method     ChildCrudView findOneByTabColorLogic(string $tab_color_logic) Return the first ChildCrudView filtered by the tab_color_logic column
 * @method     ChildCrudView findOneByCreatedAt(string $created_at) Return the first ChildCrudView filtered by the created_at column
 * @method     ChildCrudView findOneByUpdatedAt(string $updated_at) Return the first ChildCrudView filtered by the updated_at column *

 * @method     ChildCrudView requirePk($key, ConnectionInterface $con = null) Return the ChildCrudView by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudView requireOne(ConnectionInterface $con = null) Return the first ChildCrudView matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudView requireOneById(int $id) Return the first ChildCrudView filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudView requireOneByCrudConfigId(int $crud_config_id) Return the first ChildCrudView filtered by the crud_config_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudView requireOneByName(string $name) Return the first ChildCrudView filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudView requireOneByCode(string $code) Return the first ChildCrudView filtered by the code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudView requireOneBySorting(int $sorting) Return the first ChildCrudView filtered by the sorting column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudView requireOneByIsHidden(boolean $is_hidden) Return the first ChildCrudView filtered by the is_hidden column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudView requireOneByShowQuantity(boolean $show_quantity) Return the first ChildCrudView filtered by the show_quantity column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudView requireOneByDefaultOrderBy(string $default_order_by) Return the first ChildCrudView filtered by the default_order_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudView requireOneByDefaultOrderDir(string $default_order_dir) Return the first ChildCrudView filtered by the default_order_dir column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudView requireOneByTabColorLogic(string $tab_color_logic) Return the first ChildCrudView filtered by the tab_color_logic column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudView requireOneByCreatedAt(string $created_at) Return the first ChildCrudView filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudView requireOneByUpdatedAt(string $updated_at) Return the first ChildCrudView filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudView[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCrudView objects based on current ModelCriteria
 * @method     ChildCrudView[]|ObjectCollection findById(int $id) Return ChildCrudView objects filtered by the id column
 * @method     ChildCrudView[]|ObjectCollection findByCrudConfigId(int $crud_config_id) Return ChildCrudView objects filtered by the crud_config_id column
 * @method     ChildCrudView[]|ObjectCollection findByName(string $name) Return ChildCrudView objects filtered by the name column
 * @method     ChildCrudView[]|ObjectCollection findByCode(string $code) Return ChildCrudView objects filtered by the code column
 * @method     ChildCrudView[]|ObjectCollection findBySorting(int $sorting) Return ChildCrudView objects filtered by the sorting column
 * @method     ChildCrudView[]|ObjectCollection findByIsHidden(boolean $is_hidden) Return ChildCrudView objects filtered by the is_hidden column
 * @method     ChildCrudView[]|ObjectCollection findByShowQuantity(boolean $show_quantity) Return ChildCrudView objects filtered by the show_quantity column
 * @method     ChildCrudView[]|ObjectCollection findByDefaultOrderBy(string $default_order_by) Return ChildCrudView objects filtered by the default_order_by column
 * @method     ChildCrudView[]|ObjectCollection findByDefaultOrderDir(string $default_order_dir) Return ChildCrudView objects filtered by the default_order_dir column
 * @method     ChildCrudView[]|ObjectCollection findByTabColorLogic(string $tab_color_logic) Return ChildCrudView objects filtered by the tab_color_logic column
 * @method     ChildCrudView[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCrudView objects filtered by the created_at column
 * @method     ChildCrudView[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCrudView objects filtered by the updated_at column
 * @method     ChildCrudView[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CrudViewQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\CrudManager\Base\CrudViewQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\CrudManager\\CrudView', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCrudViewQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCrudViewQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCrudViewQuery) {
            return $criteria;
        }
        $query = new ChildCrudViewQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCrudView|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CrudViewTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CrudViewTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudView A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, crud_config_id, name, code, sorting, is_hidden, show_quantity, default_order_by, default_order_dir, tab_color_logic, created_at, updated_at FROM crud_view WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCrudView $obj */
            $obj = new ChildCrudView();
            $obj->hydrate($row);
            CrudViewTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCrudView|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CrudViewTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CrudViewTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CrudViewTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CrudViewTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the crud_config_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCrudConfigId(1234); // WHERE crud_config_id = 1234
     * $query->filterByCrudConfigId(array(12, 34)); // WHERE crud_config_id IN (12, 34)
     * $query->filterByCrudConfigId(array('min' => 12)); // WHERE crud_config_id > 12
     * </code>
     *
     * @see       filterByCrudConfig()
     *
     * @param     mixed $crudConfigId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByCrudConfigId($crudConfigId = null, $comparison = null)
    {
        if (is_array($crudConfigId)) {
            $useMinMax = false;
            if (isset($crudConfigId['min'])) {
                $this->addUsingAlias(CrudViewTableMap::COL_CRUD_CONFIG_ID, $crudConfigId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($crudConfigId['max'])) {
                $this->addUsingAlias(CrudViewTableMap::COL_CRUD_CONFIG_ID, $crudConfigId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewTableMap::COL_CRUD_CONFIG_ID, $crudConfigId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%', Criteria::LIKE); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewTableMap::COL_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the sorting column
     *
     * Example usage:
     * <code>
     * $query->filterBySorting(1234); // WHERE sorting = 1234
     * $query->filterBySorting(array(12, 34)); // WHERE sorting IN (12, 34)
     * $query->filterBySorting(array('min' => 12)); // WHERE sorting > 12
     * </code>
     *
     * @param     mixed $sorting The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterBySorting($sorting = null, $comparison = null)
    {
        if (is_array($sorting)) {
            $useMinMax = false;
            if (isset($sorting['min'])) {
                $this->addUsingAlias(CrudViewTableMap::COL_SORTING, $sorting['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sorting['max'])) {
                $this->addUsingAlias(CrudViewTableMap::COL_SORTING, $sorting['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewTableMap::COL_SORTING, $sorting, $comparison);
    }

    /**
     * Filter the query on the is_hidden column
     *
     * Example usage:
     * <code>
     * $query->filterByIsHidden(true); // WHERE is_hidden = true
     * $query->filterByIsHidden('yes'); // WHERE is_hidden = true
     * </code>
     *
     * @param     boolean|string $isHidden The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByIsHidden($isHidden = null, $comparison = null)
    {
        if (is_string($isHidden)) {
            $isHidden = in_array(strtolower($isHidden), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CrudViewTableMap::COL_IS_HIDDEN, $isHidden, $comparison);
    }

    /**
     * Filter the query on the show_quantity column
     *
     * Example usage:
     * <code>
     * $query->filterByShowQuantity(true); // WHERE show_quantity = true
     * $query->filterByShowQuantity('yes'); // WHERE show_quantity = true
     * </code>
     *
     * @param     boolean|string $showQuantity The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByShowQuantity($showQuantity = null, $comparison = null)
    {
        if (is_string($showQuantity)) {
            $showQuantity = in_array(strtolower($showQuantity), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CrudViewTableMap::COL_SHOW_QUANTITY, $showQuantity, $comparison);
    }

    /**
     * Filter the query on the default_order_by column
     *
     * Example usage:
     * <code>
     * $query->filterByDefaultOrderBy('fooValue');   // WHERE default_order_by = 'fooValue'
     * $query->filterByDefaultOrderBy('%fooValue%', Criteria::LIKE); // WHERE default_order_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $defaultOrderBy The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByDefaultOrderBy($defaultOrderBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($defaultOrderBy)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewTableMap::COL_DEFAULT_ORDER_BY, $defaultOrderBy, $comparison);
    }

    /**
     * Filter the query on the default_order_dir column
     *
     * Example usage:
     * <code>
     * $query->filterByDefaultOrderDir('fooValue');   // WHERE default_order_dir = 'fooValue'
     * $query->filterByDefaultOrderDir('%fooValue%', Criteria::LIKE); // WHERE default_order_dir LIKE '%fooValue%'
     * </code>
     *
     * @param     string $defaultOrderDir The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByDefaultOrderDir($defaultOrderDir = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($defaultOrderDir)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewTableMap::COL_DEFAULT_ORDER_DIR, $defaultOrderDir, $comparison);
    }

    /**
     * Filter the query on the tab_color_logic column
     *
     * Example usage:
     * <code>
     * $query->filterByTabColorLogic('fooValue');   // WHERE tab_color_logic = 'fooValue'
     * $query->filterByTabColorLogic('%fooValue%', Criteria::LIKE); // WHERE tab_color_logic LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tabColorLogic The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByTabColorLogic($tabColorLogic = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tabColorLogic)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewTableMap::COL_TAB_COLOR_LOGIC, $tabColorLogic, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CrudViewTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CrudViewTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CrudViewTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CrudViewTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudConfig object
     *
     * @param \Model\Setting\CrudManager\CrudConfig|ObjectCollection $crudConfig The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByCrudConfig($crudConfig, $comparison = null)
    {
        if ($crudConfig instanceof \Model\Setting\CrudManager\CrudConfig) {
            return $this
                ->addUsingAlias(CrudViewTableMap::COL_CRUD_CONFIG_ID, $crudConfig->getId(), $comparison);
        } elseif ($crudConfig instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CrudViewTableMap::COL_CRUD_CONFIG_ID, $crudConfig->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCrudConfig() only accepts arguments of type \Model\Setting\CrudManager\CrudConfig or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudConfig relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function joinCrudConfig($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudConfig');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudConfig');
        }

        return $this;
    }

    /**
     * Use the CrudConfig relation CrudConfig object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudConfigQuery A secondary query class using the current class as primary query
     */
    public function useCrudConfigQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudConfig($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudConfig', '\Model\Setting\CrudManager\CrudConfigQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudViewField object
     *
     * @param \Model\Setting\CrudManager\CrudViewField|ObjectCollection $crudViewField the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByCrudView($crudViewField, $comparison = null)
    {
        if ($crudViewField instanceof \Model\Setting\CrudManager\CrudViewField) {
            return $this
                ->addUsingAlias(CrudViewTableMap::COL_ID, $crudViewField->getCrudViewId(), $comparison);
        } elseif ($crudViewField instanceof ObjectCollection) {
            return $this
                ->useCrudViewQuery()
                ->filterByPrimaryKeys($crudViewField->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudView() only accepts arguments of type \Model\Setting\CrudManager\CrudViewField or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudView relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function joinCrudView($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudView');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudView');
        }

        return $this;
    }

    /**
     * Use the CrudView relation CrudViewField object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudViewFieldQuery A secondary query class using the current class as primary query
     */
    public function useCrudViewQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudView($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudView', '\Model\Setting\CrudManager\CrudViewFieldQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudViewButton object
     *
     * @param \Model\Setting\CrudManager\CrudViewButton|ObjectCollection $crudViewButton the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByCrudViewButton($crudViewButton, $comparison = null)
    {
        if ($crudViewButton instanceof \Model\Setting\CrudManager\CrudViewButton) {
            return $this
                ->addUsingAlias(CrudViewTableMap::COL_ID, $crudViewButton->getCrudViewId(), $comparison);
        } elseif ($crudViewButton instanceof ObjectCollection) {
            return $this
                ->useCrudViewButtonQuery()
                ->filterByPrimaryKeys($crudViewButton->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudViewButton() only accepts arguments of type \Model\Setting\CrudManager\CrudViewButton or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudViewButton relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function joinCrudViewButton($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudViewButton');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudViewButton');
        }

        return $this;
    }

    /**
     * Use the CrudViewButton relation CrudViewButton object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudViewButtonQuery A secondary query class using the current class as primary query
     */
    public function useCrudViewButtonQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudViewButton($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudViewButton', '\Model\Setting\CrudManager\CrudViewButtonQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudViewVisibleFilter object
     *
     * @param \Model\Setting\CrudManager\CrudViewVisibleFilter|ObjectCollection $crudViewVisibleFilter the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByCrudViewVisibleFilter($crudViewVisibleFilter, $comparison = null)
    {
        if ($crudViewVisibleFilter instanceof \Model\Setting\CrudManager\CrudViewVisibleFilter) {
            return $this
                ->addUsingAlias(CrudViewTableMap::COL_ID, $crudViewVisibleFilter->getCrudViewId(), $comparison);
        } elseif ($crudViewVisibleFilter instanceof ObjectCollection) {
            return $this
                ->useCrudViewVisibleFilterQuery()
                ->filterByPrimaryKeys($crudViewVisibleFilter->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudViewVisibleFilter() only accepts arguments of type \Model\Setting\CrudManager\CrudViewVisibleFilter or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudViewVisibleFilter relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function joinCrudViewVisibleFilter($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudViewVisibleFilter');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudViewVisibleFilter');
        }

        return $this;
    }

    /**
     * Use the CrudViewVisibleFilter relation CrudViewVisibleFilter object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudViewVisibleFilterQuery A secondary query class using the current class as primary query
     */
    public function useCrudViewVisibleFilterQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudViewVisibleFilter($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudViewVisibleFilter', '\Model\Setting\CrudManager\CrudViewVisibleFilterQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudViewHiddenFilter object
     *
     * @param \Model\Setting\CrudManager\CrudViewHiddenFilter|ObjectCollection $crudViewHiddenFilter the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudViewQuery The current query, for fluid interface
     */
    public function filterByCrudViewHiddenFilter($crudViewHiddenFilter, $comparison = null)
    {
        if ($crudViewHiddenFilter instanceof \Model\Setting\CrudManager\CrudViewHiddenFilter) {
            return $this
                ->addUsingAlias(CrudViewTableMap::COL_ID, $crudViewHiddenFilter->getCrudViewId(), $comparison);
        } elseif ($crudViewHiddenFilter instanceof ObjectCollection) {
            return $this
                ->useCrudViewHiddenFilterQuery()
                ->filterByPrimaryKeys($crudViewHiddenFilter->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudViewHiddenFilter() only accepts arguments of type \Model\Setting\CrudManager\CrudViewHiddenFilter or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudViewHiddenFilter relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function joinCrudViewHiddenFilter($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudViewHiddenFilter');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudViewHiddenFilter');
        }

        return $this;
    }

    /**
     * Use the CrudViewHiddenFilter relation CrudViewHiddenFilter object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudViewHiddenFilterQuery A secondary query class using the current class as primary query
     */
    public function useCrudViewHiddenFilterQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudViewHiddenFilter($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudViewHiddenFilter', '\Model\Setting\CrudManager\CrudViewHiddenFilterQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCrudView $crudView Object to remove from the list of results
     *
     * @return $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function prune($crudView = null)
    {
        if ($crudView) {
            $this->addUsingAlias(CrudViewTableMap::COL_ID, $crudView->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the crud_view table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CrudViewTableMap::clearInstancePool();
            CrudViewTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CrudViewTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CrudViewTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CrudViewTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudViewTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudViewTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudViewTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudViewTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudViewTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCrudViewQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudViewTableMap::COL_CREATED_AT);
    }

} // CrudViewQuery
