<?php

namespace Model\Setting\CrudManager\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Setting\CrudManager\CrudEditor as ChildCrudEditor;
use Model\Setting\CrudManager\CrudEditorButton as ChildCrudEditorButton;
use Model\Setting\CrudManager\CrudEditorButtonEvent as ChildCrudEditorButtonEvent;
use Model\Setting\CrudManager\CrudEditorButtonEventQuery as ChildCrudEditorButtonEventQuery;
use Model\Setting\CrudManager\CrudEditorButtonQuery as ChildCrudEditorButtonQuery;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilter as ChildCrudEditorButtonVisibileFilter;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery as ChildCrudEditorButtonVisibileFilterQuery;
use Model\Setting\CrudManager\CrudEditorQuery as ChildCrudEditorQuery;
use Model\Setting\CrudManager\Map\CrudEditorButtonEventTableMap;
use Model\Setting\CrudManager\Map\CrudEditorButtonTableMap;
use Model\Setting\CrudManager\Map\CrudEditorButtonVisibileFilterTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'crud_editor_button' table.
 *
 *
 *
 * @package    propel.generator.Model.Setting.CrudManager.Base
 */
abstract class CrudEditorButton implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Setting\\CrudManager\\Map\\CrudEditorButtonTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the crud_editor_id field.
     *
     * @var        int
     */
    protected $crud_editor_id;

    /**
     * The value for the title field.
     *
     * @var        string|null
     */
    protected $title;

    /**
     * The value for the icon_before_click field.
     *
     * @var        string|null
     */
    protected $icon_before_click;

    /**
     * The value for the icon_after_click field.
     *
     * @var        string|null
     */
    protected $icon_after_click;

    /**
     * The value for the color_before_click field.
     *
     * @var        string|null
     */
    protected $color_before_click;

    /**
     * The value for the color_after_click field.
     *
     * @var        string|null
     */
    protected $color_after_click;

    /**
     * The value for the is_reusable field.
     *
     * @var        boolean|null
     */
    protected $is_reusable;

    /**
     * The value for the sorting field.
     *
     * @var        int|null
     */
    protected $sorting;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        ChildCrudEditor
     */
    protected $aCrudEditor;

    /**
     * @var        ObjectCollection|ChildCrudEditorButtonVisibileFilter[] Collection to store aggregation of ChildCrudEditorButtonVisibileFilter objects.
     */
    protected $collCrudEditorButtonVisibileFilters;
    protected $collCrudEditorButtonVisibileFiltersPartial;

    /**
     * @var        ObjectCollection|ChildCrudEditorButtonEvent[] Collection to store aggregation of ChildCrudEditorButtonEvent objects.
     */
    protected $collCrudEditorButtonEvents;
    protected $collCrudEditorButtonEventsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCrudEditorButtonVisibileFilter[]
     */
    protected $crudEditorButtonVisibileFiltersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCrudEditorButtonEvent[]
     */
    protected $crudEditorButtonEventsScheduledForDeletion = null;

    /**
     * Initializes internal state of Model\Setting\CrudManager\Base\CrudEditorButton object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>CrudEditorButton</code> instance.  If
     * <code>obj</code> is an instance of <code>CrudEditorButton</code>, delegates to
     * <code>equals(CrudEditorButton)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [crud_editor_id] column value.
     *
     * @return int
     */
    public function getCrudEditorId()
    {
        return $this->crud_editor_id;
    }

    /**
     * Get the [title] column value.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [icon_before_click] column value.
     *
     * @return string|null
     */
    public function getIconBeforeClick()
    {
        return $this->icon_before_click;
    }

    /**
     * Get the [icon_after_click] column value.
     *
     * @return string|null
     */
    public function getIconAfterClick()
    {
        return $this->icon_after_click;
    }

    /**
     * Get the [color_before_click] column value.
     *
     * @return string|null
     */
    public function getColorBeforeClick()
    {
        return $this->color_before_click;
    }

    /**
     * Get the [color_after_click] column value.
     *
     * @return string|null
     */
    public function getColorAfterClick()
    {
        return $this->color_after_click;
    }

    /**
     * Get the [is_reusable] column value.
     *
     * @return boolean|null
     */
    public function getIsReusable()
    {
        return $this->is_reusable;
    }

    /**
     * Get the [is_reusable] column value.
     *
     * @return boolean|null
     */
    public function isReusable()
    {
        return $this->getIsReusable();
    }

    /**
     * Get the [sorting] column value.
     *
     * @return int|null
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[CrudEditorButtonTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [crud_editor_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     */
    public function setCrudEditorId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->crud_editor_id !== $v) {
            $this->crud_editor_id = $v;
            $this->modifiedColumns[CrudEditorButtonTableMap::COL_CRUD_EDITOR_ID] = true;
        }

        if ($this->aCrudEditor !== null && $this->aCrudEditor->getId() !== $v) {
            $this->aCrudEditor = null;
        }

        return $this;
    } // setCrudEditorId()

    /**
     * Set the value of [title] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[CrudEditorButtonTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [icon_before_click] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     */
    public function setIconBeforeClick($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->icon_before_click !== $v) {
            $this->icon_before_click = $v;
            $this->modifiedColumns[CrudEditorButtonTableMap::COL_ICON_BEFORE_CLICK] = true;
        }

        return $this;
    } // setIconBeforeClick()

    /**
     * Set the value of [icon_after_click] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     */
    public function setIconAfterClick($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->icon_after_click !== $v) {
            $this->icon_after_click = $v;
            $this->modifiedColumns[CrudEditorButtonTableMap::COL_ICON_AFTER_CLICK] = true;
        }

        return $this;
    } // setIconAfterClick()

    /**
     * Set the value of [color_before_click] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     */
    public function setColorBeforeClick($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->color_before_click !== $v) {
            $this->color_before_click = $v;
            $this->modifiedColumns[CrudEditorButtonTableMap::COL_COLOR_BEFORE_CLICK] = true;
        }

        return $this;
    } // setColorBeforeClick()

    /**
     * Set the value of [color_after_click] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     */
    public function setColorAfterClick($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->color_after_click !== $v) {
            $this->color_after_click = $v;
            $this->modifiedColumns[CrudEditorButtonTableMap::COL_COLOR_AFTER_CLICK] = true;
        }

        return $this;
    } // setColorAfterClick()

    /**
     * Sets the value of the [is_reusable] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     */
    public function setIsReusable($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_reusable !== $v) {
            $this->is_reusable = $v;
            $this->modifiedColumns[CrudEditorButtonTableMap::COL_IS_REUSABLE] = true;
        }

        return $this;
    } // setIsReusable()

    /**
     * Set the value of [sorting] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     */
    public function setSorting($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sorting !== $v) {
            $this->sorting = $v;
            $this->modifiedColumns[CrudEditorButtonTableMap::COL_SORTING] = true;
        }

        return $this;
    } // setSorting()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CrudEditorButtonTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CrudEditorButtonTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CrudEditorButtonTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CrudEditorButtonTableMap::translateFieldName('CrudEditorId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->crud_editor_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CrudEditorButtonTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CrudEditorButtonTableMap::translateFieldName('IconBeforeClick', TableMap::TYPE_PHPNAME, $indexType)];
            $this->icon_before_click = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CrudEditorButtonTableMap::translateFieldName('IconAfterClick', TableMap::TYPE_PHPNAME, $indexType)];
            $this->icon_after_click = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CrudEditorButtonTableMap::translateFieldName('ColorBeforeClick', TableMap::TYPE_PHPNAME, $indexType)];
            $this->color_before_click = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CrudEditorButtonTableMap::translateFieldName('ColorAfterClick', TableMap::TYPE_PHPNAME, $indexType)];
            $this->color_after_click = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CrudEditorButtonTableMap::translateFieldName('IsReusable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_reusable = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CrudEditorButtonTableMap::translateFieldName('Sorting', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sorting = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CrudEditorButtonTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : CrudEditorButtonTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 11; // 11 = CrudEditorButtonTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Setting\\CrudManager\\CrudEditorButton'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCrudEditor !== null && $this->crud_editor_id !== $this->aCrudEditor->getId()) {
            $this->aCrudEditor = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CrudEditorButtonTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCrudEditorButtonQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCrudEditor = null;
            $this->collCrudEditorButtonVisibileFilters = null;

            $this->collCrudEditorButtonEvents = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see CrudEditorButton::setDeleted()
     * @see CrudEditorButton::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorButtonTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCrudEditorButtonQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorButtonTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(CrudEditorButtonTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(CrudEditorButtonTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(CrudEditorButtonTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CrudEditorButtonTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCrudEditor !== null) {
                if ($this->aCrudEditor->isModified() || $this->aCrudEditor->isNew()) {
                    $affectedRows += $this->aCrudEditor->save($con);
                }
                $this->setCrudEditor($this->aCrudEditor);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->crudEditorButtonVisibileFiltersScheduledForDeletion !== null) {
                if (!$this->crudEditorButtonVisibileFiltersScheduledForDeletion->isEmpty()) {
                    \Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery::create()
                        ->filterByPrimaryKeys($this->crudEditorButtonVisibileFiltersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->crudEditorButtonVisibileFiltersScheduledForDeletion = null;
                }
            }

            if ($this->collCrudEditorButtonVisibileFilters !== null) {
                foreach ($this->collCrudEditorButtonVisibileFilters as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->crudEditorButtonEventsScheduledForDeletion !== null) {
                if (!$this->crudEditorButtonEventsScheduledForDeletion->isEmpty()) {
                    \Model\Setting\CrudManager\CrudEditorButtonEventQuery::create()
                        ->filterByPrimaryKeys($this->crudEditorButtonEventsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->crudEditorButtonEventsScheduledForDeletion = null;
                }
            }

            if ($this->collCrudEditorButtonEvents !== null) {
                foreach ($this->collCrudEditorButtonEvents as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CrudEditorButtonTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CrudEditorButtonTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_CRUD_EDITOR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'crud_editor_id';
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_ICON_BEFORE_CLICK)) {
            $modifiedColumns[':p' . $index++]  = 'icon_before_click';
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_ICON_AFTER_CLICK)) {
            $modifiedColumns[':p' . $index++]  = 'icon_after_click';
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_COLOR_BEFORE_CLICK)) {
            $modifiedColumns[':p' . $index++]  = 'color_before_click';
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_COLOR_AFTER_CLICK)) {
            $modifiedColumns[':p' . $index++]  = 'color_after_click';
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_IS_REUSABLE)) {
            $modifiedColumns[':p' . $index++]  = 'is_reusable';
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_SORTING)) {
            $modifiedColumns[':p' . $index++]  = 'sorting';
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO crud_editor_button (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'crud_editor_id':
                        $stmt->bindValue($identifier, $this->crud_editor_id, PDO::PARAM_INT);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'icon_before_click':
                        $stmt->bindValue($identifier, $this->icon_before_click, PDO::PARAM_STR);
                        break;
                    case 'icon_after_click':
                        $stmt->bindValue($identifier, $this->icon_after_click, PDO::PARAM_STR);
                        break;
                    case 'color_before_click':
                        $stmt->bindValue($identifier, $this->color_before_click, PDO::PARAM_STR);
                        break;
                    case 'color_after_click':
                        $stmt->bindValue($identifier, $this->color_after_click, PDO::PARAM_STR);
                        break;
                    case 'is_reusable':
                        $stmt->bindValue($identifier, (int) $this->is_reusable, PDO::PARAM_INT);
                        break;
                    case 'sorting':
                        $stmt->bindValue($identifier, $this->sorting, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CrudEditorButtonTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getCrudEditorId();
                break;
            case 2:
                return $this->getTitle();
                break;
            case 3:
                return $this->getIconBeforeClick();
                break;
            case 4:
                return $this->getIconAfterClick();
                break;
            case 5:
                return $this->getColorBeforeClick();
                break;
            case 6:
                return $this->getColorAfterClick();
                break;
            case 7:
                return $this->getIsReusable();
                break;
            case 8:
                return $this->getSorting();
                break;
            case 9:
                return $this->getCreatedAt();
                break;
            case 10:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['CrudEditorButton'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CrudEditorButton'][$this->hashCode()] = true;
        $keys = CrudEditorButtonTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getCrudEditorId(),
            $keys[2] => $this->getTitle(),
            $keys[3] => $this->getIconBeforeClick(),
            $keys[4] => $this->getIconAfterClick(),
            $keys[5] => $this->getColorBeforeClick(),
            $keys[6] => $this->getColorAfterClick(),
            $keys[7] => $this->getIsReusable(),
            $keys[8] => $this->getSorting(),
            $keys[9] => $this->getCreatedAt(),
            $keys[10] => $this->getUpdatedAt(),
        );
        if ($result[$keys[9]] instanceof \DateTimeInterface) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCrudEditor) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'crudEditor';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'crud_editor';
                        break;
                    default:
                        $key = 'CrudEditor';
                }

                $result[$key] = $this->aCrudEditor->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCrudEditorButtonVisibileFilters) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'crudEditorButtonVisibileFilters';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'crud_editor_button_visibile_filters';
                        break;
                    default:
                        $key = 'CrudEditorButtonVisibileFilters';
                }

                $result[$key] = $this->collCrudEditorButtonVisibileFilters->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCrudEditorButtonEvents) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'crudEditorButtonEvents';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'crud_editor_button_events';
                        break;
                    default:
                        $key = 'CrudEditorButtonEvents';
                }

                $result[$key] = $this->collCrudEditorButtonEvents->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CrudEditorButtonTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setCrudEditorId($value);
                break;
            case 2:
                $this->setTitle($value);
                break;
            case 3:
                $this->setIconBeforeClick($value);
                break;
            case 4:
                $this->setIconAfterClick($value);
                break;
            case 5:
                $this->setColorBeforeClick($value);
                break;
            case 6:
                $this->setColorAfterClick($value);
                break;
            case 7:
                $this->setIsReusable($value);
                break;
            case 8:
                $this->setSorting($value);
                break;
            case 9:
                $this->setCreatedAt($value);
                break;
            case 10:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CrudEditorButtonTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setCrudEditorId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setTitle($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setIconBeforeClick($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setIconAfterClick($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setColorBeforeClick($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setColorAfterClick($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setIsReusable($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setSorting($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setCreatedAt($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setUpdatedAt($arr[$keys[10]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CrudEditorButtonTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_ID)) {
            $criteria->add(CrudEditorButtonTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_CRUD_EDITOR_ID)) {
            $criteria->add(CrudEditorButtonTableMap::COL_CRUD_EDITOR_ID, $this->crud_editor_id);
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_TITLE)) {
            $criteria->add(CrudEditorButtonTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_ICON_BEFORE_CLICK)) {
            $criteria->add(CrudEditorButtonTableMap::COL_ICON_BEFORE_CLICK, $this->icon_before_click);
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_ICON_AFTER_CLICK)) {
            $criteria->add(CrudEditorButtonTableMap::COL_ICON_AFTER_CLICK, $this->icon_after_click);
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_COLOR_BEFORE_CLICK)) {
            $criteria->add(CrudEditorButtonTableMap::COL_COLOR_BEFORE_CLICK, $this->color_before_click);
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_COLOR_AFTER_CLICK)) {
            $criteria->add(CrudEditorButtonTableMap::COL_COLOR_AFTER_CLICK, $this->color_after_click);
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_IS_REUSABLE)) {
            $criteria->add(CrudEditorButtonTableMap::COL_IS_REUSABLE, $this->is_reusable);
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_SORTING)) {
            $criteria->add(CrudEditorButtonTableMap::COL_SORTING, $this->sorting);
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_CREATED_AT)) {
            $criteria->add(CrudEditorButtonTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(CrudEditorButtonTableMap::COL_UPDATED_AT)) {
            $criteria->add(CrudEditorButtonTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCrudEditorButtonQuery::create();
        $criteria->add(CrudEditorButtonTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Setting\CrudManager\CrudEditorButton (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setCrudEditorId($this->getCrudEditorId());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setIconBeforeClick($this->getIconBeforeClick());
        $copyObj->setIconAfterClick($this->getIconAfterClick());
        $copyObj->setColorBeforeClick($this->getColorBeforeClick());
        $copyObj->setColorAfterClick($this->getColorAfterClick());
        $copyObj->setIsReusable($this->getIsReusable());
        $copyObj->setSorting($this->getSorting());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCrudEditorButtonVisibileFilters() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCrudEditorButtonVisibileFilter($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCrudEditorButtonEvents() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCrudEditorButtonEvent($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Setting\CrudManager\CrudEditorButton Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildCrudEditor object.
     *
     * @param  ChildCrudEditor $v
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCrudEditor(ChildCrudEditor $v = null)
    {
        if ($v === null) {
            $this->setCrudEditorId(NULL);
        } else {
            $this->setCrudEditorId($v->getId());
        }

        $this->aCrudEditor = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCrudEditor object, it will not be re-added.
        if ($v !== null) {
            $v->addCrudEditorButton($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCrudEditor object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCrudEditor The associated ChildCrudEditor object.
     * @throws PropelException
     */
    public function getCrudEditor(ConnectionInterface $con = null)
    {
        if ($this->aCrudEditor === null && ($this->crud_editor_id != 0)) {
            $this->aCrudEditor = ChildCrudEditorQuery::create()->findPk($this->crud_editor_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCrudEditor->addCrudEditorButtons($this);
             */
        }

        return $this->aCrudEditor;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CrudEditorButtonVisibileFilter' === $relationName) {
            $this->initCrudEditorButtonVisibileFilters();
            return;
        }
        if ('CrudEditorButtonEvent' === $relationName) {
            $this->initCrudEditorButtonEvents();
            return;
        }
    }

    /**
     * Clears out the collCrudEditorButtonVisibileFilters collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCrudEditorButtonVisibileFilters()
     */
    public function clearCrudEditorButtonVisibileFilters()
    {
        $this->collCrudEditorButtonVisibileFilters = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCrudEditorButtonVisibileFilters collection loaded partially.
     */
    public function resetPartialCrudEditorButtonVisibileFilters($v = true)
    {
        $this->collCrudEditorButtonVisibileFiltersPartial = $v;
    }

    /**
     * Initializes the collCrudEditorButtonVisibileFilters collection.
     *
     * By default this just sets the collCrudEditorButtonVisibileFilters collection to an empty array (like clearcollCrudEditorButtonVisibileFilters());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCrudEditorButtonVisibileFilters($overrideExisting = true)
    {
        if (null !== $this->collCrudEditorButtonVisibileFilters && !$overrideExisting) {
            return;
        }

        $collectionClassName = CrudEditorButtonVisibileFilterTableMap::getTableMap()->getCollectionClassName();

        $this->collCrudEditorButtonVisibileFilters = new $collectionClassName;
        $this->collCrudEditorButtonVisibileFilters->setModel('\Model\Setting\CrudManager\CrudEditorButtonVisibileFilter');
    }

    /**
     * Gets an array of ChildCrudEditorButtonVisibileFilter objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCrudEditorButton is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCrudEditorButtonVisibileFilter[] List of ChildCrudEditorButtonVisibileFilter objects
     * @throws PropelException
     */
    public function getCrudEditorButtonVisibileFilters(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudEditorButtonVisibileFiltersPartial && !$this->isNew();
        if (null === $this->collCrudEditorButtonVisibileFilters || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCrudEditorButtonVisibileFilters) {
                    $this->initCrudEditorButtonVisibileFilters();
                } else {
                    $collectionClassName = CrudEditorButtonVisibileFilterTableMap::getTableMap()->getCollectionClassName();

                    $collCrudEditorButtonVisibileFilters = new $collectionClassName;
                    $collCrudEditorButtonVisibileFilters->setModel('\Model\Setting\CrudManager\CrudEditorButtonVisibileFilter');

                    return $collCrudEditorButtonVisibileFilters;
                }
            } else {
                $collCrudEditorButtonVisibileFilters = ChildCrudEditorButtonVisibileFilterQuery::create(null, $criteria)
                    ->filterByCrudEditorButton($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCrudEditorButtonVisibileFiltersPartial && count($collCrudEditorButtonVisibileFilters)) {
                        $this->initCrudEditorButtonVisibileFilters(false);

                        foreach ($collCrudEditorButtonVisibileFilters as $obj) {
                            if (false == $this->collCrudEditorButtonVisibileFilters->contains($obj)) {
                                $this->collCrudEditorButtonVisibileFilters->append($obj);
                            }
                        }

                        $this->collCrudEditorButtonVisibileFiltersPartial = true;
                    }

                    return $collCrudEditorButtonVisibileFilters;
                }

                if ($partial && $this->collCrudEditorButtonVisibileFilters) {
                    foreach ($this->collCrudEditorButtonVisibileFilters as $obj) {
                        if ($obj->isNew()) {
                            $collCrudEditorButtonVisibileFilters[] = $obj;
                        }
                    }
                }

                $this->collCrudEditorButtonVisibileFilters = $collCrudEditorButtonVisibileFilters;
                $this->collCrudEditorButtonVisibileFiltersPartial = false;
            }
        }

        return $this->collCrudEditorButtonVisibileFilters;
    }

    /**
     * Sets a collection of ChildCrudEditorButtonVisibileFilter objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $crudEditorButtonVisibileFilters A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCrudEditorButton The current object (for fluent API support)
     */
    public function setCrudEditorButtonVisibileFilters(Collection $crudEditorButtonVisibileFilters, ConnectionInterface $con = null)
    {
        /** @var ChildCrudEditorButtonVisibileFilter[] $crudEditorButtonVisibileFiltersToDelete */
        $crudEditorButtonVisibileFiltersToDelete = $this->getCrudEditorButtonVisibileFilters(new Criteria(), $con)->diff($crudEditorButtonVisibileFilters);


        $this->crudEditorButtonVisibileFiltersScheduledForDeletion = $crudEditorButtonVisibileFiltersToDelete;

        foreach ($crudEditorButtonVisibileFiltersToDelete as $crudEditorButtonVisibileFilterRemoved) {
            $crudEditorButtonVisibileFilterRemoved->setCrudEditorButton(null);
        }

        $this->collCrudEditorButtonVisibileFilters = null;
        foreach ($crudEditorButtonVisibileFilters as $crudEditorButtonVisibileFilter) {
            $this->addCrudEditorButtonVisibileFilter($crudEditorButtonVisibileFilter);
        }

        $this->collCrudEditorButtonVisibileFilters = $crudEditorButtonVisibileFilters;
        $this->collCrudEditorButtonVisibileFiltersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CrudEditorButtonVisibileFilter objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CrudEditorButtonVisibileFilter objects.
     * @throws PropelException
     */
    public function countCrudEditorButtonVisibileFilters(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudEditorButtonVisibileFiltersPartial && !$this->isNew();
        if (null === $this->collCrudEditorButtonVisibileFilters || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCrudEditorButtonVisibileFilters) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCrudEditorButtonVisibileFilters());
            }

            $query = ChildCrudEditorButtonVisibileFilterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCrudEditorButton($this)
                ->count($con);
        }

        return count($this->collCrudEditorButtonVisibileFilters);
    }

    /**
     * Method called to associate a ChildCrudEditorButtonVisibileFilter object to this object
     * through the ChildCrudEditorButtonVisibileFilter foreign key attribute.
     *
     * @param  ChildCrudEditorButtonVisibileFilter $l ChildCrudEditorButtonVisibileFilter
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     */
    public function addCrudEditorButtonVisibileFilter(ChildCrudEditorButtonVisibileFilter $l)
    {
        if ($this->collCrudEditorButtonVisibileFilters === null) {
            $this->initCrudEditorButtonVisibileFilters();
            $this->collCrudEditorButtonVisibileFiltersPartial = true;
        }

        if (!$this->collCrudEditorButtonVisibileFilters->contains($l)) {
            $this->doAddCrudEditorButtonVisibileFilter($l);

            if ($this->crudEditorButtonVisibileFiltersScheduledForDeletion and $this->crudEditorButtonVisibileFiltersScheduledForDeletion->contains($l)) {
                $this->crudEditorButtonVisibileFiltersScheduledForDeletion->remove($this->crudEditorButtonVisibileFiltersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCrudEditorButtonVisibileFilter $crudEditorButtonVisibileFilter The ChildCrudEditorButtonVisibileFilter object to add.
     */
    protected function doAddCrudEditorButtonVisibileFilter(ChildCrudEditorButtonVisibileFilter $crudEditorButtonVisibileFilter)
    {
        $this->collCrudEditorButtonVisibileFilters[]= $crudEditorButtonVisibileFilter;
        $crudEditorButtonVisibileFilter->setCrudEditorButton($this);
    }

    /**
     * @param  ChildCrudEditorButtonVisibileFilter $crudEditorButtonVisibileFilter The ChildCrudEditorButtonVisibileFilter object to remove.
     * @return $this|ChildCrudEditorButton The current object (for fluent API support)
     */
    public function removeCrudEditorButtonVisibileFilter(ChildCrudEditorButtonVisibileFilter $crudEditorButtonVisibileFilter)
    {
        if ($this->getCrudEditorButtonVisibileFilters()->contains($crudEditorButtonVisibileFilter)) {
            $pos = $this->collCrudEditorButtonVisibileFilters->search($crudEditorButtonVisibileFilter);
            $this->collCrudEditorButtonVisibileFilters->remove($pos);
            if (null === $this->crudEditorButtonVisibileFiltersScheduledForDeletion) {
                $this->crudEditorButtonVisibileFiltersScheduledForDeletion = clone $this->collCrudEditorButtonVisibileFilters;
                $this->crudEditorButtonVisibileFiltersScheduledForDeletion->clear();
            }
            $this->crudEditorButtonVisibileFiltersScheduledForDeletion[]= $crudEditorButtonVisibileFilter;
            $crudEditorButtonVisibileFilter->setCrudEditorButton(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CrudEditorButton is new, it will return
     * an empty collection; or if this CrudEditorButton has previously
     * been saved, it will retrieve related CrudEditorButtonVisibileFilters from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CrudEditorButton.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCrudEditorButtonVisibileFilter[] List of ChildCrudEditorButtonVisibileFilter objects
     */
    public function getCrudEditorButtonVisibileFiltersJoinFilterOperator(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCrudEditorButtonVisibileFilterQuery::create(null, $criteria);
        $query->joinWith('FilterOperator', $joinBehavior);

        return $this->getCrudEditorButtonVisibileFilters($query, $con);
    }

    /**
     * Clears out the collCrudEditorButtonEvents collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCrudEditorButtonEvents()
     */
    public function clearCrudEditorButtonEvents()
    {
        $this->collCrudEditorButtonEvents = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCrudEditorButtonEvents collection loaded partially.
     */
    public function resetPartialCrudEditorButtonEvents($v = true)
    {
        $this->collCrudEditorButtonEventsPartial = $v;
    }

    /**
     * Initializes the collCrudEditorButtonEvents collection.
     *
     * By default this just sets the collCrudEditorButtonEvents collection to an empty array (like clearcollCrudEditorButtonEvents());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCrudEditorButtonEvents($overrideExisting = true)
    {
        if (null !== $this->collCrudEditorButtonEvents && !$overrideExisting) {
            return;
        }

        $collectionClassName = CrudEditorButtonEventTableMap::getTableMap()->getCollectionClassName();

        $this->collCrudEditorButtonEvents = new $collectionClassName;
        $this->collCrudEditorButtonEvents->setModel('\Model\Setting\CrudManager\CrudEditorButtonEvent');
    }

    /**
     * Gets an array of ChildCrudEditorButtonEvent objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCrudEditorButton is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCrudEditorButtonEvent[] List of ChildCrudEditorButtonEvent objects
     * @throws PropelException
     */
    public function getCrudEditorButtonEvents(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudEditorButtonEventsPartial && !$this->isNew();
        if (null === $this->collCrudEditorButtonEvents || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCrudEditorButtonEvents) {
                    $this->initCrudEditorButtonEvents();
                } else {
                    $collectionClassName = CrudEditorButtonEventTableMap::getTableMap()->getCollectionClassName();

                    $collCrudEditorButtonEvents = new $collectionClassName;
                    $collCrudEditorButtonEvents->setModel('\Model\Setting\CrudManager\CrudEditorButtonEvent');

                    return $collCrudEditorButtonEvents;
                }
            } else {
                $collCrudEditorButtonEvents = ChildCrudEditorButtonEventQuery::create(null, $criteria)
                    ->filterByCrudEditorButton($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCrudEditorButtonEventsPartial && count($collCrudEditorButtonEvents)) {
                        $this->initCrudEditorButtonEvents(false);

                        foreach ($collCrudEditorButtonEvents as $obj) {
                            if (false == $this->collCrudEditorButtonEvents->contains($obj)) {
                                $this->collCrudEditorButtonEvents->append($obj);
                            }
                        }

                        $this->collCrudEditorButtonEventsPartial = true;
                    }

                    return $collCrudEditorButtonEvents;
                }

                if ($partial && $this->collCrudEditorButtonEvents) {
                    foreach ($this->collCrudEditorButtonEvents as $obj) {
                        if ($obj->isNew()) {
                            $collCrudEditorButtonEvents[] = $obj;
                        }
                    }
                }

                $this->collCrudEditorButtonEvents = $collCrudEditorButtonEvents;
                $this->collCrudEditorButtonEventsPartial = false;
            }
        }

        return $this->collCrudEditorButtonEvents;
    }

    /**
     * Sets a collection of ChildCrudEditorButtonEvent objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $crudEditorButtonEvents A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCrudEditorButton The current object (for fluent API support)
     */
    public function setCrudEditorButtonEvents(Collection $crudEditorButtonEvents, ConnectionInterface $con = null)
    {
        /** @var ChildCrudEditorButtonEvent[] $crudEditorButtonEventsToDelete */
        $crudEditorButtonEventsToDelete = $this->getCrudEditorButtonEvents(new Criteria(), $con)->diff($crudEditorButtonEvents);


        $this->crudEditorButtonEventsScheduledForDeletion = $crudEditorButtonEventsToDelete;

        foreach ($crudEditorButtonEventsToDelete as $crudEditorButtonEventRemoved) {
            $crudEditorButtonEventRemoved->setCrudEditorButton(null);
        }

        $this->collCrudEditorButtonEvents = null;
        foreach ($crudEditorButtonEvents as $crudEditorButtonEvent) {
            $this->addCrudEditorButtonEvent($crudEditorButtonEvent);
        }

        $this->collCrudEditorButtonEvents = $crudEditorButtonEvents;
        $this->collCrudEditorButtonEventsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CrudEditorButtonEvent objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CrudEditorButtonEvent objects.
     * @throws PropelException
     */
    public function countCrudEditorButtonEvents(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudEditorButtonEventsPartial && !$this->isNew();
        if (null === $this->collCrudEditorButtonEvents || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCrudEditorButtonEvents) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCrudEditorButtonEvents());
            }

            $query = ChildCrudEditorButtonEventQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCrudEditorButton($this)
                ->count($con);
        }

        return count($this->collCrudEditorButtonEvents);
    }

    /**
     * Method called to associate a ChildCrudEditorButtonEvent object to this object
     * through the ChildCrudEditorButtonEvent foreign key attribute.
     *
     * @param  ChildCrudEditorButtonEvent $l ChildCrudEditorButtonEvent
     * @return $this|\Model\Setting\CrudManager\CrudEditorButton The current object (for fluent API support)
     */
    public function addCrudEditorButtonEvent(ChildCrudEditorButtonEvent $l)
    {
        if ($this->collCrudEditorButtonEvents === null) {
            $this->initCrudEditorButtonEvents();
            $this->collCrudEditorButtonEventsPartial = true;
        }

        if (!$this->collCrudEditorButtonEvents->contains($l)) {
            $this->doAddCrudEditorButtonEvent($l);

            if ($this->crudEditorButtonEventsScheduledForDeletion and $this->crudEditorButtonEventsScheduledForDeletion->contains($l)) {
                $this->crudEditorButtonEventsScheduledForDeletion->remove($this->crudEditorButtonEventsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCrudEditorButtonEvent $crudEditorButtonEvent The ChildCrudEditorButtonEvent object to add.
     */
    protected function doAddCrudEditorButtonEvent(ChildCrudEditorButtonEvent $crudEditorButtonEvent)
    {
        $this->collCrudEditorButtonEvents[]= $crudEditorButtonEvent;
        $crudEditorButtonEvent->setCrudEditorButton($this);
    }

    /**
     * @param  ChildCrudEditorButtonEvent $crudEditorButtonEvent The ChildCrudEditorButtonEvent object to remove.
     * @return $this|ChildCrudEditorButton The current object (for fluent API support)
     */
    public function removeCrudEditorButtonEvent(ChildCrudEditorButtonEvent $crudEditorButtonEvent)
    {
        if ($this->getCrudEditorButtonEvents()->contains($crudEditorButtonEvent)) {
            $pos = $this->collCrudEditorButtonEvents->search($crudEditorButtonEvent);
            $this->collCrudEditorButtonEvents->remove($pos);
            if (null === $this->crudEditorButtonEventsScheduledForDeletion) {
                $this->crudEditorButtonEventsScheduledForDeletion = clone $this->collCrudEditorButtonEvents;
                $this->crudEditorButtonEventsScheduledForDeletion->clear();
            }
            $this->crudEditorButtonEventsScheduledForDeletion[]= $crudEditorButtonEvent;
            $crudEditorButtonEvent->setCrudEditorButton(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCrudEditor) {
            $this->aCrudEditor->removeCrudEditorButton($this);
        }
        $this->id = null;
        $this->crud_editor_id = null;
        $this->title = null;
        $this->icon_before_click = null;
        $this->icon_after_click = null;
        $this->color_before_click = null;
        $this->color_after_click = null;
        $this->is_reusable = null;
        $this->sorting = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCrudEditorButtonVisibileFilters) {
                foreach ($this->collCrudEditorButtonVisibileFilters as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCrudEditorButtonEvents) {
                foreach ($this->collCrudEditorButtonEvents as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCrudEditorButtonVisibileFilters = null;
        $this->collCrudEditorButtonEvents = null;
        $this->aCrudEditor = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CrudEditorButtonTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildCrudEditorButton The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[CrudEditorButtonTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
