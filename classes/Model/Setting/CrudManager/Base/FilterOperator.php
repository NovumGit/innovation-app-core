<?php

namespace Model\Setting\CrudManager\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Marketing\MailingListsCriteria;
use Model\Marketing\MailingListsCriteriaQuery;
use Model\Marketing\Base\MailingListsCriteria as BaseMailingListsCriteria;
use Model\Marketing\Map\MailingListsCriteriaTableMap;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilter as ChildCrudEditorButtonVisibileFilter;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery as ChildCrudEditorButtonVisibileFilterQuery;
use Model\Setting\CrudManager\CrudViewHiddenFilter as ChildCrudViewHiddenFilter;
use Model\Setting\CrudManager\CrudViewHiddenFilterQuery as ChildCrudViewHiddenFilterQuery;
use Model\Setting\CrudManager\CrudViewVisibleFilter as ChildCrudViewVisibleFilter;
use Model\Setting\CrudManager\CrudViewVisibleFilterQuery as ChildCrudViewVisibleFilterQuery;
use Model\Setting\CrudManager\FilterOperator as ChildFilterOperator;
use Model\Setting\CrudManager\FilterOperatorDatatype as ChildFilterOperatorDatatype;
use Model\Setting\CrudManager\FilterOperatorDatatypeQuery as ChildFilterOperatorDatatypeQuery;
use Model\Setting\CrudManager\FilterOperatorQuery as ChildFilterOperatorQuery;
use Model\Setting\CrudManager\Map\CrudEditorButtonVisibileFilterTableMap;
use Model\Setting\CrudManager\Map\CrudViewHiddenFilterTableMap;
use Model\Setting\CrudManager\Map\CrudViewVisibleFilterTableMap;
use Model\Setting\CrudManager\Map\FilterOperatorDatatypeTableMap;
use Model\Setting\CrudManager\Map\FilterOperatorTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'filter_operator' table.
 *
 *
 *
 * @package    propel.generator.Model.Setting.CrudManager.Base
 */
abstract class FilterOperator implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Setting\\CrudManager\\Map\\FilterOperatorTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the name field.
     *
     * @var        string
     */
    protected $name;

    /**
     * The value for the description field.
     *
     * @var        string
     */
    protected $description;

    /**
     * The value for the description_sentence field.
     *
     * @var        string|null
     */
    protected $description_sentence;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        ObjectCollection|MailingListsCriteria[] Collection to store aggregation of MailingListsCriteria objects.
     */
    protected $collMailingListsCriterias;
    protected $collMailingListsCriteriasPartial;

    /**
     * @var        ObjectCollection|ChildCrudEditorButtonVisibileFilter[] Collection to store aggregation of ChildCrudEditorButtonVisibileFilter objects.
     */
    protected $collCrudEditorButtonVisibileFilters;
    protected $collCrudEditorButtonVisibileFiltersPartial;

    /**
     * @var        ObjectCollection|ChildFilterOperatorDatatype[] Collection to store aggregation of ChildFilterOperatorDatatype objects.
     */
    protected $collFilterOperatorDatatypes;
    protected $collFilterOperatorDatatypesPartial;

    /**
     * @var        ObjectCollection|ChildCrudViewVisibleFilter[] Collection to store aggregation of ChildCrudViewVisibleFilter objects.
     */
    protected $collCrudViewVisibleFilters;
    protected $collCrudViewVisibleFiltersPartial;

    /**
     * @var        ObjectCollection|ChildCrudViewHiddenFilter[] Collection to store aggregation of ChildCrudViewHiddenFilter objects.
     */
    protected $collCrudViewHiddenFilters;
    protected $collCrudViewHiddenFiltersPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|MailingListsCriteria[]
     */
    protected $mailingListsCriteriasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCrudEditorButtonVisibileFilter[]
     */
    protected $crudEditorButtonVisibileFiltersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFilterOperatorDatatype[]
     */
    protected $filterOperatorDatatypesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCrudViewVisibleFilter[]
     */
    protected $crudViewVisibleFiltersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCrudViewHiddenFilter[]
     */
    protected $crudViewHiddenFiltersScheduledForDeletion = null;

    /**
     * Initializes internal state of Model\Setting\CrudManager\Base\FilterOperator object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>FilterOperator</code> instance.  If
     * <code>obj</code> is an instance of <code>FilterOperator</code>, delegates to
     * <code>equals(FilterOperator)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [description_sentence] column value.
     *
     * @return string|null
     */
    public function getDescriptionSentence()
    {
        return $this->description_sentence;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Setting\CrudManager\FilterOperator The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[FilterOperatorTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [name] column.
     *
     * @param string $v New value
     * @return $this|\Model\Setting\CrudManager\FilterOperator The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[FilterOperatorTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [description] column.
     *
     * @param string $v New value
     * @return $this|\Model\Setting\CrudManager\FilterOperator The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[FilterOperatorTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [description_sentence] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\CrudManager\FilterOperator The current object (for fluent API support)
     */
    public function setDescriptionSentence($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description_sentence !== $v) {
            $this->description_sentence = $v;
            $this->modifiedColumns[FilterOperatorTableMap::COL_DESCRIPTION_SENTENCE] = true;
        }

        return $this;
    } // setDescriptionSentence()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Setting\CrudManager\FilterOperator The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FilterOperatorTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Setting\CrudManager\FilterOperator The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[FilterOperatorTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : FilterOperatorTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : FilterOperatorTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : FilterOperatorTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : FilterOperatorTableMap::translateFieldName('DescriptionSentence', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description_sentence = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : FilterOperatorTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : FilterOperatorTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 6; // 6 = FilterOperatorTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Setting\\CrudManager\\FilterOperator'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FilterOperatorTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildFilterOperatorQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collMailingListsCriterias = null;

            $this->collCrudEditorButtonVisibileFilters = null;

            $this->collFilterOperatorDatatypes = null;

            $this->collCrudViewVisibleFilters = null;

            $this->collCrudViewHiddenFilters = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see FilterOperator::setDeleted()
     * @see FilterOperator::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FilterOperatorTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildFilterOperatorQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(FilterOperatorTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(FilterOperatorTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(FilterOperatorTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(FilterOperatorTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                FilterOperatorTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->mailingListsCriteriasScheduledForDeletion !== null) {
                if (!$this->mailingListsCriteriasScheduledForDeletion->isEmpty()) {
                    \Model\Marketing\MailingListsCriteriaQuery::create()
                        ->filterByPrimaryKeys($this->mailingListsCriteriasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->mailingListsCriteriasScheduledForDeletion = null;
                }
            }

            if ($this->collMailingListsCriterias !== null) {
                foreach ($this->collMailingListsCriterias as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->crudEditorButtonVisibileFiltersScheduledForDeletion !== null) {
                if (!$this->crudEditorButtonVisibileFiltersScheduledForDeletion->isEmpty()) {
                    \Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery::create()
                        ->filterByPrimaryKeys($this->crudEditorButtonVisibileFiltersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->crudEditorButtonVisibileFiltersScheduledForDeletion = null;
                }
            }

            if ($this->collCrudEditorButtonVisibileFilters !== null) {
                foreach ($this->collCrudEditorButtonVisibileFilters as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->filterOperatorDatatypesScheduledForDeletion !== null) {
                if (!$this->filterOperatorDatatypesScheduledForDeletion->isEmpty()) {
                    \Model\Setting\CrudManager\FilterOperatorDatatypeQuery::create()
                        ->filterByPrimaryKeys($this->filterOperatorDatatypesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->filterOperatorDatatypesScheduledForDeletion = null;
                }
            }

            if ($this->collFilterOperatorDatatypes !== null) {
                foreach ($this->collFilterOperatorDatatypes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->crudViewVisibleFiltersScheduledForDeletion !== null) {
                if (!$this->crudViewVisibleFiltersScheduledForDeletion->isEmpty()) {
                    \Model\Setting\CrudManager\CrudViewVisibleFilterQuery::create()
                        ->filterByPrimaryKeys($this->crudViewVisibleFiltersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->crudViewVisibleFiltersScheduledForDeletion = null;
                }
            }

            if ($this->collCrudViewVisibleFilters !== null) {
                foreach ($this->collCrudViewVisibleFilters as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->crudViewHiddenFiltersScheduledForDeletion !== null) {
                if (!$this->crudViewHiddenFiltersScheduledForDeletion->isEmpty()) {
                    \Model\Setting\CrudManager\CrudViewHiddenFilterQuery::create()
                        ->filterByPrimaryKeys($this->crudViewHiddenFiltersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->crudViewHiddenFiltersScheduledForDeletion = null;
                }
            }

            if ($this->collCrudViewHiddenFilters !== null) {
                foreach ($this->collCrudViewHiddenFilters as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[FilterOperatorTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . FilterOperatorTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(FilterOperatorTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(FilterOperatorTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(FilterOperatorTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }
        if ($this->isColumnModified(FilterOperatorTableMap::COL_DESCRIPTION_SENTENCE)) {
            $modifiedColumns[':p' . $index++]  = 'description_sentence';
        }
        if ($this->isColumnModified(FilterOperatorTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(FilterOperatorTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO filter_operator (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'description_sentence':
                        $stmt->bindValue($identifier, $this->description_sentence, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FilterOperatorTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getDescription();
                break;
            case 3:
                return $this->getDescriptionSentence();
                break;
            case 4:
                return $this->getCreatedAt();
                break;
            case 5:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['FilterOperator'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['FilterOperator'][$this->hashCode()] = true;
        $keys = FilterOperatorTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getDescription(),
            $keys[3] => $this->getDescriptionSentence(),
            $keys[4] => $this->getCreatedAt(),
            $keys[5] => $this->getUpdatedAt(),
        );
        if ($result[$keys[4]] instanceof \DateTimeInterface) {
            $result[$keys[4]] = $result[$keys[4]]->format('c');
        }

        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collMailingListsCriterias) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'mailingListsCriterias';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mailing_list_criterias';
                        break;
                    default:
                        $key = 'MailingListsCriterias';
                }

                $result[$key] = $this->collMailingListsCriterias->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCrudEditorButtonVisibileFilters) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'crudEditorButtonVisibileFilters';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'crud_editor_button_visibile_filters';
                        break;
                    default:
                        $key = 'CrudEditorButtonVisibileFilters';
                }

                $result[$key] = $this->collCrudEditorButtonVisibileFilters->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFilterOperatorDatatypes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'filterOperatorDatatypes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'filter_operator_datatypes';
                        break;
                    default:
                        $key = 'FilterOperatorDatatypes';
                }

                $result[$key] = $this->collFilterOperatorDatatypes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCrudViewVisibleFilters) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'crudViewVisibleFilters';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'crud_view_visible_filters';
                        break;
                    default:
                        $key = 'CrudViewVisibleFilters';
                }

                $result[$key] = $this->collCrudViewVisibleFilters->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCrudViewHiddenFilters) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'crudViewHiddenFilters';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'crud_view_hidden_filters';
                        break;
                    default:
                        $key = 'CrudViewHiddenFilters';
                }

                $result[$key] = $this->collCrudViewHiddenFilters->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Setting\CrudManager\FilterOperator
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = FilterOperatorTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Setting\CrudManager\FilterOperator
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setDescription($value);
                break;
            case 3:
                $this->setDescriptionSentence($value);
                break;
            case 4:
                $this->setCreatedAt($value);
                break;
            case 5:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = FilterOperatorTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setDescription($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDescriptionSentence($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setCreatedAt($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setUpdatedAt($arr[$keys[5]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Setting\CrudManager\FilterOperator The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(FilterOperatorTableMap::DATABASE_NAME);

        if ($this->isColumnModified(FilterOperatorTableMap::COL_ID)) {
            $criteria->add(FilterOperatorTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(FilterOperatorTableMap::COL_NAME)) {
            $criteria->add(FilterOperatorTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(FilterOperatorTableMap::COL_DESCRIPTION)) {
            $criteria->add(FilterOperatorTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(FilterOperatorTableMap::COL_DESCRIPTION_SENTENCE)) {
            $criteria->add(FilterOperatorTableMap::COL_DESCRIPTION_SENTENCE, $this->description_sentence);
        }
        if ($this->isColumnModified(FilterOperatorTableMap::COL_CREATED_AT)) {
            $criteria->add(FilterOperatorTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(FilterOperatorTableMap::COL_UPDATED_AT)) {
            $criteria->add(FilterOperatorTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildFilterOperatorQuery::create();
        $criteria->add(FilterOperatorTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Setting\CrudManager\FilterOperator (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setDescriptionSentence($this->getDescriptionSentence());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getMailingListsCriterias() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMailingListsCriteria($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCrudEditorButtonVisibileFilters() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCrudEditorButtonVisibileFilter($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFilterOperatorDatatypes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFilterOperatorDatatype($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCrudViewVisibleFilters() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCrudViewVisibleFilter($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCrudViewHiddenFilters() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCrudViewHiddenFilter($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Setting\CrudManager\FilterOperator Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('MailingListsCriteria' === $relationName) {
            $this->initMailingListsCriterias();
            return;
        }
        if ('CrudEditorButtonVisibileFilter' === $relationName) {
            $this->initCrudEditorButtonVisibileFilters();
            return;
        }
        if ('FilterOperatorDatatype' === $relationName) {
            $this->initFilterOperatorDatatypes();
            return;
        }
        if ('CrudViewVisibleFilter' === $relationName) {
            $this->initCrudViewVisibleFilters();
            return;
        }
        if ('CrudViewHiddenFilter' === $relationName) {
            $this->initCrudViewHiddenFilters();
            return;
        }
    }

    /**
     * Clears out the collMailingListsCriterias collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMailingListsCriterias()
     */
    public function clearMailingListsCriterias()
    {
        $this->collMailingListsCriterias = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMailingListsCriterias collection loaded partially.
     */
    public function resetPartialMailingListsCriterias($v = true)
    {
        $this->collMailingListsCriteriasPartial = $v;
    }

    /**
     * Initializes the collMailingListsCriterias collection.
     *
     * By default this just sets the collMailingListsCriterias collection to an empty array (like clearcollMailingListsCriterias());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMailingListsCriterias($overrideExisting = true)
    {
        if (null !== $this->collMailingListsCriterias && !$overrideExisting) {
            return;
        }

        $collectionClassName = MailingListsCriteriaTableMap::getTableMap()->getCollectionClassName();

        $this->collMailingListsCriterias = new $collectionClassName;
        $this->collMailingListsCriterias->setModel('\Model\Marketing\MailingListsCriteria');
    }

    /**
     * Gets an array of MailingListsCriteria objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildFilterOperator is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|MailingListsCriteria[] List of MailingListsCriteria objects
     * @throws PropelException
     */
    public function getMailingListsCriterias(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMailingListsCriteriasPartial && !$this->isNew();
        if (null === $this->collMailingListsCriterias || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collMailingListsCriterias) {
                    $this->initMailingListsCriterias();
                } else {
                    $collectionClassName = MailingListsCriteriaTableMap::getTableMap()->getCollectionClassName();

                    $collMailingListsCriterias = new $collectionClassName;
                    $collMailingListsCriterias->setModel('\Model\Marketing\MailingListsCriteria');

                    return $collMailingListsCriterias;
                }
            } else {
                $collMailingListsCriterias = MailingListsCriteriaQuery::create(null, $criteria)
                    ->filterByFilterOperator($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMailingListsCriteriasPartial && count($collMailingListsCriterias)) {
                        $this->initMailingListsCriterias(false);

                        foreach ($collMailingListsCriterias as $obj) {
                            if (false == $this->collMailingListsCriterias->contains($obj)) {
                                $this->collMailingListsCriterias->append($obj);
                            }
                        }

                        $this->collMailingListsCriteriasPartial = true;
                    }

                    return $collMailingListsCriterias;
                }

                if ($partial && $this->collMailingListsCriterias) {
                    foreach ($this->collMailingListsCriterias as $obj) {
                        if ($obj->isNew()) {
                            $collMailingListsCriterias[] = $obj;
                        }
                    }
                }

                $this->collMailingListsCriterias = $collMailingListsCriterias;
                $this->collMailingListsCriteriasPartial = false;
            }
        }

        return $this->collMailingListsCriterias;
    }

    /**
     * Sets a collection of MailingListsCriteria objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $mailingListsCriterias A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildFilterOperator The current object (for fluent API support)
     */
    public function setMailingListsCriterias(Collection $mailingListsCriterias, ConnectionInterface $con = null)
    {
        /** @var MailingListsCriteria[] $mailingListsCriteriasToDelete */
        $mailingListsCriteriasToDelete = $this->getMailingListsCriterias(new Criteria(), $con)->diff($mailingListsCriterias);


        $this->mailingListsCriteriasScheduledForDeletion = $mailingListsCriteriasToDelete;

        foreach ($mailingListsCriteriasToDelete as $mailingListsCriteriaRemoved) {
            $mailingListsCriteriaRemoved->setFilterOperator(null);
        }

        $this->collMailingListsCriterias = null;
        foreach ($mailingListsCriterias as $mailingListsCriteria) {
            $this->addMailingListsCriteria($mailingListsCriteria);
        }

        $this->collMailingListsCriterias = $mailingListsCriterias;
        $this->collMailingListsCriteriasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseMailingListsCriteria objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseMailingListsCriteria objects.
     * @throws PropelException
     */
    public function countMailingListsCriterias(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMailingListsCriteriasPartial && !$this->isNew();
        if (null === $this->collMailingListsCriterias || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMailingListsCriterias) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMailingListsCriterias());
            }

            $query = MailingListsCriteriaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFilterOperator($this)
                ->count($con);
        }

        return count($this->collMailingListsCriterias);
    }

    /**
     * Method called to associate a MailingListsCriteria object to this object
     * through the MailingListsCriteria foreign key attribute.
     *
     * @param  MailingListsCriteria $l MailingListsCriteria
     * @return $this|\Model\Setting\CrudManager\FilterOperator The current object (for fluent API support)
     */
    public function addMailingListsCriteria(MailingListsCriteria $l)
    {
        if ($this->collMailingListsCriterias === null) {
            $this->initMailingListsCriterias();
            $this->collMailingListsCriteriasPartial = true;
        }

        if (!$this->collMailingListsCriterias->contains($l)) {
            $this->doAddMailingListsCriteria($l);

            if ($this->mailingListsCriteriasScheduledForDeletion and $this->mailingListsCriteriasScheduledForDeletion->contains($l)) {
                $this->mailingListsCriteriasScheduledForDeletion->remove($this->mailingListsCriteriasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param MailingListsCriteria $mailingListsCriteria The MailingListsCriteria object to add.
     */
    protected function doAddMailingListsCriteria(MailingListsCriteria $mailingListsCriteria)
    {
        $this->collMailingListsCriterias[]= $mailingListsCriteria;
        $mailingListsCriteria->setFilterOperator($this);
    }

    /**
     * @param  MailingListsCriteria $mailingListsCriteria The MailingListsCriteria object to remove.
     * @return $this|ChildFilterOperator The current object (for fluent API support)
     */
    public function removeMailingListsCriteria(MailingListsCriteria $mailingListsCriteria)
    {
        if ($this->getMailingListsCriterias()->contains($mailingListsCriteria)) {
            $pos = $this->collMailingListsCriterias->search($mailingListsCriteria);
            $this->collMailingListsCriterias->remove($pos);
            if (null === $this->mailingListsCriteriasScheduledForDeletion) {
                $this->mailingListsCriteriasScheduledForDeletion = clone $this->collMailingListsCriterias;
                $this->mailingListsCriteriasScheduledForDeletion->clear();
            }
            $this->mailingListsCriteriasScheduledForDeletion[]= clone $mailingListsCriteria;
            $mailingListsCriteria->setFilterOperator(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this FilterOperator is new, it will return
     * an empty collection; or if this FilterOperator has previously
     * been saved, it will retrieve related MailingListsCriterias from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in FilterOperator.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|MailingListsCriteria[] List of MailingListsCriteria objects
     */
    public function getMailingListsCriteriasJoinMailingList(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = MailingListsCriteriaQuery::create(null, $criteria);
        $query->joinWith('MailingList', $joinBehavior);

        return $this->getMailingListsCriterias($query, $con);
    }

    /**
     * Clears out the collCrudEditorButtonVisibileFilters collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCrudEditorButtonVisibileFilters()
     */
    public function clearCrudEditorButtonVisibileFilters()
    {
        $this->collCrudEditorButtonVisibileFilters = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCrudEditorButtonVisibileFilters collection loaded partially.
     */
    public function resetPartialCrudEditorButtonVisibileFilters($v = true)
    {
        $this->collCrudEditorButtonVisibileFiltersPartial = $v;
    }

    /**
     * Initializes the collCrudEditorButtonVisibileFilters collection.
     *
     * By default this just sets the collCrudEditorButtonVisibileFilters collection to an empty array (like clearcollCrudEditorButtonVisibileFilters());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCrudEditorButtonVisibileFilters($overrideExisting = true)
    {
        if (null !== $this->collCrudEditorButtonVisibileFilters && !$overrideExisting) {
            return;
        }

        $collectionClassName = CrudEditorButtonVisibileFilterTableMap::getTableMap()->getCollectionClassName();

        $this->collCrudEditorButtonVisibileFilters = new $collectionClassName;
        $this->collCrudEditorButtonVisibileFilters->setModel('\Model\Setting\CrudManager\CrudEditorButtonVisibileFilter');
    }

    /**
     * Gets an array of ChildCrudEditorButtonVisibileFilter objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildFilterOperator is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCrudEditorButtonVisibileFilter[] List of ChildCrudEditorButtonVisibileFilter objects
     * @throws PropelException
     */
    public function getCrudEditorButtonVisibileFilters(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudEditorButtonVisibileFiltersPartial && !$this->isNew();
        if (null === $this->collCrudEditorButtonVisibileFilters || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCrudEditorButtonVisibileFilters) {
                    $this->initCrudEditorButtonVisibileFilters();
                } else {
                    $collectionClassName = CrudEditorButtonVisibileFilterTableMap::getTableMap()->getCollectionClassName();

                    $collCrudEditorButtonVisibileFilters = new $collectionClassName;
                    $collCrudEditorButtonVisibileFilters->setModel('\Model\Setting\CrudManager\CrudEditorButtonVisibileFilter');

                    return $collCrudEditorButtonVisibileFilters;
                }
            } else {
                $collCrudEditorButtonVisibileFilters = ChildCrudEditorButtonVisibileFilterQuery::create(null, $criteria)
                    ->filterByFilterOperator($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCrudEditorButtonVisibileFiltersPartial && count($collCrudEditorButtonVisibileFilters)) {
                        $this->initCrudEditorButtonVisibileFilters(false);

                        foreach ($collCrudEditorButtonVisibileFilters as $obj) {
                            if (false == $this->collCrudEditorButtonVisibileFilters->contains($obj)) {
                                $this->collCrudEditorButtonVisibileFilters->append($obj);
                            }
                        }

                        $this->collCrudEditorButtonVisibileFiltersPartial = true;
                    }

                    return $collCrudEditorButtonVisibileFilters;
                }

                if ($partial && $this->collCrudEditorButtonVisibileFilters) {
                    foreach ($this->collCrudEditorButtonVisibileFilters as $obj) {
                        if ($obj->isNew()) {
                            $collCrudEditorButtonVisibileFilters[] = $obj;
                        }
                    }
                }

                $this->collCrudEditorButtonVisibileFilters = $collCrudEditorButtonVisibileFilters;
                $this->collCrudEditorButtonVisibileFiltersPartial = false;
            }
        }

        return $this->collCrudEditorButtonVisibileFilters;
    }

    /**
     * Sets a collection of ChildCrudEditorButtonVisibileFilter objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $crudEditorButtonVisibileFilters A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildFilterOperator The current object (for fluent API support)
     */
    public function setCrudEditorButtonVisibileFilters(Collection $crudEditorButtonVisibileFilters, ConnectionInterface $con = null)
    {
        /** @var ChildCrudEditorButtonVisibileFilter[] $crudEditorButtonVisibileFiltersToDelete */
        $crudEditorButtonVisibileFiltersToDelete = $this->getCrudEditorButtonVisibileFilters(new Criteria(), $con)->diff($crudEditorButtonVisibileFilters);


        $this->crudEditorButtonVisibileFiltersScheduledForDeletion = $crudEditorButtonVisibileFiltersToDelete;

        foreach ($crudEditorButtonVisibileFiltersToDelete as $crudEditorButtonVisibileFilterRemoved) {
            $crudEditorButtonVisibileFilterRemoved->setFilterOperator(null);
        }

        $this->collCrudEditorButtonVisibileFilters = null;
        foreach ($crudEditorButtonVisibileFilters as $crudEditorButtonVisibileFilter) {
            $this->addCrudEditorButtonVisibileFilter($crudEditorButtonVisibileFilter);
        }

        $this->collCrudEditorButtonVisibileFilters = $crudEditorButtonVisibileFilters;
        $this->collCrudEditorButtonVisibileFiltersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CrudEditorButtonVisibileFilter objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CrudEditorButtonVisibileFilter objects.
     * @throws PropelException
     */
    public function countCrudEditorButtonVisibileFilters(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudEditorButtonVisibileFiltersPartial && !$this->isNew();
        if (null === $this->collCrudEditorButtonVisibileFilters || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCrudEditorButtonVisibileFilters) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCrudEditorButtonVisibileFilters());
            }

            $query = ChildCrudEditorButtonVisibileFilterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFilterOperator($this)
                ->count($con);
        }

        return count($this->collCrudEditorButtonVisibileFilters);
    }

    /**
     * Method called to associate a ChildCrudEditorButtonVisibileFilter object to this object
     * through the ChildCrudEditorButtonVisibileFilter foreign key attribute.
     *
     * @param  ChildCrudEditorButtonVisibileFilter $l ChildCrudEditorButtonVisibileFilter
     * @return $this|\Model\Setting\CrudManager\FilterOperator The current object (for fluent API support)
     */
    public function addCrudEditorButtonVisibileFilter(ChildCrudEditorButtonVisibileFilter $l)
    {
        if ($this->collCrudEditorButtonVisibileFilters === null) {
            $this->initCrudEditorButtonVisibileFilters();
            $this->collCrudEditorButtonVisibileFiltersPartial = true;
        }

        if (!$this->collCrudEditorButtonVisibileFilters->contains($l)) {
            $this->doAddCrudEditorButtonVisibileFilter($l);

            if ($this->crudEditorButtonVisibileFiltersScheduledForDeletion and $this->crudEditorButtonVisibileFiltersScheduledForDeletion->contains($l)) {
                $this->crudEditorButtonVisibileFiltersScheduledForDeletion->remove($this->crudEditorButtonVisibileFiltersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCrudEditorButtonVisibileFilter $crudEditorButtonVisibileFilter The ChildCrudEditorButtonVisibileFilter object to add.
     */
    protected function doAddCrudEditorButtonVisibileFilter(ChildCrudEditorButtonVisibileFilter $crudEditorButtonVisibileFilter)
    {
        $this->collCrudEditorButtonVisibileFilters[]= $crudEditorButtonVisibileFilter;
        $crudEditorButtonVisibileFilter->setFilterOperator($this);
    }

    /**
     * @param  ChildCrudEditorButtonVisibileFilter $crudEditorButtonVisibileFilter The ChildCrudEditorButtonVisibileFilter object to remove.
     * @return $this|ChildFilterOperator The current object (for fluent API support)
     */
    public function removeCrudEditorButtonVisibileFilter(ChildCrudEditorButtonVisibileFilter $crudEditorButtonVisibileFilter)
    {
        if ($this->getCrudEditorButtonVisibileFilters()->contains($crudEditorButtonVisibileFilter)) {
            $pos = $this->collCrudEditorButtonVisibileFilters->search($crudEditorButtonVisibileFilter);
            $this->collCrudEditorButtonVisibileFilters->remove($pos);
            if (null === $this->crudEditorButtonVisibileFiltersScheduledForDeletion) {
                $this->crudEditorButtonVisibileFiltersScheduledForDeletion = clone $this->collCrudEditorButtonVisibileFilters;
                $this->crudEditorButtonVisibileFiltersScheduledForDeletion->clear();
            }
            $this->crudEditorButtonVisibileFiltersScheduledForDeletion[]= clone $crudEditorButtonVisibileFilter;
            $crudEditorButtonVisibileFilter->setFilterOperator(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this FilterOperator is new, it will return
     * an empty collection; or if this FilterOperator has previously
     * been saved, it will retrieve related CrudEditorButtonVisibileFilters from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in FilterOperator.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCrudEditorButtonVisibileFilter[] List of ChildCrudEditorButtonVisibileFilter objects
     */
    public function getCrudEditorButtonVisibileFiltersJoinCrudEditorButton(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCrudEditorButtonVisibileFilterQuery::create(null, $criteria);
        $query->joinWith('CrudEditorButton', $joinBehavior);

        return $this->getCrudEditorButtonVisibileFilters($query, $con);
    }

    /**
     * Clears out the collFilterOperatorDatatypes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFilterOperatorDatatypes()
     */
    public function clearFilterOperatorDatatypes()
    {
        $this->collFilterOperatorDatatypes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFilterOperatorDatatypes collection loaded partially.
     */
    public function resetPartialFilterOperatorDatatypes($v = true)
    {
        $this->collFilterOperatorDatatypesPartial = $v;
    }

    /**
     * Initializes the collFilterOperatorDatatypes collection.
     *
     * By default this just sets the collFilterOperatorDatatypes collection to an empty array (like clearcollFilterOperatorDatatypes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFilterOperatorDatatypes($overrideExisting = true)
    {
        if (null !== $this->collFilterOperatorDatatypes && !$overrideExisting) {
            return;
        }

        $collectionClassName = FilterOperatorDatatypeTableMap::getTableMap()->getCollectionClassName();

        $this->collFilterOperatorDatatypes = new $collectionClassName;
        $this->collFilterOperatorDatatypes->setModel('\Model\Setting\CrudManager\FilterOperatorDatatype');
    }

    /**
     * Gets an array of ChildFilterOperatorDatatype objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildFilterOperator is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFilterOperatorDatatype[] List of ChildFilterOperatorDatatype objects
     * @throws PropelException
     */
    public function getFilterOperatorDatatypes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFilterOperatorDatatypesPartial && !$this->isNew();
        if (null === $this->collFilterOperatorDatatypes || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collFilterOperatorDatatypes) {
                    $this->initFilterOperatorDatatypes();
                } else {
                    $collectionClassName = FilterOperatorDatatypeTableMap::getTableMap()->getCollectionClassName();

                    $collFilterOperatorDatatypes = new $collectionClassName;
                    $collFilterOperatorDatatypes->setModel('\Model\Setting\CrudManager\FilterOperatorDatatype');

                    return $collFilterOperatorDatatypes;
                }
            } else {
                $collFilterOperatorDatatypes = ChildFilterOperatorDatatypeQuery::create(null, $criteria)
                    ->filterByFilterOperator($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFilterOperatorDatatypesPartial && count($collFilterOperatorDatatypes)) {
                        $this->initFilterOperatorDatatypes(false);

                        foreach ($collFilterOperatorDatatypes as $obj) {
                            if (false == $this->collFilterOperatorDatatypes->contains($obj)) {
                                $this->collFilterOperatorDatatypes->append($obj);
                            }
                        }

                        $this->collFilterOperatorDatatypesPartial = true;
                    }

                    return $collFilterOperatorDatatypes;
                }

                if ($partial && $this->collFilterOperatorDatatypes) {
                    foreach ($this->collFilterOperatorDatatypes as $obj) {
                        if ($obj->isNew()) {
                            $collFilterOperatorDatatypes[] = $obj;
                        }
                    }
                }

                $this->collFilterOperatorDatatypes = $collFilterOperatorDatatypes;
                $this->collFilterOperatorDatatypesPartial = false;
            }
        }

        return $this->collFilterOperatorDatatypes;
    }

    /**
     * Sets a collection of ChildFilterOperatorDatatype objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $filterOperatorDatatypes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildFilterOperator The current object (for fluent API support)
     */
    public function setFilterOperatorDatatypes(Collection $filterOperatorDatatypes, ConnectionInterface $con = null)
    {
        /** @var ChildFilterOperatorDatatype[] $filterOperatorDatatypesToDelete */
        $filterOperatorDatatypesToDelete = $this->getFilterOperatorDatatypes(new Criteria(), $con)->diff($filterOperatorDatatypes);


        $this->filterOperatorDatatypesScheduledForDeletion = $filterOperatorDatatypesToDelete;

        foreach ($filterOperatorDatatypesToDelete as $filterOperatorDatatypeRemoved) {
            $filterOperatorDatatypeRemoved->setFilterOperator(null);
        }

        $this->collFilterOperatorDatatypes = null;
        foreach ($filterOperatorDatatypes as $filterOperatorDatatype) {
            $this->addFilterOperatorDatatype($filterOperatorDatatype);
        }

        $this->collFilterOperatorDatatypes = $filterOperatorDatatypes;
        $this->collFilterOperatorDatatypesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FilterOperatorDatatype objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related FilterOperatorDatatype objects.
     * @throws PropelException
     */
    public function countFilterOperatorDatatypes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFilterOperatorDatatypesPartial && !$this->isNew();
        if (null === $this->collFilterOperatorDatatypes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFilterOperatorDatatypes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFilterOperatorDatatypes());
            }

            $query = ChildFilterOperatorDatatypeQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFilterOperator($this)
                ->count($con);
        }

        return count($this->collFilterOperatorDatatypes);
    }

    /**
     * Method called to associate a ChildFilterOperatorDatatype object to this object
     * through the ChildFilterOperatorDatatype foreign key attribute.
     *
     * @param  ChildFilterOperatorDatatype $l ChildFilterOperatorDatatype
     * @return $this|\Model\Setting\CrudManager\FilterOperator The current object (for fluent API support)
     */
    public function addFilterOperatorDatatype(ChildFilterOperatorDatatype $l)
    {
        if ($this->collFilterOperatorDatatypes === null) {
            $this->initFilterOperatorDatatypes();
            $this->collFilterOperatorDatatypesPartial = true;
        }

        if (!$this->collFilterOperatorDatatypes->contains($l)) {
            $this->doAddFilterOperatorDatatype($l);

            if ($this->filterOperatorDatatypesScheduledForDeletion and $this->filterOperatorDatatypesScheduledForDeletion->contains($l)) {
                $this->filterOperatorDatatypesScheduledForDeletion->remove($this->filterOperatorDatatypesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFilterOperatorDatatype $filterOperatorDatatype The ChildFilterOperatorDatatype object to add.
     */
    protected function doAddFilterOperatorDatatype(ChildFilterOperatorDatatype $filterOperatorDatatype)
    {
        $this->collFilterOperatorDatatypes[]= $filterOperatorDatatype;
        $filterOperatorDatatype->setFilterOperator($this);
    }

    /**
     * @param  ChildFilterOperatorDatatype $filterOperatorDatatype The ChildFilterOperatorDatatype object to remove.
     * @return $this|ChildFilterOperator The current object (for fluent API support)
     */
    public function removeFilterOperatorDatatype(ChildFilterOperatorDatatype $filterOperatorDatatype)
    {
        if ($this->getFilterOperatorDatatypes()->contains($filterOperatorDatatype)) {
            $pos = $this->collFilterOperatorDatatypes->search($filterOperatorDatatype);
            $this->collFilterOperatorDatatypes->remove($pos);
            if (null === $this->filterOperatorDatatypesScheduledForDeletion) {
                $this->filterOperatorDatatypesScheduledForDeletion = clone $this->collFilterOperatorDatatypes;
                $this->filterOperatorDatatypesScheduledForDeletion->clear();
            }
            $this->filterOperatorDatatypesScheduledForDeletion[]= clone $filterOperatorDatatype;
            $filterOperatorDatatype->setFilterOperator(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this FilterOperator is new, it will return
     * an empty collection; or if this FilterOperator has previously
     * been saved, it will retrieve related FilterOperatorDatatypes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in FilterOperator.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildFilterOperatorDatatype[] List of ChildFilterOperatorDatatype objects
     */
    public function getFilterOperatorDatatypesJoinFilterDatatype(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildFilterOperatorDatatypeQuery::create(null, $criteria);
        $query->joinWith('FilterDatatype', $joinBehavior);

        return $this->getFilterOperatorDatatypes($query, $con);
    }

    /**
     * Clears out the collCrudViewVisibleFilters collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCrudViewVisibleFilters()
     */
    public function clearCrudViewVisibleFilters()
    {
        $this->collCrudViewVisibleFilters = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCrudViewVisibleFilters collection loaded partially.
     */
    public function resetPartialCrudViewVisibleFilters($v = true)
    {
        $this->collCrudViewVisibleFiltersPartial = $v;
    }

    /**
     * Initializes the collCrudViewVisibleFilters collection.
     *
     * By default this just sets the collCrudViewVisibleFilters collection to an empty array (like clearcollCrudViewVisibleFilters());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCrudViewVisibleFilters($overrideExisting = true)
    {
        if (null !== $this->collCrudViewVisibleFilters && !$overrideExisting) {
            return;
        }

        $collectionClassName = CrudViewVisibleFilterTableMap::getTableMap()->getCollectionClassName();

        $this->collCrudViewVisibleFilters = new $collectionClassName;
        $this->collCrudViewVisibleFilters->setModel('\Model\Setting\CrudManager\CrudViewVisibleFilter');
    }

    /**
     * Gets an array of ChildCrudViewVisibleFilter objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildFilterOperator is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCrudViewVisibleFilter[] List of ChildCrudViewVisibleFilter objects
     * @throws PropelException
     */
    public function getCrudViewVisibleFilters(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudViewVisibleFiltersPartial && !$this->isNew();
        if (null === $this->collCrudViewVisibleFilters || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCrudViewVisibleFilters) {
                    $this->initCrudViewVisibleFilters();
                } else {
                    $collectionClassName = CrudViewVisibleFilterTableMap::getTableMap()->getCollectionClassName();

                    $collCrudViewVisibleFilters = new $collectionClassName;
                    $collCrudViewVisibleFilters->setModel('\Model\Setting\CrudManager\CrudViewVisibleFilter');

                    return $collCrudViewVisibleFilters;
                }
            } else {
                $collCrudViewVisibleFilters = ChildCrudViewVisibleFilterQuery::create(null, $criteria)
                    ->filterByFilterOperator($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCrudViewVisibleFiltersPartial && count($collCrudViewVisibleFilters)) {
                        $this->initCrudViewVisibleFilters(false);

                        foreach ($collCrudViewVisibleFilters as $obj) {
                            if (false == $this->collCrudViewVisibleFilters->contains($obj)) {
                                $this->collCrudViewVisibleFilters->append($obj);
                            }
                        }

                        $this->collCrudViewVisibleFiltersPartial = true;
                    }

                    return $collCrudViewVisibleFilters;
                }

                if ($partial && $this->collCrudViewVisibleFilters) {
                    foreach ($this->collCrudViewVisibleFilters as $obj) {
                        if ($obj->isNew()) {
                            $collCrudViewVisibleFilters[] = $obj;
                        }
                    }
                }

                $this->collCrudViewVisibleFilters = $collCrudViewVisibleFilters;
                $this->collCrudViewVisibleFiltersPartial = false;
            }
        }

        return $this->collCrudViewVisibleFilters;
    }

    /**
     * Sets a collection of ChildCrudViewVisibleFilter objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $crudViewVisibleFilters A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildFilterOperator The current object (for fluent API support)
     */
    public function setCrudViewVisibleFilters(Collection $crudViewVisibleFilters, ConnectionInterface $con = null)
    {
        /** @var ChildCrudViewVisibleFilter[] $crudViewVisibleFiltersToDelete */
        $crudViewVisibleFiltersToDelete = $this->getCrudViewVisibleFilters(new Criteria(), $con)->diff($crudViewVisibleFilters);


        $this->crudViewVisibleFiltersScheduledForDeletion = $crudViewVisibleFiltersToDelete;

        foreach ($crudViewVisibleFiltersToDelete as $crudViewVisibleFilterRemoved) {
            $crudViewVisibleFilterRemoved->setFilterOperator(null);
        }

        $this->collCrudViewVisibleFilters = null;
        foreach ($crudViewVisibleFilters as $crudViewVisibleFilter) {
            $this->addCrudViewVisibleFilter($crudViewVisibleFilter);
        }

        $this->collCrudViewVisibleFilters = $crudViewVisibleFilters;
        $this->collCrudViewVisibleFiltersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CrudViewVisibleFilter objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CrudViewVisibleFilter objects.
     * @throws PropelException
     */
    public function countCrudViewVisibleFilters(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudViewVisibleFiltersPartial && !$this->isNew();
        if (null === $this->collCrudViewVisibleFilters || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCrudViewVisibleFilters) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCrudViewVisibleFilters());
            }

            $query = ChildCrudViewVisibleFilterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFilterOperator($this)
                ->count($con);
        }

        return count($this->collCrudViewVisibleFilters);
    }

    /**
     * Method called to associate a ChildCrudViewVisibleFilter object to this object
     * through the ChildCrudViewVisibleFilter foreign key attribute.
     *
     * @param  ChildCrudViewVisibleFilter $l ChildCrudViewVisibleFilter
     * @return $this|\Model\Setting\CrudManager\FilterOperator The current object (for fluent API support)
     */
    public function addCrudViewVisibleFilter(ChildCrudViewVisibleFilter $l)
    {
        if ($this->collCrudViewVisibleFilters === null) {
            $this->initCrudViewVisibleFilters();
            $this->collCrudViewVisibleFiltersPartial = true;
        }

        if (!$this->collCrudViewVisibleFilters->contains($l)) {
            $this->doAddCrudViewVisibleFilter($l);

            if ($this->crudViewVisibleFiltersScheduledForDeletion and $this->crudViewVisibleFiltersScheduledForDeletion->contains($l)) {
                $this->crudViewVisibleFiltersScheduledForDeletion->remove($this->crudViewVisibleFiltersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCrudViewVisibleFilter $crudViewVisibleFilter The ChildCrudViewVisibleFilter object to add.
     */
    protected function doAddCrudViewVisibleFilter(ChildCrudViewVisibleFilter $crudViewVisibleFilter)
    {
        $this->collCrudViewVisibleFilters[]= $crudViewVisibleFilter;
        $crudViewVisibleFilter->setFilterOperator($this);
    }

    /**
     * @param  ChildCrudViewVisibleFilter $crudViewVisibleFilter The ChildCrudViewVisibleFilter object to remove.
     * @return $this|ChildFilterOperator The current object (for fluent API support)
     */
    public function removeCrudViewVisibleFilter(ChildCrudViewVisibleFilter $crudViewVisibleFilter)
    {
        if ($this->getCrudViewVisibleFilters()->contains($crudViewVisibleFilter)) {
            $pos = $this->collCrudViewVisibleFilters->search($crudViewVisibleFilter);
            $this->collCrudViewVisibleFilters->remove($pos);
            if (null === $this->crudViewVisibleFiltersScheduledForDeletion) {
                $this->crudViewVisibleFiltersScheduledForDeletion = clone $this->collCrudViewVisibleFilters;
                $this->crudViewVisibleFiltersScheduledForDeletion->clear();
            }
            $this->crudViewVisibleFiltersScheduledForDeletion[]= clone $crudViewVisibleFilter;
            $crudViewVisibleFilter->setFilterOperator(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this FilterOperator is new, it will return
     * an empty collection; or if this FilterOperator has previously
     * been saved, it will retrieve related CrudViewVisibleFilters from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in FilterOperator.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCrudViewVisibleFilter[] List of ChildCrudViewVisibleFilter objects
     */
    public function getCrudViewVisibleFiltersJoinCrudView(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCrudViewVisibleFilterQuery::create(null, $criteria);
        $query->joinWith('CrudView', $joinBehavior);

        return $this->getCrudViewVisibleFilters($query, $con);
    }

    /**
     * Clears out the collCrudViewHiddenFilters collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCrudViewHiddenFilters()
     */
    public function clearCrudViewHiddenFilters()
    {
        $this->collCrudViewHiddenFilters = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCrudViewHiddenFilters collection loaded partially.
     */
    public function resetPartialCrudViewHiddenFilters($v = true)
    {
        $this->collCrudViewHiddenFiltersPartial = $v;
    }

    /**
     * Initializes the collCrudViewHiddenFilters collection.
     *
     * By default this just sets the collCrudViewHiddenFilters collection to an empty array (like clearcollCrudViewHiddenFilters());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCrudViewHiddenFilters($overrideExisting = true)
    {
        if (null !== $this->collCrudViewHiddenFilters && !$overrideExisting) {
            return;
        }

        $collectionClassName = CrudViewHiddenFilterTableMap::getTableMap()->getCollectionClassName();

        $this->collCrudViewHiddenFilters = new $collectionClassName;
        $this->collCrudViewHiddenFilters->setModel('\Model\Setting\CrudManager\CrudViewHiddenFilter');
    }

    /**
     * Gets an array of ChildCrudViewHiddenFilter objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildFilterOperator is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCrudViewHiddenFilter[] List of ChildCrudViewHiddenFilter objects
     * @throws PropelException
     */
    public function getCrudViewHiddenFilters(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudViewHiddenFiltersPartial && !$this->isNew();
        if (null === $this->collCrudViewHiddenFilters || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCrudViewHiddenFilters) {
                    $this->initCrudViewHiddenFilters();
                } else {
                    $collectionClassName = CrudViewHiddenFilterTableMap::getTableMap()->getCollectionClassName();

                    $collCrudViewHiddenFilters = new $collectionClassName;
                    $collCrudViewHiddenFilters->setModel('\Model\Setting\CrudManager\CrudViewHiddenFilter');

                    return $collCrudViewHiddenFilters;
                }
            } else {
                $collCrudViewHiddenFilters = ChildCrudViewHiddenFilterQuery::create(null, $criteria)
                    ->filterByFilterOperator($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCrudViewHiddenFiltersPartial && count($collCrudViewHiddenFilters)) {
                        $this->initCrudViewHiddenFilters(false);

                        foreach ($collCrudViewHiddenFilters as $obj) {
                            if (false == $this->collCrudViewHiddenFilters->contains($obj)) {
                                $this->collCrudViewHiddenFilters->append($obj);
                            }
                        }

                        $this->collCrudViewHiddenFiltersPartial = true;
                    }

                    return $collCrudViewHiddenFilters;
                }

                if ($partial && $this->collCrudViewHiddenFilters) {
                    foreach ($this->collCrudViewHiddenFilters as $obj) {
                        if ($obj->isNew()) {
                            $collCrudViewHiddenFilters[] = $obj;
                        }
                    }
                }

                $this->collCrudViewHiddenFilters = $collCrudViewHiddenFilters;
                $this->collCrudViewHiddenFiltersPartial = false;
            }
        }

        return $this->collCrudViewHiddenFilters;
    }

    /**
     * Sets a collection of ChildCrudViewHiddenFilter objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $crudViewHiddenFilters A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildFilterOperator The current object (for fluent API support)
     */
    public function setCrudViewHiddenFilters(Collection $crudViewHiddenFilters, ConnectionInterface $con = null)
    {
        /** @var ChildCrudViewHiddenFilter[] $crudViewHiddenFiltersToDelete */
        $crudViewHiddenFiltersToDelete = $this->getCrudViewHiddenFilters(new Criteria(), $con)->diff($crudViewHiddenFilters);


        $this->crudViewHiddenFiltersScheduledForDeletion = $crudViewHiddenFiltersToDelete;

        foreach ($crudViewHiddenFiltersToDelete as $crudViewHiddenFilterRemoved) {
            $crudViewHiddenFilterRemoved->setFilterOperator(null);
        }

        $this->collCrudViewHiddenFilters = null;
        foreach ($crudViewHiddenFilters as $crudViewHiddenFilter) {
            $this->addCrudViewHiddenFilter($crudViewHiddenFilter);
        }

        $this->collCrudViewHiddenFilters = $crudViewHiddenFilters;
        $this->collCrudViewHiddenFiltersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CrudViewHiddenFilter objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CrudViewHiddenFilter objects.
     * @throws PropelException
     */
    public function countCrudViewHiddenFilters(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudViewHiddenFiltersPartial && !$this->isNew();
        if (null === $this->collCrudViewHiddenFilters || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCrudViewHiddenFilters) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCrudViewHiddenFilters());
            }

            $query = ChildCrudViewHiddenFilterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFilterOperator($this)
                ->count($con);
        }

        return count($this->collCrudViewHiddenFilters);
    }

    /**
     * Method called to associate a ChildCrudViewHiddenFilter object to this object
     * through the ChildCrudViewHiddenFilter foreign key attribute.
     *
     * @param  ChildCrudViewHiddenFilter $l ChildCrudViewHiddenFilter
     * @return $this|\Model\Setting\CrudManager\FilterOperator The current object (for fluent API support)
     */
    public function addCrudViewHiddenFilter(ChildCrudViewHiddenFilter $l)
    {
        if ($this->collCrudViewHiddenFilters === null) {
            $this->initCrudViewHiddenFilters();
            $this->collCrudViewHiddenFiltersPartial = true;
        }

        if (!$this->collCrudViewHiddenFilters->contains($l)) {
            $this->doAddCrudViewHiddenFilter($l);

            if ($this->crudViewHiddenFiltersScheduledForDeletion and $this->crudViewHiddenFiltersScheduledForDeletion->contains($l)) {
                $this->crudViewHiddenFiltersScheduledForDeletion->remove($this->crudViewHiddenFiltersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCrudViewHiddenFilter $crudViewHiddenFilter The ChildCrudViewHiddenFilter object to add.
     */
    protected function doAddCrudViewHiddenFilter(ChildCrudViewHiddenFilter $crudViewHiddenFilter)
    {
        $this->collCrudViewHiddenFilters[]= $crudViewHiddenFilter;
        $crudViewHiddenFilter->setFilterOperator($this);
    }

    /**
     * @param  ChildCrudViewHiddenFilter $crudViewHiddenFilter The ChildCrudViewHiddenFilter object to remove.
     * @return $this|ChildFilterOperator The current object (for fluent API support)
     */
    public function removeCrudViewHiddenFilter(ChildCrudViewHiddenFilter $crudViewHiddenFilter)
    {
        if ($this->getCrudViewHiddenFilters()->contains($crudViewHiddenFilter)) {
            $pos = $this->collCrudViewHiddenFilters->search($crudViewHiddenFilter);
            $this->collCrudViewHiddenFilters->remove($pos);
            if (null === $this->crudViewHiddenFiltersScheduledForDeletion) {
                $this->crudViewHiddenFiltersScheduledForDeletion = clone $this->collCrudViewHiddenFilters;
                $this->crudViewHiddenFiltersScheduledForDeletion->clear();
            }
            $this->crudViewHiddenFiltersScheduledForDeletion[]= clone $crudViewHiddenFilter;
            $crudViewHiddenFilter->setFilterOperator(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this FilterOperator is new, it will return
     * an empty collection; or if this FilterOperator has previously
     * been saved, it will retrieve related CrudViewHiddenFilters from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in FilterOperator.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCrudViewHiddenFilter[] List of ChildCrudViewHiddenFilter objects
     */
    public function getCrudViewHiddenFiltersJoinCrudView(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCrudViewHiddenFilterQuery::create(null, $criteria);
        $query->joinWith('CrudView', $joinBehavior);

        return $this->getCrudViewHiddenFilters($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->name = null;
        $this->description = null;
        $this->description_sentence = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collMailingListsCriterias) {
                foreach ($this->collMailingListsCriterias as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCrudEditorButtonVisibileFilters) {
                foreach ($this->collCrudEditorButtonVisibileFilters as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFilterOperatorDatatypes) {
                foreach ($this->collFilterOperatorDatatypes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCrudViewVisibleFilters) {
                foreach ($this->collCrudViewVisibleFilters as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCrudViewHiddenFilters) {
                foreach ($this->collCrudViewHiddenFilters as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collMailingListsCriterias = null;
        $this->collCrudEditorButtonVisibileFilters = null;
        $this->collFilterOperatorDatatypes = null;
        $this->collCrudViewVisibleFilters = null;
        $this->collCrudViewHiddenFilters = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(FilterOperatorTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildFilterOperator The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[FilterOperatorTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
