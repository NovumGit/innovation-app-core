<?php

namespace Model\Setting\CrudManager\Base;

use \Exception;
use \PDO;
use Model\Setting\CrudManager\FilterOperatorDatatype as ChildFilterOperatorDatatype;
use Model\Setting\CrudManager\FilterOperatorDatatypeQuery as ChildFilterOperatorDatatypeQuery;
use Model\Setting\CrudManager\Map\FilterOperatorDatatypeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'filter_operator_datatype' table.
 *
 *
 *
 * @method     ChildFilterOperatorDatatypeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildFilterOperatorDatatypeQuery orderByFilterDatatypeId($order = Criteria::ASC) Order by the filter_datatype_id column
 * @method     ChildFilterOperatorDatatypeQuery orderByFilterOperatorId($order = Criteria::ASC) Order by the filter_operator_id column
 * @method     ChildFilterOperatorDatatypeQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildFilterOperatorDatatypeQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildFilterOperatorDatatypeQuery groupById() Group by the id column
 * @method     ChildFilterOperatorDatatypeQuery groupByFilterDatatypeId() Group by the filter_datatype_id column
 * @method     ChildFilterOperatorDatatypeQuery groupByFilterOperatorId() Group by the filter_operator_id column
 * @method     ChildFilterOperatorDatatypeQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildFilterOperatorDatatypeQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildFilterOperatorDatatypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFilterOperatorDatatypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFilterOperatorDatatypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFilterOperatorDatatypeQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFilterOperatorDatatypeQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFilterOperatorDatatypeQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFilterOperatorDatatypeQuery leftJoinFilterDatatype($relationAlias = null) Adds a LEFT JOIN clause to the query using the FilterDatatype relation
 * @method     ChildFilterOperatorDatatypeQuery rightJoinFilterDatatype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FilterDatatype relation
 * @method     ChildFilterOperatorDatatypeQuery innerJoinFilterDatatype($relationAlias = null) Adds a INNER JOIN clause to the query using the FilterDatatype relation
 *
 * @method     ChildFilterOperatorDatatypeQuery joinWithFilterDatatype($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FilterDatatype relation
 *
 * @method     ChildFilterOperatorDatatypeQuery leftJoinWithFilterDatatype() Adds a LEFT JOIN clause and with to the query using the FilterDatatype relation
 * @method     ChildFilterOperatorDatatypeQuery rightJoinWithFilterDatatype() Adds a RIGHT JOIN clause and with to the query using the FilterDatatype relation
 * @method     ChildFilterOperatorDatatypeQuery innerJoinWithFilterDatatype() Adds a INNER JOIN clause and with to the query using the FilterDatatype relation
 *
 * @method     ChildFilterOperatorDatatypeQuery leftJoinFilterOperator($relationAlias = null) Adds a LEFT JOIN clause to the query using the FilterOperator relation
 * @method     ChildFilterOperatorDatatypeQuery rightJoinFilterOperator($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FilterOperator relation
 * @method     ChildFilterOperatorDatatypeQuery innerJoinFilterOperator($relationAlias = null) Adds a INNER JOIN clause to the query using the FilterOperator relation
 *
 * @method     ChildFilterOperatorDatatypeQuery joinWithFilterOperator($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FilterOperator relation
 *
 * @method     ChildFilterOperatorDatatypeQuery leftJoinWithFilterOperator() Adds a LEFT JOIN clause and with to the query using the FilterOperator relation
 * @method     ChildFilterOperatorDatatypeQuery rightJoinWithFilterOperator() Adds a RIGHT JOIN clause and with to the query using the FilterOperator relation
 * @method     ChildFilterOperatorDatatypeQuery innerJoinWithFilterOperator() Adds a INNER JOIN clause and with to the query using the FilterOperator relation
 *
 * @method     \Model\Setting\CrudManager\FilterDatatypeQuery|\Model\Setting\CrudManager\FilterOperatorQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFilterOperatorDatatype findOne(ConnectionInterface $con = null) Return the first ChildFilterOperatorDatatype matching the query
 * @method     ChildFilterOperatorDatatype findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFilterOperatorDatatype matching the query, or a new ChildFilterOperatorDatatype object populated from the query conditions when no match is found
 *
 * @method     ChildFilterOperatorDatatype findOneById(int $id) Return the first ChildFilterOperatorDatatype filtered by the id column
 * @method     ChildFilterOperatorDatatype findOneByFilterDatatypeId(int $filter_datatype_id) Return the first ChildFilterOperatorDatatype filtered by the filter_datatype_id column
 * @method     ChildFilterOperatorDatatype findOneByFilterOperatorId(int $filter_operator_id) Return the first ChildFilterOperatorDatatype filtered by the filter_operator_id column
 * @method     ChildFilterOperatorDatatype findOneByCreatedAt(string $created_at) Return the first ChildFilterOperatorDatatype filtered by the created_at column
 * @method     ChildFilterOperatorDatatype findOneByUpdatedAt(string $updated_at) Return the first ChildFilterOperatorDatatype filtered by the updated_at column *

 * @method     ChildFilterOperatorDatatype requirePk($key, ConnectionInterface $con = null) Return the ChildFilterOperatorDatatype by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFilterOperatorDatatype requireOne(ConnectionInterface $con = null) Return the first ChildFilterOperatorDatatype matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFilterOperatorDatatype requireOneById(int $id) Return the first ChildFilterOperatorDatatype filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFilterOperatorDatatype requireOneByFilterDatatypeId(int $filter_datatype_id) Return the first ChildFilterOperatorDatatype filtered by the filter_datatype_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFilterOperatorDatatype requireOneByFilterOperatorId(int $filter_operator_id) Return the first ChildFilterOperatorDatatype filtered by the filter_operator_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFilterOperatorDatatype requireOneByCreatedAt(string $created_at) Return the first ChildFilterOperatorDatatype filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFilterOperatorDatatype requireOneByUpdatedAt(string $updated_at) Return the first ChildFilterOperatorDatatype filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFilterOperatorDatatype[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFilterOperatorDatatype objects based on current ModelCriteria
 * @method     ChildFilterOperatorDatatype[]|ObjectCollection findById(int $id) Return ChildFilterOperatorDatatype objects filtered by the id column
 * @method     ChildFilterOperatorDatatype[]|ObjectCollection findByFilterDatatypeId(int $filter_datatype_id) Return ChildFilterOperatorDatatype objects filtered by the filter_datatype_id column
 * @method     ChildFilterOperatorDatatype[]|ObjectCollection findByFilterOperatorId(int $filter_operator_id) Return ChildFilterOperatorDatatype objects filtered by the filter_operator_id column
 * @method     ChildFilterOperatorDatatype[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildFilterOperatorDatatype objects filtered by the created_at column
 * @method     ChildFilterOperatorDatatype[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildFilterOperatorDatatype objects filtered by the updated_at column
 * @method     ChildFilterOperatorDatatype[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FilterOperatorDatatypeQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\CrudManager\Base\FilterOperatorDatatypeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\CrudManager\\FilterOperatorDatatype', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFilterOperatorDatatypeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFilterOperatorDatatypeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFilterOperatorDatatypeQuery) {
            return $criteria;
        }
        $query = new ChildFilterOperatorDatatypeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFilterOperatorDatatype|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FilterOperatorDatatypeTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = FilterOperatorDatatypeTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFilterOperatorDatatype A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, filter_datatype_id, filter_operator_id, created_at, updated_at FROM filter_operator_datatype WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFilterOperatorDatatype $obj */
            $obj = new ChildFilterOperatorDatatype();
            $obj->hydrate($row);
            FilterOperatorDatatypeTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFilterOperatorDatatype|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the filter_datatype_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFilterDatatypeId(1234); // WHERE filter_datatype_id = 1234
     * $query->filterByFilterDatatypeId(array(12, 34)); // WHERE filter_datatype_id IN (12, 34)
     * $query->filterByFilterDatatypeId(array('min' => 12)); // WHERE filter_datatype_id > 12
     * </code>
     *
     * @see       filterByFilterDatatype()
     *
     * @param     mixed $filterDatatypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function filterByFilterDatatypeId($filterDatatypeId = null, $comparison = null)
    {
        if (is_array($filterDatatypeId)) {
            $useMinMax = false;
            if (isset($filterDatatypeId['min'])) {
                $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_FILTER_DATATYPE_ID, $filterDatatypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($filterDatatypeId['max'])) {
                $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_FILTER_DATATYPE_ID, $filterDatatypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_FILTER_DATATYPE_ID, $filterDatatypeId, $comparison);
    }

    /**
     * Filter the query on the filter_operator_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFilterOperatorId(1234); // WHERE filter_operator_id = 1234
     * $query->filterByFilterOperatorId(array(12, 34)); // WHERE filter_operator_id IN (12, 34)
     * $query->filterByFilterOperatorId(array('min' => 12)); // WHERE filter_operator_id > 12
     * </code>
     *
     * @see       filterByFilterOperator()
     *
     * @param     mixed $filterOperatorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function filterByFilterOperatorId($filterOperatorId = null, $comparison = null)
    {
        if (is_array($filterOperatorId)) {
            $useMinMax = false;
            if (isset($filterOperatorId['min'])) {
                $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_FILTER_OPERATOR_ID, $filterOperatorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($filterOperatorId['max'])) {
                $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_FILTER_OPERATOR_ID, $filterOperatorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_FILTER_OPERATOR_ID, $filterOperatorId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\FilterDatatype object
     *
     * @param \Model\Setting\CrudManager\FilterDatatype|ObjectCollection $filterDatatype The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function filterByFilterDatatype($filterDatatype, $comparison = null)
    {
        if ($filterDatatype instanceof \Model\Setting\CrudManager\FilterDatatype) {
            return $this
                ->addUsingAlias(FilterOperatorDatatypeTableMap::COL_FILTER_DATATYPE_ID, $filterDatatype->getId(), $comparison);
        } elseif ($filterDatatype instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FilterOperatorDatatypeTableMap::COL_FILTER_DATATYPE_ID, $filterDatatype->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFilterDatatype() only accepts arguments of type \Model\Setting\CrudManager\FilterDatatype or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FilterDatatype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function joinFilterDatatype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FilterDatatype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FilterDatatype');
        }

        return $this;
    }

    /**
     * Use the FilterDatatype relation FilterDatatype object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\FilterDatatypeQuery A secondary query class using the current class as primary query
     */
    public function useFilterDatatypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFilterDatatype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FilterDatatype', '\Model\Setting\CrudManager\FilterDatatypeQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\FilterOperator object
     *
     * @param \Model\Setting\CrudManager\FilterOperator|ObjectCollection $filterOperator The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function filterByFilterOperator($filterOperator, $comparison = null)
    {
        if ($filterOperator instanceof \Model\Setting\CrudManager\FilterOperator) {
            return $this
                ->addUsingAlias(FilterOperatorDatatypeTableMap::COL_FILTER_OPERATOR_ID, $filterOperator->getId(), $comparison);
        } elseif ($filterOperator instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(FilterOperatorDatatypeTableMap::COL_FILTER_OPERATOR_ID, $filterOperator->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFilterOperator() only accepts arguments of type \Model\Setting\CrudManager\FilterOperator or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FilterOperator relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function joinFilterOperator($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FilterOperator');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FilterOperator');
        }

        return $this;
    }

    /**
     * Use the FilterOperator relation FilterOperator object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\FilterOperatorQuery A secondary query class using the current class as primary query
     */
    public function useFilterOperatorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFilterOperator($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FilterOperator', '\Model\Setting\CrudManager\FilterOperatorQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFilterOperatorDatatype $filterOperatorDatatype Object to remove from the list of results
     *
     * @return $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function prune($filterOperatorDatatype = null)
    {
        if ($filterOperatorDatatype) {
            $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_ID, $filterOperatorDatatype->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the filter_operator_datatype table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FilterOperatorDatatypeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FilterOperatorDatatypeTableMap::clearInstancePool();
            FilterOperatorDatatypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FilterOperatorDatatypeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FilterOperatorDatatypeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FilterOperatorDatatypeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FilterOperatorDatatypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(FilterOperatorDatatypeTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(FilterOperatorDatatypeTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(FilterOperatorDatatypeTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(FilterOperatorDatatypeTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildFilterOperatorDatatypeQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(FilterOperatorDatatypeTableMap::COL_CREATED_AT);
    }

} // FilterOperatorDatatypeQuery
