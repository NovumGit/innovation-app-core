<?php

namespace Model\Setting\CrudManager\Base;

use \Exception;
use \PDO;
use Model\Eenoverheid\Formulier;
use Model\Setting\CrudManager\CrudEditor as ChildCrudEditor;
use Model\Setting\CrudManager\CrudEditorQuery as ChildCrudEditorQuery;
use Model\Setting\CrudManager\Map\CrudEditorTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'crud_editor' table.
 *
 *
 *
 * @method     ChildCrudEditorQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCrudEditorQuery orderByCrudConfigId($order = Criteria::ASC) Order by the crud_config_id column
 * @method     ChildCrudEditorQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildCrudEditorQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildCrudEditorQuery orderByPublicDescription($order = Criteria::ASC) Order by the public_description column
 * @method     ChildCrudEditorQuery orderByPrivateDescription($order = Criteria::ASC) Order by the private_description column
 * @method     ChildCrudEditorQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCrudEditorQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCrudEditorQuery groupById() Group by the id column
 * @method     ChildCrudEditorQuery groupByCrudConfigId() Group by the crud_config_id column
 * @method     ChildCrudEditorQuery groupByName() Group by the name column
 * @method     ChildCrudEditorQuery groupByTitle() Group by the title column
 * @method     ChildCrudEditorQuery groupByPublicDescription() Group by the public_description column
 * @method     ChildCrudEditorQuery groupByPrivateDescription() Group by the private_description column
 * @method     ChildCrudEditorQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCrudEditorQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCrudEditorQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCrudEditorQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCrudEditorQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCrudEditorQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCrudEditorQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCrudEditorQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCrudEditorQuery leftJoinCrudConfig($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudConfig relation
 * @method     ChildCrudEditorQuery rightJoinCrudConfig($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudConfig relation
 * @method     ChildCrudEditorQuery innerJoinCrudConfig($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudConfig relation
 *
 * @method     ChildCrudEditorQuery joinWithCrudConfig($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudConfig relation
 *
 * @method     ChildCrudEditorQuery leftJoinWithCrudConfig() Adds a LEFT JOIN clause and with to the query using the CrudConfig relation
 * @method     ChildCrudEditorQuery rightJoinWithCrudConfig() Adds a RIGHT JOIN clause and with to the query using the CrudConfig relation
 * @method     ChildCrudEditorQuery innerJoinWithCrudConfig() Adds a INNER JOIN clause and with to the query using the CrudConfig relation
 *
 * @method     ChildCrudEditorQuery leftJoinCrudEditorBlock($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudEditorBlock relation
 * @method     ChildCrudEditorQuery rightJoinCrudEditorBlock($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudEditorBlock relation
 * @method     ChildCrudEditorQuery innerJoinCrudEditorBlock($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudEditorBlock relation
 *
 * @method     ChildCrudEditorQuery joinWithCrudEditorBlock($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudEditorBlock relation
 *
 * @method     ChildCrudEditorQuery leftJoinWithCrudEditorBlock() Adds a LEFT JOIN clause and with to the query using the CrudEditorBlock relation
 * @method     ChildCrudEditorQuery rightJoinWithCrudEditorBlock() Adds a RIGHT JOIN clause and with to the query using the CrudEditorBlock relation
 * @method     ChildCrudEditorQuery innerJoinWithCrudEditorBlock() Adds a INNER JOIN clause and with to the query using the CrudEditorBlock relation
 *
 * @method     ChildCrudEditorQuery leftJoinCrudEditorButton($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudEditorButton relation
 * @method     ChildCrudEditorQuery rightJoinCrudEditorButton($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudEditorButton relation
 * @method     ChildCrudEditorQuery innerJoinCrudEditorButton($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudEditorButton relation
 *
 * @method     ChildCrudEditorQuery joinWithCrudEditorButton($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudEditorButton relation
 *
 * @method     ChildCrudEditorQuery leftJoinWithCrudEditorButton() Adds a LEFT JOIN clause and with to the query using the CrudEditorButton relation
 * @method     ChildCrudEditorQuery rightJoinWithCrudEditorButton() Adds a RIGHT JOIN clause and with to the query using the CrudEditorButton relation
 * @method     ChildCrudEditorQuery innerJoinWithCrudEditorButton() Adds a INNER JOIN clause and with to the query using the CrudEditorButton relation
 *
 * @method     ChildCrudEditorQuery leftJoinFormulier($relationAlias = null) Adds a LEFT JOIN clause to the query using the Formulier relation
 * @method     ChildCrudEditorQuery rightJoinFormulier($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Formulier relation
 * @method     ChildCrudEditorQuery innerJoinFormulier($relationAlias = null) Adds a INNER JOIN clause to the query using the Formulier relation
 *
 * @method     ChildCrudEditorQuery joinWithFormulier($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Formulier relation
 *
 * @method     ChildCrudEditorQuery leftJoinWithFormulier() Adds a LEFT JOIN clause and with to the query using the Formulier relation
 * @method     ChildCrudEditorQuery rightJoinWithFormulier() Adds a RIGHT JOIN clause and with to the query using the Formulier relation
 * @method     ChildCrudEditorQuery innerJoinWithFormulier() Adds a INNER JOIN clause and with to the query using the Formulier relation
 *
 * @method     \Model\Setting\CrudManager\CrudConfigQuery|\Model\Setting\CrudManager\CrudEditorBlockQuery|\Model\Setting\CrudManager\CrudEditorButtonQuery|\Model\Eenoverheid\FormulierQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCrudEditor findOne(ConnectionInterface $con = null) Return the first ChildCrudEditor matching the query
 * @method     ChildCrudEditor findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCrudEditor matching the query, or a new ChildCrudEditor object populated from the query conditions when no match is found
 *
 * @method     ChildCrudEditor findOneById(int $id) Return the first ChildCrudEditor filtered by the id column
 * @method     ChildCrudEditor findOneByCrudConfigId(int $crud_config_id) Return the first ChildCrudEditor filtered by the crud_config_id column
 * @method     ChildCrudEditor findOneByName(string $name) Return the first ChildCrudEditor filtered by the name column
 * @method     ChildCrudEditor findOneByTitle(string $title) Return the first ChildCrudEditor filtered by the title column
 * @method     ChildCrudEditor findOneByPublicDescription(string $public_description) Return the first ChildCrudEditor filtered by the public_description column
 * @method     ChildCrudEditor findOneByPrivateDescription(string $private_description) Return the first ChildCrudEditor filtered by the private_description column
 * @method     ChildCrudEditor findOneByCreatedAt(string $created_at) Return the first ChildCrudEditor filtered by the created_at column
 * @method     ChildCrudEditor findOneByUpdatedAt(string $updated_at) Return the first ChildCrudEditor filtered by the updated_at column *

 * @method     ChildCrudEditor requirePk($key, ConnectionInterface $con = null) Return the ChildCrudEditor by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditor requireOne(ConnectionInterface $con = null) Return the first ChildCrudEditor matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudEditor requireOneById(int $id) Return the first ChildCrudEditor filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditor requireOneByCrudConfigId(int $crud_config_id) Return the first ChildCrudEditor filtered by the crud_config_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditor requireOneByName(string $name) Return the first ChildCrudEditor filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditor requireOneByTitle(string $title) Return the first ChildCrudEditor filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditor requireOneByPublicDescription(string $public_description) Return the first ChildCrudEditor filtered by the public_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditor requireOneByPrivateDescription(string $private_description) Return the first ChildCrudEditor filtered by the private_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditor requireOneByCreatedAt(string $created_at) Return the first ChildCrudEditor filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditor requireOneByUpdatedAt(string $updated_at) Return the first ChildCrudEditor filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudEditor[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCrudEditor objects based on current ModelCriteria
 * @method     ChildCrudEditor[]|ObjectCollection findById(int $id) Return ChildCrudEditor objects filtered by the id column
 * @method     ChildCrudEditor[]|ObjectCollection findByCrudConfigId(int $crud_config_id) Return ChildCrudEditor objects filtered by the crud_config_id column
 * @method     ChildCrudEditor[]|ObjectCollection findByName(string $name) Return ChildCrudEditor objects filtered by the name column
 * @method     ChildCrudEditor[]|ObjectCollection findByTitle(string $title) Return ChildCrudEditor objects filtered by the title column
 * @method     ChildCrudEditor[]|ObjectCollection findByPublicDescription(string $public_description) Return ChildCrudEditor objects filtered by the public_description column
 * @method     ChildCrudEditor[]|ObjectCollection findByPrivateDescription(string $private_description) Return ChildCrudEditor objects filtered by the private_description column
 * @method     ChildCrudEditor[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCrudEditor objects filtered by the created_at column
 * @method     ChildCrudEditor[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCrudEditor objects filtered by the updated_at column
 * @method     ChildCrudEditor[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CrudEditorQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\CrudManager\Base\CrudEditorQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\CrudManager\\CrudEditor', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCrudEditorQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCrudEditorQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCrudEditorQuery) {
            return $criteria;
        }
        $query = new ChildCrudEditorQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCrudEditor|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CrudEditorTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CrudEditorTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudEditor A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, crud_config_id, name, title, public_description, private_description, created_at, updated_at FROM crud_editor WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCrudEditor $obj */
            $obj = new ChildCrudEditor();
            $obj->hydrate($row);
            CrudEditorTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCrudEditor|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CrudEditorTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CrudEditorTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CrudEditorTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CrudEditorTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the crud_config_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCrudConfigId(1234); // WHERE crud_config_id = 1234
     * $query->filterByCrudConfigId(array(12, 34)); // WHERE crud_config_id IN (12, 34)
     * $query->filterByCrudConfigId(array('min' => 12)); // WHERE crud_config_id > 12
     * </code>
     *
     * @see       filterByCrudConfig()
     *
     * @param     mixed $crudConfigId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterByCrudConfigId($crudConfigId = null, $comparison = null)
    {
        if (is_array($crudConfigId)) {
            $useMinMax = false;
            if (isset($crudConfigId['min'])) {
                $this->addUsingAlias(CrudEditorTableMap::COL_CRUD_CONFIG_ID, $crudConfigId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($crudConfigId['max'])) {
                $this->addUsingAlias(CrudEditorTableMap::COL_CRUD_CONFIG_ID, $crudConfigId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorTableMap::COL_CRUD_CONFIG_ID, $crudConfigId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the public_description column
     *
     * Example usage:
     * <code>
     * $query->filterByPublicDescription('fooValue');   // WHERE public_description = 'fooValue'
     * $query->filterByPublicDescription('%fooValue%', Criteria::LIKE); // WHERE public_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $publicDescription The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterByPublicDescription($publicDescription = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($publicDescription)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorTableMap::COL_PUBLIC_DESCRIPTION, $publicDescription, $comparison);
    }

    /**
     * Filter the query on the private_description column
     *
     * Example usage:
     * <code>
     * $query->filterByPrivateDescription('fooValue');   // WHERE private_description = 'fooValue'
     * $query->filterByPrivateDescription('%fooValue%', Criteria::LIKE); // WHERE private_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $privateDescription The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterByPrivateDescription($privateDescription = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($privateDescription)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorTableMap::COL_PRIVATE_DESCRIPTION, $privateDescription, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CrudEditorTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CrudEditorTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CrudEditorTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CrudEditorTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudConfig object
     *
     * @param \Model\Setting\CrudManager\CrudConfig|ObjectCollection $crudConfig The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterByCrudConfig($crudConfig, $comparison = null)
    {
        if ($crudConfig instanceof \Model\Setting\CrudManager\CrudConfig) {
            return $this
                ->addUsingAlias(CrudEditorTableMap::COL_CRUD_CONFIG_ID, $crudConfig->getId(), $comparison);
        } elseif ($crudConfig instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CrudEditorTableMap::COL_CRUD_CONFIG_ID, $crudConfig->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCrudConfig() only accepts arguments of type \Model\Setting\CrudManager\CrudConfig or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudConfig relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function joinCrudConfig($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudConfig');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudConfig');
        }

        return $this;
    }

    /**
     * Use the CrudConfig relation CrudConfig object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudConfigQuery A secondary query class using the current class as primary query
     */
    public function useCrudConfigQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudConfig($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudConfig', '\Model\Setting\CrudManager\CrudConfigQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudEditorBlock object
     *
     * @param \Model\Setting\CrudManager\CrudEditorBlock|ObjectCollection $crudEditorBlock the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterByCrudEditorBlock($crudEditorBlock, $comparison = null)
    {
        if ($crudEditorBlock instanceof \Model\Setting\CrudManager\CrudEditorBlock) {
            return $this
                ->addUsingAlias(CrudEditorTableMap::COL_ID, $crudEditorBlock->getCrudEditorId(), $comparison);
        } elseif ($crudEditorBlock instanceof ObjectCollection) {
            return $this
                ->useCrudEditorBlockQuery()
                ->filterByPrimaryKeys($crudEditorBlock->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudEditorBlock() only accepts arguments of type \Model\Setting\CrudManager\CrudEditorBlock or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudEditorBlock relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function joinCrudEditorBlock($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudEditorBlock');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudEditorBlock');
        }

        return $this;
    }

    /**
     * Use the CrudEditorBlock relation CrudEditorBlock object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudEditorBlockQuery A secondary query class using the current class as primary query
     */
    public function useCrudEditorBlockQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudEditorBlock($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudEditorBlock', '\Model\Setting\CrudManager\CrudEditorBlockQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudEditorButton object
     *
     * @param \Model\Setting\CrudManager\CrudEditorButton|ObjectCollection $crudEditorButton the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterByCrudEditorButton($crudEditorButton, $comparison = null)
    {
        if ($crudEditorButton instanceof \Model\Setting\CrudManager\CrudEditorButton) {
            return $this
                ->addUsingAlias(CrudEditorTableMap::COL_ID, $crudEditorButton->getCrudEditorId(), $comparison);
        } elseif ($crudEditorButton instanceof ObjectCollection) {
            return $this
                ->useCrudEditorButtonQuery()
                ->filterByPrimaryKeys($crudEditorButton->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudEditorButton() only accepts arguments of type \Model\Setting\CrudManager\CrudEditorButton or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudEditorButton relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function joinCrudEditorButton($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudEditorButton');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudEditorButton');
        }

        return $this;
    }

    /**
     * Use the CrudEditorButton relation CrudEditorButton object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudEditorButtonQuery A secondary query class using the current class as primary query
     */
    public function useCrudEditorButtonQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudEditorButton($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudEditorButton', '\Model\Setting\CrudManager\CrudEditorButtonQuery');
    }

    /**
     * Filter the query by a related \Model\Eenoverheid\Formulier object
     *
     * @param \Model\Eenoverheid\Formulier|ObjectCollection $formulier the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudEditorQuery The current query, for fluid interface
     */
    public function filterByFormulier($formulier, $comparison = null)
    {
        if ($formulier instanceof \Model\Eenoverheid\Formulier) {
            return $this
                ->addUsingAlias(CrudEditorTableMap::COL_ID, $formulier->getCrudEditorId(), $comparison);
        } elseif ($formulier instanceof ObjectCollection) {
            return $this
                ->useFormulierQuery()
                ->filterByPrimaryKeys($formulier->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFormulier() only accepts arguments of type \Model\Eenoverheid\Formulier or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Formulier relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function joinFormulier($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Formulier');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Formulier');
        }

        return $this;
    }

    /**
     * Use the Formulier relation Formulier object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Eenoverheid\FormulierQuery A secondary query class using the current class as primary query
     */
    public function useFormulierQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFormulier($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Formulier', '\Model\Eenoverheid\FormulierQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCrudEditor $crudEditor Object to remove from the list of results
     *
     * @return $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function prune($crudEditor = null)
    {
        if ($crudEditor) {
            $this->addUsingAlias(CrudEditorTableMap::COL_ID, $crudEditor->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the crud_editor table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CrudEditorTableMap::clearInstancePool();
            CrudEditorTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CrudEditorTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CrudEditorTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CrudEditorTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudEditorTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudEditorTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudEditorTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudEditorTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudEditorTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCrudEditorQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudEditorTableMap::COL_CREATED_AT);
    }

} // CrudEditorQuery
