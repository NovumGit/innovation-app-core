<?php

namespace Model\Setting\CrudManager\Base;

use \Exception;
use \PDO;
use Model\Setting\CrudManager\CrudViewButton as ChildCrudViewButton;
use Model\Setting\CrudManager\CrudViewButtonQuery as ChildCrudViewButtonQuery;
use Model\Setting\CrudManager\Map\CrudViewButtonTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'crud_view_button' table.
 *
 *
 *
 * @method     ChildCrudViewButtonQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCrudViewButtonQuery orderByCrudViewId($order = Criteria::ASC) Order by the crud_view_id column
 * @method     ChildCrudViewButtonQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildCrudViewButtonQuery orderByIconBeforeClick($order = Criteria::ASC) Order by the icon_before_click column
 * @method     ChildCrudViewButtonQuery orderByIconAfterClick($order = Criteria::ASC) Order by the icon_after_click column
 * @method     ChildCrudViewButtonQuery orderByColorBeforeClick($order = Criteria::ASC) Order by the color_before_click column
 * @method     ChildCrudViewButtonQuery orderByColorAfterClick($order = Criteria::ASC) Order by the color_after_click column
 * @method     ChildCrudViewButtonQuery orderBySorting($order = Criteria::ASC) Order by the sorting column
 * @method     ChildCrudViewButtonQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCrudViewButtonQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCrudViewButtonQuery groupById() Group by the id column
 * @method     ChildCrudViewButtonQuery groupByCrudViewId() Group by the crud_view_id column
 * @method     ChildCrudViewButtonQuery groupByTitle() Group by the title column
 * @method     ChildCrudViewButtonQuery groupByIconBeforeClick() Group by the icon_before_click column
 * @method     ChildCrudViewButtonQuery groupByIconAfterClick() Group by the icon_after_click column
 * @method     ChildCrudViewButtonQuery groupByColorBeforeClick() Group by the color_before_click column
 * @method     ChildCrudViewButtonQuery groupByColorAfterClick() Group by the color_after_click column
 * @method     ChildCrudViewButtonQuery groupBySorting() Group by the sorting column
 * @method     ChildCrudViewButtonQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCrudViewButtonQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCrudViewButtonQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCrudViewButtonQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCrudViewButtonQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCrudViewButtonQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCrudViewButtonQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCrudViewButtonQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCrudViewButtonQuery leftJoinCrudView($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudView relation
 * @method     ChildCrudViewButtonQuery rightJoinCrudView($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudView relation
 * @method     ChildCrudViewButtonQuery innerJoinCrudView($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudView relation
 *
 * @method     ChildCrudViewButtonQuery joinWithCrudView($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudView relation
 *
 * @method     ChildCrudViewButtonQuery leftJoinWithCrudView() Adds a LEFT JOIN clause and with to the query using the CrudView relation
 * @method     ChildCrudViewButtonQuery rightJoinWithCrudView() Adds a RIGHT JOIN clause and with to the query using the CrudView relation
 * @method     ChildCrudViewButtonQuery innerJoinWithCrudView() Adds a INNER JOIN clause and with to the query using the CrudView relation
 *
 * @method     ChildCrudViewButtonQuery leftJoinCrudViewButtonEvent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudViewButtonEvent relation
 * @method     ChildCrudViewButtonQuery rightJoinCrudViewButtonEvent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudViewButtonEvent relation
 * @method     ChildCrudViewButtonQuery innerJoinCrudViewButtonEvent($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudViewButtonEvent relation
 *
 * @method     ChildCrudViewButtonQuery joinWithCrudViewButtonEvent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudViewButtonEvent relation
 *
 * @method     ChildCrudViewButtonQuery leftJoinWithCrudViewButtonEvent() Adds a LEFT JOIN clause and with to the query using the CrudViewButtonEvent relation
 * @method     ChildCrudViewButtonQuery rightJoinWithCrudViewButtonEvent() Adds a RIGHT JOIN clause and with to the query using the CrudViewButtonEvent relation
 * @method     ChildCrudViewButtonQuery innerJoinWithCrudViewButtonEvent() Adds a INNER JOIN clause and with to the query using the CrudViewButtonEvent relation
 *
 * @method     \Model\Setting\CrudManager\CrudViewQuery|\Model\Setting\CrudManager\CrudViewButtonEventQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCrudViewButton findOne(ConnectionInterface $con = null) Return the first ChildCrudViewButton matching the query
 * @method     ChildCrudViewButton findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCrudViewButton matching the query, or a new ChildCrudViewButton object populated from the query conditions when no match is found
 *
 * @method     ChildCrudViewButton findOneById(int $id) Return the first ChildCrudViewButton filtered by the id column
 * @method     ChildCrudViewButton findOneByCrudViewId(int $crud_view_id) Return the first ChildCrudViewButton filtered by the crud_view_id column
 * @method     ChildCrudViewButton findOneByTitle(string $title) Return the first ChildCrudViewButton filtered by the title column
 * @method     ChildCrudViewButton findOneByIconBeforeClick(string $icon_before_click) Return the first ChildCrudViewButton filtered by the icon_before_click column
 * @method     ChildCrudViewButton findOneByIconAfterClick(string $icon_after_click) Return the first ChildCrudViewButton filtered by the icon_after_click column
 * @method     ChildCrudViewButton findOneByColorBeforeClick(string $color_before_click) Return the first ChildCrudViewButton filtered by the color_before_click column
 * @method     ChildCrudViewButton findOneByColorAfterClick(string $color_after_click) Return the first ChildCrudViewButton filtered by the color_after_click column
 * @method     ChildCrudViewButton findOneBySorting(int $sorting) Return the first ChildCrudViewButton filtered by the sorting column
 * @method     ChildCrudViewButton findOneByCreatedAt(string $created_at) Return the first ChildCrudViewButton filtered by the created_at column
 * @method     ChildCrudViewButton findOneByUpdatedAt(string $updated_at) Return the first ChildCrudViewButton filtered by the updated_at column *

 * @method     ChildCrudViewButton requirePk($key, ConnectionInterface $con = null) Return the ChildCrudViewButton by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewButton requireOne(ConnectionInterface $con = null) Return the first ChildCrudViewButton matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudViewButton requireOneById(int $id) Return the first ChildCrudViewButton filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewButton requireOneByCrudViewId(int $crud_view_id) Return the first ChildCrudViewButton filtered by the crud_view_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewButton requireOneByTitle(string $title) Return the first ChildCrudViewButton filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewButton requireOneByIconBeforeClick(string $icon_before_click) Return the first ChildCrudViewButton filtered by the icon_before_click column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewButton requireOneByIconAfterClick(string $icon_after_click) Return the first ChildCrudViewButton filtered by the icon_after_click column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewButton requireOneByColorBeforeClick(string $color_before_click) Return the first ChildCrudViewButton filtered by the color_before_click column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewButton requireOneByColorAfterClick(string $color_after_click) Return the first ChildCrudViewButton filtered by the color_after_click column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewButton requireOneBySorting(int $sorting) Return the first ChildCrudViewButton filtered by the sorting column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewButton requireOneByCreatedAt(string $created_at) Return the first ChildCrudViewButton filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewButton requireOneByUpdatedAt(string $updated_at) Return the first ChildCrudViewButton filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudViewButton[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCrudViewButton objects based on current ModelCriteria
 * @method     ChildCrudViewButton[]|ObjectCollection findById(int $id) Return ChildCrudViewButton objects filtered by the id column
 * @method     ChildCrudViewButton[]|ObjectCollection findByCrudViewId(int $crud_view_id) Return ChildCrudViewButton objects filtered by the crud_view_id column
 * @method     ChildCrudViewButton[]|ObjectCollection findByTitle(string $title) Return ChildCrudViewButton objects filtered by the title column
 * @method     ChildCrudViewButton[]|ObjectCollection findByIconBeforeClick(string $icon_before_click) Return ChildCrudViewButton objects filtered by the icon_before_click column
 * @method     ChildCrudViewButton[]|ObjectCollection findByIconAfterClick(string $icon_after_click) Return ChildCrudViewButton objects filtered by the icon_after_click column
 * @method     ChildCrudViewButton[]|ObjectCollection findByColorBeforeClick(string $color_before_click) Return ChildCrudViewButton objects filtered by the color_before_click column
 * @method     ChildCrudViewButton[]|ObjectCollection findByColorAfterClick(string $color_after_click) Return ChildCrudViewButton objects filtered by the color_after_click column
 * @method     ChildCrudViewButton[]|ObjectCollection findBySorting(int $sorting) Return ChildCrudViewButton objects filtered by the sorting column
 * @method     ChildCrudViewButton[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCrudViewButton objects filtered by the created_at column
 * @method     ChildCrudViewButton[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCrudViewButton objects filtered by the updated_at column
 * @method     ChildCrudViewButton[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CrudViewButtonQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\CrudManager\Base\CrudViewButtonQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\CrudManager\\CrudViewButton', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCrudViewButtonQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCrudViewButtonQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCrudViewButtonQuery) {
            return $criteria;
        }
        $query = new ChildCrudViewButtonQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCrudViewButton|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CrudViewButtonTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CrudViewButtonTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudViewButton A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, crud_view_id, title, icon_before_click, icon_after_click, color_before_click, color_after_click, sorting, created_at, updated_at FROM crud_view_button WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCrudViewButton $obj */
            $obj = new ChildCrudViewButton();
            $obj->hydrate($row);
            CrudViewButtonTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCrudViewButton|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CrudViewButtonTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CrudViewButtonTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CrudViewButtonTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CrudViewButtonTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewButtonTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the crud_view_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCrudViewId(1234); // WHERE crud_view_id = 1234
     * $query->filterByCrudViewId(array(12, 34)); // WHERE crud_view_id IN (12, 34)
     * $query->filterByCrudViewId(array('min' => 12)); // WHERE crud_view_id > 12
     * </code>
     *
     * @see       filterByCrudView()
     *
     * @param     mixed $crudViewId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterByCrudViewId($crudViewId = null, $comparison = null)
    {
        if (is_array($crudViewId)) {
            $useMinMax = false;
            if (isset($crudViewId['min'])) {
                $this->addUsingAlias(CrudViewButtonTableMap::COL_CRUD_VIEW_ID, $crudViewId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($crudViewId['max'])) {
                $this->addUsingAlias(CrudViewButtonTableMap::COL_CRUD_VIEW_ID, $crudViewId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewButtonTableMap::COL_CRUD_VIEW_ID, $crudViewId, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewButtonTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the icon_before_click column
     *
     * Example usage:
     * <code>
     * $query->filterByIconBeforeClick('fooValue');   // WHERE icon_before_click = 'fooValue'
     * $query->filterByIconBeforeClick('%fooValue%', Criteria::LIKE); // WHERE icon_before_click LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iconBeforeClick The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterByIconBeforeClick($iconBeforeClick = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iconBeforeClick)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewButtonTableMap::COL_ICON_BEFORE_CLICK, $iconBeforeClick, $comparison);
    }

    /**
     * Filter the query on the icon_after_click column
     *
     * Example usage:
     * <code>
     * $query->filterByIconAfterClick('fooValue');   // WHERE icon_after_click = 'fooValue'
     * $query->filterByIconAfterClick('%fooValue%', Criteria::LIKE); // WHERE icon_after_click LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iconAfterClick The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterByIconAfterClick($iconAfterClick = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iconAfterClick)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewButtonTableMap::COL_ICON_AFTER_CLICK, $iconAfterClick, $comparison);
    }

    /**
     * Filter the query on the color_before_click column
     *
     * Example usage:
     * <code>
     * $query->filterByColorBeforeClick('fooValue');   // WHERE color_before_click = 'fooValue'
     * $query->filterByColorBeforeClick('%fooValue%', Criteria::LIKE); // WHERE color_before_click LIKE '%fooValue%'
     * </code>
     *
     * @param     string $colorBeforeClick The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterByColorBeforeClick($colorBeforeClick = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($colorBeforeClick)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewButtonTableMap::COL_COLOR_BEFORE_CLICK, $colorBeforeClick, $comparison);
    }

    /**
     * Filter the query on the color_after_click column
     *
     * Example usage:
     * <code>
     * $query->filterByColorAfterClick('fooValue');   // WHERE color_after_click = 'fooValue'
     * $query->filterByColorAfterClick('%fooValue%', Criteria::LIKE); // WHERE color_after_click LIKE '%fooValue%'
     * </code>
     *
     * @param     string $colorAfterClick The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterByColorAfterClick($colorAfterClick = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($colorAfterClick)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewButtonTableMap::COL_COLOR_AFTER_CLICK, $colorAfterClick, $comparison);
    }

    /**
     * Filter the query on the sorting column
     *
     * Example usage:
     * <code>
     * $query->filterBySorting(1234); // WHERE sorting = 1234
     * $query->filterBySorting(array(12, 34)); // WHERE sorting IN (12, 34)
     * $query->filterBySorting(array('min' => 12)); // WHERE sorting > 12
     * </code>
     *
     * @param     mixed $sorting The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterBySorting($sorting = null, $comparison = null)
    {
        if (is_array($sorting)) {
            $useMinMax = false;
            if (isset($sorting['min'])) {
                $this->addUsingAlias(CrudViewButtonTableMap::COL_SORTING, $sorting['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sorting['max'])) {
                $this->addUsingAlias(CrudViewButtonTableMap::COL_SORTING, $sorting['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewButtonTableMap::COL_SORTING, $sorting, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CrudViewButtonTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CrudViewButtonTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewButtonTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CrudViewButtonTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CrudViewButtonTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewButtonTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudView object
     *
     * @param \Model\Setting\CrudManager\CrudView|ObjectCollection $crudView The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterByCrudView($crudView, $comparison = null)
    {
        if ($crudView instanceof \Model\Setting\CrudManager\CrudView) {
            return $this
                ->addUsingAlias(CrudViewButtonTableMap::COL_CRUD_VIEW_ID, $crudView->getId(), $comparison);
        } elseif ($crudView instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CrudViewButtonTableMap::COL_CRUD_VIEW_ID, $crudView->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCrudView() only accepts arguments of type \Model\Setting\CrudManager\CrudView or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudView relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function joinCrudView($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudView');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudView');
        }

        return $this;
    }

    /**
     * Use the CrudView relation CrudView object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudViewQuery A secondary query class using the current class as primary query
     */
    public function useCrudViewQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudView($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudView', '\Model\Setting\CrudManager\CrudViewQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudViewButtonEvent object
     *
     * @param \Model\Setting\CrudManager\CrudViewButtonEvent|ObjectCollection $crudViewButtonEvent the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function filterByCrudViewButtonEvent($crudViewButtonEvent, $comparison = null)
    {
        if ($crudViewButtonEvent instanceof \Model\Setting\CrudManager\CrudViewButtonEvent) {
            return $this
                ->addUsingAlias(CrudViewButtonTableMap::COL_ID, $crudViewButtonEvent->getCrudViewButtonId(), $comparison);
        } elseif ($crudViewButtonEvent instanceof ObjectCollection) {
            return $this
                ->useCrudViewButtonEventQuery()
                ->filterByPrimaryKeys($crudViewButtonEvent->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudViewButtonEvent() only accepts arguments of type \Model\Setting\CrudManager\CrudViewButtonEvent or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudViewButtonEvent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function joinCrudViewButtonEvent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudViewButtonEvent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudViewButtonEvent');
        }

        return $this;
    }

    /**
     * Use the CrudViewButtonEvent relation CrudViewButtonEvent object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudViewButtonEventQuery A secondary query class using the current class as primary query
     */
    public function useCrudViewButtonEventQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCrudViewButtonEvent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudViewButtonEvent', '\Model\Setting\CrudManager\CrudViewButtonEventQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCrudViewButton $crudViewButton Object to remove from the list of results
     *
     * @return $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function prune($crudViewButton = null)
    {
        if ($crudViewButton) {
            $this->addUsingAlias(CrudViewButtonTableMap::COL_ID, $crudViewButton->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the crud_view_button table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewButtonTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CrudViewButtonTableMap::clearInstancePool();
            CrudViewButtonTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewButtonTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CrudViewButtonTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CrudViewButtonTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CrudViewButtonTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudViewButtonTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudViewButtonTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudViewButtonTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudViewButtonTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudViewButtonTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCrudViewButtonQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudViewButtonTableMap::COL_CREATED_AT);
    }

} // CrudViewButtonQuery
