<?php

namespace Model\Setting\CrudManager\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Setting\CrudManager\CrudConfig as ChildCrudConfig;
use Model\Setting\CrudManager\CrudConfigQuery as ChildCrudConfigQuery;
use Model\Setting\CrudManager\CrudView as ChildCrudView;
use Model\Setting\CrudManager\CrudViewButton as ChildCrudViewButton;
use Model\Setting\CrudManager\CrudViewButtonQuery as ChildCrudViewButtonQuery;
use Model\Setting\CrudManager\CrudViewField as ChildCrudViewField;
use Model\Setting\CrudManager\CrudViewFieldQuery as ChildCrudViewFieldQuery;
use Model\Setting\CrudManager\CrudViewHiddenFilter as ChildCrudViewHiddenFilter;
use Model\Setting\CrudManager\CrudViewHiddenFilterQuery as ChildCrudViewHiddenFilterQuery;
use Model\Setting\CrudManager\CrudViewQuery as ChildCrudViewQuery;
use Model\Setting\CrudManager\CrudViewVisibleFilter as ChildCrudViewVisibleFilter;
use Model\Setting\CrudManager\CrudViewVisibleFilterQuery as ChildCrudViewVisibleFilterQuery;
use Model\Setting\CrudManager\Map\CrudViewButtonTableMap;
use Model\Setting\CrudManager\Map\CrudViewFieldTableMap;
use Model\Setting\CrudManager\Map\CrudViewHiddenFilterTableMap;
use Model\Setting\CrudManager\Map\CrudViewTableMap;
use Model\Setting\CrudManager\Map\CrudViewVisibleFilterTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'crud_view' table.
 *
 *
 *
 * @package    propel.generator.Model.Setting.CrudManager.Base
 */
abstract class CrudView implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Setting\\CrudManager\\Map\\CrudViewTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the crud_config_id field.
     *
     * @var        int
     */
    protected $crud_config_id;

    /**
     * The value for the name field.
     *
     * @var        string|null
     */
    protected $name;

    /**
     * The value for the code field.
     *
     * @var        string|null
     */
    protected $code;

    /**
     * The value for the sorting field.
     *
     * @var        int
     */
    protected $sorting;

    /**
     * The value for the is_hidden field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_hidden;

    /**
     * The value for the show_quantity field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $show_quantity;

    /**
     * The value for the default_order_by field.
     *
     * Note: this column has a database default value of: 'id'
     * @var        string
     */
    protected $default_order_by;

    /**
     * The value for the default_order_dir field.
     *
     * Note: this column has a database default value of: 'asc'
     * @var        string
     */
    protected $default_order_dir;

    /**
     * The value for the tab_color_logic field.
     *
     * @var        string|null
     */
    protected $tab_color_logic;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        ChildCrudConfig
     */
    protected $aCrudConfig;

    /**
     * @var        ObjectCollection|ChildCrudViewField[] Collection to store aggregation of ChildCrudViewField objects.
     */
    protected $collCrudViews;
    protected $collCrudViewsPartial;

    /**
     * @var        ObjectCollection|ChildCrudViewButton[] Collection to store aggregation of ChildCrudViewButton objects.
     */
    protected $collCrudViewButtons;
    protected $collCrudViewButtonsPartial;

    /**
     * @var        ObjectCollection|ChildCrudViewVisibleFilter[] Collection to store aggregation of ChildCrudViewVisibleFilter objects.
     */
    protected $collCrudViewVisibleFilters;
    protected $collCrudViewVisibleFiltersPartial;

    /**
     * @var        ObjectCollection|ChildCrudViewHiddenFilter[] Collection to store aggregation of ChildCrudViewHiddenFilter objects.
     */
    protected $collCrudViewHiddenFilters;
    protected $collCrudViewHiddenFiltersPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCrudViewField[]
     */
    protected $crudViewsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCrudViewButton[]
     */
    protected $crudViewButtonsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCrudViewVisibleFilter[]
     */
    protected $crudViewVisibleFiltersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCrudViewHiddenFilter[]
     */
    protected $crudViewHiddenFiltersScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_hidden = false;
        $this->show_quantity = false;
        $this->default_order_by = 'id';
        $this->default_order_dir = 'asc';
    }

    /**
     * Initializes internal state of Model\Setting\CrudManager\Base\CrudView object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>CrudView</code> instance.  If
     * <code>obj</code> is an instance of <code>CrudView</code>, delegates to
     * <code>equals(CrudView)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [crud_config_id] column value.
     *
     * @return int
     */
    public function getCrudConfigId()
    {
        return $this->crud_config_id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [code] column value.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the [sorting] column value.
     *
     * @return int
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Get the [is_hidden] column value.
     *
     * @return boolean
     */
    public function getIsHidden()
    {
        return $this->is_hidden;
    }

    /**
     * Get the [is_hidden] column value.
     *
     * @return boolean
     */
    public function isHidden()
    {
        return $this->getIsHidden();
    }

    /**
     * Get the [show_quantity] column value.
     *
     * @return boolean
     */
    public function getShowQuantity()
    {
        return $this->show_quantity;
    }

    /**
     * Get the [show_quantity] column value.
     *
     * @return boolean
     */
    public function isShowQuantity()
    {
        return $this->getShowQuantity();
    }

    /**
     * Get the [default_order_by] column value.
     *
     * @return string
     */
    public function getDefaultOrderBy()
    {
        return $this->default_order_by;
    }

    /**
     * Get the [default_order_dir] column value.
     *
     * @return string
     */
    public function getDefaultOrderDir()
    {
        return $this->default_order_dir;
    }

    /**
     * Get the [tab_color_logic] column value.
     *
     * @return string|null
     */
    public function getTabColorLogic()
    {
        return $this->tab_color_logic;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[CrudViewTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [crud_config_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function setCrudConfigId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->crud_config_id !== $v) {
            $this->crud_config_id = $v;
            $this->modifiedColumns[CrudViewTableMap::COL_CRUD_CONFIG_ID] = true;
        }

        if ($this->aCrudConfig !== null && $this->aCrudConfig->getId() !== $v) {
            $this->aCrudConfig = null;
        }

        return $this;
    } // setCrudConfigId()

    /**
     * Set the value of [name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[CrudViewTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [code] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[CrudViewTableMap::COL_CODE] = true;
        }

        return $this;
    } // setCode()

    /**
     * Set the value of [sorting] column.
     *
     * @param int $v New value
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function setSorting($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sorting !== $v) {
            $this->sorting = $v;
            $this->modifiedColumns[CrudViewTableMap::COL_SORTING] = true;
        }

        return $this;
    } // setSorting()

    /**
     * Sets the value of the [is_hidden] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function setIsHidden($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_hidden !== $v) {
            $this->is_hidden = $v;
            $this->modifiedColumns[CrudViewTableMap::COL_IS_HIDDEN] = true;
        }

        return $this;
    } // setIsHidden()

    /**
     * Sets the value of the [show_quantity] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function setShowQuantity($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->show_quantity !== $v) {
            $this->show_quantity = $v;
            $this->modifiedColumns[CrudViewTableMap::COL_SHOW_QUANTITY] = true;
        }

        return $this;
    } // setShowQuantity()

    /**
     * Set the value of [default_order_by] column.
     *
     * @param string $v New value
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function setDefaultOrderBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->default_order_by !== $v) {
            $this->default_order_by = $v;
            $this->modifiedColumns[CrudViewTableMap::COL_DEFAULT_ORDER_BY] = true;
        }

        return $this;
    } // setDefaultOrderBy()

    /**
     * Set the value of [default_order_dir] column.
     *
     * @param string $v New value
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function setDefaultOrderDir($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->default_order_dir !== $v) {
            $this->default_order_dir = $v;
            $this->modifiedColumns[CrudViewTableMap::COL_DEFAULT_ORDER_DIR] = true;
        }

        return $this;
    } // setDefaultOrderDir()

    /**
     * Set the value of [tab_color_logic] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function setTabColorLogic($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tab_color_logic !== $v) {
            $this->tab_color_logic = $v;
            $this->modifiedColumns[CrudViewTableMap::COL_TAB_COLOR_LOGIC] = true;
        }

        return $this;
    } // setTabColorLogic()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CrudViewTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CrudViewTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_hidden !== false) {
                return false;
            }

            if ($this->show_quantity !== false) {
                return false;
            }

            if ($this->default_order_by !== 'id') {
                return false;
            }

            if ($this->default_order_dir !== 'asc') {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CrudViewTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CrudViewTableMap::translateFieldName('CrudConfigId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->crud_config_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CrudViewTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CrudViewTableMap::translateFieldName('Code', TableMap::TYPE_PHPNAME, $indexType)];
            $this->code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CrudViewTableMap::translateFieldName('Sorting', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sorting = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CrudViewTableMap::translateFieldName('IsHidden', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_hidden = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CrudViewTableMap::translateFieldName('ShowQuantity', TableMap::TYPE_PHPNAME, $indexType)];
            $this->show_quantity = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CrudViewTableMap::translateFieldName('DefaultOrderBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->default_order_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CrudViewTableMap::translateFieldName('DefaultOrderDir', TableMap::TYPE_PHPNAME, $indexType)];
            $this->default_order_dir = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CrudViewTableMap::translateFieldName('TabColorLogic', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tab_color_logic = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : CrudViewTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : CrudViewTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 12; // 12 = CrudViewTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Setting\\CrudManager\\CrudView'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCrudConfig !== null && $this->crud_config_id !== $this->aCrudConfig->getId()) {
            $this->aCrudConfig = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CrudViewTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCrudViewQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCrudConfig = null;
            $this->collCrudViews = null;

            $this->collCrudViewButtons = null;

            $this->collCrudViewVisibleFilters = null;

            $this->collCrudViewHiddenFilters = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see CrudView::setDeleted()
     * @see CrudView::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCrudViewQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(CrudViewTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(CrudViewTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(CrudViewTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CrudViewTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCrudConfig !== null) {
                if ($this->aCrudConfig->isModified() || $this->aCrudConfig->isNew()) {
                    $affectedRows += $this->aCrudConfig->save($con);
                }
                $this->setCrudConfig($this->aCrudConfig);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->crudViewsScheduledForDeletion !== null) {
                if (!$this->crudViewsScheduledForDeletion->isEmpty()) {
                    \Model\Setting\CrudManager\CrudViewFieldQuery::create()
                        ->filterByPrimaryKeys($this->crudViewsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->crudViewsScheduledForDeletion = null;
                }
            }

            if ($this->collCrudViews !== null) {
                foreach ($this->collCrudViews as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->crudViewButtonsScheduledForDeletion !== null) {
                if (!$this->crudViewButtonsScheduledForDeletion->isEmpty()) {
                    \Model\Setting\CrudManager\CrudViewButtonQuery::create()
                        ->filterByPrimaryKeys($this->crudViewButtonsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->crudViewButtonsScheduledForDeletion = null;
                }
            }

            if ($this->collCrudViewButtons !== null) {
                foreach ($this->collCrudViewButtons as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->crudViewVisibleFiltersScheduledForDeletion !== null) {
                if (!$this->crudViewVisibleFiltersScheduledForDeletion->isEmpty()) {
                    \Model\Setting\CrudManager\CrudViewVisibleFilterQuery::create()
                        ->filterByPrimaryKeys($this->crudViewVisibleFiltersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->crudViewVisibleFiltersScheduledForDeletion = null;
                }
            }

            if ($this->collCrudViewVisibleFilters !== null) {
                foreach ($this->collCrudViewVisibleFilters as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->crudViewHiddenFiltersScheduledForDeletion !== null) {
                if (!$this->crudViewHiddenFiltersScheduledForDeletion->isEmpty()) {
                    \Model\Setting\CrudManager\CrudViewHiddenFilterQuery::create()
                        ->filterByPrimaryKeys($this->crudViewHiddenFiltersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->crudViewHiddenFiltersScheduledForDeletion = null;
                }
            }

            if ($this->collCrudViewHiddenFilters !== null) {
                foreach ($this->collCrudViewHiddenFilters as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CrudViewTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CrudViewTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CrudViewTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_CRUD_CONFIG_ID)) {
            $modifiedColumns[':p' . $index++]  = 'crud_config_id';
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_CODE)) {
            $modifiedColumns[':p' . $index++]  = 'code';
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_SORTING)) {
            $modifiedColumns[':p' . $index++]  = 'sorting';
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_IS_HIDDEN)) {
            $modifiedColumns[':p' . $index++]  = 'is_hidden';
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_SHOW_QUANTITY)) {
            $modifiedColumns[':p' . $index++]  = 'show_quantity';
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_DEFAULT_ORDER_BY)) {
            $modifiedColumns[':p' . $index++]  = 'default_order_by';
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_DEFAULT_ORDER_DIR)) {
            $modifiedColumns[':p' . $index++]  = 'default_order_dir';
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_TAB_COLOR_LOGIC)) {
            $modifiedColumns[':p' . $index++]  = 'tab_color_logic';
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO crud_view (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'crud_config_id':
                        $stmt->bindValue($identifier, $this->crud_config_id, PDO::PARAM_INT);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'code':
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case 'sorting':
                        $stmt->bindValue($identifier, $this->sorting, PDO::PARAM_INT);
                        break;
                    case 'is_hidden':
                        $stmt->bindValue($identifier, (int) $this->is_hidden, PDO::PARAM_INT);
                        break;
                    case 'show_quantity':
                        $stmt->bindValue($identifier, (int) $this->show_quantity, PDO::PARAM_INT);
                        break;
                    case 'default_order_by':
                        $stmt->bindValue($identifier, $this->default_order_by, PDO::PARAM_STR);
                        break;
                    case 'default_order_dir':
                        $stmt->bindValue($identifier, $this->default_order_dir, PDO::PARAM_STR);
                        break;
                    case 'tab_color_logic':
                        $stmt->bindValue($identifier, $this->tab_color_logic, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CrudViewTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getCrudConfigId();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getCode();
                break;
            case 4:
                return $this->getSorting();
                break;
            case 5:
                return $this->getIsHidden();
                break;
            case 6:
                return $this->getShowQuantity();
                break;
            case 7:
                return $this->getDefaultOrderBy();
                break;
            case 8:
                return $this->getDefaultOrderDir();
                break;
            case 9:
                return $this->getTabColorLogic();
                break;
            case 10:
                return $this->getCreatedAt();
                break;
            case 11:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['CrudView'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CrudView'][$this->hashCode()] = true;
        $keys = CrudViewTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getCrudConfigId(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getCode(),
            $keys[4] => $this->getSorting(),
            $keys[5] => $this->getIsHidden(),
            $keys[6] => $this->getShowQuantity(),
            $keys[7] => $this->getDefaultOrderBy(),
            $keys[8] => $this->getDefaultOrderDir(),
            $keys[9] => $this->getTabColorLogic(),
            $keys[10] => $this->getCreatedAt(),
            $keys[11] => $this->getUpdatedAt(),
        );
        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCrudConfig) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'crudConfig';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'crud_config';
                        break;
                    default:
                        $key = 'CrudConfig';
                }

                $result[$key] = $this->aCrudConfig->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCrudViews) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'crudViewFields';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'crud_view_fields';
                        break;
                    default:
                        $key = 'CrudViews';
                }

                $result[$key] = $this->collCrudViews->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCrudViewButtons) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'crudViewButtons';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'crud_view_buttons';
                        break;
                    default:
                        $key = 'CrudViewButtons';
                }

                $result[$key] = $this->collCrudViewButtons->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCrudViewVisibleFilters) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'crudViewVisibleFilters';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'crud_view_visible_filters';
                        break;
                    default:
                        $key = 'CrudViewVisibleFilters';
                }

                $result[$key] = $this->collCrudViewVisibleFilters->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCrudViewHiddenFilters) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'crudViewHiddenFilters';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'crud_view_hidden_filters';
                        break;
                    default:
                        $key = 'CrudViewHiddenFilters';
                }

                $result[$key] = $this->collCrudViewHiddenFilters->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Setting\CrudManager\CrudView
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CrudViewTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Setting\CrudManager\CrudView
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setCrudConfigId($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setCode($value);
                break;
            case 4:
                $this->setSorting($value);
                break;
            case 5:
                $this->setIsHidden($value);
                break;
            case 6:
                $this->setShowQuantity($value);
                break;
            case 7:
                $this->setDefaultOrderBy($value);
                break;
            case 8:
                $this->setDefaultOrderDir($value);
                break;
            case 9:
                $this->setTabColorLogic($value);
                break;
            case 10:
                $this->setCreatedAt($value);
                break;
            case 11:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CrudViewTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setCrudConfigId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setName($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setCode($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSorting($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setIsHidden($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setShowQuantity($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDefaultOrderBy($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setDefaultOrderDir($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setTabColorLogic($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCreatedAt($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUpdatedAt($arr[$keys[11]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Setting\CrudManager\CrudView The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CrudViewTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CrudViewTableMap::COL_ID)) {
            $criteria->add(CrudViewTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_CRUD_CONFIG_ID)) {
            $criteria->add(CrudViewTableMap::COL_CRUD_CONFIG_ID, $this->crud_config_id);
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_NAME)) {
            $criteria->add(CrudViewTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_CODE)) {
            $criteria->add(CrudViewTableMap::COL_CODE, $this->code);
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_SORTING)) {
            $criteria->add(CrudViewTableMap::COL_SORTING, $this->sorting);
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_IS_HIDDEN)) {
            $criteria->add(CrudViewTableMap::COL_IS_HIDDEN, $this->is_hidden);
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_SHOW_QUANTITY)) {
            $criteria->add(CrudViewTableMap::COL_SHOW_QUANTITY, $this->show_quantity);
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_DEFAULT_ORDER_BY)) {
            $criteria->add(CrudViewTableMap::COL_DEFAULT_ORDER_BY, $this->default_order_by);
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_DEFAULT_ORDER_DIR)) {
            $criteria->add(CrudViewTableMap::COL_DEFAULT_ORDER_DIR, $this->default_order_dir);
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_TAB_COLOR_LOGIC)) {
            $criteria->add(CrudViewTableMap::COL_TAB_COLOR_LOGIC, $this->tab_color_logic);
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_CREATED_AT)) {
            $criteria->add(CrudViewTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(CrudViewTableMap::COL_UPDATED_AT)) {
            $criteria->add(CrudViewTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCrudViewQuery::create();
        $criteria->add(CrudViewTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Setting\CrudManager\CrudView (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setCrudConfigId($this->getCrudConfigId());
        $copyObj->setName($this->getName());
        $copyObj->setCode($this->getCode());
        $copyObj->setSorting($this->getSorting());
        $copyObj->setIsHidden($this->getIsHidden());
        $copyObj->setShowQuantity($this->getShowQuantity());
        $copyObj->setDefaultOrderBy($this->getDefaultOrderBy());
        $copyObj->setDefaultOrderDir($this->getDefaultOrderDir());
        $copyObj->setTabColorLogic($this->getTabColorLogic());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCrudViews() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCrudView($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCrudViewButtons() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCrudViewButton($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCrudViewVisibleFilters() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCrudViewVisibleFilter($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCrudViewHiddenFilters() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCrudViewHiddenFilter($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Setting\CrudManager\CrudView Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildCrudConfig object.
     *
     * @param  ChildCrudConfig $v
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCrudConfig(ChildCrudConfig $v = null)
    {
        if ($v === null) {
            $this->setCrudConfigId(NULL);
        } else {
            $this->setCrudConfigId($v->getId());
        }

        $this->aCrudConfig = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCrudConfig object, it will not be re-added.
        if ($v !== null) {
            $v->addCrudView($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCrudConfig object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCrudConfig The associated ChildCrudConfig object.
     * @throws PropelException
     */
    public function getCrudConfig(ConnectionInterface $con = null)
    {
        if ($this->aCrudConfig === null && ($this->crud_config_id != 0)) {
            $this->aCrudConfig = ChildCrudConfigQuery::create()->findPk($this->crud_config_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCrudConfig->addCrudViews($this);
             */
        }

        return $this->aCrudConfig;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CrudView' === $relationName) {
            $this->initCrudViews();
            return;
        }
        if ('CrudViewButton' === $relationName) {
            $this->initCrudViewButtons();
            return;
        }
        if ('CrudViewVisibleFilter' === $relationName) {
            $this->initCrudViewVisibleFilters();
            return;
        }
        if ('CrudViewHiddenFilter' === $relationName) {
            $this->initCrudViewHiddenFilters();
            return;
        }
    }

    /**
     * Clears out the collCrudViews collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCrudViews()
     */
    public function clearCrudViews()
    {
        $this->collCrudViews = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCrudViews collection loaded partially.
     */
    public function resetPartialCrudViews($v = true)
    {
        $this->collCrudViewsPartial = $v;
    }

    /**
     * Initializes the collCrudViews collection.
     *
     * By default this just sets the collCrudViews collection to an empty array (like clearcollCrudViews());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCrudViews($overrideExisting = true)
    {
        if (null !== $this->collCrudViews && !$overrideExisting) {
            return;
        }

        $collectionClassName = CrudViewFieldTableMap::getTableMap()->getCollectionClassName();

        $this->collCrudViews = new $collectionClassName;
        $this->collCrudViews->setModel('\Model\Setting\CrudManager\CrudViewField');
    }

    /**
     * Gets an array of ChildCrudViewField objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCrudView is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCrudViewField[] List of ChildCrudViewField objects
     * @throws PropelException
     */
    public function getCrudViews(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudViewsPartial && !$this->isNew();
        if (null === $this->collCrudViews || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCrudViews) {
                    $this->initCrudViews();
                } else {
                    $collectionClassName = CrudViewFieldTableMap::getTableMap()->getCollectionClassName();

                    $collCrudViews = new $collectionClassName;
                    $collCrudViews->setModel('\Model\Setting\CrudManager\CrudViewField');

                    return $collCrudViews;
                }
            } else {
                $collCrudViews = ChildCrudViewFieldQuery::create(null, $criteria)
                    ->filterByCrudView($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCrudViewsPartial && count($collCrudViews)) {
                        $this->initCrudViews(false);

                        foreach ($collCrudViews as $obj) {
                            if (false == $this->collCrudViews->contains($obj)) {
                                $this->collCrudViews->append($obj);
                            }
                        }

                        $this->collCrudViewsPartial = true;
                    }

                    return $collCrudViews;
                }

                if ($partial && $this->collCrudViews) {
                    foreach ($this->collCrudViews as $obj) {
                        if ($obj->isNew()) {
                            $collCrudViews[] = $obj;
                        }
                    }
                }

                $this->collCrudViews = $collCrudViews;
                $this->collCrudViewsPartial = false;
            }
        }

        return $this->collCrudViews;
    }

    /**
     * Sets a collection of ChildCrudViewField objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $crudViews A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCrudView The current object (for fluent API support)
     */
    public function setCrudViews(Collection $crudViews, ConnectionInterface $con = null)
    {
        /** @var ChildCrudViewField[] $crudViewsToDelete */
        $crudViewsToDelete = $this->getCrudViews(new Criteria(), $con)->diff($crudViews);


        $this->crudViewsScheduledForDeletion = $crudViewsToDelete;

        foreach ($crudViewsToDelete as $crudViewRemoved) {
            $crudViewRemoved->setCrudView(null);
        }

        $this->collCrudViews = null;
        foreach ($crudViews as $crudView) {
            $this->addCrudView($crudView);
        }

        $this->collCrudViews = $crudViews;
        $this->collCrudViewsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CrudViewField objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CrudViewField objects.
     * @throws PropelException
     */
    public function countCrudViews(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudViewsPartial && !$this->isNew();
        if (null === $this->collCrudViews || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCrudViews) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCrudViews());
            }

            $query = ChildCrudViewFieldQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCrudView($this)
                ->count($con);
        }

        return count($this->collCrudViews);
    }

    /**
     * Method called to associate a ChildCrudViewField object to this object
     * through the ChildCrudViewField foreign key attribute.
     *
     * @param  ChildCrudViewField $l ChildCrudViewField
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function addCrudView(ChildCrudViewField $l)
    {
        if ($this->collCrudViews === null) {
            $this->initCrudViews();
            $this->collCrudViewsPartial = true;
        }

        if (!$this->collCrudViews->contains($l)) {
            $this->doAddCrudView($l);

            if ($this->crudViewsScheduledForDeletion and $this->crudViewsScheduledForDeletion->contains($l)) {
                $this->crudViewsScheduledForDeletion->remove($this->crudViewsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCrudViewField $crudView The ChildCrudViewField object to add.
     */
    protected function doAddCrudView(ChildCrudViewField $crudView)
    {
        $this->collCrudViews[]= $crudView;
        $crudView->setCrudView($this);
    }

    /**
     * @param  ChildCrudViewField $crudView The ChildCrudViewField object to remove.
     * @return $this|ChildCrudView The current object (for fluent API support)
     */
    public function removeCrudView(ChildCrudViewField $crudView)
    {
        if ($this->getCrudViews()->contains($crudView)) {
            $pos = $this->collCrudViews->search($crudView);
            $this->collCrudViews->remove($pos);
            if (null === $this->crudViewsScheduledForDeletion) {
                $this->crudViewsScheduledForDeletion = clone $this->collCrudViews;
                $this->crudViewsScheduledForDeletion->clear();
            }
            $this->crudViewsScheduledForDeletion[]= clone $crudView;
            $crudView->setCrudView(null);
        }

        return $this;
    }

    /**
     * Clears out the collCrudViewButtons collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCrudViewButtons()
     */
    public function clearCrudViewButtons()
    {
        $this->collCrudViewButtons = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCrudViewButtons collection loaded partially.
     */
    public function resetPartialCrudViewButtons($v = true)
    {
        $this->collCrudViewButtonsPartial = $v;
    }

    /**
     * Initializes the collCrudViewButtons collection.
     *
     * By default this just sets the collCrudViewButtons collection to an empty array (like clearcollCrudViewButtons());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCrudViewButtons($overrideExisting = true)
    {
        if (null !== $this->collCrudViewButtons && !$overrideExisting) {
            return;
        }

        $collectionClassName = CrudViewButtonTableMap::getTableMap()->getCollectionClassName();

        $this->collCrudViewButtons = new $collectionClassName;
        $this->collCrudViewButtons->setModel('\Model\Setting\CrudManager\CrudViewButton');
    }

    /**
     * Gets an array of ChildCrudViewButton objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCrudView is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCrudViewButton[] List of ChildCrudViewButton objects
     * @throws PropelException
     */
    public function getCrudViewButtons(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudViewButtonsPartial && !$this->isNew();
        if (null === $this->collCrudViewButtons || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCrudViewButtons) {
                    $this->initCrudViewButtons();
                } else {
                    $collectionClassName = CrudViewButtonTableMap::getTableMap()->getCollectionClassName();

                    $collCrudViewButtons = new $collectionClassName;
                    $collCrudViewButtons->setModel('\Model\Setting\CrudManager\CrudViewButton');

                    return $collCrudViewButtons;
                }
            } else {
                $collCrudViewButtons = ChildCrudViewButtonQuery::create(null, $criteria)
                    ->filterByCrudView($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCrudViewButtonsPartial && count($collCrudViewButtons)) {
                        $this->initCrudViewButtons(false);

                        foreach ($collCrudViewButtons as $obj) {
                            if (false == $this->collCrudViewButtons->contains($obj)) {
                                $this->collCrudViewButtons->append($obj);
                            }
                        }

                        $this->collCrudViewButtonsPartial = true;
                    }

                    return $collCrudViewButtons;
                }

                if ($partial && $this->collCrudViewButtons) {
                    foreach ($this->collCrudViewButtons as $obj) {
                        if ($obj->isNew()) {
                            $collCrudViewButtons[] = $obj;
                        }
                    }
                }

                $this->collCrudViewButtons = $collCrudViewButtons;
                $this->collCrudViewButtonsPartial = false;
            }
        }

        return $this->collCrudViewButtons;
    }

    /**
     * Sets a collection of ChildCrudViewButton objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $crudViewButtons A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCrudView The current object (for fluent API support)
     */
    public function setCrudViewButtons(Collection $crudViewButtons, ConnectionInterface $con = null)
    {
        /** @var ChildCrudViewButton[] $crudViewButtonsToDelete */
        $crudViewButtonsToDelete = $this->getCrudViewButtons(new Criteria(), $con)->diff($crudViewButtons);


        $this->crudViewButtonsScheduledForDeletion = $crudViewButtonsToDelete;

        foreach ($crudViewButtonsToDelete as $crudViewButtonRemoved) {
            $crudViewButtonRemoved->setCrudView(null);
        }

        $this->collCrudViewButtons = null;
        foreach ($crudViewButtons as $crudViewButton) {
            $this->addCrudViewButton($crudViewButton);
        }

        $this->collCrudViewButtons = $crudViewButtons;
        $this->collCrudViewButtonsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CrudViewButton objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CrudViewButton objects.
     * @throws PropelException
     */
    public function countCrudViewButtons(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudViewButtonsPartial && !$this->isNew();
        if (null === $this->collCrudViewButtons || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCrudViewButtons) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCrudViewButtons());
            }

            $query = ChildCrudViewButtonQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCrudView($this)
                ->count($con);
        }

        return count($this->collCrudViewButtons);
    }

    /**
     * Method called to associate a ChildCrudViewButton object to this object
     * through the ChildCrudViewButton foreign key attribute.
     *
     * @param  ChildCrudViewButton $l ChildCrudViewButton
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function addCrudViewButton(ChildCrudViewButton $l)
    {
        if ($this->collCrudViewButtons === null) {
            $this->initCrudViewButtons();
            $this->collCrudViewButtonsPartial = true;
        }

        if (!$this->collCrudViewButtons->contains($l)) {
            $this->doAddCrudViewButton($l);

            if ($this->crudViewButtonsScheduledForDeletion and $this->crudViewButtonsScheduledForDeletion->contains($l)) {
                $this->crudViewButtonsScheduledForDeletion->remove($this->crudViewButtonsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCrudViewButton $crudViewButton The ChildCrudViewButton object to add.
     */
    protected function doAddCrudViewButton(ChildCrudViewButton $crudViewButton)
    {
        $this->collCrudViewButtons[]= $crudViewButton;
        $crudViewButton->setCrudView($this);
    }

    /**
     * @param  ChildCrudViewButton $crudViewButton The ChildCrudViewButton object to remove.
     * @return $this|ChildCrudView The current object (for fluent API support)
     */
    public function removeCrudViewButton(ChildCrudViewButton $crudViewButton)
    {
        if ($this->getCrudViewButtons()->contains($crudViewButton)) {
            $pos = $this->collCrudViewButtons->search($crudViewButton);
            $this->collCrudViewButtons->remove($pos);
            if (null === $this->crudViewButtonsScheduledForDeletion) {
                $this->crudViewButtonsScheduledForDeletion = clone $this->collCrudViewButtons;
                $this->crudViewButtonsScheduledForDeletion->clear();
            }
            $this->crudViewButtonsScheduledForDeletion[]= clone $crudViewButton;
            $crudViewButton->setCrudView(null);
        }

        return $this;
    }

    /**
     * Clears out the collCrudViewVisibleFilters collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCrudViewVisibleFilters()
     */
    public function clearCrudViewVisibleFilters()
    {
        $this->collCrudViewVisibleFilters = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCrudViewVisibleFilters collection loaded partially.
     */
    public function resetPartialCrudViewVisibleFilters($v = true)
    {
        $this->collCrudViewVisibleFiltersPartial = $v;
    }

    /**
     * Initializes the collCrudViewVisibleFilters collection.
     *
     * By default this just sets the collCrudViewVisibleFilters collection to an empty array (like clearcollCrudViewVisibleFilters());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCrudViewVisibleFilters($overrideExisting = true)
    {
        if (null !== $this->collCrudViewVisibleFilters && !$overrideExisting) {
            return;
        }

        $collectionClassName = CrudViewVisibleFilterTableMap::getTableMap()->getCollectionClassName();

        $this->collCrudViewVisibleFilters = new $collectionClassName;
        $this->collCrudViewVisibleFilters->setModel('\Model\Setting\CrudManager\CrudViewVisibleFilter');
    }

    /**
     * Gets an array of ChildCrudViewVisibleFilter objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCrudView is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCrudViewVisibleFilter[] List of ChildCrudViewVisibleFilter objects
     * @throws PropelException
     */
    public function getCrudViewVisibleFilters(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudViewVisibleFiltersPartial && !$this->isNew();
        if (null === $this->collCrudViewVisibleFilters || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCrudViewVisibleFilters) {
                    $this->initCrudViewVisibleFilters();
                } else {
                    $collectionClassName = CrudViewVisibleFilterTableMap::getTableMap()->getCollectionClassName();

                    $collCrudViewVisibleFilters = new $collectionClassName;
                    $collCrudViewVisibleFilters->setModel('\Model\Setting\CrudManager\CrudViewVisibleFilter');

                    return $collCrudViewVisibleFilters;
                }
            } else {
                $collCrudViewVisibleFilters = ChildCrudViewVisibleFilterQuery::create(null, $criteria)
                    ->filterByCrudView($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCrudViewVisibleFiltersPartial && count($collCrudViewVisibleFilters)) {
                        $this->initCrudViewVisibleFilters(false);

                        foreach ($collCrudViewVisibleFilters as $obj) {
                            if (false == $this->collCrudViewVisibleFilters->contains($obj)) {
                                $this->collCrudViewVisibleFilters->append($obj);
                            }
                        }

                        $this->collCrudViewVisibleFiltersPartial = true;
                    }

                    return $collCrudViewVisibleFilters;
                }

                if ($partial && $this->collCrudViewVisibleFilters) {
                    foreach ($this->collCrudViewVisibleFilters as $obj) {
                        if ($obj->isNew()) {
                            $collCrudViewVisibleFilters[] = $obj;
                        }
                    }
                }

                $this->collCrudViewVisibleFilters = $collCrudViewVisibleFilters;
                $this->collCrudViewVisibleFiltersPartial = false;
            }
        }

        return $this->collCrudViewVisibleFilters;
    }

    /**
     * Sets a collection of ChildCrudViewVisibleFilter objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $crudViewVisibleFilters A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCrudView The current object (for fluent API support)
     */
    public function setCrudViewVisibleFilters(Collection $crudViewVisibleFilters, ConnectionInterface $con = null)
    {
        /** @var ChildCrudViewVisibleFilter[] $crudViewVisibleFiltersToDelete */
        $crudViewVisibleFiltersToDelete = $this->getCrudViewVisibleFilters(new Criteria(), $con)->diff($crudViewVisibleFilters);


        $this->crudViewVisibleFiltersScheduledForDeletion = $crudViewVisibleFiltersToDelete;

        foreach ($crudViewVisibleFiltersToDelete as $crudViewVisibleFilterRemoved) {
            $crudViewVisibleFilterRemoved->setCrudView(null);
        }

        $this->collCrudViewVisibleFilters = null;
        foreach ($crudViewVisibleFilters as $crudViewVisibleFilter) {
            $this->addCrudViewVisibleFilter($crudViewVisibleFilter);
        }

        $this->collCrudViewVisibleFilters = $crudViewVisibleFilters;
        $this->collCrudViewVisibleFiltersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CrudViewVisibleFilter objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CrudViewVisibleFilter objects.
     * @throws PropelException
     */
    public function countCrudViewVisibleFilters(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudViewVisibleFiltersPartial && !$this->isNew();
        if (null === $this->collCrudViewVisibleFilters || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCrudViewVisibleFilters) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCrudViewVisibleFilters());
            }

            $query = ChildCrudViewVisibleFilterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCrudView($this)
                ->count($con);
        }

        return count($this->collCrudViewVisibleFilters);
    }

    /**
     * Method called to associate a ChildCrudViewVisibleFilter object to this object
     * through the ChildCrudViewVisibleFilter foreign key attribute.
     *
     * @param  ChildCrudViewVisibleFilter $l ChildCrudViewVisibleFilter
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function addCrudViewVisibleFilter(ChildCrudViewVisibleFilter $l)
    {
        if ($this->collCrudViewVisibleFilters === null) {
            $this->initCrudViewVisibleFilters();
            $this->collCrudViewVisibleFiltersPartial = true;
        }

        if (!$this->collCrudViewVisibleFilters->contains($l)) {
            $this->doAddCrudViewVisibleFilter($l);

            if ($this->crudViewVisibleFiltersScheduledForDeletion and $this->crudViewVisibleFiltersScheduledForDeletion->contains($l)) {
                $this->crudViewVisibleFiltersScheduledForDeletion->remove($this->crudViewVisibleFiltersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCrudViewVisibleFilter $crudViewVisibleFilter The ChildCrudViewVisibleFilter object to add.
     */
    protected function doAddCrudViewVisibleFilter(ChildCrudViewVisibleFilter $crudViewVisibleFilter)
    {
        $this->collCrudViewVisibleFilters[]= $crudViewVisibleFilter;
        $crudViewVisibleFilter->setCrudView($this);
    }

    /**
     * @param  ChildCrudViewVisibleFilter $crudViewVisibleFilter The ChildCrudViewVisibleFilter object to remove.
     * @return $this|ChildCrudView The current object (for fluent API support)
     */
    public function removeCrudViewVisibleFilter(ChildCrudViewVisibleFilter $crudViewVisibleFilter)
    {
        if ($this->getCrudViewVisibleFilters()->contains($crudViewVisibleFilter)) {
            $pos = $this->collCrudViewVisibleFilters->search($crudViewVisibleFilter);
            $this->collCrudViewVisibleFilters->remove($pos);
            if (null === $this->crudViewVisibleFiltersScheduledForDeletion) {
                $this->crudViewVisibleFiltersScheduledForDeletion = clone $this->collCrudViewVisibleFilters;
                $this->crudViewVisibleFiltersScheduledForDeletion->clear();
            }
            $this->crudViewVisibleFiltersScheduledForDeletion[]= clone $crudViewVisibleFilter;
            $crudViewVisibleFilter->setCrudView(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CrudView is new, it will return
     * an empty collection; or if this CrudView has previously
     * been saved, it will retrieve related CrudViewVisibleFilters from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CrudView.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCrudViewVisibleFilter[] List of ChildCrudViewVisibleFilter objects
     */
    public function getCrudViewVisibleFiltersJoinFilterOperator(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCrudViewVisibleFilterQuery::create(null, $criteria);
        $query->joinWith('FilterOperator', $joinBehavior);

        return $this->getCrudViewVisibleFilters($query, $con);
    }

    /**
     * Clears out the collCrudViewHiddenFilters collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCrudViewHiddenFilters()
     */
    public function clearCrudViewHiddenFilters()
    {
        $this->collCrudViewHiddenFilters = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCrudViewHiddenFilters collection loaded partially.
     */
    public function resetPartialCrudViewHiddenFilters($v = true)
    {
        $this->collCrudViewHiddenFiltersPartial = $v;
    }

    /**
     * Initializes the collCrudViewHiddenFilters collection.
     *
     * By default this just sets the collCrudViewHiddenFilters collection to an empty array (like clearcollCrudViewHiddenFilters());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCrudViewHiddenFilters($overrideExisting = true)
    {
        if (null !== $this->collCrudViewHiddenFilters && !$overrideExisting) {
            return;
        }

        $collectionClassName = CrudViewHiddenFilterTableMap::getTableMap()->getCollectionClassName();

        $this->collCrudViewHiddenFilters = new $collectionClassName;
        $this->collCrudViewHiddenFilters->setModel('\Model\Setting\CrudManager\CrudViewHiddenFilter');
    }

    /**
     * Gets an array of ChildCrudViewHiddenFilter objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCrudView is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCrudViewHiddenFilter[] List of ChildCrudViewHiddenFilter objects
     * @throws PropelException
     */
    public function getCrudViewHiddenFilters(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudViewHiddenFiltersPartial && !$this->isNew();
        if (null === $this->collCrudViewHiddenFilters || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCrudViewHiddenFilters) {
                    $this->initCrudViewHiddenFilters();
                } else {
                    $collectionClassName = CrudViewHiddenFilterTableMap::getTableMap()->getCollectionClassName();

                    $collCrudViewHiddenFilters = new $collectionClassName;
                    $collCrudViewHiddenFilters->setModel('\Model\Setting\CrudManager\CrudViewHiddenFilter');

                    return $collCrudViewHiddenFilters;
                }
            } else {
                $collCrudViewHiddenFilters = ChildCrudViewHiddenFilterQuery::create(null, $criteria)
                    ->filterByCrudView($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCrudViewHiddenFiltersPartial && count($collCrudViewHiddenFilters)) {
                        $this->initCrudViewHiddenFilters(false);

                        foreach ($collCrudViewHiddenFilters as $obj) {
                            if (false == $this->collCrudViewHiddenFilters->contains($obj)) {
                                $this->collCrudViewHiddenFilters->append($obj);
                            }
                        }

                        $this->collCrudViewHiddenFiltersPartial = true;
                    }

                    return $collCrudViewHiddenFilters;
                }

                if ($partial && $this->collCrudViewHiddenFilters) {
                    foreach ($this->collCrudViewHiddenFilters as $obj) {
                        if ($obj->isNew()) {
                            $collCrudViewHiddenFilters[] = $obj;
                        }
                    }
                }

                $this->collCrudViewHiddenFilters = $collCrudViewHiddenFilters;
                $this->collCrudViewHiddenFiltersPartial = false;
            }
        }

        return $this->collCrudViewHiddenFilters;
    }

    /**
     * Sets a collection of ChildCrudViewHiddenFilter objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $crudViewHiddenFilters A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCrudView The current object (for fluent API support)
     */
    public function setCrudViewHiddenFilters(Collection $crudViewHiddenFilters, ConnectionInterface $con = null)
    {
        /** @var ChildCrudViewHiddenFilter[] $crudViewHiddenFiltersToDelete */
        $crudViewHiddenFiltersToDelete = $this->getCrudViewHiddenFilters(new Criteria(), $con)->diff($crudViewHiddenFilters);


        $this->crudViewHiddenFiltersScheduledForDeletion = $crudViewHiddenFiltersToDelete;

        foreach ($crudViewHiddenFiltersToDelete as $crudViewHiddenFilterRemoved) {
            $crudViewHiddenFilterRemoved->setCrudView(null);
        }

        $this->collCrudViewHiddenFilters = null;
        foreach ($crudViewHiddenFilters as $crudViewHiddenFilter) {
            $this->addCrudViewHiddenFilter($crudViewHiddenFilter);
        }

        $this->collCrudViewHiddenFilters = $crudViewHiddenFilters;
        $this->collCrudViewHiddenFiltersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CrudViewHiddenFilter objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CrudViewHiddenFilter objects.
     * @throws PropelException
     */
    public function countCrudViewHiddenFilters(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCrudViewHiddenFiltersPartial && !$this->isNew();
        if (null === $this->collCrudViewHiddenFilters || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCrudViewHiddenFilters) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCrudViewHiddenFilters());
            }

            $query = ChildCrudViewHiddenFilterQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCrudView($this)
                ->count($con);
        }

        return count($this->collCrudViewHiddenFilters);
    }

    /**
     * Method called to associate a ChildCrudViewHiddenFilter object to this object
     * through the ChildCrudViewHiddenFilter foreign key attribute.
     *
     * @param  ChildCrudViewHiddenFilter $l ChildCrudViewHiddenFilter
     * @return $this|\Model\Setting\CrudManager\CrudView The current object (for fluent API support)
     */
    public function addCrudViewHiddenFilter(ChildCrudViewHiddenFilter $l)
    {
        if ($this->collCrudViewHiddenFilters === null) {
            $this->initCrudViewHiddenFilters();
            $this->collCrudViewHiddenFiltersPartial = true;
        }

        if (!$this->collCrudViewHiddenFilters->contains($l)) {
            $this->doAddCrudViewHiddenFilter($l);

            if ($this->crudViewHiddenFiltersScheduledForDeletion and $this->crudViewHiddenFiltersScheduledForDeletion->contains($l)) {
                $this->crudViewHiddenFiltersScheduledForDeletion->remove($this->crudViewHiddenFiltersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCrudViewHiddenFilter $crudViewHiddenFilter The ChildCrudViewHiddenFilter object to add.
     */
    protected function doAddCrudViewHiddenFilter(ChildCrudViewHiddenFilter $crudViewHiddenFilter)
    {
        $this->collCrudViewHiddenFilters[]= $crudViewHiddenFilter;
        $crudViewHiddenFilter->setCrudView($this);
    }

    /**
     * @param  ChildCrudViewHiddenFilter $crudViewHiddenFilter The ChildCrudViewHiddenFilter object to remove.
     * @return $this|ChildCrudView The current object (for fluent API support)
     */
    public function removeCrudViewHiddenFilter(ChildCrudViewHiddenFilter $crudViewHiddenFilter)
    {
        if ($this->getCrudViewHiddenFilters()->contains($crudViewHiddenFilter)) {
            $pos = $this->collCrudViewHiddenFilters->search($crudViewHiddenFilter);
            $this->collCrudViewHiddenFilters->remove($pos);
            if (null === $this->crudViewHiddenFiltersScheduledForDeletion) {
                $this->crudViewHiddenFiltersScheduledForDeletion = clone $this->collCrudViewHiddenFilters;
                $this->crudViewHiddenFiltersScheduledForDeletion->clear();
            }
            $this->crudViewHiddenFiltersScheduledForDeletion[]= clone $crudViewHiddenFilter;
            $crudViewHiddenFilter->setCrudView(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this CrudView is new, it will return
     * an empty collection; or if this CrudView has previously
     * been saved, it will retrieve related CrudViewHiddenFilters from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in CrudView.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCrudViewHiddenFilter[] List of ChildCrudViewHiddenFilter objects
     */
    public function getCrudViewHiddenFiltersJoinFilterOperator(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCrudViewHiddenFilterQuery::create(null, $criteria);
        $query->joinWith('FilterOperator', $joinBehavior);

        return $this->getCrudViewHiddenFilters($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCrudConfig) {
            $this->aCrudConfig->removeCrudView($this);
        }
        $this->id = null;
        $this->crud_config_id = null;
        $this->name = null;
        $this->code = null;
        $this->sorting = null;
        $this->is_hidden = null;
        $this->show_quantity = null;
        $this->default_order_by = null;
        $this->default_order_dir = null;
        $this->tab_color_logic = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCrudViews) {
                foreach ($this->collCrudViews as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCrudViewButtons) {
                foreach ($this->collCrudViewButtons as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCrudViewVisibleFilters) {
                foreach ($this->collCrudViewVisibleFilters as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCrudViewHiddenFilters) {
                foreach ($this->collCrudViewHiddenFilters as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCrudViews = null;
        $this->collCrudViewButtons = null;
        $this->collCrudViewVisibleFilters = null;
        $this->collCrudViewHiddenFilters = null;
        $this->aCrudConfig = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CrudViewTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildCrudView The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[CrudViewTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
