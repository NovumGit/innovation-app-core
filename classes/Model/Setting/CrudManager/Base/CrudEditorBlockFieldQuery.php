<?php

namespace Model\Setting\CrudManager\Base;

use \Exception;
use \PDO;
use Model\Setting\CrudManager\CrudEditorBlockField as ChildCrudEditorBlockField;
use Model\Setting\CrudManager\CrudEditorBlockFieldQuery as ChildCrudEditorBlockFieldQuery;
use Model\Setting\CrudManager\Map\CrudEditorBlockFieldTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'crud_editor_block_field' table.
 *
 *
 *
 * @method     ChildCrudEditorBlockFieldQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCrudEditorBlockFieldQuery orderByCrudEditorBlockId($order = Criteria::ASC) Order by the crud_editor_block_id column
 * @method     ChildCrudEditorBlockFieldQuery orderBySorting($order = Criteria::ASC) Order by the sorting column
 * @method     ChildCrudEditorBlockFieldQuery orderByReadonlyField($order = Criteria::ASC) Order by the readonly_field column
 * @method     ChildCrudEditorBlockFieldQuery orderByWidth($order = Criteria::ASC) Order by the width column
 * @method     ChildCrudEditorBlockFieldQuery orderByField($order = Criteria::ASC) Order by the field column
 * @method     ChildCrudEditorBlockFieldQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCrudEditorBlockFieldQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCrudEditorBlockFieldQuery groupById() Group by the id column
 * @method     ChildCrudEditorBlockFieldQuery groupByCrudEditorBlockId() Group by the crud_editor_block_id column
 * @method     ChildCrudEditorBlockFieldQuery groupBySorting() Group by the sorting column
 * @method     ChildCrudEditorBlockFieldQuery groupByReadonlyField() Group by the readonly_field column
 * @method     ChildCrudEditorBlockFieldQuery groupByWidth() Group by the width column
 * @method     ChildCrudEditorBlockFieldQuery groupByField() Group by the field column
 * @method     ChildCrudEditorBlockFieldQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCrudEditorBlockFieldQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCrudEditorBlockFieldQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCrudEditorBlockFieldQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCrudEditorBlockFieldQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCrudEditorBlockFieldQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCrudEditorBlockFieldQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCrudEditorBlockFieldQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCrudEditorBlockFieldQuery leftJoinCrudEditorBlock($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudEditorBlock relation
 * @method     ChildCrudEditorBlockFieldQuery rightJoinCrudEditorBlock($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudEditorBlock relation
 * @method     ChildCrudEditorBlockFieldQuery innerJoinCrudEditorBlock($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudEditorBlock relation
 *
 * @method     ChildCrudEditorBlockFieldQuery joinWithCrudEditorBlock($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudEditorBlock relation
 *
 * @method     ChildCrudEditorBlockFieldQuery leftJoinWithCrudEditorBlock() Adds a LEFT JOIN clause and with to the query using the CrudEditorBlock relation
 * @method     ChildCrudEditorBlockFieldQuery rightJoinWithCrudEditorBlock() Adds a RIGHT JOIN clause and with to the query using the CrudEditorBlock relation
 * @method     ChildCrudEditorBlockFieldQuery innerJoinWithCrudEditorBlock() Adds a INNER JOIN clause and with to the query using the CrudEditorBlock relation
 *
 * @method     \Model\Setting\CrudManager\CrudEditorBlockQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCrudEditorBlockField findOne(ConnectionInterface $con = null) Return the first ChildCrudEditorBlockField matching the query
 * @method     ChildCrudEditorBlockField findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCrudEditorBlockField matching the query, or a new ChildCrudEditorBlockField object populated from the query conditions when no match is found
 *
 * @method     ChildCrudEditorBlockField findOneById(int $id) Return the first ChildCrudEditorBlockField filtered by the id column
 * @method     ChildCrudEditorBlockField findOneByCrudEditorBlockId(int $crud_editor_block_id) Return the first ChildCrudEditorBlockField filtered by the crud_editor_block_id column
 * @method     ChildCrudEditorBlockField findOneBySorting(int $sorting) Return the first ChildCrudEditorBlockField filtered by the sorting column
 * @method     ChildCrudEditorBlockField findOneByReadonlyField(boolean $readonly_field) Return the first ChildCrudEditorBlockField filtered by the readonly_field column
 * @method     ChildCrudEditorBlockField findOneByWidth(int $width) Return the first ChildCrudEditorBlockField filtered by the width column
 * @method     ChildCrudEditorBlockField findOneByField(string $field) Return the first ChildCrudEditorBlockField filtered by the field column
 * @method     ChildCrudEditorBlockField findOneByCreatedAt(string $created_at) Return the first ChildCrudEditorBlockField filtered by the created_at column
 * @method     ChildCrudEditorBlockField findOneByUpdatedAt(string $updated_at) Return the first ChildCrudEditorBlockField filtered by the updated_at column *

 * @method     ChildCrudEditorBlockField requirePk($key, ConnectionInterface $con = null) Return the ChildCrudEditorBlockField by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlockField requireOne(ConnectionInterface $con = null) Return the first ChildCrudEditorBlockField matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudEditorBlockField requireOneById(int $id) Return the first ChildCrudEditorBlockField filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlockField requireOneByCrudEditorBlockId(int $crud_editor_block_id) Return the first ChildCrudEditorBlockField filtered by the crud_editor_block_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlockField requireOneBySorting(int $sorting) Return the first ChildCrudEditorBlockField filtered by the sorting column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlockField requireOneByReadonlyField(boolean $readonly_field) Return the first ChildCrudEditorBlockField filtered by the readonly_field column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlockField requireOneByWidth(int $width) Return the first ChildCrudEditorBlockField filtered by the width column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlockField requireOneByField(string $field) Return the first ChildCrudEditorBlockField filtered by the field column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlockField requireOneByCreatedAt(string $created_at) Return the first ChildCrudEditorBlockField filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlockField requireOneByUpdatedAt(string $updated_at) Return the first ChildCrudEditorBlockField filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudEditorBlockField[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCrudEditorBlockField objects based on current ModelCriteria
 * @method     ChildCrudEditorBlockField[]|ObjectCollection findById(int $id) Return ChildCrudEditorBlockField objects filtered by the id column
 * @method     ChildCrudEditorBlockField[]|ObjectCollection findByCrudEditorBlockId(int $crud_editor_block_id) Return ChildCrudEditorBlockField objects filtered by the crud_editor_block_id column
 * @method     ChildCrudEditorBlockField[]|ObjectCollection findBySorting(int $sorting) Return ChildCrudEditorBlockField objects filtered by the sorting column
 * @method     ChildCrudEditorBlockField[]|ObjectCollection findByReadonlyField(boolean $readonly_field) Return ChildCrudEditorBlockField objects filtered by the readonly_field column
 * @method     ChildCrudEditorBlockField[]|ObjectCollection findByWidth(int $width) Return ChildCrudEditorBlockField objects filtered by the width column
 * @method     ChildCrudEditorBlockField[]|ObjectCollection findByField(string $field) Return ChildCrudEditorBlockField objects filtered by the field column
 * @method     ChildCrudEditorBlockField[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCrudEditorBlockField objects filtered by the created_at column
 * @method     ChildCrudEditorBlockField[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCrudEditorBlockField objects filtered by the updated_at column
 * @method     ChildCrudEditorBlockField[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CrudEditorBlockFieldQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\CrudManager\Base\CrudEditorBlockFieldQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\CrudManager\\CrudEditorBlockField', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCrudEditorBlockFieldQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCrudEditorBlockFieldQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCrudEditorBlockFieldQuery) {
            return $criteria;
        }
        $query = new ChildCrudEditorBlockFieldQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCrudEditorBlockField|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CrudEditorBlockFieldTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CrudEditorBlockFieldTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudEditorBlockField A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, crud_editor_block_id, sorting, readonly_field, width, field, created_at, updated_at FROM crud_editor_block_field WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCrudEditorBlockField $obj */
            $obj = new ChildCrudEditorBlockField();
            $obj->hydrate($row);
            CrudEditorBlockFieldTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCrudEditorBlockField|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the crud_editor_block_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCrudEditorBlockId(1234); // WHERE crud_editor_block_id = 1234
     * $query->filterByCrudEditorBlockId(array(12, 34)); // WHERE crud_editor_block_id IN (12, 34)
     * $query->filterByCrudEditorBlockId(array('min' => 12)); // WHERE crud_editor_block_id > 12
     * </code>
     *
     * @see       filterByCrudEditorBlock()
     *
     * @param     mixed $crudEditorBlockId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function filterByCrudEditorBlockId($crudEditorBlockId = null, $comparison = null)
    {
        if (is_array($crudEditorBlockId)) {
            $useMinMax = false;
            if (isset($crudEditorBlockId['min'])) {
                $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_CRUD_EDITOR_BLOCK_ID, $crudEditorBlockId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($crudEditorBlockId['max'])) {
                $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_CRUD_EDITOR_BLOCK_ID, $crudEditorBlockId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_CRUD_EDITOR_BLOCK_ID, $crudEditorBlockId, $comparison);
    }

    /**
     * Filter the query on the sorting column
     *
     * Example usage:
     * <code>
     * $query->filterBySorting(1234); // WHERE sorting = 1234
     * $query->filterBySorting(array(12, 34)); // WHERE sorting IN (12, 34)
     * $query->filterBySorting(array('min' => 12)); // WHERE sorting > 12
     * </code>
     *
     * @param     mixed $sorting The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function filterBySorting($sorting = null, $comparison = null)
    {
        if (is_array($sorting)) {
            $useMinMax = false;
            if (isset($sorting['min'])) {
                $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_SORTING, $sorting['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sorting['max'])) {
                $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_SORTING, $sorting['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_SORTING, $sorting, $comparison);
    }

    /**
     * Filter the query on the readonly_field column
     *
     * Example usage:
     * <code>
     * $query->filterByReadonlyField(true); // WHERE readonly_field = true
     * $query->filterByReadonlyField('yes'); // WHERE readonly_field = true
     * </code>
     *
     * @param     boolean|string $readonlyField The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function filterByReadonlyField($readonlyField = null, $comparison = null)
    {
        if (is_string($readonlyField)) {
            $readonlyField = in_array(strtolower($readonlyField), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_READONLY_FIELD, $readonlyField, $comparison);
    }

    /**
     * Filter the query on the width column
     *
     * Example usage:
     * <code>
     * $query->filterByWidth(1234); // WHERE width = 1234
     * $query->filterByWidth(array(12, 34)); // WHERE width IN (12, 34)
     * $query->filterByWidth(array('min' => 12)); // WHERE width > 12
     * </code>
     *
     * @param     mixed $width The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function filterByWidth($width = null, $comparison = null)
    {
        if (is_array($width)) {
            $useMinMax = false;
            if (isset($width['min'])) {
                $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_WIDTH, $width['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($width['max'])) {
                $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_WIDTH, $width['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_WIDTH, $width, $comparison);
    }

    /**
     * Filter the query on the field column
     *
     * Example usage:
     * <code>
     * $query->filterByField('fooValue');   // WHERE field = 'fooValue'
     * $query->filterByField('%fooValue%', Criteria::LIKE); // WHERE field LIKE '%fooValue%'
     * </code>
     *
     * @param     string $field The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function filterByField($field = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($field)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_FIELD, $field, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudEditorBlock object
     *
     * @param \Model\Setting\CrudManager\CrudEditorBlock|ObjectCollection $crudEditorBlock The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function filterByCrudEditorBlock($crudEditorBlock, $comparison = null)
    {
        if ($crudEditorBlock instanceof \Model\Setting\CrudManager\CrudEditorBlock) {
            return $this
                ->addUsingAlias(CrudEditorBlockFieldTableMap::COL_CRUD_EDITOR_BLOCK_ID, $crudEditorBlock->getId(), $comparison);
        } elseif ($crudEditorBlock instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CrudEditorBlockFieldTableMap::COL_CRUD_EDITOR_BLOCK_ID, $crudEditorBlock->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCrudEditorBlock() only accepts arguments of type \Model\Setting\CrudManager\CrudEditorBlock or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudEditorBlock relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function joinCrudEditorBlock($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudEditorBlock');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudEditorBlock');
        }

        return $this;
    }

    /**
     * Use the CrudEditorBlock relation CrudEditorBlock object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudEditorBlockQuery A secondary query class using the current class as primary query
     */
    public function useCrudEditorBlockQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudEditorBlock($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudEditorBlock', '\Model\Setting\CrudManager\CrudEditorBlockQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCrudEditorBlockField $crudEditorBlockField Object to remove from the list of results
     *
     * @return $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function prune($crudEditorBlockField = null)
    {
        if ($crudEditorBlockField) {
            $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_ID, $crudEditorBlockField->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the crud_editor_block_field table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorBlockFieldTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CrudEditorBlockFieldTableMap::clearInstancePool();
            CrudEditorBlockFieldTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorBlockFieldTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CrudEditorBlockFieldTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CrudEditorBlockFieldTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CrudEditorBlockFieldTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudEditorBlockFieldTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudEditorBlockFieldTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudEditorBlockFieldTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudEditorBlockFieldTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCrudEditorBlockFieldQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudEditorBlockFieldTableMap::COL_CREATED_AT);
    }

} // CrudEditorBlockFieldQuery
