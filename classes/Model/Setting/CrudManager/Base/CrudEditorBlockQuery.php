<?php

namespace Model\Setting\CrudManager\Base;

use \Exception;
use \PDO;
use Model\Setting\CrudManager\CrudEditorBlock as ChildCrudEditorBlock;
use Model\Setting\CrudManager\CrudEditorBlockQuery as ChildCrudEditorBlockQuery;
use Model\Setting\CrudManager\Map\CrudEditorBlockTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'crud_editor_block' table.
 *
 *
 *
 * @method     ChildCrudEditorBlockQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCrudEditorBlockQuery orderByCrudEditorId($order = Criteria::ASC) Order by the crud_editor_id column
 * @method     ChildCrudEditorBlockQuery orderBySorting($order = Criteria::ASC) Order by the sorting column
 * @method     ChildCrudEditorBlockQuery orderByWidth($order = Criteria::ASC) Order by the width column
 * @method     ChildCrudEditorBlockQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildCrudEditorBlockQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCrudEditorBlockQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCrudEditorBlockQuery groupById() Group by the id column
 * @method     ChildCrudEditorBlockQuery groupByCrudEditorId() Group by the crud_editor_id column
 * @method     ChildCrudEditorBlockQuery groupBySorting() Group by the sorting column
 * @method     ChildCrudEditorBlockQuery groupByWidth() Group by the width column
 * @method     ChildCrudEditorBlockQuery groupByTitle() Group by the title column
 * @method     ChildCrudEditorBlockQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCrudEditorBlockQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCrudEditorBlockQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCrudEditorBlockQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCrudEditorBlockQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCrudEditorBlockQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCrudEditorBlockQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCrudEditorBlockQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCrudEditorBlockQuery leftJoinCrudEditor($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudEditor relation
 * @method     ChildCrudEditorBlockQuery rightJoinCrudEditor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudEditor relation
 * @method     ChildCrudEditorBlockQuery innerJoinCrudEditor($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudEditor relation
 *
 * @method     ChildCrudEditorBlockQuery joinWithCrudEditor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudEditor relation
 *
 * @method     ChildCrudEditorBlockQuery leftJoinWithCrudEditor() Adds a LEFT JOIN clause and with to the query using the CrudEditor relation
 * @method     ChildCrudEditorBlockQuery rightJoinWithCrudEditor() Adds a RIGHT JOIN clause and with to the query using the CrudEditor relation
 * @method     ChildCrudEditorBlockQuery innerJoinWithCrudEditor() Adds a INNER JOIN clause and with to the query using the CrudEditor relation
 *
 * @method     ChildCrudEditorBlockQuery leftJoinCrudEditorBlockField($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudEditorBlockField relation
 * @method     ChildCrudEditorBlockQuery rightJoinCrudEditorBlockField($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudEditorBlockField relation
 * @method     ChildCrudEditorBlockQuery innerJoinCrudEditorBlockField($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudEditorBlockField relation
 *
 * @method     ChildCrudEditorBlockQuery joinWithCrudEditorBlockField($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudEditorBlockField relation
 *
 * @method     ChildCrudEditorBlockQuery leftJoinWithCrudEditorBlockField() Adds a LEFT JOIN clause and with to the query using the CrudEditorBlockField relation
 * @method     ChildCrudEditorBlockQuery rightJoinWithCrudEditorBlockField() Adds a RIGHT JOIN clause and with to the query using the CrudEditorBlockField relation
 * @method     ChildCrudEditorBlockQuery innerJoinWithCrudEditorBlockField() Adds a INNER JOIN clause and with to the query using the CrudEditorBlockField relation
 *
 * @method     \Model\Setting\CrudManager\CrudEditorQuery|\Model\Setting\CrudManager\CrudEditorBlockFieldQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCrudEditorBlock findOne(ConnectionInterface $con = null) Return the first ChildCrudEditorBlock matching the query
 * @method     ChildCrudEditorBlock findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCrudEditorBlock matching the query, or a new ChildCrudEditorBlock object populated from the query conditions when no match is found
 *
 * @method     ChildCrudEditorBlock findOneById(int $id) Return the first ChildCrudEditorBlock filtered by the id column
 * @method     ChildCrudEditorBlock findOneByCrudEditorId(int $crud_editor_id) Return the first ChildCrudEditorBlock filtered by the crud_editor_id column
 * @method     ChildCrudEditorBlock findOneBySorting(int $sorting) Return the first ChildCrudEditorBlock filtered by the sorting column
 * @method     ChildCrudEditorBlock findOneByWidth(int $width) Return the first ChildCrudEditorBlock filtered by the width column
 * @method     ChildCrudEditorBlock findOneByTitle(string $title) Return the first ChildCrudEditorBlock filtered by the title column
 * @method     ChildCrudEditorBlock findOneByCreatedAt(string $created_at) Return the first ChildCrudEditorBlock filtered by the created_at column
 * @method     ChildCrudEditorBlock findOneByUpdatedAt(string $updated_at) Return the first ChildCrudEditorBlock filtered by the updated_at column *

 * @method     ChildCrudEditorBlock requirePk($key, ConnectionInterface $con = null) Return the ChildCrudEditorBlock by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlock requireOne(ConnectionInterface $con = null) Return the first ChildCrudEditorBlock matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudEditorBlock requireOneById(int $id) Return the first ChildCrudEditorBlock filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlock requireOneByCrudEditorId(int $crud_editor_id) Return the first ChildCrudEditorBlock filtered by the crud_editor_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlock requireOneBySorting(int $sorting) Return the first ChildCrudEditorBlock filtered by the sorting column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlock requireOneByWidth(int $width) Return the first ChildCrudEditorBlock filtered by the width column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlock requireOneByTitle(string $title) Return the first ChildCrudEditorBlock filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlock requireOneByCreatedAt(string $created_at) Return the first ChildCrudEditorBlock filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorBlock requireOneByUpdatedAt(string $updated_at) Return the first ChildCrudEditorBlock filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudEditorBlock[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCrudEditorBlock objects based on current ModelCriteria
 * @method     ChildCrudEditorBlock[]|ObjectCollection findById(int $id) Return ChildCrudEditorBlock objects filtered by the id column
 * @method     ChildCrudEditorBlock[]|ObjectCollection findByCrudEditorId(int $crud_editor_id) Return ChildCrudEditorBlock objects filtered by the crud_editor_id column
 * @method     ChildCrudEditorBlock[]|ObjectCollection findBySorting(int $sorting) Return ChildCrudEditorBlock objects filtered by the sorting column
 * @method     ChildCrudEditorBlock[]|ObjectCollection findByWidth(int $width) Return ChildCrudEditorBlock objects filtered by the width column
 * @method     ChildCrudEditorBlock[]|ObjectCollection findByTitle(string $title) Return ChildCrudEditorBlock objects filtered by the title column
 * @method     ChildCrudEditorBlock[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCrudEditorBlock objects filtered by the created_at column
 * @method     ChildCrudEditorBlock[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCrudEditorBlock objects filtered by the updated_at column
 * @method     ChildCrudEditorBlock[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CrudEditorBlockQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\CrudManager\Base\CrudEditorBlockQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\CrudManager\\CrudEditorBlock', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCrudEditorBlockQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCrudEditorBlockQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCrudEditorBlockQuery) {
            return $criteria;
        }
        $query = new ChildCrudEditorBlockQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCrudEditorBlock|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CrudEditorBlockTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CrudEditorBlockTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudEditorBlock A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, crud_editor_id, sorting, width, title, created_at, updated_at FROM crud_editor_block WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCrudEditorBlock $obj */
            $obj = new ChildCrudEditorBlock();
            $obj->hydrate($row);
            CrudEditorBlockTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCrudEditorBlock|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CrudEditorBlockTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CrudEditorBlockTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CrudEditorBlockTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CrudEditorBlockTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the crud_editor_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCrudEditorId(1234); // WHERE crud_editor_id = 1234
     * $query->filterByCrudEditorId(array(12, 34)); // WHERE crud_editor_id IN (12, 34)
     * $query->filterByCrudEditorId(array('min' => 12)); // WHERE crud_editor_id > 12
     * </code>
     *
     * @see       filterByCrudEditor()
     *
     * @param     mixed $crudEditorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function filterByCrudEditorId($crudEditorId = null, $comparison = null)
    {
        if (is_array($crudEditorId)) {
            $useMinMax = false;
            if (isset($crudEditorId['min'])) {
                $this->addUsingAlias(CrudEditorBlockTableMap::COL_CRUD_EDITOR_ID, $crudEditorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($crudEditorId['max'])) {
                $this->addUsingAlias(CrudEditorBlockTableMap::COL_CRUD_EDITOR_ID, $crudEditorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockTableMap::COL_CRUD_EDITOR_ID, $crudEditorId, $comparison);
    }

    /**
     * Filter the query on the sorting column
     *
     * Example usage:
     * <code>
     * $query->filterBySorting(1234); // WHERE sorting = 1234
     * $query->filterBySorting(array(12, 34)); // WHERE sorting IN (12, 34)
     * $query->filterBySorting(array('min' => 12)); // WHERE sorting > 12
     * </code>
     *
     * @param     mixed $sorting The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function filterBySorting($sorting = null, $comparison = null)
    {
        if (is_array($sorting)) {
            $useMinMax = false;
            if (isset($sorting['min'])) {
                $this->addUsingAlias(CrudEditorBlockTableMap::COL_SORTING, $sorting['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sorting['max'])) {
                $this->addUsingAlias(CrudEditorBlockTableMap::COL_SORTING, $sorting['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockTableMap::COL_SORTING, $sorting, $comparison);
    }

    /**
     * Filter the query on the width column
     *
     * Example usage:
     * <code>
     * $query->filterByWidth(1234); // WHERE width = 1234
     * $query->filterByWidth(array(12, 34)); // WHERE width IN (12, 34)
     * $query->filterByWidth(array('min' => 12)); // WHERE width > 12
     * </code>
     *
     * @param     mixed $width The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function filterByWidth($width = null, $comparison = null)
    {
        if (is_array($width)) {
            $useMinMax = false;
            if (isset($width['min'])) {
                $this->addUsingAlias(CrudEditorBlockTableMap::COL_WIDTH, $width['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($width['max'])) {
                $this->addUsingAlias(CrudEditorBlockTableMap::COL_WIDTH, $width['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockTableMap::COL_WIDTH, $width, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CrudEditorBlockTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CrudEditorBlockTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CrudEditorBlockTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CrudEditorBlockTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorBlockTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudEditor object
     *
     * @param \Model\Setting\CrudManager\CrudEditor|ObjectCollection $crudEditor The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function filterByCrudEditor($crudEditor, $comparison = null)
    {
        if ($crudEditor instanceof \Model\Setting\CrudManager\CrudEditor) {
            return $this
                ->addUsingAlias(CrudEditorBlockTableMap::COL_CRUD_EDITOR_ID, $crudEditor->getId(), $comparison);
        } elseif ($crudEditor instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CrudEditorBlockTableMap::COL_CRUD_EDITOR_ID, $crudEditor->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCrudEditor() only accepts arguments of type \Model\Setting\CrudManager\CrudEditor or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudEditor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function joinCrudEditor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudEditor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudEditor');
        }

        return $this;
    }

    /**
     * Use the CrudEditor relation CrudEditor object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudEditorQuery A secondary query class using the current class as primary query
     */
    public function useCrudEditorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudEditor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudEditor', '\Model\Setting\CrudManager\CrudEditorQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudEditorBlockField object
     *
     * @param \Model\Setting\CrudManager\CrudEditorBlockField|ObjectCollection $crudEditorBlockField the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function filterByCrudEditorBlockField($crudEditorBlockField, $comparison = null)
    {
        if ($crudEditorBlockField instanceof \Model\Setting\CrudManager\CrudEditorBlockField) {
            return $this
                ->addUsingAlias(CrudEditorBlockTableMap::COL_ID, $crudEditorBlockField->getCrudEditorBlockId(), $comparison);
        } elseif ($crudEditorBlockField instanceof ObjectCollection) {
            return $this
                ->useCrudEditorBlockFieldQuery()
                ->filterByPrimaryKeys($crudEditorBlockField->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudEditorBlockField() only accepts arguments of type \Model\Setting\CrudManager\CrudEditorBlockField or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudEditorBlockField relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function joinCrudEditorBlockField($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudEditorBlockField');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudEditorBlockField');
        }

        return $this;
    }

    /**
     * Use the CrudEditorBlockField relation CrudEditorBlockField object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudEditorBlockFieldQuery A secondary query class using the current class as primary query
     */
    public function useCrudEditorBlockFieldQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudEditorBlockField($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudEditorBlockField', '\Model\Setting\CrudManager\CrudEditorBlockFieldQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCrudEditorBlock $crudEditorBlock Object to remove from the list of results
     *
     * @return $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function prune($crudEditorBlock = null)
    {
        if ($crudEditorBlock) {
            $this->addUsingAlias(CrudEditorBlockTableMap::COL_ID, $crudEditorBlock->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the crud_editor_block table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorBlockTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CrudEditorBlockTableMap::clearInstancePool();
            CrudEditorBlockTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorBlockTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CrudEditorBlockTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CrudEditorBlockTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CrudEditorBlockTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudEditorBlockTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudEditorBlockTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudEditorBlockTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudEditorBlockTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudEditorBlockTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCrudEditorBlockQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudEditorBlockTableMap::COL_CREATED_AT);
    }

} // CrudEditorBlockQuery
