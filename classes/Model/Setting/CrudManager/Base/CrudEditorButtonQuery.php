<?php

namespace Model\Setting\CrudManager\Base;

use \Exception;
use \PDO;
use Model\Setting\CrudManager\CrudEditorButton as ChildCrudEditorButton;
use Model\Setting\CrudManager\CrudEditorButtonQuery as ChildCrudEditorButtonQuery;
use Model\Setting\CrudManager\Map\CrudEditorButtonTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'crud_editor_button' table.
 *
 *
 *
 * @method     ChildCrudEditorButtonQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCrudEditorButtonQuery orderByCrudEditorId($order = Criteria::ASC) Order by the crud_editor_id column
 * @method     ChildCrudEditorButtonQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildCrudEditorButtonQuery orderByIconBeforeClick($order = Criteria::ASC) Order by the icon_before_click column
 * @method     ChildCrudEditorButtonQuery orderByIconAfterClick($order = Criteria::ASC) Order by the icon_after_click column
 * @method     ChildCrudEditorButtonQuery orderByColorBeforeClick($order = Criteria::ASC) Order by the color_before_click column
 * @method     ChildCrudEditorButtonQuery orderByColorAfterClick($order = Criteria::ASC) Order by the color_after_click column
 * @method     ChildCrudEditorButtonQuery orderByIsReusable($order = Criteria::ASC) Order by the is_reusable column
 * @method     ChildCrudEditorButtonQuery orderBySorting($order = Criteria::ASC) Order by the sorting column
 * @method     ChildCrudEditorButtonQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCrudEditorButtonQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCrudEditorButtonQuery groupById() Group by the id column
 * @method     ChildCrudEditorButtonQuery groupByCrudEditorId() Group by the crud_editor_id column
 * @method     ChildCrudEditorButtonQuery groupByTitle() Group by the title column
 * @method     ChildCrudEditorButtonQuery groupByIconBeforeClick() Group by the icon_before_click column
 * @method     ChildCrudEditorButtonQuery groupByIconAfterClick() Group by the icon_after_click column
 * @method     ChildCrudEditorButtonQuery groupByColorBeforeClick() Group by the color_before_click column
 * @method     ChildCrudEditorButtonQuery groupByColorAfterClick() Group by the color_after_click column
 * @method     ChildCrudEditorButtonQuery groupByIsReusable() Group by the is_reusable column
 * @method     ChildCrudEditorButtonQuery groupBySorting() Group by the sorting column
 * @method     ChildCrudEditorButtonQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCrudEditorButtonQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCrudEditorButtonQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCrudEditorButtonQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCrudEditorButtonQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCrudEditorButtonQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCrudEditorButtonQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCrudEditorButtonQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCrudEditorButtonQuery leftJoinCrudEditor($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudEditor relation
 * @method     ChildCrudEditorButtonQuery rightJoinCrudEditor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudEditor relation
 * @method     ChildCrudEditorButtonQuery innerJoinCrudEditor($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudEditor relation
 *
 * @method     ChildCrudEditorButtonQuery joinWithCrudEditor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudEditor relation
 *
 * @method     ChildCrudEditorButtonQuery leftJoinWithCrudEditor() Adds a LEFT JOIN clause and with to the query using the CrudEditor relation
 * @method     ChildCrudEditorButtonQuery rightJoinWithCrudEditor() Adds a RIGHT JOIN clause and with to the query using the CrudEditor relation
 * @method     ChildCrudEditorButtonQuery innerJoinWithCrudEditor() Adds a INNER JOIN clause and with to the query using the CrudEditor relation
 *
 * @method     ChildCrudEditorButtonQuery leftJoinCrudEditorButtonVisibileFilter($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudEditorButtonVisibileFilter relation
 * @method     ChildCrudEditorButtonQuery rightJoinCrudEditorButtonVisibileFilter($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudEditorButtonVisibileFilter relation
 * @method     ChildCrudEditorButtonQuery innerJoinCrudEditorButtonVisibileFilter($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudEditorButtonVisibileFilter relation
 *
 * @method     ChildCrudEditorButtonQuery joinWithCrudEditorButtonVisibileFilter($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudEditorButtonVisibileFilter relation
 *
 * @method     ChildCrudEditorButtonQuery leftJoinWithCrudEditorButtonVisibileFilter() Adds a LEFT JOIN clause and with to the query using the CrudEditorButtonVisibileFilter relation
 * @method     ChildCrudEditorButtonQuery rightJoinWithCrudEditorButtonVisibileFilter() Adds a RIGHT JOIN clause and with to the query using the CrudEditorButtonVisibileFilter relation
 * @method     ChildCrudEditorButtonQuery innerJoinWithCrudEditorButtonVisibileFilter() Adds a INNER JOIN clause and with to the query using the CrudEditorButtonVisibileFilter relation
 *
 * @method     ChildCrudEditorButtonQuery leftJoinCrudEditorButtonEvent($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudEditorButtonEvent relation
 * @method     ChildCrudEditorButtonQuery rightJoinCrudEditorButtonEvent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudEditorButtonEvent relation
 * @method     ChildCrudEditorButtonQuery innerJoinCrudEditorButtonEvent($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudEditorButtonEvent relation
 *
 * @method     ChildCrudEditorButtonQuery joinWithCrudEditorButtonEvent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudEditorButtonEvent relation
 *
 * @method     ChildCrudEditorButtonQuery leftJoinWithCrudEditorButtonEvent() Adds a LEFT JOIN clause and with to the query using the CrudEditorButtonEvent relation
 * @method     ChildCrudEditorButtonQuery rightJoinWithCrudEditorButtonEvent() Adds a RIGHT JOIN clause and with to the query using the CrudEditorButtonEvent relation
 * @method     ChildCrudEditorButtonQuery innerJoinWithCrudEditorButtonEvent() Adds a INNER JOIN clause and with to the query using the CrudEditorButtonEvent relation
 *
 * @method     \Model\Setting\CrudManager\CrudEditorQuery|\Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery|\Model\Setting\CrudManager\CrudEditorButtonEventQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCrudEditorButton findOne(ConnectionInterface $con = null) Return the first ChildCrudEditorButton matching the query
 * @method     ChildCrudEditorButton findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCrudEditorButton matching the query, or a new ChildCrudEditorButton object populated from the query conditions when no match is found
 *
 * @method     ChildCrudEditorButton findOneById(int $id) Return the first ChildCrudEditorButton filtered by the id column
 * @method     ChildCrudEditorButton findOneByCrudEditorId(int $crud_editor_id) Return the first ChildCrudEditorButton filtered by the crud_editor_id column
 * @method     ChildCrudEditorButton findOneByTitle(string $title) Return the first ChildCrudEditorButton filtered by the title column
 * @method     ChildCrudEditorButton findOneByIconBeforeClick(string $icon_before_click) Return the first ChildCrudEditorButton filtered by the icon_before_click column
 * @method     ChildCrudEditorButton findOneByIconAfterClick(string $icon_after_click) Return the first ChildCrudEditorButton filtered by the icon_after_click column
 * @method     ChildCrudEditorButton findOneByColorBeforeClick(string $color_before_click) Return the first ChildCrudEditorButton filtered by the color_before_click column
 * @method     ChildCrudEditorButton findOneByColorAfterClick(string $color_after_click) Return the first ChildCrudEditorButton filtered by the color_after_click column
 * @method     ChildCrudEditorButton findOneByIsReusable(boolean $is_reusable) Return the first ChildCrudEditorButton filtered by the is_reusable column
 * @method     ChildCrudEditorButton findOneBySorting(int $sorting) Return the first ChildCrudEditorButton filtered by the sorting column
 * @method     ChildCrudEditorButton findOneByCreatedAt(string $created_at) Return the first ChildCrudEditorButton filtered by the created_at column
 * @method     ChildCrudEditorButton findOneByUpdatedAt(string $updated_at) Return the first ChildCrudEditorButton filtered by the updated_at column *

 * @method     ChildCrudEditorButton requirePk($key, ConnectionInterface $con = null) Return the ChildCrudEditorButton by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButton requireOne(ConnectionInterface $con = null) Return the first ChildCrudEditorButton matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudEditorButton requireOneById(int $id) Return the first ChildCrudEditorButton filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButton requireOneByCrudEditorId(int $crud_editor_id) Return the first ChildCrudEditorButton filtered by the crud_editor_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButton requireOneByTitle(string $title) Return the first ChildCrudEditorButton filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButton requireOneByIconBeforeClick(string $icon_before_click) Return the first ChildCrudEditorButton filtered by the icon_before_click column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButton requireOneByIconAfterClick(string $icon_after_click) Return the first ChildCrudEditorButton filtered by the icon_after_click column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButton requireOneByColorBeforeClick(string $color_before_click) Return the first ChildCrudEditorButton filtered by the color_before_click column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButton requireOneByColorAfterClick(string $color_after_click) Return the first ChildCrudEditorButton filtered by the color_after_click column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButton requireOneByIsReusable(boolean $is_reusable) Return the first ChildCrudEditorButton filtered by the is_reusable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButton requireOneBySorting(int $sorting) Return the first ChildCrudEditorButton filtered by the sorting column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButton requireOneByCreatedAt(string $created_at) Return the first ChildCrudEditorButton filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButton requireOneByUpdatedAt(string $updated_at) Return the first ChildCrudEditorButton filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudEditorButton[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCrudEditorButton objects based on current ModelCriteria
 * @method     ChildCrudEditorButton[]|ObjectCollection findById(int $id) Return ChildCrudEditorButton objects filtered by the id column
 * @method     ChildCrudEditorButton[]|ObjectCollection findByCrudEditorId(int $crud_editor_id) Return ChildCrudEditorButton objects filtered by the crud_editor_id column
 * @method     ChildCrudEditorButton[]|ObjectCollection findByTitle(string $title) Return ChildCrudEditorButton objects filtered by the title column
 * @method     ChildCrudEditorButton[]|ObjectCollection findByIconBeforeClick(string $icon_before_click) Return ChildCrudEditorButton objects filtered by the icon_before_click column
 * @method     ChildCrudEditorButton[]|ObjectCollection findByIconAfterClick(string $icon_after_click) Return ChildCrudEditorButton objects filtered by the icon_after_click column
 * @method     ChildCrudEditorButton[]|ObjectCollection findByColorBeforeClick(string $color_before_click) Return ChildCrudEditorButton objects filtered by the color_before_click column
 * @method     ChildCrudEditorButton[]|ObjectCollection findByColorAfterClick(string $color_after_click) Return ChildCrudEditorButton objects filtered by the color_after_click column
 * @method     ChildCrudEditorButton[]|ObjectCollection findByIsReusable(boolean $is_reusable) Return ChildCrudEditorButton objects filtered by the is_reusable column
 * @method     ChildCrudEditorButton[]|ObjectCollection findBySorting(int $sorting) Return ChildCrudEditorButton objects filtered by the sorting column
 * @method     ChildCrudEditorButton[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCrudEditorButton objects filtered by the created_at column
 * @method     ChildCrudEditorButton[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCrudEditorButton objects filtered by the updated_at column
 * @method     ChildCrudEditorButton[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CrudEditorButtonQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\CrudManager\Base\CrudEditorButtonQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\CrudManager\\CrudEditorButton', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCrudEditorButtonQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCrudEditorButtonQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCrudEditorButtonQuery) {
            return $criteria;
        }
        $query = new ChildCrudEditorButtonQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCrudEditorButton|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CrudEditorButtonTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CrudEditorButtonTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudEditorButton A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, crud_editor_id, title, icon_before_click, icon_after_click, color_before_click, color_after_click, is_reusable, sorting, created_at, updated_at FROM crud_editor_button WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCrudEditorButton $obj */
            $obj = new ChildCrudEditorButton();
            $obj->hydrate($row);
            CrudEditorButtonTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCrudEditorButton|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CrudEditorButtonTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CrudEditorButtonTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the crud_editor_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCrudEditorId(1234); // WHERE crud_editor_id = 1234
     * $query->filterByCrudEditorId(array(12, 34)); // WHERE crud_editor_id IN (12, 34)
     * $query->filterByCrudEditorId(array('min' => 12)); // WHERE crud_editor_id > 12
     * </code>
     *
     * @see       filterByCrudEditor()
     *
     * @param     mixed $crudEditorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByCrudEditorId($crudEditorId = null, $comparison = null)
    {
        if (is_array($crudEditorId)) {
            $useMinMax = false;
            if (isset($crudEditorId['min'])) {
                $this->addUsingAlias(CrudEditorButtonTableMap::COL_CRUD_EDITOR_ID, $crudEditorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($crudEditorId['max'])) {
                $this->addUsingAlias(CrudEditorButtonTableMap::COL_CRUD_EDITOR_ID, $crudEditorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_CRUD_EDITOR_ID, $crudEditorId, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the icon_before_click column
     *
     * Example usage:
     * <code>
     * $query->filterByIconBeforeClick('fooValue');   // WHERE icon_before_click = 'fooValue'
     * $query->filterByIconBeforeClick('%fooValue%', Criteria::LIKE); // WHERE icon_before_click LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iconBeforeClick The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByIconBeforeClick($iconBeforeClick = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iconBeforeClick)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_ICON_BEFORE_CLICK, $iconBeforeClick, $comparison);
    }

    /**
     * Filter the query on the icon_after_click column
     *
     * Example usage:
     * <code>
     * $query->filterByIconAfterClick('fooValue');   // WHERE icon_after_click = 'fooValue'
     * $query->filterByIconAfterClick('%fooValue%', Criteria::LIKE); // WHERE icon_after_click LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iconAfterClick The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByIconAfterClick($iconAfterClick = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iconAfterClick)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_ICON_AFTER_CLICK, $iconAfterClick, $comparison);
    }

    /**
     * Filter the query on the color_before_click column
     *
     * Example usage:
     * <code>
     * $query->filterByColorBeforeClick('fooValue');   // WHERE color_before_click = 'fooValue'
     * $query->filterByColorBeforeClick('%fooValue%', Criteria::LIKE); // WHERE color_before_click LIKE '%fooValue%'
     * </code>
     *
     * @param     string $colorBeforeClick The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByColorBeforeClick($colorBeforeClick = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($colorBeforeClick)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_COLOR_BEFORE_CLICK, $colorBeforeClick, $comparison);
    }

    /**
     * Filter the query on the color_after_click column
     *
     * Example usage:
     * <code>
     * $query->filterByColorAfterClick('fooValue');   // WHERE color_after_click = 'fooValue'
     * $query->filterByColorAfterClick('%fooValue%', Criteria::LIKE); // WHERE color_after_click LIKE '%fooValue%'
     * </code>
     *
     * @param     string $colorAfterClick The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByColorAfterClick($colorAfterClick = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($colorAfterClick)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_COLOR_AFTER_CLICK, $colorAfterClick, $comparison);
    }

    /**
     * Filter the query on the is_reusable column
     *
     * Example usage:
     * <code>
     * $query->filterByIsReusable(true); // WHERE is_reusable = true
     * $query->filterByIsReusable('yes'); // WHERE is_reusable = true
     * </code>
     *
     * @param     boolean|string $isReusable The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByIsReusable($isReusable = null, $comparison = null)
    {
        if (is_string($isReusable)) {
            $isReusable = in_array(strtolower($isReusable), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_IS_REUSABLE, $isReusable, $comparison);
    }

    /**
     * Filter the query on the sorting column
     *
     * Example usage:
     * <code>
     * $query->filterBySorting(1234); // WHERE sorting = 1234
     * $query->filterBySorting(array(12, 34)); // WHERE sorting IN (12, 34)
     * $query->filterBySorting(array('min' => 12)); // WHERE sorting > 12
     * </code>
     *
     * @param     mixed $sorting The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterBySorting($sorting = null, $comparison = null)
    {
        if (is_array($sorting)) {
            $useMinMax = false;
            if (isset($sorting['min'])) {
                $this->addUsingAlias(CrudEditorButtonTableMap::COL_SORTING, $sorting['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sorting['max'])) {
                $this->addUsingAlias(CrudEditorButtonTableMap::COL_SORTING, $sorting['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_SORTING, $sorting, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CrudEditorButtonTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CrudEditorButtonTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CrudEditorButtonTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CrudEditorButtonTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudEditor object
     *
     * @param \Model\Setting\CrudManager\CrudEditor|ObjectCollection $crudEditor The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByCrudEditor($crudEditor, $comparison = null)
    {
        if ($crudEditor instanceof \Model\Setting\CrudManager\CrudEditor) {
            return $this
                ->addUsingAlias(CrudEditorButtonTableMap::COL_CRUD_EDITOR_ID, $crudEditor->getId(), $comparison);
        } elseif ($crudEditor instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CrudEditorButtonTableMap::COL_CRUD_EDITOR_ID, $crudEditor->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCrudEditor() only accepts arguments of type \Model\Setting\CrudManager\CrudEditor or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudEditor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function joinCrudEditor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudEditor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudEditor');
        }

        return $this;
    }

    /**
     * Use the CrudEditor relation CrudEditor object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudEditorQuery A secondary query class using the current class as primary query
     */
    public function useCrudEditorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudEditor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudEditor', '\Model\Setting\CrudManager\CrudEditorQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudEditorButtonVisibileFilter object
     *
     * @param \Model\Setting\CrudManager\CrudEditorButtonVisibileFilter|ObjectCollection $crudEditorButtonVisibileFilter the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByCrudEditorButtonVisibileFilter($crudEditorButtonVisibileFilter, $comparison = null)
    {
        if ($crudEditorButtonVisibileFilter instanceof \Model\Setting\CrudManager\CrudEditorButtonVisibileFilter) {
            return $this
                ->addUsingAlias(CrudEditorButtonTableMap::COL_ID, $crudEditorButtonVisibileFilter->getCrudEditorButtonId(), $comparison);
        } elseif ($crudEditorButtonVisibileFilter instanceof ObjectCollection) {
            return $this
                ->useCrudEditorButtonVisibileFilterQuery()
                ->filterByPrimaryKeys($crudEditorButtonVisibileFilter->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudEditorButtonVisibileFilter() only accepts arguments of type \Model\Setting\CrudManager\CrudEditorButtonVisibileFilter or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudEditorButtonVisibileFilter relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function joinCrudEditorButtonVisibileFilter($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudEditorButtonVisibileFilter');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudEditorButtonVisibileFilter');
        }

        return $this;
    }

    /**
     * Use the CrudEditorButtonVisibileFilter relation CrudEditorButtonVisibileFilter object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery A secondary query class using the current class as primary query
     */
    public function useCrudEditorButtonVisibileFilterQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCrudEditorButtonVisibileFilter($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudEditorButtonVisibileFilter', '\Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudEditorButtonEvent object
     *
     * @param \Model\Setting\CrudManager\CrudEditorButtonEvent|ObjectCollection $crudEditorButtonEvent the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function filterByCrudEditorButtonEvent($crudEditorButtonEvent, $comparison = null)
    {
        if ($crudEditorButtonEvent instanceof \Model\Setting\CrudManager\CrudEditorButtonEvent) {
            return $this
                ->addUsingAlias(CrudEditorButtonTableMap::COL_ID, $crudEditorButtonEvent->getCrudEditorButtonId(), $comparison);
        } elseif ($crudEditorButtonEvent instanceof ObjectCollection) {
            return $this
                ->useCrudEditorButtonEventQuery()
                ->filterByPrimaryKeys($crudEditorButtonEvent->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudEditorButtonEvent() only accepts arguments of type \Model\Setting\CrudManager\CrudEditorButtonEvent or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudEditorButtonEvent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function joinCrudEditorButtonEvent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudEditorButtonEvent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudEditorButtonEvent');
        }

        return $this;
    }

    /**
     * Use the CrudEditorButtonEvent relation CrudEditorButtonEvent object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudEditorButtonEventQuery A secondary query class using the current class as primary query
     */
    public function useCrudEditorButtonEventQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCrudEditorButtonEvent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudEditorButtonEvent', '\Model\Setting\CrudManager\CrudEditorButtonEventQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCrudEditorButton $crudEditorButton Object to remove from the list of results
     *
     * @return $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function prune($crudEditorButton = null)
    {
        if ($crudEditorButton) {
            $this->addUsingAlias(CrudEditorButtonTableMap::COL_ID, $crudEditorButton->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the crud_editor_button table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorButtonTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CrudEditorButtonTableMap::clearInstancePool();
            CrudEditorButtonTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorButtonTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CrudEditorButtonTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CrudEditorButtonTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CrudEditorButtonTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudEditorButtonTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudEditorButtonTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudEditorButtonTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudEditorButtonTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCrudEditorButtonQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudEditorButtonTableMap::COL_CREATED_AT);
    }

} // CrudEditorButtonQuery
