<?php

namespace Model\Setting\CrudManager\Base;

use \Exception;
use \PDO;
use Model\Setting\CrudManager\CrudConfig as ChildCrudConfig;
use Model\Setting\CrudManager\CrudConfigQuery as ChildCrudConfigQuery;
use Model\Setting\CrudManager\Map\CrudConfigTableMap;
use Model\System\Event\EventHandler;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'crud_config' table.
 *
 *
 *
 * @method     ChildCrudConfigQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCrudConfigQuery orderByManagerName($order = Criteria::ASC) Order by the manager_name column
 * @method     ChildCrudConfigQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCrudConfigQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCrudConfigQuery groupById() Group by the id column
 * @method     ChildCrudConfigQuery groupByManagerName() Group by the manager_name column
 * @method     ChildCrudConfigQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCrudConfigQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCrudConfigQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCrudConfigQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCrudConfigQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCrudConfigQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCrudConfigQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCrudConfigQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCrudConfigQuery leftJoinEventHandler($relationAlias = null) Adds a LEFT JOIN clause to the query using the EventHandler relation
 * @method     ChildCrudConfigQuery rightJoinEventHandler($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EventHandler relation
 * @method     ChildCrudConfigQuery innerJoinEventHandler($relationAlias = null) Adds a INNER JOIN clause to the query using the EventHandler relation
 *
 * @method     ChildCrudConfigQuery joinWithEventHandler($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EventHandler relation
 *
 * @method     ChildCrudConfigQuery leftJoinWithEventHandler() Adds a LEFT JOIN clause and with to the query using the EventHandler relation
 * @method     ChildCrudConfigQuery rightJoinWithEventHandler() Adds a RIGHT JOIN clause and with to the query using the EventHandler relation
 * @method     ChildCrudConfigQuery innerJoinWithEventHandler() Adds a INNER JOIN clause and with to the query using the EventHandler relation
 *
 * @method     ChildCrudConfigQuery leftJoinCrudView($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudView relation
 * @method     ChildCrudConfigQuery rightJoinCrudView($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudView relation
 * @method     ChildCrudConfigQuery innerJoinCrudView($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudView relation
 *
 * @method     ChildCrudConfigQuery joinWithCrudView($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudView relation
 *
 * @method     ChildCrudConfigQuery leftJoinWithCrudView() Adds a LEFT JOIN clause and with to the query using the CrudView relation
 * @method     ChildCrudConfigQuery rightJoinWithCrudView() Adds a RIGHT JOIN clause and with to the query using the CrudView relation
 * @method     ChildCrudConfigQuery innerJoinWithCrudView() Adds a INNER JOIN clause and with to the query using the CrudView relation
 *
 * @method     ChildCrudConfigQuery leftJoinCrudEditor($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudEditor relation
 * @method     ChildCrudConfigQuery rightJoinCrudEditor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudEditor relation
 * @method     ChildCrudConfigQuery innerJoinCrudEditor($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudEditor relation
 *
 * @method     ChildCrudConfigQuery joinWithCrudEditor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudEditor relation
 *
 * @method     ChildCrudConfigQuery leftJoinWithCrudEditor() Adds a LEFT JOIN clause and with to the query using the CrudEditor relation
 * @method     ChildCrudConfigQuery rightJoinWithCrudEditor() Adds a RIGHT JOIN clause and with to the query using the CrudEditor relation
 * @method     ChildCrudConfigQuery innerJoinWithCrudEditor() Adds a INNER JOIN clause and with to the query using the CrudEditor relation
 *
 * @method     \Model\System\Event\EventHandlerQuery|\Model\Setting\CrudManager\CrudViewQuery|\Model\Setting\CrudManager\CrudEditorQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCrudConfig findOne(ConnectionInterface $con = null) Return the first ChildCrudConfig matching the query
 * @method     ChildCrudConfig findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCrudConfig matching the query, or a new ChildCrudConfig object populated from the query conditions when no match is found
 *
 * @method     ChildCrudConfig findOneById(int $id) Return the first ChildCrudConfig filtered by the id column
 * @method     ChildCrudConfig findOneByManagerName(string $manager_name) Return the first ChildCrudConfig filtered by the manager_name column
 * @method     ChildCrudConfig findOneByCreatedAt(string $created_at) Return the first ChildCrudConfig filtered by the created_at column
 * @method     ChildCrudConfig findOneByUpdatedAt(string $updated_at) Return the first ChildCrudConfig filtered by the updated_at column *

 * @method     ChildCrudConfig requirePk($key, ConnectionInterface $con = null) Return the ChildCrudConfig by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudConfig requireOne(ConnectionInterface $con = null) Return the first ChildCrudConfig matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudConfig requireOneById(int $id) Return the first ChildCrudConfig filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudConfig requireOneByManagerName(string $manager_name) Return the first ChildCrudConfig filtered by the manager_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudConfig requireOneByCreatedAt(string $created_at) Return the first ChildCrudConfig filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudConfig requireOneByUpdatedAt(string $updated_at) Return the first ChildCrudConfig filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudConfig[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCrudConfig objects based on current ModelCriteria
 * @method     ChildCrudConfig[]|ObjectCollection findById(int $id) Return ChildCrudConfig objects filtered by the id column
 * @method     ChildCrudConfig[]|ObjectCollection findByManagerName(string $manager_name) Return ChildCrudConfig objects filtered by the manager_name column
 * @method     ChildCrudConfig[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCrudConfig objects filtered by the created_at column
 * @method     ChildCrudConfig[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCrudConfig objects filtered by the updated_at column
 * @method     ChildCrudConfig[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CrudConfigQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\CrudManager\Base\CrudConfigQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\CrudManager\\CrudConfig', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCrudConfigQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCrudConfigQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCrudConfigQuery) {
            return $criteria;
        }
        $query = new ChildCrudConfigQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCrudConfig|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CrudConfigTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CrudConfigTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudConfig A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, manager_name, created_at, updated_at FROM crud_config WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCrudConfig $obj */
            $obj = new ChildCrudConfig();
            $obj->hydrate($row);
            CrudConfigTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCrudConfig|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CrudConfigTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CrudConfigTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CrudConfigTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CrudConfigTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudConfigTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the manager_name column
     *
     * Example usage:
     * <code>
     * $query->filterByManagerName('fooValue');   // WHERE manager_name = 'fooValue'
     * $query->filterByManagerName('%fooValue%', Criteria::LIKE); // WHERE manager_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $managerName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function filterByManagerName($managerName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($managerName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudConfigTableMap::COL_MANAGER_NAME, $managerName, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CrudConfigTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CrudConfigTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudConfigTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CrudConfigTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CrudConfigTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudConfigTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\Event\EventHandler object
     *
     * @param \Model\System\Event\EventHandler|ObjectCollection $eventHandler the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudConfigQuery The current query, for fluid interface
     */
    public function filterByEventHandler($eventHandler, $comparison = null)
    {
        if ($eventHandler instanceof \Model\System\Event\EventHandler) {
            return $this
                ->addUsingAlias(CrudConfigTableMap::COL_ID, $eventHandler->getCrudConfigId(), $comparison);
        } elseif ($eventHandler instanceof ObjectCollection) {
            return $this
                ->useEventHandlerQuery()
                ->filterByPrimaryKeys($eventHandler->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEventHandler() only accepts arguments of type \Model\System\Event\EventHandler or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EventHandler relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function joinEventHandler($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EventHandler');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EventHandler');
        }

        return $this;
    }

    /**
     * Use the EventHandler relation EventHandler object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\Event\EventHandlerQuery A secondary query class using the current class as primary query
     */
    public function useEventHandlerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEventHandler($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EventHandler', '\Model\System\Event\EventHandlerQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudView object
     *
     * @param \Model\Setting\CrudManager\CrudView|ObjectCollection $crudView the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudConfigQuery The current query, for fluid interface
     */
    public function filterByCrudView($crudView, $comparison = null)
    {
        if ($crudView instanceof \Model\Setting\CrudManager\CrudView) {
            return $this
                ->addUsingAlias(CrudConfigTableMap::COL_ID, $crudView->getCrudConfigId(), $comparison);
        } elseif ($crudView instanceof ObjectCollection) {
            return $this
                ->useCrudViewQuery()
                ->filterByPrimaryKeys($crudView->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudView() only accepts arguments of type \Model\Setting\CrudManager\CrudView or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudView relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function joinCrudView($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudView');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudView');
        }

        return $this;
    }

    /**
     * Use the CrudView relation CrudView object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudViewQuery A secondary query class using the current class as primary query
     */
    public function useCrudViewQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudView($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudView', '\Model\Setting\CrudManager\CrudViewQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudEditor object
     *
     * @param \Model\Setting\CrudManager\CrudEditor|ObjectCollection $crudEditor the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCrudConfigQuery The current query, for fluid interface
     */
    public function filterByCrudEditor($crudEditor, $comparison = null)
    {
        if ($crudEditor instanceof \Model\Setting\CrudManager\CrudEditor) {
            return $this
                ->addUsingAlias(CrudConfigTableMap::COL_ID, $crudEditor->getCrudConfigId(), $comparison);
        } elseif ($crudEditor instanceof ObjectCollection) {
            return $this
                ->useCrudEditorQuery()
                ->filterByPrimaryKeys($crudEditor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudEditor() only accepts arguments of type \Model\Setting\CrudManager\CrudEditor or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudEditor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function joinCrudEditor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudEditor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudEditor');
        }

        return $this;
    }

    /**
     * Use the CrudEditor relation CrudEditor object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudEditorQuery A secondary query class using the current class as primary query
     */
    public function useCrudEditorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudEditor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudEditor', '\Model\Setting\CrudManager\CrudEditorQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCrudConfig $crudConfig Object to remove from the list of results
     *
     * @return $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function prune($crudConfig = null)
    {
        if ($crudConfig) {
            $this->addUsingAlias(CrudConfigTableMap::COL_ID, $crudConfig->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the crud_config table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudConfigTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CrudConfigTableMap::clearInstancePool();
            CrudConfigTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudConfigTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CrudConfigTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CrudConfigTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CrudConfigTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudConfigTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudConfigTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudConfigTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudConfigTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudConfigTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCrudConfigQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudConfigTableMap::COL_CREATED_AT);
    }

} // CrudConfigQuery
