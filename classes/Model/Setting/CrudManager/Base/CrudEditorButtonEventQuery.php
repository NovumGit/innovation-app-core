<?php

namespace Model\Setting\CrudManager\Base;

use \Exception;
use \PDO;
use Model\Setting\CrudManager\CrudEditorButtonEvent as ChildCrudEditorButtonEvent;
use Model\Setting\CrudManager\CrudEditorButtonEventQuery as ChildCrudEditorButtonEventQuery;
use Model\Setting\CrudManager\Map\CrudEditorButtonEventTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'crud_editor_button_event' table.
 *
 *
 *
 * @method     ChildCrudEditorButtonEventQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCrudEditorButtonEventQuery orderByCrudEditorButtonId($order = Criteria::ASC) Order by the crud_editor_button_id column
 * @method     ChildCrudEditorButtonEventQuery orderByEventClass($order = Criteria::ASC) Order by the event_class column
 * @method     ChildCrudEditorButtonEventQuery orderBySorting($order = Criteria::ASC) Order by the sorting column
 * @method     ChildCrudEditorButtonEventQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCrudEditorButtonEventQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCrudEditorButtonEventQuery groupById() Group by the id column
 * @method     ChildCrudEditorButtonEventQuery groupByCrudEditorButtonId() Group by the crud_editor_button_id column
 * @method     ChildCrudEditorButtonEventQuery groupByEventClass() Group by the event_class column
 * @method     ChildCrudEditorButtonEventQuery groupBySorting() Group by the sorting column
 * @method     ChildCrudEditorButtonEventQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCrudEditorButtonEventQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCrudEditorButtonEventQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCrudEditorButtonEventQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCrudEditorButtonEventQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCrudEditorButtonEventQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCrudEditorButtonEventQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCrudEditorButtonEventQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCrudEditorButtonEventQuery leftJoinCrudEditorButton($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudEditorButton relation
 * @method     ChildCrudEditorButtonEventQuery rightJoinCrudEditorButton($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudEditorButton relation
 * @method     ChildCrudEditorButtonEventQuery innerJoinCrudEditorButton($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudEditorButton relation
 *
 * @method     ChildCrudEditorButtonEventQuery joinWithCrudEditorButton($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudEditorButton relation
 *
 * @method     ChildCrudEditorButtonEventQuery leftJoinWithCrudEditorButton() Adds a LEFT JOIN clause and with to the query using the CrudEditorButton relation
 * @method     ChildCrudEditorButtonEventQuery rightJoinWithCrudEditorButton() Adds a RIGHT JOIN clause and with to the query using the CrudEditorButton relation
 * @method     ChildCrudEditorButtonEventQuery innerJoinWithCrudEditorButton() Adds a INNER JOIN clause and with to the query using the CrudEditorButton relation
 *
 * @method     \Model\Setting\CrudManager\CrudEditorButtonQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCrudEditorButtonEvent findOne(ConnectionInterface $con = null) Return the first ChildCrudEditorButtonEvent matching the query
 * @method     ChildCrudEditorButtonEvent findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCrudEditorButtonEvent matching the query, or a new ChildCrudEditorButtonEvent object populated from the query conditions when no match is found
 *
 * @method     ChildCrudEditorButtonEvent findOneById(int $id) Return the first ChildCrudEditorButtonEvent filtered by the id column
 * @method     ChildCrudEditorButtonEvent findOneByCrudEditorButtonId(int $crud_editor_button_id) Return the first ChildCrudEditorButtonEvent filtered by the crud_editor_button_id column
 * @method     ChildCrudEditorButtonEvent findOneByEventClass(string $event_class) Return the first ChildCrudEditorButtonEvent filtered by the event_class column
 * @method     ChildCrudEditorButtonEvent findOneBySorting(int $sorting) Return the first ChildCrudEditorButtonEvent filtered by the sorting column
 * @method     ChildCrudEditorButtonEvent findOneByCreatedAt(string $created_at) Return the first ChildCrudEditorButtonEvent filtered by the created_at column
 * @method     ChildCrudEditorButtonEvent findOneByUpdatedAt(string $updated_at) Return the first ChildCrudEditorButtonEvent filtered by the updated_at column *

 * @method     ChildCrudEditorButtonEvent requirePk($key, ConnectionInterface $con = null) Return the ChildCrudEditorButtonEvent by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButtonEvent requireOne(ConnectionInterface $con = null) Return the first ChildCrudEditorButtonEvent matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudEditorButtonEvent requireOneById(int $id) Return the first ChildCrudEditorButtonEvent filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButtonEvent requireOneByCrudEditorButtonId(int $crud_editor_button_id) Return the first ChildCrudEditorButtonEvent filtered by the crud_editor_button_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButtonEvent requireOneByEventClass(string $event_class) Return the first ChildCrudEditorButtonEvent filtered by the event_class column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButtonEvent requireOneBySorting(int $sorting) Return the first ChildCrudEditorButtonEvent filtered by the sorting column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButtonEvent requireOneByCreatedAt(string $created_at) Return the first ChildCrudEditorButtonEvent filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudEditorButtonEvent requireOneByUpdatedAt(string $updated_at) Return the first ChildCrudEditorButtonEvent filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudEditorButtonEvent[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCrudEditorButtonEvent objects based on current ModelCriteria
 * @method     ChildCrudEditorButtonEvent[]|ObjectCollection findById(int $id) Return ChildCrudEditorButtonEvent objects filtered by the id column
 * @method     ChildCrudEditorButtonEvent[]|ObjectCollection findByCrudEditorButtonId(int $crud_editor_button_id) Return ChildCrudEditorButtonEvent objects filtered by the crud_editor_button_id column
 * @method     ChildCrudEditorButtonEvent[]|ObjectCollection findByEventClass(string $event_class) Return ChildCrudEditorButtonEvent objects filtered by the event_class column
 * @method     ChildCrudEditorButtonEvent[]|ObjectCollection findBySorting(int $sorting) Return ChildCrudEditorButtonEvent objects filtered by the sorting column
 * @method     ChildCrudEditorButtonEvent[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCrudEditorButtonEvent objects filtered by the created_at column
 * @method     ChildCrudEditorButtonEvent[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCrudEditorButtonEvent objects filtered by the updated_at column
 * @method     ChildCrudEditorButtonEvent[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CrudEditorButtonEventQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\CrudManager\Base\CrudEditorButtonEventQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\CrudManager\\CrudEditorButtonEvent', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCrudEditorButtonEventQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCrudEditorButtonEventQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCrudEditorButtonEventQuery) {
            return $criteria;
        }
        $query = new ChildCrudEditorButtonEventQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCrudEditorButtonEvent|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CrudEditorButtonEventTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CrudEditorButtonEventTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudEditorButtonEvent A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, crud_editor_button_id, event_class, sorting, created_at, updated_at FROM crud_editor_button_event WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCrudEditorButtonEvent $obj */
            $obj = new ChildCrudEditorButtonEvent();
            $obj->hydrate($row);
            CrudEditorButtonEventTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCrudEditorButtonEvent|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the crud_editor_button_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCrudEditorButtonId(1234); // WHERE crud_editor_button_id = 1234
     * $query->filterByCrudEditorButtonId(array(12, 34)); // WHERE crud_editor_button_id IN (12, 34)
     * $query->filterByCrudEditorButtonId(array('min' => 12)); // WHERE crud_editor_button_id > 12
     * </code>
     *
     * @see       filterByCrudEditorButton()
     *
     * @param     mixed $crudEditorButtonId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function filterByCrudEditorButtonId($crudEditorButtonId = null, $comparison = null)
    {
        if (is_array($crudEditorButtonId)) {
            $useMinMax = false;
            if (isset($crudEditorButtonId['min'])) {
                $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_CRUD_EDITOR_BUTTON_ID, $crudEditorButtonId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($crudEditorButtonId['max'])) {
                $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_CRUD_EDITOR_BUTTON_ID, $crudEditorButtonId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_CRUD_EDITOR_BUTTON_ID, $crudEditorButtonId, $comparison);
    }

    /**
     * Filter the query on the event_class column
     *
     * Example usage:
     * <code>
     * $query->filterByEventClass('fooValue');   // WHERE event_class = 'fooValue'
     * $query->filterByEventClass('%fooValue%', Criteria::LIKE); // WHERE event_class LIKE '%fooValue%'
     * </code>
     *
     * @param     string $eventClass The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function filterByEventClass($eventClass = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($eventClass)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_EVENT_CLASS, $eventClass, $comparison);
    }

    /**
     * Filter the query on the sorting column
     *
     * Example usage:
     * <code>
     * $query->filterBySorting(1234); // WHERE sorting = 1234
     * $query->filterBySorting(array(12, 34)); // WHERE sorting IN (12, 34)
     * $query->filterBySorting(array('min' => 12)); // WHERE sorting > 12
     * </code>
     *
     * @param     mixed $sorting The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function filterBySorting($sorting = null, $comparison = null)
    {
        if (is_array($sorting)) {
            $useMinMax = false;
            if (isset($sorting['min'])) {
                $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_SORTING, $sorting['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sorting['max'])) {
                $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_SORTING, $sorting['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_SORTING, $sorting, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudEditorButton object
     *
     * @param \Model\Setting\CrudManager\CrudEditorButton|ObjectCollection $crudEditorButton The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function filterByCrudEditorButton($crudEditorButton, $comparison = null)
    {
        if ($crudEditorButton instanceof \Model\Setting\CrudManager\CrudEditorButton) {
            return $this
                ->addUsingAlias(CrudEditorButtonEventTableMap::COL_CRUD_EDITOR_BUTTON_ID, $crudEditorButton->getId(), $comparison);
        } elseif ($crudEditorButton instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CrudEditorButtonEventTableMap::COL_CRUD_EDITOR_BUTTON_ID, $crudEditorButton->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCrudEditorButton() only accepts arguments of type \Model\Setting\CrudManager\CrudEditorButton or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudEditorButton relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function joinCrudEditorButton($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudEditorButton');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudEditorButton');
        }

        return $this;
    }

    /**
     * Use the CrudEditorButton relation CrudEditorButton object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudEditorButtonQuery A secondary query class using the current class as primary query
     */
    public function useCrudEditorButtonQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCrudEditorButton($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudEditorButton', '\Model\Setting\CrudManager\CrudEditorButtonQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCrudEditorButtonEvent $crudEditorButtonEvent Object to remove from the list of results
     *
     * @return $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function prune($crudEditorButtonEvent = null)
    {
        if ($crudEditorButtonEvent) {
            $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_ID, $crudEditorButtonEvent->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the crud_editor_button_event table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorButtonEventTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CrudEditorButtonEventTableMap::clearInstancePool();
            CrudEditorButtonEventTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorButtonEventTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CrudEditorButtonEventTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CrudEditorButtonEventTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CrudEditorButtonEventTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudEditorButtonEventTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudEditorButtonEventTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudEditorButtonEventTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudEditorButtonEventTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCrudEditorButtonEventQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudEditorButtonEventTableMap::COL_CREATED_AT);
    }

} // CrudEditorButtonEventQuery
