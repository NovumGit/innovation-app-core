<?php

namespace Model\Setting\CrudManager\Base;

use \Exception;
use \PDO;
use Model\Marketing\MailingListsCriteria;
use Model\Setting\CrudManager\FilterOperator as ChildFilterOperator;
use Model\Setting\CrudManager\FilterOperatorQuery as ChildFilterOperatorQuery;
use Model\Setting\CrudManager\Map\FilterOperatorTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'filter_operator' table.
 *
 *
 *
 * @method     ChildFilterOperatorQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildFilterOperatorQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildFilterOperatorQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildFilterOperatorQuery orderByDescriptionSentence($order = Criteria::ASC) Order by the description_sentence column
 * @method     ChildFilterOperatorQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildFilterOperatorQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildFilterOperatorQuery groupById() Group by the id column
 * @method     ChildFilterOperatorQuery groupByName() Group by the name column
 * @method     ChildFilterOperatorQuery groupByDescription() Group by the description column
 * @method     ChildFilterOperatorQuery groupByDescriptionSentence() Group by the description_sentence column
 * @method     ChildFilterOperatorQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildFilterOperatorQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildFilterOperatorQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildFilterOperatorQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildFilterOperatorQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildFilterOperatorQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildFilterOperatorQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildFilterOperatorQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildFilterOperatorQuery leftJoinMailingListsCriteria($relationAlias = null) Adds a LEFT JOIN clause to the query using the MailingListsCriteria relation
 * @method     ChildFilterOperatorQuery rightJoinMailingListsCriteria($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MailingListsCriteria relation
 * @method     ChildFilterOperatorQuery innerJoinMailingListsCriteria($relationAlias = null) Adds a INNER JOIN clause to the query using the MailingListsCriteria relation
 *
 * @method     ChildFilterOperatorQuery joinWithMailingListsCriteria($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MailingListsCriteria relation
 *
 * @method     ChildFilterOperatorQuery leftJoinWithMailingListsCriteria() Adds a LEFT JOIN clause and with to the query using the MailingListsCriteria relation
 * @method     ChildFilterOperatorQuery rightJoinWithMailingListsCriteria() Adds a RIGHT JOIN clause and with to the query using the MailingListsCriteria relation
 * @method     ChildFilterOperatorQuery innerJoinWithMailingListsCriteria() Adds a INNER JOIN clause and with to the query using the MailingListsCriteria relation
 *
 * @method     ChildFilterOperatorQuery leftJoinCrudEditorButtonVisibileFilter($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudEditorButtonVisibileFilter relation
 * @method     ChildFilterOperatorQuery rightJoinCrudEditorButtonVisibileFilter($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudEditorButtonVisibileFilter relation
 * @method     ChildFilterOperatorQuery innerJoinCrudEditorButtonVisibileFilter($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudEditorButtonVisibileFilter relation
 *
 * @method     ChildFilterOperatorQuery joinWithCrudEditorButtonVisibileFilter($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudEditorButtonVisibileFilter relation
 *
 * @method     ChildFilterOperatorQuery leftJoinWithCrudEditorButtonVisibileFilter() Adds a LEFT JOIN clause and with to the query using the CrudEditorButtonVisibileFilter relation
 * @method     ChildFilterOperatorQuery rightJoinWithCrudEditorButtonVisibileFilter() Adds a RIGHT JOIN clause and with to the query using the CrudEditorButtonVisibileFilter relation
 * @method     ChildFilterOperatorQuery innerJoinWithCrudEditorButtonVisibileFilter() Adds a INNER JOIN clause and with to the query using the CrudEditorButtonVisibileFilter relation
 *
 * @method     ChildFilterOperatorQuery leftJoinFilterOperatorDatatype($relationAlias = null) Adds a LEFT JOIN clause to the query using the FilterOperatorDatatype relation
 * @method     ChildFilterOperatorQuery rightJoinFilterOperatorDatatype($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FilterOperatorDatatype relation
 * @method     ChildFilterOperatorQuery innerJoinFilterOperatorDatatype($relationAlias = null) Adds a INNER JOIN clause to the query using the FilterOperatorDatatype relation
 *
 * @method     ChildFilterOperatorQuery joinWithFilterOperatorDatatype($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FilterOperatorDatatype relation
 *
 * @method     ChildFilterOperatorQuery leftJoinWithFilterOperatorDatatype() Adds a LEFT JOIN clause and with to the query using the FilterOperatorDatatype relation
 * @method     ChildFilterOperatorQuery rightJoinWithFilterOperatorDatatype() Adds a RIGHT JOIN clause and with to the query using the FilterOperatorDatatype relation
 * @method     ChildFilterOperatorQuery innerJoinWithFilterOperatorDatatype() Adds a INNER JOIN clause and with to the query using the FilterOperatorDatatype relation
 *
 * @method     ChildFilterOperatorQuery leftJoinCrudViewVisibleFilter($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudViewVisibleFilter relation
 * @method     ChildFilterOperatorQuery rightJoinCrudViewVisibleFilter($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudViewVisibleFilter relation
 * @method     ChildFilterOperatorQuery innerJoinCrudViewVisibleFilter($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudViewVisibleFilter relation
 *
 * @method     ChildFilterOperatorQuery joinWithCrudViewVisibleFilter($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudViewVisibleFilter relation
 *
 * @method     ChildFilterOperatorQuery leftJoinWithCrudViewVisibleFilter() Adds a LEFT JOIN clause and with to the query using the CrudViewVisibleFilter relation
 * @method     ChildFilterOperatorQuery rightJoinWithCrudViewVisibleFilter() Adds a RIGHT JOIN clause and with to the query using the CrudViewVisibleFilter relation
 * @method     ChildFilterOperatorQuery innerJoinWithCrudViewVisibleFilter() Adds a INNER JOIN clause and with to the query using the CrudViewVisibleFilter relation
 *
 * @method     ChildFilterOperatorQuery leftJoinCrudViewHiddenFilter($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudViewHiddenFilter relation
 * @method     ChildFilterOperatorQuery rightJoinCrudViewHiddenFilter($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudViewHiddenFilter relation
 * @method     ChildFilterOperatorQuery innerJoinCrudViewHiddenFilter($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudViewHiddenFilter relation
 *
 * @method     ChildFilterOperatorQuery joinWithCrudViewHiddenFilter($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudViewHiddenFilter relation
 *
 * @method     ChildFilterOperatorQuery leftJoinWithCrudViewHiddenFilter() Adds a LEFT JOIN clause and with to the query using the CrudViewHiddenFilter relation
 * @method     ChildFilterOperatorQuery rightJoinWithCrudViewHiddenFilter() Adds a RIGHT JOIN clause and with to the query using the CrudViewHiddenFilter relation
 * @method     ChildFilterOperatorQuery innerJoinWithCrudViewHiddenFilter() Adds a INNER JOIN clause and with to the query using the CrudViewHiddenFilter relation
 *
 * @method     \Model\Marketing\MailingListsCriteriaQuery|\Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery|\Model\Setting\CrudManager\FilterOperatorDatatypeQuery|\Model\Setting\CrudManager\CrudViewVisibleFilterQuery|\Model\Setting\CrudManager\CrudViewHiddenFilterQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildFilterOperator findOne(ConnectionInterface $con = null) Return the first ChildFilterOperator matching the query
 * @method     ChildFilterOperator findOneOrCreate(ConnectionInterface $con = null) Return the first ChildFilterOperator matching the query, or a new ChildFilterOperator object populated from the query conditions when no match is found
 *
 * @method     ChildFilterOperator findOneById(int $id) Return the first ChildFilterOperator filtered by the id column
 * @method     ChildFilterOperator findOneByName(string $name) Return the first ChildFilterOperator filtered by the name column
 * @method     ChildFilterOperator findOneByDescription(string $description) Return the first ChildFilterOperator filtered by the description column
 * @method     ChildFilterOperator findOneByDescriptionSentence(string $description_sentence) Return the first ChildFilterOperator filtered by the description_sentence column
 * @method     ChildFilterOperator findOneByCreatedAt(string $created_at) Return the first ChildFilterOperator filtered by the created_at column
 * @method     ChildFilterOperator findOneByUpdatedAt(string $updated_at) Return the first ChildFilterOperator filtered by the updated_at column *

 * @method     ChildFilterOperator requirePk($key, ConnectionInterface $con = null) Return the ChildFilterOperator by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFilterOperator requireOne(ConnectionInterface $con = null) Return the first ChildFilterOperator matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFilterOperator requireOneById(int $id) Return the first ChildFilterOperator filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFilterOperator requireOneByName(string $name) Return the first ChildFilterOperator filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFilterOperator requireOneByDescription(string $description) Return the first ChildFilterOperator filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFilterOperator requireOneByDescriptionSentence(string $description_sentence) Return the first ChildFilterOperator filtered by the description_sentence column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFilterOperator requireOneByCreatedAt(string $created_at) Return the first ChildFilterOperator filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildFilterOperator requireOneByUpdatedAt(string $updated_at) Return the first ChildFilterOperator filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildFilterOperator[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildFilterOperator objects based on current ModelCriteria
 * @method     ChildFilterOperator[]|ObjectCollection findById(int $id) Return ChildFilterOperator objects filtered by the id column
 * @method     ChildFilterOperator[]|ObjectCollection findByName(string $name) Return ChildFilterOperator objects filtered by the name column
 * @method     ChildFilterOperator[]|ObjectCollection findByDescription(string $description) Return ChildFilterOperator objects filtered by the description column
 * @method     ChildFilterOperator[]|ObjectCollection findByDescriptionSentence(string $description_sentence) Return ChildFilterOperator objects filtered by the description_sentence column
 * @method     ChildFilterOperator[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildFilterOperator objects filtered by the created_at column
 * @method     ChildFilterOperator[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildFilterOperator objects filtered by the updated_at column
 * @method     ChildFilterOperator[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class FilterOperatorQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\CrudManager\Base\FilterOperatorQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\CrudManager\\FilterOperator', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildFilterOperatorQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildFilterOperatorQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildFilterOperatorQuery) {
            return $criteria;
        }
        $query = new ChildFilterOperatorQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildFilterOperator|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(FilterOperatorTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = FilterOperatorTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildFilterOperator A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, description, description_sentence, created_at, updated_at FROM filter_operator WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildFilterOperator $obj */
            $obj = new ChildFilterOperator();
            $obj->hydrate($row);
            FilterOperatorTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildFilterOperator|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(FilterOperatorTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(FilterOperatorTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(FilterOperatorTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(FilterOperatorTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilterOperatorTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilterOperatorTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilterOperatorTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the description_sentence column
     *
     * Example usage:
     * <code>
     * $query->filterByDescriptionSentence('fooValue');   // WHERE description_sentence = 'fooValue'
     * $query->filterByDescriptionSentence('%fooValue%', Criteria::LIKE); // WHERE description_sentence LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descriptionSentence The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function filterByDescriptionSentence($descriptionSentence = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descriptionSentence)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilterOperatorTableMap::COL_DESCRIPTION_SENTENCE, $descriptionSentence, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(FilterOperatorTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(FilterOperatorTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilterOperatorTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(FilterOperatorTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(FilterOperatorTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(FilterOperatorTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Marketing\MailingListsCriteria object
     *
     * @param \Model\Marketing\MailingListsCriteria|ObjectCollection $mailingListsCriteria the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function filterByMailingListsCriteria($mailingListsCriteria, $comparison = null)
    {
        if ($mailingListsCriteria instanceof \Model\Marketing\MailingListsCriteria) {
            return $this
                ->addUsingAlias(FilterOperatorTableMap::COL_ID, $mailingListsCriteria->getFilterOperatorId(), $comparison);
        } elseif ($mailingListsCriteria instanceof ObjectCollection) {
            return $this
                ->useMailingListsCriteriaQuery()
                ->filterByPrimaryKeys($mailingListsCriteria->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMailingListsCriteria() only accepts arguments of type \Model\Marketing\MailingListsCriteria or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MailingListsCriteria relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function joinMailingListsCriteria($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MailingListsCriteria');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MailingListsCriteria');
        }

        return $this;
    }

    /**
     * Use the MailingListsCriteria relation MailingListsCriteria object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Marketing\MailingListsCriteriaQuery A secondary query class using the current class as primary query
     */
    public function useMailingListsCriteriaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMailingListsCriteria($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MailingListsCriteria', '\Model\Marketing\MailingListsCriteriaQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudEditorButtonVisibileFilter object
     *
     * @param \Model\Setting\CrudManager\CrudEditorButtonVisibileFilter|ObjectCollection $crudEditorButtonVisibileFilter the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function filterByCrudEditorButtonVisibileFilter($crudEditorButtonVisibileFilter, $comparison = null)
    {
        if ($crudEditorButtonVisibileFilter instanceof \Model\Setting\CrudManager\CrudEditorButtonVisibileFilter) {
            return $this
                ->addUsingAlias(FilterOperatorTableMap::COL_ID, $crudEditorButtonVisibileFilter->getFilterOperatorId(), $comparison);
        } elseif ($crudEditorButtonVisibileFilter instanceof ObjectCollection) {
            return $this
                ->useCrudEditorButtonVisibileFilterQuery()
                ->filterByPrimaryKeys($crudEditorButtonVisibileFilter->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudEditorButtonVisibileFilter() only accepts arguments of type \Model\Setting\CrudManager\CrudEditorButtonVisibileFilter or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudEditorButtonVisibileFilter relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function joinCrudEditorButtonVisibileFilter($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudEditorButtonVisibileFilter');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudEditorButtonVisibileFilter');
        }

        return $this;
    }

    /**
     * Use the CrudEditorButtonVisibileFilter relation CrudEditorButtonVisibileFilter object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery A secondary query class using the current class as primary query
     */
    public function useCrudEditorButtonVisibileFilterQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudEditorButtonVisibileFilter($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudEditorButtonVisibileFilter', '\Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\FilterOperatorDatatype object
     *
     * @param \Model\Setting\CrudManager\FilterOperatorDatatype|ObjectCollection $filterOperatorDatatype the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function filterByFilterOperatorDatatype($filterOperatorDatatype, $comparison = null)
    {
        if ($filterOperatorDatatype instanceof \Model\Setting\CrudManager\FilterOperatorDatatype) {
            return $this
                ->addUsingAlias(FilterOperatorTableMap::COL_ID, $filterOperatorDatatype->getFilterOperatorId(), $comparison);
        } elseif ($filterOperatorDatatype instanceof ObjectCollection) {
            return $this
                ->useFilterOperatorDatatypeQuery()
                ->filterByPrimaryKeys($filterOperatorDatatype->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFilterOperatorDatatype() only accepts arguments of type \Model\Setting\CrudManager\FilterOperatorDatatype or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FilterOperatorDatatype relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function joinFilterOperatorDatatype($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FilterOperatorDatatype');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FilterOperatorDatatype');
        }

        return $this;
    }

    /**
     * Use the FilterOperatorDatatype relation FilterOperatorDatatype object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\FilterOperatorDatatypeQuery A secondary query class using the current class as primary query
     */
    public function useFilterOperatorDatatypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFilterOperatorDatatype($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FilterOperatorDatatype', '\Model\Setting\CrudManager\FilterOperatorDatatypeQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudViewVisibleFilter object
     *
     * @param \Model\Setting\CrudManager\CrudViewVisibleFilter|ObjectCollection $crudViewVisibleFilter the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function filterByCrudViewVisibleFilter($crudViewVisibleFilter, $comparison = null)
    {
        if ($crudViewVisibleFilter instanceof \Model\Setting\CrudManager\CrudViewVisibleFilter) {
            return $this
                ->addUsingAlias(FilterOperatorTableMap::COL_ID, $crudViewVisibleFilter->getFilterOperatorId(), $comparison);
        } elseif ($crudViewVisibleFilter instanceof ObjectCollection) {
            return $this
                ->useCrudViewVisibleFilterQuery()
                ->filterByPrimaryKeys($crudViewVisibleFilter->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudViewVisibleFilter() only accepts arguments of type \Model\Setting\CrudManager\CrudViewVisibleFilter or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudViewVisibleFilter relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function joinCrudViewVisibleFilter($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudViewVisibleFilter');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudViewVisibleFilter');
        }

        return $this;
    }

    /**
     * Use the CrudViewVisibleFilter relation CrudViewVisibleFilter object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudViewVisibleFilterQuery A secondary query class using the current class as primary query
     */
    public function useCrudViewVisibleFilterQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudViewVisibleFilter($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudViewVisibleFilter', '\Model\Setting\CrudManager\CrudViewVisibleFilterQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudViewHiddenFilter object
     *
     * @param \Model\Setting\CrudManager\CrudViewHiddenFilter|ObjectCollection $crudViewHiddenFilter the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function filterByCrudViewHiddenFilter($crudViewHiddenFilter, $comparison = null)
    {
        if ($crudViewHiddenFilter instanceof \Model\Setting\CrudManager\CrudViewHiddenFilter) {
            return $this
                ->addUsingAlias(FilterOperatorTableMap::COL_ID, $crudViewHiddenFilter->getFilterOperatorId(), $comparison);
        } elseif ($crudViewHiddenFilter instanceof ObjectCollection) {
            return $this
                ->useCrudViewHiddenFilterQuery()
                ->filterByPrimaryKeys($crudViewHiddenFilter->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCrudViewHiddenFilter() only accepts arguments of type \Model\Setting\CrudManager\CrudViewHiddenFilter or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudViewHiddenFilter relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function joinCrudViewHiddenFilter($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudViewHiddenFilter');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudViewHiddenFilter');
        }

        return $this;
    }

    /**
     * Use the CrudViewHiddenFilter relation CrudViewHiddenFilter object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudViewHiddenFilterQuery A secondary query class using the current class as primary query
     */
    public function useCrudViewHiddenFilterQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudViewHiddenFilter($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudViewHiddenFilter', '\Model\Setting\CrudManager\CrudViewHiddenFilterQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildFilterOperator $filterOperator Object to remove from the list of results
     *
     * @return $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function prune($filterOperator = null)
    {
        if ($filterOperator) {
            $this->addUsingAlias(FilterOperatorTableMap::COL_ID, $filterOperator->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the filter_operator table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FilterOperatorTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            FilterOperatorTableMap::clearInstancePool();
            FilterOperatorTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FilterOperatorTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(FilterOperatorTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            FilterOperatorTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            FilterOperatorTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(FilterOperatorTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(FilterOperatorTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(FilterOperatorTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(FilterOperatorTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(FilterOperatorTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildFilterOperatorQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(FilterOperatorTableMap::COL_CREATED_AT);
    }

} // FilterOperatorQuery
