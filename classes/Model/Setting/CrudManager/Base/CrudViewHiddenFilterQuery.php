<?php

namespace Model\Setting\CrudManager\Base;

use \Exception;
use \PDO;
use Model\Setting\CrudManager\CrudViewHiddenFilter as ChildCrudViewHiddenFilter;
use Model\Setting\CrudManager\CrudViewHiddenFilterQuery as ChildCrudViewHiddenFilterQuery;
use Model\Setting\CrudManager\Map\CrudViewHiddenFilterTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'crud_view_hidden_filter' table.
 *
 *
 *
 * @method     ChildCrudViewHiddenFilterQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCrudViewHiddenFilterQuery orderByCrudViewId($order = Criteria::ASC) Order by the crud_view_id column
 * @method     ChildCrudViewHiddenFilterQuery orderByFilterOperatorId($order = Criteria::ASC) Order by the filter_operator_id column
 * @method     ChildCrudViewHiddenFilterQuery orderByFilterName($order = Criteria::ASC) Order by the filter_name column
 * @method     ChildCrudViewHiddenFilterQuery orderByFilterValue($order = Criteria::ASC) Order by the filter_value column
 * @method     ChildCrudViewHiddenFilterQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildCrudViewHiddenFilterQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildCrudViewHiddenFilterQuery groupById() Group by the id column
 * @method     ChildCrudViewHiddenFilterQuery groupByCrudViewId() Group by the crud_view_id column
 * @method     ChildCrudViewHiddenFilterQuery groupByFilterOperatorId() Group by the filter_operator_id column
 * @method     ChildCrudViewHiddenFilterQuery groupByFilterName() Group by the filter_name column
 * @method     ChildCrudViewHiddenFilterQuery groupByFilterValue() Group by the filter_value column
 * @method     ChildCrudViewHiddenFilterQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildCrudViewHiddenFilterQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildCrudViewHiddenFilterQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCrudViewHiddenFilterQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCrudViewHiddenFilterQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCrudViewHiddenFilterQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCrudViewHiddenFilterQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCrudViewHiddenFilterQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCrudViewHiddenFilterQuery leftJoinCrudView($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudView relation
 * @method     ChildCrudViewHiddenFilterQuery rightJoinCrudView($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudView relation
 * @method     ChildCrudViewHiddenFilterQuery innerJoinCrudView($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudView relation
 *
 * @method     ChildCrudViewHiddenFilterQuery joinWithCrudView($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudView relation
 *
 * @method     ChildCrudViewHiddenFilterQuery leftJoinWithCrudView() Adds a LEFT JOIN clause and with to the query using the CrudView relation
 * @method     ChildCrudViewHiddenFilterQuery rightJoinWithCrudView() Adds a RIGHT JOIN clause and with to the query using the CrudView relation
 * @method     ChildCrudViewHiddenFilterQuery innerJoinWithCrudView() Adds a INNER JOIN clause and with to the query using the CrudView relation
 *
 * @method     ChildCrudViewHiddenFilterQuery leftJoinFilterOperator($relationAlias = null) Adds a LEFT JOIN clause to the query using the FilterOperator relation
 * @method     ChildCrudViewHiddenFilterQuery rightJoinFilterOperator($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FilterOperator relation
 * @method     ChildCrudViewHiddenFilterQuery innerJoinFilterOperator($relationAlias = null) Adds a INNER JOIN clause to the query using the FilterOperator relation
 *
 * @method     ChildCrudViewHiddenFilterQuery joinWithFilterOperator($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FilterOperator relation
 *
 * @method     ChildCrudViewHiddenFilterQuery leftJoinWithFilterOperator() Adds a LEFT JOIN clause and with to the query using the FilterOperator relation
 * @method     ChildCrudViewHiddenFilterQuery rightJoinWithFilterOperator() Adds a RIGHT JOIN clause and with to the query using the FilterOperator relation
 * @method     ChildCrudViewHiddenFilterQuery innerJoinWithFilterOperator() Adds a INNER JOIN clause and with to the query using the FilterOperator relation
 *
 * @method     \Model\Setting\CrudManager\CrudViewQuery|\Model\Setting\CrudManager\FilterOperatorQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCrudViewHiddenFilter findOne(ConnectionInterface $con = null) Return the first ChildCrudViewHiddenFilter matching the query
 * @method     ChildCrudViewHiddenFilter findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCrudViewHiddenFilter matching the query, or a new ChildCrudViewHiddenFilter object populated from the query conditions when no match is found
 *
 * @method     ChildCrudViewHiddenFilter findOneById(int $id) Return the first ChildCrudViewHiddenFilter filtered by the id column
 * @method     ChildCrudViewHiddenFilter findOneByCrudViewId(int $crud_view_id) Return the first ChildCrudViewHiddenFilter filtered by the crud_view_id column
 * @method     ChildCrudViewHiddenFilter findOneByFilterOperatorId(int $filter_operator_id) Return the first ChildCrudViewHiddenFilter filtered by the filter_operator_id column
 * @method     ChildCrudViewHiddenFilter findOneByFilterName(string $filter_name) Return the first ChildCrudViewHiddenFilter filtered by the filter_name column
 * @method     ChildCrudViewHiddenFilter findOneByFilterValue(string $filter_value) Return the first ChildCrudViewHiddenFilter filtered by the filter_value column
 * @method     ChildCrudViewHiddenFilter findOneByCreatedAt(string $created_at) Return the first ChildCrudViewHiddenFilter filtered by the created_at column
 * @method     ChildCrudViewHiddenFilter findOneByUpdatedAt(string $updated_at) Return the first ChildCrudViewHiddenFilter filtered by the updated_at column *

 * @method     ChildCrudViewHiddenFilter requirePk($key, ConnectionInterface $con = null) Return the ChildCrudViewHiddenFilter by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewHiddenFilter requireOne(ConnectionInterface $con = null) Return the first ChildCrudViewHiddenFilter matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudViewHiddenFilter requireOneById(int $id) Return the first ChildCrudViewHiddenFilter filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewHiddenFilter requireOneByCrudViewId(int $crud_view_id) Return the first ChildCrudViewHiddenFilter filtered by the crud_view_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewHiddenFilter requireOneByFilterOperatorId(int $filter_operator_id) Return the first ChildCrudViewHiddenFilter filtered by the filter_operator_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewHiddenFilter requireOneByFilterName(string $filter_name) Return the first ChildCrudViewHiddenFilter filtered by the filter_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewHiddenFilter requireOneByFilterValue(string $filter_value) Return the first ChildCrudViewHiddenFilter filtered by the filter_value column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewHiddenFilter requireOneByCreatedAt(string $created_at) Return the first ChildCrudViewHiddenFilter filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCrudViewHiddenFilter requireOneByUpdatedAt(string $updated_at) Return the first ChildCrudViewHiddenFilter filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCrudViewHiddenFilter[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCrudViewHiddenFilter objects based on current ModelCriteria
 * @method     ChildCrudViewHiddenFilter[]|ObjectCollection findById(int $id) Return ChildCrudViewHiddenFilter objects filtered by the id column
 * @method     ChildCrudViewHiddenFilter[]|ObjectCollection findByCrudViewId(int $crud_view_id) Return ChildCrudViewHiddenFilter objects filtered by the crud_view_id column
 * @method     ChildCrudViewHiddenFilter[]|ObjectCollection findByFilterOperatorId(int $filter_operator_id) Return ChildCrudViewHiddenFilter objects filtered by the filter_operator_id column
 * @method     ChildCrudViewHiddenFilter[]|ObjectCollection findByFilterName(string $filter_name) Return ChildCrudViewHiddenFilter objects filtered by the filter_name column
 * @method     ChildCrudViewHiddenFilter[]|ObjectCollection findByFilterValue(string $filter_value) Return ChildCrudViewHiddenFilter objects filtered by the filter_value column
 * @method     ChildCrudViewHiddenFilter[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildCrudViewHiddenFilter objects filtered by the created_at column
 * @method     ChildCrudViewHiddenFilter[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildCrudViewHiddenFilter objects filtered by the updated_at column
 * @method     ChildCrudViewHiddenFilter[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CrudViewHiddenFilterQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\CrudManager\Base\CrudViewHiddenFilterQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\CrudManager\\CrudViewHiddenFilter', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCrudViewHiddenFilterQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCrudViewHiddenFilterQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCrudViewHiddenFilterQuery) {
            return $criteria;
        }
        $query = new ChildCrudViewHiddenFilterQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCrudViewHiddenFilter|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CrudViewHiddenFilterTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CrudViewHiddenFilterTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudViewHiddenFilter A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, crud_view_id, filter_operator_id, filter_name, filter_value, created_at, updated_at FROM crud_view_hidden_filter WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCrudViewHiddenFilter $obj */
            $obj = new ChildCrudViewHiddenFilter();
            $obj->hydrate($row);
            CrudViewHiddenFilterTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCrudViewHiddenFilter|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the crud_view_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCrudViewId(1234); // WHERE crud_view_id = 1234
     * $query->filterByCrudViewId(array(12, 34)); // WHERE crud_view_id IN (12, 34)
     * $query->filterByCrudViewId(array('min' => 12)); // WHERE crud_view_id > 12
     * </code>
     *
     * @see       filterByCrudView()
     *
     * @param     mixed $crudViewId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function filterByCrudViewId($crudViewId = null, $comparison = null)
    {
        if (is_array($crudViewId)) {
            $useMinMax = false;
            if (isset($crudViewId['min'])) {
                $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_CRUD_VIEW_ID, $crudViewId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($crudViewId['max'])) {
                $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_CRUD_VIEW_ID, $crudViewId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_CRUD_VIEW_ID, $crudViewId, $comparison);
    }

    /**
     * Filter the query on the filter_operator_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFilterOperatorId(1234); // WHERE filter_operator_id = 1234
     * $query->filterByFilterOperatorId(array(12, 34)); // WHERE filter_operator_id IN (12, 34)
     * $query->filterByFilterOperatorId(array('min' => 12)); // WHERE filter_operator_id > 12
     * </code>
     *
     * @see       filterByFilterOperator()
     *
     * @param     mixed $filterOperatorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function filterByFilterOperatorId($filterOperatorId = null, $comparison = null)
    {
        if (is_array($filterOperatorId)) {
            $useMinMax = false;
            if (isset($filterOperatorId['min'])) {
                $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_FILTER_OPERATOR_ID, $filterOperatorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($filterOperatorId['max'])) {
                $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_FILTER_OPERATOR_ID, $filterOperatorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_FILTER_OPERATOR_ID, $filterOperatorId, $comparison);
    }

    /**
     * Filter the query on the filter_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFilterName('fooValue');   // WHERE filter_name = 'fooValue'
     * $query->filterByFilterName('%fooValue%', Criteria::LIKE); // WHERE filter_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $filterName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function filterByFilterName($filterName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($filterName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_FILTER_NAME, $filterName, $comparison);
    }

    /**
     * Filter the query on the filter_value column
     *
     * Example usage:
     * <code>
     * $query->filterByFilterValue('fooValue');   // WHERE filter_value = 'fooValue'
     * $query->filterByFilterValue('%fooValue%', Criteria::LIKE); // WHERE filter_value LIKE '%fooValue%'
     * </code>
     *
     * @param     string $filterValue The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function filterByFilterValue($filterValue = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($filterValue)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_FILTER_VALUE, $filterValue, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudView object
     *
     * @param \Model\Setting\CrudManager\CrudView|ObjectCollection $crudView The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function filterByCrudView($crudView, $comparison = null)
    {
        if ($crudView instanceof \Model\Setting\CrudManager\CrudView) {
            return $this
                ->addUsingAlias(CrudViewHiddenFilterTableMap::COL_CRUD_VIEW_ID, $crudView->getId(), $comparison);
        } elseif ($crudView instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CrudViewHiddenFilterTableMap::COL_CRUD_VIEW_ID, $crudView->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCrudView() only accepts arguments of type \Model\Setting\CrudManager\CrudView or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudView relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function joinCrudView($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudView');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudView');
        }

        return $this;
    }

    /**
     * Use the CrudView relation CrudView object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudViewQuery A secondary query class using the current class as primary query
     */
    public function useCrudViewQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudView($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudView', '\Model\Setting\CrudManager\CrudViewQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\FilterOperator object
     *
     * @param \Model\Setting\CrudManager\FilterOperator|ObjectCollection $filterOperator The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function filterByFilterOperator($filterOperator, $comparison = null)
    {
        if ($filterOperator instanceof \Model\Setting\CrudManager\FilterOperator) {
            return $this
                ->addUsingAlias(CrudViewHiddenFilterTableMap::COL_FILTER_OPERATOR_ID, $filterOperator->getId(), $comparison);
        } elseif ($filterOperator instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CrudViewHiddenFilterTableMap::COL_FILTER_OPERATOR_ID, $filterOperator->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFilterOperator() only accepts arguments of type \Model\Setting\CrudManager\FilterOperator or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FilterOperator relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function joinFilterOperator($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FilterOperator');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FilterOperator');
        }

        return $this;
    }

    /**
     * Use the FilterOperator relation FilterOperator object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\FilterOperatorQuery A secondary query class using the current class as primary query
     */
    public function useFilterOperatorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFilterOperator($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FilterOperator', '\Model\Setting\CrudManager\FilterOperatorQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCrudViewHiddenFilter $crudViewHiddenFilter Object to remove from the list of results
     *
     * @return $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function prune($crudViewHiddenFilter = null)
    {
        if ($crudViewHiddenFilter) {
            $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_ID, $crudViewHiddenFilter->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the crud_view_hidden_filter table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewHiddenFilterTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CrudViewHiddenFilterTableMap::clearInstancePool();
            CrudViewHiddenFilterTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewHiddenFilterTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CrudViewHiddenFilterTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CrudViewHiddenFilterTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CrudViewHiddenFilterTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudViewHiddenFilterTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudViewHiddenFilterTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(CrudViewHiddenFilterTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(CrudViewHiddenFilterTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildCrudViewHiddenFilterQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(CrudViewHiddenFilterTableMap::COL_CREATED_AT);
    }

} // CrudViewHiddenFilterQuery
