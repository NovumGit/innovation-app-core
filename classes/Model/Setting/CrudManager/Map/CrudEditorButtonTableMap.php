<?php

namespace Model\Setting\CrudManager\Map;

use Model\Setting\CrudManager\CrudEditorButton;
use Model\Setting\CrudManager\CrudEditorButtonQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'crud_editor_button' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CrudEditorButtonTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Setting.CrudManager.Map.CrudEditorButtonTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'crud_editor_button';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Setting\\CrudManager\\CrudEditorButton';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Setting.CrudManager.CrudEditorButton';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the id field
     */
    const COL_ID = 'crud_editor_button.id';

    /**
     * the column name for the crud_editor_id field
     */
    const COL_CRUD_EDITOR_ID = 'crud_editor_button.crud_editor_id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'crud_editor_button.title';

    /**
     * the column name for the icon_before_click field
     */
    const COL_ICON_BEFORE_CLICK = 'crud_editor_button.icon_before_click';

    /**
     * the column name for the icon_after_click field
     */
    const COL_ICON_AFTER_CLICK = 'crud_editor_button.icon_after_click';

    /**
     * the column name for the color_before_click field
     */
    const COL_COLOR_BEFORE_CLICK = 'crud_editor_button.color_before_click';

    /**
     * the column name for the color_after_click field
     */
    const COL_COLOR_AFTER_CLICK = 'crud_editor_button.color_after_click';

    /**
     * the column name for the is_reusable field
     */
    const COL_IS_REUSABLE = 'crud_editor_button.is_reusable';

    /**
     * the column name for the sorting field
     */
    const COL_SORTING = 'crud_editor_button.sorting';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'crud_editor_button.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'crud_editor_button.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'CrudEditorId', 'Title', 'IconBeforeClick', 'IconAfterClick', 'ColorBeforeClick', 'ColorAfterClick', 'IsReusable', 'Sorting', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'crudEditorId', 'title', 'iconBeforeClick', 'iconAfterClick', 'colorBeforeClick', 'colorAfterClick', 'isReusable', 'sorting', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(CrudEditorButtonTableMap::COL_ID, CrudEditorButtonTableMap::COL_CRUD_EDITOR_ID, CrudEditorButtonTableMap::COL_TITLE, CrudEditorButtonTableMap::COL_ICON_BEFORE_CLICK, CrudEditorButtonTableMap::COL_ICON_AFTER_CLICK, CrudEditorButtonTableMap::COL_COLOR_BEFORE_CLICK, CrudEditorButtonTableMap::COL_COLOR_AFTER_CLICK, CrudEditorButtonTableMap::COL_IS_REUSABLE, CrudEditorButtonTableMap::COL_SORTING, CrudEditorButtonTableMap::COL_CREATED_AT, CrudEditorButtonTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'crud_editor_id', 'title', 'icon_before_click', 'icon_after_click', 'color_before_click', 'color_after_click', 'is_reusable', 'sorting', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'CrudEditorId' => 1, 'Title' => 2, 'IconBeforeClick' => 3, 'IconAfterClick' => 4, 'ColorBeforeClick' => 5, 'ColorAfterClick' => 6, 'IsReusable' => 7, 'Sorting' => 8, 'CreatedAt' => 9, 'UpdatedAt' => 10, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'crudEditorId' => 1, 'title' => 2, 'iconBeforeClick' => 3, 'iconAfterClick' => 4, 'colorBeforeClick' => 5, 'colorAfterClick' => 6, 'isReusable' => 7, 'sorting' => 8, 'createdAt' => 9, 'updatedAt' => 10, ),
        self::TYPE_COLNAME       => array(CrudEditorButtonTableMap::COL_ID => 0, CrudEditorButtonTableMap::COL_CRUD_EDITOR_ID => 1, CrudEditorButtonTableMap::COL_TITLE => 2, CrudEditorButtonTableMap::COL_ICON_BEFORE_CLICK => 3, CrudEditorButtonTableMap::COL_ICON_AFTER_CLICK => 4, CrudEditorButtonTableMap::COL_COLOR_BEFORE_CLICK => 5, CrudEditorButtonTableMap::COL_COLOR_AFTER_CLICK => 6, CrudEditorButtonTableMap::COL_IS_REUSABLE => 7, CrudEditorButtonTableMap::COL_SORTING => 8, CrudEditorButtonTableMap::COL_CREATED_AT => 9, CrudEditorButtonTableMap::COL_UPDATED_AT => 10, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'crud_editor_id' => 1, 'title' => 2, 'icon_before_click' => 3, 'icon_after_click' => 4, 'color_before_click' => 5, 'color_after_click' => 6, 'is_reusable' => 7, 'sorting' => 8, 'created_at' => 9, 'updated_at' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('crud_editor_button');
        $this->setPhpName('CrudEditorButton');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Setting\\CrudManager\\CrudEditorButton');
        $this->setPackage('Model.Setting.CrudManager');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('crud_editor_id', 'CrudEditorId', 'INTEGER', 'crud_editor', 'id', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 100, null);
        $this->addColumn('icon_before_click', 'IconBeforeClick', 'VARCHAR', false, 30, null);
        $this->addColumn('icon_after_click', 'IconAfterClick', 'VARCHAR', false, 30, null);
        $this->addColumn('color_before_click', 'ColorBeforeClick', 'VARCHAR', false, 30, null);
        $this->addColumn('color_after_click', 'ColorAfterClick', 'VARCHAR', false, 30, null);
        $this->addColumn('is_reusable', 'IsReusable', 'BOOLEAN', false, 1, null);
        $this->addColumn('sorting', 'Sorting', 'INTEGER', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CrudEditor', '\\Model\\Setting\\CrudManager\\CrudEditor', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':crud_editor_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('CrudEditorButtonVisibileFilter', '\\Model\\Setting\\CrudManager\\CrudEditorButtonVisibileFilter', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':crud_editor_button_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'CrudEditorButtonVisibileFilters', false);
        $this->addRelation('CrudEditorButtonEvent', '\\Model\\Setting\\CrudManager\\CrudEditorButtonEvent', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':crud_editor_button_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'CrudEditorButtonEvents', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to crud_editor_button     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CrudEditorButtonVisibileFilterTableMap::clearInstancePool();
        CrudEditorButtonEventTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CrudEditorButtonTableMap::CLASS_DEFAULT : CrudEditorButtonTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CrudEditorButton object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CrudEditorButtonTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CrudEditorButtonTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CrudEditorButtonTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CrudEditorButtonTableMap::OM_CLASS;
            /** @var CrudEditorButton $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CrudEditorButtonTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CrudEditorButtonTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CrudEditorButtonTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CrudEditorButton $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CrudEditorButtonTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CrudEditorButtonTableMap::COL_ID);
            $criteria->addSelectColumn(CrudEditorButtonTableMap::COL_CRUD_EDITOR_ID);
            $criteria->addSelectColumn(CrudEditorButtonTableMap::COL_TITLE);
            $criteria->addSelectColumn(CrudEditorButtonTableMap::COL_ICON_BEFORE_CLICK);
            $criteria->addSelectColumn(CrudEditorButtonTableMap::COL_ICON_AFTER_CLICK);
            $criteria->addSelectColumn(CrudEditorButtonTableMap::COL_COLOR_BEFORE_CLICK);
            $criteria->addSelectColumn(CrudEditorButtonTableMap::COL_COLOR_AFTER_CLICK);
            $criteria->addSelectColumn(CrudEditorButtonTableMap::COL_IS_REUSABLE);
            $criteria->addSelectColumn(CrudEditorButtonTableMap::COL_SORTING);
            $criteria->addSelectColumn(CrudEditorButtonTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(CrudEditorButtonTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.crud_editor_id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.icon_before_click');
            $criteria->addSelectColumn($alias . '.icon_after_click');
            $criteria->addSelectColumn($alias . '.color_before_click');
            $criteria->addSelectColumn($alias . '.color_after_click');
            $criteria->addSelectColumn($alias . '.is_reusable');
            $criteria->addSelectColumn($alias . '.sorting');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CrudEditorButtonTableMap::DATABASE_NAME)->getTable(CrudEditorButtonTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CrudEditorButtonTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CrudEditorButtonTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CrudEditorButtonTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CrudEditorButton or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CrudEditorButton object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorButtonTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Setting\CrudManager\CrudEditorButton) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CrudEditorButtonTableMap::DATABASE_NAME);
            $criteria->add(CrudEditorButtonTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CrudEditorButtonQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CrudEditorButtonTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CrudEditorButtonTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the crud_editor_button table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CrudEditorButtonQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CrudEditorButton or Criteria object.
     *
     * @param mixed               $criteria Criteria or CrudEditorButton object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorButtonTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CrudEditorButton object
        }

        if ($criteria->containsKey(CrudEditorButtonTableMap::COL_ID) && $criteria->keyContainsValue(CrudEditorButtonTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CrudEditorButtonTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CrudEditorButtonQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CrudEditorButtonTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CrudEditorButtonTableMap::buildTableMap();
