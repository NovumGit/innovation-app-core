<?php

namespace Model\Setting\CrudManager\Map;

use Model\Setting\CrudManager\CrudView;
use Model\Setting\CrudManager\CrudViewQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'crud_view' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CrudViewTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Setting.CrudManager.Map.CrudViewTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'crud_view';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Setting\\CrudManager\\CrudView';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Setting.CrudManager.CrudView';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'crud_view.id';

    /**
     * the column name for the crud_config_id field
     */
    const COL_CRUD_CONFIG_ID = 'crud_view.crud_config_id';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'crud_view.name';

    /**
     * the column name for the code field
     */
    const COL_CODE = 'crud_view.code';

    /**
     * the column name for the sorting field
     */
    const COL_SORTING = 'crud_view.sorting';

    /**
     * the column name for the is_hidden field
     */
    const COL_IS_HIDDEN = 'crud_view.is_hidden';

    /**
     * the column name for the show_quantity field
     */
    const COL_SHOW_QUANTITY = 'crud_view.show_quantity';

    /**
     * the column name for the default_order_by field
     */
    const COL_DEFAULT_ORDER_BY = 'crud_view.default_order_by';

    /**
     * the column name for the default_order_dir field
     */
    const COL_DEFAULT_ORDER_DIR = 'crud_view.default_order_dir';

    /**
     * the column name for the tab_color_logic field
     */
    const COL_TAB_COLOR_LOGIC = 'crud_view.tab_color_logic';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'crud_view.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'crud_view.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'CrudConfigId', 'Name', 'Code', 'Sorting', 'IsHidden', 'ShowQuantity', 'DefaultOrderBy', 'DefaultOrderDir', 'TabColorLogic', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'crudConfigId', 'name', 'code', 'sorting', 'isHidden', 'showQuantity', 'defaultOrderBy', 'defaultOrderDir', 'tabColorLogic', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(CrudViewTableMap::COL_ID, CrudViewTableMap::COL_CRUD_CONFIG_ID, CrudViewTableMap::COL_NAME, CrudViewTableMap::COL_CODE, CrudViewTableMap::COL_SORTING, CrudViewTableMap::COL_IS_HIDDEN, CrudViewTableMap::COL_SHOW_QUANTITY, CrudViewTableMap::COL_DEFAULT_ORDER_BY, CrudViewTableMap::COL_DEFAULT_ORDER_DIR, CrudViewTableMap::COL_TAB_COLOR_LOGIC, CrudViewTableMap::COL_CREATED_AT, CrudViewTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'crud_config_id', 'name', 'code', 'sorting', 'is_hidden', 'show_quantity', 'default_order_by', 'default_order_dir', 'tab_color_logic', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'CrudConfigId' => 1, 'Name' => 2, 'Code' => 3, 'Sorting' => 4, 'IsHidden' => 5, 'ShowQuantity' => 6, 'DefaultOrderBy' => 7, 'DefaultOrderDir' => 8, 'TabColorLogic' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'crudConfigId' => 1, 'name' => 2, 'code' => 3, 'sorting' => 4, 'isHidden' => 5, 'showQuantity' => 6, 'defaultOrderBy' => 7, 'defaultOrderDir' => 8, 'tabColorLogic' => 9, 'createdAt' => 10, 'updatedAt' => 11, ),
        self::TYPE_COLNAME       => array(CrudViewTableMap::COL_ID => 0, CrudViewTableMap::COL_CRUD_CONFIG_ID => 1, CrudViewTableMap::COL_NAME => 2, CrudViewTableMap::COL_CODE => 3, CrudViewTableMap::COL_SORTING => 4, CrudViewTableMap::COL_IS_HIDDEN => 5, CrudViewTableMap::COL_SHOW_QUANTITY => 6, CrudViewTableMap::COL_DEFAULT_ORDER_BY => 7, CrudViewTableMap::COL_DEFAULT_ORDER_DIR => 8, CrudViewTableMap::COL_TAB_COLOR_LOGIC => 9, CrudViewTableMap::COL_CREATED_AT => 10, CrudViewTableMap::COL_UPDATED_AT => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'crud_config_id' => 1, 'name' => 2, 'code' => 3, 'sorting' => 4, 'is_hidden' => 5, 'show_quantity' => 6, 'default_order_by' => 7, 'default_order_dir' => 8, 'tab_color_logic' => 9, 'created_at' => 10, 'updated_at' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('crud_view');
        $this->setPhpName('CrudView');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Setting\\CrudManager\\CrudView');
        $this->setPackage('Model.Setting.CrudManager');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('crud_config_id', 'CrudConfigId', 'INTEGER', 'crud_config', 'id', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', false, 255, null);
        $this->addColumn('code', 'Code', 'VARCHAR', false, 120, null);
        $this->addColumn('sorting', 'Sorting', 'INTEGER', true, null, null);
        $this->addColumn('is_hidden', 'IsHidden', 'BOOLEAN', true, 1, false);
        $this->addColumn('show_quantity', 'ShowQuantity', 'BOOLEAN', true, 1, false);
        $this->addColumn('default_order_by', 'DefaultOrderBy', 'VARCHAR', true, 255, 'id');
        $this->addColumn('default_order_dir', 'DefaultOrderDir', 'VARCHAR', true, 255, 'asc');
        $this->addColumn('tab_color_logic', 'TabColorLogic', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CrudConfig', '\\Model\\Setting\\CrudManager\\CrudConfig', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':crud_config_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('CrudView', '\\Model\\Setting\\CrudManager\\CrudViewField', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':crud_view_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'CrudViews', false);
        $this->addRelation('CrudViewButton', '\\Model\\Setting\\CrudManager\\CrudViewButton', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':crud_view_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'CrudViewButtons', false);
        $this->addRelation('CrudViewVisibleFilter', '\\Model\\Setting\\CrudManager\\CrudViewVisibleFilter', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':crud_view_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'CrudViewVisibleFilters', false);
        $this->addRelation('CrudViewHiddenFilter', '\\Model\\Setting\\CrudManager\\CrudViewHiddenFilter', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':crud_view_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'CrudViewHiddenFilters', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to crud_view     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CrudViewFieldTableMap::clearInstancePool();
        CrudViewButtonTableMap::clearInstancePool();
        CrudViewVisibleFilterTableMap::clearInstancePool();
        CrudViewHiddenFilterTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CrudViewTableMap::CLASS_DEFAULT : CrudViewTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CrudView object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CrudViewTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CrudViewTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CrudViewTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CrudViewTableMap::OM_CLASS;
            /** @var CrudView $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CrudViewTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CrudViewTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CrudViewTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CrudView $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CrudViewTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CrudViewTableMap::COL_ID);
            $criteria->addSelectColumn(CrudViewTableMap::COL_CRUD_CONFIG_ID);
            $criteria->addSelectColumn(CrudViewTableMap::COL_NAME);
            $criteria->addSelectColumn(CrudViewTableMap::COL_CODE);
            $criteria->addSelectColumn(CrudViewTableMap::COL_SORTING);
            $criteria->addSelectColumn(CrudViewTableMap::COL_IS_HIDDEN);
            $criteria->addSelectColumn(CrudViewTableMap::COL_SHOW_QUANTITY);
            $criteria->addSelectColumn(CrudViewTableMap::COL_DEFAULT_ORDER_BY);
            $criteria->addSelectColumn(CrudViewTableMap::COL_DEFAULT_ORDER_DIR);
            $criteria->addSelectColumn(CrudViewTableMap::COL_TAB_COLOR_LOGIC);
            $criteria->addSelectColumn(CrudViewTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(CrudViewTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.crud_config_id');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.code');
            $criteria->addSelectColumn($alias . '.sorting');
            $criteria->addSelectColumn($alias . '.is_hidden');
            $criteria->addSelectColumn($alias . '.show_quantity');
            $criteria->addSelectColumn($alias . '.default_order_by');
            $criteria->addSelectColumn($alias . '.default_order_dir');
            $criteria->addSelectColumn($alias . '.tab_color_logic');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CrudViewTableMap::DATABASE_NAME)->getTable(CrudViewTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CrudViewTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CrudViewTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CrudViewTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CrudView or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CrudView object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Setting\CrudManager\CrudView) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CrudViewTableMap::DATABASE_NAME);
            $criteria->add(CrudViewTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CrudViewQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CrudViewTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CrudViewTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the crud_view table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CrudViewQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CrudView or Criteria object.
     *
     * @param mixed               $criteria Criteria or CrudView object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CrudView object
        }

        if ($criteria->containsKey(CrudViewTableMap::COL_ID) && $criteria->keyContainsValue(CrudViewTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CrudViewTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CrudViewQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CrudViewTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CrudViewTableMap::buildTableMap();
