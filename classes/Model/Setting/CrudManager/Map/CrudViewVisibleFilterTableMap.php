<?php

namespace Model\Setting\CrudManager\Map;

use Model\Setting\CrudManager\CrudViewVisibleFilter;
use Model\Setting\CrudManager\CrudViewVisibleFilterQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'crud_view_visible_filter' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CrudViewVisibleFilterTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Setting.CrudManager.Map.CrudViewVisibleFilterTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'crud_view_visible_filter';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Setting\\CrudManager\\CrudViewVisibleFilter';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Setting.CrudManager.CrudViewVisibleFilter';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the id field
     */
    const COL_ID = 'crud_view_visible_filter.id';

    /**
     * the column name for the crud_view_id field
     */
    const COL_CRUD_VIEW_ID = 'crud_view_visible_filter.crud_view_id';

    /**
     * the column name for the filter_operator_id field
     */
    const COL_FILTER_OPERATOR_ID = 'crud_view_visible_filter.filter_operator_id';

    /**
     * the column name for the filter_name field
     */
    const COL_FILTER_NAME = 'crud_view_visible_filter.filter_name';

    /**
     * the column name for the filter_default_value field
     */
    const COL_FILTER_DEFAULT_VALUE = 'crud_view_visible_filter.filter_default_value';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'crud_view_visible_filter.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'crud_view_visible_filter.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'CrudViewId', 'FilterOperatorId', 'FilterName', 'FilterDefaultValue', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'crudViewId', 'filterOperatorId', 'filterName', 'filterDefaultValue', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(CrudViewVisibleFilterTableMap::COL_ID, CrudViewVisibleFilterTableMap::COL_CRUD_VIEW_ID, CrudViewVisibleFilterTableMap::COL_FILTER_OPERATOR_ID, CrudViewVisibleFilterTableMap::COL_FILTER_NAME, CrudViewVisibleFilterTableMap::COL_FILTER_DEFAULT_VALUE, CrudViewVisibleFilterTableMap::COL_CREATED_AT, CrudViewVisibleFilterTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'crud_view_id', 'filter_operator_id', 'filter_name', 'filter_default_value', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'CrudViewId' => 1, 'FilterOperatorId' => 2, 'FilterName' => 3, 'FilterDefaultValue' => 4, 'CreatedAt' => 5, 'UpdatedAt' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'crudViewId' => 1, 'filterOperatorId' => 2, 'filterName' => 3, 'filterDefaultValue' => 4, 'createdAt' => 5, 'updatedAt' => 6, ),
        self::TYPE_COLNAME       => array(CrudViewVisibleFilterTableMap::COL_ID => 0, CrudViewVisibleFilterTableMap::COL_CRUD_VIEW_ID => 1, CrudViewVisibleFilterTableMap::COL_FILTER_OPERATOR_ID => 2, CrudViewVisibleFilterTableMap::COL_FILTER_NAME => 3, CrudViewVisibleFilterTableMap::COL_FILTER_DEFAULT_VALUE => 4, CrudViewVisibleFilterTableMap::COL_CREATED_AT => 5, CrudViewVisibleFilterTableMap::COL_UPDATED_AT => 6, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'crud_view_id' => 1, 'filter_operator_id' => 2, 'filter_name' => 3, 'filter_default_value' => 4, 'created_at' => 5, 'updated_at' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('crud_view_visible_filter');
        $this->setPhpName('CrudViewVisibleFilter');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Setting\\CrudManager\\CrudViewVisibleFilter');
        $this->setPackage('Model.Setting.CrudManager');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('crud_view_id', 'CrudViewId', 'INTEGER', 'crud_view', 'id', true, null, null);
        $this->addForeignKey('filter_operator_id', 'FilterOperatorId', 'INTEGER', 'filter_operator', 'id', true, null, null);
        $this->addColumn('filter_name', 'FilterName', 'VARCHAR', true, 255, null);
        $this->addColumn('filter_default_value', 'FilterDefaultValue', 'VARCHAR', true, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CrudView', '\\Model\\Setting\\CrudManager\\CrudView', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':crud_view_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('FilterOperator', '\\Model\\Setting\\CrudManager\\FilterOperator', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':filter_operator_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CrudViewVisibleFilterTableMap::CLASS_DEFAULT : CrudViewVisibleFilterTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CrudViewVisibleFilter object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CrudViewVisibleFilterTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CrudViewVisibleFilterTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CrudViewVisibleFilterTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CrudViewVisibleFilterTableMap::OM_CLASS;
            /** @var CrudViewVisibleFilter $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CrudViewVisibleFilterTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CrudViewVisibleFilterTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CrudViewVisibleFilterTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CrudViewVisibleFilter $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CrudViewVisibleFilterTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CrudViewVisibleFilterTableMap::COL_ID);
            $criteria->addSelectColumn(CrudViewVisibleFilterTableMap::COL_CRUD_VIEW_ID);
            $criteria->addSelectColumn(CrudViewVisibleFilterTableMap::COL_FILTER_OPERATOR_ID);
            $criteria->addSelectColumn(CrudViewVisibleFilterTableMap::COL_FILTER_NAME);
            $criteria->addSelectColumn(CrudViewVisibleFilterTableMap::COL_FILTER_DEFAULT_VALUE);
            $criteria->addSelectColumn(CrudViewVisibleFilterTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(CrudViewVisibleFilterTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.crud_view_id');
            $criteria->addSelectColumn($alias . '.filter_operator_id');
            $criteria->addSelectColumn($alias . '.filter_name');
            $criteria->addSelectColumn($alias . '.filter_default_value');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CrudViewVisibleFilterTableMap::DATABASE_NAME)->getTable(CrudViewVisibleFilterTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CrudViewVisibleFilterTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CrudViewVisibleFilterTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CrudViewVisibleFilterTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CrudViewVisibleFilter or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CrudViewVisibleFilter object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewVisibleFilterTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Setting\CrudManager\CrudViewVisibleFilter) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CrudViewVisibleFilterTableMap::DATABASE_NAME);
            $criteria->add(CrudViewVisibleFilterTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CrudViewVisibleFilterQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CrudViewVisibleFilterTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CrudViewVisibleFilterTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the crud_view_visible_filter table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CrudViewVisibleFilterQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CrudViewVisibleFilter or Criteria object.
     *
     * @param mixed               $criteria Criteria or CrudViewVisibleFilter object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewVisibleFilterTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CrudViewVisibleFilter object
        }

        if ($criteria->containsKey(CrudViewVisibleFilterTableMap::COL_ID) && $criteria->keyContainsValue(CrudViewVisibleFilterTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CrudViewVisibleFilterTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CrudViewVisibleFilterQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CrudViewVisibleFilterTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CrudViewVisibleFilterTableMap::buildTableMap();
