<?php

namespace Model\Setting\CrudManager\Map;

use Model\Setting\CrudManager\CrudViewButton;
use Model\Setting\CrudManager\CrudViewButtonQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'crud_view_button' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CrudViewButtonTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Setting.CrudManager.Map.CrudViewButtonTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'crud_view_button';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Setting\\CrudManager\\CrudViewButton';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Setting.CrudManager.CrudViewButton';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id field
     */
    const COL_ID = 'crud_view_button.id';

    /**
     * the column name for the crud_view_id field
     */
    const COL_CRUD_VIEW_ID = 'crud_view_button.crud_view_id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'crud_view_button.title';

    /**
     * the column name for the icon_before_click field
     */
    const COL_ICON_BEFORE_CLICK = 'crud_view_button.icon_before_click';

    /**
     * the column name for the icon_after_click field
     */
    const COL_ICON_AFTER_CLICK = 'crud_view_button.icon_after_click';

    /**
     * the column name for the color_before_click field
     */
    const COL_COLOR_BEFORE_CLICK = 'crud_view_button.color_before_click';

    /**
     * the column name for the color_after_click field
     */
    const COL_COLOR_AFTER_CLICK = 'crud_view_button.color_after_click';

    /**
     * the column name for the sorting field
     */
    const COL_SORTING = 'crud_view_button.sorting';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'crud_view_button.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'crud_view_button.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'CrudViewId', 'Title', 'IconBeforeClick', 'IconAfterClick', 'ColorBeforeClick', 'ColorAfterClick', 'Sorting', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'crudViewId', 'title', 'iconBeforeClick', 'iconAfterClick', 'colorBeforeClick', 'colorAfterClick', 'sorting', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(CrudViewButtonTableMap::COL_ID, CrudViewButtonTableMap::COL_CRUD_VIEW_ID, CrudViewButtonTableMap::COL_TITLE, CrudViewButtonTableMap::COL_ICON_BEFORE_CLICK, CrudViewButtonTableMap::COL_ICON_AFTER_CLICK, CrudViewButtonTableMap::COL_COLOR_BEFORE_CLICK, CrudViewButtonTableMap::COL_COLOR_AFTER_CLICK, CrudViewButtonTableMap::COL_SORTING, CrudViewButtonTableMap::COL_CREATED_AT, CrudViewButtonTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'crud_view_id', 'title', 'icon_before_click', 'icon_after_click', 'color_before_click', 'color_after_click', 'sorting', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'CrudViewId' => 1, 'Title' => 2, 'IconBeforeClick' => 3, 'IconAfterClick' => 4, 'ColorBeforeClick' => 5, 'ColorAfterClick' => 6, 'Sorting' => 7, 'CreatedAt' => 8, 'UpdatedAt' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'crudViewId' => 1, 'title' => 2, 'iconBeforeClick' => 3, 'iconAfterClick' => 4, 'colorBeforeClick' => 5, 'colorAfterClick' => 6, 'sorting' => 7, 'createdAt' => 8, 'updatedAt' => 9, ),
        self::TYPE_COLNAME       => array(CrudViewButtonTableMap::COL_ID => 0, CrudViewButtonTableMap::COL_CRUD_VIEW_ID => 1, CrudViewButtonTableMap::COL_TITLE => 2, CrudViewButtonTableMap::COL_ICON_BEFORE_CLICK => 3, CrudViewButtonTableMap::COL_ICON_AFTER_CLICK => 4, CrudViewButtonTableMap::COL_COLOR_BEFORE_CLICK => 5, CrudViewButtonTableMap::COL_COLOR_AFTER_CLICK => 6, CrudViewButtonTableMap::COL_SORTING => 7, CrudViewButtonTableMap::COL_CREATED_AT => 8, CrudViewButtonTableMap::COL_UPDATED_AT => 9, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'crud_view_id' => 1, 'title' => 2, 'icon_before_click' => 3, 'icon_after_click' => 4, 'color_before_click' => 5, 'color_after_click' => 6, 'sorting' => 7, 'created_at' => 8, 'updated_at' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('crud_view_button');
        $this->setPhpName('CrudViewButton');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Setting\\CrudManager\\CrudViewButton');
        $this->setPackage('Model.Setting.CrudManager');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('crud_view_id', 'CrudViewId', 'INTEGER', 'crud_view', 'id', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 100, null);
        $this->addColumn('icon_before_click', 'IconBeforeClick', 'VARCHAR', false, 30, null);
        $this->addColumn('icon_after_click', 'IconAfterClick', 'VARCHAR', false, 30, null);
        $this->addColumn('color_before_click', 'ColorBeforeClick', 'VARCHAR', false, 30, null);
        $this->addColumn('color_after_click', 'ColorAfterClick', 'VARCHAR', false, 30, null);
        $this->addColumn('sorting', 'Sorting', 'INTEGER', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CrudView', '\\Model\\Setting\\CrudManager\\CrudView', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':crud_view_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('CrudViewButtonEvent', '\\Model\\Setting\\CrudManager\\CrudViewButtonEvent', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':crud_view_button_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'CrudViewButtonEvents', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to crud_view_button     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        CrudViewButtonEventTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CrudViewButtonTableMap::CLASS_DEFAULT : CrudViewButtonTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CrudViewButton object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CrudViewButtonTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CrudViewButtonTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CrudViewButtonTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CrudViewButtonTableMap::OM_CLASS;
            /** @var CrudViewButton $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CrudViewButtonTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CrudViewButtonTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CrudViewButtonTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CrudViewButton $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CrudViewButtonTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CrudViewButtonTableMap::COL_ID);
            $criteria->addSelectColumn(CrudViewButtonTableMap::COL_CRUD_VIEW_ID);
            $criteria->addSelectColumn(CrudViewButtonTableMap::COL_TITLE);
            $criteria->addSelectColumn(CrudViewButtonTableMap::COL_ICON_BEFORE_CLICK);
            $criteria->addSelectColumn(CrudViewButtonTableMap::COL_ICON_AFTER_CLICK);
            $criteria->addSelectColumn(CrudViewButtonTableMap::COL_COLOR_BEFORE_CLICK);
            $criteria->addSelectColumn(CrudViewButtonTableMap::COL_COLOR_AFTER_CLICK);
            $criteria->addSelectColumn(CrudViewButtonTableMap::COL_SORTING);
            $criteria->addSelectColumn(CrudViewButtonTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(CrudViewButtonTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.crud_view_id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.icon_before_click');
            $criteria->addSelectColumn($alias . '.icon_after_click');
            $criteria->addSelectColumn($alias . '.color_before_click');
            $criteria->addSelectColumn($alias . '.color_after_click');
            $criteria->addSelectColumn($alias . '.sorting');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CrudViewButtonTableMap::DATABASE_NAME)->getTable(CrudViewButtonTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CrudViewButtonTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CrudViewButtonTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CrudViewButtonTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CrudViewButton or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CrudViewButton object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewButtonTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Setting\CrudManager\CrudViewButton) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CrudViewButtonTableMap::DATABASE_NAME);
            $criteria->add(CrudViewButtonTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CrudViewButtonQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CrudViewButtonTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CrudViewButtonTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the crud_view_button table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CrudViewButtonQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CrudViewButton or Criteria object.
     *
     * @param mixed               $criteria Criteria or CrudViewButton object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudViewButtonTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CrudViewButton object
        }

        if ($criteria->containsKey(CrudViewButtonTableMap::COL_ID) && $criteria->keyContainsValue(CrudViewButtonTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CrudViewButtonTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CrudViewButtonQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CrudViewButtonTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CrudViewButtonTableMap::buildTableMap();
