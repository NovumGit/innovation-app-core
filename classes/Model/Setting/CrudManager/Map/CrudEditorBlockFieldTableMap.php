<?php

namespace Model\Setting\CrudManager\Map;

use Model\Setting\CrudManager\CrudEditorBlockField;
use Model\Setting\CrudManager\CrudEditorBlockFieldQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'crud_editor_block_field' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CrudEditorBlockFieldTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Setting.CrudManager.Map.CrudEditorBlockFieldTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'crud_editor_block_field';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Setting\\CrudManager\\CrudEditorBlockField';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Setting.CrudManager.CrudEditorBlockField';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    const COL_ID = 'crud_editor_block_field.id';

    /**
     * the column name for the crud_editor_block_id field
     */
    const COL_CRUD_EDITOR_BLOCK_ID = 'crud_editor_block_field.crud_editor_block_id';

    /**
     * the column name for the sorting field
     */
    const COL_SORTING = 'crud_editor_block_field.sorting';

    /**
     * the column name for the readonly_field field
     */
    const COL_READONLY_FIELD = 'crud_editor_block_field.readonly_field';

    /**
     * the column name for the width field
     */
    const COL_WIDTH = 'crud_editor_block_field.width';

    /**
     * the column name for the field field
     */
    const COL_FIELD = 'crud_editor_block_field.field';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'crud_editor_block_field.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'crud_editor_block_field.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'CrudEditorBlockId', 'Sorting', 'ReadonlyField', 'Width', 'Field', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'crudEditorBlockId', 'sorting', 'readonlyField', 'width', 'field', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(CrudEditorBlockFieldTableMap::COL_ID, CrudEditorBlockFieldTableMap::COL_CRUD_EDITOR_BLOCK_ID, CrudEditorBlockFieldTableMap::COL_SORTING, CrudEditorBlockFieldTableMap::COL_READONLY_FIELD, CrudEditorBlockFieldTableMap::COL_WIDTH, CrudEditorBlockFieldTableMap::COL_FIELD, CrudEditorBlockFieldTableMap::COL_CREATED_AT, CrudEditorBlockFieldTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'crud_editor_block_id', 'sorting', 'readonly_field', 'width', 'field', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'CrudEditorBlockId' => 1, 'Sorting' => 2, 'ReadonlyField' => 3, 'Width' => 4, 'Field' => 5, 'CreatedAt' => 6, 'UpdatedAt' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'crudEditorBlockId' => 1, 'sorting' => 2, 'readonlyField' => 3, 'width' => 4, 'field' => 5, 'createdAt' => 6, 'updatedAt' => 7, ),
        self::TYPE_COLNAME       => array(CrudEditorBlockFieldTableMap::COL_ID => 0, CrudEditorBlockFieldTableMap::COL_CRUD_EDITOR_BLOCK_ID => 1, CrudEditorBlockFieldTableMap::COL_SORTING => 2, CrudEditorBlockFieldTableMap::COL_READONLY_FIELD => 3, CrudEditorBlockFieldTableMap::COL_WIDTH => 4, CrudEditorBlockFieldTableMap::COL_FIELD => 5, CrudEditorBlockFieldTableMap::COL_CREATED_AT => 6, CrudEditorBlockFieldTableMap::COL_UPDATED_AT => 7, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'crud_editor_block_id' => 1, 'sorting' => 2, 'readonly_field' => 3, 'width' => 4, 'field' => 5, 'created_at' => 6, 'updated_at' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('crud_editor_block_field');
        $this->setPhpName('CrudEditorBlockField');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Setting\\CrudManager\\CrudEditorBlockField');
        $this->setPackage('Model.Setting.CrudManager');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('crud_editor_block_id', 'CrudEditorBlockId', 'INTEGER', 'crud_editor_block', 'id', true, null, null);
        $this->addColumn('sorting', 'Sorting', 'INTEGER', false, null, null);
        $this->addColumn('readonly_field', 'ReadonlyField', 'BOOLEAN', false, 1, false);
        $this->addColumn('width', 'Width', 'INTEGER', true, null, null);
        $this->addColumn('field', 'Field', 'VARCHAR', true, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CrudEditorBlock', '\\Model\\Setting\\CrudManager\\CrudEditorBlock', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':crud_editor_block_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CrudEditorBlockFieldTableMap::CLASS_DEFAULT : CrudEditorBlockFieldTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CrudEditorBlockField object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CrudEditorBlockFieldTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CrudEditorBlockFieldTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CrudEditorBlockFieldTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CrudEditorBlockFieldTableMap::OM_CLASS;
            /** @var CrudEditorBlockField $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CrudEditorBlockFieldTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CrudEditorBlockFieldTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CrudEditorBlockFieldTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CrudEditorBlockField $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CrudEditorBlockFieldTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CrudEditorBlockFieldTableMap::COL_ID);
            $criteria->addSelectColumn(CrudEditorBlockFieldTableMap::COL_CRUD_EDITOR_BLOCK_ID);
            $criteria->addSelectColumn(CrudEditorBlockFieldTableMap::COL_SORTING);
            $criteria->addSelectColumn(CrudEditorBlockFieldTableMap::COL_READONLY_FIELD);
            $criteria->addSelectColumn(CrudEditorBlockFieldTableMap::COL_WIDTH);
            $criteria->addSelectColumn(CrudEditorBlockFieldTableMap::COL_FIELD);
            $criteria->addSelectColumn(CrudEditorBlockFieldTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(CrudEditorBlockFieldTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.crud_editor_block_id');
            $criteria->addSelectColumn($alias . '.sorting');
            $criteria->addSelectColumn($alias . '.readonly_field');
            $criteria->addSelectColumn($alias . '.width');
            $criteria->addSelectColumn($alias . '.field');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CrudEditorBlockFieldTableMap::DATABASE_NAME)->getTable(CrudEditorBlockFieldTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CrudEditorBlockFieldTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CrudEditorBlockFieldTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CrudEditorBlockFieldTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CrudEditorBlockField or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CrudEditorBlockField object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorBlockFieldTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Setting\CrudManager\CrudEditorBlockField) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CrudEditorBlockFieldTableMap::DATABASE_NAME);
            $criteria->add(CrudEditorBlockFieldTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CrudEditorBlockFieldQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CrudEditorBlockFieldTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CrudEditorBlockFieldTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the crud_editor_block_field table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CrudEditorBlockFieldQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CrudEditorBlockField or Criteria object.
     *
     * @param mixed               $criteria Criteria or CrudEditorBlockField object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorBlockFieldTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CrudEditorBlockField object
        }

        if ($criteria->containsKey(CrudEditorBlockFieldTableMap::COL_ID) && $criteria->keyContainsValue(CrudEditorBlockFieldTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CrudEditorBlockFieldTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CrudEditorBlockFieldQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CrudEditorBlockFieldTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CrudEditorBlockFieldTableMap::buildTableMap();
