<?php

namespace Model\Setting\CrudManager\Map;

use Model\Setting\CrudManager\CrudEditorButtonVisibileFilter;
use Model\Setting\CrudManager\CrudEditorButtonVisibileFilterQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'crud_editor_button_visibile_filter' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CrudEditorButtonVisibileFilterTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Setting.CrudManager.Map.CrudEditorButtonVisibileFilterTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'crud_editor_button_visibile_filter';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Setting\\CrudManager\\CrudEditorButtonVisibileFilter';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Setting.CrudManager.CrudEditorButtonVisibileFilter';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'crud_editor_button_visibile_filter.id';

    /**
     * the column name for the crud_editor_button_id field
     */
    const COL_CRUD_EDITOR_BUTTON_ID = 'crud_editor_button_visibile_filter.crud_editor_button_id';

    /**
     * the column name for the event_id field
     */
    const COL_EVENT_ID = 'crud_editor_button_visibile_filter.event_id';

    /**
     * the column name for the sorting field
     */
    const COL_SORTING = 'crud_editor_button_visibile_filter.sorting';

    /**
     * the column name for the filter_operator_id field
     */
    const COL_FILTER_OPERATOR_ID = 'crud_editor_button_visibile_filter.filter_operator_id';

    /**
     * the column name for the filter_name field
     */
    const COL_FILTER_NAME = 'crud_editor_button_visibile_filter.filter_name';

    /**
     * the column name for the filter_value field
     */
    const COL_FILTER_VALUE = 'crud_editor_button_visibile_filter.filter_value';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'crud_editor_button_visibile_filter.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'crud_editor_button_visibile_filter.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'CrudEditorButtonId', 'EventId', 'Sorting', 'FilterOperatorId', 'FilterName', 'FilterValue', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'crudEditorButtonId', 'eventId', 'sorting', 'filterOperatorId', 'filterName', 'filterValue', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(CrudEditorButtonVisibileFilterTableMap::COL_ID, CrudEditorButtonVisibileFilterTableMap::COL_CRUD_EDITOR_BUTTON_ID, CrudEditorButtonVisibileFilterTableMap::COL_EVENT_ID, CrudEditorButtonVisibileFilterTableMap::COL_SORTING, CrudEditorButtonVisibileFilterTableMap::COL_FILTER_OPERATOR_ID, CrudEditorButtonVisibileFilterTableMap::COL_FILTER_NAME, CrudEditorButtonVisibileFilterTableMap::COL_FILTER_VALUE, CrudEditorButtonVisibileFilterTableMap::COL_CREATED_AT, CrudEditorButtonVisibileFilterTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'crud_editor_button_id', 'event_id', 'sorting', 'filter_operator_id', 'filter_name', 'filter_value', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'CrudEditorButtonId' => 1, 'EventId' => 2, 'Sorting' => 3, 'FilterOperatorId' => 4, 'FilterName' => 5, 'FilterValue' => 6, 'CreatedAt' => 7, 'UpdatedAt' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'crudEditorButtonId' => 1, 'eventId' => 2, 'sorting' => 3, 'filterOperatorId' => 4, 'filterName' => 5, 'filterValue' => 6, 'createdAt' => 7, 'updatedAt' => 8, ),
        self::TYPE_COLNAME       => array(CrudEditorButtonVisibileFilterTableMap::COL_ID => 0, CrudEditorButtonVisibileFilterTableMap::COL_CRUD_EDITOR_BUTTON_ID => 1, CrudEditorButtonVisibileFilterTableMap::COL_EVENT_ID => 2, CrudEditorButtonVisibileFilterTableMap::COL_SORTING => 3, CrudEditorButtonVisibileFilterTableMap::COL_FILTER_OPERATOR_ID => 4, CrudEditorButtonVisibileFilterTableMap::COL_FILTER_NAME => 5, CrudEditorButtonVisibileFilterTableMap::COL_FILTER_VALUE => 6, CrudEditorButtonVisibileFilterTableMap::COL_CREATED_AT => 7, CrudEditorButtonVisibileFilterTableMap::COL_UPDATED_AT => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'crud_editor_button_id' => 1, 'event_id' => 2, 'sorting' => 3, 'filter_operator_id' => 4, 'filter_name' => 5, 'filter_value' => 6, 'created_at' => 7, 'updated_at' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('crud_editor_button_visibile_filter');
        $this->setPhpName('CrudEditorButtonVisibileFilter');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Setting\\CrudManager\\CrudEditorButtonVisibileFilter');
        $this->setPackage('Model.Setting.CrudManager');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('crud_editor_button_id', 'CrudEditorButtonId', 'INTEGER', 'crud_editor_button', 'id', false, null, null);
        $this->addColumn('event_id', 'EventId', 'INTEGER', false, null, null);
        $this->addColumn('sorting', 'Sorting', 'INTEGER', false, null, null);
        $this->addForeignKey('filter_operator_id', 'FilterOperatorId', 'INTEGER', 'filter_operator', 'id', true, null, null);
        $this->addColumn('filter_name', 'FilterName', 'VARCHAR', true, 255, null);
        $this->addColumn('filter_value', 'FilterValue', 'VARCHAR', true, 255, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CrudEditorButton', '\\Model\\Setting\\CrudManager\\CrudEditorButton', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':crud_editor_button_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('FilterOperator', '\\Model\\Setting\\CrudManager\\FilterOperator', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':filter_operator_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CrudEditorButtonVisibileFilterTableMap::CLASS_DEFAULT : CrudEditorButtonVisibileFilterTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CrudEditorButtonVisibileFilter object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CrudEditorButtonVisibileFilterTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CrudEditorButtonVisibileFilterTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CrudEditorButtonVisibileFilterTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CrudEditorButtonVisibileFilterTableMap::OM_CLASS;
            /** @var CrudEditorButtonVisibileFilter $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CrudEditorButtonVisibileFilterTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CrudEditorButtonVisibileFilterTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CrudEditorButtonVisibileFilterTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CrudEditorButtonVisibileFilter $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CrudEditorButtonVisibileFilterTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CrudEditorButtonVisibileFilterTableMap::COL_ID);
            $criteria->addSelectColumn(CrudEditorButtonVisibileFilterTableMap::COL_CRUD_EDITOR_BUTTON_ID);
            $criteria->addSelectColumn(CrudEditorButtonVisibileFilterTableMap::COL_EVENT_ID);
            $criteria->addSelectColumn(CrudEditorButtonVisibileFilterTableMap::COL_SORTING);
            $criteria->addSelectColumn(CrudEditorButtonVisibileFilterTableMap::COL_FILTER_OPERATOR_ID);
            $criteria->addSelectColumn(CrudEditorButtonVisibileFilterTableMap::COL_FILTER_NAME);
            $criteria->addSelectColumn(CrudEditorButtonVisibileFilterTableMap::COL_FILTER_VALUE);
            $criteria->addSelectColumn(CrudEditorButtonVisibileFilterTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(CrudEditorButtonVisibileFilterTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.crud_editor_button_id');
            $criteria->addSelectColumn($alias . '.event_id');
            $criteria->addSelectColumn($alias . '.sorting');
            $criteria->addSelectColumn($alias . '.filter_operator_id');
            $criteria->addSelectColumn($alias . '.filter_name');
            $criteria->addSelectColumn($alias . '.filter_value');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CrudEditorButtonVisibileFilterTableMap::DATABASE_NAME)->getTable(CrudEditorButtonVisibileFilterTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CrudEditorButtonVisibileFilterTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CrudEditorButtonVisibileFilterTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CrudEditorButtonVisibileFilterTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CrudEditorButtonVisibileFilter or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CrudEditorButtonVisibileFilter object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorButtonVisibileFilterTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Setting\CrudManager\CrudEditorButtonVisibileFilter) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CrudEditorButtonVisibileFilterTableMap::DATABASE_NAME);
            $criteria->add(CrudEditorButtonVisibileFilterTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CrudEditorButtonVisibileFilterQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CrudEditorButtonVisibileFilterTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CrudEditorButtonVisibileFilterTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the crud_editor_button_visibile_filter table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CrudEditorButtonVisibileFilterQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CrudEditorButtonVisibileFilter or Criteria object.
     *
     * @param mixed               $criteria Criteria or CrudEditorButtonVisibileFilter object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CrudEditorButtonVisibileFilterTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CrudEditorButtonVisibileFilter object
        }

        if ($criteria->containsKey(CrudEditorButtonVisibileFilterTableMap::COL_ID) && $criteria->keyContainsValue(CrudEditorButtonVisibileFilterTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CrudEditorButtonVisibileFilterTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CrudEditorButtonVisibileFilterQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CrudEditorButtonVisibileFilterTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CrudEditorButtonVisibileFilterTableMap::buildTableMap();
