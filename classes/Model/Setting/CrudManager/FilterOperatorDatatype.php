<?php

namespace Model\Setting\CrudManager;

use Model\Setting\CrudManager\Base\FilterOperatorDatatype as BaseFilterOperatorDatatype;

/**
 * Skeleton subclass for representing a row from the 'filter_operator_datatype' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class FilterOperatorDatatype extends BaseFilterOperatorDatatype
{

}
