<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\Sale\SaleOrder;
use Model\Setting\MasterTable\ShippingMethod as ChildShippingMethod;
use Model\Setting\MasterTable\ShippingMethodQuery as ChildShippingMethodQuery;
use Model\Setting\MasterTable\Map\ShippingMethodTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mt_shipping_method' table.
 *
 *
 *
 * @method     ChildShippingMethodQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildShippingMethodQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildShippingMethodQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method     ChildShippingMethodQuery orderByIsDefaultFree($order = Criteria::ASC) Order by the is_default_free column
 * @method     ChildShippingMethodQuery orderByIsTrackTrace($order = Criteria::ASC) Order by the is_track_trace column
 * @method     ChildShippingMethodQuery orderByMinWeightGrams($order = Criteria::ASC) Order by the min_weight_grams column
 * @method     ChildShippingMethodQuery orderByMaxWeightGrams($order = Criteria::ASC) Order by the max_weight_grams column
 *
 * @method     ChildShippingMethodQuery groupById() Group by the id column
 * @method     ChildShippingMethodQuery groupByName() Group by the name column
 * @method     ChildShippingMethodQuery groupByCode() Group by the code column
 * @method     ChildShippingMethodQuery groupByIsDefaultFree() Group by the is_default_free column
 * @method     ChildShippingMethodQuery groupByIsTrackTrace() Group by the is_track_trace column
 * @method     ChildShippingMethodQuery groupByMinWeightGrams() Group by the min_weight_grams column
 * @method     ChildShippingMethodQuery groupByMaxWeightGrams() Group by the max_weight_grams column
 *
 * @method     ChildShippingMethodQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildShippingMethodQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildShippingMethodQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildShippingMethodQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildShippingMethodQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildShippingMethodQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildShippingMethodQuery leftJoinSaleOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrder relation
 * @method     ChildShippingMethodQuery rightJoinSaleOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrder relation
 * @method     ChildShippingMethodQuery innerJoinSaleOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrder relation
 *
 * @method     ChildShippingMethodQuery joinWithSaleOrder($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrder relation
 *
 * @method     ChildShippingMethodQuery leftJoinWithSaleOrder() Adds a LEFT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildShippingMethodQuery rightJoinWithSaleOrder() Adds a RIGHT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildShippingMethodQuery innerJoinWithSaleOrder() Adds a INNER JOIN clause and with to the query using the SaleOrder relation
 *
 * @method     ChildShippingMethodQuery leftJoinCountryShippingMethod($relationAlias = null) Adds a LEFT JOIN clause to the query using the CountryShippingMethod relation
 * @method     ChildShippingMethodQuery rightJoinCountryShippingMethod($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CountryShippingMethod relation
 * @method     ChildShippingMethodQuery innerJoinCountryShippingMethod($relationAlias = null) Adds a INNER JOIN clause to the query using the CountryShippingMethod relation
 *
 * @method     ChildShippingMethodQuery joinWithCountryShippingMethod($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CountryShippingMethod relation
 *
 * @method     ChildShippingMethodQuery leftJoinWithCountryShippingMethod() Adds a LEFT JOIN clause and with to the query using the CountryShippingMethod relation
 * @method     ChildShippingMethodQuery rightJoinWithCountryShippingMethod() Adds a RIGHT JOIN clause and with to the query using the CountryShippingMethod relation
 * @method     ChildShippingMethodQuery innerJoinWithCountryShippingMethod() Adds a INNER JOIN clause and with to the query using the CountryShippingMethod relation
 *
 * @method     \Model\Sale\SaleOrderQuery|\Model\Setting\MasterTable\CountryShippingMethodQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildShippingMethod findOne(ConnectionInterface $con = null) Return the first ChildShippingMethod matching the query
 * @method     ChildShippingMethod findOneOrCreate(ConnectionInterface $con = null) Return the first ChildShippingMethod matching the query, or a new ChildShippingMethod object populated from the query conditions when no match is found
 *
 * @method     ChildShippingMethod findOneById(int $id) Return the first ChildShippingMethod filtered by the id column
 * @method     ChildShippingMethod findOneByName(string $name) Return the first ChildShippingMethod filtered by the name column
 * @method     ChildShippingMethod findOneByCode(string $code) Return the first ChildShippingMethod filtered by the code column
 * @method     ChildShippingMethod findOneByIsDefaultFree(boolean $is_default_free) Return the first ChildShippingMethod filtered by the is_default_free column
 * @method     ChildShippingMethod findOneByIsTrackTrace(boolean $is_track_trace) Return the first ChildShippingMethod filtered by the is_track_trace column
 * @method     ChildShippingMethod findOneByMinWeightGrams(int $min_weight_grams) Return the first ChildShippingMethod filtered by the min_weight_grams column
 * @method     ChildShippingMethod findOneByMaxWeightGrams(int $max_weight_grams) Return the first ChildShippingMethod filtered by the max_weight_grams column *

 * @method     ChildShippingMethod requirePk($key, ConnectionInterface $con = null) Return the ChildShippingMethod by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShippingMethod requireOne(ConnectionInterface $con = null) Return the first ChildShippingMethod matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildShippingMethod requireOneById(int $id) Return the first ChildShippingMethod filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShippingMethod requireOneByName(string $name) Return the first ChildShippingMethod filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShippingMethod requireOneByCode(string $code) Return the first ChildShippingMethod filtered by the code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShippingMethod requireOneByIsDefaultFree(boolean $is_default_free) Return the first ChildShippingMethod filtered by the is_default_free column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShippingMethod requireOneByIsTrackTrace(boolean $is_track_trace) Return the first ChildShippingMethod filtered by the is_track_trace column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShippingMethod requireOneByMinWeightGrams(int $min_weight_grams) Return the first ChildShippingMethod filtered by the min_weight_grams column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildShippingMethod requireOneByMaxWeightGrams(int $max_weight_grams) Return the first ChildShippingMethod filtered by the max_weight_grams column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildShippingMethod[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildShippingMethod objects based on current ModelCriteria
 * @method     ChildShippingMethod[]|ObjectCollection findById(int $id) Return ChildShippingMethod objects filtered by the id column
 * @method     ChildShippingMethod[]|ObjectCollection findByName(string $name) Return ChildShippingMethod objects filtered by the name column
 * @method     ChildShippingMethod[]|ObjectCollection findByCode(string $code) Return ChildShippingMethod objects filtered by the code column
 * @method     ChildShippingMethod[]|ObjectCollection findByIsDefaultFree(boolean $is_default_free) Return ChildShippingMethod objects filtered by the is_default_free column
 * @method     ChildShippingMethod[]|ObjectCollection findByIsTrackTrace(boolean $is_track_trace) Return ChildShippingMethod objects filtered by the is_track_trace column
 * @method     ChildShippingMethod[]|ObjectCollection findByMinWeightGrams(int $min_weight_grams) Return ChildShippingMethod objects filtered by the min_weight_grams column
 * @method     ChildShippingMethod[]|ObjectCollection findByMaxWeightGrams(int $max_weight_grams) Return ChildShippingMethod objects filtered by the max_weight_grams column
 * @method     ChildShippingMethod[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ShippingMethodQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\MasterTable\Base\ShippingMethodQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\MasterTable\\ShippingMethod', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildShippingMethodQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildShippingMethodQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildShippingMethodQuery) {
            return $criteria;
        }
        $query = new ChildShippingMethodQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildShippingMethod|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ShippingMethodTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ShippingMethodTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildShippingMethod A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, code, is_default_free, is_track_trace, min_weight_grams, max_weight_grams FROM mt_shipping_method WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildShippingMethod $obj */
            $obj = new ChildShippingMethod();
            $obj->hydrate($row);
            ShippingMethodTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildShippingMethod|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildShippingMethodQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ShippingMethodTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildShippingMethodQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ShippingMethodTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShippingMethodQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ShippingMethodTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ShippingMethodTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShippingMethodTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShippingMethodQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShippingMethodTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%', Criteria::LIKE); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShippingMethodQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShippingMethodTableMap::COL_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the is_default_free column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDefaultFree(true); // WHERE is_default_free = true
     * $query->filterByIsDefaultFree('yes'); // WHERE is_default_free = true
     * </code>
     *
     * @param     boolean|string $isDefaultFree The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShippingMethodQuery The current query, for fluid interface
     */
    public function filterByIsDefaultFree($isDefaultFree = null, $comparison = null)
    {
        if (is_string($isDefaultFree)) {
            $isDefaultFree = in_array(strtolower($isDefaultFree), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ShippingMethodTableMap::COL_IS_DEFAULT_FREE, $isDefaultFree, $comparison);
    }

    /**
     * Filter the query on the is_track_trace column
     *
     * Example usage:
     * <code>
     * $query->filterByIsTrackTrace(true); // WHERE is_track_trace = true
     * $query->filterByIsTrackTrace('yes'); // WHERE is_track_trace = true
     * </code>
     *
     * @param     boolean|string $isTrackTrace The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShippingMethodQuery The current query, for fluid interface
     */
    public function filterByIsTrackTrace($isTrackTrace = null, $comparison = null)
    {
        if (is_string($isTrackTrace)) {
            $isTrackTrace = in_array(strtolower($isTrackTrace), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ShippingMethodTableMap::COL_IS_TRACK_TRACE, $isTrackTrace, $comparison);
    }

    /**
     * Filter the query on the min_weight_grams column
     *
     * Example usage:
     * <code>
     * $query->filterByMinWeightGrams(1234); // WHERE min_weight_grams = 1234
     * $query->filterByMinWeightGrams(array(12, 34)); // WHERE min_weight_grams IN (12, 34)
     * $query->filterByMinWeightGrams(array('min' => 12)); // WHERE min_weight_grams > 12
     * </code>
     *
     * @param     mixed $minWeightGrams The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShippingMethodQuery The current query, for fluid interface
     */
    public function filterByMinWeightGrams($minWeightGrams = null, $comparison = null)
    {
        if (is_array($minWeightGrams)) {
            $useMinMax = false;
            if (isset($minWeightGrams['min'])) {
                $this->addUsingAlias(ShippingMethodTableMap::COL_MIN_WEIGHT_GRAMS, $minWeightGrams['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($minWeightGrams['max'])) {
                $this->addUsingAlias(ShippingMethodTableMap::COL_MIN_WEIGHT_GRAMS, $minWeightGrams['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShippingMethodTableMap::COL_MIN_WEIGHT_GRAMS, $minWeightGrams, $comparison);
    }

    /**
     * Filter the query on the max_weight_grams column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxWeightGrams(1234); // WHERE max_weight_grams = 1234
     * $query->filterByMaxWeightGrams(array(12, 34)); // WHERE max_weight_grams IN (12, 34)
     * $query->filterByMaxWeightGrams(array('min' => 12)); // WHERE max_weight_grams > 12
     * </code>
     *
     * @param     mixed $maxWeightGrams The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildShippingMethodQuery The current query, for fluid interface
     */
    public function filterByMaxWeightGrams($maxWeightGrams = null, $comparison = null)
    {
        if (is_array($maxWeightGrams)) {
            $useMinMax = false;
            if (isset($maxWeightGrams['min'])) {
                $this->addUsingAlias(ShippingMethodTableMap::COL_MAX_WEIGHT_GRAMS, $maxWeightGrams['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxWeightGrams['max'])) {
                $this->addUsingAlias(ShippingMethodTableMap::COL_MAX_WEIGHT_GRAMS, $maxWeightGrams['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ShippingMethodTableMap::COL_MAX_WEIGHT_GRAMS, $maxWeightGrams, $comparison);
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrder object
     *
     * @param \Model\Sale\SaleOrder|ObjectCollection $saleOrder the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildShippingMethodQuery The current query, for fluid interface
     */
    public function filterBySaleOrder($saleOrder, $comparison = null)
    {
        if ($saleOrder instanceof \Model\Sale\SaleOrder) {
            return $this
                ->addUsingAlias(ShippingMethodTableMap::COL_ID, $saleOrder->getShippingMethodId(), $comparison);
        } elseif ($saleOrder instanceof ObjectCollection) {
            return $this
                ->useSaleOrderQuery()
                ->filterByPrimaryKeys($saleOrder->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleOrder() only accepts arguments of type \Model\Sale\SaleOrder or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildShippingMethodQuery The current query, for fluid interface
     */
    public function joinSaleOrder($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrder');
        }

        return $this;
    }

    /**
     * Use the SaleOrder relation SaleOrder object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaleOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrder', '\Model\Sale\SaleOrderQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\CountryShippingMethod object
     *
     * @param \Model\Setting\MasterTable\CountryShippingMethod|ObjectCollection $countryShippingMethod the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildShippingMethodQuery The current query, for fluid interface
     */
    public function filterByCountryShippingMethod($countryShippingMethod, $comparison = null)
    {
        if ($countryShippingMethod instanceof \Model\Setting\MasterTable\CountryShippingMethod) {
            return $this
                ->addUsingAlias(ShippingMethodTableMap::COL_ID, $countryShippingMethod->getShippingMethodId(), $comparison);
        } elseif ($countryShippingMethod instanceof ObjectCollection) {
            return $this
                ->useCountryShippingMethodQuery()
                ->filterByPrimaryKeys($countryShippingMethod->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCountryShippingMethod() only accepts arguments of type \Model\Setting\MasterTable\CountryShippingMethod or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CountryShippingMethod relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildShippingMethodQuery The current query, for fluid interface
     */
    public function joinCountryShippingMethod($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CountryShippingMethod');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CountryShippingMethod');
        }

        return $this;
    }

    /**
     * Use the CountryShippingMethod relation CountryShippingMethod object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\CountryShippingMethodQuery A secondary query class using the current class as primary query
     */
    public function useCountryShippingMethodQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCountryShippingMethod($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CountryShippingMethod', '\Model\Setting\MasterTable\CountryShippingMethodQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildShippingMethod $shippingMethod Object to remove from the list of results
     *
     * @return $this|ChildShippingMethodQuery The current query, for fluid interface
     */
    public function prune($shippingMethod = null)
    {
        if ($shippingMethod) {
            $this->addUsingAlias(ShippingMethodTableMap::COL_ID, $shippingMethod->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mt_shipping_method table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShippingMethodTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ShippingMethodTableMap::clearInstancePool();
            ShippingMethodTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShippingMethodTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ShippingMethodTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ShippingMethodTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ShippingMethodTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ShippingMethodQuery
