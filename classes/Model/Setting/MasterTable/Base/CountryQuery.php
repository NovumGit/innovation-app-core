<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\Company\Company;
use Model\Crm\CustomerAddress;
use Model\Reseller\Reseller;
use Model\Setting\MasterTable\Country as ChildCountry;
use Model\Setting\MasterTable\CountryQuery as ChildCountryQuery;
use Model\Setting\MasterTable\Map\CountryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mt_country' table.
 *
 *
 *
 * @method     ChildCountryQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCountryQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildCountryQuery orderByShippingZoneId($order = Criteria::ASC) Order by the shipping_zone_id column
 * @method     ChildCountryQuery orderByIso2($order = Criteria::ASC) Order by the iso2 column
 * @method     ChildCountryQuery orderByIso3($order = Criteria::ASC) Order by the iso3 column
 * @method     ChildCountryQuery orderByIsEu($order = Criteria::ASC) Order by the is_eu column
 * @method     ChildCountryQuery orderByStealthMandatory($order = Criteria::ASC) Order by the stealth_mandatory column
 * @method     ChildCountryQuery orderByStealthAdvised($order = Criteria::ASC) Order by the stealth_advised column
 * @method     ChildCountryQuery orderByLatitude($order = Criteria::ASC) Order by the latitude column
 * @method     ChildCountryQuery orderByLongitude($order = Criteria::ASC) Order by the longitude column
 *
 * @method     ChildCountryQuery groupById() Group by the id column
 * @method     ChildCountryQuery groupByName() Group by the name column
 * @method     ChildCountryQuery groupByShippingZoneId() Group by the shipping_zone_id column
 * @method     ChildCountryQuery groupByIso2() Group by the iso2 column
 * @method     ChildCountryQuery groupByIso3() Group by the iso3 column
 * @method     ChildCountryQuery groupByIsEu() Group by the is_eu column
 * @method     ChildCountryQuery groupByStealthMandatory() Group by the stealth_mandatory column
 * @method     ChildCountryQuery groupByStealthAdvised() Group by the stealth_advised column
 * @method     ChildCountryQuery groupByLatitude() Group by the latitude column
 * @method     ChildCountryQuery groupByLongitude() Group by the longitude column
 *
 * @method     ChildCountryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCountryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCountryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCountryQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCountryQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCountryQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCountryQuery leftJoinShippingZone($relationAlias = null) Adds a LEFT JOIN clause to the query using the ShippingZone relation
 * @method     ChildCountryQuery rightJoinShippingZone($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ShippingZone relation
 * @method     ChildCountryQuery innerJoinShippingZone($relationAlias = null) Adds a INNER JOIN clause to the query using the ShippingZone relation
 *
 * @method     ChildCountryQuery joinWithShippingZone($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ShippingZone relation
 *
 * @method     ChildCountryQuery leftJoinWithShippingZone() Adds a LEFT JOIN clause and with to the query using the ShippingZone relation
 * @method     ChildCountryQuery rightJoinWithShippingZone() Adds a RIGHT JOIN clause and with to the query using the ShippingZone relation
 * @method     ChildCountryQuery innerJoinWithShippingZone() Adds a INNER JOIN clause and with to the query using the ShippingZone relation
 *
 * @method     ChildCountryQuery leftJoinReseller($relationAlias = null) Adds a LEFT JOIN clause to the query using the Reseller relation
 * @method     ChildCountryQuery rightJoinReseller($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Reseller relation
 * @method     ChildCountryQuery innerJoinReseller($relationAlias = null) Adds a INNER JOIN clause to the query using the Reseller relation
 *
 * @method     ChildCountryQuery joinWithReseller($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Reseller relation
 *
 * @method     ChildCountryQuery leftJoinWithReseller() Adds a LEFT JOIN clause and with to the query using the Reseller relation
 * @method     ChildCountryQuery rightJoinWithReseller() Adds a RIGHT JOIN clause and with to the query using the Reseller relation
 * @method     ChildCountryQuery innerJoinWithReseller() Adds a INNER JOIN clause and with to the query using the Reseller relation
 *
 * @method     ChildCountryQuery leftJoinCountryShippingMethod($relationAlias = null) Adds a LEFT JOIN clause to the query using the CountryShippingMethod relation
 * @method     ChildCountryQuery rightJoinCountryShippingMethod($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CountryShippingMethod relation
 * @method     ChildCountryQuery innerJoinCountryShippingMethod($relationAlias = null) Adds a INNER JOIN clause to the query using the CountryShippingMethod relation
 *
 * @method     ChildCountryQuery joinWithCountryShippingMethod($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CountryShippingMethod relation
 *
 * @method     ChildCountryQuery leftJoinWithCountryShippingMethod() Adds a LEFT JOIN clause and with to the query using the CountryShippingMethod relation
 * @method     ChildCountryQuery rightJoinWithCountryShippingMethod() Adds a RIGHT JOIN clause and with to the query using the CountryShippingMethod relation
 * @method     ChildCountryQuery innerJoinWithCountryShippingMethod() Adds a INNER JOIN clause and with to the query using the CountryShippingMethod relation
 *
 * @method     ChildCountryQuery leftJoinCompanyRelatedByGeneralCountryId($relationAlias = null) Adds a LEFT JOIN clause to the query using the CompanyRelatedByGeneralCountryId relation
 * @method     ChildCountryQuery rightJoinCompanyRelatedByGeneralCountryId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CompanyRelatedByGeneralCountryId relation
 * @method     ChildCountryQuery innerJoinCompanyRelatedByGeneralCountryId($relationAlias = null) Adds a INNER JOIN clause to the query using the CompanyRelatedByGeneralCountryId relation
 *
 * @method     ChildCountryQuery joinWithCompanyRelatedByGeneralCountryId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CompanyRelatedByGeneralCountryId relation
 *
 * @method     ChildCountryQuery leftJoinWithCompanyRelatedByGeneralCountryId() Adds a LEFT JOIN clause and with to the query using the CompanyRelatedByGeneralCountryId relation
 * @method     ChildCountryQuery rightJoinWithCompanyRelatedByGeneralCountryId() Adds a RIGHT JOIN clause and with to the query using the CompanyRelatedByGeneralCountryId relation
 * @method     ChildCountryQuery innerJoinWithCompanyRelatedByGeneralCountryId() Adds a INNER JOIN clause and with to the query using the CompanyRelatedByGeneralCountryId relation
 *
 * @method     ChildCountryQuery leftJoinCompanyRelatedByInvoiceCountryId($relationAlias = null) Adds a LEFT JOIN clause to the query using the CompanyRelatedByInvoiceCountryId relation
 * @method     ChildCountryQuery rightJoinCompanyRelatedByInvoiceCountryId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CompanyRelatedByInvoiceCountryId relation
 * @method     ChildCountryQuery innerJoinCompanyRelatedByInvoiceCountryId($relationAlias = null) Adds a INNER JOIN clause to the query using the CompanyRelatedByInvoiceCountryId relation
 *
 * @method     ChildCountryQuery joinWithCompanyRelatedByInvoiceCountryId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CompanyRelatedByInvoiceCountryId relation
 *
 * @method     ChildCountryQuery leftJoinWithCompanyRelatedByInvoiceCountryId() Adds a LEFT JOIN clause and with to the query using the CompanyRelatedByInvoiceCountryId relation
 * @method     ChildCountryQuery rightJoinWithCompanyRelatedByInvoiceCountryId() Adds a RIGHT JOIN clause and with to the query using the CompanyRelatedByInvoiceCountryId relation
 * @method     ChildCountryQuery innerJoinWithCompanyRelatedByInvoiceCountryId() Adds a INNER JOIN clause and with to the query using the CompanyRelatedByInvoiceCountryId relation
 *
 * @method     ChildCountryQuery leftJoinCompanyRelatedByDeliveryCountryId($relationAlias = null) Adds a LEFT JOIN clause to the query using the CompanyRelatedByDeliveryCountryId relation
 * @method     ChildCountryQuery rightJoinCompanyRelatedByDeliveryCountryId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CompanyRelatedByDeliveryCountryId relation
 * @method     ChildCountryQuery innerJoinCompanyRelatedByDeliveryCountryId($relationAlias = null) Adds a INNER JOIN clause to the query using the CompanyRelatedByDeliveryCountryId relation
 *
 * @method     ChildCountryQuery joinWithCompanyRelatedByDeliveryCountryId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CompanyRelatedByDeliveryCountryId relation
 *
 * @method     ChildCountryQuery leftJoinWithCompanyRelatedByDeliveryCountryId() Adds a LEFT JOIN clause and with to the query using the CompanyRelatedByDeliveryCountryId relation
 * @method     ChildCountryQuery rightJoinWithCompanyRelatedByDeliveryCountryId() Adds a RIGHT JOIN clause and with to the query using the CompanyRelatedByDeliveryCountryId relation
 * @method     ChildCountryQuery innerJoinWithCompanyRelatedByDeliveryCountryId() Adds a INNER JOIN clause and with to the query using the CompanyRelatedByDeliveryCountryId relation
 *
 * @method     ChildCountryQuery leftJoinAddressCountry($relationAlias = null) Adds a LEFT JOIN clause to the query using the AddressCountry relation
 * @method     ChildCountryQuery rightJoinAddressCountry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AddressCountry relation
 * @method     ChildCountryQuery innerJoinAddressCountry($relationAlias = null) Adds a INNER JOIN clause to the query using the AddressCountry relation
 *
 * @method     ChildCountryQuery joinWithAddressCountry($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the AddressCountry relation
 *
 * @method     ChildCountryQuery leftJoinWithAddressCountry() Adds a LEFT JOIN clause and with to the query using the AddressCountry relation
 * @method     ChildCountryQuery rightJoinWithAddressCountry() Adds a RIGHT JOIN clause and with to the query using the AddressCountry relation
 * @method     ChildCountryQuery innerJoinWithAddressCountry() Adds a INNER JOIN clause and with to the query using the AddressCountry relation
 *
 * @method     \Model\Setting\MasterTable\ShippingZoneQuery|\Model\Reseller\ResellerQuery|\Model\Setting\MasterTable\CountryShippingMethodQuery|\Model\Company\CompanyQuery|\Model\Crm\CustomerAddressQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCountry findOne(ConnectionInterface $con = null) Return the first ChildCountry matching the query
 * @method     ChildCountry findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCountry matching the query, or a new ChildCountry object populated from the query conditions when no match is found
 *
 * @method     ChildCountry findOneById(int $id) Return the first ChildCountry filtered by the id column
 * @method     ChildCountry findOneByName(string $name) Return the first ChildCountry filtered by the name column
 * @method     ChildCountry findOneByShippingZoneId(int $shipping_zone_id) Return the first ChildCountry filtered by the shipping_zone_id column
 * @method     ChildCountry findOneByIso2(string $iso2) Return the first ChildCountry filtered by the iso2 column
 * @method     ChildCountry findOneByIso3(string $iso3) Return the first ChildCountry filtered by the iso3 column
 * @method     ChildCountry findOneByIsEu(boolean $is_eu) Return the first ChildCountry filtered by the is_eu column
 * @method     ChildCountry findOneByStealthMandatory(boolean $stealth_mandatory) Return the first ChildCountry filtered by the stealth_mandatory column
 * @method     ChildCountry findOneByStealthAdvised(boolean $stealth_advised) Return the first ChildCountry filtered by the stealth_advised column
 * @method     ChildCountry findOneByLatitude(string $latitude) Return the first ChildCountry filtered by the latitude column
 * @method     ChildCountry findOneByLongitude(string $longitude) Return the first ChildCountry filtered by the longitude column *

 * @method     ChildCountry requirePk($key, ConnectionInterface $con = null) Return the ChildCountry by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountry requireOne(ConnectionInterface $con = null) Return the first ChildCountry matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCountry requireOneById(int $id) Return the first ChildCountry filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountry requireOneByName(string $name) Return the first ChildCountry filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountry requireOneByShippingZoneId(int $shipping_zone_id) Return the first ChildCountry filtered by the shipping_zone_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountry requireOneByIso2(string $iso2) Return the first ChildCountry filtered by the iso2 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountry requireOneByIso3(string $iso3) Return the first ChildCountry filtered by the iso3 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountry requireOneByIsEu(boolean $is_eu) Return the first ChildCountry filtered by the is_eu column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountry requireOneByStealthMandatory(boolean $stealth_mandatory) Return the first ChildCountry filtered by the stealth_mandatory column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountry requireOneByStealthAdvised(boolean $stealth_advised) Return the first ChildCountry filtered by the stealth_advised column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountry requireOneByLatitude(string $latitude) Return the first ChildCountry filtered by the latitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountry requireOneByLongitude(string $longitude) Return the first ChildCountry filtered by the longitude column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCountry[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCountry objects based on current ModelCriteria
 * @method     ChildCountry[]|ObjectCollection findById(int $id) Return ChildCountry objects filtered by the id column
 * @method     ChildCountry[]|ObjectCollection findByName(string $name) Return ChildCountry objects filtered by the name column
 * @method     ChildCountry[]|ObjectCollection findByShippingZoneId(int $shipping_zone_id) Return ChildCountry objects filtered by the shipping_zone_id column
 * @method     ChildCountry[]|ObjectCollection findByIso2(string $iso2) Return ChildCountry objects filtered by the iso2 column
 * @method     ChildCountry[]|ObjectCollection findByIso3(string $iso3) Return ChildCountry objects filtered by the iso3 column
 * @method     ChildCountry[]|ObjectCollection findByIsEu(boolean $is_eu) Return ChildCountry objects filtered by the is_eu column
 * @method     ChildCountry[]|ObjectCollection findByStealthMandatory(boolean $stealth_mandatory) Return ChildCountry objects filtered by the stealth_mandatory column
 * @method     ChildCountry[]|ObjectCollection findByStealthAdvised(boolean $stealth_advised) Return ChildCountry objects filtered by the stealth_advised column
 * @method     ChildCountry[]|ObjectCollection findByLatitude(string $latitude) Return ChildCountry objects filtered by the latitude column
 * @method     ChildCountry[]|ObjectCollection findByLongitude(string $longitude) Return ChildCountry objects filtered by the longitude column
 * @method     ChildCountry[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CountryQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\MasterTable\Base\CountryQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\MasterTable\\Country', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCountryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCountryQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCountryQuery) {
            return $criteria;
        }
        $query = new ChildCountryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCountry|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CountryTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CountryTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCountry A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, shipping_zone_id, iso2, iso3, is_eu, stealth_mandatory, stealth_advised, latitude, longitude FROM mt_country WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCountry $obj */
            $obj = new ChildCountry();
            $obj->hydrate($row);
            CountryTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCountry|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CountryTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CountryTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CountryTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CountryTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountryTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountryTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the shipping_zone_id column
     *
     * Example usage:
     * <code>
     * $query->filterByShippingZoneId(1234); // WHERE shipping_zone_id = 1234
     * $query->filterByShippingZoneId(array(12, 34)); // WHERE shipping_zone_id IN (12, 34)
     * $query->filterByShippingZoneId(array('min' => 12)); // WHERE shipping_zone_id > 12
     * </code>
     *
     * @see       filterByShippingZone()
     *
     * @param     mixed $shippingZoneId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function filterByShippingZoneId($shippingZoneId = null, $comparison = null)
    {
        if (is_array($shippingZoneId)) {
            $useMinMax = false;
            if (isset($shippingZoneId['min'])) {
                $this->addUsingAlias(CountryTableMap::COL_SHIPPING_ZONE_ID, $shippingZoneId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shippingZoneId['max'])) {
                $this->addUsingAlias(CountryTableMap::COL_SHIPPING_ZONE_ID, $shippingZoneId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountryTableMap::COL_SHIPPING_ZONE_ID, $shippingZoneId, $comparison);
    }

    /**
     * Filter the query on the iso2 column
     *
     * Example usage:
     * <code>
     * $query->filterByIso2('fooValue');   // WHERE iso2 = 'fooValue'
     * $query->filterByIso2('%fooValue%', Criteria::LIKE); // WHERE iso2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iso2 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function filterByIso2($iso2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iso2)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountryTableMap::COL_ISO2, $iso2, $comparison);
    }

    /**
     * Filter the query on the iso3 column
     *
     * Example usage:
     * <code>
     * $query->filterByIso3('fooValue');   // WHERE iso3 = 'fooValue'
     * $query->filterByIso3('%fooValue%', Criteria::LIKE); // WHERE iso3 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iso3 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function filterByIso3($iso3 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iso3)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountryTableMap::COL_ISO3, $iso3, $comparison);
    }

    /**
     * Filter the query on the is_eu column
     *
     * Example usage:
     * <code>
     * $query->filterByIsEu(true); // WHERE is_eu = true
     * $query->filterByIsEu('yes'); // WHERE is_eu = true
     * </code>
     *
     * @param     boolean|string $isEu The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function filterByIsEu($isEu = null, $comparison = null)
    {
        if (is_string($isEu)) {
            $isEu = in_array(strtolower($isEu), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CountryTableMap::COL_IS_EU, $isEu, $comparison);
    }

    /**
     * Filter the query on the stealth_mandatory column
     *
     * Example usage:
     * <code>
     * $query->filterByStealthMandatory(true); // WHERE stealth_mandatory = true
     * $query->filterByStealthMandatory('yes'); // WHERE stealth_mandatory = true
     * </code>
     *
     * @param     boolean|string $stealthMandatory The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function filterByStealthMandatory($stealthMandatory = null, $comparison = null)
    {
        if (is_string($stealthMandatory)) {
            $stealthMandatory = in_array(strtolower($stealthMandatory), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CountryTableMap::COL_STEALTH_MANDATORY, $stealthMandatory, $comparison);
    }

    /**
     * Filter the query on the stealth_advised column
     *
     * Example usage:
     * <code>
     * $query->filterByStealthAdvised(true); // WHERE stealth_advised = true
     * $query->filterByStealthAdvised('yes'); // WHERE stealth_advised = true
     * </code>
     *
     * @param     boolean|string $stealthAdvised The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function filterByStealthAdvised($stealthAdvised = null, $comparison = null)
    {
        if (is_string($stealthAdvised)) {
            $stealthAdvised = in_array(strtolower($stealthAdvised), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CountryTableMap::COL_STEALTH_ADVISED, $stealthAdvised, $comparison);
    }

    /**
     * Filter the query on the latitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLatitude('fooValue');   // WHERE latitude = 'fooValue'
     * $query->filterByLatitude('%fooValue%', Criteria::LIKE); // WHERE latitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $latitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function filterByLatitude($latitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($latitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountryTableMap::COL_LATITUDE, $latitude, $comparison);
    }

    /**
     * Filter the query on the longitude column
     *
     * Example usage:
     * <code>
     * $query->filterByLongitude('fooValue');   // WHERE longitude = 'fooValue'
     * $query->filterByLongitude('%fooValue%', Criteria::LIKE); // WHERE longitude LIKE '%fooValue%'
     * </code>
     *
     * @param     string $longitude The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function filterByLongitude($longitude = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($longitude)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountryTableMap::COL_LONGITUDE, $longitude, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\ShippingZone object
     *
     * @param \Model\Setting\MasterTable\ShippingZone|ObjectCollection $shippingZone The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCountryQuery The current query, for fluid interface
     */
    public function filterByShippingZone($shippingZone, $comparison = null)
    {
        if ($shippingZone instanceof \Model\Setting\MasterTable\ShippingZone) {
            return $this
                ->addUsingAlias(CountryTableMap::COL_SHIPPING_ZONE_ID, $shippingZone->getId(), $comparison);
        } elseif ($shippingZone instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CountryTableMap::COL_SHIPPING_ZONE_ID, $shippingZone->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByShippingZone() only accepts arguments of type \Model\Setting\MasterTable\ShippingZone or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ShippingZone relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function joinShippingZone($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ShippingZone');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ShippingZone');
        }

        return $this;
    }

    /**
     * Use the ShippingZone relation ShippingZone object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\ShippingZoneQuery A secondary query class using the current class as primary query
     */
    public function useShippingZoneQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinShippingZone($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ShippingZone', '\Model\Setting\MasterTable\ShippingZoneQuery');
    }

    /**
     * Filter the query by a related \Model\Reseller\Reseller object
     *
     * @param \Model\Reseller\Reseller|ObjectCollection $reseller the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCountryQuery The current query, for fluid interface
     */
    public function filterByReseller($reseller, $comparison = null)
    {
        if ($reseller instanceof \Model\Reseller\Reseller) {
            return $this
                ->addUsingAlias(CountryTableMap::COL_ID, $reseller->getCountryId(), $comparison);
        } elseif ($reseller instanceof ObjectCollection) {
            return $this
                ->useResellerQuery()
                ->filterByPrimaryKeys($reseller->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByReseller() only accepts arguments of type \Model\Reseller\Reseller or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Reseller relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function joinReseller($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Reseller');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Reseller');
        }

        return $this;
    }

    /**
     * Use the Reseller relation Reseller object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Reseller\ResellerQuery A secondary query class using the current class as primary query
     */
    public function useResellerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinReseller($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Reseller', '\Model\Reseller\ResellerQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\CountryShippingMethod object
     *
     * @param \Model\Setting\MasterTable\CountryShippingMethod|ObjectCollection $countryShippingMethod the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCountryQuery The current query, for fluid interface
     */
    public function filterByCountryShippingMethod($countryShippingMethod, $comparison = null)
    {
        if ($countryShippingMethod instanceof \Model\Setting\MasterTable\CountryShippingMethod) {
            return $this
                ->addUsingAlias(CountryTableMap::COL_ID, $countryShippingMethod->getCountryId(), $comparison);
        } elseif ($countryShippingMethod instanceof ObjectCollection) {
            return $this
                ->useCountryShippingMethodQuery()
                ->filterByPrimaryKeys($countryShippingMethod->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCountryShippingMethod() only accepts arguments of type \Model\Setting\MasterTable\CountryShippingMethod or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CountryShippingMethod relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function joinCountryShippingMethod($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CountryShippingMethod');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CountryShippingMethod');
        }

        return $this;
    }

    /**
     * Use the CountryShippingMethod relation CountryShippingMethod object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\CountryShippingMethodQuery A secondary query class using the current class as primary query
     */
    public function useCountryShippingMethodQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCountryShippingMethod($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CountryShippingMethod', '\Model\Setting\MasterTable\CountryShippingMethodQuery');
    }

    /**
     * Filter the query by a related \Model\Company\Company object
     *
     * @param \Model\Company\Company|ObjectCollection $company the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCountryQuery The current query, for fluid interface
     */
    public function filterByCompanyRelatedByGeneralCountryId($company, $comparison = null)
    {
        if ($company instanceof \Model\Company\Company) {
            return $this
                ->addUsingAlias(CountryTableMap::COL_ID, $company->getGeneralCountryId(), $comparison);
        } elseif ($company instanceof ObjectCollection) {
            return $this
                ->useCompanyRelatedByGeneralCountryIdQuery()
                ->filterByPrimaryKeys($company->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCompanyRelatedByGeneralCountryId() only accepts arguments of type \Model\Company\Company or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CompanyRelatedByGeneralCountryId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function joinCompanyRelatedByGeneralCountryId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CompanyRelatedByGeneralCountryId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CompanyRelatedByGeneralCountryId');
        }

        return $this;
    }

    /**
     * Use the CompanyRelatedByGeneralCountryId relation Company object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Company\CompanyQuery A secondary query class using the current class as primary query
     */
    public function useCompanyRelatedByGeneralCountryIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCompanyRelatedByGeneralCountryId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CompanyRelatedByGeneralCountryId', '\Model\Company\CompanyQuery');
    }

    /**
     * Filter the query by a related \Model\Company\Company object
     *
     * @param \Model\Company\Company|ObjectCollection $company the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCountryQuery The current query, for fluid interface
     */
    public function filterByCompanyRelatedByInvoiceCountryId($company, $comparison = null)
    {
        if ($company instanceof \Model\Company\Company) {
            return $this
                ->addUsingAlias(CountryTableMap::COL_ID, $company->getInvoiceCountryId(), $comparison);
        } elseif ($company instanceof ObjectCollection) {
            return $this
                ->useCompanyRelatedByInvoiceCountryIdQuery()
                ->filterByPrimaryKeys($company->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCompanyRelatedByInvoiceCountryId() only accepts arguments of type \Model\Company\Company or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CompanyRelatedByInvoiceCountryId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function joinCompanyRelatedByInvoiceCountryId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CompanyRelatedByInvoiceCountryId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CompanyRelatedByInvoiceCountryId');
        }

        return $this;
    }

    /**
     * Use the CompanyRelatedByInvoiceCountryId relation Company object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Company\CompanyQuery A secondary query class using the current class as primary query
     */
    public function useCompanyRelatedByInvoiceCountryIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCompanyRelatedByInvoiceCountryId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CompanyRelatedByInvoiceCountryId', '\Model\Company\CompanyQuery');
    }

    /**
     * Filter the query by a related \Model\Company\Company object
     *
     * @param \Model\Company\Company|ObjectCollection $company the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCountryQuery The current query, for fluid interface
     */
    public function filterByCompanyRelatedByDeliveryCountryId($company, $comparison = null)
    {
        if ($company instanceof \Model\Company\Company) {
            return $this
                ->addUsingAlias(CountryTableMap::COL_ID, $company->getDeliveryCountryId(), $comparison);
        } elseif ($company instanceof ObjectCollection) {
            return $this
                ->useCompanyRelatedByDeliveryCountryIdQuery()
                ->filterByPrimaryKeys($company->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCompanyRelatedByDeliveryCountryId() only accepts arguments of type \Model\Company\Company or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CompanyRelatedByDeliveryCountryId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function joinCompanyRelatedByDeliveryCountryId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CompanyRelatedByDeliveryCountryId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CompanyRelatedByDeliveryCountryId');
        }

        return $this;
    }

    /**
     * Use the CompanyRelatedByDeliveryCountryId relation Company object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Company\CompanyQuery A secondary query class using the current class as primary query
     */
    public function useCompanyRelatedByDeliveryCountryIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCompanyRelatedByDeliveryCountryId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CompanyRelatedByDeliveryCountryId', '\Model\Company\CompanyQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\CustomerAddress object
     *
     * @param \Model\Crm\CustomerAddress|ObjectCollection $customerAddress the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCountryQuery The current query, for fluid interface
     */
    public function filterByAddressCountry($customerAddress, $comparison = null)
    {
        if ($customerAddress instanceof \Model\Crm\CustomerAddress) {
            return $this
                ->addUsingAlias(CountryTableMap::COL_ID, $customerAddress->getCountryId(), $comparison);
        } elseif ($customerAddress instanceof ObjectCollection) {
            return $this
                ->useAddressCountryQuery()
                ->filterByPrimaryKeys($customerAddress->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByAddressCountry() only accepts arguments of type \Model\Crm\CustomerAddress or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AddressCountry relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function joinAddressCountry($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AddressCountry');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AddressCountry');
        }

        return $this;
    }

    /**
     * Use the AddressCountry relation CustomerAddress object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerAddressQuery A secondary query class using the current class as primary query
     */
    public function useAddressCountryQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAddressCountry($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AddressCountry', '\Model\Crm\CustomerAddressQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCountry $country Object to remove from the list of results
     *
     * @return $this|ChildCountryQuery The current query, for fluid interface
     */
    public function prune($country = null)
    {
        if ($country) {
            $this->addUsingAlias(CountryTableMap::COL_ID, $country->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mt_country table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CountryTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CountryTableMap::clearInstancePool();
            CountryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CountryTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CountryTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CountryTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CountryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CountryQuery
