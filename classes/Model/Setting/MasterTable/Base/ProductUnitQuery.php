<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\Product;
use Model\Setting\MasterTable\ProductUnit as ChildProductUnit;
use Model\Setting\MasterTable\ProductUnitQuery as ChildProductUnitQuery;
use Model\Setting\MasterTable\Map\ProductUnitTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mt_unit' table.
 *
 *
 *
 * @method     ChildProductUnitQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildProductUnitQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildProductUnitQuery orderByNamePlural($order = Criteria::ASC) Order by the name_plural column
 * @method     ChildProductUnitQuery orderByCode($order = Criteria::ASC) Order by the code column
 *
 * @method     ChildProductUnitQuery groupById() Group by the id column
 * @method     ChildProductUnitQuery groupByName() Group by the name column
 * @method     ChildProductUnitQuery groupByNamePlural() Group by the name_plural column
 * @method     ChildProductUnitQuery groupByCode() Group by the code column
 *
 * @method     ChildProductUnitQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProductUnitQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProductUnitQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProductUnitQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildProductUnitQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildProductUnitQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildProductUnitQuery leftJoinProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the Product relation
 * @method     ChildProductUnitQuery rightJoinProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Product relation
 * @method     ChildProductUnitQuery innerJoinProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the Product relation
 *
 * @method     ChildProductUnitQuery joinWithProduct($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Product relation
 *
 * @method     ChildProductUnitQuery leftJoinWithProduct() Adds a LEFT JOIN clause and with to the query using the Product relation
 * @method     ChildProductUnitQuery rightJoinWithProduct() Adds a RIGHT JOIN clause and with to the query using the Product relation
 * @method     ChildProductUnitQuery innerJoinWithProduct() Adds a INNER JOIN clause and with to the query using the Product relation
 *
 * @method     ChildProductUnitQuery leftJoinProductUnitTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductUnitTranslation relation
 * @method     ChildProductUnitQuery rightJoinProductUnitTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductUnitTranslation relation
 * @method     ChildProductUnitQuery innerJoinProductUnitTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductUnitTranslation relation
 *
 * @method     ChildProductUnitQuery joinWithProductUnitTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductUnitTranslation relation
 *
 * @method     ChildProductUnitQuery leftJoinWithProductUnitTranslation() Adds a LEFT JOIN clause and with to the query using the ProductUnitTranslation relation
 * @method     ChildProductUnitQuery rightJoinWithProductUnitTranslation() Adds a RIGHT JOIN clause and with to the query using the ProductUnitTranslation relation
 * @method     ChildProductUnitQuery innerJoinWithProductUnitTranslation() Adds a INNER JOIN clause and with to the query using the ProductUnitTranslation relation
 *
 * @method     \Model\ProductQuery|\Model\Setting\MasterTable\ProductUnitTranslationQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildProductUnit findOne(ConnectionInterface $con = null) Return the first ChildProductUnit matching the query
 * @method     ChildProductUnit findOneOrCreate(ConnectionInterface $con = null) Return the first ChildProductUnit matching the query, or a new ChildProductUnit object populated from the query conditions when no match is found
 *
 * @method     ChildProductUnit findOneById(int $id) Return the first ChildProductUnit filtered by the id column
 * @method     ChildProductUnit findOneByName(string $name) Return the first ChildProductUnit filtered by the name column
 * @method     ChildProductUnit findOneByNamePlural(string $name_plural) Return the first ChildProductUnit filtered by the name_plural column
 * @method     ChildProductUnit findOneByCode(string $code) Return the first ChildProductUnit filtered by the code column *

 * @method     ChildProductUnit requirePk($key, ConnectionInterface $con = null) Return the ChildProductUnit by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProductUnit requireOne(ConnectionInterface $con = null) Return the first ChildProductUnit matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProductUnit requireOneById(int $id) Return the first ChildProductUnit filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProductUnit requireOneByName(string $name) Return the first ChildProductUnit filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProductUnit requireOneByNamePlural(string $name_plural) Return the first ChildProductUnit filtered by the name_plural column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProductUnit requireOneByCode(string $code) Return the first ChildProductUnit filtered by the code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProductUnit[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildProductUnit objects based on current ModelCriteria
 * @method     ChildProductUnit[]|ObjectCollection findById(int $id) Return ChildProductUnit objects filtered by the id column
 * @method     ChildProductUnit[]|ObjectCollection findByName(string $name) Return ChildProductUnit objects filtered by the name column
 * @method     ChildProductUnit[]|ObjectCollection findByNamePlural(string $name_plural) Return ChildProductUnit objects filtered by the name_plural column
 * @method     ChildProductUnit[]|ObjectCollection findByCode(string $code) Return ChildProductUnit objects filtered by the code column
 * @method     ChildProductUnit[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProductUnitQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\MasterTable\Base\ProductUnitQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\MasterTable\\ProductUnit', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProductUnitQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProductUnitQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProductUnitQuery) {
            return $criteria;
        }
        $query = new ChildProductUnitQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildProductUnit|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProductUnitTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ProductUnitTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProductUnit A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, name_plural, code FROM mt_unit WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildProductUnit $obj */
            $obj = new ChildProductUnit();
            $obj->hydrate($row);
            ProductUnitTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProductUnit|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProductUnitQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProductUnitTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProductUnitQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProductUnitTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductUnitQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ProductUnitTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ProductUnitTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductUnitTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductUnitQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductUnitTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the name_plural column
     *
     * Example usage:
     * <code>
     * $query->filterByNamePlural('fooValue');   // WHERE name_plural = 'fooValue'
     * $query->filterByNamePlural('%fooValue%', Criteria::LIKE); // WHERE name_plural LIKE '%fooValue%'
     * </code>
     *
     * @param     string $namePlural The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductUnitQuery The current query, for fluid interface
     */
    public function filterByNamePlural($namePlural = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($namePlural)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductUnitTableMap::COL_NAME_PLURAL, $namePlural, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%', Criteria::LIKE); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductUnitQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductUnitTableMap::COL_CODE, $code, $comparison);
    }

    /**
     * Filter the query by a related \Model\Product object
     *
     * @param \Model\Product|ObjectCollection $product the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductUnitQuery The current query, for fluid interface
     */
    public function filterByProduct($product, $comparison = null)
    {
        if ($product instanceof \Model\Product) {
            return $this
                ->addUsingAlias(ProductUnitTableMap::COL_ID, $product->getUnitId(), $comparison);
        } elseif ($product instanceof ObjectCollection) {
            return $this
                ->useProductQuery()
                ->filterByPrimaryKeys($product->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProduct() only accepts arguments of type \Model\Product or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Product relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductUnitQuery The current query, for fluid interface
     */
    public function joinProduct($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Product');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Product');
        }

        return $this;
    }

    /**
     * Use the Product relation Product object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ProductQuery A secondary query class using the current class as primary query
     */
    public function useProductQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Product', '\Model\ProductQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\ProductUnitTranslation object
     *
     * @param \Model\Setting\MasterTable\ProductUnitTranslation|ObjectCollection $productUnitTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductUnitQuery The current query, for fluid interface
     */
    public function filterByProductUnitTranslation($productUnitTranslation, $comparison = null)
    {
        if ($productUnitTranslation instanceof \Model\Setting\MasterTable\ProductUnitTranslation) {
            return $this
                ->addUsingAlias(ProductUnitTableMap::COL_ID, $productUnitTranslation->getUnitId(), $comparison);
        } elseif ($productUnitTranslation instanceof ObjectCollection) {
            return $this
                ->useProductUnitTranslationQuery()
                ->filterByPrimaryKeys($productUnitTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductUnitTranslation() only accepts arguments of type \Model\Setting\MasterTable\ProductUnitTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductUnitTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductUnitQuery The current query, for fluid interface
     */
    public function joinProductUnitTranslation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductUnitTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductUnitTranslation');
        }

        return $this;
    }

    /**
     * Use the ProductUnitTranslation relation ProductUnitTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\ProductUnitTranslationQuery A secondary query class using the current class as primary query
     */
    public function useProductUnitTranslationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProductUnitTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductUnitTranslation', '\Model\Setting\MasterTable\ProductUnitTranslationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildProductUnit $productUnit Object to remove from the list of results
     *
     * @return $this|ChildProductUnitQuery The current query, for fluid interface
     */
    public function prune($productUnit = null)
    {
        if ($productUnit) {
            $this->addUsingAlias(ProductUnitTableMap::COL_ID, $productUnit->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mt_unit table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductUnitTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProductUnitTableMap::clearInstancePool();
            ProductUnitTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductUnitTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProductUnitTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProductUnitTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProductUnitTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ProductUnitQuery
