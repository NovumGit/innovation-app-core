<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\SaleOrderNotification\SaleOrderNotification;
use Model\Setting\MasterTable\Sale_order_notification_type as ChildSale_order_notification_type;
use Model\Setting\MasterTable\Sale_order_notification_typeQuery as ChildSale_order_notification_typeQuery;
use Model\Setting\MasterTable\Map\Sale_order_notification_typeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mt_sale_order_notification_type' table.
 *
 *
 *
 * @method     ChildSale_order_notification_typeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSale_order_notification_typeQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method     ChildSale_order_notification_typeQuery orderByName($order = Criteria::ASC) Order by the name column
 *
 * @method     ChildSale_order_notification_typeQuery groupById() Group by the id column
 * @method     ChildSale_order_notification_typeQuery groupByCode() Group by the code column
 * @method     ChildSale_order_notification_typeQuery groupByName() Group by the name column
 *
 * @method     ChildSale_order_notification_typeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSale_order_notification_typeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSale_order_notification_typeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSale_order_notification_typeQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSale_order_notification_typeQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSale_order_notification_typeQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSale_order_notification_typeQuery leftJoinSaleOrderNotification($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrderNotification relation
 * @method     ChildSale_order_notification_typeQuery rightJoinSaleOrderNotification($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrderNotification relation
 * @method     ChildSale_order_notification_typeQuery innerJoinSaleOrderNotification($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrderNotification relation
 *
 * @method     ChildSale_order_notification_typeQuery joinWithSaleOrderNotification($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrderNotification relation
 *
 * @method     ChildSale_order_notification_typeQuery leftJoinWithSaleOrderNotification() Adds a LEFT JOIN clause and with to the query using the SaleOrderNotification relation
 * @method     ChildSale_order_notification_typeQuery rightJoinWithSaleOrderNotification() Adds a RIGHT JOIN clause and with to the query using the SaleOrderNotification relation
 * @method     ChildSale_order_notification_typeQuery innerJoinWithSaleOrderNotification() Adds a INNER JOIN clause and with to the query using the SaleOrderNotification relation
 *
 * @method     \Model\SaleOrderNotification\SaleOrderNotificationQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSale_order_notification_type findOne(ConnectionInterface $con = null) Return the first ChildSale_order_notification_type matching the query
 * @method     ChildSale_order_notification_type findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSale_order_notification_type matching the query, or a new ChildSale_order_notification_type object populated from the query conditions when no match is found
 *
 * @method     ChildSale_order_notification_type findOneById(int $id) Return the first ChildSale_order_notification_type filtered by the id column
 * @method     ChildSale_order_notification_type findOneByCode(string $code) Return the first ChildSale_order_notification_type filtered by the code column
 * @method     ChildSale_order_notification_type findOneByName(string $name) Return the first ChildSale_order_notification_type filtered by the name column *

 * @method     ChildSale_order_notification_type requirePk($key, ConnectionInterface $con = null) Return the ChildSale_order_notification_type by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSale_order_notification_type requireOne(ConnectionInterface $con = null) Return the first ChildSale_order_notification_type matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSale_order_notification_type requireOneById(int $id) Return the first ChildSale_order_notification_type filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSale_order_notification_type requireOneByCode(string $code) Return the first ChildSale_order_notification_type filtered by the code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSale_order_notification_type requireOneByName(string $name) Return the first ChildSale_order_notification_type filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSale_order_notification_type[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSale_order_notification_type objects based on current ModelCriteria
 * @method     ChildSale_order_notification_type[]|ObjectCollection findById(int $id) Return ChildSale_order_notification_type objects filtered by the id column
 * @method     ChildSale_order_notification_type[]|ObjectCollection findByCode(string $code) Return ChildSale_order_notification_type objects filtered by the code column
 * @method     ChildSale_order_notification_type[]|ObjectCollection findByName(string $name) Return ChildSale_order_notification_type objects filtered by the name column
 * @method     ChildSale_order_notification_type[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class Sale_order_notification_typeQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\MasterTable\Base\Sale_order_notification_typeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\MasterTable\\Sale_order_notification_type', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSale_order_notification_typeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSale_order_notification_typeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSale_order_notification_typeQuery) {
            return $criteria;
        }
        $query = new ChildSale_order_notification_typeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSale_order_notification_type|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(Sale_order_notification_typeTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = Sale_order_notification_typeTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSale_order_notification_type A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, code, name FROM mt_sale_order_notification_type WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSale_order_notification_type $obj */
            $obj = new ChildSale_order_notification_type();
            $obj->hydrate($row);
            Sale_order_notification_typeTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSale_order_notification_type|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSale_order_notification_typeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(Sale_order_notification_typeTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSale_order_notification_typeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(Sale_order_notification_typeTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSale_order_notification_typeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(Sale_order_notification_typeTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(Sale_order_notification_typeTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Sale_order_notification_typeTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%', Criteria::LIKE); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSale_order_notification_typeQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Sale_order_notification_typeTableMap::COL_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSale_order_notification_typeQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Sale_order_notification_typeTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query by a related \Model\SaleOrderNotification\SaleOrderNotification object
     *
     * @param \Model\SaleOrderNotification\SaleOrderNotification|ObjectCollection $saleOrderNotification the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSale_order_notification_typeQuery The current query, for fluid interface
     */
    public function filterBySaleOrderNotification($saleOrderNotification, $comparison = null)
    {
        if ($saleOrderNotification instanceof \Model\SaleOrderNotification\SaleOrderNotification) {
            return $this
                ->addUsingAlias(Sale_order_notification_typeTableMap::COL_ID, $saleOrderNotification->getSaleOrderNotificationTypeId(), $comparison);
        } elseif ($saleOrderNotification instanceof ObjectCollection) {
            return $this
                ->useSaleOrderNotificationQuery()
                ->filterByPrimaryKeys($saleOrderNotification->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleOrderNotification() only accepts arguments of type \Model\SaleOrderNotification\SaleOrderNotification or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrderNotification relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSale_order_notification_typeQuery The current query, for fluid interface
     */
    public function joinSaleOrderNotification($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrderNotification');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrderNotification');
        }

        return $this;
    }

    /**
     * Use the SaleOrderNotification relation SaleOrderNotification object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\SaleOrderNotification\SaleOrderNotificationQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderNotificationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaleOrderNotification($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrderNotification', '\Model\SaleOrderNotification\SaleOrderNotificationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSale_order_notification_type $sale_order_notification_type Object to remove from the list of results
     *
     * @return $this|ChildSale_order_notification_typeQuery The current query, for fluid interface
     */
    public function prune($sale_order_notification_type = null)
    {
        if ($sale_order_notification_type) {
            $this->addUsingAlias(Sale_order_notification_typeTableMap::COL_ID, $sale_order_notification_type->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mt_sale_order_notification_type table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Sale_order_notification_typeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            Sale_order_notification_typeTableMap::clearInstancePool();
            Sale_order_notification_typeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Sale_order_notification_typeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(Sale_order_notification_typeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            Sale_order_notification_typeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            Sale_order_notification_typeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // Sale_order_notification_typeQuery
