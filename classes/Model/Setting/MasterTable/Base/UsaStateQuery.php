<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\Crm\CustomerAddress;
use Model\Setting\MasterTable\UsaState as ChildUsaState;
use Model\Setting\MasterTable\UsaStateQuery as ChildUsaStateQuery;
use Model\Setting\MasterTable\Map\UsaStateTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mt_usa_state' table.
 *
 *
 *
 * @method     ChildUsaStateQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUsaStateQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildUsaStateQuery orderByAbrev($order = Criteria::ASC) Order by the abrev column
 *
 * @method     ChildUsaStateQuery groupById() Group by the id column
 * @method     ChildUsaStateQuery groupByName() Group by the name column
 * @method     ChildUsaStateQuery groupByAbrev() Group by the abrev column
 *
 * @method     ChildUsaStateQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUsaStateQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUsaStateQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUsaStateQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUsaStateQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUsaStateQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUsaStateQuery leftJoinUsaState($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsaState relation
 * @method     ChildUsaStateQuery rightJoinUsaState($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsaState relation
 * @method     ChildUsaStateQuery innerJoinUsaState($relationAlias = null) Adds a INNER JOIN clause to the query using the UsaState relation
 *
 * @method     ChildUsaStateQuery joinWithUsaState($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsaState relation
 *
 * @method     ChildUsaStateQuery leftJoinWithUsaState() Adds a LEFT JOIN clause and with to the query using the UsaState relation
 * @method     ChildUsaStateQuery rightJoinWithUsaState() Adds a RIGHT JOIN clause and with to the query using the UsaState relation
 * @method     ChildUsaStateQuery innerJoinWithUsaState() Adds a INNER JOIN clause and with to the query using the UsaState relation
 *
 * @method     \Model\Crm\CustomerAddressQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUsaState findOne(ConnectionInterface $con = null) Return the first ChildUsaState matching the query
 * @method     ChildUsaState findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUsaState matching the query, or a new ChildUsaState object populated from the query conditions when no match is found
 *
 * @method     ChildUsaState findOneById(int $id) Return the first ChildUsaState filtered by the id column
 * @method     ChildUsaState findOneByName(string $name) Return the first ChildUsaState filtered by the name column
 * @method     ChildUsaState findOneByAbrev(string $abrev) Return the first ChildUsaState filtered by the abrev column *

 * @method     ChildUsaState requirePk($key, ConnectionInterface $con = null) Return the ChildUsaState by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsaState requireOne(ConnectionInterface $con = null) Return the first ChildUsaState matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsaState requireOneById(int $id) Return the first ChildUsaState filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsaState requireOneByName(string $name) Return the first ChildUsaState filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUsaState requireOneByAbrev(string $abrev) Return the first ChildUsaState filtered by the abrev column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUsaState[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUsaState objects based on current ModelCriteria
 * @method     ChildUsaState[]|ObjectCollection findById(int $id) Return ChildUsaState objects filtered by the id column
 * @method     ChildUsaState[]|ObjectCollection findByName(string $name) Return ChildUsaState objects filtered by the name column
 * @method     ChildUsaState[]|ObjectCollection findByAbrev(string $abrev) Return ChildUsaState objects filtered by the abrev column
 * @method     ChildUsaState[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UsaStateQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\MasterTable\Base\UsaStateQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\MasterTable\\UsaState', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUsaStateQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUsaStateQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUsaStateQuery) {
            return $criteria;
        }
        $query = new ChildUsaStateQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUsaState|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UsaStateTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UsaStateTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUsaState A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, abrev FROM mt_usa_state WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUsaState $obj */
            $obj = new ChildUsaState();
            $obj->hydrate($row);
            UsaStateTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUsaState|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUsaStateQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UsaStateTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUsaStateQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UsaStateTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsaStateQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UsaStateTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UsaStateTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsaStateTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsaStateQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsaStateTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the abrev column
     *
     * Example usage:
     * <code>
     * $query->filterByAbrev('fooValue');   // WHERE abrev = 'fooValue'
     * $query->filterByAbrev('%fooValue%', Criteria::LIKE); // WHERE abrev LIKE '%fooValue%'
     * </code>
     *
     * @param     string $abrev The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUsaStateQuery The current query, for fluid interface
     */
    public function filterByAbrev($abrev = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($abrev)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsaStateTableMap::COL_ABREV, $abrev, $comparison);
    }

    /**
     * Filter the query by a related \Model\Crm\CustomerAddress object
     *
     * @param \Model\Crm\CustomerAddress|ObjectCollection $customerAddress the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUsaStateQuery The current query, for fluid interface
     */
    public function filterByUsaState($customerAddress, $comparison = null)
    {
        if ($customerAddress instanceof \Model\Crm\CustomerAddress) {
            return $this
                ->addUsingAlias(UsaStateTableMap::COL_ID, $customerAddress->getUsaStateId(), $comparison);
        } elseif ($customerAddress instanceof ObjectCollection) {
            return $this
                ->useUsaStateQuery()
                ->filterByPrimaryKeys($customerAddress->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsaState() only accepts arguments of type \Model\Crm\CustomerAddress or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsaState relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUsaStateQuery The current query, for fluid interface
     */
    public function joinUsaState($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsaState');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsaState');
        }

        return $this;
    }

    /**
     * Use the UsaState relation CustomerAddress object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerAddressQuery A secondary query class using the current class as primary query
     */
    public function useUsaStateQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsaState($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsaState', '\Model\Crm\CustomerAddressQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUsaState $usaState Object to remove from the list of results
     *
     * @return $this|ChildUsaStateQuery The current query, for fluid interface
     */
    public function prune($usaState = null)
    {
        if ($usaState) {
            $this->addUsingAlias(UsaStateTableMap::COL_ID, $usaState->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mt_usa_state table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsaStateTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UsaStateTableMap::clearInstancePool();
            UsaStateTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsaStateTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UsaStateTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UsaStateTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UsaStateTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UsaStateQuery
