<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\Setting\MasterTable\CountryShippingMethod as ChildCountryShippingMethod;
use Model\Setting\MasterTable\CountryShippingMethodQuery as ChildCountryShippingMethodQuery;
use Model\Setting\MasterTable\Map\CountryShippingMethodTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'country_shipping_method' table.
 *
 *
 *
 * @method     ChildCountryShippingMethodQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCountryShippingMethodQuery orderByCountryId($order = Criteria::ASC) Order by the country_id column
 * @method     ChildCountryShippingMethodQuery orderByShippingMethodId($order = Criteria::ASC) Order by the shipping_method_id column
 * @method     ChildCountryShippingMethodQuery orderByIsEnabledWebsite($order = Criteria::ASC) Order by the is_enabled_website column
 * @method     ChildCountryShippingMethodQuery orderByPrice($order = Criteria::ASC) Order by the price column
 *
 * @method     ChildCountryShippingMethodQuery groupById() Group by the id column
 * @method     ChildCountryShippingMethodQuery groupByCountryId() Group by the country_id column
 * @method     ChildCountryShippingMethodQuery groupByShippingMethodId() Group by the shipping_method_id column
 * @method     ChildCountryShippingMethodQuery groupByIsEnabledWebsite() Group by the is_enabled_website column
 * @method     ChildCountryShippingMethodQuery groupByPrice() Group by the price column
 *
 * @method     ChildCountryShippingMethodQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCountryShippingMethodQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCountryShippingMethodQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCountryShippingMethodQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCountryShippingMethodQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCountryShippingMethodQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCountryShippingMethodQuery leftJoinCountry($relationAlias = null) Adds a LEFT JOIN clause to the query using the Country relation
 * @method     ChildCountryShippingMethodQuery rightJoinCountry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Country relation
 * @method     ChildCountryShippingMethodQuery innerJoinCountry($relationAlias = null) Adds a INNER JOIN clause to the query using the Country relation
 *
 * @method     ChildCountryShippingMethodQuery joinWithCountry($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Country relation
 *
 * @method     ChildCountryShippingMethodQuery leftJoinWithCountry() Adds a LEFT JOIN clause and with to the query using the Country relation
 * @method     ChildCountryShippingMethodQuery rightJoinWithCountry() Adds a RIGHT JOIN clause and with to the query using the Country relation
 * @method     ChildCountryShippingMethodQuery innerJoinWithCountry() Adds a INNER JOIN clause and with to the query using the Country relation
 *
 * @method     ChildCountryShippingMethodQuery leftJoinShippingMethod($relationAlias = null) Adds a LEFT JOIN clause to the query using the ShippingMethod relation
 * @method     ChildCountryShippingMethodQuery rightJoinShippingMethod($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ShippingMethod relation
 * @method     ChildCountryShippingMethodQuery innerJoinShippingMethod($relationAlias = null) Adds a INNER JOIN clause to the query using the ShippingMethod relation
 *
 * @method     ChildCountryShippingMethodQuery joinWithShippingMethod($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ShippingMethod relation
 *
 * @method     ChildCountryShippingMethodQuery leftJoinWithShippingMethod() Adds a LEFT JOIN clause and with to the query using the ShippingMethod relation
 * @method     ChildCountryShippingMethodQuery rightJoinWithShippingMethod() Adds a RIGHT JOIN clause and with to the query using the ShippingMethod relation
 * @method     ChildCountryShippingMethodQuery innerJoinWithShippingMethod() Adds a INNER JOIN clause and with to the query using the ShippingMethod relation
 *
 * @method     \Model\Setting\MasterTable\CountryQuery|\Model\Setting\MasterTable\ShippingMethodQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCountryShippingMethod findOne(ConnectionInterface $con = null) Return the first ChildCountryShippingMethod matching the query
 * @method     ChildCountryShippingMethod findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCountryShippingMethod matching the query, or a new ChildCountryShippingMethod object populated from the query conditions when no match is found
 *
 * @method     ChildCountryShippingMethod findOneById(int $id) Return the first ChildCountryShippingMethod filtered by the id column
 * @method     ChildCountryShippingMethod findOneByCountryId(int $country_id) Return the first ChildCountryShippingMethod filtered by the country_id column
 * @method     ChildCountryShippingMethod findOneByShippingMethodId(int $shipping_method_id) Return the first ChildCountryShippingMethod filtered by the shipping_method_id column
 * @method     ChildCountryShippingMethod findOneByIsEnabledWebsite(boolean $is_enabled_website) Return the first ChildCountryShippingMethod filtered by the is_enabled_website column
 * @method     ChildCountryShippingMethod findOneByPrice(double $price) Return the first ChildCountryShippingMethod filtered by the price column *

 * @method     ChildCountryShippingMethod requirePk($key, ConnectionInterface $con = null) Return the ChildCountryShippingMethod by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountryShippingMethod requireOne(ConnectionInterface $con = null) Return the first ChildCountryShippingMethod matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCountryShippingMethod requireOneById(int $id) Return the first ChildCountryShippingMethod filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountryShippingMethod requireOneByCountryId(int $country_id) Return the first ChildCountryShippingMethod filtered by the country_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountryShippingMethod requireOneByShippingMethodId(int $shipping_method_id) Return the first ChildCountryShippingMethod filtered by the shipping_method_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountryShippingMethod requireOneByIsEnabledWebsite(boolean $is_enabled_website) Return the first ChildCountryShippingMethod filtered by the is_enabled_website column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCountryShippingMethod requireOneByPrice(double $price) Return the first ChildCountryShippingMethod filtered by the price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCountryShippingMethod[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCountryShippingMethod objects based on current ModelCriteria
 * @method     ChildCountryShippingMethod[]|ObjectCollection findById(int $id) Return ChildCountryShippingMethod objects filtered by the id column
 * @method     ChildCountryShippingMethod[]|ObjectCollection findByCountryId(int $country_id) Return ChildCountryShippingMethod objects filtered by the country_id column
 * @method     ChildCountryShippingMethod[]|ObjectCollection findByShippingMethodId(int $shipping_method_id) Return ChildCountryShippingMethod objects filtered by the shipping_method_id column
 * @method     ChildCountryShippingMethod[]|ObjectCollection findByIsEnabledWebsite(boolean $is_enabled_website) Return ChildCountryShippingMethod objects filtered by the is_enabled_website column
 * @method     ChildCountryShippingMethod[]|ObjectCollection findByPrice(double $price) Return ChildCountryShippingMethod objects filtered by the price column
 * @method     ChildCountryShippingMethod[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CountryShippingMethodQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\MasterTable\Base\CountryShippingMethodQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\MasterTable\\CountryShippingMethod', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCountryShippingMethodQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCountryShippingMethodQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCountryShippingMethodQuery) {
            return $criteria;
        }
        $query = new ChildCountryShippingMethodQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCountryShippingMethod|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CountryShippingMethodTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CountryShippingMethodTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCountryShippingMethod A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, country_id, shipping_method_id, is_enabled_website, price FROM country_shipping_method WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCountryShippingMethod $obj */
            $obj = new ChildCountryShippingMethod();
            $obj->hydrate($row);
            CountryShippingMethodTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCountryShippingMethod|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCountryShippingMethodQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CountryShippingMethodTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCountryShippingMethodQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CountryShippingMethodTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryShippingMethodQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CountryShippingMethodTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CountryShippingMethodTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountryShippingMethodTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the country_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCountryId(1234); // WHERE country_id = 1234
     * $query->filterByCountryId(array(12, 34)); // WHERE country_id IN (12, 34)
     * $query->filterByCountryId(array('min' => 12)); // WHERE country_id > 12
     * </code>
     *
     * @see       filterByCountry()
     *
     * @param     mixed $countryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryShippingMethodQuery The current query, for fluid interface
     */
    public function filterByCountryId($countryId = null, $comparison = null)
    {
        if (is_array($countryId)) {
            $useMinMax = false;
            if (isset($countryId['min'])) {
                $this->addUsingAlias(CountryShippingMethodTableMap::COL_COUNTRY_ID, $countryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countryId['max'])) {
                $this->addUsingAlias(CountryShippingMethodTableMap::COL_COUNTRY_ID, $countryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountryShippingMethodTableMap::COL_COUNTRY_ID, $countryId, $comparison);
    }

    /**
     * Filter the query on the shipping_method_id column
     *
     * Example usage:
     * <code>
     * $query->filterByShippingMethodId(1234); // WHERE shipping_method_id = 1234
     * $query->filterByShippingMethodId(array(12, 34)); // WHERE shipping_method_id IN (12, 34)
     * $query->filterByShippingMethodId(array('min' => 12)); // WHERE shipping_method_id > 12
     * </code>
     *
     * @see       filterByShippingMethod()
     *
     * @param     mixed $shippingMethodId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryShippingMethodQuery The current query, for fluid interface
     */
    public function filterByShippingMethodId($shippingMethodId = null, $comparison = null)
    {
        if (is_array($shippingMethodId)) {
            $useMinMax = false;
            if (isset($shippingMethodId['min'])) {
                $this->addUsingAlias(CountryShippingMethodTableMap::COL_SHIPPING_METHOD_ID, $shippingMethodId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shippingMethodId['max'])) {
                $this->addUsingAlias(CountryShippingMethodTableMap::COL_SHIPPING_METHOD_ID, $shippingMethodId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountryShippingMethodTableMap::COL_SHIPPING_METHOD_ID, $shippingMethodId, $comparison);
    }

    /**
     * Filter the query on the is_enabled_website column
     *
     * Example usage:
     * <code>
     * $query->filterByIsEnabledWebsite(true); // WHERE is_enabled_website = true
     * $query->filterByIsEnabledWebsite('yes'); // WHERE is_enabled_website = true
     * </code>
     *
     * @param     boolean|string $isEnabledWebsite The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryShippingMethodQuery The current query, for fluid interface
     */
    public function filterByIsEnabledWebsite($isEnabledWebsite = null, $comparison = null)
    {
        if (is_string($isEnabledWebsite)) {
            $isEnabledWebsite = in_array(strtolower($isEnabledWebsite), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CountryShippingMethodTableMap::COL_IS_ENABLED_WEBSITE, $isEnabledWebsite, $comparison);
    }

    /**
     * Filter the query on the price column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE price = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE price IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE price > 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCountryShippingMethodQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(CountryShippingMethodTableMap::COL_PRICE, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(CountryShippingMethodTableMap::COL_PRICE, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CountryShippingMethodTableMap::COL_PRICE, $price, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Country object
     *
     * @param \Model\Setting\MasterTable\Country|ObjectCollection $country The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCountryShippingMethodQuery The current query, for fluid interface
     */
    public function filterByCountry($country, $comparison = null)
    {
        if ($country instanceof \Model\Setting\MasterTable\Country) {
            return $this
                ->addUsingAlias(CountryShippingMethodTableMap::COL_COUNTRY_ID, $country->getId(), $comparison);
        } elseif ($country instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CountryShippingMethodTableMap::COL_COUNTRY_ID, $country->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCountry() only accepts arguments of type \Model\Setting\MasterTable\Country or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Country relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCountryShippingMethodQuery The current query, for fluid interface
     */
    public function joinCountry($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Country');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Country');
        }

        return $this;
    }

    /**
     * Use the Country relation Country object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\CountryQuery A secondary query class using the current class as primary query
     */
    public function useCountryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCountry($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Country', '\Model\Setting\MasterTable\CountryQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\ShippingMethod object
     *
     * @param \Model\Setting\MasterTable\ShippingMethod|ObjectCollection $shippingMethod The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCountryShippingMethodQuery The current query, for fluid interface
     */
    public function filterByShippingMethod($shippingMethod, $comparison = null)
    {
        if ($shippingMethod instanceof \Model\Setting\MasterTable\ShippingMethod) {
            return $this
                ->addUsingAlias(CountryShippingMethodTableMap::COL_SHIPPING_METHOD_ID, $shippingMethod->getId(), $comparison);
        } elseif ($shippingMethod instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CountryShippingMethodTableMap::COL_SHIPPING_METHOD_ID, $shippingMethod->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByShippingMethod() only accepts arguments of type \Model\Setting\MasterTable\ShippingMethod or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ShippingMethod relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCountryShippingMethodQuery The current query, for fluid interface
     */
    public function joinShippingMethod($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ShippingMethod');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ShippingMethod');
        }

        return $this;
    }

    /**
     * Use the ShippingMethod relation ShippingMethod object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\ShippingMethodQuery A secondary query class using the current class as primary query
     */
    public function useShippingMethodQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinShippingMethod($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ShippingMethod', '\Model\Setting\MasterTable\ShippingMethodQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCountryShippingMethod $countryShippingMethod Object to remove from the list of results
     *
     * @return $this|ChildCountryShippingMethodQuery The current query, for fluid interface
     */
    public function prune($countryShippingMethod = null)
    {
        if ($countryShippingMethod) {
            $this->addUsingAlias(CountryShippingMethodTableMap::COL_ID, $countryShippingMethod->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the country_shipping_method table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CountryShippingMethodTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CountryShippingMethodTableMap::clearInstancePool();
            CountryShippingMethodTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CountryShippingMethodTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CountryShippingMethodTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CountryShippingMethodTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CountryShippingMethodTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CountryShippingMethodQuery
