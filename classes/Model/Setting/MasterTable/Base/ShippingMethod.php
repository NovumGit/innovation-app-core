<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderQuery;
use Model\Sale\Base\SaleOrder as BaseSaleOrder;
use Model\Sale\Map\SaleOrderTableMap;
use Model\Setting\MasterTable\CountryShippingMethod as ChildCountryShippingMethod;
use Model\Setting\MasterTable\CountryShippingMethodQuery as ChildCountryShippingMethodQuery;
use Model\Setting\MasterTable\ShippingMethod as ChildShippingMethod;
use Model\Setting\MasterTable\ShippingMethodQuery as ChildShippingMethodQuery;
use Model\Setting\MasterTable\Map\CountryShippingMethodTableMap;
use Model\Setting\MasterTable\Map\ShippingMethodTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'mt_shipping_method' table.
 *
 *
 *
 * @package    propel.generator.Model.Setting.MasterTable.Base
 */
abstract class ShippingMethod implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Setting\\MasterTable\\Map\\ShippingMethodTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the name field.
     *
     * @var        string|null
     */
    protected $name;

    /**
     * The value for the code field.
     *
     * @var        string|null
     */
    protected $code;

    /**
     * The value for the is_default_free field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_default_free;

    /**
     * The value for the is_track_trace field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_track_trace;

    /**
     * The value for the min_weight_grams field.
     *
     * Note: this column has a database default value of: 0
     * @var        int|null
     */
    protected $min_weight_grams;

    /**
     * The value for the max_weight_grams field.
     *
     * Note: this column has a database default value of: 0
     * @var        int|null
     */
    protected $max_weight_grams;

    /**
     * @var        ObjectCollection|SaleOrder[] Collection to store aggregation of SaleOrder objects.
     */
    protected $collSaleOrders;
    protected $collSaleOrdersPartial;

    /**
     * @var        ObjectCollection|ChildCountryShippingMethod[] Collection to store aggregation of ChildCountryShippingMethod objects.
     */
    protected $collCountryShippingMethods;
    protected $collCountryShippingMethodsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|SaleOrder[]
     */
    protected $saleOrdersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCountryShippingMethod[]
     */
    protected $countryShippingMethodsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_default_free = false;
        $this->is_track_trace = false;
        $this->min_weight_grams = 0;
        $this->max_weight_grams = 0;
    }

    /**
     * Initializes internal state of Model\Setting\MasterTable\Base\ShippingMethod object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ShippingMethod</code> instance.  If
     * <code>obj</code> is an instance of <code>ShippingMethod</code>, delegates to
     * <code>equals(ShippingMethod)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [code] column value.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the [is_default_free] column value.
     *
     * @return boolean
     */
    public function getIsDefaultFree()
    {
        return $this->is_default_free;
    }

    /**
     * Get the [is_default_free] column value.
     *
     * @return boolean
     */
    public function isDefaultFree()
    {
        return $this->getIsDefaultFree();
    }

    /**
     * Get the [is_track_trace] column value.
     *
     * @return boolean
     */
    public function getIsTrackTrace()
    {
        return $this->is_track_trace;
    }

    /**
     * Get the [is_track_trace] column value.
     *
     * @return boolean
     */
    public function isTrackTrace()
    {
        return $this->getIsTrackTrace();
    }

    /**
     * Get the [min_weight_grams] column value.
     *
     * @return int|null
     */
    public function getMinWeightGrams()
    {
        return $this->min_weight_grams;
    }

    /**
     * Get the [max_weight_grams] column value.
     *
     * @return int|null
     */
    public function getMaxWeightGrams()
    {
        return $this->max_weight_grams;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Setting\MasterTable\ShippingMethod The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[ShippingMethodTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\MasterTable\ShippingMethod The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[ShippingMethodTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [code] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\MasterTable\ShippingMethod The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[ShippingMethodTableMap::COL_CODE] = true;
        }

        return $this;
    } // setCode()

    /**
     * Sets the value of the [is_default_free] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Setting\MasterTable\ShippingMethod The current object (for fluent API support)
     */
    public function setIsDefaultFree($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_default_free !== $v) {
            $this->is_default_free = $v;
            $this->modifiedColumns[ShippingMethodTableMap::COL_IS_DEFAULT_FREE] = true;
        }

        return $this;
    } // setIsDefaultFree()

    /**
     * Sets the value of the [is_track_trace] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Setting\MasterTable\ShippingMethod The current object (for fluent API support)
     */
    public function setIsTrackTrace($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_track_trace !== $v) {
            $this->is_track_trace = $v;
            $this->modifiedColumns[ShippingMethodTableMap::COL_IS_TRACK_TRACE] = true;
        }

        return $this;
    } // setIsTrackTrace()

    /**
     * Set the value of [min_weight_grams] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Setting\MasterTable\ShippingMethod The current object (for fluent API support)
     */
    public function setMinWeightGrams($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->min_weight_grams !== $v) {
            $this->min_weight_grams = $v;
            $this->modifiedColumns[ShippingMethodTableMap::COL_MIN_WEIGHT_GRAMS] = true;
        }

        return $this;
    } // setMinWeightGrams()

    /**
     * Set the value of [max_weight_grams] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Setting\MasterTable\ShippingMethod The current object (for fluent API support)
     */
    public function setMaxWeightGrams($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->max_weight_grams !== $v) {
            $this->max_weight_grams = $v;
            $this->modifiedColumns[ShippingMethodTableMap::COL_MAX_WEIGHT_GRAMS] = true;
        }

        return $this;
    } // setMaxWeightGrams()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_default_free !== false) {
                return false;
            }

            if ($this->is_track_trace !== false) {
                return false;
            }

            if ($this->min_weight_grams !== 0) {
                return false;
            }

            if ($this->max_weight_grams !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ShippingMethodTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ShippingMethodTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ShippingMethodTableMap::translateFieldName('Code', TableMap::TYPE_PHPNAME, $indexType)];
            $this->code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ShippingMethodTableMap::translateFieldName('IsDefaultFree', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_default_free = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ShippingMethodTableMap::translateFieldName('IsTrackTrace', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_track_trace = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ShippingMethodTableMap::translateFieldName('MinWeightGrams', TableMap::TYPE_PHPNAME, $indexType)];
            $this->min_weight_grams = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ShippingMethodTableMap::translateFieldName('MaxWeightGrams', TableMap::TYPE_PHPNAME, $indexType)];
            $this->max_weight_grams = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 7; // 7 = ShippingMethodTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Setting\\MasterTable\\ShippingMethod'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ShippingMethodTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildShippingMethodQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collSaleOrders = null;

            $this->collCountryShippingMethods = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ShippingMethod::setDeleted()
     * @see ShippingMethod::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShippingMethodTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildShippingMethodQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ShippingMethodTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ShippingMethodTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->saleOrdersScheduledForDeletion !== null) {
                if (!$this->saleOrdersScheduledForDeletion->isEmpty()) {
                    foreach ($this->saleOrdersScheduledForDeletion as $saleOrder) {
                        // need to save related object because we set the relation to null
                        $saleOrder->save($con);
                    }
                    $this->saleOrdersScheduledForDeletion = null;
                }
            }

            if ($this->collSaleOrders !== null) {
                foreach ($this->collSaleOrders as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->countryShippingMethodsScheduledForDeletion !== null) {
                if (!$this->countryShippingMethodsScheduledForDeletion->isEmpty()) {
                    \Model\Setting\MasterTable\CountryShippingMethodQuery::create()
                        ->filterByPrimaryKeys($this->countryShippingMethodsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->countryShippingMethodsScheduledForDeletion = null;
                }
            }

            if ($this->collCountryShippingMethods !== null) {
                foreach ($this->collCountryShippingMethods as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ShippingMethodTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ShippingMethodTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ShippingMethodTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(ShippingMethodTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(ShippingMethodTableMap::COL_CODE)) {
            $modifiedColumns[':p' . $index++]  = 'code';
        }
        if ($this->isColumnModified(ShippingMethodTableMap::COL_IS_DEFAULT_FREE)) {
            $modifiedColumns[':p' . $index++]  = 'is_default_free';
        }
        if ($this->isColumnModified(ShippingMethodTableMap::COL_IS_TRACK_TRACE)) {
            $modifiedColumns[':p' . $index++]  = 'is_track_trace';
        }
        if ($this->isColumnModified(ShippingMethodTableMap::COL_MIN_WEIGHT_GRAMS)) {
            $modifiedColumns[':p' . $index++]  = 'min_weight_grams';
        }
        if ($this->isColumnModified(ShippingMethodTableMap::COL_MAX_WEIGHT_GRAMS)) {
            $modifiedColumns[':p' . $index++]  = 'max_weight_grams';
        }

        $sql = sprintf(
            'INSERT INTO mt_shipping_method (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'code':
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case 'is_default_free':
                        $stmt->bindValue($identifier, (int) $this->is_default_free, PDO::PARAM_INT);
                        break;
                    case 'is_track_trace':
                        $stmt->bindValue($identifier, (int) $this->is_track_trace, PDO::PARAM_INT);
                        break;
                    case 'min_weight_grams':
                        $stmt->bindValue($identifier, $this->min_weight_grams, PDO::PARAM_INT);
                        break;
                    case 'max_weight_grams':
                        $stmt->bindValue($identifier, $this->max_weight_grams, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ShippingMethodTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getCode();
                break;
            case 3:
                return $this->getIsDefaultFree();
                break;
            case 4:
                return $this->getIsTrackTrace();
                break;
            case 5:
                return $this->getMinWeightGrams();
                break;
            case 6:
                return $this->getMaxWeightGrams();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ShippingMethod'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ShippingMethod'][$this->hashCode()] = true;
        $keys = ShippingMethodTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getCode(),
            $keys[3] => $this->getIsDefaultFree(),
            $keys[4] => $this->getIsTrackTrace(),
            $keys[5] => $this->getMinWeightGrams(),
            $keys[6] => $this->getMaxWeightGrams(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collSaleOrders) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'saleOrders';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_orders';
                        break;
                    default:
                        $key = 'SaleOrders';
                }

                $result[$key] = $this->collSaleOrders->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCountryShippingMethods) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'countryShippingMethods';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'country_shipping_methods';
                        break;
                    default:
                        $key = 'CountryShippingMethods';
                }

                $result[$key] = $this->collCountryShippingMethods->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Setting\MasterTable\ShippingMethod
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ShippingMethodTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Setting\MasterTable\ShippingMethod
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setCode($value);
                break;
            case 3:
                $this->setIsDefaultFree($value);
                break;
            case 4:
                $this->setIsTrackTrace($value);
                break;
            case 5:
                $this->setMinWeightGrams($value);
                break;
            case 6:
                $this->setMaxWeightGrams($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ShippingMethodTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCode($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setIsDefaultFree($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setIsTrackTrace($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setMinWeightGrams($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setMaxWeightGrams($arr[$keys[6]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Setting\MasterTable\ShippingMethod The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ShippingMethodTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ShippingMethodTableMap::COL_ID)) {
            $criteria->add(ShippingMethodTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(ShippingMethodTableMap::COL_NAME)) {
            $criteria->add(ShippingMethodTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(ShippingMethodTableMap::COL_CODE)) {
            $criteria->add(ShippingMethodTableMap::COL_CODE, $this->code);
        }
        if ($this->isColumnModified(ShippingMethodTableMap::COL_IS_DEFAULT_FREE)) {
            $criteria->add(ShippingMethodTableMap::COL_IS_DEFAULT_FREE, $this->is_default_free);
        }
        if ($this->isColumnModified(ShippingMethodTableMap::COL_IS_TRACK_TRACE)) {
            $criteria->add(ShippingMethodTableMap::COL_IS_TRACK_TRACE, $this->is_track_trace);
        }
        if ($this->isColumnModified(ShippingMethodTableMap::COL_MIN_WEIGHT_GRAMS)) {
            $criteria->add(ShippingMethodTableMap::COL_MIN_WEIGHT_GRAMS, $this->min_weight_grams);
        }
        if ($this->isColumnModified(ShippingMethodTableMap::COL_MAX_WEIGHT_GRAMS)) {
            $criteria->add(ShippingMethodTableMap::COL_MAX_WEIGHT_GRAMS, $this->max_weight_grams);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildShippingMethodQuery::create();
        $criteria->add(ShippingMethodTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Setting\MasterTable\ShippingMethod (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setCode($this->getCode());
        $copyObj->setIsDefaultFree($this->getIsDefaultFree());
        $copyObj->setIsTrackTrace($this->getIsTrackTrace());
        $copyObj->setMinWeightGrams($this->getMinWeightGrams());
        $copyObj->setMaxWeightGrams($this->getMaxWeightGrams());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getSaleOrders() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaleOrder($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCountryShippingMethods() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCountryShippingMethod($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Setting\MasterTable\ShippingMethod Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SaleOrder' === $relationName) {
            $this->initSaleOrders();
            return;
        }
        if ('CountryShippingMethod' === $relationName) {
            $this->initCountryShippingMethods();
            return;
        }
    }

    /**
     * Clears out the collSaleOrders collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSaleOrders()
     */
    public function clearSaleOrders()
    {
        $this->collSaleOrders = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSaleOrders collection loaded partially.
     */
    public function resetPartialSaleOrders($v = true)
    {
        $this->collSaleOrdersPartial = $v;
    }

    /**
     * Initializes the collSaleOrders collection.
     *
     * By default this just sets the collSaleOrders collection to an empty array (like clearcollSaleOrders());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaleOrders($overrideExisting = true)
    {
        if (null !== $this->collSaleOrders && !$overrideExisting) {
            return;
        }

        $collectionClassName = SaleOrderTableMap::getTableMap()->getCollectionClassName();

        $this->collSaleOrders = new $collectionClassName;
        $this->collSaleOrders->setModel('\Model\Sale\SaleOrder');
    }

    /**
     * Gets an array of SaleOrder objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildShippingMethod is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|SaleOrder[] List of SaleOrder objects
     * @throws PropelException
     */
    public function getSaleOrders(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrdersPartial && !$this->isNew();
        if (null === $this->collSaleOrders || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSaleOrders) {
                    $this->initSaleOrders();
                } else {
                    $collectionClassName = SaleOrderTableMap::getTableMap()->getCollectionClassName();

                    $collSaleOrders = new $collectionClassName;
                    $collSaleOrders->setModel('\Model\Sale\SaleOrder');

                    return $collSaleOrders;
                }
            } else {
                $collSaleOrders = SaleOrderQuery::create(null, $criteria)
                    ->filterByShippingMethod($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSaleOrdersPartial && count($collSaleOrders)) {
                        $this->initSaleOrders(false);

                        foreach ($collSaleOrders as $obj) {
                            if (false == $this->collSaleOrders->contains($obj)) {
                                $this->collSaleOrders->append($obj);
                            }
                        }

                        $this->collSaleOrdersPartial = true;
                    }

                    return $collSaleOrders;
                }

                if ($partial && $this->collSaleOrders) {
                    foreach ($this->collSaleOrders as $obj) {
                        if ($obj->isNew()) {
                            $collSaleOrders[] = $obj;
                        }
                    }
                }

                $this->collSaleOrders = $collSaleOrders;
                $this->collSaleOrdersPartial = false;
            }
        }

        return $this->collSaleOrders;
    }

    /**
     * Sets a collection of SaleOrder objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $saleOrders A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildShippingMethod The current object (for fluent API support)
     */
    public function setSaleOrders(Collection $saleOrders, ConnectionInterface $con = null)
    {
        /** @var SaleOrder[] $saleOrdersToDelete */
        $saleOrdersToDelete = $this->getSaleOrders(new Criteria(), $con)->diff($saleOrders);


        $this->saleOrdersScheduledForDeletion = $saleOrdersToDelete;

        foreach ($saleOrdersToDelete as $saleOrderRemoved) {
            $saleOrderRemoved->setShippingMethod(null);
        }

        $this->collSaleOrders = null;
        foreach ($saleOrders as $saleOrder) {
            $this->addSaleOrder($saleOrder);
        }

        $this->collSaleOrders = $saleOrders;
        $this->collSaleOrdersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSaleOrder objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSaleOrder objects.
     * @throws PropelException
     */
    public function countSaleOrders(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrdersPartial && !$this->isNew();
        if (null === $this->collSaleOrders || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaleOrders) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSaleOrders());
            }

            $query = SaleOrderQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByShippingMethod($this)
                ->count($con);
        }

        return count($this->collSaleOrders);
    }

    /**
     * Method called to associate a SaleOrder object to this object
     * through the SaleOrder foreign key attribute.
     *
     * @param  SaleOrder $l SaleOrder
     * @return $this|\Model\Setting\MasterTable\ShippingMethod The current object (for fluent API support)
     */
    public function addSaleOrder(SaleOrder $l)
    {
        if ($this->collSaleOrders === null) {
            $this->initSaleOrders();
            $this->collSaleOrdersPartial = true;
        }

        if (!$this->collSaleOrders->contains($l)) {
            $this->doAddSaleOrder($l);

            if ($this->saleOrdersScheduledForDeletion and $this->saleOrdersScheduledForDeletion->contains($l)) {
                $this->saleOrdersScheduledForDeletion->remove($this->saleOrdersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param SaleOrder $saleOrder The SaleOrder object to add.
     */
    protected function doAddSaleOrder(SaleOrder $saleOrder)
    {
        $this->collSaleOrders[]= $saleOrder;
        $saleOrder->setShippingMethod($this);
    }

    /**
     * @param  SaleOrder $saleOrder The SaleOrder object to remove.
     * @return $this|ChildShippingMethod The current object (for fluent API support)
     */
    public function removeSaleOrder(SaleOrder $saleOrder)
    {
        if ($this->getSaleOrders()->contains($saleOrder)) {
            $pos = $this->collSaleOrders->search($saleOrder);
            $this->collSaleOrders->remove($pos);
            if (null === $this->saleOrdersScheduledForDeletion) {
                $this->saleOrdersScheduledForDeletion = clone $this->collSaleOrders;
                $this->saleOrdersScheduledForDeletion->clear();
            }
            $this->saleOrdersScheduledForDeletion[]= $saleOrder;
            $saleOrder->setShippingMethod(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ShippingMethod is new, it will return
     * an empty collection; or if this ShippingMethod has previously
     * been saved, it will retrieve related SaleOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ShippingMethod.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SaleOrder[] List of SaleOrder objects
     */
    public function getSaleOrdersJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SaleOrderQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getSaleOrders($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ShippingMethod is new, it will return
     * an empty collection; or if this ShippingMethod has previously
     * been saved, it will retrieve related SaleOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ShippingMethod.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SaleOrder[] List of SaleOrder objects
     */
    public function getSaleOrdersJoinSaleOrderStatus(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SaleOrderQuery::create(null, $criteria);
        $query->joinWith('SaleOrderStatus', $joinBehavior);

        return $this->getSaleOrders($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ShippingMethod is new, it will return
     * an empty collection; or if this ShippingMethod has previously
     * been saved, it will retrieve related SaleOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ShippingMethod.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SaleOrder[] List of SaleOrder objects
     */
    public function getSaleOrdersJoinUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SaleOrderQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getSaleOrders($query, $con);
    }

    /**
     * Clears out the collCountryShippingMethods collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCountryShippingMethods()
     */
    public function clearCountryShippingMethods()
    {
        $this->collCountryShippingMethods = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCountryShippingMethods collection loaded partially.
     */
    public function resetPartialCountryShippingMethods($v = true)
    {
        $this->collCountryShippingMethodsPartial = $v;
    }

    /**
     * Initializes the collCountryShippingMethods collection.
     *
     * By default this just sets the collCountryShippingMethods collection to an empty array (like clearcollCountryShippingMethods());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCountryShippingMethods($overrideExisting = true)
    {
        if (null !== $this->collCountryShippingMethods && !$overrideExisting) {
            return;
        }

        $collectionClassName = CountryShippingMethodTableMap::getTableMap()->getCollectionClassName();

        $this->collCountryShippingMethods = new $collectionClassName;
        $this->collCountryShippingMethods->setModel('\Model\Setting\MasterTable\CountryShippingMethod');
    }

    /**
     * Gets an array of ChildCountryShippingMethod objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildShippingMethod is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCountryShippingMethod[] List of ChildCountryShippingMethod objects
     * @throws PropelException
     */
    public function getCountryShippingMethods(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCountryShippingMethodsPartial && !$this->isNew();
        if (null === $this->collCountryShippingMethods || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCountryShippingMethods) {
                    $this->initCountryShippingMethods();
                } else {
                    $collectionClassName = CountryShippingMethodTableMap::getTableMap()->getCollectionClassName();

                    $collCountryShippingMethods = new $collectionClassName;
                    $collCountryShippingMethods->setModel('\Model\Setting\MasterTable\CountryShippingMethod');

                    return $collCountryShippingMethods;
                }
            } else {
                $collCountryShippingMethods = ChildCountryShippingMethodQuery::create(null, $criteria)
                    ->filterByShippingMethod($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCountryShippingMethodsPartial && count($collCountryShippingMethods)) {
                        $this->initCountryShippingMethods(false);

                        foreach ($collCountryShippingMethods as $obj) {
                            if (false == $this->collCountryShippingMethods->contains($obj)) {
                                $this->collCountryShippingMethods->append($obj);
                            }
                        }

                        $this->collCountryShippingMethodsPartial = true;
                    }

                    return $collCountryShippingMethods;
                }

                if ($partial && $this->collCountryShippingMethods) {
                    foreach ($this->collCountryShippingMethods as $obj) {
                        if ($obj->isNew()) {
                            $collCountryShippingMethods[] = $obj;
                        }
                    }
                }

                $this->collCountryShippingMethods = $collCountryShippingMethods;
                $this->collCountryShippingMethodsPartial = false;
            }
        }

        return $this->collCountryShippingMethods;
    }

    /**
     * Sets a collection of ChildCountryShippingMethod objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $countryShippingMethods A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildShippingMethod The current object (for fluent API support)
     */
    public function setCountryShippingMethods(Collection $countryShippingMethods, ConnectionInterface $con = null)
    {
        /** @var ChildCountryShippingMethod[] $countryShippingMethodsToDelete */
        $countryShippingMethodsToDelete = $this->getCountryShippingMethods(new Criteria(), $con)->diff($countryShippingMethods);


        $this->countryShippingMethodsScheduledForDeletion = $countryShippingMethodsToDelete;

        foreach ($countryShippingMethodsToDelete as $countryShippingMethodRemoved) {
            $countryShippingMethodRemoved->setShippingMethod(null);
        }

        $this->collCountryShippingMethods = null;
        foreach ($countryShippingMethods as $countryShippingMethod) {
            $this->addCountryShippingMethod($countryShippingMethod);
        }

        $this->collCountryShippingMethods = $countryShippingMethods;
        $this->collCountryShippingMethodsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CountryShippingMethod objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CountryShippingMethod objects.
     * @throws PropelException
     */
    public function countCountryShippingMethods(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCountryShippingMethodsPartial && !$this->isNew();
        if (null === $this->collCountryShippingMethods || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCountryShippingMethods) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCountryShippingMethods());
            }

            $query = ChildCountryShippingMethodQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByShippingMethod($this)
                ->count($con);
        }

        return count($this->collCountryShippingMethods);
    }

    /**
     * Method called to associate a ChildCountryShippingMethod object to this object
     * through the ChildCountryShippingMethod foreign key attribute.
     *
     * @param  ChildCountryShippingMethod $l ChildCountryShippingMethod
     * @return $this|\Model\Setting\MasterTable\ShippingMethod The current object (for fluent API support)
     */
    public function addCountryShippingMethod(ChildCountryShippingMethod $l)
    {
        if ($this->collCountryShippingMethods === null) {
            $this->initCountryShippingMethods();
            $this->collCountryShippingMethodsPartial = true;
        }

        if (!$this->collCountryShippingMethods->contains($l)) {
            $this->doAddCountryShippingMethod($l);

            if ($this->countryShippingMethodsScheduledForDeletion and $this->countryShippingMethodsScheduledForDeletion->contains($l)) {
                $this->countryShippingMethodsScheduledForDeletion->remove($this->countryShippingMethodsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCountryShippingMethod $countryShippingMethod The ChildCountryShippingMethod object to add.
     */
    protected function doAddCountryShippingMethod(ChildCountryShippingMethod $countryShippingMethod)
    {
        $this->collCountryShippingMethods[]= $countryShippingMethod;
        $countryShippingMethod->setShippingMethod($this);
    }

    /**
     * @param  ChildCountryShippingMethod $countryShippingMethod The ChildCountryShippingMethod object to remove.
     * @return $this|ChildShippingMethod The current object (for fluent API support)
     */
    public function removeCountryShippingMethod(ChildCountryShippingMethod $countryShippingMethod)
    {
        if ($this->getCountryShippingMethods()->contains($countryShippingMethod)) {
            $pos = $this->collCountryShippingMethods->search($countryShippingMethod);
            $this->collCountryShippingMethods->remove($pos);
            if (null === $this->countryShippingMethodsScheduledForDeletion) {
                $this->countryShippingMethodsScheduledForDeletion = clone $this->collCountryShippingMethods;
                $this->countryShippingMethodsScheduledForDeletion->clear();
            }
            $this->countryShippingMethodsScheduledForDeletion[]= clone $countryShippingMethod;
            $countryShippingMethod->setShippingMethod(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ShippingMethod is new, it will return
     * an empty collection; or if this ShippingMethod has previously
     * been saved, it will retrieve related CountryShippingMethods from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ShippingMethod.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCountryShippingMethod[] List of ChildCountryShippingMethod objects
     */
    public function getCountryShippingMethodsJoinCountry(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCountryShippingMethodQuery::create(null, $criteria);
        $query->joinWith('Country', $joinBehavior);

        return $this->getCountryShippingMethods($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->name = null;
        $this->code = null;
        $this->is_default_free = null;
        $this->is_track_trace = null;
        $this->min_weight_grams = null;
        $this->max_weight_grams = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSaleOrders) {
                foreach ($this->collSaleOrders as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCountryShippingMethods) {
                foreach ($this->collCountryShippingMethods as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collSaleOrders = null;
        $this->collCountryShippingMethods = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ShippingMethodTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
