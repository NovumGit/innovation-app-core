<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\ContactMessage;
use Model\ProductTranslation;
use Model\PromoProductTranslation;
use Model\Account\User;
use Model\Category\CategoryTranslation;
use Model\Cms\SiteBlog;
use Model\Cms\SiteFAQ;
use Model\Cms\SiteFAQCategoryTranslation;
use Model\Cms\SiteFooterBlockTranslation;
use Model\Cms\SitePage;
use Model\Cms\SiteUsp;
use Model\Crm\Customer;
use Model\Setting\MasterTable\Language as ChildLanguage;
use Model\Setting\MasterTable\LanguageQuery as ChildLanguageQuery;
use Model\Setting\MasterTable\Map\LanguageTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mt_language' table.
 *
 *
 *
 * @method     ChildLanguageQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildLanguageQuery orderByItemDeleted($order = Criteria::ASC) Order by the is_deleted column
 * @method     ChildLanguageQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildLanguageQuery orderByLocaleCode($order = Criteria::ASC) Order by the locale_code column
 * @method     ChildLanguageQuery orderByIsEnabledCms($order = Criteria::ASC) Order by the is_enabled_cms column
 * @method     ChildLanguageQuery orderByIsEnabledWebshop($order = Criteria::ASC) Order by the is_enabled_webshop column
 * @method     ChildLanguageQuery orderByIsDefaultCms($order = Criteria::ASC) Order by the is_default_cms column
 * @method     ChildLanguageQuery orderByIsDefaultWebshop($order = Criteria::ASC) Order by the is_default_webshop column
 * @method     ChildLanguageQuery orderByFlagIcon($order = Criteria::ASC) Order by the flag_icon column
 * @method     ChildLanguageQuery orderByShopUrlPrefix($order = Criteria::ASC) Order by the shop_url_prefix column
 * @method     ChildLanguageQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildLanguageQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildLanguageQuery groupById() Group by the id column
 * @method     ChildLanguageQuery groupByItemDeleted() Group by the is_deleted column
 * @method     ChildLanguageQuery groupByDescription() Group by the description column
 * @method     ChildLanguageQuery groupByLocaleCode() Group by the locale_code column
 * @method     ChildLanguageQuery groupByIsEnabledCms() Group by the is_enabled_cms column
 * @method     ChildLanguageQuery groupByIsEnabledWebshop() Group by the is_enabled_webshop column
 * @method     ChildLanguageQuery groupByIsDefaultCms() Group by the is_default_cms column
 * @method     ChildLanguageQuery groupByIsDefaultWebshop() Group by the is_default_webshop column
 * @method     ChildLanguageQuery groupByFlagIcon() Group by the flag_icon column
 * @method     ChildLanguageQuery groupByShopUrlPrefix() Group by the shop_url_prefix column
 * @method     ChildLanguageQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildLanguageQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildLanguageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildLanguageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildLanguageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildLanguageQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildLanguageQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildLanguageQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildLanguageQuery leftJoinLegalFormTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the LegalFormTranslation relation
 * @method     ChildLanguageQuery rightJoinLegalFormTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LegalFormTranslation relation
 * @method     ChildLanguageQuery innerJoinLegalFormTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the LegalFormTranslation relation
 *
 * @method     ChildLanguageQuery joinWithLegalFormTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the LegalFormTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinWithLegalFormTranslation() Adds a LEFT JOIN clause and with to the query using the LegalFormTranslation relation
 * @method     ChildLanguageQuery rightJoinWithLegalFormTranslation() Adds a RIGHT JOIN clause and with to the query using the LegalFormTranslation relation
 * @method     ChildLanguageQuery innerJoinWithLegalFormTranslation() Adds a INNER JOIN clause and with to the query using the LegalFormTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinContactMessage($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContactMessage relation
 * @method     ChildLanguageQuery rightJoinContactMessage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContactMessage relation
 * @method     ChildLanguageQuery innerJoinContactMessage($relationAlias = null) Adds a INNER JOIN clause to the query using the ContactMessage relation
 *
 * @method     ChildLanguageQuery joinWithContactMessage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContactMessage relation
 *
 * @method     ChildLanguageQuery leftJoinWithContactMessage() Adds a LEFT JOIN clause and with to the query using the ContactMessage relation
 * @method     ChildLanguageQuery rightJoinWithContactMessage() Adds a RIGHT JOIN clause and with to the query using the ContactMessage relation
 * @method     ChildLanguageQuery innerJoinWithContactMessage() Adds a INNER JOIN clause and with to the query using the ContactMessage relation
 *
 * @method     ChildLanguageQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildLanguageQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildLanguageQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildLanguageQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildLanguageQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildLanguageQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildLanguageQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     ChildLanguageQuery leftJoinCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Customer relation
 * @method     ChildLanguageQuery rightJoinCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Customer relation
 * @method     ChildLanguageQuery innerJoinCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the Customer relation
 *
 * @method     ChildLanguageQuery joinWithCustomer($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Customer relation
 *
 * @method     ChildLanguageQuery leftJoinWithCustomer() Adds a LEFT JOIN clause and with to the query using the Customer relation
 * @method     ChildLanguageQuery rightJoinWithCustomer() Adds a RIGHT JOIN clause and with to the query using the Customer relation
 * @method     ChildLanguageQuery innerJoinWithCustomer() Adds a INNER JOIN clause and with to the query using the Customer relation
 *
 * @method     ChildLanguageQuery leftJoinCategoryTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CategoryTranslation relation
 * @method     ChildLanguageQuery rightJoinCategoryTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CategoryTranslation relation
 * @method     ChildLanguageQuery innerJoinCategoryTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the CategoryTranslation relation
 *
 * @method     ChildLanguageQuery joinWithCategoryTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CategoryTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinWithCategoryTranslation() Adds a LEFT JOIN clause and with to the query using the CategoryTranslation relation
 * @method     ChildLanguageQuery rightJoinWithCategoryTranslation() Adds a RIGHT JOIN clause and with to the query using the CategoryTranslation relation
 * @method     ChildLanguageQuery innerJoinWithCategoryTranslation() Adds a INNER JOIN clause and with to the query using the CategoryTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinPromoProductTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the PromoProductTranslation relation
 * @method     ChildLanguageQuery rightJoinPromoProductTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PromoProductTranslation relation
 * @method     ChildLanguageQuery innerJoinPromoProductTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the PromoProductTranslation relation
 *
 * @method     ChildLanguageQuery joinWithPromoProductTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PromoProductTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinWithPromoProductTranslation() Adds a LEFT JOIN clause and with to the query using the PromoProductTranslation relation
 * @method     ChildLanguageQuery rightJoinWithPromoProductTranslation() Adds a RIGHT JOIN clause and with to the query using the PromoProductTranslation relation
 * @method     ChildLanguageQuery innerJoinWithPromoProductTranslation() Adds a INNER JOIN clause and with to the query using the PromoProductTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinProductTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductTranslation relation
 * @method     ChildLanguageQuery rightJoinProductTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductTranslation relation
 * @method     ChildLanguageQuery innerJoinProductTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductTranslation relation
 *
 * @method     ChildLanguageQuery joinWithProductTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinWithProductTranslation() Adds a LEFT JOIN clause and with to the query using the ProductTranslation relation
 * @method     ChildLanguageQuery rightJoinWithProductTranslation() Adds a RIGHT JOIN clause and with to the query using the ProductTranslation relation
 * @method     ChildLanguageQuery innerJoinWithProductTranslation() Adds a INNER JOIN clause and with to the query using the ProductTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinVatTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the VatTranslation relation
 * @method     ChildLanguageQuery rightJoinVatTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the VatTranslation relation
 * @method     ChildLanguageQuery innerJoinVatTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the VatTranslation relation
 *
 * @method     ChildLanguageQuery joinWithVatTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the VatTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinWithVatTranslation() Adds a LEFT JOIN clause and with to the query using the VatTranslation relation
 * @method     ChildLanguageQuery rightJoinWithVatTranslation() Adds a RIGHT JOIN clause and with to the query using the VatTranslation relation
 * @method     ChildLanguageQuery innerJoinWithVatTranslation() Adds a INNER JOIN clause and with to the query using the VatTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinUsageTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsageTranslation relation
 * @method     ChildLanguageQuery rightJoinUsageTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsageTranslation relation
 * @method     ChildLanguageQuery innerJoinUsageTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the UsageTranslation relation
 *
 * @method     ChildLanguageQuery joinWithUsageTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsageTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinWithUsageTranslation() Adds a LEFT JOIN clause and with to the query using the UsageTranslation relation
 * @method     ChildLanguageQuery rightJoinWithUsageTranslation() Adds a RIGHT JOIN clause and with to the query using the UsageTranslation relation
 * @method     ChildLanguageQuery innerJoinWithUsageTranslation() Adds a INNER JOIN clause and with to the query using the UsageTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinColorTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the ColorTranslation relation
 * @method     ChildLanguageQuery rightJoinColorTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ColorTranslation relation
 * @method     ChildLanguageQuery innerJoinColorTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the ColorTranslation relation
 *
 * @method     ChildLanguageQuery joinWithColorTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ColorTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinWithColorTranslation() Adds a LEFT JOIN clause and with to the query using the ColorTranslation relation
 * @method     ChildLanguageQuery rightJoinWithColorTranslation() Adds a RIGHT JOIN clause and with to the query using the ColorTranslation relation
 * @method     ChildLanguageQuery innerJoinWithColorTranslation() Adds a INNER JOIN clause and with to the query using the ColorTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinSiteUsp($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteUsp relation
 * @method     ChildLanguageQuery rightJoinSiteUsp($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteUsp relation
 * @method     ChildLanguageQuery innerJoinSiteUsp($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteUsp relation
 *
 * @method     ChildLanguageQuery joinWithSiteUsp($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteUsp relation
 *
 * @method     ChildLanguageQuery leftJoinWithSiteUsp() Adds a LEFT JOIN clause and with to the query using the SiteUsp relation
 * @method     ChildLanguageQuery rightJoinWithSiteUsp() Adds a RIGHT JOIN clause and with to the query using the SiteUsp relation
 * @method     ChildLanguageQuery innerJoinWithSiteUsp() Adds a INNER JOIN clause and with to the query using the SiteUsp relation
 *
 * @method     ChildLanguageQuery leftJoinSiteFooterBlockTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteFooterBlockTranslation relation
 * @method     ChildLanguageQuery rightJoinSiteFooterBlockTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteFooterBlockTranslation relation
 * @method     ChildLanguageQuery innerJoinSiteFooterBlockTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteFooterBlockTranslation relation
 *
 * @method     ChildLanguageQuery joinWithSiteFooterBlockTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteFooterBlockTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinWithSiteFooterBlockTranslation() Adds a LEFT JOIN clause and with to the query using the SiteFooterBlockTranslation relation
 * @method     ChildLanguageQuery rightJoinWithSiteFooterBlockTranslation() Adds a RIGHT JOIN clause and with to the query using the SiteFooterBlockTranslation relation
 * @method     ChildLanguageQuery innerJoinWithSiteFooterBlockTranslation() Adds a INNER JOIN clause and with to the query using the SiteFooterBlockTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinProductUnitTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductUnitTranslation relation
 * @method     ChildLanguageQuery rightJoinProductUnitTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductUnitTranslation relation
 * @method     ChildLanguageQuery innerJoinProductUnitTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductUnitTranslation relation
 *
 * @method     ChildLanguageQuery joinWithProductUnitTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductUnitTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinWithProductUnitTranslation() Adds a LEFT JOIN clause and with to the query using the ProductUnitTranslation relation
 * @method     ChildLanguageQuery rightJoinWithProductUnitTranslation() Adds a RIGHT JOIN clause and with to the query using the ProductUnitTranslation relation
 * @method     ChildLanguageQuery innerJoinWithProductUnitTranslation() Adds a INNER JOIN clause and with to the query using the ProductUnitTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinProductDeliveryTimeTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductDeliveryTimeTranslation relation
 * @method     ChildLanguageQuery rightJoinProductDeliveryTimeTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductDeliveryTimeTranslation relation
 * @method     ChildLanguageQuery innerJoinProductDeliveryTimeTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductDeliveryTimeTranslation relation
 *
 * @method     ChildLanguageQuery joinWithProductDeliveryTimeTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductDeliveryTimeTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinWithProductDeliveryTimeTranslation() Adds a LEFT JOIN clause and with to the query using the ProductDeliveryTimeTranslation relation
 * @method     ChildLanguageQuery rightJoinWithProductDeliveryTimeTranslation() Adds a RIGHT JOIN clause and with to the query using the ProductDeliveryTimeTranslation relation
 * @method     ChildLanguageQuery innerJoinWithProductDeliveryTimeTranslation() Adds a INNER JOIN clause and with to the query using the ProductDeliveryTimeTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinSiteBlog($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteBlog relation
 * @method     ChildLanguageQuery rightJoinSiteBlog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteBlog relation
 * @method     ChildLanguageQuery innerJoinSiteBlog($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteBlog relation
 *
 * @method     ChildLanguageQuery joinWithSiteBlog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteBlog relation
 *
 * @method     ChildLanguageQuery leftJoinWithSiteBlog() Adds a LEFT JOIN clause and with to the query using the SiteBlog relation
 * @method     ChildLanguageQuery rightJoinWithSiteBlog() Adds a RIGHT JOIN clause and with to the query using the SiteBlog relation
 * @method     ChildLanguageQuery innerJoinWithSiteBlog() Adds a INNER JOIN clause and with to the query using the SiteBlog relation
 *
 * @method     ChildLanguageQuery leftJoinSiteFAQCategoryTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteFAQCategoryTranslation relation
 * @method     ChildLanguageQuery rightJoinSiteFAQCategoryTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteFAQCategoryTranslation relation
 * @method     ChildLanguageQuery innerJoinSiteFAQCategoryTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteFAQCategoryTranslation relation
 *
 * @method     ChildLanguageQuery joinWithSiteFAQCategoryTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteFAQCategoryTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinWithSiteFAQCategoryTranslation() Adds a LEFT JOIN clause and with to the query using the SiteFAQCategoryTranslation relation
 * @method     ChildLanguageQuery rightJoinWithSiteFAQCategoryTranslation() Adds a RIGHT JOIN clause and with to the query using the SiteFAQCategoryTranslation relation
 * @method     ChildLanguageQuery innerJoinWithSiteFAQCategoryTranslation() Adds a INNER JOIN clause and with to the query using the SiteFAQCategoryTranslation relation
 *
 * @method     ChildLanguageQuery leftJoinSiteFAQ($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteFAQ relation
 * @method     ChildLanguageQuery rightJoinSiteFAQ($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteFAQ relation
 * @method     ChildLanguageQuery innerJoinSiteFAQ($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteFAQ relation
 *
 * @method     ChildLanguageQuery joinWithSiteFAQ($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteFAQ relation
 *
 * @method     ChildLanguageQuery leftJoinWithSiteFAQ() Adds a LEFT JOIN clause and with to the query using the SiteFAQ relation
 * @method     ChildLanguageQuery rightJoinWithSiteFAQ() Adds a RIGHT JOIN clause and with to the query using the SiteFAQ relation
 * @method     ChildLanguageQuery innerJoinWithSiteFAQ() Adds a INNER JOIN clause and with to the query using the SiteFAQ relation
 *
 * @method     ChildLanguageQuery leftJoinSitePage($relationAlias = null) Adds a LEFT JOIN clause to the query using the SitePage relation
 * @method     ChildLanguageQuery rightJoinSitePage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SitePage relation
 * @method     ChildLanguageQuery innerJoinSitePage($relationAlias = null) Adds a INNER JOIN clause to the query using the SitePage relation
 *
 * @method     ChildLanguageQuery joinWithSitePage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SitePage relation
 *
 * @method     ChildLanguageQuery leftJoinWithSitePage() Adds a LEFT JOIN clause and with to the query using the SitePage relation
 * @method     ChildLanguageQuery rightJoinWithSitePage() Adds a RIGHT JOIN clause and with to the query using the SitePage relation
 * @method     ChildLanguageQuery innerJoinWithSitePage() Adds a INNER JOIN clause and with to the query using the SitePage relation
 *
 * @method     \Model\Setting\MasterTable\LegalFormTranslationQuery|\Model\ContactMessageQuery|\Model\Account\UserQuery|\Model\Crm\CustomerQuery|\Model\Category\CategoryTranslationQuery|\Model\PromoProductTranslationQuery|\Model\ProductTranslationQuery|\Model\Setting\MasterTable\VatTranslationQuery|\Model\Setting\MasterTable\UsageTranslationQuery|\Model\Setting\MasterTable\ColorTranslationQuery|\Model\Cms\SiteUspQuery|\Model\Cms\SiteFooterBlockTranslationQuery|\Model\Setting\MasterTable\ProductUnitTranslationQuery|\Model\Setting\MasterTable\ProductDeliveryTimeTranslationQuery|\Model\Cms\SiteBlogQuery|\Model\Cms\SiteFAQCategoryTranslationQuery|\Model\Cms\SiteFAQQuery|\Model\Cms\SitePageQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildLanguage findOne(ConnectionInterface $con = null) Return the first ChildLanguage matching the query
 * @method     ChildLanguage findOneOrCreate(ConnectionInterface $con = null) Return the first ChildLanguage matching the query, or a new ChildLanguage object populated from the query conditions when no match is found
 *
 * @method     ChildLanguage findOneById(int $id) Return the first ChildLanguage filtered by the id column
 * @method     ChildLanguage findOneByItemDeleted(boolean $is_deleted) Return the first ChildLanguage filtered by the is_deleted column
 * @method     ChildLanguage findOneByDescription(string $description) Return the first ChildLanguage filtered by the description column
 * @method     ChildLanguage findOneByLocaleCode(string $locale_code) Return the first ChildLanguage filtered by the locale_code column
 * @method     ChildLanguage findOneByIsEnabledCms(boolean $is_enabled_cms) Return the first ChildLanguage filtered by the is_enabled_cms column
 * @method     ChildLanguage findOneByIsEnabledWebshop(boolean $is_enabled_webshop) Return the first ChildLanguage filtered by the is_enabled_webshop column
 * @method     ChildLanguage findOneByIsDefaultCms(boolean $is_default_cms) Return the first ChildLanguage filtered by the is_default_cms column
 * @method     ChildLanguage findOneByIsDefaultWebshop(boolean $is_default_webshop) Return the first ChildLanguage filtered by the is_default_webshop column
 * @method     ChildLanguage findOneByFlagIcon(string $flag_icon) Return the first ChildLanguage filtered by the flag_icon column
 * @method     ChildLanguage findOneByShopUrlPrefix(string $shop_url_prefix) Return the first ChildLanguage filtered by the shop_url_prefix column
 * @method     ChildLanguage findOneByCreatedAt(string $created_at) Return the first ChildLanguage filtered by the created_at column
 * @method     ChildLanguage findOneByUpdatedAt(string $updated_at) Return the first ChildLanguage filtered by the updated_at column *

 * @method     ChildLanguage requirePk($key, ConnectionInterface $con = null) Return the ChildLanguage by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLanguage requireOne(ConnectionInterface $con = null) Return the first ChildLanguage matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLanguage requireOneById(int $id) Return the first ChildLanguage filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLanguage requireOneByItemDeleted(boolean $is_deleted) Return the first ChildLanguage filtered by the is_deleted column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLanguage requireOneByDescription(string $description) Return the first ChildLanguage filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLanguage requireOneByLocaleCode(string $locale_code) Return the first ChildLanguage filtered by the locale_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLanguage requireOneByIsEnabledCms(boolean $is_enabled_cms) Return the first ChildLanguage filtered by the is_enabled_cms column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLanguage requireOneByIsEnabledWebshop(boolean $is_enabled_webshop) Return the first ChildLanguage filtered by the is_enabled_webshop column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLanguage requireOneByIsDefaultCms(boolean $is_default_cms) Return the first ChildLanguage filtered by the is_default_cms column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLanguage requireOneByIsDefaultWebshop(boolean $is_default_webshop) Return the first ChildLanguage filtered by the is_default_webshop column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLanguage requireOneByFlagIcon(string $flag_icon) Return the first ChildLanguage filtered by the flag_icon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLanguage requireOneByShopUrlPrefix(string $shop_url_prefix) Return the first ChildLanguage filtered by the shop_url_prefix column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLanguage requireOneByCreatedAt(string $created_at) Return the first ChildLanguage filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLanguage requireOneByUpdatedAt(string $updated_at) Return the first ChildLanguage filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLanguage[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildLanguage objects based on current ModelCriteria
 * @method     ChildLanguage[]|ObjectCollection findById(int $id) Return ChildLanguage objects filtered by the id column
 * @method     ChildLanguage[]|ObjectCollection findByItemDeleted(boolean $is_deleted) Return ChildLanguage objects filtered by the is_deleted column
 * @method     ChildLanguage[]|ObjectCollection findByDescription(string $description) Return ChildLanguage objects filtered by the description column
 * @method     ChildLanguage[]|ObjectCollection findByLocaleCode(string $locale_code) Return ChildLanguage objects filtered by the locale_code column
 * @method     ChildLanguage[]|ObjectCollection findByIsEnabledCms(boolean $is_enabled_cms) Return ChildLanguage objects filtered by the is_enabled_cms column
 * @method     ChildLanguage[]|ObjectCollection findByIsEnabledWebshop(boolean $is_enabled_webshop) Return ChildLanguage objects filtered by the is_enabled_webshop column
 * @method     ChildLanguage[]|ObjectCollection findByIsDefaultCms(boolean $is_default_cms) Return ChildLanguage objects filtered by the is_default_cms column
 * @method     ChildLanguage[]|ObjectCollection findByIsDefaultWebshop(boolean $is_default_webshop) Return ChildLanguage objects filtered by the is_default_webshop column
 * @method     ChildLanguage[]|ObjectCollection findByFlagIcon(string $flag_icon) Return ChildLanguage objects filtered by the flag_icon column
 * @method     ChildLanguage[]|ObjectCollection findByShopUrlPrefix(string $shop_url_prefix) Return ChildLanguage objects filtered by the shop_url_prefix column
 * @method     ChildLanguage[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildLanguage objects filtered by the created_at column
 * @method     ChildLanguage[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildLanguage objects filtered by the updated_at column
 * @method     ChildLanguage[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class LanguageQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\MasterTable\Base\LanguageQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\MasterTable\\Language', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildLanguageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildLanguageQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildLanguageQuery) {
            return $criteria;
        }
        $query = new ChildLanguageQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildLanguage|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LanguageTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = LanguageTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLanguage A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, is_deleted, description, locale_code, is_enabled_cms, is_enabled_webshop, is_default_cms, is_default_webshop, flag_icon, shop_url_prefix, created_at, updated_at FROM mt_language WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildLanguage $obj */
            $obj = new ChildLanguage();
            $obj->hydrate($row);
            LanguageTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildLanguage|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LanguageTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LanguageTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(LanguageTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(LanguageTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LanguageTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the is_deleted column
     *
     * Example usage:
     * <code>
     * $query->filterByItemDeleted(true); // WHERE is_deleted = true
     * $query->filterByItemDeleted('yes'); // WHERE is_deleted = true
     * </code>
     *
     * @param     boolean|string $itemDeleted The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByItemDeleted($itemDeleted = null, $comparison = null)
    {
        if (is_string($itemDeleted)) {
            $itemDeleted = in_array(strtolower($itemDeleted), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(LanguageTableMap::COL_IS_DELETED, $itemDeleted, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LanguageTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the locale_code column
     *
     * Example usage:
     * <code>
     * $query->filterByLocaleCode('fooValue');   // WHERE locale_code = 'fooValue'
     * $query->filterByLocaleCode('%fooValue%', Criteria::LIKE); // WHERE locale_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $localeCode The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByLocaleCode($localeCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($localeCode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LanguageTableMap::COL_LOCALE_CODE, $localeCode, $comparison);
    }

    /**
     * Filter the query on the is_enabled_cms column
     *
     * Example usage:
     * <code>
     * $query->filterByIsEnabledCms(true); // WHERE is_enabled_cms = true
     * $query->filterByIsEnabledCms('yes'); // WHERE is_enabled_cms = true
     * </code>
     *
     * @param     boolean|string $isEnabledCms The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByIsEnabledCms($isEnabledCms = null, $comparison = null)
    {
        if (is_string($isEnabledCms)) {
            $isEnabledCms = in_array(strtolower($isEnabledCms), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(LanguageTableMap::COL_IS_ENABLED_CMS, $isEnabledCms, $comparison);
    }

    /**
     * Filter the query on the is_enabled_webshop column
     *
     * Example usage:
     * <code>
     * $query->filterByIsEnabledWebshop(true); // WHERE is_enabled_webshop = true
     * $query->filterByIsEnabledWebshop('yes'); // WHERE is_enabled_webshop = true
     * </code>
     *
     * @param     boolean|string $isEnabledWebshop The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByIsEnabledWebshop($isEnabledWebshop = null, $comparison = null)
    {
        if (is_string($isEnabledWebshop)) {
            $isEnabledWebshop = in_array(strtolower($isEnabledWebshop), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(LanguageTableMap::COL_IS_ENABLED_WEBSHOP, $isEnabledWebshop, $comparison);
    }

    /**
     * Filter the query on the is_default_cms column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDefaultCms(true); // WHERE is_default_cms = true
     * $query->filterByIsDefaultCms('yes'); // WHERE is_default_cms = true
     * </code>
     *
     * @param     boolean|string $isDefaultCms The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByIsDefaultCms($isDefaultCms = null, $comparison = null)
    {
        if (is_string($isDefaultCms)) {
            $isDefaultCms = in_array(strtolower($isDefaultCms), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(LanguageTableMap::COL_IS_DEFAULT_CMS, $isDefaultCms, $comparison);
    }

    /**
     * Filter the query on the is_default_webshop column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDefaultWebshop(true); // WHERE is_default_webshop = true
     * $query->filterByIsDefaultWebshop('yes'); // WHERE is_default_webshop = true
     * </code>
     *
     * @param     boolean|string $isDefaultWebshop The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByIsDefaultWebshop($isDefaultWebshop = null, $comparison = null)
    {
        if (is_string($isDefaultWebshop)) {
            $isDefaultWebshop = in_array(strtolower($isDefaultWebshop), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(LanguageTableMap::COL_IS_DEFAULT_WEBSHOP, $isDefaultWebshop, $comparison);
    }

    /**
     * Filter the query on the flag_icon column
     *
     * Example usage:
     * <code>
     * $query->filterByFlagIcon('fooValue');   // WHERE flag_icon = 'fooValue'
     * $query->filterByFlagIcon('%fooValue%', Criteria::LIKE); // WHERE flag_icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $flagIcon The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByFlagIcon($flagIcon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($flagIcon)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LanguageTableMap::COL_FLAG_ICON, $flagIcon, $comparison);
    }

    /**
     * Filter the query on the shop_url_prefix column
     *
     * Example usage:
     * <code>
     * $query->filterByShopUrlPrefix('fooValue');   // WHERE shop_url_prefix = 'fooValue'
     * $query->filterByShopUrlPrefix('%fooValue%', Criteria::LIKE); // WHERE shop_url_prefix LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shopUrlPrefix The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByShopUrlPrefix($shopUrlPrefix = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shopUrlPrefix)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LanguageTableMap::COL_SHOP_URL_PREFIX, $shopUrlPrefix, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(LanguageTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(LanguageTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LanguageTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(LanguageTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(LanguageTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LanguageTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\LegalFormTranslation object
     *
     * @param \Model\Setting\MasterTable\LegalFormTranslation|ObjectCollection $legalFormTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByLegalFormTranslation($legalFormTranslation, $comparison = null)
    {
        if ($legalFormTranslation instanceof \Model\Setting\MasterTable\LegalFormTranslation) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $legalFormTranslation->getLanguageId(), $comparison);
        } elseif ($legalFormTranslation instanceof ObjectCollection) {
            return $this
                ->useLegalFormTranslationQuery()
                ->filterByPrimaryKeys($legalFormTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLegalFormTranslation() only accepts arguments of type \Model\Setting\MasterTable\LegalFormTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LegalFormTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinLegalFormTranslation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LegalFormTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LegalFormTranslation');
        }

        return $this;
    }

    /**
     * Use the LegalFormTranslation relation LegalFormTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\LegalFormTranslationQuery A secondary query class using the current class as primary query
     */
    public function useLegalFormTranslationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLegalFormTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LegalFormTranslation', '\Model\Setting\MasterTable\LegalFormTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\ContactMessage object
     *
     * @param \Model\ContactMessage|ObjectCollection $contactMessage the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByContactMessage($contactMessage, $comparison = null)
    {
        if ($contactMessage instanceof \Model\ContactMessage) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $contactMessage->getLanguageId(), $comparison);
        } elseif ($contactMessage instanceof ObjectCollection) {
            return $this
                ->useContactMessageQuery()
                ->filterByPrimaryKeys($contactMessage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContactMessage() only accepts arguments of type \Model\ContactMessage or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContactMessage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinContactMessage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContactMessage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContactMessage');
        }

        return $this;
    }

    /**
     * Use the ContactMessage relation ContactMessage object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ContactMessageQuery A secondary query class using the current class as primary query
     */
    public function useContactMessageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinContactMessage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContactMessage', '\Model\ContactMessageQuery');
    }

    /**
     * Filter the query by a related \Model\Account\User object
     *
     * @param \Model\Account\User|ObjectCollection $user the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof \Model\Account\User) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $user->getPreferedLanguageId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            return $this
                ->useUserQuery()
                ->filterByPrimaryKeys($user->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \Model\Account\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Account\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\Model\Account\UserQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\Customer object
     *
     * @param \Model\Crm\Customer|ObjectCollection $customer the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByCustomer($customer, $comparison = null)
    {
        if ($customer instanceof \Model\Crm\Customer) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $customer->getLanguageId(), $comparison);
        } elseif ($customer instanceof ObjectCollection) {
            return $this
                ->useCustomerQuery()
                ->filterByPrimaryKeys($customer->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomer() only accepts arguments of type \Model\Crm\Customer or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Customer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinCustomer($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Customer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Customer');
        }

        return $this;
    }

    /**
     * Use the Customer relation Customer object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerQuery A secondary query class using the current class as primary query
     */
    public function useCustomerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCustomer($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Customer', '\Model\Crm\CustomerQuery');
    }

    /**
     * Filter the query by a related \Model\Category\CategoryTranslation object
     *
     * @param \Model\Category\CategoryTranslation|ObjectCollection $categoryTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByCategoryTranslation($categoryTranslation, $comparison = null)
    {
        if ($categoryTranslation instanceof \Model\Category\CategoryTranslation) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $categoryTranslation->getLanguageId(), $comparison);
        } elseif ($categoryTranslation instanceof ObjectCollection) {
            return $this
                ->useCategoryTranslationQuery()
                ->filterByPrimaryKeys($categoryTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCategoryTranslation() only accepts arguments of type \Model\Category\CategoryTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CategoryTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinCategoryTranslation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CategoryTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CategoryTranslation');
        }

        return $this;
    }

    /**
     * Use the CategoryTranslation relation CategoryTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Category\CategoryTranslationQuery A secondary query class using the current class as primary query
     */
    public function useCategoryTranslationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCategoryTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CategoryTranslation', '\Model\Category\CategoryTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\PromoProductTranslation object
     *
     * @param \Model\PromoProductTranslation|ObjectCollection $promoProductTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByPromoProductTranslation($promoProductTranslation, $comparison = null)
    {
        if ($promoProductTranslation instanceof \Model\PromoProductTranslation) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $promoProductTranslation->getLanguageId(), $comparison);
        } elseif ($promoProductTranslation instanceof ObjectCollection) {
            return $this
                ->usePromoProductTranslationQuery()
                ->filterByPrimaryKeys($promoProductTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPromoProductTranslation() only accepts arguments of type \Model\PromoProductTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PromoProductTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinPromoProductTranslation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PromoProductTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PromoProductTranslation');
        }

        return $this;
    }

    /**
     * Use the PromoProductTranslation relation PromoProductTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\PromoProductTranslationQuery A secondary query class using the current class as primary query
     */
    public function usePromoProductTranslationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPromoProductTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PromoProductTranslation', '\Model\PromoProductTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\ProductTranslation object
     *
     * @param \Model\ProductTranslation|ObjectCollection $productTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByProductTranslation($productTranslation, $comparison = null)
    {
        if ($productTranslation instanceof \Model\ProductTranslation) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $productTranslation->getLanguageId(), $comparison);
        } elseif ($productTranslation instanceof ObjectCollection) {
            return $this
                ->useProductTranslationQuery()
                ->filterByPrimaryKeys($productTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductTranslation() only accepts arguments of type \Model\ProductTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinProductTranslation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductTranslation');
        }

        return $this;
    }

    /**
     * Use the ProductTranslation relation ProductTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ProductTranslationQuery A secondary query class using the current class as primary query
     */
    public function useProductTranslationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProductTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductTranslation', '\Model\ProductTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\VatTranslation object
     *
     * @param \Model\Setting\MasterTable\VatTranslation|ObjectCollection $vatTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByVatTranslation($vatTranslation, $comparison = null)
    {
        if ($vatTranslation instanceof \Model\Setting\MasterTable\VatTranslation) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $vatTranslation->getLanguageId(), $comparison);
        } elseif ($vatTranslation instanceof ObjectCollection) {
            return $this
                ->useVatTranslationQuery()
                ->filterByPrimaryKeys($vatTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByVatTranslation() only accepts arguments of type \Model\Setting\MasterTable\VatTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the VatTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinVatTranslation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('VatTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'VatTranslation');
        }

        return $this;
    }

    /**
     * Use the VatTranslation relation VatTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\VatTranslationQuery A secondary query class using the current class as primary query
     */
    public function useVatTranslationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinVatTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'VatTranslation', '\Model\Setting\MasterTable\VatTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\UsageTranslation object
     *
     * @param \Model\Setting\MasterTable\UsageTranslation|ObjectCollection $usageTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByUsageTranslation($usageTranslation, $comparison = null)
    {
        if ($usageTranslation instanceof \Model\Setting\MasterTable\UsageTranslation) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $usageTranslation->getLanguageId(), $comparison);
        } elseif ($usageTranslation instanceof ObjectCollection) {
            return $this
                ->useUsageTranslationQuery()
                ->filterByPrimaryKeys($usageTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUsageTranslation() only accepts arguments of type \Model\Setting\MasterTable\UsageTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsageTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinUsageTranslation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsageTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsageTranslation');
        }

        return $this;
    }

    /**
     * Use the UsageTranslation relation UsageTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\UsageTranslationQuery A secondary query class using the current class as primary query
     */
    public function useUsageTranslationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsageTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsageTranslation', '\Model\Setting\MasterTable\UsageTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\ColorTranslation object
     *
     * @param \Model\Setting\MasterTable\ColorTranslation|ObjectCollection $colorTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByColorTranslation($colorTranslation, $comparison = null)
    {
        if ($colorTranslation instanceof \Model\Setting\MasterTable\ColorTranslation) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $colorTranslation->getLanguageId(), $comparison);
        } elseif ($colorTranslation instanceof ObjectCollection) {
            return $this
                ->useColorTranslationQuery()
                ->filterByPrimaryKeys($colorTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByColorTranslation() only accepts arguments of type \Model\Setting\MasterTable\ColorTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ColorTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinColorTranslation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ColorTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ColorTranslation');
        }

        return $this;
    }

    /**
     * Use the ColorTranslation relation ColorTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\ColorTranslationQuery A secondary query class using the current class as primary query
     */
    public function useColorTranslationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinColorTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ColorTranslation', '\Model\Setting\MasterTable\ColorTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteUsp object
     *
     * @param \Model\Cms\SiteUsp|ObjectCollection $siteUsp the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterBySiteUsp($siteUsp, $comparison = null)
    {
        if ($siteUsp instanceof \Model\Cms\SiteUsp) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $siteUsp->getLanguageId(), $comparison);
        } elseif ($siteUsp instanceof ObjectCollection) {
            return $this
                ->useSiteUspQuery()
                ->filterByPrimaryKeys($siteUsp->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteUsp() only accepts arguments of type \Model\Cms\SiteUsp or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteUsp relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinSiteUsp($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteUsp');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteUsp');
        }

        return $this;
    }

    /**
     * Use the SiteUsp relation SiteUsp object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteUspQuery A secondary query class using the current class as primary query
     */
    public function useSiteUspQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSiteUsp($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteUsp', '\Model\Cms\SiteUspQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteFooterBlockTranslation object
     *
     * @param \Model\Cms\SiteFooterBlockTranslation|ObjectCollection $siteFooterBlockTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterBySiteFooterBlockTranslation($siteFooterBlockTranslation, $comparison = null)
    {
        if ($siteFooterBlockTranslation instanceof \Model\Cms\SiteFooterBlockTranslation) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $siteFooterBlockTranslation->getLanguageId(), $comparison);
        } elseif ($siteFooterBlockTranslation instanceof ObjectCollection) {
            return $this
                ->useSiteFooterBlockTranslationQuery()
                ->filterByPrimaryKeys($siteFooterBlockTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteFooterBlockTranslation() only accepts arguments of type \Model\Cms\SiteFooterBlockTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteFooterBlockTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinSiteFooterBlockTranslation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteFooterBlockTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteFooterBlockTranslation');
        }

        return $this;
    }

    /**
     * Use the SiteFooterBlockTranslation relation SiteFooterBlockTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteFooterBlockTranslationQuery A secondary query class using the current class as primary query
     */
    public function useSiteFooterBlockTranslationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteFooterBlockTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteFooterBlockTranslation', '\Model\Cms\SiteFooterBlockTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\ProductUnitTranslation object
     *
     * @param \Model\Setting\MasterTable\ProductUnitTranslation|ObjectCollection $productUnitTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByProductUnitTranslation($productUnitTranslation, $comparison = null)
    {
        if ($productUnitTranslation instanceof \Model\Setting\MasterTable\ProductUnitTranslation) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $productUnitTranslation->getLanguageId(), $comparison);
        } elseif ($productUnitTranslation instanceof ObjectCollection) {
            return $this
                ->useProductUnitTranslationQuery()
                ->filterByPrimaryKeys($productUnitTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductUnitTranslation() only accepts arguments of type \Model\Setting\MasterTable\ProductUnitTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductUnitTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinProductUnitTranslation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductUnitTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductUnitTranslation');
        }

        return $this;
    }

    /**
     * Use the ProductUnitTranslation relation ProductUnitTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\ProductUnitTranslationQuery A secondary query class using the current class as primary query
     */
    public function useProductUnitTranslationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProductUnitTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductUnitTranslation', '\Model\Setting\MasterTable\ProductUnitTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\ProductDeliveryTimeTranslation object
     *
     * @param \Model\Setting\MasterTable\ProductDeliveryTimeTranslation|ObjectCollection $productDeliveryTimeTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterByProductDeliveryTimeTranslation($productDeliveryTimeTranslation, $comparison = null)
    {
        if ($productDeliveryTimeTranslation instanceof \Model\Setting\MasterTable\ProductDeliveryTimeTranslation) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $productDeliveryTimeTranslation->getLanguageId(), $comparison);
        } elseif ($productDeliveryTimeTranslation instanceof ObjectCollection) {
            return $this
                ->useProductDeliveryTimeTranslationQuery()
                ->filterByPrimaryKeys($productDeliveryTimeTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductDeliveryTimeTranslation() only accepts arguments of type \Model\Setting\MasterTable\ProductDeliveryTimeTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductDeliveryTimeTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinProductDeliveryTimeTranslation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductDeliveryTimeTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductDeliveryTimeTranslation');
        }

        return $this;
    }

    /**
     * Use the ProductDeliveryTimeTranslation relation ProductDeliveryTimeTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\ProductDeliveryTimeTranslationQuery A secondary query class using the current class as primary query
     */
    public function useProductDeliveryTimeTranslationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProductDeliveryTimeTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductDeliveryTimeTranslation', '\Model\Setting\MasterTable\ProductDeliveryTimeTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteBlog object
     *
     * @param \Model\Cms\SiteBlog|ObjectCollection $siteBlog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterBySiteBlog($siteBlog, $comparison = null)
    {
        if ($siteBlog instanceof \Model\Cms\SiteBlog) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $siteBlog->getLanguageId(), $comparison);
        } elseif ($siteBlog instanceof ObjectCollection) {
            return $this
                ->useSiteBlogQuery()
                ->filterByPrimaryKeys($siteBlog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteBlog() only accepts arguments of type \Model\Cms\SiteBlog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteBlog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinSiteBlog($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteBlog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteBlog');
        }

        return $this;
    }

    /**
     * Use the SiteBlog relation SiteBlog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteBlogQuery A secondary query class using the current class as primary query
     */
    public function useSiteBlogQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteBlog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteBlog', '\Model\Cms\SiteBlogQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteFAQCategoryTranslation object
     *
     * @param \Model\Cms\SiteFAQCategoryTranslation|ObjectCollection $siteFAQCategoryTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterBySiteFAQCategoryTranslation($siteFAQCategoryTranslation, $comparison = null)
    {
        if ($siteFAQCategoryTranslation instanceof \Model\Cms\SiteFAQCategoryTranslation) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $siteFAQCategoryTranslation->getLanguageId(), $comparison);
        } elseif ($siteFAQCategoryTranslation instanceof ObjectCollection) {
            return $this
                ->useSiteFAQCategoryTranslationQuery()
                ->filterByPrimaryKeys($siteFAQCategoryTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteFAQCategoryTranslation() only accepts arguments of type \Model\Cms\SiteFAQCategoryTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteFAQCategoryTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinSiteFAQCategoryTranslation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteFAQCategoryTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteFAQCategoryTranslation');
        }

        return $this;
    }

    /**
     * Use the SiteFAQCategoryTranslation relation SiteFAQCategoryTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteFAQCategoryTranslationQuery A secondary query class using the current class as primary query
     */
    public function useSiteFAQCategoryTranslationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSiteFAQCategoryTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteFAQCategoryTranslation', '\Model\Cms\SiteFAQCategoryTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteFAQ object
     *
     * @param \Model\Cms\SiteFAQ|ObjectCollection $siteFAQ the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterBySiteFAQ($siteFAQ, $comparison = null)
    {
        if ($siteFAQ instanceof \Model\Cms\SiteFAQ) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $siteFAQ->getLanguageId(), $comparison);
        } elseif ($siteFAQ instanceof ObjectCollection) {
            return $this
                ->useSiteFAQQuery()
                ->filterByPrimaryKeys($siteFAQ->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteFAQ() only accepts arguments of type \Model\Cms\SiteFAQ or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteFAQ relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinSiteFAQ($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteFAQ');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteFAQ');
        }

        return $this;
    }

    /**
     * Use the SiteFAQ relation SiteFAQ object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteFAQQuery A secondary query class using the current class as primary query
     */
    public function useSiteFAQQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteFAQ($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteFAQ', '\Model\Cms\SiteFAQQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SitePage object
     *
     * @param \Model\Cms\SitePage|ObjectCollection $sitePage the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLanguageQuery The current query, for fluid interface
     */
    public function filterBySitePage($sitePage, $comparison = null)
    {
        if ($sitePage instanceof \Model\Cms\SitePage) {
            return $this
                ->addUsingAlias(LanguageTableMap::COL_ID, $sitePage->getLanguageId(), $comparison);
        } elseif ($sitePage instanceof ObjectCollection) {
            return $this
                ->useSitePageQuery()
                ->filterByPrimaryKeys($sitePage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySitePage() only accepts arguments of type \Model\Cms\SitePage or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SitePage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function joinSitePage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SitePage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SitePage');
        }

        return $this;
    }

    /**
     * Use the SitePage relation SitePage object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SitePageQuery A secondary query class using the current class as primary query
     */
    public function useSitePageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSitePage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SitePage', '\Model\Cms\SitePageQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildLanguage $language Object to remove from the list of results
     *
     * @return $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function prune($language = null)
    {
        if ($language) {
            $this->addUsingAlias(LanguageTableMap::COL_ID, $language->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mt_language table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LanguageTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            LanguageTableMap::clearInstancePool();
            LanguageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LanguageTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(LanguageTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            LanguageTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            LanguageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(LanguageTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(LanguageTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(LanguageTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(LanguageTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(LanguageTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildLanguageQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(LanguageTableMap::COL_CREATED_AT);
    }

} // LanguageQuery
