<?php

namespace Model\Setting\MasterTable\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\ContactMessage;
use Model\ContactMessageQuery;
use Model\ProductTranslation;
use Model\ProductTranslationQuery;
use Model\PromoProductTranslation;
use Model\PromoProductTranslationQuery;
use Model\Account\User;
use Model\Account\UserQuery;
use Model\Account\Base\User as BaseUser;
use Model\Account\Map\UserTableMap;
use Model\Base\ContactMessage as BaseContactMessage;
use Model\Base\ProductTranslation as BaseProductTranslation;
use Model\Base\PromoProductTranslation as BasePromoProductTranslation;
use Model\Category\CategoryTranslation;
use Model\Category\CategoryTranslationQuery;
use Model\Category\Base\CategoryTranslation as BaseCategoryTranslation;
use Model\Category\Map\CategoryTranslationTableMap;
use Model\Cms\SiteBlog;
use Model\Cms\SiteBlogQuery;
use Model\Cms\SiteFAQ;
use Model\Cms\SiteFAQCategoryTranslation;
use Model\Cms\SiteFAQCategoryTranslationQuery;
use Model\Cms\SiteFAQQuery;
use Model\Cms\SiteFooterBlockTranslation;
use Model\Cms\SiteFooterBlockTranslationQuery;
use Model\Cms\SitePage;
use Model\Cms\SitePageQuery;
use Model\Cms\SiteUsp;
use Model\Cms\SiteUspQuery;
use Model\Cms\Base\SiteBlog as BaseSiteBlog;
use Model\Cms\Base\SiteFAQ as BaseSiteFAQ;
use Model\Cms\Base\SiteFAQCategoryTranslation as BaseSiteFAQCategoryTranslation;
use Model\Cms\Base\SiteFooterBlockTranslation as BaseSiteFooterBlockTranslation;
use Model\Cms\Base\SitePage as BaseSitePage;
use Model\Cms\Base\SiteUsp as BaseSiteUsp;
use Model\Cms\Map\SiteBlogTableMap;
use Model\Cms\Map\SiteFAQCategoryTranslationTableMap;
use Model\Cms\Map\SiteFAQTableMap;
use Model\Cms\Map\SiteFooterBlockTranslationTableMap;
use Model\Cms\Map\SitePageTableMap;
use Model\Cms\Map\SiteUspTableMap;
use Model\Crm\Customer;
use Model\Crm\CustomerQuery;
use Model\Crm\Base\Customer as BaseCustomer;
use Model\Crm\Map\CustomerTableMap;
use Model\Map\ContactMessageTableMap;
use Model\Map\ProductTranslationTableMap;
use Model\Map\PromoProductTranslationTableMap;
use Model\Setting\MasterTable\ColorTranslation as ChildColorTranslation;
use Model\Setting\MasterTable\ColorTranslationQuery as ChildColorTranslationQuery;
use Model\Setting\MasterTable\Language as ChildLanguage;
use Model\Setting\MasterTable\LanguageQuery as ChildLanguageQuery;
use Model\Setting\MasterTable\LegalFormTranslation as ChildLegalFormTranslation;
use Model\Setting\MasterTable\LegalFormTranslationQuery as ChildLegalFormTranslationQuery;
use Model\Setting\MasterTable\ProductDeliveryTimeTranslation as ChildProductDeliveryTimeTranslation;
use Model\Setting\MasterTable\ProductDeliveryTimeTranslationQuery as ChildProductDeliveryTimeTranslationQuery;
use Model\Setting\MasterTable\ProductUnitTranslation as ChildProductUnitTranslation;
use Model\Setting\MasterTable\ProductUnitTranslationQuery as ChildProductUnitTranslationQuery;
use Model\Setting\MasterTable\UsageTranslation as ChildUsageTranslation;
use Model\Setting\MasterTable\UsageTranslationQuery as ChildUsageTranslationQuery;
use Model\Setting\MasterTable\VatTranslation as ChildVatTranslation;
use Model\Setting\MasterTable\VatTranslationQuery as ChildVatTranslationQuery;
use Model\Setting\MasterTable\Map\ColorTranslationTableMap;
use Model\Setting\MasterTable\Map\LanguageTableMap;
use Model\Setting\MasterTable\Map\LegalFormTranslationTableMap;
use Model\Setting\MasterTable\Map\ProductDeliveryTimeTranslationTableMap;
use Model\Setting\MasterTable\Map\ProductUnitTranslationTableMap;
use Model\Setting\MasterTable\Map\UsageTranslationTableMap;
use Model\Setting\MasterTable\Map\VatTranslationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'mt_language' table.
 *
 *
 *
 * @package    propel.generator.Model.Setting.MasterTable.Base
 */
abstract class Language implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Setting\\MasterTable\\Map\\LanguageTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the is_deleted field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_deleted;

    /**
     * The value for the description field.
     *
     * @var        string|null
     */
    protected $description;

    /**
     * The value for the locale_code field.
     *
     * @var        string|null
     */
    protected $locale_code;

    /**
     * The value for the is_enabled_cms field.
     *
     * @var        boolean
     */
    protected $is_enabled_cms;

    /**
     * The value for the is_enabled_webshop field.
     *
     * @var        boolean
     */
    protected $is_enabled_webshop;

    /**
     * The value for the is_default_cms field.
     *
     * @var        boolean
     */
    protected $is_default_cms;

    /**
     * The value for the is_default_webshop field.
     *
     * @var        boolean
     */
    protected $is_default_webshop;

    /**
     * The value for the flag_icon field.
     *
     * @var        string|null
     */
    protected $flag_icon;

    /**
     * The value for the shop_url_prefix field.
     *
     * @var        string|null
     */
    protected $shop_url_prefix;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        ObjectCollection|ChildLegalFormTranslation[] Collection to store aggregation of ChildLegalFormTranslation objects.
     */
    protected $collLegalFormTranslations;
    protected $collLegalFormTranslationsPartial;

    /**
     * @var        ObjectCollection|ContactMessage[] Collection to store aggregation of ContactMessage objects.
     */
    protected $collContactMessages;
    protected $collContactMessagesPartial;

    /**
     * @var        ObjectCollection|User[] Collection to store aggregation of User objects.
     */
    protected $collUsers;
    protected $collUsersPartial;

    /**
     * @var        ObjectCollection|Customer[] Collection to store aggregation of Customer objects.
     */
    protected $collCustomers;
    protected $collCustomersPartial;

    /**
     * @var        ObjectCollection|CategoryTranslation[] Collection to store aggregation of CategoryTranslation objects.
     */
    protected $collCategoryTranslations;
    protected $collCategoryTranslationsPartial;

    /**
     * @var        ObjectCollection|PromoProductTranslation[] Collection to store aggregation of PromoProductTranslation objects.
     */
    protected $collPromoProductTranslations;
    protected $collPromoProductTranslationsPartial;

    /**
     * @var        ObjectCollection|ProductTranslation[] Collection to store aggregation of ProductTranslation objects.
     */
    protected $collProductTranslations;
    protected $collProductTranslationsPartial;

    /**
     * @var        ObjectCollection|ChildVatTranslation[] Collection to store aggregation of ChildVatTranslation objects.
     */
    protected $collVatTranslations;
    protected $collVatTranslationsPartial;

    /**
     * @var        ObjectCollection|ChildUsageTranslation[] Collection to store aggregation of ChildUsageTranslation objects.
     */
    protected $collUsageTranslations;
    protected $collUsageTranslationsPartial;

    /**
     * @var        ObjectCollection|ChildColorTranslation[] Collection to store aggregation of ChildColorTranslation objects.
     */
    protected $collColorTranslations;
    protected $collColorTranslationsPartial;

    /**
     * @var        ObjectCollection|SiteUsp[] Collection to store aggregation of SiteUsp objects.
     */
    protected $collSiteUsps;
    protected $collSiteUspsPartial;

    /**
     * @var        ObjectCollection|SiteFooterBlockTranslation[] Collection to store aggregation of SiteFooterBlockTranslation objects.
     */
    protected $collSiteFooterBlockTranslations;
    protected $collSiteFooterBlockTranslationsPartial;

    /**
     * @var        ObjectCollection|ChildProductUnitTranslation[] Collection to store aggregation of ChildProductUnitTranslation objects.
     */
    protected $collProductUnitTranslations;
    protected $collProductUnitTranslationsPartial;

    /**
     * @var        ObjectCollection|ChildProductDeliveryTimeTranslation[] Collection to store aggregation of ChildProductDeliveryTimeTranslation objects.
     */
    protected $collProductDeliveryTimeTranslations;
    protected $collProductDeliveryTimeTranslationsPartial;

    /**
     * @var        ObjectCollection|SiteBlog[] Collection to store aggregation of SiteBlog objects.
     */
    protected $collSiteBlogs;
    protected $collSiteBlogsPartial;

    /**
     * @var        ObjectCollection|SiteFAQCategoryTranslation[] Collection to store aggregation of SiteFAQCategoryTranslation objects.
     */
    protected $collSiteFAQCategoryTranslations;
    protected $collSiteFAQCategoryTranslationsPartial;

    /**
     * @var        ObjectCollection|SiteFAQ[] Collection to store aggregation of SiteFAQ objects.
     */
    protected $collSiteFAQs;
    protected $collSiteFAQsPartial;

    /**
     * @var        ObjectCollection|SitePage[] Collection to store aggregation of SitePage objects.
     */
    protected $collSitePages;
    protected $collSitePagesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildLegalFormTranslation[]
     */
    protected $legalFormTranslationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ContactMessage[]
     */
    protected $contactMessagesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|User[]
     */
    protected $usersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Customer[]
     */
    protected $customersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|CategoryTranslation[]
     */
    protected $categoryTranslationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|PromoProductTranslation[]
     */
    protected $promoProductTranslationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ProductTranslation[]
     */
    protected $productTranslationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildVatTranslation[]
     */
    protected $vatTranslationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsageTranslation[]
     */
    protected $usageTranslationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildColorTranslation[]
     */
    protected $colorTranslationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|SiteUsp[]
     */
    protected $siteUspsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|SiteFooterBlockTranslation[]
     */
    protected $siteFooterBlockTranslationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProductUnitTranslation[]
     */
    protected $productUnitTranslationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProductDeliveryTimeTranslation[]
     */
    protected $productDeliveryTimeTranslationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|SiteBlog[]
     */
    protected $siteBlogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|SiteFAQCategoryTranslation[]
     */
    protected $siteFAQCategoryTranslationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|SiteFAQ[]
     */
    protected $siteFAQsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|SitePage[]
     */
    protected $sitePagesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_deleted = false;
    }

    /**
     * Initializes internal state of Model\Setting\MasterTable\Base\Language object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Language</code> instance.  If
     * <code>obj</code> is an instance of <code>Language</code>, delegates to
     * <code>equals(Language)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [is_deleted] column value.
     *
     * @return boolean|null
     */
    public function getItemDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Get the [is_deleted] column value.
     *
     * @return boolean|null
     */
    public function isItemDeleted()
    {
        return $this->getItemDeleted();
    }

    /**
     * Get the [description] column value.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [locale_code] column value.
     *
     * @return string|null
     */
    public function getLocaleCode()
    {
        return $this->locale_code;
    }

    /**
     * Get the [is_enabled_cms] column value.
     *
     * @return boolean
     */
    public function getIsEnabledCms()
    {
        return $this->is_enabled_cms;
    }

    /**
     * Get the [is_enabled_cms] column value.
     *
     * @return boolean
     */
    public function isEnabledCms()
    {
        return $this->getIsEnabledCms();
    }

    /**
     * Get the [is_enabled_webshop] column value.
     *
     * @return boolean
     */
    public function getIsEnabledWebshop()
    {
        return $this->is_enabled_webshop;
    }

    /**
     * Get the [is_enabled_webshop] column value.
     *
     * @return boolean
     */
    public function isEnabledWebshop()
    {
        return $this->getIsEnabledWebshop();
    }

    /**
     * Get the [is_default_cms] column value.
     *
     * @return boolean
     */
    public function getIsDefaultCms()
    {
        return $this->is_default_cms;
    }

    /**
     * Get the [is_default_cms] column value.
     *
     * @return boolean
     */
    public function isDefaultCms()
    {
        return $this->getIsDefaultCms();
    }

    /**
     * Get the [is_default_webshop] column value.
     *
     * @return boolean
     */
    public function getIsDefaultWebshop()
    {
        return $this->is_default_webshop;
    }

    /**
     * Get the [is_default_webshop] column value.
     *
     * @return boolean
     */
    public function isDefaultWebshop()
    {
        return $this->getIsDefaultWebshop();
    }

    /**
     * Get the [flag_icon] column value.
     *
     * @return string|null
     */
    public function getFlagIcon()
    {
        return $this->flag_icon;
    }

    /**
     * Get the [shop_url_prefix] column value.
     *
     * @return string|null
     */
    public function getShopUrlPrefix()
    {
        return $this->shop_url_prefix;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[LanguageTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Sets the value of the [is_deleted] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function setItemDeleted($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_deleted !== $v) {
            $this->is_deleted = $v;
            $this->modifiedColumns[LanguageTableMap::COL_IS_DELETED] = true;
        }

        return $this;
    } // setItemDeleted()

    /**
     * Set the value of [description] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[LanguageTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [locale_code] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function setLocaleCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->locale_code !== $v) {
            $this->locale_code = $v;
            $this->modifiedColumns[LanguageTableMap::COL_LOCALE_CODE] = true;
        }

        return $this;
    } // setLocaleCode()

    /**
     * Sets the value of the [is_enabled_cms] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function setIsEnabledCms($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_enabled_cms !== $v) {
            $this->is_enabled_cms = $v;
            $this->modifiedColumns[LanguageTableMap::COL_IS_ENABLED_CMS] = true;
        }

        return $this;
    } // setIsEnabledCms()

    /**
     * Sets the value of the [is_enabled_webshop] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function setIsEnabledWebshop($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_enabled_webshop !== $v) {
            $this->is_enabled_webshop = $v;
            $this->modifiedColumns[LanguageTableMap::COL_IS_ENABLED_WEBSHOP] = true;
        }

        return $this;
    } // setIsEnabledWebshop()

    /**
     * Sets the value of the [is_default_cms] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function setIsDefaultCms($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_default_cms !== $v) {
            $this->is_default_cms = $v;
            $this->modifiedColumns[LanguageTableMap::COL_IS_DEFAULT_CMS] = true;
        }

        return $this;
    } // setIsDefaultCms()

    /**
     * Sets the value of the [is_default_webshop] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function setIsDefaultWebshop($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_default_webshop !== $v) {
            $this->is_default_webshop = $v;
            $this->modifiedColumns[LanguageTableMap::COL_IS_DEFAULT_WEBSHOP] = true;
        }

        return $this;
    } // setIsDefaultWebshop()

    /**
     * Set the value of [flag_icon] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function setFlagIcon($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->flag_icon !== $v) {
            $this->flag_icon = $v;
            $this->modifiedColumns[LanguageTableMap::COL_FLAG_ICON] = true;
        }

        return $this;
    } // setFlagIcon()

    /**
     * Set the value of [shop_url_prefix] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function setShopUrlPrefix($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shop_url_prefix !== $v) {
            $this->shop_url_prefix = $v;
            $this->modifiedColumns[LanguageTableMap::COL_SHOP_URL_PREFIX] = true;
        }

        return $this;
    } // setShopUrlPrefix()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[LanguageTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[LanguageTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_deleted !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : LanguageTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : LanguageTableMap::translateFieldName('ItemDeleted', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_deleted = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : LanguageTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : LanguageTableMap::translateFieldName('LocaleCode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->locale_code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : LanguageTableMap::translateFieldName('IsEnabledCms', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_enabled_cms = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : LanguageTableMap::translateFieldName('IsEnabledWebshop', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_enabled_webshop = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : LanguageTableMap::translateFieldName('IsDefaultCms', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_default_cms = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : LanguageTableMap::translateFieldName('IsDefaultWebshop', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_default_webshop = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : LanguageTableMap::translateFieldName('FlagIcon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->flag_icon = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : LanguageTableMap::translateFieldName('ShopUrlPrefix', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shop_url_prefix = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : LanguageTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : LanguageTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 12; // 12 = LanguageTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Setting\\MasterTable\\Language'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LanguageTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildLanguageQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collLegalFormTranslations = null;

            $this->collContactMessages = null;

            $this->collUsers = null;

            $this->collCustomers = null;

            $this->collCategoryTranslations = null;

            $this->collPromoProductTranslations = null;

            $this->collProductTranslations = null;

            $this->collVatTranslations = null;

            $this->collUsageTranslations = null;

            $this->collColorTranslations = null;

            $this->collSiteUsps = null;

            $this->collSiteFooterBlockTranslations = null;

            $this->collProductUnitTranslations = null;

            $this->collProductDeliveryTimeTranslations = null;

            $this->collSiteBlogs = null;

            $this->collSiteFAQCategoryTranslations = null;

            $this->collSiteFAQs = null;

            $this->collSitePages = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Language::setDeleted()
     * @see Language::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(LanguageTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildLanguageQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(LanguageTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(LanguageTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(LanguageTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(LanguageTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                LanguageTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->legalFormTranslationsScheduledForDeletion !== null) {
                if (!$this->legalFormTranslationsScheduledForDeletion->isEmpty()) {
                    \Model\Setting\MasterTable\LegalFormTranslationQuery::create()
                        ->filterByPrimaryKeys($this->legalFormTranslationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->legalFormTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collLegalFormTranslations !== null) {
                foreach ($this->collLegalFormTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->contactMessagesScheduledForDeletion !== null) {
                if (!$this->contactMessagesScheduledForDeletion->isEmpty()) {
                    foreach ($this->contactMessagesScheduledForDeletion as $contactMessage) {
                        // need to save related object because we set the relation to null
                        $contactMessage->save($con);
                    }
                    $this->contactMessagesScheduledForDeletion = null;
                }
            }

            if ($this->collContactMessages !== null) {
                foreach ($this->collContactMessages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usersScheduledForDeletion !== null) {
                if (!$this->usersScheduledForDeletion->isEmpty()) {
                    foreach ($this->usersScheduledForDeletion as $user) {
                        // need to save related object because we set the relation to null
                        $user->save($con);
                    }
                    $this->usersScheduledForDeletion = null;
                }
            }

            if ($this->collUsers !== null) {
                foreach ($this->collUsers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->customersScheduledForDeletion !== null) {
                if (!$this->customersScheduledForDeletion->isEmpty()) {
                    foreach ($this->customersScheduledForDeletion as $customer) {
                        // need to save related object because we set the relation to null
                        $customer->save($con);
                    }
                    $this->customersScheduledForDeletion = null;
                }
            }

            if ($this->collCustomers !== null) {
                foreach ($this->collCustomers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->categoryTranslationsScheduledForDeletion !== null) {
                if (!$this->categoryTranslationsScheduledForDeletion->isEmpty()) {
                    \Model\Category\CategoryTranslationQuery::create()
                        ->filterByPrimaryKeys($this->categoryTranslationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->categoryTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collCategoryTranslations !== null) {
                foreach ($this->collCategoryTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->promoProductTranslationsScheduledForDeletion !== null) {
                if (!$this->promoProductTranslationsScheduledForDeletion->isEmpty()) {
                    \Model\PromoProductTranslationQuery::create()
                        ->filterByPrimaryKeys($this->promoProductTranslationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->promoProductTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collPromoProductTranslations !== null) {
                foreach ($this->collPromoProductTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productTranslationsScheduledForDeletion !== null) {
                if (!$this->productTranslationsScheduledForDeletion->isEmpty()) {
                    \Model\ProductTranslationQuery::create()
                        ->filterByPrimaryKeys($this->productTranslationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collProductTranslations !== null) {
                foreach ($this->collProductTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->vatTranslationsScheduledForDeletion !== null) {
                if (!$this->vatTranslationsScheduledForDeletion->isEmpty()) {
                    \Model\Setting\MasterTable\VatTranslationQuery::create()
                        ->filterByPrimaryKeys($this->vatTranslationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->vatTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collVatTranslations !== null) {
                foreach ($this->collVatTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usageTranslationsScheduledForDeletion !== null) {
                if (!$this->usageTranslationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->usageTranslationsScheduledForDeletion as $usageTranslation) {
                        // need to save related object because we set the relation to null
                        $usageTranslation->save($con);
                    }
                    $this->usageTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collUsageTranslations !== null) {
                foreach ($this->collUsageTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->colorTranslationsScheduledForDeletion !== null) {
                if (!$this->colorTranslationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->colorTranslationsScheduledForDeletion as $colorTranslation) {
                        // need to save related object because we set the relation to null
                        $colorTranslation->save($con);
                    }
                    $this->colorTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collColorTranslations !== null) {
                foreach ($this->collColorTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteUspsScheduledForDeletion !== null) {
                if (!$this->siteUspsScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SiteUspQuery::create()
                        ->filterByPrimaryKeys($this->siteUspsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteUspsScheduledForDeletion = null;
                }
            }

            if ($this->collSiteUsps !== null) {
                foreach ($this->collSiteUsps as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteFooterBlockTranslationsScheduledForDeletion !== null) {
                if (!$this->siteFooterBlockTranslationsScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SiteFooterBlockTranslationQuery::create()
                        ->filterByPrimaryKeys($this->siteFooterBlockTranslationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteFooterBlockTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collSiteFooterBlockTranslations !== null) {
                foreach ($this->collSiteFooterBlockTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productUnitTranslationsScheduledForDeletion !== null) {
                if (!$this->productUnitTranslationsScheduledForDeletion->isEmpty()) {
                    \Model\Setting\MasterTable\ProductUnitTranslationQuery::create()
                        ->filterByPrimaryKeys($this->productUnitTranslationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productUnitTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collProductUnitTranslations !== null) {
                foreach ($this->collProductUnitTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productDeliveryTimeTranslationsScheduledForDeletion !== null) {
                if (!$this->productDeliveryTimeTranslationsScheduledForDeletion->isEmpty()) {
                    \Model\Setting\MasterTable\ProductDeliveryTimeTranslationQuery::create()
                        ->filterByPrimaryKeys($this->productDeliveryTimeTranslationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productDeliveryTimeTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collProductDeliveryTimeTranslations !== null) {
                foreach ($this->collProductDeliveryTimeTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteBlogsScheduledForDeletion !== null) {
                if (!$this->siteBlogsScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SiteBlogQuery::create()
                        ->filterByPrimaryKeys($this->siteBlogsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteBlogsScheduledForDeletion = null;
                }
            }

            if ($this->collSiteBlogs !== null) {
                foreach ($this->collSiteBlogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteFAQCategoryTranslationsScheduledForDeletion !== null) {
                if (!$this->siteFAQCategoryTranslationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->siteFAQCategoryTranslationsScheduledForDeletion as $siteFAQCategoryTranslation) {
                        // need to save related object because we set the relation to null
                        $siteFAQCategoryTranslation->save($con);
                    }
                    $this->siteFAQCategoryTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collSiteFAQCategoryTranslations !== null) {
                foreach ($this->collSiteFAQCategoryTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteFAQsScheduledForDeletion !== null) {
                if (!$this->siteFAQsScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SiteFAQQuery::create()
                        ->filterByPrimaryKeys($this->siteFAQsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteFAQsScheduledForDeletion = null;
                }
            }

            if ($this->collSiteFAQs !== null) {
                foreach ($this->collSiteFAQs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sitePagesScheduledForDeletion !== null) {
                if (!$this->sitePagesScheduledForDeletion->isEmpty()) {
                    foreach ($this->sitePagesScheduledForDeletion as $sitePage) {
                        // need to save related object because we set the relation to null
                        $sitePage->save($con);
                    }
                    $this->sitePagesScheduledForDeletion = null;
                }
            }

            if ($this->collSitePages !== null) {
                foreach ($this->collSitePages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[LanguageTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . LanguageTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(LanguageTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(LanguageTableMap::COL_IS_DELETED)) {
            $modifiedColumns[':p' . $index++]  = 'is_deleted';
        }
        if ($this->isColumnModified(LanguageTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }
        if ($this->isColumnModified(LanguageTableMap::COL_LOCALE_CODE)) {
            $modifiedColumns[':p' . $index++]  = 'locale_code';
        }
        if ($this->isColumnModified(LanguageTableMap::COL_IS_ENABLED_CMS)) {
            $modifiedColumns[':p' . $index++]  = 'is_enabled_cms';
        }
        if ($this->isColumnModified(LanguageTableMap::COL_IS_ENABLED_WEBSHOP)) {
            $modifiedColumns[':p' . $index++]  = 'is_enabled_webshop';
        }
        if ($this->isColumnModified(LanguageTableMap::COL_IS_DEFAULT_CMS)) {
            $modifiedColumns[':p' . $index++]  = 'is_default_cms';
        }
        if ($this->isColumnModified(LanguageTableMap::COL_IS_DEFAULT_WEBSHOP)) {
            $modifiedColumns[':p' . $index++]  = 'is_default_webshop';
        }
        if ($this->isColumnModified(LanguageTableMap::COL_FLAG_ICON)) {
            $modifiedColumns[':p' . $index++]  = 'flag_icon';
        }
        if ($this->isColumnModified(LanguageTableMap::COL_SHOP_URL_PREFIX)) {
            $modifiedColumns[':p' . $index++]  = 'shop_url_prefix';
        }
        if ($this->isColumnModified(LanguageTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(LanguageTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO mt_language (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'is_deleted':
                        $stmt->bindValue($identifier, (int) $this->is_deleted, PDO::PARAM_INT);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'locale_code':
                        $stmt->bindValue($identifier, $this->locale_code, PDO::PARAM_STR);
                        break;
                    case 'is_enabled_cms':
                        $stmt->bindValue($identifier, (int) $this->is_enabled_cms, PDO::PARAM_INT);
                        break;
                    case 'is_enabled_webshop':
                        $stmt->bindValue($identifier, (int) $this->is_enabled_webshop, PDO::PARAM_INT);
                        break;
                    case 'is_default_cms':
                        $stmt->bindValue($identifier, (int) $this->is_default_cms, PDO::PARAM_INT);
                        break;
                    case 'is_default_webshop':
                        $stmt->bindValue($identifier, (int) $this->is_default_webshop, PDO::PARAM_INT);
                        break;
                    case 'flag_icon':
                        $stmt->bindValue($identifier, $this->flag_icon, PDO::PARAM_STR);
                        break;
                    case 'shop_url_prefix':
                        $stmt->bindValue($identifier, $this->shop_url_prefix, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = LanguageTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getItemDeleted();
                break;
            case 2:
                return $this->getDescription();
                break;
            case 3:
                return $this->getLocaleCode();
                break;
            case 4:
                return $this->getIsEnabledCms();
                break;
            case 5:
                return $this->getIsEnabledWebshop();
                break;
            case 6:
                return $this->getIsDefaultCms();
                break;
            case 7:
                return $this->getIsDefaultWebshop();
                break;
            case 8:
                return $this->getFlagIcon();
                break;
            case 9:
                return $this->getShopUrlPrefix();
                break;
            case 10:
                return $this->getCreatedAt();
                break;
            case 11:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Language'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Language'][$this->hashCode()] = true;
        $keys = LanguageTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getItemDeleted(),
            $keys[2] => $this->getDescription(),
            $keys[3] => $this->getLocaleCode(),
            $keys[4] => $this->getIsEnabledCms(),
            $keys[5] => $this->getIsEnabledWebshop(),
            $keys[6] => $this->getIsDefaultCms(),
            $keys[7] => $this->getIsDefaultWebshop(),
            $keys[8] => $this->getFlagIcon(),
            $keys[9] => $this->getShopUrlPrefix(),
            $keys[10] => $this->getCreatedAt(),
            $keys[11] => $this->getUpdatedAt(),
        );
        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collLegalFormTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'legalFormTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_legal_form_translations';
                        break;
                    default:
                        $key = 'LegalFormTranslations';
                }

                $result[$key] = $this->collLegalFormTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collContactMessages) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contactMessages';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contact_messages';
                        break;
                    default:
                        $key = 'ContactMessages';
                }

                $result[$key] = $this->collContactMessages->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'users';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'users';
                        break;
                    default:
                        $key = 'Users';
                }

                $result[$key] = $this->collUsers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCustomers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'customers';
                        break;
                    default:
                        $key = 'Customers';
                }

                $result[$key] = $this->collCustomers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCategoryTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'categoryTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'category_translations';
                        break;
                    default:
                        $key = 'CategoryTranslations';
                }

                $result[$key] = $this->collCategoryTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPromoProductTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'promoProductTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'promo_product_translations';
                        break;
                    default:
                        $key = 'PromoProductTranslations';
                }

                $result[$key] = $this->collPromoProductTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProductTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_translations';
                        break;
                    default:
                        $key = 'ProductTranslations';
                }

                $result[$key] = $this->collProductTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collVatTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'vatTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_vat_translations';
                        break;
                    default:
                        $key = 'VatTranslations';
                }

                $result[$key] = $this->collVatTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsageTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usageTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_usage_translations';
                        break;
                    default:
                        $key = 'UsageTranslations';
                }

                $result[$key] = $this->collUsageTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collColorTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'colorTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_color_translations';
                        break;
                    default:
                        $key = 'ColorTranslations';
                }

                $result[$key] = $this->collColorTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteUsps) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteUsps';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_usps';
                        break;
                    default:
                        $key = 'SiteUsps';
                }

                $result[$key] = $this->collSiteUsps->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteFooterBlockTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteFooterBlockTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_footer_block_translations';
                        break;
                    default:
                        $key = 'SiteFooterBlockTranslations';
                }

                $result[$key] = $this->collSiteFooterBlockTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProductUnitTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productUnitTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_unit_translations';
                        break;
                    default:
                        $key = 'ProductUnitTranslations';
                }

                $result[$key] = $this->collProductUnitTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProductDeliveryTimeTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productDeliveryTimeTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_delivery_time_translations';
                        break;
                    default:
                        $key = 'ProductDeliveryTimeTranslations';
                }

                $result[$key] = $this->collProductDeliveryTimeTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteBlogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteBlogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_blogs';
                        break;
                    default:
                        $key = 'SiteBlogs';
                }

                $result[$key] = $this->collSiteBlogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteFAQCategoryTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteFAQCategoryTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_faq_category_translations';
                        break;
                    default:
                        $key = 'SiteFAQCategoryTranslations';
                }

                $result[$key] = $this->collSiteFAQCategoryTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteFAQs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteFAQs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_faqs';
                        break;
                    default:
                        $key = 'SiteFAQs';
                }

                $result[$key] = $this->collSiteFAQs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSitePages) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sitePages';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_pages';
                        break;
                    default:
                        $key = 'SitePages';
                }

                $result[$key] = $this->collSitePages->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Setting\MasterTable\Language
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = LanguageTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Setting\MasterTable\Language
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setItemDeleted($value);
                break;
            case 2:
                $this->setDescription($value);
                break;
            case 3:
                $this->setLocaleCode($value);
                break;
            case 4:
                $this->setIsEnabledCms($value);
                break;
            case 5:
                $this->setIsEnabledWebshop($value);
                break;
            case 6:
                $this->setIsDefaultCms($value);
                break;
            case 7:
                $this->setIsDefaultWebshop($value);
                break;
            case 8:
                $this->setFlagIcon($value);
                break;
            case 9:
                $this->setShopUrlPrefix($value);
                break;
            case 10:
                $this->setCreatedAt($value);
                break;
            case 11:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = LanguageTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setItemDeleted($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setDescription($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setLocaleCode($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setIsEnabledCms($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setIsEnabledWebshop($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIsDefaultCms($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setIsDefaultWebshop($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setFlagIcon($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setShopUrlPrefix($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setCreatedAt($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUpdatedAt($arr[$keys[11]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Setting\MasterTable\Language The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(LanguageTableMap::DATABASE_NAME);

        if ($this->isColumnModified(LanguageTableMap::COL_ID)) {
            $criteria->add(LanguageTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(LanguageTableMap::COL_IS_DELETED)) {
            $criteria->add(LanguageTableMap::COL_IS_DELETED, $this->is_deleted);
        }
        if ($this->isColumnModified(LanguageTableMap::COL_DESCRIPTION)) {
            $criteria->add(LanguageTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(LanguageTableMap::COL_LOCALE_CODE)) {
            $criteria->add(LanguageTableMap::COL_LOCALE_CODE, $this->locale_code);
        }
        if ($this->isColumnModified(LanguageTableMap::COL_IS_ENABLED_CMS)) {
            $criteria->add(LanguageTableMap::COL_IS_ENABLED_CMS, $this->is_enabled_cms);
        }
        if ($this->isColumnModified(LanguageTableMap::COL_IS_ENABLED_WEBSHOP)) {
            $criteria->add(LanguageTableMap::COL_IS_ENABLED_WEBSHOP, $this->is_enabled_webshop);
        }
        if ($this->isColumnModified(LanguageTableMap::COL_IS_DEFAULT_CMS)) {
            $criteria->add(LanguageTableMap::COL_IS_DEFAULT_CMS, $this->is_default_cms);
        }
        if ($this->isColumnModified(LanguageTableMap::COL_IS_DEFAULT_WEBSHOP)) {
            $criteria->add(LanguageTableMap::COL_IS_DEFAULT_WEBSHOP, $this->is_default_webshop);
        }
        if ($this->isColumnModified(LanguageTableMap::COL_FLAG_ICON)) {
            $criteria->add(LanguageTableMap::COL_FLAG_ICON, $this->flag_icon);
        }
        if ($this->isColumnModified(LanguageTableMap::COL_SHOP_URL_PREFIX)) {
            $criteria->add(LanguageTableMap::COL_SHOP_URL_PREFIX, $this->shop_url_prefix);
        }
        if ($this->isColumnModified(LanguageTableMap::COL_CREATED_AT)) {
            $criteria->add(LanguageTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(LanguageTableMap::COL_UPDATED_AT)) {
            $criteria->add(LanguageTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildLanguageQuery::create();
        $criteria->add(LanguageTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Setting\MasterTable\Language (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setItemDeleted($this->getItemDeleted());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setLocaleCode($this->getLocaleCode());
        $copyObj->setIsEnabledCms($this->getIsEnabledCms());
        $copyObj->setIsEnabledWebshop($this->getIsEnabledWebshop());
        $copyObj->setIsDefaultCms($this->getIsDefaultCms());
        $copyObj->setIsDefaultWebshop($this->getIsDefaultWebshop());
        $copyObj->setFlagIcon($this->getFlagIcon());
        $copyObj->setShopUrlPrefix($this->getShopUrlPrefix());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getLegalFormTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addLegalFormTranslation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getContactMessages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContactMessage($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUser($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCustomers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomer($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCategoryTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCategoryTranslation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPromoProductTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPromoProductTranslation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProductTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductTranslation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getVatTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addVatTranslation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsageTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsageTranslation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getColorTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addColorTranslation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteUsps() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteUsp($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteFooterBlockTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteFooterBlockTranslation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProductUnitTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductUnitTranslation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProductDeliveryTimeTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductDeliveryTimeTranslation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteBlogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteBlog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteFAQCategoryTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteFAQCategoryTranslation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteFAQs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteFAQ($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSitePages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSitePage($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Setting\MasterTable\Language Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('LegalFormTranslation' === $relationName) {
            $this->initLegalFormTranslations();
            return;
        }
        if ('ContactMessage' === $relationName) {
            $this->initContactMessages();
            return;
        }
        if ('User' === $relationName) {
            $this->initUsers();
            return;
        }
        if ('Customer' === $relationName) {
            $this->initCustomers();
            return;
        }
        if ('CategoryTranslation' === $relationName) {
            $this->initCategoryTranslations();
            return;
        }
        if ('PromoProductTranslation' === $relationName) {
            $this->initPromoProductTranslations();
            return;
        }
        if ('ProductTranslation' === $relationName) {
            $this->initProductTranslations();
            return;
        }
        if ('VatTranslation' === $relationName) {
            $this->initVatTranslations();
            return;
        }
        if ('UsageTranslation' === $relationName) {
            $this->initUsageTranslations();
            return;
        }
        if ('ColorTranslation' === $relationName) {
            $this->initColorTranslations();
            return;
        }
        if ('SiteUsp' === $relationName) {
            $this->initSiteUsps();
            return;
        }
        if ('SiteFooterBlockTranslation' === $relationName) {
            $this->initSiteFooterBlockTranslations();
            return;
        }
        if ('ProductUnitTranslation' === $relationName) {
            $this->initProductUnitTranslations();
            return;
        }
        if ('ProductDeliveryTimeTranslation' === $relationName) {
            $this->initProductDeliveryTimeTranslations();
            return;
        }
        if ('SiteBlog' === $relationName) {
            $this->initSiteBlogs();
            return;
        }
        if ('SiteFAQCategoryTranslation' === $relationName) {
            $this->initSiteFAQCategoryTranslations();
            return;
        }
        if ('SiteFAQ' === $relationName) {
            $this->initSiteFAQs();
            return;
        }
        if ('SitePage' === $relationName) {
            $this->initSitePages();
            return;
        }
    }

    /**
     * Clears out the collLegalFormTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addLegalFormTranslations()
     */
    public function clearLegalFormTranslations()
    {
        $this->collLegalFormTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collLegalFormTranslations collection loaded partially.
     */
    public function resetPartialLegalFormTranslations($v = true)
    {
        $this->collLegalFormTranslationsPartial = $v;
    }

    /**
     * Initializes the collLegalFormTranslations collection.
     *
     * By default this just sets the collLegalFormTranslations collection to an empty array (like clearcollLegalFormTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initLegalFormTranslations($overrideExisting = true)
    {
        if (null !== $this->collLegalFormTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = LegalFormTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collLegalFormTranslations = new $collectionClassName;
        $this->collLegalFormTranslations->setModel('\Model\Setting\MasterTable\LegalFormTranslation');
    }

    /**
     * Gets an array of ChildLegalFormTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildLegalFormTranslation[] List of ChildLegalFormTranslation objects
     * @throws PropelException
     */
    public function getLegalFormTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collLegalFormTranslationsPartial && !$this->isNew();
        if (null === $this->collLegalFormTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collLegalFormTranslations) {
                    $this->initLegalFormTranslations();
                } else {
                    $collectionClassName = LegalFormTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collLegalFormTranslations = new $collectionClassName;
                    $collLegalFormTranslations->setModel('\Model\Setting\MasterTable\LegalFormTranslation');

                    return $collLegalFormTranslations;
                }
            } else {
                $collLegalFormTranslations = ChildLegalFormTranslationQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collLegalFormTranslationsPartial && count($collLegalFormTranslations)) {
                        $this->initLegalFormTranslations(false);

                        foreach ($collLegalFormTranslations as $obj) {
                            if (false == $this->collLegalFormTranslations->contains($obj)) {
                                $this->collLegalFormTranslations->append($obj);
                            }
                        }

                        $this->collLegalFormTranslationsPartial = true;
                    }

                    return $collLegalFormTranslations;
                }

                if ($partial && $this->collLegalFormTranslations) {
                    foreach ($this->collLegalFormTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collLegalFormTranslations[] = $obj;
                        }
                    }
                }

                $this->collLegalFormTranslations = $collLegalFormTranslations;
                $this->collLegalFormTranslationsPartial = false;
            }
        }

        return $this->collLegalFormTranslations;
    }

    /**
     * Sets a collection of ChildLegalFormTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $legalFormTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setLegalFormTranslations(Collection $legalFormTranslations, ConnectionInterface $con = null)
    {
        /** @var ChildLegalFormTranslation[] $legalFormTranslationsToDelete */
        $legalFormTranslationsToDelete = $this->getLegalFormTranslations(new Criteria(), $con)->diff($legalFormTranslations);


        $this->legalFormTranslationsScheduledForDeletion = $legalFormTranslationsToDelete;

        foreach ($legalFormTranslationsToDelete as $legalFormTranslationRemoved) {
            $legalFormTranslationRemoved->setLanguage(null);
        }

        $this->collLegalFormTranslations = null;
        foreach ($legalFormTranslations as $legalFormTranslation) {
            $this->addLegalFormTranslation($legalFormTranslation);
        }

        $this->collLegalFormTranslations = $legalFormTranslations;
        $this->collLegalFormTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related LegalFormTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related LegalFormTranslation objects.
     * @throws PropelException
     */
    public function countLegalFormTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collLegalFormTranslationsPartial && !$this->isNew();
        if (null === $this->collLegalFormTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collLegalFormTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getLegalFormTranslations());
            }

            $query = ChildLegalFormTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collLegalFormTranslations);
    }

    /**
     * Method called to associate a ChildLegalFormTranslation object to this object
     * through the ChildLegalFormTranslation foreign key attribute.
     *
     * @param  ChildLegalFormTranslation $l ChildLegalFormTranslation
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addLegalFormTranslation(ChildLegalFormTranslation $l)
    {
        if ($this->collLegalFormTranslations === null) {
            $this->initLegalFormTranslations();
            $this->collLegalFormTranslationsPartial = true;
        }

        if (!$this->collLegalFormTranslations->contains($l)) {
            $this->doAddLegalFormTranslation($l);

            if ($this->legalFormTranslationsScheduledForDeletion and $this->legalFormTranslationsScheduledForDeletion->contains($l)) {
                $this->legalFormTranslationsScheduledForDeletion->remove($this->legalFormTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildLegalFormTranslation $legalFormTranslation The ChildLegalFormTranslation object to add.
     */
    protected function doAddLegalFormTranslation(ChildLegalFormTranslation $legalFormTranslation)
    {
        $this->collLegalFormTranslations[]= $legalFormTranslation;
        $legalFormTranslation->setLanguage($this);
    }

    /**
     * @param  ChildLegalFormTranslation $legalFormTranslation The ChildLegalFormTranslation object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeLegalFormTranslation(ChildLegalFormTranslation $legalFormTranslation)
    {
        if ($this->getLegalFormTranslations()->contains($legalFormTranslation)) {
            $pos = $this->collLegalFormTranslations->search($legalFormTranslation);
            $this->collLegalFormTranslations->remove($pos);
            if (null === $this->legalFormTranslationsScheduledForDeletion) {
                $this->legalFormTranslationsScheduledForDeletion = clone $this->collLegalFormTranslations;
                $this->legalFormTranslationsScheduledForDeletion->clear();
            }
            $this->legalFormTranslationsScheduledForDeletion[]= clone $legalFormTranslation;
            $legalFormTranslation->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related LegalFormTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildLegalFormTranslation[] List of ChildLegalFormTranslation objects
     */
    public function getLegalFormTranslationsJoinLegalForm(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildLegalFormTranslationQuery::create(null, $criteria);
        $query->joinWith('LegalForm', $joinBehavior);

        return $this->getLegalFormTranslations($query, $con);
    }

    /**
     * Clears out the collContactMessages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContactMessages()
     */
    public function clearContactMessages()
    {
        $this->collContactMessages = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContactMessages collection loaded partially.
     */
    public function resetPartialContactMessages($v = true)
    {
        $this->collContactMessagesPartial = $v;
    }

    /**
     * Initializes the collContactMessages collection.
     *
     * By default this just sets the collContactMessages collection to an empty array (like clearcollContactMessages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContactMessages($overrideExisting = true)
    {
        if (null !== $this->collContactMessages && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContactMessageTableMap::getTableMap()->getCollectionClassName();

        $this->collContactMessages = new $collectionClassName;
        $this->collContactMessages->setModel('\Model\ContactMessage');
    }

    /**
     * Gets an array of ContactMessage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ContactMessage[] List of ContactMessage objects
     * @throws PropelException
     */
    public function getContactMessages(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContactMessagesPartial && !$this->isNew();
        if (null === $this->collContactMessages || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collContactMessages) {
                    $this->initContactMessages();
                } else {
                    $collectionClassName = ContactMessageTableMap::getTableMap()->getCollectionClassName();

                    $collContactMessages = new $collectionClassName;
                    $collContactMessages->setModel('\Model\ContactMessage');

                    return $collContactMessages;
                }
            } else {
                $collContactMessages = ContactMessageQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContactMessagesPartial && count($collContactMessages)) {
                        $this->initContactMessages(false);

                        foreach ($collContactMessages as $obj) {
                            if (false == $this->collContactMessages->contains($obj)) {
                                $this->collContactMessages->append($obj);
                            }
                        }

                        $this->collContactMessagesPartial = true;
                    }

                    return $collContactMessages;
                }

                if ($partial && $this->collContactMessages) {
                    foreach ($this->collContactMessages as $obj) {
                        if ($obj->isNew()) {
                            $collContactMessages[] = $obj;
                        }
                    }
                }

                $this->collContactMessages = $collContactMessages;
                $this->collContactMessagesPartial = false;
            }
        }

        return $this->collContactMessages;
    }

    /**
     * Sets a collection of ContactMessage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contactMessages A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setContactMessages(Collection $contactMessages, ConnectionInterface $con = null)
    {
        /** @var ContactMessage[] $contactMessagesToDelete */
        $contactMessagesToDelete = $this->getContactMessages(new Criteria(), $con)->diff($contactMessages);


        $this->contactMessagesScheduledForDeletion = $contactMessagesToDelete;

        foreach ($contactMessagesToDelete as $contactMessageRemoved) {
            $contactMessageRemoved->setLanguage(null);
        }

        $this->collContactMessages = null;
        foreach ($contactMessages as $contactMessage) {
            $this->addContactMessage($contactMessage);
        }

        $this->collContactMessages = $contactMessages;
        $this->collContactMessagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseContactMessage objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseContactMessage objects.
     * @throws PropelException
     */
    public function countContactMessages(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContactMessagesPartial && !$this->isNew();
        if (null === $this->collContactMessages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContactMessages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContactMessages());
            }

            $query = ContactMessageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collContactMessages);
    }

    /**
     * Method called to associate a ContactMessage object to this object
     * through the ContactMessage foreign key attribute.
     *
     * @param  ContactMessage $l ContactMessage
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addContactMessage(ContactMessage $l)
    {
        if ($this->collContactMessages === null) {
            $this->initContactMessages();
            $this->collContactMessagesPartial = true;
        }

        if (!$this->collContactMessages->contains($l)) {
            $this->doAddContactMessage($l);

            if ($this->contactMessagesScheduledForDeletion and $this->contactMessagesScheduledForDeletion->contains($l)) {
                $this->contactMessagesScheduledForDeletion->remove($this->contactMessagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ContactMessage $contactMessage The ContactMessage object to add.
     */
    protected function doAddContactMessage(ContactMessage $contactMessage)
    {
        $this->collContactMessages[]= $contactMessage;
        $contactMessage->setLanguage($this);
    }

    /**
     * @param  ContactMessage $contactMessage The ContactMessage object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeContactMessage(ContactMessage $contactMessage)
    {
        if ($this->getContactMessages()->contains($contactMessage)) {
            $pos = $this->collContactMessages->search($contactMessage);
            $this->collContactMessages->remove($pos);
            if (null === $this->contactMessagesScheduledForDeletion) {
                $this->contactMessagesScheduledForDeletion = clone $this->collContactMessages;
                $this->contactMessagesScheduledForDeletion->clear();
            }
            $this->contactMessagesScheduledForDeletion[]= $contactMessage;
            $contactMessage->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related ContactMessages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ContactMessage[] List of ContactMessage objects
     */
    public function getContactMessagesJoinCustomer(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ContactMessageQuery::create(null, $criteria);
        $query->joinWith('Customer', $joinBehavior);

        return $this->getContactMessages($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related ContactMessages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ContactMessage[] List of ContactMessage objects
     */
    public function getContactMessagesJoinContactMessageType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ContactMessageQuery::create(null, $criteria);
        $query->joinWith('ContactMessageType', $joinBehavior);

        return $this->getContactMessages($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related ContactMessages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ContactMessage[] List of ContactMessage objects
     */
    public function getContactMessagesJoinContactMessageStatus(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ContactMessageQuery::create(null, $criteria);
        $query->joinWith('ContactMessageStatus', $joinBehavior);

        return $this->getContactMessages($query, $con);
    }

    /**
     * Clears out the collUsers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsers()
     */
    public function clearUsers()
    {
        $this->collUsers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsers collection loaded partially.
     */
    public function resetPartialUsers($v = true)
    {
        $this->collUsersPartial = $v;
    }

    /**
     * Initializes the collUsers collection.
     *
     * By default this just sets the collUsers collection to an empty array (like clearcollUsers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsers($overrideExisting = true)
    {
        if (null !== $this->collUsers && !$overrideExisting) {
            return;
        }

        $collectionClassName = UserTableMap::getTableMap()->getCollectionClassName();

        $this->collUsers = new $collectionClassName;
        $this->collUsers->setModel('\Model\Account\User');
    }

    /**
     * Gets an array of User objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|User[] List of User objects
     * @throws PropelException
     */
    public function getUsers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsersPartial && !$this->isNew();
        if (null === $this->collUsers || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUsers) {
                    $this->initUsers();
                } else {
                    $collectionClassName = UserTableMap::getTableMap()->getCollectionClassName();

                    $collUsers = new $collectionClassName;
                    $collUsers->setModel('\Model\Account\User');

                    return $collUsers;
                }
            } else {
                $collUsers = UserQuery::create(null, $criteria)
                    ->filterByPreferredLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsersPartial && count($collUsers)) {
                        $this->initUsers(false);

                        foreach ($collUsers as $obj) {
                            if (false == $this->collUsers->contains($obj)) {
                                $this->collUsers->append($obj);
                            }
                        }

                        $this->collUsersPartial = true;
                    }

                    return $collUsers;
                }

                if ($partial && $this->collUsers) {
                    foreach ($this->collUsers as $obj) {
                        if ($obj->isNew()) {
                            $collUsers[] = $obj;
                        }
                    }
                }

                $this->collUsers = $collUsers;
                $this->collUsersPartial = false;
            }
        }

        return $this->collUsers;
    }

    /**
     * Sets a collection of User objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $users A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setUsers(Collection $users, ConnectionInterface $con = null)
    {
        /** @var User[] $usersToDelete */
        $usersToDelete = $this->getUsers(new Criteria(), $con)->diff($users);


        $this->usersScheduledForDeletion = $usersToDelete;

        foreach ($usersToDelete as $userRemoved) {
            $userRemoved->setPreferredLanguage(null);
        }

        $this->collUsers = null;
        foreach ($users as $user) {
            $this->addUser($user);
        }

        $this->collUsers = $users;
        $this->collUsersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseUser objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseUser objects.
     * @throws PropelException
     */
    public function countUsers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsersPartial && !$this->isNew();
        if (null === $this->collUsers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsers());
            }

            $query = UserQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByPreferredLanguage($this)
                ->count($con);
        }

        return count($this->collUsers);
    }

    /**
     * Method called to associate a User object to this object
     * through the User foreign key attribute.
     *
     * @param  User $l User
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addUser(User $l)
    {
        if ($this->collUsers === null) {
            $this->initUsers();
            $this->collUsersPartial = true;
        }

        if (!$this->collUsers->contains($l)) {
            $this->doAddUser($l);

            if ($this->usersScheduledForDeletion and $this->usersScheduledForDeletion->contains($l)) {
                $this->usersScheduledForDeletion->remove($this->usersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param User $user The User object to add.
     */
    protected function doAddUser(User $user)
    {
        $this->collUsers[]= $user;
        $user->setPreferredLanguage($this);
    }

    /**
     * @param  User $user The User object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeUser(User $user)
    {
        if ($this->getUsers()->contains($user)) {
            $pos = $this->collUsers->search($user);
            $this->collUsers->remove($pos);
            if (null === $this->usersScheduledForDeletion) {
                $this->usersScheduledForDeletion = clone $this->collUsers;
                $this->usersScheduledForDeletion->clear();
            }
            $this->usersScheduledForDeletion[]= $user;
            $user->setPreferredLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related Users from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|User[] List of User objects
     */
    public function getUsersJoinRole(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = UserQuery::create(null, $criteria);
        $query->joinWith('Role', $joinBehavior);

        return $this->getUsers($query, $con);
    }

    /**
     * Clears out the collCustomers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomers()
     */
    public function clearCustomers()
    {
        $this->collCustomers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomers collection loaded partially.
     */
    public function resetPartialCustomers($v = true)
    {
        $this->collCustomersPartial = $v;
    }

    /**
     * Initializes the collCustomers collection.
     *
     * By default this just sets the collCustomers collection to an empty array (like clearcollCustomers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomers($overrideExisting = true)
    {
        if (null !== $this->collCustomers && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomerTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomers = new $collectionClassName;
        $this->collCustomers->setModel('\Model\Crm\Customer');
    }

    /**
     * Gets an array of Customer objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Customer[] List of Customer objects
     * @throws PropelException
     */
    public function getCustomers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomersPartial && !$this->isNew();
        if (null === $this->collCustomers || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCustomers) {
                    $this->initCustomers();
                } else {
                    $collectionClassName = CustomerTableMap::getTableMap()->getCollectionClassName();

                    $collCustomers = new $collectionClassName;
                    $collCustomers->setModel('\Model\Crm\Customer');

                    return $collCustomers;
                }
            } else {
                $collCustomers = CustomerQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomersPartial && count($collCustomers)) {
                        $this->initCustomers(false);

                        foreach ($collCustomers as $obj) {
                            if (false == $this->collCustomers->contains($obj)) {
                                $this->collCustomers->append($obj);
                            }
                        }

                        $this->collCustomersPartial = true;
                    }

                    return $collCustomers;
                }

                if ($partial && $this->collCustomers) {
                    foreach ($this->collCustomers as $obj) {
                        if ($obj->isNew()) {
                            $collCustomers[] = $obj;
                        }
                    }
                }

                $this->collCustomers = $collCustomers;
                $this->collCustomersPartial = false;
            }
        }

        return $this->collCustomers;
    }

    /**
     * Sets a collection of Customer objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setCustomers(Collection $customers, ConnectionInterface $con = null)
    {
        /** @var Customer[] $customersToDelete */
        $customersToDelete = $this->getCustomers(new Criteria(), $con)->diff($customers);


        $this->customersScheduledForDeletion = $customersToDelete;

        foreach ($customersToDelete as $customerRemoved) {
            $customerRemoved->setLanguage(null);
        }

        $this->collCustomers = null;
        foreach ($customers as $customer) {
            $this->addCustomer($customer);
        }

        $this->collCustomers = $customers;
        $this->collCustomersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseCustomer objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseCustomer objects.
     * @throws PropelException
     */
    public function countCustomers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomersPartial && !$this->isNew();
        if (null === $this->collCustomers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomers());
            }

            $query = CustomerQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collCustomers);
    }

    /**
     * Method called to associate a Customer object to this object
     * through the Customer foreign key attribute.
     *
     * @param  Customer $l Customer
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addCustomer(Customer $l)
    {
        if ($this->collCustomers === null) {
            $this->initCustomers();
            $this->collCustomersPartial = true;
        }

        if (!$this->collCustomers->contains($l)) {
            $this->doAddCustomer($l);

            if ($this->customersScheduledForDeletion and $this->customersScheduledForDeletion->contains($l)) {
                $this->customersScheduledForDeletion->remove($this->customersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Customer $customer The Customer object to add.
     */
    protected function doAddCustomer(Customer $customer)
    {
        $this->collCustomers[]= $customer;
        $customer->setLanguage($this);
    }

    /**
     * @param  Customer $customer The Customer object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeCustomer(Customer $customer)
    {
        if ($this->getCustomers()->contains($customer)) {
            $pos = $this->collCustomers->search($customer);
            $this->collCustomers->remove($pos);
            if (null === $this->customersScheduledForDeletion) {
                $this->customersScheduledForDeletion = clone $this->collCustomers;
                $this->customersScheduledForDeletion->clear();
            }
            $this->customersScheduledForDeletion[]= $customer;
            $customer->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related Customers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Customer[] List of Customer objects
     */
    public function getCustomersJoinPayTerm(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CustomerQuery::create(null, $criteria);
        $query->joinWith('PayTerm', $joinBehavior);

        return $this->getCustomers($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related Customers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Customer[] List of Customer objects
     */
    public function getCustomersJoinBranche(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CustomerQuery::create(null, $criteria);
        $query->joinWith('Branche', $joinBehavior);

        return $this->getCustomers($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related Customers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Customer[] List of Customer objects
     */
    public function getCustomersJoinCustomerSector(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CustomerQuery::create(null, $criteria);
        $query->joinWith('CustomerSector', $joinBehavior);

        return $this->getCustomers($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related Customers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Customer[] List of Customer objects
     */
    public function getCustomersJoinCustomerType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CustomerQuery::create(null, $criteria);
        $query->joinWith('CustomerType', $joinBehavior);

        return $this->getCustomers($query, $con);
    }

    /**
     * Clears out the collCategoryTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCategoryTranslations()
     */
    public function clearCategoryTranslations()
    {
        $this->collCategoryTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCategoryTranslations collection loaded partially.
     */
    public function resetPartialCategoryTranslations($v = true)
    {
        $this->collCategoryTranslationsPartial = $v;
    }

    /**
     * Initializes the collCategoryTranslations collection.
     *
     * By default this just sets the collCategoryTranslations collection to an empty array (like clearcollCategoryTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCategoryTranslations($overrideExisting = true)
    {
        if (null !== $this->collCategoryTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = CategoryTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collCategoryTranslations = new $collectionClassName;
        $this->collCategoryTranslations->setModel('\Model\Category\CategoryTranslation');
    }

    /**
     * Gets an array of CategoryTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|CategoryTranslation[] List of CategoryTranslation objects
     * @throws PropelException
     */
    public function getCategoryTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCategoryTranslationsPartial && !$this->isNew();
        if (null === $this->collCategoryTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCategoryTranslations) {
                    $this->initCategoryTranslations();
                } else {
                    $collectionClassName = CategoryTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collCategoryTranslations = new $collectionClassName;
                    $collCategoryTranslations->setModel('\Model\Category\CategoryTranslation');

                    return $collCategoryTranslations;
                }
            } else {
                $collCategoryTranslations = CategoryTranslationQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCategoryTranslationsPartial && count($collCategoryTranslations)) {
                        $this->initCategoryTranslations(false);

                        foreach ($collCategoryTranslations as $obj) {
                            if (false == $this->collCategoryTranslations->contains($obj)) {
                                $this->collCategoryTranslations->append($obj);
                            }
                        }

                        $this->collCategoryTranslationsPartial = true;
                    }

                    return $collCategoryTranslations;
                }

                if ($partial && $this->collCategoryTranslations) {
                    foreach ($this->collCategoryTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collCategoryTranslations[] = $obj;
                        }
                    }
                }

                $this->collCategoryTranslations = $collCategoryTranslations;
                $this->collCategoryTranslationsPartial = false;
            }
        }

        return $this->collCategoryTranslations;
    }

    /**
     * Sets a collection of CategoryTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $categoryTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setCategoryTranslations(Collection $categoryTranslations, ConnectionInterface $con = null)
    {
        /** @var CategoryTranslation[] $categoryTranslationsToDelete */
        $categoryTranslationsToDelete = $this->getCategoryTranslations(new Criteria(), $con)->diff($categoryTranslations);


        $this->categoryTranslationsScheduledForDeletion = $categoryTranslationsToDelete;

        foreach ($categoryTranslationsToDelete as $categoryTranslationRemoved) {
            $categoryTranslationRemoved->setLanguage(null);
        }

        $this->collCategoryTranslations = null;
        foreach ($categoryTranslations as $categoryTranslation) {
            $this->addCategoryTranslation($categoryTranslation);
        }

        $this->collCategoryTranslations = $categoryTranslations;
        $this->collCategoryTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseCategoryTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseCategoryTranslation objects.
     * @throws PropelException
     */
    public function countCategoryTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCategoryTranslationsPartial && !$this->isNew();
        if (null === $this->collCategoryTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCategoryTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCategoryTranslations());
            }

            $query = CategoryTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collCategoryTranslations);
    }

    /**
     * Method called to associate a CategoryTranslation object to this object
     * through the CategoryTranslation foreign key attribute.
     *
     * @param  CategoryTranslation $l CategoryTranslation
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addCategoryTranslation(CategoryTranslation $l)
    {
        if ($this->collCategoryTranslations === null) {
            $this->initCategoryTranslations();
            $this->collCategoryTranslationsPartial = true;
        }

        if (!$this->collCategoryTranslations->contains($l)) {
            $this->doAddCategoryTranslation($l);

            if ($this->categoryTranslationsScheduledForDeletion and $this->categoryTranslationsScheduledForDeletion->contains($l)) {
                $this->categoryTranslationsScheduledForDeletion->remove($this->categoryTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param CategoryTranslation $categoryTranslation The CategoryTranslation object to add.
     */
    protected function doAddCategoryTranslation(CategoryTranslation $categoryTranslation)
    {
        $this->collCategoryTranslations[]= $categoryTranslation;
        $categoryTranslation->setLanguage($this);
    }

    /**
     * @param  CategoryTranslation $categoryTranslation The CategoryTranslation object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeCategoryTranslation(CategoryTranslation $categoryTranslation)
    {
        if ($this->getCategoryTranslations()->contains($categoryTranslation)) {
            $pos = $this->collCategoryTranslations->search($categoryTranslation);
            $this->collCategoryTranslations->remove($pos);
            if (null === $this->categoryTranslationsScheduledForDeletion) {
                $this->categoryTranslationsScheduledForDeletion = clone $this->collCategoryTranslations;
                $this->categoryTranslationsScheduledForDeletion->clear();
            }
            $this->categoryTranslationsScheduledForDeletion[]= clone $categoryTranslation;
            $categoryTranslation->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related CategoryTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|CategoryTranslation[] List of CategoryTranslation objects
     */
    public function getCategoryTranslationsJoinCategory(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CategoryTranslationQuery::create(null, $criteria);
        $query->joinWith('Category', $joinBehavior);

        return $this->getCategoryTranslations($query, $con);
    }

    /**
     * Clears out the collPromoProductTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPromoProductTranslations()
     */
    public function clearPromoProductTranslations()
    {
        $this->collPromoProductTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPromoProductTranslations collection loaded partially.
     */
    public function resetPartialPromoProductTranslations($v = true)
    {
        $this->collPromoProductTranslationsPartial = $v;
    }

    /**
     * Initializes the collPromoProductTranslations collection.
     *
     * By default this just sets the collPromoProductTranslations collection to an empty array (like clearcollPromoProductTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPromoProductTranslations($overrideExisting = true)
    {
        if (null !== $this->collPromoProductTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = PromoProductTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collPromoProductTranslations = new $collectionClassName;
        $this->collPromoProductTranslations->setModel('\Model\PromoProductTranslation');
    }

    /**
     * Gets an array of PromoProductTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|PromoProductTranslation[] List of PromoProductTranslation objects
     * @throws PropelException
     */
    public function getPromoProductTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPromoProductTranslationsPartial && !$this->isNew();
        if (null === $this->collPromoProductTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPromoProductTranslations) {
                    $this->initPromoProductTranslations();
                } else {
                    $collectionClassName = PromoProductTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collPromoProductTranslations = new $collectionClassName;
                    $collPromoProductTranslations->setModel('\Model\PromoProductTranslation');

                    return $collPromoProductTranslations;
                }
            } else {
                $collPromoProductTranslations = PromoProductTranslationQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPromoProductTranslationsPartial && count($collPromoProductTranslations)) {
                        $this->initPromoProductTranslations(false);

                        foreach ($collPromoProductTranslations as $obj) {
                            if (false == $this->collPromoProductTranslations->contains($obj)) {
                                $this->collPromoProductTranslations->append($obj);
                            }
                        }

                        $this->collPromoProductTranslationsPartial = true;
                    }

                    return $collPromoProductTranslations;
                }

                if ($partial && $this->collPromoProductTranslations) {
                    foreach ($this->collPromoProductTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collPromoProductTranslations[] = $obj;
                        }
                    }
                }

                $this->collPromoProductTranslations = $collPromoProductTranslations;
                $this->collPromoProductTranslationsPartial = false;
            }
        }

        return $this->collPromoProductTranslations;
    }

    /**
     * Sets a collection of PromoProductTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $promoProductTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setPromoProductTranslations(Collection $promoProductTranslations, ConnectionInterface $con = null)
    {
        /** @var PromoProductTranslation[] $promoProductTranslationsToDelete */
        $promoProductTranslationsToDelete = $this->getPromoProductTranslations(new Criteria(), $con)->diff($promoProductTranslations);


        $this->promoProductTranslationsScheduledForDeletion = $promoProductTranslationsToDelete;

        foreach ($promoProductTranslationsToDelete as $promoProductTranslationRemoved) {
            $promoProductTranslationRemoved->setLanguage(null);
        }

        $this->collPromoProductTranslations = null;
        foreach ($promoProductTranslations as $promoProductTranslation) {
            $this->addPromoProductTranslation($promoProductTranslation);
        }

        $this->collPromoProductTranslations = $promoProductTranslations;
        $this->collPromoProductTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BasePromoProductTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BasePromoProductTranslation objects.
     * @throws PropelException
     */
    public function countPromoProductTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPromoProductTranslationsPartial && !$this->isNew();
        if (null === $this->collPromoProductTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPromoProductTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPromoProductTranslations());
            }

            $query = PromoProductTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collPromoProductTranslations);
    }

    /**
     * Method called to associate a PromoProductTranslation object to this object
     * through the PromoProductTranslation foreign key attribute.
     *
     * @param  PromoProductTranslation $l PromoProductTranslation
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addPromoProductTranslation(PromoProductTranslation $l)
    {
        if ($this->collPromoProductTranslations === null) {
            $this->initPromoProductTranslations();
            $this->collPromoProductTranslationsPartial = true;
        }

        if (!$this->collPromoProductTranslations->contains($l)) {
            $this->doAddPromoProductTranslation($l);

            if ($this->promoProductTranslationsScheduledForDeletion and $this->promoProductTranslationsScheduledForDeletion->contains($l)) {
                $this->promoProductTranslationsScheduledForDeletion->remove($this->promoProductTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param PromoProductTranslation $promoProductTranslation The PromoProductTranslation object to add.
     */
    protected function doAddPromoProductTranslation(PromoProductTranslation $promoProductTranslation)
    {
        $this->collPromoProductTranslations[]= $promoProductTranslation;
        $promoProductTranslation->setLanguage($this);
    }

    /**
     * @param  PromoProductTranslation $promoProductTranslation The PromoProductTranslation object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removePromoProductTranslation(PromoProductTranslation $promoProductTranslation)
    {
        if ($this->getPromoProductTranslations()->contains($promoProductTranslation)) {
            $pos = $this->collPromoProductTranslations->search($promoProductTranslation);
            $this->collPromoProductTranslations->remove($pos);
            if (null === $this->promoProductTranslationsScheduledForDeletion) {
                $this->promoProductTranslationsScheduledForDeletion = clone $this->collPromoProductTranslations;
                $this->promoProductTranslationsScheduledForDeletion->clear();
            }
            $this->promoProductTranslationsScheduledForDeletion[]= $promoProductTranslation;
            $promoProductTranslation->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related PromoProductTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|PromoProductTranslation[] List of PromoProductTranslation objects
     */
    public function getPromoProductTranslationsJoinPromoProduct(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = PromoProductTranslationQuery::create(null, $criteria);
        $query->joinWith('PromoProduct', $joinBehavior);

        return $this->getPromoProductTranslations($query, $con);
    }

    /**
     * Clears out the collProductTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductTranslations()
     */
    public function clearProductTranslations()
    {
        $this->collProductTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductTranslations collection loaded partially.
     */
    public function resetPartialProductTranslations($v = true)
    {
        $this->collProductTranslationsPartial = $v;
    }

    /**
     * Initializes the collProductTranslations collection.
     *
     * By default this just sets the collProductTranslations collection to an empty array (like clearcollProductTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductTranslations($overrideExisting = true)
    {
        if (null !== $this->collProductTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collProductTranslations = new $collectionClassName;
        $this->collProductTranslations->setModel('\Model\ProductTranslation');
    }

    /**
     * Gets an array of ProductTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ProductTranslation[] List of ProductTranslation objects
     * @throws PropelException
     */
    public function getProductTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductTranslationsPartial && !$this->isNew();
        if (null === $this->collProductTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductTranslations) {
                    $this->initProductTranslations();
                } else {
                    $collectionClassName = ProductTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collProductTranslations = new $collectionClassName;
                    $collProductTranslations->setModel('\Model\ProductTranslation');

                    return $collProductTranslations;
                }
            } else {
                $collProductTranslations = ProductTranslationQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductTranslationsPartial && count($collProductTranslations)) {
                        $this->initProductTranslations(false);

                        foreach ($collProductTranslations as $obj) {
                            if (false == $this->collProductTranslations->contains($obj)) {
                                $this->collProductTranslations->append($obj);
                            }
                        }

                        $this->collProductTranslationsPartial = true;
                    }

                    return $collProductTranslations;
                }

                if ($partial && $this->collProductTranslations) {
                    foreach ($this->collProductTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collProductTranslations[] = $obj;
                        }
                    }
                }

                $this->collProductTranslations = $collProductTranslations;
                $this->collProductTranslationsPartial = false;
            }
        }

        return $this->collProductTranslations;
    }

    /**
     * Sets a collection of ProductTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setProductTranslations(Collection $productTranslations, ConnectionInterface $con = null)
    {
        /** @var ProductTranslation[] $productTranslationsToDelete */
        $productTranslationsToDelete = $this->getProductTranslations(new Criteria(), $con)->diff($productTranslations);


        $this->productTranslationsScheduledForDeletion = $productTranslationsToDelete;

        foreach ($productTranslationsToDelete as $productTranslationRemoved) {
            $productTranslationRemoved->setLanguage(null);
        }

        $this->collProductTranslations = null;
        foreach ($productTranslations as $productTranslation) {
            $this->addProductTranslation($productTranslation);
        }

        $this->collProductTranslations = $productTranslations;
        $this->collProductTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseProductTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseProductTranslation objects.
     * @throws PropelException
     */
    public function countProductTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductTranslationsPartial && !$this->isNew();
        if (null === $this->collProductTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductTranslations());
            }

            $query = ProductTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collProductTranslations);
    }

    /**
     * Method called to associate a ProductTranslation object to this object
     * through the ProductTranslation foreign key attribute.
     *
     * @param  ProductTranslation $l ProductTranslation
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addProductTranslation(ProductTranslation $l)
    {
        if ($this->collProductTranslations === null) {
            $this->initProductTranslations();
            $this->collProductTranslationsPartial = true;
        }

        if (!$this->collProductTranslations->contains($l)) {
            $this->doAddProductTranslation($l);

            if ($this->productTranslationsScheduledForDeletion and $this->productTranslationsScheduledForDeletion->contains($l)) {
                $this->productTranslationsScheduledForDeletion->remove($this->productTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ProductTranslation $productTranslation The ProductTranslation object to add.
     */
    protected function doAddProductTranslation(ProductTranslation $productTranslation)
    {
        $this->collProductTranslations[]= $productTranslation;
        $productTranslation->setLanguage($this);
    }

    /**
     * @param  ProductTranslation $productTranslation The ProductTranslation object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeProductTranslation(ProductTranslation $productTranslation)
    {
        if ($this->getProductTranslations()->contains($productTranslation)) {
            $pos = $this->collProductTranslations->search($productTranslation);
            $this->collProductTranslations->remove($pos);
            if (null === $this->productTranslationsScheduledForDeletion) {
                $this->productTranslationsScheduledForDeletion = clone $this->collProductTranslations;
                $this->productTranslationsScheduledForDeletion->clear();
            }
            $this->productTranslationsScheduledForDeletion[]= clone $productTranslation;
            $productTranslation->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related ProductTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ProductTranslation[] List of ProductTranslation objects
     */
    public function getProductTranslationsJoinProduct(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductTranslationQuery::create(null, $criteria);
        $query->joinWith('Product', $joinBehavior);

        return $this->getProductTranslations($query, $con);
    }

    /**
     * Clears out the collVatTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addVatTranslations()
     */
    public function clearVatTranslations()
    {
        $this->collVatTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collVatTranslations collection loaded partially.
     */
    public function resetPartialVatTranslations($v = true)
    {
        $this->collVatTranslationsPartial = $v;
    }

    /**
     * Initializes the collVatTranslations collection.
     *
     * By default this just sets the collVatTranslations collection to an empty array (like clearcollVatTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initVatTranslations($overrideExisting = true)
    {
        if (null !== $this->collVatTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = VatTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collVatTranslations = new $collectionClassName;
        $this->collVatTranslations->setModel('\Model\Setting\MasterTable\VatTranslation');
    }

    /**
     * Gets an array of ChildVatTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildVatTranslation[] List of ChildVatTranslation objects
     * @throws PropelException
     */
    public function getVatTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collVatTranslationsPartial && !$this->isNew();
        if (null === $this->collVatTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collVatTranslations) {
                    $this->initVatTranslations();
                } else {
                    $collectionClassName = VatTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collVatTranslations = new $collectionClassName;
                    $collVatTranslations->setModel('\Model\Setting\MasterTable\VatTranslation');

                    return $collVatTranslations;
                }
            } else {
                $collVatTranslations = ChildVatTranslationQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collVatTranslationsPartial && count($collVatTranslations)) {
                        $this->initVatTranslations(false);

                        foreach ($collVatTranslations as $obj) {
                            if (false == $this->collVatTranslations->contains($obj)) {
                                $this->collVatTranslations->append($obj);
                            }
                        }

                        $this->collVatTranslationsPartial = true;
                    }

                    return $collVatTranslations;
                }

                if ($partial && $this->collVatTranslations) {
                    foreach ($this->collVatTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collVatTranslations[] = $obj;
                        }
                    }
                }

                $this->collVatTranslations = $collVatTranslations;
                $this->collVatTranslationsPartial = false;
            }
        }

        return $this->collVatTranslations;
    }

    /**
     * Sets a collection of ChildVatTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $vatTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setVatTranslations(Collection $vatTranslations, ConnectionInterface $con = null)
    {
        /** @var ChildVatTranslation[] $vatTranslationsToDelete */
        $vatTranslationsToDelete = $this->getVatTranslations(new Criteria(), $con)->diff($vatTranslations);


        $this->vatTranslationsScheduledForDeletion = $vatTranslationsToDelete;

        foreach ($vatTranslationsToDelete as $vatTranslationRemoved) {
            $vatTranslationRemoved->setLanguage(null);
        }

        $this->collVatTranslations = null;
        foreach ($vatTranslations as $vatTranslation) {
            $this->addVatTranslation($vatTranslation);
        }

        $this->collVatTranslations = $vatTranslations;
        $this->collVatTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related VatTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related VatTranslation objects.
     * @throws PropelException
     */
    public function countVatTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collVatTranslationsPartial && !$this->isNew();
        if (null === $this->collVatTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collVatTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getVatTranslations());
            }

            $query = ChildVatTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collVatTranslations);
    }

    /**
     * Method called to associate a ChildVatTranslation object to this object
     * through the ChildVatTranslation foreign key attribute.
     *
     * @param  ChildVatTranslation $l ChildVatTranslation
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addVatTranslation(ChildVatTranslation $l)
    {
        if ($this->collVatTranslations === null) {
            $this->initVatTranslations();
            $this->collVatTranslationsPartial = true;
        }

        if (!$this->collVatTranslations->contains($l)) {
            $this->doAddVatTranslation($l);

            if ($this->vatTranslationsScheduledForDeletion and $this->vatTranslationsScheduledForDeletion->contains($l)) {
                $this->vatTranslationsScheduledForDeletion->remove($this->vatTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildVatTranslation $vatTranslation The ChildVatTranslation object to add.
     */
    protected function doAddVatTranslation(ChildVatTranslation $vatTranslation)
    {
        $this->collVatTranslations[]= $vatTranslation;
        $vatTranslation->setLanguage($this);
    }

    /**
     * @param  ChildVatTranslation $vatTranslation The ChildVatTranslation object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeVatTranslation(ChildVatTranslation $vatTranslation)
    {
        if ($this->getVatTranslations()->contains($vatTranslation)) {
            $pos = $this->collVatTranslations->search($vatTranslation);
            $this->collVatTranslations->remove($pos);
            if (null === $this->vatTranslationsScheduledForDeletion) {
                $this->vatTranslationsScheduledForDeletion = clone $this->collVatTranslations;
                $this->vatTranslationsScheduledForDeletion->clear();
            }
            $this->vatTranslationsScheduledForDeletion[]= clone $vatTranslation;
            $vatTranslation->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related VatTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildVatTranslation[] List of ChildVatTranslation objects
     */
    public function getVatTranslationsJoinVat(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildVatTranslationQuery::create(null, $criteria);
        $query->joinWith('Vat', $joinBehavior);

        return $this->getVatTranslations($query, $con);
    }

    /**
     * Clears out the collUsageTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsageTranslations()
     */
    public function clearUsageTranslations()
    {
        $this->collUsageTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsageTranslations collection loaded partially.
     */
    public function resetPartialUsageTranslations($v = true)
    {
        $this->collUsageTranslationsPartial = $v;
    }

    /**
     * Initializes the collUsageTranslations collection.
     *
     * By default this just sets the collUsageTranslations collection to an empty array (like clearcollUsageTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsageTranslations($overrideExisting = true)
    {
        if (null !== $this->collUsageTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsageTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collUsageTranslations = new $collectionClassName;
        $this->collUsageTranslations->setModel('\Model\Setting\MasterTable\UsageTranslation');
    }

    /**
     * Gets an array of ChildUsageTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsageTranslation[] List of ChildUsageTranslation objects
     * @throws PropelException
     */
    public function getUsageTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsageTranslationsPartial && !$this->isNew();
        if (null === $this->collUsageTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUsageTranslations) {
                    $this->initUsageTranslations();
                } else {
                    $collectionClassName = UsageTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collUsageTranslations = new $collectionClassName;
                    $collUsageTranslations->setModel('\Model\Setting\MasterTable\UsageTranslation');

                    return $collUsageTranslations;
                }
            } else {
                $collUsageTranslations = ChildUsageTranslationQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsageTranslationsPartial && count($collUsageTranslations)) {
                        $this->initUsageTranslations(false);

                        foreach ($collUsageTranslations as $obj) {
                            if (false == $this->collUsageTranslations->contains($obj)) {
                                $this->collUsageTranslations->append($obj);
                            }
                        }

                        $this->collUsageTranslationsPartial = true;
                    }

                    return $collUsageTranslations;
                }

                if ($partial && $this->collUsageTranslations) {
                    foreach ($this->collUsageTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collUsageTranslations[] = $obj;
                        }
                    }
                }

                $this->collUsageTranslations = $collUsageTranslations;
                $this->collUsageTranslationsPartial = false;
            }
        }

        return $this->collUsageTranslations;
    }

    /**
     * Sets a collection of ChildUsageTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usageTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setUsageTranslations(Collection $usageTranslations, ConnectionInterface $con = null)
    {
        /** @var ChildUsageTranslation[] $usageTranslationsToDelete */
        $usageTranslationsToDelete = $this->getUsageTranslations(new Criteria(), $con)->diff($usageTranslations);


        $this->usageTranslationsScheduledForDeletion = $usageTranslationsToDelete;

        foreach ($usageTranslationsToDelete as $usageTranslationRemoved) {
            $usageTranslationRemoved->setLanguage(null);
        }

        $this->collUsageTranslations = null;
        foreach ($usageTranslations as $usageTranslation) {
            $this->addUsageTranslation($usageTranslation);
        }

        $this->collUsageTranslations = $usageTranslations;
        $this->collUsageTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsageTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsageTranslation objects.
     * @throws PropelException
     */
    public function countUsageTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsageTranslationsPartial && !$this->isNew();
        if (null === $this->collUsageTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsageTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsageTranslations());
            }

            $query = ChildUsageTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collUsageTranslations);
    }

    /**
     * Method called to associate a ChildUsageTranslation object to this object
     * through the ChildUsageTranslation foreign key attribute.
     *
     * @param  ChildUsageTranslation $l ChildUsageTranslation
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addUsageTranslation(ChildUsageTranslation $l)
    {
        if ($this->collUsageTranslations === null) {
            $this->initUsageTranslations();
            $this->collUsageTranslationsPartial = true;
        }

        if (!$this->collUsageTranslations->contains($l)) {
            $this->doAddUsageTranslation($l);

            if ($this->usageTranslationsScheduledForDeletion and $this->usageTranslationsScheduledForDeletion->contains($l)) {
                $this->usageTranslationsScheduledForDeletion->remove($this->usageTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsageTranslation $usageTranslation The ChildUsageTranslation object to add.
     */
    protected function doAddUsageTranslation(ChildUsageTranslation $usageTranslation)
    {
        $this->collUsageTranslations[]= $usageTranslation;
        $usageTranslation->setLanguage($this);
    }

    /**
     * @param  ChildUsageTranslation $usageTranslation The ChildUsageTranslation object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeUsageTranslation(ChildUsageTranslation $usageTranslation)
    {
        if ($this->getUsageTranslations()->contains($usageTranslation)) {
            $pos = $this->collUsageTranslations->search($usageTranslation);
            $this->collUsageTranslations->remove($pos);
            if (null === $this->usageTranslationsScheduledForDeletion) {
                $this->usageTranslationsScheduledForDeletion = clone $this->collUsageTranslations;
                $this->usageTranslationsScheduledForDeletion->clear();
            }
            $this->usageTranslationsScheduledForDeletion[]= $usageTranslation;
            $usageTranslation->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related UsageTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsageTranslation[] List of ChildUsageTranslation objects
     */
    public function getUsageTranslationsJoinUsage(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsageTranslationQuery::create(null, $criteria);
        $query->joinWith('Usage', $joinBehavior);

        return $this->getUsageTranslations($query, $con);
    }

    /**
     * Clears out the collColorTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addColorTranslations()
     */
    public function clearColorTranslations()
    {
        $this->collColorTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collColorTranslations collection loaded partially.
     */
    public function resetPartialColorTranslations($v = true)
    {
        $this->collColorTranslationsPartial = $v;
    }

    /**
     * Initializes the collColorTranslations collection.
     *
     * By default this just sets the collColorTranslations collection to an empty array (like clearcollColorTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initColorTranslations($overrideExisting = true)
    {
        if (null !== $this->collColorTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = ColorTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collColorTranslations = new $collectionClassName;
        $this->collColorTranslations->setModel('\Model\Setting\MasterTable\ColorTranslation');
    }

    /**
     * Gets an array of ChildColorTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildColorTranslation[] List of ChildColorTranslation objects
     * @throws PropelException
     */
    public function getColorTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collColorTranslationsPartial && !$this->isNew();
        if (null === $this->collColorTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collColorTranslations) {
                    $this->initColorTranslations();
                } else {
                    $collectionClassName = ColorTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collColorTranslations = new $collectionClassName;
                    $collColorTranslations->setModel('\Model\Setting\MasterTable\ColorTranslation');

                    return $collColorTranslations;
                }
            } else {
                $collColorTranslations = ChildColorTranslationQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collColorTranslationsPartial && count($collColorTranslations)) {
                        $this->initColorTranslations(false);

                        foreach ($collColorTranslations as $obj) {
                            if (false == $this->collColorTranslations->contains($obj)) {
                                $this->collColorTranslations->append($obj);
                            }
                        }

                        $this->collColorTranslationsPartial = true;
                    }

                    return $collColorTranslations;
                }

                if ($partial && $this->collColorTranslations) {
                    foreach ($this->collColorTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collColorTranslations[] = $obj;
                        }
                    }
                }

                $this->collColorTranslations = $collColorTranslations;
                $this->collColorTranslationsPartial = false;
            }
        }

        return $this->collColorTranslations;
    }

    /**
     * Sets a collection of ChildColorTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $colorTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setColorTranslations(Collection $colorTranslations, ConnectionInterface $con = null)
    {
        /** @var ChildColorTranslation[] $colorTranslationsToDelete */
        $colorTranslationsToDelete = $this->getColorTranslations(new Criteria(), $con)->diff($colorTranslations);


        $this->colorTranslationsScheduledForDeletion = $colorTranslationsToDelete;

        foreach ($colorTranslationsToDelete as $colorTranslationRemoved) {
            $colorTranslationRemoved->setLanguage(null);
        }

        $this->collColorTranslations = null;
        foreach ($colorTranslations as $colorTranslation) {
            $this->addColorTranslation($colorTranslation);
        }

        $this->collColorTranslations = $colorTranslations;
        $this->collColorTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ColorTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ColorTranslation objects.
     * @throws PropelException
     */
    public function countColorTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collColorTranslationsPartial && !$this->isNew();
        if (null === $this->collColorTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collColorTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getColorTranslations());
            }

            $query = ChildColorTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collColorTranslations);
    }

    /**
     * Method called to associate a ChildColorTranslation object to this object
     * through the ChildColorTranslation foreign key attribute.
     *
     * @param  ChildColorTranslation $l ChildColorTranslation
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addColorTranslation(ChildColorTranslation $l)
    {
        if ($this->collColorTranslations === null) {
            $this->initColorTranslations();
            $this->collColorTranslationsPartial = true;
        }

        if (!$this->collColorTranslations->contains($l)) {
            $this->doAddColorTranslation($l);

            if ($this->colorTranslationsScheduledForDeletion and $this->colorTranslationsScheduledForDeletion->contains($l)) {
                $this->colorTranslationsScheduledForDeletion->remove($this->colorTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildColorTranslation $colorTranslation The ChildColorTranslation object to add.
     */
    protected function doAddColorTranslation(ChildColorTranslation $colorTranslation)
    {
        $this->collColorTranslations[]= $colorTranslation;
        $colorTranslation->setLanguage($this);
    }

    /**
     * @param  ChildColorTranslation $colorTranslation The ChildColorTranslation object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeColorTranslation(ChildColorTranslation $colorTranslation)
    {
        if ($this->getColorTranslations()->contains($colorTranslation)) {
            $pos = $this->collColorTranslations->search($colorTranslation);
            $this->collColorTranslations->remove($pos);
            if (null === $this->colorTranslationsScheduledForDeletion) {
                $this->colorTranslationsScheduledForDeletion = clone $this->collColorTranslations;
                $this->colorTranslationsScheduledForDeletion->clear();
            }
            $this->colorTranslationsScheduledForDeletion[]= $colorTranslation;
            $colorTranslation->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related ColorTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildColorTranslation[] List of ChildColorTranslation objects
     */
    public function getColorTranslationsJoinColor(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildColorTranslationQuery::create(null, $criteria);
        $query->joinWith('Color', $joinBehavior);

        return $this->getColorTranslations($query, $con);
    }

    /**
     * Clears out the collSiteUsps collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteUsps()
     */
    public function clearSiteUsps()
    {
        $this->collSiteUsps = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteUsps collection loaded partially.
     */
    public function resetPartialSiteUsps($v = true)
    {
        $this->collSiteUspsPartial = $v;
    }

    /**
     * Initializes the collSiteUsps collection.
     *
     * By default this just sets the collSiteUsps collection to an empty array (like clearcollSiteUsps());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteUsps($overrideExisting = true)
    {
        if (null !== $this->collSiteUsps && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteUspTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteUsps = new $collectionClassName;
        $this->collSiteUsps->setModel('\Model\Cms\SiteUsp');
    }

    /**
     * Gets an array of SiteUsp objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|SiteUsp[] List of SiteUsp objects
     * @throws PropelException
     */
    public function getSiteUsps(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteUspsPartial && !$this->isNew();
        if (null === $this->collSiteUsps || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteUsps) {
                    $this->initSiteUsps();
                } else {
                    $collectionClassName = SiteUspTableMap::getTableMap()->getCollectionClassName();

                    $collSiteUsps = new $collectionClassName;
                    $collSiteUsps->setModel('\Model\Cms\SiteUsp');

                    return $collSiteUsps;
                }
            } else {
                $collSiteUsps = SiteUspQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteUspsPartial && count($collSiteUsps)) {
                        $this->initSiteUsps(false);

                        foreach ($collSiteUsps as $obj) {
                            if (false == $this->collSiteUsps->contains($obj)) {
                                $this->collSiteUsps->append($obj);
                            }
                        }

                        $this->collSiteUspsPartial = true;
                    }

                    return $collSiteUsps;
                }

                if ($partial && $this->collSiteUsps) {
                    foreach ($this->collSiteUsps as $obj) {
                        if ($obj->isNew()) {
                            $collSiteUsps[] = $obj;
                        }
                    }
                }

                $this->collSiteUsps = $collSiteUsps;
                $this->collSiteUspsPartial = false;
            }
        }

        return $this->collSiteUsps;
    }

    /**
     * Sets a collection of SiteUsp objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteUsps A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setSiteUsps(Collection $siteUsps, ConnectionInterface $con = null)
    {
        /** @var SiteUsp[] $siteUspsToDelete */
        $siteUspsToDelete = $this->getSiteUsps(new Criteria(), $con)->diff($siteUsps);


        $this->siteUspsScheduledForDeletion = $siteUspsToDelete;

        foreach ($siteUspsToDelete as $siteUspRemoved) {
            $siteUspRemoved->setLanguage(null);
        }

        $this->collSiteUsps = null;
        foreach ($siteUsps as $siteUsp) {
            $this->addSiteUsp($siteUsp);
        }

        $this->collSiteUsps = $siteUsps;
        $this->collSiteUspsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSiteUsp objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSiteUsp objects.
     * @throws PropelException
     */
    public function countSiteUsps(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteUspsPartial && !$this->isNew();
        if (null === $this->collSiteUsps || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteUsps) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteUsps());
            }

            $query = SiteUspQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collSiteUsps);
    }

    /**
     * Method called to associate a SiteUsp object to this object
     * through the SiteUsp foreign key attribute.
     *
     * @param  SiteUsp $l SiteUsp
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addSiteUsp(SiteUsp $l)
    {
        if ($this->collSiteUsps === null) {
            $this->initSiteUsps();
            $this->collSiteUspsPartial = true;
        }

        if (!$this->collSiteUsps->contains($l)) {
            $this->doAddSiteUsp($l);

            if ($this->siteUspsScheduledForDeletion and $this->siteUspsScheduledForDeletion->contains($l)) {
                $this->siteUspsScheduledForDeletion->remove($this->siteUspsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param SiteUsp $siteUsp The SiteUsp object to add.
     */
    protected function doAddSiteUsp(SiteUsp $siteUsp)
    {
        $this->collSiteUsps[]= $siteUsp;
        $siteUsp->setLanguage($this);
    }

    /**
     * @param  SiteUsp $siteUsp The SiteUsp object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeSiteUsp(SiteUsp $siteUsp)
    {
        if ($this->getSiteUsps()->contains($siteUsp)) {
            $pos = $this->collSiteUsps->search($siteUsp);
            $this->collSiteUsps->remove($pos);
            if (null === $this->siteUspsScheduledForDeletion) {
                $this->siteUspsScheduledForDeletion = clone $this->collSiteUsps;
                $this->siteUspsScheduledForDeletion->clear();
            }
            $this->siteUspsScheduledForDeletion[]= $siteUsp;
            $siteUsp->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related SiteUsps from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SiteUsp[] List of SiteUsp objects
     */
    public function getSiteUspsJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SiteUspQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getSiteUsps($query, $con);
    }

    /**
     * Clears out the collSiteFooterBlockTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteFooterBlockTranslations()
     */
    public function clearSiteFooterBlockTranslations()
    {
        $this->collSiteFooterBlockTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteFooterBlockTranslations collection loaded partially.
     */
    public function resetPartialSiteFooterBlockTranslations($v = true)
    {
        $this->collSiteFooterBlockTranslationsPartial = $v;
    }

    /**
     * Initializes the collSiteFooterBlockTranslations collection.
     *
     * By default this just sets the collSiteFooterBlockTranslations collection to an empty array (like clearcollSiteFooterBlockTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteFooterBlockTranslations($overrideExisting = true)
    {
        if (null !== $this->collSiteFooterBlockTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteFooterBlockTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteFooterBlockTranslations = new $collectionClassName;
        $this->collSiteFooterBlockTranslations->setModel('\Model\Cms\SiteFooterBlockTranslation');
    }

    /**
     * Gets an array of SiteFooterBlockTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|SiteFooterBlockTranslation[] List of SiteFooterBlockTranslation objects
     * @throws PropelException
     */
    public function getSiteFooterBlockTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFooterBlockTranslationsPartial && !$this->isNew();
        if (null === $this->collSiteFooterBlockTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteFooterBlockTranslations) {
                    $this->initSiteFooterBlockTranslations();
                } else {
                    $collectionClassName = SiteFooterBlockTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collSiteFooterBlockTranslations = new $collectionClassName;
                    $collSiteFooterBlockTranslations->setModel('\Model\Cms\SiteFooterBlockTranslation');

                    return $collSiteFooterBlockTranslations;
                }
            } else {
                $collSiteFooterBlockTranslations = SiteFooterBlockTranslationQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteFooterBlockTranslationsPartial && count($collSiteFooterBlockTranslations)) {
                        $this->initSiteFooterBlockTranslations(false);

                        foreach ($collSiteFooterBlockTranslations as $obj) {
                            if (false == $this->collSiteFooterBlockTranslations->contains($obj)) {
                                $this->collSiteFooterBlockTranslations->append($obj);
                            }
                        }

                        $this->collSiteFooterBlockTranslationsPartial = true;
                    }

                    return $collSiteFooterBlockTranslations;
                }

                if ($partial && $this->collSiteFooterBlockTranslations) {
                    foreach ($this->collSiteFooterBlockTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collSiteFooterBlockTranslations[] = $obj;
                        }
                    }
                }

                $this->collSiteFooterBlockTranslations = $collSiteFooterBlockTranslations;
                $this->collSiteFooterBlockTranslationsPartial = false;
            }
        }

        return $this->collSiteFooterBlockTranslations;
    }

    /**
     * Sets a collection of SiteFooterBlockTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteFooterBlockTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setSiteFooterBlockTranslations(Collection $siteFooterBlockTranslations, ConnectionInterface $con = null)
    {
        /** @var SiteFooterBlockTranslation[] $siteFooterBlockTranslationsToDelete */
        $siteFooterBlockTranslationsToDelete = $this->getSiteFooterBlockTranslations(new Criteria(), $con)->diff($siteFooterBlockTranslations);


        $this->siteFooterBlockTranslationsScheduledForDeletion = $siteFooterBlockTranslationsToDelete;

        foreach ($siteFooterBlockTranslationsToDelete as $siteFooterBlockTranslationRemoved) {
            $siteFooterBlockTranslationRemoved->setLanguage(null);
        }

        $this->collSiteFooterBlockTranslations = null;
        foreach ($siteFooterBlockTranslations as $siteFooterBlockTranslation) {
            $this->addSiteFooterBlockTranslation($siteFooterBlockTranslation);
        }

        $this->collSiteFooterBlockTranslations = $siteFooterBlockTranslations;
        $this->collSiteFooterBlockTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSiteFooterBlockTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSiteFooterBlockTranslation objects.
     * @throws PropelException
     */
    public function countSiteFooterBlockTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFooterBlockTranslationsPartial && !$this->isNew();
        if (null === $this->collSiteFooterBlockTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteFooterBlockTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteFooterBlockTranslations());
            }

            $query = SiteFooterBlockTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collSiteFooterBlockTranslations);
    }

    /**
     * Method called to associate a SiteFooterBlockTranslation object to this object
     * through the SiteFooterBlockTranslation foreign key attribute.
     *
     * @param  SiteFooterBlockTranslation $l SiteFooterBlockTranslation
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addSiteFooterBlockTranslation(SiteFooterBlockTranslation $l)
    {
        if ($this->collSiteFooterBlockTranslations === null) {
            $this->initSiteFooterBlockTranslations();
            $this->collSiteFooterBlockTranslationsPartial = true;
        }

        if (!$this->collSiteFooterBlockTranslations->contains($l)) {
            $this->doAddSiteFooterBlockTranslation($l);

            if ($this->siteFooterBlockTranslationsScheduledForDeletion and $this->siteFooterBlockTranslationsScheduledForDeletion->contains($l)) {
                $this->siteFooterBlockTranslationsScheduledForDeletion->remove($this->siteFooterBlockTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param SiteFooterBlockTranslation $siteFooterBlockTranslation The SiteFooterBlockTranslation object to add.
     */
    protected function doAddSiteFooterBlockTranslation(SiteFooterBlockTranslation $siteFooterBlockTranslation)
    {
        $this->collSiteFooterBlockTranslations[]= $siteFooterBlockTranslation;
        $siteFooterBlockTranslation->setLanguage($this);
    }

    /**
     * @param  SiteFooterBlockTranslation $siteFooterBlockTranslation The SiteFooterBlockTranslation object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeSiteFooterBlockTranslation(SiteFooterBlockTranslation $siteFooterBlockTranslation)
    {
        if ($this->getSiteFooterBlockTranslations()->contains($siteFooterBlockTranslation)) {
            $pos = $this->collSiteFooterBlockTranslations->search($siteFooterBlockTranslation);
            $this->collSiteFooterBlockTranslations->remove($pos);
            if (null === $this->siteFooterBlockTranslationsScheduledForDeletion) {
                $this->siteFooterBlockTranslationsScheduledForDeletion = clone $this->collSiteFooterBlockTranslations;
                $this->siteFooterBlockTranslationsScheduledForDeletion->clear();
            }
            $this->siteFooterBlockTranslationsScheduledForDeletion[]= clone $siteFooterBlockTranslation;
            $siteFooterBlockTranslation->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related SiteFooterBlockTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SiteFooterBlockTranslation[] List of SiteFooterBlockTranslation objects
     */
    public function getSiteFooterBlockTranslationsJoinSiteFooterBlock(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SiteFooterBlockTranslationQuery::create(null, $criteria);
        $query->joinWith('SiteFooterBlock', $joinBehavior);

        return $this->getSiteFooterBlockTranslations($query, $con);
    }

    /**
     * Clears out the collProductUnitTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductUnitTranslations()
     */
    public function clearProductUnitTranslations()
    {
        $this->collProductUnitTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductUnitTranslations collection loaded partially.
     */
    public function resetPartialProductUnitTranslations($v = true)
    {
        $this->collProductUnitTranslationsPartial = $v;
    }

    /**
     * Initializes the collProductUnitTranslations collection.
     *
     * By default this just sets the collProductUnitTranslations collection to an empty array (like clearcollProductUnitTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductUnitTranslations($overrideExisting = true)
    {
        if (null !== $this->collProductUnitTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductUnitTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collProductUnitTranslations = new $collectionClassName;
        $this->collProductUnitTranslations->setModel('\Model\Setting\MasterTable\ProductUnitTranslation');
    }

    /**
     * Gets an array of ChildProductUnitTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProductUnitTranslation[] List of ChildProductUnitTranslation objects
     * @throws PropelException
     */
    public function getProductUnitTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductUnitTranslationsPartial && !$this->isNew();
        if (null === $this->collProductUnitTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductUnitTranslations) {
                    $this->initProductUnitTranslations();
                } else {
                    $collectionClassName = ProductUnitTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collProductUnitTranslations = new $collectionClassName;
                    $collProductUnitTranslations->setModel('\Model\Setting\MasterTable\ProductUnitTranslation');

                    return $collProductUnitTranslations;
                }
            } else {
                $collProductUnitTranslations = ChildProductUnitTranslationQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductUnitTranslationsPartial && count($collProductUnitTranslations)) {
                        $this->initProductUnitTranslations(false);

                        foreach ($collProductUnitTranslations as $obj) {
                            if (false == $this->collProductUnitTranslations->contains($obj)) {
                                $this->collProductUnitTranslations->append($obj);
                            }
                        }

                        $this->collProductUnitTranslationsPartial = true;
                    }

                    return $collProductUnitTranslations;
                }

                if ($partial && $this->collProductUnitTranslations) {
                    foreach ($this->collProductUnitTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collProductUnitTranslations[] = $obj;
                        }
                    }
                }

                $this->collProductUnitTranslations = $collProductUnitTranslations;
                $this->collProductUnitTranslationsPartial = false;
            }
        }

        return $this->collProductUnitTranslations;
    }

    /**
     * Sets a collection of ChildProductUnitTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productUnitTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setProductUnitTranslations(Collection $productUnitTranslations, ConnectionInterface $con = null)
    {
        /** @var ChildProductUnitTranslation[] $productUnitTranslationsToDelete */
        $productUnitTranslationsToDelete = $this->getProductUnitTranslations(new Criteria(), $con)->diff($productUnitTranslations);


        $this->productUnitTranslationsScheduledForDeletion = $productUnitTranslationsToDelete;

        foreach ($productUnitTranslationsToDelete as $productUnitTranslationRemoved) {
            $productUnitTranslationRemoved->setLanguage(null);
        }

        $this->collProductUnitTranslations = null;
        foreach ($productUnitTranslations as $productUnitTranslation) {
            $this->addProductUnitTranslation($productUnitTranslation);
        }

        $this->collProductUnitTranslations = $productUnitTranslations;
        $this->collProductUnitTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProductUnitTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProductUnitTranslation objects.
     * @throws PropelException
     */
    public function countProductUnitTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductUnitTranslationsPartial && !$this->isNew();
        if (null === $this->collProductUnitTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductUnitTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductUnitTranslations());
            }

            $query = ChildProductUnitTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collProductUnitTranslations);
    }

    /**
     * Method called to associate a ChildProductUnitTranslation object to this object
     * through the ChildProductUnitTranslation foreign key attribute.
     *
     * @param  ChildProductUnitTranslation $l ChildProductUnitTranslation
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addProductUnitTranslation(ChildProductUnitTranslation $l)
    {
        if ($this->collProductUnitTranslations === null) {
            $this->initProductUnitTranslations();
            $this->collProductUnitTranslationsPartial = true;
        }

        if (!$this->collProductUnitTranslations->contains($l)) {
            $this->doAddProductUnitTranslation($l);

            if ($this->productUnitTranslationsScheduledForDeletion and $this->productUnitTranslationsScheduledForDeletion->contains($l)) {
                $this->productUnitTranslationsScheduledForDeletion->remove($this->productUnitTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProductUnitTranslation $productUnitTranslation The ChildProductUnitTranslation object to add.
     */
    protected function doAddProductUnitTranslation(ChildProductUnitTranslation $productUnitTranslation)
    {
        $this->collProductUnitTranslations[]= $productUnitTranslation;
        $productUnitTranslation->setLanguage($this);
    }

    /**
     * @param  ChildProductUnitTranslation $productUnitTranslation The ChildProductUnitTranslation object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeProductUnitTranslation(ChildProductUnitTranslation $productUnitTranslation)
    {
        if ($this->getProductUnitTranslations()->contains($productUnitTranslation)) {
            $pos = $this->collProductUnitTranslations->search($productUnitTranslation);
            $this->collProductUnitTranslations->remove($pos);
            if (null === $this->productUnitTranslationsScheduledForDeletion) {
                $this->productUnitTranslationsScheduledForDeletion = clone $this->collProductUnitTranslations;
                $this->productUnitTranslationsScheduledForDeletion->clear();
            }
            $this->productUnitTranslationsScheduledForDeletion[]= clone $productUnitTranslation;
            $productUnitTranslation->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related ProductUnitTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProductUnitTranslation[] List of ChildProductUnitTranslation objects
     */
    public function getProductUnitTranslationsJoinUnit(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProductUnitTranslationQuery::create(null, $criteria);
        $query->joinWith('Unit', $joinBehavior);

        return $this->getProductUnitTranslations($query, $con);
    }

    /**
     * Clears out the collProductDeliveryTimeTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductDeliveryTimeTranslations()
     */
    public function clearProductDeliveryTimeTranslations()
    {
        $this->collProductDeliveryTimeTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductDeliveryTimeTranslations collection loaded partially.
     */
    public function resetPartialProductDeliveryTimeTranslations($v = true)
    {
        $this->collProductDeliveryTimeTranslationsPartial = $v;
    }

    /**
     * Initializes the collProductDeliveryTimeTranslations collection.
     *
     * By default this just sets the collProductDeliveryTimeTranslations collection to an empty array (like clearcollProductDeliveryTimeTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductDeliveryTimeTranslations($overrideExisting = true)
    {
        if (null !== $this->collProductDeliveryTimeTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductDeliveryTimeTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collProductDeliveryTimeTranslations = new $collectionClassName;
        $this->collProductDeliveryTimeTranslations->setModel('\Model\Setting\MasterTable\ProductDeliveryTimeTranslation');
    }

    /**
     * Gets an array of ChildProductDeliveryTimeTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProductDeliveryTimeTranslation[] List of ChildProductDeliveryTimeTranslation objects
     * @throws PropelException
     */
    public function getProductDeliveryTimeTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductDeliveryTimeTranslationsPartial && !$this->isNew();
        if (null === $this->collProductDeliveryTimeTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductDeliveryTimeTranslations) {
                    $this->initProductDeliveryTimeTranslations();
                } else {
                    $collectionClassName = ProductDeliveryTimeTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collProductDeliveryTimeTranslations = new $collectionClassName;
                    $collProductDeliveryTimeTranslations->setModel('\Model\Setting\MasterTable\ProductDeliveryTimeTranslation');

                    return $collProductDeliveryTimeTranslations;
                }
            } else {
                $collProductDeliveryTimeTranslations = ChildProductDeliveryTimeTranslationQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductDeliveryTimeTranslationsPartial && count($collProductDeliveryTimeTranslations)) {
                        $this->initProductDeliveryTimeTranslations(false);

                        foreach ($collProductDeliveryTimeTranslations as $obj) {
                            if (false == $this->collProductDeliveryTimeTranslations->contains($obj)) {
                                $this->collProductDeliveryTimeTranslations->append($obj);
                            }
                        }

                        $this->collProductDeliveryTimeTranslationsPartial = true;
                    }

                    return $collProductDeliveryTimeTranslations;
                }

                if ($partial && $this->collProductDeliveryTimeTranslations) {
                    foreach ($this->collProductDeliveryTimeTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collProductDeliveryTimeTranslations[] = $obj;
                        }
                    }
                }

                $this->collProductDeliveryTimeTranslations = $collProductDeliveryTimeTranslations;
                $this->collProductDeliveryTimeTranslationsPartial = false;
            }
        }

        return $this->collProductDeliveryTimeTranslations;
    }

    /**
     * Sets a collection of ChildProductDeliveryTimeTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productDeliveryTimeTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setProductDeliveryTimeTranslations(Collection $productDeliveryTimeTranslations, ConnectionInterface $con = null)
    {
        /** @var ChildProductDeliveryTimeTranslation[] $productDeliveryTimeTranslationsToDelete */
        $productDeliveryTimeTranslationsToDelete = $this->getProductDeliveryTimeTranslations(new Criteria(), $con)->diff($productDeliveryTimeTranslations);


        $this->productDeliveryTimeTranslationsScheduledForDeletion = $productDeliveryTimeTranslationsToDelete;

        foreach ($productDeliveryTimeTranslationsToDelete as $productDeliveryTimeTranslationRemoved) {
            $productDeliveryTimeTranslationRemoved->setLanguage(null);
        }

        $this->collProductDeliveryTimeTranslations = null;
        foreach ($productDeliveryTimeTranslations as $productDeliveryTimeTranslation) {
            $this->addProductDeliveryTimeTranslation($productDeliveryTimeTranslation);
        }

        $this->collProductDeliveryTimeTranslations = $productDeliveryTimeTranslations;
        $this->collProductDeliveryTimeTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProductDeliveryTimeTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProductDeliveryTimeTranslation objects.
     * @throws PropelException
     */
    public function countProductDeliveryTimeTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductDeliveryTimeTranslationsPartial && !$this->isNew();
        if (null === $this->collProductDeliveryTimeTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductDeliveryTimeTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductDeliveryTimeTranslations());
            }

            $query = ChildProductDeliveryTimeTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collProductDeliveryTimeTranslations);
    }

    /**
     * Method called to associate a ChildProductDeliveryTimeTranslation object to this object
     * through the ChildProductDeliveryTimeTranslation foreign key attribute.
     *
     * @param  ChildProductDeliveryTimeTranslation $l ChildProductDeliveryTimeTranslation
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addProductDeliveryTimeTranslation(ChildProductDeliveryTimeTranslation $l)
    {
        if ($this->collProductDeliveryTimeTranslations === null) {
            $this->initProductDeliveryTimeTranslations();
            $this->collProductDeliveryTimeTranslationsPartial = true;
        }

        if (!$this->collProductDeliveryTimeTranslations->contains($l)) {
            $this->doAddProductDeliveryTimeTranslation($l);

            if ($this->productDeliveryTimeTranslationsScheduledForDeletion and $this->productDeliveryTimeTranslationsScheduledForDeletion->contains($l)) {
                $this->productDeliveryTimeTranslationsScheduledForDeletion->remove($this->productDeliveryTimeTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProductDeliveryTimeTranslation $productDeliveryTimeTranslation The ChildProductDeliveryTimeTranslation object to add.
     */
    protected function doAddProductDeliveryTimeTranslation(ChildProductDeliveryTimeTranslation $productDeliveryTimeTranslation)
    {
        $this->collProductDeliveryTimeTranslations[]= $productDeliveryTimeTranslation;
        $productDeliveryTimeTranslation->setLanguage($this);
    }

    /**
     * @param  ChildProductDeliveryTimeTranslation $productDeliveryTimeTranslation The ChildProductDeliveryTimeTranslation object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeProductDeliveryTimeTranslation(ChildProductDeliveryTimeTranslation $productDeliveryTimeTranslation)
    {
        if ($this->getProductDeliveryTimeTranslations()->contains($productDeliveryTimeTranslation)) {
            $pos = $this->collProductDeliveryTimeTranslations->search($productDeliveryTimeTranslation);
            $this->collProductDeliveryTimeTranslations->remove($pos);
            if (null === $this->productDeliveryTimeTranslationsScheduledForDeletion) {
                $this->productDeliveryTimeTranslationsScheduledForDeletion = clone $this->collProductDeliveryTimeTranslations;
                $this->productDeliveryTimeTranslationsScheduledForDeletion->clear();
            }
            $this->productDeliveryTimeTranslationsScheduledForDeletion[]= clone $productDeliveryTimeTranslation;
            $productDeliveryTimeTranslation->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related ProductDeliveryTimeTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProductDeliveryTimeTranslation[] List of ChildProductDeliveryTimeTranslation objects
     */
    public function getProductDeliveryTimeTranslationsJoinDeliveryTime(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProductDeliveryTimeTranslationQuery::create(null, $criteria);
        $query->joinWith('DeliveryTime', $joinBehavior);

        return $this->getProductDeliveryTimeTranslations($query, $con);
    }

    /**
     * Clears out the collSiteBlogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteBlogs()
     */
    public function clearSiteBlogs()
    {
        $this->collSiteBlogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteBlogs collection loaded partially.
     */
    public function resetPartialSiteBlogs($v = true)
    {
        $this->collSiteBlogsPartial = $v;
    }

    /**
     * Initializes the collSiteBlogs collection.
     *
     * By default this just sets the collSiteBlogs collection to an empty array (like clearcollSiteBlogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteBlogs($overrideExisting = true)
    {
        if (null !== $this->collSiteBlogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteBlogTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteBlogs = new $collectionClassName;
        $this->collSiteBlogs->setModel('\Model\Cms\SiteBlog');
    }

    /**
     * Gets an array of SiteBlog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|SiteBlog[] List of SiteBlog objects
     * @throws PropelException
     */
    public function getSiteBlogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteBlogsPartial && !$this->isNew();
        if (null === $this->collSiteBlogs || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteBlogs) {
                    $this->initSiteBlogs();
                } else {
                    $collectionClassName = SiteBlogTableMap::getTableMap()->getCollectionClassName();

                    $collSiteBlogs = new $collectionClassName;
                    $collSiteBlogs->setModel('\Model\Cms\SiteBlog');

                    return $collSiteBlogs;
                }
            } else {
                $collSiteBlogs = SiteBlogQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteBlogsPartial && count($collSiteBlogs)) {
                        $this->initSiteBlogs(false);

                        foreach ($collSiteBlogs as $obj) {
                            if (false == $this->collSiteBlogs->contains($obj)) {
                                $this->collSiteBlogs->append($obj);
                            }
                        }

                        $this->collSiteBlogsPartial = true;
                    }

                    return $collSiteBlogs;
                }

                if ($partial && $this->collSiteBlogs) {
                    foreach ($this->collSiteBlogs as $obj) {
                        if ($obj->isNew()) {
                            $collSiteBlogs[] = $obj;
                        }
                    }
                }

                $this->collSiteBlogs = $collSiteBlogs;
                $this->collSiteBlogsPartial = false;
            }
        }

        return $this->collSiteBlogs;
    }

    /**
     * Sets a collection of SiteBlog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteBlogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setSiteBlogs(Collection $siteBlogs, ConnectionInterface $con = null)
    {
        /** @var SiteBlog[] $siteBlogsToDelete */
        $siteBlogsToDelete = $this->getSiteBlogs(new Criteria(), $con)->diff($siteBlogs);


        $this->siteBlogsScheduledForDeletion = $siteBlogsToDelete;

        foreach ($siteBlogsToDelete as $siteBlogRemoved) {
            $siteBlogRemoved->setLanguage(null);
        }

        $this->collSiteBlogs = null;
        foreach ($siteBlogs as $siteBlog) {
            $this->addSiteBlog($siteBlog);
        }

        $this->collSiteBlogs = $siteBlogs;
        $this->collSiteBlogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSiteBlog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSiteBlog objects.
     * @throws PropelException
     */
    public function countSiteBlogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteBlogsPartial && !$this->isNew();
        if (null === $this->collSiteBlogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteBlogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteBlogs());
            }

            $query = SiteBlogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collSiteBlogs);
    }

    /**
     * Method called to associate a SiteBlog object to this object
     * through the SiteBlog foreign key attribute.
     *
     * @param  SiteBlog $l SiteBlog
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addSiteBlog(SiteBlog $l)
    {
        if ($this->collSiteBlogs === null) {
            $this->initSiteBlogs();
            $this->collSiteBlogsPartial = true;
        }

        if (!$this->collSiteBlogs->contains($l)) {
            $this->doAddSiteBlog($l);

            if ($this->siteBlogsScheduledForDeletion and $this->siteBlogsScheduledForDeletion->contains($l)) {
                $this->siteBlogsScheduledForDeletion->remove($this->siteBlogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param SiteBlog $siteBlog The SiteBlog object to add.
     */
    protected function doAddSiteBlog(SiteBlog $siteBlog)
    {
        $this->collSiteBlogs[]= $siteBlog;
        $siteBlog->setLanguage($this);
    }

    /**
     * @param  SiteBlog $siteBlog The SiteBlog object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeSiteBlog(SiteBlog $siteBlog)
    {
        if ($this->getSiteBlogs()->contains($siteBlog)) {
            $pos = $this->collSiteBlogs->search($siteBlog);
            $this->collSiteBlogs->remove($pos);
            if (null === $this->siteBlogsScheduledForDeletion) {
                $this->siteBlogsScheduledForDeletion = clone $this->collSiteBlogs;
                $this->siteBlogsScheduledForDeletion->clear();
            }
            $this->siteBlogsScheduledForDeletion[]= clone $siteBlog;
            $siteBlog->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related SiteBlogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SiteBlog[] List of SiteBlog objects
     */
    public function getSiteBlogsJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SiteBlogQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getSiteBlogs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related SiteBlogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SiteBlog[] List of SiteBlog objects
     */
    public function getSiteBlogsJoinUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SiteBlogQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getSiteBlogs($query, $con);
    }

    /**
     * Clears out the collSiteFAQCategoryTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteFAQCategoryTranslations()
     */
    public function clearSiteFAQCategoryTranslations()
    {
        $this->collSiteFAQCategoryTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteFAQCategoryTranslations collection loaded partially.
     */
    public function resetPartialSiteFAQCategoryTranslations($v = true)
    {
        $this->collSiteFAQCategoryTranslationsPartial = $v;
    }

    /**
     * Initializes the collSiteFAQCategoryTranslations collection.
     *
     * By default this just sets the collSiteFAQCategoryTranslations collection to an empty array (like clearcollSiteFAQCategoryTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteFAQCategoryTranslations($overrideExisting = true)
    {
        if (null !== $this->collSiteFAQCategoryTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteFAQCategoryTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteFAQCategoryTranslations = new $collectionClassName;
        $this->collSiteFAQCategoryTranslations->setModel('\Model\Cms\SiteFAQCategoryTranslation');
    }

    /**
     * Gets an array of SiteFAQCategoryTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|SiteFAQCategoryTranslation[] List of SiteFAQCategoryTranslation objects
     * @throws PropelException
     */
    public function getSiteFAQCategoryTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFAQCategoryTranslationsPartial && !$this->isNew();
        if (null === $this->collSiteFAQCategoryTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteFAQCategoryTranslations) {
                    $this->initSiteFAQCategoryTranslations();
                } else {
                    $collectionClassName = SiteFAQCategoryTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collSiteFAQCategoryTranslations = new $collectionClassName;
                    $collSiteFAQCategoryTranslations->setModel('\Model\Cms\SiteFAQCategoryTranslation');

                    return $collSiteFAQCategoryTranslations;
                }
            } else {
                $collSiteFAQCategoryTranslations = SiteFAQCategoryTranslationQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteFAQCategoryTranslationsPartial && count($collSiteFAQCategoryTranslations)) {
                        $this->initSiteFAQCategoryTranslations(false);

                        foreach ($collSiteFAQCategoryTranslations as $obj) {
                            if (false == $this->collSiteFAQCategoryTranslations->contains($obj)) {
                                $this->collSiteFAQCategoryTranslations->append($obj);
                            }
                        }

                        $this->collSiteFAQCategoryTranslationsPartial = true;
                    }

                    return $collSiteFAQCategoryTranslations;
                }

                if ($partial && $this->collSiteFAQCategoryTranslations) {
                    foreach ($this->collSiteFAQCategoryTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collSiteFAQCategoryTranslations[] = $obj;
                        }
                    }
                }

                $this->collSiteFAQCategoryTranslations = $collSiteFAQCategoryTranslations;
                $this->collSiteFAQCategoryTranslationsPartial = false;
            }
        }

        return $this->collSiteFAQCategoryTranslations;
    }

    /**
     * Sets a collection of SiteFAQCategoryTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteFAQCategoryTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setSiteFAQCategoryTranslations(Collection $siteFAQCategoryTranslations, ConnectionInterface $con = null)
    {
        /** @var SiteFAQCategoryTranslation[] $siteFAQCategoryTranslationsToDelete */
        $siteFAQCategoryTranslationsToDelete = $this->getSiteFAQCategoryTranslations(new Criteria(), $con)->diff($siteFAQCategoryTranslations);


        $this->siteFAQCategoryTranslationsScheduledForDeletion = $siteFAQCategoryTranslationsToDelete;

        foreach ($siteFAQCategoryTranslationsToDelete as $siteFAQCategoryTranslationRemoved) {
            $siteFAQCategoryTranslationRemoved->setLanguage(null);
        }

        $this->collSiteFAQCategoryTranslations = null;
        foreach ($siteFAQCategoryTranslations as $siteFAQCategoryTranslation) {
            $this->addSiteFAQCategoryTranslation($siteFAQCategoryTranslation);
        }

        $this->collSiteFAQCategoryTranslations = $siteFAQCategoryTranslations;
        $this->collSiteFAQCategoryTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSiteFAQCategoryTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSiteFAQCategoryTranslation objects.
     * @throws PropelException
     */
    public function countSiteFAQCategoryTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFAQCategoryTranslationsPartial && !$this->isNew();
        if (null === $this->collSiteFAQCategoryTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteFAQCategoryTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteFAQCategoryTranslations());
            }

            $query = SiteFAQCategoryTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collSiteFAQCategoryTranslations);
    }

    /**
     * Method called to associate a SiteFAQCategoryTranslation object to this object
     * through the SiteFAQCategoryTranslation foreign key attribute.
     *
     * @param  SiteFAQCategoryTranslation $l SiteFAQCategoryTranslation
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addSiteFAQCategoryTranslation(SiteFAQCategoryTranslation $l)
    {
        if ($this->collSiteFAQCategoryTranslations === null) {
            $this->initSiteFAQCategoryTranslations();
            $this->collSiteFAQCategoryTranslationsPartial = true;
        }

        if (!$this->collSiteFAQCategoryTranslations->contains($l)) {
            $this->doAddSiteFAQCategoryTranslation($l);

            if ($this->siteFAQCategoryTranslationsScheduledForDeletion and $this->siteFAQCategoryTranslationsScheduledForDeletion->contains($l)) {
                $this->siteFAQCategoryTranslationsScheduledForDeletion->remove($this->siteFAQCategoryTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param SiteFAQCategoryTranslation $siteFAQCategoryTranslation The SiteFAQCategoryTranslation object to add.
     */
    protected function doAddSiteFAQCategoryTranslation(SiteFAQCategoryTranslation $siteFAQCategoryTranslation)
    {
        $this->collSiteFAQCategoryTranslations[]= $siteFAQCategoryTranslation;
        $siteFAQCategoryTranslation->setLanguage($this);
    }

    /**
     * @param  SiteFAQCategoryTranslation $siteFAQCategoryTranslation The SiteFAQCategoryTranslation object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeSiteFAQCategoryTranslation(SiteFAQCategoryTranslation $siteFAQCategoryTranslation)
    {
        if ($this->getSiteFAQCategoryTranslations()->contains($siteFAQCategoryTranslation)) {
            $pos = $this->collSiteFAQCategoryTranslations->search($siteFAQCategoryTranslation);
            $this->collSiteFAQCategoryTranslations->remove($pos);
            if (null === $this->siteFAQCategoryTranslationsScheduledForDeletion) {
                $this->siteFAQCategoryTranslationsScheduledForDeletion = clone $this->collSiteFAQCategoryTranslations;
                $this->siteFAQCategoryTranslationsScheduledForDeletion->clear();
            }
            $this->siteFAQCategoryTranslationsScheduledForDeletion[]= $siteFAQCategoryTranslation;
            $siteFAQCategoryTranslation->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related SiteFAQCategoryTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SiteFAQCategoryTranslation[] List of SiteFAQCategoryTranslation objects
     */
    public function getSiteFAQCategoryTranslationsJoinFaqCategory(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SiteFAQCategoryTranslationQuery::create(null, $criteria);
        $query->joinWith('FaqCategory', $joinBehavior);

        return $this->getSiteFAQCategoryTranslations($query, $con);
    }

    /**
     * Clears out the collSiteFAQs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteFAQs()
     */
    public function clearSiteFAQs()
    {
        $this->collSiteFAQs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteFAQs collection loaded partially.
     */
    public function resetPartialSiteFAQs($v = true)
    {
        $this->collSiteFAQsPartial = $v;
    }

    /**
     * Initializes the collSiteFAQs collection.
     *
     * By default this just sets the collSiteFAQs collection to an empty array (like clearcollSiteFAQs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteFAQs($overrideExisting = true)
    {
        if (null !== $this->collSiteFAQs && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteFAQTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteFAQs = new $collectionClassName;
        $this->collSiteFAQs->setModel('\Model\Cms\SiteFAQ');
    }

    /**
     * Gets an array of SiteFAQ objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|SiteFAQ[] List of SiteFAQ objects
     * @throws PropelException
     */
    public function getSiteFAQs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFAQsPartial && !$this->isNew();
        if (null === $this->collSiteFAQs || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteFAQs) {
                    $this->initSiteFAQs();
                } else {
                    $collectionClassName = SiteFAQTableMap::getTableMap()->getCollectionClassName();

                    $collSiteFAQs = new $collectionClassName;
                    $collSiteFAQs->setModel('\Model\Cms\SiteFAQ');

                    return $collSiteFAQs;
                }
            } else {
                $collSiteFAQs = SiteFAQQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteFAQsPartial && count($collSiteFAQs)) {
                        $this->initSiteFAQs(false);

                        foreach ($collSiteFAQs as $obj) {
                            if (false == $this->collSiteFAQs->contains($obj)) {
                                $this->collSiteFAQs->append($obj);
                            }
                        }

                        $this->collSiteFAQsPartial = true;
                    }

                    return $collSiteFAQs;
                }

                if ($partial && $this->collSiteFAQs) {
                    foreach ($this->collSiteFAQs as $obj) {
                        if ($obj->isNew()) {
                            $collSiteFAQs[] = $obj;
                        }
                    }
                }

                $this->collSiteFAQs = $collSiteFAQs;
                $this->collSiteFAQsPartial = false;
            }
        }

        return $this->collSiteFAQs;
    }

    /**
     * Sets a collection of SiteFAQ objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteFAQs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setSiteFAQs(Collection $siteFAQs, ConnectionInterface $con = null)
    {
        /** @var SiteFAQ[] $siteFAQsToDelete */
        $siteFAQsToDelete = $this->getSiteFAQs(new Criteria(), $con)->diff($siteFAQs);


        $this->siteFAQsScheduledForDeletion = $siteFAQsToDelete;

        foreach ($siteFAQsToDelete as $siteFAQRemoved) {
            $siteFAQRemoved->setLanguage(null);
        }

        $this->collSiteFAQs = null;
        foreach ($siteFAQs as $siteFAQ) {
            $this->addSiteFAQ($siteFAQ);
        }

        $this->collSiteFAQs = $siteFAQs;
        $this->collSiteFAQsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSiteFAQ objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSiteFAQ objects.
     * @throws PropelException
     */
    public function countSiteFAQs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFAQsPartial && !$this->isNew();
        if (null === $this->collSiteFAQs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteFAQs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteFAQs());
            }

            $query = SiteFAQQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collSiteFAQs);
    }

    /**
     * Method called to associate a SiteFAQ object to this object
     * through the SiteFAQ foreign key attribute.
     *
     * @param  SiteFAQ $l SiteFAQ
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addSiteFAQ(SiteFAQ $l)
    {
        if ($this->collSiteFAQs === null) {
            $this->initSiteFAQs();
            $this->collSiteFAQsPartial = true;
        }

        if (!$this->collSiteFAQs->contains($l)) {
            $this->doAddSiteFAQ($l);

            if ($this->siteFAQsScheduledForDeletion and $this->siteFAQsScheduledForDeletion->contains($l)) {
                $this->siteFAQsScheduledForDeletion->remove($this->siteFAQsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param SiteFAQ $siteFAQ The SiteFAQ object to add.
     */
    protected function doAddSiteFAQ(SiteFAQ $siteFAQ)
    {
        $this->collSiteFAQs[]= $siteFAQ;
        $siteFAQ->setLanguage($this);
    }

    /**
     * @param  SiteFAQ $siteFAQ The SiteFAQ object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeSiteFAQ(SiteFAQ $siteFAQ)
    {
        if ($this->getSiteFAQs()->contains($siteFAQ)) {
            $pos = $this->collSiteFAQs->search($siteFAQ);
            $this->collSiteFAQs->remove($pos);
            if (null === $this->siteFAQsScheduledForDeletion) {
                $this->siteFAQsScheduledForDeletion = clone $this->collSiteFAQs;
                $this->siteFAQsScheduledForDeletion->clear();
            }
            $this->siteFAQsScheduledForDeletion[]= clone $siteFAQ;
            $siteFAQ->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related SiteFAQs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SiteFAQ[] List of SiteFAQ objects
     */
    public function getSiteFAQsJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SiteFAQQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getSiteFAQs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related SiteFAQs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SiteFAQ[] List of SiteFAQ objects
     */
    public function getSiteFAQsJoinFaqCategory(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SiteFAQQuery::create(null, $criteria);
        $query->joinWith('FaqCategory', $joinBehavior);

        return $this->getSiteFAQs($query, $con);
    }

    /**
     * Clears out the collSitePages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSitePages()
     */
    public function clearSitePages()
    {
        $this->collSitePages = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSitePages collection loaded partially.
     */
    public function resetPartialSitePages($v = true)
    {
        $this->collSitePagesPartial = $v;
    }

    /**
     * Initializes the collSitePages collection.
     *
     * By default this just sets the collSitePages collection to an empty array (like clearcollSitePages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSitePages($overrideExisting = true)
    {
        if (null !== $this->collSitePages && !$overrideExisting) {
            return;
        }

        $collectionClassName = SitePageTableMap::getTableMap()->getCollectionClassName();

        $this->collSitePages = new $collectionClassName;
        $this->collSitePages->setModel('\Model\Cms\SitePage');
    }

    /**
     * Gets an array of SitePage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildLanguage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|SitePage[] List of SitePage objects
     * @throws PropelException
     */
    public function getSitePages(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSitePagesPartial && !$this->isNew();
        if (null === $this->collSitePages || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSitePages) {
                    $this->initSitePages();
                } else {
                    $collectionClassName = SitePageTableMap::getTableMap()->getCollectionClassName();

                    $collSitePages = new $collectionClassName;
                    $collSitePages->setModel('\Model\Cms\SitePage');

                    return $collSitePages;
                }
            } else {
                $collSitePages = SitePageQuery::create(null, $criteria)
                    ->filterByLanguage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSitePagesPartial && count($collSitePages)) {
                        $this->initSitePages(false);

                        foreach ($collSitePages as $obj) {
                            if (false == $this->collSitePages->contains($obj)) {
                                $this->collSitePages->append($obj);
                            }
                        }

                        $this->collSitePagesPartial = true;
                    }

                    return $collSitePages;
                }

                if ($partial && $this->collSitePages) {
                    foreach ($this->collSitePages as $obj) {
                        if ($obj->isNew()) {
                            $collSitePages[] = $obj;
                        }
                    }
                }

                $this->collSitePages = $collSitePages;
                $this->collSitePagesPartial = false;
            }
        }

        return $this->collSitePages;
    }

    /**
     * Sets a collection of SitePage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sitePages A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function setSitePages(Collection $sitePages, ConnectionInterface $con = null)
    {
        /** @var SitePage[] $sitePagesToDelete */
        $sitePagesToDelete = $this->getSitePages(new Criteria(), $con)->diff($sitePages);


        $this->sitePagesScheduledForDeletion = $sitePagesToDelete;

        foreach ($sitePagesToDelete as $sitePageRemoved) {
            $sitePageRemoved->setLanguage(null);
        }

        $this->collSitePages = null;
        foreach ($sitePages as $sitePage) {
            $this->addSitePage($sitePage);
        }

        $this->collSitePages = $sitePages;
        $this->collSitePagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSitePage objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSitePage objects.
     * @throws PropelException
     */
    public function countSitePages(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSitePagesPartial && !$this->isNew();
        if (null === $this->collSitePages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSitePages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSitePages());
            }

            $query = SitePageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByLanguage($this)
                ->count($con);
        }

        return count($this->collSitePages);
    }

    /**
     * Method called to associate a SitePage object to this object
     * through the SitePage foreign key attribute.
     *
     * @param  SitePage $l SitePage
     * @return $this|\Model\Setting\MasterTable\Language The current object (for fluent API support)
     */
    public function addSitePage(SitePage $l)
    {
        if ($this->collSitePages === null) {
            $this->initSitePages();
            $this->collSitePagesPartial = true;
        }

        if (!$this->collSitePages->contains($l)) {
            $this->doAddSitePage($l);

            if ($this->sitePagesScheduledForDeletion and $this->sitePagesScheduledForDeletion->contains($l)) {
                $this->sitePagesScheduledForDeletion->remove($this->sitePagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param SitePage $sitePage The SitePage object to add.
     */
    protected function doAddSitePage(SitePage $sitePage)
    {
        $this->collSitePages[]= $sitePage;
        $sitePage->setLanguage($this);
    }

    /**
     * @param  SitePage $sitePage The SitePage object to remove.
     * @return $this|ChildLanguage The current object (for fluent API support)
     */
    public function removeSitePage(SitePage $sitePage)
    {
        if ($this->getSitePages()->contains($sitePage)) {
            $pos = $this->collSitePages->search($sitePage);
            $this->collSitePages->remove($pos);
            if (null === $this->sitePagesScheduledForDeletion) {
                $this->sitePagesScheduledForDeletion = clone $this->collSitePages;
                $this->sitePagesScheduledForDeletion->clear();
            }
            $this->sitePagesScheduledForDeletion[]= $sitePage;
            $sitePage->setLanguage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related SitePages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SitePage[] List of SitePage objects
     */
    public function getSitePagesJoinSiteNavigaton(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SitePageQuery::create(null, $criteria);
        $query->joinWith('SiteNavigaton', $joinBehavior);

        return $this->getSitePages($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Language is new, it will return
     * an empty collection; or if this Language has previously
     * been saved, it will retrieve related SitePages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Language.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SitePage[] List of SitePage objects
     */
    public function getSitePagesJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SitePageQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getSitePages($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->is_deleted = null;
        $this->description = null;
        $this->locale_code = null;
        $this->is_enabled_cms = null;
        $this->is_enabled_webshop = null;
        $this->is_default_cms = null;
        $this->is_default_webshop = null;
        $this->flag_icon = null;
        $this->shop_url_prefix = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collLegalFormTranslations) {
                foreach ($this->collLegalFormTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collContactMessages) {
                foreach ($this->collContactMessages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsers) {
                foreach ($this->collUsers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCustomers) {
                foreach ($this->collCustomers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCategoryTranslations) {
                foreach ($this->collCategoryTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPromoProductTranslations) {
                foreach ($this->collPromoProductTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProductTranslations) {
                foreach ($this->collProductTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collVatTranslations) {
                foreach ($this->collVatTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsageTranslations) {
                foreach ($this->collUsageTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collColorTranslations) {
                foreach ($this->collColorTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteUsps) {
                foreach ($this->collSiteUsps as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteFooterBlockTranslations) {
                foreach ($this->collSiteFooterBlockTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProductUnitTranslations) {
                foreach ($this->collProductUnitTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProductDeliveryTimeTranslations) {
                foreach ($this->collProductDeliveryTimeTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteBlogs) {
                foreach ($this->collSiteBlogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteFAQCategoryTranslations) {
                foreach ($this->collSiteFAQCategoryTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteFAQs) {
                foreach ($this->collSiteFAQs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSitePages) {
                foreach ($this->collSitePages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collLegalFormTranslations = null;
        $this->collContactMessages = null;
        $this->collUsers = null;
        $this->collCustomers = null;
        $this->collCategoryTranslations = null;
        $this->collPromoProductTranslations = null;
        $this->collProductTranslations = null;
        $this->collVatTranslations = null;
        $this->collUsageTranslations = null;
        $this->collColorTranslations = null;
        $this->collSiteUsps = null;
        $this->collSiteFooterBlockTranslations = null;
        $this->collProductUnitTranslations = null;
        $this->collProductDeliveryTimeTranslations = null;
        $this->collSiteBlogs = null;
        $this->collSiteFAQCategoryTranslations = null;
        $this->collSiteFAQs = null;
        $this->collSitePages = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(LanguageTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildLanguage The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[LanguageTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
