<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\Crm\Customer;
use Model\Setting\MasterTable\PayTerm as ChildPayTerm;
use Model\Setting\MasterTable\PayTermQuery as ChildPayTermQuery;
use Model\Setting\MasterTable\Map\PayTermTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mt_payterm' table.
 *
 *
 *
 * @method     ChildPayTermQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPayTermQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildPayTermQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method     ChildPayTermQuery orderByTotalDays($order = Criteria::ASC) Order by the total_days column
 * @method     ChildPayTermQuery orderByPeriod($order = Criteria::ASC) Order by the period column
 * @method     ChildPayTermQuery orderByIsDefault($order = Criteria::ASC) Order by the is_default column
 *
 * @method     ChildPayTermQuery groupById() Group by the id column
 * @method     ChildPayTermQuery groupByName() Group by the name column
 * @method     ChildPayTermQuery groupByCode() Group by the code column
 * @method     ChildPayTermQuery groupByTotalDays() Group by the total_days column
 * @method     ChildPayTermQuery groupByPeriod() Group by the period column
 * @method     ChildPayTermQuery groupByIsDefault() Group by the is_default column
 *
 * @method     ChildPayTermQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPayTermQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPayTermQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPayTermQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPayTermQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPayTermQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPayTermQuery leftJoinPayTerm($relationAlias = null) Adds a LEFT JOIN clause to the query using the PayTerm relation
 * @method     ChildPayTermQuery rightJoinPayTerm($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PayTerm relation
 * @method     ChildPayTermQuery innerJoinPayTerm($relationAlias = null) Adds a INNER JOIN clause to the query using the PayTerm relation
 *
 * @method     ChildPayTermQuery joinWithPayTerm($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PayTerm relation
 *
 * @method     ChildPayTermQuery leftJoinWithPayTerm() Adds a LEFT JOIN clause and with to the query using the PayTerm relation
 * @method     ChildPayTermQuery rightJoinWithPayTerm() Adds a RIGHT JOIN clause and with to the query using the PayTerm relation
 * @method     ChildPayTermQuery innerJoinWithPayTerm() Adds a INNER JOIN clause and with to the query using the PayTerm relation
 *
 * @method     \Model\Crm\CustomerQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPayTerm findOne(ConnectionInterface $con = null) Return the first ChildPayTerm matching the query
 * @method     ChildPayTerm findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPayTerm matching the query, or a new ChildPayTerm object populated from the query conditions when no match is found
 *
 * @method     ChildPayTerm findOneById(int $id) Return the first ChildPayTerm filtered by the id column
 * @method     ChildPayTerm findOneByName(string $name) Return the first ChildPayTerm filtered by the name column
 * @method     ChildPayTerm findOneByCode(string $code) Return the first ChildPayTerm filtered by the code column
 * @method     ChildPayTerm findOneByTotalDays(int $total_days) Return the first ChildPayTerm filtered by the total_days column
 * @method     ChildPayTerm findOneByPeriod(string $period) Return the first ChildPayTerm filtered by the period column
 * @method     ChildPayTerm findOneByIsDefault(boolean $is_default) Return the first ChildPayTerm filtered by the is_default column *

 * @method     ChildPayTerm requirePk($key, ConnectionInterface $con = null) Return the ChildPayTerm by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPayTerm requireOne(ConnectionInterface $con = null) Return the first ChildPayTerm matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPayTerm requireOneById(int $id) Return the first ChildPayTerm filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPayTerm requireOneByName(string $name) Return the first ChildPayTerm filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPayTerm requireOneByCode(string $code) Return the first ChildPayTerm filtered by the code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPayTerm requireOneByTotalDays(int $total_days) Return the first ChildPayTerm filtered by the total_days column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPayTerm requireOneByPeriod(string $period) Return the first ChildPayTerm filtered by the period column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPayTerm requireOneByIsDefault(boolean $is_default) Return the first ChildPayTerm filtered by the is_default column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPayTerm[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPayTerm objects based on current ModelCriteria
 * @method     ChildPayTerm[]|ObjectCollection findById(int $id) Return ChildPayTerm objects filtered by the id column
 * @method     ChildPayTerm[]|ObjectCollection findByName(string $name) Return ChildPayTerm objects filtered by the name column
 * @method     ChildPayTerm[]|ObjectCollection findByCode(string $code) Return ChildPayTerm objects filtered by the code column
 * @method     ChildPayTerm[]|ObjectCollection findByTotalDays(int $total_days) Return ChildPayTerm objects filtered by the total_days column
 * @method     ChildPayTerm[]|ObjectCollection findByPeriod(string $period) Return ChildPayTerm objects filtered by the period column
 * @method     ChildPayTerm[]|ObjectCollection findByIsDefault(boolean $is_default) Return ChildPayTerm objects filtered by the is_default column
 * @method     ChildPayTerm[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PayTermQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\MasterTable\Base\PayTermQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\MasterTable\\PayTerm', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPayTermQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPayTermQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPayTermQuery) {
            return $criteria;
        }
        $query = new ChildPayTermQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPayTerm|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PayTermTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PayTermTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPayTerm A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, code, total_days, period, is_default FROM mt_payterm WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPayTerm $obj */
            $obj = new ChildPayTerm();
            $obj->hydrate($row);
            PayTermTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPayTerm|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPayTermQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PayTermTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPayTermQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PayTermTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPayTermQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PayTermTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PayTermTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PayTermTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPayTermQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PayTermTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%', Criteria::LIKE); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPayTermQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PayTermTableMap::COL_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the total_days column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalDays(1234); // WHERE total_days = 1234
     * $query->filterByTotalDays(array(12, 34)); // WHERE total_days IN (12, 34)
     * $query->filterByTotalDays(array('min' => 12)); // WHERE total_days > 12
     * </code>
     *
     * @param     mixed $totalDays The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPayTermQuery The current query, for fluid interface
     */
    public function filterByTotalDays($totalDays = null, $comparison = null)
    {
        if (is_array($totalDays)) {
            $useMinMax = false;
            if (isset($totalDays['min'])) {
                $this->addUsingAlias(PayTermTableMap::COL_TOTAL_DAYS, $totalDays['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalDays['max'])) {
                $this->addUsingAlias(PayTermTableMap::COL_TOTAL_DAYS, $totalDays['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PayTermTableMap::COL_TOTAL_DAYS, $totalDays, $comparison);
    }

    /**
     * Filter the query on the period column
     *
     * Example usage:
     * <code>
     * $query->filterByPeriod('fooValue');   // WHERE period = 'fooValue'
     * $query->filterByPeriod('%fooValue%', Criteria::LIKE); // WHERE period LIKE '%fooValue%'
     * </code>
     *
     * @param     string $period The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPayTermQuery The current query, for fluid interface
     */
    public function filterByPeriod($period = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($period)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PayTermTableMap::COL_PERIOD, $period, $comparison);
    }

    /**
     * Filter the query on the is_default column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDefault(true); // WHERE is_default = true
     * $query->filterByIsDefault('yes'); // WHERE is_default = true
     * </code>
     *
     * @param     boolean|string $isDefault The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPayTermQuery The current query, for fluid interface
     */
    public function filterByIsDefault($isDefault = null, $comparison = null)
    {
        if (is_string($isDefault)) {
            $isDefault = in_array(strtolower($isDefault), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PayTermTableMap::COL_IS_DEFAULT, $isDefault, $comparison);
    }

    /**
     * Filter the query by a related \Model\Crm\Customer object
     *
     * @param \Model\Crm\Customer|ObjectCollection $customer the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPayTermQuery The current query, for fluid interface
     */
    public function filterByPayTerm($customer, $comparison = null)
    {
        if ($customer instanceof \Model\Crm\Customer) {
            return $this
                ->addUsingAlias(PayTermTableMap::COL_ID, $customer->getPaytermId(), $comparison);
        } elseif ($customer instanceof ObjectCollection) {
            return $this
                ->usePayTermQuery()
                ->filterByPrimaryKeys($customer->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPayTerm() only accepts arguments of type \Model\Crm\Customer or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PayTerm relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPayTermQuery The current query, for fluid interface
     */
    public function joinPayTerm($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PayTerm');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PayTerm');
        }

        return $this;
    }

    /**
     * Use the PayTerm relation Customer object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerQuery A secondary query class using the current class as primary query
     */
    public function usePayTermQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPayTerm($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PayTerm', '\Model\Crm\CustomerQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPayTerm $payTerm Object to remove from the list of results
     *
     * @return $this|ChildPayTermQuery The current query, for fluid interface
     */
    public function prune($payTerm = null)
    {
        if ($payTerm) {
            $this->addUsingAlias(PayTermTableMap::COL_ID, $payTerm->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mt_payterm table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PayTermTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PayTermTableMap::clearInstancePool();
            PayTermTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PayTermTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PayTermTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PayTermTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PayTermTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PayTermQuery
