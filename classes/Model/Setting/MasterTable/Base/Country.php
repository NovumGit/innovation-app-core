<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\Company\Company;
use Model\Company\CompanyQuery;
use Model\Company\Base\Company as BaseCompany;
use Model\Company\Map\CompanyTableMap;
use Model\Crm\CustomerAddress;
use Model\Crm\CustomerAddressQuery;
use Model\Crm\Base\CustomerAddress as BaseCustomerAddress;
use Model\Crm\Map\CustomerAddressTableMap;
use Model\Reseller\Reseller;
use Model\Reseller\ResellerQuery;
use Model\Reseller\Base\Reseller as BaseReseller;
use Model\Reseller\Map\ResellerTableMap;
use Model\Setting\MasterTable\Country as ChildCountry;
use Model\Setting\MasterTable\CountryQuery as ChildCountryQuery;
use Model\Setting\MasterTable\CountryShippingMethod as ChildCountryShippingMethod;
use Model\Setting\MasterTable\CountryShippingMethodQuery as ChildCountryShippingMethodQuery;
use Model\Setting\MasterTable\ShippingZone as ChildShippingZone;
use Model\Setting\MasterTable\ShippingZoneQuery as ChildShippingZoneQuery;
use Model\Setting\MasterTable\Map\CountryShippingMethodTableMap;
use Model\Setting\MasterTable\Map\CountryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'mt_country' table.
 *
 *
 *
 * @package    propel.generator.Model.Setting.MasterTable.Base
 */
abstract class Country implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Setting\\MasterTable\\Map\\CountryTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the name field.
     *
     * @var        string|null
     */
    protected $name;

    /**
     * The value for the shipping_zone_id field.
     *
     * @var        int|null
     */
    protected $shipping_zone_id;

    /**
     * The value for the iso2 field.
     *
     * @var        string|null
     */
    protected $iso2;

    /**
     * The value for the iso3 field.
     *
     * @var        string|null
     */
    protected $iso3;

    /**
     * The value for the is_eu field.
     *
     * @var        boolean|null
     */
    protected $is_eu;

    /**
     * The value for the stealth_mandatory field.
     *
     * @var        boolean|null
     */
    protected $stealth_mandatory;

    /**
     * The value for the stealth_advised field.
     *
     * @var        boolean|null
     */
    protected $stealth_advised;

    /**
     * The value for the latitude field.
     *
     * @var        string|null
     */
    protected $latitude;

    /**
     * The value for the longitude field.
     *
     * @var        string|null
     */
    protected $longitude;

    /**
     * @var        ChildShippingZone
     */
    protected $aShippingZone;

    /**
     * @var        ObjectCollection|Reseller[] Collection to store aggregation of Reseller objects.
     */
    protected $collResellers;
    protected $collResellersPartial;

    /**
     * @var        ObjectCollection|ChildCountryShippingMethod[] Collection to store aggregation of ChildCountryShippingMethod objects.
     */
    protected $collCountryShippingMethods;
    protected $collCountryShippingMethodsPartial;

    /**
     * @var        ObjectCollection|Company[] Collection to store aggregation of Company objects.
     */
    protected $collCompaniesRelatedByGeneralCountryId;
    protected $collCompaniesRelatedByGeneralCountryIdPartial;

    /**
     * @var        ObjectCollection|Company[] Collection to store aggregation of Company objects.
     */
    protected $collCompaniesRelatedByInvoiceCountryId;
    protected $collCompaniesRelatedByInvoiceCountryIdPartial;

    /**
     * @var        ObjectCollection|Company[] Collection to store aggregation of Company objects.
     */
    protected $collCompaniesRelatedByDeliveryCountryId;
    protected $collCompaniesRelatedByDeliveryCountryIdPartial;

    /**
     * @var        ObjectCollection|CustomerAddress[] Collection to store aggregation of CustomerAddress objects.
     */
    protected $collAddressCountries;
    protected $collAddressCountriesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Reseller[]
     */
    protected $resellersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCountryShippingMethod[]
     */
    protected $countryShippingMethodsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Company[]
     */
    protected $companiesRelatedByGeneralCountryIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Company[]
     */
    protected $companiesRelatedByInvoiceCountryIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Company[]
     */
    protected $companiesRelatedByDeliveryCountryIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|CustomerAddress[]
     */
    protected $addressCountriesScheduledForDeletion = null;

    /**
     * Initializes internal state of Model\Setting\MasterTable\Base\Country object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Country</code> instance.  If
     * <code>obj</code> is an instance of <code>Country</code>, delegates to
     * <code>equals(Country)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [shipping_zone_id] column value.
     *
     * @return int|null
     */
    public function getShippingZoneId()
    {
        return $this->shipping_zone_id;
    }

    /**
     * Get the [iso2] column value.
     *
     * @return string|null
     */
    public function getIso2()
    {
        return $this->iso2;
    }

    /**
     * Get the [iso3] column value.
     *
     * @return string|null
     */
    public function getIso3()
    {
        return $this->iso3;
    }

    /**
     * Get the [is_eu] column value.
     *
     * @return boolean|null
     */
    public function getIsEu()
    {
        return $this->is_eu;
    }

    /**
     * Get the [is_eu] column value.
     *
     * @return boolean|null
     */
    public function isEu()
    {
        return $this->getIsEu();
    }

    /**
     * Get the [stealth_mandatory] column value.
     *
     * @return boolean|null
     */
    public function getStealthMandatory()
    {
        return $this->stealth_mandatory;
    }

    /**
     * Get the [stealth_mandatory] column value.
     *
     * @return boolean|null
     */
    public function isStealthMandatory()
    {
        return $this->getStealthMandatory();
    }

    /**
     * Get the [stealth_advised] column value.
     *
     * @return boolean|null
     */
    public function getStealthAdvised()
    {
        return $this->stealth_advised;
    }

    /**
     * Get the [stealth_advised] column value.
     *
     * @return boolean|null
     */
    public function isStealthAdvised()
    {
        return $this->getStealthAdvised();
    }

    /**
     * Get the [latitude] column value.
     *
     * @return string|null
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Get the [longitude] column value.
     *
     * @return string|null
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[CountryTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[CountryTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [shipping_zone_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function setShippingZoneId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->shipping_zone_id !== $v) {
            $this->shipping_zone_id = $v;
            $this->modifiedColumns[CountryTableMap::COL_SHIPPING_ZONE_ID] = true;
        }

        if ($this->aShippingZone !== null && $this->aShippingZone->getId() !== $v) {
            $this->aShippingZone = null;
        }

        return $this;
    } // setShippingZoneId()

    /**
     * Set the value of [iso2] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function setIso2($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->iso2 !== $v) {
            $this->iso2 = $v;
            $this->modifiedColumns[CountryTableMap::COL_ISO2] = true;
        }

        return $this;
    } // setIso2()

    /**
     * Set the value of [iso3] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function setIso3($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->iso3 !== $v) {
            $this->iso3 = $v;
            $this->modifiedColumns[CountryTableMap::COL_ISO3] = true;
        }

        return $this;
    } // setIso3()

    /**
     * Sets the value of the [is_eu] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function setIsEu($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_eu !== $v) {
            $this->is_eu = $v;
            $this->modifiedColumns[CountryTableMap::COL_IS_EU] = true;
        }

        return $this;
    } // setIsEu()

    /**
     * Sets the value of the [stealth_mandatory] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function setStealthMandatory($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->stealth_mandatory !== $v) {
            $this->stealth_mandatory = $v;
            $this->modifiedColumns[CountryTableMap::COL_STEALTH_MANDATORY] = true;
        }

        return $this;
    } // setStealthMandatory()

    /**
     * Sets the value of the [stealth_advised] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function setStealthAdvised($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->stealth_advised !== $v) {
            $this->stealth_advised = $v;
            $this->modifiedColumns[CountryTableMap::COL_STEALTH_ADVISED] = true;
        }

        return $this;
    } // setStealthAdvised()

    /**
     * Set the value of [latitude] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function setLatitude($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->latitude !== $v) {
            $this->latitude = $v;
            $this->modifiedColumns[CountryTableMap::COL_LATITUDE] = true;
        }

        return $this;
    } // setLatitude()

    /**
     * Set the value of [longitude] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function setLongitude($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->longitude !== $v) {
            $this->longitude = $v;
            $this->modifiedColumns[CountryTableMap::COL_LONGITUDE] = true;
        }

        return $this;
    } // setLongitude()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CountryTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CountryTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CountryTableMap::translateFieldName('ShippingZoneId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shipping_zone_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CountryTableMap::translateFieldName('Iso2', TableMap::TYPE_PHPNAME, $indexType)];
            $this->iso2 = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CountryTableMap::translateFieldName('Iso3', TableMap::TYPE_PHPNAME, $indexType)];
            $this->iso3 = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CountryTableMap::translateFieldName('IsEu', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_eu = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CountryTableMap::translateFieldName('StealthMandatory', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stealth_mandatory = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CountryTableMap::translateFieldName('StealthAdvised', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stealth_advised = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CountryTableMap::translateFieldName('Latitude', TableMap::TYPE_PHPNAME, $indexType)];
            $this->latitude = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CountryTableMap::translateFieldName('Longitude', TableMap::TYPE_PHPNAME, $indexType)];
            $this->longitude = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = CountryTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Setting\\MasterTable\\Country'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aShippingZone !== null && $this->shipping_zone_id !== $this->aShippingZone->getId()) {
            $this->aShippingZone = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CountryTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCountryQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aShippingZone = null;
            $this->collResellers = null;

            $this->collCountryShippingMethods = null;

            $this->collCompaniesRelatedByGeneralCountryId = null;

            $this->collCompaniesRelatedByInvoiceCountryId = null;

            $this->collCompaniesRelatedByDeliveryCountryId = null;

            $this->collAddressCountries = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Country::setDeleted()
     * @see Country::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CountryTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCountryQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CountryTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CountryTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aShippingZone !== null) {
                if ($this->aShippingZone->isModified() || $this->aShippingZone->isNew()) {
                    $affectedRows += $this->aShippingZone->save($con);
                }
                $this->setShippingZone($this->aShippingZone);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->resellersScheduledForDeletion !== null) {
                if (!$this->resellersScheduledForDeletion->isEmpty()) {
                    \Model\Reseller\ResellerQuery::create()
                        ->filterByPrimaryKeys($this->resellersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->resellersScheduledForDeletion = null;
                }
            }

            if ($this->collResellers !== null) {
                foreach ($this->collResellers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->countryShippingMethodsScheduledForDeletion !== null) {
                if (!$this->countryShippingMethodsScheduledForDeletion->isEmpty()) {
                    \Model\Setting\MasterTable\CountryShippingMethodQuery::create()
                        ->filterByPrimaryKeys($this->countryShippingMethodsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->countryShippingMethodsScheduledForDeletion = null;
                }
            }

            if ($this->collCountryShippingMethods !== null) {
                foreach ($this->collCountryShippingMethods as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->companiesRelatedByGeneralCountryIdScheduledForDeletion !== null) {
                if (!$this->companiesRelatedByGeneralCountryIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->companiesRelatedByGeneralCountryIdScheduledForDeletion as $companyRelatedByGeneralCountryId) {
                        // need to save related object because we set the relation to null
                        $companyRelatedByGeneralCountryId->save($con);
                    }
                    $this->companiesRelatedByGeneralCountryIdScheduledForDeletion = null;
                }
            }

            if ($this->collCompaniesRelatedByGeneralCountryId !== null) {
                foreach ($this->collCompaniesRelatedByGeneralCountryId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->companiesRelatedByInvoiceCountryIdScheduledForDeletion !== null) {
                if (!$this->companiesRelatedByInvoiceCountryIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->companiesRelatedByInvoiceCountryIdScheduledForDeletion as $companyRelatedByInvoiceCountryId) {
                        // need to save related object because we set the relation to null
                        $companyRelatedByInvoiceCountryId->save($con);
                    }
                    $this->companiesRelatedByInvoiceCountryIdScheduledForDeletion = null;
                }
            }

            if ($this->collCompaniesRelatedByInvoiceCountryId !== null) {
                foreach ($this->collCompaniesRelatedByInvoiceCountryId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->companiesRelatedByDeliveryCountryIdScheduledForDeletion !== null) {
                if (!$this->companiesRelatedByDeliveryCountryIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->companiesRelatedByDeliveryCountryIdScheduledForDeletion as $companyRelatedByDeliveryCountryId) {
                        // need to save related object because we set the relation to null
                        $companyRelatedByDeliveryCountryId->save($con);
                    }
                    $this->companiesRelatedByDeliveryCountryIdScheduledForDeletion = null;
                }
            }

            if ($this->collCompaniesRelatedByDeliveryCountryId !== null) {
                foreach ($this->collCompaniesRelatedByDeliveryCountryId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->addressCountriesScheduledForDeletion !== null) {
                if (!$this->addressCountriesScheduledForDeletion->isEmpty()) {
                    foreach ($this->addressCountriesScheduledForDeletion as $addressCountry) {
                        // need to save related object because we set the relation to null
                        $addressCountry->save($con);
                    }
                    $this->addressCountriesScheduledForDeletion = null;
                }
            }

            if ($this->collAddressCountries !== null) {
                foreach ($this->collAddressCountries as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CountryTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CountryTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CountryTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(CountryTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(CountryTableMap::COL_SHIPPING_ZONE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'shipping_zone_id';
        }
        if ($this->isColumnModified(CountryTableMap::COL_ISO2)) {
            $modifiedColumns[':p' . $index++]  = 'iso2';
        }
        if ($this->isColumnModified(CountryTableMap::COL_ISO3)) {
            $modifiedColumns[':p' . $index++]  = 'iso3';
        }
        if ($this->isColumnModified(CountryTableMap::COL_IS_EU)) {
            $modifiedColumns[':p' . $index++]  = 'is_eu';
        }
        if ($this->isColumnModified(CountryTableMap::COL_STEALTH_MANDATORY)) {
            $modifiedColumns[':p' . $index++]  = 'stealth_mandatory';
        }
        if ($this->isColumnModified(CountryTableMap::COL_STEALTH_ADVISED)) {
            $modifiedColumns[':p' . $index++]  = 'stealth_advised';
        }
        if ($this->isColumnModified(CountryTableMap::COL_LATITUDE)) {
            $modifiedColumns[':p' . $index++]  = 'latitude';
        }
        if ($this->isColumnModified(CountryTableMap::COL_LONGITUDE)) {
            $modifiedColumns[':p' . $index++]  = 'longitude';
        }

        $sql = sprintf(
            'INSERT INTO mt_country (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'shipping_zone_id':
                        $stmt->bindValue($identifier, $this->shipping_zone_id, PDO::PARAM_INT);
                        break;
                    case 'iso2':
                        $stmt->bindValue($identifier, $this->iso2, PDO::PARAM_STR);
                        break;
                    case 'iso3':
                        $stmt->bindValue($identifier, $this->iso3, PDO::PARAM_STR);
                        break;
                    case 'is_eu':
                        $stmt->bindValue($identifier, (int) $this->is_eu, PDO::PARAM_INT);
                        break;
                    case 'stealth_mandatory':
                        $stmt->bindValue($identifier, (int) $this->stealth_mandatory, PDO::PARAM_INT);
                        break;
                    case 'stealth_advised':
                        $stmt->bindValue($identifier, (int) $this->stealth_advised, PDO::PARAM_INT);
                        break;
                    case 'latitude':
                        $stmt->bindValue($identifier, $this->latitude, PDO::PARAM_STR);
                        break;
                    case 'longitude':
                        $stmt->bindValue($identifier, $this->longitude, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CountryTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getShippingZoneId();
                break;
            case 3:
                return $this->getIso2();
                break;
            case 4:
                return $this->getIso3();
                break;
            case 5:
                return $this->getIsEu();
                break;
            case 6:
                return $this->getStealthMandatory();
                break;
            case 7:
                return $this->getStealthAdvised();
                break;
            case 8:
                return $this->getLatitude();
                break;
            case 9:
                return $this->getLongitude();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Country'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Country'][$this->hashCode()] = true;
        $keys = CountryTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getShippingZoneId(),
            $keys[3] => $this->getIso2(),
            $keys[4] => $this->getIso3(),
            $keys[5] => $this->getIsEu(),
            $keys[6] => $this->getStealthMandatory(),
            $keys[7] => $this->getStealthAdvised(),
            $keys[8] => $this->getLatitude(),
            $keys[9] => $this->getLongitude(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aShippingZone) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'shippingZone';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_shipping_zone';
                        break;
                    default:
                        $key = 'ShippingZone';
                }

                $result[$key] = $this->aShippingZone->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collResellers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'resellers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'resellers';
                        break;
                    default:
                        $key = 'Resellers';
                }

                $result[$key] = $this->collResellers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCountryShippingMethods) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'countryShippingMethods';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'country_shipping_methods';
                        break;
                    default:
                        $key = 'CountryShippingMethods';
                }

                $result[$key] = $this->collCountryShippingMethods->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCompaniesRelatedByGeneralCountryId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'companies';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'own_companies';
                        break;
                    default:
                        $key = 'Companies';
                }

                $result[$key] = $this->collCompaniesRelatedByGeneralCountryId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCompaniesRelatedByInvoiceCountryId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'companies';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'own_companies';
                        break;
                    default:
                        $key = 'Companies';
                }

                $result[$key] = $this->collCompaniesRelatedByInvoiceCountryId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCompaniesRelatedByDeliveryCountryId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'companies';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'own_companies';
                        break;
                    default:
                        $key = 'Companies';
                }

                $result[$key] = $this->collCompaniesRelatedByDeliveryCountryId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collAddressCountries) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customerAddresses';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'customer_addresses';
                        break;
                    default:
                        $key = 'AddressCountries';
                }

                $result[$key] = $this->collAddressCountries->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Setting\MasterTable\Country
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CountryTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Setting\MasterTable\Country
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setShippingZoneId($value);
                break;
            case 3:
                $this->setIso2($value);
                break;
            case 4:
                $this->setIso3($value);
                break;
            case 5:
                $this->setIsEu($value);
                break;
            case 6:
                $this->setStealthMandatory($value);
                break;
            case 7:
                $this->setStealthAdvised($value);
                break;
            case 8:
                $this->setLatitude($value);
                break;
            case 9:
                $this->setLongitude($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CountryTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setShippingZoneId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setIso2($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setIso3($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setIsEu($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setStealthMandatory($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setStealthAdvised($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setLatitude($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setLongitude($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Setting\MasterTable\Country The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CountryTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CountryTableMap::COL_ID)) {
            $criteria->add(CountryTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(CountryTableMap::COL_NAME)) {
            $criteria->add(CountryTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(CountryTableMap::COL_SHIPPING_ZONE_ID)) {
            $criteria->add(CountryTableMap::COL_SHIPPING_ZONE_ID, $this->shipping_zone_id);
        }
        if ($this->isColumnModified(CountryTableMap::COL_ISO2)) {
            $criteria->add(CountryTableMap::COL_ISO2, $this->iso2);
        }
        if ($this->isColumnModified(CountryTableMap::COL_ISO3)) {
            $criteria->add(CountryTableMap::COL_ISO3, $this->iso3);
        }
        if ($this->isColumnModified(CountryTableMap::COL_IS_EU)) {
            $criteria->add(CountryTableMap::COL_IS_EU, $this->is_eu);
        }
        if ($this->isColumnModified(CountryTableMap::COL_STEALTH_MANDATORY)) {
            $criteria->add(CountryTableMap::COL_STEALTH_MANDATORY, $this->stealth_mandatory);
        }
        if ($this->isColumnModified(CountryTableMap::COL_STEALTH_ADVISED)) {
            $criteria->add(CountryTableMap::COL_STEALTH_ADVISED, $this->stealth_advised);
        }
        if ($this->isColumnModified(CountryTableMap::COL_LATITUDE)) {
            $criteria->add(CountryTableMap::COL_LATITUDE, $this->latitude);
        }
        if ($this->isColumnModified(CountryTableMap::COL_LONGITUDE)) {
            $criteria->add(CountryTableMap::COL_LONGITUDE, $this->longitude);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCountryQuery::create();
        $criteria->add(CountryTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Setting\MasterTable\Country (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setShippingZoneId($this->getShippingZoneId());
        $copyObj->setIso2($this->getIso2());
        $copyObj->setIso3($this->getIso3());
        $copyObj->setIsEu($this->getIsEu());
        $copyObj->setStealthMandatory($this->getStealthMandatory());
        $copyObj->setStealthAdvised($this->getStealthAdvised());
        $copyObj->setLatitude($this->getLatitude());
        $copyObj->setLongitude($this->getLongitude());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getResellers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addReseller($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCountryShippingMethods() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCountryShippingMethod($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCompaniesRelatedByGeneralCountryId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCompanyRelatedByGeneralCountryId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCompaniesRelatedByInvoiceCountryId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCompanyRelatedByInvoiceCountryId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCompaniesRelatedByDeliveryCountryId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCompanyRelatedByDeliveryCountryId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getAddressCountries() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addAddressCountry($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Setting\MasterTable\Country Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildShippingZone object.
     *
     * @param  ChildShippingZone|null $v
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     * @throws PropelException
     */
    public function setShippingZone(ChildShippingZone $v = null)
    {
        if ($v === null) {
            $this->setShippingZoneId(NULL);
        } else {
            $this->setShippingZoneId($v->getId());
        }

        $this->aShippingZone = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildShippingZone object, it will not be re-added.
        if ($v !== null) {
            $v->addCountry($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildShippingZone object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildShippingZone|null The associated ChildShippingZone object.
     * @throws PropelException
     */
    public function getShippingZone(ConnectionInterface $con = null)
    {
        if ($this->aShippingZone === null && ($this->shipping_zone_id != 0)) {
            $this->aShippingZone = ChildShippingZoneQuery::create()->findPk($this->shipping_zone_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aShippingZone->addCountries($this);
             */
        }

        return $this->aShippingZone;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Reseller' === $relationName) {
            $this->initResellers();
            return;
        }
        if ('CountryShippingMethod' === $relationName) {
            $this->initCountryShippingMethods();
            return;
        }
        if ('CompanyRelatedByGeneralCountryId' === $relationName) {
            $this->initCompaniesRelatedByGeneralCountryId();
            return;
        }
        if ('CompanyRelatedByInvoiceCountryId' === $relationName) {
            $this->initCompaniesRelatedByInvoiceCountryId();
            return;
        }
        if ('CompanyRelatedByDeliveryCountryId' === $relationName) {
            $this->initCompaniesRelatedByDeliveryCountryId();
            return;
        }
        if ('AddressCountry' === $relationName) {
            $this->initAddressCountries();
            return;
        }
    }

    /**
     * Clears out the collResellers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addResellers()
     */
    public function clearResellers()
    {
        $this->collResellers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collResellers collection loaded partially.
     */
    public function resetPartialResellers($v = true)
    {
        $this->collResellersPartial = $v;
    }

    /**
     * Initializes the collResellers collection.
     *
     * By default this just sets the collResellers collection to an empty array (like clearcollResellers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initResellers($overrideExisting = true)
    {
        if (null !== $this->collResellers && !$overrideExisting) {
            return;
        }

        $collectionClassName = ResellerTableMap::getTableMap()->getCollectionClassName();

        $this->collResellers = new $collectionClassName;
        $this->collResellers->setModel('\Model\Reseller\Reseller');
    }

    /**
     * Gets an array of Reseller objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCountry is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Reseller[] List of Reseller objects
     * @throws PropelException
     */
    public function getResellers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collResellersPartial && !$this->isNew();
        if (null === $this->collResellers || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collResellers) {
                    $this->initResellers();
                } else {
                    $collectionClassName = ResellerTableMap::getTableMap()->getCollectionClassName();

                    $collResellers = new $collectionClassName;
                    $collResellers->setModel('\Model\Reseller\Reseller');

                    return $collResellers;
                }
            } else {
                $collResellers = ResellerQuery::create(null, $criteria)
                    ->filterByCountry($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collResellersPartial && count($collResellers)) {
                        $this->initResellers(false);

                        foreach ($collResellers as $obj) {
                            if (false == $this->collResellers->contains($obj)) {
                                $this->collResellers->append($obj);
                            }
                        }

                        $this->collResellersPartial = true;
                    }

                    return $collResellers;
                }

                if ($partial && $this->collResellers) {
                    foreach ($this->collResellers as $obj) {
                        if ($obj->isNew()) {
                            $collResellers[] = $obj;
                        }
                    }
                }

                $this->collResellers = $collResellers;
                $this->collResellersPartial = false;
            }
        }

        return $this->collResellers;
    }

    /**
     * Sets a collection of Reseller objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $resellers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCountry The current object (for fluent API support)
     */
    public function setResellers(Collection $resellers, ConnectionInterface $con = null)
    {
        /** @var Reseller[] $resellersToDelete */
        $resellersToDelete = $this->getResellers(new Criteria(), $con)->diff($resellers);


        $this->resellersScheduledForDeletion = $resellersToDelete;

        foreach ($resellersToDelete as $resellerRemoved) {
            $resellerRemoved->setCountry(null);
        }

        $this->collResellers = null;
        foreach ($resellers as $reseller) {
            $this->addReseller($reseller);
        }

        $this->collResellers = $resellers;
        $this->collResellersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseReseller objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseReseller objects.
     * @throws PropelException
     */
    public function countResellers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collResellersPartial && !$this->isNew();
        if (null === $this->collResellers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collResellers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getResellers());
            }

            $query = ResellerQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCountry($this)
                ->count($con);
        }

        return count($this->collResellers);
    }

    /**
     * Method called to associate a Reseller object to this object
     * through the Reseller foreign key attribute.
     *
     * @param  Reseller $l Reseller
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function addReseller(Reseller $l)
    {
        if ($this->collResellers === null) {
            $this->initResellers();
            $this->collResellersPartial = true;
        }

        if (!$this->collResellers->contains($l)) {
            $this->doAddReseller($l);

            if ($this->resellersScheduledForDeletion and $this->resellersScheduledForDeletion->contains($l)) {
                $this->resellersScheduledForDeletion->remove($this->resellersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Reseller $reseller The Reseller object to add.
     */
    protected function doAddReseller(Reseller $reseller)
    {
        $this->collResellers[]= $reseller;
        $reseller->setCountry($this);
    }

    /**
     * @param  Reseller $reseller The Reseller object to remove.
     * @return $this|ChildCountry The current object (for fluent API support)
     */
    public function removeReseller(Reseller $reseller)
    {
        if ($this->getResellers()->contains($reseller)) {
            $pos = $this->collResellers->search($reseller);
            $this->collResellers->remove($pos);
            if (null === $this->resellersScheduledForDeletion) {
                $this->resellersScheduledForDeletion = clone $this->collResellers;
                $this->resellersScheduledForDeletion->clear();
            }
            $this->resellersScheduledForDeletion[]= $reseller;
            $reseller->setCountry(null);
        }

        return $this;
    }

    /**
     * Clears out the collCountryShippingMethods collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCountryShippingMethods()
     */
    public function clearCountryShippingMethods()
    {
        $this->collCountryShippingMethods = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCountryShippingMethods collection loaded partially.
     */
    public function resetPartialCountryShippingMethods($v = true)
    {
        $this->collCountryShippingMethodsPartial = $v;
    }

    /**
     * Initializes the collCountryShippingMethods collection.
     *
     * By default this just sets the collCountryShippingMethods collection to an empty array (like clearcollCountryShippingMethods());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCountryShippingMethods($overrideExisting = true)
    {
        if (null !== $this->collCountryShippingMethods && !$overrideExisting) {
            return;
        }

        $collectionClassName = CountryShippingMethodTableMap::getTableMap()->getCollectionClassName();

        $this->collCountryShippingMethods = new $collectionClassName;
        $this->collCountryShippingMethods->setModel('\Model\Setting\MasterTable\CountryShippingMethod');
    }

    /**
     * Gets an array of ChildCountryShippingMethod objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCountry is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCountryShippingMethod[] List of ChildCountryShippingMethod objects
     * @throws PropelException
     */
    public function getCountryShippingMethods(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCountryShippingMethodsPartial && !$this->isNew();
        if (null === $this->collCountryShippingMethods || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCountryShippingMethods) {
                    $this->initCountryShippingMethods();
                } else {
                    $collectionClassName = CountryShippingMethodTableMap::getTableMap()->getCollectionClassName();

                    $collCountryShippingMethods = new $collectionClassName;
                    $collCountryShippingMethods->setModel('\Model\Setting\MasterTable\CountryShippingMethod');

                    return $collCountryShippingMethods;
                }
            } else {
                $collCountryShippingMethods = ChildCountryShippingMethodQuery::create(null, $criteria)
                    ->filterByCountry($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCountryShippingMethodsPartial && count($collCountryShippingMethods)) {
                        $this->initCountryShippingMethods(false);

                        foreach ($collCountryShippingMethods as $obj) {
                            if (false == $this->collCountryShippingMethods->contains($obj)) {
                                $this->collCountryShippingMethods->append($obj);
                            }
                        }

                        $this->collCountryShippingMethodsPartial = true;
                    }

                    return $collCountryShippingMethods;
                }

                if ($partial && $this->collCountryShippingMethods) {
                    foreach ($this->collCountryShippingMethods as $obj) {
                        if ($obj->isNew()) {
                            $collCountryShippingMethods[] = $obj;
                        }
                    }
                }

                $this->collCountryShippingMethods = $collCountryShippingMethods;
                $this->collCountryShippingMethodsPartial = false;
            }
        }

        return $this->collCountryShippingMethods;
    }

    /**
     * Sets a collection of ChildCountryShippingMethod objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $countryShippingMethods A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCountry The current object (for fluent API support)
     */
    public function setCountryShippingMethods(Collection $countryShippingMethods, ConnectionInterface $con = null)
    {
        /** @var ChildCountryShippingMethod[] $countryShippingMethodsToDelete */
        $countryShippingMethodsToDelete = $this->getCountryShippingMethods(new Criteria(), $con)->diff($countryShippingMethods);


        $this->countryShippingMethodsScheduledForDeletion = $countryShippingMethodsToDelete;

        foreach ($countryShippingMethodsToDelete as $countryShippingMethodRemoved) {
            $countryShippingMethodRemoved->setCountry(null);
        }

        $this->collCountryShippingMethods = null;
        foreach ($countryShippingMethods as $countryShippingMethod) {
            $this->addCountryShippingMethod($countryShippingMethod);
        }

        $this->collCountryShippingMethods = $countryShippingMethods;
        $this->collCountryShippingMethodsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CountryShippingMethod objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CountryShippingMethod objects.
     * @throws PropelException
     */
    public function countCountryShippingMethods(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCountryShippingMethodsPartial && !$this->isNew();
        if (null === $this->collCountryShippingMethods || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCountryShippingMethods) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCountryShippingMethods());
            }

            $query = ChildCountryShippingMethodQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCountry($this)
                ->count($con);
        }

        return count($this->collCountryShippingMethods);
    }

    /**
     * Method called to associate a ChildCountryShippingMethod object to this object
     * through the ChildCountryShippingMethod foreign key attribute.
     *
     * @param  ChildCountryShippingMethod $l ChildCountryShippingMethod
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function addCountryShippingMethod(ChildCountryShippingMethod $l)
    {
        if ($this->collCountryShippingMethods === null) {
            $this->initCountryShippingMethods();
            $this->collCountryShippingMethodsPartial = true;
        }

        if (!$this->collCountryShippingMethods->contains($l)) {
            $this->doAddCountryShippingMethod($l);

            if ($this->countryShippingMethodsScheduledForDeletion and $this->countryShippingMethodsScheduledForDeletion->contains($l)) {
                $this->countryShippingMethodsScheduledForDeletion->remove($this->countryShippingMethodsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCountryShippingMethod $countryShippingMethod The ChildCountryShippingMethod object to add.
     */
    protected function doAddCountryShippingMethod(ChildCountryShippingMethod $countryShippingMethod)
    {
        $this->collCountryShippingMethods[]= $countryShippingMethod;
        $countryShippingMethod->setCountry($this);
    }

    /**
     * @param  ChildCountryShippingMethod $countryShippingMethod The ChildCountryShippingMethod object to remove.
     * @return $this|ChildCountry The current object (for fluent API support)
     */
    public function removeCountryShippingMethod(ChildCountryShippingMethod $countryShippingMethod)
    {
        if ($this->getCountryShippingMethods()->contains($countryShippingMethod)) {
            $pos = $this->collCountryShippingMethods->search($countryShippingMethod);
            $this->collCountryShippingMethods->remove($pos);
            if (null === $this->countryShippingMethodsScheduledForDeletion) {
                $this->countryShippingMethodsScheduledForDeletion = clone $this->collCountryShippingMethods;
                $this->countryShippingMethodsScheduledForDeletion->clear();
            }
            $this->countryShippingMethodsScheduledForDeletion[]= clone $countryShippingMethod;
            $countryShippingMethod->setCountry(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related CountryShippingMethods from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCountryShippingMethod[] List of ChildCountryShippingMethod objects
     */
    public function getCountryShippingMethodsJoinShippingMethod(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCountryShippingMethodQuery::create(null, $criteria);
        $query->joinWith('ShippingMethod', $joinBehavior);

        return $this->getCountryShippingMethods($query, $con);
    }

    /**
     * Clears out the collCompaniesRelatedByGeneralCountryId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCompaniesRelatedByGeneralCountryId()
     */
    public function clearCompaniesRelatedByGeneralCountryId()
    {
        $this->collCompaniesRelatedByGeneralCountryId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCompaniesRelatedByGeneralCountryId collection loaded partially.
     */
    public function resetPartialCompaniesRelatedByGeneralCountryId($v = true)
    {
        $this->collCompaniesRelatedByGeneralCountryIdPartial = $v;
    }

    /**
     * Initializes the collCompaniesRelatedByGeneralCountryId collection.
     *
     * By default this just sets the collCompaniesRelatedByGeneralCountryId collection to an empty array (like clearcollCompaniesRelatedByGeneralCountryId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCompaniesRelatedByGeneralCountryId($overrideExisting = true)
    {
        if (null !== $this->collCompaniesRelatedByGeneralCountryId && !$overrideExisting) {
            return;
        }

        $collectionClassName = CompanyTableMap::getTableMap()->getCollectionClassName();

        $this->collCompaniesRelatedByGeneralCountryId = new $collectionClassName;
        $this->collCompaniesRelatedByGeneralCountryId->setModel('\Model\Company\Company');
    }

    /**
     * Gets an array of Company objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCountry is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Company[] List of Company objects
     * @throws PropelException
     */
    public function getCompaniesRelatedByGeneralCountryId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCompaniesRelatedByGeneralCountryIdPartial && !$this->isNew();
        if (null === $this->collCompaniesRelatedByGeneralCountryId || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCompaniesRelatedByGeneralCountryId) {
                    $this->initCompaniesRelatedByGeneralCountryId();
                } else {
                    $collectionClassName = CompanyTableMap::getTableMap()->getCollectionClassName();

                    $collCompaniesRelatedByGeneralCountryId = new $collectionClassName;
                    $collCompaniesRelatedByGeneralCountryId->setModel('\Model\Company\Company');

                    return $collCompaniesRelatedByGeneralCountryId;
                }
            } else {
                $collCompaniesRelatedByGeneralCountryId = CompanyQuery::create(null, $criteria)
                    ->filterByGeneralCountry($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCompaniesRelatedByGeneralCountryIdPartial && count($collCompaniesRelatedByGeneralCountryId)) {
                        $this->initCompaniesRelatedByGeneralCountryId(false);

                        foreach ($collCompaniesRelatedByGeneralCountryId as $obj) {
                            if (false == $this->collCompaniesRelatedByGeneralCountryId->contains($obj)) {
                                $this->collCompaniesRelatedByGeneralCountryId->append($obj);
                            }
                        }

                        $this->collCompaniesRelatedByGeneralCountryIdPartial = true;
                    }

                    return $collCompaniesRelatedByGeneralCountryId;
                }

                if ($partial && $this->collCompaniesRelatedByGeneralCountryId) {
                    foreach ($this->collCompaniesRelatedByGeneralCountryId as $obj) {
                        if ($obj->isNew()) {
                            $collCompaniesRelatedByGeneralCountryId[] = $obj;
                        }
                    }
                }

                $this->collCompaniesRelatedByGeneralCountryId = $collCompaniesRelatedByGeneralCountryId;
                $this->collCompaniesRelatedByGeneralCountryIdPartial = false;
            }
        }

        return $this->collCompaniesRelatedByGeneralCountryId;
    }

    /**
     * Sets a collection of Company objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $companiesRelatedByGeneralCountryId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCountry The current object (for fluent API support)
     */
    public function setCompaniesRelatedByGeneralCountryId(Collection $companiesRelatedByGeneralCountryId, ConnectionInterface $con = null)
    {
        /** @var Company[] $companiesRelatedByGeneralCountryIdToDelete */
        $companiesRelatedByGeneralCountryIdToDelete = $this->getCompaniesRelatedByGeneralCountryId(new Criteria(), $con)->diff($companiesRelatedByGeneralCountryId);


        $this->companiesRelatedByGeneralCountryIdScheduledForDeletion = $companiesRelatedByGeneralCountryIdToDelete;

        foreach ($companiesRelatedByGeneralCountryIdToDelete as $companyRelatedByGeneralCountryIdRemoved) {
            $companyRelatedByGeneralCountryIdRemoved->setGeneralCountry(null);
        }

        $this->collCompaniesRelatedByGeneralCountryId = null;
        foreach ($companiesRelatedByGeneralCountryId as $companyRelatedByGeneralCountryId) {
            $this->addCompanyRelatedByGeneralCountryId($companyRelatedByGeneralCountryId);
        }

        $this->collCompaniesRelatedByGeneralCountryId = $companiesRelatedByGeneralCountryId;
        $this->collCompaniesRelatedByGeneralCountryIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseCompany objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseCompany objects.
     * @throws PropelException
     */
    public function countCompaniesRelatedByGeneralCountryId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCompaniesRelatedByGeneralCountryIdPartial && !$this->isNew();
        if (null === $this->collCompaniesRelatedByGeneralCountryId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCompaniesRelatedByGeneralCountryId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCompaniesRelatedByGeneralCountryId());
            }

            $query = CompanyQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByGeneralCountry($this)
                ->count($con);
        }

        return count($this->collCompaniesRelatedByGeneralCountryId);
    }

    /**
     * Method called to associate a Company object to this object
     * through the Company foreign key attribute.
     *
     * @param  Company $l Company
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function addCompanyRelatedByGeneralCountryId(Company $l)
    {
        if ($this->collCompaniesRelatedByGeneralCountryId === null) {
            $this->initCompaniesRelatedByGeneralCountryId();
            $this->collCompaniesRelatedByGeneralCountryIdPartial = true;
        }

        if (!$this->collCompaniesRelatedByGeneralCountryId->contains($l)) {
            $this->doAddCompanyRelatedByGeneralCountryId($l);

            if ($this->companiesRelatedByGeneralCountryIdScheduledForDeletion and $this->companiesRelatedByGeneralCountryIdScheduledForDeletion->contains($l)) {
                $this->companiesRelatedByGeneralCountryIdScheduledForDeletion->remove($this->companiesRelatedByGeneralCountryIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Company $companyRelatedByGeneralCountryId The Company object to add.
     */
    protected function doAddCompanyRelatedByGeneralCountryId(Company $companyRelatedByGeneralCountryId)
    {
        $this->collCompaniesRelatedByGeneralCountryId[]= $companyRelatedByGeneralCountryId;
        $companyRelatedByGeneralCountryId->setGeneralCountry($this);
    }

    /**
     * @param  Company $companyRelatedByGeneralCountryId The Company object to remove.
     * @return $this|ChildCountry The current object (for fluent API support)
     */
    public function removeCompanyRelatedByGeneralCountryId(Company $companyRelatedByGeneralCountryId)
    {
        if ($this->getCompaniesRelatedByGeneralCountryId()->contains($companyRelatedByGeneralCountryId)) {
            $pos = $this->collCompaniesRelatedByGeneralCountryId->search($companyRelatedByGeneralCountryId);
            $this->collCompaniesRelatedByGeneralCountryId->remove($pos);
            if (null === $this->companiesRelatedByGeneralCountryIdScheduledForDeletion) {
                $this->companiesRelatedByGeneralCountryIdScheduledForDeletion = clone $this->collCompaniesRelatedByGeneralCountryId;
                $this->companiesRelatedByGeneralCountryIdScheduledForDeletion->clear();
            }
            $this->companiesRelatedByGeneralCountryIdScheduledForDeletion[]= $companyRelatedByGeneralCountryId;
            $companyRelatedByGeneralCountryId->setGeneralCountry(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related CompaniesRelatedByGeneralCountryId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Company[] List of Company objects
     */
    public function getCompaniesRelatedByGeneralCountryIdJoinLegalForm(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CompanyQuery::create(null, $criteria);
        $query->joinWith('LegalForm', $joinBehavior);

        return $this->getCompaniesRelatedByGeneralCountryId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related CompaniesRelatedByGeneralCountryId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Company[] List of Company objects
     */
    public function getCompaniesRelatedByGeneralCountryIdJoinOwnCompanyLogo(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CompanyQuery::create(null, $criteria);
        $query->joinWith('OwnCompanyLogo', $joinBehavior);

        return $this->getCompaniesRelatedByGeneralCountryId($query, $con);
    }

    /**
     * Clears out the collCompaniesRelatedByInvoiceCountryId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCompaniesRelatedByInvoiceCountryId()
     */
    public function clearCompaniesRelatedByInvoiceCountryId()
    {
        $this->collCompaniesRelatedByInvoiceCountryId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCompaniesRelatedByInvoiceCountryId collection loaded partially.
     */
    public function resetPartialCompaniesRelatedByInvoiceCountryId($v = true)
    {
        $this->collCompaniesRelatedByInvoiceCountryIdPartial = $v;
    }

    /**
     * Initializes the collCompaniesRelatedByInvoiceCountryId collection.
     *
     * By default this just sets the collCompaniesRelatedByInvoiceCountryId collection to an empty array (like clearcollCompaniesRelatedByInvoiceCountryId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCompaniesRelatedByInvoiceCountryId($overrideExisting = true)
    {
        if (null !== $this->collCompaniesRelatedByInvoiceCountryId && !$overrideExisting) {
            return;
        }

        $collectionClassName = CompanyTableMap::getTableMap()->getCollectionClassName();

        $this->collCompaniesRelatedByInvoiceCountryId = new $collectionClassName;
        $this->collCompaniesRelatedByInvoiceCountryId->setModel('\Model\Company\Company');
    }

    /**
     * Gets an array of Company objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCountry is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Company[] List of Company objects
     * @throws PropelException
     */
    public function getCompaniesRelatedByInvoiceCountryId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCompaniesRelatedByInvoiceCountryIdPartial && !$this->isNew();
        if (null === $this->collCompaniesRelatedByInvoiceCountryId || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCompaniesRelatedByInvoiceCountryId) {
                    $this->initCompaniesRelatedByInvoiceCountryId();
                } else {
                    $collectionClassName = CompanyTableMap::getTableMap()->getCollectionClassName();

                    $collCompaniesRelatedByInvoiceCountryId = new $collectionClassName;
                    $collCompaniesRelatedByInvoiceCountryId->setModel('\Model\Company\Company');

                    return $collCompaniesRelatedByInvoiceCountryId;
                }
            } else {
                $collCompaniesRelatedByInvoiceCountryId = CompanyQuery::create(null, $criteria)
                    ->filterByInvoiceCountry($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCompaniesRelatedByInvoiceCountryIdPartial && count($collCompaniesRelatedByInvoiceCountryId)) {
                        $this->initCompaniesRelatedByInvoiceCountryId(false);

                        foreach ($collCompaniesRelatedByInvoiceCountryId as $obj) {
                            if (false == $this->collCompaniesRelatedByInvoiceCountryId->contains($obj)) {
                                $this->collCompaniesRelatedByInvoiceCountryId->append($obj);
                            }
                        }

                        $this->collCompaniesRelatedByInvoiceCountryIdPartial = true;
                    }

                    return $collCompaniesRelatedByInvoiceCountryId;
                }

                if ($partial && $this->collCompaniesRelatedByInvoiceCountryId) {
                    foreach ($this->collCompaniesRelatedByInvoiceCountryId as $obj) {
                        if ($obj->isNew()) {
                            $collCompaniesRelatedByInvoiceCountryId[] = $obj;
                        }
                    }
                }

                $this->collCompaniesRelatedByInvoiceCountryId = $collCompaniesRelatedByInvoiceCountryId;
                $this->collCompaniesRelatedByInvoiceCountryIdPartial = false;
            }
        }

        return $this->collCompaniesRelatedByInvoiceCountryId;
    }

    /**
     * Sets a collection of Company objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $companiesRelatedByInvoiceCountryId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCountry The current object (for fluent API support)
     */
    public function setCompaniesRelatedByInvoiceCountryId(Collection $companiesRelatedByInvoiceCountryId, ConnectionInterface $con = null)
    {
        /** @var Company[] $companiesRelatedByInvoiceCountryIdToDelete */
        $companiesRelatedByInvoiceCountryIdToDelete = $this->getCompaniesRelatedByInvoiceCountryId(new Criteria(), $con)->diff($companiesRelatedByInvoiceCountryId);


        $this->companiesRelatedByInvoiceCountryIdScheduledForDeletion = $companiesRelatedByInvoiceCountryIdToDelete;

        foreach ($companiesRelatedByInvoiceCountryIdToDelete as $companyRelatedByInvoiceCountryIdRemoved) {
            $companyRelatedByInvoiceCountryIdRemoved->setInvoiceCountry(null);
        }

        $this->collCompaniesRelatedByInvoiceCountryId = null;
        foreach ($companiesRelatedByInvoiceCountryId as $companyRelatedByInvoiceCountryId) {
            $this->addCompanyRelatedByInvoiceCountryId($companyRelatedByInvoiceCountryId);
        }

        $this->collCompaniesRelatedByInvoiceCountryId = $companiesRelatedByInvoiceCountryId;
        $this->collCompaniesRelatedByInvoiceCountryIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseCompany objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseCompany objects.
     * @throws PropelException
     */
    public function countCompaniesRelatedByInvoiceCountryId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCompaniesRelatedByInvoiceCountryIdPartial && !$this->isNew();
        if (null === $this->collCompaniesRelatedByInvoiceCountryId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCompaniesRelatedByInvoiceCountryId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCompaniesRelatedByInvoiceCountryId());
            }

            $query = CompanyQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByInvoiceCountry($this)
                ->count($con);
        }

        return count($this->collCompaniesRelatedByInvoiceCountryId);
    }

    /**
     * Method called to associate a Company object to this object
     * through the Company foreign key attribute.
     *
     * @param  Company $l Company
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function addCompanyRelatedByInvoiceCountryId(Company $l)
    {
        if ($this->collCompaniesRelatedByInvoiceCountryId === null) {
            $this->initCompaniesRelatedByInvoiceCountryId();
            $this->collCompaniesRelatedByInvoiceCountryIdPartial = true;
        }

        if (!$this->collCompaniesRelatedByInvoiceCountryId->contains($l)) {
            $this->doAddCompanyRelatedByInvoiceCountryId($l);

            if ($this->companiesRelatedByInvoiceCountryIdScheduledForDeletion and $this->companiesRelatedByInvoiceCountryIdScheduledForDeletion->contains($l)) {
                $this->companiesRelatedByInvoiceCountryIdScheduledForDeletion->remove($this->companiesRelatedByInvoiceCountryIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Company $companyRelatedByInvoiceCountryId The Company object to add.
     */
    protected function doAddCompanyRelatedByInvoiceCountryId(Company $companyRelatedByInvoiceCountryId)
    {
        $this->collCompaniesRelatedByInvoiceCountryId[]= $companyRelatedByInvoiceCountryId;
        $companyRelatedByInvoiceCountryId->setInvoiceCountry($this);
    }

    /**
     * @param  Company $companyRelatedByInvoiceCountryId The Company object to remove.
     * @return $this|ChildCountry The current object (for fluent API support)
     */
    public function removeCompanyRelatedByInvoiceCountryId(Company $companyRelatedByInvoiceCountryId)
    {
        if ($this->getCompaniesRelatedByInvoiceCountryId()->contains($companyRelatedByInvoiceCountryId)) {
            $pos = $this->collCompaniesRelatedByInvoiceCountryId->search($companyRelatedByInvoiceCountryId);
            $this->collCompaniesRelatedByInvoiceCountryId->remove($pos);
            if (null === $this->companiesRelatedByInvoiceCountryIdScheduledForDeletion) {
                $this->companiesRelatedByInvoiceCountryIdScheduledForDeletion = clone $this->collCompaniesRelatedByInvoiceCountryId;
                $this->companiesRelatedByInvoiceCountryIdScheduledForDeletion->clear();
            }
            $this->companiesRelatedByInvoiceCountryIdScheduledForDeletion[]= $companyRelatedByInvoiceCountryId;
            $companyRelatedByInvoiceCountryId->setInvoiceCountry(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related CompaniesRelatedByInvoiceCountryId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Company[] List of Company objects
     */
    public function getCompaniesRelatedByInvoiceCountryIdJoinLegalForm(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CompanyQuery::create(null, $criteria);
        $query->joinWith('LegalForm', $joinBehavior);

        return $this->getCompaniesRelatedByInvoiceCountryId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related CompaniesRelatedByInvoiceCountryId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Company[] List of Company objects
     */
    public function getCompaniesRelatedByInvoiceCountryIdJoinOwnCompanyLogo(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CompanyQuery::create(null, $criteria);
        $query->joinWith('OwnCompanyLogo', $joinBehavior);

        return $this->getCompaniesRelatedByInvoiceCountryId($query, $con);
    }

    /**
     * Clears out the collCompaniesRelatedByDeliveryCountryId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCompaniesRelatedByDeliveryCountryId()
     */
    public function clearCompaniesRelatedByDeliveryCountryId()
    {
        $this->collCompaniesRelatedByDeliveryCountryId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCompaniesRelatedByDeliveryCountryId collection loaded partially.
     */
    public function resetPartialCompaniesRelatedByDeliveryCountryId($v = true)
    {
        $this->collCompaniesRelatedByDeliveryCountryIdPartial = $v;
    }

    /**
     * Initializes the collCompaniesRelatedByDeliveryCountryId collection.
     *
     * By default this just sets the collCompaniesRelatedByDeliveryCountryId collection to an empty array (like clearcollCompaniesRelatedByDeliveryCountryId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCompaniesRelatedByDeliveryCountryId($overrideExisting = true)
    {
        if (null !== $this->collCompaniesRelatedByDeliveryCountryId && !$overrideExisting) {
            return;
        }

        $collectionClassName = CompanyTableMap::getTableMap()->getCollectionClassName();

        $this->collCompaniesRelatedByDeliveryCountryId = new $collectionClassName;
        $this->collCompaniesRelatedByDeliveryCountryId->setModel('\Model\Company\Company');
    }

    /**
     * Gets an array of Company objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCountry is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Company[] List of Company objects
     * @throws PropelException
     */
    public function getCompaniesRelatedByDeliveryCountryId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCompaniesRelatedByDeliveryCountryIdPartial && !$this->isNew();
        if (null === $this->collCompaniesRelatedByDeliveryCountryId || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCompaniesRelatedByDeliveryCountryId) {
                    $this->initCompaniesRelatedByDeliveryCountryId();
                } else {
                    $collectionClassName = CompanyTableMap::getTableMap()->getCollectionClassName();

                    $collCompaniesRelatedByDeliveryCountryId = new $collectionClassName;
                    $collCompaniesRelatedByDeliveryCountryId->setModel('\Model\Company\Company');

                    return $collCompaniesRelatedByDeliveryCountryId;
                }
            } else {
                $collCompaniesRelatedByDeliveryCountryId = CompanyQuery::create(null, $criteria)
                    ->filterByDeliveryCountry($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCompaniesRelatedByDeliveryCountryIdPartial && count($collCompaniesRelatedByDeliveryCountryId)) {
                        $this->initCompaniesRelatedByDeliveryCountryId(false);

                        foreach ($collCompaniesRelatedByDeliveryCountryId as $obj) {
                            if (false == $this->collCompaniesRelatedByDeliveryCountryId->contains($obj)) {
                                $this->collCompaniesRelatedByDeliveryCountryId->append($obj);
                            }
                        }

                        $this->collCompaniesRelatedByDeliveryCountryIdPartial = true;
                    }

                    return $collCompaniesRelatedByDeliveryCountryId;
                }

                if ($partial && $this->collCompaniesRelatedByDeliveryCountryId) {
                    foreach ($this->collCompaniesRelatedByDeliveryCountryId as $obj) {
                        if ($obj->isNew()) {
                            $collCompaniesRelatedByDeliveryCountryId[] = $obj;
                        }
                    }
                }

                $this->collCompaniesRelatedByDeliveryCountryId = $collCompaniesRelatedByDeliveryCountryId;
                $this->collCompaniesRelatedByDeliveryCountryIdPartial = false;
            }
        }

        return $this->collCompaniesRelatedByDeliveryCountryId;
    }

    /**
     * Sets a collection of Company objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $companiesRelatedByDeliveryCountryId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCountry The current object (for fluent API support)
     */
    public function setCompaniesRelatedByDeliveryCountryId(Collection $companiesRelatedByDeliveryCountryId, ConnectionInterface $con = null)
    {
        /** @var Company[] $companiesRelatedByDeliveryCountryIdToDelete */
        $companiesRelatedByDeliveryCountryIdToDelete = $this->getCompaniesRelatedByDeliveryCountryId(new Criteria(), $con)->diff($companiesRelatedByDeliveryCountryId);


        $this->companiesRelatedByDeliveryCountryIdScheduledForDeletion = $companiesRelatedByDeliveryCountryIdToDelete;

        foreach ($companiesRelatedByDeliveryCountryIdToDelete as $companyRelatedByDeliveryCountryIdRemoved) {
            $companyRelatedByDeliveryCountryIdRemoved->setDeliveryCountry(null);
        }

        $this->collCompaniesRelatedByDeliveryCountryId = null;
        foreach ($companiesRelatedByDeliveryCountryId as $companyRelatedByDeliveryCountryId) {
            $this->addCompanyRelatedByDeliveryCountryId($companyRelatedByDeliveryCountryId);
        }

        $this->collCompaniesRelatedByDeliveryCountryId = $companiesRelatedByDeliveryCountryId;
        $this->collCompaniesRelatedByDeliveryCountryIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseCompany objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseCompany objects.
     * @throws PropelException
     */
    public function countCompaniesRelatedByDeliveryCountryId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCompaniesRelatedByDeliveryCountryIdPartial && !$this->isNew();
        if (null === $this->collCompaniesRelatedByDeliveryCountryId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCompaniesRelatedByDeliveryCountryId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCompaniesRelatedByDeliveryCountryId());
            }

            $query = CompanyQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDeliveryCountry($this)
                ->count($con);
        }

        return count($this->collCompaniesRelatedByDeliveryCountryId);
    }

    /**
     * Method called to associate a Company object to this object
     * through the Company foreign key attribute.
     *
     * @param  Company $l Company
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function addCompanyRelatedByDeliveryCountryId(Company $l)
    {
        if ($this->collCompaniesRelatedByDeliveryCountryId === null) {
            $this->initCompaniesRelatedByDeliveryCountryId();
            $this->collCompaniesRelatedByDeliveryCountryIdPartial = true;
        }

        if (!$this->collCompaniesRelatedByDeliveryCountryId->contains($l)) {
            $this->doAddCompanyRelatedByDeliveryCountryId($l);

            if ($this->companiesRelatedByDeliveryCountryIdScheduledForDeletion and $this->companiesRelatedByDeliveryCountryIdScheduledForDeletion->contains($l)) {
                $this->companiesRelatedByDeliveryCountryIdScheduledForDeletion->remove($this->companiesRelatedByDeliveryCountryIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Company $companyRelatedByDeliveryCountryId The Company object to add.
     */
    protected function doAddCompanyRelatedByDeliveryCountryId(Company $companyRelatedByDeliveryCountryId)
    {
        $this->collCompaniesRelatedByDeliveryCountryId[]= $companyRelatedByDeliveryCountryId;
        $companyRelatedByDeliveryCountryId->setDeliveryCountry($this);
    }

    /**
     * @param  Company $companyRelatedByDeliveryCountryId The Company object to remove.
     * @return $this|ChildCountry The current object (for fluent API support)
     */
    public function removeCompanyRelatedByDeliveryCountryId(Company $companyRelatedByDeliveryCountryId)
    {
        if ($this->getCompaniesRelatedByDeliveryCountryId()->contains($companyRelatedByDeliveryCountryId)) {
            $pos = $this->collCompaniesRelatedByDeliveryCountryId->search($companyRelatedByDeliveryCountryId);
            $this->collCompaniesRelatedByDeliveryCountryId->remove($pos);
            if (null === $this->companiesRelatedByDeliveryCountryIdScheduledForDeletion) {
                $this->companiesRelatedByDeliveryCountryIdScheduledForDeletion = clone $this->collCompaniesRelatedByDeliveryCountryId;
                $this->companiesRelatedByDeliveryCountryIdScheduledForDeletion->clear();
            }
            $this->companiesRelatedByDeliveryCountryIdScheduledForDeletion[]= $companyRelatedByDeliveryCountryId;
            $companyRelatedByDeliveryCountryId->setDeliveryCountry(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related CompaniesRelatedByDeliveryCountryId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Company[] List of Company objects
     */
    public function getCompaniesRelatedByDeliveryCountryIdJoinLegalForm(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CompanyQuery::create(null, $criteria);
        $query->joinWith('LegalForm', $joinBehavior);

        return $this->getCompaniesRelatedByDeliveryCountryId($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related CompaniesRelatedByDeliveryCountryId from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Company[] List of Company objects
     */
    public function getCompaniesRelatedByDeliveryCountryIdJoinOwnCompanyLogo(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CompanyQuery::create(null, $criteria);
        $query->joinWith('OwnCompanyLogo', $joinBehavior);

        return $this->getCompaniesRelatedByDeliveryCountryId($query, $con);
    }

    /**
     * Clears out the collAddressCountries collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addAddressCountries()
     */
    public function clearAddressCountries()
    {
        $this->collAddressCountries = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collAddressCountries collection loaded partially.
     */
    public function resetPartialAddressCountries($v = true)
    {
        $this->collAddressCountriesPartial = $v;
    }

    /**
     * Initializes the collAddressCountries collection.
     *
     * By default this just sets the collAddressCountries collection to an empty array (like clearcollAddressCountries());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initAddressCountries($overrideExisting = true)
    {
        if (null !== $this->collAddressCountries && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomerAddressTableMap::getTableMap()->getCollectionClassName();

        $this->collAddressCountries = new $collectionClassName;
        $this->collAddressCountries->setModel('\Model\Crm\CustomerAddress');
    }

    /**
     * Gets an array of CustomerAddress objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCountry is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|CustomerAddress[] List of CustomerAddress objects
     * @throws PropelException
     */
    public function getAddressCountries(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collAddressCountriesPartial && !$this->isNew();
        if (null === $this->collAddressCountries || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collAddressCountries) {
                    $this->initAddressCountries();
                } else {
                    $collectionClassName = CustomerAddressTableMap::getTableMap()->getCollectionClassName();

                    $collAddressCountries = new $collectionClassName;
                    $collAddressCountries->setModel('\Model\Crm\CustomerAddress');

                    return $collAddressCountries;
                }
            } else {
                $collAddressCountries = CustomerAddressQuery::create(null, $criteria)
                    ->filterByCountry($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collAddressCountriesPartial && count($collAddressCountries)) {
                        $this->initAddressCountries(false);

                        foreach ($collAddressCountries as $obj) {
                            if (false == $this->collAddressCountries->contains($obj)) {
                                $this->collAddressCountries->append($obj);
                            }
                        }

                        $this->collAddressCountriesPartial = true;
                    }

                    return $collAddressCountries;
                }

                if ($partial && $this->collAddressCountries) {
                    foreach ($this->collAddressCountries as $obj) {
                        if ($obj->isNew()) {
                            $collAddressCountries[] = $obj;
                        }
                    }
                }

                $this->collAddressCountries = $collAddressCountries;
                $this->collAddressCountriesPartial = false;
            }
        }

        return $this->collAddressCountries;
    }

    /**
     * Sets a collection of CustomerAddress objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $addressCountries A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCountry The current object (for fluent API support)
     */
    public function setAddressCountries(Collection $addressCountries, ConnectionInterface $con = null)
    {
        /** @var CustomerAddress[] $addressCountriesToDelete */
        $addressCountriesToDelete = $this->getAddressCountries(new Criteria(), $con)->diff($addressCountries);


        $this->addressCountriesScheduledForDeletion = $addressCountriesToDelete;

        foreach ($addressCountriesToDelete as $addressCountryRemoved) {
            $addressCountryRemoved->setCountry(null);
        }

        $this->collAddressCountries = null;
        foreach ($addressCountries as $addressCountry) {
            $this->addAddressCountry($addressCountry);
        }

        $this->collAddressCountries = $addressCountries;
        $this->collAddressCountriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseCustomerAddress objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseCustomerAddress objects.
     * @throws PropelException
     */
    public function countAddressCountries(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collAddressCountriesPartial && !$this->isNew();
        if (null === $this->collAddressCountries || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collAddressCountries) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getAddressCountries());
            }

            $query = CustomerAddressQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCountry($this)
                ->count($con);
        }

        return count($this->collAddressCountries);
    }

    /**
     * Method called to associate a CustomerAddress object to this object
     * through the CustomerAddress foreign key attribute.
     *
     * @param  CustomerAddress $l CustomerAddress
     * @return $this|\Model\Setting\MasterTable\Country The current object (for fluent API support)
     */
    public function addAddressCountry(CustomerAddress $l)
    {
        if ($this->collAddressCountries === null) {
            $this->initAddressCountries();
            $this->collAddressCountriesPartial = true;
        }

        if (!$this->collAddressCountries->contains($l)) {
            $this->doAddAddressCountry($l);

            if ($this->addressCountriesScheduledForDeletion and $this->addressCountriesScheduledForDeletion->contains($l)) {
                $this->addressCountriesScheduledForDeletion->remove($this->addressCountriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param CustomerAddress $addressCountry The CustomerAddress object to add.
     */
    protected function doAddAddressCountry(CustomerAddress $addressCountry)
    {
        $this->collAddressCountries[]= $addressCountry;
        $addressCountry->setCountry($this);
    }

    /**
     * @param  CustomerAddress $addressCountry The CustomerAddress object to remove.
     * @return $this|ChildCountry The current object (for fluent API support)
     */
    public function removeAddressCountry(CustomerAddress $addressCountry)
    {
        if ($this->getAddressCountries()->contains($addressCountry)) {
            $pos = $this->collAddressCountries->search($addressCountry);
            $this->collAddressCountries->remove($pos);
            if (null === $this->addressCountriesScheduledForDeletion) {
                $this->addressCountriesScheduledForDeletion = clone $this->collAddressCountries;
                $this->addressCountriesScheduledForDeletion->clear();
            }
            $this->addressCountriesScheduledForDeletion[]= $addressCountry;
            $addressCountry->setCountry(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related AddressCountries from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|CustomerAddress[] List of CustomerAddress objects
     */
    public function getAddressCountriesJoinUsaState(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CustomerAddressQuery::create(null, $criteria);
        $query->joinWith('UsaState', $joinBehavior);

        return $this->getAddressCountries($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related AddressCountries from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|CustomerAddress[] List of CustomerAddress objects
     */
    public function getAddressCountriesJoinCustomer(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CustomerAddressQuery::create(null, $criteria);
        $query->joinWith('Customer', $joinBehavior);

        return $this->getAddressCountries($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Country is new, it will return
     * an empty collection; or if this Country has previously
     * been saved, it will retrieve related AddressCountries from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Country.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|CustomerAddress[] List of CustomerAddress objects
     */
    public function getAddressCountriesJoinCustomerAddressType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CustomerAddressQuery::create(null, $criteria);
        $query->joinWith('CustomerAddressType', $joinBehavior);

        return $this->getAddressCountries($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aShippingZone) {
            $this->aShippingZone->removeCountry($this);
        }
        $this->id = null;
        $this->name = null;
        $this->shipping_zone_id = null;
        $this->iso2 = null;
        $this->iso3 = null;
        $this->is_eu = null;
        $this->stealth_mandatory = null;
        $this->stealth_advised = null;
        $this->latitude = null;
        $this->longitude = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collResellers) {
                foreach ($this->collResellers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCountryShippingMethods) {
                foreach ($this->collCountryShippingMethods as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCompaniesRelatedByGeneralCountryId) {
                foreach ($this->collCompaniesRelatedByGeneralCountryId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCompaniesRelatedByInvoiceCountryId) {
                foreach ($this->collCompaniesRelatedByInvoiceCountryId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCompaniesRelatedByDeliveryCountryId) {
                foreach ($this->collCompaniesRelatedByDeliveryCountryId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collAddressCountries) {
                foreach ($this->collAddressCountries as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collResellers = null;
        $this->collCountryShippingMethods = null;
        $this->collCompaniesRelatedByGeneralCountryId = null;
        $this->collCompaniesRelatedByInvoiceCountryId = null;
        $this->collCompaniesRelatedByDeliveryCountryId = null;
        $this->collAddressCountries = null;
        $this->aShippingZone = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CountryTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
