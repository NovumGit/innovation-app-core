<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\ProductUsage;
use Model\ProductUsageQuery;
use Model\Base\ProductUsage as BaseProductUsage;
use Model\Map\ProductUsageTableMap;
use Model\Setting\MasterTable\Usage as ChildUsage;
use Model\Setting\MasterTable\UsageQuery as ChildUsageQuery;
use Model\Setting\MasterTable\UsageTranslation as ChildUsageTranslation;
use Model\Setting\MasterTable\UsageTranslationQuery as ChildUsageTranslationQuery;
use Model\Setting\MasterTable\Map\UsageTableMap;
use Model\Setting\MasterTable\Map\UsageTranslationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'mt_usage' table.
 *
 *
 *
 * @package    propel.generator.Model.Setting.MasterTable.Base
 */
abstract class Usage implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Setting\\MasterTable\\Map\\UsageTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the old_id field.
     *
     * @var        int|null
     */
    protected $old_id;

    /**
     * The value for the name field.
     *
     * @var        string|null
     */
    protected $name;

    /**
     * @var        ObjectCollection|ProductUsage[] Collection to store aggregation of ProductUsage objects.
     */
    protected $collProductUsages;
    protected $collProductUsagesPartial;

    /**
     * @var        ObjectCollection|ChildUsageTranslation[] Collection to store aggregation of ChildUsageTranslation objects.
     */
    protected $collUsageTranslations;
    protected $collUsageTranslationsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ProductUsage[]
     */
    protected $productUsagesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUsageTranslation[]
     */
    protected $usageTranslationsScheduledForDeletion = null;

    /**
     * Initializes internal state of Model\Setting\MasterTable\Base\Usage object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Usage</code> instance.  If
     * <code>obj</code> is an instance of <code>Usage</code>, delegates to
     * <code>equals(Usage)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [old_id] column value.
     *
     * @return int|null
     */
    public function getOldId()
    {
        return $this->old_id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Setting\MasterTable\Usage The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[UsageTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [old_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Setting\MasterTable\Usage The current object (for fluent API support)
     */
    public function setOldId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->old_id !== $v) {
            $this->old_id = $v;
            $this->modifiedColumns[UsageTableMap::COL_OLD_ID] = true;
        }

        return $this;
    } // setOldId()

    /**
     * Set the value of [name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Setting\MasterTable\Usage The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[UsageTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UsageTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UsageTableMap::translateFieldName('OldId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->old_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UsageTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 3; // 3 = UsageTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Setting\\MasterTable\\Usage'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UsageTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUsageQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collProductUsages = null;

            $this->collUsageTranslations = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Usage::setDeleted()
     * @see Usage::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsageTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUsageQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UsageTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UsageTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->productUsagesScheduledForDeletion !== null) {
                if (!$this->productUsagesScheduledForDeletion->isEmpty()) {
                    \Model\ProductUsageQuery::create()
                        ->filterByPrimaryKeys($this->productUsagesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productUsagesScheduledForDeletion = null;
                }
            }

            if ($this->collProductUsages !== null) {
                foreach ($this->collProductUsages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usageTranslationsScheduledForDeletion !== null) {
                if (!$this->usageTranslationsScheduledForDeletion->isEmpty()) {
                    \Model\Setting\MasterTable\UsageTranslationQuery::create()
                        ->filterByPrimaryKeys($this->usageTranslationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usageTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collUsageTranslations !== null) {
                foreach ($this->collUsageTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UsageTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UsageTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UsageTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(UsageTableMap::COL_OLD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'old_id';
        }
        if ($this->isColumnModified(UsageTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }

        $sql = sprintf(
            'INSERT INTO mt_usage (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'old_id':
                        $stmt->bindValue($identifier, $this->old_id, PDO::PARAM_INT);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UsageTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getOldId();
                break;
            case 2:
                return $this->getName();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Usage'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Usage'][$this->hashCode()] = true;
        $keys = UsageTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getOldId(),
            $keys[2] => $this->getName(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collProductUsages) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productUsages';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_usages';
                        break;
                    default:
                        $key = 'ProductUsages';
                }

                $result[$key] = $this->collProductUsages->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsageTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'usageTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_usage_translations';
                        break;
                    default:
                        $key = 'UsageTranslations';
                }

                $result[$key] = $this->collUsageTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Setting\MasterTable\Usage
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UsageTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Setting\MasterTable\Usage
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setOldId($value);
                break;
            case 2:
                $this->setName($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UsageTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setOldId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setName($arr[$keys[2]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Setting\MasterTable\Usage The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UsageTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UsageTableMap::COL_ID)) {
            $criteria->add(UsageTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(UsageTableMap::COL_OLD_ID)) {
            $criteria->add(UsageTableMap::COL_OLD_ID, $this->old_id);
        }
        if ($this->isColumnModified(UsageTableMap::COL_NAME)) {
            $criteria->add(UsageTableMap::COL_NAME, $this->name);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUsageQuery::create();
        $criteria->add(UsageTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Setting\MasterTable\Usage (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOldId($this->getOldId());
        $copyObj->setName($this->getName());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getProductUsages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductUsage($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsageTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUsageTranslation($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Setting\MasterTable\Usage Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ProductUsage' === $relationName) {
            $this->initProductUsages();
            return;
        }
        if ('UsageTranslation' === $relationName) {
            $this->initUsageTranslations();
            return;
        }
    }

    /**
     * Clears out the collProductUsages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductUsages()
     */
    public function clearProductUsages()
    {
        $this->collProductUsages = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductUsages collection loaded partially.
     */
    public function resetPartialProductUsages($v = true)
    {
        $this->collProductUsagesPartial = $v;
    }

    /**
     * Initializes the collProductUsages collection.
     *
     * By default this just sets the collProductUsages collection to an empty array (like clearcollProductUsages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductUsages($overrideExisting = true)
    {
        if (null !== $this->collProductUsages && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductUsageTableMap::getTableMap()->getCollectionClassName();

        $this->collProductUsages = new $collectionClassName;
        $this->collProductUsages->setModel('\Model\ProductUsage');
    }

    /**
     * Gets an array of ProductUsage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ProductUsage[] List of ProductUsage objects
     * @throws PropelException
     */
    public function getProductUsages(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductUsagesPartial && !$this->isNew();
        if (null === $this->collProductUsages || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductUsages) {
                    $this->initProductUsages();
                } else {
                    $collectionClassName = ProductUsageTableMap::getTableMap()->getCollectionClassName();

                    $collProductUsages = new $collectionClassName;
                    $collProductUsages->setModel('\Model\ProductUsage');

                    return $collProductUsages;
                }
            } else {
                $collProductUsages = ProductUsageQuery::create(null, $criteria)
                    ->filterByUsage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductUsagesPartial && count($collProductUsages)) {
                        $this->initProductUsages(false);

                        foreach ($collProductUsages as $obj) {
                            if (false == $this->collProductUsages->contains($obj)) {
                                $this->collProductUsages->append($obj);
                            }
                        }

                        $this->collProductUsagesPartial = true;
                    }

                    return $collProductUsages;
                }

                if ($partial && $this->collProductUsages) {
                    foreach ($this->collProductUsages as $obj) {
                        if ($obj->isNew()) {
                            $collProductUsages[] = $obj;
                        }
                    }
                }

                $this->collProductUsages = $collProductUsages;
                $this->collProductUsagesPartial = false;
            }
        }

        return $this->collProductUsages;
    }

    /**
     * Sets a collection of ProductUsage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productUsages A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsage The current object (for fluent API support)
     */
    public function setProductUsages(Collection $productUsages, ConnectionInterface $con = null)
    {
        /** @var ProductUsage[] $productUsagesToDelete */
        $productUsagesToDelete = $this->getProductUsages(new Criteria(), $con)->diff($productUsages);


        $this->productUsagesScheduledForDeletion = $productUsagesToDelete;

        foreach ($productUsagesToDelete as $productUsageRemoved) {
            $productUsageRemoved->setUsage(null);
        }

        $this->collProductUsages = null;
        foreach ($productUsages as $productUsage) {
            $this->addProductUsage($productUsage);
        }

        $this->collProductUsages = $productUsages;
        $this->collProductUsagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseProductUsage objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseProductUsage objects.
     * @throws PropelException
     */
    public function countProductUsages(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductUsagesPartial && !$this->isNew();
        if (null === $this->collProductUsages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductUsages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductUsages());
            }

            $query = ProductUsageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsage($this)
                ->count($con);
        }

        return count($this->collProductUsages);
    }

    /**
     * Method called to associate a ProductUsage object to this object
     * through the ProductUsage foreign key attribute.
     *
     * @param  ProductUsage $l ProductUsage
     * @return $this|\Model\Setting\MasterTable\Usage The current object (for fluent API support)
     */
    public function addProductUsage(ProductUsage $l)
    {
        if ($this->collProductUsages === null) {
            $this->initProductUsages();
            $this->collProductUsagesPartial = true;
        }

        if (!$this->collProductUsages->contains($l)) {
            $this->doAddProductUsage($l);

            if ($this->productUsagesScheduledForDeletion and $this->productUsagesScheduledForDeletion->contains($l)) {
                $this->productUsagesScheduledForDeletion->remove($this->productUsagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ProductUsage $productUsage The ProductUsage object to add.
     */
    protected function doAddProductUsage(ProductUsage $productUsage)
    {
        $this->collProductUsages[]= $productUsage;
        $productUsage->setUsage($this);
    }

    /**
     * @param  ProductUsage $productUsage The ProductUsage object to remove.
     * @return $this|ChildUsage The current object (for fluent API support)
     */
    public function removeProductUsage(ProductUsage $productUsage)
    {
        if ($this->getProductUsages()->contains($productUsage)) {
            $pos = $this->collProductUsages->search($productUsage);
            $this->collProductUsages->remove($pos);
            if (null === $this->productUsagesScheduledForDeletion) {
                $this->productUsagesScheduledForDeletion = clone $this->collProductUsages;
                $this->productUsagesScheduledForDeletion->clear();
            }
            $this->productUsagesScheduledForDeletion[]= clone $productUsage;
            $productUsage->setUsage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usage is new, it will return
     * an empty collection; or if this Usage has previously
     * been saved, it will retrieve related ProductUsages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usage.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ProductUsage[] List of ProductUsage objects
     */
    public function getProductUsagesJoinProduct(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductUsageQuery::create(null, $criteria);
        $query->joinWith('Product', $joinBehavior);

        return $this->getProductUsages($query, $con);
    }

    /**
     * Clears out the collUsageTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsageTranslations()
     */
    public function clearUsageTranslations()
    {
        $this->collUsageTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsageTranslations collection loaded partially.
     */
    public function resetPartialUsageTranslations($v = true)
    {
        $this->collUsageTranslationsPartial = $v;
    }

    /**
     * Initializes the collUsageTranslations collection.
     *
     * By default this just sets the collUsageTranslations collection to an empty array (like clearcollUsageTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsageTranslations($overrideExisting = true)
    {
        if (null !== $this->collUsageTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = UsageTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collUsageTranslations = new $collectionClassName;
        $this->collUsageTranslations->setModel('\Model\Setting\MasterTable\UsageTranslation');
    }

    /**
     * Gets an array of ChildUsageTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUsage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUsageTranslation[] List of ChildUsageTranslation objects
     * @throws PropelException
     */
    public function getUsageTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsageTranslationsPartial && !$this->isNew();
        if (null === $this->collUsageTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUsageTranslations) {
                    $this->initUsageTranslations();
                } else {
                    $collectionClassName = UsageTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collUsageTranslations = new $collectionClassName;
                    $collUsageTranslations->setModel('\Model\Setting\MasterTable\UsageTranslation');

                    return $collUsageTranslations;
                }
            } else {
                $collUsageTranslations = ChildUsageTranslationQuery::create(null, $criteria)
                    ->filterByUsage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsageTranslationsPartial && count($collUsageTranslations)) {
                        $this->initUsageTranslations(false);

                        foreach ($collUsageTranslations as $obj) {
                            if (false == $this->collUsageTranslations->contains($obj)) {
                                $this->collUsageTranslations->append($obj);
                            }
                        }

                        $this->collUsageTranslationsPartial = true;
                    }

                    return $collUsageTranslations;
                }

                if ($partial && $this->collUsageTranslations) {
                    foreach ($this->collUsageTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collUsageTranslations[] = $obj;
                        }
                    }
                }

                $this->collUsageTranslations = $collUsageTranslations;
                $this->collUsageTranslationsPartial = false;
            }
        }

        return $this->collUsageTranslations;
    }

    /**
     * Sets a collection of ChildUsageTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $usageTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUsage The current object (for fluent API support)
     */
    public function setUsageTranslations(Collection $usageTranslations, ConnectionInterface $con = null)
    {
        /** @var ChildUsageTranslation[] $usageTranslationsToDelete */
        $usageTranslationsToDelete = $this->getUsageTranslations(new Criteria(), $con)->diff($usageTranslations);


        $this->usageTranslationsScheduledForDeletion = $usageTranslationsToDelete;

        foreach ($usageTranslationsToDelete as $usageTranslationRemoved) {
            $usageTranslationRemoved->setUsage(null);
        }

        $this->collUsageTranslations = null;
        foreach ($usageTranslations as $usageTranslation) {
            $this->addUsageTranslation($usageTranslation);
        }

        $this->collUsageTranslations = $usageTranslations;
        $this->collUsageTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UsageTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UsageTranslation objects.
     * @throws PropelException
     */
    public function countUsageTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsageTranslationsPartial && !$this->isNew();
        if (null === $this->collUsageTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsageTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsageTranslations());
            }

            $query = ChildUsageTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsage($this)
                ->count($con);
        }

        return count($this->collUsageTranslations);
    }

    /**
     * Method called to associate a ChildUsageTranslation object to this object
     * through the ChildUsageTranslation foreign key attribute.
     *
     * @param  ChildUsageTranslation $l ChildUsageTranslation
     * @return $this|\Model\Setting\MasterTable\Usage The current object (for fluent API support)
     */
    public function addUsageTranslation(ChildUsageTranslation $l)
    {
        if ($this->collUsageTranslations === null) {
            $this->initUsageTranslations();
            $this->collUsageTranslationsPartial = true;
        }

        if (!$this->collUsageTranslations->contains($l)) {
            $this->doAddUsageTranslation($l);

            if ($this->usageTranslationsScheduledForDeletion and $this->usageTranslationsScheduledForDeletion->contains($l)) {
                $this->usageTranslationsScheduledForDeletion->remove($this->usageTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUsageTranslation $usageTranslation The ChildUsageTranslation object to add.
     */
    protected function doAddUsageTranslation(ChildUsageTranslation $usageTranslation)
    {
        $this->collUsageTranslations[]= $usageTranslation;
        $usageTranslation->setUsage($this);
    }

    /**
     * @param  ChildUsageTranslation $usageTranslation The ChildUsageTranslation object to remove.
     * @return $this|ChildUsage The current object (for fluent API support)
     */
    public function removeUsageTranslation(ChildUsageTranslation $usageTranslation)
    {
        if ($this->getUsageTranslations()->contains($usageTranslation)) {
            $pos = $this->collUsageTranslations->search($usageTranslation);
            $this->collUsageTranslations->remove($pos);
            if (null === $this->usageTranslationsScheduledForDeletion) {
                $this->usageTranslationsScheduledForDeletion = clone $this->collUsageTranslations;
                $this->usageTranslationsScheduledForDeletion->clear();
            }
            $this->usageTranslationsScheduledForDeletion[]= $usageTranslation;
            $usageTranslation->setUsage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usage is new, it will return
     * an empty collection; or if this Usage has previously
     * been saved, it will retrieve related UsageTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usage.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUsageTranslation[] List of ChildUsageTranslation objects
     */
    public function getUsageTranslationsJoinLanguage(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUsageTranslationQuery::create(null, $criteria);
        $query->joinWith('Language', $joinBehavior);

        return $this->getUsageTranslations($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->old_id = null;
        $this->name = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collProductUsages) {
                foreach ($this->collProductUsages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsageTranslations) {
                foreach ($this->collUsageTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collProductUsages = null;
        $this->collUsageTranslations = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UsageTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
