<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\Setting\MasterTable\ProductDeliveryTimeTranslation as ChildProductDeliveryTimeTranslation;
use Model\Setting\MasterTable\ProductDeliveryTimeTranslationQuery as ChildProductDeliveryTimeTranslationQuery;
use Model\Setting\MasterTable\Map\ProductDeliveryTimeTranslationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mt_delivery_time_translation' table.
 *
 *
 *
 * @method     ChildProductDeliveryTimeTranslationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildProductDeliveryTimeTranslationQuery orderByLanguageId($order = Criteria::ASC) Order by the language_id column
 * @method     ChildProductDeliveryTimeTranslationQuery orderByDeliveryTimeId($order = Criteria::ASC) Order by the delivery_time_id column
 * @method     ChildProductDeliveryTimeTranslationQuery orderByName($order = Criteria::ASC) Order by the name column
 *
 * @method     ChildProductDeliveryTimeTranslationQuery groupById() Group by the id column
 * @method     ChildProductDeliveryTimeTranslationQuery groupByLanguageId() Group by the language_id column
 * @method     ChildProductDeliveryTimeTranslationQuery groupByDeliveryTimeId() Group by the delivery_time_id column
 * @method     ChildProductDeliveryTimeTranslationQuery groupByName() Group by the name column
 *
 * @method     ChildProductDeliveryTimeTranslationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProductDeliveryTimeTranslationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProductDeliveryTimeTranslationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProductDeliveryTimeTranslationQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildProductDeliveryTimeTranslationQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildProductDeliveryTimeTranslationQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildProductDeliveryTimeTranslationQuery leftJoinLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Language relation
 * @method     ChildProductDeliveryTimeTranslationQuery rightJoinLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Language relation
 * @method     ChildProductDeliveryTimeTranslationQuery innerJoinLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the Language relation
 *
 * @method     ChildProductDeliveryTimeTranslationQuery joinWithLanguage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Language relation
 *
 * @method     ChildProductDeliveryTimeTranslationQuery leftJoinWithLanguage() Adds a LEFT JOIN clause and with to the query using the Language relation
 * @method     ChildProductDeliveryTimeTranslationQuery rightJoinWithLanguage() Adds a RIGHT JOIN clause and with to the query using the Language relation
 * @method     ChildProductDeliveryTimeTranslationQuery innerJoinWithLanguage() Adds a INNER JOIN clause and with to the query using the Language relation
 *
 * @method     ChildProductDeliveryTimeTranslationQuery leftJoinDeliveryTime($relationAlias = null) Adds a LEFT JOIN clause to the query using the DeliveryTime relation
 * @method     ChildProductDeliveryTimeTranslationQuery rightJoinDeliveryTime($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DeliveryTime relation
 * @method     ChildProductDeliveryTimeTranslationQuery innerJoinDeliveryTime($relationAlias = null) Adds a INNER JOIN clause to the query using the DeliveryTime relation
 *
 * @method     ChildProductDeliveryTimeTranslationQuery joinWithDeliveryTime($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DeliveryTime relation
 *
 * @method     ChildProductDeliveryTimeTranslationQuery leftJoinWithDeliveryTime() Adds a LEFT JOIN clause and with to the query using the DeliveryTime relation
 * @method     ChildProductDeliveryTimeTranslationQuery rightJoinWithDeliveryTime() Adds a RIGHT JOIN clause and with to the query using the DeliveryTime relation
 * @method     ChildProductDeliveryTimeTranslationQuery innerJoinWithDeliveryTime() Adds a INNER JOIN clause and with to the query using the DeliveryTime relation
 *
 * @method     \Model\Setting\MasterTable\LanguageQuery|\Model\Setting\MasterTable\ProductDeliveryTimeQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildProductDeliveryTimeTranslation findOne(ConnectionInterface $con = null) Return the first ChildProductDeliveryTimeTranslation matching the query
 * @method     ChildProductDeliveryTimeTranslation findOneOrCreate(ConnectionInterface $con = null) Return the first ChildProductDeliveryTimeTranslation matching the query, or a new ChildProductDeliveryTimeTranslation object populated from the query conditions when no match is found
 *
 * @method     ChildProductDeliveryTimeTranslation findOneById(int $id) Return the first ChildProductDeliveryTimeTranslation filtered by the id column
 * @method     ChildProductDeliveryTimeTranslation findOneByLanguageId(int $language_id) Return the first ChildProductDeliveryTimeTranslation filtered by the language_id column
 * @method     ChildProductDeliveryTimeTranslation findOneByDeliveryTimeId(int $delivery_time_id) Return the first ChildProductDeliveryTimeTranslation filtered by the delivery_time_id column
 * @method     ChildProductDeliveryTimeTranslation findOneByName(string $name) Return the first ChildProductDeliveryTimeTranslation filtered by the name column *

 * @method     ChildProductDeliveryTimeTranslation requirePk($key, ConnectionInterface $con = null) Return the ChildProductDeliveryTimeTranslation by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProductDeliveryTimeTranslation requireOne(ConnectionInterface $con = null) Return the first ChildProductDeliveryTimeTranslation matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProductDeliveryTimeTranslation requireOneById(int $id) Return the first ChildProductDeliveryTimeTranslation filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProductDeliveryTimeTranslation requireOneByLanguageId(int $language_id) Return the first ChildProductDeliveryTimeTranslation filtered by the language_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProductDeliveryTimeTranslation requireOneByDeliveryTimeId(int $delivery_time_id) Return the first ChildProductDeliveryTimeTranslation filtered by the delivery_time_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProductDeliveryTimeTranslation requireOneByName(string $name) Return the first ChildProductDeliveryTimeTranslation filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProductDeliveryTimeTranslation[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildProductDeliveryTimeTranslation objects based on current ModelCriteria
 * @method     ChildProductDeliveryTimeTranslation[]|ObjectCollection findById(int $id) Return ChildProductDeliveryTimeTranslation objects filtered by the id column
 * @method     ChildProductDeliveryTimeTranslation[]|ObjectCollection findByLanguageId(int $language_id) Return ChildProductDeliveryTimeTranslation objects filtered by the language_id column
 * @method     ChildProductDeliveryTimeTranslation[]|ObjectCollection findByDeliveryTimeId(int $delivery_time_id) Return ChildProductDeliveryTimeTranslation objects filtered by the delivery_time_id column
 * @method     ChildProductDeliveryTimeTranslation[]|ObjectCollection findByName(string $name) Return ChildProductDeliveryTimeTranslation objects filtered by the name column
 * @method     ChildProductDeliveryTimeTranslation[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProductDeliveryTimeTranslationQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\MasterTable\Base\ProductDeliveryTimeTranslationQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\MasterTable\\ProductDeliveryTimeTranslation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProductDeliveryTimeTranslationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProductDeliveryTimeTranslationQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProductDeliveryTimeTranslationQuery) {
            return $criteria;
        }
        $query = new ChildProductDeliveryTimeTranslationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildProductDeliveryTimeTranslation|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProductDeliveryTimeTranslationTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ProductDeliveryTimeTranslationTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProductDeliveryTimeTranslation A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, language_id, delivery_time_id, name FROM mt_delivery_time_translation WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildProductDeliveryTimeTranslation $obj */
            $obj = new ChildProductDeliveryTimeTranslation();
            $obj->hydrate($row);
            ProductDeliveryTimeTranslationTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProductDeliveryTimeTranslation|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProductDeliveryTimeTranslationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProductDeliveryTimeTranslationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductDeliveryTimeTranslationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the language_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguageId(1234); // WHERE language_id = 1234
     * $query->filterByLanguageId(array(12, 34)); // WHERE language_id IN (12, 34)
     * $query->filterByLanguageId(array('min' => 12)); // WHERE language_id > 12
     * </code>
     *
     * @see       filterByLanguage()
     *
     * @param     mixed $languageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductDeliveryTimeTranslationQuery The current query, for fluid interface
     */
    public function filterByLanguageId($languageId = null, $comparison = null)
    {
        if (is_array($languageId)) {
            $useMinMax = false;
            if (isset($languageId['min'])) {
                $this->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_LANGUAGE_ID, $languageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($languageId['max'])) {
                $this->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_LANGUAGE_ID, $languageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_LANGUAGE_ID, $languageId, $comparison);
    }

    /**
     * Filter the query on the delivery_time_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDeliveryTimeId(1234); // WHERE delivery_time_id = 1234
     * $query->filterByDeliveryTimeId(array(12, 34)); // WHERE delivery_time_id IN (12, 34)
     * $query->filterByDeliveryTimeId(array('min' => 12)); // WHERE delivery_time_id > 12
     * </code>
     *
     * @see       filterByDeliveryTime()
     *
     * @param     mixed $deliveryTimeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductDeliveryTimeTranslationQuery The current query, for fluid interface
     */
    public function filterByDeliveryTimeId($deliveryTimeId = null, $comparison = null)
    {
        if (is_array($deliveryTimeId)) {
            $useMinMax = false;
            if (isset($deliveryTimeId['min'])) {
                $this->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_DELIVERY_TIME_ID, $deliveryTimeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deliveryTimeId['max'])) {
                $this->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_DELIVERY_TIME_ID, $deliveryTimeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_DELIVERY_TIME_ID, $deliveryTimeId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductDeliveryTimeTranslationQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Language object
     *
     * @param \Model\Setting\MasterTable\Language|ObjectCollection $language The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProductDeliveryTimeTranslationQuery The current query, for fluid interface
     */
    public function filterByLanguage($language, $comparison = null)
    {
        if ($language instanceof \Model\Setting\MasterTable\Language) {
            return $this
                ->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_LANGUAGE_ID, $language->getId(), $comparison);
        } elseif ($language instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_LANGUAGE_ID, $language->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLanguage() only accepts arguments of type \Model\Setting\MasterTable\Language or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Language relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductDeliveryTimeTranslationQuery The current query, for fluid interface
     */
    public function joinLanguage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Language');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Language');
        }

        return $this;
    }

    /**
     * Use the Language relation Language object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\LanguageQuery A secondary query class using the current class as primary query
     */
    public function useLanguageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLanguage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Language', '\Model\Setting\MasterTable\LanguageQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\ProductDeliveryTime object
     *
     * @param \Model\Setting\MasterTable\ProductDeliveryTime|ObjectCollection $productDeliveryTime The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProductDeliveryTimeTranslationQuery The current query, for fluid interface
     */
    public function filterByDeliveryTime($productDeliveryTime, $comparison = null)
    {
        if ($productDeliveryTime instanceof \Model\Setting\MasterTable\ProductDeliveryTime) {
            return $this
                ->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_DELIVERY_TIME_ID, $productDeliveryTime->getId(), $comparison);
        } elseif ($productDeliveryTime instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_DELIVERY_TIME_ID, $productDeliveryTime->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDeliveryTime() only accepts arguments of type \Model\Setting\MasterTable\ProductDeliveryTime or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DeliveryTime relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductDeliveryTimeTranslationQuery The current query, for fluid interface
     */
    public function joinDeliveryTime($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DeliveryTime');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DeliveryTime');
        }

        return $this;
    }

    /**
     * Use the DeliveryTime relation ProductDeliveryTime object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\ProductDeliveryTimeQuery A secondary query class using the current class as primary query
     */
    public function useDeliveryTimeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDeliveryTime($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DeliveryTime', '\Model\Setting\MasterTable\ProductDeliveryTimeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildProductDeliveryTimeTranslation $productDeliveryTimeTranslation Object to remove from the list of results
     *
     * @return $this|ChildProductDeliveryTimeTranslationQuery The current query, for fluid interface
     */
    public function prune($productDeliveryTimeTranslation = null)
    {
        if ($productDeliveryTimeTranslation) {
            $this->addUsingAlias(ProductDeliveryTimeTranslationTableMap::COL_ID, $productDeliveryTimeTranslation->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mt_delivery_time_translation table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductDeliveryTimeTranslationTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProductDeliveryTimeTranslationTableMap::clearInstancePool();
            ProductDeliveryTimeTranslationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductDeliveryTimeTranslationTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProductDeliveryTimeTranslationTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProductDeliveryTimeTranslationTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProductDeliveryTimeTranslationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ProductDeliveryTimeTranslationQuery
