<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\Setting\MasterTable\LegalFormTranslation as ChildLegalFormTranslation;
use Model\Setting\MasterTable\LegalFormTranslationQuery as ChildLegalFormTranslationQuery;
use Model\Setting\MasterTable\Map\LegalFormTranslationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mt_legal_form_translation' table.
 *
 *
 *
 * @method     ChildLegalFormTranslationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildLegalFormTranslationQuery orderByLegalFormId($order = Criteria::ASC) Order by the legal_form_id column
 * @method     ChildLegalFormTranslationQuery orderByLanguageId($order = Criteria::ASC) Order by the language_id column
 * @method     ChildLegalFormTranslationQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildLegalFormTranslationQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildLegalFormTranslationQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildLegalFormTranslationQuery groupById() Group by the id column
 * @method     ChildLegalFormTranslationQuery groupByLegalFormId() Group by the legal_form_id column
 * @method     ChildLegalFormTranslationQuery groupByLanguageId() Group by the language_id column
 * @method     ChildLegalFormTranslationQuery groupByName() Group by the name column
 * @method     ChildLegalFormTranslationQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildLegalFormTranslationQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildLegalFormTranslationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildLegalFormTranslationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildLegalFormTranslationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildLegalFormTranslationQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildLegalFormTranslationQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildLegalFormTranslationQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildLegalFormTranslationQuery leftJoinLegalForm($relationAlias = null) Adds a LEFT JOIN clause to the query using the LegalForm relation
 * @method     ChildLegalFormTranslationQuery rightJoinLegalForm($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LegalForm relation
 * @method     ChildLegalFormTranslationQuery innerJoinLegalForm($relationAlias = null) Adds a INNER JOIN clause to the query using the LegalForm relation
 *
 * @method     ChildLegalFormTranslationQuery joinWithLegalForm($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the LegalForm relation
 *
 * @method     ChildLegalFormTranslationQuery leftJoinWithLegalForm() Adds a LEFT JOIN clause and with to the query using the LegalForm relation
 * @method     ChildLegalFormTranslationQuery rightJoinWithLegalForm() Adds a RIGHT JOIN clause and with to the query using the LegalForm relation
 * @method     ChildLegalFormTranslationQuery innerJoinWithLegalForm() Adds a INNER JOIN clause and with to the query using the LegalForm relation
 *
 * @method     ChildLegalFormTranslationQuery leftJoinLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Language relation
 * @method     ChildLegalFormTranslationQuery rightJoinLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Language relation
 * @method     ChildLegalFormTranslationQuery innerJoinLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the Language relation
 *
 * @method     ChildLegalFormTranslationQuery joinWithLanguage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Language relation
 *
 * @method     ChildLegalFormTranslationQuery leftJoinWithLanguage() Adds a LEFT JOIN clause and with to the query using the Language relation
 * @method     ChildLegalFormTranslationQuery rightJoinWithLanguage() Adds a RIGHT JOIN clause and with to the query using the Language relation
 * @method     ChildLegalFormTranslationQuery innerJoinWithLanguage() Adds a INNER JOIN clause and with to the query using the Language relation
 *
 * @method     \Model\Setting\MasterTable\LegalFormQuery|\Model\Setting\MasterTable\LanguageQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildLegalFormTranslation findOne(ConnectionInterface $con = null) Return the first ChildLegalFormTranslation matching the query
 * @method     ChildLegalFormTranslation findOneOrCreate(ConnectionInterface $con = null) Return the first ChildLegalFormTranslation matching the query, or a new ChildLegalFormTranslation object populated from the query conditions when no match is found
 *
 * @method     ChildLegalFormTranslation findOneById(int $id) Return the first ChildLegalFormTranslation filtered by the id column
 * @method     ChildLegalFormTranslation findOneByLegalFormId(int $legal_form_id) Return the first ChildLegalFormTranslation filtered by the legal_form_id column
 * @method     ChildLegalFormTranslation findOneByLanguageId(int $language_id) Return the first ChildLegalFormTranslation filtered by the language_id column
 * @method     ChildLegalFormTranslation findOneByName(string $name) Return the first ChildLegalFormTranslation filtered by the name column
 * @method     ChildLegalFormTranslation findOneByCreatedAt(string $created_at) Return the first ChildLegalFormTranslation filtered by the created_at column
 * @method     ChildLegalFormTranslation findOneByUpdatedAt(string $updated_at) Return the first ChildLegalFormTranslation filtered by the updated_at column *

 * @method     ChildLegalFormTranslation requirePk($key, ConnectionInterface $con = null) Return the ChildLegalFormTranslation by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLegalFormTranslation requireOne(ConnectionInterface $con = null) Return the first ChildLegalFormTranslation matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLegalFormTranslation requireOneById(int $id) Return the first ChildLegalFormTranslation filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLegalFormTranslation requireOneByLegalFormId(int $legal_form_id) Return the first ChildLegalFormTranslation filtered by the legal_form_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLegalFormTranslation requireOneByLanguageId(int $language_id) Return the first ChildLegalFormTranslation filtered by the language_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLegalFormTranslation requireOneByName(string $name) Return the first ChildLegalFormTranslation filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLegalFormTranslation requireOneByCreatedAt(string $created_at) Return the first ChildLegalFormTranslation filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLegalFormTranslation requireOneByUpdatedAt(string $updated_at) Return the first ChildLegalFormTranslation filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLegalFormTranslation[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildLegalFormTranslation objects based on current ModelCriteria
 * @method     ChildLegalFormTranslation[]|ObjectCollection findById(int $id) Return ChildLegalFormTranslation objects filtered by the id column
 * @method     ChildLegalFormTranslation[]|ObjectCollection findByLegalFormId(int $legal_form_id) Return ChildLegalFormTranslation objects filtered by the legal_form_id column
 * @method     ChildLegalFormTranslation[]|ObjectCollection findByLanguageId(int $language_id) Return ChildLegalFormTranslation objects filtered by the language_id column
 * @method     ChildLegalFormTranslation[]|ObjectCollection findByName(string $name) Return ChildLegalFormTranslation objects filtered by the name column
 * @method     ChildLegalFormTranslation[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildLegalFormTranslation objects filtered by the created_at column
 * @method     ChildLegalFormTranslation[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildLegalFormTranslation objects filtered by the updated_at column
 * @method     ChildLegalFormTranslation[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class LegalFormTranslationQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\MasterTable\Base\LegalFormTranslationQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\MasterTable\\LegalFormTranslation', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildLegalFormTranslationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildLegalFormTranslationQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildLegalFormTranslationQuery) {
            return $criteria;
        }
        $query = new ChildLegalFormTranslationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildLegalFormTranslation|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LegalFormTranslationTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = LegalFormTranslationTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLegalFormTranslation A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, legal_form_id, language_id, name, created_at, updated_at FROM mt_legal_form_translation WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildLegalFormTranslation $obj */
            $obj = new ChildLegalFormTranslation();
            $obj->hydrate($row);
            LegalFormTranslationTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildLegalFormTranslation|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LegalFormTranslationTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LegalFormTranslationTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(LegalFormTranslationTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(LegalFormTranslationTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LegalFormTranslationTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the legal_form_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLegalFormId(1234); // WHERE legal_form_id = 1234
     * $query->filterByLegalFormId(array(12, 34)); // WHERE legal_form_id IN (12, 34)
     * $query->filterByLegalFormId(array('min' => 12)); // WHERE legal_form_id > 12
     * </code>
     *
     * @see       filterByLegalForm()
     *
     * @param     mixed $legalFormId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function filterByLegalFormId($legalFormId = null, $comparison = null)
    {
        if (is_array($legalFormId)) {
            $useMinMax = false;
            if (isset($legalFormId['min'])) {
                $this->addUsingAlias(LegalFormTranslationTableMap::COL_LEGAL_FORM_ID, $legalFormId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($legalFormId['max'])) {
                $this->addUsingAlias(LegalFormTranslationTableMap::COL_LEGAL_FORM_ID, $legalFormId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LegalFormTranslationTableMap::COL_LEGAL_FORM_ID, $legalFormId, $comparison);
    }

    /**
     * Filter the query on the language_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguageId(1234); // WHERE language_id = 1234
     * $query->filterByLanguageId(array(12, 34)); // WHERE language_id IN (12, 34)
     * $query->filterByLanguageId(array('min' => 12)); // WHERE language_id > 12
     * </code>
     *
     * @see       filterByLanguage()
     *
     * @param     mixed $languageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function filterByLanguageId($languageId = null, $comparison = null)
    {
        if (is_array($languageId)) {
            $useMinMax = false;
            if (isset($languageId['min'])) {
                $this->addUsingAlias(LegalFormTranslationTableMap::COL_LANGUAGE_ID, $languageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($languageId['max'])) {
                $this->addUsingAlias(LegalFormTranslationTableMap::COL_LANGUAGE_ID, $languageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LegalFormTranslationTableMap::COL_LANGUAGE_ID, $languageId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LegalFormTranslationTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(LegalFormTranslationTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(LegalFormTranslationTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LegalFormTranslationTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(LegalFormTranslationTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(LegalFormTranslationTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LegalFormTranslationTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\LegalForm object
     *
     * @param \Model\Setting\MasterTable\LegalForm|ObjectCollection $legalForm The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function filterByLegalForm($legalForm, $comparison = null)
    {
        if ($legalForm instanceof \Model\Setting\MasterTable\LegalForm) {
            return $this
                ->addUsingAlias(LegalFormTranslationTableMap::COL_LEGAL_FORM_ID, $legalForm->getId(), $comparison);
        } elseif ($legalForm instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LegalFormTranslationTableMap::COL_LEGAL_FORM_ID, $legalForm->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLegalForm() only accepts arguments of type \Model\Setting\MasterTable\LegalForm or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LegalForm relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function joinLegalForm($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LegalForm');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LegalForm');
        }

        return $this;
    }

    /**
     * Use the LegalForm relation LegalForm object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\LegalFormQuery A secondary query class using the current class as primary query
     */
    public function useLegalFormQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLegalForm($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LegalForm', '\Model\Setting\MasterTable\LegalFormQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Language object
     *
     * @param \Model\Setting\MasterTable\Language|ObjectCollection $language The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function filterByLanguage($language, $comparison = null)
    {
        if ($language instanceof \Model\Setting\MasterTable\Language) {
            return $this
                ->addUsingAlias(LegalFormTranslationTableMap::COL_LANGUAGE_ID, $language->getId(), $comparison);
        } elseif ($language instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LegalFormTranslationTableMap::COL_LANGUAGE_ID, $language->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLanguage() only accepts arguments of type \Model\Setting\MasterTable\Language or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Language relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function joinLanguage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Language');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Language');
        }

        return $this;
    }

    /**
     * Use the Language relation Language object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\LanguageQuery A secondary query class using the current class as primary query
     */
    public function useLanguageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLanguage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Language', '\Model\Setting\MasterTable\LanguageQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildLegalFormTranslation $legalFormTranslation Object to remove from the list of results
     *
     * @return $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function prune($legalFormTranslation = null)
    {
        if ($legalFormTranslation) {
            $this->addUsingAlias(LegalFormTranslationTableMap::COL_ID, $legalFormTranslation->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mt_legal_form_translation table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LegalFormTranslationTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            LegalFormTranslationTableMap::clearInstancePool();
            LegalFormTranslationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LegalFormTranslationTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(LegalFormTranslationTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            LegalFormTranslationTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            LegalFormTranslationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(LegalFormTranslationTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(LegalFormTranslationTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(LegalFormTranslationTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(LegalFormTranslationTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(LegalFormTranslationTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildLegalFormTranslationQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(LegalFormTranslationTableMap::COL_CREATED_AT);
    }

} // LegalFormTranslationQuery
