<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\Sale\SaleOrderPayment;
use Model\Setting\MasterTable\PaymentMethod as ChildPaymentMethod;
use Model\Setting\MasterTable\PaymentMethodQuery as ChildPaymentMethodQuery;
use Model\Setting\MasterTable\Map\PaymentMethodTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'payment_method' table.
 *
 *
 *
 * @method     ChildPaymentMethodQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildPaymentMethodQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildPaymentMethodQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method     ChildPaymentMethodQuery orderByPsp($order = Criteria::ASC) Order by the psp column
 * @method     ChildPaymentMethodQuery orderByIsEnabledWebshop($order = Criteria::ASC) Order by the is_enabled_webshop column
 *
 * @method     ChildPaymentMethodQuery groupById() Group by the id column
 * @method     ChildPaymentMethodQuery groupByName() Group by the name column
 * @method     ChildPaymentMethodQuery groupByCode() Group by the code column
 * @method     ChildPaymentMethodQuery groupByPsp() Group by the psp column
 * @method     ChildPaymentMethodQuery groupByIsEnabledWebshop() Group by the is_enabled_webshop column
 *
 * @method     ChildPaymentMethodQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildPaymentMethodQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildPaymentMethodQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildPaymentMethodQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildPaymentMethodQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildPaymentMethodQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildPaymentMethodQuery leftJoinSaleOrderPayment($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrderPayment relation
 * @method     ChildPaymentMethodQuery rightJoinSaleOrderPayment($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrderPayment relation
 * @method     ChildPaymentMethodQuery innerJoinSaleOrderPayment($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrderPayment relation
 *
 * @method     ChildPaymentMethodQuery joinWithSaleOrderPayment($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrderPayment relation
 *
 * @method     ChildPaymentMethodQuery leftJoinWithSaleOrderPayment() Adds a LEFT JOIN clause and with to the query using the SaleOrderPayment relation
 * @method     ChildPaymentMethodQuery rightJoinWithSaleOrderPayment() Adds a RIGHT JOIN clause and with to the query using the SaleOrderPayment relation
 * @method     ChildPaymentMethodQuery innerJoinWithSaleOrderPayment() Adds a INNER JOIN clause and with to the query using the SaleOrderPayment relation
 *
 * @method     \Model\Sale\SaleOrderPaymentQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildPaymentMethod findOne(ConnectionInterface $con = null) Return the first ChildPaymentMethod matching the query
 * @method     ChildPaymentMethod findOneOrCreate(ConnectionInterface $con = null) Return the first ChildPaymentMethod matching the query, or a new ChildPaymentMethod object populated from the query conditions when no match is found
 *
 * @method     ChildPaymentMethod findOneById(int $id) Return the first ChildPaymentMethod filtered by the id column
 * @method     ChildPaymentMethod findOneByName(string $name) Return the first ChildPaymentMethod filtered by the name column
 * @method     ChildPaymentMethod findOneByCode(string $code) Return the first ChildPaymentMethod filtered by the code column
 * @method     ChildPaymentMethod findOneByPsp(string $psp) Return the first ChildPaymentMethod filtered by the psp column
 * @method     ChildPaymentMethod findOneByIsEnabledWebshop(boolean $is_enabled_webshop) Return the first ChildPaymentMethod filtered by the is_enabled_webshop column *

 * @method     ChildPaymentMethod requirePk($key, ConnectionInterface $con = null) Return the ChildPaymentMethod by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentMethod requireOne(ConnectionInterface $con = null) Return the first ChildPaymentMethod matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaymentMethod requireOneById(int $id) Return the first ChildPaymentMethod filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentMethod requireOneByName(string $name) Return the first ChildPaymentMethod filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentMethod requireOneByCode(string $code) Return the first ChildPaymentMethod filtered by the code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentMethod requireOneByPsp(string $psp) Return the first ChildPaymentMethod filtered by the psp column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildPaymentMethod requireOneByIsEnabledWebshop(boolean $is_enabled_webshop) Return the first ChildPaymentMethod filtered by the is_enabled_webshop column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildPaymentMethod[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildPaymentMethod objects based on current ModelCriteria
 * @method     ChildPaymentMethod[]|ObjectCollection findById(int $id) Return ChildPaymentMethod objects filtered by the id column
 * @method     ChildPaymentMethod[]|ObjectCollection findByName(string $name) Return ChildPaymentMethod objects filtered by the name column
 * @method     ChildPaymentMethod[]|ObjectCollection findByCode(string $code) Return ChildPaymentMethod objects filtered by the code column
 * @method     ChildPaymentMethod[]|ObjectCollection findByPsp(string $psp) Return ChildPaymentMethod objects filtered by the psp column
 * @method     ChildPaymentMethod[]|ObjectCollection findByIsEnabledWebshop(boolean $is_enabled_webshop) Return ChildPaymentMethod objects filtered by the is_enabled_webshop column
 * @method     ChildPaymentMethod[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class PaymentMethodQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\MasterTable\Base\PaymentMethodQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\MasterTable\\PaymentMethod', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildPaymentMethodQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildPaymentMethodQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildPaymentMethodQuery) {
            return $criteria;
        }
        $query = new ChildPaymentMethodQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildPaymentMethod|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(PaymentMethodTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = PaymentMethodTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildPaymentMethod A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, code, psp, is_enabled_webshop FROM payment_method WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildPaymentMethod $obj */
            $obj = new ChildPaymentMethod();
            $obj->hydrate($row);
            PaymentMethodTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildPaymentMethod|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildPaymentMethodQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PaymentMethodTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildPaymentMethodQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PaymentMethodTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentMethodQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PaymentMethodTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PaymentMethodTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentMethodTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentMethodQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentMethodTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%', Criteria::LIKE); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentMethodQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentMethodTableMap::COL_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the psp column
     *
     * Example usage:
     * <code>
     * $query->filterByPsp('fooValue');   // WHERE psp = 'fooValue'
     * $query->filterByPsp('%fooValue%', Criteria::LIKE); // WHERE psp LIKE '%fooValue%'
     * </code>
     *
     * @param     string $psp The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentMethodQuery The current query, for fluid interface
     */
    public function filterByPsp($psp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($psp)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PaymentMethodTableMap::COL_PSP, $psp, $comparison);
    }

    /**
     * Filter the query on the is_enabled_webshop column
     *
     * Example usage:
     * <code>
     * $query->filterByIsEnabledWebshop(true); // WHERE is_enabled_webshop = true
     * $query->filterByIsEnabledWebshop('yes'); // WHERE is_enabled_webshop = true
     * </code>
     *
     * @param     boolean|string $isEnabledWebshop The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildPaymentMethodQuery The current query, for fluid interface
     */
    public function filterByIsEnabledWebshop($isEnabledWebshop = null, $comparison = null)
    {
        if (is_string($isEnabledWebshop)) {
            $isEnabledWebshop = in_array(strtolower($isEnabledWebshop), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(PaymentMethodTableMap::COL_IS_ENABLED_WEBSHOP, $isEnabledWebshop, $comparison);
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrderPayment object
     *
     * @param \Model\Sale\SaleOrderPayment|ObjectCollection $saleOrderPayment the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildPaymentMethodQuery The current query, for fluid interface
     */
    public function filterBySaleOrderPayment($saleOrderPayment, $comparison = null)
    {
        if ($saleOrderPayment instanceof \Model\Sale\SaleOrderPayment) {
            return $this
                ->addUsingAlias(PaymentMethodTableMap::COL_ID, $saleOrderPayment->getPaymentMethodId(), $comparison);
        } elseif ($saleOrderPayment instanceof ObjectCollection) {
            return $this
                ->useSaleOrderPaymentQuery()
                ->filterByPrimaryKeys($saleOrderPayment->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleOrderPayment() only accepts arguments of type \Model\Sale\SaleOrderPayment or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrderPayment relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildPaymentMethodQuery The current query, for fluid interface
     */
    public function joinSaleOrderPayment($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrderPayment');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrderPayment');
        }

        return $this;
    }

    /**
     * Use the SaleOrderPayment relation SaleOrderPayment object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderPaymentQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderPaymentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaleOrderPayment($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrderPayment', '\Model\Sale\SaleOrderPaymentQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildPaymentMethod $paymentMethod Object to remove from the list of results
     *
     * @return $this|ChildPaymentMethodQuery The current query, for fluid interface
     */
    public function prune($paymentMethod = null)
    {
        if ($paymentMethod) {
            $this->addUsingAlias(PaymentMethodTableMap::COL_ID, $paymentMethod->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the payment_method table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentMethodTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            PaymentMethodTableMap::clearInstancePool();
            PaymentMethodTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PaymentMethodTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(PaymentMethodTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            PaymentMethodTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            PaymentMethodTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // PaymentMethodQuery
