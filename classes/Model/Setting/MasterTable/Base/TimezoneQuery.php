<?php

namespace Model\Setting\MasterTable\Base;

use \Exception;
use \PDO;
use Model\Setting\MasterTable\Timezone as ChildTimezone;
use Model\Setting\MasterTable\TimezoneQuery as ChildTimezoneQuery;
use Model\Setting\MasterTable\Map\TimezoneTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'timezone' table.
 *
 *
 *
 * @method     ChildTimezoneQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildTimezoneQuery orderByContinent($order = Criteria::ASC) Order by the continent column
 * @method     ChildTimezoneQuery orderByZoneName($order = Criteria::ASC) Order by the zone_name column
 * @method     ChildTimezoneQuery orderByTimeDifference($order = Criteria::ASC) Order by the time_difference column
 *
 * @method     ChildTimezoneQuery groupById() Group by the id column
 * @method     ChildTimezoneQuery groupByContinent() Group by the continent column
 * @method     ChildTimezoneQuery groupByZoneName() Group by the zone_name column
 * @method     ChildTimezoneQuery groupByTimeDifference() Group by the time_difference column
 *
 * @method     ChildTimezoneQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildTimezoneQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildTimezoneQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildTimezoneQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildTimezoneQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildTimezoneQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildTimezone findOne(ConnectionInterface $con = null) Return the first ChildTimezone matching the query
 * @method     ChildTimezone findOneOrCreate(ConnectionInterface $con = null) Return the first ChildTimezone matching the query, or a new ChildTimezone object populated from the query conditions when no match is found
 *
 * @method     ChildTimezone findOneById(int $id) Return the first ChildTimezone filtered by the id column
 * @method     ChildTimezone findOneByContinent(string $continent) Return the first ChildTimezone filtered by the continent column
 * @method     ChildTimezone findOneByZoneName(string $zone_name) Return the first ChildTimezone filtered by the zone_name column
 * @method     ChildTimezone findOneByTimeDifference(string $time_difference) Return the first ChildTimezone filtered by the time_difference column *

 * @method     ChildTimezone requirePk($key, ConnectionInterface $con = null) Return the ChildTimezone by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTimezone requireOne(ConnectionInterface $con = null) Return the first ChildTimezone matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTimezone requireOneById(int $id) Return the first ChildTimezone filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTimezone requireOneByContinent(string $continent) Return the first ChildTimezone filtered by the continent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTimezone requireOneByZoneName(string $zone_name) Return the first ChildTimezone filtered by the zone_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildTimezone requireOneByTimeDifference(string $time_difference) Return the first ChildTimezone filtered by the time_difference column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildTimezone[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildTimezone objects based on current ModelCriteria
 * @method     ChildTimezone[]|ObjectCollection findById(int $id) Return ChildTimezone objects filtered by the id column
 * @method     ChildTimezone[]|ObjectCollection findByContinent(string $continent) Return ChildTimezone objects filtered by the continent column
 * @method     ChildTimezone[]|ObjectCollection findByZoneName(string $zone_name) Return ChildTimezone objects filtered by the zone_name column
 * @method     ChildTimezone[]|ObjectCollection findByTimeDifference(string $time_difference) Return ChildTimezone objects filtered by the time_difference column
 * @method     ChildTimezone[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class TimezoneQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Setting\MasterTable\Base\TimezoneQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Setting\\MasterTable\\Timezone', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildTimezoneQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildTimezoneQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildTimezoneQuery) {
            return $criteria;
        }
        $query = new ChildTimezoneQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildTimezone|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(TimezoneTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = TimezoneTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildTimezone A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, continent, zone_name, time_difference FROM timezone WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildTimezone $obj */
            $obj = new ChildTimezone();
            $obj->hydrate($row);
            TimezoneTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildTimezone|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildTimezoneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(TimezoneTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildTimezoneQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(TimezoneTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTimezoneQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(TimezoneTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(TimezoneTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TimezoneTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the continent column
     *
     * Example usage:
     * <code>
     * $query->filterByContinent('fooValue');   // WHERE continent = 'fooValue'
     * $query->filterByContinent('%fooValue%', Criteria::LIKE); // WHERE continent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $continent The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTimezoneQuery The current query, for fluid interface
     */
    public function filterByContinent($continent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($continent)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TimezoneTableMap::COL_CONTINENT, $continent, $comparison);
    }

    /**
     * Filter the query on the zone_name column
     *
     * Example usage:
     * <code>
     * $query->filterByZoneName('fooValue');   // WHERE zone_name = 'fooValue'
     * $query->filterByZoneName('%fooValue%', Criteria::LIKE); // WHERE zone_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $zoneName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTimezoneQuery The current query, for fluid interface
     */
    public function filterByZoneName($zoneName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($zoneName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TimezoneTableMap::COL_ZONE_NAME, $zoneName, $comparison);
    }

    /**
     * Filter the query on the time_difference column
     *
     * Example usage:
     * <code>
     * $query->filterByTimeDifference('fooValue');   // WHERE time_difference = 'fooValue'
     * $query->filterByTimeDifference('%fooValue%', Criteria::LIKE); // WHERE time_difference LIKE '%fooValue%'
     * </code>
     *
     * @param     string $timeDifference The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildTimezoneQuery The current query, for fluid interface
     */
    public function filterByTimeDifference($timeDifference = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($timeDifference)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(TimezoneTableMap::COL_TIME_DIFFERENCE, $timeDifference, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildTimezone $timezone Object to remove from the list of results
     *
     * @return $this|ChildTimezoneQuery The current query, for fluid interface
     */
    public function prune($timezone = null)
    {
        if ($timezone) {
            $this->addUsingAlias(TimezoneTableMap::COL_ID, $timezone->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the timezone table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TimezoneTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            TimezoneTableMap::clearInstancePool();
            TimezoneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(TimezoneTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(TimezoneTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            TimezoneTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            TimezoneTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // TimezoneQuery
