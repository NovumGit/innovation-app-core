<?php

namespace Model\Setting\MasterTable\Map;

use Model\Setting\MasterTable\CountryShippingMethod;
use Model\Setting\MasterTable\CountryShippingMethodQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'country_shipping_method' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CountryShippingMethodTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Setting.MasterTable.Map.CountryShippingMethodTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'country_shipping_method';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Setting\\MasterTable\\CountryShippingMethod';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Setting.MasterTable.CountryShippingMethod';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 5;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 5;

    /**
     * the column name for the id field
     */
    const COL_ID = 'country_shipping_method.id';

    /**
     * the column name for the country_id field
     */
    const COL_COUNTRY_ID = 'country_shipping_method.country_id';

    /**
     * the column name for the shipping_method_id field
     */
    const COL_SHIPPING_METHOD_ID = 'country_shipping_method.shipping_method_id';

    /**
     * the column name for the is_enabled_website field
     */
    const COL_IS_ENABLED_WEBSITE = 'country_shipping_method.is_enabled_website';

    /**
     * the column name for the price field
     */
    const COL_PRICE = 'country_shipping_method.price';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'CountryId', 'ShippingMethodId', 'IsEnabledWebsite', 'Price', ),
        self::TYPE_CAMELNAME     => array('id', 'countryId', 'shippingMethodId', 'isEnabledWebsite', 'price', ),
        self::TYPE_COLNAME       => array(CountryShippingMethodTableMap::COL_ID, CountryShippingMethodTableMap::COL_COUNTRY_ID, CountryShippingMethodTableMap::COL_SHIPPING_METHOD_ID, CountryShippingMethodTableMap::COL_IS_ENABLED_WEBSITE, CountryShippingMethodTableMap::COL_PRICE, ),
        self::TYPE_FIELDNAME     => array('id', 'country_id', 'shipping_method_id', 'is_enabled_website', 'price', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'CountryId' => 1, 'ShippingMethodId' => 2, 'IsEnabledWebsite' => 3, 'Price' => 4, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'countryId' => 1, 'shippingMethodId' => 2, 'isEnabledWebsite' => 3, 'price' => 4, ),
        self::TYPE_COLNAME       => array(CountryShippingMethodTableMap::COL_ID => 0, CountryShippingMethodTableMap::COL_COUNTRY_ID => 1, CountryShippingMethodTableMap::COL_SHIPPING_METHOD_ID => 2, CountryShippingMethodTableMap::COL_IS_ENABLED_WEBSITE => 3, CountryShippingMethodTableMap::COL_PRICE => 4, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'country_id' => 1, 'shipping_method_id' => 2, 'is_enabled_website' => 3, 'price' => 4, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('country_shipping_method');
        $this->setPhpName('CountryShippingMethod');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Setting\\MasterTable\\CountryShippingMethod');
        $this->setPackage('Model.Setting.MasterTable');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('country_id', 'CountryId', 'INTEGER', 'mt_country', 'id', true, null, null);
        $this->addForeignKey('shipping_method_id', 'ShippingMethodId', 'INTEGER', 'mt_shipping_method', 'id', true, null, null);
        $this->addColumn('is_enabled_website', 'IsEnabledWebsite', 'BOOLEAN', true, 1, false);
        $this->addColumn('price', 'Price', 'FLOAT', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Country', '\\Model\\Setting\\MasterTable\\Country', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':country_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('ShippingMethod', '\\Model\\Setting\\MasterTable\\ShippingMethod', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':shipping_method_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CountryShippingMethodTableMap::CLASS_DEFAULT : CountryShippingMethodTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CountryShippingMethod object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CountryShippingMethodTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CountryShippingMethodTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CountryShippingMethodTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CountryShippingMethodTableMap::OM_CLASS;
            /** @var CountryShippingMethod $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CountryShippingMethodTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CountryShippingMethodTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CountryShippingMethodTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CountryShippingMethod $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CountryShippingMethodTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CountryShippingMethodTableMap::COL_ID);
            $criteria->addSelectColumn(CountryShippingMethodTableMap::COL_COUNTRY_ID);
            $criteria->addSelectColumn(CountryShippingMethodTableMap::COL_SHIPPING_METHOD_ID);
            $criteria->addSelectColumn(CountryShippingMethodTableMap::COL_IS_ENABLED_WEBSITE);
            $criteria->addSelectColumn(CountryShippingMethodTableMap::COL_PRICE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.country_id');
            $criteria->addSelectColumn($alias . '.shipping_method_id');
            $criteria->addSelectColumn($alias . '.is_enabled_website');
            $criteria->addSelectColumn($alias . '.price');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CountryShippingMethodTableMap::DATABASE_NAME)->getTable(CountryShippingMethodTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CountryShippingMethodTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CountryShippingMethodTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CountryShippingMethodTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CountryShippingMethod or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CountryShippingMethod object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CountryShippingMethodTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Setting\MasterTable\CountryShippingMethod) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CountryShippingMethodTableMap::DATABASE_NAME);
            $criteria->add(CountryShippingMethodTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CountryShippingMethodQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CountryShippingMethodTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CountryShippingMethodTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the country_shipping_method table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CountryShippingMethodQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CountryShippingMethod or Criteria object.
     *
     * @param mixed               $criteria Criteria or CountryShippingMethod object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CountryShippingMethodTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CountryShippingMethod object
        }

        if ($criteria->containsKey(CountryShippingMethodTableMap::COL_ID) && $criteria->keyContainsValue(CountryShippingMethodTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CountryShippingMethodTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CountryShippingMethodQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CountryShippingMethodTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CountryShippingMethodTableMap::buildTableMap();
