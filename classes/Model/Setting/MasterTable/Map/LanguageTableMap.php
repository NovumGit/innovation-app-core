<?php

namespace Model\Setting\MasterTable\Map;

use Model\Cms\Map\SiteBlogTableMap;
use Model\Cms\Map\SiteUspTableMap;
use Model\Map\ContactMessageTableMap;
use Model\Map\PromoProductTranslationTableMap;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'mt_language' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class LanguageTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Setting.MasterTable.Map.LanguageTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'mt_language';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Setting\\MasterTable\\Language';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Setting.MasterTable.Language';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'mt_language.id';

    /**
     * the column name for the is_deleted field
     */
    const COL_IS_DELETED = 'mt_language.is_deleted';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'mt_language.description';

    /**
     * the column name for the locale_code field
     */
    const COL_LOCALE_CODE = 'mt_language.locale_code';

    /**
     * the column name for the is_enabled_cms field
     */
    const COL_IS_ENABLED_CMS = 'mt_language.is_enabled_cms';

    /**
     * the column name for the is_enabled_webshop field
     */
    const COL_IS_ENABLED_WEBSHOP = 'mt_language.is_enabled_webshop';

    /**
     * the column name for the is_default_cms field
     */
    const COL_IS_DEFAULT_CMS = 'mt_language.is_default_cms';

    /**
     * the column name for the is_default_webshop field
     */
    const COL_IS_DEFAULT_WEBSHOP = 'mt_language.is_default_webshop';

    /**
     * the column name for the flag_icon field
     */
    const COL_FLAG_ICON = 'mt_language.flag_icon';

    /**
     * the column name for the shop_url_prefix field
     */
    const COL_SHOP_URL_PREFIX = 'mt_language.shop_url_prefix';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'mt_language.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'mt_language.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ItemDeleted', 'Description', 'LocaleCode', 'IsEnabledCms', 'IsEnabledWebshop', 'IsDefaultCms', 'IsDefaultWebshop', 'FlagIcon', 'ShopUrlPrefix', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'itemDeleted', 'description', 'localeCode', 'isEnabledCms', 'isEnabledWebshop', 'isDefaultCms', 'isDefaultWebshop', 'flagIcon', 'shopUrlPrefix', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(LanguageTableMap::COL_ID, LanguageTableMap::COL_IS_DELETED, LanguageTableMap::COL_DESCRIPTION, LanguageTableMap::COL_LOCALE_CODE, LanguageTableMap::COL_IS_ENABLED_CMS, LanguageTableMap::COL_IS_ENABLED_WEBSHOP, LanguageTableMap::COL_IS_DEFAULT_CMS, LanguageTableMap::COL_IS_DEFAULT_WEBSHOP, LanguageTableMap::COL_FLAG_ICON, LanguageTableMap::COL_SHOP_URL_PREFIX, LanguageTableMap::COL_CREATED_AT, LanguageTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'is_deleted', 'description', 'locale_code', 'is_enabled_cms', 'is_enabled_webshop', 'is_default_cms', 'is_default_webshop', 'flag_icon', 'shop_url_prefix', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ItemDeleted' => 1, 'Description' => 2, 'LocaleCode' => 3, 'IsEnabledCms' => 4, 'IsEnabledWebshop' => 5, 'IsDefaultCms' => 6, 'IsDefaultWebshop' => 7, 'FlagIcon' => 8, 'ShopUrlPrefix' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'itemDeleted' => 1, 'description' => 2, 'localeCode' => 3, 'isEnabledCms' => 4, 'isEnabledWebshop' => 5, 'isDefaultCms' => 6, 'isDefaultWebshop' => 7, 'flagIcon' => 8, 'shopUrlPrefix' => 9, 'createdAt' => 10, 'updatedAt' => 11, ),
        self::TYPE_COLNAME       => array(LanguageTableMap::COL_ID => 0, LanguageTableMap::COL_IS_DELETED => 1, LanguageTableMap::COL_DESCRIPTION => 2, LanguageTableMap::COL_LOCALE_CODE => 3, LanguageTableMap::COL_IS_ENABLED_CMS => 4, LanguageTableMap::COL_IS_ENABLED_WEBSHOP => 5, LanguageTableMap::COL_IS_DEFAULT_CMS => 6, LanguageTableMap::COL_IS_DEFAULT_WEBSHOP => 7, LanguageTableMap::COL_FLAG_ICON => 8, LanguageTableMap::COL_SHOP_URL_PREFIX => 9, LanguageTableMap::COL_CREATED_AT => 10, LanguageTableMap::COL_UPDATED_AT => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'is_deleted' => 1, 'description' => 2, 'locale_code' => 3, 'is_enabled_cms' => 4, 'is_enabled_webshop' => 5, 'is_default_cms' => 6, 'is_default_webshop' => 7, 'flag_icon' => 8, 'shop_url_prefix' => 9, 'created_at' => 10, 'updated_at' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('mt_language');
        $this->setPhpName('Language');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Setting\\MasterTable\\Language');
        $this->setPackage('Model.Setting.MasterTable');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('is_deleted', 'ItemDeleted', 'BOOLEAN', false, 1, false);
        $this->addColumn('description', 'Description', 'VARCHAR', false, 120, null);
        $this->addColumn('locale_code', 'LocaleCode', 'VARCHAR', false, 120, null);
        $this->addColumn('is_enabled_cms', 'IsEnabledCms', 'BOOLEAN', true, 1, null);
        $this->addColumn('is_enabled_webshop', 'IsEnabledWebshop', 'BOOLEAN', true, 1, null);
        $this->addColumn('is_default_cms', 'IsDefaultCms', 'BOOLEAN', true, 1, null);
        $this->addColumn('is_default_webshop', 'IsDefaultWebshop', 'BOOLEAN', true, 1, null);
        $this->addColumn('flag_icon', 'FlagIcon', 'VARCHAR', false, 20, null);
        $this->addColumn('shop_url_prefix', 'ShopUrlPrefix', 'VARCHAR', false, 20, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('LegalFormTranslation', '\\Model\\Setting\\MasterTable\\LegalFormTranslation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'LegalFormTranslations', false);
        $this->addRelation('ContactMessage', '\\Model\\ContactMessage', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'SET NULL', null, 'ContactMessages', false);
        $this->addRelation('User', '\\Model\\Account\\User', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':prefered_language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'Users', false);
        $this->addRelation('Customer', '\\Model\\Crm\\Customer', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'Customers', false);
        $this->addRelation('CategoryTranslation', '\\Model\\Category\\CategoryTranslation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'CategoryTranslations', false);
        $this->addRelation('PromoProductTranslation', '\\Model\\PromoProductTranslation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'PromoProductTranslations', false);
        $this->addRelation('ProductTranslation', '\\Model\\ProductTranslation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'ProductTranslations', false);
        $this->addRelation('VatTranslation', '\\Model\\Setting\\MasterTable\\VatTranslation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'VatTranslations', false);
        $this->addRelation('UsageTranslation', '\\Model\\Setting\\MasterTable\\UsageTranslation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'UsageTranslations', false);
        $this->addRelation('ColorTranslation', '\\Model\\Setting\\MasterTable\\ColorTranslation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'ColorTranslations', false);
        $this->addRelation('SiteUsp', '\\Model\\Cms\\SiteUsp', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'SiteUsps', false);
        $this->addRelation('SiteFooterBlockTranslation', '\\Model\\Cms\\SiteFooterBlockTranslation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'SiteFooterBlockTranslations', false);
        $this->addRelation('ProductUnitTranslation', '\\Model\\Setting\\MasterTable\\ProductUnitTranslation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'ProductUnitTranslations', false);
        $this->addRelation('ProductDeliveryTimeTranslation', '\\Model\\Setting\\MasterTable\\ProductDeliveryTimeTranslation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'ProductDeliveryTimeTranslations', false);
        $this->addRelation('SiteBlog', '\\Model\\Cms\\SiteBlog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'SiteBlogs', false);
        $this->addRelation('SiteFAQCategoryTranslation', '\\Model\\Cms\\SiteFAQCategoryTranslation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'SiteFAQCategoryTranslations', false);
        $this->addRelation('SiteFAQ', '\\Model\\Cms\\SiteFAQ', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'SiteFAQs', false);
        $this->addRelation('SitePage', '\\Model\\Cms\\SitePage', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'SitePages', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to mt_language     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ContactMessageTableMap::clearInstancePool();
        PromoProductTranslationTableMap::clearInstancePool();
        SiteUspTableMap::clearInstancePool();
        SiteBlogTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? LanguageTableMap::CLASS_DEFAULT : LanguageTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Language object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = LanguageTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = LanguageTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + LanguageTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = LanguageTableMap::OM_CLASS;
            /** @var Language $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            LanguageTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = LanguageTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = LanguageTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Language $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                LanguageTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(LanguageTableMap::COL_ID);
            $criteria->addSelectColumn(LanguageTableMap::COL_IS_DELETED);
            $criteria->addSelectColumn(LanguageTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(LanguageTableMap::COL_LOCALE_CODE);
            $criteria->addSelectColumn(LanguageTableMap::COL_IS_ENABLED_CMS);
            $criteria->addSelectColumn(LanguageTableMap::COL_IS_ENABLED_WEBSHOP);
            $criteria->addSelectColumn(LanguageTableMap::COL_IS_DEFAULT_CMS);
            $criteria->addSelectColumn(LanguageTableMap::COL_IS_DEFAULT_WEBSHOP);
            $criteria->addSelectColumn(LanguageTableMap::COL_FLAG_ICON);
            $criteria->addSelectColumn(LanguageTableMap::COL_SHOP_URL_PREFIX);
            $criteria->addSelectColumn(LanguageTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(LanguageTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.is_deleted');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.locale_code');
            $criteria->addSelectColumn($alias . '.is_enabled_cms');
            $criteria->addSelectColumn($alias . '.is_enabled_webshop');
            $criteria->addSelectColumn($alias . '.is_default_cms');
            $criteria->addSelectColumn($alias . '.is_default_webshop');
            $criteria->addSelectColumn($alias . '.flag_icon');
            $criteria->addSelectColumn($alias . '.shop_url_prefix');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(LanguageTableMap::DATABASE_NAME)->getTable(LanguageTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(LanguageTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(LanguageTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new LanguageTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Language or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Language object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LanguageTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Setting\MasterTable\Language) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(LanguageTableMap::DATABASE_NAME);
            $criteria->add(LanguageTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = LanguageQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            LanguageTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                LanguageTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the mt_language table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return LanguageQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Language or Criteria object.
     *
     * @param mixed               $criteria Criteria or Language object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LanguageTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Language object
        }

        if ($criteria->containsKey(LanguageTableMap::COL_ID) && $criteria->keyContainsValue(LanguageTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.LanguageTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = LanguageQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // LanguageTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
LanguageTableMap::buildTableMap();
