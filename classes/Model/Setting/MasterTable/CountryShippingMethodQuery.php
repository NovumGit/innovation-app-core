<?php

namespace Model\Setting\MasterTable;

use Core\QueryMapper;
use Exception\LogicException;
use Model\Setting\MasterTable\Base\CountryShippingMethodQuery as BaseCountryShippingMethodQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'country_shipping_method' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CountryShippingMethodQuery extends BaseCountryShippingMethodQuery
{

    static function getShippingMethodsByCountryName($sCountryName, $bFilterEnabledWebsite = true):array
    {
        $oCountry = CountryQuery::create()->findOneByName($sCountryName);

        return self::getShippingMethodsByCountryId($oCountry->getId(), $bFilterEnabledWebsite);
    }
    static function getShippingMethodsByCountryId($iCountryId, $bFilterEnabledWebsite = true):array
    {
        $aCountryShippingMethodsQuery = self::create();
        if($bFilterEnabledWebsite != null)
        {
            $aCountryShippingMethodsQuery->filterByIsEnabledWebsite($bFilterEnabledWebsite);
        }
        $aCountryShippingMethods = $aCountryShippingMethodsQuery->findByCountryId($iCountryId);


        $aShippingMethods = [];
        foreach ($aCountryShippingMethods as $oCountryShippingMethod)
        {

            $aShippingMethods[] = [
                'method' => $oCountryShippingMethod->getShippingMethod(),
                'price' => $oCountryShippingMethod->getPrice()
            ];
        }

        if(empty($aShippingMethods))
        {
            throw new LogicException("No shipping methods have been defined for this country.");
        }
        return $aShippingMethods;
    }
    static function getShippingMethodsByCountryIdAndWeight(int $iCountryId, int $iWeightGrams, bool $bFilterEnabledWebsite = true)
    {
        $sQuery = "SELECT id FROM 
                      mt_shipping_method 
                    WHERE 
                    ( 
                      (min_weight_grams < $iWeightGrams) OR min_weight_grams IS NULL 
                    )
                    AND 
                    (
                      (max_weight_grams >= $iWeightGrams) OR max_weight_grams IS NULL 
                    )";

        $aItems = QueryMapper::fetchArray($sQuery);

        $aShippingMethodInWeightRange = [];
        if(!empty($aItems))
        {
            foreach($aItems as $aItem)
            {
                $aShippingMethodInWeightRange[] = $aItem['id'];
            }
        }

        $aCountryShippingMethodsQuery = self::create();
        if($bFilterEnabledWebsite != null)
        {
            $aCountryShippingMethodsQuery->filterByIsEnabledWebsite($bFilterEnabledWebsite);
        }
        $aCountryShippingMethodsQuery->filterByShippingMethodId($aShippingMethodInWeightRange);
        $aCountryShippingMethods = $aCountryShippingMethodsQuery->findByCountryId($iCountryId);
        $aShippingMethods = [];
        foreach ($aCountryShippingMethods as $oCountryShippingMethod)
        {
            $aShippingMethods[] = [
                'method' => $oCountryShippingMethod->getShippingMethod(),
                'price' => $oCountryShippingMethod->getPrice()
            ];
        }
        if(empty($aShippingMethods))
        {
            throw new LogicException("No shipping methods have been defined for this country.");
        }
        return $aShippingMethods;
    }
}
