<?php

namespace Model\Setting\MasterTable;

use Model\Setting\MasterTable\Base\SaleOrderStatusQuery as BaseSaleOrderStatusQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'mt_sale_order_status' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SaleOrderStatusQuery extends BaseSaleOrderStatusQuery
{
    function getOrCreate($sName, $sCode)
    {
        $oSaleOrderStatus = self::findOneByCode($sCode);
        if(!$oSaleOrderStatus instanceof SaleOrderStatus)
        {
            // Alledrie
            $oSaleOrderStatus = new SaleOrderStatus();
            $oSaleOrderStatus->setCode($sCode);
            $oSaleOrderStatus->setName($sName);
            $oSaleOrderStatus->save();
        }

        return $oSaleOrderStatus;
    }

}
