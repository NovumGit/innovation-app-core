<?php

namespace Model\Setting\MasterTable;

use Model\Setting\MasterTable\Base\Sale_order_notification_type as BaseSale_order_notification_type;

/**
 * Skeleton subclass for representing a row from the 'mt_sale_order_notification_type' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Sale_order_notification_type extends BaseSale_order_notification_type
{

}
