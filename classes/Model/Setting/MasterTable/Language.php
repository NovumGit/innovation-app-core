<?php

namespace Model\Setting\MasterTable;

use Model\Setting\MasterTable\Base\Language as BaseLanguage;

/**
 * Skeleton subclass for representing a row from the 'mt_language' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Language extends BaseLanguage
{

    /**
     * @param int $sSize [16|24|32|48|64|icns|ico]
     * @return string
     */
    function getFlagIconPath($sSize = 24)
    {
        return "/img/flags/flags-iso/flat/$sSize/".strtoupper($this->getFlagIcon()).'.png';
    }
}
