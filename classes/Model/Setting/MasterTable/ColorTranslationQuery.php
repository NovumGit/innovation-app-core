<?php

namespace Model\Setting\MasterTable;

use Model\Setting\MasterTable\Base\ColorTranslationQuery as BaseColorTranslationQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'mt_color_translation' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ColorTranslationQuery extends BaseColorTranslationQuery
{

}
