<?php

namespace Model\Setting\MasterTable;

use Model\Setting\MasterTable\Base\ActivityTypeQuery as BaseActivityTypeQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'activity_type' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ActivityTypeQuery extends BaseActivityTypeQuery
{

}
