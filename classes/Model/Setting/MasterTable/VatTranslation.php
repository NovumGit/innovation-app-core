<?php

namespace Model\Setting\MasterTable;

use Model\Setting\MasterTable\Base\VatTranslation as BaseVatTranslation;

/**
 * Skeleton subclass for representing a row from the 'mt_vat_translation' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class VatTranslation extends BaseVatTranslation
{

}
