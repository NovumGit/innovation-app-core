<?php

namespace Model\Setting\MasterTable;

use Core\PropelSqlTrait;
use Core\QueryMapper;
use Core\Utils;
use Model\Setting\MasterTable\Base\LanguageQuery as BaseLanguageQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\Collection\ObjectCollection;

/**
 * Skeleton subclass for performing query and update operations on the 'mt_language' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class LanguageQuery extends BaseLanguageQuery
{
	use PropelSqlTrait;

	static $aAllLanguages = [];
	static function getByIdCached($iLanguageID)
	{
		if(empty(self::$aAllLanguages))
		{
			$aLanguages = LanguageQuery::create()->find();
			foreach($aLanguages as $oLanguage)
			{
				self::$aAllLanguages[$oLanguage->getId()] = $oLanguage;
			}
		}
		if(isset(self::$aAllLanguages[$iLanguageID]))
		{
			return self::$aAllLanguages[$iLanguageID];
		}
		return null;
	}

    function findOneByLocaleCode(string $sLocaleCode)
    {
        if(!isset($_SESSION[$sLocaleCode]))
        {
            $_SESSION[$sLocaleCode] = parent::findOneByLocaleCode($sLocaleCode);
        }
        return $_SESSION[$sLocaleCode];
    }

    function findByIsEnabledWebshopOrIsEnabledCms() : ObjectCollection
    {
        $sQuery = "SELECT id FROM 
                        mt_language
                    WHERE
                      (is_enabled_cms = 1 OR is_enabled_webshop = 1)
                      AND is_deleted = 0";

        $aLanguageIds = QueryMapper::fetchArray($sQuery);

        if(!$aLanguageIds)
        {
            // Not a single language is enabled
            return null;
        }
        $aLanguageIdsFlat = [];
        foreach($aLanguageIds as $aLanguageId)
        {
            $aLanguageIdsFlat[] = $aLanguageId['id'];
        }

        $aLanguages = LanguageQuery::create()->filterById($aLanguageIdsFlat, Criteria::IN)->find();

        return $aLanguages;
    }

}
