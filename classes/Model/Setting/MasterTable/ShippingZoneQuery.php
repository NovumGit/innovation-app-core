<?php

namespace Model\Setting\MasterTable;

use Model\Setting\MasterTable\Base\ShippingZoneQuery as BaseShippingZoneQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'mt_shipping_zone' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ShippingZoneQuery extends BaseShippingZoneQuery
{

}
