<?php

namespace Model\Setting\MasterTable;

use Model\Setting\MasterTable\Base\LegalFormQuery as BaseLegalFormQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'mt_legal_form' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class LegalFormQuery extends BaseLegalFormQuery
{

}
