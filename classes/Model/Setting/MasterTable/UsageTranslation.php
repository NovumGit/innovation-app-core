<?php

namespace Model\Setting\MasterTable;

use Model\Setting\MasterTable\Base\UsageTranslation as BaseUsageTranslation;

/**
 * Skeleton subclass for representing a row from the 'mt_usage_translation' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UsageTranslation extends BaseUsageTranslation
{

}
