<?php

namespace Model\Setting\MasterTable;

use Model\Setting\MasterTable\Base\MasterPackingSlip as BaseMasterPackingSlip;

/**
 * Skeleton subclass for representing a row from the 'mt_packing_slip' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MasterPackingSlip extends BaseMasterPackingSlip
{

}
