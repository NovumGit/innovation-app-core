<?php

namespace Model\User;

use Model\User\Base\UserRegistry as BaseUserRegistry;

/**
 * Skeleton subclass for representing a row from the 'user_registry' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UserRegistry extends BaseUserRegistry
{

}
