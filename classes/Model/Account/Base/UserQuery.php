<?php

namespace Model\Account\Base;

use \Exception;
use \PDO;
use Model\Product;
use Model\Account\User as ChildUser;
use Model\Account\UserQuery as ChildUserQuery;
use Model\Account\Map\UserTableMap;
use Model\Cms\SiteBlog;
use Model\Crm\CustomerNote;
use Model\Crm\UserLinkedIn;
use Model\Sale\SaleOrder;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\Role;
use Model\Supplier\Supplier;
use Model\User\UserActivity;
use Model\User\UserRegistry;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'user' table.
 *
 *
 *
 * @method     ChildUserQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUserQuery orderByItemDeleted($order = Criteria::ASC) Order by the is_deleted column
 * @method     ChildUserQuery orderByCanChangeRoles($order = Criteria::ASC) Order by the can_change_roles column
 * @method     ChildUserQuery orderByRoleId($order = Criteria::ASC) Order by the role_id column
 * @method     ChildUserQuery orderByPreferedLanguageId($order = Criteria::ASC) Order by the prefered_language_id column
 * @method     ChildUserQuery orderByFirstName($order = Criteria::ASC) Order by the first_name column
 * @method     ChildUserQuery orderByLastName($order = Criteria::ASC) Order by the last_name column
 * @method     ChildUserQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildUserQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildUserQuery orderByPhoneMobile($order = Criteria::ASC) Order by the phone_mobile column
 * @method     ChildUserQuery orderByWebsite($order = Criteria::ASC) Order by the website column
 * @method     ChildUserQuery orderByTwitter($order = Criteria::ASC) Order by the twitter column
 * @method     ChildUserQuery orderByAboutMe($order = Criteria::ASC) Order by the about_me column
 * @method     ChildUserQuery orderByPrivateEmail($order = Criteria::ASC) Order by the private_email column
 * @method     ChildUserQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildUserQuery orderBySalt($order = Criteria::ASC) Order by the salt column
 * @method     ChildUserQuery orderBySourceNetwork($order = Criteria::ASC) Order by the source_network column
 * @method     ChildUserQuery orderByCreated($order = Criteria::ASC) Order by the created column
 * @method     ChildUserQuery orderByModified($order = Criteria::ASC) Order by the modified column
 * @method     ChildUserQuery orderByMustChangePassword($order = Criteria::ASC) Order by the must_change_password column
 * @method     ChildUserQuery orderByHasProfilePic($order = Criteria::ASC) Order by the has_profile_pic column
 * @method     ChildUserQuery orderByPicExt($order = Criteria::ASC) Order by the pic_ext column
 *
 * @method     ChildUserQuery groupById() Group by the id column
 * @method     ChildUserQuery groupByItemDeleted() Group by the is_deleted column
 * @method     ChildUserQuery groupByCanChangeRoles() Group by the can_change_roles column
 * @method     ChildUserQuery groupByRoleId() Group by the role_id column
 * @method     ChildUserQuery groupByPreferedLanguageId() Group by the prefered_language_id column
 * @method     ChildUserQuery groupByFirstName() Group by the first_name column
 * @method     ChildUserQuery groupByLastName() Group by the last_name column
 * @method     ChildUserQuery groupByEmail() Group by the email column
 * @method     ChildUserQuery groupByPhone() Group by the phone column
 * @method     ChildUserQuery groupByPhoneMobile() Group by the phone_mobile column
 * @method     ChildUserQuery groupByWebsite() Group by the website column
 * @method     ChildUserQuery groupByTwitter() Group by the twitter column
 * @method     ChildUserQuery groupByAboutMe() Group by the about_me column
 * @method     ChildUserQuery groupByPrivateEmail() Group by the private_email column
 * @method     ChildUserQuery groupByPassword() Group by the password column
 * @method     ChildUserQuery groupBySalt() Group by the salt column
 * @method     ChildUserQuery groupBySourceNetwork() Group by the source_network column
 * @method     ChildUserQuery groupByCreated() Group by the created column
 * @method     ChildUserQuery groupByModified() Group by the modified column
 * @method     ChildUserQuery groupByMustChangePassword() Group by the must_change_password column
 * @method     ChildUserQuery groupByHasProfilePic() Group by the has_profile_pic column
 * @method     ChildUserQuery groupByPicExt() Group by the pic_ext column
 *
 * @method     ChildUserQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUserQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUserQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUserQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUserQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUserQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUserQuery leftJoinPreferredLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the PreferredLanguage relation
 * @method     ChildUserQuery rightJoinPreferredLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PreferredLanguage relation
 * @method     ChildUserQuery innerJoinPreferredLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the PreferredLanguage relation
 *
 * @method     ChildUserQuery joinWithPreferredLanguage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PreferredLanguage relation
 *
 * @method     ChildUserQuery leftJoinWithPreferredLanguage() Adds a LEFT JOIN clause and with to the query using the PreferredLanguage relation
 * @method     ChildUserQuery rightJoinWithPreferredLanguage() Adds a RIGHT JOIN clause and with to the query using the PreferredLanguage relation
 * @method     ChildUserQuery innerJoinWithPreferredLanguage() Adds a INNER JOIN clause and with to the query using the PreferredLanguage relation
 *
 * @method     ChildUserQuery leftJoinRole($relationAlias = null) Adds a LEFT JOIN clause to the query using the Role relation
 * @method     ChildUserQuery rightJoinRole($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Role relation
 * @method     ChildUserQuery innerJoinRole($relationAlias = null) Adds a INNER JOIN clause to the query using the Role relation
 *
 * @method     ChildUserQuery joinWithRole($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Role relation
 *
 * @method     ChildUserQuery leftJoinWithRole() Adds a LEFT JOIN clause and with to the query using the Role relation
 * @method     ChildUserQuery rightJoinWithRole() Adds a RIGHT JOIN clause and with to the query using the Role relation
 * @method     ChildUserQuery innerJoinWithRole() Adds a INNER JOIN clause and with to the query using the Role relation
 *
 * @method     ChildUserQuery leftJoinUserObj($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserObj relation
 * @method     ChildUserQuery rightJoinUserObj($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserObj relation
 * @method     ChildUserQuery innerJoinUserObj($relationAlias = null) Adds a INNER JOIN clause to the query using the UserObj relation
 *
 * @method     ChildUserQuery joinWithUserObj($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserObj relation
 *
 * @method     ChildUserQuery leftJoinWithUserObj() Adds a LEFT JOIN clause and with to the query using the UserObj relation
 * @method     ChildUserQuery rightJoinWithUserObj() Adds a RIGHT JOIN clause and with to the query using the UserObj relation
 * @method     ChildUserQuery innerJoinWithUserObj() Adds a INNER JOIN clause and with to the query using the UserObj relation
 *
 * @method     ChildUserQuery leftJoinUserRegistry($relationAlias = null) Adds a LEFT JOIN clause to the query using the UserRegistry relation
 * @method     ChildUserQuery rightJoinUserRegistry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UserRegistry relation
 * @method     ChildUserQuery innerJoinUserRegistry($relationAlias = null) Adds a INNER JOIN clause to the query using the UserRegistry relation
 *
 * @method     ChildUserQuery joinWithUserRegistry($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UserRegistry relation
 *
 * @method     ChildUserQuery leftJoinWithUserRegistry() Adds a LEFT JOIN clause and with to the query using the UserRegistry relation
 * @method     ChildUserQuery rightJoinWithUserRegistry() Adds a RIGHT JOIN clause and with to the query using the UserRegistry relation
 * @method     ChildUserQuery innerJoinWithUserRegistry() Adds a INNER JOIN clause and with to the query using the UserRegistry relation
 *
 * @method     ChildUserQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildUserQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildUserQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildUserQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildUserQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildUserQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildUserQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     ChildUserQuery leftJoinFacebookUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the FacebookUser relation
 * @method     ChildUserQuery rightJoinFacebookUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FacebookUser relation
 * @method     ChildUserQuery innerJoinFacebookUser($relationAlias = null) Adds a INNER JOIN clause to the query using the FacebookUser relation
 *
 * @method     ChildUserQuery joinWithFacebookUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FacebookUser relation
 *
 * @method     ChildUserQuery leftJoinWithFacebookUser() Adds a LEFT JOIN clause and with to the query using the FacebookUser relation
 * @method     ChildUserQuery rightJoinWithFacebookUser() Adds a RIGHT JOIN clause and with to the query using the FacebookUser relation
 * @method     ChildUserQuery innerJoinWithFacebookUser() Adds a INNER JOIN clause and with to the query using the FacebookUser relation
 *
 * @method     ChildUserQuery leftJoinSupplier($relationAlias = null) Adds a LEFT JOIN clause to the query using the Supplier relation
 * @method     ChildUserQuery rightJoinSupplier($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Supplier relation
 * @method     ChildUserQuery innerJoinSupplier($relationAlias = null) Adds a INNER JOIN clause to the query using the Supplier relation
 *
 * @method     ChildUserQuery joinWithSupplier($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Supplier relation
 *
 * @method     ChildUserQuery leftJoinWithSupplier() Adds a LEFT JOIN clause and with to the query using the Supplier relation
 * @method     ChildUserQuery rightJoinWithSupplier() Adds a RIGHT JOIN clause and with to the query using the Supplier relation
 * @method     ChildUserQuery innerJoinWithSupplier() Adds a INNER JOIN clause and with to the query using the Supplier relation
 *
 * @method     ChildUserQuery leftJoinProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the Product relation
 * @method     ChildUserQuery rightJoinProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Product relation
 * @method     ChildUserQuery innerJoinProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the Product relation
 *
 * @method     ChildUserQuery joinWithProduct($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Product relation
 *
 * @method     ChildUserQuery leftJoinWithProduct() Adds a LEFT JOIN clause and with to the query using the Product relation
 * @method     ChildUserQuery rightJoinWithProduct() Adds a RIGHT JOIN clause and with to the query using the Product relation
 * @method     ChildUserQuery innerJoinWithProduct() Adds a INNER JOIN clause and with to the query using the Product relation
 *
 * @method     ChildUserQuery leftJoinSaleOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrder relation
 * @method     ChildUserQuery rightJoinSaleOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrder relation
 * @method     ChildUserQuery innerJoinSaleOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrder relation
 *
 * @method     ChildUserQuery joinWithSaleOrder($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrder relation
 *
 * @method     ChildUserQuery leftJoinWithSaleOrder() Adds a LEFT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildUserQuery rightJoinWithSaleOrder() Adds a RIGHT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildUserQuery innerJoinWithSaleOrder() Adds a INNER JOIN clause and with to the query using the SaleOrder relation
 *
 * @method     ChildUserQuery leftJoinCustomerNote($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomerNote relation
 * @method     ChildUserQuery rightJoinCustomerNote($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomerNote relation
 * @method     ChildUserQuery innerJoinCustomerNote($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomerNote relation
 *
 * @method     ChildUserQuery joinWithCustomerNote($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomerNote relation
 *
 * @method     ChildUserQuery leftJoinWithCustomerNote() Adds a LEFT JOIN clause and with to the query using the CustomerNote relation
 * @method     ChildUserQuery rightJoinWithCustomerNote() Adds a RIGHT JOIN clause and with to the query using the CustomerNote relation
 * @method     ChildUserQuery innerJoinWithCustomerNote() Adds a INNER JOIN clause and with to the query using the CustomerNote relation
 *
 * @method     ChildUserQuery leftJoinSiteBlog($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteBlog relation
 * @method     ChildUserQuery rightJoinSiteBlog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteBlog relation
 * @method     ChildUserQuery innerJoinSiteBlog($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteBlog relation
 *
 * @method     ChildUserQuery joinWithSiteBlog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteBlog relation
 *
 * @method     ChildUserQuery leftJoinWithSiteBlog() Adds a LEFT JOIN clause and with to the query using the SiteBlog relation
 * @method     ChildUserQuery rightJoinWithSiteBlog() Adds a RIGHT JOIN clause and with to the query using the SiteBlog relation
 * @method     ChildUserQuery innerJoinWithSiteBlog() Adds a INNER JOIN clause and with to the query using the SiteBlog relation
 *
 * @method     \Model\Setting\MasterTable\LanguageQuery|\Model\Setting\MasterTable\RoleQuery|\Model\User\UserActivityQuery|\Model\User\UserRegistryQuery|\Model\Crm\UserLinkedInQuery|\Model\Account\FacebookUserQuery|\Model\Supplier\SupplierQuery|\Model\ProductQuery|\Model\Sale\SaleOrderQuery|\Model\Crm\CustomerNoteQuery|\Model\Cms\SiteBlogQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUser findOne(ConnectionInterface $con = null) Return the first ChildUser matching the query
 * @method     ChildUser findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUser matching the query, or a new ChildUser object populated from the query conditions when no match is found
 *
 * @method     ChildUser findOneById(int $id) Return the first ChildUser filtered by the id column
 * @method     ChildUser findOneByItemDeleted(boolean $is_deleted) Return the first ChildUser filtered by the is_deleted column
 * @method     ChildUser findOneByCanChangeRoles(boolean $can_change_roles) Return the first ChildUser filtered by the can_change_roles column
 * @method     ChildUser findOneByRoleId(int $role_id) Return the first ChildUser filtered by the role_id column
 * @method     ChildUser findOneByPreferedLanguageId(int $prefered_language_id) Return the first ChildUser filtered by the prefered_language_id column
 * @method     ChildUser findOneByFirstName(string $first_name) Return the first ChildUser filtered by the first_name column
 * @method     ChildUser findOneByLastName(string $last_name) Return the first ChildUser filtered by the last_name column
 * @method     ChildUser findOneByEmail(string $email) Return the first ChildUser filtered by the email column
 * @method     ChildUser findOneByPhone(string $phone) Return the first ChildUser filtered by the phone column
 * @method     ChildUser findOneByPhoneMobile(string $phone_mobile) Return the first ChildUser filtered by the phone_mobile column
 * @method     ChildUser findOneByWebsite(string $website) Return the first ChildUser filtered by the website column
 * @method     ChildUser findOneByTwitter(string $twitter) Return the first ChildUser filtered by the twitter column
 * @method     ChildUser findOneByAboutMe(string $about_me) Return the first ChildUser filtered by the about_me column
 * @method     ChildUser findOneByPrivateEmail(string $private_email) Return the first ChildUser filtered by the private_email column
 * @method     ChildUser findOneByPassword(string $password) Return the first ChildUser filtered by the password column
 * @method     ChildUser findOneBySalt(string $salt) Return the first ChildUser filtered by the salt column
 * @method     ChildUser findOneBySourceNetwork(string $source_network) Return the first ChildUser filtered by the source_network column
 * @method     ChildUser findOneByCreated(string $created) Return the first ChildUser filtered by the created column
 * @method     ChildUser findOneByModified(string $modified) Return the first ChildUser filtered by the modified column
 * @method     ChildUser findOneByMustChangePassword(boolean $must_change_password) Return the first ChildUser filtered by the must_change_password column
 * @method     ChildUser findOneByHasProfilePic(boolean $has_profile_pic) Return the first ChildUser filtered by the has_profile_pic column
 * @method     ChildUser findOneByPicExt(string $pic_ext) Return the first ChildUser filtered by the pic_ext column *

 * @method     ChildUser requirePk($key, ConnectionInterface $con = null) Return the ChildUser by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOne(ConnectionInterface $con = null) Return the first ChildUser matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUser requireOneById(int $id) Return the first ChildUser filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByItemDeleted(boolean $is_deleted) Return the first ChildUser filtered by the is_deleted column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByCanChangeRoles(boolean $can_change_roles) Return the first ChildUser filtered by the can_change_roles column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByRoleId(int $role_id) Return the first ChildUser filtered by the role_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByPreferedLanguageId(int $prefered_language_id) Return the first ChildUser filtered by the prefered_language_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByFirstName(string $first_name) Return the first ChildUser filtered by the first_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByLastName(string $last_name) Return the first ChildUser filtered by the last_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByEmail(string $email) Return the first ChildUser filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByPhone(string $phone) Return the first ChildUser filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByPhoneMobile(string $phone_mobile) Return the first ChildUser filtered by the phone_mobile column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByWebsite(string $website) Return the first ChildUser filtered by the website column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByTwitter(string $twitter) Return the first ChildUser filtered by the twitter column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByAboutMe(string $about_me) Return the first ChildUser filtered by the about_me column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByPrivateEmail(string $private_email) Return the first ChildUser filtered by the private_email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByPassword(string $password) Return the first ChildUser filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneBySalt(string $salt) Return the first ChildUser filtered by the salt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneBySourceNetwork(string $source_network) Return the first ChildUser filtered by the source_network column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByCreated(string $created) Return the first ChildUser filtered by the created column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByModified(string $modified) Return the first ChildUser filtered by the modified column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByMustChangePassword(boolean $must_change_password) Return the first ChildUser filtered by the must_change_password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByHasProfilePic(boolean $has_profile_pic) Return the first ChildUser filtered by the has_profile_pic column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUser requireOneByPicExt(string $pic_ext) Return the first ChildUser filtered by the pic_ext column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUser[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUser objects based on current ModelCriteria
 * @method     ChildUser[]|ObjectCollection findById(int $id) Return ChildUser objects filtered by the id column
 * @method     ChildUser[]|ObjectCollection findByItemDeleted(boolean $is_deleted) Return ChildUser objects filtered by the is_deleted column
 * @method     ChildUser[]|ObjectCollection findByCanChangeRoles(boolean $can_change_roles) Return ChildUser objects filtered by the can_change_roles column
 * @method     ChildUser[]|ObjectCollection findByRoleId(int $role_id) Return ChildUser objects filtered by the role_id column
 * @method     ChildUser[]|ObjectCollection findByPreferedLanguageId(int $prefered_language_id) Return ChildUser objects filtered by the prefered_language_id column
 * @method     ChildUser[]|ObjectCollection findByFirstName(string $first_name) Return ChildUser objects filtered by the first_name column
 * @method     ChildUser[]|ObjectCollection findByLastName(string $last_name) Return ChildUser objects filtered by the last_name column
 * @method     ChildUser[]|ObjectCollection findByEmail(string $email) Return ChildUser objects filtered by the email column
 * @method     ChildUser[]|ObjectCollection findByPhone(string $phone) Return ChildUser objects filtered by the phone column
 * @method     ChildUser[]|ObjectCollection findByPhoneMobile(string $phone_mobile) Return ChildUser objects filtered by the phone_mobile column
 * @method     ChildUser[]|ObjectCollection findByWebsite(string $website) Return ChildUser objects filtered by the website column
 * @method     ChildUser[]|ObjectCollection findByTwitter(string $twitter) Return ChildUser objects filtered by the twitter column
 * @method     ChildUser[]|ObjectCollection findByAboutMe(string $about_me) Return ChildUser objects filtered by the about_me column
 * @method     ChildUser[]|ObjectCollection findByPrivateEmail(string $private_email) Return ChildUser objects filtered by the private_email column
 * @method     ChildUser[]|ObjectCollection findByPassword(string $password) Return ChildUser objects filtered by the password column
 * @method     ChildUser[]|ObjectCollection findBySalt(string $salt) Return ChildUser objects filtered by the salt column
 * @method     ChildUser[]|ObjectCollection findBySourceNetwork(string $source_network) Return ChildUser objects filtered by the source_network column
 * @method     ChildUser[]|ObjectCollection findByCreated(string $created) Return ChildUser objects filtered by the created column
 * @method     ChildUser[]|ObjectCollection findByModified(string $modified) Return ChildUser objects filtered by the modified column
 * @method     ChildUser[]|ObjectCollection findByMustChangePassword(boolean $must_change_password) Return ChildUser objects filtered by the must_change_password column
 * @method     ChildUser[]|ObjectCollection findByHasProfilePic(boolean $has_profile_pic) Return ChildUser objects filtered by the has_profile_pic column
 * @method     ChildUser[]|ObjectCollection findByPicExt(string $pic_ext) Return ChildUser objects filtered by the pic_ext column
 * @method     ChildUser[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UserQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Account\Base\UserQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Account\\User', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUserQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUserQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUserQuery) {
            return $criteria;
        }
        $query = new ChildUserQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUser|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UserTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUser A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, is_deleted, can_change_roles, role_id, prefered_language_id, first_name, last_name, email, phone, phone_mobile, website, twitter, about_me, private_email, password, salt, source_network, created, modified, must_change_password, has_profile_pic, pic_ext FROM user WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUser $obj */
            $obj = new ChildUser();
            $obj->hydrate($row);
            UserTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUser|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UserTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UserTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UserTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UserTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the is_deleted column
     *
     * Example usage:
     * <code>
     * $query->filterByItemDeleted(true); // WHERE is_deleted = true
     * $query->filterByItemDeleted('yes'); // WHERE is_deleted = true
     * </code>
     *
     * @param     boolean|string $itemDeleted The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByItemDeleted($itemDeleted = null, $comparison = null)
    {
        if (is_string($itemDeleted)) {
            $itemDeleted = in_array(strtolower($itemDeleted), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserTableMap::COL_IS_DELETED, $itemDeleted, $comparison);
    }

    /**
     * Filter the query on the can_change_roles column
     *
     * Example usage:
     * <code>
     * $query->filterByCanChangeRoles(true); // WHERE can_change_roles = true
     * $query->filterByCanChangeRoles('yes'); // WHERE can_change_roles = true
     * </code>
     *
     * @param     boolean|string $canChangeRoles The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByCanChangeRoles($canChangeRoles = null, $comparison = null)
    {
        if (is_string($canChangeRoles)) {
            $canChangeRoles = in_array(strtolower($canChangeRoles), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserTableMap::COL_CAN_CHANGE_ROLES, $canChangeRoles, $comparison);
    }

    /**
     * Filter the query on the role_id column
     *
     * Example usage:
     * <code>
     * $query->filterByRoleId(1234); // WHERE role_id = 1234
     * $query->filterByRoleId(array(12, 34)); // WHERE role_id IN (12, 34)
     * $query->filterByRoleId(array('min' => 12)); // WHERE role_id > 12
     * </code>
     *
     * @see       filterByRole()
     *
     * @param     mixed $roleId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByRoleId($roleId = null, $comparison = null)
    {
        if (is_array($roleId)) {
            $useMinMax = false;
            if (isset($roleId['min'])) {
                $this->addUsingAlias(UserTableMap::COL_ROLE_ID, $roleId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($roleId['max'])) {
                $this->addUsingAlias(UserTableMap::COL_ROLE_ID, $roleId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_ROLE_ID, $roleId, $comparison);
    }

    /**
     * Filter the query on the prefered_language_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPreferedLanguageId(1234); // WHERE prefered_language_id = 1234
     * $query->filterByPreferedLanguageId(array(12, 34)); // WHERE prefered_language_id IN (12, 34)
     * $query->filterByPreferedLanguageId(array('min' => 12)); // WHERE prefered_language_id > 12
     * </code>
     *
     * @see       filterByPreferredLanguage()
     *
     * @param     mixed $preferedLanguageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByPreferedLanguageId($preferedLanguageId = null, $comparison = null)
    {
        if (is_array($preferedLanguageId)) {
            $useMinMax = false;
            if (isset($preferedLanguageId['min'])) {
                $this->addUsingAlias(UserTableMap::COL_PREFERED_LANGUAGE_ID, $preferedLanguageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($preferedLanguageId['max'])) {
                $this->addUsingAlias(UserTableMap::COL_PREFERED_LANGUAGE_ID, $preferedLanguageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_PREFERED_LANGUAGE_ID, $preferedLanguageId, $comparison);
    }

    /**
     * Filter the query on the first_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstName('fooValue');   // WHERE first_name = 'fooValue'
     * $query->filterByFirstName('%fooValue%', Criteria::LIKE); // WHERE first_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByFirstName($firstName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_FIRST_NAME, $firstName, $comparison);
    }

    /**
     * Filter the query on the last_name column
     *
     * Example usage:
     * <code>
     * $query->filterByLastName('fooValue');   // WHERE last_name = 'fooValue'
     * $query->filterByLastName('%fooValue%', Criteria::LIKE); // WHERE last_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByLastName($lastName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_LAST_NAME, $lastName, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%', Criteria::LIKE); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the phone_mobile column
     *
     * Example usage:
     * <code>
     * $query->filterByPhoneMobile('fooValue');   // WHERE phone_mobile = 'fooValue'
     * $query->filterByPhoneMobile('%fooValue%', Criteria::LIKE); // WHERE phone_mobile LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phoneMobile The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByPhoneMobile($phoneMobile = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phoneMobile)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_PHONE_MOBILE, $phoneMobile, $comparison);
    }

    /**
     * Filter the query on the website column
     *
     * Example usage:
     * <code>
     * $query->filterByWebsite('fooValue');   // WHERE website = 'fooValue'
     * $query->filterByWebsite('%fooValue%', Criteria::LIKE); // WHERE website LIKE '%fooValue%'
     * </code>
     *
     * @param     string $website The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByWebsite($website = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($website)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_WEBSITE, $website, $comparison);
    }

    /**
     * Filter the query on the twitter column
     *
     * Example usage:
     * <code>
     * $query->filterByTwitter('fooValue');   // WHERE twitter = 'fooValue'
     * $query->filterByTwitter('%fooValue%', Criteria::LIKE); // WHERE twitter LIKE '%fooValue%'
     * </code>
     *
     * @param     string $twitter The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByTwitter($twitter = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($twitter)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_TWITTER, $twitter, $comparison);
    }

    /**
     * Filter the query on the about_me column
     *
     * Example usage:
     * <code>
     * $query->filterByAboutMe('fooValue');   // WHERE about_me = 'fooValue'
     * $query->filterByAboutMe('%fooValue%', Criteria::LIKE); // WHERE about_me LIKE '%fooValue%'
     * </code>
     *
     * @param     string $aboutMe The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByAboutMe($aboutMe = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($aboutMe)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_ABOUT_ME, $aboutMe, $comparison);
    }

    /**
     * Filter the query on the private_email column
     *
     * Example usage:
     * <code>
     * $query->filterByPrivateEmail('fooValue');   // WHERE private_email = 'fooValue'
     * $query->filterByPrivateEmail('%fooValue%', Criteria::LIKE); // WHERE private_email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $privateEmail The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByPrivateEmail($privateEmail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($privateEmail)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_PRIVATE_EMAIL, $privateEmail, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the salt column
     *
     * Example usage:
     * <code>
     * $query->filterBySalt('fooValue');   // WHERE salt = 'fooValue'
     * $query->filterBySalt('%fooValue%', Criteria::LIKE); // WHERE salt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $salt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterBySalt($salt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($salt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_SALT, $salt, $comparison);
    }

    /**
     * Filter the query on the source_network column
     *
     * Example usage:
     * <code>
     * $query->filterBySourceNetwork('fooValue');   // WHERE source_network = 'fooValue'
     * $query->filterBySourceNetwork('%fooValue%', Criteria::LIKE); // WHERE source_network LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sourceNetwork The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterBySourceNetwork($sourceNetwork = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sourceNetwork)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_SOURCE_NETWORK, $sourceNetwork, $comparison);
    }

    /**
     * Filter the query on the created column
     *
     * Example usage:
     * <code>
     * $query->filterByCreated('2011-03-14'); // WHERE created = '2011-03-14'
     * $query->filterByCreated('now'); // WHERE created = '2011-03-14'
     * $query->filterByCreated(array('max' => 'yesterday')); // WHERE created > '2011-03-13'
     * </code>
     *
     * @param     mixed $created The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByCreated($created = null, $comparison = null)
    {
        if (is_array($created)) {
            $useMinMax = false;
            if (isset($created['min'])) {
                $this->addUsingAlias(UserTableMap::COL_CREATED, $created['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($created['max'])) {
                $this->addUsingAlias(UserTableMap::COL_CREATED, $created['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_CREATED, $created, $comparison);
    }

    /**
     * Filter the query on the modified column
     *
     * Example usage:
     * <code>
     * $query->filterByModified('2011-03-14'); // WHERE modified = '2011-03-14'
     * $query->filterByModified('now'); // WHERE modified = '2011-03-14'
     * $query->filterByModified(array('max' => 'yesterday')); // WHERE modified > '2011-03-13'
     * </code>
     *
     * @param     mixed $modified The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByModified($modified = null, $comparison = null)
    {
        if (is_array($modified)) {
            $useMinMax = false;
            if (isset($modified['min'])) {
                $this->addUsingAlias(UserTableMap::COL_MODIFIED, $modified['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($modified['max'])) {
                $this->addUsingAlias(UserTableMap::COL_MODIFIED, $modified['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_MODIFIED, $modified, $comparison);
    }

    /**
     * Filter the query on the must_change_password column
     *
     * Example usage:
     * <code>
     * $query->filterByMustChangePassword(true); // WHERE must_change_password = true
     * $query->filterByMustChangePassword('yes'); // WHERE must_change_password = true
     * </code>
     *
     * @param     boolean|string $mustChangePassword The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByMustChangePassword($mustChangePassword = null, $comparison = null)
    {
        if (is_string($mustChangePassword)) {
            $mustChangePassword = in_array(strtolower($mustChangePassword), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserTableMap::COL_MUST_CHANGE_PASSWORD, $mustChangePassword, $comparison);
    }

    /**
     * Filter the query on the has_profile_pic column
     *
     * Example usage:
     * <code>
     * $query->filterByHasProfilePic(true); // WHERE has_profile_pic = true
     * $query->filterByHasProfilePic('yes'); // WHERE has_profile_pic = true
     * </code>
     *
     * @param     boolean|string $hasProfilePic The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByHasProfilePic($hasProfilePic = null, $comparison = null)
    {
        if (is_string($hasProfilePic)) {
            $hasProfilePic = in_array(strtolower($hasProfilePic), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UserTableMap::COL_HAS_PROFILE_PIC, $hasProfilePic, $comparison);
    }

    /**
     * Filter the query on the pic_ext column
     *
     * Example usage:
     * <code>
     * $query->filterByPicExt('fooValue');   // WHERE pic_ext = 'fooValue'
     * $query->filterByPicExt('%fooValue%', Criteria::LIKE); // WHERE pic_ext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $picExt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function filterByPicExt($picExt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($picExt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UserTableMap::COL_PIC_EXT, $picExt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Language object
     *
     * @param \Model\Setting\MasterTable\Language|ObjectCollection $language The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByPreferredLanguage($language, $comparison = null)
    {
        if ($language instanceof \Model\Setting\MasterTable\Language) {
            return $this
                ->addUsingAlias(UserTableMap::COL_PREFERED_LANGUAGE_ID, $language->getId(), $comparison);
        } elseif ($language instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserTableMap::COL_PREFERED_LANGUAGE_ID, $language->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPreferredLanguage() only accepts arguments of type \Model\Setting\MasterTable\Language or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PreferredLanguage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinPreferredLanguage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PreferredLanguage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PreferredLanguage');
        }

        return $this;
    }

    /**
     * Use the PreferredLanguage relation Language object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\LanguageQuery A secondary query class using the current class as primary query
     */
    public function usePreferredLanguageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPreferredLanguage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PreferredLanguage', '\Model\Setting\MasterTable\LanguageQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Role object
     *
     * @param \Model\Setting\MasterTable\Role|ObjectCollection $role The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByRole($role, $comparison = null)
    {
        if ($role instanceof \Model\Setting\MasterTable\Role) {
            return $this
                ->addUsingAlias(UserTableMap::COL_ROLE_ID, $role->getId(), $comparison);
        } elseif ($role instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UserTableMap::COL_ROLE_ID, $role->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByRole() only accepts arguments of type \Model\Setting\MasterTable\Role or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Role relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinRole($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Role');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Role');
        }

        return $this;
    }

    /**
     * Use the Role relation Role object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\RoleQuery A secondary query class using the current class as primary query
     */
    public function useRoleQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRole($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Role', '\Model\Setting\MasterTable\RoleQuery');
    }

    /**
     * Filter the query by a related \Model\User\UserActivity object
     *
     * @param \Model\User\UserActivity|ObjectCollection $userActivity the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByUserObj($userActivity, $comparison = null)
    {
        if ($userActivity instanceof \Model\User\UserActivity) {
            return $this
                ->addUsingAlias(UserTableMap::COL_ID, $userActivity->getUserId(), $comparison);
        } elseif ($userActivity instanceof ObjectCollection) {
            return $this
                ->useUserObjQuery()
                ->filterByPrimaryKeys($userActivity->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserObj() only accepts arguments of type \Model\User\UserActivity or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserObj relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinUserObj($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserObj');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserObj');
        }

        return $this;
    }

    /**
     * Use the UserObj relation UserActivity object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\User\UserActivityQuery A secondary query class using the current class as primary query
     */
    public function useUserObjQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserObj($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserObj', '\Model\User\UserActivityQuery');
    }

    /**
     * Filter the query by a related \Model\User\UserRegistry object
     *
     * @param \Model\User\UserRegistry|ObjectCollection $userRegistry the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByUserRegistry($userRegistry, $comparison = null)
    {
        if ($userRegistry instanceof \Model\User\UserRegistry) {
            return $this
                ->addUsingAlias(UserTableMap::COL_ID, $userRegistry->getUserId(), $comparison);
        } elseif ($userRegistry instanceof ObjectCollection) {
            return $this
                ->useUserRegistryQuery()
                ->filterByPrimaryKeys($userRegistry->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUserRegistry() only accepts arguments of type \Model\User\UserRegistry or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UserRegistry relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinUserRegistry($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UserRegistry');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UserRegistry');
        }

        return $this;
    }

    /**
     * Use the UserRegistry relation UserRegistry object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\User\UserRegistryQuery A secondary query class using the current class as primary query
     */
    public function useUserRegistryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUserRegistry($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UserRegistry', '\Model\User\UserRegistryQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\UserLinkedIn object
     *
     * @param \Model\Crm\UserLinkedIn|ObjectCollection $userLinkedIn the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByUser($userLinkedIn, $comparison = null)
    {
        if ($userLinkedIn instanceof \Model\Crm\UserLinkedIn) {
            return $this
                ->addUsingAlias(UserTableMap::COL_ID, $userLinkedIn->getUserId(), $comparison);
        } elseif ($userLinkedIn instanceof ObjectCollection) {
            return $this
                ->useUserQuery()
                ->filterByPrimaryKeys($userLinkedIn->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \Model\Crm\UserLinkedIn or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation UserLinkedIn object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\UserLinkedInQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\Model\Crm\UserLinkedInQuery');
    }

    /**
     * Filter the query by a related \Model\Account\FacebookUser object
     *
     * @param \Model\Account\FacebookUser|ObjectCollection $facebookUser the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByFacebookUser($facebookUser, $comparison = null)
    {
        if ($facebookUser instanceof \Model\Account\FacebookUser) {
            return $this
                ->addUsingAlias(UserTableMap::COL_ID, $facebookUser->getUserId(), $comparison);
        } elseif ($facebookUser instanceof ObjectCollection) {
            return $this
                ->useFacebookUserQuery()
                ->filterByPrimaryKeys($facebookUser->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByFacebookUser() only accepts arguments of type \Model\Account\FacebookUser or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FacebookUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinFacebookUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FacebookUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FacebookUser');
        }

        return $this;
    }

    /**
     * Use the FacebookUser relation FacebookUser object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Account\FacebookUserQuery A secondary query class using the current class as primary query
     */
    public function useFacebookUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFacebookUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FacebookUser', '\Model\Account\FacebookUserQuery');
    }

    /**
     * Filter the query by a related \Model\Supplier\Supplier object
     *
     * @param \Model\Supplier\Supplier|ObjectCollection $supplier the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterBySupplier($supplier, $comparison = null)
    {
        if ($supplier instanceof \Model\Supplier\Supplier) {
            return $this
                ->addUsingAlias(UserTableMap::COL_ID, $supplier->getAccountManagerId(), $comparison);
        } elseif ($supplier instanceof ObjectCollection) {
            return $this
                ->useSupplierQuery()
                ->filterByPrimaryKeys($supplier->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySupplier() only accepts arguments of type \Model\Supplier\Supplier or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Supplier relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinSupplier($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Supplier');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Supplier');
        }

        return $this;
    }

    /**
     * Use the Supplier relation Supplier object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Supplier\SupplierQuery A secondary query class using the current class as primary query
     */
    public function useSupplierQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSupplier($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Supplier', '\Model\Supplier\SupplierQuery');
    }

    /**
     * Filter the query by a related \Model\Product object
     *
     * @param \Model\Product|ObjectCollection $product the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByProduct($product, $comparison = null)
    {
        if ($product instanceof \Model\Product) {
            return $this
                ->addUsingAlias(UserTableMap::COL_ID, $product->getAdvertiserUserId(), $comparison);
        } elseif ($product instanceof ObjectCollection) {
            return $this
                ->useProductQuery()
                ->filterByPrimaryKeys($product->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProduct() only accepts arguments of type \Model\Product or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Product relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinProduct($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Product');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Product');
        }

        return $this;
    }

    /**
     * Use the Product relation Product object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ProductQuery A secondary query class using the current class as primary query
     */
    public function useProductQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Product', '\Model\ProductQuery');
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrder object
     *
     * @param \Model\Sale\SaleOrder|ObjectCollection $saleOrder the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterBySaleOrder($saleOrder, $comparison = null)
    {
        if ($saleOrder instanceof \Model\Sale\SaleOrder) {
            return $this
                ->addUsingAlias(UserTableMap::COL_ID, $saleOrder->getCreatedByUserId(), $comparison);
        } elseif ($saleOrder instanceof ObjectCollection) {
            return $this
                ->useSaleOrderQuery()
                ->filterByPrimaryKeys($saleOrder->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleOrder() only accepts arguments of type \Model\Sale\SaleOrder or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinSaleOrder($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrder');
        }

        return $this;
    }

    /**
     * Use the SaleOrder relation SaleOrder object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaleOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrder', '\Model\Sale\SaleOrderQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\CustomerNote object
     *
     * @param \Model\Crm\CustomerNote|ObjectCollection $customerNote the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterByCustomerNote($customerNote, $comparison = null)
    {
        if ($customerNote instanceof \Model\Crm\CustomerNote) {
            return $this
                ->addUsingAlias(UserTableMap::COL_ID, $customerNote->getCreatedByUserId(), $comparison);
        } elseif ($customerNote instanceof ObjectCollection) {
            return $this
                ->useCustomerNoteQuery()
                ->filterByPrimaryKeys($customerNote->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomerNote() only accepts arguments of type \Model\Crm\CustomerNote or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomerNote relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinCustomerNote($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomerNote');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomerNote');
        }

        return $this;
    }

    /**
     * Use the CustomerNote relation CustomerNote object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerNoteQuery A secondary query class using the current class as primary query
     */
    public function useCustomerNoteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCustomerNote($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomerNote', '\Model\Crm\CustomerNoteQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteBlog object
     *
     * @param \Model\Cms\SiteBlog|ObjectCollection $siteBlog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUserQuery The current query, for fluid interface
     */
    public function filterBySiteBlog($siteBlog, $comparison = null)
    {
        if ($siteBlog instanceof \Model\Cms\SiteBlog) {
            return $this
                ->addUsingAlias(UserTableMap::COL_ID, $siteBlog->getCreatedByUserId(), $comparison);
        } elseif ($siteBlog instanceof ObjectCollection) {
            return $this
                ->useSiteBlogQuery()
                ->filterByPrimaryKeys($siteBlog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteBlog() only accepts arguments of type \Model\Cms\SiteBlog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteBlog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function joinSiteBlog($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteBlog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteBlog');
        }

        return $this;
    }

    /**
     * Use the SiteBlog relation SiteBlog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteBlogQuery A secondary query class using the current class as primary query
     */
    public function useSiteBlogQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteBlog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteBlog', '\Model\Cms\SiteBlogQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUser $user Object to remove from the list of results
     *
     * @return $this|ChildUserQuery The current query, for fluid interface
     */
    public function prune($user = null)
    {
        if ($user) {
            $this->addUsingAlias(UserTableMap::COL_ID, $user->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the user table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UserTableMap::clearInstancePool();
            UserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UserTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UserTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UserTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UserQuery
