<?php

namespace Model\Account\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Product;
use Model\ProductQuery;
use Model\Account\FacebookUser as ChildFacebookUser;
use Model\Account\FacebookUserQuery as ChildFacebookUserQuery;
use Model\Account\User as ChildUser;
use Model\Account\UserQuery as ChildUserQuery;
use Model\Account\Map\FacebookUserTableMap;
use Model\Account\Map\UserTableMap;
use Model\Base\Product as BaseProduct;
use Model\Cms\SiteBlog;
use Model\Cms\SiteBlogQuery;
use Model\Cms\Base\SiteBlog as BaseSiteBlog;
use Model\Cms\Map\SiteBlogTableMap;
use Model\Crm\CustomerNote;
use Model\Crm\CustomerNoteQuery;
use Model\Crm\UserLinkedIn;
use Model\Crm\UserLinkedInQuery;
use Model\Crm\Base\CustomerNote as BaseCustomerNote;
use Model\Crm\Base\UserLinkedIn as BaseUserLinkedIn;
use Model\Crm\Map\CustomerNoteTableMap;
use Model\Crm\Map\UserLinkedInTableMap;
use Model\Map\ProductTableMap;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderQuery;
use Model\Sale\Base\SaleOrder as BaseSaleOrder;
use Model\Sale\Map\SaleOrderTableMap;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;
use Model\Setting\MasterTable\Role;
use Model\Setting\MasterTable\RoleQuery;
use Model\Supplier\Supplier;
use Model\Supplier\SupplierQuery;
use Model\Supplier\Base\Supplier as BaseSupplier;
use Model\Supplier\Map\SupplierTableMap;
use Model\User\UserActivity;
use Model\User\UserActivityQuery;
use Model\User\UserRegistry;
use Model\User\UserRegistryQuery;
use Model\User\Base\UserActivity as BaseUserActivity;
use Model\User\Base\UserRegistry as BaseUserRegistry;
use Model\User\Map\UserActivityTableMap;
use Model\User\Map\UserRegistryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'user' table.
 *
 *
 *
 * @package    propel.generator.Model.Account.Base
 */
abstract class User implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Account\\Map\\UserTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the is_deleted field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_deleted;

    /**
     * The value for the can_change_roles field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $can_change_roles;

    /**
     * The value for the role_id field.
     *
     * @var        int
     */
    protected $role_id;

    /**
     * The value for the prefered_language_id field.
     *
     * @var        int|null
     */
    protected $prefered_language_id;

    /**
     * The value for the first_name field.
     *
     * @var        string
     */
    protected $first_name;

    /**
     * The value for the last_name field.
     *
     * @var        string
     */
    protected $last_name;

    /**
     * The value for the email field.
     *
     * @var        string
     */
    protected $email;

    /**
     * The value for the phone field.
     *
     * @var        string|null
     */
    protected $phone;

    /**
     * The value for the phone_mobile field.
     *
     * @var        string|null
     */
    protected $phone_mobile;

    /**
     * The value for the website field.
     *
     * @var        string|null
     */
    protected $website;

    /**
     * The value for the twitter field.
     *
     * @var        string|null
     */
    protected $twitter;

    /**
     * The value for the about_me field.
     *
     * @var        string|null
     */
    protected $about_me;

    /**
     * The value for the private_email field.
     *
     * @var        string|null
     */
    protected $private_email;

    /**
     * The value for the password field.
     *
     * @var        string|null
     */
    protected $password;

    /**
     * The value for the salt field.
     *
     * @var        string|null
     */
    protected $salt;

    /**
     * The value for the source_network field.
     *
     * @var        string|null
     */
    protected $source_network;

    /**
     * The value for the created field.
     *
     * @var        DateTime|null
     */
    protected $created;

    /**
     * The value for the modified field.
     *
     * @var        DateTime|null
     */
    protected $modified;

    /**
     * The value for the must_change_password field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $must_change_password;

    /**
     * The value for the has_profile_pic field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $has_profile_pic;

    /**
     * The value for the pic_ext field.
     *
     * @var        string|null
     */
    protected $pic_ext;

    /**
     * @var        Language
     */
    protected $aPreferredLanguage;

    /**
     * @var        Role
     */
    protected $aRole;

    /**
     * @var        ObjectCollection|UserActivity[] Collection to store aggregation of UserActivity objects.
     */
    protected $collUserObjs;
    protected $collUserObjsPartial;

    /**
     * @var        ObjectCollection|UserRegistry[] Collection to store aggregation of UserRegistry objects.
     */
    protected $collUserRegistries;
    protected $collUserRegistriesPartial;

    /**
     * @var        ObjectCollection|UserLinkedIn[] Collection to store aggregation of UserLinkedIn objects.
     */
    protected $collUsers;
    protected $collUsersPartial;

    /**
     * @var        ObjectCollection|ChildFacebookUser[] Collection to store aggregation of ChildFacebookUser objects.
     */
    protected $collFacebookUsers;
    protected $collFacebookUsersPartial;

    /**
     * @var        ObjectCollection|Supplier[] Collection to store aggregation of Supplier objects.
     */
    protected $collSuppliers;
    protected $collSuppliersPartial;

    /**
     * @var        ObjectCollection|Product[] Collection to store aggregation of Product objects.
     */
    protected $collProducts;
    protected $collProductsPartial;

    /**
     * @var        ObjectCollection|SaleOrder[] Collection to store aggregation of SaleOrder objects.
     */
    protected $collSaleOrders;
    protected $collSaleOrdersPartial;

    /**
     * @var        ObjectCollection|CustomerNote[] Collection to store aggregation of CustomerNote objects.
     */
    protected $collCustomerNotes;
    protected $collCustomerNotesPartial;

    /**
     * @var        ObjectCollection|SiteBlog[] Collection to store aggregation of SiteBlog objects.
     */
    protected $collSiteBlogs;
    protected $collSiteBlogsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|UserActivity[]
     */
    protected $userObjsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|UserRegistry[]
     */
    protected $userRegistriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|UserLinkedIn[]
     */
    protected $usersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildFacebookUser[]
     */
    protected $facebookUsersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Supplier[]
     */
    protected $suppliersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Product[]
     */
    protected $productsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|SaleOrder[]
     */
    protected $saleOrdersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|CustomerNote[]
     */
    protected $customerNotesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|SiteBlog[]
     */
    protected $siteBlogsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_deleted = false;
        $this->can_change_roles = false;
        $this->must_change_password = false;
        $this->has_profile_pic = false;
    }

    /**
     * Initializes internal state of Model\Account\Base\User object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>User</code> instance.  If
     * <code>obj</code> is an instance of <code>User</code>, delegates to
     * <code>equals(User)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [is_deleted] column value.
     *
     * @return boolean|null
     */
    public function getItemDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Get the [is_deleted] column value.
     *
     * @return boolean|null
     */
    public function isItemDeleted()
    {
        return $this->getItemDeleted();
    }

    /**
     * Get the [can_change_roles] column value.
     *
     * @return boolean
     */
    public function getCanChangeRoles()
    {
        return $this->can_change_roles;
    }

    /**
     * Get the [can_change_roles] column value.
     *
     * @return boolean
     */
    public function isCanChangeRoles()
    {
        return $this->getCanChangeRoles();
    }

    /**
     * Get the [role_id] column value.
     *
     * @return int
     */
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * Get the [prefered_language_id] column value.
     *
     * @return int|null
     */
    public function getPreferedLanguageId()
    {
        return $this->prefered_language_id;
    }

    /**
     * Get the [first_name] column value.
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Get the [last_name] column value.
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [phone] column value.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get the [phone_mobile] column value.
     *
     * @return string|null
     */
    public function getPhoneMobile()
    {
        return $this->phone_mobile;
    }

    /**
     * Get the [website] column value.
     *
     * @return string|null
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Get the [twitter] column value.
     *
     * @return string|null
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Get the [about_me] column value.
     *
     * @return string|null
     */
    public function getAboutMe()
    {
        return $this->about_me;
    }

    /**
     * Get the [private_email] column value.
     *
     * @return string|null
     */
    public function getPrivateEmail()
    {
        return $this->private_email;
    }

    /**
     * Get the [password] column value.
     *
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the [salt] column value.
     *
     * @return string|null
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Get the [source_network] column value.
     *
     * @return string|null
     */
    public function getSourceNetwork()
    {
        return $this->source_network;
    }

    /**
     * Get the [optionally formatted] temporal [created] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreated($format = null)
    {
        if ($format === null) {
            return $this->created;
        } else {
            return $this->created instanceof \DateTimeInterface ? $this->created->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [modified] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getModified($format = null)
    {
        if ($format === null) {
            return $this->modified;
        } else {
            return $this->modified instanceof \DateTimeInterface ? $this->modified->format($format) : null;
        }
    }

    /**
     * Get the [must_change_password] column value.
     *
     * @return boolean|null
     */
    public function getMustChangePassword()
    {
        return $this->must_change_password;
    }

    /**
     * Get the [must_change_password] column value.
     *
     * @return boolean|null
     */
    public function isMustChangePassword()
    {
        return $this->getMustChangePassword();
    }

    /**
     * Get the [has_profile_pic] column value.
     *
     * @return boolean|null
     */
    public function getHasProfilePic()
    {
        return $this->has_profile_pic;
    }

    /**
     * Get the [has_profile_pic] column value.
     *
     * @return boolean|null
     */
    public function hasProfilePic()
    {
        return $this->getHasProfilePic();
    }

    /**
     * Get the [pic_ext] column value.
     *
     * @return string|null
     */
    public function getPicExt()
    {
        return $this->pic_ext;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[UserTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Sets the value of the [is_deleted] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setItemDeleted($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_deleted !== $v) {
            $this->is_deleted = $v;
            $this->modifiedColumns[UserTableMap::COL_IS_DELETED] = true;
        }

        return $this;
    } // setItemDeleted()

    /**
     * Sets the value of the [can_change_roles] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setCanChangeRoles($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->can_change_roles !== $v) {
            $this->can_change_roles = $v;
            $this->modifiedColumns[UserTableMap::COL_CAN_CHANGE_ROLES] = true;
        }

        return $this;
    } // setCanChangeRoles()

    /**
     * Set the value of [role_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setRoleId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->role_id !== $v) {
            $this->role_id = $v;
            $this->modifiedColumns[UserTableMap::COL_ROLE_ID] = true;
        }

        if ($this->aRole !== null && $this->aRole->getId() !== $v) {
            $this->aRole = null;
        }

        return $this;
    } // setRoleId()

    /**
     * Set the value of [prefered_language_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setPreferedLanguageId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prefered_language_id !== $v) {
            $this->prefered_language_id = $v;
            $this->modifiedColumns[UserTableMap::COL_PREFERED_LANGUAGE_ID] = true;
        }

        if ($this->aPreferredLanguage !== null && $this->aPreferredLanguage->getId() !== $v) {
            $this->aPreferredLanguage = null;
        }

        return $this;
    } // setPreferedLanguageId()

    /**
     * Set the value of [first_name] column.
     *
     * @param string $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setFirstName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->first_name !== $v) {
            $this->first_name = $v;
            $this->modifiedColumns[UserTableMap::COL_FIRST_NAME] = true;
        }

        return $this;
    } // setFirstName()

    /**
     * Set the value of [last_name] column.
     *
     * @param string $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setLastName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->last_name !== $v) {
            $this->last_name = $v;
            $this->modifiedColumns[UserTableMap::COL_LAST_NAME] = true;
        }

        return $this;
    } // setLastName()

    /**
     * Set the value of [email] column.
     *
     * @param string $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[UserTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [phone] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[UserTableMap::COL_PHONE] = true;
        }

        return $this;
    } // setPhone()

    /**
     * Set the value of [phone_mobile] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setPhoneMobile($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone_mobile !== $v) {
            $this->phone_mobile = $v;
            $this->modifiedColumns[UserTableMap::COL_PHONE_MOBILE] = true;
        }

        return $this;
    } // setPhoneMobile()

    /**
     * Set the value of [website] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setWebsite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->website !== $v) {
            $this->website = $v;
            $this->modifiedColumns[UserTableMap::COL_WEBSITE] = true;
        }

        return $this;
    } // setWebsite()

    /**
     * Set the value of [twitter] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setTwitter($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->twitter !== $v) {
            $this->twitter = $v;
            $this->modifiedColumns[UserTableMap::COL_TWITTER] = true;
        }

        return $this;
    } // setTwitter()

    /**
     * Set the value of [about_me] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setAboutMe($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->about_me !== $v) {
            $this->about_me = $v;
            $this->modifiedColumns[UserTableMap::COL_ABOUT_ME] = true;
        }

        return $this;
    } // setAboutMe()

    /**
     * Set the value of [private_email] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setPrivateEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->private_email !== $v) {
            $this->private_email = $v;
            $this->modifiedColumns[UserTableMap::COL_PRIVATE_EMAIL] = true;
        }

        return $this;
    } // setPrivateEmail()

    /**
     * Set the value of [password] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[UserTableMap::COL_PASSWORD] = true;
        }

        return $this;
    } // setPassword()

    /**
     * Set the value of [salt] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setSalt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->salt !== $v) {
            $this->salt = $v;
            $this->modifiedColumns[UserTableMap::COL_SALT] = true;
        }

        return $this;
    } // setSalt()

    /**
     * Set the value of [source_network] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setSourceNetwork($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->source_network !== $v) {
            $this->source_network = $v;
            $this->modifiedColumns[UserTableMap::COL_SOURCE_NETWORK] = true;
        }

        return $this;
    } // setSourceNetwork()

    /**
     * Sets the value of [created] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setCreated($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created !== null || $dt !== null) {
            if ($this->created === null || $dt === null || $dt->format("Y-m-d") !== $this->created->format("Y-m-d")) {
                $this->created = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserTableMap::COL_CREATED] = true;
            }
        } // if either are not null

        return $this;
    } // setCreated()

    /**
     * Sets the value of [modified] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setModified($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->modified !== null || $dt !== null) {
            if ($this->modified === null || $dt === null || $dt->format("Y-m-d") !== $this->modified->format("Y-m-d")) {
                $this->modified = $dt === null ? null : clone $dt;
                $this->modifiedColumns[UserTableMap::COL_MODIFIED] = true;
            }
        } // if either are not null

        return $this;
    } // setModified()

    /**
     * Sets the value of the [must_change_password] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setMustChangePassword($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->must_change_password !== $v) {
            $this->must_change_password = $v;
            $this->modifiedColumns[UserTableMap::COL_MUST_CHANGE_PASSWORD] = true;
        }

        return $this;
    } // setMustChangePassword()

    /**
     * Sets the value of the [has_profile_pic] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setHasProfilePic($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_profile_pic !== $v) {
            $this->has_profile_pic = $v;
            $this->modifiedColumns[UserTableMap::COL_HAS_PROFILE_PIC] = true;
        }

        return $this;
    } // setHasProfilePic()

    /**
     * Set the value of [pic_ext] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function setPicExt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pic_ext !== $v) {
            $this->pic_ext = $v;
            $this->modifiedColumns[UserTableMap::COL_PIC_EXT] = true;
        }

        return $this;
    } // setPicExt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_deleted !== false) {
                return false;
            }

            if ($this->can_change_roles !== false) {
                return false;
            }

            if ($this->must_change_password !== false) {
                return false;
            }

            if ($this->has_profile_pic !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UserTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UserTableMap::translateFieldName('ItemDeleted', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_deleted = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UserTableMap::translateFieldName('CanChangeRoles', TableMap::TYPE_PHPNAME, $indexType)];
            $this->can_change_roles = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UserTableMap::translateFieldName('RoleId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->role_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UserTableMap::translateFieldName('PreferedLanguageId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prefered_language_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UserTableMap::translateFieldName('FirstName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->first_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : UserTableMap::translateFieldName('LastName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->last_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : UserTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : UserTableMap::translateFieldName('Phone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->phone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : UserTableMap::translateFieldName('PhoneMobile', TableMap::TYPE_PHPNAME, $indexType)];
            $this->phone_mobile = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : UserTableMap::translateFieldName('Website', TableMap::TYPE_PHPNAME, $indexType)];
            $this->website = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : UserTableMap::translateFieldName('Twitter', TableMap::TYPE_PHPNAME, $indexType)];
            $this->twitter = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : UserTableMap::translateFieldName('AboutMe', TableMap::TYPE_PHPNAME, $indexType)];
            $this->about_me = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : UserTableMap::translateFieldName('PrivateEmail', TableMap::TYPE_PHPNAME, $indexType)];
            $this->private_email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : UserTableMap::translateFieldName('Password', TableMap::TYPE_PHPNAME, $indexType)];
            $this->password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : UserTableMap::translateFieldName('Salt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->salt = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : UserTableMap::translateFieldName('SourceNetwork', TableMap::TYPE_PHPNAME, $indexType)];
            $this->source_network = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : UserTableMap::translateFieldName('Created', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->created = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : UserTableMap::translateFieldName('Modified', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->modified = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : UserTableMap::translateFieldName('MustChangePassword', TableMap::TYPE_PHPNAME, $indexType)];
            $this->must_change_password = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : UserTableMap::translateFieldName('HasProfilePic', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_profile_pic = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : UserTableMap::translateFieldName('PicExt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pic_ext = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 22; // 22 = UserTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Account\\User'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aRole !== null && $this->role_id !== $this->aRole->getId()) {
            $this->aRole = null;
        }
        if ($this->aPreferredLanguage !== null && $this->prefered_language_id !== $this->aPreferredLanguage->getId()) {
            $this->aPreferredLanguage = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UserTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUserQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aPreferredLanguage = null;
            $this->aRole = null;
            $this->collUserObjs = null;

            $this->collUserRegistries = null;

            $this->collUsers = null;

            $this->collFacebookUsers = null;

            $this->collSuppliers = null;

            $this->collProducts = null;

            $this->collSaleOrders = null;

            $this->collCustomerNotes = null;

            $this->collSiteBlogs = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see User::setDeleted()
     * @see User::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUserQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UserTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aPreferredLanguage !== null) {
                if ($this->aPreferredLanguage->isModified() || $this->aPreferredLanguage->isNew()) {
                    $affectedRows += $this->aPreferredLanguage->save($con);
                }
                $this->setPreferredLanguage($this->aPreferredLanguage);
            }

            if ($this->aRole !== null) {
                if ($this->aRole->isModified() || $this->aRole->isNew()) {
                    $affectedRows += $this->aRole->save($con);
                }
                $this->setRole($this->aRole);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->userObjsScheduledForDeletion !== null) {
                if (!$this->userObjsScheduledForDeletion->isEmpty()) {
                    \Model\User\UserActivityQuery::create()
                        ->filterByPrimaryKeys($this->userObjsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userObjsScheduledForDeletion = null;
                }
            }

            if ($this->collUserObjs !== null) {
                foreach ($this->collUserObjs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->userRegistriesScheduledForDeletion !== null) {
                if (!$this->userRegistriesScheduledForDeletion->isEmpty()) {
                    \Model\User\UserRegistryQuery::create()
                        ->filterByPrimaryKeys($this->userRegistriesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->userRegistriesScheduledForDeletion = null;
                }
            }

            if ($this->collUserRegistries !== null) {
                foreach ($this->collUserRegistries as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->usersScheduledForDeletion !== null) {
                if (!$this->usersScheduledForDeletion->isEmpty()) {
                    \Model\Crm\UserLinkedInQuery::create()
                        ->filterByPrimaryKeys($this->usersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->usersScheduledForDeletion = null;
                }
            }

            if ($this->collUsers !== null) {
                foreach ($this->collUsers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->facebookUsersScheduledForDeletion !== null) {
                if (!$this->facebookUsersScheduledForDeletion->isEmpty()) {
                    foreach ($this->facebookUsersScheduledForDeletion as $facebookUser) {
                        // need to save related object because we set the relation to null
                        $facebookUser->save($con);
                    }
                    $this->facebookUsersScheduledForDeletion = null;
                }
            }

            if ($this->collFacebookUsers !== null) {
                foreach ($this->collFacebookUsers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->suppliersScheduledForDeletion !== null) {
                if (!$this->suppliersScheduledForDeletion->isEmpty()) {
                    foreach ($this->suppliersScheduledForDeletion as $supplier) {
                        // need to save related object because we set the relation to null
                        $supplier->save($con);
                    }
                    $this->suppliersScheduledForDeletion = null;
                }
            }

            if ($this->collSuppliers !== null) {
                foreach ($this->collSuppliers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productsScheduledForDeletion !== null) {
                if (!$this->productsScheduledForDeletion->isEmpty()) {
                    foreach ($this->productsScheduledForDeletion as $product) {
                        // need to save related object because we set the relation to null
                        $product->save($con);
                    }
                    $this->productsScheduledForDeletion = null;
                }
            }

            if ($this->collProducts !== null) {
                foreach ($this->collProducts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saleOrdersScheduledForDeletion !== null) {
                if (!$this->saleOrdersScheduledForDeletion->isEmpty()) {
                    foreach ($this->saleOrdersScheduledForDeletion as $saleOrder) {
                        // need to save related object because we set the relation to null
                        $saleOrder->save($con);
                    }
                    $this->saleOrdersScheduledForDeletion = null;
                }
            }

            if ($this->collSaleOrders !== null) {
                foreach ($this->collSaleOrders as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->customerNotesScheduledForDeletion !== null) {
                if (!$this->customerNotesScheduledForDeletion->isEmpty()) {
                    foreach ($this->customerNotesScheduledForDeletion as $customerNote) {
                        // need to save related object because we set the relation to null
                        $customerNote->save($con);
                    }
                    $this->customerNotesScheduledForDeletion = null;
                }
            }

            if ($this->collCustomerNotes !== null) {
                foreach ($this->collCustomerNotes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteBlogsScheduledForDeletion !== null) {
                if (!$this->siteBlogsScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SiteBlogQuery::create()
                        ->filterByPrimaryKeys($this->siteBlogsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteBlogsScheduledForDeletion = null;
                }
            }

            if ($this->collSiteBlogs !== null) {
                foreach ($this->collSiteBlogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UserTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UserTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UserTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(UserTableMap::COL_IS_DELETED)) {
            $modifiedColumns[':p' . $index++]  = 'is_deleted';
        }
        if ($this->isColumnModified(UserTableMap::COL_CAN_CHANGE_ROLES)) {
            $modifiedColumns[':p' . $index++]  = 'can_change_roles';
        }
        if ($this->isColumnModified(UserTableMap::COL_ROLE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'role_id';
        }
        if ($this->isColumnModified(UserTableMap::COL_PREFERED_LANGUAGE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'prefered_language_id';
        }
        if ($this->isColumnModified(UserTableMap::COL_FIRST_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'first_name';
        }
        if ($this->isColumnModified(UserTableMap::COL_LAST_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'last_name';
        }
        if ($this->isColumnModified(UserTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'email';
        }
        if ($this->isColumnModified(UserTableMap::COL_PHONE)) {
            $modifiedColumns[':p' . $index++]  = 'phone';
        }
        if ($this->isColumnModified(UserTableMap::COL_PHONE_MOBILE)) {
            $modifiedColumns[':p' . $index++]  = 'phone_mobile';
        }
        if ($this->isColumnModified(UserTableMap::COL_WEBSITE)) {
            $modifiedColumns[':p' . $index++]  = 'website';
        }
        if ($this->isColumnModified(UserTableMap::COL_TWITTER)) {
            $modifiedColumns[':p' . $index++]  = 'twitter';
        }
        if ($this->isColumnModified(UserTableMap::COL_ABOUT_ME)) {
            $modifiedColumns[':p' . $index++]  = 'about_me';
        }
        if ($this->isColumnModified(UserTableMap::COL_PRIVATE_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'private_email';
        }
        if ($this->isColumnModified(UserTableMap::COL_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'password';
        }
        if ($this->isColumnModified(UserTableMap::COL_SALT)) {
            $modifiedColumns[':p' . $index++]  = 'salt';
        }
        if ($this->isColumnModified(UserTableMap::COL_SOURCE_NETWORK)) {
            $modifiedColumns[':p' . $index++]  = 'source_network';
        }
        if ($this->isColumnModified(UserTableMap::COL_CREATED)) {
            $modifiedColumns[':p' . $index++]  = 'created';
        }
        if ($this->isColumnModified(UserTableMap::COL_MODIFIED)) {
            $modifiedColumns[':p' . $index++]  = 'modified';
        }
        if ($this->isColumnModified(UserTableMap::COL_MUST_CHANGE_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'must_change_password';
        }
        if ($this->isColumnModified(UserTableMap::COL_HAS_PROFILE_PIC)) {
            $modifiedColumns[':p' . $index++]  = 'has_profile_pic';
        }
        if ($this->isColumnModified(UserTableMap::COL_PIC_EXT)) {
            $modifiedColumns[':p' . $index++]  = 'pic_ext';
        }

        $sql = sprintf(
            'INSERT INTO user (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'is_deleted':
                        $stmt->bindValue($identifier, (int) $this->is_deleted, PDO::PARAM_INT);
                        break;
                    case 'can_change_roles':
                        $stmt->bindValue($identifier, (int) $this->can_change_roles, PDO::PARAM_INT);
                        break;
                    case 'role_id':
                        $stmt->bindValue($identifier, $this->role_id, PDO::PARAM_INT);
                        break;
                    case 'prefered_language_id':
                        $stmt->bindValue($identifier, $this->prefered_language_id, PDO::PARAM_INT);
                        break;
                    case 'first_name':
                        $stmt->bindValue($identifier, $this->first_name, PDO::PARAM_STR);
                        break;
                    case 'last_name':
                        $stmt->bindValue($identifier, $this->last_name, PDO::PARAM_STR);
                        break;
                    case 'email':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case 'phone':
                        $stmt->bindValue($identifier, $this->phone, PDO::PARAM_STR);
                        break;
                    case 'phone_mobile':
                        $stmt->bindValue($identifier, $this->phone_mobile, PDO::PARAM_STR);
                        break;
                    case 'website':
                        $stmt->bindValue($identifier, $this->website, PDO::PARAM_STR);
                        break;
                    case 'twitter':
                        $stmt->bindValue($identifier, $this->twitter, PDO::PARAM_STR);
                        break;
                    case 'about_me':
                        $stmt->bindValue($identifier, $this->about_me, PDO::PARAM_STR);
                        break;
                    case 'private_email':
                        $stmt->bindValue($identifier, $this->private_email, PDO::PARAM_STR);
                        break;
                    case 'password':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case 'salt':
                        $stmt->bindValue($identifier, $this->salt, PDO::PARAM_STR);
                        break;
                    case 'source_network':
                        $stmt->bindValue($identifier, $this->source_network, PDO::PARAM_STR);
                        break;
                    case 'created':
                        $stmt->bindValue($identifier, $this->created ? $this->created->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'modified':
                        $stmt->bindValue($identifier, $this->modified ? $this->modified->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'must_change_password':
                        $stmt->bindValue($identifier, (int) $this->must_change_password, PDO::PARAM_INT);
                        break;
                    case 'has_profile_pic':
                        $stmt->bindValue($identifier, (int) $this->has_profile_pic, PDO::PARAM_INT);
                        break;
                    case 'pic_ext':
                        $stmt->bindValue($identifier, $this->pic_ext, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getItemDeleted();
                break;
            case 2:
                return $this->getCanChangeRoles();
                break;
            case 3:
                return $this->getRoleId();
                break;
            case 4:
                return $this->getPreferedLanguageId();
                break;
            case 5:
                return $this->getFirstName();
                break;
            case 6:
                return $this->getLastName();
                break;
            case 7:
                return $this->getEmail();
                break;
            case 8:
                return $this->getPhone();
                break;
            case 9:
                return $this->getPhoneMobile();
                break;
            case 10:
                return $this->getWebsite();
                break;
            case 11:
                return $this->getTwitter();
                break;
            case 12:
                return $this->getAboutMe();
                break;
            case 13:
                return $this->getPrivateEmail();
                break;
            case 14:
                return $this->getPassword();
                break;
            case 15:
                return $this->getSalt();
                break;
            case 16:
                return $this->getSourceNetwork();
                break;
            case 17:
                return $this->getCreated();
                break;
            case 18:
                return $this->getModified();
                break;
            case 19:
                return $this->getMustChangePassword();
                break;
            case 20:
                return $this->getHasProfilePic();
                break;
            case 21:
                return $this->getPicExt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['User'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['User'][$this->hashCode()] = true;
        $keys = UserTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getItemDeleted(),
            $keys[2] => $this->getCanChangeRoles(),
            $keys[3] => $this->getRoleId(),
            $keys[4] => $this->getPreferedLanguageId(),
            $keys[5] => $this->getFirstName(),
            $keys[6] => $this->getLastName(),
            $keys[7] => $this->getEmail(),
            $keys[8] => $this->getPhone(),
            $keys[9] => $this->getPhoneMobile(),
            $keys[10] => $this->getWebsite(),
            $keys[11] => $this->getTwitter(),
            $keys[12] => $this->getAboutMe(),
            $keys[13] => $this->getPrivateEmail(),
            $keys[14] => $this->getPassword(),
            $keys[15] => $this->getSalt(),
            $keys[16] => $this->getSourceNetwork(),
            $keys[17] => $this->getCreated(),
            $keys[18] => $this->getModified(),
            $keys[19] => $this->getMustChangePassword(),
            $keys[20] => $this->getHasProfilePic(),
            $keys[21] => $this->getPicExt(),
        );
        if ($result[$keys[17]] instanceof \DateTimeInterface) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        if ($result[$keys[18]] instanceof \DateTimeInterface) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aPreferredLanguage) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'language';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_language';
                        break;
                    default:
                        $key = 'PreferredLanguage';
                }

                $result[$key] = $this->aPreferredLanguage->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aRole) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'role';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'role';
                        break;
                    default:
                        $key = 'Role';
                }

                $result[$key] = $this->aRole->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collUserObjs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userActivities';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_activities';
                        break;
                    default:
                        $key = 'UserObjs';
                }

                $result[$key] = $this->collUserObjs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUserRegistries) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userRegistries';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_registries';
                        break;
                    default:
                        $key = 'UserRegistries';
                }

                $result[$key] = $this->collUserRegistries->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUsers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'userLinkedIns';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_linkedins';
                        break;
                    default:
                        $key = 'Users';
                }

                $result[$key] = $this->collUsers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collFacebookUsers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'facebookUsers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'facebook_users';
                        break;
                    default:
                        $key = 'FacebookUsers';
                }

                $result[$key] = $this->collFacebookUsers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSuppliers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'suppliers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'suppliers';
                        break;
                    default:
                        $key = 'Suppliers';
                }

                $result[$key] = $this->collSuppliers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProducts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'products';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'products';
                        break;
                    default:
                        $key = 'Products';
                }

                $result[$key] = $this->collProducts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaleOrders) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'saleOrders';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_orders';
                        break;
                    default:
                        $key = 'SaleOrders';
                }

                $result[$key] = $this->collSaleOrders->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCustomerNotes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customerNotes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'customer_notes';
                        break;
                    default:
                        $key = 'CustomerNotes';
                }

                $result[$key] = $this->collCustomerNotes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteBlogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteBlogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_blogs';
                        break;
                    default:
                        $key = 'SiteBlogs';
                }

                $result[$key] = $this->collSiteBlogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Account\User
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UserTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Account\User
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setItemDeleted($value);
                break;
            case 2:
                $this->setCanChangeRoles($value);
                break;
            case 3:
                $this->setRoleId($value);
                break;
            case 4:
                $this->setPreferedLanguageId($value);
                break;
            case 5:
                $this->setFirstName($value);
                break;
            case 6:
                $this->setLastName($value);
                break;
            case 7:
                $this->setEmail($value);
                break;
            case 8:
                $this->setPhone($value);
                break;
            case 9:
                $this->setPhoneMobile($value);
                break;
            case 10:
                $this->setWebsite($value);
                break;
            case 11:
                $this->setTwitter($value);
                break;
            case 12:
                $this->setAboutMe($value);
                break;
            case 13:
                $this->setPrivateEmail($value);
                break;
            case 14:
                $this->setPassword($value);
                break;
            case 15:
                $this->setSalt($value);
                break;
            case 16:
                $this->setSourceNetwork($value);
                break;
            case 17:
                $this->setCreated($value);
                break;
            case 18:
                $this->setModified($value);
                break;
            case 19:
                $this->setMustChangePassword($value);
                break;
            case 20:
                $this->setHasProfilePic($value);
                break;
            case 21:
                $this->setPicExt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UserTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setItemDeleted($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCanChangeRoles($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setRoleId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setPreferedLanguageId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setFirstName($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setLastName($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setEmail($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setPhone($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setPhoneMobile($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setWebsite($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setTwitter($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setAboutMe($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setPrivateEmail($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setPassword($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setSalt($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setSourceNetwork($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setCreated($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setModified($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setMustChangePassword($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setHasProfilePic($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setPicExt($arr[$keys[21]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Account\User The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UserTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UserTableMap::COL_ID)) {
            $criteria->add(UserTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(UserTableMap::COL_IS_DELETED)) {
            $criteria->add(UserTableMap::COL_IS_DELETED, $this->is_deleted);
        }
        if ($this->isColumnModified(UserTableMap::COL_CAN_CHANGE_ROLES)) {
            $criteria->add(UserTableMap::COL_CAN_CHANGE_ROLES, $this->can_change_roles);
        }
        if ($this->isColumnModified(UserTableMap::COL_ROLE_ID)) {
            $criteria->add(UserTableMap::COL_ROLE_ID, $this->role_id);
        }
        if ($this->isColumnModified(UserTableMap::COL_PREFERED_LANGUAGE_ID)) {
            $criteria->add(UserTableMap::COL_PREFERED_LANGUAGE_ID, $this->prefered_language_id);
        }
        if ($this->isColumnModified(UserTableMap::COL_FIRST_NAME)) {
            $criteria->add(UserTableMap::COL_FIRST_NAME, $this->first_name);
        }
        if ($this->isColumnModified(UserTableMap::COL_LAST_NAME)) {
            $criteria->add(UserTableMap::COL_LAST_NAME, $this->last_name);
        }
        if ($this->isColumnModified(UserTableMap::COL_EMAIL)) {
            $criteria->add(UserTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(UserTableMap::COL_PHONE)) {
            $criteria->add(UserTableMap::COL_PHONE, $this->phone);
        }
        if ($this->isColumnModified(UserTableMap::COL_PHONE_MOBILE)) {
            $criteria->add(UserTableMap::COL_PHONE_MOBILE, $this->phone_mobile);
        }
        if ($this->isColumnModified(UserTableMap::COL_WEBSITE)) {
            $criteria->add(UserTableMap::COL_WEBSITE, $this->website);
        }
        if ($this->isColumnModified(UserTableMap::COL_TWITTER)) {
            $criteria->add(UserTableMap::COL_TWITTER, $this->twitter);
        }
        if ($this->isColumnModified(UserTableMap::COL_ABOUT_ME)) {
            $criteria->add(UserTableMap::COL_ABOUT_ME, $this->about_me);
        }
        if ($this->isColumnModified(UserTableMap::COL_PRIVATE_EMAIL)) {
            $criteria->add(UserTableMap::COL_PRIVATE_EMAIL, $this->private_email);
        }
        if ($this->isColumnModified(UserTableMap::COL_PASSWORD)) {
            $criteria->add(UserTableMap::COL_PASSWORD, $this->password);
        }
        if ($this->isColumnModified(UserTableMap::COL_SALT)) {
            $criteria->add(UserTableMap::COL_SALT, $this->salt);
        }
        if ($this->isColumnModified(UserTableMap::COL_SOURCE_NETWORK)) {
            $criteria->add(UserTableMap::COL_SOURCE_NETWORK, $this->source_network);
        }
        if ($this->isColumnModified(UserTableMap::COL_CREATED)) {
            $criteria->add(UserTableMap::COL_CREATED, $this->created);
        }
        if ($this->isColumnModified(UserTableMap::COL_MODIFIED)) {
            $criteria->add(UserTableMap::COL_MODIFIED, $this->modified);
        }
        if ($this->isColumnModified(UserTableMap::COL_MUST_CHANGE_PASSWORD)) {
            $criteria->add(UserTableMap::COL_MUST_CHANGE_PASSWORD, $this->must_change_password);
        }
        if ($this->isColumnModified(UserTableMap::COL_HAS_PROFILE_PIC)) {
            $criteria->add(UserTableMap::COL_HAS_PROFILE_PIC, $this->has_profile_pic);
        }
        if ($this->isColumnModified(UserTableMap::COL_PIC_EXT)) {
            $criteria->add(UserTableMap::COL_PIC_EXT, $this->pic_ext);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUserQuery::create();
        $criteria->add(UserTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Account\User (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setItemDeleted($this->getItemDeleted());
        $copyObj->setCanChangeRoles($this->getCanChangeRoles());
        $copyObj->setRoleId($this->getRoleId());
        $copyObj->setPreferedLanguageId($this->getPreferedLanguageId());
        $copyObj->setFirstName($this->getFirstName());
        $copyObj->setLastName($this->getLastName());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setPhoneMobile($this->getPhoneMobile());
        $copyObj->setWebsite($this->getWebsite());
        $copyObj->setTwitter($this->getTwitter());
        $copyObj->setAboutMe($this->getAboutMe());
        $copyObj->setPrivateEmail($this->getPrivateEmail());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setSalt($this->getSalt());
        $copyObj->setSourceNetwork($this->getSourceNetwork());
        $copyObj->setCreated($this->getCreated());
        $copyObj->setModified($this->getModified());
        $copyObj->setMustChangePassword($this->getMustChangePassword());
        $copyObj->setHasProfilePic($this->getHasProfilePic());
        $copyObj->setPicExt($this->getPicExt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getUserObjs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserObj($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUserRegistries() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUserRegistry($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUsers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUser($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getFacebookUsers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addFacebookUser($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSuppliers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSupplier($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProducts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProduct($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaleOrders() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaleOrder($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCustomerNotes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomerNote($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteBlogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteBlog($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Account\User Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a Language object.
     *
     * @param  Language|null $v
     * @return $this|\Model\Account\User The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPreferredLanguage(Language $v = null)
    {
        if ($v === null) {
            $this->setPreferedLanguageId(NULL);
        } else {
            $this->setPreferedLanguageId($v->getId());
        }

        $this->aPreferredLanguage = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Language object, it will not be re-added.
        if ($v !== null) {
            $v->addUser($this);
        }


        return $this;
    }


    /**
     * Get the associated Language object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Language|null The associated Language object.
     * @throws PropelException
     */
    public function getPreferredLanguage(ConnectionInterface $con = null)
    {
        if ($this->aPreferredLanguage === null && ($this->prefered_language_id != 0)) {
            $this->aPreferredLanguage = LanguageQuery::create()->findPk($this->prefered_language_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPreferredLanguage->addUsers($this);
             */
        }

        return $this->aPreferredLanguage;
    }

    /**
     * Declares an association between this object and a Role object.
     *
     * @param  Role $v
     * @return $this|\Model\Account\User The current object (for fluent API support)
     * @throws PropelException
     */
    public function setRole(Role $v = null)
    {
        if ($v === null) {
            $this->setRoleId(NULL);
        } else {
            $this->setRoleId($v->getId());
        }

        $this->aRole = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Role object, it will not be re-added.
        if ($v !== null) {
            $v->addUser($this);
        }


        return $this;
    }


    /**
     * Get the associated Role object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Role The associated Role object.
     * @throws PropelException
     */
    public function getRole(ConnectionInterface $con = null)
    {
        if ($this->aRole === null && ($this->role_id != 0)) {
            $this->aRole = RoleQuery::create()->findPk($this->role_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aRole->addUsers($this);
             */
        }

        return $this->aRole;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('UserObj' === $relationName) {
            $this->initUserObjs();
            return;
        }
        if ('UserRegistry' === $relationName) {
            $this->initUserRegistries();
            return;
        }
        if ('User' === $relationName) {
            $this->initUsers();
            return;
        }
        if ('FacebookUser' === $relationName) {
            $this->initFacebookUsers();
            return;
        }
        if ('Supplier' === $relationName) {
            $this->initSuppliers();
            return;
        }
        if ('Product' === $relationName) {
            $this->initProducts();
            return;
        }
        if ('SaleOrder' === $relationName) {
            $this->initSaleOrders();
            return;
        }
        if ('CustomerNote' === $relationName) {
            $this->initCustomerNotes();
            return;
        }
        if ('SiteBlog' === $relationName) {
            $this->initSiteBlogs();
            return;
        }
    }

    /**
     * Clears out the collUserObjs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUserObjs()
     */
    public function clearUserObjs()
    {
        $this->collUserObjs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUserObjs collection loaded partially.
     */
    public function resetPartialUserObjs($v = true)
    {
        $this->collUserObjsPartial = $v;
    }

    /**
     * Initializes the collUserObjs collection.
     *
     * By default this just sets the collUserObjs collection to an empty array (like clearcollUserObjs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserObjs($overrideExisting = true)
    {
        if (null !== $this->collUserObjs && !$overrideExisting) {
            return;
        }

        $collectionClassName = UserActivityTableMap::getTableMap()->getCollectionClassName();

        $this->collUserObjs = new $collectionClassName;
        $this->collUserObjs->setModel('\Model\User\UserActivity');
    }

    /**
     * Gets an array of UserActivity objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|UserActivity[] List of UserActivity objects
     * @throws PropelException
     */
    public function getUserObjs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUserObjsPartial && !$this->isNew();
        if (null === $this->collUserObjs || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUserObjs) {
                    $this->initUserObjs();
                } else {
                    $collectionClassName = UserActivityTableMap::getTableMap()->getCollectionClassName();

                    $collUserObjs = new $collectionClassName;
                    $collUserObjs->setModel('\Model\User\UserActivity');

                    return $collUserObjs;
                }
            } else {
                $collUserObjs = UserActivityQuery::create(null, $criteria)
                    ->filterByUserObj($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUserObjsPartial && count($collUserObjs)) {
                        $this->initUserObjs(false);

                        foreach ($collUserObjs as $obj) {
                            if (false == $this->collUserObjs->contains($obj)) {
                                $this->collUserObjs->append($obj);
                            }
                        }

                        $this->collUserObjsPartial = true;
                    }

                    return $collUserObjs;
                }

                if ($partial && $this->collUserObjs) {
                    foreach ($this->collUserObjs as $obj) {
                        if ($obj->isNew()) {
                            $collUserObjs[] = $obj;
                        }
                    }
                }

                $this->collUserObjs = $collUserObjs;
                $this->collUserObjsPartial = false;
            }
        }

        return $this->collUserObjs;
    }

    /**
     * Sets a collection of UserActivity objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $userObjs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setUserObjs(Collection $userObjs, ConnectionInterface $con = null)
    {
        /** @var UserActivity[] $userObjsToDelete */
        $userObjsToDelete = $this->getUserObjs(new Criteria(), $con)->diff($userObjs);


        $this->userObjsScheduledForDeletion = $userObjsToDelete;

        foreach ($userObjsToDelete as $userObjRemoved) {
            $userObjRemoved->setUserObj(null);
        }

        $this->collUserObjs = null;
        foreach ($userObjs as $userObj) {
            $this->addUserObj($userObj);
        }

        $this->collUserObjs = $userObjs;
        $this->collUserObjsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseUserActivity objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseUserActivity objects.
     * @throws PropelException
     */
    public function countUserObjs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUserObjsPartial && !$this->isNew();
        if (null === $this->collUserObjs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserObjs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserObjs());
            }

            $query = UserActivityQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUserObj($this)
                ->count($con);
        }

        return count($this->collUserObjs);
    }

    /**
     * Method called to associate a UserActivity object to this object
     * through the UserActivity foreign key attribute.
     *
     * @param  UserActivity $l UserActivity
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function addUserObj(UserActivity $l)
    {
        if ($this->collUserObjs === null) {
            $this->initUserObjs();
            $this->collUserObjsPartial = true;
        }

        if (!$this->collUserObjs->contains($l)) {
            $this->doAddUserObj($l);

            if ($this->userObjsScheduledForDeletion and $this->userObjsScheduledForDeletion->contains($l)) {
                $this->userObjsScheduledForDeletion->remove($this->userObjsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param UserActivity $userObj The UserActivity object to add.
     */
    protected function doAddUserObj(UserActivity $userObj)
    {
        $this->collUserObjs[]= $userObj;
        $userObj->setUserObj($this);
    }

    /**
     * @param  UserActivity $userObj The UserActivity object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeUserObj(UserActivity $userObj)
    {
        if ($this->getUserObjs()->contains($userObj)) {
            $pos = $this->collUserObjs->search($userObj);
            $this->collUserObjs->remove($pos);
            if (null === $this->userObjsScheduledForDeletion) {
                $this->userObjsScheduledForDeletion = clone $this->collUserObjs;
                $this->userObjsScheduledForDeletion->clear();
            }
            $this->userObjsScheduledForDeletion[]= clone $userObj;
            $userObj->setUserObj(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related UserObjs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|UserActivity[] List of UserActivity objects
     */
    public function getUserObjsJoinActivityType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = UserActivityQuery::create(null, $criteria);
        $query->joinWith('ActivityType', $joinBehavior);

        return $this->getUserObjs($query, $con);
    }

    /**
     * Clears out the collUserRegistries collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUserRegistries()
     */
    public function clearUserRegistries()
    {
        $this->collUserRegistries = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUserRegistries collection loaded partially.
     */
    public function resetPartialUserRegistries($v = true)
    {
        $this->collUserRegistriesPartial = $v;
    }

    /**
     * Initializes the collUserRegistries collection.
     *
     * By default this just sets the collUserRegistries collection to an empty array (like clearcollUserRegistries());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUserRegistries($overrideExisting = true)
    {
        if (null !== $this->collUserRegistries && !$overrideExisting) {
            return;
        }

        $collectionClassName = UserRegistryTableMap::getTableMap()->getCollectionClassName();

        $this->collUserRegistries = new $collectionClassName;
        $this->collUserRegistries->setModel('\Model\User\UserRegistry');
    }

    /**
     * Gets an array of UserRegistry objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|UserRegistry[] List of UserRegistry objects
     * @throws PropelException
     */
    public function getUserRegistries(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUserRegistriesPartial && !$this->isNew();
        if (null === $this->collUserRegistries || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUserRegistries) {
                    $this->initUserRegistries();
                } else {
                    $collectionClassName = UserRegistryTableMap::getTableMap()->getCollectionClassName();

                    $collUserRegistries = new $collectionClassName;
                    $collUserRegistries->setModel('\Model\User\UserRegistry');

                    return $collUserRegistries;
                }
            } else {
                $collUserRegistries = UserRegistryQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUserRegistriesPartial && count($collUserRegistries)) {
                        $this->initUserRegistries(false);

                        foreach ($collUserRegistries as $obj) {
                            if (false == $this->collUserRegistries->contains($obj)) {
                                $this->collUserRegistries->append($obj);
                            }
                        }

                        $this->collUserRegistriesPartial = true;
                    }

                    return $collUserRegistries;
                }

                if ($partial && $this->collUserRegistries) {
                    foreach ($this->collUserRegistries as $obj) {
                        if ($obj->isNew()) {
                            $collUserRegistries[] = $obj;
                        }
                    }
                }

                $this->collUserRegistries = $collUserRegistries;
                $this->collUserRegistriesPartial = false;
            }
        }

        return $this->collUserRegistries;
    }

    /**
     * Sets a collection of UserRegistry objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $userRegistries A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setUserRegistries(Collection $userRegistries, ConnectionInterface $con = null)
    {
        /** @var UserRegistry[] $userRegistriesToDelete */
        $userRegistriesToDelete = $this->getUserRegistries(new Criteria(), $con)->diff($userRegistries);


        $this->userRegistriesScheduledForDeletion = $userRegistriesToDelete;

        foreach ($userRegistriesToDelete as $userRegistryRemoved) {
            $userRegistryRemoved->setUser(null);
        }

        $this->collUserRegistries = null;
        foreach ($userRegistries as $userRegistry) {
            $this->addUserRegistry($userRegistry);
        }

        $this->collUserRegistries = $userRegistries;
        $this->collUserRegistriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseUserRegistry objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseUserRegistry objects.
     * @throws PropelException
     */
    public function countUserRegistries(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUserRegistriesPartial && !$this->isNew();
        if (null === $this->collUserRegistries || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUserRegistries) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUserRegistries());
            }

            $query = UserRegistryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collUserRegistries);
    }

    /**
     * Method called to associate a UserRegistry object to this object
     * through the UserRegistry foreign key attribute.
     *
     * @param  UserRegistry $l UserRegistry
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function addUserRegistry(UserRegistry $l)
    {
        if ($this->collUserRegistries === null) {
            $this->initUserRegistries();
            $this->collUserRegistriesPartial = true;
        }

        if (!$this->collUserRegistries->contains($l)) {
            $this->doAddUserRegistry($l);

            if ($this->userRegistriesScheduledForDeletion and $this->userRegistriesScheduledForDeletion->contains($l)) {
                $this->userRegistriesScheduledForDeletion->remove($this->userRegistriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param UserRegistry $userRegistry The UserRegistry object to add.
     */
    protected function doAddUserRegistry(UserRegistry $userRegistry)
    {
        $this->collUserRegistries[]= $userRegistry;
        $userRegistry->setUser($this);
    }

    /**
     * @param  UserRegistry $userRegistry The UserRegistry object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeUserRegistry(UserRegistry $userRegistry)
    {
        if ($this->getUserRegistries()->contains($userRegistry)) {
            $pos = $this->collUserRegistries->search($userRegistry);
            $this->collUserRegistries->remove($pos);
            if (null === $this->userRegistriesScheduledForDeletion) {
                $this->userRegistriesScheduledForDeletion = clone $this->collUserRegistries;
                $this->userRegistriesScheduledForDeletion->clear();
            }
            $this->userRegistriesScheduledForDeletion[]= clone $userRegistry;
            $userRegistry->setUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collUsers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUsers()
     */
    public function clearUsers()
    {
        $this->collUsers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUsers collection loaded partially.
     */
    public function resetPartialUsers($v = true)
    {
        $this->collUsersPartial = $v;
    }

    /**
     * Initializes the collUsers collection.
     *
     * By default this just sets the collUsers collection to an empty array (like clearcollUsers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUsers($overrideExisting = true)
    {
        if (null !== $this->collUsers && !$overrideExisting) {
            return;
        }

        $collectionClassName = UserLinkedInTableMap::getTableMap()->getCollectionClassName();

        $this->collUsers = new $collectionClassName;
        $this->collUsers->setModel('\Model\Crm\UserLinkedIn');
    }

    /**
     * Gets an array of UserLinkedIn objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|UserLinkedIn[] List of UserLinkedIn objects
     * @throws PropelException
     */
    public function getUsers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUsersPartial && !$this->isNew();
        if (null === $this->collUsers || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUsers) {
                    $this->initUsers();
                } else {
                    $collectionClassName = UserLinkedInTableMap::getTableMap()->getCollectionClassName();

                    $collUsers = new $collectionClassName;
                    $collUsers->setModel('\Model\Crm\UserLinkedIn');

                    return $collUsers;
                }
            } else {
                $collUsers = UserLinkedInQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUsersPartial && count($collUsers)) {
                        $this->initUsers(false);

                        foreach ($collUsers as $obj) {
                            if (false == $this->collUsers->contains($obj)) {
                                $this->collUsers->append($obj);
                            }
                        }

                        $this->collUsersPartial = true;
                    }

                    return $collUsers;
                }

                if ($partial && $this->collUsers) {
                    foreach ($this->collUsers as $obj) {
                        if ($obj->isNew()) {
                            $collUsers[] = $obj;
                        }
                    }
                }

                $this->collUsers = $collUsers;
                $this->collUsersPartial = false;
            }
        }

        return $this->collUsers;
    }

    /**
     * Sets a collection of UserLinkedIn objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $users A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setUsers(Collection $users, ConnectionInterface $con = null)
    {
        /** @var UserLinkedIn[] $usersToDelete */
        $usersToDelete = $this->getUsers(new Criteria(), $con)->diff($users);


        $this->usersScheduledForDeletion = $usersToDelete;

        foreach ($usersToDelete as $userRemoved) {
            $userRemoved->setUser(null);
        }

        $this->collUsers = null;
        foreach ($users as $user) {
            $this->addUser($user);
        }

        $this->collUsers = $users;
        $this->collUsersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseUserLinkedIn objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseUserLinkedIn objects.
     * @throws PropelException
     */
    public function countUsers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUsersPartial && !$this->isNew();
        if (null === $this->collUsers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUsers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUsers());
            }

            $query = UserLinkedInQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collUsers);
    }

    /**
     * Method called to associate a UserLinkedIn object to this object
     * through the UserLinkedIn foreign key attribute.
     *
     * @param  UserLinkedIn $l UserLinkedIn
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function addUser(UserLinkedIn $l)
    {
        if ($this->collUsers === null) {
            $this->initUsers();
            $this->collUsersPartial = true;
        }

        if (!$this->collUsers->contains($l)) {
            $this->doAddUser($l);

            if ($this->usersScheduledForDeletion and $this->usersScheduledForDeletion->contains($l)) {
                $this->usersScheduledForDeletion->remove($this->usersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param UserLinkedIn $user The UserLinkedIn object to add.
     */
    protected function doAddUser(UserLinkedIn $user)
    {
        $this->collUsers[]= $user;
        $user->setUser($this);
    }

    /**
     * @param  UserLinkedIn $user The UserLinkedIn object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeUser(UserLinkedIn $user)
    {
        if ($this->getUsers()->contains($user)) {
            $pos = $this->collUsers->search($user);
            $this->collUsers->remove($pos);
            if (null === $this->usersScheduledForDeletion) {
                $this->usersScheduledForDeletion = clone $this->collUsers;
                $this->usersScheduledForDeletion->clear();
            }
            $this->usersScheduledForDeletion[]= clone $user;
            $user->setUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collFacebookUsers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addFacebookUsers()
     */
    public function clearFacebookUsers()
    {
        $this->collFacebookUsers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collFacebookUsers collection loaded partially.
     */
    public function resetPartialFacebookUsers($v = true)
    {
        $this->collFacebookUsersPartial = $v;
    }

    /**
     * Initializes the collFacebookUsers collection.
     *
     * By default this just sets the collFacebookUsers collection to an empty array (like clearcollFacebookUsers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initFacebookUsers($overrideExisting = true)
    {
        if (null !== $this->collFacebookUsers && !$overrideExisting) {
            return;
        }

        $collectionClassName = FacebookUserTableMap::getTableMap()->getCollectionClassName();

        $this->collFacebookUsers = new $collectionClassName;
        $this->collFacebookUsers->setModel('\Model\Account\FacebookUser');
    }

    /**
     * Gets an array of ChildFacebookUser objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildFacebookUser[] List of ChildFacebookUser objects
     * @throws PropelException
     */
    public function getFacebookUsers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collFacebookUsersPartial && !$this->isNew();
        if (null === $this->collFacebookUsers || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collFacebookUsers) {
                    $this->initFacebookUsers();
                } else {
                    $collectionClassName = FacebookUserTableMap::getTableMap()->getCollectionClassName();

                    $collFacebookUsers = new $collectionClassName;
                    $collFacebookUsers->setModel('\Model\Account\FacebookUser');

                    return $collFacebookUsers;
                }
            } else {
                $collFacebookUsers = ChildFacebookUserQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collFacebookUsersPartial && count($collFacebookUsers)) {
                        $this->initFacebookUsers(false);

                        foreach ($collFacebookUsers as $obj) {
                            if (false == $this->collFacebookUsers->contains($obj)) {
                                $this->collFacebookUsers->append($obj);
                            }
                        }

                        $this->collFacebookUsersPartial = true;
                    }

                    return $collFacebookUsers;
                }

                if ($partial && $this->collFacebookUsers) {
                    foreach ($this->collFacebookUsers as $obj) {
                        if ($obj->isNew()) {
                            $collFacebookUsers[] = $obj;
                        }
                    }
                }

                $this->collFacebookUsers = $collFacebookUsers;
                $this->collFacebookUsersPartial = false;
            }
        }

        return $this->collFacebookUsers;
    }

    /**
     * Sets a collection of ChildFacebookUser objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $facebookUsers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setFacebookUsers(Collection $facebookUsers, ConnectionInterface $con = null)
    {
        /** @var ChildFacebookUser[] $facebookUsersToDelete */
        $facebookUsersToDelete = $this->getFacebookUsers(new Criteria(), $con)->diff($facebookUsers);


        $this->facebookUsersScheduledForDeletion = $facebookUsersToDelete;

        foreach ($facebookUsersToDelete as $facebookUserRemoved) {
            $facebookUserRemoved->setUser(null);
        }

        $this->collFacebookUsers = null;
        foreach ($facebookUsers as $facebookUser) {
            $this->addFacebookUser($facebookUser);
        }

        $this->collFacebookUsers = $facebookUsers;
        $this->collFacebookUsersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related FacebookUser objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related FacebookUser objects.
     * @throws PropelException
     */
    public function countFacebookUsers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collFacebookUsersPartial && !$this->isNew();
        if (null === $this->collFacebookUsers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collFacebookUsers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getFacebookUsers());
            }

            $query = ChildFacebookUserQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collFacebookUsers);
    }

    /**
     * Method called to associate a ChildFacebookUser object to this object
     * through the ChildFacebookUser foreign key attribute.
     *
     * @param  ChildFacebookUser $l ChildFacebookUser
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function addFacebookUser(ChildFacebookUser $l)
    {
        if ($this->collFacebookUsers === null) {
            $this->initFacebookUsers();
            $this->collFacebookUsersPartial = true;
        }

        if (!$this->collFacebookUsers->contains($l)) {
            $this->doAddFacebookUser($l);

            if ($this->facebookUsersScheduledForDeletion and $this->facebookUsersScheduledForDeletion->contains($l)) {
                $this->facebookUsersScheduledForDeletion->remove($this->facebookUsersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildFacebookUser $facebookUser The ChildFacebookUser object to add.
     */
    protected function doAddFacebookUser(ChildFacebookUser $facebookUser)
    {
        $this->collFacebookUsers[]= $facebookUser;
        $facebookUser->setUser($this);
    }

    /**
     * @param  ChildFacebookUser $facebookUser The ChildFacebookUser object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeFacebookUser(ChildFacebookUser $facebookUser)
    {
        if ($this->getFacebookUsers()->contains($facebookUser)) {
            $pos = $this->collFacebookUsers->search($facebookUser);
            $this->collFacebookUsers->remove($pos);
            if (null === $this->facebookUsersScheduledForDeletion) {
                $this->facebookUsersScheduledForDeletion = clone $this->collFacebookUsers;
                $this->facebookUsersScheduledForDeletion->clear();
            }
            $this->facebookUsersScheduledForDeletion[]= $facebookUser;
            $facebookUser->setUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collSuppliers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSuppliers()
     */
    public function clearSuppliers()
    {
        $this->collSuppliers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSuppliers collection loaded partially.
     */
    public function resetPartialSuppliers($v = true)
    {
        $this->collSuppliersPartial = $v;
    }

    /**
     * Initializes the collSuppliers collection.
     *
     * By default this just sets the collSuppliers collection to an empty array (like clearcollSuppliers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSuppliers($overrideExisting = true)
    {
        if (null !== $this->collSuppliers && !$overrideExisting) {
            return;
        }

        $collectionClassName = SupplierTableMap::getTableMap()->getCollectionClassName();

        $this->collSuppliers = new $collectionClassName;
        $this->collSuppliers->setModel('\Model\Supplier\Supplier');
    }

    /**
     * Gets an array of Supplier objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Supplier[] List of Supplier objects
     * @throws PropelException
     */
    public function getSuppliers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSuppliersPartial && !$this->isNew();
        if (null === $this->collSuppliers || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSuppliers) {
                    $this->initSuppliers();
                } else {
                    $collectionClassName = SupplierTableMap::getTableMap()->getCollectionClassName();

                    $collSuppliers = new $collectionClassName;
                    $collSuppliers->setModel('\Model\Supplier\Supplier');

                    return $collSuppliers;
                }
            } else {
                $collSuppliers = SupplierQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSuppliersPartial && count($collSuppliers)) {
                        $this->initSuppliers(false);

                        foreach ($collSuppliers as $obj) {
                            if (false == $this->collSuppliers->contains($obj)) {
                                $this->collSuppliers->append($obj);
                            }
                        }

                        $this->collSuppliersPartial = true;
                    }

                    return $collSuppliers;
                }

                if ($partial && $this->collSuppliers) {
                    foreach ($this->collSuppliers as $obj) {
                        if ($obj->isNew()) {
                            $collSuppliers[] = $obj;
                        }
                    }
                }

                $this->collSuppliers = $collSuppliers;
                $this->collSuppliersPartial = false;
            }
        }

        return $this->collSuppliers;
    }

    /**
     * Sets a collection of Supplier objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $suppliers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setSuppliers(Collection $suppliers, ConnectionInterface $con = null)
    {
        /** @var Supplier[] $suppliersToDelete */
        $suppliersToDelete = $this->getSuppliers(new Criteria(), $con)->diff($suppliers);


        $this->suppliersScheduledForDeletion = $suppliersToDelete;

        foreach ($suppliersToDelete as $supplierRemoved) {
            $supplierRemoved->setUser(null);
        }

        $this->collSuppliers = null;
        foreach ($suppliers as $supplier) {
            $this->addSupplier($supplier);
        }

        $this->collSuppliers = $suppliers;
        $this->collSuppliersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSupplier objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSupplier objects.
     * @throws PropelException
     */
    public function countSuppliers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSuppliersPartial && !$this->isNew();
        if (null === $this->collSuppliers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSuppliers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSuppliers());
            }

            $query = SupplierQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collSuppliers);
    }

    /**
     * Method called to associate a Supplier object to this object
     * through the Supplier foreign key attribute.
     *
     * @param  Supplier $l Supplier
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function addSupplier(Supplier $l)
    {
        if ($this->collSuppliers === null) {
            $this->initSuppliers();
            $this->collSuppliersPartial = true;
        }

        if (!$this->collSuppliers->contains($l)) {
            $this->doAddSupplier($l);

            if ($this->suppliersScheduledForDeletion and $this->suppliersScheduledForDeletion->contains($l)) {
                $this->suppliersScheduledForDeletion->remove($this->suppliersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Supplier $supplier The Supplier object to add.
     */
    protected function doAddSupplier(Supplier $supplier)
    {
        $this->collSuppliers[]= $supplier;
        $supplier->setUser($this);
    }

    /**
     * @param  Supplier $supplier The Supplier object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeSupplier(Supplier $supplier)
    {
        if ($this->getSuppliers()->contains($supplier)) {
            $pos = $this->collSuppliers->search($supplier);
            $this->collSuppliers->remove($pos);
            if (null === $this->suppliersScheduledForDeletion) {
                $this->suppliersScheduledForDeletion = clone $this->collSuppliers;
                $this->suppliersScheduledForDeletion->clear();
            }
            $this->suppliersScheduledForDeletion[]= $supplier;
            $supplier->setUser(null);
        }

        return $this;
    }

    /**
     * Clears out the collProducts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProducts()
     */
    public function clearProducts()
    {
        $this->collProducts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProducts collection loaded partially.
     */
    public function resetPartialProducts($v = true)
    {
        $this->collProductsPartial = $v;
    }

    /**
     * Initializes the collProducts collection.
     *
     * By default this just sets the collProducts collection to an empty array (like clearcollProducts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProducts($overrideExisting = true)
    {
        if (null !== $this->collProducts && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductTableMap::getTableMap()->getCollectionClassName();

        $this->collProducts = new $collectionClassName;
        $this->collProducts->setModel('\Model\Product');
    }

    /**
     * Gets an array of Product objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Product[] List of Product objects
     * @throws PropelException
     */
    public function getProducts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductsPartial && !$this->isNew();
        if (null === $this->collProducts || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProducts) {
                    $this->initProducts();
                } else {
                    $collectionClassName = ProductTableMap::getTableMap()->getCollectionClassName();

                    $collProducts = new $collectionClassName;
                    $collProducts->setModel('\Model\Product');

                    return $collProducts;
                }
            } else {
                $collProducts = ProductQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductsPartial && count($collProducts)) {
                        $this->initProducts(false);

                        foreach ($collProducts as $obj) {
                            if (false == $this->collProducts->contains($obj)) {
                                $this->collProducts->append($obj);
                            }
                        }

                        $this->collProductsPartial = true;
                    }

                    return $collProducts;
                }

                if ($partial && $this->collProducts) {
                    foreach ($this->collProducts as $obj) {
                        if ($obj->isNew()) {
                            $collProducts[] = $obj;
                        }
                    }
                }

                $this->collProducts = $collProducts;
                $this->collProductsPartial = false;
            }
        }

        return $this->collProducts;
    }

    /**
     * Sets a collection of Product objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $products A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setProducts(Collection $products, ConnectionInterface $con = null)
    {
        /** @var Product[] $productsToDelete */
        $productsToDelete = $this->getProducts(new Criteria(), $con)->diff($products);


        $this->productsScheduledForDeletion = $productsToDelete;

        foreach ($productsToDelete as $productRemoved) {
            $productRemoved->setUser(null);
        }

        $this->collProducts = null;
        foreach ($products as $product) {
            $this->addProduct($product);
        }

        $this->collProducts = $products;
        $this->collProductsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseProduct objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseProduct objects.
     * @throws PropelException
     */
    public function countProducts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductsPartial && !$this->isNew();
        if (null === $this->collProducts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProducts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProducts());
            }

            $query = ProductQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collProducts);
    }

    /**
     * Method called to associate a Product object to this object
     * through the Product foreign key attribute.
     *
     * @param  Product $l Product
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function addProduct(Product $l)
    {
        if ($this->collProducts === null) {
            $this->initProducts();
            $this->collProductsPartial = true;
        }

        if (!$this->collProducts->contains($l)) {
            $this->doAddProduct($l);

            if ($this->productsScheduledForDeletion and $this->productsScheduledForDeletion->contains($l)) {
                $this->productsScheduledForDeletion->remove($this->productsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Product $product The Product object to add.
     */
    protected function doAddProduct(Product $product)
    {
        $this->collProducts[]= $product;
        $product->setUser($this);
    }

    /**
     * @param  Product $product The Product object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeProduct(Product $product)
    {
        if ($this->getProducts()->contains($product)) {
            $pos = $this->collProducts->search($product);
            $this->collProducts->remove($pos);
            if (null === $this->productsScheduledForDeletion) {
                $this->productsScheduledForDeletion = clone $this->collProducts;
                $this->productsScheduledForDeletion->clear();
            }
            $this->productsScheduledForDeletion[]= $product;
            $product->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related Products from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Product[] List of Product objects
     */
    public function getProductsJoinBrand(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductQuery::create(null, $criteria);
        $query->joinWith('Brand', $joinBehavior);

        return $this->getProducts($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related Products from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Product[] List of Product objects
     */
    public function getProductsJoinProductUnit(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductQuery::create(null, $criteria);
        $query->joinWith('ProductUnit', $joinBehavior);

        return $this->getProducts($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related Products from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Product[] List of Product objects
     */
    public function getProductsJoinSupplierRef(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductQuery::create(null, $criteria);
        $query->joinWith('SupplierRef', $joinBehavior);

        return $this->getProducts($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related Products from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Product[] List of Product objects
     */
    public function getProductsJoinCategory(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductQuery::create(null, $criteria);
        $query->joinWith('Category', $joinBehavior);

        return $this->getProducts($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related Products from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Product[] List of Product objects
     */
    public function getProductsJoinVat(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductQuery::create(null, $criteria);
        $query->joinWith('Vat', $joinBehavior);

        return $this->getProducts($query, $con);
    }

    /**
     * Clears out the collSaleOrders collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSaleOrders()
     */
    public function clearSaleOrders()
    {
        $this->collSaleOrders = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSaleOrders collection loaded partially.
     */
    public function resetPartialSaleOrders($v = true)
    {
        $this->collSaleOrdersPartial = $v;
    }

    /**
     * Initializes the collSaleOrders collection.
     *
     * By default this just sets the collSaleOrders collection to an empty array (like clearcollSaleOrders());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaleOrders($overrideExisting = true)
    {
        if (null !== $this->collSaleOrders && !$overrideExisting) {
            return;
        }

        $collectionClassName = SaleOrderTableMap::getTableMap()->getCollectionClassName();

        $this->collSaleOrders = new $collectionClassName;
        $this->collSaleOrders->setModel('\Model\Sale\SaleOrder');
    }

    /**
     * Gets an array of SaleOrder objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|SaleOrder[] List of SaleOrder objects
     * @throws PropelException
     */
    public function getSaleOrders(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrdersPartial && !$this->isNew();
        if (null === $this->collSaleOrders || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSaleOrders) {
                    $this->initSaleOrders();
                } else {
                    $collectionClassName = SaleOrderTableMap::getTableMap()->getCollectionClassName();

                    $collSaleOrders = new $collectionClassName;
                    $collSaleOrders->setModel('\Model\Sale\SaleOrder');

                    return $collSaleOrders;
                }
            } else {
                $collSaleOrders = SaleOrderQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSaleOrdersPartial && count($collSaleOrders)) {
                        $this->initSaleOrders(false);

                        foreach ($collSaleOrders as $obj) {
                            if (false == $this->collSaleOrders->contains($obj)) {
                                $this->collSaleOrders->append($obj);
                            }
                        }

                        $this->collSaleOrdersPartial = true;
                    }

                    return $collSaleOrders;
                }

                if ($partial && $this->collSaleOrders) {
                    foreach ($this->collSaleOrders as $obj) {
                        if ($obj->isNew()) {
                            $collSaleOrders[] = $obj;
                        }
                    }
                }

                $this->collSaleOrders = $collSaleOrders;
                $this->collSaleOrdersPartial = false;
            }
        }

        return $this->collSaleOrders;
    }

    /**
     * Sets a collection of SaleOrder objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $saleOrders A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setSaleOrders(Collection $saleOrders, ConnectionInterface $con = null)
    {
        /** @var SaleOrder[] $saleOrdersToDelete */
        $saleOrdersToDelete = $this->getSaleOrders(new Criteria(), $con)->diff($saleOrders);


        $this->saleOrdersScheduledForDeletion = $saleOrdersToDelete;

        foreach ($saleOrdersToDelete as $saleOrderRemoved) {
            $saleOrderRemoved->setUser(null);
        }

        $this->collSaleOrders = null;
        foreach ($saleOrders as $saleOrder) {
            $this->addSaleOrder($saleOrder);
        }

        $this->collSaleOrders = $saleOrders;
        $this->collSaleOrdersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSaleOrder objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSaleOrder objects.
     * @throws PropelException
     */
    public function countSaleOrders(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrdersPartial && !$this->isNew();
        if (null === $this->collSaleOrders || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaleOrders) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSaleOrders());
            }

            $query = SaleOrderQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collSaleOrders);
    }

    /**
     * Method called to associate a SaleOrder object to this object
     * through the SaleOrder foreign key attribute.
     *
     * @param  SaleOrder $l SaleOrder
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function addSaleOrder(SaleOrder $l)
    {
        if ($this->collSaleOrders === null) {
            $this->initSaleOrders();
            $this->collSaleOrdersPartial = true;
        }

        if (!$this->collSaleOrders->contains($l)) {
            $this->doAddSaleOrder($l);

            if ($this->saleOrdersScheduledForDeletion and $this->saleOrdersScheduledForDeletion->contains($l)) {
                $this->saleOrdersScheduledForDeletion->remove($this->saleOrdersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param SaleOrder $saleOrder The SaleOrder object to add.
     */
    protected function doAddSaleOrder(SaleOrder $saleOrder)
    {
        $this->collSaleOrders[]= $saleOrder;
        $saleOrder->setUser($this);
    }

    /**
     * @param  SaleOrder $saleOrder The SaleOrder object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeSaleOrder(SaleOrder $saleOrder)
    {
        if ($this->getSaleOrders()->contains($saleOrder)) {
            $pos = $this->collSaleOrders->search($saleOrder);
            $this->collSaleOrders->remove($pos);
            if (null === $this->saleOrdersScheduledForDeletion) {
                $this->saleOrdersScheduledForDeletion = clone $this->collSaleOrders;
                $this->saleOrdersScheduledForDeletion->clear();
            }
            $this->saleOrdersScheduledForDeletion[]= $saleOrder;
            $saleOrder->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related SaleOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SaleOrder[] List of SaleOrder objects
     */
    public function getSaleOrdersJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SaleOrderQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getSaleOrders($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related SaleOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SaleOrder[] List of SaleOrder objects
     */
    public function getSaleOrdersJoinSaleOrderStatus(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SaleOrderQuery::create(null, $criteria);
        $query->joinWith('SaleOrderStatus', $joinBehavior);

        return $this->getSaleOrders($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related SaleOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SaleOrder[] List of SaleOrder objects
     */
    public function getSaleOrdersJoinShippingMethod(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SaleOrderQuery::create(null, $criteria);
        $query->joinWith('ShippingMethod', $joinBehavior);

        return $this->getSaleOrders($query, $con);
    }

    /**
     * Clears out the collCustomerNotes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomerNotes()
     */
    public function clearCustomerNotes()
    {
        $this->collCustomerNotes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomerNotes collection loaded partially.
     */
    public function resetPartialCustomerNotes($v = true)
    {
        $this->collCustomerNotesPartial = $v;
    }

    /**
     * Initializes the collCustomerNotes collection.
     *
     * By default this just sets the collCustomerNotes collection to an empty array (like clearcollCustomerNotes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomerNotes($overrideExisting = true)
    {
        if (null !== $this->collCustomerNotes && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomerNoteTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomerNotes = new $collectionClassName;
        $this->collCustomerNotes->setModel('\Model\Crm\CustomerNote');
    }

    /**
     * Gets an array of CustomerNote objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|CustomerNote[] List of CustomerNote objects
     * @throws PropelException
     */
    public function getCustomerNotes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerNotesPartial && !$this->isNew();
        if (null === $this->collCustomerNotes || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCustomerNotes) {
                    $this->initCustomerNotes();
                } else {
                    $collectionClassName = CustomerNoteTableMap::getTableMap()->getCollectionClassName();

                    $collCustomerNotes = new $collectionClassName;
                    $collCustomerNotes->setModel('\Model\Crm\CustomerNote');

                    return $collCustomerNotes;
                }
            } else {
                $collCustomerNotes = CustomerNoteQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomerNotesPartial && count($collCustomerNotes)) {
                        $this->initCustomerNotes(false);

                        foreach ($collCustomerNotes as $obj) {
                            if (false == $this->collCustomerNotes->contains($obj)) {
                                $this->collCustomerNotes->append($obj);
                            }
                        }

                        $this->collCustomerNotesPartial = true;
                    }

                    return $collCustomerNotes;
                }

                if ($partial && $this->collCustomerNotes) {
                    foreach ($this->collCustomerNotes as $obj) {
                        if ($obj->isNew()) {
                            $collCustomerNotes[] = $obj;
                        }
                    }
                }

                $this->collCustomerNotes = $collCustomerNotes;
                $this->collCustomerNotesPartial = false;
            }
        }

        return $this->collCustomerNotes;
    }

    /**
     * Sets a collection of CustomerNote objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customerNotes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setCustomerNotes(Collection $customerNotes, ConnectionInterface $con = null)
    {
        /** @var CustomerNote[] $customerNotesToDelete */
        $customerNotesToDelete = $this->getCustomerNotes(new Criteria(), $con)->diff($customerNotes);


        $this->customerNotesScheduledForDeletion = $customerNotesToDelete;

        foreach ($customerNotesToDelete as $customerNoteRemoved) {
            $customerNoteRemoved->setUser(null);
        }

        $this->collCustomerNotes = null;
        foreach ($customerNotes as $customerNote) {
            $this->addCustomerNote($customerNote);
        }

        $this->collCustomerNotes = $customerNotes;
        $this->collCustomerNotesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseCustomerNote objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseCustomerNote objects.
     * @throws PropelException
     */
    public function countCustomerNotes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerNotesPartial && !$this->isNew();
        if (null === $this->collCustomerNotes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomerNotes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomerNotes());
            }

            $query = CustomerNoteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collCustomerNotes);
    }

    /**
     * Method called to associate a CustomerNote object to this object
     * through the CustomerNote foreign key attribute.
     *
     * @param  CustomerNote $l CustomerNote
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function addCustomerNote(CustomerNote $l)
    {
        if ($this->collCustomerNotes === null) {
            $this->initCustomerNotes();
            $this->collCustomerNotesPartial = true;
        }

        if (!$this->collCustomerNotes->contains($l)) {
            $this->doAddCustomerNote($l);

            if ($this->customerNotesScheduledForDeletion and $this->customerNotesScheduledForDeletion->contains($l)) {
                $this->customerNotesScheduledForDeletion->remove($this->customerNotesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param CustomerNote $customerNote The CustomerNote object to add.
     */
    protected function doAddCustomerNote(CustomerNote $customerNote)
    {
        $this->collCustomerNotes[]= $customerNote;
        $customerNote->setUser($this);
    }

    /**
     * @param  CustomerNote $customerNote The CustomerNote object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeCustomerNote(CustomerNote $customerNote)
    {
        if ($this->getCustomerNotes()->contains($customerNote)) {
            $pos = $this->collCustomerNotes->search($customerNote);
            $this->collCustomerNotes->remove($pos);
            if (null === $this->customerNotesScheduledForDeletion) {
                $this->customerNotesScheduledForDeletion = clone $this->collCustomerNotes;
                $this->customerNotesScheduledForDeletion->clear();
            }
            $this->customerNotesScheduledForDeletion[]= $customerNote;
            $customerNote->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related CustomerNotes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|CustomerNote[] List of CustomerNote objects
     */
    public function getCustomerNotesJoinCustomer(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CustomerNoteQuery::create(null, $criteria);
        $query->joinWith('Customer', $joinBehavior);

        return $this->getCustomerNotes($query, $con);
    }

    /**
     * Clears out the collSiteBlogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteBlogs()
     */
    public function clearSiteBlogs()
    {
        $this->collSiteBlogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteBlogs collection loaded partially.
     */
    public function resetPartialSiteBlogs($v = true)
    {
        $this->collSiteBlogsPartial = $v;
    }

    /**
     * Initializes the collSiteBlogs collection.
     *
     * By default this just sets the collSiteBlogs collection to an empty array (like clearcollSiteBlogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteBlogs($overrideExisting = true)
    {
        if (null !== $this->collSiteBlogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteBlogTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteBlogs = new $collectionClassName;
        $this->collSiteBlogs->setModel('\Model\Cms\SiteBlog');
    }

    /**
     * Gets an array of SiteBlog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUser is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|SiteBlog[] List of SiteBlog objects
     * @throws PropelException
     */
    public function getSiteBlogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteBlogsPartial && !$this->isNew();
        if (null === $this->collSiteBlogs || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteBlogs) {
                    $this->initSiteBlogs();
                } else {
                    $collectionClassName = SiteBlogTableMap::getTableMap()->getCollectionClassName();

                    $collSiteBlogs = new $collectionClassName;
                    $collSiteBlogs->setModel('\Model\Cms\SiteBlog');

                    return $collSiteBlogs;
                }
            } else {
                $collSiteBlogs = SiteBlogQuery::create(null, $criteria)
                    ->filterByUser($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteBlogsPartial && count($collSiteBlogs)) {
                        $this->initSiteBlogs(false);

                        foreach ($collSiteBlogs as $obj) {
                            if (false == $this->collSiteBlogs->contains($obj)) {
                                $this->collSiteBlogs->append($obj);
                            }
                        }

                        $this->collSiteBlogsPartial = true;
                    }

                    return $collSiteBlogs;
                }

                if ($partial && $this->collSiteBlogs) {
                    foreach ($this->collSiteBlogs as $obj) {
                        if ($obj->isNew()) {
                            $collSiteBlogs[] = $obj;
                        }
                    }
                }

                $this->collSiteBlogs = $collSiteBlogs;
                $this->collSiteBlogsPartial = false;
            }
        }

        return $this->collSiteBlogs;
    }

    /**
     * Sets a collection of SiteBlog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteBlogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function setSiteBlogs(Collection $siteBlogs, ConnectionInterface $con = null)
    {
        /** @var SiteBlog[] $siteBlogsToDelete */
        $siteBlogsToDelete = $this->getSiteBlogs(new Criteria(), $con)->diff($siteBlogs);


        $this->siteBlogsScheduledForDeletion = $siteBlogsToDelete;

        foreach ($siteBlogsToDelete as $siteBlogRemoved) {
            $siteBlogRemoved->setUser(null);
        }

        $this->collSiteBlogs = null;
        foreach ($siteBlogs as $siteBlog) {
            $this->addSiteBlog($siteBlog);
        }

        $this->collSiteBlogs = $siteBlogs;
        $this->collSiteBlogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSiteBlog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSiteBlog objects.
     * @throws PropelException
     */
    public function countSiteBlogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteBlogsPartial && !$this->isNew();
        if (null === $this->collSiteBlogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteBlogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteBlogs());
            }

            $query = SiteBlogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUser($this)
                ->count($con);
        }

        return count($this->collSiteBlogs);
    }

    /**
     * Method called to associate a SiteBlog object to this object
     * through the SiteBlog foreign key attribute.
     *
     * @param  SiteBlog $l SiteBlog
     * @return $this|\Model\Account\User The current object (for fluent API support)
     */
    public function addSiteBlog(SiteBlog $l)
    {
        if ($this->collSiteBlogs === null) {
            $this->initSiteBlogs();
            $this->collSiteBlogsPartial = true;
        }

        if (!$this->collSiteBlogs->contains($l)) {
            $this->doAddSiteBlog($l);

            if ($this->siteBlogsScheduledForDeletion and $this->siteBlogsScheduledForDeletion->contains($l)) {
                $this->siteBlogsScheduledForDeletion->remove($this->siteBlogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param SiteBlog $siteBlog The SiteBlog object to add.
     */
    protected function doAddSiteBlog(SiteBlog $siteBlog)
    {
        $this->collSiteBlogs[]= $siteBlog;
        $siteBlog->setUser($this);
    }

    /**
     * @param  SiteBlog $siteBlog The SiteBlog object to remove.
     * @return $this|ChildUser The current object (for fluent API support)
     */
    public function removeSiteBlog(SiteBlog $siteBlog)
    {
        if ($this->getSiteBlogs()->contains($siteBlog)) {
            $pos = $this->collSiteBlogs->search($siteBlog);
            $this->collSiteBlogs->remove($pos);
            if (null === $this->siteBlogsScheduledForDeletion) {
                $this->siteBlogsScheduledForDeletion = clone $this->collSiteBlogs;
                $this->siteBlogsScheduledForDeletion->clear();
            }
            $this->siteBlogsScheduledForDeletion[]= clone $siteBlog;
            $siteBlog->setUser(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related SiteBlogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SiteBlog[] List of SiteBlog objects
     */
    public function getSiteBlogsJoinLanguage(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SiteBlogQuery::create(null, $criteria);
        $query->joinWith('Language', $joinBehavior);

        return $this->getSiteBlogs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this User is new, it will return
     * an empty collection; or if this User has previously
     * been saved, it will retrieve related SiteBlogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in User.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SiteBlog[] List of SiteBlog objects
     */
    public function getSiteBlogsJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SiteBlogQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getSiteBlogs($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aPreferredLanguage) {
            $this->aPreferredLanguage->removeUser($this);
        }
        if (null !== $this->aRole) {
            $this->aRole->removeUser($this);
        }
        $this->id = null;
        $this->is_deleted = null;
        $this->can_change_roles = null;
        $this->role_id = null;
        $this->prefered_language_id = null;
        $this->first_name = null;
        $this->last_name = null;
        $this->email = null;
        $this->phone = null;
        $this->phone_mobile = null;
        $this->website = null;
        $this->twitter = null;
        $this->about_me = null;
        $this->private_email = null;
        $this->password = null;
        $this->salt = null;
        $this->source_network = null;
        $this->created = null;
        $this->modified = null;
        $this->must_change_password = null;
        $this->has_profile_pic = null;
        $this->pic_ext = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collUserObjs) {
                foreach ($this->collUserObjs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUserRegistries) {
                foreach ($this->collUserRegistries as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUsers) {
                foreach ($this->collUsers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collFacebookUsers) {
                foreach ($this->collFacebookUsers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSuppliers) {
                foreach ($this->collSuppliers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProducts) {
                foreach ($this->collProducts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaleOrders) {
                foreach ($this->collSaleOrders as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCustomerNotes) {
                foreach ($this->collCustomerNotes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteBlogs) {
                foreach ($this->collSiteBlogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collUserObjs = null;
        $this->collUserRegistries = null;
        $this->collUsers = null;
        $this->collFacebookUsers = null;
        $this->collSuppliers = null;
        $this->collProducts = null;
        $this->collSaleOrders = null;
        $this->collCustomerNotes = null;
        $this->collSiteBlogs = null;
        $this->aPreferredLanguage = null;
        $this->aRole = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UserTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
