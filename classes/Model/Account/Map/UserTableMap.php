<?php

namespace Model\Account\Map;

use Model\Account\User;
use Model\Account\UserQuery;
use Model\Crm\Map\CustomerNoteTableMap;
use Model\Sale\Map\SaleOrderTableMap;
use Model\Supplier\Map\SupplierTableMap;
use Model\User\Map\UserRegistryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'user' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class UserTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Account.Map.UserTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'user';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Account\\User';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Account.User';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 22;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 22;

    /**
     * the column name for the id field
     */
    const COL_ID = 'user.id';

    /**
     * the column name for the is_deleted field
     */
    const COL_IS_DELETED = 'user.is_deleted';

    /**
     * the column name for the can_change_roles field
     */
    const COL_CAN_CHANGE_ROLES = 'user.can_change_roles';

    /**
     * the column name for the role_id field
     */
    const COL_ROLE_ID = 'user.role_id';

    /**
     * the column name for the prefered_language_id field
     */
    const COL_PREFERED_LANGUAGE_ID = 'user.prefered_language_id';

    /**
     * the column name for the first_name field
     */
    const COL_FIRST_NAME = 'user.first_name';

    /**
     * the column name for the last_name field
     */
    const COL_LAST_NAME = 'user.last_name';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'user.email';

    /**
     * the column name for the phone field
     */
    const COL_PHONE = 'user.phone';

    /**
     * the column name for the phone_mobile field
     */
    const COL_PHONE_MOBILE = 'user.phone_mobile';

    /**
     * the column name for the website field
     */
    const COL_WEBSITE = 'user.website';

    /**
     * the column name for the twitter field
     */
    const COL_TWITTER = 'user.twitter';

    /**
     * the column name for the about_me field
     */
    const COL_ABOUT_ME = 'user.about_me';

    /**
     * the column name for the private_email field
     */
    const COL_PRIVATE_EMAIL = 'user.private_email';

    /**
     * the column name for the password field
     */
    const COL_PASSWORD = 'user.password';

    /**
     * the column name for the salt field
     */
    const COL_SALT = 'user.salt';

    /**
     * the column name for the source_network field
     */
    const COL_SOURCE_NETWORK = 'user.source_network';

    /**
     * the column name for the created field
     */
    const COL_CREATED = 'user.created';

    /**
     * the column name for the modified field
     */
    const COL_MODIFIED = 'user.modified';

    /**
     * the column name for the must_change_password field
     */
    const COL_MUST_CHANGE_PASSWORD = 'user.must_change_password';

    /**
     * the column name for the has_profile_pic field
     */
    const COL_HAS_PROFILE_PIC = 'user.has_profile_pic';

    /**
     * the column name for the pic_ext field
     */
    const COL_PIC_EXT = 'user.pic_ext';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ItemDeleted', 'CanChangeRoles', 'RoleId', 'PreferedLanguageId', 'FirstName', 'LastName', 'Email', 'Phone', 'PhoneMobile', 'Website', 'Twitter', 'AboutMe', 'PrivateEmail', 'Password', 'Salt', 'SourceNetwork', 'Created', 'Modified', 'MustChangePassword', 'HasProfilePic', 'PicExt', ),
        self::TYPE_CAMELNAME     => array('id', 'itemDeleted', 'canChangeRoles', 'roleId', 'preferedLanguageId', 'firstName', 'lastName', 'email', 'phone', 'phoneMobile', 'website', 'twitter', 'aboutMe', 'privateEmail', 'password', 'salt', 'sourceNetwork', 'created', 'modified', 'mustChangePassword', 'hasProfilePic', 'picExt', ),
        self::TYPE_COLNAME       => array(UserTableMap::COL_ID, UserTableMap::COL_IS_DELETED, UserTableMap::COL_CAN_CHANGE_ROLES, UserTableMap::COL_ROLE_ID, UserTableMap::COL_PREFERED_LANGUAGE_ID, UserTableMap::COL_FIRST_NAME, UserTableMap::COL_LAST_NAME, UserTableMap::COL_EMAIL, UserTableMap::COL_PHONE, UserTableMap::COL_PHONE_MOBILE, UserTableMap::COL_WEBSITE, UserTableMap::COL_TWITTER, UserTableMap::COL_ABOUT_ME, UserTableMap::COL_PRIVATE_EMAIL, UserTableMap::COL_PASSWORD, UserTableMap::COL_SALT, UserTableMap::COL_SOURCE_NETWORK, UserTableMap::COL_CREATED, UserTableMap::COL_MODIFIED, UserTableMap::COL_MUST_CHANGE_PASSWORD, UserTableMap::COL_HAS_PROFILE_PIC, UserTableMap::COL_PIC_EXT, ),
        self::TYPE_FIELDNAME     => array('id', 'is_deleted', 'can_change_roles', 'role_id', 'prefered_language_id', 'first_name', 'last_name', 'email', 'phone', 'phone_mobile', 'website', 'twitter', 'about_me', 'private_email', 'password', 'salt', 'source_network', 'created', 'modified', 'must_change_password', 'has_profile_pic', 'pic_ext', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ItemDeleted' => 1, 'CanChangeRoles' => 2, 'RoleId' => 3, 'PreferedLanguageId' => 4, 'FirstName' => 5, 'LastName' => 6, 'Email' => 7, 'Phone' => 8, 'PhoneMobile' => 9, 'Website' => 10, 'Twitter' => 11, 'AboutMe' => 12, 'PrivateEmail' => 13, 'Password' => 14, 'Salt' => 15, 'SourceNetwork' => 16, 'Created' => 17, 'Modified' => 18, 'MustChangePassword' => 19, 'HasProfilePic' => 20, 'PicExt' => 21, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'itemDeleted' => 1, 'canChangeRoles' => 2, 'roleId' => 3, 'preferedLanguageId' => 4, 'firstName' => 5, 'lastName' => 6, 'email' => 7, 'phone' => 8, 'phoneMobile' => 9, 'website' => 10, 'twitter' => 11, 'aboutMe' => 12, 'privateEmail' => 13, 'password' => 14, 'salt' => 15, 'sourceNetwork' => 16, 'created' => 17, 'modified' => 18, 'mustChangePassword' => 19, 'hasProfilePic' => 20, 'picExt' => 21, ),
        self::TYPE_COLNAME       => array(UserTableMap::COL_ID => 0, UserTableMap::COL_IS_DELETED => 1, UserTableMap::COL_CAN_CHANGE_ROLES => 2, UserTableMap::COL_ROLE_ID => 3, UserTableMap::COL_PREFERED_LANGUAGE_ID => 4, UserTableMap::COL_FIRST_NAME => 5, UserTableMap::COL_LAST_NAME => 6, UserTableMap::COL_EMAIL => 7, UserTableMap::COL_PHONE => 8, UserTableMap::COL_PHONE_MOBILE => 9, UserTableMap::COL_WEBSITE => 10, UserTableMap::COL_TWITTER => 11, UserTableMap::COL_ABOUT_ME => 12, UserTableMap::COL_PRIVATE_EMAIL => 13, UserTableMap::COL_PASSWORD => 14, UserTableMap::COL_SALT => 15, UserTableMap::COL_SOURCE_NETWORK => 16, UserTableMap::COL_CREATED => 17, UserTableMap::COL_MODIFIED => 18, UserTableMap::COL_MUST_CHANGE_PASSWORD => 19, UserTableMap::COL_HAS_PROFILE_PIC => 20, UserTableMap::COL_PIC_EXT => 21, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'is_deleted' => 1, 'can_change_roles' => 2, 'role_id' => 3, 'prefered_language_id' => 4, 'first_name' => 5, 'last_name' => 6, 'email' => 7, 'phone' => 8, 'phone_mobile' => 9, 'website' => 10, 'twitter' => 11, 'about_me' => 12, 'private_email' => 13, 'password' => 14, 'salt' => 15, 'source_network' => 16, 'created' => 17, 'modified' => 18, 'must_change_password' => 19, 'has_profile_pic' => 20, 'pic_ext' => 21, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user');
        $this->setPhpName('User');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Account\\User');
        $this->setPackage('Model.Account');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('is_deleted', 'ItemDeleted', 'BOOLEAN', false, 1, false);
        $this->addColumn('can_change_roles', 'CanChangeRoles', 'BOOLEAN', true, 1, false);
        $this->addForeignKey('role_id', 'RoleId', 'INTEGER', 'role', 'id', true, null, null);
        $this->addForeignKey('prefered_language_id', 'PreferedLanguageId', 'INTEGER', 'mt_language', 'id', false, null, null);
        $this->addColumn('first_name', 'FirstName', 'VARCHAR', true, 255, null);
        $this->addColumn('last_name', 'LastName', 'VARCHAR', true, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 150, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', false, 255, null);
        $this->addColumn('phone_mobile', 'PhoneMobile', 'VARCHAR', false, 255, null);
        $this->addColumn('website', 'Website', 'VARCHAR', false, 255, null);
        $this->addColumn('twitter', 'Twitter', 'VARCHAR', false, 255, null);
        $this->addColumn('about_me', 'AboutMe', 'VARCHAR', false, 255, null);
        $this->addColumn('private_email', 'PrivateEmail', 'VARCHAR', false, 255, null);
        $this->addColumn('password', 'Password', 'VARCHAR', false, 256, null);
        $this->addColumn('salt', 'Salt', 'VARCHAR', false, 25, null);
        $this->addColumn('source_network', 'SourceNetwork', 'VARCHAR', false, 255, null);
        $this->addColumn('created', 'Created', 'DATE', false, null, null);
        $this->addColumn('modified', 'Modified', 'DATE', false, null, null);
        $this->addColumn('must_change_password', 'MustChangePassword', 'BOOLEAN', false, 1, false);
        $this->addColumn('has_profile_pic', 'HasProfilePic', 'BOOLEAN', false, 1, false);
        $this->addColumn('pic_ext', 'PicExt', 'VARCHAR', false, 25, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PreferredLanguage', '\\Model\\Setting\\MasterTable\\Language', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':prefered_language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('Role', '\\Model\\Setting\\MasterTable\\Role', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':role_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('UserObj', '\\Model\\User\\UserActivity', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'UserObjs', false);
        $this->addRelation('UserRegistry', '\\Model\\User\\UserRegistry', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'UserRegistries', false);
        $this->addRelation('User', '\\Model\\Crm\\UserLinkedIn', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'Users', false);
        $this->addRelation('FacebookUser', '\\Model\\Account\\FacebookUser', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'FacebookUsers', false);
        $this->addRelation('Supplier', '\\Model\\Supplier\\Supplier', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':account_manager_id',
    1 => ':id',
  ),
), 'SET NULL', null, 'Suppliers', false);
        $this->addRelation('Product', '\\Model\\Product', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':advertiser_user_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'Products', false);
        $this->addRelation('SaleOrder', '\\Model\\Sale\\SaleOrder', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':created_by_user_id',
    1 => ':id',
  ),
), 'SET NULL', null, 'SaleOrders', false);
        $this->addRelation('CustomerNote', '\\Model\\Crm\\CustomerNote', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':created_by_user_id',
    1 => ':id',
  ),
), 'SET NULL', null, 'CustomerNotes', false);
        $this->addRelation('SiteBlog', '\\Model\\Cms\\SiteBlog', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':created_by_user_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'SiteBlogs', false);
    } // buildRelations()
    /**
     * Method to invalidate the instance pool of all tables related to user     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        UserRegistryTableMap::clearInstancePool();
        SupplierTableMap::clearInstancePool();
        SaleOrderTableMap::clearInstancePool();
        CustomerNoteTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UserTableMap::CLASS_DEFAULT : UserTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (User object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UserTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UserTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UserTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UserTableMap::OM_CLASS;
            /** @var User $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UserTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UserTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UserTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var User $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UserTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UserTableMap::COL_ID);
            $criteria->addSelectColumn(UserTableMap::COL_IS_DELETED);
            $criteria->addSelectColumn(UserTableMap::COL_CAN_CHANGE_ROLES);
            $criteria->addSelectColumn(UserTableMap::COL_ROLE_ID);
            $criteria->addSelectColumn(UserTableMap::COL_PREFERED_LANGUAGE_ID);
            $criteria->addSelectColumn(UserTableMap::COL_FIRST_NAME);
            $criteria->addSelectColumn(UserTableMap::COL_LAST_NAME);
            $criteria->addSelectColumn(UserTableMap::COL_EMAIL);
            $criteria->addSelectColumn(UserTableMap::COL_PHONE);
            $criteria->addSelectColumn(UserTableMap::COL_PHONE_MOBILE);
            $criteria->addSelectColumn(UserTableMap::COL_WEBSITE);
            $criteria->addSelectColumn(UserTableMap::COL_TWITTER);
            $criteria->addSelectColumn(UserTableMap::COL_ABOUT_ME);
            $criteria->addSelectColumn(UserTableMap::COL_PRIVATE_EMAIL);
            $criteria->addSelectColumn(UserTableMap::COL_PASSWORD);
            $criteria->addSelectColumn(UserTableMap::COL_SALT);
            $criteria->addSelectColumn(UserTableMap::COL_SOURCE_NETWORK);
            $criteria->addSelectColumn(UserTableMap::COL_CREATED);
            $criteria->addSelectColumn(UserTableMap::COL_MODIFIED);
            $criteria->addSelectColumn(UserTableMap::COL_MUST_CHANGE_PASSWORD);
            $criteria->addSelectColumn(UserTableMap::COL_HAS_PROFILE_PIC);
            $criteria->addSelectColumn(UserTableMap::COL_PIC_EXT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.is_deleted');
            $criteria->addSelectColumn($alias . '.can_change_roles');
            $criteria->addSelectColumn($alias . '.role_id');
            $criteria->addSelectColumn($alias . '.prefered_language_id');
            $criteria->addSelectColumn($alias . '.first_name');
            $criteria->addSelectColumn($alias . '.last_name');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.phone_mobile');
            $criteria->addSelectColumn($alias . '.website');
            $criteria->addSelectColumn($alias . '.twitter');
            $criteria->addSelectColumn($alias . '.about_me');
            $criteria->addSelectColumn($alias . '.private_email');
            $criteria->addSelectColumn($alias . '.password');
            $criteria->addSelectColumn($alias . '.salt');
            $criteria->addSelectColumn($alias . '.source_network');
            $criteria->addSelectColumn($alias . '.created');
            $criteria->addSelectColumn($alias . '.modified');
            $criteria->addSelectColumn($alias . '.must_change_password');
            $criteria->addSelectColumn($alias . '.has_profile_pic');
            $criteria->addSelectColumn($alias . '.pic_ext');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UserTableMap::DATABASE_NAME)->getTable(UserTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UserTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UserTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UserTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a User or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or User object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Account\User) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UserTableMap::DATABASE_NAME);
            $criteria->add(UserTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = UserQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UserTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UserTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the user table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UserQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a User or Criteria object.
     *
     * @param mixed               $criteria Criteria or User object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from User object
        }

        if ($criteria->containsKey(UserTableMap::COL_ID) && $criteria->keyContainsValue(UserTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UserTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = UserQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UserTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UserTableMap::buildTableMap();
