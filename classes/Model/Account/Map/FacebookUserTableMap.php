<?php

namespace Model\Account\Map;

use Model\Account\FacebookUser;
use Model\Account\FacebookUserQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'facebook_user' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class FacebookUserTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Account.Map.FacebookUserTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'facebook_user';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Account\\FacebookUser';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Account.FacebookUser';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id field
     */
    const COL_ID = 'facebook_user.id';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'facebook_user.user_id';

    /**
     * the column name for the facebook_id field
     */
    const COL_FACEBOOK_ID = 'facebook_user.facebook_id';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'facebook_user.email';

    /**
     * the column name for the first_name field
     */
    const COL_FIRST_NAME = 'facebook_user.first_name';

    /**
     * the column name for the last_name field
     */
    const COL_LAST_NAME = 'facebook_user.last_name';

    /**
     * the column name for the gender field
     */
    const COL_GENDER = 'facebook_user.gender';

    /**
     * the column name for the link field
     */
    const COL_LINK = 'facebook_user.link';

    /**
     * the column name for the created field
     */
    const COL_CREATED = 'facebook_user.created';

    /**
     * the column name for the modified field
     */
    const COL_MODIFIED = 'facebook_user.modified';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'UserId', 'FacebookId', 'Email', 'FirstName', 'LastName', 'Gender', 'Link', 'Created', 'Modified', ),
        self::TYPE_CAMELNAME     => array('id', 'userId', 'facebookId', 'email', 'firstName', 'lastName', 'gender', 'link', 'created', 'modified', ),
        self::TYPE_COLNAME       => array(FacebookUserTableMap::COL_ID, FacebookUserTableMap::COL_USER_ID, FacebookUserTableMap::COL_FACEBOOK_ID, FacebookUserTableMap::COL_EMAIL, FacebookUserTableMap::COL_FIRST_NAME, FacebookUserTableMap::COL_LAST_NAME, FacebookUserTableMap::COL_GENDER, FacebookUserTableMap::COL_LINK, FacebookUserTableMap::COL_CREATED, FacebookUserTableMap::COL_MODIFIED, ),
        self::TYPE_FIELDNAME     => array('id', 'user_id', 'facebook_id', 'email', 'first_name', 'last_name', 'gender', 'link', 'created', 'modified', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'UserId' => 1, 'FacebookId' => 2, 'Email' => 3, 'FirstName' => 4, 'LastName' => 5, 'Gender' => 6, 'Link' => 7, 'Created' => 8, 'Modified' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'userId' => 1, 'facebookId' => 2, 'email' => 3, 'firstName' => 4, 'lastName' => 5, 'gender' => 6, 'link' => 7, 'created' => 8, 'modified' => 9, ),
        self::TYPE_COLNAME       => array(FacebookUserTableMap::COL_ID => 0, FacebookUserTableMap::COL_USER_ID => 1, FacebookUserTableMap::COL_FACEBOOK_ID => 2, FacebookUserTableMap::COL_EMAIL => 3, FacebookUserTableMap::COL_FIRST_NAME => 4, FacebookUserTableMap::COL_LAST_NAME => 5, FacebookUserTableMap::COL_GENDER => 6, FacebookUserTableMap::COL_LINK => 7, FacebookUserTableMap::COL_CREATED => 8, FacebookUserTableMap::COL_MODIFIED => 9, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'user_id' => 1, 'facebook_id' => 2, 'email' => 3, 'first_name' => 4, 'last_name' => 5, 'gender' => 6, 'link' => 7, 'created' => 8, 'modified' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('facebook_user');
        $this->setPhpName('FacebookUser');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Account\\FacebookUser');
        $this->setPackage('Model.Account');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'user', 'id', false, null, null);
        $this->addColumn('facebook_id', 'FacebookId', 'VARCHAR', true, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 100, null);
        $this->addColumn('first_name', 'FirstName', 'VARCHAR', true, 255, null);
        $this->addColumn('last_name', 'LastName', 'VARCHAR', true, 255, null);
        $this->addColumn('gender', 'Gender', 'VARCHAR', true, 255, null);
        $this->addColumn('link', 'Link', 'VARCHAR', true, 255, null);
        $this->addColumn('created', 'Created', 'DATE', true, null, null);
        $this->addColumn('modified', 'Modified', 'DATE', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('User', '\\Model\\Account\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? FacebookUserTableMap::CLASS_DEFAULT : FacebookUserTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (FacebookUser object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = FacebookUserTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = FacebookUserTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + FacebookUserTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = FacebookUserTableMap::OM_CLASS;
            /** @var FacebookUser $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            FacebookUserTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = FacebookUserTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = FacebookUserTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var FacebookUser $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                FacebookUserTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(FacebookUserTableMap::COL_ID);
            $criteria->addSelectColumn(FacebookUserTableMap::COL_USER_ID);
            $criteria->addSelectColumn(FacebookUserTableMap::COL_FACEBOOK_ID);
            $criteria->addSelectColumn(FacebookUserTableMap::COL_EMAIL);
            $criteria->addSelectColumn(FacebookUserTableMap::COL_FIRST_NAME);
            $criteria->addSelectColumn(FacebookUserTableMap::COL_LAST_NAME);
            $criteria->addSelectColumn(FacebookUserTableMap::COL_GENDER);
            $criteria->addSelectColumn(FacebookUserTableMap::COL_LINK);
            $criteria->addSelectColumn(FacebookUserTableMap::COL_CREATED);
            $criteria->addSelectColumn(FacebookUserTableMap::COL_MODIFIED);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.facebook_id');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.first_name');
            $criteria->addSelectColumn($alias . '.last_name');
            $criteria->addSelectColumn($alias . '.gender');
            $criteria->addSelectColumn($alias . '.link');
            $criteria->addSelectColumn($alias . '.created');
            $criteria->addSelectColumn($alias . '.modified');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(FacebookUserTableMap::DATABASE_NAME)->getTable(FacebookUserTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(FacebookUserTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(FacebookUserTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new FacebookUserTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a FacebookUser or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or FacebookUser object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacebookUserTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Account\FacebookUser) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(FacebookUserTableMap::DATABASE_NAME);
            $criteria->add(FacebookUserTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = FacebookUserQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            FacebookUserTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                FacebookUserTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the facebook_user table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return FacebookUserQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a FacebookUser or Criteria object.
     *
     * @param mixed               $criteria Criteria or FacebookUser object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(FacebookUserTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from FacebookUser object
        }

        if ($criteria->containsKey(FacebookUserTableMap::COL_ID) && $criteria->keyContainsValue(FacebookUserTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.FacebookUserTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = FacebookUserQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // FacebookUserTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
FacebookUserTableMap::buildTableMap();
