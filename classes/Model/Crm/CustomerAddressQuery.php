<?php

namespace Model\Crm;

use Exception\LogicException;
use Model\Crm\Base\CustomerAddressQuery as BaseCustomerAddressQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'customer_address' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerAddressQuery extends BaseCustomerAddressQuery
{

    function getByCustomerIdTypeCode($iCustomerId, $sTypeCode):CustomerAddressQuery
    {
        $aAllowedTypes = ['delivery', 'invoice', 'general'];
        if(!in_array($sTypeCode, $aAllowedTypes))
        {
            throw new LogicException("Type code must be one of ".join(', ', $aAllowedTypes).' got '.$sTypeCode);
        }
        $oCustomerAddressType = CustomerAddressTypeQuery::create()->findOneByCode($sTypeCode);
        $oCustomerAddressQuery = self::create();
        return $oCustomerAddressQuery->filterByCustomerId($iCustomerId)->filterByCustomerAddressTypeId($oCustomerAddressType->getId());
    }
}
