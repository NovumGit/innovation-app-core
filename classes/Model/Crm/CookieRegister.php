<?php

namespace Model\Crm;

use Model\Crm\Base\CookieRegister as BaseCookieRegister;

/**
 * Skeleton subclass for representing a row from the 'cookie_register' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CookieRegister extends BaseCookieRegister
{

}
