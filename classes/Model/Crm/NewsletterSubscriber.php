<?php

namespace Model\Crm;

use Model\Crm\Base\NewsletterSubscriber as BaseNewsletterSubscriber;

/**
 * Skeleton subclass for representing a row from the 'newsletter_subscriber' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class NewsletterSubscriber extends BaseNewsletterSubscriber
{

}
