<?php

namespace Model\Crm;

use Model\Crm\Base\CustomerSectorQuery as BaseCustomerSectorQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'mt_customer_sector' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerSectorQuery extends BaseCustomerSectorQuery
{

}
