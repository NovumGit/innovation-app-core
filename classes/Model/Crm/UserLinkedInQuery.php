<?php

namespace Model\Crm;

use Model\Crm\Base\UserLinkedInQuery as BaseUserLinkedInQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'user_linkedin' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UserLinkedInQuery extends BaseUserLinkedInQuery
{

}
