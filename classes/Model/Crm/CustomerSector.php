<?php

namespace Model\Crm;

use Model\Crm\Base\CustomerSector as BaseCustomerSector;

/**
 * Skeleton subclass for representing a row from the 'mt_customer_sector' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerSector extends BaseCustomerSector
{

}
