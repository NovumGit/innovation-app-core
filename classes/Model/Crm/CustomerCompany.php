<?php

namespace Model\Crm;

use Core\Utils;
use Model\Crm\Base\CustomerCompany as BaseCustomerCompany;

/**
 * Skeleton subclass for representing a row from the 'customer_company' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerCompany extends BaseCustomerCompany
{

    function getCustomerUrl()
    {
        return '/'.Utils::slugify($this->getCompanyName()).'-'.$this->getCustomerId();
    }
}
