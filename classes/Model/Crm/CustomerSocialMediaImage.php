<?php

namespace Model\Crm;

use Model\Crm\Base\CustomerSocialMediaImage as BaseCustomerSocialMediaImage;

/**
 * Skeleton subclass for representing a row from the 'customer_social_media_image' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerSocialMediaImage extends BaseCustomerSocialMediaImage
{
    function getUrl()
    {
        return '/img/customer/'.$this->getCustomerId().'/'.$this->getSocialNetwork().'_'.$this->getImageLocation().'.'.$this->getFileExt();
    }

}
