<?php

namespace Model\Crm\Base;

use \Exception;
use \PDO;
use Model\ContactMessage;
use Model\Category\CustomerWishlist;
use Model\Crm\Customer as ChildCustomer;
use Model\Crm\CustomerQuery as ChildCustomerQuery;
use Model\Crm\Map\CustomerTableMap;
use Model\Finance\SaleInvoice;
use Model\Setting\MasterTable\Branche;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\PayTerm;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'customer' table.
 *
 *
 *
 * @method     ChildCustomerQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCustomerQuery orderByImportSource($order = Criteria::ASC) Order by the import_source column
 * @method     ChildCustomerQuery orderByAccountingId($order = Criteria::ASC) Order by the accounting_id column
 * @method     ChildCustomerQuery orderByOldId($order = Criteria::ASC) Order by the old_id column
 * @method     ChildCustomerQuery orderByIp($order = Criteria::ASC) Order by the ip column
 * @method     ChildCustomerQuery orderByCreatedOn($order = Criteria::ASC) Order by the created_on column
 * @method     ChildCustomerQuery orderByItemDeleted($order = Criteria::ASC) Order by the is_deleted column
 * @method     ChildCustomerQuery orderByIsNewsletterSubscriber($order = Criteria::ASC) Order by the is_newsletter_subscriber column
 * @method     ChildCustomerQuery orderByLanguageId($order = Criteria::ASC) Order by the language_id column
 * @method     ChildCustomerQuery orderByIsIntracommunautair($order = Criteria::ASC) Order by the is_intracommunautair column
 * @method     ChildCustomerQuery orderByPaytermId($order = Criteria::ASC) Order by the payterm_id column
 * @method     ChildCustomerQuery orderByBrancheId($order = Criteria::ASC) Order by the branche_id column
 * @method     ChildCustomerQuery orderBySectorId($order = Criteria::ASC) Order by the sector_id column
 * @method     ChildCustomerQuery orderByCustomerTypeId($order = Criteria::ASC) Order by the customer_type_id column
 * @method     ChildCustomerQuery orderByDebitor($order = Criteria::ASC) Order by the debitor column
 * @method     ChildCustomerQuery orderByGender($order = Criteria::ASC) Order by the gender column
 * @method     ChildCustomerQuery orderByFirstName($order = Criteria::ASC) Order by the first_name column
 * @method     ChildCustomerQuery orderByInsertion($order = Criteria::ASC) Order by the insertion column
 * @method     ChildCustomerQuery orderByLastName($order = Criteria::ASC) Order by the last_name column
 * @method     ChildCustomerQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildCustomerQuery orderByPassword($order = Criteria::ASC) Order by the password column
 * @method     ChildCustomerQuery orderBySalt($order = Criteria::ASC) Order by the salt column
 * @method     ChildCustomerQuery orderByOncePasswordReset($order = Criteria::ASC) Order by the once_password_reset column
 * @method     ChildCustomerQuery orderByIsWholesale($order = Criteria::ASC) Order by the is_wholesale column
 * @method     ChildCustomerQuery orderByIsWholesaleApproved($order = Criteria::ASC) Order by the is_wholesale_approved column
 * @method     ChildCustomerQuery orderByCanBuyOnCredit($order = Criteria::ASC) Order by the can_buy_on_credit column
 * @method     ChildCustomerQuery orderByIban($order = Criteria::ASC) Order by the iban column
 * @method     ChildCustomerQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildCustomerQuery orderByMobile($order = Criteria::ASC) Order by the mobile column
 * @method     ChildCustomerQuery orderByFax($order = Criteria::ASC) Order by the fax column
 * @method     ChildCustomerQuery orderByWebsite($order = Criteria::ASC) Order by the website column
 * @method     ChildCustomerQuery orderByTag($order = Criteria::ASC) Order by the tag column
 * @method     ChildCustomerQuery orderByIsVerified($order = Criteria::ASC) Order by the is_verified column
 * @method     ChildCustomerQuery orderByVerificationKey($order = Criteria::ASC) Order by the verification_key column
 *
 * @method     ChildCustomerQuery groupById() Group by the id column
 * @method     ChildCustomerQuery groupByImportSource() Group by the import_source column
 * @method     ChildCustomerQuery groupByAccountingId() Group by the accounting_id column
 * @method     ChildCustomerQuery groupByOldId() Group by the old_id column
 * @method     ChildCustomerQuery groupByIp() Group by the ip column
 * @method     ChildCustomerQuery groupByCreatedOn() Group by the created_on column
 * @method     ChildCustomerQuery groupByItemDeleted() Group by the is_deleted column
 * @method     ChildCustomerQuery groupByIsNewsletterSubscriber() Group by the is_newsletter_subscriber column
 * @method     ChildCustomerQuery groupByLanguageId() Group by the language_id column
 * @method     ChildCustomerQuery groupByIsIntracommunautair() Group by the is_intracommunautair column
 * @method     ChildCustomerQuery groupByPaytermId() Group by the payterm_id column
 * @method     ChildCustomerQuery groupByBrancheId() Group by the branche_id column
 * @method     ChildCustomerQuery groupBySectorId() Group by the sector_id column
 * @method     ChildCustomerQuery groupByCustomerTypeId() Group by the customer_type_id column
 * @method     ChildCustomerQuery groupByDebitor() Group by the debitor column
 * @method     ChildCustomerQuery groupByGender() Group by the gender column
 * @method     ChildCustomerQuery groupByFirstName() Group by the first_name column
 * @method     ChildCustomerQuery groupByInsertion() Group by the insertion column
 * @method     ChildCustomerQuery groupByLastName() Group by the last_name column
 * @method     ChildCustomerQuery groupByEmail() Group by the email column
 * @method     ChildCustomerQuery groupByPassword() Group by the password column
 * @method     ChildCustomerQuery groupBySalt() Group by the salt column
 * @method     ChildCustomerQuery groupByOncePasswordReset() Group by the once_password_reset column
 * @method     ChildCustomerQuery groupByIsWholesale() Group by the is_wholesale column
 * @method     ChildCustomerQuery groupByIsWholesaleApproved() Group by the is_wholesale_approved column
 * @method     ChildCustomerQuery groupByCanBuyOnCredit() Group by the can_buy_on_credit column
 * @method     ChildCustomerQuery groupByIban() Group by the iban column
 * @method     ChildCustomerQuery groupByPhone() Group by the phone column
 * @method     ChildCustomerQuery groupByMobile() Group by the mobile column
 * @method     ChildCustomerQuery groupByFax() Group by the fax column
 * @method     ChildCustomerQuery groupByWebsite() Group by the website column
 * @method     ChildCustomerQuery groupByTag() Group by the tag column
 * @method     ChildCustomerQuery groupByIsVerified() Group by the is_verified column
 * @method     ChildCustomerQuery groupByVerificationKey() Group by the verification_key column
 *
 * @method     ChildCustomerQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCustomerQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCustomerQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCustomerQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCustomerQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCustomerQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCustomerQuery leftJoinLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Language relation
 * @method     ChildCustomerQuery rightJoinLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Language relation
 * @method     ChildCustomerQuery innerJoinLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the Language relation
 *
 * @method     ChildCustomerQuery joinWithLanguage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Language relation
 *
 * @method     ChildCustomerQuery leftJoinWithLanguage() Adds a LEFT JOIN clause and with to the query using the Language relation
 * @method     ChildCustomerQuery rightJoinWithLanguage() Adds a RIGHT JOIN clause and with to the query using the Language relation
 * @method     ChildCustomerQuery innerJoinWithLanguage() Adds a INNER JOIN clause and with to the query using the Language relation
 *
 * @method     ChildCustomerQuery leftJoinBranche($relationAlias = null) Adds a LEFT JOIN clause to the query using the Branche relation
 * @method     ChildCustomerQuery rightJoinBranche($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Branche relation
 * @method     ChildCustomerQuery innerJoinBranche($relationAlias = null) Adds a INNER JOIN clause to the query using the Branche relation
 *
 * @method     ChildCustomerQuery joinWithBranche($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Branche relation
 *
 * @method     ChildCustomerQuery leftJoinWithBranche() Adds a LEFT JOIN clause and with to the query using the Branche relation
 * @method     ChildCustomerQuery rightJoinWithBranche() Adds a RIGHT JOIN clause and with to the query using the Branche relation
 * @method     ChildCustomerQuery innerJoinWithBranche() Adds a INNER JOIN clause and with to the query using the Branche relation
 *
 * @method     ChildCustomerQuery leftJoinCustomerSector($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomerSector relation
 * @method     ChildCustomerQuery rightJoinCustomerSector($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomerSector relation
 * @method     ChildCustomerQuery innerJoinCustomerSector($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomerSector relation
 *
 * @method     ChildCustomerQuery joinWithCustomerSector($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomerSector relation
 *
 * @method     ChildCustomerQuery leftJoinWithCustomerSector() Adds a LEFT JOIN clause and with to the query using the CustomerSector relation
 * @method     ChildCustomerQuery rightJoinWithCustomerSector() Adds a RIGHT JOIN clause and with to the query using the CustomerSector relation
 * @method     ChildCustomerQuery innerJoinWithCustomerSector() Adds a INNER JOIN clause and with to the query using the CustomerSector relation
 *
 * @method     ChildCustomerQuery leftJoinPayTerm($relationAlias = null) Adds a LEFT JOIN clause to the query using the PayTerm relation
 * @method     ChildCustomerQuery rightJoinPayTerm($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PayTerm relation
 * @method     ChildCustomerQuery innerJoinPayTerm($relationAlias = null) Adds a INNER JOIN clause to the query using the PayTerm relation
 *
 * @method     ChildCustomerQuery joinWithPayTerm($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PayTerm relation
 *
 * @method     ChildCustomerQuery leftJoinWithPayTerm() Adds a LEFT JOIN clause and with to the query using the PayTerm relation
 * @method     ChildCustomerQuery rightJoinWithPayTerm() Adds a RIGHT JOIN clause and with to the query using the PayTerm relation
 * @method     ChildCustomerQuery innerJoinWithPayTerm() Adds a INNER JOIN clause and with to the query using the PayTerm relation
 *
 * @method     ChildCustomerQuery leftJoinCustomerType($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomerType relation
 * @method     ChildCustomerQuery rightJoinCustomerType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomerType relation
 * @method     ChildCustomerQuery innerJoinCustomerType($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomerType relation
 *
 * @method     ChildCustomerQuery joinWithCustomerType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomerType relation
 *
 * @method     ChildCustomerQuery leftJoinWithCustomerType() Adds a LEFT JOIN clause and with to the query using the CustomerType relation
 * @method     ChildCustomerQuery rightJoinWithCustomerType() Adds a RIGHT JOIN clause and with to the query using the CustomerType relation
 * @method     ChildCustomerQuery innerJoinWithCustomerType() Adds a INNER JOIN clause and with to the query using the CustomerType relation
 *
 * @method     ChildCustomerQuery leftJoinContactMessage($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContactMessage relation
 * @method     ChildCustomerQuery rightJoinContactMessage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContactMessage relation
 * @method     ChildCustomerQuery innerJoinContactMessage($relationAlias = null) Adds a INNER JOIN clause to the query using the ContactMessage relation
 *
 * @method     ChildCustomerQuery joinWithContactMessage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContactMessage relation
 *
 * @method     ChildCustomerQuery leftJoinWithContactMessage() Adds a LEFT JOIN clause and with to the query using the ContactMessage relation
 * @method     ChildCustomerQuery rightJoinWithContactMessage() Adds a RIGHT JOIN clause and with to the query using the ContactMessage relation
 * @method     ChildCustomerQuery innerJoinWithContactMessage() Adds a INNER JOIN clause and with to the query using the ContactMessage relation
 *
 * @method     ChildCustomerQuery leftJoinNewsletterSubscriber($relationAlias = null) Adds a LEFT JOIN clause to the query using the NewsletterSubscriber relation
 * @method     ChildCustomerQuery rightJoinNewsletterSubscriber($relationAlias = null) Adds a RIGHT JOIN clause to the query using the NewsletterSubscriber relation
 * @method     ChildCustomerQuery innerJoinNewsletterSubscriber($relationAlias = null) Adds a INNER JOIN clause to the query using the NewsletterSubscriber relation
 *
 * @method     ChildCustomerQuery joinWithNewsletterSubscriber($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the NewsletterSubscriber relation
 *
 * @method     ChildCustomerQuery leftJoinWithNewsletterSubscriber() Adds a LEFT JOIN clause and with to the query using the NewsletterSubscriber relation
 * @method     ChildCustomerQuery rightJoinWithNewsletterSubscriber() Adds a RIGHT JOIN clause and with to the query using the NewsletterSubscriber relation
 * @method     ChildCustomerQuery innerJoinWithNewsletterSubscriber() Adds a INNER JOIN clause and with to the query using the NewsletterSubscriber relation
 *
 * @method     ChildCustomerQuery leftJoinCustomerCompany($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomerCompany relation
 * @method     ChildCustomerQuery rightJoinCustomerCompany($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomerCompany relation
 * @method     ChildCustomerQuery innerJoinCustomerCompany($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomerCompany relation
 *
 * @method     ChildCustomerQuery joinWithCustomerCompany($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomerCompany relation
 *
 * @method     ChildCustomerQuery leftJoinWithCustomerCompany() Adds a LEFT JOIN clause and with to the query using the CustomerCompany relation
 * @method     ChildCustomerQuery rightJoinWithCustomerCompany() Adds a RIGHT JOIN clause and with to the query using the CustomerCompany relation
 * @method     ChildCustomerQuery innerJoinWithCustomerCompany() Adds a INNER JOIN clause and with to the query using the CustomerCompany relation
 *
 * @method     ChildCustomerQuery leftJoinCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Customer relation
 * @method     ChildCustomerQuery rightJoinCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Customer relation
 * @method     ChildCustomerQuery innerJoinCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the Customer relation
 *
 * @method     ChildCustomerQuery joinWithCustomer($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Customer relation
 *
 * @method     ChildCustomerQuery leftJoinWithCustomer() Adds a LEFT JOIN clause and with to the query using the Customer relation
 * @method     ChildCustomerQuery rightJoinWithCustomer() Adds a RIGHT JOIN clause and with to the query using the Customer relation
 * @method     ChildCustomerQuery innerJoinWithCustomer() Adds a INNER JOIN clause and with to the query using the Customer relation
 *
 * @method     ChildCustomerQuery leftJoinCustomerNote($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomerNote relation
 * @method     ChildCustomerQuery rightJoinCustomerNote($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomerNote relation
 * @method     ChildCustomerQuery innerJoinCustomerNote($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomerNote relation
 *
 * @method     ChildCustomerQuery joinWithCustomerNote($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomerNote relation
 *
 * @method     ChildCustomerQuery leftJoinWithCustomerNote() Adds a LEFT JOIN clause and with to the query using the CustomerNote relation
 * @method     ChildCustomerQuery rightJoinWithCustomerNote() Adds a RIGHT JOIN clause and with to the query using the CustomerNote relation
 * @method     ChildCustomerQuery innerJoinWithCustomerNote() Adds a INNER JOIN clause and with to the query using the CustomerNote relation
 *
 * @method     ChildCustomerQuery leftJoinCustomerProperty($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomerProperty relation
 * @method     ChildCustomerQuery rightJoinCustomerProperty($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomerProperty relation
 * @method     ChildCustomerQuery innerJoinCustomerProperty($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomerProperty relation
 *
 * @method     ChildCustomerQuery joinWithCustomerProperty($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomerProperty relation
 *
 * @method     ChildCustomerQuery leftJoinWithCustomerProperty() Adds a LEFT JOIN clause and with to the query using the CustomerProperty relation
 * @method     ChildCustomerQuery rightJoinWithCustomerProperty() Adds a RIGHT JOIN clause and with to the query using the CustomerProperty relation
 * @method     ChildCustomerQuery innerJoinWithCustomerProperty() Adds a INNER JOIN clause and with to the query using the CustomerProperty relation
 *
 * @method     ChildCustomerQuery leftJoinCustomerWishlist($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomerWishlist relation
 * @method     ChildCustomerQuery rightJoinCustomerWishlist($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomerWishlist relation
 * @method     ChildCustomerQuery innerJoinCustomerWishlist($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomerWishlist relation
 *
 * @method     ChildCustomerQuery joinWithCustomerWishlist($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomerWishlist relation
 *
 * @method     ChildCustomerQuery leftJoinWithCustomerWishlist() Adds a LEFT JOIN clause and with to the query using the CustomerWishlist relation
 * @method     ChildCustomerQuery rightJoinWithCustomerWishlist() Adds a RIGHT JOIN clause and with to the query using the CustomerWishlist relation
 * @method     ChildCustomerQuery innerJoinWithCustomerWishlist() Adds a INNER JOIN clause and with to the query using the CustomerWishlist relation
 *
 * @method     ChildCustomerQuery leftJoinCustomerPhoto($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomerPhoto relation
 * @method     ChildCustomerQuery rightJoinCustomerPhoto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomerPhoto relation
 * @method     ChildCustomerQuery innerJoinCustomerPhoto($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomerPhoto relation
 *
 * @method     ChildCustomerQuery joinWithCustomerPhoto($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomerPhoto relation
 *
 * @method     ChildCustomerQuery leftJoinWithCustomerPhoto() Adds a LEFT JOIN clause and with to the query using the CustomerPhoto relation
 * @method     ChildCustomerQuery rightJoinWithCustomerPhoto() Adds a RIGHT JOIN clause and with to the query using the CustomerPhoto relation
 * @method     ChildCustomerQuery innerJoinWithCustomerPhoto() Adds a INNER JOIN clause and with to the query using the CustomerPhoto relation
 *
 * @method     ChildCustomerQuery leftJoinCustomerLogo($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomerLogo relation
 * @method     ChildCustomerQuery rightJoinCustomerLogo($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomerLogo relation
 * @method     ChildCustomerQuery innerJoinCustomerLogo($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomerLogo relation
 *
 * @method     ChildCustomerQuery joinWithCustomerLogo($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomerLogo relation
 *
 * @method     ChildCustomerQuery leftJoinWithCustomerLogo() Adds a LEFT JOIN clause and with to the query using the CustomerLogo relation
 * @method     ChildCustomerQuery rightJoinWithCustomerLogo() Adds a RIGHT JOIN clause and with to the query using the CustomerLogo relation
 * @method     ChildCustomerQuery innerJoinWithCustomerLogo() Adds a INNER JOIN clause and with to the query using the CustomerLogo relation
 *
 * @method     ChildCustomerQuery leftJoinSaleInvoice($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleInvoice relation
 * @method     ChildCustomerQuery rightJoinSaleInvoice($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleInvoice relation
 * @method     ChildCustomerQuery innerJoinSaleInvoice($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleInvoice relation
 *
 * @method     ChildCustomerQuery joinWithSaleInvoice($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleInvoice relation
 *
 * @method     ChildCustomerQuery leftJoinWithSaleInvoice() Adds a LEFT JOIN clause and with to the query using the SaleInvoice relation
 * @method     ChildCustomerQuery rightJoinWithSaleInvoice() Adds a RIGHT JOIN clause and with to the query using the SaleInvoice relation
 * @method     ChildCustomerQuery innerJoinWithSaleInvoice() Adds a INNER JOIN clause and with to the query using the SaleInvoice relation
 *
 * @method     \Model\Setting\MasterTable\LanguageQuery|\Model\Setting\MasterTable\BrancheQuery|\Model\Crm\CustomerSectorQuery|\Model\Setting\MasterTable\PayTermQuery|\Model\Crm\CustomerTypeQuery|\Model\ContactMessageQuery|\Model\Crm\NewsletterSubscriberQuery|\Model\Crm\CustomerCompanyQuery|\Model\Crm\CustomerAddressQuery|\Model\Crm\CustomerNoteQuery|\Model\Crm\CustomerPropertyQuery|\Model\Category\CustomerWishlistQuery|\Model\Crm\CustomerPhotoQuery|\Model\Crm\CustomerLogoQuery|\Model\Finance\SaleInvoiceQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCustomer findOne(ConnectionInterface $con = null) Return the first ChildCustomer matching the query
 * @method     ChildCustomer findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCustomer matching the query, or a new ChildCustomer object populated from the query conditions when no match is found
 *
 * @method     ChildCustomer findOneById(int $id) Return the first ChildCustomer filtered by the id column
 * @method     ChildCustomer findOneByImportSource(string $import_source) Return the first ChildCustomer filtered by the import_source column
 * @method     ChildCustomer findOneByAccountingId(string $accounting_id) Return the first ChildCustomer filtered by the accounting_id column
 * @method     ChildCustomer findOneByOldId(string $old_id) Return the first ChildCustomer filtered by the old_id column
 * @method     ChildCustomer findOneByIp(string $ip) Return the first ChildCustomer filtered by the ip column
 * @method     ChildCustomer findOneByCreatedOn(string $created_on) Return the first ChildCustomer filtered by the created_on column
 * @method     ChildCustomer findOneByItemDeleted(boolean $is_deleted) Return the first ChildCustomer filtered by the is_deleted column
 * @method     ChildCustomer findOneByIsNewsletterSubscriber(boolean $is_newsletter_subscriber) Return the first ChildCustomer filtered by the is_newsletter_subscriber column
 * @method     ChildCustomer findOneByLanguageId(int $language_id) Return the first ChildCustomer filtered by the language_id column
 * @method     ChildCustomer findOneByIsIntracommunautair(int $is_intracommunautair) Return the first ChildCustomer filtered by the is_intracommunautair column
 * @method     ChildCustomer findOneByPaytermId(int $payterm_id) Return the first ChildCustomer filtered by the payterm_id column
 * @method     ChildCustomer findOneByBrancheId(int $branche_id) Return the first ChildCustomer filtered by the branche_id column
 * @method     ChildCustomer findOneBySectorId(int $sector_id) Return the first ChildCustomer filtered by the sector_id column
 * @method     ChildCustomer findOneByCustomerTypeId(int $customer_type_id) Return the first ChildCustomer filtered by the customer_type_id column
 * @method     ChildCustomer findOneByDebitor(string $debitor) Return the first ChildCustomer filtered by the debitor column
 * @method     ChildCustomer findOneByGender(string $gender) Return the first ChildCustomer filtered by the gender column
 * @method     ChildCustomer findOneByFirstName(string $first_name) Return the first ChildCustomer filtered by the first_name column
 * @method     ChildCustomer findOneByInsertion(string $insertion) Return the first ChildCustomer filtered by the insertion column
 * @method     ChildCustomer findOneByLastName(string $last_name) Return the first ChildCustomer filtered by the last_name column
 * @method     ChildCustomer findOneByEmail(string $email) Return the first ChildCustomer filtered by the email column
 * @method     ChildCustomer findOneByPassword(string $password) Return the first ChildCustomer filtered by the password column
 * @method     ChildCustomer findOneBySalt(string $salt) Return the first ChildCustomer filtered by the salt column
 * @method     ChildCustomer findOneByOncePasswordReset(boolean $once_password_reset) Return the first ChildCustomer filtered by the once_password_reset column
 * @method     ChildCustomer findOneByIsWholesale(boolean $is_wholesale) Return the first ChildCustomer filtered by the is_wholesale column
 * @method     ChildCustomer findOneByIsWholesaleApproved(boolean $is_wholesale_approved) Return the first ChildCustomer filtered by the is_wholesale_approved column
 * @method     ChildCustomer findOneByCanBuyOnCredit(boolean $can_buy_on_credit) Return the first ChildCustomer filtered by the can_buy_on_credit column
 * @method     ChildCustomer findOneByIban(string $iban) Return the first ChildCustomer filtered by the iban column
 * @method     ChildCustomer findOneByPhone(string $phone) Return the first ChildCustomer filtered by the phone column
 * @method     ChildCustomer findOneByMobile(string $mobile) Return the first ChildCustomer filtered by the mobile column
 * @method     ChildCustomer findOneByFax(string $fax) Return the first ChildCustomer filtered by the fax column
 * @method     ChildCustomer findOneByWebsite(string $website) Return the first ChildCustomer filtered by the website column
 * @method     ChildCustomer findOneByTag(string $tag) Return the first ChildCustomer filtered by the tag column
 * @method     ChildCustomer findOneByIsVerified(boolean $is_verified) Return the first ChildCustomer filtered by the is_verified column
 * @method     ChildCustomer findOneByVerificationKey(string $verification_key) Return the first ChildCustomer filtered by the verification_key column *

 * @method     ChildCustomer requirePk($key, ConnectionInterface $con = null) Return the ChildCustomer by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOne(ConnectionInterface $con = null) Return the first ChildCustomer matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomer requireOneById(int $id) Return the first ChildCustomer filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByImportSource(string $import_source) Return the first ChildCustomer filtered by the import_source column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByAccountingId(string $accounting_id) Return the first ChildCustomer filtered by the accounting_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByOldId(string $old_id) Return the first ChildCustomer filtered by the old_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByIp(string $ip) Return the first ChildCustomer filtered by the ip column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByCreatedOn(string $created_on) Return the first ChildCustomer filtered by the created_on column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByItemDeleted(boolean $is_deleted) Return the first ChildCustomer filtered by the is_deleted column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByIsNewsletterSubscriber(boolean $is_newsletter_subscriber) Return the first ChildCustomer filtered by the is_newsletter_subscriber column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByLanguageId(int $language_id) Return the first ChildCustomer filtered by the language_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByIsIntracommunautair(int $is_intracommunautair) Return the first ChildCustomer filtered by the is_intracommunautair column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByPaytermId(int $payterm_id) Return the first ChildCustomer filtered by the payterm_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByBrancheId(int $branche_id) Return the first ChildCustomer filtered by the branche_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneBySectorId(int $sector_id) Return the first ChildCustomer filtered by the sector_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByCustomerTypeId(int $customer_type_id) Return the first ChildCustomer filtered by the customer_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByDebitor(string $debitor) Return the first ChildCustomer filtered by the debitor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByGender(string $gender) Return the first ChildCustomer filtered by the gender column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByFirstName(string $first_name) Return the first ChildCustomer filtered by the first_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByInsertion(string $insertion) Return the first ChildCustomer filtered by the insertion column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByLastName(string $last_name) Return the first ChildCustomer filtered by the last_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByEmail(string $email) Return the first ChildCustomer filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByPassword(string $password) Return the first ChildCustomer filtered by the password column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneBySalt(string $salt) Return the first ChildCustomer filtered by the salt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByOncePasswordReset(boolean $once_password_reset) Return the first ChildCustomer filtered by the once_password_reset column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByIsWholesale(boolean $is_wholesale) Return the first ChildCustomer filtered by the is_wholesale column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByIsWholesaleApproved(boolean $is_wholesale_approved) Return the first ChildCustomer filtered by the is_wholesale_approved column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByCanBuyOnCredit(boolean $can_buy_on_credit) Return the first ChildCustomer filtered by the can_buy_on_credit column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByIban(string $iban) Return the first ChildCustomer filtered by the iban column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByPhone(string $phone) Return the first ChildCustomer filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByMobile(string $mobile) Return the first ChildCustomer filtered by the mobile column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByFax(string $fax) Return the first ChildCustomer filtered by the fax column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByWebsite(string $website) Return the first ChildCustomer filtered by the website column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByTag(string $tag) Return the first ChildCustomer filtered by the tag column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByIsVerified(boolean $is_verified) Return the first ChildCustomer filtered by the is_verified column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomer requireOneByVerificationKey(string $verification_key) Return the first ChildCustomer filtered by the verification_key column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomer[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCustomer objects based on current ModelCriteria
 * @method     ChildCustomer[]|ObjectCollection findById(int $id) Return ChildCustomer objects filtered by the id column
 * @method     ChildCustomer[]|ObjectCollection findByImportSource(string $import_source) Return ChildCustomer objects filtered by the import_source column
 * @method     ChildCustomer[]|ObjectCollection findByAccountingId(string $accounting_id) Return ChildCustomer objects filtered by the accounting_id column
 * @method     ChildCustomer[]|ObjectCollection findByOldId(string $old_id) Return ChildCustomer objects filtered by the old_id column
 * @method     ChildCustomer[]|ObjectCollection findByIp(string $ip) Return ChildCustomer objects filtered by the ip column
 * @method     ChildCustomer[]|ObjectCollection findByCreatedOn(string $created_on) Return ChildCustomer objects filtered by the created_on column
 * @method     ChildCustomer[]|ObjectCollection findByItemDeleted(boolean $is_deleted) Return ChildCustomer objects filtered by the is_deleted column
 * @method     ChildCustomer[]|ObjectCollection findByIsNewsletterSubscriber(boolean $is_newsletter_subscriber) Return ChildCustomer objects filtered by the is_newsletter_subscriber column
 * @method     ChildCustomer[]|ObjectCollection findByLanguageId(int $language_id) Return ChildCustomer objects filtered by the language_id column
 * @method     ChildCustomer[]|ObjectCollection findByIsIntracommunautair(int $is_intracommunautair) Return ChildCustomer objects filtered by the is_intracommunautair column
 * @method     ChildCustomer[]|ObjectCollection findByPaytermId(int $payterm_id) Return ChildCustomer objects filtered by the payterm_id column
 * @method     ChildCustomer[]|ObjectCollection findByBrancheId(int $branche_id) Return ChildCustomer objects filtered by the branche_id column
 * @method     ChildCustomer[]|ObjectCollection findBySectorId(int $sector_id) Return ChildCustomer objects filtered by the sector_id column
 * @method     ChildCustomer[]|ObjectCollection findByCustomerTypeId(int $customer_type_id) Return ChildCustomer objects filtered by the customer_type_id column
 * @method     ChildCustomer[]|ObjectCollection findByDebitor(string $debitor) Return ChildCustomer objects filtered by the debitor column
 * @method     ChildCustomer[]|ObjectCollection findByGender(string $gender) Return ChildCustomer objects filtered by the gender column
 * @method     ChildCustomer[]|ObjectCollection findByFirstName(string $first_name) Return ChildCustomer objects filtered by the first_name column
 * @method     ChildCustomer[]|ObjectCollection findByInsertion(string $insertion) Return ChildCustomer objects filtered by the insertion column
 * @method     ChildCustomer[]|ObjectCollection findByLastName(string $last_name) Return ChildCustomer objects filtered by the last_name column
 * @method     ChildCustomer[]|ObjectCollection findByEmail(string $email) Return ChildCustomer objects filtered by the email column
 * @method     ChildCustomer[]|ObjectCollection findByPassword(string $password) Return ChildCustomer objects filtered by the password column
 * @method     ChildCustomer[]|ObjectCollection findBySalt(string $salt) Return ChildCustomer objects filtered by the salt column
 * @method     ChildCustomer[]|ObjectCollection findByOncePasswordReset(boolean $once_password_reset) Return ChildCustomer objects filtered by the once_password_reset column
 * @method     ChildCustomer[]|ObjectCollection findByIsWholesale(boolean $is_wholesale) Return ChildCustomer objects filtered by the is_wholesale column
 * @method     ChildCustomer[]|ObjectCollection findByIsWholesaleApproved(boolean $is_wholesale_approved) Return ChildCustomer objects filtered by the is_wholesale_approved column
 * @method     ChildCustomer[]|ObjectCollection findByCanBuyOnCredit(boolean $can_buy_on_credit) Return ChildCustomer objects filtered by the can_buy_on_credit column
 * @method     ChildCustomer[]|ObjectCollection findByIban(string $iban) Return ChildCustomer objects filtered by the iban column
 * @method     ChildCustomer[]|ObjectCollection findByPhone(string $phone) Return ChildCustomer objects filtered by the phone column
 * @method     ChildCustomer[]|ObjectCollection findByMobile(string $mobile) Return ChildCustomer objects filtered by the mobile column
 * @method     ChildCustomer[]|ObjectCollection findByFax(string $fax) Return ChildCustomer objects filtered by the fax column
 * @method     ChildCustomer[]|ObjectCollection findByWebsite(string $website) Return ChildCustomer objects filtered by the website column
 * @method     ChildCustomer[]|ObjectCollection findByTag(string $tag) Return ChildCustomer objects filtered by the tag column
 * @method     ChildCustomer[]|ObjectCollection findByIsVerified(boolean $is_verified) Return ChildCustomer objects filtered by the is_verified column
 * @method     ChildCustomer[]|ObjectCollection findByVerificationKey(string $verification_key) Return ChildCustomer objects filtered by the verification_key column
 * @method     ChildCustomer[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CustomerQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Crm\Base\CustomerQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Crm\\Customer', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCustomerQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCustomerQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCustomerQuery) {
            return $criteria;
        }
        $query = new ChildCustomerQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCustomer|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CustomerTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CustomerTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomer A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, import_source, accounting_id, old_id, ip, created_on, is_deleted, is_newsletter_subscriber, language_id, is_intracommunautair, payterm_id, branche_id, sector_id, customer_type_id, debitor, gender, first_name, insertion, last_name, email, password, salt, once_password_reset, is_wholesale, is_wholesale_approved, can_buy_on_credit, iban, phone, mobile, fax, website, tag, is_verified, verification_key FROM customer WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCustomer $obj */
            $obj = new ChildCustomer();
            $obj->hydrate($row);
            CustomerTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCustomer|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CustomerTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CustomerTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CustomerTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CustomerTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the import_source column
     *
     * Example usage:
     * <code>
     * $query->filterByImportSource('fooValue');   // WHERE import_source = 'fooValue'
     * $query->filterByImportSource('%fooValue%', Criteria::LIKE); // WHERE import_source LIKE '%fooValue%'
     * </code>
     *
     * @param     string $importSource The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByImportSource($importSource = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($importSource)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_IMPORT_SOURCE, $importSource, $comparison);
    }

    /**
     * Filter the query on the accounting_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAccountingId('fooValue');   // WHERE accounting_id = 'fooValue'
     * $query->filterByAccountingId('%fooValue%', Criteria::LIKE); // WHERE accounting_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $accountingId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByAccountingId($accountingId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($accountingId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_ACCOUNTING_ID, $accountingId, $comparison);
    }

    /**
     * Filter the query on the old_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldId('fooValue');   // WHERE old_id = 'fooValue'
     * $query->filterByOldId('%fooValue%', Criteria::LIKE); // WHERE old_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $oldId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByOldId($oldId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($oldId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_OLD_ID, $oldId, $comparison);
    }

    /**
     * Filter the query on the ip column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE ip = 'fooValue'
     * $query->filterByIp('%fooValue%', Criteria::LIKE); // WHERE ip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_IP, $ip, $comparison);
    }

    /**
     * Filter the query on the created_on column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedOn('2011-03-14'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn('now'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn(array('max' => 'yesterday')); // WHERE created_on > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdOn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByCreatedOn($createdOn = null, $comparison = null)
    {
        if (is_array($createdOn)) {
            $useMinMax = false;
            if (isset($createdOn['min'])) {
                $this->addUsingAlias(CustomerTableMap::COL_CREATED_ON, $createdOn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdOn['max'])) {
                $this->addUsingAlias(CustomerTableMap::COL_CREATED_ON, $createdOn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_CREATED_ON, $createdOn, $comparison);
    }

    /**
     * Filter the query on the is_deleted column
     *
     * Example usage:
     * <code>
     * $query->filterByItemDeleted(true); // WHERE is_deleted = true
     * $query->filterByItemDeleted('yes'); // WHERE is_deleted = true
     * </code>
     *
     * @param     boolean|string $itemDeleted The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByItemDeleted($itemDeleted = null, $comparison = null)
    {
        if (is_string($itemDeleted)) {
            $itemDeleted = in_array(strtolower($itemDeleted), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CustomerTableMap::COL_IS_DELETED, $itemDeleted, $comparison);
    }

    /**
     * Filter the query on the is_newsletter_subscriber column
     *
     * Example usage:
     * <code>
     * $query->filterByIsNewsletterSubscriber(true); // WHERE is_newsletter_subscriber = true
     * $query->filterByIsNewsletterSubscriber('yes'); // WHERE is_newsletter_subscriber = true
     * </code>
     *
     * @param     boolean|string $isNewsletterSubscriber The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByIsNewsletterSubscriber($isNewsletterSubscriber = null, $comparison = null)
    {
        if (is_string($isNewsletterSubscriber)) {
            $isNewsletterSubscriber = in_array(strtolower($isNewsletterSubscriber), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CustomerTableMap::COL_IS_NEWSLETTER_SUBSCRIBER, $isNewsletterSubscriber, $comparison);
    }

    /**
     * Filter the query on the language_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguageId(1234); // WHERE language_id = 1234
     * $query->filterByLanguageId(array(12, 34)); // WHERE language_id IN (12, 34)
     * $query->filterByLanguageId(array('min' => 12)); // WHERE language_id > 12
     * </code>
     *
     * @see       filterByLanguage()
     *
     * @param     mixed $languageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByLanguageId($languageId = null, $comparison = null)
    {
        if (is_array($languageId)) {
            $useMinMax = false;
            if (isset($languageId['min'])) {
                $this->addUsingAlias(CustomerTableMap::COL_LANGUAGE_ID, $languageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($languageId['max'])) {
                $this->addUsingAlias(CustomerTableMap::COL_LANGUAGE_ID, $languageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_LANGUAGE_ID, $languageId, $comparison);
    }

    /**
     * Filter the query on the is_intracommunautair column
     *
     * Example usage:
     * <code>
     * $query->filterByIsIntracommunautair(1234); // WHERE is_intracommunautair = 1234
     * $query->filterByIsIntracommunautair(array(12, 34)); // WHERE is_intracommunautair IN (12, 34)
     * $query->filterByIsIntracommunautair(array('min' => 12)); // WHERE is_intracommunautair > 12
     * </code>
     *
     * @param     mixed $isIntracommunautair The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByIsIntracommunautair($isIntracommunautair = null, $comparison = null)
    {
        if (is_array($isIntracommunautair)) {
            $useMinMax = false;
            if (isset($isIntracommunautair['min'])) {
                $this->addUsingAlias(CustomerTableMap::COL_IS_INTRACOMMUNAUTAIR, $isIntracommunautair['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isIntracommunautair['max'])) {
                $this->addUsingAlias(CustomerTableMap::COL_IS_INTRACOMMUNAUTAIR, $isIntracommunautair['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_IS_INTRACOMMUNAUTAIR, $isIntracommunautair, $comparison);
    }

    /**
     * Filter the query on the payterm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPaytermId(1234); // WHERE payterm_id = 1234
     * $query->filterByPaytermId(array(12, 34)); // WHERE payterm_id IN (12, 34)
     * $query->filterByPaytermId(array('min' => 12)); // WHERE payterm_id > 12
     * </code>
     *
     * @see       filterByPayTerm()
     *
     * @param     mixed $paytermId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByPaytermId($paytermId = null, $comparison = null)
    {
        if (is_array($paytermId)) {
            $useMinMax = false;
            if (isset($paytermId['min'])) {
                $this->addUsingAlias(CustomerTableMap::COL_PAYTERM_ID, $paytermId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($paytermId['max'])) {
                $this->addUsingAlias(CustomerTableMap::COL_PAYTERM_ID, $paytermId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_PAYTERM_ID, $paytermId, $comparison);
    }

    /**
     * Filter the query on the branche_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBrancheId(1234); // WHERE branche_id = 1234
     * $query->filterByBrancheId(array(12, 34)); // WHERE branche_id IN (12, 34)
     * $query->filterByBrancheId(array('min' => 12)); // WHERE branche_id > 12
     * </code>
     *
     * @see       filterByBranche()
     *
     * @param     mixed $brancheId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByBrancheId($brancheId = null, $comparison = null)
    {
        if (is_array($brancheId)) {
            $useMinMax = false;
            if (isset($brancheId['min'])) {
                $this->addUsingAlias(CustomerTableMap::COL_BRANCHE_ID, $brancheId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($brancheId['max'])) {
                $this->addUsingAlias(CustomerTableMap::COL_BRANCHE_ID, $brancheId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_BRANCHE_ID, $brancheId, $comparison);
    }

    /**
     * Filter the query on the sector_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySectorId(1234); // WHERE sector_id = 1234
     * $query->filterBySectorId(array(12, 34)); // WHERE sector_id IN (12, 34)
     * $query->filterBySectorId(array('min' => 12)); // WHERE sector_id > 12
     * </code>
     *
     * @see       filterByCustomerSector()
     *
     * @param     mixed $sectorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterBySectorId($sectorId = null, $comparison = null)
    {
        if (is_array($sectorId)) {
            $useMinMax = false;
            if (isset($sectorId['min'])) {
                $this->addUsingAlias(CustomerTableMap::COL_SECTOR_ID, $sectorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sectorId['max'])) {
                $this->addUsingAlias(CustomerTableMap::COL_SECTOR_ID, $sectorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_SECTOR_ID, $sectorId, $comparison);
    }

    /**
     * Filter the query on the customer_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerTypeId(1234); // WHERE customer_type_id = 1234
     * $query->filterByCustomerTypeId(array(12, 34)); // WHERE customer_type_id IN (12, 34)
     * $query->filterByCustomerTypeId(array('min' => 12)); // WHERE customer_type_id > 12
     * </code>
     *
     * @see       filterByCustomerType()
     *
     * @param     mixed $customerTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByCustomerTypeId($customerTypeId = null, $comparison = null)
    {
        if (is_array($customerTypeId)) {
            $useMinMax = false;
            if (isset($customerTypeId['min'])) {
                $this->addUsingAlias(CustomerTableMap::COL_CUSTOMER_TYPE_ID, $customerTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerTypeId['max'])) {
                $this->addUsingAlias(CustomerTableMap::COL_CUSTOMER_TYPE_ID, $customerTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_CUSTOMER_TYPE_ID, $customerTypeId, $comparison);
    }

    /**
     * Filter the query on the debitor column
     *
     * Example usage:
     * <code>
     * $query->filterByDebitor('fooValue');   // WHERE debitor = 'fooValue'
     * $query->filterByDebitor('%fooValue%', Criteria::LIKE); // WHERE debitor LIKE '%fooValue%'
     * </code>
     *
     * @param     string $debitor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByDebitor($debitor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($debitor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_DEBITOR, $debitor, $comparison);
    }

    /**
     * Filter the query on the gender column
     *
     * Example usage:
     * <code>
     * $query->filterByGender('fooValue');   // WHERE gender = 'fooValue'
     * $query->filterByGender('%fooValue%', Criteria::LIKE); // WHERE gender LIKE '%fooValue%'
     * </code>
     *
     * @param     string $gender The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByGender($gender = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($gender)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_GENDER, $gender, $comparison);
    }

    /**
     * Filter the query on the first_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstName('fooValue');   // WHERE first_name = 'fooValue'
     * $query->filterByFirstName('%fooValue%', Criteria::LIKE); // WHERE first_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $firstName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByFirstName($firstName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($firstName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_FIRST_NAME, $firstName, $comparison);
    }

    /**
     * Filter the query on the insertion column
     *
     * Example usage:
     * <code>
     * $query->filterByInsertion('fooValue');   // WHERE insertion = 'fooValue'
     * $query->filterByInsertion('%fooValue%', Criteria::LIKE); // WHERE insertion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $insertion The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByInsertion($insertion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($insertion)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_INSERTION, $insertion, $comparison);
    }

    /**
     * Filter the query on the last_name column
     *
     * Example usage:
     * <code>
     * $query->filterByLastName('fooValue');   // WHERE last_name = 'fooValue'
     * $query->filterByLastName('%fooValue%', Criteria::LIKE); // WHERE last_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $lastName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByLastName($lastName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($lastName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_LAST_NAME, $lastName, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%', Criteria::LIKE); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query on the salt column
     *
     * Example usage:
     * <code>
     * $query->filterBySalt('fooValue');   // WHERE salt = 'fooValue'
     * $query->filterBySalt('%fooValue%', Criteria::LIKE); // WHERE salt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $salt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterBySalt($salt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($salt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_SALT, $salt, $comparison);
    }

    /**
     * Filter the query on the once_password_reset column
     *
     * Example usage:
     * <code>
     * $query->filterByOncePasswordReset(true); // WHERE once_password_reset = true
     * $query->filterByOncePasswordReset('yes'); // WHERE once_password_reset = true
     * </code>
     *
     * @param     boolean|string $oncePasswordReset The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByOncePasswordReset($oncePasswordReset = null, $comparison = null)
    {
        if (is_string($oncePasswordReset)) {
            $oncePasswordReset = in_array(strtolower($oncePasswordReset), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CustomerTableMap::COL_ONCE_PASSWORD_RESET, $oncePasswordReset, $comparison);
    }

    /**
     * Filter the query on the is_wholesale column
     *
     * Example usage:
     * <code>
     * $query->filterByIsWholesale(true); // WHERE is_wholesale = true
     * $query->filterByIsWholesale('yes'); // WHERE is_wholesale = true
     * </code>
     *
     * @param     boolean|string $isWholesale The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByIsWholesale($isWholesale = null, $comparison = null)
    {
        if (is_string($isWholesale)) {
            $isWholesale = in_array(strtolower($isWholesale), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CustomerTableMap::COL_IS_WHOLESALE, $isWholesale, $comparison);
    }

    /**
     * Filter the query on the is_wholesale_approved column
     *
     * Example usage:
     * <code>
     * $query->filterByIsWholesaleApproved(true); // WHERE is_wholesale_approved = true
     * $query->filterByIsWholesaleApproved('yes'); // WHERE is_wholesale_approved = true
     * </code>
     *
     * @param     boolean|string $isWholesaleApproved The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByIsWholesaleApproved($isWholesaleApproved = null, $comparison = null)
    {
        if (is_string($isWholesaleApproved)) {
            $isWholesaleApproved = in_array(strtolower($isWholesaleApproved), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CustomerTableMap::COL_IS_WHOLESALE_APPROVED, $isWholesaleApproved, $comparison);
    }

    /**
     * Filter the query on the can_buy_on_credit column
     *
     * Example usage:
     * <code>
     * $query->filterByCanBuyOnCredit(true); // WHERE can_buy_on_credit = true
     * $query->filterByCanBuyOnCredit('yes'); // WHERE can_buy_on_credit = true
     * </code>
     *
     * @param     boolean|string $canBuyOnCredit The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByCanBuyOnCredit($canBuyOnCredit = null, $comparison = null)
    {
        if (is_string($canBuyOnCredit)) {
            $canBuyOnCredit = in_array(strtolower($canBuyOnCredit), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CustomerTableMap::COL_CAN_BUY_ON_CREDIT, $canBuyOnCredit, $comparison);
    }

    /**
     * Filter the query on the iban column
     *
     * Example usage:
     * <code>
     * $query->filterByIban('fooValue');   // WHERE iban = 'fooValue'
     * $query->filterByIban('%fooValue%', Criteria::LIKE); // WHERE iban LIKE '%fooValue%'
     * </code>
     *
     * @param     string $iban The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByIban($iban = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($iban)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_IBAN, $iban, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%', Criteria::LIKE); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the mobile column
     *
     * Example usage:
     * <code>
     * $query->filterByMobile('fooValue');   // WHERE mobile = 'fooValue'
     * $query->filterByMobile('%fooValue%', Criteria::LIKE); // WHERE mobile LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mobile The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByMobile($mobile = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mobile)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_MOBILE, $mobile, $comparison);
    }

    /**
     * Filter the query on the fax column
     *
     * Example usage:
     * <code>
     * $query->filterByFax('fooValue');   // WHERE fax = 'fooValue'
     * $query->filterByFax('%fooValue%', Criteria::LIKE); // WHERE fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fax The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByFax($fax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fax)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_FAX, $fax, $comparison);
    }

    /**
     * Filter the query on the website column
     *
     * Example usage:
     * <code>
     * $query->filterByWebsite('fooValue');   // WHERE website = 'fooValue'
     * $query->filterByWebsite('%fooValue%', Criteria::LIKE); // WHERE website LIKE '%fooValue%'
     * </code>
     *
     * @param     string $website The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByWebsite($website = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($website)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_WEBSITE, $website, $comparison);
    }

    /**
     * Filter the query on the tag column
     *
     * Example usage:
     * <code>
     * $query->filterByTag('fooValue');   // WHERE tag = 'fooValue'
     * $query->filterByTag('%fooValue%', Criteria::LIKE); // WHERE tag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tag The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByTag($tag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tag)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_TAG, $tag, $comparison);
    }

    /**
     * Filter the query on the is_verified column
     *
     * Example usage:
     * <code>
     * $query->filterByIsVerified(true); // WHERE is_verified = true
     * $query->filterByIsVerified('yes'); // WHERE is_verified = true
     * </code>
     *
     * @param     boolean|string $isVerified The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByIsVerified($isVerified = null, $comparison = null)
    {
        if (is_string($isVerified)) {
            $isVerified = in_array(strtolower($isVerified), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CustomerTableMap::COL_IS_VERIFIED, $isVerified, $comparison);
    }

    /**
     * Filter the query on the verification_key column
     *
     * Example usage:
     * <code>
     * $query->filterByVerificationKey('fooValue');   // WHERE verification_key = 'fooValue'
     * $query->filterByVerificationKey('%fooValue%', Criteria::LIKE); // WHERE verification_key LIKE '%fooValue%'
     * </code>
     *
     * @param     string $verificationKey The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByVerificationKey($verificationKey = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($verificationKey)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerTableMap::COL_VERIFICATION_KEY, $verificationKey, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Language object
     *
     * @param \Model\Setting\MasterTable\Language|ObjectCollection $language The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByLanguage($language, $comparison = null)
    {
        if ($language instanceof \Model\Setting\MasterTable\Language) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_LANGUAGE_ID, $language->getId(), $comparison);
        } elseif ($language instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomerTableMap::COL_LANGUAGE_ID, $language->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLanguage() only accepts arguments of type \Model\Setting\MasterTable\Language or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Language relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinLanguage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Language');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Language');
        }

        return $this;
    }

    /**
     * Use the Language relation Language object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\LanguageQuery A secondary query class using the current class as primary query
     */
    public function useLanguageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLanguage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Language', '\Model\Setting\MasterTable\LanguageQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Branche object
     *
     * @param \Model\Setting\MasterTable\Branche|ObjectCollection $branche The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByBranche($branche, $comparison = null)
    {
        if ($branche instanceof \Model\Setting\MasterTable\Branche) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_BRANCHE_ID, $branche->getId(), $comparison);
        } elseif ($branche instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomerTableMap::COL_BRANCHE_ID, $branche->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByBranche() only accepts arguments of type \Model\Setting\MasterTable\Branche or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Branche relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinBranche($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Branche');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Branche');
        }

        return $this;
    }

    /**
     * Use the Branche relation Branche object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\BrancheQuery A secondary query class using the current class as primary query
     */
    public function useBrancheQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBranche($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Branche', '\Model\Setting\MasterTable\BrancheQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\CustomerSector object
     *
     * @param \Model\Crm\CustomerSector|ObjectCollection $customerSector The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByCustomerSector($customerSector, $comparison = null)
    {
        if ($customerSector instanceof \Model\Crm\CustomerSector) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_SECTOR_ID, $customerSector->getId(), $comparison);
        } elseif ($customerSector instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomerTableMap::COL_SECTOR_ID, $customerSector->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCustomerSector() only accepts arguments of type \Model\Crm\CustomerSector or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomerSector relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinCustomerSector($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomerSector');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomerSector');
        }

        return $this;
    }

    /**
     * Use the CustomerSector relation CustomerSector object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerSectorQuery A secondary query class using the current class as primary query
     */
    public function useCustomerSectorQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCustomerSector($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomerSector', '\Model\Crm\CustomerSectorQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\PayTerm object
     *
     * @param \Model\Setting\MasterTable\PayTerm|ObjectCollection $payTerm The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByPayTerm($payTerm, $comparison = null)
    {
        if ($payTerm instanceof \Model\Setting\MasterTable\PayTerm) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_PAYTERM_ID, $payTerm->getId(), $comparison);
        } elseif ($payTerm instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomerTableMap::COL_PAYTERM_ID, $payTerm->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPayTerm() only accepts arguments of type \Model\Setting\MasterTable\PayTerm or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PayTerm relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinPayTerm($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PayTerm');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PayTerm');
        }

        return $this;
    }

    /**
     * Use the PayTerm relation PayTerm object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\PayTermQuery A secondary query class using the current class as primary query
     */
    public function usePayTermQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPayTerm($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PayTerm', '\Model\Setting\MasterTable\PayTermQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\CustomerType object
     *
     * @param \Model\Crm\CustomerType|ObjectCollection $customerType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByCustomerType($customerType, $comparison = null)
    {
        if ($customerType instanceof \Model\Crm\CustomerType) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_CUSTOMER_TYPE_ID, $customerType->getId(), $comparison);
        } elseif ($customerType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomerTableMap::COL_CUSTOMER_TYPE_ID, $customerType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCustomerType() only accepts arguments of type \Model\Crm\CustomerType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomerType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinCustomerType($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomerType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomerType');
        }

        return $this;
    }

    /**
     * Use the CustomerType relation CustomerType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerTypeQuery A secondary query class using the current class as primary query
     */
    public function useCustomerTypeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCustomerType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomerType', '\Model\Crm\CustomerTypeQuery');
    }

    /**
     * Filter the query by a related \Model\ContactMessage object
     *
     * @param \Model\ContactMessage|ObjectCollection $contactMessage the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByContactMessage($contactMessage, $comparison = null)
    {
        if ($contactMessage instanceof \Model\ContactMessage) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_ID, $contactMessage->getCustomerId(), $comparison);
        } elseif ($contactMessage instanceof ObjectCollection) {
            return $this
                ->useContactMessageQuery()
                ->filterByPrimaryKeys($contactMessage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByContactMessage() only accepts arguments of type \Model\ContactMessage or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContactMessage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinContactMessage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContactMessage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContactMessage');
        }

        return $this;
    }

    /**
     * Use the ContactMessage relation ContactMessage object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ContactMessageQuery A secondary query class using the current class as primary query
     */
    public function useContactMessageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinContactMessage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContactMessage', '\Model\ContactMessageQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\NewsletterSubscriber object
     *
     * @param \Model\Crm\NewsletterSubscriber|ObjectCollection $newsletterSubscriber the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByNewsletterSubscriber($newsletterSubscriber, $comparison = null)
    {
        if ($newsletterSubscriber instanceof \Model\Crm\NewsletterSubscriber) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_ID, $newsletterSubscriber->getCustomerId(), $comparison);
        } elseif ($newsletterSubscriber instanceof ObjectCollection) {
            return $this
                ->useNewsletterSubscriberQuery()
                ->filterByPrimaryKeys($newsletterSubscriber->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByNewsletterSubscriber() only accepts arguments of type \Model\Crm\NewsletterSubscriber or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the NewsletterSubscriber relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinNewsletterSubscriber($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('NewsletterSubscriber');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'NewsletterSubscriber');
        }

        return $this;
    }

    /**
     * Use the NewsletterSubscriber relation NewsletterSubscriber object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\NewsletterSubscriberQuery A secondary query class using the current class as primary query
     */
    public function useNewsletterSubscriberQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinNewsletterSubscriber($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'NewsletterSubscriber', '\Model\Crm\NewsletterSubscriberQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\CustomerCompany object
     *
     * @param \Model\Crm\CustomerCompany|ObjectCollection $customerCompany the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByCustomerCompany($customerCompany, $comparison = null)
    {
        if ($customerCompany instanceof \Model\Crm\CustomerCompany) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_ID, $customerCompany->getCustomerId(), $comparison);
        } elseif ($customerCompany instanceof ObjectCollection) {
            return $this
                ->useCustomerCompanyQuery()
                ->filterByPrimaryKeys($customerCompany->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomerCompany() only accepts arguments of type \Model\Crm\CustomerCompany or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomerCompany relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinCustomerCompany($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomerCompany');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomerCompany');
        }

        return $this;
    }

    /**
     * Use the CustomerCompany relation CustomerCompany object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerCompanyQuery A secondary query class using the current class as primary query
     */
    public function useCustomerCompanyQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCustomerCompany($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomerCompany', '\Model\Crm\CustomerCompanyQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\CustomerAddress object
     *
     * @param \Model\Crm\CustomerAddress|ObjectCollection $customerAddress the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByCustomer($customerAddress, $comparison = null)
    {
        if ($customerAddress instanceof \Model\Crm\CustomerAddress) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_ID, $customerAddress->getCustomerId(), $comparison);
        } elseif ($customerAddress instanceof ObjectCollection) {
            return $this
                ->useCustomerQuery()
                ->filterByPrimaryKeys($customerAddress->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomer() only accepts arguments of type \Model\Crm\CustomerAddress or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Customer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinCustomer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Customer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Customer');
        }

        return $this;
    }

    /**
     * Use the Customer relation CustomerAddress object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerAddressQuery A secondary query class using the current class as primary query
     */
    public function useCustomerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCustomer($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Customer', '\Model\Crm\CustomerAddressQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\CustomerNote object
     *
     * @param \Model\Crm\CustomerNote|ObjectCollection $customerNote the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByCustomerNote($customerNote, $comparison = null)
    {
        if ($customerNote instanceof \Model\Crm\CustomerNote) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_ID, $customerNote->getCustomerId(), $comparison);
        } elseif ($customerNote instanceof ObjectCollection) {
            return $this
                ->useCustomerNoteQuery()
                ->filterByPrimaryKeys($customerNote->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomerNote() only accepts arguments of type \Model\Crm\CustomerNote or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomerNote relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinCustomerNote($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomerNote');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomerNote');
        }

        return $this;
    }

    /**
     * Use the CustomerNote relation CustomerNote object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerNoteQuery A secondary query class using the current class as primary query
     */
    public function useCustomerNoteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCustomerNote($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomerNote', '\Model\Crm\CustomerNoteQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\CustomerProperty object
     *
     * @param \Model\Crm\CustomerProperty|ObjectCollection $customerProperty the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByCustomerProperty($customerProperty, $comparison = null)
    {
        if ($customerProperty instanceof \Model\Crm\CustomerProperty) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_ID, $customerProperty->getCustomerId(), $comparison);
        } elseif ($customerProperty instanceof ObjectCollection) {
            return $this
                ->useCustomerPropertyQuery()
                ->filterByPrimaryKeys($customerProperty->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomerProperty() only accepts arguments of type \Model\Crm\CustomerProperty or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomerProperty relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinCustomerProperty($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomerProperty');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomerProperty');
        }

        return $this;
    }

    /**
     * Use the CustomerProperty relation CustomerProperty object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerPropertyQuery A secondary query class using the current class as primary query
     */
    public function useCustomerPropertyQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCustomerProperty($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomerProperty', '\Model\Crm\CustomerPropertyQuery');
    }

    /**
     * Filter the query by a related \Model\Category\CustomerWishlist object
     *
     * @param \Model\Category\CustomerWishlist|ObjectCollection $customerWishlist the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByCustomerWishlist($customerWishlist, $comparison = null)
    {
        if ($customerWishlist instanceof \Model\Category\CustomerWishlist) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_ID, $customerWishlist->getCustomerId(), $comparison);
        } elseif ($customerWishlist instanceof ObjectCollection) {
            return $this
                ->useCustomerWishlistQuery()
                ->filterByPrimaryKeys($customerWishlist->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomerWishlist() only accepts arguments of type \Model\Category\CustomerWishlist or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomerWishlist relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinCustomerWishlist($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomerWishlist');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomerWishlist');
        }

        return $this;
    }

    /**
     * Use the CustomerWishlist relation CustomerWishlist object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Category\CustomerWishlistQuery A secondary query class using the current class as primary query
     */
    public function useCustomerWishlistQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCustomerWishlist($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomerWishlist', '\Model\Category\CustomerWishlistQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\CustomerPhoto object
     *
     * @param \Model\Crm\CustomerPhoto|ObjectCollection $customerPhoto the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByCustomerPhoto($customerPhoto, $comparison = null)
    {
        if ($customerPhoto instanceof \Model\Crm\CustomerPhoto) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_ID, $customerPhoto->getCustomerId(), $comparison);
        } elseif ($customerPhoto instanceof ObjectCollection) {
            return $this
                ->useCustomerPhotoQuery()
                ->filterByPrimaryKeys($customerPhoto->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomerPhoto() only accepts arguments of type \Model\Crm\CustomerPhoto or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomerPhoto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinCustomerPhoto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomerPhoto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomerPhoto');
        }

        return $this;
    }

    /**
     * Use the CustomerPhoto relation CustomerPhoto object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerPhotoQuery A secondary query class using the current class as primary query
     */
    public function useCustomerPhotoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCustomerPhoto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomerPhoto', '\Model\Crm\CustomerPhotoQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\CustomerLogo object
     *
     * @param \Model\Crm\CustomerLogo|ObjectCollection $customerLogo the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterByCustomerLogo($customerLogo, $comparison = null)
    {
        if ($customerLogo instanceof \Model\Crm\CustomerLogo) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_ID, $customerLogo->getCustomerId(), $comparison);
        } elseif ($customerLogo instanceof ObjectCollection) {
            return $this
                ->useCustomerLogoQuery()
                ->filterByPrimaryKeys($customerLogo->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomerLogo() only accepts arguments of type \Model\Crm\CustomerLogo or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomerLogo relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinCustomerLogo($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomerLogo');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomerLogo');
        }

        return $this;
    }

    /**
     * Use the CustomerLogo relation CustomerLogo object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerLogoQuery A secondary query class using the current class as primary query
     */
    public function useCustomerLogoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCustomerLogo($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomerLogo', '\Model\Crm\CustomerLogoQuery');
    }

    /**
     * Filter the query by a related \Model\Finance\SaleInvoice object
     *
     * @param \Model\Finance\SaleInvoice|ObjectCollection $saleInvoice the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCustomerQuery The current query, for fluid interface
     */
    public function filterBySaleInvoice($saleInvoice, $comparison = null)
    {
        if ($saleInvoice instanceof \Model\Finance\SaleInvoice) {
            return $this
                ->addUsingAlias(CustomerTableMap::COL_ID, $saleInvoice->getCustomerId(), $comparison);
        } elseif ($saleInvoice instanceof ObjectCollection) {
            return $this
                ->useSaleInvoiceQuery()
                ->filterByPrimaryKeys($saleInvoice->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleInvoice() only accepts arguments of type \Model\Finance\SaleInvoice or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleInvoice relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function joinSaleInvoice($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleInvoice');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleInvoice');
        }

        return $this;
    }

    /**
     * Use the SaleInvoice relation SaleInvoice object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Finance\SaleInvoiceQuery A secondary query class using the current class as primary query
     */
    public function useSaleInvoiceQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaleInvoice($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleInvoice', '\Model\Finance\SaleInvoiceQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCustomer $customer Object to remove from the list of results
     *
     * @return $this|ChildCustomerQuery The current query, for fluid interface
     */
    public function prune($customer = null)
    {
        if ($customer) {
            $this->addUsingAlias(CustomerTableMap::COL_ID, $customer->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the customer table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CustomerTableMap::clearInstancePool();
            CustomerTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CustomerTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CustomerTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CustomerTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 3600)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CustomerTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(CustomerTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

} // CustomerQuery
