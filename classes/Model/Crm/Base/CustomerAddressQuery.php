<?php

namespace Model\Crm\Base;

use \Exception;
use \PDO;
use Model\Crm\CustomerAddress as ChildCustomerAddress;
use Model\Crm\CustomerAddressQuery as ChildCustomerAddressQuery;
use Model\Crm\Map\CustomerAddressTableMap;
use Model\Setting\MasterTable\Country;
use Model\Setting\MasterTable\UsaState;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'customer_address' table.
 *
 *
 *
 * @method     ChildCustomerAddressQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCustomerAddressQuery orderByCustomerId($order = Criteria::ASC) Order by the customer_id column
 * @method     ChildCustomerAddressQuery orderByCustomerAddressTypeId($order = Criteria::ASC) Order by the customer_address_type_id column
 * @method     ChildCustomerAddressQuery orderByIsDefault($order = Criteria::ASC) Order by the is_default column
 * @method     ChildCustomerAddressQuery orderByCompanyName($order = Criteria::ASC) Order by the company_name column
 * @method     ChildCustomerAddressQuery orderByAttnName($order = Criteria::ASC) Order by the attn_name column
 * @method     ChildCustomerAddressQuery orderByStreet($order = Criteria::ASC) Order by the street column
 * @method     ChildCustomerAddressQuery orderByNumber($order = Criteria::ASC) Order by the number column
 * @method     ChildCustomerAddressQuery orderByNumberAdd($order = Criteria::ASC) Order by the number_add column
 * @method     ChildCustomerAddressQuery orderByPostal($order = Criteria::ASC) Order by the postal column
 * @method     ChildCustomerAddressQuery orderByCity($order = Criteria::ASC) Order by the city column
 * @method     ChildCustomerAddressQuery orderByRegion($order = Criteria::ASC) Order by the region column
 * @method     ChildCustomerAddressQuery orderByCountryId($order = Criteria::ASC) Order by the country_id column
 * @method     ChildCustomerAddressQuery orderByUsaStateId($order = Criteria::ASC) Order by the usa_state_id column
 * @method     ChildCustomerAddressQuery orderByAddressL1($order = Criteria::ASC) Order by the address_l1 column
 * @method     ChildCustomerAddressQuery orderByAddressL2($order = Criteria::ASC) Order by the address_l2 column
 *
 * @method     ChildCustomerAddressQuery groupById() Group by the id column
 * @method     ChildCustomerAddressQuery groupByCustomerId() Group by the customer_id column
 * @method     ChildCustomerAddressQuery groupByCustomerAddressTypeId() Group by the customer_address_type_id column
 * @method     ChildCustomerAddressQuery groupByIsDefault() Group by the is_default column
 * @method     ChildCustomerAddressQuery groupByCompanyName() Group by the company_name column
 * @method     ChildCustomerAddressQuery groupByAttnName() Group by the attn_name column
 * @method     ChildCustomerAddressQuery groupByStreet() Group by the street column
 * @method     ChildCustomerAddressQuery groupByNumber() Group by the number column
 * @method     ChildCustomerAddressQuery groupByNumberAdd() Group by the number_add column
 * @method     ChildCustomerAddressQuery groupByPostal() Group by the postal column
 * @method     ChildCustomerAddressQuery groupByCity() Group by the city column
 * @method     ChildCustomerAddressQuery groupByRegion() Group by the region column
 * @method     ChildCustomerAddressQuery groupByCountryId() Group by the country_id column
 * @method     ChildCustomerAddressQuery groupByUsaStateId() Group by the usa_state_id column
 * @method     ChildCustomerAddressQuery groupByAddressL1() Group by the address_l1 column
 * @method     ChildCustomerAddressQuery groupByAddressL2() Group by the address_l2 column
 *
 * @method     ChildCustomerAddressQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCustomerAddressQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCustomerAddressQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCustomerAddressQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCustomerAddressQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCustomerAddressQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCustomerAddressQuery leftJoinCountry($relationAlias = null) Adds a LEFT JOIN clause to the query using the Country relation
 * @method     ChildCustomerAddressQuery rightJoinCountry($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Country relation
 * @method     ChildCustomerAddressQuery innerJoinCountry($relationAlias = null) Adds a INNER JOIN clause to the query using the Country relation
 *
 * @method     ChildCustomerAddressQuery joinWithCountry($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Country relation
 *
 * @method     ChildCustomerAddressQuery leftJoinWithCountry() Adds a LEFT JOIN clause and with to the query using the Country relation
 * @method     ChildCustomerAddressQuery rightJoinWithCountry() Adds a RIGHT JOIN clause and with to the query using the Country relation
 * @method     ChildCustomerAddressQuery innerJoinWithCountry() Adds a INNER JOIN clause and with to the query using the Country relation
 *
 * @method     ChildCustomerAddressQuery leftJoinUsaState($relationAlias = null) Adds a LEFT JOIN clause to the query using the UsaState relation
 * @method     ChildCustomerAddressQuery rightJoinUsaState($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UsaState relation
 * @method     ChildCustomerAddressQuery innerJoinUsaState($relationAlias = null) Adds a INNER JOIN clause to the query using the UsaState relation
 *
 * @method     ChildCustomerAddressQuery joinWithUsaState($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UsaState relation
 *
 * @method     ChildCustomerAddressQuery leftJoinWithUsaState() Adds a LEFT JOIN clause and with to the query using the UsaState relation
 * @method     ChildCustomerAddressQuery rightJoinWithUsaState() Adds a RIGHT JOIN clause and with to the query using the UsaState relation
 * @method     ChildCustomerAddressQuery innerJoinWithUsaState() Adds a INNER JOIN clause and with to the query using the UsaState relation
 *
 * @method     ChildCustomerAddressQuery leftJoinCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Customer relation
 * @method     ChildCustomerAddressQuery rightJoinCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Customer relation
 * @method     ChildCustomerAddressQuery innerJoinCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the Customer relation
 *
 * @method     ChildCustomerAddressQuery joinWithCustomer($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Customer relation
 *
 * @method     ChildCustomerAddressQuery leftJoinWithCustomer() Adds a LEFT JOIN clause and with to the query using the Customer relation
 * @method     ChildCustomerAddressQuery rightJoinWithCustomer() Adds a RIGHT JOIN clause and with to the query using the Customer relation
 * @method     ChildCustomerAddressQuery innerJoinWithCustomer() Adds a INNER JOIN clause and with to the query using the Customer relation
 *
 * @method     ChildCustomerAddressQuery leftJoinCustomerAddressType($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomerAddressType relation
 * @method     ChildCustomerAddressQuery rightJoinCustomerAddressType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomerAddressType relation
 * @method     ChildCustomerAddressQuery innerJoinCustomerAddressType($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomerAddressType relation
 *
 * @method     ChildCustomerAddressQuery joinWithCustomerAddressType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomerAddressType relation
 *
 * @method     ChildCustomerAddressQuery leftJoinWithCustomerAddressType() Adds a LEFT JOIN clause and with to the query using the CustomerAddressType relation
 * @method     ChildCustomerAddressQuery rightJoinWithCustomerAddressType() Adds a RIGHT JOIN clause and with to the query using the CustomerAddressType relation
 * @method     ChildCustomerAddressQuery innerJoinWithCustomerAddressType() Adds a INNER JOIN clause and with to the query using the CustomerAddressType relation
 *
 * @method     \Model\Setting\MasterTable\CountryQuery|\Model\Setting\MasterTable\UsaStateQuery|\Model\Crm\CustomerQuery|\Model\Crm\CustomerAddressTypeQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCustomerAddress findOne(ConnectionInterface $con = null) Return the first ChildCustomerAddress matching the query
 * @method     ChildCustomerAddress findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCustomerAddress matching the query, or a new ChildCustomerAddress object populated from the query conditions when no match is found
 *
 * @method     ChildCustomerAddress findOneById(int $id) Return the first ChildCustomerAddress filtered by the id column
 * @method     ChildCustomerAddress findOneByCustomerId(int $customer_id) Return the first ChildCustomerAddress filtered by the customer_id column
 * @method     ChildCustomerAddress findOneByCustomerAddressTypeId(int $customer_address_type_id) Return the first ChildCustomerAddress filtered by the customer_address_type_id column
 * @method     ChildCustomerAddress findOneByIsDefault(int $is_default) Return the first ChildCustomerAddress filtered by the is_default column
 * @method     ChildCustomerAddress findOneByCompanyName(string $company_name) Return the first ChildCustomerAddress filtered by the company_name column
 * @method     ChildCustomerAddress findOneByAttnName(string $attn_name) Return the first ChildCustomerAddress filtered by the attn_name column
 * @method     ChildCustomerAddress findOneByStreet(string $street) Return the first ChildCustomerAddress filtered by the street column
 * @method     ChildCustomerAddress findOneByNumber(string $number) Return the first ChildCustomerAddress filtered by the number column
 * @method     ChildCustomerAddress findOneByNumberAdd(string $number_add) Return the first ChildCustomerAddress filtered by the number_add column
 * @method     ChildCustomerAddress findOneByPostal(string $postal) Return the first ChildCustomerAddress filtered by the postal column
 * @method     ChildCustomerAddress findOneByCity(string $city) Return the first ChildCustomerAddress filtered by the city column
 * @method     ChildCustomerAddress findOneByRegion(string $region) Return the first ChildCustomerAddress filtered by the region column
 * @method     ChildCustomerAddress findOneByCountryId(int $country_id) Return the first ChildCustomerAddress filtered by the country_id column
 * @method     ChildCustomerAddress findOneByUsaStateId(int $usa_state_id) Return the first ChildCustomerAddress filtered by the usa_state_id column
 * @method     ChildCustomerAddress findOneByAddressL1(string $address_l1) Return the first ChildCustomerAddress filtered by the address_l1 column
 * @method     ChildCustomerAddress findOneByAddressL2(string $address_l2) Return the first ChildCustomerAddress filtered by the address_l2 column *

 * @method     ChildCustomerAddress requirePk($key, ConnectionInterface $con = null) Return the ChildCustomerAddress by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOne(ConnectionInterface $con = null) Return the first ChildCustomerAddress matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomerAddress requireOneById(int $id) Return the first ChildCustomerAddress filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByCustomerId(int $customer_id) Return the first ChildCustomerAddress filtered by the customer_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByCustomerAddressTypeId(int $customer_address_type_id) Return the first ChildCustomerAddress filtered by the customer_address_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByIsDefault(int $is_default) Return the first ChildCustomerAddress filtered by the is_default column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByCompanyName(string $company_name) Return the first ChildCustomerAddress filtered by the company_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByAttnName(string $attn_name) Return the first ChildCustomerAddress filtered by the attn_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByStreet(string $street) Return the first ChildCustomerAddress filtered by the street column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByNumber(string $number) Return the first ChildCustomerAddress filtered by the number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByNumberAdd(string $number_add) Return the first ChildCustomerAddress filtered by the number_add column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByPostal(string $postal) Return the first ChildCustomerAddress filtered by the postal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByCity(string $city) Return the first ChildCustomerAddress filtered by the city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByRegion(string $region) Return the first ChildCustomerAddress filtered by the region column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByCountryId(int $country_id) Return the first ChildCustomerAddress filtered by the country_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByUsaStateId(int $usa_state_id) Return the first ChildCustomerAddress filtered by the usa_state_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByAddressL1(string $address_l1) Return the first ChildCustomerAddress filtered by the address_l1 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerAddress requireOneByAddressL2(string $address_l2) Return the first ChildCustomerAddress filtered by the address_l2 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomerAddress[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCustomerAddress objects based on current ModelCriteria
 * @method     ChildCustomerAddress[]|ObjectCollection findById(int $id) Return ChildCustomerAddress objects filtered by the id column
 * @method     ChildCustomerAddress[]|ObjectCollection findByCustomerId(int $customer_id) Return ChildCustomerAddress objects filtered by the customer_id column
 * @method     ChildCustomerAddress[]|ObjectCollection findByCustomerAddressTypeId(int $customer_address_type_id) Return ChildCustomerAddress objects filtered by the customer_address_type_id column
 * @method     ChildCustomerAddress[]|ObjectCollection findByIsDefault(int $is_default) Return ChildCustomerAddress objects filtered by the is_default column
 * @method     ChildCustomerAddress[]|ObjectCollection findByCompanyName(string $company_name) Return ChildCustomerAddress objects filtered by the company_name column
 * @method     ChildCustomerAddress[]|ObjectCollection findByAttnName(string $attn_name) Return ChildCustomerAddress objects filtered by the attn_name column
 * @method     ChildCustomerAddress[]|ObjectCollection findByStreet(string $street) Return ChildCustomerAddress objects filtered by the street column
 * @method     ChildCustomerAddress[]|ObjectCollection findByNumber(string $number) Return ChildCustomerAddress objects filtered by the number column
 * @method     ChildCustomerAddress[]|ObjectCollection findByNumberAdd(string $number_add) Return ChildCustomerAddress objects filtered by the number_add column
 * @method     ChildCustomerAddress[]|ObjectCollection findByPostal(string $postal) Return ChildCustomerAddress objects filtered by the postal column
 * @method     ChildCustomerAddress[]|ObjectCollection findByCity(string $city) Return ChildCustomerAddress objects filtered by the city column
 * @method     ChildCustomerAddress[]|ObjectCollection findByRegion(string $region) Return ChildCustomerAddress objects filtered by the region column
 * @method     ChildCustomerAddress[]|ObjectCollection findByCountryId(int $country_id) Return ChildCustomerAddress objects filtered by the country_id column
 * @method     ChildCustomerAddress[]|ObjectCollection findByUsaStateId(int $usa_state_id) Return ChildCustomerAddress objects filtered by the usa_state_id column
 * @method     ChildCustomerAddress[]|ObjectCollection findByAddressL1(string $address_l1) Return ChildCustomerAddress objects filtered by the address_l1 column
 * @method     ChildCustomerAddress[]|ObjectCollection findByAddressL2(string $address_l2) Return ChildCustomerAddress objects filtered by the address_l2 column
 * @method     ChildCustomerAddress[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CustomerAddressQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Crm\Base\CustomerAddressQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Crm\\CustomerAddress', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCustomerAddressQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCustomerAddressQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCustomerAddressQuery) {
            return $criteria;
        }
        $query = new ChildCustomerAddressQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCustomerAddress|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CustomerAddressTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CustomerAddressTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerAddress A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, customer_id, customer_address_type_id, is_default, company_name, attn_name, street, number, number_add, postal, city, region, country_id, usa_state_id, address_l1, address_l2 FROM customer_address WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCustomerAddress $obj */
            $obj = new ChildCustomerAddress();
            $obj->hydrate($row);
            CustomerAddressTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCustomerAddress|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CustomerAddressTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CustomerAddressTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CustomerAddressTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CustomerAddressTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the customer_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerId(1234); // WHERE customer_id = 1234
     * $query->filterByCustomerId(array(12, 34)); // WHERE customer_id IN (12, 34)
     * $query->filterByCustomerId(array('min' => 12)); // WHERE customer_id > 12
     * </code>
     *
     * @see       filterByCustomer()
     *
     * @param     mixed $customerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByCustomerId($customerId = null, $comparison = null)
    {
        if (is_array($customerId)) {
            $useMinMax = false;
            if (isset($customerId['min'])) {
                $this->addUsingAlias(CustomerAddressTableMap::COL_CUSTOMER_ID, $customerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerId['max'])) {
                $this->addUsingAlias(CustomerAddressTableMap::COL_CUSTOMER_ID, $customerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_CUSTOMER_ID, $customerId, $comparison);
    }

    /**
     * Filter the query on the customer_address_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerAddressTypeId(1234); // WHERE customer_address_type_id = 1234
     * $query->filterByCustomerAddressTypeId(array(12, 34)); // WHERE customer_address_type_id IN (12, 34)
     * $query->filterByCustomerAddressTypeId(array('min' => 12)); // WHERE customer_address_type_id > 12
     * </code>
     *
     * @see       filterByCustomerAddressType()
     *
     * @param     mixed $customerAddressTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByCustomerAddressTypeId($customerAddressTypeId = null, $comparison = null)
    {
        if (is_array($customerAddressTypeId)) {
            $useMinMax = false;
            if (isset($customerAddressTypeId['min'])) {
                $this->addUsingAlias(CustomerAddressTableMap::COL_CUSTOMER_ADDRESS_TYPE_ID, $customerAddressTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerAddressTypeId['max'])) {
                $this->addUsingAlias(CustomerAddressTableMap::COL_CUSTOMER_ADDRESS_TYPE_ID, $customerAddressTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_CUSTOMER_ADDRESS_TYPE_ID, $customerAddressTypeId, $comparison);
    }

    /**
     * Filter the query on the is_default column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDefault(1234); // WHERE is_default = 1234
     * $query->filterByIsDefault(array(12, 34)); // WHERE is_default IN (12, 34)
     * $query->filterByIsDefault(array('min' => 12)); // WHERE is_default > 12
     * </code>
     *
     * @param     mixed $isDefault The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByIsDefault($isDefault = null, $comparison = null)
    {
        if (is_array($isDefault)) {
            $useMinMax = false;
            if (isset($isDefault['min'])) {
                $this->addUsingAlias(CustomerAddressTableMap::COL_IS_DEFAULT, $isDefault['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($isDefault['max'])) {
                $this->addUsingAlias(CustomerAddressTableMap::COL_IS_DEFAULT, $isDefault['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_IS_DEFAULT, $isDefault, $comparison);
    }

    /**
     * Filter the query on the company_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCompanyName('fooValue');   // WHERE company_name = 'fooValue'
     * $query->filterByCompanyName('%fooValue%', Criteria::LIKE); // WHERE company_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $companyName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByCompanyName($companyName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($companyName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_COMPANY_NAME, $companyName, $comparison);
    }

    /**
     * Filter the query on the attn_name column
     *
     * Example usage:
     * <code>
     * $query->filterByAttnName('fooValue');   // WHERE attn_name = 'fooValue'
     * $query->filterByAttnName('%fooValue%', Criteria::LIKE); // WHERE attn_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $attnName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByAttnName($attnName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($attnName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_ATTN_NAME, $attnName, $comparison);
    }

    /**
     * Filter the query on the street column
     *
     * Example usage:
     * <code>
     * $query->filterByStreet('fooValue');   // WHERE street = 'fooValue'
     * $query->filterByStreet('%fooValue%', Criteria::LIKE); // WHERE street LIKE '%fooValue%'
     * </code>
     *
     * @param     string $street The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByStreet($street = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($street)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_STREET, $street, $comparison);
    }

    /**
     * Filter the query on the number column
     *
     * Example usage:
     * <code>
     * $query->filterByNumber('fooValue');   // WHERE number = 'fooValue'
     * $query->filterByNumber('%fooValue%', Criteria::LIKE); // WHERE number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $number The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByNumber($number = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($number)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_NUMBER, $number, $comparison);
    }

    /**
     * Filter the query on the number_add column
     *
     * Example usage:
     * <code>
     * $query->filterByNumberAdd('fooValue');   // WHERE number_add = 'fooValue'
     * $query->filterByNumberAdd('%fooValue%', Criteria::LIKE); // WHERE number_add LIKE '%fooValue%'
     * </code>
     *
     * @param     string $numberAdd The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByNumberAdd($numberAdd = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($numberAdd)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_NUMBER_ADD, $numberAdd, $comparison);
    }

    /**
     * Filter the query on the postal column
     *
     * Example usage:
     * <code>
     * $query->filterByPostal('fooValue');   // WHERE postal = 'fooValue'
     * $query->filterByPostal('%fooValue%', Criteria::LIKE); // WHERE postal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $postal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByPostal($postal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($postal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_POSTAL, $postal, $comparison);
    }

    /**
     * Filter the query on the city column
     *
     * Example usage:
     * <code>
     * $query->filterByCity('fooValue');   // WHERE city = 'fooValue'
     * $query->filterByCity('%fooValue%', Criteria::LIKE); // WHERE city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $city The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByCity($city = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($city)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_CITY, $city, $comparison);
    }

    /**
     * Filter the query on the region column
     *
     * Example usage:
     * <code>
     * $query->filterByRegion('fooValue');   // WHERE region = 'fooValue'
     * $query->filterByRegion('%fooValue%', Criteria::LIKE); // WHERE region LIKE '%fooValue%'
     * </code>
     *
     * @param     string $region The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByRegion($region = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($region)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_REGION, $region, $comparison);
    }

    /**
     * Filter the query on the country_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCountryId(1234); // WHERE country_id = 1234
     * $query->filterByCountryId(array(12, 34)); // WHERE country_id IN (12, 34)
     * $query->filterByCountryId(array('min' => 12)); // WHERE country_id > 12
     * </code>
     *
     * @see       filterByCountry()
     *
     * @param     mixed $countryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByCountryId($countryId = null, $comparison = null)
    {
        if (is_array($countryId)) {
            $useMinMax = false;
            if (isset($countryId['min'])) {
                $this->addUsingAlias(CustomerAddressTableMap::COL_COUNTRY_ID, $countryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($countryId['max'])) {
                $this->addUsingAlias(CustomerAddressTableMap::COL_COUNTRY_ID, $countryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_COUNTRY_ID, $countryId, $comparison);
    }

    /**
     * Filter the query on the usa_state_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUsaStateId(1234); // WHERE usa_state_id = 1234
     * $query->filterByUsaStateId(array(12, 34)); // WHERE usa_state_id IN (12, 34)
     * $query->filterByUsaStateId(array('min' => 12)); // WHERE usa_state_id > 12
     * </code>
     *
     * @see       filterByUsaState()
     *
     * @param     mixed $usaStateId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByUsaStateId($usaStateId = null, $comparison = null)
    {
        if (is_array($usaStateId)) {
            $useMinMax = false;
            if (isset($usaStateId['min'])) {
                $this->addUsingAlias(CustomerAddressTableMap::COL_USA_STATE_ID, $usaStateId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usaStateId['max'])) {
                $this->addUsingAlias(CustomerAddressTableMap::COL_USA_STATE_ID, $usaStateId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_USA_STATE_ID, $usaStateId, $comparison);
    }

    /**
     * Filter the query on the address_l1 column
     *
     * Example usage:
     * <code>
     * $query->filterByAddressL1('fooValue');   // WHERE address_l1 = 'fooValue'
     * $query->filterByAddressL1('%fooValue%', Criteria::LIKE); // WHERE address_l1 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $addressL1 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByAddressL1($addressL1 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($addressL1)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_ADDRESS_L1, $addressL1, $comparison);
    }

    /**
     * Filter the query on the address_l2 column
     *
     * Example usage:
     * <code>
     * $query->filterByAddressL2('fooValue');   // WHERE address_l2 = 'fooValue'
     * $query->filterByAddressL2('%fooValue%', Criteria::LIKE); // WHERE address_l2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $addressL2 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByAddressL2($addressL2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($addressL2)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerAddressTableMap::COL_ADDRESS_L2, $addressL2, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Country object
     *
     * @param \Model\Setting\MasterTable\Country|ObjectCollection $country The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByCountry($country, $comparison = null)
    {
        if ($country instanceof \Model\Setting\MasterTable\Country) {
            return $this
                ->addUsingAlias(CustomerAddressTableMap::COL_COUNTRY_ID, $country->getId(), $comparison);
        } elseif ($country instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomerAddressTableMap::COL_COUNTRY_ID, $country->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCountry() only accepts arguments of type \Model\Setting\MasterTable\Country or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Country relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function joinCountry($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Country');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Country');
        }

        return $this;
    }

    /**
     * Use the Country relation Country object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\CountryQuery A secondary query class using the current class as primary query
     */
    public function useCountryQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCountry($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Country', '\Model\Setting\MasterTable\CountryQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\UsaState object
     *
     * @param \Model\Setting\MasterTable\UsaState|ObjectCollection $usaState The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByUsaState($usaState, $comparison = null)
    {
        if ($usaState instanceof \Model\Setting\MasterTable\UsaState) {
            return $this
                ->addUsingAlias(CustomerAddressTableMap::COL_USA_STATE_ID, $usaState->getId(), $comparison);
        } elseif ($usaState instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomerAddressTableMap::COL_USA_STATE_ID, $usaState->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsaState() only accepts arguments of type \Model\Setting\MasterTable\UsaState or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UsaState relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function joinUsaState($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UsaState');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UsaState');
        }

        return $this;
    }

    /**
     * Use the UsaState relation UsaState object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\UsaStateQuery A secondary query class using the current class as primary query
     */
    public function useUsaStateQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUsaState($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UsaState', '\Model\Setting\MasterTable\UsaStateQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\Customer object
     *
     * @param \Model\Crm\Customer|ObjectCollection $customer The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByCustomer($customer, $comparison = null)
    {
        if ($customer instanceof \Model\Crm\Customer) {
            return $this
                ->addUsingAlias(CustomerAddressTableMap::COL_CUSTOMER_ID, $customer->getId(), $comparison);
        } elseif ($customer instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomerAddressTableMap::COL_CUSTOMER_ID, $customer->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCustomer() only accepts arguments of type \Model\Crm\Customer or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Customer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function joinCustomer($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Customer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Customer');
        }

        return $this;
    }

    /**
     * Use the Customer relation Customer object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerQuery A secondary query class using the current class as primary query
     */
    public function useCustomerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCustomer($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Customer', '\Model\Crm\CustomerQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\CustomerAddressType object
     *
     * @param \Model\Crm\CustomerAddressType|ObjectCollection $customerAddressType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function filterByCustomerAddressType($customerAddressType, $comparison = null)
    {
        if ($customerAddressType instanceof \Model\Crm\CustomerAddressType) {
            return $this
                ->addUsingAlias(CustomerAddressTableMap::COL_CUSTOMER_ADDRESS_TYPE_ID, $customerAddressType->getId(), $comparison);
        } elseif ($customerAddressType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomerAddressTableMap::COL_CUSTOMER_ADDRESS_TYPE_ID, $customerAddressType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCustomerAddressType() only accepts arguments of type \Model\Crm\CustomerAddressType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomerAddressType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function joinCustomerAddressType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomerAddressType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomerAddressType');
        }

        return $this;
    }

    /**
     * Use the CustomerAddressType relation CustomerAddressType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerAddressTypeQuery A secondary query class using the current class as primary query
     */
    public function useCustomerAddressTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCustomerAddressType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomerAddressType', '\Model\Crm\CustomerAddressTypeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCustomerAddress $customerAddress Object to remove from the list of results
     *
     * @return $this|ChildCustomerAddressQuery The current query, for fluid interface
     */
    public function prune($customerAddress = null)
    {
        if ($customerAddress) {
            $this->addUsingAlias(CustomerAddressTableMap::COL_ID, $customerAddress->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the customer_address table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerAddressTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CustomerAddressTableMap::clearInstancePool();
            CustomerAddressTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerAddressTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CustomerAddressTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CustomerAddressTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CustomerAddressTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CustomerAddressQuery
