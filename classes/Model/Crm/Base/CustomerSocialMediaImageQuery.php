<?php

namespace Model\Crm\Base;

use \Exception;
use \PDO;
use Model\Crm\CustomerSocialMediaImage as ChildCustomerSocialMediaImage;
use Model\Crm\CustomerSocialMediaImageQuery as ChildCustomerSocialMediaImageQuery;
use Model\Crm\Map\CustomerSocialMediaImageTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'customer_social_media_image' table.
 *
 *
 *
 * @method     ChildCustomerSocialMediaImageQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCustomerSocialMediaImageQuery orderByCustomerId($order = Criteria::ASC) Order by the customer_id column
 * @method     ChildCustomerSocialMediaImageQuery orderBySocialNetwork($order = Criteria::ASC) Order by the social_network column
 * @method     ChildCustomerSocialMediaImageQuery orderByImageLocation($order = Criteria::ASC) Order by the image_location column
 * @method     ChildCustomerSocialMediaImageQuery orderByOriginalName($order = Criteria::ASC) Order by the original_name column
 * @method     ChildCustomerSocialMediaImageQuery orderByFileExt($order = Criteria::ASC) Order by the file_ext column
 * @method     ChildCustomerSocialMediaImageQuery orderByMime($order = Criteria::ASC) Order by the mime column
 *
 * @method     ChildCustomerSocialMediaImageQuery groupById() Group by the id column
 * @method     ChildCustomerSocialMediaImageQuery groupByCustomerId() Group by the customer_id column
 * @method     ChildCustomerSocialMediaImageQuery groupBySocialNetwork() Group by the social_network column
 * @method     ChildCustomerSocialMediaImageQuery groupByImageLocation() Group by the image_location column
 * @method     ChildCustomerSocialMediaImageQuery groupByOriginalName() Group by the original_name column
 * @method     ChildCustomerSocialMediaImageQuery groupByFileExt() Group by the file_ext column
 * @method     ChildCustomerSocialMediaImageQuery groupByMime() Group by the mime column
 *
 * @method     ChildCustomerSocialMediaImageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCustomerSocialMediaImageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCustomerSocialMediaImageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCustomerSocialMediaImageQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCustomerSocialMediaImageQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCustomerSocialMediaImageQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCustomerSocialMediaImage findOne(ConnectionInterface $con = null) Return the first ChildCustomerSocialMediaImage matching the query
 * @method     ChildCustomerSocialMediaImage findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCustomerSocialMediaImage matching the query, or a new ChildCustomerSocialMediaImage object populated from the query conditions when no match is found
 *
 * @method     ChildCustomerSocialMediaImage findOneById(int $id) Return the first ChildCustomerSocialMediaImage filtered by the id column
 * @method     ChildCustomerSocialMediaImage findOneByCustomerId(int $customer_id) Return the first ChildCustomerSocialMediaImage filtered by the customer_id column
 * @method     ChildCustomerSocialMediaImage findOneBySocialNetwork(string $social_network) Return the first ChildCustomerSocialMediaImage filtered by the social_network column
 * @method     ChildCustomerSocialMediaImage findOneByImageLocation(string $image_location) Return the first ChildCustomerSocialMediaImage filtered by the image_location column
 * @method     ChildCustomerSocialMediaImage findOneByOriginalName(string $original_name) Return the first ChildCustomerSocialMediaImage filtered by the original_name column
 * @method     ChildCustomerSocialMediaImage findOneByFileExt(string $file_ext) Return the first ChildCustomerSocialMediaImage filtered by the file_ext column
 * @method     ChildCustomerSocialMediaImage findOneByMime(string $mime) Return the first ChildCustomerSocialMediaImage filtered by the mime column *

 * @method     ChildCustomerSocialMediaImage requirePk($key, ConnectionInterface $con = null) Return the ChildCustomerSocialMediaImage by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerSocialMediaImage requireOne(ConnectionInterface $con = null) Return the first ChildCustomerSocialMediaImage matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomerSocialMediaImage requireOneById(int $id) Return the first ChildCustomerSocialMediaImage filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerSocialMediaImage requireOneByCustomerId(int $customer_id) Return the first ChildCustomerSocialMediaImage filtered by the customer_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerSocialMediaImage requireOneBySocialNetwork(string $social_network) Return the first ChildCustomerSocialMediaImage filtered by the social_network column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerSocialMediaImage requireOneByImageLocation(string $image_location) Return the first ChildCustomerSocialMediaImage filtered by the image_location column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerSocialMediaImage requireOneByOriginalName(string $original_name) Return the first ChildCustomerSocialMediaImage filtered by the original_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerSocialMediaImage requireOneByFileExt(string $file_ext) Return the first ChildCustomerSocialMediaImage filtered by the file_ext column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerSocialMediaImage requireOneByMime(string $mime) Return the first ChildCustomerSocialMediaImage filtered by the mime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomerSocialMediaImage[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCustomerSocialMediaImage objects based on current ModelCriteria
 * @method     ChildCustomerSocialMediaImage[]|ObjectCollection findById(int $id) Return ChildCustomerSocialMediaImage objects filtered by the id column
 * @method     ChildCustomerSocialMediaImage[]|ObjectCollection findByCustomerId(int $customer_id) Return ChildCustomerSocialMediaImage objects filtered by the customer_id column
 * @method     ChildCustomerSocialMediaImage[]|ObjectCollection findBySocialNetwork(string $social_network) Return ChildCustomerSocialMediaImage objects filtered by the social_network column
 * @method     ChildCustomerSocialMediaImage[]|ObjectCollection findByImageLocation(string $image_location) Return ChildCustomerSocialMediaImage objects filtered by the image_location column
 * @method     ChildCustomerSocialMediaImage[]|ObjectCollection findByOriginalName(string $original_name) Return ChildCustomerSocialMediaImage objects filtered by the original_name column
 * @method     ChildCustomerSocialMediaImage[]|ObjectCollection findByFileExt(string $file_ext) Return ChildCustomerSocialMediaImage objects filtered by the file_ext column
 * @method     ChildCustomerSocialMediaImage[]|ObjectCollection findByMime(string $mime) Return ChildCustomerSocialMediaImage objects filtered by the mime column
 * @method     ChildCustomerSocialMediaImage[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CustomerSocialMediaImageQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Crm\Base\CustomerSocialMediaImageQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Crm\\CustomerSocialMediaImage', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCustomerSocialMediaImageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCustomerSocialMediaImageQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCustomerSocialMediaImageQuery) {
            return $criteria;
        }
        $query = new ChildCustomerSocialMediaImageQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCustomerSocialMediaImage|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CustomerSocialMediaImageTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CustomerSocialMediaImageTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerSocialMediaImage A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, customer_id, social_network, image_location, original_name, file_ext, mime FROM customer_social_media_image WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCustomerSocialMediaImage $obj */
            $obj = new ChildCustomerSocialMediaImage();
            $obj->hydrate($row);
            CustomerSocialMediaImageTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCustomerSocialMediaImage|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCustomerSocialMediaImageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCustomerSocialMediaImageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerSocialMediaImageQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the customer_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerId(1234); // WHERE customer_id = 1234
     * $query->filterByCustomerId(array(12, 34)); // WHERE customer_id IN (12, 34)
     * $query->filterByCustomerId(array('min' => 12)); // WHERE customer_id > 12
     * </code>
     *
     * @param     mixed $customerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerSocialMediaImageQuery The current query, for fluid interface
     */
    public function filterByCustomerId($customerId = null, $comparison = null)
    {
        if (is_array($customerId)) {
            $useMinMax = false;
            if (isset($customerId['min'])) {
                $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_CUSTOMER_ID, $customerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerId['max'])) {
                $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_CUSTOMER_ID, $customerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_CUSTOMER_ID, $customerId, $comparison);
    }

    /**
     * Filter the query on the social_network column
     *
     * Example usage:
     * <code>
     * $query->filterBySocialNetwork('fooValue');   // WHERE social_network = 'fooValue'
     * $query->filterBySocialNetwork('%fooValue%', Criteria::LIKE); // WHERE social_network LIKE '%fooValue%'
     * </code>
     *
     * @param     string $socialNetwork The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerSocialMediaImageQuery The current query, for fluid interface
     */
    public function filterBySocialNetwork($socialNetwork = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($socialNetwork)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_SOCIAL_NETWORK, $socialNetwork, $comparison);
    }

    /**
     * Filter the query on the image_location column
     *
     * Example usage:
     * <code>
     * $query->filterByImageLocation('fooValue');   // WHERE image_location = 'fooValue'
     * $query->filterByImageLocation('%fooValue%', Criteria::LIKE); // WHERE image_location LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageLocation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerSocialMediaImageQuery The current query, for fluid interface
     */
    public function filterByImageLocation($imageLocation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageLocation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_IMAGE_LOCATION, $imageLocation, $comparison);
    }

    /**
     * Filter the query on the original_name column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginalName('fooValue');   // WHERE original_name = 'fooValue'
     * $query->filterByOriginalName('%fooValue%', Criteria::LIKE); // WHERE original_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $originalName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerSocialMediaImageQuery The current query, for fluid interface
     */
    public function filterByOriginalName($originalName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($originalName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_ORIGINAL_NAME, $originalName, $comparison);
    }

    /**
     * Filter the query on the file_ext column
     *
     * Example usage:
     * <code>
     * $query->filterByFileExt('fooValue');   // WHERE file_ext = 'fooValue'
     * $query->filterByFileExt('%fooValue%', Criteria::LIKE); // WHERE file_ext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fileExt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerSocialMediaImageQuery The current query, for fluid interface
     */
    public function filterByFileExt($fileExt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fileExt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_FILE_EXT, $fileExt, $comparison);
    }

    /**
     * Filter the query on the mime column
     *
     * Example usage:
     * <code>
     * $query->filterByMime('fooValue');   // WHERE mime = 'fooValue'
     * $query->filterByMime('%fooValue%', Criteria::LIKE); // WHERE mime LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mime The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerSocialMediaImageQuery The current query, for fluid interface
     */
    public function filterByMime($mime = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mime)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_MIME, $mime, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCustomerSocialMediaImage $customerSocialMediaImage Object to remove from the list of results
     *
     * @return $this|ChildCustomerSocialMediaImageQuery The current query, for fluid interface
     */
    public function prune($customerSocialMediaImage = null)
    {
        if ($customerSocialMediaImage) {
            $this->addUsingAlias(CustomerSocialMediaImageTableMap::COL_ID, $customerSocialMediaImage->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the customer_social_media_image table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerSocialMediaImageTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CustomerSocialMediaImageTableMap::clearInstancePool();
            CustomerSocialMediaImageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerSocialMediaImageTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CustomerSocialMediaImageTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CustomerSocialMediaImageTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CustomerSocialMediaImageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CustomerSocialMediaImageQuery
