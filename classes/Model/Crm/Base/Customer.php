<?php

namespace Model\Crm\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\ContactMessage;
use Model\ContactMessageQuery;
use Model\Base\ContactMessage as BaseContactMessage;
use Model\Category\CustomerWishlist;
use Model\Category\CustomerWishlistQuery;
use Model\Category\Base\CustomerWishlist as BaseCustomerWishlist;
use Model\Category\Map\CustomerWishlistTableMap;
use Model\Crm\Customer as ChildCustomer;
use Model\Crm\CustomerAddress as ChildCustomerAddress;
use Model\Crm\CustomerAddressQuery as ChildCustomerAddressQuery;
use Model\Crm\CustomerCompany as ChildCustomerCompany;
use Model\Crm\CustomerCompanyQuery as ChildCustomerCompanyQuery;
use Model\Crm\CustomerLogo as ChildCustomerLogo;
use Model\Crm\CustomerLogoQuery as ChildCustomerLogoQuery;
use Model\Crm\CustomerNote as ChildCustomerNote;
use Model\Crm\CustomerNoteQuery as ChildCustomerNoteQuery;
use Model\Crm\CustomerPhoto as ChildCustomerPhoto;
use Model\Crm\CustomerPhotoQuery as ChildCustomerPhotoQuery;
use Model\Crm\CustomerProperty as ChildCustomerProperty;
use Model\Crm\CustomerPropertyQuery as ChildCustomerPropertyQuery;
use Model\Crm\CustomerQuery as ChildCustomerQuery;
use Model\Crm\CustomerSector as ChildCustomerSector;
use Model\Crm\CustomerSectorQuery as ChildCustomerSectorQuery;
use Model\Crm\CustomerType as ChildCustomerType;
use Model\Crm\CustomerTypeQuery as ChildCustomerTypeQuery;
use Model\Crm\NewsletterSubscriber as ChildNewsletterSubscriber;
use Model\Crm\NewsletterSubscriberQuery as ChildNewsletterSubscriberQuery;
use Model\Crm\Map\CustomerAddressTableMap;
use Model\Crm\Map\CustomerCompanyTableMap;
use Model\Crm\Map\CustomerLogoTableMap;
use Model\Crm\Map\CustomerNoteTableMap;
use Model\Crm\Map\CustomerPhotoTableMap;
use Model\Crm\Map\CustomerPropertyTableMap;
use Model\Crm\Map\CustomerTableMap;
use Model\Crm\Map\NewsletterSubscriberTableMap;
use Model\Finance\SaleInvoice;
use Model\Finance\SaleInvoiceQuery;
use Model\Finance\Base\SaleInvoice as BaseSaleInvoice;
use Model\Finance\Map\SaleInvoiceTableMap;
use Model\Map\ContactMessageTableMap;
use Model\Setting\MasterTable\Branche;
use Model\Setting\MasterTable\BrancheQuery;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;
use Model\Setting\MasterTable\PayTerm;
use Model\Setting\MasterTable\PayTermQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'customer' table.
 *
 *
 *
 * @package    propel.generator.Model.Crm.Base
 */
abstract class Customer implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Crm\\Map\\CustomerTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the import_source field.
     *
     * @var        string|null
     */
    protected $import_source;

    /**
     * The value for the accounting_id field.
     *
     * @var        string|null
     */
    protected $accounting_id;

    /**
     * The value for the old_id field.
     *
     * @var        string|null
     */
    protected $old_id;

    /**
     * The value for the ip field.
     *
     * @var        string|null
     */
    protected $ip;

    /**
     * The value for the created_on field.
     *
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        DateTime|null
     */
    protected $created_on;

    /**
     * The value for the is_deleted field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_deleted;

    /**
     * The value for the is_newsletter_subscriber field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_newsletter_subscriber;

    /**
     * The value for the language_id field.
     *
     * @var        int|null
     */
    protected $language_id;

    /**
     * The value for the is_intracommunautair field.
     *
     * @var        int|null
     */
    protected $is_intracommunautair;

    /**
     * The value for the payterm_id field.
     *
     * @var        int|null
     */
    protected $payterm_id;

    /**
     * The value for the branche_id field.
     *
     * @var        int|null
     */
    protected $branche_id;

    /**
     * The value for the sector_id field.
     *
     * @var        int|null
     */
    protected $sector_id;

    /**
     * The value for the customer_type_id field.
     *
     * @var        int|null
     */
    protected $customer_type_id;

    /**
     * The value for the debitor field.
     *
     * @var        string|null
     */
    protected $debitor;

    /**
     * The value for the gender field.
     *
     * @var        string|null
     */
    protected $gender;

    /**
     * The value for the first_name field.
     *
     * @var        string|null
     */
    protected $first_name;

    /**
     * The value for the insertion field.
     *
     * @var        string|null
     */
    protected $insertion;

    /**
     * The value for the last_name field.
     *
     * @var        string|null
     */
    protected $last_name;

    /**
     * The value for the email field.
     *
     * @var        string|null
     */
    protected $email;

    /**
     * The value for the password field.
     *
     * @var        string|null
     */
    protected $password;

    /**
     * The value for the salt field.
     *
     * @var        string|null
     */
    protected $salt;

    /**
     * The value for the once_password_reset field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $once_password_reset;

    /**
     * The value for the is_wholesale field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_wholesale;

    /**
     * The value for the is_wholesale_approved field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_wholesale_approved;

    /**
     * The value for the can_buy_on_credit field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $can_buy_on_credit;

    /**
     * The value for the iban field.
     *
     * @var        string|null
     */
    protected $iban;

    /**
     * The value for the phone field.
     *
     * @var        string|null
     */
    protected $phone;

    /**
     * The value for the mobile field.
     *
     * @var        string|null
     */
    protected $mobile;

    /**
     * The value for the fax field.
     *
     * @var        string|null
     */
    protected $fax;

    /**
     * The value for the website field.
     *
     * @var        string|null
     */
    protected $website;

    /**
     * The value for the tag field.
     *
     * @var        string|null
     */
    protected $tag;

    /**
     * The value for the is_verified field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_verified;

    /**
     * The value for the verification_key field.
     *
     * @var        string|null
     */
    protected $verification_key;

    /**
     * @var        Language
     */
    protected $aLanguage;

    /**
     * @var        Branche
     */
    protected $aBranche;

    /**
     * @var        ChildCustomerSector
     */
    protected $aCustomerSector;

    /**
     * @var        PayTerm
     */
    protected $aPayTerm;

    /**
     * @var        ChildCustomerType
     */
    protected $aCustomerType;

    /**
     * @var        ObjectCollection|ContactMessage[] Collection to store aggregation of ContactMessage objects.
     */
    protected $collContactMessages;
    protected $collContactMessagesPartial;

    /**
     * @var        ObjectCollection|ChildNewsletterSubscriber[] Collection to store aggregation of ChildNewsletterSubscriber objects.
     */
    protected $collNewsletterSubscribers;
    protected $collNewsletterSubscribersPartial;

    /**
     * @var        ObjectCollection|ChildCustomerCompany[] Collection to store aggregation of ChildCustomerCompany objects.
     */
    protected $collCustomerCompanies;
    protected $collCustomerCompaniesPartial;

    /**
     * @var        ObjectCollection|ChildCustomerAddress[] Collection to store aggregation of ChildCustomerAddress objects.
     */
    protected $collCustomers;
    protected $collCustomersPartial;

    /**
     * @var        ObjectCollection|ChildCustomerNote[] Collection to store aggregation of ChildCustomerNote objects.
     */
    protected $collCustomerNotes;
    protected $collCustomerNotesPartial;

    /**
     * @var        ObjectCollection|ChildCustomerProperty[] Collection to store aggregation of ChildCustomerProperty objects.
     */
    protected $collCustomerProperties;
    protected $collCustomerPropertiesPartial;

    /**
     * @var        ObjectCollection|CustomerWishlist[] Collection to store aggregation of CustomerWishlist objects.
     */
    protected $collCustomerWishlists;
    protected $collCustomerWishlistsPartial;

    /**
     * @var        ObjectCollection|ChildCustomerPhoto[] Collection to store aggregation of ChildCustomerPhoto objects.
     */
    protected $collCustomerPhotos;
    protected $collCustomerPhotosPartial;

    /**
     * @var        ObjectCollection|ChildCustomerLogo[] Collection to store aggregation of ChildCustomerLogo objects.
     */
    protected $collCustomerLogos;
    protected $collCustomerLogosPartial;

    /**
     * @var        ObjectCollection|SaleInvoice[] Collection to store aggregation of SaleInvoice objects.
     */
    protected $collSaleInvoices;
    protected $collSaleInvoicesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ContactMessage[]
     */
    protected $contactMessagesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildNewsletterSubscriber[]
     */
    protected $newsletterSubscribersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCustomerCompany[]
     */
    protected $customerCompaniesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCustomerAddress[]
     */
    protected $customersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCustomerNote[]
     */
    protected $customerNotesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCustomerProperty[]
     */
    protected $customerPropertiesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|CustomerWishlist[]
     */
    protected $customerWishlistsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCustomerPhoto[]
     */
    protected $customerPhotosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCustomerLogo[]
     */
    protected $customerLogosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|SaleInvoice[]
     */
    protected $saleInvoicesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_deleted = false;
        $this->is_newsletter_subscriber = false;
        $this->once_password_reset = false;
        $this->is_wholesale = false;
        $this->is_wholesale_approved = false;
        $this->can_buy_on_credit = false;
        $this->is_verified = false;
    }

    /**
     * Initializes internal state of Model\Crm\Base\Customer object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Customer</code> instance.  If
     * <code>obj</code> is an instance of <code>Customer</code>, delegates to
     * <code>equals(Customer)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [import_source] column value.
     *
     * @return string|null
     */
    public function getImportSource()
    {
        return $this->import_source;
    }

    /**
     * Get the [accounting_id] column value.
     *
     * @return string|null
     */
    public function getAccountingId()
    {
        return $this->accounting_id;
    }

    /**
     * Get the [old_id] column value.
     *
     * @return string|null
     */
    public function getOldId()
    {
        return $this->old_id;
    }

    /**
     * Get the [ip] column value.
     *
     * @return string|null
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Get the [optionally formatted] temporal [created_on] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedOn($format = null)
    {
        if ($format === null) {
            return $this->created_on;
        } else {
            return $this->created_on instanceof \DateTimeInterface ? $this->created_on->format($format) : null;
        }
    }

    /**
     * Get the [is_deleted] column value.
     *
     * @return boolean|null
     */
    public function getItemDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Get the [is_deleted] column value.
     *
     * @return boolean|null
     */
    public function isItemDeleted()
    {
        return $this->getItemDeleted();
    }

    /**
     * Get the [is_newsletter_subscriber] column value.
     *
     * @return boolean|null
     */
    public function getIsNewsletterSubscriber()
    {
        return $this->is_newsletter_subscriber;
    }

    /**
     * Get the [is_newsletter_subscriber] column value.
     *
     * @return boolean|null
     */
    public function isNewsletterSubscriber()
    {
        return $this->getIsNewsletterSubscriber();
    }

    /**
     * Get the [language_id] column value.
     *
     * @return int|null
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * Get the [is_intracommunautair] column value.
     *
     * @return int|null
     */
    public function getIsIntracommunautair()
    {
        return $this->is_intracommunautair;
    }

    /**
     * Get the [payterm_id] column value.
     *
     * @return int|null
     */
    public function getPaytermId()
    {
        return $this->payterm_id;
    }

    /**
     * Get the [branche_id] column value.
     *
     * @return int|null
     */
    public function getBrancheId()
    {
        return $this->branche_id;
    }

    /**
     * Get the [sector_id] column value.
     *
     * @return int|null
     */
    public function getSectorId()
    {
        return $this->sector_id;
    }

    /**
     * Get the [customer_type_id] column value.
     *
     * @return int|null
     */
    public function getCustomerTypeId()
    {
        return $this->customer_type_id;
    }

    /**
     * Get the [debitor] column value.
     *
     * @return string|null
     */
    public function getDebitor()
    {
        return $this->debitor;
    }

    /**
     * Get the [gender] column value.
     *
     * @return string|null
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Get the [first_name] column value.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Get the [insertion] column value.
     *
     * @return string|null
     */
    public function getInsertion()
    {
        return $this->insertion;
    }

    /**
     * Get the [last_name] column value.
     *
     * @return string|null
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Get the [email] column value.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [password] column value.
     *
     * @return string|null
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Get the [salt] column value.
     *
     * @return string|null
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Get the [once_password_reset] column value.
     *
     * @return boolean|null
     */
    public function getOncePasswordReset()
    {
        return $this->once_password_reset;
    }

    /**
     * Get the [once_password_reset] column value.
     *
     * @return boolean|null
     */
    public function isOncePasswordReset()
    {
        return $this->getOncePasswordReset();
    }

    /**
     * Get the [is_wholesale] column value.
     *
     * @return boolean|null
     */
    public function getIsWholesale()
    {
        return $this->is_wholesale;
    }

    /**
     * Get the [is_wholesale] column value.
     *
     * @return boolean|null
     */
    public function isWholesale()
    {
        return $this->getIsWholesale();
    }

    /**
     * Get the [is_wholesale_approved] column value.
     *
     * @return boolean|null
     */
    public function getIsWholesaleApproved()
    {
        return $this->is_wholesale_approved;
    }

    /**
     * Get the [is_wholesale_approved] column value.
     *
     * @return boolean|null
     */
    public function isWholesaleApproved()
    {
        return $this->getIsWholesaleApproved();
    }

    /**
     * Get the [can_buy_on_credit] column value.
     *
     * @return boolean|null
     */
    public function getCanBuyOnCredit()
    {
        return $this->can_buy_on_credit;
    }

    /**
     * Get the [can_buy_on_credit] column value.
     *
     * @return boolean|null
     */
    public function isCanBuyOnCredit()
    {
        return $this->getCanBuyOnCredit();
    }

    /**
     * Get the [iban] column value.
     *
     * @return string|null
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Get the [phone] column value.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get the [mobile] column value.
     *
     * @return string|null
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Get the [fax] column value.
     *
     * @return string|null
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Get the [website] column value.
     *
     * @return string|null
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Get the [tag] column value.
     *
     * @return string|null
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Get the [is_verified] column value.
     *
     * @return boolean|null
     */
    public function getIsVerified()
    {
        return $this->is_verified;
    }

    /**
     * Get the [is_verified] column value.
     *
     * @return boolean|null
     */
    public function isVerified()
    {
        return $this->getIsVerified();
    }

    /**
     * Get the [verification_key] column value.
     *
     * @return string|null
     */
    public function getVerificationKey()
    {
        return $this->verification_key;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[CustomerTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [import_source] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setImportSource($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->import_source !== $v) {
            $this->import_source = $v;
            $this->modifiedColumns[CustomerTableMap::COL_IMPORT_SOURCE] = true;
        }

        return $this;
    } // setImportSource()

    /**
     * Set the value of [accounting_id] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setAccountingId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->accounting_id !== $v) {
            $this->accounting_id = $v;
            $this->modifiedColumns[CustomerTableMap::COL_ACCOUNTING_ID] = true;
        }

        return $this;
    } // setAccountingId()

    /**
     * Set the value of [old_id] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setOldId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->old_id !== $v) {
            $this->old_id = $v;
            $this->modifiedColumns[CustomerTableMap::COL_OLD_ID] = true;
        }

        return $this;
    } // setOldId()

    /**
     * Set the value of [ip] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setIp($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ip !== $v) {
            $this->ip = $v;
            $this->modifiedColumns[CustomerTableMap::COL_IP] = true;
        }

        return $this;
    } // setIp()

    /**
     * Sets the value of [created_on] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setCreatedOn($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_on !== null || $dt !== null) {
            if ($this->created_on === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_on->format("Y-m-d H:i:s.u")) {
                $this->created_on = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CustomerTableMap::COL_CREATED_ON] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedOn()

    /**
     * Sets the value of the [is_deleted] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setItemDeleted($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_deleted !== $v) {
            $this->is_deleted = $v;
            $this->modifiedColumns[CustomerTableMap::COL_IS_DELETED] = true;
        }

        return $this;
    } // setItemDeleted()

    /**
     * Sets the value of the [is_newsletter_subscriber] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setIsNewsletterSubscriber($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_newsletter_subscriber !== $v) {
            $this->is_newsletter_subscriber = $v;
            $this->modifiedColumns[CustomerTableMap::COL_IS_NEWSLETTER_SUBSCRIBER] = true;
        }

        return $this;
    } // setIsNewsletterSubscriber()

    /**
     * Set the value of [language_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setLanguageId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->language_id !== $v) {
            $this->language_id = $v;
            $this->modifiedColumns[CustomerTableMap::COL_LANGUAGE_ID] = true;
        }

        if ($this->aLanguage !== null && $this->aLanguage->getId() !== $v) {
            $this->aLanguage = null;
        }

        return $this;
    } // setLanguageId()

    /**
     * Set the value of [is_intracommunautair] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setIsIntracommunautair($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->is_intracommunautair !== $v) {
            $this->is_intracommunautair = $v;
            $this->modifiedColumns[CustomerTableMap::COL_IS_INTRACOMMUNAUTAIR] = true;
        }

        return $this;
    } // setIsIntracommunautair()

    /**
     * Set the value of [payterm_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setPaytermId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->payterm_id !== $v) {
            $this->payterm_id = $v;
            $this->modifiedColumns[CustomerTableMap::COL_PAYTERM_ID] = true;
        }

        if ($this->aPayTerm !== null && $this->aPayTerm->getId() !== $v) {
            $this->aPayTerm = null;
        }

        return $this;
    } // setPaytermId()

    /**
     * Set the value of [branche_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setBrancheId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->branche_id !== $v) {
            $this->branche_id = $v;
            $this->modifiedColumns[CustomerTableMap::COL_BRANCHE_ID] = true;
        }

        if ($this->aBranche !== null && $this->aBranche->getId() !== $v) {
            $this->aBranche = null;
        }

        return $this;
    } // setBrancheId()

    /**
     * Set the value of [sector_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setSectorId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sector_id !== $v) {
            $this->sector_id = $v;
            $this->modifiedColumns[CustomerTableMap::COL_SECTOR_ID] = true;
        }

        if ($this->aCustomerSector !== null && $this->aCustomerSector->getId() !== $v) {
            $this->aCustomerSector = null;
        }

        return $this;
    } // setSectorId()

    /**
     * Set the value of [customer_type_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setCustomerTypeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->customer_type_id !== $v) {
            $this->customer_type_id = $v;
            $this->modifiedColumns[CustomerTableMap::COL_CUSTOMER_TYPE_ID] = true;
        }

        if ($this->aCustomerType !== null && $this->aCustomerType->getId() !== $v) {
            $this->aCustomerType = null;
        }

        return $this;
    } // setCustomerTypeId()

    /**
     * Set the value of [debitor] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setDebitor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->debitor !== $v) {
            $this->debitor = $v;
            $this->modifiedColumns[CustomerTableMap::COL_DEBITOR] = true;
        }

        return $this;
    } // setDebitor()

    /**
     * Set the value of [gender] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setGender($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->gender !== $v) {
            $this->gender = $v;
            $this->modifiedColumns[CustomerTableMap::COL_GENDER] = true;
        }

        return $this;
    } // setGender()

    /**
     * Set the value of [first_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setFirstName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->first_name !== $v) {
            $this->first_name = $v;
            $this->modifiedColumns[CustomerTableMap::COL_FIRST_NAME] = true;
        }

        return $this;
    } // setFirstName()

    /**
     * Set the value of [insertion] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setInsertion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->insertion !== $v) {
            $this->insertion = $v;
            $this->modifiedColumns[CustomerTableMap::COL_INSERTION] = true;
        }

        return $this;
    } // setInsertion()

    /**
     * Set the value of [last_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setLastName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->last_name !== $v) {
            $this->last_name = $v;
            $this->modifiedColumns[CustomerTableMap::COL_LAST_NAME] = true;
        }

        return $this;
    } // setLastName()

    /**
     * Set the value of [email] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[CustomerTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [password] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[CustomerTableMap::COL_PASSWORD] = true;
        }

        return $this;
    } // setPassword()

    /**
     * Set the value of [salt] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setSalt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->salt !== $v) {
            $this->salt = $v;
            $this->modifiedColumns[CustomerTableMap::COL_SALT] = true;
        }

        return $this;
    } // setSalt()

    /**
     * Sets the value of the [once_password_reset] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setOncePasswordReset($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->once_password_reset !== $v) {
            $this->once_password_reset = $v;
            $this->modifiedColumns[CustomerTableMap::COL_ONCE_PASSWORD_RESET] = true;
        }

        return $this;
    } // setOncePasswordReset()

    /**
     * Sets the value of the [is_wholesale] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setIsWholesale($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_wholesale !== $v) {
            $this->is_wholesale = $v;
            $this->modifiedColumns[CustomerTableMap::COL_IS_WHOLESALE] = true;
        }

        return $this;
    } // setIsWholesale()

    /**
     * Sets the value of the [is_wholesale_approved] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setIsWholesaleApproved($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_wholesale_approved !== $v) {
            $this->is_wholesale_approved = $v;
            $this->modifiedColumns[CustomerTableMap::COL_IS_WHOLESALE_APPROVED] = true;
        }

        return $this;
    } // setIsWholesaleApproved()

    /**
     * Sets the value of the [can_buy_on_credit] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setCanBuyOnCredit($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->can_buy_on_credit !== $v) {
            $this->can_buy_on_credit = $v;
            $this->modifiedColumns[CustomerTableMap::COL_CAN_BUY_ON_CREDIT] = true;
        }

        return $this;
    } // setCanBuyOnCredit()

    /**
     * Set the value of [iban] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setIban($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->iban !== $v) {
            $this->iban = $v;
            $this->modifiedColumns[CustomerTableMap::COL_IBAN] = true;
        }

        return $this;
    } // setIban()

    /**
     * Set the value of [phone] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[CustomerTableMap::COL_PHONE] = true;
        }

        return $this;
    } // setPhone()

    /**
     * Set the value of [mobile] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setMobile($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->mobile !== $v) {
            $this->mobile = $v;
            $this->modifiedColumns[CustomerTableMap::COL_MOBILE] = true;
        }

        return $this;
    } // setMobile()

    /**
     * Set the value of [fax] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setFax($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fax !== $v) {
            $this->fax = $v;
            $this->modifiedColumns[CustomerTableMap::COL_FAX] = true;
        }

        return $this;
    } // setFax()

    /**
     * Set the value of [website] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setWebsite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->website !== $v) {
            $this->website = $v;
            $this->modifiedColumns[CustomerTableMap::COL_WEBSITE] = true;
        }

        return $this;
    } // setWebsite()

    /**
     * Set the value of [tag] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setTag($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tag !== $v) {
            $this->tag = $v;
            $this->modifiedColumns[CustomerTableMap::COL_TAG] = true;
        }

        return $this;
    } // setTag()

    /**
     * Sets the value of the [is_verified] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setIsVerified($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_verified !== $v) {
            $this->is_verified = $v;
            $this->modifiedColumns[CustomerTableMap::COL_IS_VERIFIED] = true;
        }

        return $this;
    } // setIsVerified()

    /**
     * Set the value of [verification_key] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function setVerificationKey($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->verification_key !== $v) {
            $this->verification_key = $v;
            $this->modifiedColumns[CustomerTableMap::COL_VERIFICATION_KEY] = true;
        }

        return $this;
    } // setVerificationKey()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_deleted !== false) {
                return false;
            }

            if ($this->is_newsletter_subscriber !== false) {
                return false;
            }

            if ($this->once_password_reset !== false) {
                return false;
            }

            if ($this->is_wholesale !== false) {
                return false;
            }

            if ($this->is_wholesale_approved !== false) {
                return false;
            }

            if ($this->can_buy_on_credit !== false) {
                return false;
            }

            if ($this->is_verified !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CustomerTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CustomerTableMap::translateFieldName('ImportSource', TableMap::TYPE_PHPNAME, $indexType)];
            $this->import_source = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CustomerTableMap::translateFieldName('AccountingId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->accounting_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CustomerTableMap::translateFieldName('OldId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->old_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CustomerTableMap::translateFieldName('Ip', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ip = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CustomerTableMap::translateFieldName('CreatedOn', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_on = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CustomerTableMap::translateFieldName('ItemDeleted', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_deleted = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CustomerTableMap::translateFieldName('IsNewsletterSubscriber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_newsletter_subscriber = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CustomerTableMap::translateFieldName('LanguageId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->language_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CustomerTableMap::translateFieldName('IsIntracommunautair', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_intracommunautair = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : CustomerTableMap::translateFieldName('PaytermId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->payterm_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : CustomerTableMap::translateFieldName('BrancheId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->branche_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : CustomerTableMap::translateFieldName('SectorId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sector_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : CustomerTableMap::translateFieldName('CustomerTypeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_type_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : CustomerTableMap::translateFieldName('Debitor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->debitor = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : CustomerTableMap::translateFieldName('Gender', TableMap::TYPE_PHPNAME, $indexType)];
            $this->gender = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : CustomerTableMap::translateFieldName('FirstName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->first_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : CustomerTableMap::translateFieldName('Insertion', TableMap::TYPE_PHPNAME, $indexType)];
            $this->insertion = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : CustomerTableMap::translateFieldName('LastName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->last_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : CustomerTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : CustomerTableMap::translateFieldName('Password', TableMap::TYPE_PHPNAME, $indexType)];
            $this->password = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : CustomerTableMap::translateFieldName('Salt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->salt = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : CustomerTableMap::translateFieldName('OncePasswordReset', TableMap::TYPE_PHPNAME, $indexType)];
            $this->once_password_reset = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : CustomerTableMap::translateFieldName('IsWholesale', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_wholesale = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : CustomerTableMap::translateFieldName('IsWholesaleApproved', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_wholesale_approved = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : CustomerTableMap::translateFieldName('CanBuyOnCredit', TableMap::TYPE_PHPNAME, $indexType)];
            $this->can_buy_on_credit = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : CustomerTableMap::translateFieldName('Iban', TableMap::TYPE_PHPNAME, $indexType)];
            $this->iban = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : CustomerTableMap::translateFieldName('Phone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->phone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : CustomerTableMap::translateFieldName('Mobile', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mobile = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : CustomerTableMap::translateFieldName('Fax', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fax = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : CustomerTableMap::translateFieldName('Website', TableMap::TYPE_PHPNAME, $indexType)];
            $this->website = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 31 + $startcol : CustomerTableMap::translateFieldName('Tag', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tag = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 32 + $startcol : CustomerTableMap::translateFieldName('IsVerified', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_verified = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 33 + $startcol : CustomerTableMap::translateFieldName('VerificationKey', TableMap::TYPE_PHPNAME, $indexType)];
            $this->verification_key = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 34; // 34 = CustomerTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Crm\\Customer'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aLanguage !== null && $this->language_id !== $this->aLanguage->getId()) {
            $this->aLanguage = null;
        }
        if ($this->aPayTerm !== null && $this->payterm_id !== $this->aPayTerm->getId()) {
            $this->aPayTerm = null;
        }
        if ($this->aBranche !== null && $this->branche_id !== $this->aBranche->getId()) {
            $this->aBranche = null;
        }
        if ($this->aCustomerSector !== null && $this->sector_id !== $this->aCustomerSector->getId()) {
            $this->aCustomerSector = null;
        }
        if ($this->aCustomerType !== null && $this->customer_type_id !== $this->aCustomerType->getId()) {
            $this->aCustomerType = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CustomerTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCustomerQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aLanguage = null;
            $this->aBranche = null;
            $this->aCustomerSector = null;
            $this->aPayTerm = null;
            $this->aCustomerType = null;
            $this->collContactMessages = null;

            $this->collNewsletterSubscribers = null;

            $this->collCustomerCompanies = null;

            $this->collCustomers = null;

            $this->collCustomerNotes = null;

            $this->collCustomerProperties = null;

            $this->collCustomerWishlists = null;

            $this->collCustomerPhotos = null;

            $this->collCustomerLogos = null;

            $this->collSaleInvoices = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Customer::setDeleted()
     * @see Customer::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCustomerQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CustomerTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aLanguage !== null) {
                if ($this->aLanguage->isModified() || $this->aLanguage->isNew()) {
                    $affectedRows += $this->aLanguage->save($con);
                }
                $this->setLanguage($this->aLanguage);
            }

            if ($this->aBranche !== null) {
                if ($this->aBranche->isModified() || $this->aBranche->isNew()) {
                    $affectedRows += $this->aBranche->save($con);
                }
                $this->setBranche($this->aBranche);
            }

            if ($this->aCustomerSector !== null) {
                if ($this->aCustomerSector->isModified() || $this->aCustomerSector->isNew()) {
                    $affectedRows += $this->aCustomerSector->save($con);
                }
                $this->setCustomerSector($this->aCustomerSector);
            }

            if ($this->aPayTerm !== null) {
                if ($this->aPayTerm->isModified() || $this->aPayTerm->isNew()) {
                    $affectedRows += $this->aPayTerm->save($con);
                }
                $this->setPayTerm($this->aPayTerm);
            }

            if ($this->aCustomerType !== null) {
                if ($this->aCustomerType->isModified() || $this->aCustomerType->isNew()) {
                    $affectedRows += $this->aCustomerType->save($con);
                }
                $this->setCustomerType($this->aCustomerType);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->contactMessagesScheduledForDeletion !== null) {
                if (!$this->contactMessagesScheduledForDeletion->isEmpty()) {
                    \Model\ContactMessageQuery::create()
                        ->filterByPrimaryKeys($this->contactMessagesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->contactMessagesScheduledForDeletion = null;
                }
            }

            if ($this->collContactMessages !== null) {
                foreach ($this->collContactMessages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->newsletterSubscribersScheduledForDeletion !== null) {
                if (!$this->newsletterSubscribersScheduledForDeletion->isEmpty()) {
                    foreach ($this->newsletterSubscribersScheduledForDeletion as $newsletterSubscriber) {
                        // need to save related object because we set the relation to null
                        $newsletterSubscriber->save($con);
                    }
                    $this->newsletterSubscribersScheduledForDeletion = null;
                }
            }

            if ($this->collNewsletterSubscribers !== null) {
                foreach ($this->collNewsletterSubscribers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->customerCompaniesScheduledForDeletion !== null) {
                if (!$this->customerCompaniesScheduledForDeletion->isEmpty()) {
                    \Model\Crm\CustomerCompanyQuery::create()
                        ->filterByPrimaryKeys($this->customerCompaniesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->customerCompaniesScheduledForDeletion = null;
                }
            }

            if ($this->collCustomerCompanies !== null) {
                foreach ($this->collCustomerCompanies as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->customersScheduledForDeletion !== null) {
                if (!$this->customersScheduledForDeletion->isEmpty()) {
                    \Model\Crm\CustomerAddressQuery::create()
                        ->filterByPrimaryKeys($this->customersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->customersScheduledForDeletion = null;
                }
            }

            if ($this->collCustomers !== null) {
                foreach ($this->collCustomers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->customerNotesScheduledForDeletion !== null) {
                if (!$this->customerNotesScheduledForDeletion->isEmpty()) {
                    \Model\Crm\CustomerNoteQuery::create()
                        ->filterByPrimaryKeys($this->customerNotesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->customerNotesScheduledForDeletion = null;
                }
            }

            if ($this->collCustomerNotes !== null) {
                foreach ($this->collCustomerNotes as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->customerPropertiesScheduledForDeletion !== null) {
                if (!$this->customerPropertiesScheduledForDeletion->isEmpty()) {
                    \Model\Crm\CustomerPropertyQuery::create()
                        ->filterByPrimaryKeys($this->customerPropertiesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->customerPropertiesScheduledForDeletion = null;
                }
            }

            if ($this->collCustomerProperties !== null) {
                foreach ($this->collCustomerProperties as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->customerWishlistsScheduledForDeletion !== null) {
                if (!$this->customerWishlistsScheduledForDeletion->isEmpty()) {
                    \Model\Category\CustomerWishlistQuery::create()
                        ->filterByPrimaryKeys($this->customerWishlistsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->customerWishlistsScheduledForDeletion = null;
                }
            }

            if ($this->collCustomerWishlists !== null) {
                foreach ($this->collCustomerWishlists as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->customerPhotosScheduledForDeletion !== null) {
                if (!$this->customerPhotosScheduledForDeletion->isEmpty()) {
                    \Model\Crm\CustomerPhotoQuery::create()
                        ->filterByPrimaryKeys($this->customerPhotosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->customerPhotosScheduledForDeletion = null;
                }
            }

            if ($this->collCustomerPhotos !== null) {
                foreach ($this->collCustomerPhotos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->customerLogosScheduledForDeletion !== null) {
                if (!$this->customerLogosScheduledForDeletion->isEmpty()) {
                    \Model\Crm\CustomerLogoQuery::create()
                        ->filterByPrimaryKeys($this->customerLogosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->customerLogosScheduledForDeletion = null;
                }
            }

            if ($this->collCustomerLogos !== null) {
                foreach ($this->collCustomerLogos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saleInvoicesScheduledForDeletion !== null) {
                if (!$this->saleInvoicesScheduledForDeletion->isEmpty()) {
                    foreach ($this->saleInvoicesScheduledForDeletion as $saleInvoice) {
                        // need to save related object because we set the relation to null
                        $saleInvoice->save($con);
                    }
                    $this->saleInvoicesScheduledForDeletion = null;
                }
            }

            if ($this->collSaleInvoices !== null) {
                foreach ($this->collSaleInvoices as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CustomerTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CustomerTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CustomerTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IMPORT_SOURCE)) {
            $modifiedColumns[':p' . $index++]  = 'import_source';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_ACCOUNTING_ID)) {
            $modifiedColumns[':p' . $index++]  = 'accounting_id';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_OLD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'old_id';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IP)) {
            $modifiedColumns[':p' . $index++]  = 'ip';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_CREATED_ON)) {
            $modifiedColumns[':p' . $index++]  = 'created_on';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IS_DELETED)) {
            $modifiedColumns[':p' . $index++]  = 'is_deleted';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IS_NEWSLETTER_SUBSCRIBER)) {
            $modifiedColumns[':p' . $index++]  = 'is_newsletter_subscriber';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_LANGUAGE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'language_id';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IS_INTRACOMMUNAUTAIR)) {
            $modifiedColumns[':p' . $index++]  = 'is_intracommunautair';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_PAYTERM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'payterm_id';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_BRANCHE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'branche_id';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_SECTOR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'sector_id';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_CUSTOMER_TYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'customer_type_id';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_DEBITOR)) {
            $modifiedColumns[':p' . $index++]  = 'debitor';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_GENDER)) {
            $modifiedColumns[':p' . $index++]  = 'gender';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_FIRST_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'first_name';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_INSERTION)) {
            $modifiedColumns[':p' . $index++]  = 'insertion';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_LAST_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'last_name';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'email';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = 'password';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_SALT)) {
            $modifiedColumns[':p' . $index++]  = 'salt';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_ONCE_PASSWORD_RESET)) {
            $modifiedColumns[':p' . $index++]  = 'once_password_reset';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IS_WHOLESALE)) {
            $modifiedColumns[':p' . $index++]  = 'is_wholesale';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IS_WHOLESALE_APPROVED)) {
            $modifiedColumns[':p' . $index++]  = 'is_wholesale_approved';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_CAN_BUY_ON_CREDIT)) {
            $modifiedColumns[':p' . $index++]  = 'can_buy_on_credit';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IBAN)) {
            $modifiedColumns[':p' . $index++]  = 'iban';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_PHONE)) {
            $modifiedColumns[':p' . $index++]  = 'phone';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_MOBILE)) {
            $modifiedColumns[':p' . $index++]  = 'mobile';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_FAX)) {
            $modifiedColumns[':p' . $index++]  = 'fax';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_WEBSITE)) {
            $modifiedColumns[':p' . $index++]  = 'website';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_TAG)) {
            $modifiedColumns[':p' . $index++]  = 'tag';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IS_VERIFIED)) {
            $modifiedColumns[':p' . $index++]  = 'is_verified';
        }
        if ($this->isColumnModified(CustomerTableMap::COL_VERIFICATION_KEY)) {
            $modifiedColumns[':p' . $index++]  = 'verification_key';
        }

        $sql = sprintf(
            'INSERT INTO customer (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'import_source':
                        $stmt->bindValue($identifier, $this->import_source, PDO::PARAM_STR);
                        break;
                    case 'accounting_id':
                        $stmt->bindValue($identifier, $this->accounting_id, PDO::PARAM_STR);
                        break;
                    case 'old_id':
                        $stmt->bindValue($identifier, $this->old_id, PDO::PARAM_STR);
                        break;
                    case 'ip':
                        $stmt->bindValue($identifier, $this->ip, PDO::PARAM_STR);
                        break;
                    case 'created_on':
                        $stmt->bindValue($identifier, $this->created_on ? $this->created_on->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'is_deleted':
                        $stmt->bindValue($identifier, (int) $this->is_deleted, PDO::PARAM_INT);
                        break;
                    case 'is_newsletter_subscriber':
                        $stmt->bindValue($identifier, (int) $this->is_newsletter_subscriber, PDO::PARAM_INT);
                        break;
                    case 'language_id':
                        $stmt->bindValue($identifier, $this->language_id, PDO::PARAM_INT);
                        break;
                    case 'is_intracommunautair':
                        $stmt->bindValue($identifier, $this->is_intracommunautair, PDO::PARAM_INT);
                        break;
                    case 'payterm_id':
                        $stmt->bindValue($identifier, $this->payterm_id, PDO::PARAM_INT);
                        break;
                    case 'branche_id':
                        $stmt->bindValue($identifier, $this->branche_id, PDO::PARAM_INT);
                        break;
                    case 'sector_id':
                        $stmt->bindValue($identifier, $this->sector_id, PDO::PARAM_INT);
                        break;
                    case 'customer_type_id':
                        $stmt->bindValue($identifier, $this->customer_type_id, PDO::PARAM_INT);
                        break;
                    case 'debitor':
                        $stmt->bindValue($identifier, $this->debitor, PDO::PARAM_STR);
                        break;
                    case 'gender':
                        $stmt->bindValue($identifier, $this->gender, PDO::PARAM_STR);
                        break;
                    case 'first_name':
                        $stmt->bindValue($identifier, $this->first_name, PDO::PARAM_STR);
                        break;
                    case 'insertion':
                        $stmt->bindValue($identifier, $this->insertion, PDO::PARAM_STR);
                        break;
                    case 'last_name':
                        $stmt->bindValue($identifier, $this->last_name, PDO::PARAM_STR);
                        break;
                    case 'email':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case 'password':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                    case 'salt':
                        $stmt->bindValue($identifier, $this->salt, PDO::PARAM_STR);
                        break;
                    case 'once_password_reset':
                        $stmt->bindValue($identifier, (int) $this->once_password_reset, PDO::PARAM_INT);
                        break;
                    case 'is_wholesale':
                        $stmt->bindValue($identifier, (int) $this->is_wholesale, PDO::PARAM_INT);
                        break;
                    case 'is_wholesale_approved':
                        $stmt->bindValue($identifier, (int) $this->is_wholesale_approved, PDO::PARAM_INT);
                        break;
                    case 'can_buy_on_credit':
                        $stmt->bindValue($identifier, (int) $this->can_buy_on_credit, PDO::PARAM_INT);
                        break;
                    case 'iban':
                        $stmt->bindValue($identifier, $this->iban, PDO::PARAM_STR);
                        break;
                    case 'phone':
                        $stmt->bindValue($identifier, $this->phone, PDO::PARAM_STR);
                        break;
                    case 'mobile':
                        $stmt->bindValue($identifier, $this->mobile, PDO::PARAM_STR);
                        break;
                    case 'fax':
                        $stmt->bindValue($identifier, $this->fax, PDO::PARAM_STR);
                        break;
                    case 'website':
                        $stmt->bindValue($identifier, $this->website, PDO::PARAM_STR);
                        break;
                    case 'tag':
                        $stmt->bindValue($identifier, $this->tag, PDO::PARAM_STR);
                        break;
                    case 'is_verified':
                        $stmt->bindValue($identifier, (int) $this->is_verified, PDO::PARAM_INT);
                        break;
                    case 'verification_key':
                        $stmt->bindValue($identifier, $this->verification_key, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CustomerTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getImportSource();
                break;
            case 2:
                return $this->getAccountingId();
                break;
            case 3:
                return $this->getOldId();
                break;
            case 4:
                return $this->getIp();
                break;
            case 5:
                return $this->getCreatedOn();
                break;
            case 6:
                return $this->getItemDeleted();
                break;
            case 7:
                return $this->getIsNewsletterSubscriber();
                break;
            case 8:
                return $this->getLanguageId();
                break;
            case 9:
                return $this->getIsIntracommunautair();
                break;
            case 10:
                return $this->getPaytermId();
                break;
            case 11:
                return $this->getBrancheId();
                break;
            case 12:
                return $this->getSectorId();
                break;
            case 13:
                return $this->getCustomerTypeId();
                break;
            case 14:
                return $this->getDebitor();
                break;
            case 15:
                return $this->getGender();
                break;
            case 16:
                return $this->getFirstName();
                break;
            case 17:
                return $this->getInsertion();
                break;
            case 18:
                return $this->getLastName();
                break;
            case 19:
                return $this->getEmail();
                break;
            case 20:
                return $this->getPassword();
                break;
            case 21:
                return $this->getSalt();
                break;
            case 22:
                return $this->getOncePasswordReset();
                break;
            case 23:
                return $this->getIsWholesale();
                break;
            case 24:
                return $this->getIsWholesaleApproved();
                break;
            case 25:
                return $this->getCanBuyOnCredit();
                break;
            case 26:
                return $this->getIban();
                break;
            case 27:
                return $this->getPhone();
                break;
            case 28:
                return $this->getMobile();
                break;
            case 29:
                return $this->getFax();
                break;
            case 30:
                return $this->getWebsite();
                break;
            case 31:
                return $this->getTag();
                break;
            case 32:
                return $this->getIsVerified();
                break;
            case 33:
                return $this->getVerificationKey();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Customer'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Customer'][$this->hashCode()] = true;
        $keys = CustomerTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getImportSource(),
            $keys[2] => $this->getAccountingId(),
            $keys[3] => $this->getOldId(),
            $keys[4] => $this->getIp(),
            $keys[5] => $this->getCreatedOn(),
            $keys[6] => $this->getItemDeleted(),
            $keys[7] => $this->getIsNewsletterSubscriber(),
            $keys[8] => $this->getLanguageId(),
            $keys[9] => $this->getIsIntracommunautair(),
            $keys[10] => $this->getPaytermId(),
            $keys[11] => $this->getBrancheId(),
            $keys[12] => $this->getSectorId(),
            $keys[13] => $this->getCustomerTypeId(),
            $keys[14] => $this->getDebitor(),
            $keys[15] => $this->getGender(),
            $keys[16] => $this->getFirstName(),
            $keys[17] => $this->getInsertion(),
            $keys[18] => $this->getLastName(),
            $keys[19] => $this->getEmail(),
            $keys[20] => $this->getPassword(),
            $keys[21] => $this->getSalt(),
            $keys[22] => $this->getOncePasswordReset(),
            $keys[23] => $this->getIsWholesale(),
            $keys[24] => $this->getIsWholesaleApproved(),
            $keys[25] => $this->getCanBuyOnCredit(),
            $keys[26] => $this->getIban(),
            $keys[27] => $this->getPhone(),
            $keys[28] => $this->getMobile(),
            $keys[29] => $this->getFax(),
            $keys[30] => $this->getWebsite(),
            $keys[31] => $this->getTag(),
            $keys[32] => $this->getIsVerified(),
            $keys[33] => $this->getVerificationKey(),
        );
        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aLanguage) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'language';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_language';
                        break;
                    default:
                        $key = 'Language';
                }

                $result[$key] = $this->aLanguage->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aBranche) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'branche';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_branche';
                        break;
                    default:
                        $key = 'Branche';
                }

                $result[$key] = $this->aBranche->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCustomerSector) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customerSector';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_customer_sector';
                        break;
                    default:
                        $key = 'CustomerSector';
                }

                $result[$key] = $this->aCustomerSector->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPayTerm) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'payTerm';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_payterm';
                        break;
                    default:
                        $key = 'PayTerm';
                }

                $result[$key] = $this->aPayTerm->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCustomerType) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customerType';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_customer_type';
                        break;
                    default:
                        $key = 'CustomerType';
                }

                $result[$key] = $this->aCustomerType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collContactMessages) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contactMessages';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contact_messages';
                        break;
                    default:
                        $key = 'ContactMessages';
                }

                $result[$key] = $this->collContactMessages->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collNewsletterSubscribers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'newsletterSubscribers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'newsletter_subscribers';
                        break;
                    default:
                        $key = 'NewsletterSubscribers';
                }

                $result[$key] = $this->collNewsletterSubscribers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCustomerCompanies) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customerCompanies';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'customer_companies';
                        break;
                    default:
                        $key = 'CustomerCompanies';
                }

                $result[$key] = $this->collCustomerCompanies->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCustomers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customerAddresses';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'customer_addresses';
                        break;
                    default:
                        $key = 'Customers';
                }

                $result[$key] = $this->collCustomers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCustomerNotes) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customerNotes';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'customer_notes';
                        break;
                    default:
                        $key = 'CustomerNotes';
                }

                $result[$key] = $this->collCustomerNotes->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCustomerProperties) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customerProperties';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'customer_properties';
                        break;
                    default:
                        $key = 'CustomerProperties';
                }

                $result[$key] = $this->collCustomerProperties->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCustomerWishlists) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customerWishlists';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'customer_wishlists';
                        break;
                    default:
                        $key = 'CustomerWishlists';
                }

                $result[$key] = $this->collCustomerWishlists->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCustomerPhotos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customerPhotos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'customer_photos';
                        break;
                    default:
                        $key = 'CustomerPhotos';
                }

                $result[$key] = $this->collCustomerPhotos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCustomerLogos) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customerLogos';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'customer_logos';
                        break;
                    default:
                        $key = 'CustomerLogos';
                }

                $result[$key] = $this->collCustomerLogos->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaleInvoices) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'saleInvoices';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_invoices';
                        break;
                    default:
                        $key = 'SaleInvoices';
                }

                $result[$key] = $this->collSaleInvoices->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Crm\Customer
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CustomerTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Crm\Customer
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setImportSource($value);
                break;
            case 2:
                $this->setAccountingId($value);
                break;
            case 3:
                $this->setOldId($value);
                break;
            case 4:
                $this->setIp($value);
                break;
            case 5:
                $this->setCreatedOn($value);
                break;
            case 6:
                $this->setItemDeleted($value);
                break;
            case 7:
                $this->setIsNewsletterSubscriber($value);
                break;
            case 8:
                $this->setLanguageId($value);
                break;
            case 9:
                $this->setIsIntracommunautair($value);
                break;
            case 10:
                $this->setPaytermId($value);
                break;
            case 11:
                $this->setBrancheId($value);
                break;
            case 12:
                $this->setSectorId($value);
                break;
            case 13:
                $this->setCustomerTypeId($value);
                break;
            case 14:
                $this->setDebitor($value);
                break;
            case 15:
                $this->setGender($value);
                break;
            case 16:
                $this->setFirstName($value);
                break;
            case 17:
                $this->setInsertion($value);
                break;
            case 18:
                $this->setLastName($value);
                break;
            case 19:
                $this->setEmail($value);
                break;
            case 20:
                $this->setPassword($value);
                break;
            case 21:
                $this->setSalt($value);
                break;
            case 22:
                $this->setOncePasswordReset($value);
                break;
            case 23:
                $this->setIsWholesale($value);
                break;
            case 24:
                $this->setIsWholesaleApproved($value);
                break;
            case 25:
                $this->setCanBuyOnCredit($value);
                break;
            case 26:
                $this->setIban($value);
                break;
            case 27:
                $this->setPhone($value);
                break;
            case 28:
                $this->setMobile($value);
                break;
            case 29:
                $this->setFax($value);
                break;
            case 30:
                $this->setWebsite($value);
                break;
            case 31:
                $this->setTag($value);
                break;
            case 32:
                $this->setIsVerified($value);
                break;
            case 33:
                $this->setVerificationKey($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CustomerTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setImportSource($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setAccountingId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setOldId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setIp($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setCreatedOn($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setItemDeleted($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setIsNewsletterSubscriber($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setLanguageId($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setIsIntracommunautair($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setPaytermId($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setBrancheId($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setSectorId($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setCustomerTypeId($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setDebitor($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setGender($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setFirstName($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setInsertion($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setLastName($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setEmail($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setPassword($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setSalt($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setOncePasswordReset($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setIsWholesale($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setIsWholesaleApproved($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setCanBuyOnCredit($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setIban($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setPhone($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setMobile($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setFax($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setWebsite($arr[$keys[30]]);
        }
        if (array_key_exists($keys[31], $arr)) {
            $this->setTag($arr[$keys[31]]);
        }
        if (array_key_exists($keys[32], $arr)) {
            $this->setIsVerified($arr[$keys[32]]);
        }
        if (array_key_exists($keys[33], $arr)) {
            $this->setVerificationKey($arr[$keys[33]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Crm\Customer The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CustomerTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CustomerTableMap::COL_ID)) {
            $criteria->add(CustomerTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IMPORT_SOURCE)) {
            $criteria->add(CustomerTableMap::COL_IMPORT_SOURCE, $this->import_source);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_ACCOUNTING_ID)) {
            $criteria->add(CustomerTableMap::COL_ACCOUNTING_ID, $this->accounting_id);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_OLD_ID)) {
            $criteria->add(CustomerTableMap::COL_OLD_ID, $this->old_id);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IP)) {
            $criteria->add(CustomerTableMap::COL_IP, $this->ip);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_CREATED_ON)) {
            $criteria->add(CustomerTableMap::COL_CREATED_ON, $this->created_on);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IS_DELETED)) {
            $criteria->add(CustomerTableMap::COL_IS_DELETED, $this->is_deleted);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IS_NEWSLETTER_SUBSCRIBER)) {
            $criteria->add(CustomerTableMap::COL_IS_NEWSLETTER_SUBSCRIBER, $this->is_newsletter_subscriber);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_LANGUAGE_ID)) {
            $criteria->add(CustomerTableMap::COL_LANGUAGE_ID, $this->language_id);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IS_INTRACOMMUNAUTAIR)) {
            $criteria->add(CustomerTableMap::COL_IS_INTRACOMMUNAUTAIR, $this->is_intracommunautair);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_PAYTERM_ID)) {
            $criteria->add(CustomerTableMap::COL_PAYTERM_ID, $this->payterm_id);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_BRANCHE_ID)) {
            $criteria->add(CustomerTableMap::COL_BRANCHE_ID, $this->branche_id);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_SECTOR_ID)) {
            $criteria->add(CustomerTableMap::COL_SECTOR_ID, $this->sector_id);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_CUSTOMER_TYPE_ID)) {
            $criteria->add(CustomerTableMap::COL_CUSTOMER_TYPE_ID, $this->customer_type_id);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_DEBITOR)) {
            $criteria->add(CustomerTableMap::COL_DEBITOR, $this->debitor);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_GENDER)) {
            $criteria->add(CustomerTableMap::COL_GENDER, $this->gender);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_FIRST_NAME)) {
            $criteria->add(CustomerTableMap::COL_FIRST_NAME, $this->first_name);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_INSERTION)) {
            $criteria->add(CustomerTableMap::COL_INSERTION, $this->insertion);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_LAST_NAME)) {
            $criteria->add(CustomerTableMap::COL_LAST_NAME, $this->last_name);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_EMAIL)) {
            $criteria->add(CustomerTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_PASSWORD)) {
            $criteria->add(CustomerTableMap::COL_PASSWORD, $this->password);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_SALT)) {
            $criteria->add(CustomerTableMap::COL_SALT, $this->salt);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_ONCE_PASSWORD_RESET)) {
            $criteria->add(CustomerTableMap::COL_ONCE_PASSWORD_RESET, $this->once_password_reset);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IS_WHOLESALE)) {
            $criteria->add(CustomerTableMap::COL_IS_WHOLESALE, $this->is_wholesale);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IS_WHOLESALE_APPROVED)) {
            $criteria->add(CustomerTableMap::COL_IS_WHOLESALE_APPROVED, $this->is_wholesale_approved);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_CAN_BUY_ON_CREDIT)) {
            $criteria->add(CustomerTableMap::COL_CAN_BUY_ON_CREDIT, $this->can_buy_on_credit);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IBAN)) {
            $criteria->add(CustomerTableMap::COL_IBAN, $this->iban);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_PHONE)) {
            $criteria->add(CustomerTableMap::COL_PHONE, $this->phone);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_MOBILE)) {
            $criteria->add(CustomerTableMap::COL_MOBILE, $this->mobile);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_FAX)) {
            $criteria->add(CustomerTableMap::COL_FAX, $this->fax);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_WEBSITE)) {
            $criteria->add(CustomerTableMap::COL_WEBSITE, $this->website);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_TAG)) {
            $criteria->add(CustomerTableMap::COL_TAG, $this->tag);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_IS_VERIFIED)) {
            $criteria->add(CustomerTableMap::COL_IS_VERIFIED, $this->is_verified);
        }
        if ($this->isColumnModified(CustomerTableMap::COL_VERIFICATION_KEY)) {
            $criteria->add(CustomerTableMap::COL_VERIFICATION_KEY, $this->verification_key);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCustomerQuery::create();
        $criteria->add(CustomerTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Crm\Customer (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setImportSource($this->getImportSource());
        $copyObj->setAccountingId($this->getAccountingId());
        $copyObj->setOldId($this->getOldId());
        $copyObj->setIp($this->getIp());
        $copyObj->setCreatedOn($this->getCreatedOn());
        $copyObj->setItemDeleted($this->getItemDeleted());
        $copyObj->setIsNewsletterSubscriber($this->getIsNewsletterSubscriber());
        $copyObj->setLanguageId($this->getLanguageId());
        $copyObj->setIsIntracommunautair($this->getIsIntracommunautair());
        $copyObj->setPaytermId($this->getPaytermId());
        $copyObj->setBrancheId($this->getBrancheId());
        $copyObj->setSectorId($this->getSectorId());
        $copyObj->setCustomerTypeId($this->getCustomerTypeId());
        $copyObj->setDebitor($this->getDebitor());
        $copyObj->setGender($this->getGender());
        $copyObj->setFirstName($this->getFirstName());
        $copyObj->setInsertion($this->getInsertion());
        $copyObj->setLastName($this->getLastName());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setPassword($this->getPassword());
        $copyObj->setSalt($this->getSalt());
        $copyObj->setOncePasswordReset($this->getOncePasswordReset());
        $copyObj->setIsWholesale($this->getIsWholesale());
        $copyObj->setIsWholesaleApproved($this->getIsWholesaleApproved());
        $copyObj->setCanBuyOnCredit($this->getCanBuyOnCredit());
        $copyObj->setIban($this->getIban());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setMobile($this->getMobile());
        $copyObj->setFax($this->getFax());
        $copyObj->setWebsite($this->getWebsite());
        $copyObj->setTag($this->getTag());
        $copyObj->setIsVerified($this->getIsVerified());
        $copyObj->setVerificationKey($this->getVerificationKey());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getContactMessages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addContactMessage($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getNewsletterSubscribers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addNewsletterSubscriber($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCustomerCompanies() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomerCompany($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCustomers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomer($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCustomerNotes() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomerNote($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCustomerProperties() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomerProperty($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCustomerWishlists() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomerWishlist($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCustomerPhotos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomerPhoto($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCustomerLogos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomerLogo($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaleInvoices() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaleInvoice($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Crm\Customer Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a Language object.
     *
     * @param  Language|null $v
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLanguage(Language $v = null)
    {
        if ($v === null) {
            $this->setLanguageId(NULL);
        } else {
            $this->setLanguageId($v->getId());
        }

        $this->aLanguage = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Language object, it will not be re-added.
        if ($v !== null) {
            $v->addCustomer($this);
        }


        return $this;
    }


    /**
     * Get the associated Language object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Language|null The associated Language object.
     * @throws PropelException
     */
    public function getLanguage(ConnectionInterface $con = null)
    {
        if ($this->aLanguage === null && ($this->language_id != 0)) {
            $this->aLanguage = LanguageQuery::create()->findPk($this->language_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLanguage->addCustomers($this);
             */
        }

        return $this->aLanguage;
    }

    /**
     * Declares an association between this object and a Branche object.
     *
     * @param  Branche|null $v
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBranche(Branche $v = null)
    {
        if ($v === null) {
            $this->setBrancheId(NULL);
        } else {
            $this->setBrancheId($v->getId());
        }

        $this->aBranche = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Branche object, it will not be re-added.
        if ($v !== null) {
            $v->addCustomer($this);
        }


        return $this;
    }


    /**
     * Get the associated Branche object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Branche|null The associated Branche object.
     * @throws PropelException
     */
    public function getBranche(ConnectionInterface $con = null)
    {
        if ($this->aBranche === null && ($this->branche_id != 0)) {
            $this->aBranche = BrancheQuery::create()->findPk($this->branche_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBranche->addCustomers($this);
             */
        }

        return $this->aBranche;
    }

    /**
     * Declares an association between this object and a ChildCustomerSector object.
     *
     * @param  ChildCustomerSector|null $v
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCustomerSector(ChildCustomerSector $v = null)
    {
        if ($v === null) {
            $this->setSectorId(NULL);
        } else {
            $this->setSectorId($v->getId());
        }

        $this->aCustomerSector = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCustomerSector object, it will not be re-added.
        if ($v !== null) {
            $v->addCustomerSector($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCustomerSector object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCustomerSector|null The associated ChildCustomerSector object.
     * @throws PropelException
     */
    public function getCustomerSector(ConnectionInterface $con = null)
    {
        if ($this->aCustomerSector === null && ($this->sector_id != 0)) {
            $this->aCustomerSector = ChildCustomerSectorQuery::create()->findPk($this->sector_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCustomerSector->addCustomerSectors($this);
             */
        }

        return $this->aCustomerSector;
    }

    /**
     * Declares an association between this object and a PayTerm object.
     *
     * @param  PayTerm|null $v
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPayTerm(PayTerm $v = null)
    {
        if ($v === null) {
            $this->setPaytermId(NULL);
        } else {
            $this->setPaytermId($v->getId());
        }

        $this->aPayTerm = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PayTerm object, it will not be re-added.
        if ($v !== null) {
            $v->addPayTerm($this);
        }


        return $this;
    }


    /**
     * Get the associated PayTerm object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return PayTerm|null The associated PayTerm object.
     * @throws PropelException
     */
    public function getPayTerm(ConnectionInterface $con = null)
    {
        if ($this->aPayTerm === null && ($this->payterm_id != 0)) {
            $this->aPayTerm = PayTermQuery::create()->findPk($this->payterm_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPayTerm->addPayTerms($this);
             */
        }

        return $this->aPayTerm;
    }

    /**
     * Declares an association between this object and a ChildCustomerType object.
     *
     * @param  ChildCustomerType|null $v
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCustomerType(ChildCustomerType $v = null)
    {
        if ($v === null) {
            $this->setCustomerTypeId(NULL);
        } else {
            $this->setCustomerTypeId($v->getId());
        }

        $this->aCustomerType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildCustomerType object, it will not be re-added.
        if ($v !== null) {
            $v->addCustomerType($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildCustomerType object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildCustomerType|null The associated ChildCustomerType object.
     * @throws PropelException
     */
    public function getCustomerType(ConnectionInterface $con = null)
    {
        if ($this->aCustomerType === null && ($this->customer_type_id != 0)) {
            $this->aCustomerType = ChildCustomerTypeQuery::create()->findPk($this->customer_type_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCustomerType->addCustomerTypes($this);
             */
        }

        return $this->aCustomerType;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ContactMessage' === $relationName) {
            $this->initContactMessages();
            return;
        }
        if ('NewsletterSubscriber' === $relationName) {
            $this->initNewsletterSubscribers();
            return;
        }
        if ('CustomerCompany' === $relationName) {
            $this->initCustomerCompanies();
            return;
        }
        if ('Customer' === $relationName) {
            $this->initCustomers();
            return;
        }
        if ('CustomerNote' === $relationName) {
            $this->initCustomerNotes();
            return;
        }
        if ('CustomerProperty' === $relationName) {
            $this->initCustomerProperties();
            return;
        }
        if ('CustomerWishlist' === $relationName) {
            $this->initCustomerWishlists();
            return;
        }
        if ('CustomerPhoto' === $relationName) {
            $this->initCustomerPhotos();
            return;
        }
        if ('CustomerLogo' === $relationName) {
            $this->initCustomerLogos();
            return;
        }
        if ('SaleInvoice' === $relationName) {
            $this->initSaleInvoices();
            return;
        }
    }

    /**
     * Clears out the collContactMessages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addContactMessages()
     */
    public function clearContactMessages()
    {
        $this->collContactMessages = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collContactMessages collection loaded partially.
     */
    public function resetPartialContactMessages($v = true)
    {
        $this->collContactMessagesPartial = $v;
    }

    /**
     * Initializes the collContactMessages collection.
     *
     * By default this just sets the collContactMessages collection to an empty array (like clearcollContactMessages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initContactMessages($overrideExisting = true)
    {
        if (null !== $this->collContactMessages && !$overrideExisting) {
            return;
        }

        $collectionClassName = ContactMessageTableMap::getTableMap()->getCollectionClassName();

        $this->collContactMessages = new $collectionClassName;
        $this->collContactMessages->setModel('\Model\ContactMessage');
    }

    /**
     * Gets an array of ContactMessage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCustomer is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ContactMessage[] List of ContactMessage objects
     * @throws PropelException
     */
    public function getContactMessages(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collContactMessagesPartial && !$this->isNew();
        if (null === $this->collContactMessages || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collContactMessages) {
                    $this->initContactMessages();
                } else {
                    $collectionClassName = ContactMessageTableMap::getTableMap()->getCollectionClassName();

                    $collContactMessages = new $collectionClassName;
                    $collContactMessages->setModel('\Model\ContactMessage');

                    return $collContactMessages;
                }
            } else {
                $collContactMessages = ContactMessageQuery::create(null, $criteria)
                    ->filterByCustomer($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collContactMessagesPartial && count($collContactMessages)) {
                        $this->initContactMessages(false);

                        foreach ($collContactMessages as $obj) {
                            if (false == $this->collContactMessages->contains($obj)) {
                                $this->collContactMessages->append($obj);
                            }
                        }

                        $this->collContactMessagesPartial = true;
                    }

                    return $collContactMessages;
                }

                if ($partial && $this->collContactMessages) {
                    foreach ($this->collContactMessages as $obj) {
                        if ($obj->isNew()) {
                            $collContactMessages[] = $obj;
                        }
                    }
                }

                $this->collContactMessages = $collContactMessages;
                $this->collContactMessagesPartial = false;
            }
        }

        return $this->collContactMessages;
    }

    /**
     * Sets a collection of ContactMessage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $contactMessages A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function setContactMessages(Collection $contactMessages, ConnectionInterface $con = null)
    {
        /** @var ContactMessage[] $contactMessagesToDelete */
        $contactMessagesToDelete = $this->getContactMessages(new Criteria(), $con)->diff($contactMessages);


        $this->contactMessagesScheduledForDeletion = $contactMessagesToDelete;

        foreach ($contactMessagesToDelete as $contactMessageRemoved) {
            $contactMessageRemoved->setCustomer(null);
        }

        $this->collContactMessages = null;
        foreach ($contactMessages as $contactMessage) {
            $this->addContactMessage($contactMessage);
        }

        $this->collContactMessages = $contactMessages;
        $this->collContactMessagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseContactMessage objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseContactMessage objects.
     * @throws PropelException
     */
    public function countContactMessages(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collContactMessagesPartial && !$this->isNew();
        if (null === $this->collContactMessages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collContactMessages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getContactMessages());
            }

            $query = ContactMessageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCustomer($this)
                ->count($con);
        }

        return count($this->collContactMessages);
    }

    /**
     * Method called to associate a ContactMessage object to this object
     * through the ContactMessage foreign key attribute.
     *
     * @param  ContactMessage $l ContactMessage
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function addContactMessage(ContactMessage $l)
    {
        if ($this->collContactMessages === null) {
            $this->initContactMessages();
            $this->collContactMessagesPartial = true;
        }

        if (!$this->collContactMessages->contains($l)) {
            $this->doAddContactMessage($l);

            if ($this->contactMessagesScheduledForDeletion and $this->contactMessagesScheduledForDeletion->contains($l)) {
                $this->contactMessagesScheduledForDeletion->remove($this->contactMessagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ContactMessage $contactMessage The ContactMessage object to add.
     */
    protected function doAddContactMessage(ContactMessage $contactMessage)
    {
        $this->collContactMessages[]= $contactMessage;
        $contactMessage->setCustomer($this);
    }

    /**
     * @param  ContactMessage $contactMessage The ContactMessage object to remove.
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function removeContactMessage(ContactMessage $contactMessage)
    {
        if ($this->getContactMessages()->contains($contactMessage)) {
            $pos = $this->collContactMessages->search($contactMessage);
            $this->collContactMessages->remove($pos);
            if (null === $this->contactMessagesScheduledForDeletion) {
                $this->contactMessagesScheduledForDeletion = clone $this->collContactMessages;
                $this->contactMessagesScheduledForDeletion->clear();
            }
            $this->contactMessagesScheduledForDeletion[]= $contactMessage;
            $contactMessage->setCustomer(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer is new, it will return
     * an empty collection; or if this Customer has previously
     * been saved, it will retrieve related ContactMessages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Customer.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ContactMessage[] List of ContactMessage objects
     */
    public function getContactMessagesJoinLanguage(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ContactMessageQuery::create(null, $criteria);
        $query->joinWith('Language', $joinBehavior);

        return $this->getContactMessages($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer is new, it will return
     * an empty collection; or if this Customer has previously
     * been saved, it will retrieve related ContactMessages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Customer.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ContactMessage[] List of ContactMessage objects
     */
    public function getContactMessagesJoinContactMessageType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ContactMessageQuery::create(null, $criteria);
        $query->joinWith('ContactMessageType', $joinBehavior);

        return $this->getContactMessages($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer is new, it will return
     * an empty collection; or if this Customer has previously
     * been saved, it will retrieve related ContactMessages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Customer.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ContactMessage[] List of ContactMessage objects
     */
    public function getContactMessagesJoinContactMessageStatus(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ContactMessageQuery::create(null, $criteria);
        $query->joinWith('ContactMessageStatus', $joinBehavior);

        return $this->getContactMessages($query, $con);
    }

    /**
     * Clears out the collNewsletterSubscribers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addNewsletterSubscribers()
     */
    public function clearNewsletterSubscribers()
    {
        $this->collNewsletterSubscribers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collNewsletterSubscribers collection loaded partially.
     */
    public function resetPartialNewsletterSubscribers($v = true)
    {
        $this->collNewsletterSubscribersPartial = $v;
    }

    /**
     * Initializes the collNewsletterSubscribers collection.
     *
     * By default this just sets the collNewsletterSubscribers collection to an empty array (like clearcollNewsletterSubscribers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initNewsletterSubscribers($overrideExisting = true)
    {
        if (null !== $this->collNewsletterSubscribers && !$overrideExisting) {
            return;
        }

        $collectionClassName = NewsletterSubscriberTableMap::getTableMap()->getCollectionClassName();

        $this->collNewsletterSubscribers = new $collectionClassName;
        $this->collNewsletterSubscribers->setModel('\Model\Crm\NewsletterSubscriber');
    }

    /**
     * Gets an array of ChildNewsletterSubscriber objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCustomer is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildNewsletterSubscriber[] List of ChildNewsletterSubscriber objects
     * @throws PropelException
     */
    public function getNewsletterSubscribers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collNewsletterSubscribersPartial && !$this->isNew();
        if (null === $this->collNewsletterSubscribers || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collNewsletterSubscribers) {
                    $this->initNewsletterSubscribers();
                } else {
                    $collectionClassName = NewsletterSubscriberTableMap::getTableMap()->getCollectionClassName();

                    $collNewsletterSubscribers = new $collectionClassName;
                    $collNewsletterSubscribers->setModel('\Model\Crm\NewsletterSubscriber');

                    return $collNewsletterSubscribers;
                }
            } else {
                $collNewsletterSubscribers = ChildNewsletterSubscriberQuery::create(null, $criteria)
                    ->filterByCustomer($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collNewsletterSubscribersPartial && count($collNewsletterSubscribers)) {
                        $this->initNewsletterSubscribers(false);

                        foreach ($collNewsletterSubscribers as $obj) {
                            if (false == $this->collNewsletterSubscribers->contains($obj)) {
                                $this->collNewsletterSubscribers->append($obj);
                            }
                        }

                        $this->collNewsletterSubscribersPartial = true;
                    }

                    return $collNewsletterSubscribers;
                }

                if ($partial && $this->collNewsletterSubscribers) {
                    foreach ($this->collNewsletterSubscribers as $obj) {
                        if ($obj->isNew()) {
                            $collNewsletterSubscribers[] = $obj;
                        }
                    }
                }

                $this->collNewsletterSubscribers = $collNewsletterSubscribers;
                $this->collNewsletterSubscribersPartial = false;
            }
        }

        return $this->collNewsletterSubscribers;
    }

    /**
     * Sets a collection of ChildNewsletterSubscriber objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $newsletterSubscribers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function setNewsletterSubscribers(Collection $newsletterSubscribers, ConnectionInterface $con = null)
    {
        /** @var ChildNewsletterSubscriber[] $newsletterSubscribersToDelete */
        $newsletterSubscribersToDelete = $this->getNewsletterSubscribers(new Criteria(), $con)->diff($newsletterSubscribers);


        $this->newsletterSubscribersScheduledForDeletion = $newsletterSubscribersToDelete;

        foreach ($newsletterSubscribersToDelete as $newsletterSubscriberRemoved) {
            $newsletterSubscriberRemoved->setCustomer(null);
        }

        $this->collNewsletterSubscribers = null;
        foreach ($newsletterSubscribers as $newsletterSubscriber) {
            $this->addNewsletterSubscriber($newsletterSubscriber);
        }

        $this->collNewsletterSubscribers = $newsletterSubscribers;
        $this->collNewsletterSubscribersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related NewsletterSubscriber objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related NewsletterSubscriber objects.
     * @throws PropelException
     */
    public function countNewsletterSubscribers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collNewsletterSubscribersPartial && !$this->isNew();
        if (null === $this->collNewsletterSubscribers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collNewsletterSubscribers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getNewsletterSubscribers());
            }

            $query = ChildNewsletterSubscriberQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCustomer($this)
                ->count($con);
        }

        return count($this->collNewsletterSubscribers);
    }

    /**
     * Method called to associate a ChildNewsletterSubscriber object to this object
     * through the ChildNewsletterSubscriber foreign key attribute.
     *
     * @param  ChildNewsletterSubscriber $l ChildNewsletterSubscriber
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function addNewsletterSubscriber(ChildNewsletterSubscriber $l)
    {
        if ($this->collNewsletterSubscribers === null) {
            $this->initNewsletterSubscribers();
            $this->collNewsletterSubscribersPartial = true;
        }

        if (!$this->collNewsletterSubscribers->contains($l)) {
            $this->doAddNewsletterSubscriber($l);

            if ($this->newsletterSubscribersScheduledForDeletion and $this->newsletterSubscribersScheduledForDeletion->contains($l)) {
                $this->newsletterSubscribersScheduledForDeletion->remove($this->newsletterSubscribersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildNewsletterSubscriber $newsletterSubscriber The ChildNewsletterSubscriber object to add.
     */
    protected function doAddNewsletterSubscriber(ChildNewsletterSubscriber $newsletterSubscriber)
    {
        $this->collNewsletterSubscribers[]= $newsletterSubscriber;
        $newsletterSubscriber->setCustomer($this);
    }

    /**
     * @param  ChildNewsletterSubscriber $newsletterSubscriber The ChildNewsletterSubscriber object to remove.
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function removeNewsletterSubscriber(ChildNewsletterSubscriber $newsletterSubscriber)
    {
        if ($this->getNewsletterSubscribers()->contains($newsletterSubscriber)) {
            $pos = $this->collNewsletterSubscribers->search($newsletterSubscriber);
            $this->collNewsletterSubscribers->remove($pos);
            if (null === $this->newsletterSubscribersScheduledForDeletion) {
                $this->newsletterSubscribersScheduledForDeletion = clone $this->collNewsletterSubscribers;
                $this->newsletterSubscribersScheduledForDeletion->clear();
            }
            $this->newsletterSubscribersScheduledForDeletion[]= $newsletterSubscriber;
            $newsletterSubscriber->setCustomer(null);
        }

        return $this;
    }

    /**
     * Clears out the collCustomerCompanies collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomerCompanies()
     */
    public function clearCustomerCompanies()
    {
        $this->collCustomerCompanies = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomerCompanies collection loaded partially.
     */
    public function resetPartialCustomerCompanies($v = true)
    {
        $this->collCustomerCompaniesPartial = $v;
    }

    /**
     * Initializes the collCustomerCompanies collection.
     *
     * By default this just sets the collCustomerCompanies collection to an empty array (like clearcollCustomerCompanies());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomerCompanies($overrideExisting = true)
    {
        if (null !== $this->collCustomerCompanies && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomerCompanyTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomerCompanies = new $collectionClassName;
        $this->collCustomerCompanies->setModel('\Model\Crm\CustomerCompany');
    }

    /**
     * Gets an array of ChildCustomerCompany objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCustomer is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCustomerCompany[] List of ChildCustomerCompany objects
     * @throws PropelException
     */
    public function getCustomerCompanies(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerCompaniesPartial && !$this->isNew();
        if (null === $this->collCustomerCompanies || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCustomerCompanies) {
                    $this->initCustomerCompanies();
                } else {
                    $collectionClassName = CustomerCompanyTableMap::getTableMap()->getCollectionClassName();

                    $collCustomerCompanies = new $collectionClassName;
                    $collCustomerCompanies->setModel('\Model\Crm\CustomerCompany');

                    return $collCustomerCompanies;
                }
            } else {
                $collCustomerCompanies = ChildCustomerCompanyQuery::create(null, $criteria)
                    ->filterByCustomer($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomerCompaniesPartial && count($collCustomerCompanies)) {
                        $this->initCustomerCompanies(false);

                        foreach ($collCustomerCompanies as $obj) {
                            if (false == $this->collCustomerCompanies->contains($obj)) {
                                $this->collCustomerCompanies->append($obj);
                            }
                        }

                        $this->collCustomerCompaniesPartial = true;
                    }

                    return $collCustomerCompanies;
                }

                if ($partial && $this->collCustomerCompanies) {
                    foreach ($this->collCustomerCompanies as $obj) {
                        if ($obj->isNew()) {
                            $collCustomerCompanies[] = $obj;
                        }
                    }
                }

                $this->collCustomerCompanies = $collCustomerCompanies;
                $this->collCustomerCompaniesPartial = false;
            }
        }

        return $this->collCustomerCompanies;
    }

    /**
     * Sets a collection of ChildCustomerCompany objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customerCompanies A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function setCustomerCompanies(Collection $customerCompanies, ConnectionInterface $con = null)
    {
        /** @var ChildCustomerCompany[] $customerCompaniesToDelete */
        $customerCompaniesToDelete = $this->getCustomerCompanies(new Criteria(), $con)->diff($customerCompanies);


        $this->customerCompaniesScheduledForDeletion = $customerCompaniesToDelete;

        foreach ($customerCompaniesToDelete as $customerCompanyRemoved) {
            $customerCompanyRemoved->setCustomer(null);
        }

        $this->collCustomerCompanies = null;
        foreach ($customerCompanies as $customerCompany) {
            $this->addCustomerCompany($customerCompany);
        }

        $this->collCustomerCompanies = $customerCompanies;
        $this->collCustomerCompaniesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CustomerCompany objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CustomerCompany objects.
     * @throws PropelException
     */
    public function countCustomerCompanies(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerCompaniesPartial && !$this->isNew();
        if (null === $this->collCustomerCompanies || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomerCompanies) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomerCompanies());
            }

            $query = ChildCustomerCompanyQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCustomer($this)
                ->count($con);
        }

        return count($this->collCustomerCompanies);
    }

    /**
     * Method called to associate a ChildCustomerCompany object to this object
     * through the ChildCustomerCompany foreign key attribute.
     *
     * @param  ChildCustomerCompany $l ChildCustomerCompany
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function addCustomerCompany(ChildCustomerCompany $l)
    {
        if ($this->collCustomerCompanies === null) {
            $this->initCustomerCompanies();
            $this->collCustomerCompaniesPartial = true;
        }

        if (!$this->collCustomerCompanies->contains($l)) {
            $this->doAddCustomerCompany($l);

            if ($this->customerCompaniesScheduledForDeletion and $this->customerCompaniesScheduledForDeletion->contains($l)) {
                $this->customerCompaniesScheduledForDeletion->remove($this->customerCompaniesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCustomerCompany $customerCompany The ChildCustomerCompany object to add.
     */
    protected function doAddCustomerCompany(ChildCustomerCompany $customerCompany)
    {
        $this->collCustomerCompanies[]= $customerCompany;
        $customerCompany->setCustomer($this);
    }

    /**
     * @param  ChildCustomerCompany $customerCompany The ChildCustomerCompany object to remove.
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function removeCustomerCompany(ChildCustomerCompany $customerCompany)
    {
        if ($this->getCustomerCompanies()->contains($customerCompany)) {
            $pos = $this->collCustomerCompanies->search($customerCompany);
            $this->collCustomerCompanies->remove($pos);
            if (null === $this->customerCompaniesScheduledForDeletion) {
                $this->customerCompaniesScheduledForDeletion = clone $this->collCustomerCompanies;
                $this->customerCompaniesScheduledForDeletion->clear();
            }
            $this->customerCompaniesScheduledForDeletion[]= $customerCompany;
            $customerCompany->setCustomer(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer is new, it will return
     * an empty collection; or if this Customer has previously
     * been saved, it will retrieve related CustomerCompanies from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Customer.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCustomerCompany[] List of ChildCustomerCompany objects
     */
    public function getCustomerCompaniesJoinCompanyCategory(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCustomerCompanyQuery::create(null, $criteria);
        $query->joinWith('CompanyCategory', $joinBehavior);

        return $this->getCustomerCompanies($query, $con);
    }

    /**
     * Clears out the collCustomers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomers()
     */
    public function clearCustomers()
    {
        $this->collCustomers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomers collection loaded partially.
     */
    public function resetPartialCustomers($v = true)
    {
        $this->collCustomersPartial = $v;
    }

    /**
     * Initializes the collCustomers collection.
     *
     * By default this just sets the collCustomers collection to an empty array (like clearcollCustomers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomers($overrideExisting = true)
    {
        if (null !== $this->collCustomers && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomerAddressTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomers = new $collectionClassName;
        $this->collCustomers->setModel('\Model\Crm\CustomerAddress');
    }

    /**
     * Gets an array of ChildCustomerAddress objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCustomer is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCustomerAddress[] List of ChildCustomerAddress objects
     * @throws PropelException
     */
    public function getCustomers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomersPartial && !$this->isNew();
        if (null === $this->collCustomers || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCustomers) {
                    $this->initCustomers();
                } else {
                    $collectionClassName = CustomerAddressTableMap::getTableMap()->getCollectionClassName();

                    $collCustomers = new $collectionClassName;
                    $collCustomers->setModel('\Model\Crm\CustomerAddress');

                    return $collCustomers;
                }
            } else {
                $collCustomers = ChildCustomerAddressQuery::create(null, $criteria)
                    ->filterByCustomer($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomersPartial && count($collCustomers)) {
                        $this->initCustomers(false);

                        foreach ($collCustomers as $obj) {
                            if (false == $this->collCustomers->contains($obj)) {
                                $this->collCustomers->append($obj);
                            }
                        }

                        $this->collCustomersPartial = true;
                    }

                    return $collCustomers;
                }

                if ($partial && $this->collCustomers) {
                    foreach ($this->collCustomers as $obj) {
                        if ($obj->isNew()) {
                            $collCustomers[] = $obj;
                        }
                    }
                }

                $this->collCustomers = $collCustomers;
                $this->collCustomersPartial = false;
            }
        }

        return $this->collCustomers;
    }

    /**
     * Sets a collection of ChildCustomerAddress objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function setCustomers(Collection $customers, ConnectionInterface $con = null)
    {
        /** @var ChildCustomerAddress[] $customersToDelete */
        $customersToDelete = $this->getCustomers(new Criteria(), $con)->diff($customers);


        $this->customersScheduledForDeletion = $customersToDelete;

        foreach ($customersToDelete as $customerRemoved) {
            $customerRemoved->setCustomer(null);
        }

        $this->collCustomers = null;
        foreach ($customers as $customer) {
            $this->addCustomer($customer);
        }

        $this->collCustomers = $customers;
        $this->collCustomersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CustomerAddress objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CustomerAddress objects.
     * @throws PropelException
     */
    public function countCustomers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomersPartial && !$this->isNew();
        if (null === $this->collCustomers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomers());
            }

            $query = ChildCustomerAddressQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCustomer($this)
                ->count($con);
        }

        return count($this->collCustomers);
    }

    /**
     * Method called to associate a ChildCustomerAddress object to this object
     * through the ChildCustomerAddress foreign key attribute.
     *
     * @param  ChildCustomerAddress $l ChildCustomerAddress
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function addCustomer(ChildCustomerAddress $l)
    {
        if ($this->collCustomers === null) {
            $this->initCustomers();
            $this->collCustomersPartial = true;
        }

        if (!$this->collCustomers->contains($l)) {
            $this->doAddCustomer($l);

            if ($this->customersScheduledForDeletion and $this->customersScheduledForDeletion->contains($l)) {
                $this->customersScheduledForDeletion->remove($this->customersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCustomerAddress $customer The ChildCustomerAddress object to add.
     */
    protected function doAddCustomer(ChildCustomerAddress $customer)
    {
        $this->collCustomers[]= $customer;
        $customer->setCustomer($this);
    }

    /**
     * @param  ChildCustomerAddress $customer The ChildCustomerAddress object to remove.
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function removeCustomer(ChildCustomerAddress $customer)
    {
        if ($this->getCustomers()->contains($customer)) {
            $pos = $this->collCustomers->search($customer);
            $this->collCustomers->remove($pos);
            if (null === $this->customersScheduledForDeletion) {
                $this->customersScheduledForDeletion = clone $this->collCustomers;
                $this->customersScheduledForDeletion->clear();
            }
            $this->customersScheduledForDeletion[]= clone $customer;
            $customer->setCustomer(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer is new, it will return
     * an empty collection; or if this Customer has previously
     * been saved, it will retrieve related Customers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Customer.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCustomerAddress[] List of ChildCustomerAddress objects
     */
    public function getCustomersJoinCountry(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCustomerAddressQuery::create(null, $criteria);
        $query->joinWith('Country', $joinBehavior);

        return $this->getCustomers($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer is new, it will return
     * an empty collection; or if this Customer has previously
     * been saved, it will retrieve related Customers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Customer.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCustomerAddress[] List of ChildCustomerAddress objects
     */
    public function getCustomersJoinUsaState(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCustomerAddressQuery::create(null, $criteria);
        $query->joinWith('UsaState', $joinBehavior);

        return $this->getCustomers($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer is new, it will return
     * an empty collection; or if this Customer has previously
     * been saved, it will retrieve related Customers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Customer.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCustomerAddress[] List of ChildCustomerAddress objects
     */
    public function getCustomersJoinCustomerAddressType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCustomerAddressQuery::create(null, $criteria);
        $query->joinWith('CustomerAddressType', $joinBehavior);

        return $this->getCustomers($query, $con);
    }

    /**
     * Clears out the collCustomerNotes collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomerNotes()
     */
    public function clearCustomerNotes()
    {
        $this->collCustomerNotes = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomerNotes collection loaded partially.
     */
    public function resetPartialCustomerNotes($v = true)
    {
        $this->collCustomerNotesPartial = $v;
    }

    /**
     * Initializes the collCustomerNotes collection.
     *
     * By default this just sets the collCustomerNotes collection to an empty array (like clearcollCustomerNotes());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomerNotes($overrideExisting = true)
    {
        if (null !== $this->collCustomerNotes && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomerNoteTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomerNotes = new $collectionClassName;
        $this->collCustomerNotes->setModel('\Model\Crm\CustomerNote');
    }

    /**
     * Gets an array of ChildCustomerNote objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCustomer is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCustomerNote[] List of ChildCustomerNote objects
     * @throws PropelException
     */
    public function getCustomerNotes(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerNotesPartial && !$this->isNew();
        if (null === $this->collCustomerNotes || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCustomerNotes) {
                    $this->initCustomerNotes();
                } else {
                    $collectionClassName = CustomerNoteTableMap::getTableMap()->getCollectionClassName();

                    $collCustomerNotes = new $collectionClassName;
                    $collCustomerNotes->setModel('\Model\Crm\CustomerNote');

                    return $collCustomerNotes;
                }
            } else {
                $collCustomerNotes = ChildCustomerNoteQuery::create(null, $criteria)
                    ->filterByCustomer($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomerNotesPartial && count($collCustomerNotes)) {
                        $this->initCustomerNotes(false);

                        foreach ($collCustomerNotes as $obj) {
                            if (false == $this->collCustomerNotes->contains($obj)) {
                                $this->collCustomerNotes->append($obj);
                            }
                        }

                        $this->collCustomerNotesPartial = true;
                    }

                    return $collCustomerNotes;
                }

                if ($partial && $this->collCustomerNotes) {
                    foreach ($this->collCustomerNotes as $obj) {
                        if ($obj->isNew()) {
                            $collCustomerNotes[] = $obj;
                        }
                    }
                }

                $this->collCustomerNotes = $collCustomerNotes;
                $this->collCustomerNotesPartial = false;
            }
        }

        return $this->collCustomerNotes;
    }

    /**
     * Sets a collection of ChildCustomerNote objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customerNotes A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function setCustomerNotes(Collection $customerNotes, ConnectionInterface $con = null)
    {
        /** @var ChildCustomerNote[] $customerNotesToDelete */
        $customerNotesToDelete = $this->getCustomerNotes(new Criteria(), $con)->diff($customerNotes);


        $this->customerNotesScheduledForDeletion = $customerNotesToDelete;

        foreach ($customerNotesToDelete as $customerNoteRemoved) {
            $customerNoteRemoved->setCustomer(null);
        }

        $this->collCustomerNotes = null;
        foreach ($customerNotes as $customerNote) {
            $this->addCustomerNote($customerNote);
        }

        $this->collCustomerNotes = $customerNotes;
        $this->collCustomerNotesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CustomerNote objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CustomerNote objects.
     * @throws PropelException
     */
    public function countCustomerNotes(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerNotesPartial && !$this->isNew();
        if (null === $this->collCustomerNotes || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomerNotes) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomerNotes());
            }

            $query = ChildCustomerNoteQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCustomer($this)
                ->count($con);
        }

        return count($this->collCustomerNotes);
    }

    /**
     * Method called to associate a ChildCustomerNote object to this object
     * through the ChildCustomerNote foreign key attribute.
     *
     * @param  ChildCustomerNote $l ChildCustomerNote
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function addCustomerNote(ChildCustomerNote $l)
    {
        if ($this->collCustomerNotes === null) {
            $this->initCustomerNotes();
            $this->collCustomerNotesPartial = true;
        }

        if (!$this->collCustomerNotes->contains($l)) {
            $this->doAddCustomerNote($l);

            if ($this->customerNotesScheduledForDeletion and $this->customerNotesScheduledForDeletion->contains($l)) {
                $this->customerNotesScheduledForDeletion->remove($this->customerNotesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCustomerNote $customerNote The ChildCustomerNote object to add.
     */
    protected function doAddCustomerNote(ChildCustomerNote $customerNote)
    {
        $this->collCustomerNotes[]= $customerNote;
        $customerNote->setCustomer($this);
    }

    /**
     * @param  ChildCustomerNote $customerNote The ChildCustomerNote object to remove.
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function removeCustomerNote(ChildCustomerNote $customerNote)
    {
        if ($this->getCustomerNotes()->contains($customerNote)) {
            $pos = $this->collCustomerNotes->search($customerNote);
            $this->collCustomerNotes->remove($pos);
            if (null === $this->customerNotesScheduledForDeletion) {
                $this->customerNotesScheduledForDeletion = clone $this->collCustomerNotes;
                $this->customerNotesScheduledForDeletion->clear();
            }
            $this->customerNotesScheduledForDeletion[]= clone $customerNote;
            $customerNote->setCustomer(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer is new, it will return
     * an empty collection; or if this Customer has previously
     * been saved, it will retrieve related CustomerNotes from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Customer.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCustomerNote[] List of ChildCustomerNote objects
     */
    public function getCustomerNotesJoinUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCustomerNoteQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getCustomerNotes($query, $con);
    }

    /**
     * Clears out the collCustomerProperties collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomerProperties()
     */
    public function clearCustomerProperties()
    {
        $this->collCustomerProperties = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomerProperties collection loaded partially.
     */
    public function resetPartialCustomerProperties($v = true)
    {
        $this->collCustomerPropertiesPartial = $v;
    }

    /**
     * Initializes the collCustomerProperties collection.
     *
     * By default this just sets the collCustomerProperties collection to an empty array (like clearcollCustomerProperties());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomerProperties($overrideExisting = true)
    {
        if (null !== $this->collCustomerProperties && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomerPropertyTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomerProperties = new $collectionClassName;
        $this->collCustomerProperties->setModel('\Model\Crm\CustomerProperty');
    }

    /**
     * Gets an array of ChildCustomerProperty objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCustomer is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCustomerProperty[] List of ChildCustomerProperty objects
     * @throws PropelException
     */
    public function getCustomerProperties(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerPropertiesPartial && !$this->isNew();
        if (null === $this->collCustomerProperties || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCustomerProperties) {
                    $this->initCustomerProperties();
                } else {
                    $collectionClassName = CustomerPropertyTableMap::getTableMap()->getCollectionClassName();

                    $collCustomerProperties = new $collectionClassName;
                    $collCustomerProperties->setModel('\Model\Crm\CustomerProperty');

                    return $collCustomerProperties;
                }
            } else {
                $collCustomerProperties = ChildCustomerPropertyQuery::create(null, $criteria)
                    ->filterByCustomer($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomerPropertiesPartial && count($collCustomerProperties)) {
                        $this->initCustomerProperties(false);

                        foreach ($collCustomerProperties as $obj) {
                            if (false == $this->collCustomerProperties->contains($obj)) {
                                $this->collCustomerProperties->append($obj);
                            }
                        }

                        $this->collCustomerPropertiesPartial = true;
                    }

                    return $collCustomerProperties;
                }

                if ($partial && $this->collCustomerProperties) {
                    foreach ($this->collCustomerProperties as $obj) {
                        if ($obj->isNew()) {
                            $collCustomerProperties[] = $obj;
                        }
                    }
                }

                $this->collCustomerProperties = $collCustomerProperties;
                $this->collCustomerPropertiesPartial = false;
            }
        }

        return $this->collCustomerProperties;
    }

    /**
     * Sets a collection of ChildCustomerProperty objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customerProperties A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function setCustomerProperties(Collection $customerProperties, ConnectionInterface $con = null)
    {
        /** @var ChildCustomerProperty[] $customerPropertiesToDelete */
        $customerPropertiesToDelete = $this->getCustomerProperties(new Criteria(), $con)->diff($customerProperties);


        $this->customerPropertiesScheduledForDeletion = $customerPropertiesToDelete;

        foreach ($customerPropertiesToDelete as $customerPropertyRemoved) {
            $customerPropertyRemoved->setCustomer(null);
        }

        $this->collCustomerProperties = null;
        foreach ($customerProperties as $customerProperty) {
            $this->addCustomerProperty($customerProperty);
        }

        $this->collCustomerProperties = $customerProperties;
        $this->collCustomerPropertiesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CustomerProperty objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CustomerProperty objects.
     * @throws PropelException
     */
    public function countCustomerProperties(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerPropertiesPartial && !$this->isNew();
        if (null === $this->collCustomerProperties || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomerProperties) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomerProperties());
            }

            $query = ChildCustomerPropertyQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCustomer($this)
                ->count($con);
        }

        return count($this->collCustomerProperties);
    }

    /**
     * Method called to associate a ChildCustomerProperty object to this object
     * through the ChildCustomerProperty foreign key attribute.
     *
     * @param  ChildCustomerProperty $l ChildCustomerProperty
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function addCustomerProperty(ChildCustomerProperty $l)
    {
        if ($this->collCustomerProperties === null) {
            $this->initCustomerProperties();
            $this->collCustomerPropertiesPartial = true;
        }

        if (!$this->collCustomerProperties->contains($l)) {
            $this->doAddCustomerProperty($l);

            if ($this->customerPropertiesScheduledForDeletion and $this->customerPropertiesScheduledForDeletion->contains($l)) {
                $this->customerPropertiesScheduledForDeletion->remove($this->customerPropertiesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCustomerProperty $customerProperty The ChildCustomerProperty object to add.
     */
    protected function doAddCustomerProperty(ChildCustomerProperty $customerProperty)
    {
        $this->collCustomerProperties[]= $customerProperty;
        $customerProperty->setCustomer($this);
    }

    /**
     * @param  ChildCustomerProperty $customerProperty The ChildCustomerProperty object to remove.
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function removeCustomerProperty(ChildCustomerProperty $customerProperty)
    {
        if ($this->getCustomerProperties()->contains($customerProperty)) {
            $pos = $this->collCustomerProperties->search($customerProperty);
            $this->collCustomerProperties->remove($pos);
            if (null === $this->customerPropertiesScheduledForDeletion) {
                $this->customerPropertiesScheduledForDeletion = clone $this->collCustomerProperties;
                $this->customerPropertiesScheduledForDeletion->clear();
            }
            $this->customerPropertiesScheduledForDeletion[]= clone $customerProperty;
            $customerProperty->setCustomer(null);
        }

        return $this;
    }

    /**
     * Clears out the collCustomerWishlists collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomerWishlists()
     */
    public function clearCustomerWishlists()
    {
        $this->collCustomerWishlists = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomerWishlists collection loaded partially.
     */
    public function resetPartialCustomerWishlists($v = true)
    {
        $this->collCustomerWishlistsPartial = $v;
    }

    /**
     * Initializes the collCustomerWishlists collection.
     *
     * By default this just sets the collCustomerWishlists collection to an empty array (like clearcollCustomerWishlists());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomerWishlists($overrideExisting = true)
    {
        if (null !== $this->collCustomerWishlists && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomerWishlistTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomerWishlists = new $collectionClassName;
        $this->collCustomerWishlists->setModel('\Model\Category\CustomerWishlist');
    }

    /**
     * Gets an array of CustomerWishlist objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCustomer is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|CustomerWishlist[] List of CustomerWishlist objects
     * @throws PropelException
     */
    public function getCustomerWishlists(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerWishlistsPartial && !$this->isNew();
        if (null === $this->collCustomerWishlists || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCustomerWishlists) {
                    $this->initCustomerWishlists();
                } else {
                    $collectionClassName = CustomerWishlistTableMap::getTableMap()->getCollectionClassName();

                    $collCustomerWishlists = new $collectionClassName;
                    $collCustomerWishlists->setModel('\Model\Category\CustomerWishlist');

                    return $collCustomerWishlists;
                }
            } else {
                $collCustomerWishlists = CustomerWishlistQuery::create(null, $criteria)
                    ->filterByCustomer($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomerWishlistsPartial && count($collCustomerWishlists)) {
                        $this->initCustomerWishlists(false);

                        foreach ($collCustomerWishlists as $obj) {
                            if (false == $this->collCustomerWishlists->contains($obj)) {
                                $this->collCustomerWishlists->append($obj);
                            }
                        }

                        $this->collCustomerWishlistsPartial = true;
                    }

                    return $collCustomerWishlists;
                }

                if ($partial && $this->collCustomerWishlists) {
                    foreach ($this->collCustomerWishlists as $obj) {
                        if ($obj->isNew()) {
                            $collCustomerWishlists[] = $obj;
                        }
                    }
                }

                $this->collCustomerWishlists = $collCustomerWishlists;
                $this->collCustomerWishlistsPartial = false;
            }
        }

        return $this->collCustomerWishlists;
    }

    /**
     * Sets a collection of CustomerWishlist objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customerWishlists A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function setCustomerWishlists(Collection $customerWishlists, ConnectionInterface $con = null)
    {
        /** @var CustomerWishlist[] $customerWishlistsToDelete */
        $customerWishlistsToDelete = $this->getCustomerWishlists(new Criteria(), $con)->diff($customerWishlists);


        $this->customerWishlistsScheduledForDeletion = $customerWishlistsToDelete;

        foreach ($customerWishlistsToDelete as $customerWishlistRemoved) {
            $customerWishlistRemoved->setCustomer(null);
        }

        $this->collCustomerWishlists = null;
        foreach ($customerWishlists as $customerWishlist) {
            $this->addCustomerWishlist($customerWishlist);
        }

        $this->collCustomerWishlists = $customerWishlists;
        $this->collCustomerWishlistsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseCustomerWishlist objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseCustomerWishlist objects.
     * @throws PropelException
     */
    public function countCustomerWishlists(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerWishlistsPartial && !$this->isNew();
        if (null === $this->collCustomerWishlists || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomerWishlists) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomerWishlists());
            }

            $query = CustomerWishlistQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCustomer($this)
                ->count($con);
        }

        return count($this->collCustomerWishlists);
    }

    /**
     * Method called to associate a CustomerWishlist object to this object
     * through the CustomerWishlist foreign key attribute.
     *
     * @param  CustomerWishlist $l CustomerWishlist
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function addCustomerWishlist(CustomerWishlist $l)
    {
        if ($this->collCustomerWishlists === null) {
            $this->initCustomerWishlists();
            $this->collCustomerWishlistsPartial = true;
        }

        if (!$this->collCustomerWishlists->contains($l)) {
            $this->doAddCustomerWishlist($l);

            if ($this->customerWishlistsScheduledForDeletion and $this->customerWishlistsScheduledForDeletion->contains($l)) {
                $this->customerWishlistsScheduledForDeletion->remove($this->customerWishlistsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param CustomerWishlist $customerWishlist The CustomerWishlist object to add.
     */
    protected function doAddCustomerWishlist(CustomerWishlist $customerWishlist)
    {
        $this->collCustomerWishlists[]= $customerWishlist;
        $customerWishlist->setCustomer($this);
    }

    /**
     * @param  CustomerWishlist $customerWishlist The CustomerWishlist object to remove.
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function removeCustomerWishlist(CustomerWishlist $customerWishlist)
    {
        if ($this->getCustomerWishlists()->contains($customerWishlist)) {
            $pos = $this->collCustomerWishlists->search($customerWishlist);
            $this->collCustomerWishlists->remove($pos);
            if (null === $this->customerWishlistsScheduledForDeletion) {
                $this->customerWishlistsScheduledForDeletion = clone $this->collCustomerWishlists;
                $this->customerWishlistsScheduledForDeletion->clear();
            }
            $this->customerWishlistsScheduledForDeletion[]= clone $customerWishlist;
            $customerWishlist->setCustomer(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Customer is new, it will return
     * an empty collection; or if this Customer has previously
     * been saved, it will retrieve related CustomerWishlists from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Customer.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|CustomerWishlist[] List of CustomerWishlist objects
     */
    public function getCustomerWishlistsJoinProduct(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CustomerWishlistQuery::create(null, $criteria);
        $query->joinWith('Product', $joinBehavior);

        return $this->getCustomerWishlists($query, $con);
    }

    /**
     * Clears out the collCustomerPhotos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomerPhotos()
     */
    public function clearCustomerPhotos()
    {
        $this->collCustomerPhotos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomerPhotos collection loaded partially.
     */
    public function resetPartialCustomerPhotos($v = true)
    {
        $this->collCustomerPhotosPartial = $v;
    }

    /**
     * Initializes the collCustomerPhotos collection.
     *
     * By default this just sets the collCustomerPhotos collection to an empty array (like clearcollCustomerPhotos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomerPhotos($overrideExisting = true)
    {
        if (null !== $this->collCustomerPhotos && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomerPhotoTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomerPhotos = new $collectionClassName;
        $this->collCustomerPhotos->setModel('\Model\Crm\CustomerPhoto');
    }

    /**
     * Gets an array of ChildCustomerPhoto objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCustomer is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCustomerPhoto[] List of ChildCustomerPhoto objects
     * @throws PropelException
     */
    public function getCustomerPhotos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerPhotosPartial && !$this->isNew();
        if (null === $this->collCustomerPhotos || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCustomerPhotos) {
                    $this->initCustomerPhotos();
                } else {
                    $collectionClassName = CustomerPhotoTableMap::getTableMap()->getCollectionClassName();

                    $collCustomerPhotos = new $collectionClassName;
                    $collCustomerPhotos->setModel('\Model\Crm\CustomerPhoto');

                    return $collCustomerPhotos;
                }
            } else {
                $collCustomerPhotos = ChildCustomerPhotoQuery::create(null, $criteria)
                    ->filterByCustomer($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomerPhotosPartial && count($collCustomerPhotos)) {
                        $this->initCustomerPhotos(false);

                        foreach ($collCustomerPhotos as $obj) {
                            if (false == $this->collCustomerPhotos->contains($obj)) {
                                $this->collCustomerPhotos->append($obj);
                            }
                        }

                        $this->collCustomerPhotosPartial = true;
                    }

                    return $collCustomerPhotos;
                }

                if ($partial && $this->collCustomerPhotos) {
                    foreach ($this->collCustomerPhotos as $obj) {
                        if ($obj->isNew()) {
                            $collCustomerPhotos[] = $obj;
                        }
                    }
                }

                $this->collCustomerPhotos = $collCustomerPhotos;
                $this->collCustomerPhotosPartial = false;
            }
        }

        return $this->collCustomerPhotos;
    }

    /**
     * Sets a collection of ChildCustomerPhoto objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customerPhotos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function setCustomerPhotos(Collection $customerPhotos, ConnectionInterface $con = null)
    {
        /** @var ChildCustomerPhoto[] $customerPhotosToDelete */
        $customerPhotosToDelete = $this->getCustomerPhotos(new Criteria(), $con)->diff($customerPhotos);


        $this->customerPhotosScheduledForDeletion = $customerPhotosToDelete;

        foreach ($customerPhotosToDelete as $customerPhotoRemoved) {
            $customerPhotoRemoved->setCustomer(null);
        }

        $this->collCustomerPhotos = null;
        foreach ($customerPhotos as $customerPhoto) {
            $this->addCustomerPhoto($customerPhoto);
        }

        $this->collCustomerPhotos = $customerPhotos;
        $this->collCustomerPhotosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CustomerPhoto objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CustomerPhoto objects.
     * @throws PropelException
     */
    public function countCustomerPhotos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerPhotosPartial && !$this->isNew();
        if (null === $this->collCustomerPhotos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomerPhotos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomerPhotos());
            }

            $query = ChildCustomerPhotoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCustomer($this)
                ->count($con);
        }

        return count($this->collCustomerPhotos);
    }

    /**
     * Method called to associate a ChildCustomerPhoto object to this object
     * through the ChildCustomerPhoto foreign key attribute.
     *
     * @param  ChildCustomerPhoto $l ChildCustomerPhoto
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function addCustomerPhoto(ChildCustomerPhoto $l)
    {
        if ($this->collCustomerPhotos === null) {
            $this->initCustomerPhotos();
            $this->collCustomerPhotosPartial = true;
        }

        if (!$this->collCustomerPhotos->contains($l)) {
            $this->doAddCustomerPhoto($l);

            if ($this->customerPhotosScheduledForDeletion and $this->customerPhotosScheduledForDeletion->contains($l)) {
                $this->customerPhotosScheduledForDeletion->remove($this->customerPhotosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCustomerPhoto $customerPhoto The ChildCustomerPhoto object to add.
     */
    protected function doAddCustomerPhoto(ChildCustomerPhoto $customerPhoto)
    {
        $this->collCustomerPhotos[]= $customerPhoto;
        $customerPhoto->setCustomer($this);
    }

    /**
     * @param  ChildCustomerPhoto $customerPhoto The ChildCustomerPhoto object to remove.
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function removeCustomerPhoto(ChildCustomerPhoto $customerPhoto)
    {
        if ($this->getCustomerPhotos()->contains($customerPhoto)) {
            $pos = $this->collCustomerPhotos->search($customerPhoto);
            $this->collCustomerPhotos->remove($pos);
            if (null === $this->customerPhotosScheduledForDeletion) {
                $this->customerPhotosScheduledForDeletion = clone $this->collCustomerPhotos;
                $this->customerPhotosScheduledForDeletion->clear();
            }
            $this->customerPhotosScheduledForDeletion[]= clone $customerPhoto;
            $customerPhoto->setCustomer(null);
        }

        return $this;
    }

    /**
     * Clears out the collCustomerLogos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomerLogos()
     */
    public function clearCustomerLogos()
    {
        $this->collCustomerLogos = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomerLogos collection loaded partially.
     */
    public function resetPartialCustomerLogos($v = true)
    {
        $this->collCustomerLogosPartial = $v;
    }

    /**
     * Initializes the collCustomerLogos collection.
     *
     * By default this just sets the collCustomerLogos collection to an empty array (like clearcollCustomerLogos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomerLogos($overrideExisting = true)
    {
        if (null !== $this->collCustomerLogos && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomerLogoTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomerLogos = new $collectionClassName;
        $this->collCustomerLogos->setModel('\Model\Crm\CustomerLogo');
    }

    /**
     * Gets an array of ChildCustomerLogo objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCustomer is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCustomerLogo[] List of ChildCustomerLogo objects
     * @throws PropelException
     */
    public function getCustomerLogos(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerLogosPartial && !$this->isNew();
        if (null === $this->collCustomerLogos || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCustomerLogos) {
                    $this->initCustomerLogos();
                } else {
                    $collectionClassName = CustomerLogoTableMap::getTableMap()->getCollectionClassName();

                    $collCustomerLogos = new $collectionClassName;
                    $collCustomerLogos->setModel('\Model\Crm\CustomerLogo');

                    return $collCustomerLogos;
                }
            } else {
                $collCustomerLogos = ChildCustomerLogoQuery::create(null, $criteria)
                    ->filterByCustomer($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomerLogosPartial && count($collCustomerLogos)) {
                        $this->initCustomerLogos(false);

                        foreach ($collCustomerLogos as $obj) {
                            if (false == $this->collCustomerLogos->contains($obj)) {
                                $this->collCustomerLogos->append($obj);
                            }
                        }

                        $this->collCustomerLogosPartial = true;
                    }

                    return $collCustomerLogos;
                }

                if ($partial && $this->collCustomerLogos) {
                    foreach ($this->collCustomerLogos as $obj) {
                        if ($obj->isNew()) {
                            $collCustomerLogos[] = $obj;
                        }
                    }
                }

                $this->collCustomerLogos = $collCustomerLogos;
                $this->collCustomerLogosPartial = false;
            }
        }

        return $this->collCustomerLogos;
    }

    /**
     * Sets a collection of ChildCustomerLogo objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customerLogos A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function setCustomerLogos(Collection $customerLogos, ConnectionInterface $con = null)
    {
        /** @var ChildCustomerLogo[] $customerLogosToDelete */
        $customerLogosToDelete = $this->getCustomerLogos(new Criteria(), $con)->diff($customerLogos);


        $this->customerLogosScheduledForDeletion = $customerLogosToDelete;

        foreach ($customerLogosToDelete as $customerLogoRemoved) {
            $customerLogoRemoved->setCustomer(null);
        }

        $this->collCustomerLogos = null;
        foreach ($customerLogos as $customerLogo) {
            $this->addCustomerLogo($customerLogo);
        }

        $this->collCustomerLogos = $customerLogos;
        $this->collCustomerLogosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CustomerLogo objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CustomerLogo objects.
     * @throws PropelException
     */
    public function countCustomerLogos(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerLogosPartial && !$this->isNew();
        if (null === $this->collCustomerLogos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomerLogos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomerLogos());
            }

            $query = ChildCustomerLogoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCustomer($this)
                ->count($con);
        }

        return count($this->collCustomerLogos);
    }

    /**
     * Method called to associate a ChildCustomerLogo object to this object
     * through the ChildCustomerLogo foreign key attribute.
     *
     * @param  ChildCustomerLogo $l ChildCustomerLogo
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function addCustomerLogo(ChildCustomerLogo $l)
    {
        if ($this->collCustomerLogos === null) {
            $this->initCustomerLogos();
            $this->collCustomerLogosPartial = true;
        }

        if (!$this->collCustomerLogos->contains($l)) {
            $this->doAddCustomerLogo($l);

            if ($this->customerLogosScheduledForDeletion and $this->customerLogosScheduledForDeletion->contains($l)) {
                $this->customerLogosScheduledForDeletion->remove($this->customerLogosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCustomerLogo $customerLogo The ChildCustomerLogo object to add.
     */
    protected function doAddCustomerLogo(ChildCustomerLogo $customerLogo)
    {
        $this->collCustomerLogos[]= $customerLogo;
        $customerLogo->setCustomer($this);
    }

    /**
     * @param  ChildCustomerLogo $customerLogo The ChildCustomerLogo object to remove.
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function removeCustomerLogo(ChildCustomerLogo $customerLogo)
    {
        if ($this->getCustomerLogos()->contains($customerLogo)) {
            $pos = $this->collCustomerLogos->search($customerLogo);
            $this->collCustomerLogos->remove($pos);
            if (null === $this->customerLogosScheduledForDeletion) {
                $this->customerLogosScheduledForDeletion = clone $this->collCustomerLogos;
                $this->customerLogosScheduledForDeletion->clear();
            }
            $this->customerLogosScheduledForDeletion[]= clone $customerLogo;
            $customerLogo->setCustomer(null);
        }

        return $this;
    }

    /**
     * Clears out the collSaleInvoices collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSaleInvoices()
     */
    public function clearSaleInvoices()
    {
        $this->collSaleInvoices = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSaleInvoices collection loaded partially.
     */
    public function resetPartialSaleInvoices($v = true)
    {
        $this->collSaleInvoicesPartial = $v;
    }

    /**
     * Initializes the collSaleInvoices collection.
     *
     * By default this just sets the collSaleInvoices collection to an empty array (like clearcollSaleInvoices());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaleInvoices($overrideExisting = true)
    {
        if (null !== $this->collSaleInvoices && !$overrideExisting) {
            return;
        }

        $collectionClassName = SaleInvoiceTableMap::getTableMap()->getCollectionClassName();

        $this->collSaleInvoices = new $collectionClassName;
        $this->collSaleInvoices->setModel('\Model\Finance\SaleInvoice');
    }

    /**
     * Gets an array of SaleInvoice objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCustomer is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|SaleInvoice[] List of SaleInvoice objects
     * @throws PropelException
     */
    public function getSaleInvoices(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleInvoicesPartial && !$this->isNew();
        if (null === $this->collSaleInvoices || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSaleInvoices) {
                    $this->initSaleInvoices();
                } else {
                    $collectionClassName = SaleInvoiceTableMap::getTableMap()->getCollectionClassName();

                    $collSaleInvoices = new $collectionClassName;
                    $collSaleInvoices->setModel('\Model\Finance\SaleInvoice');

                    return $collSaleInvoices;
                }
            } else {
                $collSaleInvoices = SaleInvoiceQuery::create(null, $criteria)
                    ->filterByCustomer($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSaleInvoicesPartial && count($collSaleInvoices)) {
                        $this->initSaleInvoices(false);

                        foreach ($collSaleInvoices as $obj) {
                            if (false == $this->collSaleInvoices->contains($obj)) {
                                $this->collSaleInvoices->append($obj);
                            }
                        }

                        $this->collSaleInvoicesPartial = true;
                    }

                    return $collSaleInvoices;
                }

                if ($partial && $this->collSaleInvoices) {
                    foreach ($this->collSaleInvoices as $obj) {
                        if ($obj->isNew()) {
                            $collSaleInvoices[] = $obj;
                        }
                    }
                }

                $this->collSaleInvoices = $collSaleInvoices;
                $this->collSaleInvoicesPartial = false;
            }
        }

        return $this->collSaleInvoices;
    }

    /**
     * Sets a collection of SaleInvoice objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $saleInvoices A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function setSaleInvoices(Collection $saleInvoices, ConnectionInterface $con = null)
    {
        /** @var SaleInvoice[] $saleInvoicesToDelete */
        $saleInvoicesToDelete = $this->getSaleInvoices(new Criteria(), $con)->diff($saleInvoices);


        $this->saleInvoicesScheduledForDeletion = $saleInvoicesToDelete;

        foreach ($saleInvoicesToDelete as $saleInvoiceRemoved) {
            $saleInvoiceRemoved->setCustomer(null);
        }

        $this->collSaleInvoices = null;
        foreach ($saleInvoices as $saleInvoice) {
            $this->addSaleInvoice($saleInvoice);
        }

        $this->collSaleInvoices = $saleInvoices;
        $this->collSaleInvoicesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSaleInvoice objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSaleInvoice objects.
     * @throws PropelException
     */
    public function countSaleInvoices(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleInvoicesPartial && !$this->isNew();
        if (null === $this->collSaleInvoices || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaleInvoices) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSaleInvoices());
            }

            $query = SaleInvoiceQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCustomer($this)
                ->count($con);
        }

        return count($this->collSaleInvoices);
    }

    /**
     * Method called to associate a SaleInvoice object to this object
     * through the SaleInvoice foreign key attribute.
     *
     * @param  SaleInvoice $l SaleInvoice
     * @return $this|\Model\Crm\Customer The current object (for fluent API support)
     */
    public function addSaleInvoice(SaleInvoice $l)
    {
        if ($this->collSaleInvoices === null) {
            $this->initSaleInvoices();
            $this->collSaleInvoicesPartial = true;
        }

        if (!$this->collSaleInvoices->contains($l)) {
            $this->doAddSaleInvoice($l);

            if ($this->saleInvoicesScheduledForDeletion and $this->saleInvoicesScheduledForDeletion->contains($l)) {
                $this->saleInvoicesScheduledForDeletion->remove($this->saleInvoicesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param SaleInvoice $saleInvoice The SaleInvoice object to add.
     */
    protected function doAddSaleInvoice(SaleInvoice $saleInvoice)
    {
        $this->collSaleInvoices[]= $saleInvoice;
        $saleInvoice->setCustomer($this);
    }

    /**
     * @param  SaleInvoice $saleInvoice The SaleInvoice object to remove.
     * @return $this|ChildCustomer The current object (for fluent API support)
     */
    public function removeSaleInvoice(SaleInvoice $saleInvoice)
    {
        if ($this->getSaleInvoices()->contains($saleInvoice)) {
            $pos = $this->collSaleInvoices->search($saleInvoice);
            $this->collSaleInvoices->remove($pos);
            if (null === $this->saleInvoicesScheduledForDeletion) {
                $this->saleInvoicesScheduledForDeletion = clone $this->collSaleInvoices;
                $this->saleInvoicesScheduledForDeletion->clear();
            }
            $this->saleInvoicesScheduledForDeletion[]= $saleInvoice;
            $saleInvoice->setCustomer(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aLanguage) {
            $this->aLanguage->removeCustomer($this);
        }
        if (null !== $this->aBranche) {
            $this->aBranche->removeCustomer($this);
        }
        if (null !== $this->aCustomerSector) {
            $this->aCustomerSector->removeCustomerSector($this);
        }
        if (null !== $this->aPayTerm) {
            $this->aPayTerm->removePayTerm($this);
        }
        if (null !== $this->aCustomerType) {
            $this->aCustomerType->removeCustomerType($this);
        }
        $this->id = null;
        $this->import_source = null;
        $this->accounting_id = null;
        $this->old_id = null;
        $this->ip = null;
        $this->created_on = null;
        $this->is_deleted = null;
        $this->is_newsletter_subscriber = null;
        $this->language_id = null;
        $this->is_intracommunautair = null;
        $this->payterm_id = null;
        $this->branche_id = null;
        $this->sector_id = null;
        $this->customer_type_id = null;
        $this->debitor = null;
        $this->gender = null;
        $this->first_name = null;
        $this->insertion = null;
        $this->last_name = null;
        $this->email = null;
        $this->password = null;
        $this->salt = null;
        $this->once_password_reset = null;
        $this->is_wholesale = null;
        $this->is_wholesale_approved = null;
        $this->can_buy_on_credit = null;
        $this->iban = null;
        $this->phone = null;
        $this->mobile = null;
        $this->fax = null;
        $this->website = null;
        $this->tag = null;
        $this->is_verified = null;
        $this->verification_key = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collContactMessages) {
                foreach ($this->collContactMessages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collNewsletterSubscribers) {
                foreach ($this->collNewsletterSubscribers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCustomerCompanies) {
                foreach ($this->collCustomerCompanies as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCustomers) {
                foreach ($this->collCustomers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCustomerNotes) {
                foreach ($this->collCustomerNotes as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCustomerProperties) {
                foreach ($this->collCustomerProperties as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCustomerWishlists) {
                foreach ($this->collCustomerWishlists as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCustomerPhotos) {
                foreach ($this->collCustomerPhotos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCustomerLogos) {
                foreach ($this->collCustomerLogos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaleInvoices) {
                foreach ($this->collSaleInvoices as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collContactMessages = null;
        $this->collNewsletterSubscribers = null;
        $this->collCustomerCompanies = null;
        $this->collCustomers = null;
        $this->collCustomerNotes = null;
        $this->collCustomerProperties = null;
        $this->collCustomerWishlists = null;
        $this->collCustomerPhotos = null;
        $this->collCustomerLogos = null;
        $this->collSaleInvoices = null;
        $this->aLanguage = null;
        $this->aBranche = null;
        $this->aCustomerSector = null;
        $this->aPayTerm = null;
        $this->aCustomerType = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CustomerTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
