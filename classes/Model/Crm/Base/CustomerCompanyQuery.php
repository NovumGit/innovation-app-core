<?php

namespace Model\Crm\Base;

use \Exception;
use \PDO;
use Model\Crm\CustomerCompany as ChildCustomerCompany;
use Model\Crm\CustomerCompanyQuery as ChildCustomerCompanyQuery;
use Model\Crm\Map\CustomerCompanyTableMap;
use Model\Setting\MasterTable\MtCompanyCategory;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'customer_company' table.
 *
 *
 *
 * @method     ChildCustomerCompanyQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCustomerCompanyQuery orderByCustomerId($order = Criteria::ASC) Order by the customer_id column
 * @method     ChildCustomerCompanyQuery orderByLegalFormId($order = Criteria::ASC) Order by the legal_form_id column
 * @method     ChildCustomerCompanyQuery orderByCompanyTypeId($order = Criteria::ASC) Order by the company_type_id column
 * @method     ChildCustomerCompanyQuery orderByCompanyName($order = Criteria::ASC) Order by the company_name column
 * @method     ChildCustomerCompanyQuery orderByChamberOfCommerce($order = Criteria::ASC) Order by the chamber_of_commerce column
 * @method     ChildCustomerCompanyQuery orderByVatNumber($order = Criteria::ASC) Order by the vat_number column
 *
 * @method     ChildCustomerCompanyQuery groupById() Group by the id column
 * @method     ChildCustomerCompanyQuery groupByCustomerId() Group by the customer_id column
 * @method     ChildCustomerCompanyQuery groupByLegalFormId() Group by the legal_form_id column
 * @method     ChildCustomerCompanyQuery groupByCompanyTypeId() Group by the company_type_id column
 * @method     ChildCustomerCompanyQuery groupByCompanyName() Group by the company_name column
 * @method     ChildCustomerCompanyQuery groupByChamberOfCommerce() Group by the chamber_of_commerce column
 * @method     ChildCustomerCompanyQuery groupByVatNumber() Group by the vat_number column
 *
 * @method     ChildCustomerCompanyQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCustomerCompanyQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCustomerCompanyQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCustomerCompanyQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCustomerCompanyQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCustomerCompanyQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCustomerCompanyQuery leftJoinCompanyCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the CompanyCategory relation
 * @method     ChildCustomerCompanyQuery rightJoinCompanyCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CompanyCategory relation
 * @method     ChildCustomerCompanyQuery innerJoinCompanyCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the CompanyCategory relation
 *
 * @method     ChildCustomerCompanyQuery joinWithCompanyCategory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CompanyCategory relation
 *
 * @method     ChildCustomerCompanyQuery leftJoinWithCompanyCategory() Adds a LEFT JOIN clause and with to the query using the CompanyCategory relation
 * @method     ChildCustomerCompanyQuery rightJoinWithCompanyCategory() Adds a RIGHT JOIN clause and with to the query using the CompanyCategory relation
 * @method     ChildCustomerCompanyQuery innerJoinWithCompanyCategory() Adds a INNER JOIN clause and with to the query using the CompanyCategory relation
 *
 * @method     ChildCustomerCompanyQuery leftJoinCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Customer relation
 * @method     ChildCustomerCompanyQuery rightJoinCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Customer relation
 * @method     ChildCustomerCompanyQuery innerJoinCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the Customer relation
 *
 * @method     ChildCustomerCompanyQuery joinWithCustomer($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Customer relation
 *
 * @method     ChildCustomerCompanyQuery leftJoinWithCustomer() Adds a LEFT JOIN clause and with to the query using the Customer relation
 * @method     ChildCustomerCompanyQuery rightJoinWithCustomer() Adds a RIGHT JOIN clause and with to the query using the Customer relation
 * @method     ChildCustomerCompanyQuery innerJoinWithCustomer() Adds a INNER JOIN clause and with to the query using the Customer relation
 *
 * @method     \Model\Setting\MasterTable\MtCompanyCategoryQuery|\Model\Crm\CustomerQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCustomerCompany findOne(ConnectionInterface $con = null) Return the first ChildCustomerCompany matching the query
 * @method     ChildCustomerCompany findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCustomerCompany matching the query, or a new ChildCustomerCompany object populated from the query conditions when no match is found
 *
 * @method     ChildCustomerCompany findOneById(int $id) Return the first ChildCustomerCompany filtered by the id column
 * @method     ChildCustomerCompany findOneByCustomerId(int $customer_id) Return the first ChildCustomerCompany filtered by the customer_id column
 * @method     ChildCustomerCompany findOneByLegalFormId(int $legal_form_id) Return the first ChildCustomerCompany filtered by the legal_form_id column
 * @method     ChildCustomerCompany findOneByCompanyTypeId(int $company_type_id) Return the first ChildCustomerCompany filtered by the company_type_id column
 * @method     ChildCustomerCompany findOneByCompanyName(string $company_name) Return the first ChildCustomerCompany filtered by the company_name column
 * @method     ChildCustomerCompany findOneByChamberOfCommerce(string $chamber_of_commerce) Return the first ChildCustomerCompany filtered by the chamber_of_commerce column
 * @method     ChildCustomerCompany findOneByVatNumber(string $vat_number) Return the first ChildCustomerCompany filtered by the vat_number column *

 * @method     ChildCustomerCompany requirePk($key, ConnectionInterface $con = null) Return the ChildCustomerCompany by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerCompany requireOne(ConnectionInterface $con = null) Return the first ChildCustomerCompany matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomerCompany requireOneById(int $id) Return the first ChildCustomerCompany filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerCompany requireOneByCustomerId(int $customer_id) Return the first ChildCustomerCompany filtered by the customer_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerCompany requireOneByLegalFormId(int $legal_form_id) Return the first ChildCustomerCompany filtered by the legal_form_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerCompany requireOneByCompanyTypeId(int $company_type_id) Return the first ChildCustomerCompany filtered by the company_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerCompany requireOneByCompanyName(string $company_name) Return the first ChildCustomerCompany filtered by the company_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerCompany requireOneByChamberOfCommerce(string $chamber_of_commerce) Return the first ChildCustomerCompany filtered by the chamber_of_commerce column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomerCompany requireOneByVatNumber(string $vat_number) Return the first ChildCustomerCompany filtered by the vat_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomerCompany[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCustomerCompany objects based on current ModelCriteria
 * @method     ChildCustomerCompany[]|ObjectCollection findById(int $id) Return ChildCustomerCompany objects filtered by the id column
 * @method     ChildCustomerCompany[]|ObjectCollection findByCustomerId(int $customer_id) Return ChildCustomerCompany objects filtered by the customer_id column
 * @method     ChildCustomerCompany[]|ObjectCollection findByLegalFormId(int $legal_form_id) Return ChildCustomerCompany objects filtered by the legal_form_id column
 * @method     ChildCustomerCompany[]|ObjectCollection findByCompanyTypeId(int $company_type_id) Return ChildCustomerCompany objects filtered by the company_type_id column
 * @method     ChildCustomerCompany[]|ObjectCollection findByCompanyName(string $company_name) Return ChildCustomerCompany objects filtered by the company_name column
 * @method     ChildCustomerCompany[]|ObjectCollection findByChamberOfCommerce(string $chamber_of_commerce) Return ChildCustomerCompany objects filtered by the chamber_of_commerce column
 * @method     ChildCustomerCompany[]|ObjectCollection findByVatNumber(string $vat_number) Return ChildCustomerCompany objects filtered by the vat_number column
 * @method     ChildCustomerCompany[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CustomerCompanyQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Crm\Base\CustomerCompanyQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Crm\\CustomerCompany', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCustomerCompanyQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCustomerCompanyQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCustomerCompanyQuery) {
            return $criteria;
        }
        $query = new ChildCustomerCompanyQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCustomerCompany|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CustomerCompanyTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CustomerCompanyTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerCompany A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, customer_id, legal_form_id, company_type_id, company_name, chamber_of_commerce, vat_number FROM customer_company WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCustomerCompany $obj */
            $obj = new ChildCustomerCompany();
            $obj->hydrate($row);
            CustomerCompanyTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCustomerCompany|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CustomerCompanyTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CustomerCompanyTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CustomerCompanyTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CustomerCompanyTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerCompanyTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the customer_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerId(1234); // WHERE customer_id = 1234
     * $query->filterByCustomerId(array(12, 34)); // WHERE customer_id IN (12, 34)
     * $query->filterByCustomerId(array('min' => 12)); // WHERE customer_id > 12
     * </code>
     *
     * @see       filterByCustomer()
     *
     * @param     mixed $customerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function filterByCustomerId($customerId = null, $comparison = null)
    {
        if (is_array($customerId)) {
            $useMinMax = false;
            if (isset($customerId['min'])) {
                $this->addUsingAlias(CustomerCompanyTableMap::COL_CUSTOMER_ID, $customerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerId['max'])) {
                $this->addUsingAlias(CustomerCompanyTableMap::COL_CUSTOMER_ID, $customerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerCompanyTableMap::COL_CUSTOMER_ID, $customerId, $comparison);
    }

    /**
     * Filter the query on the legal_form_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLegalFormId(1234); // WHERE legal_form_id = 1234
     * $query->filterByLegalFormId(array(12, 34)); // WHERE legal_form_id IN (12, 34)
     * $query->filterByLegalFormId(array('min' => 12)); // WHERE legal_form_id > 12
     * </code>
     *
     * @param     mixed $legalFormId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function filterByLegalFormId($legalFormId = null, $comparison = null)
    {
        if (is_array($legalFormId)) {
            $useMinMax = false;
            if (isset($legalFormId['min'])) {
                $this->addUsingAlias(CustomerCompanyTableMap::COL_LEGAL_FORM_ID, $legalFormId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($legalFormId['max'])) {
                $this->addUsingAlias(CustomerCompanyTableMap::COL_LEGAL_FORM_ID, $legalFormId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerCompanyTableMap::COL_LEGAL_FORM_ID, $legalFormId, $comparison);
    }

    /**
     * Filter the query on the company_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCompanyTypeId(1234); // WHERE company_type_id = 1234
     * $query->filterByCompanyTypeId(array(12, 34)); // WHERE company_type_id IN (12, 34)
     * $query->filterByCompanyTypeId(array('min' => 12)); // WHERE company_type_id > 12
     * </code>
     *
     * @see       filterByCompanyCategory()
     *
     * @param     mixed $companyTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function filterByCompanyTypeId($companyTypeId = null, $comparison = null)
    {
        if (is_array($companyTypeId)) {
            $useMinMax = false;
            if (isset($companyTypeId['min'])) {
                $this->addUsingAlias(CustomerCompanyTableMap::COL_COMPANY_TYPE_ID, $companyTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($companyTypeId['max'])) {
                $this->addUsingAlias(CustomerCompanyTableMap::COL_COMPANY_TYPE_ID, $companyTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerCompanyTableMap::COL_COMPANY_TYPE_ID, $companyTypeId, $comparison);
    }

    /**
     * Filter the query on the company_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCompanyName('fooValue');   // WHERE company_name = 'fooValue'
     * $query->filterByCompanyName('%fooValue%', Criteria::LIKE); // WHERE company_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $companyName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function filterByCompanyName($companyName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($companyName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerCompanyTableMap::COL_COMPANY_NAME, $companyName, $comparison);
    }

    /**
     * Filter the query on the chamber_of_commerce column
     *
     * Example usage:
     * <code>
     * $query->filterByChamberOfCommerce('fooValue');   // WHERE chamber_of_commerce = 'fooValue'
     * $query->filterByChamberOfCommerce('%fooValue%', Criteria::LIKE); // WHERE chamber_of_commerce LIKE '%fooValue%'
     * </code>
     *
     * @param     string $chamberOfCommerce The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function filterByChamberOfCommerce($chamberOfCommerce = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($chamberOfCommerce)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerCompanyTableMap::COL_CHAMBER_OF_COMMERCE, $chamberOfCommerce, $comparison);
    }

    /**
     * Filter the query on the vat_number column
     *
     * Example usage:
     * <code>
     * $query->filterByVatNumber('fooValue');   // WHERE vat_number = 'fooValue'
     * $query->filterByVatNumber('%fooValue%', Criteria::LIKE); // WHERE vat_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $vatNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function filterByVatNumber($vatNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($vatNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomerCompanyTableMap::COL_VAT_NUMBER, $vatNumber, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\MtCompanyCategory object
     *
     * @param \Model\Setting\MasterTable\MtCompanyCategory|ObjectCollection $mtCompanyCategory The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function filterByCompanyCategory($mtCompanyCategory, $comparison = null)
    {
        if ($mtCompanyCategory instanceof \Model\Setting\MasterTable\MtCompanyCategory) {
            return $this
                ->addUsingAlias(CustomerCompanyTableMap::COL_COMPANY_TYPE_ID, $mtCompanyCategory->getId(), $comparison);
        } elseif ($mtCompanyCategory instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomerCompanyTableMap::COL_COMPANY_TYPE_ID, $mtCompanyCategory->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCompanyCategory() only accepts arguments of type \Model\Setting\MasterTable\MtCompanyCategory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CompanyCategory relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function joinCompanyCategory($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CompanyCategory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CompanyCategory');
        }

        return $this;
    }

    /**
     * Use the CompanyCategory relation MtCompanyCategory object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\MtCompanyCategoryQuery A secondary query class using the current class as primary query
     */
    public function useCompanyCategoryQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCompanyCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CompanyCategory', '\Model\Setting\MasterTable\MtCompanyCategoryQuery');
    }

    /**
     * Filter the query by a related \Model\Crm\Customer object
     *
     * @param \Model\Crm\Customer|ObjectCollection $customer The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function filterByCustomer($customer, $comparison = null)
    {
        if ($customer instanceof \Model\Crm\Customer) {
            return $this
                ->addUsingAlias(CustomerCompanyTableMap::COL_CUSTOMER_ID, $customer->getId(), $comparison);
        } elseif ($customer instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomerCompanyTableMap::COL_CUSTOMER_ID, $customer->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCustomer() only accepts arguments of type \Model\Crm\Customer or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Customer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function joinCustomer($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Customer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Customer');
        }

        return $this;
    }

    /**
     * Use the Customer relation Customer object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerQuery A secondary query class using the current class as primary query
     */
    public function useCustomerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCustomer($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Customer', '\Model\Crm\CustomerQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCustomerCompany $customerCompany Object to remove from the list of results
     *
     * @return $this|ChildCustomerCompanyQuery The current query, for fluid interface
     */
    public function prune($customerCompany = null)
    {
        if ($customerCompany) {
            $this->addUsingAlias(CustomerCompanyTableMap::COL_ID, $customerCompany->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the customer_company table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerCompanyTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CustomerCompanyTableMap::clearInstancePool();
            CustomerCompanyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerCompanyTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CustomerCompanyTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CustomerCompanyTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CustomerCompanyTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CustomerCompanyQuery
