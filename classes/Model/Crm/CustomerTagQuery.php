<?php

namespace Model\Crm;

use Model\Crm\Base\CustomerTagQuery as BaseCustomerTagQuery;
use Model\Product\Tag\Tag;
use Model\Product\Tag\TagQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'customer_tag' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerTagQuery extends BaseCustomerTagQuery
{

    public static function registerTagGroup($sTags, $iCustomerId)
    {
        CustomerTagQuery::create()->filterByCustomerId($iCustomerId)->delete();
        if(!$sTags)
        {
            return;
        }
        $aTags = explode(',', $sTags);

        foreach ($aTags as $sTag)
        {
            $oTag = TagQuery::create()->findOneByTag($sTag);

            if(!$oTag instanceof Tag)
            {
                $oTag = new Tag();
                $oTag->setTag(trim($sTag));
                $oTag->save();
            }
            $oCustomerTag = CustomerTagQuery::create()->filterByTagId($oTag->getId())->filterByCustomerId($iCustomerId)->findOne();

            if(!$oCustomerTag instanceof CustomerTag)
            {
                $oCustomerTag = new CustomerTag();
                $oCustomerTag->setTagId($oTag->getId());
                $oCustomerTag->setCustomerId($iCustomerId);
                $oCustomerTag->save();
            }
        }
    }
    public static function getAllTagsByCustomerIdCsv($iCustomerId):string
    {
        $aTagsObjects = self::getAllTagsByCustomerId($iCustomerId);

        if(empty($aTagsObjects))
        {
            return '';
        }
        $aOut = [];
        foreach($aTagsObjects as $oTag)
        {
            if($oTag instanceof Tag)
            {
                $aOut[] = $oTag->getTag();
            }
        }
        return join(',', $aOut);
    }
    public static function getAllTagsByCustomerId($iCustomerId):array
    {
        $aCustomerTags = CustomerTagQuery::create()->filterByCustomerId($iCustomerId)->find();
        if($aCustomerTags->isEmpty())
        {
            return [];
        }
        $aTags = [];
        foreach($aCustomerTags as $oCustomerTag)
        {
            $aTags[] = TagQuery::create()->findOneById($oCustomerTag->getTagId());
        }
        return $aTags;
    }
}
