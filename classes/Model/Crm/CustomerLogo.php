<?php

namespace Model\Crm;

use Model\Crm\Base\CustomerLogo as BaseCustomerLogo;
use Core\Config;

/**
 * Skeleton subclass for representing a row from the 'customer_logo' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerLogo extends BaseCustomerLogo
{
    function getMyFileName()
    {

        $sRelativeDir = '/img/customer/'.$this->getCustomerId().'/company_logo.'.$this->getFileExt();
        return Config::getDataDir().$sRelativeDir;
    }
    function deleteWithFile()
    {
        $sMyFileName = $this->getMyFileName();
        if(file_exists($sMyFileName))
        {
            unlink($sMyFileName);
        }
        $this->delete();
    }
}
