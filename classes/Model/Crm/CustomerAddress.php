<?php

namespace Model\Crm;

use Model\Crm\Base\CustomerAddress as BaseCustomerAddress;
use Model\Setting\MasterTable\CountryQuery;

/**
 * Skeleton subclass for representing a row from the 'customer_address' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerAddress extends BaseCustomerAddress
{
    function getPostalShortened($iMaxLen = null)
    {
        $sStr = self::getPostal();
        if($sStr == null)
        {
            return $sStr;
        }
        return substr($sStr, 0, $iMaxLen);
    }
    function getAddressL1Shortened($iMaxLen = null)
    {
        $sStr = self::getAddressL1();
        if($sStr == null)
        {
            return $sStr;
        }
        return substr($sStr, 0, $iMaxLen);
    }

    static function createBlankWithDefaults()
    {
        $oCustomerAddress = new CustomerAddress();
        $oCustomerAddress->setCountryId(CountryQuery::create()->findOneByName('The Netherlands')->getId());
        return $oCustomerAddress;
    }

}
