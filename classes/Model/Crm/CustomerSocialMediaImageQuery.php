<?php

namespace Model\Crm;

use Model\Crm\Base\CustomerSocialMediaImageQuery as BaseCustomerSocialMediaImageQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'customer_social_media_image' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerSocialMediaImageQuery extends BaseCustomerSocialMediaImageQuery
{
    static function findByCustomerAsssoc($iCustomerId)
    {
        $aItems = self::create()->findByCustomerId($iCustomerId);

        if($aItems->isEmpty())
        {
            return null;
        }
        $aOut = [];
        foreach($aItems as $aItem)
        {
            $aOut[$aItem->getSocialNetwork().'_'.$aItem->getImageLocation()] = $aItem;
        }
        return $aOut;
    }

}
