<?php

namespace Model\Crm;

use Core\PropelSqlTrait;
use Model\Crm\Base\CustomerQuery as BaseCustomerQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'customer' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerQuery extends BaseCustomerQuery
{
	use PropelSqlTrait;


    function orderByCompanyName($order = Criteria::ASC)
    {
        return $this->useCustomerCompanyQuery()->orderByCompanyName($order)->endUse();
    }

}
