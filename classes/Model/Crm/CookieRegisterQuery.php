<?php

namespace Model\Crm;

use Model\Crm\Base\CookieRegisterQuery as BaseCookieRegisterQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'cookie_register' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CookieRegisterQuery extends BaseCookieRegisterQuery
{

}
