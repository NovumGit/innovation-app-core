<?php

namespace Model\Crm;

use Model\Crm\Base\CustomerProperty as BaseCustomerProperty;

/**
 * Skeleton subclass for representing a row from the 'customer_property' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerProperty extends BaseCustomerProperty
{

}
