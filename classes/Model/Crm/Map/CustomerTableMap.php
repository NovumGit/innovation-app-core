<?php

namespace Model\Crm\Map;

use Model\Category\Map\CustomerWishlistTableMap;
use Model\Crm\Customer;
use Model\Crm\CustomerQuery;
use Model\Map\ContactMessageTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'customer' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CustomerTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Crm.Map.CustomerTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'customer';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Crm\\Customer';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Crm.Customer';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 34;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 34;

    /**
     * the column name for the id field
     */
    const COL_ID = 'customer.id';

    /**
     * the column name for the import_source field
     */
    const COL_IMPORT_SOURCE = 'customer.import_source';

    /**
     * the column name for the accounting_id field
     */
    const COL_ACCOUNTING_ID = 'customer.accounting_id';

    /**
     * the column name for the old_id field
     */
    const COL_OLD_ID = 'customer.old_id';

    /**
     * the column name for the ip field
     */
    const COL_IP = 'customer.ip';

    /**
     * the column name for the created_on field
     */
    const COL_CREATED_ON = 'customer.created_on';

    /**
     * the column name for the is_deleted field
     */
    const COL_IS_DELETED = 'customer.is_deleted';

    /**
     * the column name for the is_newsletter_subscriber field
     */
    const COL_IS_NEWSLETTER_SUBSCRIBER = 'customer.is_newsletter_subscriber';

    /**
     * the column name for the language_id field
     */
    const COL_LANGUAGE_ID = 'customer.language_id';

    /**
     * the column name for the is_intracommunautair field
     */
    const COL_IS_INTRACOMMUNAUTAIR = 'customer.is_intracommunautair';

    /**
     * the column name for the payterm_id field
     */
    const COL_PAYTERM_ID = 'customer.payterm_id';

    /**
     * the column name for the branche_id field
     */
    const COL_BRANCHE_ID = 'customer.branche_id';

    /**
     * the column name for the sector_id field
     */
    const COL_SECTOR_ID = 'customer.sector_id';

    /**
     * the column name for the customer_type_id field
     */
    const COL_CUSTOMER_TYPE_ID = 'customer.customer_type_id';

    /**
     * the column name for the debitor field
     */
    const COL_DEBITOR = 'customer.debitor';

    /**
     * the column name for the gender field
     */
    const COL_GENDER = 'customer.gender';

    /**
     * the column name for the first_name field
     */
    const COL_FIRST_NAME = 'customer.first_name';

    /**
     * the column name for the insertion field
     */
    const COL_INSERTION = 'customer.insertion';

    /**
     * the column name for the last_name field
     */
    const COL_LAST_NAME = 'customer.last_name';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'customer.email';

    /**
     * the column name for the password field
     */
    const COL_PASSWORD = 'customer.password';

    /**
     * the column name for the salt field
     */
    const COL_SALT = 'customer.salt';

    /**
     * the column name for the once_password_reset field
     */
    const COL_ONCE_PASSWORD_RESET = 'customer.once_password_reset';

    /**
     * the column name for the is_wholesale field
     */
    const COL_IS_WHOLESALE = 'customer.is_wholesale';

    /**
     * the column name for the is_wholesale_approved field
     */
    const COL_IS_WHOLESALE_APPROVED = 'customer.is_wholesale_approved';

    /**
     * the column name for the can_buy_on_credit field
     */
    const COL_CAN_BUY_ON_CREDIT = 'customer.can_buy_on_credit';

    /**
     * the column name for the iban field
     */
    const COL_IBAN = 'customer.iban';

    /**
     * the column name for the phone field
     */
    const COL_PHONE = 'customer.phone';

    /**
     * the column name for the mobile field
     */
    const COL_MOBILE = 'customer.mobile';

    /**
     * the column name for the fax field
     */
    const COL_FAX = 'customer.fax';

    /**
     * the column name for the website field
     */
    const COL_WEBSITE = 'customer.website';

    /**
     * the column name for the tag field
     */
    const COL_TAG = 'customer.tag';

    /**
     * the column name for the is_verified field
     */
    const COL_IS_VERIFIED = 'customer.is_verified';

    /**
     * the column name for the verification_key field
     */
    const COL_VERIFICATION_KEY = 'customer.verification_key';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ImportSource', 'AccountingId', 'OldId', 'Ip', 'CreatedOn', 'ItemDeleted', 'IsNewsletterSubscriber', 'LanguageId', 'IsIntracommunautair', 'PaytermId', 'BrancheId', 'SectorId', 'CustomerTypeId', 'Debitor', 'Gender', 'FirstName', 'Insertion', 'LastName', 'Email', 'Password', 'Salt', 'OncePasswordReset', 'IsWholesale', 'IsWholesaleApproved', 'CanBuyOnCredit', 'Iban', 'Phone', 'Mobile', 'Fax', 'Website', 'Tag', 'IsVerified', 'VerificationKey', ),
        self::TYPE_CAMELNAME     => array('id', 'importSource', 'accountingId', 'oldId', 'ip', 'createdOn', 'itemDeleted', 'isNewsletterSubscriber', 'languageId', 'isIntracommunautair', 'paytermId', 'brancheId', 'sectorId', 'customerTypeId', 'debitor', 'gender', 'firstName', 'insertion', 'lastName', 'email', 'password', 'salt', 'oncePasswordReset', 'isWholesale', 'isWholesaleApproved', 'canBuyOnCredit', 'iban', 'phone', 'mobile', 'fax', 'website', 'tag', 'isVerified', 'verificationKey', ),
        self::TYPE_COLNAME       => array(CustomerTableMap::COL_ID, CustomerTableMap::COL_IMPORT_SOURCE, CustomerTableMap::COL_ACCOUNTING_ID, CustomerTableMap::COL_OLD_ID, CustomerTableMap::COL_IP, CustomerTableMap::COL_CREATED_ON, CustomerTableMap::COL_IS_DELETED, CustomerTableMap::COL_IS_NEWSLETTER_SUBSCRIBER, CustomerTableMap::COL_LANGUAGE_ID, CustomerTableMap::COL_IS_INTRACOMMUNAUTAIR, CustomerTableMap::COL_PAYTERM_ID, CustomerTableMap::COL_BRANCHE_ID, CustomerTableMap::COL_SECTOR_ID, CustomerTableMap::COL_CUSTOMER_TYPE_ID, CustomerTableMap::COL_DEBITOR, CustomerTableMap::COL_GENDER, CustomerTableMap::COL_FIRST_NAME, CustomerTableMap::COL_INSERTION, CustomerTableMap::COL_LAST_NAME, CustomerTableMap::COL_EMAIL, CustomerTableMap::COL_PASSWORD, CustomerTableMap::COL_SALT, CustomerTableMap::COL_ONCE_PASSWORD_RESET, CustomerTableMap::COL_IS_WHOLESALE, CustomerTableMap::COL_IS_WHOLESALE_APPROVED, CustomerTableMap::COL_CAN_BUY_ON_CREDIT, CustomerTableMap::COL_IBAN, CustomerTableMap::COL_PHONE, CustomerTableMap::COL_MOBILE, CustomerTableMap::COL_FAX, CustomerTableMap::COL_WEBSITE, CustomerTableMap::COL_TAG, CustomerTableMap::COL_IS_VERIFIED, CustomerTableMap::COL_VERIFICATION_KEY, ),
        self::TYPE_FIELDNAME     => array('id', 'import_source', 'accounting_id', 'old_id', 'ip', 'created_on', 'is_deleted', 'is_newsletter_subscriber', 'language_id', 'is_intracommunautair', 'payterm_id', 'branche_id', 'sector_id', 'customer_type_id', 'debitor', 'gender', 'first_name', 'insertion', 'last_name', 'email', 'password', 'salt', 'once_password_reset', 'is_wholesale', 'is_wholesale_approved', 'can_buy_on_credit', 'iban', 'phone', 'mobile', 'fax', 'website', 'tag', 'is_verified', 'verification_key', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ImportSource' => 1, 'AccountingId' => 2, 'OldId' => 3, 'Ip' => 4, 'CreatedOn' => 5, 'ItemDeleted' => 6, 'IsNewsletterSubscriber' => 7, 'LanguageId' => 8, 'IsIntracommunautair' => 9, 'PaytermId' => 10, 'BrancheId' => 11, 'SectorId' => 12, 'CustomerTypeId' => 13, 'Debitor' => 14, 'Gender' => 15, 'FirstName' => 16, 'Insertion' => 17, 'LastName' => 18, 'Email' => 19, 'Password' => 20, 'Salt' => 21, 'OncePasswordReset' => 22, 'IsWholesale' => 23, 'IsWholesaleApproved' => 24, 'CanBuyOnCredit' => 25, 'Iban' => 26, 'Phone' => 27, 'Mobile' => 28, 'Fax' => 29, 'Website' => 30, 'Tag' => 31, 'IsVerified' => 32, 'VerificationKey' => 33, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'importSource' => 1, 'accountingId' => 2, 'oldId' => 3, 'ip' => 4, 'createdOn' => 5, 'itemDeleted' => 6, 'isNewsletterSubscriber' => 7, 'languageId' => 8, 'isIntracommunautair' => 9, 'paytermId' => 10, 'brancheId' => 11, 'sectorId' => 12, 'customerTypeId' => 13, 'debitor' => 14, 'gender' => 15, 'firstName' => 16, 'insertion' => 17, 'lastName' => 18, 'email' => 19, 'password' => 20, 'salt' => 21, 'oncePasswordReset' => 22, 'isWholesale' => 23, 'isWholesaleApproved' => 24, 'canBuyOnCredit' => 25, 'iban' => 26, 'phone' => 27, 'mobile' => 28, 'fax' => 29, 'website' => 30, 'tag' => 31, 'isVerified' => 32, 'verificationKey' => 33, ),
        self::TYPE_COLNAME       => array(CustomerTableMap::COL_ID => 0, CustomerTableMap::COL_IMPORT_SOURCE => 1, CustomerTableMap::COL_ACCOUNTING_ID => 2, CustomerTableMap::COL_OLD_ID => 3, CustomerTableMap::COL_IP => 4, CustomerTableMap::COL_CREATED_ON => 5, CustomerTableMap::COL_IS_DELETED => 6, CustomerTableMap::COL_IS_NEWSLETTER_SUBSCRIBER => 7, CustomerTableMap::COL_LANGUAGE_ID => 8, CustomerTableMap::COL_IS_INTRACOMMUNAUTAIR => 9, CustomerTableMap::COL_PAYTERM_ID => 10, CustomerTableMap::COL_BRANCHE_ID => 11, CustomerTableMap::COL_SECTOR_ID => 12, CustomerTableMap::COL_CUSTOMER_TYPE_ID => 13, CustomerTableMap::COL_DEBITOR => 14, CustomerTableMap::COL_GENDER => 15, CustomerTableMap::COL_FIRST_NAME => 16, CustomerTableMap::COL_INSERTION => 17, CustomerTableMap::COL_LAST_NAME => 18, CustomerTableMap::COL_EMAIL => 19, CustomerTableMap::COL_PASSWORD => 20, CustomerTableMap::COL_SALT => 21, CustomerTableMap::COL_ONCE_PASSWORD_RESET => 22, CustomerTableMap::COL_IS_WHOLESALE => 23, CustomerTableMap::COL_IS_WHOLESALE_APPROVED => 24, CustomerTableMap::COL_CAN_BUY_ON_CREDIT => 25, CustomerTableMap::COL_IBAN => 26, CustomerTableMap::COL_PHONE => 27, CustomerTableMap::COL_MOBILE => 28, CustomerTableMap::COL_FAX => 29, CustomerTableMap::COL_WEBSITE => 30, CustomerTableMap::COL_TAG => 31, CustomerTableMap::COL_IS_VERIFIED => 32, CustomerTableMap::COL_VERIFICATION_KEY => 33, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'import_source' => 1, 'accounting_id' => 2, 'old_id' => 3, 'ip' => 4, 'created_on' => 5, 'is_deleted' => 6, 'is_newsletter_subscriber' => 7, 'language_id' => 8, 'is_intracommunautair' => 9, 'payterm_id' => 10, 'branche_id' => 11, 'sector_id' => 12, 'customer_type_id' => 13, 'debitor' => 14, 'gender' => 15, 'first_name' => 16, 'insertion' => 17, 'last_name' => 18, 'email' => 19, 'password' => 20, 'salt' => 21, 'once_password_reset' => 22, 'is_wholesale' => 23, 'is_wholesale_approved' => 24, 'can_buy_on_credit' => 25, 'iban' => 26, 'phone' => 27, 'mobile' => 28, 'fax' => 29, 'website' => 30, 'tag' => 31, 'is_verified' => 32, 'verification_key' => 33, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('customer');
        $this->setPhpName('Customer');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Crm\\Customer');
        $this->setPackage('Model.Crm');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('import_source', 'ImportSource', 'VARCHAR', false, 120, null);
        $this->addColumn('accounting_id', 'AccountingId', 'VARCHAR', false, 255, null);
        $this->addColumn('old_id', 'OldId', 'VARCHAR', false, 255, null);
        $this->addColumn('ip', 'Ip', 'VARCHAR', false, 255, null);
        $this->addColumn('created_on', 'CreatedOn', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('is_deleted', 'ItemDeleted', 'BOOLEAN', false, 1, false);
        $this->addColumn('is_newsletter_subscriber', 'IsNewsletterSubscriber', 'BOOLEAN', false, 1, false);
        $this->addForeignKey('language_id', 'LanguageId', 'INTEGER', 'mt_language', 'id', false, null, null);
        $this->addColumn('is_intracommunautair', 'IsIntracommunautair', 'INTEGER', false, null, null);
        $this->addForeignKey('payterm_id', 'PaytermId', 'INTEGER', 'mt_payterm', 'id', false, null, null);
        $this->addForeignKey('branche_id', 'BrancheId', 'INTEGER', 'mt_branche', 'id', false, null, null);
        $this->addForeignKey('sector_id', 'SectorId', 'INTEGER', 'mt_customer_sector', 'id', false, null, null);
        $this->addForeignKey('customer_type_id', 'CustomerTypeId', 'INTEGER', 'mt_customer_type', 'id', false, null, null);
        $this->addColumn('debitor', 'Debitor', 'VARCHAR', false, 120, null);
        $this->addColumn('gender', 'Gender', 'VARCHAR', false, 255, null);
        $this->addColumn('first_name', 'FirstName', 'VARCHAR', false, 75, null);
        $this->addColumn('insertion', 'Insertion', 'VARCHAR', false, 75, null);
        $this->addColumn('last_name', 'LastName', 'VARCHAR', false, 75, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 150, null);
        $this->addColumn('password', 'Password', 'VARCHAR', false, 256, null);
        $this->addColumn('salt', 'Salt', 'VARCHAR', false, 25, null);
        $this->addColumn('once_password_reset', 'OncePasswordReset', 'BOOLEAN', false, 1, false);
        $this->addColumn('is_wholesale', 'IsWholesale', 'BOOLEAN', false, 1, false);
        $this->addColumn('is_wholesale_approved', 'IsWholesaleApproved', 'BOOLEAN', false, 1, false);
        $this->addColumn('can_buy_on_credit', 'CanBuyOnCredit', 'BOOLEAN', false, 1, false);
        $this->addColumn('iban', 'Iban', 'VARCHAR', false, 25, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', false, 255, null);
        $this->addColumn('mobile', 'Mobile', 'VARCHAR', false, 255, null);
        $this->addColumn('fax', 'Fax', 'VARCHAR', false, 255, null);
        $this->addColumn('website', 'Website', 'VARCHAR', false, 255, null);
        $this->addColumn('tag', 'Tag', 'VARCHAR', false, 255, null);
        $this->addColumn('is_verified', 'IsVerified', 'BOOLEAN', false, 1, false);
        $this->addColumn('verification_key', 'VerificationKey', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Language', '\\Model\\Setting\\MasterTable\\Language', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('Branche', '\\Model\\Setting\\MasterTable\\Branche', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':branche_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('CustomerSector', '\\Model\\Crm\\CustomerSector', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':sector_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('PayTerm', '\\Model\\Setting\\MasterTable\\PayTerm', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':payterm_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('CustomerType', '\\Model\\Crm\\CustomerType', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':customer_type_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('ContactMessage', '\\Model\\ContactMessage', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'ContactMessages', false);
        $this->addRelation('NewsletterSubscriber', '\\Model\\Crm\\NewsletterSubscriber', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'SET NULL', null, 'NewsletterSubscribers', false);
        $this->addRelation('CustomerCompany', '\\Model\\Crm\\CustomerCompany', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'CustomerCompanies', false);
        $this->addRelation('Customer', '\\Model\\Crm\\CustomerAddress', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'Customers', false);
        $this->addRelation('CustomerNote', '\\Model\\Crm\\CustomerNote', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'CustomerNotes', false);
        $this->addRelation('CustomerProperty', '\\Model\\Crm\\CustomerProperty', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'CustomerProperties', false);
        $this->addRelation('CustomerWishlist', '\\Model\\Category\\CustomerWishlist', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'CustomerWishlists', false);
        $this->addRelation('CustomerPhoto', '\\Model\\Crm\\CustomerPhoto', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'CustomerPhotos', false);
        $this->addRelation('CustomerLogo', '\\Model\\Crm\\CustomerLogo', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'CustomerLogos', false);
        $this->addRelation('SaleInvoice', '\\Model\\Finance\\SaleInvoice', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'SaleInvoices', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '3600', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to customer     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ContactMessageTableMap::clearInstancePool();
        NewsletterSubscriberTableMap::clearInstancePool();
        CustomerCompanyTableMap::clearInstancePool();
        CustomerAddressTableMap::clearInstancePool();
        CustomerNoteTableMap::clearInstancePool();
        CustomerPropertyTableMap::clearInstancePool();
        CustomerWishlistTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CustomerTableMap::CLASS_DEFAULT : CustomerTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Customer object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CustomerTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CustomerTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CustomerTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CustomerTableMap::OM_CLASS;
            /** @var Customer $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CustomerTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CustomerTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CustomerTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Customer $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CustomerTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CustomerTableMap::COL_ID);
            $criteria->addSelectColumn(CustomerTableMap::COL_IMPORT_SOURCE);
            $criteria->addSelectColumn(CustomerTableMap::COL_ACCOUNTING_ID);
            $criteria->addSelectColumn(CustomerTableMap::COL_OLD_ID);
            $criteria->addSelectColumn(CustomerTableMap::COL_IP);
            $criteria->addSelectColumn(CustomerTableMap::COL_CREATED_ON);
            $criteria->addSelectColumn(CustomerTableMap::COL_IS_DELETED);
            $criteria->addSelectColumn(CustomerTableMap::COL_IS_NEWSLETTER_SUBSCRIBER);
            $criteria->addSelectColumn(CustomerTableMap::COL_LANGUAGE_ID);
            $criteria->addSelectColumn(CustomerTableMap::COL_IS_INTRACOMMUNAUTAIR);
            $criteria->addSelectColumn(CustomerTableMap::COL_PAYTERM_ID);
            $criteria->addSelectColumn(CustomerTableMap::COL_BRANCHE_ID);
            $criteria->addSelectColumn(CustomerTableMap::COL_SECTOR_ID);
            $criteria->addSelectColumn(CustomerTableMap::COL_CUSTOMER_TYPE_ID);
            $criteria->addSelectColumn(CustomerTableMap::COL_DEBITOR);
            $criteria->addSelectColumn(CustomerTableMap::COL_GENDER);
            $criteria->addSelectColumn(CustomerTableMap::COL_FIRST_NAME);
            $criteria->addSelectColumn(CustomerTableMap::COL_INSERTION);
            $criteria->addSelectColumn(CustomerTableMap::COL_LAST_NAME);
            $criteria->addSelectColumn(CustomerTableMap::COL_EMAIL);
            $criteria->addSelectColumn(CustomerTableMap::COL_PASSWORD);
            $criteria->addSelectColumn(CustomerTableMap::COL_SALT);
            $criteria->addSelectColumn(CustomerTableMap::COL_ONCE_PASSWORD_RESET);
            $criteria->addSelectColumn(CustomerTableMap::COL_IS_WHOLESALE);
            $criteria->addSelectColumn(CustomerTableMap::COL_IS_WHOLESALE_APPROVED);
            $criteria->addSelectColumn(CustomerTableMap::COL_CAN_BUY_ON_CREDIT);
            $criteria->addSelectColumn(CustomerTableMap::COL_IBAN);
            $criteria->addSelectColumn(CustomerTableMap::COL_PHONE);
            $criteria->addSelectColumn(CustomerTableMap::COL_MOBILE);
            $criteria->addSelectColumn(CustomerTableMap::COL_FAX);
            $criteria->addSelectColumn(CustomerTableMap::COL_WEBSITE);
            $criteria->addSelectColumn(CustomerTableMap::COL_TAG);
            $criteria->addSelectColumn(CustomerTableMap::COL_IS_VERIFIED);
            $criteria->addSelectColumn(CustomerTableMap::COL_VERIFICATION_KEY);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.import_source');
            $criteria->addSelectColumn($alias . '.accounting_id');
            $criteria->addSelectColumn($alias . '.old_id');
            $criteria->addSelectColumn($alias . '.ip');
            $criteria->addSelectColumn($alias . '.created_on');
            $criteria->addSelectColumn($alias . '.is_deleted');
            $criteria->addSelectColumn($alias . '.is_newsletter_subscriber');
            $criteria->addSelectColumn($alias . '.language_id');
            $criteria->addSelectColumn($alias . '.is_intracommunautair');
            $criteria->addSelectColumn($alias . '.payterm_id');
            $criteria->addSelectColumn($alias . '.branche_id');
            $criteria->addSelectColumn($alias . '.sector_id');
            $criteria->addSelectColumn($alias . '.customer_type_id');
            $criteria->addSelectColumn($alias . '.debitor');
            $criteria->addSelectColumn($alias . '.gender');
            $criteria->addSelectColumn($alias . '.first_name');
            $criteria->addSelectColumn($alias . '.insertion');
            $criteria->addSelectColumn($alias . '.last_name');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.password');
            $criteria->addSelectColumn($alias . '.salt');
            $criteria->addSelectColumn($alias . '.once_password_reset');
            $criteria->addSelectColumn($alias . '.is_wholesale');
            $criteria->addSelectColumn($alias . '.is_wholesale_approved');
            $criteria->addSelectColumn($alias . '.can_buy_on_credit');
            $criteria->addSelectColumn($alias . '.iban');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.mobile');
            $criteria->addSelectColumn($alias . '.fax');
            $criteria->addSelectColumn($alias . '.website');
            $criteria->addSelectColumn($alias . '.tag');
            $criteria->addSelectColumn($alias . '.is_verified');
            $criteria->addSelectColumn($alias . '.verification_key');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CustomerTableMap::DATABASE_NAME)->getTable(CustomerTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CustomerTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CustomerTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CustomerTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Customer or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Customer object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Crm\Customer) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CustomerTableMap::DATABASE_NAME);
            $criteria->add(CustomerTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CustomerQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CustomerTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CustomerTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the customer table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CustomerQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Customer or Criteria object.
     *
     * @param mixed               $criteria Criteria or Customer object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Customer object
        }

        if ($criteria->containsKey(CustomerTableMap::COL_ID) && $criteria->keyContainsValue(CustomerTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CustomerTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CustomerQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CustomerTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CustomerTableMap::buildTableMap();
