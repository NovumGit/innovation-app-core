<?php

namespace Model\Crm\Map;

use Model\Crm\UserLinkedIn;
use Model\Crm\UserLinkedInQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'user_linkedin' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class UserLinkedInTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Crm.Map.UserLinkedInTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'user_linkedin';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Crm\\UserLinkedIn';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Crm.UserLinkedIn';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'user_linkedin.id';

    /**
     * the column name for the created_on field
     */
    const COL_CREATED_ON = 'user_linkedin.created_on';

    /**
     * the column name for the user_id field
     */
    const COL_USER_ID = 'user_linkedin.user_id';

    /**
     * the column name for the token field
     */
    const COL_TOKEN = 'user_linkedin.token';

    /**
     * the column name for the token_expires field
     */
    const COL_TOKEN_EXPIRES = 'user_linkedin.token_expires';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'user_linkedin.email';

    /**
     * the column name for the headline field
     */
    const COL_HEADLINE = 'user_linkedin.headline';

    /**
     * the column name for the picture_url field
     */
    const COL_PICTURE_URL = 'user_linkedin.picture_url';

    /**
     * the column name for the public_profile_url field
     */
    const COL_PUBLIC_PROFILE_URL = 'user_linkedin.public_profile_url';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'CreatedOn', 'UserId', 'Token', 'TokenExpires', 'Email', 'Headline', 'PictureUrl', 'PublicProfileUrl', ),
        self::TYPE_CAMELNAME     => array('id', 'createdOn', 'userId', 'token', 'tokenExpires', 'email', 'headline', 'pictureUrl', 'publicProfileUrl', ),
        self::TYPE_COLNAME       => array(UserLinkedInTableMap::COL_ID, UserLinkedInTableMap::COL_CREATED_ON, UserLinkedInTableMap::COL_USER_ID, UserLinkedInTableMap::COL_TOKEN, UserLinkedInTableMap::COL_TOKEN_EXPIRES, UserLinkedInTableMap::COL_EMAIL, UserLinkedInTableMap::COL_HEADLINE, UserLinkedInTableMap::COL_PICTURE_URL, UserLinkedInTableMap::COL_PUBLIC_PROFILE_URL, ),
        self::TYPE_FIELDNAME     => array('id', 'created_on', 'user_id', 'token', 'token_expires', 'email', 'headline', 'picture_url', 'public_profile_url', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'CreatedOn' => 1, 'UserId' => 2, 'Token' => 3, 'TokenExpires' => 4, 'Email' => 5, 'Headline' => 6, 'PictureUrl' => 7, 'PublicProfileUrl' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'createdOn' => 1, 'userId' => 2, 'token' => 3, 'tokenExpires' => 4, 'email' => 5, 'headline' => 6, 'pictureUrl' => 7, 'publicProfileUrl' => 8, ),
        self::TYPE_COLNAME       => array(UserLinkedInTableMap::COL_ID => 0, UserLinkedInTableMap::COL_CREATED_ON => 1, UserLinkedInTableMap::COL_USER_ID => 2, UserLinkedInTableMap::COL_TOKEN => 3, UserLinkedInTableMap::COL_TOKEN_EXPIRES => 4, UserLinkedInTableMap::COL_EMAIL => 5, UserLinkedInTableMap::COL_HEADLINE => 6, UserLinkedInTableMap::COL_PICTURE_URL => 7, UserLinkedInTableMap::COL_PUBLIC_PROFILE_URL => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'created_on' => 1, 'user_id' => 2, 'token' => 3, 'token_expires' => 4, 'email' => 5, 'headline' => 6, 'picture_url' => 7, 'public_profile_url' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('user_linkedin');
        $this->setPhpName('UserLinkedIn');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Crm\\UserLinkedIn');
        $this->setPackage('Model.Crm');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('created_on', 'CreatedOn', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
        $this->addForeignKey('user_id', 'UserId', 'INTEGER', 'user', 'id', true, null, null);
        $this->addColumn('token', 'Token', 'VARCHAR', true, 255, null);
        $this->addColumn('token_expires', 'TokenExpires', 'VARCHAR', true, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 255, null);
        $this->addColumn('headline', 'Headline', 'VARCHAR', false, 255, null);
        $this->addColumn('picture_url', 'PictureUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('public_profile_url', 'PublicProfileUrl', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('User', '\\Model\\Account\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':user_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UserLinkedInTableMap::CLASS_DEFAULT : UserLinkedInTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (UserLinkedIn object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UserLinkedInTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UserLinkedInTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UserLinkedInTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UserLinkedInTableMap::OM_CLASS;
            /** @var UserLinkedIn $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UserLinkedInTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UserLinkedInTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UserLinkedInTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var UserLinkedIn $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UserLinkedInTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UserLinkedInTableMap::COL_ID);
            $criteria->addSelectColumn(UserLinkedInTableMap::COL_CREATED_ON);
            $criteria->addSelectColumn(UserLinkedInTableMap::COL_USER_ID);
            $criteria->addSelectColumn(UserLinkedInTableMap::COL_TOKEN);
            $criteria->addSelectColumn(UserLinkedInTableMap::COL_TOKEN_EXPIRES);
            $criteria->addSelectColumn(UserLinkedInTableMap::COL_EMAIL);
            $criteria->addSelectColumn(UserLinkedInTableMap::COL_HEADLINE);
            $criteria->addSelectColumn(UserLinkedInTableMap::COL_PICTURE_URL);
            $criteria->addSelectColumn(UserLinkedInTableMap::COL_PUBLIC_PROFILE_URL);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.created_on');
            $criteria->addSelectColumn($alias . '.user_id');
            $criteria->addSelectColumn($alias . '.token');
            $criteria->addSelectColumn($alias . '.token_expires');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.headline');
            $criteria->addSelectColumn($alias . '.picture_url');
            $criteria->addSelectColumn($alias . '.public_profile_url');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UserLinkedInTableMap::DATABASE_NAME)->getTable(UserLinkedInTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UserLinkedInTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UserLinkedInTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UserLinkedInTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a UserLinkedIn or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or UserLinkedIn object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserLinkedInTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Crm\UserLinkedIn) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UserLinkedInTableMap::DATABASE_NAME);
            $criteria->add(UserLinkedInTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = UserLinkedInQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UserLinkedInTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UserLinkedInTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the user_linkedin table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UserLinkedInQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a UserLinkedIn or Criteria object.
     *
     * @param mixed               $criteria Criteria or UserLinkedIn object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UserLinkedInTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from UserLinkedIn object
        }

        if ($criteria->containsKey(UserLinkedInTableMap::COL_ID) && $criteria->keyContainsValue(UserLinkedInTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UserLinkedInTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = UserLinkedInQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UserLinkedInTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UserLinkedInTableMap::buildTableMap();
