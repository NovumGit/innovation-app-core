<?php

namespace Model\Crm\Map;

use Model\Crm\NewsletterSubscriber;
use Model\Crm\NewsletterSubscriberQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'newsletter_subscriber' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class NewsletterSubscriberTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Crm.Map.NewsletterSubscriberTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'newsletter_subscriber';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Crm\\NewsletterSubscriber';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Crm.NewsletterSubscriber';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'newsletter_subscriber.id';

    /**
     * the column name for the customer_id field
     */
    const COL_CUSTOMER_ID = 'newsletter_subscriber.customer_id';

    /**
     * the column name for the company_name field
     */
    const COL_COMPANY_NAME = 'newsletter_subscriber.company_name';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'newsletter_subscriber.email';

    /**
     * the column name for the first_name field
     */
    const COL_FIRST_NAME = 'newsletter_subscriber.first_name';

    /**
     * the column name for the last_name field
     */
    const COL_LAST_NAME = 'newsletter_subscriber.last_name';

    /**
     * the column name for the confirmed field
     */
    const COL_CONFIRMED = 'newsletter_subscriber.confirmed';

    /**
     * the column name for the confirm_hash field
     */
    const COL_CONFIRM_HASH = 'newsletter_subscriber.confirm_hash';

    /**
     * the column name for the created_on field
     */
    const COL_CREATED_ON = 'newsletter_subscriber.created_on';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'CustomerId', 'CompanyName', 'Email', 'FirstName', 'LastName', 'Confirmed', 'ConfirmHash', 'CreatedOn', ),
        self::TYPE_CAMELNAME     => array('id', 'customerId', 'companyName', 'email', 'firstName', 'lastName', 'confirmed', 'confirmHash', 'createdOn', ),
        self::TYPE_COLNAME       => array(NewsletterSubscriberTableMap::COL_ID, NewsletterSubscriberTableMap::COL_CUSTOMER_ID, NewsletterSubscriberTableMap::COL_COMPANY_NAME, NewsletterSubscriberTableMap::COL_EMAIL, NewsletterSubscriberTableMap::COL_FIRST_NAME, NewsletterSubscriberTableMap::COL_LAST_NAME, NewsletterSubscriberTableMap::COL_CONFIRMED, NewsletterSubscriberTableMap::COL_CONFIRM_HASH, NewsletterSubscriberTableMap::COL_CREATED_ON, ),
        self::TYPE_FIELDNAME     => array('id', 'customer_id', 'company_name', 'email', 'first_name', 'last_name', 'confirmed', 'confirm_hash', 'created_on', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'CustomerId' => 1, 'CompanyName' => 2, 'Email' => 3, 'FirstName' => 4, 'LastName' => 5, 'Confirmed' => 6, 'ConfirmHash' => 7, 'CreatedOn' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'customerId' => 1, 'companyName' => 2, 'email' => 3, 'firstName' => 4, 'lastName' => 5, 'confirmed' => 6, 'confirmHash' => 7, 'createdOn' => 8, ),
        self::TYPE_COLNAME       => array(NewsletterSubscriberTableMap::COL_ID => 0, NewsletterSubscriberTableMap::COL_CUSTOMER_ID => 1, NewsletterSubscriberTableMap::COL_COMPANY_NAME => 2, NewsletterSubscriberTableMap::COL_EMAIL => 3, NewsletterSubscriberTableMap::COL_FIRST_NAME => 4, NewsletterSubscriberTableMap::COL_LAST_NAME => 5, NewsletterSubscriberTableMap::COL_CONFIRMED => 6, NewsletterSubscriberTableMap::COL_CONFIRM_HASH => 7, NewsletterSubscriberTableMap::COL_CREATED_ON => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'customer_id' => 1, 'company_name' => 2, 'email' => 3, 'first_name' => 4, 'last_name' => 5, 'confirmed' => 6, 'confirm_hash' => 7, 'created_on' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('newsletter_subscriber');
        $this->setPhpName('NewsletterSubscriber');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Crm\\NewsletterSubscriber');
        $this->setPackage('Model.Crm');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('customer_id', 'CustomerId', 'INTEGER', 'customer', 'id', false, null, null);
        $this->addColumn('company_name', 'CompanyName', 'VARCHAR', false, 120, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 150, null);
        $this->addColumn('first_name', 'FirstName', 'VARCHAR', false, 75, null);
        $this->addColumn('last_name', 'LastName', 'VARCHAR', false, 75, null);
        $this->addColumn('confirmed', 'Confirmed', 'BOOLEAN', true, 1, false);
        $this->addColumn('confirm_hash', 'ConfirmHash', 'VARCHAR', false, 255, null);
        $this->addColumn('created_on', 'CreatedOn', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Customer', '\\Model\\Crm\\Customer', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? NewsletterSubscriberTableMap::CLASS_DEFAULT : NewsletterSubscriberTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (NewsletterSubscriber object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = NewsletterSubscriberTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = NewsletterSubscriberTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + NewsletterSubscriberTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = NewsletterSubscriberTableMap::OM_CLASS;
            /** @var NewsletterSubscriber $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            NewsletterSubscriberTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = NewsletterSubscriberTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = NewsletterSubscriberTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var NewsletterSubscriber $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                NewsletterSubscriberTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(NewsletterSubscriberTableMap::COL_ID);
            $criteria->addSelectColumn(NewsletterSubscriberTableMap::COL_CUSTOMER_ID);
            $criteria->addSelectColumn(NewsletterSubscriberTableMap::COL_COMPANY_NAME);
            $criteria->addSelectColumn(NewsletterSubscriberTableMap::COL_EMAIL);
            $criteria->addSelectColumn(NewsletterSubscriberTableMap::COL_FIRST_NAME);
            $criteria->addSelectColumn(NewsletterSubscriberTableMap::COL_LAST_NAME);
            $criteria->addSelectColumn(NewsletterSubscriberTableMap::COL_CONFIRMED);
            $criteria->addSelectColumn(NewsletterSubscriberTableMap::COL_CONFIRM_HASH);
            $criteria->addSelectColumn(NewsletterSubscriberTableMap::COL_CREATED_ON);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.customer_id');
            $criteria->addSelectColumn($alias . '.company_name');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.first_name');
            $criteria->addSelectColumn($alias . '.last_name');
            $criteria->addSelectColumn($alias . '.confirmed');
            $criteria->addSelectColumn($alias . '.confirm_hash');
            $criteria->addSelectColumn($alias . '.created_on');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(NewsletterSubscriberTableMap::DATABASE_NAME)->getTable(NewsletterSubscriberTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(NewsletterSubscriberTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(NewsletterSubscriberTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new NewsletterSubscriberTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a NewsletterSubscriber or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or NewsletterSubscriber object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(NewsletterSubscriberTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Crm\NewsletterSubscriber) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(NewsletterSubscriberTableMap::DATABASE_NAME);
            $criteria->add(NewsletterSubscriberTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = NewsletterSubscriberQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            NewsletterSubscriberTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                NewsletterSubscriberTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the newsletter_subscriber table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return NewsletterSubscriberQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a NewsletterSubscriber or Criteria object.
     *
     * @param mixed               $criteria Criteria or NewsletterSubscriber object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(NewsletterSubscriberTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from NewsletterSubscriber object
        }

        if ($criteria->containsKey(NewsletterSubscriberTableMap::COL_ID) && $criteria->keyContainsValue(NewsletterSubscriberTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.NewsletterSubscriberTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = NewsletterSubscriberQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // NewsletterSubscriberTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
NewsletterSubscriberTableMap::buildTableMap();
