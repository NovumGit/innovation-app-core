<?php

namespace Model\Crm\Map;

use Model\Crm\CustomerAddress;
use Model\Crm\CustomerAddressQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'customer_address' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CustomerAddressTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Crm.Map.CustomerAddressTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'customer_address';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Crm\\CustomerAddress';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Crm.CustomerAddress';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 16;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 16;

    /**
     * the column name for the id field
     */
    const COL_ID = 'customer_address.id';

    /**
     * the column name for the customer_id field
     */
    const COL_CUSTOMER_ID = 'customer_address.customer_id';

    /**
     * the column name for the customer_address_type_id field
     */
    const COL_CUSTOMER_ADDRESS_TYPE_ID = 'customer_address.customer_address_type_id';

    /**
     * the column name for the is_default field
     */
    const COL_IS_DEFAULT = 'customer_address.is_default';

    /**
     * the column name for the company_name field
     */
    const COL_COMPANY_NAME = 'customer_address.company_name';

    /**
     * the column name for the attn_name field
     */
    const COL_ATTN_NAME = 'customer_address.attn_name';

    /**
     * the column name for the street field
     */
    const COL_STREET = 'customer_address.street';

    /**
     * the column name for the number field
     */
    const COL_NUMBER = 'customer_address.number';

    /**
     * the column name for the number_add field
     */
    const COL_NUMBER_ADD = 'customer_address.number_add';

    /**
     * the column name for the postal field
     */
    const COL_POSTAL = 'customer_address.postal';

    /**
     * the column name for the city field
     */
    const COL_CITY = 'customer_address.city';

    /**
     * the column name for the region field
     */
    const COL_REGION = 'customer_address.region';

    /**
     * the column name for the country_id field
     */
    const COL_COUNTRY_ID = 'customer_address.country_id';

    /**
     * the column name for the usa_state_id field
     */
    const COL_USA_STATE_ID = 'customer_address.usa_state_id';

    /**
     * the column name for the address_l1 field
     */
    const COL_ADDRESS_L1 = 'customer_address.address_l1';

    /**
     * the column name for the address_l2 field
     */
    const COL_ADDRESS_L2 = 'customer_address.address_l2';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'CustomerId', 'CustomerAddressTypeId', 'IsDefault', 'CompanyName', 'AttnName', 'Street', 'Number', 'NumberAdd', 'Postal', 'City', 'Region', 'CountryId', 'UsaStateId', 'AddressL1', 'AddressL2', ),
        self::TYPE_CAMELNAME     => array('id', 'customerId', 'customerAddressTypeId', 'isDefault', 'companyName', 'attnName', 'street', 'number', 'numberAdd', 'postal', 'city', 'region', 'countryId', 'usaStateId', 'addressL1', 'addressL2', ),
        self::TYPE_COLNAME       => array(CustomerAddressTableMap::COL_ID, CustomerAddressTableMap::COL_CUSTOMER_ID, CustomerAddressTableMap::COL_CUSTOMER_ADDRESS_TYPE_ID, CustomerAddressTableMap::COL_IS_DEFAULT, CustomerAddressTableMap::COL_COMPANY_NAME, CustomerAddressTableMap::COL_ATTN_NAME, CustomerAddressTableMap::COL_STREET, CustomerAddressTableMap::COL_NUMBER, CustomerAddressTableMap::COL_NUMBER_ADD, CustomerAddressTableMap::COL_POSTAL, CustomerAddressTableMap::COL_CITY, CustomerAddressTableMap::COL_REGION, CustomerAddressTableMap::COL_COUNTRY_ID, CustomerAddressTableMap::COL_USA_STATE_ID, CustomerAddressTableMap::COL_ADDRESS_L1, CustomerAddressTableMap::COL_ADDRESS_L2, ),
        self::TYPE_FIELDNAME     => array('id', 'customer_id', 'customer_address_type_id', 'is_default', 'company_name', 'attn_name', 'street', 'number', 'number_add', 'postal', 'city', 'region', 'country_id', 'usa_state_id', 'address_l1', 'address_l2', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'CustomerId' => 1, 'CustomerAddressTypeId' => 2, 'IsDefault' => 3, 'CompanyName' => 4, 'AttnName' => 5, 'Street' => 6, 'Number' => 7, 'NumberAdd' => 8, 'Postal' => 9, 'City' => 10, 'Region' => 11, 'CountryId' => 12, 'UsaStateId' => 13, 'AddressL1' => 14, 'AddressL2' => 15, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'customerId' => 1, 'customerAddressTypeId' => 2, 'isDefault' => 3, 'companyName' => 4, 'attnName' => 5, 'street' => 6, 'number' => 7, 'numberAdd' => 8, 'postal' => 9, 'city' => 10, 'region' => 11, 'countryId' => 12, 'usaStateId' => 13, 'addressL1' => 14, 'addressL2' => 15, ),
        self::TYPE_COLNAME       => array(CustomerAddressTableMap::COL_ID => 0, CustomerAddressTableMap::COL_CUSTOMER_ID => 1, CustomerAddressTableMap::COL_CUSTOMER_ADDRESS_TYPE_ID => 2, CustomerAddressTableMap::COL_IS_DEFAULT => 3, CustomerAddressTableMap::COL_COMPANY_NAME => 4, CustomerAddressTableMap::COL_ATTN_NAME => 5, CustomerAddressTableMap::COL_STREET => 6, CustomerAddressTableMap::COL_NUMBER => 7, CustomerAddressTableMap::COL_NUMBER_ADD => 8, CustomerAddressTableMap::COL_POSTAL => 9, CustomerAddressTableMap::COL_CITY => 10, CustomerAddressTableMap::COL_REGION => 11, CustomerAddressTableMap::COL_COUNTRY_ID => 12, CustomerAddressTableMap::COL_USA_STATE_ID => 13, CustomerAddressTableMap::COL_ADDRESS_L1 => 14, CustomerAddressTableMap::COL_ADDRESS_L2 => 15, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'customer_id' => 1, 'customer_address_type_id' => 2, 'is_default' => 3, 'company_name' => 4, 'attn_name' => 5, 'street' => 6, 'number' => 7, 'number_add' => 8, 'postal' => 9, 'city' => 10, 'region' => 11, 'country_id' => 12, 'usa_state_id' => 13, 'address_l1' => 14, 'address_l2' => 15, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('customer_address');
        $this->setPhpName('CustomerAddress');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Crm\\CustomerAddress');
        $this->setPackage('Model.Crm');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('customer_id', 'CustomerId', 'INTEGER', 'customer', 'id', true, null, null);
        $this->addForeignKey('customer_address_type_id', 'CustomerAddressTypeId', 'INTEGER', 'mt_customer_address_type', 'id', true, null, null);
        $this->addColumn('is_default', 'IsDefault', 'INTEGER', true, null, false);
        $this->addColumn('company_name', 'CompanyName', 'VARCHAR', false, 120, null);
        $this->addColumn('attn_name', 'AttnName', 'VARCHAR', false, 120, null);
        $this->addColumn('street', 'Street', 'VARCHAR', false, 255, null);
        $this->addColumn('number', 'Number', 'VARCHAR', false, 150, null);
        $this->addColumn('number_add', 'NumberAdd', 'VARCHAR', false, 150, null);
        $this->addColumn('postal', 'Postal', 'VARCHAR', false, 150, null);
        $this->addColumn('city', 'City', 'VARCHAR', false, 150, null);
        $this->addColumn('region', 'Region', 'VARCHAR', false, 150, null);
        $this->addForeignKey('country_id', 'CountryId', 'INTEGER', 'mt_country', 'id', false, null, null);
        $this->addForeignKey('usa_state_id', 'UsaStateId', 'INTEGER', 'mt_usa_state', 'id', false, null, null);
        $this->addColumn('address_l1', 'AddressL1', 'VARCHAR', false, 255, null);
        $this->addColumn('address_l2', 'AddressL2', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Country', '\\Model\\Setting\\MasterTable\\Country', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':country_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('UsaState', '\\Model\\Setting\\MasterTable\\UsaState', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':usa_state_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('Customer', '\\Model\\Crm\\Customer', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('CustomerAddressType', '\\Model\\Crm\\CustomerAddressType', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':customer_address_type_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CustomerAddressTableMap::CLASS_DEFAULT : CustomerAddressTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CustomerAddress object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CustomerAddressTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CustomerAddressTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CustomerAddressTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CustomerAddressTableMap::OM_CLASS;
            /** @var CustomerAddress $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CustomerAddressTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CustomerAddressTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CustomerAddressTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CustomerAddress $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CustomerAddressTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_ID);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_CUSTOMER_ID);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_CUSTOMER_ADDRESS_TYPE_ID);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_IS_DEFAULT);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_COMPANY_NAME);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_ATTN_NAME);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_STREET);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_NUMBER);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_NUMBER_ADD);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_POSTAL);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_CITY);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_REGION);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_COUNTRY_ID);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_USA_STATE_ID);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_ADDRESS_L1);
            $criteria->addSelectColumn(CustomerAddressTableMap::COL_ADDRESS_L2);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.customer_id');
            $criteria->addSelectColumn($alias . '.customer_address_type_id');
            $criteria->addSelectColumn($alias . '.is_default');
            $criteria->addSelectColumn($alias . '.company_name');
            $criteria->addSelectColumn($alias . '.attn_name');
            $criteria->addSelectColumn($alias . '.street');
            $criteria->addSelectColumn($alias . '.number');
            $criteria->addSelectColumn($alias . '.number_add');
            $criteria->addSelectColumn($alias . '.postal');
            $criteria->addSelectColumn($alias . '.city');
            $criteria->addSelectColumn($alias . '.region');
            $criteria->addSelectColumn($alias . '.country_id');
            $criteria->addSelectColumn($alias . '.usa_state_id');
            $criteria->addSelectColumn($alias . '.address_l1');
            $criteria->addSelectColumn($alias . '.address_l2');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CustomerAddressTableMap::DATABASE_NAME)->getTable(CustomerAddressTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CustomerAddressTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CustomerAddressTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CustomerAddressTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CustomerAddress or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CustomerAddress object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerAddressTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Crm\CustomerAddress) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CustomerAddressTableMap::DATABASE_NAME);
            $criteria->add(CustomerAddressTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CustomerAddressQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CustomerAddressTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CustomerAddressTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the customer_address table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CustomerAddressQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CustomerAddress or Criteria object.
     *
     * @param mixed               $criteria Criteria or CustomerAddress object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomerAddressTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CustomerAddress object
        }

        if ($criteria->containsKey(CustomerAddressTableMap::COL_ID) && $criteria->keyContainsValue(CustomerAddressTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CustomerAddressTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CustomerAddressQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CustomerAddressTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CustomerAddressTableMap::buildTableMap();
