<?php

namespace Model\Crm;

use Core\Config;
use Model\Crm\Base\CustomerPhoto as BaseCustomerPhoto;

/**
 * Skeleton subclass for representing a row from the 'customer_photo' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerPhoto extends BaseCustomerPhoto
{

    function getMyFileName()
    {
        $sRelativeDir = '/img/customer/'.$this->getCustomerId().'/company_photos/'.$this->getId().'.'.$this->getFileExt();
        return Config::getDataDir().$sRelativeDir;
    }
    function deleteWithFile()
    {
        $sMyFileName = $this->getMyFileName();
        if(file_exists($sMyFileName))
        {
            unlink($sMyFileName);
        }
        $this->delete();
    }

}
