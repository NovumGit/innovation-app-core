<?php

namespace Model\Crm;

use Exception\LogicException;
use Model\Crm\Base\CustomerPropertyQuery as BaseCustomerPropertyQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'customer_property' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerPropertyQuery extends BaseCustomerPropertyQuery
{
    static function getItem($iCustomerId, $sKey)
    {
        $oCustomerProperty = self::create()->filterByCustomerId($iCustomerId)->filterByPropertyKey($sKey)->findOne();
        if(!$oCustomerProperty instanceof CustomerProperty)
        {
            return null;
        }
        return $oCustomerProperty->getPropertyValue();
    }
    static function storeItem($iCustomerId, $sKey, $sValue)
    {
        $oCustomerProperty = self::create()->filterByCustomerId($iCustomerId)->filterByPropertyKey($sKey)->findOne();
        if(!$oCustomerProperty instanceof CustomerProperty)
        {
            $oCustomerProperty = new CustomerProperty();
            $oCustomerProperty->setCustomerId($iCustomerId);
            $oCustomerProperty->setPropertyKey($sKey);
        }
        $oCustomerProperty->setPropertyValue($sValue);
        $oCustomerProperty->save();
    }
    static function storeSet($iCustomerId, $aDataSet)
    {
        $aAllowedProperties = [
            'short_description',
            'long_description',
            'tags',
            'parking_garage',
            'parking_street',
            'parking_free',
            'parking_valet',
            'travel_tips',
            'airport_distance',
            'public_transport_distance',
            'social_facebook',
            'social_twitter',
            'social_instagram',
            'social_youtube',
            'social_googleplus',
            'social_pinterest',
            'social_linkedin',
            'open_monday',
            'open_tuesday',
            'open_wednesday',
            'open_thursday',
            'open_friday',
            'open_saturday',
            'open_sunday',
            'close_monday',
            'close_tuesday',
            'close_wednesday',
            'close_thursday',
            'close_friday',
            'close_saturday',
            'close_sunday',
            'is_monday_open',
            'is_tuesday_open',
            'is_wednesday_open',
            'is_thursday_open',
            'is_friday_open',
            'is_saturday_open',
            'is_sunday_open',
            'note_open_monday',
            'note_open_tuesday',
            'note_open_wednesday',
            'note_open_thursday',
            'note_open_friday',
            'note_open_saturday',
            'note_open_sunday',
            'open_new_year_day',
            'open_good_friday',
            'open_first_easterday',
            'open_second_easterday',
            'open_kingsday',
            'open_ascension_day',
            'open_liberation_day',
            'open_1st_pentecost',
            'open_2nd_pentecost',
            'open_1st_chrismas_day',
            'open_1nd_chrismas_day',
            'open_new_years_eve',
            'close_new_year_day',
            'close_good_friday',
            'close_first_easterday',
            'close_second_easterday',
            'close_kingsday',
            'close_ascension_day',
            'close_liberation_day',
            'close_1st_pentecost',
            'close_2nd_pentecost',
            'close_1st_chrismas_day',
            'close_1nd_chrismas_day',
            'close_new_years_eve',
            'is_new_year_day_open',
            'is_good_friday_open',
            'is_first_easterday_open',
            'is_second_easterday_open',
            'is_kingsday_open',
            'is_ascension_day_open',
            'is_liberation_day_open',
            'is_1st_pentecost_open',
            'is_2nd_pentecost_open',
            'is_1st_chrismas_day_open',
            'is_1nd_chrismas_day_open',
            'is_new_years_eve_open',
            'note_open_new_year_day',
            'note_open_good_friday',
            'note_open_first_easterday',
            'note_open_second_easterday',
            'note_open_kingsday',
            'note_open_ascension_day',
            'note_open_liberation_day',
            'note_open_1st_pentecost',
            'note_open_2nd_pentecost',
            'note_open_1st_chrismas_day',
            'note_open_1nd_chrismas_day',
            'note_open_new_years_eve',
            'pay_method_cash',
            'pay_method_pin',
            'pay_method_contactless',
            'pay_method_bank',
            'pay_method_ideal',
            'pay_method_customerpass',
            'pay_method_vvv_note',
            'pay_method_under_reimbursement',
            'pay_method_prepayed',
            'pay_method_other_giftcard',
            'pay_method_spread_paypment',
            'pay_method_trough_insurance',
            'pay_method_other_payment_methods',
            'pay_method_visa',
            'pay_method_meastro',
            'pay_method_western_union',
            'pay_method_skrill',
            'pay_method_master_card',
            'pay_method_cirrus',
            'pay_method_paypal',
            'pay_method_wallet_pay',
            'pay_method_american_express',
            'pay_method_discover',
            'pay_method_amazon_pay',
            'pay_method_apple_pay',
            'contact_email',
            'contact_snapchat',
            'contact_whatsapp',
            'contact_skype',
            'contact_viber',
            'contact_phone',
            'contact_wechat',
            'qpinion_description',
            'survey_question_1',
            'survey_question_2',
            'survey_question_3',
            'survey_question_4',
            'survey_question_5',
            'survey_question_6',

            'survey_answer_1',
            'survey_answer_2',
            'survey_answer_3',
            'survey_answer_4',
            'survey_answer_5',
            'survey_answer_6',

            'survey_options_1',
            'survey_options_2',
            'survey_options_3',
            'survey_options_4',
            'survey_options_5',
            'survey_options_6',

            'survey_generated_once',
            'open_questions'
        ];
        foreach($aDataSet as $sKey => $sValue)
        {
            if(!in_array($sKey, $aAllowedProperties))
            {
                throw new LogicException("PropertyModel $sKey is not in the allowed property list.");
            }
            $oCustomerProperty = self::create()->filterByCustomerId($iCustomerId)->filterByPropertyKey($sKey)->findOne();


            if(!$oCustomerProperty instanceof CustomerProperty)
            {
                $oCustomerProperty = new CustomerProperty();
                $oCustomerProperty->setCustomerId($iCustomerId);
                $oCustomerProperty->setPropertyKey($sKey);
            }

            $oCustomerProperty->setPropertyValue($sValue);
            $oCustomerProperty->save();
        }
        return null;
    }
    static function getSet($iCustomerId)
    {
        $oPropertyIterator = CustomerPropertyQuery::create()->findByCustomerId($iCustomerId);

        if($oPropertyIterator->isEmpty())
        {
            return null;
        }
        $aProperties = [];
        foreach($oPropertyIterator as $oProperty)
        {
            $aProperties[$oProperty->getPropertyKey()] = $oProperty->getPropertyValue();
        }
        return $aProperties;
    }
}
