<?php

namespace Model\Crm;

use Model\Crm\Base\CustomerLogoQuery as BaseCustomerLogoQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'customer_logo' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerLogoQuery extends BaseCustomerLogoQuery
{

}
