<?php

namespace Model\Marketing;

use Model\Marketing\Base\MailingMessagesQuery as BaseMailingMessagesQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'mailing_messages' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MailingMessagesQuery extends BaseMailingMessagesQuery
{

}
