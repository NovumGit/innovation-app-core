<?php

namespace Model\Marketing;

use Model\Marketing\Base\MailingMessages as BaseMailingMessages;

/**
 * Skeleton subclass for representing a row from the 'mailing_messages' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MailingMessages extends BaseMailingMessages
{

}
