<?php

namespace Model\Marketing;

use Model\Marketing\Base\Coupon as BaseCoupon;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderQuery;

/**
 * Skeleton subclass for representing a row from the 'coupon' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Coupon extends BaseCoupon
{
    function hasBeenUsed()
    {
        $oSaleOrder = SaleOrderQuery::create()->findOneByCouponCode($this->getRedeemCode());
        return ($oSaleOrder instanceof SaleOrder);
    }
}
