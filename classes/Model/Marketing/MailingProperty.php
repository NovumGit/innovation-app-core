<?php

namespace Model\Marketing;

use Model\Marketing\Base\MailingProperty as BaseMailingProperty;

/**
 * Skeleton subclass for representing a row from the 'mailing_property' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MailingProperty extends BaseMailingProperty
{
    static function store($iMailingId, $iKey, $sValue)
    {
        $oMailingPropertyQuery = MailingPropertyQuery::create();
        $oMailingPropertyQuery->filterByPropertyKey($iKey);
        $oMailingPropertyQuery->filterByMailingId($iMailingId);
        $oMailingProperty = $oMailingPropertyQuery->findOne();

        if(!$oMailingProperty instanceof MailingProperty)
        {
            $oMailingProperty = new MailingProperty();
            $oMailingProperty->setPropertyKey($iKey);
            $oMailingProperty->setMailingId($iMailingId);
        }
        $oMailingProperty->setPropertyValue($sValue);
        $oMailingProperty->save();
    }

    static function getAllPropertiesAssoc($iMailingId)
    {
        $oMailingPropertyQuery = MailingPropertyQuery::create();
        $oMailingPropertyQuery->filterByMailingId($iMailingId);
        $aMailingProperties = $oMailingPropertyQuery->find();
        $aOut = [];

        if(!empty($aMailingProperties))
        {
            foreach($aMailingProperties as $oMailingProperty)
            {
                $aOut[$oMailingProperty->getPropertyKey()] = $oMailingProperty->getPropertyValue();
            }
        }
        return $aOut;
    }
}
