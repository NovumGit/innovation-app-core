<?php

namespace Model\Marketing;

use Crud\ICrudFilterModel;
use Model\Marketing\Base\MailingListsCriteria as BaseMailingListsCriteria;

/**
 * Skeleton subclass for representing a row from the 'mailing_list_criteria' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MailingListsCriteria extends BaseMailingListsCriteria implements ICrudFilterModel
{

}
