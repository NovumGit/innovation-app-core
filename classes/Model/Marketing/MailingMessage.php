<?php

namespace Model\Marketing;

use Model\Marketing\Base\MailingMessage as BaseMailingMessage;

/**
 * Skeleton subclass for representing a row from the 'mailing_message' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MailingMessage extends BaseMailingMessage
{

}
