<?php

namespace Model\Marketing;

use AdminModules\Marketing\Mailing\Mailing_list\FilterHelper;
use Model\Crm\CustomerQuery;
use Model\Marketing\Base\MailingList as BaseMailingList;

/**
 * Skeleton subclass for representing a row from the 'mailing_list' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MailingList extends BaseMailingList
{
    function getUnsubscribedCount()
    {
        $oCustomerQuery = new CustomerQuery();
        $oCustomerQuery->filterByIsNewsletterSubscriber(false);
        $oCustomerQuery->filterByItemDeleted(false);
        $oCustomerQuery = FilterHelper::applyFilters($oCustomerQuery, $this->getId());

        return $oCustomerQuery->count();
    }
    function getSubscriberCount()
    {
        $oCustomerQuery = new CustomerQuery();
        $oCustomerQuery->filterByIsNewsletterSubscriber(true);
        $oCustomerQuery->filterByItemDeleted(false);
        $oCustomerQuery = FilterHelper::applyFilters($oCustomerQuery, $this->getId());

        return $oCustomerQuery->count();
    }
}
