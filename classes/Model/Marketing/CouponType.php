<?php

namespace Model\Marketing;

use Model\Marketing\Base\CouponType as BaseCouponType;

/**
 * Skeleton subclass for representing a row from the 'coupon_type' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CouponType extends BaseCouponType
{

}
