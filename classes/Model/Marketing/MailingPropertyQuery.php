<?php

namespace Model\Marketing;

use Model\Marketing\Base\MailingPropertyQuery as BaseMailingPropertyQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'mailing_property' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MailingPropertyQuery extends BaseMailingPropertyQuery
{

}
