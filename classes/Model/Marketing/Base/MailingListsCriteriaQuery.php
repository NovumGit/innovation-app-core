<?php

namespace Model\Marketing\Base;

use \Exception;
use \PDO;
use Model\Marketing\MailingListsCriteria as ChildMailingListsCriteria;
use Model\Marketing\MailingListsCriteriaQuery as ChildMailingListsCriteriaQuery;
use Model\Marketing\Map\MailingListsCriteriaTableMap;
use Model\Setting\CrudManager\FilterOperator;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mailing_list_criteria' table.
 *
 *
 *
 * @method     ChildMailingListsCriteriaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildMailingListsCriteriaQuery orderByMailingListId($order = Criteria::ASC) Order by the mailing_list_id column
 * @method     ChildMailingListsCriteriaQuery orderByFilterOperatorId($order = Criteria::ASC) Order by the filter_operator_id column
 * @method     ChildMailingListsCriteriaQuery orderByFilterName($order = Criteria::ASC) Order by the filter_name column
 * @method     ChildMailingListsCriteriaQuery orderByFilterValue($order = Criteria::ASC) Order by the filter_value column
 *
 * @method     ChildMailingListsCriteriaQuery groupById() Group by the id column
 * @method     ChildMailingListsCriteriaQuery groupByMailingListId() Group by the mailing_list_id column
 * @method     ChildMailingListsCriteriaQuery groupByFilterOperatorId() Group by the filter_operator_id column
 * @method     ChildMailingListsCriteriaQuery groupByFilterName() Group by the filter_name column
 * @method     ChildMailingListsCriteriaQuery groupByFilterValue() Group by the filter_value column
 *
 * @method     ChildMailingListsCriteriaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMailingListsCriteriaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMailingListsCriteriaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMailingListsCriteriaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMailingListsCriteriaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMailingListsCriteriaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMailingListsCriteriaQuery leftJoinFilterOperator($relationAlias = null) Adds a LEFT JOIN clause to the query using the FilterOperator relation
 * @method     ChildMailingListsCriteriaQuery rightJoinFilterOperator($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FilterOperator relation
 * @method     ChildMailingListsCriteriaQuery innerJoinFilterOperator($relationAlias = null) Adds a INNER JOIN clause to the query using the FilterOperator relation
 *
 * @method     ChildMailingListsCriteriaQuery joinWithFilterOperator($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FilterOperator relation
 *
 * @method     ChildMailingListsCriteriaQuery leftJoinWithFilterOperator() Adds a LEFT JOIN clause and with to the query using the FilterOperator relation
 * @method     ChildMailingListsCriteriaQuery rightJoinWithFilterOperator() Adds a RIGHT JOIN clause and with to the query using the FilterOperator relation
 * @method     ChildMailingListsCriteriaQuery innerJoinWithFilterOperator() Adds a INNER JOIN clause and with to the query using the FilterOperator relation
 *
 * @method     ChildMailingListsCriteriaQuery leftJoinMailingList($relationAlias = null) Adds a LEFT JOIN clause to the query using the MailingList relation
 * @method     ChildMailingListsCriteriaQuery rightJoinMailingList($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MailingList relation
 * @method     ChildMailingListsCriteriaQuery innerJoinMailingList($relationAlias = null) Adds a INNER JOIN clause to the query using the MailingList relation
 *
 * @method     ChildMailingListsCriteriaQuery joinWithMailingList($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MailingList relation
 *
 * @method     ChildMailingListsCriteriaQuery leftJoinWithMailingList() Adds a LEFT JOIN clause and with to the query using the MailingList relation
 * @method     ChildMailingListsCriteriaQuery rightJoinWithMailingList() Adds a RIGHT JOIN clause and with to the query using the MailingList relation
 * @method     ChildMailingListsCriteriaQuery innerJoinWithMailingList() Adds a INNER JOIN clause and with to the query using the MailingList relation
 *
 * @method     \Model\Setting\CrudManager\FilterOperatorQuery|\Model\Marketing\MailingListQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMailingListsCriteria findOne(ConnectionInterface $con = null) Return the first ChildMailingListsCriteria matching the query
 * @method     ChildMailingListsCriteria findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMailingListsCriteria matching the query, or a new ChildMailingListsCriteria object populated from the query conditions when no match is found
 *
 * @method     ChildMailingListsCriteria findOneById(int $id) Return the first ChildMailingListsCriteria filtered by the id column
 * @method     ChildMailingListsCriteria findOneByMailingListId(int $mailing_list_id) Return the first ChildMailingListsCriteria filtered by the mailing_list_id column
 * @method     ChildMailingListsCriteria findOneByFilterOperatorId(int $filter_operator_id) Return the first ChildMailingListsCriteria filtered by the filter_operator_id column
 * @method     ChildMailingListsCriteria findOneByFilterName(string $filter_name) Return the first ChildMailingListsCriteria filtered by the filter_name column
 * @method     ChildMailingListsCriteria findOneByFilterValue(string $filter_value) Return the first ChildMailingListsCriteria filtered by the filter_value column *

 * @method     ChildMailingListsCriteria requirePk($key, ConnectionInterface $con = null) Return the ChildMailingListsCriteria by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailingListsCriteria requireOne(ConnectionInterface $con = null) Return the first ChildMailingListsCriteria matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMailingListsCriteria requireOneById(int $id) Return the first ChildMailingListsCriteria filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailingListsCriteria requireOneByMailingListId(int $mailing_list_id) Return the first ChildMailingListsCriteria filtered by the mailing_list_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailingListsCriteria requireOneByFilterOperatorId(int $filter_operator_id) Return the first ChildMailingListsCriteria filtered by the filter_operator_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailingListsCriteria requireOneByFilterName(string $filter_name) Return the first ChildMailingListsCriteria filtered by the filter_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailingListsCriteria requireOneByFilterValue(string $filter_value) Return the first ChildMailingListsCriteria filtered by the filter_value column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMailingListsCriteria[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMailingListsCriteria objects based on current ModelCriteria
 * @method     ChildMailingListsCriteria[]|ObjectCollection findById(int $id) Return ChildMailingListsCriteria objects filtered by the id column
 * @method     ChildMailingListsCriteria[]|ObjectCollection findByMailingListId(int $mailing_list_id) Return ChildMailingListsCriteria objects filtered by the mailing_list_id column
 * @method     ChildMailingListsCriteria[]|ObjectCollection findByFilterOperatorId(int $filter_operator_id) Return ChildMailingListsCriteria objects filtered by the filter_operator_id column
 * @method     ChildMailingListsCriteria[]|ObjectCollection findByFilterName(string $filter_name) Return ChildMailingListsCriteria objects filtered by the filter_name column
 * @method     ChildMailingListsCriteria[]|ObjectCollection findByFilterValue(string $filter_value) Return ChildMailingListsCriteria objects filtered by the filter_value column
 * @method     ChildMailingListsCriteria[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MailingListsCriteriaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Marketing\Base\MailingListsCriteriaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Marketing\\MailingListsCriteria', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMailingListsCriteriaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMailingListsCriteriaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMailingListsCriteriaQuery) {
            return $criteria;
        }
        $query = new ChildMailingListsCriteriaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMailingListsCriteria|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MailingListsCriteriaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MailingListsCriteriaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMailingListsCriteria A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, mailing_list_id, filter_operator_id, filter_name, filter_value FROM mailing_list_criteria WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMailingListsCriteria $obj */
            $obj = new ChildMailingListsCriteria();
            $obj->hydrate($row);
            MailingListsCriteriaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMailingListsCriteria|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMailingListsCriteriaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MailingListsCriteriaTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMailingListsCriteriaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MailingListsCriteriaTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingListsCriteriaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MailingListsCriteriaTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MailingListsCriteriaTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingListsCriteriaTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the mailing_list_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMailingListId(1234); // WHERE mailing_list_id = 1234
     * $query->filterByMailingListId(array(12, 34)); // WHERE mailing_list_id IN (12, 34)
     * $query->filterByMailingListId(array('min' => 12)); // WHERE mailing_list_id > 12
     * </code>
     *
     * @see       filterByMailingList()
     *
     * @param     mixed $mailingListId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingListsCriteriaQuery The current query, for fluid interface
     */
    public function filterByMailingListId($mailingListId = null, $comparison = null)
    {
        if (is_array($mailingListId)) {
            $useMinMax = false;
            if (isset($mailingListId['min'])) {
                $this->addUsingAlias(MailingListsCriteriaTableMap::COL_MAILING_LIST_ID, $mailingListId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mailingListId['max'])) {
                $this->addUsingAlias(MailingListsCriteriaTableMap::COL_MAILING_LIST_ID, $mailingListId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingListsCriteriaTableMap::COL_MAILING_LIST_ID, $mailingListId, $comparison);
    }

    /**
     * Filter the query on the filter_operator_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFilterOperatorId(1234); // WHERE filter_operator_id = 1234
     * $query->filterByFilterOperatorId(array(12, 34)); // WHERE filter_operator_id IN (12, 34)
     * $query->filterByFilterOperatorId(array('min' => 12)); // WHERE filter_operator_id > 12
     * </code>
     *
     * @see       filterByFilterOperator()
     *
     * @param     mixed $filterOperatorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingListsCriteriaQuery The current query, for fluid interface
     */
    public function filterByFilterOperatorId($filterOperatorId = null, $comparison = null)
    {
        if (is_array($filterOperatorId)) {
            $useMinMax = false;
            if (isset($filterOperatorId['min'])) {
                $this->addUsingAlias(MailingListsCriteriaTableMap::COL_FILTER_OPERATOR_ID, $filterOperatorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($filterOperatorId['max'])) {
                $this->addUsingAlias(MailingListsCriteriaTableMap::COL_FILTER_OPERATOR_ID, $filterOperatorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingListsCriteriaTableMap::COL_FILTER_OPERATOR_ID, $filterOperatorId, $comparison);
    }

    /**
     * Filter the query on the filter_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFilterName('fooValue');   // WHERE filter_name = 'fooValue'
     * $query->filterByFilterName('%fooValue%', Criteria::LIKE); // WHERE filter_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $filterName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingListsCriteriaQuery The current query, for fluid interface
     */
    public function filterByFilterName($filterName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($filterName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingListsCriteriaTableMap::COL_FILTER_NAME, $filterName, $comparison);
    }

    /**
     * Filter the query on the filter_value column
     *
     * Example usage:
     * <code>
     * $query->filterByFilterValue('fooValue');   // WHERE filter_value = 'fooValue'
     * $query->filterByFilterValue('%fooValue%', Criteria::LIKE); // WHERE filter_value LIKE '%fooValue%'
     * </code>
     *
     * @param     string $filterValue The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingListsCriteriaQuery The current query, for fluid interface
     */
    public function filterByFilterValue($filterValue = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($filterValue)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingListsCriteriaTableMap::COL_FILTER_VALUE, $filterValue, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\FilterOperator object
     *
     * @param \Model\Setting\CrudManager\FilterOperator|ObjectCollection $filterOperator The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMailingListsCriteriaQuery The current query, for fluid interface
     */
    public function filterByFilterOperator($filterOperator, $comparison = null)
    {
        if ($filterOperator instanceof \Model\Setting\CrudManager\FilterOperator) {
            return $this
                ->addUsingAlias(MailingListsCriteriaTableMap::COL_FILTER_OPERATOR_ID, $filterOperator->getId(), $comparison);
        } elseif ($filterOperator instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MailingListsCriteriaTableMap::COL_FILTER_OPERATOR_ID, $filterOperator->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFilterOperator() only accepts arguments of type \Model\Setting\CrudManager\FilterOperator or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FilterOperator relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMailingListsCriteriaQuery The current query, for fluid interface
     */
    public function joinFilterOperator($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FilterOperator');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FilterOperator');
        }

        return $this;
    }

    /**
     * Use the FilterOperator relation FilterOperator object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\FilterOperatorQuery A secondary query class using the current class as primary query
     */
    public function useFilterOperatorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFilterOperator($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FilterOperator', '\Model\Setting\CrudManager\FilterOperatorQuery');
    }

    /**
     * Filter the query by a related \Model\Marketing\MailingList object
     *
     * @param \Model\Marketing\MailingList|ObjectCollection $mailingList The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMailingListsCriteriaQuery The current query, for fluid interface
     */
    public function filterByMailingList($mailingList, $comparison = null)
    {
        if ($mailingList instanceof \Model\Marketing\MailingList) {
            return $this
                ->addUsingAlias(MailingListsCriteriaTableMap::COL_MAILING_LIST_ID, $mailingList->getId(), $comparison);
        } elseif ($mailingList instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MailingListsCriteriaTableMap::COL_MAILING_LIST_ID, $mailingList->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMailingList() only accepts arguments of type \Model\Marketing\MailingList or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MailingList relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMailingListsCriteriaQuery The current query, for fluid interface
     */
    public function joinMailingList($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MailingList');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MailingList');
        }

        return $this;
    }

    /**
     * Use the MailingList relation MailingList object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Marketing\MailingListQuery A secondary query class using the current class as primary query
     */
    public function useMailingListQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMailingList($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MailingList', '\Model\Marketing\MailingListQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMailingListsCriteria $mailingListsCriteria Object to remove from the list of results
     *
     * @return $this|ChildMailingListsCriteriaQuery The current query, for fluid interface
     */
    public function prune($mailingListsCriteria = null)
    {
        if ($mailingListsCriteria) {
            $this->addUsingAlias(MailingListsCriteriaTableMap::COL_ID, $mailingListsCriteria->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mailing_list_criteria table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MailingListsCriteriaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MailingListsCriteriaTableMap::clearInstancePool();
            MailingListsCriteriaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MailingListsCriteriaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MailingListsCriteriaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MailingListsCriteriaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MailingListsCriteriaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MailingListsCriteriaQuery
