<?php

namespace Model\Marketing\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Marketing\Mailing as ChildMailing;
use Model\Marketing\MailingList as ChildMailingList;
use Model\Marketing\MailingListQuery as ChildMailingListQuery;
use Model\Marketing\MailingMessage as ChildMailingMessage;
use Model\Marketing\MailingMessageQuery as ChildMailingMessageQuery;
use Model\Marketing\MailingProperty as ChildMailingProperty;
use Model\Marketing\MailingPropertyQuery as ChildMailingPropertyQuery;
use Model\Marketing\MailingQuery as ChildMailingQuery;
use Model\Marketing\MailingTemplate as ChildMailingTemplate;
use Model\Marketing\MailingTemplateQuery as ChildMailingTemplateQuery;
use Model\Marketing\Map\MailingMessageTableMap;
use Model\Marketing\Map\MailingPropertyTableMap;
use Model\Marketing\Map\MailingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'mailing' table.
 *
 *
 *
 * @package    propel.generator.Model.Marketing.Base
 */
abstract class Mailing implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Marketing\\Map\\MailingTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the mailing_list_id field.
     *
     * @var        int
     */
    protected $mailing_list_id;

    /**
     * The value for the mailing_template_id field.
     *
     * @var        int
     */
    protected $mailing_template_id;

    /**
     * The value for the from_address field.
     *
     * @var        string
     */
    protected $from_address;

    /**
     * The value for the subject field.
     *
     * @var        string
     */
    protected $subject;

    /**
     * The value for the contents field.
     *
     * @var        string
     */
    protected $contents;

    /**
     * The value for the is_playing field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_playing;

    /**
     * The value for the total_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int|null
     */
    protected $total_count;

    /**
     * The value for the error_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int|null
     */
    protected $error_count;

    /**
     * The value for the send_count field.
     *
     * Note: this column has a database default value of: 0
     * @var        int|null
     */
    protected $send_count;

    /**
     * The value for the status field.
     *
     * @var        string|null
     */
    protected $status;

    /**
     * The value for the created_date field.
     *
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        DateTime|null
     */
    protected $created_date;

    /**
     * @var        ChildMailingList
     */
    protected $aMailingList;

    /**
     * @var        ChildMailingTemplate
     */
    protected $aMailingTemplate;

    /**
     * @var        ObjectCollection|ChildMailingMessage[] Collection to store aggregation of ChildMailingMessage objects.
     */
    protected $collMailingMessages;
    protected $collMailingMessagesPartial;

    /**
     * @var        ObjectCollection|ChildMailingProperty[] Collection to store aggregation of ChildMailingProperty objects.
     */
    protected $collMailingProperties;
    protected $collMailingPropertiesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMailingMessage[]
     */
    protected $mailingMessagesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildMailingProperty[]
     */
    protected $mailingPropertiesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_playing = false;
        $this->total_count = 0;
        $this->error_count = 0;
        $this->send_count = 0;
    }

    /**
     * Initializes internal state of Model\Marketing\Base\Mailing object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Mailing</code> instance.  If
     * <code>obj</code> is an instance of <code>Mailing</code>, delegates to
     * <code>equals(Mailing)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [mailing_list_id] column value.
     *
     * @return int
     */
    public function getMailingListId()
    {
        return $this->mailing_list_id;
    }

    /**
     * Get the [mailing_template_id] column value.
     *
     * @return int
     */
    public function getMailingTemplateId()
    {
        return $this->mailing_template_id;
    }

    /**
     * Get the [from_address] column value.
     *
     * @return string
     */
    public function getFromAddress()
    {
        return $this->from_address;
    }

    /**
     * Get the [subject] column value.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Get the [contents] column value.
     *
     * @return string
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * Get the [is_playing] column value.
     *
     * @return boolean|null
     */
    public function getIsPlaying()
    {
        return $this->is_playing;
    }

    /**
     * Get the [is_playing] column value.
     *
     * @return boolean|null
     */
    public function isPlaying()
    {
        return $this->getIsPlaying();
    }

    /**
     * Get the [total_count] column value.
     *
     * @return int|null
     */
    public function getTotalCount()
    {
        return $this->total_count;
    }

    /**
     * Get the [error_count] column value.
     *
     * @return int|null
     */
    public function getErrorCount()
    {
        return $this->error_count;
    }

    /**
     * Get the [send_count] column value.
     *
     * @return int|null
     */
    public function getSendCount()
    {
        return $this->send_count;
    }

    /**
     * Get the [status] column value.
     *
     * @return string|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get the [optionally formatted] temporal [created_date] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedDate($format = null)
    {
        if ($format === null) {
            return $this->created_date;
        } else {
            return $this->created_date instanceof \DateTimeInterface ? $this->created_date->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[MailingTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [mailing_list_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function setMailingListId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->mailing_list_id !== $v) {
            $this->mailing_list_id = $v;
            $this->modifiedColumns[MailingTableMap::COL_MAILING_LIST_ID] = true;
        }

        if ($this->aMailingList !== null && $this->aMailingList->getId() !== $v) {
            $this->aMailingList = null;
        }

        return $this;
    } // setMailingListId()

    /**
     * Set the value of [mailing_template_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function setMailingTemplateId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->mailing_template_id !== $v) {
            $this->mailing_template_id = $v;
            $this->modifiedColumns[MailingTableMap::COL_MAILING_TEMPLATE_ID] = true;
        }

        if ($this->aMailingTemplate !== null && $this->aMailingTemplate->getId() !== $v) {
            $this->aMailingTemplate = null;
        }

        return $this;
    } // setMailingTemplateId()

    /**
     * Set the value of [from_address] column.
     *
     * @param string $v New value
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function setFromAddress($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->from_address !== $v) {
            $this->from_address = $v;
            $this->modifiedColumns[MailingTableMap::COL_FROM_ADDRESS] = true;
        }

        return $this;
    } // setFromAddress()

    /**
     * Set the value of [subject] column.
     *
     * @param string $v New value
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function setSubject($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->subject !== $v) {
            $this->subject = $v;
            $this->modifiedColumns[MailingTableMap::COL_SUBJECT] = true;
        }

        return $this;
    } // setSubject()

    /**
     * Set the value of [contents] column.
     *
     * @param string $v New value
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function setContents($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->contents !== $v) {
            $this->contents = $v;
            $this->modifiedColumns[MailingTableMap::COL_CONTENTS] = true;
        }

        return $this;
    } // setContents()

    /**
     * Sets the value of the [is_playing] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function setIsPlaying($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_playing !== $v) {
            $this->is_playing = $v;
            $this->modifiedColumns[MailingTableMap::COL_IS_PLAYING] = true;
        }

        return $this;
    } // setIsPlaying()

    /**
     * Set the value of [total_count] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function setTotalCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->total_count !== $v) {
            $this->total_count = $v;
            $this->modifiedColumns[MailingTableMap::COL_TOTAL_COUNT] = true;
        }

        return $this;
    } // setTotalCount()

    /**
     * Set the value of [error_count] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function setErrorCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->error_count !== $v) {
            $this->error_count = $v;
            $this->modifiedColumns[MailingTableMap::COL_ERROR_COUNT] = true;
        }

        return $this;
    } // setErrorCount()

    /**
     * Set the value of [send_count] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function setSendCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->send_count !== $v) {
            $this->send_count = $v;
            $this->modifiedColumns[MailingTableMap::COL_SEND_COUNT] = true;
        }

        return $this;
    } // setSendCount()

    /**
     * Set the value of [status] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function setStatus($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->status !== $v) {
            $this->status = $v;
            $this->modifiedColumns[MailingTableMap::COL_STATUS] = true;
        }

        return $this;
    } // setStatus()

    /**
     * Sets the value of [created_date] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function setCreatedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_date !== null || $dt !== null) {
            if ($this->created_date === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_date->format("Y-m-d H:i:s.u")) {
                $this->created_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[MailingTableMap::COL_CREATED_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedDate()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_playing !== false) {
                return false;
            }

            if ($this->total_count !== 0) {
                return false;
            }

            if ($this->error_count !== 0) {
                return false;
            }

            if ($this->send_count !== 0) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : MailingTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : MailingTableMap::translateFieldName('MailingListId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mailing_list_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : MailingTableMap::translateFieldName('MailingTemplateId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->mailing_template_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : MailingTableMap::translateFieldName('FromAddress', TableMap::TYPE_PHPNAME, $indexType)];
            $this->from_address = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : MailingTableMap::translateFieldName('Subject', TableMap::TYPE_PHPNAME, $indexType)];
            $this->subject = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : MailingTableMap::translateFieldName('Contents', TableMap::TYPE_PHPNAME, $indexType)];
            $this->contents = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : MailingTableMap::translateFieldName('IsPlaying', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_playing = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : MailingTableMap::translateFieldName('TotalCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->total_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : MailingTableMap::translateFieldName('ErrorCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->error_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : MailingTableMap::translateFieldName('SendCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->send_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : MailingTableMap::translateFieldName('Status', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : MailingTableMap::translateFieldName('CreatedDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 12; // 12 = MailingTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Marketing\\Mailing'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aMailingList !== null && $this->mailing_list_id !== $this->aMailingList->getId()) {
            $this->aMailingList = null;
        }
        if ($this->aMailingTemplate !== null && $this->mailing_template_id !== $this->aMailingTemplate->getId()) {
            $this->aMailingTemplate = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MailingTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildMailingQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aMailingList = null;
            $this->aMailingTemplate = null;
            $this->collMailingMessages = null;

            $this->collMailingProperties = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Mailing::setDeleted()
     * @see Mailing::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MailingTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildMailingQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(MailingTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MailingTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aMailingList !== null) {
                if ($this->aMailingList->isModified() || $this->aMailingList->isNew()) {
                    $affectedRows += $this->aMailingList->save($con);
                }
                $this->setMailingList($this->aMailingList);
            }

            if ($this->aMailingTemplate !== null) {
                if ($this->aMailingTemplate->isModified() || $this->aMailingTemplate->isNew()) {
                    $affectedRows += $this->aMailingTemplate->save($con);
                }
                $this->setMailingTemplate($this->aMailingTemplate);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->mailingMessagesScheduledForDeletion !== null) {
                if (!$this->mailingMessagesScheduledForDeletion->isEmpty()) {
                    \Model\Marketing\MailingMessageQuery::create()
                        ->filterByPrimaryKeys($this->mailingMessagesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->mailingMessagesScheduledForDeletion = null;
                }
            }

            if ($this->collMailingMessages !== null) {
                foreach ($this->collMailingMessages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->mailingPropertiesScheduledForDeletion !== null) {
                if (!$this->mailingPropertiesScheduledForDeletion->isEmpty()) {
                    \Model\Marketing\MailingPropertyQuery::create()
                        ->filterByPrimaryKeys($this->mailingPropertiesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->mailingPropertiesScheduledForDeletion = null;
                }
            }

            if ($this->collMailingProperties !== null) {
                foreach ($this->collMailingProperties as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[MailingTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . MailingTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(MailingTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(MailingTableMap::COL_MAILING_LIST_ID)) {
            $modifiedColumns[':p' . $index++]  = 'mailing_list_id';
        }
        if ($this->isColumnModified(MailingTableMap::COL_MAILING_TEMPLATE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'mailing_template_id';
        }
        if ($this->isColumnModified(MailingTableMap::COL_FROM_ADDRESS)) {
            $modifiedColumns[':p' . $index++]  = 'from_address';
        }
        if ($this->isColumnModified(MailingTableMap::COL_SUBJECT)) {
            $modifiedColumns[':p' . $index++]  = 'subject';
        }
        if ($this->isColumnModified(MailingTableMap::COL_CONTENTS)) {
            $modifiedColumns[':p' . $index++]  = 'contents';
        }
        if ($this->isColumnModified(MailingTableMap::COL_IS_PLAYING)) {
            $modifiedColumns[':p' . $index++]  = 'is_playing';
        }
        if ($this->isColumnModified(MailingTableMap::COL_TOTAL_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'total_count';
        }
        if ($this->isColumnModified(MailingTableMap::COL_ERROR_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'error_count';
        }
        if ($this->isColumnModified(MailingTableMap::COL_SEND_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'send_count';
        }
        if ($this->isColumnModified(MailingTableMap::COL_STATUS)) {
            $modifiedColumns[':p' . $index++]  = 'status';
        }
        if ($this->isColumnModified(MailingTableMap::COL_CREATED_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'created_date';
        }

        $sql = sprintf(
            'INSERT INTO mailing (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'mailing_list_id':
                        $stmt->bindValue($identifier, $this->mailing_list_id, PDO::PARAM_INT);
                        break;
                    case 'mailing_template_id':
                        $stmt->bindValue($identifier, $this->mailing_template_id, PDO::PARAM_INT);
                        break;
                    case 'from_address':
                        $stmt->bindValue($identifier, $this->from_address, PDO::PARAM_STR);
                        break;
                    case 'subject':
                        $stmt->bindValue($identifier, $this->subject, PDO::PARAM_STR);
                        break;
                    case 'contents':
                        $stmt->bindValue($identifier, $this->contents, PDO::PARAM_STR);
                        break;
                    case 'is_playing':
                        $stmt->bindValue($identifier, (int) $this->is_playing, PDO::PARAM_INT);
                        break;
                    case 'total_count':
                        $stmt->bindValue($identifier, $this->total_count, PDO::PARAM_INT);
                        break;
                    case 'error_count':
                        $stmt->bindValue($identifier, $this->error_count, PDO::PARAM_INT);
                        break;
                    case 'send_count':
                        $stmt->bindValue($identifier, $this->send_count, PDO::PARAM_INT);
                        break;
                    case 'status':
                        $stmt->bindValue($identifier, $this->status, PDO::PARAM_STR);
                        break;
                    case 'created_date':
                        $stmt->bindValue($identifier, $this->created_date ? $this->created_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MailingTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getMailingListId();
                break;
            case 2:
                return $this->getMailingTemplateId();
                break;
            case 3:
                return $this->getFromAddress();
                break;
            case 4:
                return $this->getSubject();
                break;
            case 5:
                return $this->getContents();
                break;
            case 6:
                return $this->getIsPlaying();
                break;
            case 7:
                return $this->getTotalCount();
                break;
            case 8:
                return $this->getErrorCount();
                break;
            case 9:
                return $this->getSendCount();
                break;
            case 10:
                return $this->getStatus();
                break;
            case 11:
                return $this->getCreatedDate();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Mailing'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Mailing'][$this->hashCode()] = true;
        $keys = MailingTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getMailingListId(),
            $keys[2] => $this->getMailingTemplateId(),
            $keys[3] => $this->getFromAddress(),
            $keys[4] => $this->getSubject(),
            $keys[5] => $this->getContents(),
            $keys[6] => $this->getIsPlaying(),
            $keys[7] => $this->getTotalCount(),
            $keys[8] => $this->getErrorCount(),
            $keys[9] => $this->getSendCount(),
            $keys[10] => $this->getStatus(),
            $keys[11] => $this->getCreatedDate(),
        );
        if ($result[$keys[11]] instanceof \DateTimeInterface) {
            $result[$keys[11]] = $result[$keys[11]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aMailingList) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'mailingList';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mailing_list';
                        break;
                    default:
                        $key = 'MailingList';
                }

                $result[$key] = $this->aMailingList->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aMailingTemplate) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'mailingTemplate';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mailing_template';
                        break;
                    default:
                        $key = 'MailingTemplate';
                }

                $result[$key] = $this->aMailingTemplate->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collMailingMessages) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'mailingMessages';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mailing_messages';
                        break;
                    default:
                        $key = 'MailingMessages';
                }

                $result[$key] = $this->collMailingMessages->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMailingProperties) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'mailingProperties';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mailing_properties';
                        break;
                    default:
                        $key = 'MailingProperties';
                }

                $result[$key] = $this->collMailingProperties->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Marketing\Mailing
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = MailingTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Marketing\Mailing
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setMailingListId($value);
                break;
            case 2:
                $this->setMailingTemplateId($value);
                break;
            case 3:
                $this->setFromAddress($value);
                break;
            case 4:
                $this->setSubject($value);
                break;
            case 5:
                $this->setContents($value);
                break;
            case 6:
                $this->setIsPlaying($value);
                break;
            case 7:
                $this->setTotalCount($value);
                break;
            case 8:
                $this->setErrorCount($value);
                break;
            case 9:
                $this->setSendCount($value);
                break;
            case 10:
                $this->setStatus($value);
                break;
            case 11:
                $this->setCreatedDate($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = MailingTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setMailingListId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setMailingTemplateId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setFromAddress($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSubject($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setContents($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIsPlaying($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setTotalCount($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setErrorCount($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setSendCount($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setStatus($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setCreatedDate($arr[$keys[11]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Marketing\Mailing The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(MailingTableMap::DATABASE_NAME);

        if ($this->isColumnModified(MailingTableMap::COL_ID)) {
            $criteria->add(MailingTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(MailingTableMap::COL_MAILING_LIST_ID)) {
            $criteria->add(MailingTableMap::COL_MAILING_LIST_ID, $this->mailing_list_id);
        }
        if ($this->isColumnModified(MailingTableMap::COL_MAILING_TEMPLATE_ID)) {
            $criteria->add(MailingTableMap::COL_MAILING_TEMPLATE_ID, $this->mailing_template_id);
        }
        if ($this->isColumnModified(MailingTableMap::COL_FROM_ADDRESS)) {
            $criteria->add(MailingTableMap::COL_FROM_ADDRESS, $this->from_address);
        }
        if ($this->isColumnModified(MailingTableMap::COL_SUBJECT)) {
            $criteria->add(MailingTableMap::COL_SUBJECT, $this->subject);
        }
        if ($this->isColumnModified(MailingTableMap::COL_CONTENTS)) {
            $criteria->add(MailingTableMap::COL_CONTENTS, $this->contents);
        }
        if ($this->isColumnModified(MailingTableMap::COL_IS_PLAYING)) {
            $criteria->add(MailingTableMap::COL_IS_PLAYING, $this->is_playing);
        }
        if ($this->isColumnModified(MailingTableMap::COL_TOTAL_COUNT)) {
            $criteria->add(MailingTableMap::COL_TOTAL_COUNT, $this->total_count);
        }
        if ($this->isColumnModified(MailingTableMap::COL_ERROR_COUNT)) {
            $criteria->add(MailingTableMap::COL_ERROR_COUNT, $this->error_count);
        }
        if ($this->isColumnModified(MailingTableMap::COL_SEND_COUNT)) {
            $criteria->add(MailingTableMap::COL_SEND_COUNT, $this->send_count);
        }
        if ($this->isColumnModified(MailingTableMap::COL_STATUS)) {
            $criteria->add(MailingTableMap::COL_STATUS, $this->status);
        }
        if ($this->isColumnModified(MailingTableMap::COL_CREATED_DATE)) {
            $criteria->add(MailingTableMap::COL_CREATED_DATE, $this->created_date);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildMailingQuery::create();
        $criteria->add(MailingTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Marketing\Mailing (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setMailingListId($this->getMailingListId());
        $copyObj->setMailingTemplateId($this->getMailingTemplateId());
        $copyObj->setFromAddress($this->getFromAddress());
        $copyObj->setSubject($this->getSubject());
        $copyObj->setContents($this->getContents());
        $copyObj->setIsPlaying($this->getIsPlaying());
        $copyObj->setTotalCount($this->getTotalCount());
        $copyObj->setErrorCount($this->getErrorCount());
        $copyObj->setSendCount($this->getSendCount());
        $copyObj->setStatus($this->getStatus());
        $copyObj->setCreatedDate($this->getCreatedDate());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getMailingMessages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMailingMessage($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMailingProperties() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMailingProperty($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Marketing\Mailing Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildMailingList object.
     *
     * @param  ChildMailingList $v
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMailingList(ChildMailingList $v = null)
    {
        if ($v === null) {
            $this->setMailingListId(NULL);
        } else {
            $this->setMailingListId($v->getId());
        }

        $this->aMailingList = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMailingList object, it will not be re-added.
        if ($v !== null) {
            $v->addMailing($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMailingList object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildMailingList The associated ChildMailingList object.
     * @throws PropelException
     */
    public function getMailingList(ConnectionInterface $con = null)
    {
        if ($this->aMailingList === null && ($this->mailing_list_id != 0)) {
            $this->aMailingList = ChildMailingListQuery::create()->findPk($this->mailing_list_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMailingList->addMailings($this);
             */
        }

        return $this->aMailingList;
    }

    /**
     * Declares an association between this object and a ChildMailingTemplate object.
     *
     * @param  ChildMailingTemplate $v
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     * @throws PropelException
     */
    public function setMailingTemplate(ChildMailingTemplate $v = null)
    {
        if ($v === null) {
            $this->setMailingTemplateId(NULL);
        } else {
            $this->setMailingTemplateId($v->getId());
        }

        $this->aMailingTemplate = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildMailingTemplate object, it will not be re-added.
        if ($v !== null) {
            $v->addMailing($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildMailingTemplate object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildMailingTemplate The associated ChildMailingTemplate object.
     * @throws PropelException
     */
    public function getMailingTemplate(ConnectionInterface $con = null)
    {
        if ($this->aMailingTemplate === null && ($this->mailing_template_id != 0)) {
            $this->aMailingTemplate = ChildMailingTemplateQuery::create()->findPk($this->mailing_template_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aMailingTemplate->addMailings($this);
             */
        }

        return $this->aMailingTemplate;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('MailingMessage' === $relationName) {
            $this->initMailingMessages();
            return;
        }
        if ('MailingProperty' === $relationName) {
            $this->initMailingProperties();
            return;
        }
    }

    /**
     * Clears out the collMailingMessages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMailingMessages()
     */
    public function clearMailingMessages()
    {
        $this->collMailingMessages = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMailingMessages collection loaded partially.
     */
    public function resetPartialMailingMessages($v = true)
    {
        $this->collMailingMessagesPartial = $v;
    }

    /**
     * Initializes the collMailingMessages collection.
     *
     * By default this just sets the collMailingMessages collection to an empty array (like clearcollMailingMessages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMailingMessages($overrideExisting = true)
    {
        if (null !== $this->collMailingMessages && !$overrideExisting) {
            return;
        }

        $collectionClassName = MailingMessageTableMap::getTableMap()->getCollectionClassName();

        $this->collMailingMessages = new $collectionClassName;
        $this->collMailingMessages->setModel('\Model\Marketing\MailingMessage');
    }

    /**
     * Gets an array of ChildMailingMessage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMailing is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMailingMessage[] List of ChildMailingMessage objects
     * @throws PropelException
     */
    public function getMailingMessages(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMailingMessagesPartial && !$this->isNew();
        if (null === $this->collMailingMessages || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collMailingMessages) {
                    $this->initMailingMessages();
                } else {
                    $collectionClassName = MailingMessageTableMap::getTableMap()->getCollectionClassName();

                    $collMailingMessages = new $collectionClassName;
                    $collMailingMessages->setModel('\Model\Marketing\MailingMessage');

                    return $collMailingMessages;
                }
            } else {
                $collMailingMessages = ChildMailingMessageQuery::create(null, $criteria)
                    ->filterByMailing($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMailingMessagesPartial && count($collMailingMessages)) {
                        $this->initMailingMessages(false);

                        foreach ($collMailingMessages as $obj) {
                            if (false == $this->collMailingMessages->contains($obj)) {
                                $this->collMailingMessages->append($obj);
                            }
                        }

                        $this->collMailingMessagesPartial = true;
                    }

                    return $collMailingMessages;
                }

                if ($partial && $this->collMailingMessages) {
                    foreach ($this->collMailingMessages as $obj) {
                        if ($obj->isNew()) {
                            $collMailingMessages[] = $obj;
                        }
                    }
                }

                $this->collMailingMessages = $collMailingMessages;
                $this->collMailingMessagesPartial = false;
            }
        }

        return $this->collMailingMessages;
    }

    /**
     * Sets a collection of ChildMailingMessage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $mailingMessages A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMailing The current object (for fluent API support)
     */
    public function setMailingMessages(Collection $mailingMessages, ConnectionInterface $con = null)
    {
        /** @var ChildMailingMessage[] $mailingMessagesToDelete */
        $mailingMessagesToDelete = $this->getMailingMessages(new Criteria(), $con)->diff($mailingMessages);


        $this->mailingMessagesScheduledForDeletion = $mailingMessagesToDelete;

        foreach ($mailingMessagesToDelete as $mailingMessageRemoved) {
            $mailingMessageRemoved->setMailing(null);
        }

        $this->collMailingMessages = null;
        foreach ($mailingMessages as $mailingMessage) {
            $this->addMailingMessage($mailingMessage);
        }

        $this->collMailingMessages = $mailingMessages;
        $this->collMailingMessagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MailingMessage objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MailingMessage objects.
     * @throws PropelException
     */
    public function countMailingMessages(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMailingMessagesPartial && !$this->isNew();
        if (null === $this->collMailingMessages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMailingMessages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMailingMessages());
            }

            $query = ChildMailingMessageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMailing($this)
                ->count($con);
        }

        return count($this->collMailingMessages);
    }

    /**
     * Method called to associate a ChildMailingMessage object to this object
     * through the ChildMailingMessage foreign key attribute.
     *
     * @param  ChildMailingMessage $l ChildMailingMessage
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function addMailingMessage(ChildMailingMessage $l)
    {
        if ($this->collMailingMessages === null) {
            $this->initMailingMessages();
            $this->collMailingMessagesPartial = true;
        }

        if (!$this->collMailingMessages->contains($l)) {
            $this->doAddMailingMessage($l);

            if ($this->mailingMessagesScheduledForDeletion and $this->mailingMessagesScheduledForDeletion->contains($l)) {
                $this->mailingMessagesScheduledForDeletion->remove($this->mailingMessagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMailingMessage $mailingMessage The ChildMailingMessage object to add.
     */
    protected function doAddMailingMessage(ChildMailingMessage $mailingMessage)
    {
        $this->collMailingMessages[]= $mailingMessage;
        $mailingMessage->setMailing($this);
    }

    /**
     * @param  ChildMailingMessage $mailingMessage The ChildMailingMessage object to remove.
     * @return $this|ChildMailing The current object (for fluent API support)
     */
    public function removeMailingMessage(ChildMailingMessage $mailingMessage)
    {
        if ($this->getMailingMessages()->contains($mailingMessage)) {
            $pos = $this->collMailingMessages->search($mailingMessage);
            $this->collMailingMessages->remove($pos);
            if (null === $this->mailingMessagesScheduledForDeletion) {
                $this->mailingMessagesScheduledForDeletion = clone $this->collMailingMessages;
                $this->mailingMessagesScheduledForDeletion->clear();
            }
            $this->mailingMessagesScheduledForDeletion[]= clone $mailingMessage;
            $mailingMessage->setMailing(null);
        }

        return $this;
    }

    /**
     * Clears out the collMailingProperties collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMailingProperties()
     */
    public function clearMailingProperties()
    {
        $this->collMailingProperties = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMailingProperties collection loaded partially.
     */
    public function resetPartialMailingProperties($v = true)
    {
        $this->collMailingPropertiesPartial = $v;
    }

    /**
     * Initializes the collMailingProperties collection.
     *
     * By default this just sets the collMailingProperties collection to an empty array (like clearcollMailingProperties());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMailingProperties($overrideExisting = true)
    {
        if (null !== $this->collMailingProperties && !$overrideExisting) {
            return;
        }

        $collectionClassName = MailingPropertyTableMap::getTableMap()->getCollectionClassName();

        $this->collMailingProperties = new $collectionClassName;
        $this->collMailingProperties->setModel('\Model\Marketing\MailingProperty');
    }

    /**
     * Gets an array of ChildMailingProperty objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildMailing is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildMailingProperty[] List of ChildMailingProperty objects
     * @throws PropelException
     */
    public function getMailingProperties(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMailingPropertiesPartial && !$this->isNew();
        if (null === $this->collMailingProperties || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collMailingProperties) {
                    $this->initMailingProperties();
                } else {
                    $collectionClassName = MailingPropertyTableMap::getTableMap()->getCollectionClassName();

                    $collMailingProperties = new $collectionClassName;
                    $collMailingProperties->setModel('\Model\Marketing\MailingProperty');

                    return $collMailingProperties;
                }
            } else {
                $collMailingProperties = ChildMailingPropertyQuery::create(null, $criteria)
                    ->filterByMailing($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMailingPropertiesPartial && count($collMailingProperties)) {
                        $this->initMailingProperties(false);

                        foreach ($collMailingProperties as $obj) {
                            if (false == $this->collMailingProperties->contains($obj)) {
                                $this->collMailingProperties->append($obj);
                            }
                        }

                        $this->collMailingPropertiesPartial = true;
                    }

                    return $collMailingProperties;
                }

                if ($partial && $this->collMailingProperties) {
                    foreach ($this->collMailingProperties as $obj) {
                        if ($obj->isNew()) {
                            $collMailingProperties[] = $obj;
                        }
                    }
                }

                $this->collMailingProperties = $collMailingProperties;
                $this->collMailingPropertiesPartial = false;
            }
        }

        return $this->collMailingProperties;
    }

    /**
     * Sets a collection of ChildMailingProperty objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $mailingProperties A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildMailing The current object (for fluent API support)
     */
    public function setMailingProperties(Collection $mailingProperties, ConnectionInterface $con = null)
    {
        /** @var ChildMailingProperty[] $mailingPropertiesToDelete */
        $mailingPropertiesToDelete = $this->getMailingProperties(new Criteria(), $con)->diff($mailingProperties);


        $this->mailingPropertiesScheduledForDeletion = $mailingPropertiesToDelete;

        foreach ($mailingPropertiesToDelete as $mailingPropertyRemoved) {
            $mailingPropertyRemoved->setMailing(null);
        }

        $this->collMailingProperties = null;
        foreach ($mailingProperties as $mailingProperty) {
            $this->addMailingProperty($mailingProperty);
        }

        $this->collMailingProperties = $mailingProperties;
        $this->collMailingPropertiesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related MailingProperty objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related MailingProperty objects.
     * @throws PropelException
     */
    public function countMailingProperties(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMailingPropertiesPartial && !$this->isNew();
        if (null === $this->collMailingProperties || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMailingProperties) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMailingProperties());
            }

            $query = ChildMailingPropertyQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByMailing($this)
                ->count($con);
        }

        return count($this->collMailingProperties);
    }

    /**
     * Method called to associate a ChildMailingProperty object to this object
     * through the ChildMailingProperty foreign key attribute.
     *
     * @param  ChildMailingProperty $l ChildMailingProperty
     * @return $this|\Model\Marketing\Mailing The current object (for fluent API support)
     */
    public function addMailingProperty(ChildMailingProperty $l)
    {
        if ($this->collMailingProperties === null) {
            $this->initMailingProperties();
            $this->collMailingPropertiesPartial = true;
        }

        if (!$this->collMailingProperties->contains($l)) {
            $this->doAddMailingProperty($l);

            if ($this->mailingPropertiesScheduledForDeletion and $this->mailingPropertiesScheduledForDeletion->contains($l)) {
                $this->mailingPropertiesScheduledForDeletion->remove($this->mailingPropertiesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildMailingProperty $mailingProperty The ChildMailingProperty object to add.
     */
    protected function doAddMailingProperty(ChildMailingProperty $mailingProperty)
    {
        $this->collMailingProperties[]= $mailingProperty;
        $mailingProperty->setMailing($this);
    }

    /**
     * @param  ChildMailingProperty $mailingProperty The ChildMailingProperty object to remove.
     * @return $this|ChildMailing The current object (for fluent API support)
     */
    public function removeMailingProperty(ChildMailingProperty $mailingProperty)
    {
        if ($this->getMailingProperties()->contains($mailingProperty)) {
            $pos = $this->collMailingProperties->search($mailingProperty);
            $this->collMailingProperties->remove($pos);
            if (null === $this->mailingPropertiesScheduledForDeletion) {
                $this->mailingPropertiesScheduledForDeletion = clone $this->collMailingProperties;
                $this->mailingPropertiesScheduledForDeletion->clear();
            }
            $this->mailingPropertiesScheduledForDeletion[]= clone $mailingProperty;
            $mailingProperty->setMailing(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aMailingList) {
            $this->aMailingList->removeMailing($this);
        }
        if (null !== $this->aMailingTemplate) {
            $this->aMailingTemplate->removeMailing($this);
        }
        $this->id = null;
        $this->mailing_list_id = null;
        $this->mailing_template_id = null;
        $this->from_address = null;
        $this->subject = null;
        $this->contents = null;
        $this->is_playing = null;
        $this->total_count = null;
        $this->error_count = null;
        $this->send_count = null;
        $this->status = null;
        $this->created_date = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collMailingMessages) {
                foreach ($this->collMailingMessages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMailingProperties) {
                foreach ($this->collMailingProperties as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collMailingMessages = null;
        $this->collMailingProperties = null;
        $this->aMailingList = null;
        $this->aMailingTemplate = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MailingTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
