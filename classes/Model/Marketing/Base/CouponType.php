<?php

namespace Model\Marketing\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Marketing\Coupon as ChildCoupon;
use Model\Marketing\CouponQuery as ChildCouponQuery;
use Model\Marketing\CouponType as ChildCouponType;
use Model\Marketing\CouponTypeQuery as ChildCouponTypeQuery;
use Model\Marketing\Map\CouponTableMap;
use Model\Marketing\Map\CouponTypeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'coupon_type' table.
 *
 *
 *
 * @package    propel.generator.Model.Marketing.Base
 */
abstract class CouponType implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Marketing\\Map\\CouponTypeTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the title field.
     *
     * @var        string
     */
    protected $title;

    /**
     * The value for the coupon_type field.
     *
     * @var        int|null
     */
    protected $coupon_type;

    /**
     * The value for the multi_redeem field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $multi_redeem;

    /**
     * The value for the discount_amount field.
     *
     * @var        double|null
     */
    protected $discount_amount;

    /**
     * The value for the discount_percentage field.
     *
     * @var        double|null
     */
    protected $discount_percentage;

    /**
     * The value for the valid_after field.
     *
     * @var        DateTime
     */
    protected $valid_after;

    /**
     * The value for the valid_until field.
     *
     * @var        DateTime
     */
    protected $valid_until;

    /**
     * @var        ObjectCollection|ChildCoupon[] Collection to store aggregation of ChildCoupon objects.
     */
    protected $collCoupons;
    protected $collCouponsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCoupon[]
     */
    protected $couponsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->multi_redeem = false;
    }

    /**
     * Initializes internal state of Model\Marketing\Base\CouponType object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>CouponType</code> instance.  If
     * <code>obj</code> is an instance of <code>CouponType</code>, delegates to
     * <code>equals(CouponType)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [coupon_type] column value.
     *
     * @return string|null
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getCouponType()
    {
        if (null === $this->coupon_type) {
            return null;
        }
        $valueSet = CouponTypeTableMap::getValueSet(CouponTypeTableMap::COL_COUPON_TYPE);
        if (!isset($valueSet[$this->coupon_type])) {
            throw new PropelException('Unknown stored enum key: ' . $this->coupon_type);
        }

        return $valueSet[$this->coupon_type];
    }

    /**
     * Get the [multi_redeem] column value.
     *
     * @return boolean
     */
    public function getMultiRedeem()
    {
        return $this->multi_redeem;
    }

    /**
     * Get the [multi_redeem] column value.
     *
     * @return boolean
     */
    public function isMultiRedeem()
    {
        return $this->getMultiRedeem();
    }

    /**
     * Get the [discount_amount] column value.
     *
     * @return double|null
     */
    public function getDiscountAmount()
    {
        return $this->discount_amount;
    }

    /**
     * Get the [discount_percentage] column value.
     *
     * @return double|null
     */
    public function getDiscountPercentage()
    {
        return $this->discount_percentage;
    }

    /**
     * Get the [optionally formatted] temporal [valid_after] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getValidAfter($format = null)
    {
        if ($format === null) {
            return $this->valid_after;
        } else {
            return $this->valid_after instanceof \DateTimeInterface ? $this->valid_after->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [valid_until] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getValidUntil($format = null)
    {
        if ($format === null) {
            return $this->valid_until;
        } else {
            return $this->valid_until instanceof \DateTimeInterface ? $this->valid_until->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Marketing\CouponType The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[CouponTypeTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [title] column.
     *
     * @param string $v New value
     * @return $this|\Model\Marketing\CouponType The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[CouponTypeTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [coupon_type] column.
     *
     * @param  string|null $v new value
     * @return $this|\Model\Marketing\CouponType The current object (for fluent API support)
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function setCouponType($v)
    {
        if ($v !== null) {
            $valueSet = CouponTypeTableMap::getValueSet(CouponTypeTableMap::COL_COUPON_TYPE);
            if (!in_array($v, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $v));
            }
            $v = array_search($v, $valueSet);
        }

        if ($this->coupon_type !== $v) {
            $this->coupon_type = $v;
            $this->modifiedColumns[CouponTypeTableMap::COL_COUPON_TYPE] = true;
        }

        return $this;
    } // setCouponType()

    /**
     * Sets the value of the [multi_redeem] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Marketing\CouponType The current object (for fluent API support)
     */
    public function setMultiRedeem($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->multi_redeem !== $v) {
            $this->multi_redeem = $v;
            $this->modifiedColumns[CouponTypeTableMap::COL_MULTI_REDEEM] = true;
        }

        return $this;
    } // setMultiRedeem()

    /**
     * Set the value of [discount_amount] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Marketing\CouponType The current object (for fluent API support)
     */
    public function setDiscountAmount($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->discount_amount !== $v) {
            $this->discount_amount = $v;
            $this->modifiedColumns[CouponTypeTableMap::COL_DISCOUNT_AMOUNT] = true;
        }

        return $this;
    } // setDiscountAmount()

    /**
     * Set the value of [discount_percentage] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Marketing\CouponType The current object (for fluent API support)
     */
    public function setDiscountPercentage($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->discount_percentage !== $v) {
            $this->discount_percentage = $v;
            $this->modifiedColumns[CouponTypeTableMap::COL_DISCOUNT_PERCENTAGE] = true;
        }

        return $this;
    } // setDiscountPercentage()

    /**
     * Sets the value of [valid_after] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Marketing\CouponType The current object (for fluent API support)
     */
    public function setValidAfter($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->valid_after !== null || $dt !== null) {
            if ($this->valid_after === null || $dt === null || $dt->format("Y-m-d") !== $this->valid_after->format("Y-m-d")) {
                $this->valid_after = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CouponTypeTableMap::COL_VALID_AFTER] = true;
            }
        } // if either are not null

        return $this;
    } // setValidAfter()

    /**
     * Sets the value of [valid_until] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Marketing\CouponType The current object (for fluent API support)
     */
    public function setValidUntil($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->valid_until !== null || $dt !== null) {
            if ($this->valid_until === null || $dt === null || $dt->format("Y-m-d") !== $this->valid_until->format("Y-m-d")) {
                $this->valid_until = $dt === null ? null : clone $dt;
                $this->modifiedColumns[CouponTypeTableMap::COL_VALID_UNTIL] = true;
            }
        } // if either are not null

        return $this;
    } // setValidUntil()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->multi_redeem !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CouponTypeTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CouponTypeTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CouponTypeTableMap::translateFieldName('CouponType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->coupon_type = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CouponTypeTableMap::translateFieldName('MultiRedeem', TableMap::TYPE_PHPNAME, $indexType)];
            $this->multi_redeem = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CouponTypeTableMap::translateFieldName('DiscountAmount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->discount_amount = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CouponTypeTableMap::translateFieldName('DiscountPercentage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->discount_percentage = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CouponTypeTableMap::translateFieldName('ValidAfter', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->valid_after = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CouponTypeTableMap::translateFieldName('ValidUntil', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->valid_until = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 8; // 8 = CouponTypeTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Marketing\\CouponType'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CouponTypeTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCouponTypeQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collCoupons = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see CouponType::setDeleted()
     * @see CouponType::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CouponTypeTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCouponTypeQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CouponTypeTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CouponTypeTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->couponsScheduledForDeletion !== null) {
                if (!$this->couponsScheduledForDeletion->isEmpty()) {
                    \Model\Marketing\CouponQuery::create()
                        ->filterByPrimaryKeys($this->couponsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->couponsScheduledForDeletion = null;
                }
            }

            if ($this->collCoupons !== null) {
                foreach ($this->collCoupons as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CouponTypeTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CouponTypeTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CouponTypeTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_COUPON_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'coupon_type';
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_MULTI_REDEEM)) {
            $modifiedColumns[':p' . $index++]  = 'multi_redeem';
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_DISCOUNT_AMOUNT)) {
            $modifiedColumns[':p' . $index++]  = 'discount_amount';
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_DISCOUNT_PERCENTAGE)) {
            $modifiedColumns[':p' . $index++]  = 'discount_percentage';
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_VALID_AFTER)) {
            $modifiedColumns[':p' . $index++]  = 'valid_after';
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_VALID_UNTIL)) {
            $modifiedColumns[':p' . $index++]  = 'valid_until';
        }

        $sql = sprintf(
            'INSERT INTO coupon_type (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'coupon_type':
                        $stmt->bindValue($identifier, $this->coupon_type, PDO::PARAM_INT);
                        break;
                    case 'multi_redeem':
                        $stmt->bindValue($identifier, (int) $this->multi_redeem, PDO::PARAM_INT);
                        break;
                    case 'discount_amount':
                        $stmt->bindValue($identifier, $this->discount_amount, PDO::PARAM_STR);
                        break;
                    case 'discount_percentage':
                        $stmt->bindValue($identifier, $this->discount_percentage, PDO::PARAM_STR);
                        break;
                    case 'valid_after':
                        $stmt->bindValue($identifier, $this->valid_after ? $this->valid_after->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'valid_until':
                        $stmt->bindValue($identifier, $this->valid_until ? $this->valid_until->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CouponTypeTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getTitle();
                break;
            case 2:
                return $this->getCouponType();
                break;
            case 3:
                return $this->getMultiRedeem();
                break;
            case 4:
                return $this->getDiscountAmount();
                break;
            case 5:
                return $this->getDiscountPercentage();
                break;
            case 6:
                return $this->getValidAfter();
                break;
            case 7:
                return $this->getValidUntil();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['CouponType'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['CouponType'][$this->hashCode()] = true;
        $keys = CouponTypeTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getTitle(),
            $keys[2] => $this->getCouponType(),
            $keys[3] => $this->getMultiRedeem(),
            $keys[4] => $this->getDiscountAmount(),
            $keys[5] => $this->getDiscountPercentage(),
            $keys[6] => $this->getValidAfter(),
            $keys[7] => $this->getValidUntil(),
        );
        if ($result[$keys[6]] instanceof \DateTimeInterface) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        if ($result[$keys[7]] instanceof \DateTimeInterface) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collCoupons) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'coupons';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'coupons';
                        break;
                    default:
                        $key = 'Coupons';
                }

                $result[$key] = $this->collCoupons->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Marketing\CouponType
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CouponTypeTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Marketing\CouponType
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setTitle($value);
                break;
            case 2:
                $valueSet = CouponTypeTableMap::getValueSet(CouponTypeTableMap::COL_COUPON_TYPE);
                if (isset($valueSet[$value])) {
                    $value = $valueSet[$value];
                }
                $this->setCouponType($value);
                break;
            case 3:
                $this->setMultiRedeem($value);
                break;
            case 4:
                $this->setDiscountAmount($value);
                break;
            case 5:
                $this->setDiscountPercentage($value);
                break;
            case 6:
                $this->setValidAfter($value);
                break;
            case 7:
                $this->setValidUntil($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CouponTypeTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTitle($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCouponType($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setMultiRedeem($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDiscountAmount($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDiscountPercentage($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setValidAfter($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setValidUntil($arr[$keys[7]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Marketing\CouponType The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CouponTypeTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CouponTypeTableMap::COL_ID)) {
            $criteria->add(CouponTypeTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_TITLE)) {
            $criteria->add(CouponTypeTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_COUPON_TYPE)) {
            $criteria->add(CouponTypeTableMap::COL_COUPON_TYPE, $this->coupon_type);
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_MULTI_REDEEM)) {
            $criteria->add(CouponTypeTableMap::COL_MULTI_REDEEM, $this->multi_redeem);
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_DISCOUNT_AMOUNT)) {
            $criteria->add(CouponTypeTableMap::COL_DISCOUNT_AMOUNT, $this->discount_amount);
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_DISCOUNT_PERCENTAGE)) {
            $criteria->add(CouponTypeTableMap::COL_DISCOUNT_PERCENTAGE, $this->discount_percentage);
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_VALID_AFTER)) {
            $criteria->add(CouponTypeTableMap::COL_VALID_AFTER, $this->valid_after);
        }
        if ($this->isColumnModified(CouponTypeTableMap::COL_VALID_UNTIL)) {
            $criteria->add(CouponTypeTableMap::COL_VALID_UNTIL, $this->valid_until);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCouponTypeQuery::create();
        $criteria->add(CouponTypeTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Marketing\CouponType (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTitle($this->getTitle());
        $copyObj->setCouponType($this->getCouponType());
        $copyObj->setMultiRedeem($this->getMultiRedeem());
        $copyObj->setDiscountAmount($this->getDiscountAmount());
        $copyObj->setDiscountPercentage($this->getDiscountPercentage());
        $copyObj->setValidAfter($this->getValidAfter());
        $copyObj->setValidUntil($this->getValidUntil());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCoupons() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCoupon($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Marketing\CouponType Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Coupon' === $relationName) {
            $this->initCoupons();
            return;
        }
    }

    /**
     * Clears out the collCoupons collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCoupons()
     */
    public function clearCoupons()
    {
        $this->collCoupons = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCoupons collection loaded partially.
     */
    public function resetPartialCoupons($v = true)
    {
        $this->collCouponsPartial = $v;
    }

    /**
     * Initializes the collCoupons collection.
     *
     * By default this just sets the collCoupons collection to an empty array (like clearcollCoupons());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCoupons($overrideExisting = true)
    {
        if (null !== $this->collCoupons && !$overrideExisting) {
            return;
        }

        $collectionClassName = CouponTableMap::getTableMap()->getCollectionClassName();

        $this->collCoupons = new $collectionClassName;
        $this->collCoupons->setModel('\Model\Marketing\Coupon');
    }

    /**
     * Gets an array of ChildCoupon objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCouponType is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCoupon[] List of ChildCoupon objects
     * @throws PropelException
     */
    public function getCoupons(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCouponsPartial && !$this->isNew();
        if (null === $this->collCoupons || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCoupons) {
                    $this->initCoupons();
                } else {
                    $collectionClassName = CouponTableMap::getTableMap()->getCollectionClassName();

                    $collCoupons = new $collectionClassName;
                    $collCoupons->setModel('\Model\Marketing\Coupon');

                    return $collCoupons;
                }
            } else {
                $collCoupons = ChildCouponQuery::create(null, $criteria)
                    ->filterByCouponType($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCouponsPartial && count($collCoupons)) {
                        $this->initCoupons(false);

                        foreach ($collCoupons as $obj) {
                            if (false == $this->collCoupons->contains($obj)) {
                                $this->collCoupons->append($obj);
                            }
                        }

                        $this->collCouponsPartial = true;
                    }

                    return $collCoupons;
                }

                if ($partial && $this->collCoupons) {
                    foreach ($this->collCoupons as $obj) {
                        if ($obj->isNew()) {
                            $collCoupons[] = $obj;
                        }
                    }
                }

                $this->collCoupons = $collCoupons;
                $this->collCouponsPartial = false;
            }
        }

        return $this->collCoupons;
    }

    /**
     * Sets a collection of ChildCoupon objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $coupons A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCouponType The current object (for fluent API support)
     */
    public function setCoupons(Collection $coupons, ConnectionInterface $con = null)
    {
        /** @var ChildCoupon[] $couponsToDelete */
        $couponsToDelete = $this->getCoupons(new Criteria(), $con)->diff($coupons);


        $this->couponsScheduledForDeletion = $couponsToDelete;

        foreach ($couponsToDelete as $couponRemoved) {
            $couponRemoved->setCouponType(null);
        }

        $this->collCoupons = null;
        foreach ($coupons as $coupon) {
            $this->addCoupon($coupon);
        }

        $this->collCoupons = $coupons;
        $this->collCouponsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related Coupon objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related Coupon objects.
     * @throws PropelException
     */
    public function countCoupons(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCouponsPartial && !$this->isNew();
        if (null === $this->collCoupons || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCoupons) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCoupons());
            }

            $query = ChildCouponQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCouponType($this)
                ->count($con);
        }

        return count($this->collCoupons);
    }

    /**
     * Method called to associate a ChildCoupon object to this object
     * through the ChildCoupon foreign key attribute.
     *
     * @param  ChildCoupon $l ChildCoupon
     * @return $this|\Model\Marketing\CouponType The current object (for fluent API support)
     */
    public function addCoupon(ChildCoupon $l)
    {
        if ($this->collCoupons === null) {
            $this->initCoupons();
            $this->collCouponsPartial = true;
        }

        if (!$this->collCoupons->contains($l)) {
            $this->doAddCoupon($l);

            if ($this->couponsScheduledForDeletion and $this->couponsScheduledForDeletion->contains($l)) {
                $this->couponsScheduledForDeletion->remove($this->couponsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCoupon $coupon The ChildCoupon object to add.
     */
    protected function doAddCoupon(ChildCoupon $coupon)
    {
        $this->collCoupons[]= $coupon;
        $coupon->setCouponType($this);
    }

    /**
     * @param  ChildCoupon $coupon The ChildCoupon object to remove.
     * @return $this|ChildCouponType The current object (for fluent API support)
     */
    public function removeCoupon(ChildCoupon $coupon)
    {
        if ($this->getCoupons()->contains($coupon)) {
            $pos = $this->collCoupons->search($coupon);
            $this->collCoupons->remove($pos);
            if (null === $this->couponsScheduledForDeletion) {
                $this->couponsScheduledForDeletion = clone $this->collCoupons;
                $this->couponsScheduledForDeletion->clear();
            }
            $this->couponsScheduledForDeletion[]= clone $coupon;
            $coupon->setCouponType(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->title = null;
        $this->coupon_type = null;
        $this->multi_redeem = null;
        $this->discount_amount = null;
        $this->discount_percentage = null;
        $this->valid_after = null;
        $this->valid_until = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCoupons) {
                foreach ($this->collCoupons as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCoupons = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CouponTypeTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
