<?php

namespace Model\Marketing\Base;

use \Exception;
use \PDO;
use Model\Marketing\Mailing as ChildMailing;
use Model\Marketing\MailingQuery as ChildMailingQuery;
use Model\Marketing\Map\MailingTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mailing' table.
 *
 *
 *
 * @method     ChildMailingQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildMailingQuery orderByMailingListId($order = Criteria::ASC) Order by the mailing_list_id column
 * @method     ChildMailingQuery orderByMailingTemplateId($order = Criteria::ASC) Order by the mailing_template_id column
 * @method     ChildMailingQuery orderByFromAddress($order = Criteria::ASC) Order by the from_address column
 * @method     ChildMailingQuery orderBySubject($order = Criteria::ASC) Order by the subject column
 * @method     ChildMailingQuery orderByContents($order = Criteria::ASC) Order by the contents column
 * @method     ChildMailingQuery orderByIsPlaying($order = Criteria::ASC) Order by the is_playing column
 * @method     ChildMailingQuery orderByTotalCount($order = Criteria::ASC) Order by the total_count column
 * @method     ChildMailingQuery orderByErrorCount($order = Criteria::ASC) Order by the error_count column
 * @method     ChildMailingQuery orderBySendCount($order = Criteria::ASC) Order by the send_count column
 * @method     ChildMailingQuery orderByStatus($order = Criteria::ASC) Order by the status column
 * @method     ChildMailingQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 *
 * @method     ChildMailingQuery groupById() Group by the id column
 * @method     ChildMailingQuery groupByMailingListId() Group by the mailing_list_id column
 * @method     ChildMailingQuery groupByMailingTemplateId() Group by the mailing_template_id column
 * @method     ChildMailingQuery groupByFromAddress() Group by the from_address column
 * @method     ChildMailingQuery groupBySubject() Group by the subject column
 * @method     ChildMailingQuery groupByContents() Group by the contents column
 * @method     ChildMailingQuery groupByIsPlaying() Group by the is_playing column
 * @method     ChildMailingQuery groupByTotalCount() Group by the total_count column
 * @method     ChildMailingQuery groupByErrorCount() Group by the error_count column
 * @method     ChildMailingQuery groupBySendCount() Group by the send_count column
 * @method     ChildMailingQuery groupByStatus() Group by the status column
 * @method     ChildMailingQuery groupByCreatedDate() Group by the created_date column
 *
 * @method     ChildMailingQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMailingQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMailingQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMailingQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMailingQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMailingQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMailingQuery leftJoinMailingList($relationAlias = null) Adds a LEFT JOIN clause to the query using the MailingList relation
 * @method     ChildMailingQuery rightJoinMailingList($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MailingList relation
 * @method     ChildMailingQuery innerJoinMailingList($relationAlias = null) Adds a INNER JOIN clause to the query using the MailingList relation
 *
 * @method     ChildMailingQuery joinWithMailingList($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MailingList relation
 *
 * @method     ChildMailingQuery leftJoinWithMailingList() Adds a LEFT JOIN clause and with to the query using the MailingList relation
 * @method     ChildMailingQuery rightJoinWithMailingList() Adds a RIGHT JOIN clause and with to the query using the MailingList relation
 * @method     ChildMailingQuery innerJoinWithMailingList() Adds a INNER JOIN clause and with to the query using the MailingList relation
 *
 * @method     ChildMailingQuery leftJoinMailingTemplate($relationAlias = null) Adds a LEFT JOIN clause to the query using the MailingTemplate relation
 * @method     ChildMailingQuery rightJoinMailingTemplate($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MailingTemplate relation
 * @method     ChildMailingQuery innerJoinMailingTemplate($relationAlias = null) Adds a INNER JOIN clause to the query using the MailingTemplate relation
 *
 * @method     ChildMailingQuery joinWithMailingTemplate($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MailingTemplate relation
 *
 * @method     ChildMailingQuery leftJoinWithMailingTemplate() Adds a LEFT JOIN clause and with to the query using the MailingTemplate relation
 * @method     ChildMailingQuery rightJoinWithMailingTemplate() Adds a RIGHT JOIN clause and with to the query using the MailingTemplate relation
 * @method     ChildMailingQuery innerJoinWithMailingTemplate() Adds a INNER JOIN clause and with to the query using the MailingTemplate relation
 *
 * @method     ChildMailingQuery leftJoinMailingMessage($relationAlias = null) Adds a LEFT JOIN clause to the query using the MailingMessage relation
 * @method     ChildMailingQuery rightJoinMailingMessage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MailingMessage relation
 * @method     ChildMailingQuery innerJoinMailingMessage($relationAlias = null) Adds a INNER JOIN clause to the query using the MailingMessage relation
 *
 * @method     ChildMailingQuery joinWithMailingMessage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MailingMessage relation
 *
 * @method     ChildMailingQuery leftJoinWithMailingMessage() Adds a LEFT JOIN clause and with to the query using the MailingMessage relation
 * @method     ChildMailingQuery rightJoinWithMailingMessage() Adds a RIGHT JOIN clause and with to the query using the MailingMessage relation
 * @method     ChildMailingQuery innerJoinWithMailingMessage() Adds a INNER JOIN clause and with to the query using the MailingMessage relation
 *
 * @method     ChildMailingQuery leftJoinMailingProperty($relationAlias = null) Adds a LEFT JOIN clause to the query using the MailingProperty relation
 * @method     ChildMailingQuery rightJoinMailingProperty($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MailingProperty relation
 * @method     ChildMailingQuery innerJoinMailingProperty($relationAlias = null) Adds a INNER JOIN clause to the query using the MailingProperty relation
 *
 * @method     ChildMailingQuery joinWithMailingProperty($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MailingProperty relation
 *
 * @method     ChildMailingQuery leftJoinWithMailingProperty() Adds a LEFT JOIN clause and with to the query using the MailingProperty relation
 * @method     ChildMailingQuery rightJoinWithMailingProperty() Adds a RIGHT JOIN clause and with to the query using the MailingProperty relation
 * @method     ChildMailingQuery innerJoinWithMailingProperty() Adds a INNER JOIN clause and with to the query using the MailingProperty relation
 *
 * @method     \Model\Marketing\MailingListQuery|\Model\Marketing\MailingTemplateQuery|\Model\Marketing\MailingMessageQuery|\Model\Marketing\MailingPropertyQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMailing findOne(ConnectionInterface $con = null) Return the first ChildMailing matching the query
 * @method     ChildMailing findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMailing matching the query, or a new ChildMailing object populated from the query conditions when no match is found
 *
 * @method     ChildMailing findOneById(int $id) Return the first ChildMailing filtered by the id column
 * @method     ChildMailing findOneByMailingListId(int $mailing_list_id) Return the first ChildMailing filtered by the mailing_list_id column
 * @method     ChildMailing findOneByMailingTemplateId(int $mailing_template_id) Return the first ChildMailing filtered by the mailing_template_id column
 * @method     ChildMailing findOneByFromAddress(string $from_address) Return the first ChildMailing filtered by the from_address column
 * @method     ChildMailing findOneBySubject(string $subject) Return the first ChildMailing filtered by the subject column
 * @method     ChildMailing findOneByContents(string $contents) Return the first ChildMailing filtered by the contents column
 * @method     ChildMailing findOneByIsPlaying(boolean $is_playing) Return the first ChildMailing filtered by the is_playing column
 * @method     ChildMailing findOneByTotalCount(int $total_count) Return the first ChildMailing filtered by the total_count column
 * @method     ChildMailing findOneByErrorCount(int $error_count) Return the first ChildMailing filtered by the error_count column
 * @method     ChildMailing findOneBySendCount(int $send_count) Return the first ChildMailing filtered by the send_count column
 * @method     ChildMailing findOneByStatus(string $status) Return the first ChildMailing filtered by the status column
 * @method     ChildMailing findOneByCreatedDate(string $created_date) Return the first ChildMailing filtered by the created_date column *

 * @method     ChildMailing requirePk($key, ConnectionInterface $con = null) Return the ChildMailing by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailing requireOne(ConnectionInterface $con = null) Return the first ChildMailing matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMailing requireOneById(int $id) Return the first ChildMailing filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailing requireOneByMailingListId(int $mailing_list_id) Return the first ChildMailing filtered by the mailing_list_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailing requireOneByMailingTemplateId(int $mailing_template_id) Return the first ChildMailing filtered by the mailing_template_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailing requireOneByFromAddress(string $from_address) Return the first ChildMailing filtered by the from_address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailing requireOneBySubject(string $subject) Return the first ChildMailing filtered by the subject column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailing requireOneByContents(string $contents) Return the first ChildMailing filtered by the contents column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailing requireOneByIsPlaying(boolean $is_playing) Return the first ChildMailing filtered by the is_playing column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailing requireOneByTotalCount(int $total_count) Return the first ChildMailing filtered by the total_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailing requireOneByErrorCount(int $error_count) Return the first ChildMailing filtered by the error_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailing requireOneBySendCount(int $send_count) Return the first ChildMailing filtered by the send_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailing requireOneByStatus(string $status) Return the first ChildMailing filtered by the status column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailing requireOneByCreatedDate(string $created_date) Return the first ChildMailing filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMailing[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMailing objects based on current ModelCriteria
 * @method     ChildMailing[]|ObjectCollection findById(int $id) Return ChildMailing objects filtered by the id column
 * @method     ChildMailing[]|ObjectCollection findByMailingListId(int $mailing_list_id) Return ChildMailing objects filtered by the mailing_list_id column
 * @method     ChildMailing[]|ObjectCollection findByMailingTemplateId(int $mailing_template_id) Return ChildMailing objects filtered by the mailing_template_id column
 * @method     ChildMailing[]|ObjectCollection findByFromAddress(string $from_address) Return ChildMailing objects filtered by the from_address column
 * @method     ChildMailing[]|ObjectCollection findBySubject(string $subject) Return ChildMailing objects filtered by the subject column
 * @method     ChildMailing[]|ObjectCollection findByContents(string $contents) Return ChildMailing objects filtered by the contents column
 * @method     ChildMailing[]|ObjectCollection findByIsPlaying(boolean $is_playing) Return ChildMailing objects filtered by the is_playing column
 * @method     ChildMailing[]|ObjectCollection findByTotalCount(int $total_count) Return ChildMailing objects filtered by the total_count column
 * @method     ChildMailing[]|ObjectCollection findByErrorCount(int $error_count) Return ChildMailing objects filtered by the error_count column
 * @method     ChildMailing[]|ObjectCollection findBySendCount(int $send_count) Return ChildMailing objects filtered by the send_count column
 * @method     ChildMailing[]|ObjectCollection findByStatus(string $status) Return ChildMailing objects filtered by the status column
 * @method     ChildMailing[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildMailing objects filtered by the created_date column
 * @method     ChildMailing[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MailingQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Marketing\Base\MailingQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Marketing\\Mailing', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMailingQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMailingQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMailingQuery) {
            return $criteria;
        }
        $query = new ChildMailingQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMailing|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MailingTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MailingTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMailing A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, mailing_list_id, mailing_template_id, from_address, subject, contents, is_playing, total_count, error_count, send_count, status, created_date FROM mailing WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMailing $obj */
            $obj = new ChildMailing();
            $obj->hydrate($row);
            MailingTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMailing|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MailingTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MailingTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MailingTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MailingTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the mailing_list_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMailingListId(1234); // WHERE mailing_list_id = 1234
     * $query->filterByMailingListId(array(12, 34)); // WHERE mailing_list_id IN (12, 34)
     * $query->filterByMailingListId(array('min' => 12)); // WHERE mailing_list_id > 12
     * </code>
     *
     * @see       filterByMailingList()
     *
     * @param     mixed $mailingListId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterByMailingListId($mailingListId = null, $comparison = null)
    {
        if (is_array($mailingListId)) {
            $useMinMax = false;
            if (isset($mailingListId['min'])) {
                $this->addUsingAlias(MailingTableMap::COL_MAILING_LIST_ID, $mailingListId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mailingListId['max'])) {
                $this->addUsingAlias(MailingTableMap::COL_MAILING_LIST_ID, $mailingListId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingTableMap::COL_MAILING_LIST_ID, $mailingListId, $comparison);
    }

    /**
     * Filter the query on the mailing_template_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMailingTemplateId(1234); // WHERE mailing_template_id = 1234
     * $query->filterByMailingTemplateId(array(12, 34)); // WHERE mailing_template_id IN (12, 34)
     * $query->filterByMailingTemplateId(array('min' => 12)); // WHERE mailing_template_id > 12
     * </code>
     *
     * @see       filterByMailingTemplate()
     *
     * @param     mixed $mailingTemplateId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterByMailingTemplateId($mailingTemplateId = null, $comparison = null)
    {
        if (is_array($mailingTemplateId)) {
            $useMinMax = false;
            if (isset($mailingTemplateId['min'])) {
                $this->addUsingAlias(MailingTableMap::COL_MAILING_TEMPLATE_ID, $mailingTemplateId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mailingTemplateId['max'])) {
                $this->addUsingAlias(MailingTableMap::COL_MAILING_TEMPLATE_ID, $mailingTemplateId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingTableMap::COL_MAILING_TEMPLATE_ID, $mailingTemplateId, $comparison);
    }

    /**
     * Filter the query on the from_address column
     *
     * Example usage:
     * <code>
     * $query->filterByFromAddress('fooValue');   // WHERE from_address = 'fooValue'
     * $query->filterByFromAddress('%fooValue%', Criteria::LIKE); // WHERE from_address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fromAddress The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterByFromAddress($fromAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fromAddress)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingTableMap::COL_FROM_ADDRESS, $fromAddress, $comparison);
    }

    /**
     * Filter the query on the subject column
     *
     * Example usage:
     * <code>
     * $query->filterBySubject('fooValue');   // WHERE subject = 'fooValue'
     * $query->filterBySubject('%fooValue%', Criteria::LIKE); // WHERE subject LIKE '%fooValue%'
     * </code>
     *
     * @param     string $subject The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterBySubject($subject = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($subject)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingTableMap::COL_SUBJECT, $subject, $comparison);
    }

    /**
     * Filter the query on the contents column
     *
     * Example usage:
     * <code>
     * $query->filterByContents('fooValue');   // WHERE contents = 'fooValue'
     * $query->filterByContents('%fooValue%', Criteria::LIKE); // WHERE contents LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contents The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterByContents($contents = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contents)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingTableMap::COL_CONTENTS, $contents, $comparison);
    }

    /**
     * Filter the query on the is_playing column
     *
     * Example usage:
     * <code>
     * $query->filterByIsPlaying(true); // WHERE is_playing = true
     * $query->filterByIsPlaying('yes'); // WHERE is_playing = true
     * </code>
     *
     * @param     boolean|string $isPlaying The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterByIsPlaying($isPlaying = null, $comparison = null)
    {
        if (is_string($isPlaying)) {
            $isPlaying = in_array(strtolower($isPlaying), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(MailingTableMap::COL_IS_PLAYING, $isPlaying, $comparison);
    }

    /**
     * Filter the query on the total_count column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalCount(1234); // WHERE total_count = 1234
     * $query->filterByTotalCount(array(12, 34)); // WHERE total_count IN (12, 34)
     * $query->filterByTotalCount(array('min' => 12)); // WHERE total_count > 12
     * </code>
     *
     * @param     mixed $totalCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterByTotalCount($totalCount = null, $comparison = null)
    {
        if (is_array($totalCount)) {
            $useMinMax = false;
            if (isset($totalCount['min'])) {
                $this->addUsingAlias(MailingTableMap::COL_TOTAL_COUNT, $totalCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalCount['max'])) {
                $this->addUsingAlias(MailingTableMap::COL_TOTAL_COUNT, $totalCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingTableMap::COL_TOTAL_COUNT, $totalCount, $comparison);
    }

    /**
     * Filter the query on the error_count column
     *
     * Example usage:
     * <code>
     * $query->filterByErrorCount(1234); // WHERE error_count = 1234
     * $query->filterByErrorCount(array(12, 34)); // WHERE error_count IN (12, 34)
     * $query->filterByErrorCount(array('min' => 12)); // WHERE error_count > 12
     * </code>
     *
     * @param     mixed $errorCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterByErrorCount($errorCount = null, $comparison = null)
    {
        if (is_array($errorCount)) {
            $useMinMax = false;
            if (isset($errorCount['min'])) {
                $this->addUsingAlias(MailingTableMap::COL_ERROR_COUNT, $errorCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($errorCount['max'])) {
                $this->addUsingAlias(MailingTableMap::COL_ERROR_COUNT, $errorCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingTableMap::COL_ERROR_COUNT, $errorCount, $comparison);
    }

    /**
     * Filter the query on the send_count column
     *
     * Example usage:
     * <code>
     * $query->filterBySendCount(1234); // WHERE send_count = 1234
     * $query->filterBySendCount(array(12, 34)); // WHERE send_count IN (12, 34)
     * $query->filterBySendCount(array('min' => 12)); // WHERE send_count > 12
     * </code>
     *
     * @param     mixed $sendCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterBySendCount($sendCount = null, $comparison = null)
    {
        if (is_array($sendCount)) {
            $useMinMax = false;
            if (isset($sendCount['min'])) {
                $this->addUsingAlias(MailingTableMap::COL_SEND_COUNT, $sendCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sendCount['max'])) {
                $this->addUsingAlias(MailingTableMap::COL_SEND_COUNT, $sendCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingTableMap::COL_SEND_COUNT, $sendCount, $comparison);
    }

    /**
     * Filter the query on the status column
     *
     * Example usage:
     * <code>
     * $query->filterByStatus('fooValue');   // WHERE status = 'fooValue'
     * $query->filterByStatus('%fooValue%', Criteria::LIKE); // WHERE status LIKE '%fooValue%'
     * </code>
     *
     * @param     string $status The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterByStatus($status = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($status)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingTableMap::COL_STATUS, $status, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(MailingTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(MailingTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query by a related \Model\Marketing\MailingList object
     *
     * @param \Model\Marketing\MailingList|ObjectCollection $mailingList The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMailingQuery The current query, for fluid interface
     */
    public function filterByMailingList($mailingList, $comparison = null)
    {
        if ($mailingList instanceof \Model\Marketing\MailingList) {
            return $this
                ->addUsingAlias(MailingTableMap::COL_MAILING_LIST_ID, $mailingList->getId(), $comparison);
        } elseif ($mailingList instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MailingTableMap::COL_MAILING_LIST_ID, $mailingList->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMailingList() only accepts arguments of type \Model\Marketing\MailingList or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MailingList relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function joinMailingList($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MailingList');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MailingList');
        }

        return $this;
    }

    /**
     * Use the MailingList relation MailingList object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Marketing\MailingListQuery A secondary query class using the current class as primary query
     */
    public function useMailingListQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMailingList($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MailingList', '\Model\Marketing\MailingListQuery');
    }

    /**
     * Filter the query by a related \Model\Marketing\MailingTemplate object
     *
     * @param \Model\Marketing\MailingTemplate|ObjectCollection $mailingTemplate The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMailingQuery The current query, for fluid interface
     */
    public function filterByMailingTemplate($mailingTemplate, $comparison = null)
    {
        if ($mailingTemplate instanceof \Model\Marketing\MailingTemplate) {
            return $this
                ->addUsingAlias(MailingTableMap::COL_MAILING_TEMPLATE_ID, $mailingTemplate->getId(), $comparison);
        } elseif ($mailingTemplate instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MailingTableMap::COL_MAILING_TEMPLATE_ID, $mailingTemplate->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMailingTemplate() only accepts arguments of type \Model\Marketing\MailingTemplate or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MailingTemplate relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function joinMailingTemplate($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MailingTemplate');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MailingTemplate');
        }

        return $this;
    }

    /**
     * Use the MailingTemplate relation MailingTemplate object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Marketing\MailingTemplateQuery A secondary query class using the current class as primary query
     */
    public function useMailingTemplateQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMailingTemplate($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MailingTemplate', '\Model\Marketing\MailingTemplateQuery');
    }

    /**
     * Filter the query by a related \Model\Marketing\MailingMessage object
     *
     * @param \Model\Marketing\MailingMessage|ObjectCollection $mailingMessage the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMailingQuery The current query, for fluid interface
     */
    public function filterByMailingMessage($mailingMessage, $comparison = null)
    {
        if ($mailingMessage instanceof \Model\Marketing\MailingMessage) {
            return $this
                ->addUsingAlias(MailingTableMap::COL_ID, $mailingMessage->getMailingId(), $comparison);
        } elseif ($mailingMessage instanceof ObjectCollection) {
            return $this
                ->useMailingMessageQuery()
                ->filterByPrimaryKeys($mailingMessage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMailingMessage() only accepts arguments of type \Model\Marketing\MailingMessage or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MailingMessage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function joinMailingMessage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MailingMessage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MailingMessage');
        }

        return $this;
    }

    /**
     * Use the MailingMessage relation MailingMessage object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Marketing\MailingMessageQuery A secondary query class using the current class as primary query
     */
    public function useMailingMessageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMailingMessage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MailingMessage', '\Model\Marketing\MailingMessageQuery');
    }

    /**
     * Filter the query by a related \Model\Marketing\MailingProperty object
     *
     * @param \Model\Marketing\MailingProperty|ObjectCollection $mailingProperty the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildMailingQuery The current query, for fluid interface
     */
    public function filterByMailingProperty($mailingProperty, $comparison = null)
    {
        if ($mailingProperty instanceof \Model\Marketing\MailingProperty) {
            return $this
                ->addUsingAlias(MailingTableMap::COL_ID, $mailingProperty->getMailingId(), $comparison);
        } elseif ($mailingProperty instanceof ObjectCollection) {
            return $this
                ->useMailingPropertyQuery()
                ->filterByPrimaryKeys($mailingProperty->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMailingProperty() only accepts arguments of type \Model\Marketing\MailingProperty or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MailingProperty relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function joinMailingProperty($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MailingProperty');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MailingProperty');
        }

        return $this;
    }

    /**
     * Use the MailingProperty relation MailingProperty object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Marketing\MailingPropertyQuery A secondary query class using the current class as primary query
     */
    public function useMailingPropertyQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMailingProperty($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MailingProperty', '\Model\Marketing\MailingPropertyQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMailing $mailing Object to remove from the list of results
     *
     * @return $this|ChildMailingQuery The current query, for fluid interface
     */
    public function prune($mailing = null)
    {
        if ($mailing) {
            $this->addUsingAlias(MailingTableMap::COL_ID, $mailing->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mailing table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MailingTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MailingTableMap::clearInstancePool();
            MailingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MailingTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MailingTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MailingTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MailingTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MailingQuery
