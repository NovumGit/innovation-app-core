<?php

namespace Model\Marketing\Base;

use \Exception;
use \PDO;
use Model\Marketing\CouponType as ChildCouponType;
use Model\Marketing\CouponTypeQuery as ChildCouponTypeQuery;
use Model\Marketing\Map\CouponTypeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'coupon_type' table.
 *
 *
 *
 * @method     ChildCouponTypeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCouponTypeQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildCouponTypeQuery orderByCouponType($order = Criteria::ASC) Order by the coupon_type column
 * @method     ChildCouponTypeQuery orderByMultiRedeem($order = Criteria::ASC) Order by the multi_redeem column
 * @method     ChildCouponTypeQuery orderByDiscountAmount($order = Criteria::ASC) Order by the discount_amount column
 * @method     ChildCouponTypeQuery orderByDiscountPercentage($order = Criteria::ASC) Order by the discount_percentage column
 * @method     ChildCouponTypeQuery orderByValidAfter($order = Criteria::ASC) Order by the valid_after column
 * @method     ChildCouponTypeQuery orderByValidUntil($order = Criteria::ASC) Order by the valid_until column
 *
 * @method     ChildCouponTypeQuery groupById() Group by the id column
 * @method     ChildCouponTypeQuery groupByTitle() Group by the title column
 * @method     ChildCouponTypeQuery groupByCouponType() Group by the coupon_type column
 * @method     ChildCouponTypeQuery groupByMultiRedeem() Group by the multi_redeem column
 * @method     ChildCouponTypeQuery groupByDiscountAmount() Group by the discount_amount column
 * @method     ChildCouponTypeQuery groupByDiscountPercentage() Group by the discount_percentage column
 * @method     ChildCouponTypeQuery groupByValidAfter() Group by the valid_after column
 * @method     ChildCouponTypeQuery groupByValidUntil() Group by the valid_until column
 *
 * @method     ChildCouponTypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCouponTypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCouponTypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCouponTypeQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCouponTypeQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCouponTypeQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCouponTypeQuery leftJoinCoupon($relationAlias = null) Adds a LEFT JOIN clause to the query using the Coupon relation
 * @method     ChildCouponTypeQuery rightJoinCoupon($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Coupon relation
 * @method     ChildCouponTypeQuery innerJoinCoupon($relationAlias = null) Adds a INNER JOIN clause to the query using the Coupon relation
 *
 * @method     ChildCouponTypeQuery joinWithCoupon($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Coupon relation
 *
 * @method     ChildCouponTypeQuery leftJoinWithCoupon() Adds a LEFT JOIN clause and with to the query using the Coupon relation
 * @method     ChildCouponTypeQuery rightJoinWithCoupon() Adds a RIGHT JOIN clause and with to the query using the Coupon relation
 * @method     ChildCouponTypeQuery innerJoinWithCoupon() Adds a INNER JOIN clause and with to the query using the Coupon relation
 *
 * @method     \Model\Marketing\CouponQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCouponType findOne(ConnectionInterface $con = null) Return the first ChildCouponType matching the query
 * @method     ChildCouponType findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCouponType matching the query, or a new ChildCouponType object populated from the query conditions when no match is found
 *
 * @method     ChildCouponType findOneById(int $id) Return the first ChildCouponType filtered by the id column
 * @method     ChildCouponType findOneByTitle(string $title) Return the first ChildCouponType filtered by the title column
 * @method     ChildCouponType findOneByCouponType(int $coupon_type) Return the first ChildCouponType filtered by the coupon_type column
 * @method     ChildCouponType findOneByMultiRedeem(boolean $multi_redeem) Return the first ChildCouponType filtered by the multi_redeem column
 * @method     ChildCouponType findOneByDiscountAmount(double $discount_amount) Return the first ChildCouponType filtered by the discount_amount column
 * @method     ChildCouponType findOneByDiscountPercentage(double $discount_percentage) Return the first ChildCouponType filtered by the discount_percentage column
 * @method     ChildCouponType findOneByValidAfter(string $valid_after) Return the first ChildCouponType filtered by the valid_after column
 * @method     ChildCouponType findOneByValidUntil(string $valid_until) Return the first ChildCouponType filtered by the valid_until column *

 * @method     ChildCouponType requirePk($key, ConnectionInterface $con = null) Return the ChildCouponType by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCouponType requireOne(ConnectionInterface $con = null) Return the first ChildCouponType matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCouponType requireOneById(int $id) Return the first ChildCouponType filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCouponType requireOneByTitle(string $title) Return the first ChildCouponType filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCouponType requireOneByCouponType(int $coupon_type) Return the first ChildCouponType filtered by the coupon_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCouponType requireOneByMultiRedeem(boolean $multi_redeem) Return the first ChildCouponType filtered by the multi_redeem column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCouponType requireOneByDiscountAmount(double $discount_amount) Return the first ChildCouponType filtered by the discount_amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCouponType requireOneByDiscountPercentage(double $discount_percentage) Return the first ChildCouponType filtered by the discount_percentage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCouponType requireOneByValidAfter(string $valid_after) Return the first ChildCouponType filtered by the valid_after column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCouponType requireOneByValidUntil(string $valid_until) Return the first ChildCouponType filtered by the valid_until column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCouponType[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCouponType objects based on current ModelCriteria
 * @method     ChildCouponType[]|ObjectCollection findById(int $id) Return ChildCouponType objects filtered by the id column
 * @method     ChildCouponType[]|ObjectCollection findByTitle(string $title) Return ChildCouponType objects filtered by the title column
 * @method     ChildCouponType[]|ObjectCollection findByCouponType(int $coupon_type) Return ChildCouponType objects filtered by the coupon_type column
 * @method     ChildCouponType[]|ObjectCollection findByMultiRedeem(boolean $multi_redeem) Return ChildCouponType objects filtered by the multi_redeem column
 * @method     ChildCouponType[]|ObjectCollection findByDiscountAmount(double $discount_amount) Return ChildCouponType objects filtered by the discount_amount column
 * @method     ChildCouponType[]|ObjectCollection findByDiscountPercentage(double $discount_percentage) Return ChildCouponType objects filtered by the discount_percentage column
 * @method     ChildCouponType[]|ObjectCollection findByValidAfter(string $valid_after) Return ChildCouponType objects filtered by the valid_after column
 * @method     ChildCouponType[]|ObjectCollection findByValidUntil(string $valid_until) Return ChildCouponType objects filtered by the valid_until column
 * @method     ChildCouponType[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CouponTypeQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Marketing\Base\CouponTypeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Marketing\\CouponType', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCouponTypeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCouponTypeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCouponTypeQuery) {
            return $criteria;
        }
        $query = new ChildCouponTypeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCouponType|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CouponTypeTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CouponTypeTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCouponType A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, title, coupon_type, multi_redeem, discount_amount, discount_percentage, valid_after, valid_until FROM coupon_type WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCouponType $obj */
            $obj = new ChildCouponType();
            $obj->hydrate($row);
            CouponTypeTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCouponType|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCouponTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CouponTypeTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCouponTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CouponTypeTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCouponTypeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CouponTypeTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CouponTypeTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CouponTypeTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCouponTypeQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CouponTypeTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the coupon_type column
     *
     * @param     mixed $couponType The value to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCouponTypeQuery The current query, for fluid interface
     */
    public function filterByCouponType($couponType = null, $comparison = null)
    {
        $valueSet = CouponTypeTableMap::getValueSet(CouponTypeTableMap::COL_COUPON_TYPE);
        if (is_scalar($couponType)) {
            if (!in_array($couponType, $valueSet)) {
                throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $couponType));
            }
            $couponType = array_search($couponType, $valueSet);
        } elseif (is_array($couponType)) {
            $convertedValues = array();
            foreach ($couponType as $value) {
                if (!in_array($value, $valueSet)) {
                    throw new PropelException(sprintf('Value "%s" is not accepted in this enumerated column', $value));
                }
                $convertedValues []= array_search($value, $valueSet);
            }
            $couponType = $convertedValues;
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CouponTypeTableMap::COL_COUPON_TYPE, $couponType, $comparison);
    }

    /**
     * Filter the query on the multi_redeem column
     *
     * Example usage:
     * <code>
     * $query->filterByMultiRedeem(true); // WHERE multi_redeem = true
     * $query->filterByMultiRedeem('yes'); // WHERE multi_redeem = true
     * </code>
     *
     * @param     boolean|string $multiRedeem The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCouponTypeQuery The current query, for fluid interface
     */
    public function filterByMultiRedeem($multiRedeem = null, $comparison = null)
    {
        if (is_string($multiRedeem)) {
            $multiRedeem = in_array(strtolower($multiRedeem), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CouponTypeTableMap::COL_MULTI_REDEEM, $multiRedeem, $comparison);
    }

    /**
     * Filter the query on the discount_amount column
     *
     * Example usage:
     * <code>
     * $query->filterByDiscountAmount(1234); // WHERE discount_amount = 1234
     * $query->filterByDiscountAmount(array(12, 34)); // WHERE discount_amount IN (12, 34)
     * $query->filterByDiscountAmount(array('min' => 12)); // WHERE discount_amount > 12
     * </code>
     *
     * @param     mixed $discountAmount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCouponTypeQuery The current query, for fluid interface
     */
    public function filterByDiscountAmount($discountAmount = null, $comparison = null)
    {
        if (is_array($discountAmount)) {
            $useMinMax = false;
            if (isset($discountAmount['min'])) {
                $this->addUsingAlias(CouponTypeTableMap::COL_DISCOUNT_AMOUNT, $discountAmount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($discountAmount['max'])) {
                $this->addUsingAlias(CouponTypeTableMap::COL_DISCOUNT_AMOUNT, $discountAmount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CouponTypeTableMap::COL_DISCOUNT_AMOUNT, $discountAmount, $comparison);
    }

    /**
     * Filter the query on the discount_percentage column
     *
     * Example usage:
     * <code>
     * $query->filterByDiscountPercentage(1234); // WHERE discount_percentage = 1234
     * $query->filterByDiscountPercentage(array(12, 34)); // WHERE discount_percentage IN (12, 34)
     * $query->filterByDiscountPercentage(array('min' => 12)); // WHERE discount_percentage > 12
     * </code>
     *
     * @param     mixed $discountPercentage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCouponTypeQuery The current query, for fluid interface
     */
    public function filterByDiscountPercentage($discountPercentage = null, $comparison = null)
    {
        if (is_array($discountPercentage)) {
            $useMinMax = false;
            if (isset($discountPercentage['min'])) {
                $this->addUsingAlias(CouponTypeTableMap::COL_DISCOUNT_PERCENTAGE, $discountPercentage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($discountPercentage['max'])) {
                $this->addUsingAlias(CouponTypeTableMap::COL_DISCOUNT_PERCENTAGE, $discountPercentage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CouponTypeTableMap::COL_DISCOUNT_PERCENTAGE, $discountPercentage, $comparison);
    }

    /**
     * Filter the query on the valid_after column
     *
     * Example usage:
     * <code>
     * $query->filterByValidAfter('2011-03-14'); // WHERE valid_after = '2011-03-14'
     * $query->filterByValidAfter('now'); // WHERE valid_after = '2011-03-14'
     * $query->filterByValidAfter(array('max' => 'yesterday')); // WHERE valid_after > '2011-03-13'
     * </code>
     *
     * @param     mixed $validAfter The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCouponTypeQuery The current query, for fluid interface
     */
    public function filterByValidAfter($validAfter = null, $comparison = null)
    {
        if (is_array($validAfter)) {
            $useMinMax = false;
            if (isset($validAfter['min'])) {
                $this->addUsingAlias(CouponTypeTableMap::COL_VALID_AFTER, $validAfter['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($validAfter['max'])) {
                $this->addUsingAlias(CouponTypeTableMap::COL_VALID_AFTER, $validAfter['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CouponTypeTableMap::COL_VALID_AFTER, $validAfter, $comparison);
    }

    /**
     * Filter the query on the valid_until column
     *
     * Example usage:
     * <code>
     * $query->filterByValidUntil('2011-03-14'); // WHERE valid_until = '2011-03-14'
     * $query->filterByValidUntil('now'); // WHERE valid_until = '2011-03-14'
     * $query->filterByValidUntil(array('max' => 'yesterday')); // WHERE valid_until > '2011-03-13'
     * </code>
     *
     * @param     mixed $validUntil The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCouponTypeQuery The current query, for fluid interface
     */
    public function filterByValidUntil($validUntil = null, $comparison = null)
    {
        if (is_array($validUntil)) {
            $useMinMax = false;
            if (isset($validUntil['min'])) {
                $this->addUsingAlias(CouponTypeTableMap::COL_VALID_UNTIL, $validUntil['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($validUntil['max'])) {
                $this->addUsingAlias(CouponTypeTableMap::COL_VALID_UNTIL, $validUntil['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CouponTypeTableMap::COL_VALID_UNTIL, $validUntil, $comparison);
    }

    /**
     * Filter the query by a related \Model\Marketing\Coupon object
     *
     * @param \Model\Marketing\Coupon|ObjectCollection $coupon the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCouponTypeQuery The current query, for fluid interface
     */
    public function filterByCoupon($coupon, $comparison = null)
    {
        if ($coupon instanceof \Model\Marketing\Coupon) {
            return $this
                ->addUsingAlias(CouponTypeTableMap::COL_ID, $coupon->getCouponTypeId(), $comparison);
        } elseif ($coupon instanceof ObjectCollection) {
            return $this
                ->useCouponQuery()
                ->filterByPrimaryKeys($coupon->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCoupon() only accepts arguments of type \Model\Marketing\Coupon or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Coupon relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCouponTypeQuery The current query, for fluid interface
     */
    public function joinCoupon($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Coupon');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Coupon');
        }

        return $this;
    }

    /**
     * Use the Coupon relation Coupon object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Marketing\CouponQuery A secondary query class using the current class as primary query
     */
    public function useCouponQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCoupon($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Coupon', '\Model\Marketing\CouponQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCouponType $couponType Object to remove from the list of results
     *
     * @return $this|ChildCouponTypeQuery The current query, for fluid interface
     */
    public function prune($couponType = null)
    {
        if ($couponType) {
            $this->addUsingAlias(CouponTypeTableMap::COL_ID, $couponType->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the coupon_type table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CouponTypeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CouponTypeTableMap::clearInstancePool();
            CouponTypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CouponTypeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CouponTypeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CouponTypeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CouponTypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CouponTypeQuery
