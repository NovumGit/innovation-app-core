<?php

namespace Model\Marketing\Base;

use \Exception;
use \PDO;
use Model\Marketing\MailingMessage as ChildMailingMessage;
use Model\Marketing\MailingMessageQuery as ChildMailingMessageQuery;
use Model\Marketing\Map\MailingMessageTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mailing_message' table.
 *
 *
 *
 * @method     ChildMailingMessageQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildMailingMessageQuery orderByMailingId($order = Criteria::ASC) Order by the mailing_id column
 * @method     ChildMailingMessageQuery orderByToAddress($order = Criteria::ASC) Order by the to_address column
 * @method     ChildMailingMessageQuery orderByToFullName($order = Criteria::ASC) Order by the to_full_name column
 * @method     ChildMailingMessageQuery orderBySubject($order = Criteria::ASC) Order by the subject column
 * @method     ChildMailingMessageQuery orderByContents($order = Criteria::ASC) Order by the contents column
 * @method     ChildMailingMessageQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 *
 * @method     ChildMailingMessageQuery groupById() Group by the id column
 * @method     ChildMailingMessageQuery groupByMailingId() Group by the mailing_id column
 * @method     ChildMailingMessageQuery groupByToAddress() Group by the to_address column
 * @method     ChildMailingMessageQuery groupByToFullName() Group by the to_full_name column
 * @method     ChildMailingMessageQuery groupBySubject() Group by the subject column
 * @method     ChildMailingMessageQuery groupByContents() Group by the contents column
 * @method     ChildMailingMessageQuery groupByCreatedDate() Group by the created_date column
 *
 * @method     ChildMailingMessageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMailingMessageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMailingMessageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMailingMessageQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMailingMessageQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMailingMessageQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMailingMessageQuery leftJoinMailing($relationAlias = null) Adds a LEFT JOIN clause to the query using the Mailing relation
 * @method     ChildMailingMessageQuery rightJoinMailing($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Mailing relation
 * @method     ChildMailingMessageQuery innerJoinMailing($relationAlias = null) Adds a INNER JOIN clause to the query using the Mailing relation
 *
 * @method     ChildMailingMessageQuery joinWithMailing($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Mailing relation
 *
 * @method     ChildMailingMessageQuery leftJoinWithMailing() Adds a LEFT JOIN clause and with to the query using the Mailing relation
 * @method     ChildMailingMessageQuery rightJoinWithMailing() Adds a RIGHT JOIN clause and with to the query using the Mailing relation
 * @method     ChildMailingMessageQuery innerJoinWithMailing() Adds a INNER JOIN clause and with to the query using the Mailing relation
 *
 * @method     \Model\Marketing\MailingQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMailingMessage findOne(ConnectionInterface $con = null) Return the first ChildMailingMessage matching the query
 * @method     ChildMailingMessage findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMailingMessage matching the query, or a new ChildMailingMessage object populated from the query conditions when no match is found
 *
 * @method     ChildMailingMessage findOneById(int $id) Return the first ChildMailingMessage filtered by the id column
 * @method     ChildMailingMessage findOneByMailingId(int $mailing_id) Return the first ChildMailingMessage filtered by the mailing_id column
 * @method     ChildMailingMessage findOneByToAddress(string $to_address) Return the first ChildMailingMessage filtered by the to_address column
 * @method     ChildMailingMessage findOneByToFullName(string $to_full_name) Return the first ChildMailingMessage filtered by the to_full_name column
 * @method     ChildMailingMessage findOneBySubject(string $subject) Return the first ChildMailingMessage filtered by the subject column
 * @method     ChildMailingMessage findOneByContents(string $contents) Return the first ChildMailingMessage filtered by the contents column
 * @method     ChildMailingMessage findOneByCreatedDate(string $created_date) Return the first ChildMailingMessage filtered by the created_date column *

 * @method     ChildMailingMessage requirePk($key, ConnectionInterface $con = null) Return the ChildMailingMessage by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailingMessage requireOne(ConnectionInterface $con = null) Return the first ChildMailingMessage matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMailingMessage requireOneById(int $id) Return the first ChildMailingMessage filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailingMessage requireOneByMailingId(int $mailing_id) Return the first ChildMailingMessage filtered by the mailing_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailingMessage requireOneByToAddress(string $to_address) Return the first ChildMailingMessage filtered by the to_address column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailingMessage requireOneByToFullName(string $to_full_name) Return the first ChildMailingMessage filtered by the to_full_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailingMessage requireOneBySubject(string $subject) Return the first ChildMailingMessage filtered by the subject column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailingMessage requireOneByContents(string $contents) Return the first ChildMailingMessage filtered by the contents column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailingMessage requireOneByCreatedDate(string $created_date) Return the first ChildMailingMessage filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMailingMessage[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMailingMessage objects based on current ModelCriteria
 * @method     ChildMailingMessage[]|ObjectCollection findById(int $id) Return ChildMailingMessage objects filtered by the id column
 * @method     ChildMailingMessage[]|ObjectCollection findByMailingId(int $mailing_id) Return ChildMailingMessage objects filtered by the mailing_id column
 * @method     ChildMailingMessage[]|ObjectCollection findByToAddress(string $to_address) Return ChildMailingMessage objects filtered by the to_address column
 * @method     ChildMailingMessage[]|ObjectCollection findByToFullName(string $to_full_name) Return ChildMailingMessage objects filtered by the to_full_name column
 * @method     ChildMailingMessage[]|ObjectCollection findBySubject(string $subject) Return ChildMailingMessage objects filtered by the subject column
 * @method     ChildMailingMessage[]|ObjectCollection findByContents(string $contents) Return ChildMailingMessage objects filtered by the contents column
 * @method     ChildMailingMessage[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildMailingMessage objects filtered by the created_date column
 * @method     ChildMailingMessage[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MailingMessageQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Marketing\Base\MailingMessageQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Marketing\\MailingMessage', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMailingMessageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMailingMessageQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMailingMessageQuery) {
            return $criteria;
        }
        $query = new ChildMailingMessageQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMailingMessage|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MailingMessageTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MailingMessageTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMailingMessage A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, mailing_id, to_address, to_full_name, subject, contents, created_date FROM mailing_message WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMailingMessage $obj */
            $obj = new ChildMailingMessage();
            $obj->hydrate($row);
            MailingMessageTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMailingMessage|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMailingMessageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MailingMessageTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMailingMessageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MailingMessageTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingMessageQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MailingMessageTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MailingMessageTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingMessageTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the mailing_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMailingId(1234); // WHERE mailing_id = 1234
     * $query->filterByMailingId(array(12, 34)); // WHERE mailing_id IN (12, 34)
     * $query->filterByMailingId(array('min' => 12)); // WHERE mailing_id > 12
     * </code>
     *
     * @see       filterByMailing()
     *
     * @param     mixed $mailingId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingMessageQuery The current query, for fluid interface
     */
    public function filterByMailingId($mailingId = null, $comparison = null)
    {
        if (is_array($mailingId)) {
            $useMinMax = false;
            if (isset($mailingId['min'])) {
                $this->addUsingAlias(MailingMessageTableMap::COL_MAILING_ID, $mailingId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mailingId['max'])) {
                $this->addUsingAlias(MailingMessageTableMap::COL_MAILING_ID, $mailingId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingMessageTableMap::COL_MAILING_ID, $mailingId, $comparison);
    }

    /**
     * Filter the query on the to_address column
     *
     * Example usage:
     * <code>
     * $query->filterByToAddress('fooValue');   // WHERE to_address = 'fooValue'
     * $query->filterByToAddress('%fooValue%', Criteria::LIKE); // WHERE to_address LIKE '%fooValue%'
     * </code>
     *
     * @param     string $toAddress The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingMessageQuery The current query, for fluid interface
     */
    public function filterByToAddress($toAddress = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($toAddress)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingMessageTableMap::COL_TO_ADDRESS, $toAddress, $comparison);
    }

    /**
     * Filter the query on the to_full_name column
     *
     * Example usage:
     * <code>
     * $query->filterByToFullName('fooValue');   // WHERE to_full_name = 'fooValue'
     * $query->filterByToFullName('%fooValue%', Criteria::LIKE); // WHERE to_full_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $toFullName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingMessageQuery The current query, for fluid interface
     */
    public function filterByToFullName($toFullName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($toFullName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingMessageTableMap::COL_TO_FULL_NAME, $toFullName, $comparison);
    }

    /**
     * Filter the query on the subject column
     *
     * Example usage:
     * <code>
     * $query->filterBySubject('fooValue');   // WHERE subject = 'fooValue'
     * $query->filterBySubject('%fooValue%', Criteria::LIKE); // WHERE subject LIKE '%fooValue%'
     * </code>
     *
     * @param     string $subject The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingMessageQuery The current query, for fluid interface
     */
    public function filterBySubject($subject = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($subject)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingMessageTableMap::COL_SUBJECT, $subject, $comparison);
    }

    /**
     * Filter the query on the contents column
     *
     * Example usage:
     * <code>
     * $query->filterByContents('fooValue');   // WHERE contents = 'fooValue'
     * $query->filterByContents('%fooValue%', Criteria::LIKE); // WHERE contents LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contents The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingMessageQuery The current query, for fluid interface
     */
    public function filterByContents($contents = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contents)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingMessageTableMap::COL_CONTENTS, $contents, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailingMessageQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(MailingMessageTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(MailingMessageTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailingMessageTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query by a related \Model\Marketing\Mailing object
     *
     * @param \Model\Marketing\Mailing|ObjectCollection $mailing The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMailingMessageQuery The current query, for fluid interface
     */
    public function filterByMailing($mailing, $comparison = null)
    {
        if ($mailing instanceof \Model\Marketing\Mailing) {
            return $this
                ->addUsingAlias(MailingMessageTableMap::COL_MAILING_ID, $mailing->getId(), $comparison);
        } elseif ($mailing instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MailingMessageTableMap::COL_MAILING_ID, $mailing->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMailing() only accepts arguments of type \Model\Marketing\Mailing or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Mailing relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMailingMessageQuery The current query, for fluid interface
     */
    public function joinMailing($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Mailing');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Mailing');
        }

        return $this;
    }

    /**
     * Use the Mailing relation Mailing object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Marketing\MailingQuery A secondary query class using the current class as primary query
     */
    public function useMailingQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMailing($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Mailing', '\Model\Marketing\MailingQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMailingMessage $mailingMessage Object to remove from the list of results
     *
     * @return $this|ChildMailingMessageQuery The current query, for fluid interface
     */
    public function prune($mailingMessage = null)
    {
        if ($mailingMessage) {
            $this->addUsingAlias(MailingMessageTableMap::COL_ID, $mailingMessage->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mailing_message table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MailingMessageTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MailingMessageTableMap::clearInstancePool();
            MailingMessageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MailingMessageTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MailingMessageTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MailingMessageTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MailingMessageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MailingMessageQuery
