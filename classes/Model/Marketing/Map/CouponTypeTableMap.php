<?php

namespace Model\Marketing\Map;

use Model\Marketing\CouponType;
use Model\Marketing\CouponTypeQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'coupon_type' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CouponTypeTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Marketing.Map.CouponTypeTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'coupon_type';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Marketing\\CouponType';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Marketing.CouponType';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    const COL_ID = 'coupon_type.id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'coupon_type.title';

    /**
     * the column name for the coupon_type field
     */
    const COL_COUPON_TYPE = 'coupon_type.coupon_type';

    /**
     * the column name for the multi_redeem field
     */
    const COL_MULTI_REDEEM = 'coupon_type.multi_redeem';

    /**
     * the column name for the discount_amount field
     */
    const COL_DISCOUNT_AMOUNT = 'coupon_type.discount_amount';

    /**
     * the column name for the discount_percentage field
     */
    const COL_DISCOUNT_PERCENTAGE = 'coupon_type.discount_percentage';

    /**
     * the column name for the valid_after field
     */
    const COL_VALID_AFTER = 'coupon_type.valid_after';

    /**
     * the column name for the valid_until field
     */
    const COL_VALID_UNTIL = 'coupon_type.valid_until';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /** The enumerated values for the coupon_type field */
    const COL_COUPON_TYPE_PERCENTAGE = 'percentage';
    const COL_COUPON_TYPE_AMOUNT = 'amount';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Title', 'CouponType', 'MultiRedeem', 'DiscountAmount', 'DiscountPercentage', 'ValidAfter', 'ValidUntil', ),
        self::TYPE_CAMELNAME     => array('id', 'title', 'couponType', 'multiRedeem', 'discountAmount', 'discountPercentage', 'validAfter', 'validUntil', ),
        self::TYPE_COLNAME       => array(CouponTypeTableMap::COL_ID, CouponTypeTableMap::COL_TITLE, CouponTypeTableMap::COL_COUPON_TYPE, CouponTypeTableMap::COL_MULTI_REDEEM, CouponTypeTableMap::COL_DISCOUNT_AMOUNT, CouponTypeTableMap::COL_DISCOUNT_PERCENTAGE, CouponTypeTableMap::COL_VALID_AFTER, CouponTypeTableMap::COL_VALID_UNTIL, ),
        self::TYPE_FIELDNAME     => array('id', 'title', 'coupon_type', 'multi_redeem', 'discount_amount', 'discount_percentage', 'valid_after', 'valid_until', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Title' => 1, 'CouponType' => 2, 'MultiRedeem' => 3, 'DiscountAmount' => 4, 'DiscountPercentage' => 5, 'ValidAfter' => 6, 'ValidUntil' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'title' => 1, 'couponType' => 2, 'multiRedeem' => 3, 'discountAmount' => 4, 'discountPercentage' => 5, 'validAfter' => 6, 'validUntil' => 7, ),
        self::TYPE_COLNAME       => array(CouponTypeTableMap::COL_ID => 0, CouponTypeTableMap::COL_TITLE => 1, CouponTypeTableMap::COL_COUPON_TYPE => 2, CouponTypeTableMap::COL_MULTI_REDEEM => 3, CouponTypeTableMap::COL_DISCOUNT_AMOUNT => 4, CouponTypeTableMap::COL_DISCOUNT_PERCENTAGE => 5, CouponTypeTableMap::COL_VALID_AFTER => 6, CouponTypeTableMap::COL_VALID_UNTIL => 7, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'title' => 1, 'coupon_type' => 2, 'multi_redeem' => 3, 'discount_amount' => 4, 'discount_percentage' => 5, 'valid_after' => 6, 'valid_until' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /** The enumerated values for this table */
    protected static $enumValueSets = array(
                CouponTypeTableMap::COL_COUPON_TYPE => array(
                            self::COL_COUPON_TYPE_PERCENTAGE,
            self::COL_COUPON_TYPE_AMOUNT,
        ),
    );

    /**
     * Gets the list of values for all ENUM and SET columns
     * @return array
     */
    public static function getValueSets()
    {
      return static::$enumValueSets;
    }

    /**
     * Gets the list of values for an ENUM or SET column
     * @param string $colname
     * @return array list of possible values for the column
     */
    public static function getValueSet($colname)
    {
        $valueSets = self::getValueSets();

        return $valueSets[$colname];
    }

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('coupon_type');
        $this->setPhpName('CouponType');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Marketing\\CouponType');
        $this->setPackage('Model.Marketing');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', true, 255, null);
        $this->addColumn('coupon_type', 'CouponType', 'ENUM', false, null, null);
        $this->getColumn('coupon_type')->setValueSet(array (
  0 => 'percentage',
  1 => 'amount',
));
        $this->addColumn('multi_redeem', 'MultiRedeem', 'BOOLEAN', true, 1, false);
        $this->addColumn('discount_amount', 'DiscountAmount', 'FLOAT', false, null, null);
        $this->addColumn('discount_percentage', 'DiscountPercentage', 'FLOAT', false, null, null);
        $this->addColumn('valid_after', 'ValidAfter', 'DATE', true, null, null);
        $this->addColumn('valid_until', 'ValidUntil', 'DATE', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Coupon', '\\Model\\Marketing\\Coupon', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':coupon_type_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'Coupons', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CouponTypeTableMap::CLASS_DEFAULT : CouponTypeTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CouponType object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CouponTypeTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CouponTypeTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CouponTypeTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CouponTypeTableMap::OM_CLASS;
            /** @var CouponType $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CouponTypeTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CouponTypeTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CouponTypeTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CouponType $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CouponTypeTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CouponTypeTableMap::COL_ID);
            $criteria->addSelectColumn(CouponTypeTableMap::COL_TITLE);
            $criteria->addSelectColumn(CouponTypeTableMap::COL_COUPON_TYPE);
            $criteria->addSelectColumn(CouponTypeTableMap::COL_MULTI_REDEEM);
            $criteria->addSelectColumn(CouponTypeTableMap::COL_DISCOUNT_AMOUNT);
            $criteria->addSelectColumn(CouponTypeTableMap::COL_DISCOUNT_PERCENTAGE);
            $criteria->addSelectColumn(CouponTypeTableMap::COL_VALID_AFTER);
            $criteria->addSelectColumn(CouponTypeTableMap::COL_VALID_UNTIL);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.coupon_type');
            $criteria->addSelectColumn($alias . '.multi_redeem');
            $criteria->addSelectColumn($alias . '.discount_amount');
            $criteria->addSelectColumn($alias . '.discount_percentage');
            $criteria->addSelectColumn($alias . '.valid_after');
            $criteria->addSelectColumn($alias . '.valid_until');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CouponTypeTableMap::DATABASE_NAME)->getTable(CouponTypeTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CouponTypeTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CouponTypeTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CouponTypeTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CouponType or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CouponType object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CouponTypeTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Marketing\CouponType) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CouponTypeTableMap::DATABASE_NAME);
            $criteria->add(CouponTypeTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CouponTypeQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CouponTypeTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CouponTypeTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the coupon_type table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CouponTypeQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CouponType or Criteria object.
     *
     * @param mixed               $criteria Criteria or CouponType object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CouponTypeTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CouponType object
        }

        if ($criteria->containsKey(CouponTypeTableMap::COL_ID) && $criteria->keyContainsValue(CouponTypeTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CouponTypeTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CouponTypeQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CouponTypeTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CouponTypeTableMap::buildTableMap();
