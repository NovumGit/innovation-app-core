<?php

namespace Model\SystemRegistry;

use Model\SystemRegistry\Base\SystemRegistryQuery as BaseSystemRegistryQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'system_registry' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SystemRegistryQuery extends BaseSystemRegistryQuery
{

}
