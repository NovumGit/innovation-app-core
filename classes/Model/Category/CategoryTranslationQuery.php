<?php

namespace Model\Category;

use Core\PropelSqlTrait;
use Model\Category\Base\CategoryTranslationQuery as BaseCategoryTranslationQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'category_translation' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CategoryTranslationQuery extends BaseCategoryTranslationQuery
{
	use PropelSqlTrait;

    function findOneByLanguageAndCategoryId($iCategoryId, $iLangageId):CategoryTranslation
    {
        $oCategoryTranslationQuery = CategoryTranslationQuery::create();
        $oCategoryTranslationQuery->filterByCategoryId($iCategoryId);
        $oCategoryTranslationQuery->filterByLanguageId($iLangageId);
        return $oCategoryTranslationQuery->findOne();

    }
}
