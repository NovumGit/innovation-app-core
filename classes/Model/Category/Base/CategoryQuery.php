<?php

namespace Model\Category\Base;

use \Exception;
use \PDO;
use Model\Product;
use Model\Category\Category as ChildCategory;
use Model\Category\CategoryQuery as ChildCategoryQuery;
use Model\Category\Map\CategoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'category' table.
 *
 *
 *
 * @method     ChildCategoryQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCategoryQuery orderByOldId($order = Criteria::ASC) Order by the old_id column
 * @method     ChildCategoryQuery orderBySorting($order = Criteria::ASC) Order by the sorting column
 * @method     ChildCategoryQuery orderByParentId($order = Criteria::ASC) Order by the parent_id column
 * @method     ChildCategoryQuery orderByTag($order = Criteria::ASC) Order by the tag column
 * @method     ChildCategoryQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildCategoryQuery orderByNameSingular($order = Criteria::ASC) Order by the name_singular column
 * @method     ChildCategoryQuery orderByNamePlural($order = Criteria::ASC) Order by the name_plural column
 * @method     ChildCategoryQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildCategoryQuery orderByHasImage($order = Criteria::ASC) Order by the has_image column
 * @method     ChildCategoryQuery orderByInBestsellers($order = Criteria::ASC) Order by the in_bestsellers column
 * @method     ChildCategoryQuery orderByImageExt($order = Criteria::ASC) Order by the image_ext column
 * @method     ChildCategoryQuery orderByIcon($order = Criteria::ASC) Order by the icon column
 *
 * @method     ChildCategoryQuery groupById() Group by the id column
 * @method     ChildCategoryQuery groupByOldId() Group by the old_id column
 * @method     ChildCategoryQuery groupBySorting() Group by the sorting column
 * @method     ChildCategoryQuery groupByParentId() Group by the parent_id column
 * @method     ChildCategoryQuery groupByTag() Group by the tag column
 * @method     ChildCategoryQuery groupByName() Group by the name column
 * @method     ChildCategoryQuery groupByNameSingular() Group by the name_singular column
 * @method     ChildCategoryQuery groupByNamePlural() Group by the name_plural column
 * @method     ChildCategoryQuery groupByDescription() Group by the description column
 * @method     ChildCategoryQuery groupByHasImage() Group by the has_image column
 * @method     ChildCategoryQuery groupByInBestsellers() Group by the in_bestsellers column
 * @method     ChildCategoryQuery groupByImageExt() Group by the image_ext column
 * @method     ChildCategoryQuery groupByIcon() Group by the icon column
 *
 * @method     ChildCategoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCategoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCategoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCategoryQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCategoryQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCategoryQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCategoryQuery leftJoinProductMultiCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductMultiCategory relation
 * @method     ChildCategoryQuery rightJoinProductMultiCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductMultiCategory relation
 * @method     ChildCategoryQuery innerJoinProductMultiCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductMultiCategory relation
 *
 * @method     ChildCategoryQuery joinWithProductMultiCategory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductMultiCategory relation
 *
 * @method     ChildCategoryQuery leftJoinWithProductMultiCategory() Adds a LEFT JOIN clause and with to the query using the ProductMultiCategory relation
 * @method     ChildCategoryQuery rightJoinWithProductMultiCategory() Adds a RIGHT JOIN clause and with to the query using the ProductMultiCategory relation
 * @method     ChildCategoryQuery innerJoinWithProductMultiCategory() Adds a INNER JOIN clause and with to the query using the ProductMultiCategory relation
 *
 * @method     ChildCategoryQuery leftJoinCategoryTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the CategoryTranslation relation
 * @method     ChildCategoryQuery rightJoinCategoryTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CategoryTranslation relation
 * @method     ChildCategoryQuery innerJoinCategoryTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the CategoryTranslation relation
 *
 * @method     ChildCategoryQuery joinWithCategoryTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CategoryTranslation relation
 *
 * @method     ChildCategoryQuery leftJoinWithCategoryTranslation() Adds a LEFT JOIN clause and with to the query using the CategoryTranslation relation
 * @method     ChildCategoryQuery rightJoinWithCategoryTranslation() Adds a RIGHT JOIN clause and with to the query using the CategoryTranslation relation
 * @method     ChildCategoryQuery innerJoinWithCategoryTranslation() Adds a INNER JOIN clause and with to the query using the CategoryTranslation relation
 *
 * @method     ChildCategoryQuery leftJoinProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the Product relation
 * @method     ChildCategoryQuery rightJoinProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Product relation
 * @method     ChildCategoryQuery innerJoinProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the Product relation
 *
 * @method     ChildCategoryQuery joinWithProduct($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Product relation
 *
 * @method     ChildCategoryQuery leftJoinWithProduct() Adds a LEFT JOIN clause and with to the query using the Product relation
 * @method     ChildCategoryQuery rightJoinWithProduct() Adds a RIGHT JOIN clause and with to the query using the Product relation
 * @method     ChildCategoryQuery innerJoinWithProduct() Adds a INNER JOIN clause and with to the query using the Product relation
 *
 * @method     \Model\Category\ProductMultiCategoryQuery|\Model\Category\CategoryTranslationQuery|\Model\ProductQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCategory findOne(ConnectionInterface $con = null) Return the first ChildCategory matching the query
 * @method     ChildCategory findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCategory matching the query, or a new ChildCategory object populated from the query conditions when no match is found
 *
 * @method     ChildCategory findOneById(int $id) Return the first ChildCategory filtered by the id column
 * @method     ChildCategory findOneByOldId(string $old_id) Return the first ChildCategory filtered by the old_id column
 * @method     ChildCategory findOneBySorting(int $sorting) Return the first ChildCategory filtered by the sorting column
 * @method     ChildCategory findOneByParentId(int $parent_id) Return the first ChildCategory filtered by the parent_id column
 * @method     ChildCategory findOneByTag(string $tag) Return the first ChildCategory filtered by the tag column
 * @method     ChildCategory findOneByName(string $name) Return the first ChildCategory filtered by the name column
 * @method     ChildCategory findOneByNameSingular(string $name_singular) Return the first ChildCategory filtered by the name_singular column
 * @method     ChildCategory findOneByNamePlural(string $name_plural) Return the first ChildCategory filtered by the name_plural column
 * @method     ChildCategory findOneByDescription(string $description) Return the first ChildCategory filtered by the description column
 * @method     ChildCategory findOneByHasImage(boolean $has_image) Return the first ChildCategory filtered by the has_image column
 * @method     ChildCategory findOneByInBestsellers(boolean $in_bestsellers) Return the first ChildCategory filtered by the in_bestsellers column
 * @method     ChildCategory findOneByImageExt(string $image_ext) Return the first ChildCategory filtered by the image_ext column
 * @method     ChildCategory findOneByIcon(string $icon) Return the first ChildCategory filtered by the icon column *

 * @method     ChildCategory requirePk($key, ConnectionInterface $con = null) Return the ChildCategory by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategory requireOne(ConnectionInterface $con = null) Return the first ChildCategory matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCategory requireOneById(int $id) Return the first ChildCategory filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategory requireOneByOldId(string $old_id) Return the first ChildCategory filtered by the old_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategory requireOneBySorting(int $sorting) Return the first ChildCategory filtered by the sorting column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategory requireOneByParentId(int $parent_id) Return the first ChildCategory filtered by the parent_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategory requireOneByTag(string $tag) Return the first ChildCategory filtered by the tag column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategory requireOneByName(string $name) Return the first ChildCategory filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategory requireOneByNameSingular(string $name_singular) Return the first ChildCategory filtered by the name_singular column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategory requireOneByNamePlural(string $name_plural) Return the first ChildCategory filtered by the name_plural column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategory requireOneByDescription(string $description) Return the first ChildCategory filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategory requireOneByHasImage(boolean $has_image) Return the first ChildCategory filtered by the has_image column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategory requireOneByInBestsellers(boolean $in_bestsellers) Return the first ChildCategory filtered by the in_bestsellers column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategory requireOneByImageExt(string $image_ext) Return the first ChildCategory filtered by the image_ext column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCategory requireOneByIcon(string $icon) Return the first ChildCategory filtered by the icon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCategory[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCategory objects based on current ModelCriteria
 * @method     ChildCategory[]|ObjectCollection findById(int $id) Return ChildCategory objects filtered by the id column
 * @method     ChildCategory[]|ObjectCollection findByOldId(string $old_id) Return ChildCategory objects filtered by the old_id column
 * @method     ChildCategory[]|ObjectCollection findBySorting(int $sorting) Return ChildCategory objects filtered by the sorting column
 * @method     ChildCategory[]|ObjectCollection findByParentId(int $parent_id) Return ChildCategory objects filtered by the parent_id column
 * @method     ChildCategory[]|ObjectCollection findByTag(string $tag) Return ChildCategory objects filtered by the tag column
 * @method     ChildCategory[]|ObjectCollection findByName(string $name) Return ChildCategory objects filtered by the name column
 * @method     ChildCategory[]|ObjectCollection findByNameSingular(string $name_singular) Return ChildCategory objects filtered by the name_singular column
 * @method     ChildCategory[]|ObjectCollection findByNamePlural(string $name_plural) Return ChildCategory objects filtered by the name_plural column
 * @method     ChildCategory[]|ObjectCollection findByDescription(string $description) Return ChildCategory objects filtered by the description column
 * @method     ChildCategory[]|ObjectCollection findByHasImage(boolean $has_image) Return ChildCategory objects filtered by the has_image column
 * @method     ChildCategory[]|ObjectCollection findByInBestsellers(boolean $in_bestsellers) Return ChildCategory objects filtered by the in_bestsellers column
 * @method     ChildCategory[]|ObjectCollection findByImageExt(string $image_ext) Return ChildCategory objects filtered by the image_ext column
 * @method     ChildCategory[]|ObjectCollection findByIcon(string $icon) Return ChildCategory objects filtered by the icon column
 * @method     ChildCategory[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CategoryQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Category\Base\CategoryQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Category\\Category', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCategoryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCategoryQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCategoryQuery) {
            return $criteria;
        }
        $query = new ChildCategoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCategory|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CategoryTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CategoryTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCategory A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, old_id, sorting, parent_id, tag, name, name_singular, name_plural, description, has_image, in_bestsellers, image_ext, icon FROM category WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCategory $obj */
            $obj = new ChildCategory();
            $obj->hydrate($row);
            CategoryTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCategory|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CategoryTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CategoryTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CategoryTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CategoryTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategoryTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the old_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldId('fooValue');   // WHERE old_id = 'fooValue'
     * $query->filterByOldId('%fooValue%', Criteria::LIKE); // WHERE old_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $oldId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByOldId($oldId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($oldId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategoryTableMap::COL_OLD_ID, $oldId, $comparison);
    }

    /**
     * Filter the query on the sorting column
     *
     * Example usage:
     * <code>
     * $query->filterBySorting(1234); // WHERE sorting = 1234
     * $query->filterBySorting(array(12, 34)); // WHERE sorting IN (12, 34)
     * $query->filterBySorting(array('min' => 12)); // WHERE sorting > 12
     * </code>
     *
     * @param     mixed $sorting The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterBySorting($sorting = null, $comparison = null)
    {
        if (is_array($sorting)) {
            $useMinMax = false;
            if (isset($sorting['min'])) {
                $this->addUsingAlias(CategoryTableMap::COL_SORTING, $sorting['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sorting['max'])) {
                $this->addUsingAlias(CategoryTableMap::COL_SORTING, $sorting['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategoryTableMap::COL_SORTING, $sorting, $comparison);
    }

    /**
     * Filter the query on the parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE parent_id > 12
     * </code>
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(CategoryTableMap::COL_PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(CategoryTableMap::COL_PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategoryTableMap::COL_PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the tag column
     *
     * Example usage:
     * <code>
     * $query->filterByTag('fooValue');   // WHERE tag = 'fooValue'
     * $query->filterByTag('%fooValue%', Criteria::LIKE); // WHERE tag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tag The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByTag($tag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tag)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategoryTableMap::COL_TAG, $tag, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategoryTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the name_singular column
     *
     * Example usage:
     * <code>
     * $query->filterByNameSingular('fooValue');   // WHERE name_singular = 'fooValue'
     * $query->filterByNameSingular('%fooValue%', Criteria::LIKE); // WHERE name_singular LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nameSingular The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByNameSingular($nameSingular = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nameSingular)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategoryTableMap::COL_NAME_SINGULAR, $nameSingular, $comparison);
    }

    /**
     * Filter the query on the name_plural column
     *
     * Example usage:
     * <code>
     * $query->filterByNamePlural('fooValue');   // WHERE name_plural = 'fooValue'
     * $query->filterByNamePlural('%fooValue%', Criteria::LIKE); // WHERE name_plural LIKE '%fooValue%'
     * </code>
     *
     * @param     string $namePlural The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByNamePlural($namePlural = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($namePlural)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategoryTableMap::COL_NAME_PLURAL, $namePlural, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategoryTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the has_image column
     *
     * Example usage:
     * <code>
     * $query->filterByHasImage(true); // WHERE has_image = true
     * $query->filterByHasImage('yes'); // WHERE has_image = true
     * </code>
     *
     * @param     boolean|string $hasImage The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByHasImage($hasImage = null, $comparison = null)
    {
        if (is_string($hasImage)) {
            $hasImage = in_array(strtolower($hasImage), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CategoryTableMap::COL_HAS_IMAGE, $hasImage, $comparison);
    }

    /**
     * Filter the query on the in_bestsellers column
     *
     * Example usage:
     * <code>
     * $query->filterByInBestsellers(true); // WHERE in_bestsellers = true
     * $query->filterByInBestsellers('yes'); // WHERE in_bestsellers = true
     * </code>
     *
     * @param     boolean|string $inBestsellers The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByInBestsellers($inBestsellers = null, $comparison = null)
    {
        if (is_string($inBestsellers)) {
            $inBestsellers = in_array(strtolower($inBestsellers), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CategoryTableMap::COL_IN_BESTSELLERS, $inBestsellers, $comparison);
    }

    /**
     * Filter the query on the image_ext column
     *
     * Example usage:
     * <code>
     * $query->filterByImageExt('fooValue');   // WHERE image_ext = 'fooValue'
     * $query->filterByImageExt('%fooValue%', Criteria::LIKE); // WHERE image_ext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageExt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByImageExt($imageExt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageExt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategoryTableMap::COL_IMAGE_EXT, $imageExt, $comparison);
    }

    /**
     * Filter the query on the icon column
     *
     * Example usage:
     * <code>
     * $query->filterByIcon('fooValue');   // WHERE icon = 'fooValue'
     * $query->filterByIcon('%fooValue%', Criteria::LIKE); // WHERE icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $icon The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByIcon($icon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($icon)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CategoryTableMap::COL_ICON, $icon, $comparison);
    }

    /**
     * Filter the query by a related \Model\Category\ProductMultiCategory object
     *
     * @param \Model\Category\ProductMultiCategory|ObjectCollection $productMultiCategory the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByProductMultiCategory($productMultiCategory, $comparison = null)
    {
        if ($productMultiCategory instanceof \Model\Category\ProductMultiCategory) {
            return $this
                ->addUsingAlias(CategoryTableMap::COL_ID, $productMultiCategory->getCategoryId(), $comparison);
        } elseif ($productMultiCategory instanceof ObjectCollection) {
            return $this
                ->useProductMultiCategoryQuery()
                ->filterByPrimaryKeys($productMultiCategory->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductMultiCategory() only accepts arguments of type \Model\Category\ProductMultiCategory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductMultiCategory relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function joinProductMultiCategory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductMultiCategory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductMultiCategory');
        }

        return $this;
    }

    /**
     * Use the ProductMultiCategory relation ProductMultiCategory object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Category\ProductMultiCategoryQuery A secondary query class using the current class as primary query
     */
    public function useProductMultiCategoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProductMultiCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductMultiCategory', '\Model\Category\ProductMultiCategoryQuery');
    }

    /**
     * Filter the query by a related \Model\Category\CategoryTranslation object
     *
     * @param \Model\Category\CategoryTranslation|ObjectCollection $categoryTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByCategoryTranslation($categoryTranslation, $comparison = null)
    {
        if ($categoryTranslation instanceof \Model\Category\CategoryTranslation) {
            return $this
                ->addUsingAlias(CategoryTableMap::COL_ID, $categoryTranslation->getCategoryId(), $comparison);
        } elseif ($categoryTranslation instanceof ObjectCollection) {
            return $this
                ->useCategoryTranslationQuery()
                ->filterByPrimaryKeys($categoryTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCategoryTranslation() only accepts arguments of type \Model\Category\CategoryTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CategoryTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function joinCategoryTranslation($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CategoryTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CategoryTranslation');
        }

        return $this;
    }

    /**
     * Use the CategoryTranslation relation CategoryTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Category\CategoryTranslationQuery A secondary query class using the current class as primary query
     */
    public function useCategoryTranslationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCategoryTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CategoryTranslation', '\Model\Category\CategoryTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\Product object
     *
     * @param \Model\Product|ObjectCollection $product the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCategoryQuery The current query, for fluid interface
     */
    public function filterByProduct($product, $comparison = null)
    {
        if ($product instanceof \Model\Product) {
            return $this
                ->addUsingAlias(CategoryTableMap::COL_ID, $product->getCategoryId(), $comparison);
        } elseif ($product instanceof ObjectCollection) {
            return $this
                ->useProductQuery()
                ->filterByPrimaryKeys($product->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProduct() only accepts arguments of type \Model\Product or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Product relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function joinProduct($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Product');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Product');
        }

        return $this;
    }

    /**
     * Use the Product relation Product object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ProductQuery A secondary query class using the current class as primary query
     */
    public function useProductQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Product', '\Model\ProductQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCategory $category Object to remove from the list of results
     *
     * @return $this|ChildCategoryQuery The current query, for fluid interface
     */
    public function prune($category = null)
    {
        if ($category) {
            $this->addUsingAlias(CategoryTableMap::COL_ID, $category->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the category table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CategoryTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CategoryTableMap::clearInstancePool();
            CategoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CategoryTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CategoryTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CategoryTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CategoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 3600)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CategoryTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(CategoryTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

} // CategoryQuery
