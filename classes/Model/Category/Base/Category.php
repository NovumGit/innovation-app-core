<?php

namespace Model\Category\Base;

use \Exception;
use \PDO;
use Model\Product;
use Model\ProductQuery;
use Model\Base\Product as BaseProduct;
use Model\Category\Category as ChildCategory;
use Model\Category\CategoryQuery as ChildCategoryQuery;
use Model\Category\CategoryTranslation as ChildCategoryTranslation;
use Model\Category\CategoryTranslationQuery as ChildCategoryTranslationQuery;
use Model\Category\ProductMultiCategory as ChildProductMultiCategory;
use Model\Category\ProductMultiCategoryQuery as ChildProductMultiCategoryQuery;
use Model\Category\Map\CategoryTableMap;
use Model\Category\Map\CategoryTranslationTableMap;
use Model\Category\Map\ProductMultiCategoryTableMap;
use Model\Map\ProductTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'category' table.
 *
 *
 *
 * @package    propel.generator.Model.Category.Base
 */
abstract class Category implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Category\\Map\\CategoryTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the old_id field.
     *
     * @var        string|null
     */
    protected $old_id;

    /**
     * The value for the sorting field.
     *
     * @var        int|null
     */
    protected $sorting;

    /**
     * The value for the parent_id field.
     *
     * @var        int|null
     */
    protected $parent_id;

    /**
     * The value for the tag field.
     *
     * @var        string|null
     */
    protected $tag;

    /**
     * The value for the name field.
     *
     * @var        string|null
     */
    protected $name;

    /**
     * The value for the name_singular field.
     *
     * @var        string|null
     */
    protected $name_singular;

    /**
     * The value for the name_plural field.
     *
     * @var        string|null
     */
    protected $name_plural;

    /**
     * The value for the description field.
     *
     * @var        string|null
     */
    protected $description;

    /**
     * The value for the has_image field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $has_image;

    /**
     * The value for the in_bestsellers field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $in_bestsellers;

    /**
     * The value for the image_ext field.
     *
     * @var        string|null
     */
    protected $image_ext;

    /**
     * The value for the icon field.
     *
     * @var        string|null
     */
    protected $icon;

    /**
     * @var        ObjectCollection|ChildProductMultiCategory[] Collection to store aggregation of ChildProductMultiCategory objects.
     */
    protected $collProductMultiCategories;
    protected $collProductMultiCategoriesPartial;

    /**
     * @var        ObjectCollection|ChildCategoryTranslation[] Collection to store aggregation of ChildCategoryTranslation objects.
     */
    protected $collCategoryTranslations;
    protected $collCategoryTranslationsPartial;

    /**
     * @var        ObjectCollection|Product[] Collection to store aggregation of Product objects.
     */
    protected $collProducts;
    protected $collProductsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProductMultiCategory[]
     */
    protected $productMultiCategoriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCategoryTranslation[]
     */
    protected $categoryTranslationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Product[]
     */
    protected $productsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->has_image = false;
        $this->in_bestsellers = false;
    }

    /**
     * Initializes internal state of Model\Category\Base\Category object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Category</code> instance.  If
     * <code>obj</code> is an instance of <code>Category</code>, delegates to
     * <code>equals(Category)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [old_id] column value.
     *
     * @return string|null
     */
    public function getOldId()
    {
        return $this->old_id;
    }

    /**
     * Get the [sorting] column value.
     *
     * @return int|null
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Get the [parent_id] column value.
     *
     * @return int|null
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Get the [tag] column value.
     *
     * @return string|null
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Get the [name] column value.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [name_singular] column value.
     *
     * @return string|null
     */
    public function getNameSingular()
    {
        return $this->name_singular;
    }

    /**
     * Get the [name_plural] column value.
     *
     * @return string|null
     */
    public function getNamePlural()
    {
        return $this->name_plural;
    }

    /**
     * Get the [description] column value.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [has_image] column value.
     *
     * @return boolean|null
     */
    public function getHasImage()
    {
        return $this->has_image;
    }

    /**
     * Get the [has_image] column value.
     *
     * @return boolean|null
     */
    public function hasImage()
    {
        return $this->getHasImage();
    }

    /**
     * Get the [in_bestsellers] column value.
     *
     * @return boolean|null
     */
    public function getInBestsellers()
    {
        return $this->in_bestsellers;
    }

    /**
     * Get the [in_bestsellers] column value.
     *
     * @return boolean|null
     */
    public function isInBestsellers()
    {
        return $this->getInBestsellers();
    }

    /**
     * Get the [image_ext] column value.
     *
     * @return string|null
     */
    public function getImageExt()
    {
        return $this->image_ext;
    }

    /**
     * Get the [icon] column value.
     *
     * @return string|null
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[CategoryTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [old_id] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function setOldId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->old_id !== $v) {
            $this->old_id = $v;
            $this->modifiedColumns[CategoryTableMap::COL_OLD_ID] = true;
        }

        return $this;
    } // setOldId()

    /**
     * Set the value of [sorting] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function setSorting($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sorting !== $v) {
            $this->sorting = $v;
            $this->modifiedColumns[CategoryTableMap::COL_SORTING] = true;
        }

        return $this;
    } // setSorting()

    /**
     * Set the value of [parent_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function setParentId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->parent_id !== $v) {
            $this->parent_id = $v;
            $this->modifiedColumns[CategoryTableMap::COL_PARENT_ID] = true;
        }

        return $this;
    } // setParentId()

    /**
     * Set the value of [tag] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function setTag($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tag !== $v) {
            $this->tag = $v;
            $this->modifiedColumns[CategoryTableMap::COL_TAG] = true;
        }

        return $this;
    } // setTag()

    /**
     * Set the value of [name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[CategoryTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [name_singular] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function setNameSingular($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name_singular !== $v) {
            $this->name_singular = $v;
            $this->modifiedColumns[CategoryTableMap::COL_NAME_SINGULAR] = true;
        }

        return $this;
    } // setNameSingular()

    /**
     * Set the value of [name_plural] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function setNamePlural($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name_plural !== $v) {
            $this->name_plural = $v;
            $this->modifiedColumns[CategoryTableMap::COL_NAME_PLURAL] = true;
        }

        return $this;
    } // setNamePlural()

    /**
     * Set the value of [description] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[CategoryTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Sets the value of the [has_image] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function setHasImage($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_image !== $v) {
            $this->has_image = $v;
            $this->modifiedColumns[CategoryTableMap::COL_HAS_IMAGE] = true;
        }

        return $this;
    } // setHasImage()

    /**
     * Sets the value of the [in_bestsellers] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function setInBestsellers($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->in_bestsellers !== $v) {
            $this->in_bestsellers = $v;
            $this->modifiedColumns[CategoryTableMap::COL_IN_BESTSELLERS] = true;
        }

        return $this;
    } // setInBestsellers()

    /**
     * Set the value of [image_ext] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function setImageExt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->image_ext !== $v) {
            $this->image_ext = $v;
            $this->modifiedColumns[CategoryTableMap::COL_IMAGE_EXT] = true;
        }

        return $this;
    } // setImageExt()

    /**
     * Set the value of [icon] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function setIcon($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->icon !== $v) {
            $this->icon = $v;
            $this->modifiedColumns[CategoryTableMap::COL_ICON] = true;
        }

        return $this;
    } // setIcon()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->has_image !== false) {
                return false;
            }

            if ($this->in_bestsellers !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : CategoryTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : CategoryTableMap::translateFieldName('OldId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->old_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : CategoryTableMap::translateFieldName('Sorting', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sorting = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : CategoryTableMap::translateFieldName('ParentId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->parent_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : CategoryTableMap::translateFieldName('Tag', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tag = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : CategoryTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : CategoryTableMap::translateFieldName('NameSingular', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name_singular = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : CategoryTableMap::translateFieldName('NamePlural', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name_plural = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : CategoryTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : CategoryTableMap::translateFieldName('HasImage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_image = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : CategoryTableMap::translateFieldName('InBestsellers', TableMap::TYPE_PHPNAME, $indexType)];
            $this->in_bestsellers = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : CategoryTableMap::translateFieldName('ImageExt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->image_ext = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : CategoryTableMap::translateFieldName('Icon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->icon = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 13; // 13 = CategoryTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Category\\Category'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CategoryTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildCategoryQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collProductMultiCategories = null;

            $this->collCategoryTranslations = null;

            $this->collProducts = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Category::setDeleted()
     * @see Category::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CategoryTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildCategoryQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(CategoryTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                CategoryTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->productMultiCategoriesScheduledForDeletion !== null) {
                if (!$this->productMultiCategoriesScheduledForDeletion->isEmpty()) {
                    \Model\Category\ProductMultiCategoryQuery::create()
                        ->filterByPrimaryKeys($this->productMultiCategoriesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productMultiCategoriesScheduledForDeletion = null;
                }
            }

            if ($this->collProductMultiCategories !== null) {
                foreach ($this->collProductMultiCategories as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->categoryTranslationsScheduledForDeletion !== null) {
                if (!$this->categoryTranslationsScheduledForDeletion->isEmpty()) {
                    \Model\Category\CategoryTranslationQuery::create()
                        ->filterByPrimaryKeys($this->categoryTranslationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->categoryTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collCategoryTranslations !== null) {
                foreach ($this->collCategoryTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productsScheduledForDeletion !== null) {
                if (!$this->productsScheduledForDeletion->isEmpty()) {
                    \Model\ProductQuery::create()
                        ->filterByPrimaryKeys($this->productsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productsScheduledForDeletion = null;
                }
            }

            if ($this->collProducts !== null) {
                foreach ($this->collProducts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[CategoryTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . CategoryTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(CategoryTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(CategoryTableMap::COL_OLD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'old_id';
        }
        if ($this->isColumnModified(CategoryTableMap::COL_SORTING)) {
            $modifiedColumns[':p' . $index++]  = 'sorting';
        }
        if ($this->isColumnModified(CategoryTableMap::COL_PARENT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'parent_id';
        }
        if ($this->isColumnModified(CategoryTableMap::COL_TAG)) {
            $modifiedColumns[':p' . $index++]  = 'tag';
        }
        if ($this->isColumnModified(CategoryTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(CategoryTableMap::COL_NAME_SINGULAR)) {
            $modifiedColumns[':p' . $index++]  = 'name_singular';
        }
        if ($this->isColumnModified(CategoryTableMap::COL_NAME_PLURAL)) {
            $modifiedColumns[':p' . $index++]  = 'name_plural';
        }
        if ($this->isColumnModified(CategoryTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }
        if ($this->isColumnModified(CategoryTableMap::COL_HAS_IMAGE)) {
            $modifiedColumns[':p' . $index++]  = 'has_image';
        }
        if ($this->isColumnModified(CategoryTableMap::COL_IN_BESTSELLERS)) {
            $modifiedColumns[':p' . $index++]  = 'in_bestsellers';
        }
        if ($this->isColumnModified(CategoryTableMap::COL_IMAGE_EXT)) {
            $modifiedColumns[':p' . $index++]  = 'image_ext';
        }
        if ($this->isColumnModified(CategoryTableMap::COL_ICON)) {
            $modifiedColumns[':p' . $index++]  = 'icon';
        }

        $sql = sprintf(
            'INSERT INTO category (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'old_id':
                        $stmt->bindValue($identifier, $this->old_id, PDO::PARAM_STR);
                        break;
                    case 'sorting':
                        $stmt->bindValue($identifier, $this->sorting, PDO::PARAM_INT);
                        break;
                    case 'parent_id':
                        $stmt->bindValue($identifier, $this->parent_id, PDO::PARAM_INT);
                        break;
                    case 'tag':
                        $stmt->bindValue($identifier, $this->tag, PDO::PARAM_STR);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'name_singular':
                        $stmt->bindValue($identifier, $this->name_singular, PDO::PARAM_STR);
                        break;
                    case 'name_plural':
                        $stmt->bindValue($identifier, $this->name_plural, PDO::PARAM_STR);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'has_image':
                        $stmt->bindValue($identifier, (int) $this->has_image, PDO::PARAM_INT);
                        break;
                    case 'in_bestsellers':
                        $stmt->bindValue($identifier, (int) $this->in_bestsellers, PDO::PARAM_INT);
                        break;
                    case 'image_ext':
                        $stmt->bindValue($identifier, $this->image_ext, PDO::PARAM_STR);
                        break;
                    case 'icon':
                        $stmt->bindValue($identifier, $this->icon, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CategoryTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getOldId();
                break;
            case 2:
                return $this->getSorting();
                break;
            case 3:
                return $this->getParentId();
                break;
            case 4:
                return $this->getTag();
                break;
            case 5:
                return $this->getName();
                break;
            case 6:
                return $this->getNameSingular();
                break;
            case 7:
                return $this->getNamePlural();
                break;
            case 8:
                return $this->getDescription();
                break;
            case 9:
                return $this->getHasImage();
                break;
            case 10:
                return $this->getInBestsellers();
                break;
            case 11:
                return $this->getImageExt();
                break;
            case 12:
                return $this->getIcon();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Category'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Category'][$this->hashCode()] = true;
        $keys = CategoryTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getOldId(),
            $keys[2] => $this->getSorting(),
            $keys[3] => $this->getParentId(),
            $keys[4] => $this->getTag(),
            $keys[5] => $this->getName(),
            $keys[6] => $this->getNameSingular(),
            $keys[7] => $this->getNamePlural(),
            $keys[8] => $this->getDescription(),
            $keys[9] => $this->getHasImage(),
            $keys[10] => $this->getInBestsellers(),
            $keys[11] => $this->getImageExt(),
            $keys[12] => $this->getIcon(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collProductMultiCategories) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productMultiCategories';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_multi_categories';
                        break;
                    default:
                        $key = 'ProductMultiCategories';
                }

                $result[$key] = $this->collProductMultiCategories->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCategoryTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'categoryTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'category_translations';
                        break;
                    default:
                        $key = 'CategoryTranslations';
                }

                $result[$key] = $this->collCategoryTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProducts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'products';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'products';
                        break;
                    default:
                        $key = 'Products';
                }

                $result[$key] = $this->collProducts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Category\Category
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = CategoryTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Category\Category
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setOldId($value);
                break;
            case 2:
                $this->setSorting($value);
                break;
            case 3:
                $this->setParentId($value);
                break;
            case 4:
                $this->setTag($value);
                break;
            case 5:
                $this->setName($value);
                break;
            case 6:
                $this->setNameSingular($value);
                break;
            case 7:
                $this->setNamePlural($value);
                break;
            case 8:
                $this->setDescription($value);
                break;
            case 9:
                $this->setHasImage($value);
                break;
            case 10:
                $this->setInBestsellers($value);
                break;
            case 11:
                $this->setImageExt($value);
                break;
            case 12:
                $this->setIcon($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = CategoryTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setOldId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setSorting($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setParentId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setTag($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setName($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setNameSingular($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setNamePlural($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setDescription($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setHasImage($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setInBestsellers($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setImageExt($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setIcon($arr[$keys[12]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Category\Category The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(CategoryTableMap::DATABASE_NAME);

        if ($this->isColumnModified(CategoryTableMap::COL_ID)) {
            $criteria->add(CategoryTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(CategoryTableMap::COL_OLD_ID)) {
            $criteria->add(CategoryTableMap::COL_OLD_ID, $this->old_id);
        }
        if ($this->isColumnModified(CategoryTableMap::COL_SORTING)) {
            $criteria->add(CategoryTableMap::COL_SORTING, $this->sorting);
        }
        if ($this->isColumnModified(CategoryTableMap::COL_PARENT_ID)) {
            $criteria->add(CategoryTableMap::COL_PARENT_ID, $this->parent_id);
        }
        if ($this->isColumnModified(CategoryTableMap::COL_TAG)) {
            $criteria->add(CategoryTableMap::COL_TAG, $this->tag);
        }
        if ($this->isColumnModified(CategoryTableMap::COL_NAME)) {
            $criteria->add(CategoryTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(CategoryTableMap::COL_NAME_SINGULAR)) {
            $criteria->add(CategoryTableMap::COL_NAME_SINGULAR, $this->name_singular);
        }
        if ($this->isColumnModified(CategoryTableMap::COL_NAME_PLURAL)) {
            $criteria->add(CategoryTableMap::COL_NAME_PLURAL, $this->name_plural);
        }
        if ($this->isColumnModified(CategoryTableMap::COL_DESCRIPTION)) {
            $criteria->add(CategoryTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(CategoryTableMap::COL_HAS_IMAGE)) {
            $criteria->add(CategoryTableMap::COL_HAS_IMAGE, $this->has_image);
        }
        if ($this->isColumnModified(CategoryTableMap::COL_IN_BESTSELLERS)) {
            $criteria->add(CategoryTableMap::COL_IN_BESTSELLERS, $this->in_bestsellers);
        }
        if ($this->isColumnModified(CategoryTableMap::COL_IMAGE_EXT)) {
            $criteria->add(CategoryTableMap::COL_IMAGE_EXT, $this->image_ext);
        }
        if ($this->isColumnModified(CategoryTableMap::COL_ICON)) {
            $criteria->add(CategoryTableMap::COL_ICON, $this->icon);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildCategoryQuery::create();
        $criteria->add(CategoryTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Category\Category (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOldId($this->getOldId());
        $copyObj->setSorting($this->getSorting());
        $copyObj->setParentId($this->getParentId());
        $copyObj->setTag($this->getTag());
        $copyObj->setName($this->getName());
        $copyObj->setNameSingular($this->getNameSingular());
        $copyObj->setNamePlural($this->getNamePlural());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setHasImage($this->getHasImage());
        $copyObj->setInBestsellers($this->getInBestsellers());
        $copyObj->setImageExt($this->getImageExt());
        $copyObj->setIcon($this->getIcon());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getProductMultiCategories() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductMultiCategory($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCategoryTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCategoryTranslation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProducts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProduct($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Category\Category Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ProductMultiCategory' === $relationName) {
            $this->initProductMultiCategories();
            return;
        }
        if ('CategoryTranslation' === $relationName) {
            $this->initCategoryTranslations();
            return;
        }
        if ('Product' === $relationName) {
            $this->initProducts();
            return;
        }
    }

    /**
     * Clears out the collProductMultiCategories collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductMultiCategories()
     */
    public function clearProductMultiCategories()
    {
        $this->collProductMultiCategories = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductMultiCategories collection loaded partially.
     */
    public function resetPartialProductMultiCategories($v = true)
    {
        $this->collProductMultiCategoriesPartial = $v;
    }

    /**
     * Initializes the collProductMultiCategories collection.
     *
     * By default this just sets the collProductMultiCategories collection to an empty array (like clearcollProductMultiCategories());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductMultiCategories($overrideExisting = true)
    {
        if (null !== $this->collProductMultiCategories && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductMultiCategoryTableMap::getTableMap()->getCollectionClassName();

        $this->collProductMultiCategories = new $collectionClassName;
        $this->collProductMultiCategories->setModel('\Model\Category\ProductMultiCategory');
    }

    /**
     * Gets an array of ChildProductMultiCategory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCategory is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProductMultiCategory[] List of ChildProductMultiCategory objects
     * @throws PropelException
     */
    public function getProductMultiCategories(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductMultiCategoriesPartial && !$this->isNew();
        if (null === $this->collProductMultiCategories || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductMultiCategories) {
                    $this->initProductMultiCategories();
                } else {
                    $collectionClassName = ProductMultiCategoryTableMap::getTableMap()->getCollectionClassName();

                    $collProductMultiCategories = new $collectionClassName;
                    $collProductMultiCategories->setModel('\Model\Category\ProductMultiCategory');

                    return $collProductMultiCategories;
                }
            } else {
                $collProductMultiCategories = ChildProductMultiCategoryQuery::create(null, $criteria)
                    ->filterByCategory($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductMultiCategoriesPartial && count($collProductMultiCategories)) {
                        $this->initProductMultiCategories(false);

                        foreach ($collProductMultiCategories as $obj) {
                            if (false == $this->collProductMultiCategories->contains($obj)) {
                                $this->collProductMultiCategories->append($obj);
                            }
                        }

                        $this->collProductMultiCategoriesPartial = true;
                    }

                    return $collProductMultiCategories;
                }

                if ($partial && $this->collProductMultiCategories) {
                    foreach ($this->collProductMultiCategories as $obj) {
                        if ($obj->isNew()) {
                            $collProductMultiCategories[] = $obj;
                        }
                    }
                }

                $this->collProductMultiCategories = $collProductMultiCategories;
                $this->collProductMultiCategoriesPartial = false;
            }
        }

        return $this->collProductMultiCategories;
    }

    /**
     * Sets a collection of ChildProductMultiCategory objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productMultiCategories A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCategory The current object (for fluent API support)
     */
    public function setProductMultiCategories(Collection $productMultiCategories, ConnectionInterface $con = null)
    {
        /** @var ChildProductMultiCategory[] $productMultiCategoriesToDelete */
        $productMultiCategoriesToDelete = $this->getProductMultiCategories(new Criteria(), $con)->diff($productMultiCategories);


        $this->productMultiCategoriesScheduledForDeletion = $productMultiCategoriesToDelete;

        foreach ($productMultiCategoriesToDelete as $productMultiCategoryRemoved) {
            $productMultiCategoryRemoved->setCategory(null);
        }

        $this->collProductMultiCategories = null;
        foreach ($productMultiCategories as $productMultiCategory) {
            $this->addProductMultiCategory($productMultiCategory);
        }

        $this->collProductMultiCategories = $productMultiCategories;
        $this->collProductMultiCategoriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProductMultiCategory objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProductMultiCategory objects.
     * @throws PropelException
     */
    public function countProductMultiCategories(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductMultiCategoriesPartial && !$this->isNew();
        if (null === $this->collProductMultiCategories || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductMultiCategories) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductMultiCategories());
            }

            $query = ChildProductMultiCategoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCategory($this)
                ->count($con);
        }

        return count($this->collProductMultiCategories);
    }

    /**
     * Method called to associate a ChildProductMultiCategory object to this object
     * through the ChildProductMultiCategory foreign key attribute.
     *
     * @param  ChildProductMultiCategory $l ChildProductMultiCategory
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function addProductMultiCategory(ChildProductMultiCategory $l)
    {
        if ($this->collProductMultiCategories === null) {
            $this->initProductMultiCategories();
            $this->collProductMultiCategoriesPartial = true;
        }

        if (!$this->collProductMultiCategories->contains($l)) {
            $this->doAddProductMultiCategory($l);

            if ($this->productMultiCategoriesScheduledForDeletion and $this->productMultiCategoriesScheduledForDeletion->contains($l)) {
                $this->productMultiCategoriesScheduledForDeletion->remove($this->productMultiCategoriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProductMultiCategory $productMultiCategory The ChildProductMultiCategory object to add.
     */
    protected function doAddProductMultiCategory(ChildProductMultiCategory $productMultiCategory)
    {
        $this->collProductMultiCategories[]= $productMultiCategory;
        $productMultiCategory->setCategory($this);
    }

    /**
     * @param  ChildProductMultiCategory $productMultiCategory The ChildProductMultiCategory object to remove.
     * @return $this|ChildCategory The current object (for fluent API support)
     */
    public function removeProductMultiCategory(ChildProductMultiCategory $productMultiCategory)
    {
        if ($this->getProductMultiCategories()->contains($productMultiCategory)) {
            $pos = $this->collProductMultiCategories->search($productMultiCategory);
            $this->collProductMultiCategories->remove($pos);
            if (null === $this->productMultiCategoriesScheduledForDeletion) {
                $this->productMultiCategoriesScheduledForDeletion = clone $this->collProductMultiCategories;
                $this->productMultiCategoriesScheduledForDeletion->clear();
            }
            $this->productMultiCategoriesScheduledForDeletion[]= clone $productMultiCategory;
            $productMultiCategory->setCategory(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Category is new, it will return
     * an empty collection; or if this Category has previously
     * been saved, it will retrieve related ProductMultiCategories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Category.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProductMultiCategory[] List of ChildProductMultiCategory objects
     */
    public function getProductMultiCategoriesJoinProduct(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProductMultiCategoryQuery::create(null, $criteria);
        $query->joinWith('Product', $joinBehavior);

        return $this->getProductMultiCategories($query, $con);
    }

    /**
     * Clears out the collCategoryTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCategoryTranslations()
     */
    public function clearCategoryTranslations()
    {
        $this->collCategoryTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCategoryTranslations collection loaded partially.
     */
    public function resetPartialCategoryTranslations($v = true)
    {
        $this->collCategoryTranslationsPartial = $v;
    }

    /**
     * Initializes the collCategoryTranslations collection.
     *
     * By default this just sets the collCategoryTranslations collection to an empty array (like clearcollCategoryTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCategoryTranslations($overrideExisting = true)
    {
        if (null !== $this->collCategoryTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = CategoryTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collCategoryTranslations = new $collectionClassName;
        $this->collCategoryTranslations->setModel('\Model\Category\CategoryTranslation');
    }

    /**
     * Gets an array of ChildCategoryTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCategory is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCategoryTranslation[] List of ChildCategoryTranslation objects
     * @throws PropelException
     */
    public function getCategoryTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCategoryTranslationsPartial && !$this->isNew();
        if (null === $this->collCategoryTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCategoryTranslations) {
                    $this->initCategoryTranslations();
                } else {
                    $collectionClassName = CategoryTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collCategoryTranslations = new $collectionClassName;
                    $collCategoryTranslations->setModel('\Model\Category\CategoryTranslation');

                    return $collCategoryTranslations;
                }
            } else {
                $collCategoryTranslations = ChildCategoryTranslationQuery::create(null, $criteria)
                    ->filterByCategory($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCategoryTranslationsPartial && count($collCategoryTranslations)) {
                        $this->initCategoryTranslations(false);

                        foreach ($collCategoryTranslations as $obj) {
                            if (false == $this->collCategoryTranslations->contains($obj)) {
                                $this->collCategoryTranslations->append($obj);
                            }
                        }

                        $this->collCategoryTranslationsPartial = true;
                    }

                    return $collCategoryTranslations;
                }

                if ($partial && $this->collCategoryTranslations) {
                    foreach ($this->collCategoryTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collCategoryTranslations[] = $obj;
                        }
                    }
                }

                $this->collCategoryTranslations = $collCategoryTranslations;
                $this->collCategoryTranslationsPartial = false;
            }
        }

        return $this->collCategoryTranslations;
    }

    /**
     * Sets a collection of ChildCategoryTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $categoryTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCategory The current object (for fluent API support)
     */
    public function setCategoryTranslations(Collection $categoryTranslations, ConnectionInterface $con = null)
    {
        /** @var ChildCategoryTranslation[] $categoryTranslationsToDelete */
        $categoryTranslationsToDelete = $this->getCategoryTranslations(new Criteria(), $con)->diff($categoryTranslations);


        $this->categoryTranslationsScheduledForDeletion = $categoryTranslationsToDelete;

        foreach ($categoryTranslationsToDelete as $categoryTranslationRemoved) {
            $categoryTranslationRemoved->setCategory(null);
        }

        $this->collCategoryTranslations = null;
        foreach ($categoryTranslations as $categoryTranslation) {
            $this->addCategoryTranslation($categoryTranslation);
        }

        $this->collCategoryTranslations = $categoryTranslations;
        $this->collCategoryTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CategoryTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CategoryTranslation objects.
     * @throws PropelException
     */
    public function countCategoryTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCategoryTranslationsPartial && !$this->isNew();
        if (null === $this->collCategoryTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCategoryTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCategoryTranslations());
            }

            $query = ChildCategoryTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCategory($this)
                ->count($con);
        }

        return count($this->collCategoryTranslations);
    }

    /**
     * Method called to associate a ChildCategoryTranslation object to this object
     * through the ChildCategoryTranslation foreign key attribute.
     *
     * @param  ChildCategoryTranslation $l ChildCategoryTranslation
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function addCategoryTranslation(ChildCategoryTranslation $l)
    {
        if ($this->collCategoryTranslations === null) {
            $this->initCategoryTranslations();
            $this->collCategoryTranslationsPartial = true;
        }

        if (!$this->collCategoryTranslations->contains($l)) {
            $this->doAddCategoryTranslation($l);

            if ($this->categoryTranslationsScheduledForDeletion and $this->categoryTranslationsScheduledForDeletion->contains($l)) {
                $this->categoryTranslationsScheduledForDeletion->remove($this->categoryTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCategoryTranslation $categoryTranslation The ChildCategoryTranslation object to add.
     */
    protected function doAddCategoryTranslation(ChildCategoryTranslation $categoryTranslation)
    {
        $this->collCategoryTranslations[]= $categoryTranslation;
        $categoryTranslation->setCategory($this);
    }

    /**
     * @param  ChildCategoryTranslation $categoryTranslation The ChildCategoryTranslation object to remove.
     * @return $this|ChildCategory The current object (for fluent API support)
     */
    public function removeCategoryTranslation(ChildCategoryTranslation $categoryTranslation)
    {
        if ($this->getCategoryTranslations()->contains($categoryTranslation)) {
            $pos = $this->collCategoryTranslations->search($categoryTranslation);
            $this->collCategoryTranslations->remove($pos);
            if (null === $this->categoryTranslationsScheduledForDeletion) {
                $this->categoryTranslationsScheduledForDeletion = clone $this->collCategoryTranslations;
                $this->categoryTranslationsScheduledForDeletion->clear();
            }
            $this->categoryTranslationsScheduledForDeletion[]= $categoryTranslation;
            $categoryTranslation->setCategory(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Category is new, it will return
     * an empty collection; or if this Category has previously
     * been saved, it will retrieve related CategoryTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Category.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCategoryTranslation[] List of ChildCategoryTranslation objects
     */
    public function getCategoryTranslationsJoinLanguage(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCategoryTranslationQuery::create(null, $criteria);
        $query->joinWith('Language', $joinBehavior);

        return $this->getCategoryTranslations($query, $con);
    }

    /**
     * Clears out the collProducts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProducts()
     */
    public function clearProducts()
    {
        $this->collProducts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProducts collection loaded partially.
     */
    public function resetPartialProducts($v = true)
    {
        $this->collProductsPartial = $v;
    }

    /**
     * Initializes the collProducts collection.
     *
     * By default this just sets the collProducts collection to an empty array (like clearcollProducts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProducts($overrideExisting = true)
    {
        if (null !== $this->collProducts && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductTableMap::getTableMap()->getCollectionClassName();

        $this->collProducts = new $collectionClassName;
        $this->collProducts->setModel('\Model\Product');
    }

    /**
     * Gets an array of Product objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildCategory is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Product[] List of Product objects
     * @throws PropelException
     */
    public function getProducts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductsPartial && !$this->isNew();
        if (null === $this->collProducts || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProducts) {
                    $this->initProducts();
                } else {
                    $collectionClassName = ProductTableMap::getTableMap()->getCollectionClassName();

                    $collProducts = new $collectionClassName;
                    $collProducts->setModel('\Model\Product');

                    return $collProducts;
                }
            } else {
                $collProducts = ProductQuery::create(null, $criteria)
                    ->filterByCategory($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductsPartial && count($collProducts)) {
                        $this->initProducts(false);

                        foreach ($collProducts as $obj) {
                            if (false == $this->collProducts->contains($obj)) {
                                $this->collProducts->append($obj);
                            }
                        }

                        $this->collProductsPartial = true;
                    }

                    return $collProducts;
                }

                if ($partial && $this->collProducts) {
                    foreach ($this->collProducts as $obj) {
                        if ($obj->isNew()) {
                            $collProducts[] = $obj;
                        }
                    }
                }

                $this->collProducts = $collProducts;
                $this->collProductsPartial = false;
            }
        }

        return $this->collProducts;
    }

    /**
     * Sets a collection of Product objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $products A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildCategory The current object (for fluent API support)
     */
    public function setProducts(Collection $products, ConnectionInterface $con = null)
    {
        /** @var Product[] $productsToDelete */
        $productsToDelete = $this->getProducts(new Criteria(), $con)->diff($products);


        $this->productsScheduledForDeletion = $productsToDelete;

        foreach ($productsToDelete as $productRemoved) {
            $productRemoved->setCategory(null);
        }

        $this->collProducts = null;
        foreach ($products as $product) {
            $this->addProduct($product);
        }

        $this->collProducts = $products;
        $this->collProductsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseProduct objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseProduct objects.
     * @throws PropelException
     */
    public function countProducts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductsPartial && !$this->isNew();
        if (null === $this->collProducts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProducts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProducts());
            }

            $query = ProductQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByCategory($this)
                ->count($con);
        }

        return count($this->collProducts);
    }

    /**
     * Method called to associate a Product object to this object
     * through the Product foreign key attribute.
     *
     * @param  Product $l Product
     * @return $this|\Model\Category\Category The current object (for fluent API support)
     */
    public function addProduct(Product $l)
    {
        if ($this->collProducts === null) {
            $this->initProducts();
            $this->collProductsPartial = true;
        }

        if (!$this->collProducts->contains($l)) {
            $this->doAddProduct($l);

            if ($this->productsScheduledForDeletion and $this->productsScheduledForDeletion->contains($l)) {
                $this->productsScheduledForDeletion->remove($this->productsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Product $product The Product object to add.
     */
    protected function doAddProduct(Product $product)
    {
        $this->collProducts[]= $product;
        $product->setCategory($this);
    }

    /**
     * @param  Product $product The Product object to remove.
     * @return $this|ChildCategory The current object (for fluent API support)
     */
    public function removeProduct(Product $product)
    {
        if ($this->getProducts()->contains($product)) {
            $pos = $this->collProducts->search($product);
            $this->collProducts->remove($pos);
            if (null === $this->productsScheduledForDeletion) {
                $this->productsScheduledForDeletion = clone $this->collProducts;
                $this->productsScheduledForDeletion->clear();
            }
            $this->productsScheduledForDeletion[]= clone $product;
            $product->setCategory(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Category is new, it will return
     * an empty collection; or if this Category has previously
     * been saved, it will retrieve related Products from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Category.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Product[] List of Product objects
     */
    public function getProductsJoinBrand(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductQuery::create(null, $criteria);
        $query->joinWith('Brand', $joinBehavior);

        return $this->getProducts($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Category is new, it will return
     * an empty collection; or if this Category has previously
     * been saved, it will retrieve related Products from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Category.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Product[] List of Product objects
     */
    public function getProductsJoinProductUnit(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductQuery::create(null, $criteria);
        $query->joinWith('ProductUnit', $joinBehavior);

        return $this->getProducts($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Category is new, it will return
     * an empty collection; or if this Category has previously
     * been saved, it will retrieve related Products from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Category.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Product[] List of Product objects
     */
    public function getProductsJoinSupplierRef(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductQuery::create(null, $criteria);
        $query->joinWith('SupplierRef', $joinBehavior);

        return $this->getProducts($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Category is new, it will return
     * an empty collection; or if this Category has previously
     * been saved, it will retrieve related Products from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Category.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Product[] List of Product objects
     */
    public function getProductsJoinUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getProducts($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Category is new, it will return
     * an empty collection; or if this Category has previously
     * been saved, it will retrieve related Products from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Category.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Product[] List of Product objects
     */
    public function getProductsJoinVat(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductQuery::create(null, $criteria);
        $query->joinWith('Vat', $joinBehavior);

        return $this->getProducts($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->old_id = null;
        $this->sorting = null;
        $this->parent_id = null;
        $this->tag = null;
        $this->name = null;
        $this->name_singular = null;
        $this->name_plural = null;
        $this->description = null;
        $this->has_image = null;
        $this->in_bestsellers = null;
        $this->image_ext = null;
        $this->icon = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collProductMultiCategories) {
                foreach ($this->collProductMultiCategories as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCategoryTranslations) {
                foreach ($this->collCategoryTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProducts) {
                foreach ($this->collProducts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collProductMultiCategories = null;
        $this->collCategoryTranslations = null;
        $this->collProducts = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(CategoryTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
