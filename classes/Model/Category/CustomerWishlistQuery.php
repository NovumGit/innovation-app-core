<?php

namespace Model\Category;

use Model\Category\Base\CustomerWishlistQuery as BaseCustomerWishlistQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'customer_wishlist' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomerWishlistQuery extends BaseCustomerWishlistQuery
{
    static function removeItem($iProductId, $iCustomerId)
    {
        self::create()->filterByCustomerId($iCustomerId)->filterByProductId($iProductId)->delete();

        $aWishlistItems = CustomerWishlistQuery::create()->orderBySort(Criteria::ASC)->findByCustomerId($iCustomerId);

        $i = 0;
        foreach($aWishlistItems as $oWishlistItem)
        {
            $i++;
            $oWishlistItem->setSort($i);
            $oWishlistItem->save();
        }
    }
    static function addItem($iProductId, $iCustomerId)
    {
        $oCustomerWishList = self::create()->filterByCustomerId($iCustomerId)->filterByProductId($iProductId)->findOne();

        if($oCustomerWishList instanceof CustomerWishlist)
        {
            $oCustomerWishList->delete();

            $aWishlistItems = CustomerWishlistQuery::create()->orderBySort(Criteria::ASC)->findByCustomerId($iCustomerId);

            $i = 0;
            foreach($aWishlistItems as $oWishlistItem)
            {
                $i++;
                $oWishlistItem->setSort($i);
                $oWishlistItem->save();
            }
        }
        $oLastWishlistItems = CustomerWishlistQuery::create()->orderBySort(Criteria::DESC)->findOneByCustomerId($iCustomerId);
        if($oLastWishlistItems instanceof CustomerWishlist)
        {
            $iLastSort = $oLastWishlistItems->getSort();
        }
        else
        {
            $iLastSort = 0;
        }
        $oNewWishListItem = new CustomerWishlist();
        $oNewWishListItem->setCustomerId($iCustomerId);
        $oNewWishListItem->setSort($iLastSort+1);
        $oNewWishListItem->setProductId($iProductId);
        $oNewWishListItem->save();
    }
}
