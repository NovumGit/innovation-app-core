<?php

namespace Model\Category;

use Core\Utils;
use Model\Category\Base\CategoryTranslation as BaseCategoryTranslation;

/**
 * Skeleton subclass for representing a row from the 'category_translation' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CategoryTranslation extends BaseCategoryTranslation
{
    function getShortenedName($iCount)
    {
        if(strlen($this->getName()) > $iCount)
        {
            return trim(substr($this->getName(), 0, $iCount -3)).'...';
        }
        return $this->getName();
    }
    function getCrumblepath($aRecursiveOut = [])
    {
        $oCategory = $this->getCategory();
        $oLanguage = $this->getLanguage();

        if($oCategory->getParentId())
        {
            $oCategoryTranslationQuery = CategoryTranslationQuery::create();
            $oCategoryTranslationQuery->filterByLanguageId($oLanguage->getId());
            $oCategoryTranslationQuery->filterByCategoryId($oCategory->getParentId());
            $oCategoryTranslation = $oCategoryTranslationQuery->findOne();

            $aRecursiveOut = $oCategoryTranslation->getCrumblepath($aRecursiveOut);

        }
        $aRecursiveOut[] = '<a href="/'.$oLanguage->getShopUrlPrefix().$oCategory->getUrl().'">'.$this->getShortenedName(31).'</a>';


        return $aRecursiveOut;

    }
    function getWebshopUrl()
    {
        if($this->getUrl())
        {
            $sUrl = $this->getUrl();
        }
        else
        {
            $sUrl = Utils::slugify($this->getName());
        }

        return '/c/'.$sUrl.'-'.$this->getCategoryId();
    }

}
