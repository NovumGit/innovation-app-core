<?php

namespace Model\Category;

use Core\Utils;
use Model\Category\Base\Category as BaseCategory;
use Propel\Runtime\Collection\ObjectCollection;

/**
 * Skeleton subclass for representing a row from the 'category' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Category extends BaseCategory
{

    function getCountMultiCategoryProducts()
    {
        if(!$this->getId())
        {
            return 0;
        }
        $iCategoryId = $this->getId();
        return ProductMultiCategoryQuery::create()->findByCategoryId($iCategoryId)->count();
    }
    function getSubCategories(): ObjectCollection
    {
        $aCategories = CategoryQuery::create()->findByParentId($this->getId());
        return $aCategories;
    }
    function getTranslation($iLanguageId)
    {
        $oCategoryTranslationQuery = CategoryTranslationQuery::create();
        $oCategoryTranslationQuery->filterByCategoryId($this->getId());
        $oCategoryTranslationQuery->filterByLanguageId($iLanguageId);
        return $oCategoryTranslationQuery->findOne();
    }
    function countMultiCategoryLinkedProducts()
    {
        return ProductMultiCategoryQuery::create()->filterByCategoryId($this->getId())->count();
    }

    function getImagePath()
    {
        if($this->getHasImage())
        {
            return '/img/category/'.$this->getId().'.'.$this->getImageExt();
        }
//        throw new LogicException("This category has no image, the developer did something stupid, he should have checked that first.");
    }
    function getUrl()
    {
        return '/c/'.Utils::slugify($this->getName()).'-'.$this->getId();
    }
}
