<?php

namespace Model\Sale;

use Model\Sale\Base\OrderItemProduct as BaseOrderItemProduct;

/**
 * Skeleton subclass for representing a row from the 'sale_order_item_product' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class OrderItemProduct extends BaseOrderItemProduct
{

}
