<?php

namespace Model\Sale\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Account\User;
use Model\Account\UserQuery;
use Model\Cms\Site;
use Model\Cms\SiteQuery;
use Model\Finance\Payment;
use Model\Finance\PaymentQuery;
use Model\Finance\Base\Payment as BasePayment;
use Model\Finance\Map\PaymentTableMap;
use Model\Sale\SaleOrder as ChildSaleOrder;
use Model\Sale\SaleOrderEmail as ChildSaleOrderEmail;
use Model\Sale\SaleOrderEmailQuery as ChildSaleOrderEmailQuery;
use Model\Sale\SaleOrderItem as ChildSaleOrderItem;
use Model\Sale\SaleOrderItemQuery as ChildSaleOrderItemQuery;
use Model\Sale\SaleOrderPayment as ChildSaleOrderPayment;
use Model\Sale\SaleOrderPaymentQuery as ChildSaleOrderPaymentQuery;
use Model\Sale\SaleOrderQuery as ChildSaleOrderQuery;
use Model\SaleOrderNotification\SaleOrderNotification;
use Model\SaleOrderNotification\SaleOrderNotificationQuery;
use Model\SaleOrderNotification\Base\SaleOrderNotification as BaseSaleOrderNotification;
use Model\SaleOrderNotification\Map\SaleOrderNotificationTableMap;
use Model\Sale\Map\SaleOrderEmailTableMap;
use Model\Sale\Map\SaleOrderItemTableMap;
use Model\Sale\Map\SaleOrderPaymentTableMap;
use Model\Sale\Map\SaleOrderTableMap;
use Model\Setting\MasterTable\SaleOrderStatus;
use Model\Setting\MasterTable\SaleOrderStatusQuery;
use Model\Setting\MasterTable\ShippingMethod;
use Model\Setting\MasterTable\ShippingMethodQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'sale_order' table.
 *
 *
 *
 * @package    propel.generator.Model.Sale.Base
 */
abstract class SaleOrder implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Sale\\Map\\SaleOrderTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the site_id field.
     *
     * @var        int|null
     */
    protected $site_id;

    /**
     * The value for the language_id field.
     *
     * @var        int|null
     */
    protected $language_id;

    /**
     * The value for the is_closed field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_closed;

    /**
     * The value for the is_fully_paid field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_fully_paid;

    /**
     * The value for the pay_date field.
     *
     * @var        DateTime|null
     */
    protected $pay_date;

    /**
     * The value for the is_ready_for_shipping field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_ready_for_shipping;

    /**
     * The value for the is_shipping_checked field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_shipping_checked;

    /**
     * The value for the is_fully_shipped field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_fully_shipped;

    /**
     * The value for the is_pushed_to_accounting field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_pushed_to_accounting;

    /**
     * The value for the ship_date field.
     *
     * @var        DateTime|null
     */
    protected $ship_date;

    /**
     * The value for the sale_order_status_id field.
     *
     * Note: this column has a database default value of: 0
     * @var        int|null
     */
    protected $sale_order_status_id;

    /**
     * The value for the handled_by field.
     *
     * @var        string|null
     */
    protected $handled_by;

    /**
     * The value for the source field.
     *
     * Note: this column has a database default value of: 'intern'
     * @var        string|null
     */
    protected $source;

    /**
     * The value for the is_completed field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_completed;

    /**
     * The value for the created_by_user_id field.
     *
     * @var        int|null
     */
    protected $created_by_user_id;

    /**
     * The value for the created_on field.
     *
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        DateTime|null
     */
    protected $created_on;

    /**
     * The value for the estimated_delivery_date field.
     *
     * @var        DateTime|null
     */
    protected $estimated_delivery_date;

    /**
     * The value for the customer_id field.
     *
     * @var        int|null
     */
    protected $customer_id;

    /**
     * The value for the envelope_weight_grams field.
     *
     * @var        int|null
     */
    protected $envelope_weight_grams;

    /**
     * The value for the webshop_user_session_id field.
     *
     * @var        int|null
     */
    protected $webshop_user_session_id;

    /**
     * The value for the invoice_number field.
     *
     * @var        string|null
     */
    protected $invoice_number;

    /**
     * The value for the invoice_date field.
     *
     * @var        DateTime|null
     */
    protected $invoice_date;

    /**
     * The value for the payterm_days field.
     *
     * @var        int|null
     */
    protected $payterm_days;

    /**
     * The value for the payterm_original_id field.
     *
     * @var        int|null
     */
    protected $payterm_original_id;

    /**
     * The value for the payterm_string field.
     *
     * @var        string|null
     */
    protected $payterm_string;

    /**
     * The value for the order_number field.
     *
     * Note: this column has a database default value of: 'false'
     * @var        string|null
     */
    protected $order_number;

    /**
     * The value for the is_deleted field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_deleted;

    /**
     * The value for the company_name field.
     *
     * @var        string|null
     */
    protected $company_name;

    /**
     * The value for the debitor field.
     *
     * @var        string|null
     */
    protected $debitor;

    /**
     * The value for the our_custom_folder field.
     *
     * @var        string|null
     */
    protected $our_custom_folder;

    /**
     * The value for the our_slogan field.
     *
     * @var        string|null
     */
    protected $our_slogan;

    /**
     * The value for the our_phone field.
     *
     * @var        string|null
     */
    protected $our_phone;

    /**
     * The value for the our_fax field.
     *
     * @var        string|null
     */
    protected $our_fax;

    /**
     * The value for the our_website field.
     *
     * @var        string|null
     */
    protected $our_website;

    /**
     * The value for the our_email field.
     *
     * @var        string|null
     */
    protected $our_email;

    /**
     * The value for the our_vat_number field.
     *
     * @var        string|null
     */
    protected $our_vat_number;

    /**
     * The value for the our_chamber_of_commerce field.
     *
     * @var        string|null
     */
    protected $our_chamber_of_commerce;

    /**
     * The value for the our_iban_number field.
     *
     * @var        string|null
     */
    protected $our_iban_number;

    /**
     * The value for the our_bic_number field.
     *
     * @var        string|null
     */
    protected $our_bic_number;

    /**
     * The value for the our_general_company_name field.
     *
     * @var        string|null
     */
    protected $our_general_company_name;

    /**
     * The value for the our_general_street field.
     *
     * @var        string|null
     */
    protected $our_general_street;

    /**
     * The value for the our_general_number field.
     *
     * @var        string|null
     */
    protected $our_general_number;

    /**
     * The value for the our_general_number_add field.
     *
     * @var        string|null
     */
    protected $our_general_number_add;

    /**
     * The value for the our_general_postal field.
     *
     * @var        string|null
     */
    protected $our_general_postal;

    /**
     * The value for the our_general_po_box field.
     *
     * @var        string|null
     */
    protected $our_general_po_box;

    /**
     * The value for the our_general_city field.
     *
     * @var        string|null
     */
    protected $our_general_city;

    /**
     * The value for the our_general_country field.
     *
     * @var        string|null
     */
    protected $our_general_country;

    /**
     * The value for the customer_phone field.
     *
     * @var        string|null
     */
    protected $customer_phone;

    /**
     * The value for the customer_fax field.
     *
     * @var        string|null
     */
    protected $customer_fax;

    /**
     * The value for the customer_email field.
     *
     * @var        string|null
     */
    protected $customer_email;

    /**
     * The value for the customer_chamber_of_commerce field.
     *
     * @var        string|null
     */
    protected $customer_chamber_of_commerce;

    /**
     * The value for the customer_vat_number field.
     *
     * @var        string|null
     */
    protected $customer_vat_number;

    /**
     * The value for the customer_order_reference field.
     *
     * @var        string|null
     */
    protected $customer_order_reference;

    /**
     * The value for the customer_order_placed_by field.
     *
     * @var        string|null
     */
    protected $customer_order_placed_by;

    /**
     * The value for the customer_invoice_address_id field.
     *
     * @var        int|null
     */
    protected $customer_invoice_address_id;

    /**
     * The value for the customer_invoice_company_name field.
     *
     * @var        string|null
     */
    protected $customer_invoice_company_name;

    /**
     * The value for the customer_invoice_attn_name field.
     *
     * @var        string|null
     */
    protected $customer_invoice_attn_name;

    /**
     * The value for the customer_invoice_street field.
     *
     * @var        string|null
     */
    protected $customer_invoice_street;

    /**
     * The value for the customer_invoice_number field.
     *
     * @var        string|null
     */
    protected $customer_invoice_number;

    /**
     * The value for the customer_invoice_number_add field.
     *
     * @var        string|null
     */
    protected $customer_invoice_number_add;

    /**
     * The value for the customer_invoice_postal field.
     *
     * @var        string|null
     */
    protected $customer_invoice_postal;

    /**
     * The value for the customer_invoice_city field.
     *
     * @var        string|null
     */
    protected $customer_invoice_city;

    /**
     * The value for the customer_invoice_country field.
     *
     * @var        string|null
     */
    protected $customer_invoice_country;

    /**
     * The value for the customer_invoice_usa_state field.
     *
     * @var        string|null
     */
    protected $customer_invoice_usa_state;

    /**
     * The value for the customer_invoice_address_l1 field.
     *
     * @var        string|null
     */
    protected $customer_invoice_address_l1;

    /**
     * The value for the customer_invoice_address_l2 field.
     *
     * @var        string|null
     */
    protected $customer_invoice_address_l2;

    /**
     * The value for the customer_delivery_address_id field.
     *
     * @var        int|null
     */
    protected $customer_delivery_address_id;

    /**
     * The value for the customer_delivery_company_name field.
     *
     * @var        string|null
     */
    protected $customer_delivery_company_name;

    /**
     * The value for the customer_delivery_attn_name field.
     *
     * @var        string|null
     */
    protected $customer_delivery_attn_name;

    /**
     * The value for the customer_delivery_street field.
     *
     * @var        string|null
     */
    protected $customer_delivery_street;

    /**
     * The value for the customer_delivery_number field.
     *
     * @var        string|null
     */
    protected $customer_delivery_number;

    /**
     * The value for the customer_delivery_number_add field.
     *
     * @var        string|null
     */
    protected $customer_delivery_number_add;

    /**
     * The value for the customer_delivery_postal field.
     *
     * @var        string|null
     */
    protected $customer_delivery_postal;

    /**
     * The value for the customer_delivery_city field.
     *
     * @var        string|null
     */
    protected $customer_delivery_city;

    /**
     * The value for the customer_delivery_country field.
     *
     * @var        string|null
     */
    protected $customer_delivery_country;

    /**
     * The value for the customer_delivery_usa_state field.
     *
     * @var        string|null
     */
    protected $customer_delivery_usa_state;

    /**
     * The value for the customer_delivery_address_l1 field.
     *
     * @var        string|null
     */
    protected $customer_delivery_address_l1;

    /**
     * The value for the customer_delivery_address_l2 field.
     *
     * @var        string|null
     */
    protected $customer_delivery_address_l2;

    /**
     * The value for the cp_first_name field.
     *
     * @var        string|null
     */
    protected $cp_first_name;

    /**
     * The value for the cp_last_name field.
     *
     * @var        string|null
     */
    protected $cp_last_name;

    /**
     * The value for the cp_sallutation field.
     *
     * @var        string|null
     */
    protected $cp_sallutation;

    /**
     * The value for the reference field.
     *
     * @var        string|null
     */
    protected $reference;

    /**
     * The value for the shipping_note field.
     *
     * @var        string|null
     */
    protected $shipping_note;

    /**
     * The value for the customer_order_note field.
     *
     * @var        string|null
     */
    protected $customer_order_note;

    /**
     * The value for the shipping_order_note field.
     *
     * @var        string|null
     */
    protected $shipping_order_note;

    /**
     * The value for the has_wrap field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $has_wrap;

    /**
     * The value for the wrapping_price field.
     *
     * Note: this column has a database default value of: 0.0
     * @var        double|null
     */
    protected $wrapping_price;

    /**
     * The value for the shipping_carrier field.
     *
     * @var        string|null
     */
    protected $shipping_carrier;

    /**
     * The value for the shipping_price field.
     *
     * @var        double|null
     */
    protected $shipping_price;

    /**
     * The value for the shipping_method_id field.
     *
     * @var        int|null
     */
    protected $shipping_method_id;

    /**
     * The value for the shipping_has_track_trace field.
     *
     * @var        boolean|null
     */
    protected $shipping_has_track_trace;

    /**
     * The value for the shipping_id field.
     *
     * @var        double|null
     */
    protected $shipping_id;

    /**
     * The value for the tracking_code field.
     *
     * @var        string|null
     */
    protected $tracking_code;

    /**
     * The value for the tracking_url field.
     *
     * @var        string|null
     */
    protected $tracking_url;

    /**
     * The value for the paymethod_name field.
     *
     * @var        string|null
     */
    protected $paymethod_name;

    /**
     * The value for the paymethod_code field.
     *
     * @var        string|null
     */
    protected $paymethod_code;

    /**
     * The value for the paymethod_id field.
     *
     * @var        int|null
     */
    protected $paymethod_id;

    /**
     * The value for the shipping_registration_date field.
     *
     * @var        DateTime|null
     */
    protected $shipping_registration_date;

    /**
     * The value for the invoice_by_postal field.
     *
     * @var        boolean|null
     */
    protected $invoice_by_postal;

    /**
     * The value for the packing_slip_type field.
     *
     * @var        string|null
     */
    protected $packing_slip_type;

    /**
     * The value for the work_in_progress field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $work_in_progress;

    /**
     * The value for the package_amount field.
     *
     * @var        int|null
     */
    protected $package_amount;

    /**
     * The value for the coupon_code field.
     *
     * @var        string|null
     */
    protected $coupon_code;

    /**
     * The value for the coupon_discount_amount field.
     *
     * @var        double|null
     */
    protected $coupon_discount_amount;

    /**
     * The value for the coupon_discount_percentage field.
     *
     * @var        double|null
     */
    protected $coupon_discount_percentage;

    /**
     * The value for the hide_untill field.
     *
     * @var        DateTime|null
     */
    protected $hide_untill;

    /**
     * @var        Site
     */
    protected $aSite;

    /**
     * @var        SaleOrderStatus
     */
    protected $aSaleOrderStatus;

    /**
     * @var        ShippingMethod
     */
    protected $aShippingMethod;

    /**
     * @var        User
     */
    protected $aUser;

    /**
     * @var        ObjectCollection|ChildSaleOrderPayment[] Collection to store aggregation of ChildSaleOrderPayment objects.
     */
    protected $collSaleOrderPayments;
    protected $collSaleOrderPaymentsPartial;

    /**
     * @var        ObjectCollection|Payment[] Collection to store aggregation of Payment objects.
     */
    protected $collPayments;
    protected $collPaymentsPartial;

    /**
     * @var        ObjectCollection|ChildSaleOrderEmail[] Collection to store aggregation of ChildSaleOrderEmail objects.
     */
    protected $collSaleOrderEmails;
    protected $collSaleOrderEmailsPartial;

    /**
     * @var        ObjectCollection|ChildSaleOrderItem[] Collection to store aggregation of ChildSaleOrderItem objects.
     */
    protected $collSaleOrderItems;
    protected $collSaleOrderItemsPartial;

    /**
     * @var        ObjectCollection|SaleOrderNotification[] Collection to store aggregation of SaleOrderNotification objects.
     */
    protected $collSaleOrderNotifications;
    protected $collSaleOrderNotificationsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSaleOrderPayment[]
     */
    protected $saleOrderPaymentsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Payment[]
     */
    protected $paymentsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSaleOrderEmail[]
     */
    protected $saleOrderEmailsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSaleOrderItem[]
     */
    protected $saleOrderItemsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|SaleOrderNotification[]
     */
    protected $saleOrderNotificationsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_closed = false;
        $this->is_fully_paid = false;
        $this->is_ready_for_shipping = false;
        $this->is_shipping_checked = false;
        $this->is_fully_shipped = false;
        $this->is_pushed_to_accounting = false;
        $this->sale_order_status_id = 0;
        $this->source = 'intern';
        $this->is_completed = false;
        $this->order_number = 'false';
        $this->is_deleted = false;
        $this->has_wrap = false;
        $this->wrapping_price = 0.0;
        $this->work_in_progress = false;
    }

    /**
     * Initializes internal state of Model\Sale\Base\SaleOrder object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>SaleOrder</code> instance.  If
     * <code>obj</code> is an instance of <code>SaleOrder</code>, delegates to
     * <code>equals(SaleOrder)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [site_id] column value.
     *
     * @return int|null
     */
    public function getSiteId()
    {
        return $this->site_id;
    }

    /**
     * Get the [language_id] column value.
     *
     * @return int|null
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * Get the [is_closed] column value.
     *
     * @return boolean|null
     */
    public function getIsClosed()
    {
        return $this->is_closed;
    }

    /**
     * Get the [is_closed] column value.
     *
     * @return boolean|null
     */
    public function isClosed()
    {
        return $this->getIsClosed();
    }

    /**
     * Get the [is_fully_paid] column value.
     *
     * @return boolean|null
     */
    public function getIsFullyPaid()
    {
        return $this->is_fully_paid;
    }

    /**
     * Get the [is_fully_paid] column value.
     *
     * @return boolean|null
     */
    public function isFullyPaid()
    {
        return $this->getIsFullyPaid();
    }

    /**
     * Get the [optionally formatted] temporal [pay_date] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getPayDate($format = null)
    {
        if ($format === null) {
            return $this->pay_date;
        } else {
            return $this->pay_date instanceof \DateTimeInterface ? $this->pay_date->format($format) : null;
        }
    }

    /**
     * Get the [is_ready_for_shipping] column value.
     *
     * @return boolean|null
     */
    public function getIsReadyForShipping()
    {
        return $this->is_ready_for_shipping;
    }

    /**
     * Get the [is_ready_for_shipping] column value.
     *
     * @return boolean|null
     */
    public function isReadyForShipping()
    {
        return $this->getIsReadyForShipping();
    }

    /**
     * Get the [is_shipping_checked] column value.
     *
     * @return boolean|null
     */
    public function getIsShippingChecked()
    {
        return $this->is_shipping_checked;
    }

    /**
     * Get the [is_shipping_checked] column value.
     *
     * @return boolean|null
     */
    public function isShippingChecked()
    {
        return $this->getIsShippingChecked();
    }

    /**
     * Get the [is_fully_shipped] column value.
     *
     * @return boolean|null
     */
    public function getIsFullyShipped()
    {
        return $this->is_fully_shipped;
    }

    /**
     * Get the [is_fully_shipped] column value.
     *
     * @return boolean|null
     */
    public function isFullyShipped()
    {
        return $this->getIsFullyShipped();
    }

    /**
     * Get the [is_pushed_to_accounting] column value.
     *
     * @return boolean|null
     */
    public function getIsPushedToAccounting()
    {
        return $this->is_pushed_to_accounting;
    }

    /**
     * Get the [is_pushed_to_accounting] column value.
     *
     * @return boolean|null
     */
    public function isPushedToAccounting()
    {
        return $this->getIsPushedToAccounting();
    }

    /**
     * Get the [optionally formatted] temporal [ship_date] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getShipDate($format = null)
    {
        if ($format === null) {
            return $this->ship_date;
        } else {
            return $this->ship_date instanceof \DateTimeInterface ? $this->ship_date->format($format) : null;
        }
    }

    /**
     * Get the [sale_order_status_id] column value.
     *
     * @return int|null
     */
    public function getSaleOrderStatusId()
    {
        return $this->sale_order_status_id;
    }

    /**
     * Get the [handled_by] column value.
     *
     * @return string|null
     */
    public function getHandledBy()
    {
        return $this->handled_by;
    }

    /**
     * Get the [source] column value.
     *
     * @return string|null
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Get the [is_completed] column value.
     *
     * @return boolean|null
     */
    public function getIsCompleted()
    {
        return $this->is_completed;
    }

    /**
     * Get the [is_completed] column value.
     *
     * @return boolean|null
     */
    public function isCompleted()
    {
        return $this->getIsCompleted();
    }

    /**
     * Get the [created_by_user_id] column value.
     *
     * @return int|null
     */
    public function getCreatedByUserId()
    {
        return $this->created_by_user_id;
    }

    /**
     * Get the [optionally formatted] temporal [created_on] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedOn($format = null)
    {
        if ($format === null) {
            return $this->created_on;
        } else {
            return $this->created_on instanceof \DateTimeInterface ? $this->created_on->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [estimated_delivery_date] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getEstimatedDeliveryDate($format = null)
    {
        if ($format === null) {
            return $this->estimated_delivery_date;
        } else {
            return $this->estimated_delivery_date instanceof \DateTimeInterface ? $this->estimated_delivery_date->format($format) : null;
        }
    }

    /**
     * Get the [customer_id] column value.
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * Get the [envelope_weight_grams] column value.
     *
     * @return int|null
     */
    public function getEnvelopeWeightGrams()
    {
        return $this->envelope_weight_grams;
    }

    /**
     * Get the [webshop_user_session_id] column value.
     *
     * @return int|null
     */
    public function getWebshopUserSessionId()
    {
        return $this->webshop_user_session_id;
    }

    /**
     * Get the [invoice_number] column value.
     *
     * @return string|null
     */
    public function getInvoiceNumber()
    {
        return $this->invoice_number;
    }

    /**
     * Get the [optionally formatted] temporal [invoice_date] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getInvoiceDate($format = null)
    {
        if ($format === null) {
            return $this->invoice_date;
        } else {
            return $this->invoice_date instanceof \DateTimeInterface ? $this->invoice_date->format($format) : null;
        }
    }

    /**
     * Get the [payterm_days] column value.
     *
     * @return int|null
     */
    public function getPaytermDays()
    {
        return $this->payterm_days;
    }

    /**
     * Get the [payterm_original_id] column value.
     *
     * @return int|null
     */
    public function getPaytermOriginalId()
    {
        return $this->payterm_original_id;
    }

    /**
     * Get the [payterm_string] column value.
     *
     * @return string|null
     */
    public function getPaytermString()
    {
        return $this->payterm_string;
    }

    /**
     * Get the [order_number] column value.
     *
     * @return string|null
     */
    public function getOrderNumber()
    {
        return $this->order_number;
    }

    /**
     * Get the [is_deleted] column value.
     *
     * @return boolean|null
     */
    public function getItemDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * Get the [is_deleted] column value.
     *
     * @return boolean|null
     */
    public function isItemDeleted()
    {
        return $this->getItemDeleted();
    }

    /**
     * Get the [company_name] column value.
     *
     * @return string|null
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Get the [debitor] column value.
     *
     * @return string|null
     */
    public function getDebitor()
    {
        return $this->debitor;
    }

    /**
     * Get the [our_custom_folder] column value.
     *
     * @return string|null
     */
    public function getOurCustomFolder()
    {
        return $this->our_custom_folder;
    }

    /**
     * Get the [our_slogan] column value.
     *
     * @return string|null
     */
    public function getOurSlogan()
    {
        return $this->our_slogan;
    }

    /**
     * Get the [our_phone] column value.
     *
     * @return string|null
     */
    public function getOurPhone()
    {
        return $this->our_phone;
    }

    /**
     * Get the [our_fax] column value.
     *
     * @return string|null
     */
    public function getOurFax()
    {
        return $this->our_fax;
    }

    /**
     * Get the [our_website] column value.
     *
     * @return string|null
     */
    public function getOurWebsite()
    {
        return $this->our_website;
    }

    /**
     * Get the [our_email] column value.
     *
     * @return string|null
     */
    public function getOurEmail()
    {
        return $this->our_email;
    }

    /**
     * Get the [our_vat_number] column value.
     *
     * @return string|null
     */
    public function getOurVatNumber()
    {
        return $this->our_vat_number;
    }

    /**
     * Get the [our_chamber_of_commerce] column value.
     *
     * @return string|null
     */
    public function getOurChamberOfCommerce()
    {
        return $this->our_chamber_of_commerce;
    }

    /**
     * Get the [our_iban_number] column value.
     *
     * @return string|null
     */
    public function getOurIbanNumber()
    {
        return $this->our_iban_number;
    }

    /**
     * Get the [our_bic_number] column value.
     *
     * @return string|null
     */
    public function getOurBicNumber()
    {
        return $this->our_bic_number;
    }

    /**
     * Get the [our_general_company_name] column value.
     *
     * @return string|null
     */
    public function getOurGeneralCompanyName()
    {
        return $this->our_general_company_name;
    }

    /**
     * Get the [our_general_street] column value.
     *
     * @return string|null
     */
    public function getOurGeneralStreet()
    {
        return $this->our_general_street;
    }

    /**
     * Get the [our_general_number] column value.
     *
     * @return string|null
     */
    public function getOurGeneralNumber()
    {
        return $this->our_general_number;
    }

    /**
     * Get the [our_general_number_add] column value.
     *
     * @return string|null
     */
    public function getOurGeneralNumberAdd()
    {
        return $this->our_general_number_add;
    }

    /**
     * Get the [our_general_postal] column value.
     *
     * @return string|null
     */
    public function getOurGeneralPostal()
    {
        return $this->our_general_postal;
    }

    /**
     * Get the [our_general_po_box] column value.
     *
     * @return string|null
     */
    public function getOurGeneralPoBox()
    {
        return $this->our_general_po_box;
    }

    /**
     * Get the [our_general_city] column value.
     *
     * @return string|null
     */
    public function getOurGeneralCity()
    {
        return $this->our_general_city;
    }

    /**
     * Get the [our_general_country] column value.
     *
     * @return string|null
     */
    public function getOurGeneralCountry()
    {
        return $this->our_general_country;
    }

    /**
     * Get the [customer_phone] column value.
     *
     * @return string|null
     */
    public function getCustomerPhone()
    {
        return $this->customer_phone;
    }

    /**
     * Get the [customer_fax] column value.
     *
     * @return string|null
     */
    public function getCustomerFax()
    {
        return $this->customer_fax;
    }

    /**
     * Get the [customer_email] column value.
     *
     * @return string|null
     */
    public function getCustomerEmail()
    {
        return $this->customer_email;
    }

    /**
     * Get the [customer_chamber_of_commerce] column value.
     *
     * @return string|null
     */
    public function getCustomerChamberOfCommerce()
    {
        return $this->customer_chamber_of_commerce;
    }

    /**
     * Get the [customer_vat_number] column value.
     *
     * @return string|null
     */
    public function getCustomerVatNumber()
    {
        return $this->customer_vat_number;
    }

    /**
     * Get the [customer_order_reference] column value.
     *
     * @return string|null
     */
    public function getCustomerOrderReference()
    {
        return $this->customer_order_reference;
    }

    /**
     * Get the [customer_order_placed_by] column value.
     *
     * @return string|null
     */
    public function getCustomerOrderPlacedBy()
    {
        return $this->customer_order_placed_by;
    }

    /**
     * Get the [customer_invoice_address_id] column value.
     *
     * @return int|null
     */
    public function getCustomerInvoiceAddressId()
    {
        return $this->customer_invoice_address_id;
    }

    /**
     * Get the [customer_invoice_company_name] column value.
     *
     * @return string|null
     */
    public function getCustomerInvoiceCompanyName()
    {
        return $this->customer_invoice_company_name;
    }

    /**
     * Get the [customer_invoice_attn_name] column value.
     *
     * @return string|null
     */
    public function getCustomerInvoiceAttnName()
    {
        return $this->customer_invoice_attn_name;
    }

    /**
     * Get the [customer_invoice_street] column value.
     *
     * @return string|null
     */
    public function getCustomerInvoiceStreet()
    {
        return $this->customer_invoice_street;
    }

    /**
     * Get the [customer_invoice_number] column value.
     *
     * @return string|null
     */
    public function getCustomerInvoiceNumber()
    {
        return $this->customer_invoice_number;
    }

    /**
     * Get the [customer_invoice_number_add] column value.
     *
     * @return string|null
     */
    public function getCustomerInvoiceNumberAdd()
    {
        return $this->customer_invoice_number_add;
    }

    /**
     * Get the [customer_invoice_postal] column value.
     *
     * @return string|null
     */
    public function getCustomerInvoicePostal()
    {
        return $this->customer_invoice_postal;
    }

    /**
     * Get the [customer_invoice_city] column value.
     *
     * @return string|null
     */
    public function getCustomerInvoiceCity()
    {
        return $this->customer_invoice_city;
    }

    /**
     * Get the [customer_invoice_country] column value.
     *
     * @return string|null
     */
    public function getCustomerInvoiceCountry()
    {
        return $this->customer_invoice_country;
    }

    /**
     * Get the [customer_invoice_usa_state] column value.
     *
     * @return string|null
     */
    public function getCustomerInvoiceUsaState()
    {
        return $this->customer_invoice_usa_state;
    }

    /**
     * Get the [customer_invoice_address_l1] column value.
     *
     * @return string|null
     */
    public function getCustomerInvoiceAddressL1()
    {
        return $this->customer_invoice_address_l1;
    }

    /**
     * Get the [customer_invoice_address_l2] column value.
     *
     * @return string|null
     */
    public function getCustomerInvoiceAddressL2()
    {
        return $this->customer_invoice_address_l2;
    }

    /**
     * Get the [customer_delivery_address_id] column value.
     *
     * @return int|null
     */
    public function getCustomerDeliveryAddressId()
    {
        return $this->customer_delivery_address_id;
    }

    /**
     * Get the [customer_delivery_company_name] column value.
     *
     * @return string|null
     */
    public function getCustomerDeliveryCompanyName()
    {
        return $this->customer_delivery_company_name;
    }

    /**
     * Get the [customer_delivery_attn_name] column value.
     *
     * @return string|null
     */
    public function getCustomerDeliveryAttnName()
    {
        return $this->customer_delivery_attn_name;
    }

    /**
     * Get the [customer_delivery_street] column value.
     *
     * @return string|null
     */
    public function getCustomerDeliveryStreet()
    {
        return $this->customer_delivery_street;
    }

    /**
     * Get the [customer_delivery_number] column value.
     *
     * @return string|null
     */
    public function getCustomerDeliveryNumber()
    {
        return $this->customer_delivery_number;
    }

    /**
     * Get the [customer_delivery_number_add] column value.
     *
     * @return string|null
     */
    public function getCustomerDeliveryNumberAdd()
    {
        return $this->customer_delivery_number_add;
    }

    /**
     * Get the [customer_delivery_postal] column value.
     *
     * @return string|null
     */
    public function getCustomerDeliveryPostal()
    {
        return $this->customer_delivery_postal;
    }

    /**
     * Get the [customer_delivery_city] column value.
     *
     * @return string|null
     */
    public function getCustomerDeliveryCity()
    {
        return $this->customer_delivery_city;
    }

    /**
     * Get the [customer_delivery_country] column value.
     *
     * @return string|null
     */
    public function getCustomerDeliveryCountry()
    {
        return $this->customer_delivery_country;
    }

    /**
     * Get the [customer_delivery_usa_state] column value.
     *
     * @return string|null
     */
    public function getCustomerDeliveryUsaState()
    {
        return $this->customer_delivery_usa_state;
    }

    /**
     * Get the [customer_delivery_address_l1] column value.
     *
     * @return string|null
     */
    public function getCustomerDeliveryAddressL1()
    {
        return $this->customer_delivery_address_l1;
    }

    /**
     * Get the [customer_delivery_address_l2] column value.
     *
     * @return string|null
     */
    public function getCustomerDeliveryAddressL2()
    {
        return $this->customer_delivery_address_l2;
    }

    /**
     * Get the [cp_first_name] column value.
     *
     * @return string|null
     */
    public function getCpFirstName()
    {
        return $this->cp_first_name;
    }

    /**
     * Get the [cp_last_name] column value.
     *
     * @return string|null
     */
    public function getCpLastName()
    {
        return $this->cp_last_name;
    }

    /**
     * Get the [cp_sallutation] column value.
     *
     * @return string|null
     */
    public function getCpSallutation()
    {
        return $this->cp_sallutation;
    }

    /**
     * Get the [reference] column value.
     *
     * @return string|null
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Get the [shipping_note] column value.
     *
     * @return string|null
     */
    public function getShippingNote()
    {
        return $this->shipping_note;
    }

    /**
     * Get the [customer_order_note] column value.
     *
     * @return string|null
     */
    public function getCustomerOrderNote()
    {
        return $this->customer_order_note;
    }

    /**
     * Get the [shipping_order_note] column value.
     *
     * @return string|null
     */
    public function getShippingOrderNote()
    {
        return $this->shipping_order_note;
    }

    /**
     * Get the [has_wrap] column value.
     *
     * @return boolean|null
     */
    public function getHasWrap()
    {
        return $this->has_wrap;
    }

    /**
     * Get the [has_wrap] column value.
     *
     * @return boolean|null
     */
    public function hasWrap()
    {
        return $this->getHasWrap();
    }

    /**
     * Get the [wrapping_price] column value.
     *
     * @return double|null
     */
    public function getWrappingPrice()
    {
        return $this->wrapping_price;
    }

    /**
     * Get the [shipping_carrier] column value.
     *
     * @return string|null
     */
    public function getShippingCarrier()
    {
        return $this->shipping_carrier;
    }

    /**
     * Get the [shipping_price] column value.
     *
     * @return double|null
     */
    public function getShippingPrice()
    {
        return $this->shipping_price;
    }

    /**
     * Get the [shipping_method_id] column value.
     *
     * @return int|null
     */
    public function getShippingMethodId()
    {
        return $this->shipping_method_id;
    }

    /**
     * Get the [shipping_has_track_trace] column value.
     *
     * @return boolean|null
     */
    public function getShippingHasTrackTrace()
    {
        return $this->shipping_has_track_trace;
    }

    /**
     * Get the [shipping_has_track_trace] column value.
     *
     * @return boolean|null
     */
    public function isShippingHasTrackTrace()
    {
        return $this->getShippingHasTrackTrace();
    }

    /**
     * Get the [shipping_id] column value.
     *
     * @return double|null
     */
    public function getShippingId()
    {
        return $this->shipping_id;
    }

    /**
     * Get the [tracking_code] column value.
     *
     * @return string|null
     */
    public function getTrackingCode()
    {
        return $this->tracking_code;
    }

    /**
     * Get the [tracking_url] column value.
     *
     * @return string|null
     */
    public function getTrackingUrl()
    {
        return $this->tracking_url;
    }

    /**
     * Get the [paymethod_name] column value.
     *
     * @return string|null
     */
    public function getPaymethodName()
    {
        return $this->paymethod_name;
    }

    /**
     * Get the [paymethod_code] column value.
     *
     * @return string|null
     */
    public function getPaymethodCode()
    {
        return $this->paymethod_code;
    }

    /**
     * Get the [paymethod_id] column value.
     *
     * @return int|null
     */
    public function getPaymethodId()
    {
        return $this->paymethod_id;
    }

    /**
     * Get the [optionally formatted] temporal [shipping_registration_date] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getShippingRegistrationDate($format = null)
    {
        if ($format === null) {
            return $this->shipping_registration_date;
        } else {
            return $this->shipping_registration_date instanceof \DateTimeInterface ? $this->shipping_registration_date->format($format) : null;
        }
    }

    /**
     * Get the [invoice_by_postal] column value.
     *
     * @return boolean|null
     */
    public function getInvoiceByPostal()
    {
        return $this->invoice_by_postal;
    }

    /**
     * Get the [invoice_by_postal] column value.
     *
     * @return boolean|null
     */
    public function isInvoiceByPostal()
    {
        return $this->getInvoiceByPostal();
    }

    /**
     * Get the [packing_slip_type] column value.
     *
     * @return string|null
     */
    public function getPackingSlipType()
    {
        return $this->packing_slip_type;
    }

    /**
     * Get the [work_in_progress] column value.
     *
     * @return boolean|null
     */
    public function getWorkInProgress()
    {
        return $this->work_in_progress;
    }

    /**
     * Get the [work_in_progress] column value.
     *
     * @return boolean|null
     */
    public function isWorkInProgress()
    {
        return $this->getWorkInProgress();
    }

    /**
     * Get the [package_amount] column value.
     *
     * @return int|null
     */
    public function getPackageAmount()
    {
        return $this->package_amount;
    }

    /**
     * Get the [coupon_code] column value.
     *
     * @return string|null
     */
    public function getCouponCode()
    {
        return $this->coupon_code;
    }

    /**
     * Get the [coupon_discount_amount] column value.
     *
     * @return double|null
     */
    public function getCouponDiscountAmount()
    {
        return $this->coupon_discount_amount;
    }

    /**
     * Get the [coupon_discount_percentage] column value.
     *
     * @return double|null
     */
    public function getCouponDiscountPercentage()
    {
        return $this->coupon_discount_percentage;
    }

    /**
     * Get the [optionally formatted] temporal [hide_untill] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getHideUntill($format = null)
    {
        if ($format === null) {
            return $this->hide_untill;
        } else {
            return $this->hide_untill instanceof \DateTimeInterface ? $this->hide_untill->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [site_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setSiteId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->site_id !== $v) {
            $this->site_id = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_SITE_ID] = true;
        }

        if ($this->aSite !== null && $this->aSite->getId() !== $v) {
            $this->aSite = null;
        }

        return $this;
    } // setSiteId()

    /**
     * Set the value of [language_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setLanguageId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->language_id !== $v) {
            $this->language_id = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_LANGUAGE_ID] = true;
        }

        return $this;
    } // setLanguageId()

    /**
     * Sets the value of the [is_closed] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setIsClosed($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_closed !== $v) {
            $this->is_closed = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_IS_CLOSED] = true;
        }

        return $this;
    } // setIsClosed()

    /**
     * Sets the value of the [is_fully_paid] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setIsFullyPaid($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_fully_paid !== $v) {
            $this->is_fully_paid = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_IS_FULLY_PAID] = true;
        }

        return $this;
    } // setIsFullyPaid()

    /**
     * Sets the value of [pay_date] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setPayDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->pay_date !== null || $dt !== null) {
            if ($this->pay_date === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->pay_date->format("Y-m-d H:i:s.u")) {
                $this->pay_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleOrderTableMap::COL_PAY_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setPayDate()

    /**
     * Sets the value of the [is_ready_for_shipping] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setIsReadyForShipping($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_ready_for_shipping !== $v) {
            $this->is_ready_for_shipping = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_IS_READY_FOR_SHIPPING] = true;
        }

        return $this;
    } // setIsReadyForShipping()

    /**
     * Sets the value of the [is_shipping_checked] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setIsShippingChecked($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_shipping_checked !== $v) {
            $this->is_shipping_checked = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_IS_SHIPPING_CHECKED] = true;
        }

        return $this;
    } // setIsShippingChecked()

    /**
     * Sets the value of the [is_fully_shipped] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setIsFullyShipped($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_fully_shipped !== $v) {
            $this->is_fully_shipped = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_IS_FULLY_SHIPPED] = true;
        }

        return $this;
    } // setIsFullyShipped()

    /**
     * Sets the value of the [is_pushed_to_accounting] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setIsPushedToAccounting($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_pushed_to_accounting !== $v) {
            $this->is_pushed_to_accounting = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_IS_PUSHED_TO_ACCOUNTING] = true;
        }

        return $this;
    } // setIsPushedToAccounting()

    /**
     * Sets the value of [ship_date] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setShipDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->ship_date !== null || $dt !== null) {
            if ($this->ship_date === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->ship_date->format("Y-m-d H:i:s.u")) {
                $this->ship_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleOrderTableMap::COL_SHIP_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setShipDate()

    /**
     * Set the value of [sale_order_status_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setSaleOrderStatusId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sale_order_status_id !== $v) {
            $this->sale_order_status_id = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_SALE_ORDER_STATUS_ID] = true;
        }

        if ($this->aSaleOrderStatus !== null && $this->aSaleOrderStatus->getId() !== $v) {
            $this->aSaleOrderStatus = null;
        }

        return $this;
    } // setSaleOrderStatusId()

    /**
     * Set the value of [handled_by] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setHandledBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->handled_by !== $v) {
            $this->handled_by = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_HANDLED_BY] = true;
        }

        return $this;
    } // setHandledBy()

    /**
     * Set the value of [source] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setSource($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->source !== $v) {
            $this->source = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_SOURCE] = true;
        }

        return $this;
    } // setSource()

    /**
     * Sets the value of the [is_completed] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setIsCompleted($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_completed !== $v) {
            $this->is_completed = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_IS_COMPLETED] = true;
        }

        return $this;
    } // setIsCompleted()

    /**
     * Set the value of [created_by_user_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCreatedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->created_by_user_id !== $v) {
            $this->created_by_user_id = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CREATED_BY_USER_ID] = true;
        }

        if ($this->aUser !== null && $this->aUser->getId() !== $v) {
            $this->aUser = null;
        }

        return $this;
    } // setCreatedByUserId()

    /**
     * Sets the value of [created_on] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCreatedOn($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_on !== null || $dt !== null) {
            if ($this->created_on === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_on->format("Y-m-d H:i:s.u")) {
                $this->created_on = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleOrderTableMap::COL_CREATED_ON] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedOn()

    /**
     * Sets the value of [estimated_delivery_date] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setEstimatedDeliveryDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->estimated_delivery_date !== null || $dt !== null) {
            if ($this->estimated_delivery_date === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->estimated_delivery_date->format("Y-m-d H:i:s.u")) {
                $this->estimated_delivery_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleOrderTableMap::COL_ESTIMATED_DELIVERY_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setEstimatedDeliveryDate()

    /**
     * Set the value of [customer_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->customer_id !== $v) {
            $this->customer_id = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_ID] = true;
        }

        return $this;
    } // setCustomerId()

    /**
     * Set the value of [envelope_weight_grams] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setEnvelopeWeightGrams($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->envelope_weight_grams !== $v) {
            $this->envelope_weight_grams = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_ENVELOPE_WEIGHT_GRAMS] = true;
        }

        return $this;
    } // setEnvelopeWeightGrams()

    /**
     * Set the value of [webshop_user_session_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setWebshopUserSessionId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->webshop_user_session_id !== $v) {
            $this->webshop_user_session_id = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_WEBSHOP_USER_SESSION_ID] = true;
        }

        return $this;
    } // setWebshopUserSessionId()

    /**
     * Set the value of [invoice_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setInvoiceNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->invoice_number !== $v) {
            $this->invoice_number = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_INVOICE_NUMBER] = true;
        }

        return $this;
    } // setInvoiceNumber()

    /**
     * Sets the value of [invoice_date] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setInvoiceDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->invoice_date !== null || $dt !== null) {
            if ($this->invoice_date === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->invoice_date->format("Y-m-d H:i:s.u")) {
                $this->invoice_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleOrderTableMap::COL_INVOICE_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setInvoiceDate()

    /**
     * Set the value of [payterm_days] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setPaytermDays($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->payterm_days !== $v) {
            $this->payterm_days = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_PAYTERM_DAYS] = true;
        }

        return $this;
    } // setPaytermDays()

    /**
     * Set the value of [payterm_original_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setPaytermOriginalId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->payterm_original_id !== $v) {
            $this->payterm_original_id = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_PAYTERM_ORIGINAL_ID] = true;
        }

        return $this;
    } // setPaytermOriginalId()

    /**
     * Set the value of [payterm_string] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setPaytermString($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->payterm_string !== $v) {
            $this->payterm_string = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_PAYTERM_STRING] = true;
        }

        return $this;
    } // setPaytermString()

    /**
     * Set the value of [order_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOrderNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->order_number !== $v) {
            $this->order_number = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_ORDER_NUMBER] = true;
        }

        return $this;
    } // setOrderNumber()

    /**
     * Sets the value of the [is_deleted] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setItemDeleted($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_deleted !== $v) {
            $this->is_deleted = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_IS_DELETED] = true;
        }

        return $this;
    } // setItemDeleted()

    /**
     * Set the value of [company_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCompanyName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->company_name !== $v) {
            $this->company_name = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_COMPANY_NAME] = true;
        }

        return $this;
    } // setCompanyName()

    /**
     * Set the value of [debitor] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setDebitor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->debitor !== $v) {
            $this->debitor = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_DEBITOR] = true;
        }

        return $this;
    } // setDebitor()

    /**
     * Set the value of [our_custom_folder] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurCustomFolder($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_custom_folder !== $v) {
            $this->our_custom_folder = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_CUSTOM_FOLDER] = true;
        }

        return $this;
    } // setOurCustomFolder()

    /**
     * Set the value of [our_slogan] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurSlogan($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_slogan !== $v) {
            $this->our_slogan = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_SLOGAN] = true;
        }

        return $this;
    } // setOurSlogan()

    /**
     * Set the value of [our_phone] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_phone !== $v) {
            $this->our_phone = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_PHONE] = true;
        }

        return $this;
    } // setOurPhone()

    /**
     * Set the value of [our_fax] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurFax($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_fax !== $v) {
            $this->our_fax = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_FAX] = true;
        }

        return $this;
    } // setOurFax()

    /**
     * Set the value of [our_website] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurWebsite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_website !== $v) {
            $this->our_website = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_WEBSITE] = true;
        }

        return $this;
    } // setOurWebsite()

    /**
     * Set the value of [our_email] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_email !== $v) {
            $this->our_email = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_EMAIL] = true;
        }

        return $this;
    } // setOurEmail()

    /**
     * Set the value of [our_vat_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurVatNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_vat_number !== $v) {
            $this->our_vat_number = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_VAT_NUMBER] = true;
        }

        return $this;
    } // setOurVatNumber()

    /**
     * Set the value of [our_chamber_of_commerce] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurChamberOfCommerce($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_chamber_of_commerce !== $v) {
            $this->our_chamber_of_commerce = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_CHAMBER_OF_COMMERCE] = true;
        }

        return $this;
    } // setOurChamberOfCommerce()

    /**
     * Set the value of [our_iban_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurIbanNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_iban_number !== $v) {
            $this->our_iban_number = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_IBAN_NUMBER] = true;
        }

        return $this;
    } // setOurIbanNumber()

    /**
     * Set the value of [our_bic_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurBicNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_bic_number !== $v) {
            $this->our_bic_number = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_BIC_NUMBER] = true;
        }

        return $this;
    } // setOurBicNumber()

    /**
     * Set the value of [our_general_company_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurGeneralCompanyName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_general_company_name !== $v) {
            $this->our_general_company_name = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_GENERAL_COMPANY_NAME] = true;
        }

        return $this;
    } // setOurGeneralCompanyName()

    /**
     * Set the value of [our_general_street] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurGeneralStreet($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_general_street !== $v) {
            $this->our_general_street = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_GENERAL_STREET] = true;
        }

        return $this;
    } // setOurGeneralStreet()

    /**
     * Set the value of [our_general_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurGeneralNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_general_number !== $v) {
            $this->our_general_number = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_GENERAL_NUMBER] = true;
        }

        return $this;
    } // setOurGeneralNumber()

    /**
     * Set the value of [our_general_number_add] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurGeneralNumberAdd($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_general_number_add !== $v) {
            $this->our_general_number_add = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_GENERAL_NUMBER_ADD] = true;
        }

        return $this;
    } // setOurGeneralNumberAdd()

    /**
     * Set the value of [our_general_postal] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurGeneralPostal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_general_postal !== $v) {
            $this->our_general_postal = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_GENERAL_POSTAL] = true;
        }

        return $this;
    } // setOurGeneralPostal()

    /**
     * Set the value of [our_general_po_box] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurGeneralPoBox($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_general_po_box !== $v) {
            $this->our_general_po_box = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_GENERAL_PO_BOX] = true;
        }

        return $this;
    } // setOurGeneralPoBox()

    /**
     * Set the value of [our_general_city] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurGeneralCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_general_city !== $v) {
            $this->our_general_city = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_GENERAL_CITY] = true;
        }

        return $this;
    } // setOurGeneralCity()

    /**
     * Set the value of [our_general_country] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setOurGeneralCountry($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_general_country !== $v) {
            $this->our_general_country = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_OUR_GENERAL_COUNTRY] = true;
        }

        return $this;
    } // setOurGeneralCountry()

    /**
     * Set the value of [customer_phone] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_phone !== $v) {
            $this->customer_phone = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_PHONE] = true;
        }

        return $this;
    } // setCustomerPhone()

    /**
     * Set the value of [customer_fax] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerFax($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_fax !== $v) {
            $this->customer_fax = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_FAX] = true;
        }

        return $this;
    } // setCustomerFax()

    /**
     * Set the value of [customer_email] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_email !== $v) {
            $this->customer_email = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_EMAIL] = true;
        }

        return $this;
    } // setCustomerEmail()

    /**
     * Set the value of [customer_chamber_of_commerce] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerChamberOfCommerce($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_chamber_of_commerce !== $v) {
            $this->customer_chamber_of_commerce = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_CHAMBER_OF_COMMERCE] = true;
        }

        return $this;
    } // setCustomerChamberOfCommerce()

    /**
     * Set the value of [customer_vat_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerVatNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_vat_number !== $v) {
            $this->customer_vat_number = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_VAT_NUMBER] = true;
        }

        return $this;
    } // setCustomerVatNumber()

    /**
     * Set the value of [customer_order_reference] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerOrderReference($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_order_reference !== $v) {
            $this->customer_order_reference = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_ORDER_REFERENCE] = true;
        }

        return $this;
    } // setCustomerOrderReference()

    /**
     * Set the value of [customer_order_placed_by] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerOrderPlacedBy($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_order_placed_by !== $v) {
            $this->customer_order_placed_by = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_ORDER_PLACED_BY] = true;
        }

        return $this;
    } // setCustomerOrderPlacedBy()

    /**
     * Set the value of [customer_invoice_address_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerInvoiceAddressId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->customer_invoice_address_id !== $v) {
            $this->customer_invoice_address_id = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_ID] = true;
        }

        return $this;
    } // setCustomerInvoiceAddressId()

    /**
     * Set the value of [customer_invoice_company_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerInvoiceCompanyName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_invoice_company_name !== $v) {
            $this->customer_invoice_company_name = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_INVOICE_COMPANY_NAME] = true;
        }

        return $this;
    } // setCustomerInvoiceCompanyName()

    /**
     * Set the value of [customer_invoice_attn_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerInvoiceAttnName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_invoice_attn_name !== $v) {
            $this->customer_invoice_attn_name = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_INVOICE_ATTN_NAME] = true;
        }

        return $this;
    } // setCustomerInvoiceAttnName()

    /**
     * Set the value of [customer_invoice_street] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerInvoiceStreet($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_invoice_street !== $v) {
            $this->customer_invoice_street = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_INVOICE_STREET] = true;
        }

        return $this;
    } // setCustomerInvoiceStreet()

    /**
     * Set the value of [customer_invoice_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerInvoiceNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_invoice_number !== $v) {
            $this->customer_invoice_number = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER] = true;
        }

        return $this;
    } // setCustomerInvoiceNumber()

    /**
     * Set the value of [customer_invoice_number_add] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerInvoiceNumberAdd($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_invoice_number_add !== $v) {
            $this->customer_invoice_number_add = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER_ADD] = true;
        }

        return $this;
    } // setCustomerInvoiceNumberAdd()

    /**
     * Set the value of [customer_invoice_postal] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerInvoicePostal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_invoice_postal !== $v) {
            $this->customer_invoice_postal = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_INVOICE_POSTAL] = true;
        }

        return $this;
    } // setCustomerInvoicePostal()

    /**
     * Set the value of [customer_invoice_city] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerInvoiceCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_invoice_city !== $v) {
            $this->customer_invoice_city = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_INVOICE_CITY] = true;
        }

        return $this;
    } // setCustomerInvoiceCity()

    /**
     * Set the value of [customer_invoice_country] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerInvoiceCountry($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_invoice_country !== $v) {
            $this->customer_invoice_country = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_INVOICE_COUNTRY] = true;
        }

        return $this;
    } // setCustomerInvoiceCountry()

    /**
     * Set the value of [customer_invoice_usa_state] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerInvoiceUsaState($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_invoice_usa_state !== $v) {
            $this->customer_invoice_usa_state = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_INVOICE_USA_STATE] = true;
        }

        return $this;
    } // setCustomerInvoiceUsaState()

    /**
     * Set the value of [customer_invoice_address_l1] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerInvoiceAddressL1($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_invoice_address_l1 !== $v) {
            $this->customer_invoice_address_l1 = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L1] = true;
        }

        return $this;
    } // setCustomerInvoiceAddressL1()

    /**
     * Set the value of [customer_invoice_address_l2] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerInvoiceAddressL2($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_invoice_address_l2 !== $v) {
            $this->customer_invoice_address_l2 = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L2] = true;
        }

        return $this;
    } // setCustomerInvoiceAddressL2()

    /**
     * Set the value of [customer_delivery_address_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerDeliveryAddressId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->customer_delivery_address_id !== $v) {
            $this->customer_delivery_address_id = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_ID] = true;
        }

        return $this;
    } // setCustomerDeliveryAddressId()

    /**
     * Set the value of [customer_delivery_company_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerDeliveryCompanyName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_delivery_company_name !== $v) {
            $this->customer_delivery_company_name = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COMPANY_NAME] = true;
        }

        return $this;
    } // setCustomerDeliveryCompanyName()

    /**
     * Set the value of [customer_delivery_attn_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerDeliveryAttnName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_delivery_attn_name !== $v) {
            $this->customer_delivery_attn_name = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ATTN_NAME] = true;
        }

        return $this;
    } // setCustomerDeliveryAttnName()

    /**
     * Set the value of [customer_delivery_street] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerDeliveryStreet($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_delivery_street !== $v) {
            $this->customer_delivery_street = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_DELIVERY_STREET] = true;
        }

        return $this;
    } // setCustomerDeliveryStreet()

    /**
     * Set the value of [customer_delivery_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerDeliveryNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_delivery_number !== $v) {
            $this->customer_delivery_number = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER] = true;
        }

        return $this;
    } // setCustomerDeliveryNumber()

    /**
     * Set the value of [customer_delivery_number_add] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerDeliveryNumberAdd($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_delivery_number_add !== $v) {
            $this->customer_delivery_number_add = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER_ADD] = true;
        }

        return $this;
    } // setCustomerDeliveryNumberAdd()

    /**
     * Set the value of [customer_delivery_postal] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerDeliveryPostal($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_delivery_postal !== $v) {
            $this->customer_delivery_postal = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_DELIVERY_POSTAL] = true;
        }

        return $this;
    } // setCustomerDeliveryPostal()

    /**
     * Set the value of [customer_delivery_city] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerDeliveryCity($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_delivery_city !== $v) {
            $this->customer_delivery_city = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_DELIVERY_CITY] = true;
        }

        return $this;
    } // setCustomerDeliveryCity()

    /**
     * Set the value of [customer_delivery_country] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerDeliveryCountry($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_delivery_country !== $v) {
            $this->customer_delivery_country = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COUNTRY] = true;
        }

        return $this;
    } // setCustomerDeliveryCountry()

    /**
     * Set the value of [customer_delivery_usa_state] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerDeliveryUsaState($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_delivery_usa_state !== $v) {
            $this->customer_delivery_usa_state = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_DELIVERY_USA_STATE] = true;
        }

        return $this;
    } // setCustomerDeliveryUsaState()

    /**
     * Set the value of [customer_delivery_address_l1] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerDeliveryAddressL1($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_delivery_address_l1 !== $v) {
            $this->customer_delivery_address_l1 = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L1] = true;
        }

        return $this;
    } // setCustomerDeliveryAddressL1()

    /**
     * Set the value of [customer_delivery_address_l2] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerDeliveryAddressL2($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_delivery_address_l2 !== $v) {
            $this->customer_delivery_address_l2 = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L2] = true;
        }

        return $this;
    } // setCustomerDeliveryAddressL2()

    /**
     * Set the value of [cp_first_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCpFirstName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cp_first_name !== $v) {
            $this->cp_first_name = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CP_FIRST_NAME] = true;
        }

        return $this;
    } // setCpFirstName()

    /**
     * Set the value of [cp_last_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCpLastName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cp_last_name !== $v) {
            $this->cp_last_name = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CP_LAST_NAME] = true;
        }

        return $this;
    } // setCpLastName()

    /**
     * Set the value of [cp_sallutation] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCpSallutation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cp_sallutation !== $v) {
            $this->cp_sallutation = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CP_SALLUTATION] = true;
        }

        return $this;
    } // setCpSallutation()

    /**
     * Set the value of [reference] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setReference($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->reference !== $v) {
            $this->reference = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_REFERENCE] = true;
        }

        return $this;
    } // setReference()

    /**
     * Set the value of [shipping_note] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setShippingNote($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shipping_note !== $v) {
            $this->shipping_note = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_SHIPPING_NOTE] = true;
        }

        return $this;
    } // setShippingNote()

    /**
     * Set the value of [customer_order_note] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCustomerOrderNote($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->customer_order_note !== $v) {
            $this->customer_order_note = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_CUSTOMER_ORDER_NOTE] = true;
        }

        return $this;
    } // setCustomerOrderNote()

    /**
     * Set the value of [shipping_order_note] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setShippingOrderNote($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shipping_order_note !== $v) {
            $this->shipping_order_note = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_SHIPPING_ORDER_NOTE] = true;
        }

        return $this;
    } // setShippingOrderNote()

    /**
     * Sets the value of the [has_wrap] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setHasWrap($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_wrap !== $v) {
            $this->has_wrap = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_HAS_WRAP] = true;
        }

        return $this;
    } // setHasWrap()

    /**
     * Set the value of [wrapping_price] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setWrappingPrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->wrapping_price !== $v) {
            $this->wrapping_price = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_WRAPPING_PRICE] = true;
        }

        return $this;
    } // setWrappingPrice()

    /**
     * Set the value of [shipping_carrier] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setShippingCarrier($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->shipping_carrier !== $v) {
            $this->shipping_carrier = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_SHIPPING_CARRIER] = true;
        }

        return $this;
    } // setShippingCarrier()

    /**
     * Set the value of [shipping_price] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setShippingPrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->shipping_price !== $v) {
            $this->shipping_price = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_SHIPPING_PRICE] = true;
        }

        return $this;
    } // setShippingPrice()

    /**
     * Set the value of [shipping_method_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setShippingMethodId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->shipping_method_id !== $v) {
            $this->shipping_method_id = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_SHIPPING_METHOD_ID] = true;
        }

        if ($this->aShippingMethod !== null && $this->aShippingMethod->getId() !== $v) {
            $this->aShippingMethod = null;
        }

        return $this;
    } // setShippingMethodId()

    /**
     * Sets the value of the [shipping_has_track_trace] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setShippingHasTrackTrace($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->shipping_has_track_trace !== $v) {
            $this->shipping_has_track_trace = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_SHIPPING_HAS_TRACK_TRACE] = true;
        }

        return $this;
    } // setShippingHasTrackTrace()

    /**
     * Set the value of [shipping_id] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setShippingId($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->shipping_id !== $v) {
            $this->shipping_id = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_SHIPPING_ID] = true;
        }

        return $this;
    } // setShippingId()

    /**
     * Set the value of [tracking_code] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setTrackingCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tracking_code !== $v) {
            $this->tracking_code = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_TRACKING_CODE] = true;
        }

        return $this;
    } // setTrackingCode()

    /**
     * Set the value of [tracking_url] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setTrackingUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tracking_url !== $v) {
            $this->tracking_url = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_TRACKING_URL] = true;
        }

        return $this;
    } // setTrackingUrl()

    /**
     * Set the value of [paymethod_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setPaymethodName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->paymethod_name !== $v) {
            $this->paymethod_name = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_PAYMETHOD_NAME] = true;
        }

        return $this;
    } // setPaymethodName()

    /**
     * Set the value of [paymethod_code] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setPaymethodCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->paymethod_code !== $v) {
            $this->paymethod_code = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_PAYMETHOD_CODE] = true;
        }

        return $this;
    } // setPaymethodCode()

    /**
     * Set the value of [paymethod_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setPaymethodId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->paymethod_id !== $v) {
            $this->paymethod_id = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_PAYMETHOD_ID] = true;
        }

        return $this;
    } // setPaymethodId()

    /**
     * Sets the value of [shipping_registration_date] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setShippingRegistrationDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->shipping_registration_date !== null || $dt !== null) {
            if ($this->shipping_registration_date === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->shipping_registration_date->format("Y-m-d H:i:s.u")) {
                $this->shipping_registration_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleOrderTableMap::COL_SHIPPING_REGISTRATION_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setShippingRegistrationDate()

    /**
     * Sets the value of the [invoice_by_postal] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setInvoiceByPostal($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->invoice_by_postal !== $v) {
            $this->invoice_by_postal = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_INVOICE_BY_POSTAL] = true;
        }

        return $this;
    } // setInvoiceByPostal()

    /**
     * Set the value of [packing_slip_type] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setPackingSlipType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->packing_slip_type !== $v) {
            $this->packing_slip_type = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_PACKING_SLIP_TYPE] = true;
        }

        return $this;
    } // setPackingSlipType()

    /**
     * Sets the value of the [work_in_progress] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setWorkInProgress($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->work_in_progress !== $v) {
            $this->work_in_progress = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_WORK_IN_PROGRESS] = true;
        }

        return $this;
    } // setWorkInProgress()

    /**
     * Set the value of [package_amount] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setPackageAmount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->package_amount !== $v) {
            $this->package_amount = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_PACKAGE_AMOUNT] = true;
        }

        return $this;
    } // setPackageAmount()

    /**
     * Set the value of [coupon_code] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCouponCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->coupon_code !== $v) {
            $this->coupon_code = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_COUPON_CODE] = true;
        }

        return $this;
    } // setCouponCode()

    /**
     * Set the value of [coupon_discount_amount] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCouponDiscountAmount($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->coupon_discount_amount !== $v) {
            $this->coupon_discount_amount = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_COUPON_DISCOUNT_AMOUNT] = true;
        }

        return $this;
    } // setCouponDiscountAmount()

    /**
     * Set the value of [coupon_discount_percentage] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setCouponDiscountPercentage($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->coupon_discount_percentage !== $v) {
            $this->coupon_discount_percentage = $v;
            $this->modifiedColumns[SaleOrderTableMap::COL_COUPON_DISCOUNT_PERCENTAGE] = true;
        }

        return $this;
    } // setCouponDiscountPercentage()

    /**
     * Sets the value of [hide_untill] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function setHideUntill($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->hide_untill !== null || $dt !== null) {
            if ($this->hide_untill === null || $dt === null || $dt->format("Y-m-d") !== $this->hide_untill->format("Y-m-d")) {
                $this->hide_untill = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleOrderTableMap::COL_HIDE_UNTILL] = true;
            }
        } // if either are not null

        return $this;
    } // setHideUntill()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_closed !== false) {
                return false;
            }

            if ($this->is_fully_paid !== false) {
                return false;
            }

            if ($this->is_ready_for_shipping !== false) {
                return false;
            }

            if ($this->is_shipping_checked !== false) {
                return false;
            }

            if ($this->is_fully_shipped !== false) {
                return false;
            }

            if ($this->is_pushed_to_accounting !== false) {
                return false;
            }

            if ($this->sale_order_status_id !== 0) {
                return false;
            }

            if ($this->source !== 'intern') {
                return false;
            }

            if ($this->is_completed !== false) {
                return false;
            }

            if ($this->order_number !== 'false') {
                return false;
            }

            if ($this->is_deleted !== false) {
                return false;
            }

            if ($this->has_wrap !== false) {
                return false;
            }

            if ($this->wrapping_price !== 0.0) {
                return false;
            }

            if ($this->work_in_progress !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SaleOrderTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SaleOrderTableMap::translateFieldName('SiteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->site_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SaleOrderTableMap::translateFieldName('LanguageId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->language_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SaleOrderTableMap::translateFieldName('IsClosed', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_closed = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : SaleOrderTableMap::translateFieldName('IsFullyPaid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_fully_paid = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : SaleOrderTableMap::translateFieldName('PayDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->pay_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : SaleOrderTableMap::translateFieldName('IsReadyForShipping', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_ready_for_shipping = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : SaleOrderTableMap::translateFieldName('IsShippingChecked', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_shipping_checked = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : SaleOrderTableMap::translateFieldName('IsFullyShipped', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_fully_shipped = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : SaleOrderTableMap::translateFieldName('IsPushedToAccounting', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_pushed_to_accounting = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : SaleOrderTableMap::translateFieldName('ShipDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->ship_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : SaleOrderTableMap::translateFieldName('SaleOrderStatusId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sale_order_status_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : SaleOrderTableMap::translateFieldName('HandledBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->handled_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : SaleOrderTableMap::translateFieldName('Source', TableMap::TYPE_PHPNAME, $indexType)];
            $this->source = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : SaleOrderTableMap::translateFieldName('IsCompleted', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_completed = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : SaleOrderTableMap::translateFieldName('CreatedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->created_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : SaleOrderTableMap::translateFieldName('CreatedOn', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_on = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : SaleOrderTableMap::translateFieldName('EstimatedDeliveryDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->estimated_delivery_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : SaleOrderTableMap::translateFieldName('CustomerId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : SaleOrderTableMap::translateFieldName('EnvelopeWeightGrams', TableMap::TYPE_PHPNAME, $indexType)];
            $this->envelope_weight_grams = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : SaleOrderTableMap::translateFieldName('WebshopUserSessionId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->webshop_user_session_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : SaleOrderTableMap::translateFieldName('InvoiceNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->invoice_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : SaleOrderTableMap::translateFieldName('InvoiceDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->invoice_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : SaleOrderTableMap::translateFieldName('PaytermDays', TableMap::TYPE_PHPNAME, $indexType)];
            $this->payterm_days = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : SaleOrderTableMap::translateFieldName('PaytermOriginalId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->payterm_original_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : SaleOrderTableMap::translateFieldName('PaytermString', TableMap::TYPE_PHPNAME, $indexType)];
            $this->payterm_string = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : SaleOrderTableMap::translateFieldName('OrderNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->order_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : SaleOrderTableMap::translateFieldName('ItemDeleted', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_deleted = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : SaleOrderTableMap::translateFieldName('CompanyName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->company_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : SaleOrderTableMap::translateFieldName('Debitor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->debitor = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : SaleOrderTableMap::translateFieldName('OurCustomFolder', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_custom_folder = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 31 + $startcol : SaleOrderTableMap::translateFieldName('OurSlogan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_slogan = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 32 + $startcol : SaleOrderTableMap::translateFieldName('OurPhone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_phone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 33 + $startcol : SaleOrderTableMap::translateFieldName('OurFax', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_fax = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 34 + $startcol : SaleOrderTableMap::translateFieldName('OurWebsite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_website = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 35 + $startcol : SaleOrderTableMap::translateFieldName('OurEmail', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 36 + $startcol : SaleOrderTableMap::translateFieldName('OurVatNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_vat_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 37 + $startcol : SaleOrderTableMap::translateFieldName('OurChamberOfCommerce', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_chamber_of_commerce = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 38 + $startcol : SaleOrderTableMap::translateFieldName('OurIbanNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_iban_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 39 + $startcol : SaleOrderTableMap::translateFieldName('OurBicNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_bic_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 40 + $startcol : SaleOrderTableMap::translateFieldName('OurGeneralCompanyName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_general_company_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 41 + $startcol : SaleOrderTableMap::translateFieldName('OurGeneralStreet', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_general_street = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 42 + $startcol : SaleOrderTableMap::translateFieldName('OurGeneralNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_general_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 43 + $startcol : SaleOrderTableMap::translateFieldName('OurGeneralNumberAdd', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_general_number_add = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 44 + $startcol : SaleOrderTableMap::translateFieldName('OurGeneralPostal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_general_postal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 45 + $startcol : SaleOrderTableMap::translateFieldName('OurGeneralPoBox', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_general_po_box = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 46 + $startcol : SaleOrderTableMap::translateFieldName('OurGeneralCity', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_general_city = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 47 + $startcol : SaleOrderTableMap::translateFieldName('OurGeneralCountry', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_general_country = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 48 + $startcol : SaleOrderTableMap::translateFieldName('CustomerPhone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_phone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 49 + $startcol : SaleOrderTableMap::translateFieldName('CustomerFax', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_fax = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 50 + $startcol : SaleOrderTableMap::translateFieldName('CustomerEmail', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 51 + $startcol : SaleOrderTableMap::translateFieldName('CustomerChamberOfCommerce', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_chamber_of_commerce = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 52 + $startcol : SaleOrderTableMap::translateFieldName('CustomerVatNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_vat_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 53 + $startcol : SaleOrderTableMap::translateFieldName('CustomerOrderReference', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_order_reference = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 54 + $startcol : SaleOrderTableMap::translateFieldName('CustomerOrderPlacedBy', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_order_placed_by = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 55 + $startcol : SaleOrderTableMap::translateFieldName('CustomerInvoiceAddressId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_invoice_address_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 56 + $startcol : SaleOrderTableMap::translateFieldName('CustomerInvoiceCompanyName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_invoice_company_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 57 + $startcol : SaleOrderTableMap::translateFieldName('CustomerInvoiceAttnName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_invoice_attn_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 58 + $startcol : SaleOrderTableMap::translateFieldName('CustomerInvoiceStreet', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_invoice_street = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 59 + $startcol : SaleOrderTableMap::translateFieldName('CustomerInvoiceNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_invoice_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 60 + $startcol : SaleOrderTableMap::translateFieldName('CustomerInvoiceNumberAdd', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_invoice_number_add = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 61 + $startcol : SaleOrderTableMap::translateFieldName('CustomerInvoicePostal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_invoice_postal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 62 + $startcol : SaleOrderTableMap::translateFieldName('CustomerInvoiceCity', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_invoice_city = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 63 + $startcol : SaleOrderTableMap::translateFieldName('CustomerInvoiceCountry', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_invoice_country = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 64 + $startcol : SaleOrderTableMap::translateFieldName('CustomerInvoiceUsaState', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_invoice_usa_state = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 65 + $startcol : SaleOrderTableMap::translateFieldName('CustomerInvoiceAddressL1', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_invoice_address_l1 = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 66 + $startcol : SaleOrderTableMap::translateFieldName('CustomerInvoiceAddressL2', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_invoice_address_l2 = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 67 + $startcol : SaleOrderTableMap::translateFieldName('CustomerDeliveryAddressId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_delivery_address_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 68 + $startcol : SaleOrderTableMap::translateFieldName('CustomerDeliveryCompanyName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_delivery_company_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 69 + $startcol : SaleOrderTableMap::translateFieldName('CustomerDeliveryAttnName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_delivery_attn_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 70 + $startcol : SaleOrderTableMap::translateFieldName('CustomerDeliveryStreet', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_delivery_street = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 71 + $startcol : SaleOrderTableMap::translateFieldName('CustomerDeliveryNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_delivery_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 72 + $startcol : SaleOrderTableMap::translateFieldName('CustomerDeliveryNumberAdd', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_delivery_number_add = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 73 + $startcol : SaleOrderTableMap::translateFieldName('CustomerDeliveryPostal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_delivery_postal = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 74 + $startcol : SaleOrderTableMap::translateFieldName('CustomerDeliveryCity', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_delivery_city = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 75 + $startcol : SaleOrderTableMap::translateFieldName('CustomerDeliveryCountry', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_delivery_country = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 76 + $startcol : SaleOrderTableMap::translateFieldName('CustomerDeliveryUsaState', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_delivery_usa_state = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 77 + $startcol : SaleOrderTableMap::translateFieldName('CustomerDeliveryAddressL1', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_delivery_address_l1 = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 78 + $startcol : SaleOrderTableMap::translateFieldName('CustomerDeliveryAddressL2', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_delivery_address_l2 = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 79 + $startcol : SaleOrderTableMap::translateFieldName('CpFirstName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cp_first_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 80 + $startcol : SaleOrderTableMap::translateFieldName('CpLastName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cp_last_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 81 + $startcol : SaleOrderTableMap::translateFieldName('CpSallutation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cp_sallutation = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 82 + $startcol : SaleOrderTableMap::translateFieldName('Reference', TableMap::TYPE_PHPNAME, $indexType)];
            $this->reference = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 83 + $startcol : SaleOrderTableMap::translateFieldName('ShippingNote', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shipping_note = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 84 + $startcol : SaleOrderTableMap::translateFieldName('CustomerOrderNote', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_order_note = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 85 + $startcol : SaleOrderTableMap::translateFieldName('ShippingOrderNote', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shipping_order_note = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 86 + $startcol : SaleOrderTableMap::translateFieldName('HasWrap', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_wrap = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 87 + $startcol : SaleOrderTableMap::translateFieldName('WrappingPrice', TableMap::TYPE_PHPNAME, $indexType)];
            $this->wrapping_price = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 88 + $startcol : SaleOrderTableMap::translateFieldName('ShippingCarrier', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shipping_carrier = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 89 + $startcol : SaleOrderTableMap::translateFieldName('ShippingPrice', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shipping_price = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 90 + $startcol : SaleOrderTableMap::translateFieldName('ShippingMethodId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shipping_method_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 91 + $startcol : SaleOrderTableMap::translateFieldName('ShippingHasTrackTrace', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shipping_has_track_trace = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 92 + $startcol : SaleOrderTableMap::translateFieldName('ShippingId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->shipping_id = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 93 + $startcol : SaleOrderTableMap::translateFieldName('TrackingCode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tracking_code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 94 + $startcol : SaleOrderTableMap::translateFieldName('TrackingUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tracking_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 95 + $startcol : SaleOrderTableMap::translateFieldName('PaymethodName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->paymethod_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 96 + $startcol : SaleOrderTableMap::translateFieldName('PaymethodCode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->paymethod_code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 97 + $startcol : SaleOrderTableMap::translateFieldName('PaymethodId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->paymethod_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 98 + $startcol : SaleOrderTableMap::translateFieldName('ShippingRegistrationDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->shipping_registration_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 99 + $startcol : SaleOrderTableMap::translateFieldName('InvoiceByPostal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->invoice_by_postal = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 100 + $startcol : SaleOrderTableMap::translateFieldName('PackingSlipType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->packing_slip_type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 101 + $startcol : SaleOrderTableMap::translateFieldName('WorkInProgress', TableMap::TYPE_PHPNAME, $indexType)];
            $this->work_in_progress = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 102 + $startcol : SaleOrderTableMap::translateFieldName('PackageAmount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->package_amount = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 103 + $startcol : SaleOrderTableMap::translateFieldName('CouponCode', TableMap::TYPE_PHPNAME, $indexType)];
            $this->coupon_code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 104 + $startcol : SaleOrderTableMap::translateFieldName('CouponDiscountAmount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->coupon_discount_amount = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 105 + $startcol : SaleOrderTableMap::translateFieldName('CouponDiscountPercentage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->coupon_discount_percentage = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 106 + $startcol : SaleOrderTableMap::translateFieldName('HideUntill', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00') {
                $col = null;
            }
            $this->hide_untill = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 107; // 107 = SaleOrderTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Sale\\SaleOrder'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aSite !== null && $this->site_id !== $this->aSite->getId()) {
            $this->aSite = null;
        }
        if ($this->aSaleOrderStatus !== null && $this->sale_order_status_id !== $this->aSaleOrderStatus->getId()) {
            $this->aSaleOrderStatus = null;
        }
        if ($this->aUser !== null && $this->created_by_user_id !== $this->aUser->getId()) {
            $this->aUser = null;
        }
        if ($this->aShippingMethod !== null && $this->shipping_method_id !== $this->aShippingMethod->getId()) {
            $this->aShippingMethod = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SaleOrderTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSaleOrderQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSite = null;
            $this->aSaleOrderStatus = null;
            $this->aShippingMethod = null;
            $this->aUser = null;
            $this->collSaleOrderPayments = null;

            $this->collPayments = null;

            $this->collSaleOrderEmails = null;

            $this->collSaleOrderItems = null;

            $this->collSaleOrderNotifications = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see SaleOrder::setDeleted()
     * @see SaleOrder::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSaleOrderQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SaleOrderTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSite !== null) {
                if ($this->aSite->isModified() || $this->aSite->isNew()) {
                    $affectedRows += $this->aSite->save($con);
                }
                $this->setSite($this->aSite);
            }

            if ($this->aSaleOrderStatus !== null) {
                if ($this->aSaleOrderStatus->isModified() || $this->aSaleOrderStatus->isNew()) {
                    $affectedRows += $this->aSaleOrderStatus->save($con);
                }
                $this->setSaleOrderStatus($this->aSaleOrderStatus);
            }

            if ($this->aShippingMethod !== null) {
                if ($this->aShippingMethod->isModified() || $this->aShippingMethod->isNew()) {
                    $affectedRows += $this->aShippingMethod->save($con);
                }
                $this->setShippingMethod($this->aShippingMethod);
            }

            if ($this->aUser !== null) {
                if ($this->aUser->isModified() || $this->aUser->isNew()) {
                    $affectedRows += $this->aUser->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->saleOrderPaymentsScheduledForDeletion !== null) {
                if (!$this->saleOrderPaymentsScheduledForDeletion->isEmpty()) {
                    \Model\Sale\SaleOrderPaymentQuery::create()
                        ->filterByPrimaryKeys($this->saleOrderPaymentsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->saleOrderPaymentsScheduledForDeletion = null;
                }
            }

            if ($this->collSaleOrderPayments !== null) {
                foreach ($this->collSaleOrderPayments as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->paymentsScheduledForDeletion !== null) {
                if (!$this->paymentsScheduledForDeletion->isEmpty()) {
                    foreach ($this->paymentsScheduledForDeletion as $payment) {
                        // need to save related object because we set the relation to null
                        $payment->save($con);
                    }
                    $this->paymentsScheduledForDeletion = null;
                }
            }

            if ($this->collPayments !== null) {
                foreach ($this->collPayments as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saleOrderEmailsScheduledForDeletion !== null) {
                if (!$this->saleOrderEmailsScheduledForDeletion->isEmpty()) {
                    \Model\Sale\SaleOrderEmailQuery::create()
                        ->filterByPrimaryKeys($this->saleOrderEmailsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->saleOrderEmailsScheduledForDeletion = null;
                }
            }

            if ($this->collSaleOrderEmails !== null) {
                foreach ($this->collSaleOrderEmails as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saleOrderItemsScheduledForDeletion !== null) {
                if (!$this->saleOrderItemsScheduledForDeletion->isEmpty()) {
                    \Model\Sale\SaleOrderItemQuery::create()
                        ->filterByPrimaryKeys($this->saleOrderItemsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->saleOrderItemsScheduledForDeletion = null;
                }
            }

            if ($this->collSaleOrderItems !== null) {
                foreach ($this->collSaleOrderItems as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saleOrderNotificationsScheduledForDeletion !== null) {
                if (!$this->saleOrderNotificationsScheduledForDeletion->isEmpty()) {
                    foreach ($this->saleOrderNotificationsScheduledForDeletion as $saleOrderNotification) {
                        // need to save related object because we set the relation to null
                        $saleOrderNotification->save($con);
                    }
                    $this->saleOrderNotificationsScheduledForDeletion = null;
                }
            }

            if ($this->collSaleOrderNotifications !== null) {
                foreach ($this->collSaleOrderNotifications as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[SaleOrderTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SaleOrderTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SaleOrderTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SITE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'site_id';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_LANGUAGE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'language_id';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_CLOSED)) {
            $modifiedColumns[':p' . $index++]  = 'is_closed';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_FULLY_PAID)) {
            $modifiedColumns[':p' . $index++]  = 'is_fully_paid';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAY_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'pay_date';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_READY_FOR_SHIPPING)) {
            $modifiedColumns[':p' . $index++]  = 'is_ready_for_shipping';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_SHIPPING_CHECKED)) {
            $modifiedColumns[':p' . $index++]  = 'is_shipping_checked';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_FULLY_SHIPPED)) {
            $modifiedColumns[':p' . $index++]  = 'is_fully_shipped';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_PUSHED_TO_ACCOUNTING)) {
            $modifiedColumns[':p' . $index++]  = 'is_pushed_to_accounting';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIP_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'ship_date';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SALE_ORDER_STATUS_ID)) {
            $modifiedColumns[':p' . $index++]  = 'sale_order_status_id';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_HANDLED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'handled_by';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SOURCE)) {
            $modifiedColumns[':p' . $index++]  = 'source';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_COMPLETED)) {
            $modifiedColumns[':p' . $index++]  = 'is_completed';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CREATED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'created_by_user_id';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CREATED_ON)) {
            $modifiedColumns[':p' . $index++]  = 'created_on';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_ESTIMATED_DELIVERY_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'estimated_delivery_date';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'customer_id';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_ENVELOPE_WEIGHT_GRAMS)) {
            $modifiedColumns[':p' . $index++]  = 'envelope_weight_grams';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_WEBSHOP_USER_SESSION_ID)) {
            $modifiedColumns[':p' . $index++]  = 'webshop_user_session_id';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_INVOICE_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'invoice_number';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_INVOICE_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'invoice_date';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAYTERM_DAYS)) {
            $modifiedColumns[':p' . $index++]  = 'payterm_days';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAYTERM_ORIGINAL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'payterm_original_id';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAYTERM_STRING)) {
            $modifiedColumns[':p' . $index++]  = 'payterm_string';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_ORDER_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'order_number';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_DELETED)) {
            $modifiedColumns[':p' . $index++]  = 'is_deleted';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_COMPANY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'company_name';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_DEBITOR)) {
            $modifiedColumns[':p' . $index++]  = 'debitor';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_CUSTOM_FOLDER)) {
            $modifiedColumns[':p' . $index++]  = 'our_custom_folder';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_SLOGAN)) {
            $modifiedColumns[':p' . $index++]  = 'our_slogan';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_PHONE)) {
            $modifiedColumns[':p' . $index++]  = 'our_phone';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_FAX)) {
            $modifiedColumns[':p' . $index++]  = 'our_fax';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_WEBSITE)) {
            $modifiedColumns[':p' . $index++]  = 'our_website';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'our_email';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_VAT_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'our_vat_number';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_CHAMBER_OF_COMMERCE)) {
            $modifiedColumns[':p' . $index++]  = 'our_chamber_of_commerce';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_IBAN_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'our_iban_number';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_BIC_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'our_bic_number';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_COMPANY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'our_general_company_name';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_STREET)) {
            $modifiedColumns[':p' . $index++]  = 'our_general_street';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'our_general_number';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_NUMBER_ADD)) {
            $modifiedColumns[':p' . $index++]  = 'our_general_number_add';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_POSTAL)) {
            $modifiedColumns[':p' . $index++]  = 'our_general_postal';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_PO_BOX)) {
            $modifiedColumns[':p' . $index++]  = 'our_general_po_box';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_CITY)) {
            $modifiedColumns[':p' . $index++]  = 'our_general_city';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_COUNTRY)) {
            $modifiedColumns[':p' . $index++]  = 'our_general_country';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_PHONE)) {
            $modifiedColumns[':p' . $index++]  = 'customer_phone';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_FAX)) {
            $modifiedColumns[':p' . $index++]  = 'customer_fax';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'customer_email';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_CHAMBER_OF_COMMERCE)) {
            $modifiedColumns[':p' . $index++]  = 'customer_chamber_of_commerce';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_VAT_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'customer_vat_number';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_ORDER_REFERENCE)) {
            $modifiedColumns[':p' . $index++]  = 'customer_order_reference';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_ORDER_PLACED_BY)) {
            $modifiedColumns[':p' . $index++]  = 'customer_order_placed_by';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_ID)) {
            $modifiedColumns[':p' . $index++]  = 'customer_invoice_address_id';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_COMPANY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'customer_invoice_company_name';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ATTN_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'customer_invoice_attn_name';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_STREET)) {
            $modifiedColumns[':p' . $index++]  = 'customer_invoice_street';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'customer_invoice_number';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER_ADD)) {
            $modifiedColumns[':p' . $index++]  = 'customer_invoice_number_add';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_POSTAL)) {
            $modifiedColumns[':p' . $index++]  = 'customer_invoice_postal';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_CITY)) {
            $modifiedColumns[':p' . $index++]  = 'customer_invoice_city';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_COUNTRY)) {
            $modifiedColumns[':p' . $index++]  = 'customer_invoice_country';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_USA_STATE)) {
            $modifiedColumns[':p' . $index++]  = 'customer_invoice_usa_state';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L1)) {
            $modifiedColumns[':p' . $index++]  = 'customer_invoice_address_l1';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L2)) {
            $modifiedColumns[':p' . $index++]  = 'customer_invoice_address_l2';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_ID)) {
            $modifiedColumns[':p' . $index++]  = 'customer_delivery_address_id';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COMPANY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'customer_delivery_company_name';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ATTN_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'customer_delivery_attn_name';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_STREET)) {
            $modifiedColumns[':p' . $index++]  = 'customer_delivery_street';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'customer_delivery_number';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER_ADD)) {
            $modifiedColumns[':p' . $index++]  = 'customer_delivery_number_add';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_POSTAL)) {
            $modifiedColumns[':p' . $index++]  = 'customer_delivery_postal';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_CITY)) {
            $modifiedColumns[':p' . $index++]  = 'customer_delivery_city';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COUNTRY)) {
            $modifiedColumns[':p' . $index++]  = 'customer_delivery_country';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_USA_STATE)) {
            $modifiedColumns[':p' . $index++]  = 'customer_delivery_usa_state';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L1)) {
            $modifiedColumns[':p' . $index++]  = 'customer_delivery_address_l1';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L2)) {
            $modifiedColumns[':p' . $index++]  = 'customer_delivery_address_l2';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CP_FIRST_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'cp_first_name';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CP_LAST_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'cp_last_name';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CP_SALLUTATION)) {
            $modifiedColumns[':p' . $index++]  = 'cp_sallutation';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_REFERENCE)) {
            $modifiedColumns[':p' . $index++]  = 'reference';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_NOTE)) {
            $modifiedColumns[':p' . $index++]  = 'shipping_note';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_ORDER_NOTE)) {
            $modifiedColumns[':p' . $index++]  = 'customer_order_note';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_ORDER_NOTE)) {
            $modifiedColumns[':p' . $index++]  = 'shipping_order_note';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_HAS_WRAP)) {
            $modifiedColumns[':p' . $index++]  = 'has_wrap';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_WRAPPING_PRICE)) {
            $modifiedColumns[':p' . $index++]  = 'wrapping_price';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_CARRIER)) {
            $modifiedColumns[':p' . $index++]  = 'shipping_carrier';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_PRICE)) {
            $modifiedColumns[':p' . $index++]  = 'shipping_price';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_METHOD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'shipping_method_id';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_HAS_TRACK_TRACE)) {
            $modifiedColumns[':p' . $index++]  = 'shipping_has_track_trace';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_ID)) {
            $modifiedColumns[':p' . $index++]  = 'shipping_id';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_TRACKING_CODE)) {
            $modifiedColumns[':p' . $index++]  = 'tracking_code';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_TRACKING_URL)) {
            $modifiedColumns[':p' . $index++]  = 'tracking_url';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAYMETHOD_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'paymethod_name';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAYMETHOD_CODE)) {
            $modifiedColumns[':p' . $index++]  = 'paymethod_code';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAYMETHOD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'paymethod_id';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_REGISTRATION_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'shipping_registration_date';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_INVOICE_BY_POSTAL)) {
            $modifiedColumns[':p' . $index++]  = 'invoice_by_postal';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PACKING_SLIP_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'packing_slip_type';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_WORK_IN_PROGRESS)) {
            $modifiedColumns[':p' . $index++]  = 'work_in_progress';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PACKAGE_AMOUNT)) {
            $modifiedColumns[':p' . $index++]  = 'package_amount';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_COUPON_CODE)) {
            $modifiedColumns[':p' . $index++]  = 'coupon_code';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_COUPON_DISCOUNT_AMOUNT)) {
            $modifiedColumns[':p' . $index++]  = 'coupon_discount_amount';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_COUPON_DISCOUNT_PERCENTAGE)) {
            $modifiedColumns[':p' . $index++]  = 'coupon_discount_percentage';
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_HIDE_UNTILL)) {
            $modifiedColumns[':p' . $index++]  = 'hide_untill';
        }

        $sql = sprintf(
            'INSERT INTO sale_order (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'site_id':
                        $stmt->bindValue($identifier, $this->site_id, PDO::PARAM_INT);
                        break;
                    case 'language_id':
                        $stmt->bindValue($identifier, $this->language_id, PDO::PARAM_INT);
                        break;
                    case 'is_closed':
                        $stmt->bindValue($identifier, (int) $this->is_closed, PDO::PARAM_INT);
                        break;
                    case 'is_fully_paid':
                        $stmt->bindValue($identifier, (int) $this->is_fully_paid, PDO::PARAM_INT);
                        break;
                    case 'pay_date':
                        $stmt->bindValue($identifier, $this->pay_date ? $this->pay_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'is_ready_for_shipping':
                        $stmt->bindValue($identifier, (int) $this->is_ready_for_shipping, PDO::PARAM_INT);
                        break;
                    case 'is_shipping_checked':
                        $stmt->bindValue($identifier, (int) $this->is_shipping_checked, PDO::PARAM_INT);
                        break;
                    case 'is_fully_shipped':
                        $stmt->bindValue($identifier, (int) $this->is_fully_shipped, PDO::PARAM_INT);
                        break;
                    case 'is_pushed_to_accounting':
                        $stmt->bindValue($identifier, (int) $this->is_pushed_to_accounting, PDO::PARAM_INT);
                        break;
                    case 'ship_date':
                        $stmt->bindValue($identifier, $this->ship_date ? $this->ship_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'sale_order_status_id':
                        $stmt->bindValue($identifier, $this->sale_order_status_id, PDO::PARAM_INT);
                        break;
                    case 'handled_by':
                        $stmt->bindValue($identifier, $this->handled_by, PDO::PARAM_STR);
                        break;
                    case 'source':
                        $stmt->bindValue($identifier, $this->source, PDO::PARAM_STR);
                        break;
                    case 'is_completed':
                        $stmt->bindValue($identifier, (int) $this->is_completed, PDO::PARAM_INT);
                        break;
                    case 'created_by_user_id':
                        $stmt->bindValue($identifier, $this->created_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'created_on':
                        $stmt->bindValue($identifier, $this->created_on ? $this->created_on->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'estimated_delivery_date':
                        $stmt->bindValue($identifier, $this->estimated_delivery_date ? $this->estimated_delivery_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'customer_id':
                        $stmt->bindValue($identifier, $this->customer_id, PDO::PARAM_INT);
                        break;
                    case 'envelope_weight_grams':
                        $stmt->bindValue($identifier, $this->envelope_weight_grams, PDO::PARAM_INT);
                        break;
                    case 'webshop_user_session_id':
                        $stmt->bindValue($identifier, $this->webshop_user_session_id, PDO::PARAM_INT);
                        break;
                    case 'invoice_number':
                        $stmt->bindValue($identifier, $this->invoice_number, PDO::PARAM_STR);
                        break;
                    case 'invoice_date':
                        $stmt->bindValue($identifier, $this->invoice_date ? $this->invoice_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'payterm_days':
                        $stmt->bindValue($identifier, $this->payterm_days, PDO::PARAM_INT);
                        break;
                    case 'payterm_original_id':
                        $stmt->bindValue($identifier, $this->payterm_original_id, PDO::PARAM_INT);
                        break;
                    case 'payterm_string':
                        $stmt->bindValue($identifier, $this->payterm_string, PDO::PARAM_STR);
                        break;
                    case 'order_number':
                        $stmt->bindValue($identifier, $this->order_number, PDO::PARAM_STR);
                        break;
                    case 'is_deleted':
                        $stmt->bindValue($identifier, (int) $this->is_deleted, PDO::PARAM_INT);
                        break;
                    case 'company_name':
                        $stmt->bindValue($identifier, $this->company_name, PDO::PARAM_STR);
                        break;
                    case 'debitor':
                        $stmt->bindValue($identifier, $this->debitor, PDO::PARAM_STR);
                        break;
                    case 'our_custom_folder':
                        $stmt->bindValue($identifier, $this->our_custom_folder, PDO::PARAM_STR);
                        break;
                    case 'our_slogan':
                        $stmt->bindValue($identifier, $this->our_slogan, PDO::PARAM_STR);
                        break;
                    case 'our_phone':
                        $stmt->bindValue($identifier, $this->our_phone, PDO::PARAM_STR);
                        break;
                    case 'our_fax':
                        $stmt->bindValue($identifier, $this->our_fax, PDO::PARAM_STR);
                        break;
                    case 'our_website':
                        $stmt->bindValue($identifier, $this->our_website, PDO::PARAM_STR);
                        break;
                    case 'our_email':
                        $stmt->bindValue($identifier, $this->our_email, PDO::PARAM_STR);
                        break;
                    case 'our_vat_number':
                        $stmt->bindValue($identifier, $this->our_vat_number, PDO::PARAM_STR);
                        break;
                    case 'our_chamber_of_commerce':
                        $stmt->bindValue($identifier, $this->our_chamber_of_commerce, PDO::PARAM_STR);
                        break;
                    case 'our_iban_number':
                        $stmt->bindValue($identifier, $this->our_iban_number, PDO::PARAM_STR);
                        break;
                    case 'our_bic_number':
                        $stmt->bindValue($identifier, $this->our_bic_number, PDO::PARAM_STR);
                        break;
                    case 'our_general_company_name':
                        $stmt->bindValue($identifier, $this->our_general_company_name, PDO::PARAM_STR);
                        break;
                    case 'our_general_street':
                        $stmt->bindValue($identifier, $this->our_general_street, PDO::PARAM_STR);
                        break;
                    case 'our_general_number':
                        $stmt->bindValue($identifier, $this->our_general_number, PDO::PARAM_STR);
                        break;
                    case 'our_general_number_add':
                        $stmt->bindValue($identifier, $this->our_general_number_add, PDO::PARAM_STR);
                        break;
                    case 'our_general_postal':
                        $stmt->bindValue($identifier, $this->our_general_postal, PDO::PARAM_STR);
                        break;
                    case 'our_general_po_box':
                        $stmt->bindValue($identifier, $this->our_general_po_box, PDO::PARAM_STR);
                        break;
                    case 'our_general_city':
                        $stmt->bindValue($identifier, $this->our_general_city, PDO::PARAM_STR);
                        break;
                    case 'our_general_country':
                        $stmt->bindValue($identifier, $this->our_general_country, PDO::PARAM_STR);
                        break;
                    case 'customer_phone':
                        $stmt->bindValue($identifier, $this->customer_phone, PDO::PARAM_STR);
                        break;
                    case 'customer_fax':
                        $stmt->bindValue($identifier, $this->customer_fax, PDO::PARAM_STR);
                        break;
                    case 'customer_email':
                        $stmt->bindValue($identifier, $this->customer_email, PDO::PARAM_STR);
                        break;
                    case 'customer_chamber_of_commerce':
                        $stmt->bindValue($identifier, $this->customer_chamber_of_commerce, PDO::PARAM_STR);
                        break;
                    case 'customer_vat_number':
                        $stmt->bindValue($identifier, $this->customer_vat_number, PDO::PARAM_STR);
                        break;
                    case 'customer_order_reference':
                        $stmt->bindValue($identifier, $this->customer_order_reference, PDO::PARAM_STR);
                        break;
                    case 'customer_order_placed_by':
                        $stmt->bindValue($identifier, $this->customer_order_placed_by, PDO::PARAM_STR);
                        break;
                    case 'customer_invoice_address_id':
                        $stmt->bindValue($identifier, $this->customer_invoice_address_id, PDO::PARAM_INT);
                        break;
                    case 'customer_invoice_company_name':
                        $stmt->bindValue($identifier, $this->customer_invoice_company_name, PDO::PARAM_STR);
                        break;
                    case 'customer_invoice_attn_name':
                        $stmt->bindValue($identifier, $this->customer_invoice_attn_name, PDO::PARAM_STR);
                        break;
                    case 'customer_invoice_street':
                        $stmt->bindValue($identifier, $this->customer_invoice_street, PDO::PARAM_STR);
                        break;
                    case 'customer_invoice_number':
                        $stmt->bindValue($identifier, $this->customer_invoice_number, PDO::PARAM_STR);
                        break;
                    case 'customer_invoice_number_add':
                        $stmt->bindValue($identifier, $this->customer_invoice_number_add, PDO::PARAM_STR);
                        break;
                    case 'customer_invoice_postal':
                        $stmt->bindValue($identifier, $this->customer_invoice_postal, PDO::PARAM_STR);
                        break;
                    case 'customer_invoice_city':
                        $stmt->bindValue($identifier, $this->customer_invoice_city, PDO::PARAM_STR);
                        break;
                    case 'customer_invoice_country':
                        $stmt->bindValue($identifier, $this->customer_invoice_country, PDO::PARAM_STR);
                        break;
                    case 'customer_invoice_usa_state':
                        $stmt->bindValue($identifier, $this->customer_invoice_usa_state, PDO::PARAM_STR);
                        break;
                    case 'customer_invoice_address_l1':
                        $stmt->bindValue($identifier, $this->customer_invoice_address_l1, PDO::PARAM_STR);
                        break;
                    case 'customer_invoice_address_l2':
                        $stmt->bindValue($identifier, $this->customer_invoice_address_l2, PDO::PARAM_STR);
                        break;
                    case 'customer_delivery_address_id':
                        $stmt->bindValue($identifier, $this->customer_delivery_address_id, PDO::PARAM_INT);
                        break;
                    case 'customer_delivery_company_name':
                        $stmt->bindValue($identifier, $this->customer_delivery_company_name, PDO::PARAM_STR);
                        break;
                    case 'customer_delivery_attn_name':
                        $stmt->bindValue($identifier, $this->customer_delivery_attn_name, PDO::PARAM_STR);
                        break;
                    case 'customer_delivery_street':
                        $stmt->bindValue($identifier, $this->customer_delivery_street, PDO::PARAM_STR);
                        break;
                    case 'customer_delivery_number':
                        $stmt->bindValue($identifier, $this->customer_delivery_number, PDO::PARAM_STR);
                        break;
                    case 'customer_delivery_number_add':
                        $stmt->bindValue($identifier, $this->customer_delivery_number_add, PDO::PARAM_STR);
                        break;
                    case 'customer_delivery_postal':
                        $stmt->bindValue($identifier, $this->customer_delivery_postal, PDO::PARAM_STR);
                        break;
                    case 'customer_delivery_city':
                        $stmt->bindValue($identifier, $this->customer_delivery_city, PDO::PARAM_STR);
                        break;
                    case 'customer_delivery_country':
                        $stmt->bindValue($identifier, $this->customer_delivery_country, PDO::PARAM_STR);
                        break;
                    case 'customer_delivery_usa_state':
                        $stmt->bindValue($identifier, $this->customer_delivery_usa_state, PDO::PARAM_STR);
                        break;
                    case 'customer_delivery_address_l1':
                        $stmt->bindValue($identifier, $this->customer_delivery_address_l1, PDO::PARAM_STR);
                        break;
                    case 'customer_delivery_address_l2':
                        $stmt->bindValue($identifier, $this->customer_delivery_address_l2, PDO::PARAM_STR);
                        break;
                    case 'cp_first_name':
                        $stmt->bindValue($identifier, $this->cp_first_name, PDO::PARAM_STR);
                        break;
                    case 'cp_last_name':
                        $stmt->bindValue($identifier, $this->cp_last_name, PDO::PARAM_STR);
                        break;
                    case 'cp_sallutation':
                        $stmt->bindValue($identifier, $this->cp_sallutation, PDO::PARAM_STR);
                        break;
                    case 'reference':
                        $stmt->bindValue($identifier, $this->reference, PDO::PARAM_STR);
                        break;
                    case 'shipping_note':
                        $stmt->bindValue($identifier, $this->shipping_note, PDO::PARAM_STR);
                        break;
                    case 'customer_order_note':
                        $stmt->bindValue($identifier, $this->customer_order_note, PDO::PARAM_STR);
                        break;
                    case 'shipping_order_note':
                        $stmt->bindValue($identifier, $this->shipping_order_note, PDO::PARAM_STR);
                        break;
                    case 'has_wrap':
                        $stmt->bindValue($identifier, (int) $this->has_wrap, PDO::PARAM_INT);
                        break;
                    case 'wrapping_price':
                        $stmt->bindValue($identifier, $this->wrapping_price, PDO::PARAM_STR);
                        break;
                    case 'shipping_carrier':
                        $stmt->bindValue($identifier, $this->shipping_carrier, PDO::PARAM_STR);
                        break;
                    case 'shipping_price':
                        $stmt->bindValue($identifier, $this->shipping_price, PDO::PARAM_STR);
                        break;
                    case 'shipping_method_id':
                        $stmt->bindValue($identifier, $this->shipping_method_id, PDO::PARAM_INT);
                        break;
                    case 'shipping_has_track_trace':
                        $stmt->bindValue($identifier, (int) $this->shipping_has_track_trace, PDO::PARAM_INT);
                        break;
                    case 'shipping_id':
                        $stmt->bindValue($identifier, $this->shipping_id, PDO::PARAM_STR);
                        break;
                    case 'tracking_code':
                        $stmt->bindValue($identifier, $this->tracking_code, PDO::PARAM_STR);
                        break;
                    case 'tracking_url':
                        $stmt->bindValue($identifier, $this->tracking_url, PDO::PARAM_STR);
                        break;
                    case 'paymethod_name':
                        $stmt->bindValue($identifier, $this->paymethod_name, PDO::PARAM_STR);
                        break;
                    case 'paymethod_code':
                        $stmt->bindValue($identifier, $this->paymethod_code, PDO::PARAM_STR);
                        break;
                    case 'paymethod_id':
                        $stmt->bindValue($identifier, $this->paymethod_id, PDO::PARAM_INT);
                        break;
                    case 'shipping_registration_date':
                        $stmt->bindValue($identifier, $this->shipping_registration_date ? $this->shipping_registration_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'invoice_by_postal':
                        $stmt->bindValue($identifier, (int) $this->invoice_by_postal, PDO::PARAM_INT);
                        break;
                    case 'packing_slip_type':
                        $stmt->bindValue($identifier, $this->packing_slip_type, PDO::PARAM_STR);
                        break;
                    case 'work_in_progress':
                        $stmt->bindValue($identifier, (int) $this->work_in_progress, PDO::PARAM_INT);
                        break;
                    case 'package_amount':
                        $stmt->bindValue($identifier, $this->package_amount, PDO::PARAM_INT);
                        break;
                    case 'coupon_code':
                        $stmt->bindValue($identifier, $this->coupon_code, PDO::PARAM_STR);
                        break;
                    case 'coupon_discount_amount':
                        $stmt->bindValue($identifier, $this->coupon_discount_amount, PDO::PARAM_STR);
                        break;
                    case 'coupon_discount_percentage':
                        $stmt->bindValue($identifier, $this->coupon_discount_percentage, PDO::PARAM_STR);
                        break;
                    case 'hide_untill':
                        $stmt->bindValue($identifier, $this->hide_untill ? $this->hide_untill->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SaleOrderTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getSiteId();
                break;
            case 2:
                return $this->getLanguageId();
                break;
            case 3:
                return $this->getIsClosed();
                break;
            case 4:
                return $this->getIsFullyPaid();
                break;
            case 5:
                return $this->getPayDate();
                break;
            case 6:
                return $this->getIsReadyForShipping();
                break;
            case 7:
                return $this->getIsShippingChecked();
                break;
            case 8:
                return $this->getIsFullyShipped();
                break;
            case 9:
                return $this->getIsPushedToAccounting();
                break;
            case 10:
                return $this->getShipDate();
                break;
            case 11:
                return $this->getSaleOrderStatusId();
                break;
            case 12:
                return $this->getHandledBy();
                break;
            case 13:
                return $this->getSource();
                break;
            case 14:
                return $this->getIsCompleted();
                break;
            case 15:
                return $this->getCreatedByUserId();
                break;
            case 16:
                return $this->getCreatedOn();
                break;
            case 17:
                return $this->getEstimatedDeliveryDate();
                break;
            case 18:
                return $this->getCustomerId();
                break;
            case 19:
                return $this->getEnvelopeWeightGrams();
                break;
            case 20:
                return $this->getWebshopUserSessionId();
                break;
            case 21:
                return $this->getInvoiceNumber();
                break;
            case 22:
                return $this->getInvoiceDate();
                break;
            case 23:
                return $this->getPaytermDays();
                break;
            case 24:
                return $this->getPaytermOriginalId();
                break;
            case 25:
                return $this->getPaytermString();
                break;
            case 26:
                return $this->getOrderNumber();
                break;
            case 27:
                return $this->getItemDeleted();
                break;
            case 28:
                return $this->getCompanyName();
                break;
            case 29:
                return $this->getDebitor();
                break;
            case 30:
                return $this->getOurCustomFolder();
                break;
            case 31:
                return $this->getOurSlogan();
                break;
            case 32:
                return $this->getOurPhone();
                break;
            case 33:
                return $this->getOurFax();
                break;
            case 34:
                return $this->getOurWebsite();
                break;
            case 35:
                return $this->getOurEmail();
                break;
            case 36:
                return $this->getOurVatNumber();
                break;
            case 37:
                return $this->getOurChamberOfCommerce();
                break;
            case 38:
                return $this->getOurIbanNumber();
                break;
            case 39:
                return $this->getOurBicNumber();
                break;
            case 40:
                return $this->getOurGeneralCompanyName();
                break;
            case 41:
                return $this->getOurGeneralStreet();
                break;
            case 42:
                return $this->getOurGeneralNumber();
                break;
            case 43:
                return $this->getOurGeneralNumberAdd();
                break;
            case 44:
                return $this->getOurGeneralPostal();
                break;
            case 45:
                return $this->getOurGeneralPoBox();
                break;
            case 46:
                return $this->getOurGeneralCity();
                break;
            case 47:
                return $this->getOurGeneralCountry();
                break;
            case 48:
                return $this->getCustomerPhone();
                break;
            case 49:
                return $this->getCustomerFax();
                break;
            case 50:
                return $this->getCustomerEmail();
                break;
            case 51:
                return $this->getCustomerChamberOfCommerce();
                break;
            case 52:
                return $this->getCustomerVatNumber();
                break;
            case 53:
                return $this->getCustomerOrderReference();
                break;
            case 54:
                return $this->getCustomerOrderPlacedBy();
                break;
            case 55:
                return $this->getCustomerInvoiceAddressId();
                break;
            case 56:
                return $this->getCustomerInvoiceCompanyName();
                break;
            case 57:
                return $this->getCustomerInvoiceAttnName();
                break;
            case 58:
                return $this->getCustomerInvoiceStreet();
                break;
            case 59:
                return $this->getCustomerInvoiceNumber();
                break;
            case 60:
                return $this->getCustomerInvoiceNumberAdd();
                break;
            case 61:
                return $this->getCustomerInvoicePostal();
                break;
            case 62:
                return $this->getCustomerInvoiceCity();
                break;
            case 63:
                return $this->getCustomerInvoiceCountry();
                break;
            case 64:
                return $this->getCustomerInvoiceUsaState();
                break;
            case 65:
                return $this->getCustomerInvoiceAddressL1();
                break;
            case 66:
                return $this->getCustomerInvoiceAddressL2();
                break;
            case 67:
                return $this->getCustomerDeliveryAddressId();
                break;
            case 68:
                return $this->getCustomerDeliveryCompanyName();
                break;
            case 69:
                return $this->getCustomerDeliveryAttnName();
                break;
            case 70:
                return $this->getCustomerDeliveryStreet();
                break;
            case 71:
                return $this->getCustomerDeliveryNumber();
                break;
            case 72:
                return $this->getCustomerDeliveryNumberAdd();
                break;
            case 73:
                return $this->getCustomerDeliveryPostal();
                break;
            case 74:
                return $this->getCustomerDeliveryCity();
                break;
            case 75:
                return $this->getCustomerDeliveryCountry();
                break;
            case 76:
                return $this->getCustomerDeliveryUsaState();
                break;
            case 77:
                return $this->getCustomerDeliveryAddressL1();
                break;
            case 78:
                return $this->getCustomerDeliveryAddressL2();
                break;
            case 79:
                return $this->getCpFirstName();
                break;
            case 80:
                return $this->getCpLastName();
                break;
            case 81:
                return $this->getCpSallutation();
                break;
            case 82:
                return $this->getReference();
                break;
            case 83:
                return $this->getShippingNote();
                break;
            case 84:
                return $this->getCustomerOrderNote();
                break;
            case 85:
                return $this->getShippingOrderNote();
                break;
            case 86:
                return $this->getHasWrap();
                break;
            case 87:
                return $this->getWrappingPrice();
                break;
            case 88:
                return $this->getShippingCarrier();
                break;
            case 89:
                return $this->getShippingPrice();
                break;
            case 90:
                return $this->getShippingMethodId();
                break;
            case 91:
                return $this->getShippingHasTrackTrace();
                break;
            case 92:
                return $this->getShippingId();
                break;
            case 93:
                return $this->getTrackingCode();
                break;
            case 94:
                return $this->getTrackingUrl();
                break;
            case 95:
                return $this->getPaymethodName();
                break;
            case 96:
                return $this->getPaymethodCode();
                break;
            case 97:
                return $this->getPaymethodId();
                break;
            case 98:
                return $this->getShippingRegistrationDate();
                break;
            case 99:
                return $this->getInvoiceByPostal();
                break;
            case 100:
                return $this->getPackingSlipType();
                break;
            case 101:
                return $this->getWorkInProgress();
                break;
            case 102:
                return $this->getPackageAmount();
                break;
            case 103:
                return $this->getCouponCode();
                break;
            case 104:
                return $this->getCouponDiscountAmount();
                break;
            case 105:
                return $this->getCouponDiscountPercentage();
                break;
            case 106:
                return $this->getHideUntill();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['SaleOrder'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SaleOrder'][$this->hashCode()] = true;
        $keys = SaleOrderTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getSiteId(),
            $keys[2] => $this->getLanguageId(),
            $keys[3] => $this->getIsClosed(),
            $keys[4] => $this->getIsFullyPaid(),
            $keys[5] => $this->getPayDate(),
            $keys[6] => $this->getIsReadyForShipping(),
            $keys[7] => $this->getIsShippingChecked(),
            $keys[8] => $this->getIsFullyShipped(),
            $keys[9] => $this->getIsPushedToAccounting(),
            $keys[10] => $this->getShipDate(),
            $keys[11] => $this->getSaleOrderStatusId(),
            $keys[12] => $this->getHandledBy(),
            $keys[13] => $this->getSource(),
            $keys[14] => $this->getIsCompleted(),
            $keys[15] => $this->getCreatedByUserId(),
            $keys[16] => $this->getCreatedOn(),
            $keys[17] => $this->getEstimatedDeliveryDate(),
            $keys[18] => $this->getCustomerId(),
            $keys[19] => $this->getEnvelopeWeightGrams(),
            $keys[20] => $this->getWebshopUserSessionId(),
            $keys[21] => $this->getInvoiceNumber(),
            $keys[22] => $this->getInvoiceDate(),
            $keys[23] => $this->getPaytermDays(),
            $keys[24] => $this->getPaytermOriginalId(),
            $keys[25] => $this->getPaytermString(),
            $keys[26] => $this->getOrderNumber(),
            $keys[27] => $this->getItemDeleted(),
            $keys[28] => $this->getCompanyName(),
            $keys[29] => $this->getDebitor(),
            $keys[30] => $this->getOurCustomFolder(),
            $keys[31] => $this->getOurSlogan(),
            $keys[32] => $this->getOurPhone(),
            $keys[33] => $this->getOurFax(),
            $keys[34] => $this->getOurWebsite(),
            $keys[35] => $this->getOurEmail(),
            $keys[36] => $this->getOurVatNumber(),
            $keys[37] => $this->getOurChamberOfCommerce(),
            $keys[38] => $this->getOurIbanNumber(),
            $keys[39] => $this->getOurBicNumber(),
            $keys[40] => $this->getOurGeneralCompanyName(),
            $keys[41] => $this->getOurGeneralStreet(),
            $keys[42] => $this->getOurGeneralNumber(),
            $keys[43] => $this->getOurGeneralNumberAdd(),
            $keys[44] => $this->getOurGeneralPostal(),
            $keys[45] => $this->getOurGeneralPoBox(),
            $keys[46] => $this->getOurGeneralCity(),
            $keys[47] => $this->getOurGeneralCountry(),
            $keys[48] => $this->getCustomerPhone(),
            $keys[49] => $this->getCustomerFax(),
            $keys[50] => $this->getCustomerEmail(),
            $keys[51] => $this->getCustomerChamberOfCommerce(),
            $keys[52] => $this->getCustomerVatNumber(),
            $keys[53] => $this->getCustomerOrderReference(),
            $keys[54] => $this->getCustomerOrderPlacedBy(),
            $keys[55] => $this->getCustomerInvoiceAddressId(),
            $keys[56] => $this->getCustomerInvoiceCompanyName(),
            $keys[57] => $this->getCustomerInvoiceAttnName(),
            $keys[58] => $this->getCustomerInvoiceStreet(),
            $keys[59] => $this->getCustomerInvoiceNumber(),
            $keys[60] => $this->getCustomerInvoiceNumberAdd(),
            $keys[61] => $this->getCustomerInvoicePostal(),
            $keys[62] => $this->getCustomerInvoiceCity(),
            $keys[63] => $this->getCustomerInvoiceCountry(),
            $keys[64] => $this->getCustomerInvoiceUsaState(),
            $keys[65] => $this->getCustomerInvoiceAddressL1(),
            $keys[66] => $this->getCustomerInvoiceAddressL2(),
            $keys[67] => $this->getCustomerDeliveryAddressId(),
            $keys[68] => $this->getCustomerDeliveryCompanyName(),
            $keys[69] => $this->getCustomerDeliveryAttnName(),
            $keys[70] => $this->getCustomerDeliveryStreet(),
            $keys[71] => $this->getCustomerDeliveryNumber(),
            $keys[72] => $this->getCustomerDeliveryNumberAdd(),
            $keys[73] => $this->getCustomerDeliveryPostal(),
            $keys[74] => $this->getCustomerDeliveryCity(),
            $keys[75] => $this->getCustomerDeliveryCountry(),
            $keys[76] => $this->getCustomerDeliveryUsaState(),
            $keys[77] => $this->getCustomerDeliveryAddressL1(),
            $keys[78] => $this->getCustomerDeliveryAddressL2(),
            $keys[79] => $this->getCpFirstName(),
            $keys[80] => $this->getCpLastName(),
            $keys[81] => $this->getCpSallutation(),
            $keys[82] => $this->getReference(),
            $keys[83] => $this->getShippingNote(),
            $keys[84] => $this->getCustomerOrderNote(),
            $keys[85] => $this->getShippingOrderNote(),
            $keys[86] => $this->getHasWrap(),
            $keys[87] => $this->getWrappingPrice(),
            $keys[88] => $this->getShippingCarrier(),
            $keys[89] => $this->getShippingPrice(),
            $keys[90] => $this->getShippingMethodId(),
            $keys[91] => $this->getShippingHasTrackTrace(),
            $keys[92] => $this->getShippingId(),
            $keys[93] => $this->getTrackingCode(),
            $keys[94] => $this->getTrackingUrl(),
            $keys[95] => $this->getPaymethodName(),
            $keys[96] => $this->getPaymethodCode(),
            $keys[97] => $this->getPaymethodId(),
            $keys[98] => $this->getShippingRegistrationDate(),
            $keys[99] => $this->getInvoiceByPostal(),
            $keys[100] => $this->getPackingSlipType(),
            $keys[101] => $this->getWorkInProgress(),
            $keys[102] => $this->getPackageAmount(),
            $keys[103] => $this->getCouponCode(),
            $keys[104] => $this->getCouponDiscountAmount(),
            $keys[105] => $this->getCouponDiscountPercentage(),
            $keys[106] => $this->getHideUntill(),
        );
        if ($result[$keys[5]] instanceof \DateTimeInterface) {
            $result[$keys[5]] = $result[$keys[5]]->format('c');
        }

        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[16]] instanceof \DateTimeInterface) {
            $result[$keys[16]] = $result[$keys[16]]->format('c');
        }

        if ($result[$keys[17]] instanceof \DateTimeInterface) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        if ($result[$keys[22]] instanceof \DateTimeInterface) {
            $result[$keys[22]] = $result[$keys[22]]->format('c');
        }

        if ($result[$keys[98]] instanceof \DateTimeInterface) {
            $result[$keys[98]] = $result[$keys[98]]->format('c');
        }

        if ($result[$keys[106]] instanceof \DateTimeInterface) {
            $result[$keys[106]] = $result[$keys[106]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSite) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'site';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site';
                        break;
                    default:
                        $key = 'Site';
                }

                $result[$key] = $this->aSite->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSaleOrderStatus) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'saleOrderStatus';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_sale_order_status';
                        break;
                    default:
                        $key = 'SaleOrderStatus';
                }

                $result[$key] = $this->aSaleOrderStatus->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aShippingMethod) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'shippingMethod';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_shipping_method';
                        break;
                    default:
                        $key = 'ShippingMethod';
                }

                $result[$key] = $this->aShippingMethod->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user';
                        break;
                    default:
                        $key = 'User';
                }

                $result[$key] = $this->aUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSaleOrderPayments) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'saleOrderPayments';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_order_payments';
                        break;
                    default:
                        $key = 'SaleOrderPayments';
                }

                $result[$key] = $this->collSaleOrderPayments->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPayments) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'payments';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'payments';
                        break;
                    default:
                        $key = 'Payments';
                }

                $result[$key] = $this->collPayments->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaleOrderEmails) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'saleOrderEmails';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_order_emails';
                        break;
                    default:
                        $key = 'SaleOrderEmails';
                }

                $result[$key] = $this->collSaleOrderEmails->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaleOrderItems) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'saleOrderItems';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_order_items';
                        break;
                    default:
                        $key = 'SaleOrderItems';
                }

                $result[$key] = $this->collSaleOrderItems->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaleOrderNotifications) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'saleOrderNotifications';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_order_notifications';
                        break;
                    default:
                        $key = 'SaleOrderNotifications';
                }

                $result[$key] = $this->collSaleOrderNotifications->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Sale\SaleOrder
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SaleOrderTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Sale\SaleOrder
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setSiteId($value);
                break;
            case 2:
                $this->setLanguageId($value);
                break;
            case 3:
                $this->setIsClosed($value);
                break;
            case 4:
                $this->setIsFullyPaid($value);
                break;
            case 5:
                $this->setPayDate($value);
                break;
            case 6:
                $this->setIsReadyForShipping($value);
                break;
            case 7:
                $this->setIsShippingChecked($value);
                break;
            case 8:
                $this->setIsFullyShipped($value);
                break;
            case 9:
                $this->setIsPushedToAccounting($value);
                break;
            case 10:
                $this->setShipDate($value);
                break;
            case 11:
                $this->setSaleOrderStatusId($value);
                break;
            case 12:
                $this->setHandledBy($value);
                break;
            case 13:
                $this->setSource($value);
                break;
            case 14:
                $this->setIsCompleted($value);
                break;
            case 15:
                $this->setCreatedByUserId($value);
                break;
            case 16:
                $this->setCreatedOn($value);
                break;
            case 17:
                $this->setEstimatedDeliveryDate($value);
                break;
            case 18:
                $this->setCustomerId($value);
                break;
            case 19:
                $this->setEnvelopeWeightGrams($value);
                break;
            case 20:
                $this->setWebshopUserSessionId($value);
                break;
            case 21:
                $this->setInvoiceNumber($value);
                break;
            case 22:
                $this->setInvoiceDate($value);
                break;
            case 23:
                $this->setPaytermDays($value);
                break;
            case 24:
                $this->setPaytermOriginalId($value);
                break;
            case 25:
                $this->setPaytermString($value);
                break;
            case 26:
                $this->setOrderNumber($value);
                break;
            case 27:
                $this->setItemDeleted($value);
                break;
            case 28:
                $this->setCompanyName($value);
                break;
            case 29:
                $this->setDebitor($value);
                break;
            case 30:
                $this->setOurCustomFolder($value);
                break;
            case 31:
                $this->setOurSlogan($value);
                break;
            case 32:
                $this->setOurPhone($value);
                break;
            case 33:
                $this->setOurFax($value);
                break;
            case 34:
                $this->setOurWebsite($value);
                break;
            case 35:
                $this->setOurEmail($value);
                break;
            case 36:
                $this->setOurVatNumber($value);
                break;
            case 37:
                $this->setOurChamberOfCommerce($value);
                break;
            case 38:
                $this->setOurIbanNumber($value);
                break;
            case 39:
                $this->setOurBicNumber($value);
                break;
            case 40:
                $this->setOurGeneralCompanyName($value);
                break;
            case 41:
                $this->setOurGeneralStreet($value);
                break;
            case 42:
                $this->setOurGeneralNumber($value);
                break;
            case 43:
                $this->setOurGeneralNumberAdd($value);
                break;
            case 44:
                $this->setOurGeneralPostal($value);
                break;
            case 45:
                $this->setOurGeneralPoBox($value);
                break;
            case 46:
                $this->setOurGeneralCity($value);
                break;
            case 47:
                $this->setOurGeneralCountry($value);
                break;
            case 48:
                $this->setCustomerPhone($value);
                break;
            case 49:
                $this->setCustomerFax($value);
                break;
            case 50:
                $this->setCustomerEmail($value);
                break;
            case 51:
                $this->setCustomerChamberOfCommerce($value);
                break;
            case 52:
                $this->setCustomerVatNumber($value);
                break;
            case 53:
                $this->setCustomerOrderReference($value);
                break;
            case 54:
                $this->setCustomerOrderPlacedBy($value);
                break;
            case 55:
                $this->setCustomerInvoiceAddressId($value);
                break;
            case 56:
                $this->setCustomerInvoiceCompanyName($value);
                break;
            case 57:
                $this->setCustomerInvoiceAttnName($value);
                break;
            case 58:
                $this->setCustomerInvoiceStreet($value);
                break;
            case 59:
                $this->setCustomerInvoiceNumber($value);
                break;
            case 60:
                $this->setCustomerInvoiceNumberAdd($value);
                break;
            case 61:
                $this->setCustomerInvoicePostal($value);
                break;
            case 62:
                $this->setCustomerInvoiceCity($value);
                break;
            case 63:
                $this->setCustomerInvoiceCountry($value);
                break;
            case 64:
                $this->setCustomerInvoiceUsaState($value);
                break;
            case 65:
                $this->setCustomerInvoiceAddressL1($value);
                break;
            case 66:
                $this->setCustomerInvoiceAddressL2($value);
                break;
            case 67:
                $this->setCustomerDeliveryAddressId($value);
                break;
            case 68:
                $this->setCustomerDeliveryCompanyName($value);
                break;
            case 69:
                $this->setCustomerDeliveryAttnName($value);
                break;
            case 70:
                $this->setCustomerDeliveryStreet($value);
                break;
            case 71:
                $this->setCustomerDeliveryNumber($value);
                break;
            case 72:
                $this->setCustomerDeliveryNumberAdd($value);
                break;
            case 73:
                $this->setCustomerDeliveryPostal($value);
                break;
            case 74:
                $this->setCustomerDeliveryCity($value);
                break;
            case 75:
                $this->setCustomerDeliveryCountry($value);
                break;
            case 76:
                $this->setCustomerDeliveryUsaState($value);
                break;
            case 77:
                $this->setCustomerDeliveryAddressL1($value);
                break;
            case 78:
                $this->setCustomerDeliveryAddressL2($value);
                break;
            case 79:
                $this->setCpFirstName($value);
                break;
            case 80:
                $this->setCpLastName($value);
                break;
            case 81:
                $this->setCpSallutation($value);
                break;
            case 82:
                $this->setReference($value);
                break;
            case 83:
                $this->setShippingNote($value);
                break;
            case 84:
                $this->setCustomerOrderNote($value);
                break;
            case 85:
                $this->setShippingOrderNote($value);
                break;
            case 86:
                $this->setHasWrap($value);
                break;
            case 87:
                $this->setWrappingPrice($value);
                break;
            case 88:
                $this->setShippingCarrier($value);
                break;
            case 89:
                $this->setShippingPrice($value);
                break;
            case 90:
                $this->setShippingMethodId($value);
                break;
            case 91:
                $this->setShippingHasTrackTrace($value);
                break;
            case 92:
                $this->setShippingId($value);
                break;
            case 93:
                $this->setTrackingCode($value);
                break;
            case 94:
                $this->setTrackingUrl($value);
                break;
            case 95:
                $this->setPaymethodName($value);
                break;
            case 96:
                $this->setPaymethodCode($value);
                break;
            case 97:
                $this->setPaymethodId($value);
                break;
            case 98:
                $this->setShippingRegistrationDate($value);
                break;
            case 99:
                $this->setInvoiceByPostal($value);
                break;
            case 100:
                $this->setPackingSlipType($value);
                break;
            case 101:
                $this->setWorkInProgress($value);
                break;
            case 102:
                $this->setPackageAmount($value);
                break;
            case 103:
                $this->setCouponCode($value);
                break;
            case 104:
                $this->setCouponDiscountAmount($value);
                break;
            case 105:
                $this->setCouponDiscountPercentage($value);
                break;
            case 106:
                $this->setHideUntill($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SaleOrderTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setSiteId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setLanguageId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setIsClosed($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setIsFullyPaid($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setPayDate($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIsReadyForShipping($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setIsShippingChecked($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setIsFullyShipped($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setIsPushedToAccounting($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setShipDate($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setSaleOrderStatusId($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setHandledBy($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setSource($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setIsCompleted($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setCreatedByUserId($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setCreatedOn($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setEstimatedDeliveryDate($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setCustomerId($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setEnvelopeWeightGrams($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setWebshopUserSessionId($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setInvoiceNumber($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setInvoiceDate($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setPaytermDays($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setPaytermOriginalId($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setPaytermString($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setOrderNumber($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setItemDeleted($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setCompanyName($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setDebitor($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setOurCustomFolder($arr[$keys[30]]);
        }
        if (array_key_exists($keys[31], $arr)) {
            $this->setOurSlogan($arr[$keys[31]]);
        }
        if (array_key_exists($keys[32], $arr)) {
            $this->setOurPhone($arr[$keys[32]]);
        }
        if (array_key_exists($keys[33], $arr)) {
            $this->setOurFax($arr[$keys[33]]);
        }
        if (array_key_exists($keys[34], $arr)) {
            $this->setOurWebsite($arr[$keys[34]]);
        }
        if (array_key_exists($keys[35], $arr)) {
            $this->setOurEmail($arr[$keys[35]]);
        }
        if (array_key_exists($keys[36], $arr)) {
            $this->setOurVatNumber($arr[$keys[36]]);
        }
        if (array_key_exists($keys[37], $arr)) {
            $this->setOurChamberOfCommerce($arr[$keys[37]]);
        }
        if (array_key_exists($keys[38], $arr)) {
            $this->setOurIbanNumber($arr[$keys[38]]);
        }
        if (array_key_exists($keys[39], $arr)) {
            $this->setOurBicNumber($arr[$keys[39]]);
        }
        if (array_key_exists($keys[40], $arr)) {
            $this->setOurGeneralCompanyName($arr[$keys[40]]);
        }
        if (array_key_exists($keys[41], $arr)) {
            $this->setOurGeneralStreet($arr[$keys[41]]);
        }
        if (array_key_exists($keys[42], $arr)) {
            $this->setOurGeneralNumber($arr[$keys[42]]);
        }
        if (array_key_exists($keys[43], $arr)) {
            $this->setOurGeneralNumberAdd($arr[$keys[43]]);
        }
        if (array_key_exists($keys[44], $arr)) {
            $this->setOurGeneralPostal($arr[$keys[44]]);
        }
        if (array_key_exists($keys[45], $arr)) {
            $this->setOurGeneralPoBox($arr[$keys[45]]);
        }
        if (array_key_exists($keys[46], $arr)) {
            $this->setOurGeneralCity($arr[$keys[46]]);
        }
        if (array_key_exists($keys[47], $arr)) {
            $this->setOurGeneralCountry($arr[$keys[47]]);
        }
        if (array_key_exists($keys[48], $arr)) {
            $this->setCustomerPhone($arr[$keys[48]]);
        }
        if (array_key_exists($keys[49], $arr)) {
            $this->setCustomerFax($arr[$keys[49]]);
        }
        if (array_key_exists($keys[50], $arr)) {
            $this->setCustomerEmail($arr[$keys[50]]);
        }
        if (array_key_exists($keys[51], $arr)) {
            $this->setCustomerChamberOfCommerce($arr[$keys[51]]);
        }
        if (array_key_exists($keys[52], $arr)) {
            $this->setCustomerVatNumber($arr[$keys[52]]);
        }
        if (array_key_exists($keys[53], $arr)) {
            $this->setCustomerOrderReference($arr[$keys[53]]);
        }
        if (array_key_exists($keys[54], $arr)) {
            $this->setCustomerOrderPlacedBy($arr[$keys[54]]);
        }
        if (array_key_exists($keys[55], $arr)) {
            $this->setCustomerInvoiceAddressId($arr[$keys[55]]);
        }
        if (array_key_exists($keys[56], $arr)) {
            $this->setCustomerInvoiceCompanyName($arr[$keys[56]]);
        }
        if (array_key_exists($keys[57], $arr)) {
            $this->setCustomerInvoiceAttnName($arr[$keys[57]]);
        }
        if (array_key_exists($keys[58], $arr)) {
            $this->setCustomerInvoiceStreet($arr[$keys[58]]);
        }
        if (array_key_exists($keys[59], $arr)) {
            $this->setCustomerInvoiceNumber($arr[$keys[59]]);
        }
        if (array_key_exists($keys[60], $arr)) {
            $this->setCustomerInvoiceNumberAdd($arr[$keys[60]]);
        }
        if (array_key_exists($keys[61], $arr)) {
            $this->setCustomerInvoicePostal($arr[$keys[61]]);
        }
        if (array_key_exists($keys[62], $arr)) {
            $this->setCustomerInvoiceCity($arr[$keys[62]]);
        }
        if (array_key_exists($keys[63], $arr)) {
            $this->setCustomerInvoiceCountry($arr[$keys[63]]);
        }
        if (array_key_exists($keys[64], $arr)) {
            $this->setCustomerInvoiceUsaState($arr[$keys[64]]);
        }
        if (array_key_exists($keys[65], $arr)) {
            $this->setCustomerInvoiceAddressL1($arr[$keys[65]]);
        }
        if (array_key_exists($keys[66], $arr)) {
            $this->setCustomerInvoiceAddressL2($arr[$keys[66]]);
        }
        if (array_key_exists($keys[67], $arr)) {
            $this->setCustomerDeliveryAddressId($arr[$keys[67]]);
        }
        if (array_key_exists($keys[68], $arr)) {
            $this->setCustomerDeliveryCompanyName($arr[$keys[68]]);
        }
        if (array_key_exists($keys[69], $arr)) {
            $this->setCustomerDeliveryAttnName($arr[$keys[69]]);
        }
        if (array_key_exists($keys[70], $arr)) {
            $this->setCustomerDeliveryStreet($arr[$keys[70]]);
        }
        if (array_key_exists($keys[71], $arr)) {
            $this->setCustomerDeliveryNumber($arr[$keys[71]]);
        }
        if (array_key_exists($keys[72], $arr)) {
            $this->setCustomerDeliveryNumberAdd($arr[$keys[72]]);
        }
        if (array_key_exists($keys[73], $arr)) {
            $this->setCustomerDeliveryPostal($arr[$keys[73]]);
        }
        if (array_key_exists($keys[74], $arr)) {
            $this->setCustomerDeliveryCity($arr[$keys[74]]);
        }
        if (array_key_exists($keys[75], $arr)) {
            $this->setCustomerDeliveryCountry($arr[$keys[75]]);
        }
        if (array_key_exists($keys[76], $arr)) {
            $this->setCustomerDeliveryUsaState($arr[$keys[76]]);
        }
        if (array_key_exists($keys[77], $arr)) {
            $this->setCustomerDeliveryAddressL1($arr[$keys[77]]);
        }
        if (array_key_exists($keys[78], $arr)) {
            $this->setCustomerDeliveryAddressL2($arr[$keys[78]]);
        }
        if (array_key_exists($keys[79], $arr)) {
            $this->setCpFirstName($arr[$keys[79]]);
        }
        if (array_key_exists($keys[80], $arr)) {
            $this->setCpLastName($arr[$keys[80]]);
        }
        if (array_key_exists($keys[81], $arr)) {
            $this->setCpSallutation($arr[$keys[81]]);
        }
        if (array_key_exists($keys[82], $arr)) {
            $this->setReference($arr[$keys[82]]);
        }
        if (array_key_exists($keys[83], $arr)) {
            $this->setShippingNote($arr[$keys[83]]);
        }
        if (array_key_exists($keys[84], $arr)) {
            $this->setCustomerOrderNote($arr[$keys[84]]);
        }
        if (array_key_exists($keys[85], $arr)) {
            $this->setShippingOrderNote($arr[$keys[85]]);
        }
        if (array_key_exists($keys[86], $arr)) {
            $this->setHasWrap($arr[$keys[86]]);
        }
        if (array_key_exists($keys[87], $arr)) {
            $this->setWrappingPrice($arr[$keys[87]]);
        }
        if (array_key_exists($keys[88], $arr)) {
            $this->setShippingCarrier($arr[$keys[88]]);
        }
        if (array_key_exists($keys[89], $arr)) {
            $this->setShippingPrice($arr[$keys[89]]);
        }
        if (array_key_exists($keys[90], $arr)) {
            $this->setShippingMethodId($arr[$keys[90]]);
        }
        if (array_key_exists($keys[91], $arr)) {
            $this->setShippingHasTrackTrace($arr[$keys[91]]);
        }
        if (array_key_exists($keys[92], $arr)) {
            $this->setShippingId($arr[$keys[92]]);
        }
        if (array_key_exists($keys[93], $arr)) {
            $this->setTrackingCode($arr[$keys[93]]);
        }
        if (array_key_exists($keys[94], $arr)) {
            $this->setTrackingUrl($arr[$keys[94]]);
        }
        if (array_key_exists($keys[95], $arr)) {
            $this->setPaymethodName($arr[$keys[95]]);
        }
        if (array_key_exists($keys[96], $arr)) {
            $this->setPaymethodCode($arr[$keys[96]]);
        }
        if (array_key_exists($keys[97], $arr)) {
            $this->setPaymethodId($arr[$keys[97]]);
        }
        if (array_key_exists($keys[98], $arr)) {
            $this->setShippingRegistrationDate($arr[$keys[98]]);
        }
        if (array_key_exists($keys[99], $arr)) {
            $this->setInvoiceByPostal($arr[$keys[99]]);
        }
        if (array_key_exists($keys[100], $arr)) {
            $this->setPackingSlipType($arr[$keys[100]]);
        }
        if (array_key_exists($keys[101], $arr)) {
            $this->setWorkInProgress($arr[$keys[101]]);
        }
        if (array_key_exists($keys[102], $arr)) {
            $this->setPackageAmount($arr[$keys[102]]);
        }
        if (array_key_exists($keys[103], $arr)) {
            $this->setCouponCode($arr[$keys[103]]);
        }
        if (array_key_exists($keys[104], $arr)) {
            $this->setCouponDiscountAmount($arr[$keys[104]]);
        }
        if (array_key_exists($keys[105], $arr)) {
            $this->setCouponDiscountPercentage($arr[$keys[105]]);
        }
        if (array_key_exists($keys[106], $arr)) {
            $this->setHideUntill($arr[$keys[106]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Sale\SaleOrder The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SaleOrderTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SaleOrderTableMap::COL_ID)) {
            $criteria->add(SaleOrderTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SITE_ID)) {
            $criteria->add(SaleOrderTableMap::COL_SITE_ID, $this->site_id);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_LANGUAGE_ID)) {
            $criteria->add(SaleOrderTableMap::COL_LANGUAGE_ID, $this->language_id);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_CLOSED)) {
            $criteria->add(SaleOrderTableMap::COL_IS_CLOSED, $this->is_closed);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_FULLY_PAID)) {
            $criteria->add(SaleOrderTableMap::COL_IS_FULLY_PAID, $this->is_fully_paid);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAY_DATE)) {
            $criteria->add(SaleOrderTableMap::COL_PAY_DATE, $this->pay_date);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_READY_FOR_SHIPPING)) {
            $criteria->add(SaleOrderTableMap::COL_IS_READY_FOR_SHIPPING, $this->is_ready_for_shipping);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_SHIPPING_CHECKED)) {
            $criteria->add(SaleOrderTableMap::COL_IS_SHIPPING_CHECKED, $this->is_shipping_checked);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_FULLY_SHIPPED)) {
            $criteria->add(SaleOrderTableMap::COL_IS_FULLY_SHIPPED, $this->is_fully_shipped);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_PUSHED_TO_ACCOUNTING)) {
            $criteria->add(SaleOrderTableMap::COL_IS_PUSHED_TO_ACCOUNTING, $this->is_pushed_to_accounting);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIP_DATE)) {
            $criteria->add(SaleOrderTableMap::COL_SHIP_DATE, $this->ship_date);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SALE_ORDER_STATUS_ID)) {
            $criteria->add(SaleOrderTableMap::COL_SALE_ORDER_STATUS_ID, $this->sale_order_status_id);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_HANDLED_BY)) {
            $criteria->add(SaleOrderTableMap::COL_HANDLED_BY, $this->handled_by);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SOURCE)) {
            $criteria->add(SaleOrderTableMap::COL_SOURCE, $this->source);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_COMPLETED)) {
            $criteria->add(SaleOrderTableMap::COL_IS_COMPLETED, $this->is_completed);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CREATED_BY_USER_ID)) {
            $criteria->add(SaleOrderTableMap::COL_CREATED_BY_USER_ID, $this->created_by_user_id);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CREATED_ON)) {
            $criteria->add(SaleOrderTableMap::COL_CREATED_ON, $this->created_on);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_ESTIMATED_DELIVERY_DATE)) {
            $criteria->add(SaleOrderTableMap::COL_ESTIMATED_DELIVERY_DATE, $this->estimated_delivery_date);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_ID)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_ID, $this->customer_id);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_ENVELOPE_WEIGHT_GRAMS)) {
            $criteria->add(SaleOrderTableMap::COL_ENVELOPE_WEIGHT_GRAMS, $this->envelope_weight_grams);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_WEBSHOP_USER_SESSION_ID)) {
            $criteria->add(SaleOrderTableMap::COL_WEBSHOP_USER_SESSION_ID, $this->webshop_user_session_id);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_INVOICE_NUMBER)) {
            $criteria->add(SaleOrderTableMap::COL_INVOICE_NUMBER, $this->invoice_number);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_INVOICE_DATE)) {
            $criteria->add(SaleOrderTableMap::COL_INVOICE_DATE, $this->invoice_date);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAYTERM_DAYS)) {
            $criteria->add(SaleOrderTableMap::COL_PAYTERM_DAYS, $this->payterm_days);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAYTERM_ORIGINAL_ID)) {
            $criteria->add(SaleOrderTableMap::COL_PAYTERM_ORIGINAL_ID, $this->payterm_original_id);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAYTERM_STRING)) {
            $criteria->add(SaleOrderTableMap::COL_PAYTERM_STRING, $this->payterm_string);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_ORDER_NUMBER)) {
            $criteria->add(SaleOrderTableMap::COL_ORDER_NUMBER, $this->order_number);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_IS_DELETED)) {
            $criteria->add(SaleOrderTableMap::COL_IS_DELETED, $this->is_deleted);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_COMPANY_NAME)) {
            $criteria->add(SaleOrderTableMap::COL_COMPANY_NAME, $this->company_name);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_DEBITOR)) {
            $criteria->add(SaleOrderTableMap::COL_DEBITOR, $this->debitor);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_CUSTOM_FOLDER)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_CUSTOM_FOLDER, $this->our_custom_folder);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_SLOGAN)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_SLOGAN, $this->our_slogan);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_PHONE)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_PHONE, $this->our_phone);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_FAX)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_FAX, $this->our_fax);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_WEBSITE)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_WEBSITE, $this->our_website);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_EMAIL)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_EMAIL, $this->our_email);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_VAT_NUMBER)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_VAT_NUMBER, $this->our_vat_number);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_CHAMBER_OF_COMMERCE)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_CHAMBER_OF_COMMERCE, $this->our_chamber_of_commerce);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_IBAN_NUMBER)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_IBAN_NUMBER, $this->our_iban_number);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_BIC_NUMBER)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_BIC_NUMBER, $this->our_bic_number);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_COMPANY_NAME)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_GENERAL_COMPANY_NAME, $this->our_general_company_name);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_STREET)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_GENERAL_STREET, $this->our_general_street);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_NUMBER)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_GENERAL_NUMBER, $this->our_general_number);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_NUMBER_ADD)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_GENERAL_NUMBER_ADD, $this->our_general_number_add);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_POSTAL)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_GENERAL_POSTAL, $this->our_general_postal);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_PO_BOX)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_GENERAL_PO_BOX, $this->our_general_po_box);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_CITY)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_GENERAL_CITY, $this->our_general_city);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_OUR_GENERAL_COUNTRY)) {
            $criteria->add(SaleOrderTableMap::COL_OUR_GENERAL_COUNTRY, $this->our_general_country);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_PHONE)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_PHONE, $this->customer_phone);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_FAX)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_FAX, $this->customer_fax);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_EMAIL)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_EMAIL, $this->customer_email);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_CHAMBER_OF_COMMERCE)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_CHAMBER_OF_COMMERCE, $this->customer_chamber_of_commerce);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_VAT_NUMBER)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_VAT_NUMBER, $this->customer_vat_number);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_ORDER_REFERENCE)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_ORDER_REFERENCE, $this->customer_order_reference);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_ORDER_PLACED_BY)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_ORDER_PLACED_BY, $this->customer_order_placed_by);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_ID)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_ID, $this->customer_invoice_address_id);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_COMPANY_NAME)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_INVOICE_COMPANY_NAME, $this->customer_invoice_company_name);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ATTN_NAME)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ATTN_NAME, $this->customer_invoice_attn_name);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_STREET)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_INVOICE_STREET, $this->customer_invoice_street);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER, $this->customer_invoice_number);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER_ADD)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER_ADD, $this->customer_invoice_number_add);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_POSTAL)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_INVOICE_POSTAL, $this->customer_invoice_postal);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_CITY)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_INVOICE_CITY, $this->customer_invoice_city);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_COUNTRY)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_INVOICE_COUNTRY, $this->customer_invoice_country);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_USA_STATE)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_INVOICE_USA_STATE, $this->customer_invoice_usa_state);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L1)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L1, $this->customer_invoice_address_l1);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L2)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L2, $this->customer_invoice_address_l2);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_ID)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_ID, $this->customer_delivery_address_id);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COMPANY_NAME)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COMPANY_NAME, $this->customer_delivery_company_name);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ATTN_NAME)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ATTN_NAME, $this->customer_delivery_attn_name);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_STREET)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_STREET, $this->customer_delivery_street);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER, $this->customer_delivery_number);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER_ADD)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER_ADD, $this->customer_delivery_number_add);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_POSTAL)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_POSTAL, $this->customer_delivery_postal);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_CITY)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_CITY, $this->customer_delivery_city);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COUNTRY)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COUNTRY, $this->customer_delivery_country);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_USA_STATE)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_USA_STATE, $this->customer_delivery_usa_state);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L1)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L1, $this->customer_delivery_address_l1);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L2)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L2, $this->customer_delivery_address_l2);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CP_FIRST_NAME)) {
            $criteria->add(SaleOrderTableMap::COL_CP_FIRST_NAME, $this->cp_first_name);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CP_LAST_NAME)) {
            $criteria->add(SaleOrderTableMap::COL_CP_LAST_NAME, $this->cp_last_name);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CP_SALLUTATION)) {
            $criteria->add(SaleOrderTableMap::COL_CP_SALLUTATION, $this->cp_sallutation);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_REFERENCE)) {
            $criteria->add(SaleOrderTableMap::COL_REFERENCE, $this->reference);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_NOTE)) {
            $criteria->add(SaleOrderTableMap::COL_SHIPPING_NOTE, $this->shipping_note);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_CUSTOMER_ORDER_NOTE)) {
            $criteria->add(SaleOrderTableMap::COL_CUSTOMER_ORDER_NOTE, $this->customer_order_note);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_ORDER_NOTE)) {
            $criteria->add(SaleOrderTableMap::COL_SHIPPING_ORDER_NOTE, $this->shipping_order_note);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_HAS_WRAP)) {
            $criteria->add(SaleOrderTableMap::COL_HAS_WRAP, $this->has_wrap);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_WRAPPING_PRICE)) {
            $criteria->add(SaleOrderTableMap::COL_WRAPPING_PRICE, $this->wrapping_price);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_CARRIER)) {
            $criteria->add(SaleOrderTableMap::COL_SHIPPING_CARRIER, $this->shipping_carrier);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_PRICE)) {
            $criteria->add(SaleOrderTableMap::COL_SHIPPING_PRICE, $this->shipping_price);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_METHOD_ID)) {
            $criteria->add(SaleOrderTableMap::COL_SHIPPING_METHOD_ID, $this->shipping_method_id);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_HAS_TRACK_TRACE)) {
            $criteria->add(SaleOrderTableMap::COL_SHIPPING_HAS_TRACK_TRACE, $this->shipping_has_track_trace);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_ID)) {
            $criteria->add(SaleOrderTableMap::COL_SHIPPING_ID, $this->shipping_id);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_TRACKING_CODE)) {
            $criteria->add(SaleOrderTableMap::COL_TRACKING_CODE, $this->tracking_code);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_TRACKING_URL)) {
            $criteria->add(SaleOrderTableMap::COL_TRACKING_URL, $this->tracking_url);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAYMETHOD_NAME)) {
            $criteria->add(SaleOrderTableMap::COL_PAYMETHOD_NAME, $this->paymethod_name);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAYMETHOD_CODE)) {
            $criteria->add(SaleOrderTableMap::COL_PAYMETHOD_CODE, $this->paymethod_code);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PAYMETHOD_ID)) {
            $criteria->add(SaleOrderTableMap::COL_PAYMETHOD_ID, $this->paymethod_id);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_SHIPPING_REGISTRATION_DATE)) {
            $criteria->add(SaleOrderTableMap::COL_SHIPPING_REGISTRATION_DATE, $this->shipping_registration_date);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_INVOICE_BY_POSTAL)) {
            $criteria->add(SaleOrderTableMap::COL_INVOICE_BY_POSTAL, $this->invoice_by_postal);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PACKING_SLIP_TYPE)) {
            $criteria->add(SaleOrderTableMap::COL_PACKING_SLIP_TYPE, $this->packing_slip_type);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_WORK_IN_PROGRESS)) {
            $criteria->add(SaleOrderTableMap::COL_WORK_IN_PROGRESS, $this->work_in_progress);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_PACKAGE_AMOUNT)) {
            $criteria->add(SaleOrderTableMap::COL_PACKAGE_AMOUNT, $this->package_amount);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_COUPON_CODE)) {
            $criteria->add(SaleOrderTableMap::COL_COUPON_CODE, $this->coupon_code);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_COUPON_DISCOUNT_AMOUNT)) {
            $criteria->add(SaleOrderTableMap::COL_COUPON_DISCOUNT_AMOUNT, $this->coupon_discount_amount);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_COUPON_DISCOUNT_PERCENTAGE)) {
            $criteria->add(SaleOrderTableMap::COL_COUPON_DISCOUNT_PERCENTAGE, $this->coupon_discount_percentage);
        }
        if ($this->isColumnModified(SaleOrderTableMap::COL_HIDE_UNTILL)) {
            $criteria->add(SaleOrderTableMap::COL_HIDE_UNTILL, $this->hide_untill);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSaleOrderQuery::create();
        $criteria->add(SaleOrderTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Sale\SaleOrder (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSiteId($this->getSiteId());
        $copyObj->setLanguageId($this->getLanguageId());
        $copyObj->setIsClosed($this->getIsClosed());
        $copyObj->setIsFullyPaid($this->getIsFullyPaid());
        $copyObj->setPayDate($this->getPayDate());
        $copyObj->setIsReadyForShipping($this->getIsReadyForShipping());
        $copyObj->setIsShippingChecked($this->getIsShippingChecked());
        $copyObj->setIsFullyShipped($this->getIsFullyShipped());
        $copyObj->setIsPushedToAccounting($this->getIsPushedToAccounting());
        $copyObj->setShipDate($this->getShipDate());
        $copyObj->setSaleOrderStatusId($this->getSaleOrderStatusId());
        $copyObj->setHandledBy($this->getHandledBy());
        $copyObj->setSource($this->getSource());
        $copyObj->setIsCompleted($this->getIsCompleted());
        $copyObj->setCreatedByUserId($this->getCreatedByUserId());
        $copyObj->setCreatedOn($this->getCreatedOn());
        $copyObj->setEstimatedDeliveryDate($this->getEstimatedDeliveryDate());
        $copyObj->setCustomerId($this->getCustomerId());
        $copyObj->setEnvelopeWeightGrams($this->getEnvelopeWeightGrams());
        $copyObj->setWebshopUserSessionId($this->getWebshopUserSessionId());
        $copyObj->setInvoiceNumber($this->getInvoiceNumber());
        $copyObj->setInvoiceDate($this->getInvoiceDate());
        $copyObj->setPaytermDays($this->getPaytermDays());
        $copyObj->setPaytermOriginalId($this->getPaytermOriginalId());
        $copyObj->setPaytermString($this->getPaytermString());
        $copyObj->setOrderNumber($this->getOrderNumber());
        $copyObj->setItemDeleted($this->getItemDeleted());
        $copyObj->setCompanyName($this->getCompanyName());
        $copyObj->setDebitor($this->getDebitor());
        $copyObj->setOurCustomFolder($this->getOurCustomFolder());
        $copyObj->setOurSlogan($this->getOurSlogan());
        $copyObj->setOurPhone($this->getOurPhone());
        $copyObj->setOurFax($this->getOurFax());
        $copyObj->setOurWebsite($this->getOurWebsite());
        $copyObj->setOurEmail($this->getOurEmail());
        $copyObj->setOurVatNumber($this->getOurVatNumber());
        $copyObj->setOurChamberOfCommerce($this->getOurChamberOfCommerce());
        $copyObj->setOurIbanNumber($this->getOurIbanNumber());
        $copyObj->setOurBicNumber($this->getOurBicNumber());
        $copyObj->setOurGeneralCompanyName($this->getOurGeneralCompanyName());
        $copyObj->setOurGeneralStreet($this->getOurGeneralStreet());
        $copyObj->setOurGeneralNumber($this->getOurGeneralNumber());
        $copyObj->setOurGeneralNumberAdd($this->getOurGeneralNumberAdd());
        $copyObj->setOurGeneralPostal($this->getOurGeneralPostal());
        $copyObj->setOurGeneralPoBox($this->getOurGeneralPoBox());
        $copyObj->setOurGeneralCity($this->getOurGeneralCity());
        $copyObj->setOurGeneralCountry($this->getOurGeneralCountry());
        $copyObj->setCustomerPhone($this->getCustomerPhone());
        $copyObj->setCustomerFax($this->getCustomerFax());
        $copyObj->setCustomerEmail($this->getCustomerEmail());
        $copyObj->setCustomerChamberOfCommerce($this->getCustomerChamberOfCommerce());
        $copyObj->setCustomerVatNumber($this->getCustomerVatNumber());
        $copyObj->setCustomerOrderReference($this->getCustomerOrderReference());
        $copyObj->setCustomerOrderPlacedBy($this->getCustomerOrderPlacedBy());
        $copyObj->setCustomerInvoiceAddressId($this->getCustomerInvoiceAddressId());
        $copyObj->setCustomerInvoiceCompanyName($this->getCustomerInvoiceCompanyName());
        $copyObj->setCustomerInvoiceAttnName($this->getCustomerInvoiceAttnName());
        $copyObj->setCustomerInvoiceStreet($this->getCustomerInvoiceStreet());
        $copyObj->setCustomerInvoiceNumber($this->getCustomerInvoiceNumber());
        $copyObj->setCustomerInvoiceNumberAdd($this->getCustomerInvoiceNumberAdd());
        $copyObj->setCustomerInvoicePostal($this->getCustomerInvoicePostal());
        $copyObj->setCustomerInvoiceCity($this->getCustomerInvoiceCity());
        $copyObj->setCustomerInvoiceCountry($this->getCustomerInvoiceCountry());
        $copyObj->setCustomerInvoiceUsaState($this->getCustomerInvoiceUsaState());
        $copyObj->setCustomerInvoiceAddressL1($this->getCustomerInvoiceAddressL1());
        $copyObj->setCustomerInvoiceAddressL2($this->getCustomerInvoiceAddressL2());
        $copyObj->setCustomerDeliveryAddressId($this->getCustomerDeliveryAddressId());
        $copyObj->setCustomerDeliveryCompanyName($this->getCustomerDeliveryCompanyName());
        $copyObj->setCustomerDeliveryAttnName($this->getCustomerDeliveryAttnName());
        $copyObj->setCustomerDeliveryStreet($this->getCustomerDeliveryStreet());
        $copyObj->setCustomerDeliveryNumber($this->getCustomerDeliveryNumber());
        $copyObj->setCustomerDeliveryNumberAdd($this->getCustomerDeliveryNumberAdd());
        $copyObj->setCustomerDeliveryPostal($this->getCustomerDeliveryPostal());
        $copyObj->setCustomerDeliveryCity($this->getCustomerDeliveryCity());
        $copyObj->setCustomerDeliveryCountry($this->getCustomerDeliveryCountry());
        $copyObj->setCustomerDeliveryUsaState($this->getCustomerDeliveryUsaState());
        $copyObj->setCustomerDeliveryAddressL1($this->getCustomerDeliveryAddressL1());
        $copyObj->setCustomerDeliveryAddressL2($this->getCustomerDeliveryAddressL2());
        $copyObj->setCpFirstName($this->getCpFirstName());
        $copyObj->setCpLastName($this->getCpLastName());
        $copyObj->setCpSallutation($this->getCpSallutation());
        $copyObj->setReference($this->getReference());
        $copyObj->setShippingNote($this->getShippingNote());
        $copyObj->setCustomerOrderNote($this->getCustomerOrderNote());
        $copyObj->setShippingOrderNote($this->getShippingOrderNote());
        $copyObj->setHasWrap($this->getHasWrap());
        $copyObj->setWrappingPrice($this->getWrappingPrice());
        $copyObj->setShippingCarrier($this->getShippingCarrier());
        $copyObj->setShippingPrice($this->getShippingPrice());
        $copyObj->setShippingMethodId($this->getShippingMethodId());
        $copyObj->setShippingHasTrackTrace($this->getShippingHasTrackTrace());
        $copyObj->setShippingId($this->getShippingId());
        $copyObj->setTrackingCode($this->getTrackingCode());
        $copyObj->setTrackingUrl($this->getTrackingUrl());
        $copyObj->setPaymethodName($this->getPaymethodName());
        $copyObj->setPaymethodCode($this->getPaymethodCode());
        $copyObj->setPaymethodId($this->getPaymethodId());
        $copyObj->setShippingRegistrationDate($this->getShippingRegistrationDate());
        $copyObj->setInvoiceByPostal($this->getInvoiceByPostal());
        $copyObj->setPackingSlipType($this->getPackingSlipType());
        $copyObj->setWorkInProgress($this->getWorkInProgress());
        $copyObj->setPackageAmount($this->getPackageAmount());
        $copyObj->setCouponCode($this->getCouponCode());
        $copyObj->setCouponDiscountAmount($this->getCouponDiscountAmount());
        $copyObj->setCouponDiscountPercentage($this->getCouponDiscountPercentage());
        $copyObj->setHideUntill($this->getHideUntill());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getSaleOrderPayments() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaleOrderPayment($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPayments() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPayment($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaleOrderEmails() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaleOrderEmail($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaleOrderItems() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaleOrderItem($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaleOrderNotifications() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaleOrderNotification($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Sale\SaleOrder Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a Site object.
     *
     * @param  Site|null $v
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSite(Site $v = null)
    {
        if ($v === null) {
            $this->setSiteId(NULL);
        } else {
            $this->setSiteId($v->getId());
        }

        $this->aSite = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Site object, it will not be re-added.
        if ($v !== null) {
            $v->addSaleOrder($this);
        }


        return $this;
    }


    /**
     * Get the associated Site object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Site|null The associated Site object.
     * @throws PropelException
     */
    public function getSite(ConnectionInterface $con = null)
    {
        if ($this->aSite === null && ($this->site_id != 0)) {
            $this->aSite = SiteQuery::create()->findPk($this->site_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSite->addSaleOrders($this);
             */
        }

        return $this->aSite;
    }

    /**
     * Declares an association between this object and a SaleOrderStatus object.
     *
     * @param  SaleOrderStatus|null $v
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSaleOrderStatus(SaleOrderStatus $v = null)
    {
        if ($v === null) {
            $this->setSaleOrderStatusId(0);
        } else {
            $this->setSaleOrderStatusId($v->getId());
        }

        $this->aSaleOrderStatus = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the SaleOrderStatus object, it will not be re-added.
        if ($v !== null) {
            $v->addSaleOrder($this);
        }


        return $this;
    }


    /**
     * Get the associated SaleOrderStatus object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return SaleOrderStatus|null The associated SaleOrderStatus object.
     * @throws PropelException
     */
    public function getSaleOrderStatus(ConnectionInterface $con = null)
    {
        if ($this->aSaleOrderStatus === null && ($this->sale_order_status_id != 0)) {
            $this->aSaleOrderStatus = SaleOrderStatusQuery::create()->findPk($this->sale_order_status_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSaleOrderStatus->addSaleOrders($this);
             */
        }

        return $this->aSaleOrderStatus;
    }

    /**
     * Declares an association between this object and a ShippingMethod object.
     *
     * @param  ShippingMethod|null $v
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     * @throws PropelException
     */
    public function setShippingMethod(ShippingMethod $v = null)
    {
        if ($v === null) {
            $this->setShippingMethodId(NULL);
        } else {
            $this->setShippingMethodId($v->getId());
        }

        $this->aShippingMethod = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ShippingMethod object, it will not be re-added.
        if ($v !== null) {
            $v->addSaleOrder($this);
        }


        return $this;
    }


    /**
     * Get the associated ShippingMethod object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ShippingMethod|null The associated ShippingMethod object.
     * @throws PropelException
     */
    public function getShippingMethod(ConnectionInterface $con = null)
    {
        if ($this->aShippingMethod === null && ($this->shipping_method_id != 0)) {
            $this->aShippingMethod = ShippingMethodQuery::create()->findPk($this->shipping_method_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aShippingMethod->addSaleOrders($this);
             */
        }

        return $this->aShippingMethod;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User|null $v
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUser(User $v = null)
    {
        if ($v === null) {
            $this->setCreatedByUserId(NULL);
        } else {
            $this->setCreatedByUserId($v->getId());
        }

        $this->aUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addSaleOrder($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User|null The associated User object.
     * @throws PropelException
     */
    public function getUser(ConnectionInterface $con = null)
    {
        if ($this->aUser === null && ($this->created_by_user_id != 0)) {
            $this->aUser = UserQuery::create()->findPk($this->created_by_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUser->addSaleOrders($this);
             */
        }

        return $this->aUser;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SaleOrderPayment' === $relationName) {
            $this->initSaleOrderPayments();
            return;
        }
        if ('Payment' === $relationName) {
            $this->initPayments();
            return;
        }
        if ('SaleOrderEmail' === $relationName) {
            $this->initSaleOrderEmails();
            return;
        }
        if ('SaleOrderItem' === $relationName) {
            $this->initSaleOrderItems();
            return;
        }
        if ('SaleOrderNotification' === $relationName) {
            $this->initSaleOrderNotifications();
            return;
        }
    }

    /**
     * Clears out the collSaleOrderPayments collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSaleOrderPayments()
     */
    public function clearSaleOrderPayments()
    {
        $this->collSaleOrderPayments = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSaleOrderPayments collection loaded partially.
     */
    public function resetPartialSaleOrderPayments($v = true)
    {
        $this->collSaleOrderPaymentsPartial = $v;
    }

    /**
     * Initializes the collSaleOrderPayments collection.
     *
     * By default this just sets the collSaleOrderPayments collection to an empty array (like clearcollSaleOrderPayments());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaleOrderPayments($overrideExisting = true)
    {
        if (null !== $this->collSaleOrderPayments && !$overrideExisting) {
            return;
        }

        $collectionClassName = SaleOrderPaymentTableMap::getTableMap()->getCollectionClassName();

        $this->collSaleOrderPayments = new $collectionClassName;
        $this->collSaleOrderPayments->setModel('\Model\Sale\SaleOrderPayment');
    }

    /**
     * Gets an array of ChildSaleOrderPayment objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSaleOrder is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSaleOrderPayment[] List of ChildSaleOrderPayment objects
     * @throws PropelException
     */
    public function getSaleOrderPayments(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderPaymentsPartial && !$this->isNew();
        if (null === $this->collSaleOrderPayments || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSaleOrderPayments) {
                    $this->initSaleOrderPayments();
                } else {
                    $collectionClassName = SaleOrderPaymentTableMap::getTableMap()->getCollectionClassName();

                    $collSaleOrderPayments = new $collectionClassName;
                    $collSaleOrderPayments->setModel('\Model\Sale\SaleOrderPayment');

                    return $collSaleOrderPayments;
                }
            } else {
                $collSaleOrderPayments = ChildSaleOrderPaymentQuery::create(null, $criteria)
                    ->filterBySaleOrder($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSaleOrderPaymentsPartial && count($collSaleOrderPayments)) {
                        $this->initSaleOrderPayments(false);

                        foreach ($collSaleOrderPayments as $obj) {
                            if (false == $this->collSaleOrderPayments->contains($obj)) {
                                $this->collSaleOrderPayments->append($obj);
                            }
                        }

                        $this->collSaleOrderPaymentsPartial = true;
                    }

                    return $collSaleOrderPayments;
                }

                if ($partial && $this->collSaleOrderPayments) {
                    foreach ($this->collSaleOrderPayments as $obj) {
                        if ($obj->isNew()) {
                            $collSaleOrderPayments[] = $obj;
                        }
                    }
                }

                $this->collSaleOrderPayments = $collSaleOrderPayments;
                $this->collSaleOrderPaymentsPartial = false;
            }
        }

        return $this->collSaleOrderPayments;
    }

    /**
     * Sets a collection of ChildSaleOrderPayment objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $saleOrderPayments A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSaleOrder The current object (for fluent API support)
     */
    public function setSaleOrderPayments(Collection $saleOrderPayments, ConnectionInterface $con = null)
    {
        /** @var ChildSaleOrderPayment[] $saleOrderPaymentsToDelete */
        $saleOrderPaymentsToDelete = $this->getSaleOrderPayments(new Criteria(), $con)->diff($saleOrderPayments);


        $this->saleOrderPaymentsScheduledForDeletion = $saleOrderPaymentsToDelete;

        foreach ($saleOrderPaymentsToDelete as $saleOrderPaymentRemoved) {
            $saleOrderPaymentRemoved->setSaleOrder(null);
        }

        $this->collSaleOrderPayments = null;
        foreach ($saleOrderPayments as $saleOrderPayment) {
            $this->addSaleOrderPayment($saleOrderPayment);
        }

        $this->collSaleOrderPayments = $saleOrderPayments;
        $this->collSaleOrderPaymentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SaleOrderPayment objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SaleOrderPayment objects.
     * @throws PropelException
     */
    public function countSaleOrderPayments(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderPaymentsPartial && !$this->isNew();
        if (null === $this->collSaleOrderPayments || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaleOrderPayments) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSaleOrderPayments());
            }

            $query = ChildSaleOrderPaymentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySaleOrder($this)
                ->count($con);
        }

        return count($this->collSaleOrderPayments);
    }

    /**
     * Method called to associate a ChildSaleOrderPayment object to this object
     * through the ChildSaleOrderPayment foreign key attribute.
     *
     * @param  ChildSaleOrderPayment $l ChildSaleOrderPayment
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function addSaleOrderPayment(ChildSaleOrderPayment $l)
    {
        if ($this->collSaleOrderPayments === null) {
            $this->initSaleOrderPayments();
            $this->collSaleOrderPaymentsPartial = true;
        }

        if (!$this->collSaleOrderPayments->contains($l)) {
            $this->doAddSaleOrderPayment($l);

            if ($this->saleOrderPaymentsScheduledForDeletion and $this->saleOrderPaymentsScheduledForDeletion->contains($l)) {
                $this->saleOrderPaymentsScheduledForDeletion->remove($this->saleOrderPaymentsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSaleOrderPayment $saleOrderPayment The ChildSaleOrderPayment object to add.
     */
    protected function doAddSaleOrderPayment(ChildSaleOrderPayment $saleOrderPayment)
    {
        $this->collSaleOrderPayments[]= $saleOrderPayment;
        $saleOrderPayment->setSaleOrder($this);
    }

    /**
     * @param  ChildSaleOrderPayment $saleOrderPayment The ChildSaleOrderPayment object to remove.
     * @return $this|ChildSaleOrder The current object (for fluent API support)
     */
    public function removeSaleOrderPayment(ChildSaleOrderPayment $saleOrderPayment)
    {
        if ($this->getSaleOrderPayments()->contains($saleOrderPayment)) {
            $pos = $this->collSaleOrderPayments->search($saleOrderPayment);
            $this->collSaleOrderPayments->remove($pos);
            if (null === $this->saleOrderPaymentsScheduledForDeletion) {
                $this->saleOrderPaymentsScheduledForDeletion = clone $this->collSaleOrderPayments;
                $this->saleOrderPaymentsScheduledForDeletion->clear();
            }
            $this->saleOrderPaymentsScheduledForDeletion[]= $saleOrderPayment;
            $saleOrderPayment->setSaleOrder(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SaleOrder is new, it will return
     * an empty collection; or if this SaleOrder has previously
     * been saved, it will retrieve related SaleOrderPayments from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SaleOrder.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSaleOrderPayment[] List of ChildSaleOrderPayment objects
     */
    public function getSaleOrderPaymentsJoinPaymentMethod(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSaleOrderPaymentQuery::create(null, $criteria);
        $query->joinWith('PaymentMethod', $joinBehavior);

        return $this->getSaleOrderPayments($query, $con);
    }

    /**
     * Clears out the collPayments collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPayments()
     */
    public function clearPayments()
    {
        $this->collPayments = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPayments collection loaded partially.
     */
    public function resetPartialPayments($v = true)
    {
        $this->collPaymentsPartial = $v;
    }

    /**
     * Initializes the collPayments collection.
     *
     * By default this just sets the collPayments collection to an empty array (like clearcollPayments());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPayments($overrideExisting = true)
    {
        if (null !== $this->collPayments && !$overrideExisting) {
            return;
        }

        $collectionClassName = PaymentTableMap::getTableMap()->getCollectionClassName();

        $this->collPayments = new $collectionClassName;
        $this->collPayments->setModel('\Model\Finance\Payment');
    }

    /**
     * Gets an array of Payment objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSaleOrder is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Payment[] List of Payment objects
     * @throws PropelException
     */
    public function getPayments(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPaymentsPartial && !$this->isNew();
        if (null === $this->collPayments || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPayments) {
                    $this->initPayments();
                } else {
                    $collectionClassName = PaymentTableMap::getTableMap()->getCollectionClassName();

                    $collPayments = new $collectionClassName;
                    $collPayments->setModel('\Model\Finance\Payment');

                    return $collPayments;
                }
            } else {
                $collPayments = PaymentQuery::create(null, $criteria)
                    ->filterBySaleOrder($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPaymentsPartial && count($collPayments)) {
                        $this->initPayments(false);

                        foreach ($collPayments as $obj) {
                            if (false == $this->collPayments->contains($obj)) {
                                $this->collPayments->append($obj);
                            }
                        }

                        $this->collPaymentsPartial = true;
                    }

                    return $collPayments;
                }

                if ($partial && $this->collPayments) {
                    foreach ($this->collPayments as $obj) {
                        if ($obj->isNew()) {
                            $collPayments[] = $obj;
                        }
                    }
                }

                $this->collPayments = $collPayments;
                $this->collPaymentsPartial = false;
            }
        }

        return $this->collPayments;
    }

    /**
     * Sets a collection of Payment objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $payments A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSaleOrder The current object (for fluent API support)
     */
    public function setPayments(Collection $payments, ConnectionInterface $con = null)
    {
        /** @var Payment[] $paymentsToDelete */
        $paymentsToDelete = $this->getPayments(new Criteria(), $con)->diff($payments);


        $this->paymentsScheduledForDeletion = $paymentsToDelete;

        foreach ($paymentsToDelete as $paymentRemoved) {
            $paymentRemoved->setSaleOrder(null);
        }

        $this->collPayments = null;
        foreach ($payments as $payment) {
            $this->addPayment($payment);
        }

        $this->collPayments = $payments;
        $this->collPaymentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BasePayment objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BasePayment objects.
     * @throws PropelException
     */
    public function countPayments(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPaymentsPartial && !$this->isNew();
        if (null === $this->collPayments || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPayments) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPayments());
            }

            $query = PaymentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySaleOrder($this)
                ->count($con);
        }

        return count($this->collPayments);
    }

    /**
     * Method called to associate a Payment object to this object
     * through the Payment foreign key attribute.
     *
     * @param  Payment $l Payment
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function addPayment(Payment $l)
    {
        if ($this->collPayments === null) {
            $this->initPayments();
            $this->collPaymentsPartial = true;
        }

        if (!$this->collPayments->contains($l)) {
            $this->doAddPayment($l);

            if ($this->paymentsScheduledForDeletion and $this->paymentsScheduledForDeletion->contains($l)) {
                $this->paymentsScheduledForDeletion->remove($this->paymentsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Payment $payment The Payment object to add.
     */
    protected function doAddPayment(Payment $payment)
    {
        $this->collPayments[]= $payment;
        $payment->setSaleOrder($this);
    }

    /**
     * @param  Payment $payment The Payment object to remove.
     * @return $this|ChildSaleOrder The current object (for fluent API support)
     */
    public function removePayment(Payment $payment)
    {
        if ($this->getPayments()->contains($payment)) {
            $pos = $this->collPayments->search($payment);
            $this->collPayments->remove($pos);
            if (null === $this->paymentsScheduledForDeletion) {
                $this->paymentsScheduledForDeletion = clone $this->collPayments;
                $this->paymentsScheduledForDeletion->clear();
            }
            $this->paymentsScheduledForDeletion[]= $payment;
            $payment->setSaleOrder(null);
        }

        return $this;
    }

    /**
     * Clears out the collSaleOrderEmails collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSaleOrderEmails()
     */
    public function clearSaleOrderEmails()
    {
        $this->collSaleOrderEmails = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSaleOrderEmails collection loaded partially.
     */
    public function resetPartialSaleOrderEmails($v = true)
    {
        $this->collSaleOrderEmailsPartial = $v;
    }

    /**
     * Initializes the collSaleOrderEmails collection.
     *
     * By default this just sets the collSaleOrderEmails collection to an empty array (like clearcollSaleOrderEmails());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaleOrderEmails($overrideExisting = true)
    {
        if (null !== $this->collSaleOrderEmails && !$overrideExisting) {
            return;
        }

        $collectionClassName = SaleOrderEmailTableMap::getTableMap()->getCollectionClassName();

        $this->collSaleOrderEmails = new $collectionClassName;
        $this->collSaleOrderEmails->setModel('\Model\Sale\SaleOrderEmail');
    }

    /**
     * Gets an array of ChildSaleOrderEmail objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSaleOrder is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSaleOrderEmail[] List of ChildSaleOrderEmail objects
     * @throws PropelException
     */
    public function getSaleOrderEmails(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderEmailsPartial && !$this->isNew();
        if (null === $this->collSaleOrderEmails || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSaleOrderEmails) {
                    $this->initSaleOrderEmails();
                } else {
                    $collectionClassName = SaleOrderEmailTableMap::getTableMap()->getCollectionClassName();

                    $collSaleOrderEmails = new $collectionClassName;
                    $collSaleOrderEmails->setModel('\Model\Sale\SaleOrderEmail');

                    return $collSaleOrderEmails;
                }
            } else {
                $collSaleOrderEmails = ChildSaleOrderEmailQuery::create(null, $criteria)
                    ->filterBySaleOrder($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSaleOrderEmailsPartial && count($collSaleOrderEmails)) {
                        $this->initSaleOrderEmails(false);

                        foreach ($collSaleOrderEmails as $obj) {
                            if (false == $this->collSaleOrderEmails->contains($obj)) {
                                $this->collSaleOrderEmails->append($obj);
                            }
                        }

                        $this->collSaleOrderEmailsPartial = true;
                    }

                    return $collSaleOrderEmails;
                }

                if ($partial && $this->collSaleOrderEmails) {
                    foreach ($this->collSaleOrderEmails as $obj) {
                        if ($obj->isNew()) {
                            $collSaleOrderEmails[] = $obj;
                        }
                    }
                }

                $this->collSaleOrderEmails = $collSaleOrderEmails;
                $this->collSaleOrderEmailsPartial = false;
            }
        }

        return $this->collSaleOrderEmails;
    }

    /**
     * Sets a collection of ChildSaleOrderEmail objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $saleOrderEmails A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSaleOrder The current object (for fluent API support)
     */
    public function setSaleOrderEmails(Collection $saleOrderEmails, ConnectionInterface $con = null)
    {
        /** @var ChildSaleOrderEmail[] $saleOrderEmailsToDelete */
        $saleOrderEmailsToDelete = $this->getSaleOrderEmails(new Criteria(), $con)->diff($saleOrderEmails);


        $this->saleOrderEmailsScheduledForDeletion = $saleOrderEmailsToDelete;

        foreach ($saleOrderEmailsToDelete as $saleOrderEmailRemoved) {
            $saleOrderEmailRemoved->setSaleOrder(null);
        }

        $this->collSaleOrderEmails = null;
        foreach ($saleOrderEmails as $saleOrderEmail) {
            $this->addSaleOrderEmail($saleOrderEmail);
        }

        $this->collSaleOrderEmails = $saleOrderEmails;
        $this->collSaleOrderEmailsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SaleOrderEmail objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SaleOrderEmail objects.
     * @throws PropelException
     */
    public function countSaleOrderEmails(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderEmailsPartial && !$this->isNew();
        if (null === $this->collSaleOrderEmails || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaleOrderEmails) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSaleOrderEmails());
            }

            $query = ChildSaleOrderEmailQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySaleOrder($this)
                ->count($con);
        }

        return count($this->collSaleOrderEmails);
    }

    /**
     * Method called to associate a ChildSaleOrderEmail object to this object
     * through the ChildSaleOrderEmail foreign key attribute.
     *
     * @param  ChildSaleOrderEmail $l ChildSaleOrderEmail
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function addSaleOrderEmail(ChildSaleOrderEmail $l)
    {
        if ($this->collSaleOrderEmails === null) {
            $this->initSaleOrderEmails();
            $this->collSaleOrderEmailsPartial = true;
        }

        if (!$this->collSaleOrderEmails->contains($l)) {
            $this->doAddSaleOrderEmail($l);

            if ($this->saleOrderEmailsScheduledForDeletion and $this->saleOrderEmailsScheduledForDeletion->contains($l)) {
                $this->saleOrderEmailsScheduledForDeletion->remove($this->saleOrderEmailsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSaleOrderEmail $saleOrderEmail The ChildSaleOrderEmail object to add.
     */
    protected function doAddSaleOrderEmail(ChildSaleOrderEmail $saleOrderEmail)
    {
        $this->collSaleOrderEmails[]= $saleOrderEmail;
        $saleOrderEmail->setSaleOrder($this);
    }

    /**
     * @param  ChildSaleOrderEmail $saleOrderEmail The ChildSaleOrderEmail object to remove.
     * @return $this|ChildSaleOrder The current object (for fluent API support)
     */
    public function removeSaleOrderEmail(ChildSaleOrderEmail $saleOrderEmail)
    {
        if ($this->getSaleOrderEmails()->contains($saleOrderEmail)) {
            $pos = $this->collSaleOrderEmails->search($saleOrderEmail);
            $this->collSaleOrderEmails->remove($pos);
            if (null === $this->saleOrderEmailsScheduledForDeletion) {
                $this->saleOrderEmailsScheduledForDeletion = clone $this->collSaleOrderEmails;
                $this->saleOrderEmailsScheduledForDeletion->clear();
            }
            $this->saleOrderEmailsScheduledForDeletion[]= clone $saleOrderEmail;
            $saleOrderEmail->setSaleOrder(null);
        }

        return $this;
    }

    /**
     * Clears out the collSaleOrderItems collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSaleOrderItems()
     */
    public function clearSaleOrderItems()
    {
        $this->collSaleOrderItems = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSaleOrderItems collection loaded partially.
     */
    public function resetPartialSaleOrderItems($v = true)
    {
        $this->collSaleOrderItemsPartial = $v;
    }

    /**
     * Initializes the collSaleOrderItems collection.
     *
     * By default this just sets the collSaleOrderItems collection to an empty array (like clearcollSaleOrderItems());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaleOrderItems($overrideExisting = true)
    {
        if (null !== $this->collSaleOrderItems && !$overrideExisting) {
            return;
        }

        $collectionClassName = SaleOrderItemTableMap::getTableMap()->getCollectionClassName();

        $this->collSaleOrderItems = new $collectionClassName;
        $this->collSaleOrderItems->setModel('\Model\Sale\SaleOrderItem');
    }

    /**
     * Gets an array of ChildSaleOrderItem objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSaleOrder is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSaleOrderItem[] List of ChildSaleOrderItem objects
     * @throws PropelException
     */
    public function getSaleOrderItems(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderItemsPartial && !$this->isNew();
        if (null === $this->collSaleOrderItems || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSaleOrderItems) {
                    $this->initSaleOrderItems();
                } else {
                    $collectionClassName = SaleOrderItemTableMap::getTableMap()->getCollectionClassName();

                    $collSaleOrderItems = new $collectionClassName;
                    $collSaleOrderItems->setModel('\Model\Sale\SaleOrderItem');

                    return $collSaleOrderItems;
                }
            } else {
                $collSaleOrderItems = ChildSaleOrderItemQuery::create(null, $criteria)
                    ->filterBySaleOrder($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSaleOrderItemsPartial && count($collSaleOrderItems)) {
                        $this->initSaleOrderItems(false);

                        foreach ($collSaleOrderItems as $obj) {
                            if (false == $this->collSaleOrderItems->contains($obj)) {
                                $this->collSaleOrderItems->append($obj);
                            }
                        }

                        $this->collSaleOrderItemsPartial = true;
                    }

                    return $collSaleOrderItems;
                }

                if ($partial && $this->collSaleOrderItems) {
                    foreach ($this->collSaleOrderItems as $obj) {
                        if ($obj->isNew()) {
                            $collSaleOrderItems[] = $obj;
                        }
                    }
                }

                $this->collSaleOrderItems = $collSaleOrderItems;
                $this->collSaleOrderItemsPartial = false;
            }
        }

        return $this->collSaleOrderItems;
    }

    /**
     * Sets a collection of ChildSaleOrderItem objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $saleOrderItems A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSaleOrder The current object (for fluent API support)
     */
    public function setSaleOrderItems(Collection $saleOrderItems, ConnectionInterface $con = null)
    {
        /** @var ChildSaleOrderItem[] $saleOrderItemsToDelete */
        $saleOrderItemsToDelete = $this->getSaleOrderItems(new Criteria(), $con)->diff($saleOrderItems);


        $this->saleOrderItemsScheduledForDeletion = $saleOrderItemsToDelete;

        foreach ($saleOrderItemsToDelete as $saleOrderItemRemoved) {
            $saleOrderItemRemoved->setSaleOrder(null);
        }

        $this->collSaleOrderItems = null;
        foreach ($saleOrderItems as $saleOrderItem) {
            $this->addSaleOrderItem($saleOrderItem);
        }

        $this->collSaleOrderItems = $saleOrderItems;
        $this->collSaleOrderItemsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SaleOrderItem objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SaleOrderItem objects.
     * @throws PropelException
     */
    public function countSaleOrderItems(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderItemsPartial && !$this->isNew();
        if (null === $this->collSaleOrderItems || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaleOrderItems) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSaleOrderItems());
            }

            $query = ChildSaleOrderItemQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySaleOrder($this)
                ->count($con);
        }

        return count($this->collSaleOrderItems);
    }

    /**
     * Method called to associate a ChildSaleOrderItem object to this object
     * through the ChildSaleOrderItem foreign key attribute.
     *
     * @param  ChildSaleOrderItem $l ChildSaleOrderItem
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function addSaleOrderItem(ChildSaleOrderItem $l)
    {
        if ($this->collSaleOrderItems === null) {
            $this->initSaleOrderItems();
            $this->collSaleOrderItemsPartial = true;
        }

        if (!$this->collSaleOrderItems->contains($l)) {
            $this->doAddSaleOrderItem($l);

            if ($this->saleOrderItemsScheduledForDeletion and $this->saleOrderItemsScheduledForDeletion->contains($l)) {
                $this->saleOrderItemsScheduledForDeletion->remove($this->saleOrderItemsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSaleOrderItem $saleOrderItem The ChildSaleOrderItem object to add.
     */
    protected function doAddSaleOrderItem(ChildSaleOrderItem $saleOrderItem)
    {
        $this->collSaleOrderItems[]= $saleOrderItem;
        $saleOrderItem->setSaleOrder($this);
    }

    /**
     * @param  ChildSaleOrderItem $saleOrderItem The ChildSaleOrderItem object to remove.
     * @return $this|ChildSaleOrder The current object (for fluent API support)
     */
    public function removeSaleOrderItem(ChildSaleOrderItem $saleOrderItem)
    {
        if ($this->getSaleOrderItems()->contains($saleOrderItem)) {
            $pos = $this->collSaleOrderItems->search($saleOrderItem);
            $this->collSaleOrderItems->remove($pos);
            if (null === $this->saleOrderItemsScheduledForDeletion) {
                $this->saleOrderItemsScheduledForDeletion = clone $this->collSaleOrderItems;
                $this->saleOrderItemsScheduledForDeletion->clear();
            }
            $this->saleOrderItemsScheduledForDeletion[]= clone $saleOrderItem;
            $saleOrderItem->setSaleOrder(null);
        }

        return $this;
    }

    /**
     * Clears out the collSaleOrderNotifications collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSaleOrderNotifications()
     */
    public function clearSaleOrderNotifications()
    {
        $this->collSaleOrderNotifications = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSaleOrderNotifications collection loaded partially.
     */
    public function resetPartialSaleOrderNotifications($v = true)
    {
        $this->collSaleOrderNotificationsPartial = $v;
    }

    /**
     * Initializes the collSaleOrderNotifications collection.
     *
     * By default this just sets the collSaleOrderNotifications collection to an empty array (like clearcollSaleOrderNotifications());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaleOrderNotifications($overrideExisting = true)
    {
        if (null !== $this->collSaleOrderNotifications && !$overrideExisting) {
            return;
        }

        $collectionClassName = SaleOrderNotificationTableMap::getTableMap()->getCollectionClassName();

        $this->collSaleOrderNotifications = new $collectionClassName;
        $this->collSaleOrderNotifications->setModel('\Model\SaleOrderNotification\SaleOrderNotification');
    }

    /**
     * Gets an array of SaleOrderNotification objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSaleOrder is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|SaleOrderNotification[] List of SaleOrderNotification objects
     * @throws PropelException
     */
    public function getSaleOrderNotifications(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderNotificationsPartial && !$this->isNew();
        if (null === $this->collSaleOrderNotifications || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSaleOrderNotifications) {
                    $this->initSaleOrderNotifications();
                } else {
                    $collectionClassName = SaleOrderNotificationTableMap::getTableMap()->getCollectionClassName();

                    $collSaleOrderNotifications = new $collectionClassName;
                    $collSaleOrderNotifications->setModel('\Model\SaleOrderNotification\SaleOrderNotification');

                    return $collSaleOrderNotifications;
                }
            } else {
                $collSaleOrderNotifications = SaleOrderNotificationQuery::create(null, $criteria)
                    ->filterBySaleOrder($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSaleOrderNotificationsPartial && count($collSaleOrderNotifications)) {
                        $this->initSaleOrderNotifications(false);

                        foreach ($collSaleOrderNotifications as $obj) {
                            if (false == $this->collSaleOrderNotifications->contains($obj)) {
                                $this->collSaleOrderNotifications->append($obj);
                            }
                        }

                        $this->collSaleOrderNotificationsPartial = true;
                    }

                    return $collSaleOrderNotifications;
                }

                if ($partial && $this->collSaleOrderNotifications) {
                    foreach ($this->collSaleOrderNotifications as $obj) {
                        if ($obj->isNew()) {
                            $collSaleOrderNotifications[] = $obj;
                        }
                    }
                }

                $this->collSaleOrderNotifications = $collSaleOrderNotifications;
                $this->collSaleOrderNotificationsPartial = false;
            }
        }

        return $this->collSaleOrderNotifications;
    }

    /**
     * Sets a collection of SaleOrderNotification objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $saleOrderNotifications A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSaleOrder The current object (for fluent API support)
     */
    public function setSaleOrderNotifications(Collection $saleOrderNotifications, ConnectionInterface $con = null)
    {
        /** @var SaleOrderNotification[] $saleOrderNotificationsToDelete */
        $saleOrderNotificationsToDelete = $this->getSaleOrderNotifications(new Criteria(), $con)->diff($saleOrderNotifications);


        $this->saleOrderNotificationsScheduledForDeletion = $saleOrderNotificationsToDelete;

        foreach ($saleOrderNotificationsToDelete as $saleOrderNotificationRemoved) {
            $saleOrderNotificationRemoved->setSaleOrder(null);
        }

        $this->collSaleOrderNotifications = null;
        foreach ($saleOrderNotifications as $saleOrderNotification) {
            $this->addSaleOrderNotification($saleOrderNotification);
        }

        $this->collSaleOrderNotifications = $saleOrderNotifications;
        $this->collSaleOrderNotificationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSaleOrderNotification objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSaleOrderNotification objects.
     * @throws PropelException
     */
    public function countSaleOrderNotifications(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderNotificationsPartial && !$this->isNew();
        if (null === $this->collSaleOrderNotifications || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaleOrderNotifications) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSaleOrderNotifications());
            }

            $query = SaleOrderNotificationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySaleOrder($this)
                ->count($con);
        }

        return count($this->collSaleOrderNotifications);
    }

    /**
     * Method called to associate a SaleOrderNotification object to this object
     * through the SaleOrderNotification foreign key attribute.
     *
     * @param  SaleOrderNotification $l SaleOrderNotification
     * @return $this|\Model\Sale\SaleOrder The current object (for fluent API support)
     */
    public function addSaleOrderNotification(SaleOrderNotification $l)
    {
        if ($this->collSaleOrderNotifications === null) {
            $this->initSaleOrderNotifications();
            $this->collSaleOrderNotificationsPartial = true;
        }

        if (!$this->collSaleOrderNotifications->contains($l)) {
            $this->doAddSaleOrderNotification($l);

            if ($this->saleOrderNotificationsScheduledForDeletion and $this->saleOrderNotificationsScheduledForDeletion->contains($l)) {
                $this->saleOrderNotificationsScheduledForDeletion->remove($this->saleOrderNotificationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param SaleOrderNotification $saleOrderNotification The SaleOrderNotification object to add.
     */
    protected function doAddSaleOrderNotification(SaleOrderNotification $saleOrderNotification)
    {
        $this->collSaleOrderNotifications[]= $saleOrderNotification;
        $saleOrderNotification->setSaleOrder($this);
    }

    /**
     * @param  SaleOrderNotification $saleOrderNotification The SaleOrderNotification object to remove.
     * @return $this|ChildSaleOrder The current object (for fluent API support)
     */
    public function removeSaleOrderNotification(SaleOrderNotification $saleOrderNotification)
    {
        if ($this->getSaleOrderNotifications()->contains($saleOrderNotification)) {
            $pos = $this->collSaleOrderNotifications->search($saleOrderNotification);
            $this->collSaleOrderNotifications->remove($pos);
            if (null === $this->saleOrderNotificationsScheduledForDeletion) {
                $this->saleOrderNotificationsScheduledForDeletion = clone $this->collSaleOrderNotifications;
                $this->saleOrderNotificationsScheduledForDeletion->clear();
            }
            $this->saleOrderNotificationsScheduledForDeletion[]= $saleOrderNotification;
            $saleOrderNotification->setSaleOrder(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SaleOrder is new, it will return
     * an empty collection; or if this SaleOrder has previously
     * been saved, it will retrieve related SaleOrderNotifications from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SaleOrder.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SaleOrderNotification[] List of SaleOrderNotification objects
     */
    public function getSaleOrderNotificationsJoinSaleOrderNotificationType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SaleOrderNotificationQuery::create(null, $criteria);
        $query->joinWith('SaleOrderNotificationType', $joinBehavior);

        return $this->getSaleOrderNotifications($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aSite) {
            $this->aSite->removeSaleOrder($this);
        }
        if (null !== $this->aSaleOrderStatus) {
            $this->aSaleOrderStatus->removeSaleOrder($this);
        }
        if (null !== $this->aShippingMethod) {
            $this->aShippingMethod->removeSaleOrder($this);
        }
        if (null !== $this->aUser) {
            $this->aUser->removeSaleOrder($this);
        }
        $this->id = null;
        $this->site_id = null;
        $this->language_id = null;
        $this->is_closed = null;
        $this->is_fully_paid = null;
        $this->pay_date = null;
        $this->is_ready_for_shipping = null;
        $this->is_shipping_checked = null;
        $this->is_fully_shipped = null;
        $this->is_pushed_to_accounting = null;
        $this->ship_date = null;
        $this->sale_order_status_id = null;
        $this->handled_by = null;
        $this->source = null;
        $this->is_completed = null;
        $this->created_by_user_id = null;
        $this->created_on = null;
        $this->estimated_delivery_date = null;
        $this->customer_id = null;
        $this->envelope_weight_grams = null;
        $this->webshop_user_session_id = null;
        $this->invoice_number = null;
        $this->invoice_date = null;
        $this->payterm_days = null;
        $this->payterm_original_id = null;
        $this->payterm_string = null;
        $this->order_number = null;
        $this->is_deleted = null;
        $this->company_name = null;
        $this->debitor = null;
        $this->our_custom_folder = null;
        $this->our_slogan = null;
        $this->our_phone = null;
        $this->our_fax = null;
        $this->our_website = null;
        $this->our_email = null;
        $this->our_vat_number = null;
        $this->our_chamber_of_commerce = null;
        $this->our_iban_number = null;
        $this->our_bic_number = null;
        $this->our_general_company_name = null;
        $this->our_general_street = null;
        $this->our_general_number = null;
        $this->our_general_number_add = null;
        $this->our_general_postal = null;
        $this->our_general_po_box = null;
        $this->our_general_city = null;
        $this->our_general_country = null;
        $this->customer_phone = null;
        $this->customer_fax = null;
        $this->customer_email = null;
        $this->customer_chamber_of_commerce = null;
        $this->customer_vat_number = null;
        $this->customer_order_reference = null;
        $this->customer_order_placed_by = null;
        $this->customer_invoice_address_id = null;
        $this->customer_invoice_company_name = null;
        $this->customer_invoice_attn_name = null;
        $this->customer_invoice_street = null;
        $this->customer_invoice_number = null;
        $this->customer_invoice_number_add = null;
        $this->customer_invoice_postal = null;
        $this->customer_invoice_city = null;
        $this->customer_invoice_country = null;
        $this->customer_invoice_usa_state = null;
        $this->customer_invoice_address_l1 = null;
        $this->customer_invoice_address_l2 = null;
        $this->customer_delivery_address_id = null;
        $this->customer_delivery_company_name = null;
        $this->customer_delivery_attn_name = null;
        $this->customer_delivery_street = null;
        $this->customer_delivery_number = null;
        $this->customer_delivery_number_add = null;
        $this->customer_delivery_postal = null;
        $this->customer_delivery_city = null;
        $this->customer_delivery_country = null;
        $this->customer_delivery_usa_state = null;
        $this->customer_delivery_address_l1 = null;
        $this->customer_delivery_address_l2 = null;
        $this->cp_first_name = null;
        $this->cp_last_name = null;
        $this->cp_sallutation = null;
        $this->reference = null;
        $this->shipping_note = null;
        $this->customer_order_note = null;
        $this->shipping_order_note = null;
        $this->has_wrap = null;
        $this->wrapping_price = null;
        $this->shipping_carrier = null;
        $this->shipping_price = null;
        $this->shipping_method_id = null;
        $this->shipping_has_track_trace = null;
        $this->shipping_id = null;
        $this->tracking_code = null;
        $this->tracking_url = null;
        $this->paymethod_name = null;
        $this->paymethod_code = null;
        $this->paymethod_id = null;
        $this->shipping_registration_date = null;
        $this->invoice_by_postal = null;
        $this->packing_slip_type = null;
        $this->work_in_progress = null;
        $this->package_amount = null;
        $this->coupon_code = null;
        $this->coupon_discount_amount = null;
        $this->coupon_discount_percentage = null;
        $this->hide_untill = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSaleOrderPayments) {
                foreach ($this->collSaleOrderPayments as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPayments) {
                foreach ($this->collPayments as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaleOrderEmails) {
                foreach ($this->collSaleOrderEmails as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaleOrderItems) {
                foreach ($this->collSaleOrderItems as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaleOrderNotifications) {
                foreach ($this->collSaleOrderNotifications as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collSaleOrderPayments = null;
        $this->collPayments = null;
        $this->collSaleOrderEmails = null;
        $this->collSaleOrderItems = null;
        $this->collSaleOrderNotifications = null;
        $this->aSite = null;
        $this->aSaleOrderStatus = null;
        $this->aShippingMethod = null;
        $this->aUser = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SaleOrderTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
