<?php

namespace Model\Sale\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Sale\OrderItemProduct as ChildOrderItemProduct;
use Model\Sale\OrderItemProductQuery as ChildOrderItemProductQuery;
use Model\Sale\SaleOrder as ChildSaleOrder;
use Model\Sale\SaleOrderItem as ChildSaleOrderItem;
use Model\Sale\SaleOrderItemQuery as ChildSaleOrderItemQuery;
use Model\Sale\SaleOrderItemStockClaimed as ChildSaleOrderItemStockClaimed;
use Model\Sale\SaleOrderItemStockClaimedQuery as ChildSaleOrderItemStockClaimedQuery;
use Model\Sale\SaleOrderQuery as ChildSaleOrderQuery;
use Model\Sale\Map\OrderItemProductTableMap;
use Model\Sale\Map\SaleOrderItemStockClaimedTableMap;
use Model\Sale\Map\SaleOrderItemTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'sale_order_item' table.
 *
 *
 *
 * @package    propel.generator.Model.Sale.Base
 */
abstract class SaleOrderItem implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Sale\\Map\\SaleOrderItemTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the created_on field.
     *
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        DateTime|null
     */
    protected $created_on;

    /**
     * The value for the sale_order_id field.
     *
     * @var        int
     */
    protected $sale_order_id;

    /**
     * The value for the product_number field.
     *
     * @var        string|null
     */
    protected $product_number;

    /**
     * The value for the description field.
     *
     * @var        string
     */
    protected $description;

    /**
     * The value for the quantity field.
     *
     * @var        double
     */
    protected $quantity;

    /**
     * The value for the single_item_weight field.
     *
     * @var        int|null
     */
    protected $single_item_weight;

    /**
     * The value for the price field.
     *
     * @var        double
     */
    protected $price;

    /**
     * The value for the vat_percentage field.
     *
     * @var        int|null
     */
    protected $vat_percentage;

    /**
     * The value for the manual_entry field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $manual_entry;

    /**
     * @var        ChildSaleOrder
     */
    protected $aSaleOrder;

    /**
     * @var        ObjectCollection|ChildOrderItemProduct[] Collection to store aggregation of ChildOrderItemProduct objects.
     */
    protected $collSaleOrderItemProducts;
    protected $collSaleOrderItemProductsPartial;

    /**
     * @var        ObjectCollection|ChildSaleOrderItemStockClaimed[] Collection to store aggregation of ChildSaleOrderItemStockClaimed objects.
     */
    protected $collSaleOrderItemStockClaimeds;
    protected $collSaleOrderItemStockClaimedsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildOrderItemProduct[]
     */
    protected $saleOrderItemProductsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSaleOrderItemStockClaimed[]
     */
    protected $saleOrderItemStockClaimedsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->manual_entry = false;
    }

    /**
     * Initializes internal state of Model\Sale\Base\SaleOrderItem object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>SaleOrderItem</code> instance.  If
     * <code>obj</code> is an instance of <code>SaleOrderItem</code>, delegates to
     * <code>equals(SaleOrderItem)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [optionally formatted] temporal [created_on] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedOn($format = null)
    {
        if ($format === null) {
            return $this->created_on;
        } else {
            return $this->created_on instanceof \DateTimeInterface ? $this->created_on->format($format) : null;
        }
    }

    /**
     * Get the [sale_order_id] column value.
     *
     * @return int
     */
    public function getSaleOrderId()
    {
        return $this->sale_order_id;
    }

    /**
     * Get the [product_number] column value.
     *
     * @return string|null
     */
    public function getProductNumber()
    {
        return $this->product_number;
    }

    /**
     * Get the [description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [quantity] column value.
     *
     * @return double
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Get the [single_item_weight] column value.
     *
     * @return int|null
     */
    public function getSingleItemWeight()
    {
        return $this->single_item_weight;
    }

    /**
     * Get the [price] column value.
     *
     * @return double
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Get the [vat_percentage] column value.
     *
     * @return int|null
     */
    public function getVatPercentage()
    {
        return $this->vat_percentage;
    }

    /**
     * Get the [manual_entry] column value.
     *
     * @return boolean|null
     */
    public function getManualEntry()
    {
        return $this->manual_entry;
    }

    /**
     * Get the [manual_entry] column value.
     *
     * @return boolean|null
     */
    public function isManualEntry()
    {
        return $this->getManualEntry();
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Sale\SaleOrderItem The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[SaleOrderItemTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Sets the value of [created_on] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Sale\SaleOrderItem The current object (for fluent API support)
     */
    public function setCreatedOn($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_on !== null || $dt !== null) {
            if ($this->created_on === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_on->format("Y-m-d H:i:s.u")) {
                $this->created_on = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleOrderItemTableMap::COL_CREATED_ON] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedOn()

    /**
     * Set the value of [sale_order_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Sale\SaleOrderItem The current object (for fluent API support)
     */
    public function setSaleOrderId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sale_order_id !== $v) {
            $this->sale_order_id = $v;
            $this->modifiedColumns[SaleOrderItemTableMap::COL_SALE_ORDER_ID] = true;
        }

        if ($this->aSaleOrder !== null && $this->aSaleOrder->getId() !== $v) {
            $this->aSaleOrder = null;
        }

        return $this;
    } // setSaleOrderId()

    /**
     * Set the value of [product_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrderItem The current object (for fluent API support)
     */
    public function setProductNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->product_number !== $v) {
            $this->product_number = $v;
            $this->modifiedColumns[SaleOrderItemTableMap::COL_PRODUCT_NUMBER] = true;
        }

        return $this;
    } // setProductNumber()

    /**
     * Set the value of [description] column.
     *
     * @param string $v New value
     * @return $this|\Model\Sale\SaleOrderItem The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[SaleOrderItemTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [quantity] column.
     *
     * @param double $v New value
     * @return $this|\Model\Sale\SaleOrderItem The current object (for fluent API support)
     */
    public function setQuantity($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->quantity !== $v) {
            $this->quantity = $v;
            $this->modifiedColumns[SaleOrderItemTableMap::COL_QUANTITY] = true;
        }

        return $this;
    } // setQuantity()

    /**
     * Set the value of [single_item_weight] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrderItem The current object (for fluent API support)
     */
    public function setSingleItemWeight($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->single_item_weight !== $v) {
            $this->single_item_weight = $v;
            $this->modifiedColumns[SaleOrderItemTableMap::COL_SINGLE_ITEM_WEIGHT] = true;
        }

        return $this;
    } // setSingleItemWeight()

    /**
     * Set the value of [price] column.
     *
     * @param double $v New value
     * @return $this|\Model\Sale\SaleOrderItem The current object (for fluent API support)
     */
    public function setPrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->price !== $v) {
            $this->price = $v;
            $this->modifiedColumns[SaleOrderItemTableMap::COL_PRICE] = true;
        }

        return $this;
    } // setPrice()

    /**
     * Set the value of [vat_percentage] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrderItem The current object (for fluent API support)
     */
    public function setVatPercentage($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vat_percentage !== $v) {
            $this->vat_percentage = $v;
            $this->modifiedColumns[SaleOrderItemTableMap::COL_VAT_PERCENTAGE] = true;
        }

        return $this;
    } // setVatPercentage()

    /**
     * Sets the value of the [manual_entry] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrderItem The current object (for fluent API support)
     */
    public function setManualEntry($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->manual_entry !== $v) {
            $this->manual_entry = $v;
            $this->modifiedColumns[SaleOrderItemTableMap::COL_MANUAL_ENTRY] = true;
        }

        return $this;
    } // setManualEntry()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->manual_entry !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SaleOrderItemTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SaleOrderItemTableMap::translateFieldName('CreatedOn', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_on = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SaleOrderItemTableMap::translateFieldName('SaleOrderId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sale_order_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SaleOrderItemTableMap::translateFieldName('ProductNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->product_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : SaleOrderItemTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : SaleOrderItemTableMap::translateFieldName('Quantity', TableMap::TYPE_PHPNAME, $indexType)];
            $this->quantity = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : SaleOrderItemTableMap::translateFieldName('SingleItemWeight', TableMap::TYPE_PHPNAME, $indexType)];
            $this->single_item_weight = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : SaleOrderItemTableMap::translateFieldName('Price', TableMap::TYPE_PHPNAME, $indexType)];
            $this->price = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : SaleOrderItemTableMap::translateFieldName('VatPercentage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vat_percentage = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : SaleOrderItemTableMap::translateFieldName('ManualEntry', TableMap::TYPE_PHPNAME, $indexType)];
            $this->manual_entry = (null !== $col) ? (boolean) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = SaleOrderItemTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Sale\\SaleOrderItem'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aSaleOrder !== null && $this->sale_order_id !== $this->aSaleOrder->getId()) {
            $this->aSaleOrder = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SaleOrderItemTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSaleOrderItemQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSaleOrder = null;
            $this->collSaleOrderItemProducts = null;

            $this->collSaleOrderItemStockClaimeds = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see SaleOrderItem::setDeleted()
     * @see SaleOrderItem::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderItemTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSaleOrderItemQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderItemTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SaleOrderItemTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSaleOrder !== null) {
                if ($this->aSaleOrder->isModified() || $this->aSaleOrder->isNew()) {
                    $affectedRows += $this->aSaleOrder->save($con);
                }
                $this->setSaleOrder($this->aSaleOrder);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->saleOrderItemProductsScheduledForDeletion !== null) {
                if (!$this->saleOrderItemProductsScheduledForDeletion->isEmpty()) {
                    \Model\Sale\OrderItemProductQuery::create()
                        ->filterByPrimaryKeys($this->saleOrderItemProductsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->saleOrderItemProductsScheduledForDeletion = null;
                }
            }

            if ($this->collSaleOrderItemProducts !== null) {
                foreach ($this->collSaleOrderItemProducts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saleOrderItemStockClaimedsScheduledForDeletion !== null) {
                if (!$this->saleOrderItemStockClaimedsScheduledForDeletion->isEmpty()) {
                    \Model\Sale\SaleOrderItemStockClaimedQuery::create()
                        ->filterByPrimaryKeys($this->saleOrderItemStockClaimedsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->saleOrderItemStockClaimedsScheduledForDeletion = null;
                }
            }

            if ($this->collSaleOrderItemStockClaimeds !== null) {
                foreach ($this->collSaleOrderItemStockClaimeds as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[SaleOrderItemTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SaleOrderItemTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_CREATED_ON)) {
            $modifiedColumns[':p' . $index++]  = 'created_on';
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_SALE_ORDER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'sale_order_id';
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_PRODUCT_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'product_number';
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_QUANTITY)) {
            $modifiedColumns[':p' . $index++]  = 'quantity';
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_SINGLE_ITEM_WEIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'single_item_weight';
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_PRICE)) {
            $modifiedColumns[':p' . $index++]  = 'price';
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_VAT_PERCENTAGE)) {
            $modifiedColumns[':p' . $index++]  = 'vat_percentage';
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_MANUAL_ENTRY)) {
            $modifiedColumns[':p' . $index++]  = 'manual_entry';
        }

        $sql = sprintf(
            'INSERT INTO sale_order_item (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'created_on':
                        $stmt->bindValue($identifier, $this->created_on ? $this->created_on->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'sale_order_id':
                        $stmt->bindValue($identifier, $this->sale_order_id, PDO::PARAM_INT);
                        break;
                    case 'product_number':
                        $stmt->bindValue($identifier, $this->product_number, PDO::PARAM_STR);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'quantity':
                        $stmt->bindValue($identifier, $this->quantity, PDO::PARAM_STR);
                        break;
                    case 'single_item_weight':
                        $stmt->bindValue($identifier, $this->single_item_weight, PDO::PARAM_INT);
                        break;
                    case 'price':
                        $stmt->bindValue($identifier, $this->price, PDO::PARAM_STR);
                        break;
                    case 'vat_percentage':
                        $stmt->bindValue($identifier, $this->vat_percentage, PDO::PARAM_INT);
                        break;
                    case 'manual_entry':
                        $stmt->bindValue($identifier, (int) $this->manual_entry, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SaleOrderItemTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getCreatedOn();
                break;
            case 2:
                return $this->getSaleOrderId();
                break;
            case 3:
                return $this->getProductNumber();
                break;
            case 4:
                return $this->getDescription();
                break;
            case 5:
                return $this->getQuantity();
                break;
            case 6:
                return $this->getSingleItemWeight();
                break;
            case 7:
                return $this->getPrice();
                break;
            case 8:
                return $this->getVatPercentage();
                break;
            case 9:
                return $this->getManualEntry();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['SaleOrderItem'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SaleOrderItem'][$this->hashCode()] = true;
        $keys = SaleOrderItemTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getCreatedOn(),
            $keys[2] => $this->getSaleOrderId(),
            $keys[3] => $this->getProductNumber(),
            $keys[4] => $this->getDescription(),
            $keys[5] => $this->getQuantity(),
            $keys[6] => $this->getSingleItemWeight(),
            $keys[7] => $this->getPrice(),
            $keys[8] => $this->getVatPercentage(),
            $keys[9] => $this->getManualEntry(),
        );
        if ($result[$keys[1]] instanceof \DateTimeInterface) {
            $result[$keys[1]] = $result[$keys[1]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSaleOrder) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'saleOrder';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_order';
                        break;
                    default:
                        $key = 'SaleOrder';
                }

                $result[$key] = $this->aSaleOrder->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSaleOrderItemProducts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'orderItemProducts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_order_item_products';
                        break;
                    default:
                        $key = 'SaleOrderItemProducts';
                }

                $result[$key] = $this->collSaleOrderItemProducts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaleOrderItemStockClaimeds) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'saleOrderItemStockClaimeds';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_order_item_stock_claimeds';
                        break;
                    default:
                        $key = 'SaleOrderItemStockClaimeds';
                }

                $result[$key] = $this->collSaleOrderItemStockClaimeds->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Sale\SaleOrderItem
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SaleOrderItemTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Sale\SaleOrderItem
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setCreatedOn($value);
                break;
            case 2:
                $this->setSaleOrderId($value);
                break;
            case 3:
                $this->setProductNumber($value);
                break;
            case 4:
                $this->setDescription($value);
                break;
            case 5:
                $this->setQuantity($value);
                break;
            case 6:
                $this->setSingleItemWeight($value);
                break;
            case 7:
                $this->setPrice($value);
                break;
            case 8:
                $this->setVatPercentage($value);
                break;
            case 9:
                $this->setManualEntry($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SaleOrderItemTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setCreatedOn($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setSaleOrderId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setProductNumber($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setDescription($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setQuantity($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setSingleItemWeight($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setPrice($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setVatPercentage($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setManualEntry($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Sale\SaleOrderItem The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SaleOrderItemTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SaleOrderItemTableMap::COL_ID)) {
            $criteria->add(SaleOrderItemTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_CREATED_ON)) {
            $criteria->add(SaleOrderItemTableMap::COL_CREATED_ON, $this->created_on);
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_SALE_ORDER_ID)) {
            $criteria->add(SaleOrderItemTableMap::COL_SALE_ORDER_ID, $this->sale_order_id);
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_PRODUCT_NUMBER)) {
            $criteria->add(SaleOrderItemTableMap::COL_PRODUCT_NUMBER, $this->product_number);
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_DESCRIPTION)) {
            $criteria->add(SaleOrderItemTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_QUANTITY)) {
            $criteria->add(SaleOrderItemTableMap::COL_QUANTITY, $this->quantity);
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_SINGLE_ITEM_WEIGHT)) {
            $criteria->add(SaleOrderItemTableMap::COL_SINGLE_ITEM_WEIGHT, $this->single_item_weight);
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_PRICE)) {
            $criteria->add(SaleOrderItemTableMap::COL_PRICE, $this->price);
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_VAT_PERCENTAGE)) {
            $criteria->add(SaleOrderItemTableMap::COL_VAT_PERCENTAGE, $this->vat_percentage);
        }
        if ($this->isColumnModified(SaleOrderItemTableMap::COL_MANUAL_ENTRY)) {
            $criteria->add(SaleOrderItemTableMap::COL_MANUAL_ENTRY, $this->manual_entry);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSaleOrderItemQuery::create();
        $criteria->add(SaleOrderItemTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Sale\SaleOrderItem (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setCreatedOn($this->getCreatedOn());
        $copyObj->setSaleOrderId($this->getSaleOrderId());
        $copyObj->setProductNumber($this->getProductNumber());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setQuantity($this->getQuantity());
        $copyObj->setSingleItemWeight($this->getSingleItemWeight());
        $copyObj->setPrice($this->getPrice());
        $copyObj->setVatPercentage($this->getVatPercentage());
        $copyObj->setManualEntry($this->getManualEntry());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getSaleOrderItemProducts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaleOrderItemProduct($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaleOrderItemStockClaimeds() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaleOrderItemStockClaimed($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Sale\SaleOrderItem Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildSaleOrder object.
     *
     * @param  ChildSaleOrder $v
     * @return $this|\Model\Sale\SaleOrderItem The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSaleOrder(ChildSaleOrder $v = null)
    {
        if ($v === null) {
            $this->setSaleOrderId(NULL);
        } else {
            $this->setSaleOrderId($v->getId());
        }

        $this->aSaleOrder = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSaleOrder object, it will not be re-added.
        if ($v !== null) {
            $v->addSaleOrderItem($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSaleOrder object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSaleOrder The associated ChildSaleOrder object.
     * @throws PropelException
     */
    public function getSaleOrder(ConnectionInterface $con = null)
    {
        if ($this->aSaleOrder === null && ($this->sale_order_id != 0)) {
            $this->aSaleOrder = ChildSaleOrderQuery::create()->findPk($this->sale_order_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSaleOrder->addSaleOrderItems($this);
             */
        }

        return $this->aSaleOrder;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SaleOrderItemProduct' === $relationName) {
            $this->initSaleOrderItemProducts();
            return;
        }
        if ('SaleOrderItemStockClaimed' === $relationName) {
            $this->initSaleOrderItemStockClaimeds();
            return;
        }
    }

    /**
     * Clears out the collSaleOrderItemProducts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSaleOrderItemProducts()
     */
    public function clearSaleOrderItemProducts()
    {
        $this->collSaleOrderItemProducts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSaleOrderItemProducts collection loaded partially.
     */
    public function resetPartialSaleOrderItemProducts($v = true)
    {
        $this->collSaleOrderItemProductsPartial = $v;
    }

    /**
     * Initializes the collSaleOrderItemProducts collection.
     *
     * By default this just sets the collSaleOrderItemProducts collection to an empty array (like clearcollSaleOrderItemProducts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaleOrderItemProducts($overrideExisting = true)
    {
        if (null !== $this->collSaleOrderItemProducts && !$overrideExisting) {
            return;
        }

        $collectionClassName = OrderItemProductTableMap::getTableMap()->getCollectionClassName();

        $this->collSaleOrderItemProducts = new $collectionClassName;
        $this->collSaleOrderItemProducts->setModel('\Model\Sale\OrderItemProduct');
    }

    /**
     * Gets an array of ChildOrderItemProduct objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSaleOrderItem is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildOrderItemProduct[] List of ChildOrderItemProduct objects
     * @throws PropelException
     */
    public function getSaleOrderItemProducts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderItemProductsPartial && !$this->isNew();
        if (null === $this->collSaleOrderItemProducts || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSaleOrderItemProducts) {
                    $this->initSaleOrderItemProducts();
                } else {
                    $collectionClassName = OrderItemProductTableMap::getTableMap()->getCollectionClassName();

                    $collSaleOrderItemProducts = new $collectionClassName;
                    $collSaleOrderItemProducts->setModel('\Model\Sale\OrderItemProduct');

                    return $collSaleOrderItemProducts;
                }
            } else {
                $collSaleOrderItemProducts = ChildOrderItemProductQuery::create(null, $criteria)
                    ->filterByOrderItem($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSaleOrderItemProductsPartial && count($collSaleOrderItemProducts)) {
                        $this->initSaleOrderItemProducts(false);

                        foreach ($collSaleOrderItemProducts as $obj) {
                            if (false == $this->collSaleOrderItemProducts->contains($obj)) {
                                $this->collSaleOrderItemProducts->append($obj);
                            }
                        }

                        $this->collSaleOrderItemProductsPartial = true;
                    }

                    return $collSaleOrderItemProducts;
                }

                if ($partial && $this->collSaleOrderItemProducts) {
                    foreach ($this->collSaleOrderItemProducts as $obj) {
                        if ($obj->isNew()) {
                            $collSaleOrderItemProducts[] = $obj;
                        }
                    }
                }

                $this->collSaleOrderItemProducts = $collSaleOrderItemProducts;
                $this->collSaleOrderItemProductsPartial = false;
            }
        }

        return $this->collSaleOrderItemProducts;
    }

    /**
     * Sets a collection of ChildOrderItemProduct objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $saleOrderItemProducts A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSaleOrderItem The current object (for fluent API support)
     */
    public function setSaleOrderItemProducts(Collection $saleOrderItemProducts, ConnectionInterface $con = null)
    {
        /** @var ChildOrderItemProduct[] $saleOrderItemProductsToDelete */
        $saleOrderItemProductsToDelete = $this->getSaleOrderItemProducts(new Criteria(), $con)->diff($saleOrderItemProducts);


        $this->saleOrderItemProductsScheduledForDeletion = $saleOrderItemProductsToDelete;

        foreach ($saleOrderItemProductsToDelete as $saleOrderItemProductRemoved) {
            $saleOrderItemProductRemoved->setOrderItem(null);
        }

        $this->collSaleOrderItemProducts = null;
        foreach ($saleOrderItemProducts as $saleOrderItemProduct) {
            $this->addSaleOrderItemProduct($saleOrderItemProduct);
        }

        $this->collSaleOrderItemProducts = $saleOrderItemProducts;
        $this->collSaleOrderItemProductsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related OrderItemProduct objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related OrderItemProduct objects.
     * @throws PropelException
     */
    public function countSaleOrderItemProducts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderItemProductsPartial && !$this->isNew();
        if (null === $this->collSaleOrderItemProducts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaleOrderItemProducts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSaleOrderItemProducts());
            }

            $query = ChildOrderItemProductQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByOrderItem($this)
                ->count($con);
        }

        return count($this->collSaleOrderItemProducts);
    }

    /**
     * Method called to associate a ChildOrderItemProduct object to this object
     * through the ChildOrderItemProduct foreign key attribute.
     *
     * @param  ChildOrderItemProduct $l ChildOrderItemProduct
     * @return $this|\Model\Sale\SaleOrderItem The current object (for fluent API support)
     */
    public function addSaleOrderItemProduct(ChildOrderItemProduct $l)
    {
        if ($this->collSaleOrderItemProducts === null) {
            $this->initSaleOrderItemProducts();
            $this->collSaleOrderItemProductsPartial = true;
        }

        if (!$this->collSaleOrderItemProducts->contains($l)) {
            $this->doAddSaleOrderItemProduct($l);

            if ($this->saleOrderItemProductsScheduledForDeletion and $this->saleOrderItemProductsScheduledForDeletion->contains($l)) {
                $this->saleOrderItemProductsScheduledForDeletion->remove($this->saleOrderItemProductsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildOrderItemProduct $saleOrderItemProduct The ChildOrderItemProduct object to add.
     */
    protected function doAddSaleOrderItemProduct(ChildOrderItemProduct $saleOrderItemProduct)
    {
        $this->collSaleOrderItemProducts[]= $saleOrderItemProduct;
        $saleOrderItemProduct->setOrderItem($this);
    }

    /**
     * @param  ChildOrderItemProduct $saleOrderItemProduct The ChildOrderItemProduct object to remove.
     * @return $this|ChildSaleOrderItem The current object (for fluent API support)
     */
    public function removeSaleOrderItemProduct(ChildOrderItemProduct $saleOrderItemProduct)
    {
        if ($this->getSaleOrderItemProducts()->contains($saleOrderItemProduct)) {
            $pos = $this->collSaleOrderItemProducts->search($saleOrderItemProduct);
            $this->collSaleOrderItemProducts->remove($pos);
            if (null === $this->saleOrderItemProductsScheduledForDeletion) {
                $this->saleOrderItemProductsScheduledForDeletion = clone $this->collSaleOrderItemProducts;
                $this->saleOrderItemProductsScheduledForDeletion->clear();
            }
            $this->saleOrderItemProductsScheduledForDeletion[]= clone $saleOrderItemProduct;
            $saleOrderItemProduct->setOrderItem(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SaleOrderItem is new, it will return
     * an empty collection; or if this SaleOrderItem has previously
     * been saved, it will retrieve related SaleOrderItemProducts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SaleOrderItem.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildOrderItemProduct[] List of ChildOrderItemProduct objects
     */
    public function getSaleOrderItemProductsJoinProduct(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildOrderItemProductQuery::create(null, $criteria);
        $query->joinWith('Product', $joinBehavior);

        return $this->getSaleOrderItemProducts($query, $con);
    }

    /**
     * Clears out the collSaleOrderItemStockClaimeds collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSaleOrderItemStockClaimeds()
     */
    public function clearSaleOrderItemStockClaimeds()
    {
        $this->collSaleOrderItemStockClaimeds = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSaleOrderItemStockClaimeds collection loaded partially.
     */
    public function resetPartialSaleOrderItemStockClaimeds($v = true)
    {
        $this->collSaleOrderItemStockClaimedsPartial = $v;
    }

    /**
     * Initializes the collSaleOrderItemStockClaimeds collection.
     *
     * By default this just sets the collSaleOrderItemStockClaimeds collection to an empty array (like clearcollSaleOrderItemStockClaimeds());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaleOrderItemStockClaimeds($overrideExisting = true)
    {
        if (null !== $this->collSaleOrderItemStockClaimeds && !$overrideExisting) {
            return;
        }

        $collectionClassName = SaleOrderItemStockClaimedTableMap::getTableMap()->getCollectionClassName();

        $this->collSaleOrderItemStockClaimeds = new $collectionClassName;
        $this->collSaleOrderItemStockClaimeds->setModel('\Model\Sale\SaleOrderItemStockClaimed');
    }

    /**
     * Gets an array of ChildSaleOrderItemStockClaimed objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSaleOrderItem is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSaleOrderItemStockClaimed[] List of ChildSaleOrderItemStockClaimed objects
     * @throws PropelException
     */
    public function getSaleOrderItemStockClaimeds(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderItemStockClaimedsPartial && !$this->isNew();
        if (null === $this->collSaleOrderItemStockClaimeds || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSaleOrderItemStockClaimeds) {
                    $this->initSaleOrderItemStockClaimeds();
                } else {
                    $collectionClassName = SaleOrderItemStockClaimedTableMap::getTableMap()->getCollectionClassName();

                    $collSaleOrderItemStockClaimeds = new $collectionClassName;
                    $collSaleOrderItemStockClaimeds->setModel('\Model\Sale\SaleOrderItemStockClaimed');

                    return $collSaleOrderItemStockClaimeds;
                }
            } else {
                $collSaleOrderItemStockClaimeds = ChildSaleOrderItemStockClaimedQuery::create(null, $criteria)
                    ->filterBySaleOrderItem($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSaleOrderItemStockClaimedsPartial && count($collSaleOrderItemStockClaimeds)) {
                        $this->initSaleOrderItemStockClaimeds(false);

                        foreach ($collSaleOrderItemStockClaimeds as $obj) {
                            if (false == $this->collSaleOrderItemStockClaimeds->contains($obj)) {
                                $this->collSaleOrderItemStockClaimeds->append($obj);
                            }
                        }

                        $this->collSaleOrderItemStockClaimedsPartial = true;
                    }

                    return $collSaleOrderItemStockClaimeds;
                }

                if ($partial && $this->collSaleOrderItemStockClaimeds) {
                    foreach ($this->collSaleOrderItemStockClaimeds as $obj) {
                        if ($obj->isNew()) {
                            $collSaleOrderItemStockClaimeds[] = $obj;
                        }
                    }
                }

                $this->collSaleOrderItemStockClaimeds = $collSaleOrderItemStockClaimeds;
                $this->collSaleOrderItemStockClaimedsPartial = false;
            }
        }

        return $this->collSaleOrderItemStockClaimeds;
    }

    /**
     * Sets a collection of ChildSaleOrderItemStockClaimed objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $saleOrderItemStockClaimeds A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSaleOrderItem The current object (for fluent API support)
     */
    public function setSaleOrderItemStockClaimeds(Collection $saleOrderItemStockClaimeds, ConnectionInterface $con = null)
    {
        /** @var ChildSaleOrderItemStockClaimed[] $saleOrderItemStockClaimedsToDelete */
        $saleOrderItemStockClaimedsToDelete = $this->getSaleOrderItemStockClaimeds(new Criteria(), $con)->diff($saleOrderItemStockClaimeds);


        $this->saleOrderItemStockClaimedsScheduledForDeletion = $saleOrderItemStockClaimedsToDelete;

        foreach ($saleOrderItemStockClaimedsToDelete as $saleOrderItemStockClaimedRemoved) {
            $saleOrderItemStockClaimedRemoved->setSaleOrderItem(null);
        }

        $this->collSaleOrderItemStockClaimeds = null;
        foreach ($saleOrderItemStockClaimeds as $saleOrderItemStockClaimed) {
            $this->addSaleOrderItemStockClaimed($saleOrderItemStockClaimed);
        }

        $this->collSaleOrderItemStockClaimeds = $saleOrderItemStockClaimeds;
        $this->collSaleOrderItemStockClaimedsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SaleOrderItemStockClaimed objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SaleOrderItemStockClaimed objects.
     * @throws PropelException
     */
    public function countSaleOrderItemStockClaimeds(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderItemStockClaimedsPartial && !$this->isNew();
        if (null === $this->collSaleOrderItemStockClaimeds || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaleOrderItemStockClaimeds) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSaleOrderItemStockClaimeds());
            }

            $query = ChildSaleOrderItemStockClaimedQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySaleOrderItem($this)
                ->count($con);
        }

        return count($this->collSaleOrderItemStockClaimeds);
    }

    /**
     * Method called to associate a ChildSaleOrderItemStockClaimed object to this object
     * through the ChildSaleOrderItemStockClaimed foreign key attribute.
     *
     * @param  ChildSaleOrderItemStockClaimed $l ChildSaleOrderItemStockClaimed
     * @return $this|\Model\Sale\SaleOrderItem The current object (for fluent API support)
     */
    public function addSaleOrderItemStockClaimed(ChildSaleOrderItemStockClaimed $l)
    {
        if ($this->collSaleOrderItemStockClaimeds === null) {
            $this->initSaleOrderItemStockClaimeds();
            $this->collSaleOrderItemStockClaimedsPartial = true;
        }

        if (!$this->collSaleOrderItemStockClaimeds->contains($l)) {
            $this->doAddSaleOrderItemStockClaimed($l);

            if ($this->saleOrderItemStockClaimedsScheduledForDeletion and $this->saleOrderItemStockClaimedsScheduledForDeletion->contains($l)) {
                $this->saleOrderItemStockClaimedsScheduledForDeletion->remove($this->saleOrderItemStockClaimedsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSaleOrderItemStockClaimed $saleOrderItemStockClaimed The ChildSaleOrderItemStockClaimed object to add.
     */
    protected function doAddSaleOrderItemStockClaimed(ChildSaleOrderItemStockClaimed $saleOrderItemStockClaimed)
    {
        $this->collSaleOrderItemStockClaimeds[]= $saleOrderItemStockClaimed;
        $saleOrderItemStockClaimed->setSaleOrderItem($this);
    }

    /**
     * @param  ChildSaleOrderItemStockClaimed $saleOrderItemStockClaimed The ChildSaleOrderItemStockClaimed object to remove.
     * @return $this|ChildSaleOrderItem The current object (for fluent API support)
     */
    public function removeSaleOrderItemStockClaimed(ChildSaleOrderItemStockClaimed $saleOrderItemStockClaimed)
    {
        if ($this->getSaleOrderItemStockClaimeds()->contains($saleOrderItemStockClaimed)) {
            $pos = $this->collSaleOrderItemStockClaimeds->search($saleOrderItemStockClaimed);
            $this->collSaleOrderItemStockClaimeds->remove($pos);
            if (null === $this->saleOrderItemStockClaimedsScheduledForDeletion) {
                $this->saleOrderItemStockClaimedsScheduledForDeletion = clone $this->collSaleOrderItemStockClaimeds;
                $this->saleOrderItemStockClaimedsScheduledForDeletion->clear();
            }
            $this->saleOrderItemStockClaimedsScheduledForDeletion[]= clone $saleOrderItemStockClaimed;
            $saleOrderItemStockClaimed->setSaleOrderItem(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SaleOrderItem is new, it will return
     * an empty collection; or if this SaleOrderItem has previously
     * been saved, it will retrieve related SaleOrderItemStockClaimeds from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SaleOrderItem.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSaleOrderItemStockClaimed[] List of ChildSaleOrderItemStockClaimed objects
     */
    public function getSaleOrderItemStockClaimedsJoinProduct(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSaleOrderItemStockClaimedQuery::create(null, $criteria);
        $query->joinWith('Product', $joinBehavior);

        return $this->getSaleOrderItemStockClaimeds($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aSaleOrder) {
            $this->aSaleOrder->removeSaleOrderItem($this);
        }
        $this->id = null;
        $this->created_on = null;
        $this->sale_order_id = null;
        $this->product_number = null;
        $this->description = null;
        $this->quantity = null;
        $this->single_item_weight = null;
        $this->price = null;
        $this->vat_percentage = null;
        $this->manual_entry = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSaleOrderItemProducts) {
                foreach ($this->collSaleOrderItemProducts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaleOrderItemStockClaimeds) {
                foreach ($this->collSaleOrderItemStockClaimeds as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collSaleOrderItemProducts = null;
        $this->collSaleOrderItemStockClaimeds = null;
        $this->aSaleOrder = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SaleOrderItemTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
