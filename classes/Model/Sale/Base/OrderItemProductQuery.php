<?php

namespace Model\Sale\Base;

use \Exception;
use \PDO;
use Model\Product;
use Model\Sale\OrderItemProduct as ChildOrderItemProduct;
use Model\Sale\OrderItemProductQuery as ChildOrderItemProductQuery;
use Model\Sale\Map\OrderItemProductTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'sale_order_item_product' table.
 *
 *
 *
 * @method     ChildOrderItemProductQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildOrderItemProductQuery orderBySaleOrderItemId($order = Criteria::ASC) Order by the sale_order_item_id column
 * @method     ChildOrderItemProductQuery orderByProductId($order = Criteria::ASC) Order by the product_id column
 * @method     ChildOrderItemProductQuery orderBySalePrice($order = Criteria::ASC) Order by the sale_price column
 * @method     ChildOrderItemProductQuery orderByWidth($order = Criteria::ASC) Order by the width column
 * @method     ChildOrderItemProductQuery orderByWeight($order = Criteria::ASC) Order by the weight column
 * @method     ChildOrderItemProductQuery orderByHeight($order = Criteria::ASC) Order by the height column
 * @method     ChildOrderItemProductQuery orderByThickness($order = Criteria::ASC) Order by the thickness column
 * @method     ChildOrderItemProductQuery orderByCategoryName($order = Criteria::ASC) Order by the category_name column
 * @method     ChildOrderItemProductQuery orderByMaterialName($order = Criteria::ASC) Order by the material_name column
 *
 * @method     ChildOrderItemProductQuery groupById() Group by the id column
 * @method     ChildOrderItemProductQuery groupBySaleOrderItemId() Group by the sale_order_item_id column
 * @method     ChildOrderItemProductQuery groupByProductId() Group by the product_id column
 * @method     ChildOrderItemProductQuery groupBySalePrice() Group by the sale_price column
 * @method     ChildOrderItemProductQuery groupByWidth() Group by the width column
 * @method     ChildOrderItemProductQuery groupByWeight() Group by the weight column
 * @method     ChildOrderItemProductQuery groupByHeight() Group by the height column
 * @method     ChildOrderItemProductQuery groupByThickness() Group by the thickness column
 * @method     ChildOrderItemProductQuery groupByCategoryName() Group by the category_name column
 * @method     ChildOrderItemProductQuery groupByMaterialName() Group by the material_name column
 *
 * @method     ChildOrderItemProductQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildOrderItemProductQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildOrderItemProductQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildOrderItemProductQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildOrderItemProductQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildOrderItemProductQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildOrderItemProductQuery leftJoinOrderItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the OrderItem relation
 * @method     ChildOrderItemProductQuery rightJoinOrderItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OrderItem relation
 * @method     ChildOrderItemProductQuery innerJoinOrderItem($relationAlias = null) Adds a INNER JOIN clause to the query using the OrderItem relation
 *
 * @method     ChildOrderItemProductQuery joinWithOrderItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OrderItem relation
 *
 * @method     ChildOrderItemProductQuery leftJoinWithOrderItem() Adds a LEFT JOIN clause and with to the query using the OrderItem relation
 * @method     ChildOrderItemProductQuery rightJoinWithOrderItem() Adds a RIGHT JOIN clause and with to the query using the OrderItem relation
 * @method     ChildOrderItemProductQuery innerJoinWithOrderItem() Adds a INNER JOIN clause and with to the query using the OrderItem relation
 *
 * @method     ChildOrderItemProductQuery leftJoinProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the Product relation
 * @method     ChildOrderItemProductQuery rightJoinProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Product relation
 * @method     ChildOrderItemProductQuery innerJoinProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the Product relation
 *
 * @method     ChildOrderItemProductQuery joinWithProduct($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Product relation
 *
 * @method     ChildOrderItemProductQuery leftJoinWithProduct() Adds a LEFT JOIN clause and with to the query using the Product relation
 * @method     ChildOrderItemProductQuery rightJoinWithProduct() Adds a RIGHT JOIN clause and with to the query using the Product relation
 * @method     ChildOrderItemProductQuery innerJoinWithProduct() Adds a INNER JOIN clause and with to the query using the Product relation
 *
 * @method     \Model\Sale\SaleOrderItemQuery|\Model\ProductQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildOrderItemProduct findOne(ConnectionInterface $con = null) Return the first ChildOrderItemProduct matching the query
 * @method     ChildOrderItemProduct findOneOrCreate(ConnectionInterface $con = null) Return the first ChildOrderItemProduct matching the query, or a new ChildOrderItemProduct object populated from the query conditions when no match is found
 *
 * @method     ChildOrderItemProduct findOneById(int $id) Return the first ChildOrderItemProduct filtered by the id column
 * @method     ChildOrderItemProduct findOneBySaleOrderItemId(int $sale_order_item_id) Return the first ChildOrderItemProduct filtered by the sale_order_item_id column
 * @method     ChildOrderItemProduct findOneByProductId(int $product_id) Return the first ChildOrderItemProduct filtered by the product_id column
 * @method     ChildOrderItemProduct findOneBySalePrice(double $sale_price) Return the first ChildOrderItemProduct filtered by the sale_price column
 * @method     ChildOrderItemProduct findOneByWidth(double $width) Return the first ChildOrderItemProduct filtered by the width column
 * @method     ChildOrderItemProduct findOneByWeight(double $weight) Return the first ChildOrderItemProduct filtered by the weight column
 * @method     ChildOrderItemProduct findOneByHeight(double $height) Return the first ChildOrderItemProduct filtered by the height column
 * @method     ChildOrderItemProduct findOneByThickness(double $thickness) Return the first ChildOrderItemProduct filtered by the thickness column
 * @method     ChildOrderItemProduct findOneByCategoryName(string $category_name) Return the first ChildOrderItemProduct filtered by the category_name column
 * @method     ChildOrderItemProduct findOneByMaterialName(string $material_name) Return the first ChildOrderItemProduct filtered by the material_name column *

 * @method     ChildOrderItemProduct requirePk($key, ConnectionInterface $con = null) Return the ChildOrderItemProduct by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOrderItemProduct requireOne(ConnectionInterface $con = null) Return the first ChildOrderItemProduct matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOrderItemProduct requireOneById(int $id) Return the first ChildOrderItemProduct filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOrderItemProduct requireOneBySaleOrderItemId(int $sale_order_item_id) Return the first ChildOrderItemProduct filtered by the sale_order_item_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOrderItemProduct requireOneByProductId(int $product_id) Return the first ChildOrderItemProduct filtered by the product_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOrderItemProduct requireOneBySalePrice(double $sale_price) Return the first ChildOrderItemProduct filtered by the sale_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOrderItemProduct requireOneByWidth(double $width) Return the first ChildOrderItemProduct filtered by the width column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOrderItemProduct requireOneByWeight(double $weight) Return the first ChildOrderItemProduct filtered by the weight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOrderItemProduct requireOneByHeight(double $height) Return the first ChildOrderItemProduct filtered by the height column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOrderItemProduct requireOneByThickness(double $thickness) Return the first ChildOrderItemProduct filtered by the thickness column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOrderItemProduct requireOneByCategoryName(string $category_name) Return the first ChildOrderItemProduct filtered by the category_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildOrderItemProduct requireOneByMaterialName(string $material_name) Return the first ChildOrderItemProduct filtered by the material_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildOrderItemProduct[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildOrderItemProduct objects based on current ModelCriteria
 * @method     ChildOrderItemProduct[]|ObjectCollection findById(int $id) Return ChildOrderItemProduct objects filtered by the id column
 * @method     ChildOrderItemProduct[]|ObjectCollection findBySaleOrderItemId(int $sale_order_item_id) Return ChildOrderItemProduct objects filtered by the sale_order_item_id column
 * @method     ChildOrderItemProduct[]|ObjectCollection findByProductId(int $product_id) Return ChildOrderItemProduct objects filtered by the product_id column
 * @method     ChildOrderItemProduct[]|ObjectCollection findBySalePrice(double $sale_price) Return ChildOrderItemProduct objects filtered by the sale_price column
 * @method     ChildOrderItemProduct[]|ObjectCollection findByWidth(double $width) Return ChildOrderItemProduct objects filtered by the width column
 * @method     ChildOrderItemProduct[]|ObjectCollection findByWeight(double $weight) Return ChildOrderItemProduct objects filtered by the weight column
 * @method     ChildOrderItemProduct[]|ObjectCollection findByHeight(double $height) Return ChildOrderItemProduct objects filtered by the height column
 * @method     ChildOrderItemProduct[]|ObjectCollection findByThickness(double $thickness) Return ChildOrderItemProduct objects filtered by the thickness column
 * @method     ChildOrderItemProduct[]|ObjectCollection findByCategoryName(string $category_name) Return ChildOrderItemProduct objects filtered by the category_name column
 * @method     ChildOrderItemProduct[]|ObjectCollection findByMaterialName(string $material_name) Return ChildOrderItemProduct objects filtered by the material_name column
 * @method     ChildOrderItemProduct[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class OrderItemProductQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Sale\Base\OrderItemProductQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Sale\\OrderItemProduct', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildOrderItemProductQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildOrderItemProductQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildOrderItemProductQuery) {
            return $criteria;
        }
        $query = new ChildOrderItemProductQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildOrderItemProduct|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(OrderItemProductTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = OrderItemProductTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOrderItemProduct A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, sale_order_item_id, product_id, sale_price, width, weight, height, thickness, category_name, material_name FROM sale_order_item_product WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildOrderItemProduct $obj */
            $obj = new ChildOrderItemProduct();
            $obj->hydrate($row);
            OrderItemProductTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildOrderItemProduct|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(OrderItemProductTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(OrderItemProductTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OrderItemProductTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the sale_order_item_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySaleOrderItemId(1234); // WHERE sale_order_item_id = 1234
     * $query->filterBySaleOrderItemId(array(12, 34)); // WHERE sale_order_item_id IN (12, 34)
     * $query->filterBySaleOrderItemId(array('min' => 12)); // WHERE sale_order_item_id > 12
     * </code>
     *
     * @see       filterByOrderItem()
     *
     * @param     mixed $saleOrderItemId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterBySaleOrderItemId($saleOrderItemId = null, $comparison = null)
    {
        if (is_array($saleOrderItemId)) {
            $useMinMax = false;
            if (isset($saleOrderItemId['min'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_SALE_ORDER_ITEM_ID, $saleOrderItemId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($saleOrderItemId['max'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_SALE_ORDER_ITEM_ID, $saleOrderItemId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OrderItemProductTableMap::COL_SALE_ORDER_ITEM_ID, $saleOrderItemId, $comparison);
    }

    /**
     * Filter the query on the product_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProductId(1234); // WHERE product_id = 1234
     * $query->filterByProductId(array(12, 34)); // WHERE product_id IN (12, 34)
     * $query->filterByProductId(array('min' => 12)); // WHERE product_id > 12
     * </code>
     *
     * @see       filterByProduct()
     *
     * @param     mixed $productId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterByProductId($productId = null, $comparison = null)
    {
        if (is_array($productId)) {
            $useMinMax = false;
            if (isset($productId['min'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_PRODUCT_ID, $productId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productId['max'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_PRODUCT_ID, $productId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OrderItemProductTableMap::COL_PRODUCT_ID, $productId, $comparison);
    }

    /**
     * Filter the query on the sale_price column
     *
     * Example usage:
     * <code>
     * $query->filterBySalePrice(1234); // WHERE sale_price = 1234
     * $query->filterBySalePrice(array(12, 34)); // WHERE sale_price IN (12, 34)
     * $query->filterBySalePrice(array('min' => 12)); // WHERE sale_price > 12
     * </code>
     *
     * @param     mixed $salePrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterBySalePrice($salePrice = null, $comparison = null)
    {
        if (is_array($salePrice)) {
            $useMinMax = false;
            if (isset($salePrice['min'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_SALE_PRICE, $salePrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($salePrice['max'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_SALE_PRICE, $salePrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OrderItemProductTableMap::COL_SALE_PRICE, $salePrice, $comparison);
    }

    /**
     * Filter the query on the width column
     *
     * Example usage:
     * <code>
     * $query->filterByWidth(1234); // WHERE width = 1234
     * $query->filterByWidth(array(12, 34)); // WHERE width IN (12, 34)
     * $query->filterByWidth(array('min' => 12)); // WHERE width > 12
     * </code>
     *
     * @param     mixed $width The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterByWidth($width = null, $comparison = null)
    {
        if (is_array($width)) {
            $useMinMax = false;
            if (isset($width['min'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_WIDTH, $width['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($width['max'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_WIDTH, $width['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OrderItemProductTableMap::COL_WIDTH, $width, $comparison);
    }

    /**
     * Filter the query on the weight column
     *
     * Example usage:
     * <code>
     * $query->filterByWeight(1234); // WHERE weight = 1234
     * $query->filterByWeight(array(12, 34)); // WHERE weight IN (12, 34)
     * $query->filterByWeight(array('min' => 12)); // WHERE weight > 12
     * </code>
     *
     * @param     mixed $weight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterByWeight($weight = null, $comparison = null)
    {
        if (is_array($weight)) {
            $useMinMax = false;
            if (isset($weight['min'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_WEIGHT, $weight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($weight['max'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_WEIGHT, $weight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OrderItemProductTableMap::COL_WEIGHT, $weight, $comparison);
    }

    /**
     * Filter the query on the height column
     *
     * Example usage:
     * <code>
     * $query->filterByHeight(1234); // WHERE height = 1234
     * $query->filterByHeight(array(12, 34)); // WHERE height IN (12, 34)
     * $query->filterByHeight(array('min' => 12)); // WHERE height > 12
     * </code>
     *
     * @param     mixed $height The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterByHeight($height = null, $comparison = null)
    {
        if (is_array($height)) {
            $useMinMax = false;
            if (isset($height['min'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_HEIGHT, $height['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($height['max'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_HEIGHT, $height['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OrderItemProductTableMap::COL_HEIGHT, $height, $comparison);
    }

    /**
     * Filter the query on the thickness column
     *
     * Example usage:
     * <code>
     * $query->filterByThickness(1234); // WHERE thickness = 1234
     * $query->filterByThickness(array(12, 34)); // WHERE thickness IN (12, 34)
     * $query->filterByThickness(array('min' => 12)); // WHERE thickness > 12
     * </code>
     *
     * @param     mixed $thickness The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterByThickness($thickness = null, $comparison = null)
    {
        if (is_array($thickness)) {
            $useMinMax = false;
            if (isset($thickness['min'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_THICKNESS, $thickness['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($thickness['max'])) {
                $this->addUsingAlias(OrderItemProductTableMap::COL_THICKNESS, $thickness['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OrderItemProductTableMap::COL_THICKNESS, $thickness, $comparison);
    }

    /**
     * Filter the query on the category_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryName('fooValue');   // WHERE category_name = 'fooValue'
     * $query->filterByCategoryName('%fooValue%', Criteria::LIKE); // WHERE category_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $categoryName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterByCategoryName($categoryName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($categoryName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OrderItemProductTableMap::COL_CATEGORY_NAME, $categoryName, $comparison);
    }

    /**
     * Filter the query on the material_name column
     *
     * Example usage:
     * <code>
     * $query->filterByMaterialName('fooValue');   // WHERE material_name = 'fooValue'
     * $query->filterByMaterialName('%fooValue%', Criteria::LIKE); // WHERE material_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $materialName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterByMaterialName($materialName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($materialName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(OrderItemProductTableMap::COL_MATERIAL_NAME, $materialName, $comparison);
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrderItem object
     *
     * @param \Model\Sale\SaleOrderItem|ObjectCollection $saleOrderItem The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterByOrderItem($saleOrderItem, $comparison = null)
    {
        if ($saleOrderItem instanceof \Model\Sale\SaleOrderItem) {
            return $this
                ->addUsingAlias(OrderItemProductTableMap::COL_SALE_ORDER_ITEM_ID, $saleOrderItem->getId(), $comparison);
        } elseif ($saleOrderItem instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OrderItemProductTableMap::COL_SALE_ORDER_ITEM_ID, $saleOrderItem->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByOrderItem() only accepts arguments of type \Model\Sale\SaleOrderItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OrderItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function joinOrderItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OrderItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OrderItem');
        }

        return $this;
    }

    /**
     * Use the OrderItem relation SaleOrderItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderItemQuery A secondary query class using the current class as primary query
     */
    public function useOrderItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinOrderItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OrderItem', '\Model\Sale\SaleOrderItemQuery');
    }

    /**
     * Filter the query by a related \Model\Product object
     *
     * @param \Model\Product|ObjectCollection $product The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function filterByProduct($product, $comparison = null)
    {
        if ($product instanceof \Model\Product) {
            return $this
                ->addUsingAlias(OrderItemProductTableMap::COL_PRODUCT_ID, $product->getId(), $comparison);
        } elseif ($product instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(OrderItemProductTableMap::COL_PRODUCT_ID, $product->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProduct() only accepts arguments of type \Model\Product or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Product relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function joinProduct($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Product');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Product');
        }

        return $this;
    }

    /**
     * Use the Product relation Product object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ProductQuery A secondary query class using the current class as primary query
     */
    public function useProductQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Product', '\Model\ProductQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildOrderItemProduct $orderItemProduct Object to remove from the list of results
     *
     * @return $this|ChildOrderItemProductQuery The current query, for fluid interface
     */
    public function prune($orderItemProduct = null)
    {
        if ($orderItemProduct) {
            $this->addUsingAlias(OrderItemProductTableMap::COL_ID, $orderItemProduct->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the sale_order_item_product table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OrderItemProductTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            OrderItemProductTableMap::clearInstancePool();
            OrderItemProductTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(OrderItemProductTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(OrderItemProductTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            OrderItemProductTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            OrderItemProductTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // OrderItemProductQuery
