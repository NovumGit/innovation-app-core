<?php

namespace Model\Sale\Base;

use \Exception;
use \PDO;
use Model\Account\User;
use Model\Cms\Site;
use Model\Finance\Payment;
use Model\Sale\SaleOrder as ChildSaleOrder;
use Model\Sale\SaleOrderQuery as ChildSaleOrderQuery;
use Model\SaleOrderNotification\SaleOrderNotification;
use Model\Sale\Map\SaleOrderTableMap;
use Model\Setting\MasterTable\SaleOrderStatus;
use Model\Setting\MasterTable\ShippingMethod;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'sale_order' table.
 *
 *
 *
 * @method     ChildSaleOrderQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSaleOrderQuery orderBySiteId($order = Criteria::ASC) Order by the site_id column
 * @method     ChildSaleOrderQuery orderByLanguageId($order = Criteria::ASC) Order by the language_id column
 * @method     ChildSaleOrderQuery orderByIsClosed($order = Criteria::ASC) Order by the is_closed column
 * @method     ChildSaleOrderQuery orderByIsFullyPaid($order = Criteria::ASC) Order by the is_fully_paid column
 * @method     ChildSaleOrderQuery orderByPayDate($order = Criteria::ASC) Order by the pay_date column
 * @method     ChildSaleOrderQuery orderByIsReadyForShipping($order = Criteria::ASC) Order by the is_ready_for_shipping column
 * @method     ChildSaleOrderQuery orderByIsShippingChecked($order = Criteria::ASC) Order by the is_shipping_checked column
 * @method     ChildSaleOrderQuery orderByIsFullyShipped($order = Criteria::ASC) Order by the is_fully_shipped column
 * @method     ChildSaleOrderQuery orderByIsPushedToAccounting($order = Criteria::ASC) Order by the is_pushed_to_accounting column
 * @method     ChildSaleOrderQuery orderByShipDate($order = Criteria::ASC) Order by the ship_date column
 * @method     ChildSaleOrderQuery orderBySaleOrderStatusId($order = Criteria::ASC) Order by the sale_order_status_id column
 * @method     ChildSaleOrderQuery orderByHandledBy($order = Criteria::ASC) Order by the handled_by column
 * @method     ChildSaleOrderQuery orderBySource($order = Criteria::ASC) Order by the source column
 * @method     ChildSaleOrderQuery orderByIsCompleted($order = Criteria::ASC) Order by the is_completed column
 * @method     ChildSaleOrderQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the created_by_user_id column
 * @method     ChildSaleOrderQuery orderByCreatedOn($order = Criteria::ASC) Order by the created_on column
 * @method     ChildSaleOrderQuery orderByEstimatedDeliveryDate($order = Criteria::ASC) Order by the estimated_delivery_date column
 * @method     ChildSaleOrderQuery orderByCustomerId($order = Criteria::ASC) Order by the customer_id column
 * @method     ChildSaleOrderQuery orderByEnvelopeWeightGrams($order = Criteria::ASC) Order by the envelope_weight_grams column
 * @method     ChildSaleOrderQuery orderByWebshopUserSessionId($order = Criteria::ASC) Order by the webshop_user_session_id column
 * @method     ChildSaleOrderQuery orderByInvoiceNumber($order = Criteria::ASC) Order by the invoice_number column
 * @method     ChildSaleOrderQuery orderByInvoiceDate($order = Criteria::ASC) Order by the invoice_date column
 * @method     ChildSaleOrderQuery orderByPaytermDays($order = Criteria::ASC) Order by the payterm_days column
 * @method     ChildSaleOrderQuery orderByPaytermOriginalId($order = Criteria::ASC) Order by the payterm_original_id column
 * @method     ChildSaleOrderQuery orderByPaytermString($order = Criteria::ASC) Order by the payterm_string column
 * @method     ChildSaleOrderQuery orderByOrderNumber($order = Criteria::ASC) Order by the order_number column
 * @method     ChildSaleOrderQuery orderByItemDeleted($order = Criteria::ASC) Order by the is_deleted column
 * @method     ChildSaleOrderQuery orderByCompanyName($order = Criteria::ASC) Order by the company_name column
 * @method     ChildSaleOrderQuery orderByDebitor($order = Criteria::ASC) Order by the debitor column
 * @method     ChildSaleOrderQuery orderByOurCustomFolder($order = Criteria::ASC) Order by the our_custom_folder column
 * @method     ChildSaleOrderQuery orderByOurSlogan($order = Criteria::ASC) Order by the our_slogan column
 * @method     ChildSaleOrderQuery orderByOurPhone($order = Criteria::ASC) Order by the our_phone column
 * @method     ChildSaleOrderQuery orderByOurFax($order = Criteria::ASC) Order by the our_fax column
 * @method     ChildSaleOrderQuery orderByOurWebsite($order = Criteria::ASC) Order by the our_website column
 * @method     ChildSaleOrderQuery orderByOurEmail($order = Criteria::ASC) Order by the our_email column
 * @method     ChildSaleOrderQuery orderByOurVatNumber($order = Criteria::ASC) Order by the our_vat_number column
 * @method     ChildSaleOrderQuery orderByOurChamberOfCommerce($order = Criteria::ASC) Order by the our_chamber_of_commerce column
 * @method     ChildSaleOrderQuery orderByOurIbanNumber($order = Criteria::ASC) Order by the our_iban_number column
 * @method     ChildSaleOrderQuery orderByOurBicNumber($order = Criteria::ASC) Order by the our_bic_number column
 * @method     ChildSaleOrderQuery orderByOurGeneralCompanyName($order = Criteria::ASC) Order by the our_general_company_name column
 * @method     ChildSaleOrderQuery orderByOurGeneralStreet($order = Criteria::ASC) Order by the our_general_street column
 * @method     ChildSaleOrderQuery orderByOurGeneralNumber($order = Criteria::ASC) Order by the our_general_number column
 * @method     ChildSaleOrderQuery orderByOurGeneralNumberAdd($order = Criteria::ASC) Order by the our_general_number_add column
 * @method     ChildSaleOrderQuery orderByOurGeneralPostal($order = Criteria::ASC) Order by the our_general_postal column
 * @method     ChildSaleOrderQuery orderByOurGeneralPoBox($order = Criteria::ASC) Order by the our_general_po_box column
 * @method     ChildSaleOrderQuery orderByOurGeneralCity($order = Criteria::ASC) Order by the our_general_city column
 * @method     ChildSaleOrderQuery orderByOurGeneralCountry($order = Criteria::ASC) Order by the our_general_country column
 * @method     ChildSaleOrderQuery orderByCustomerPhone($order = Criteria::ASC) Order by the customer_phone column
 * @method     ChildSaleOrderQuery orderByCustomerFax($order = Criteria::ASC) Order by the customer_fax column
 * @method     ChildSaleOrderQuery orderByCustomerEmail($order = Criteria::ASC) Order by the customer_email column
 * @method     ChildSaleOrderQuery orderByCustomerChamberOfCommerce($order = Criteria::ASC) Order by the customer_chamber_of_commerce column
 * @method     ChildSaleOrderQuery orderByCustomerVatNumber($order = Criteria::ASC) Order by the customer_vat_number column
 * @method     ChildSaleOrderQuery orderByCustomerOrderReference($order = Criteria::ASC) Order by the customer_order_reference column
 * @method     ChildSaleOrderQuery orderByCustomerOrderPlacedBy($order = Criteria::ASC) Order by the customer_order_placed_by column
 * @method     ChildSaleOrderQuery orderByCustomerInvoiceAddressId($order = Criteria::ASC) Order by the customer_invoice_address_id column
 * @method     ChildSaleOrderQuery orderByCustomerInvoiceCompanyName($order = Criteria::ASC) Order by the customer_invoice_company_name column
 * @method     ChildSaleOrderQuery orderByCustomerInvoiceAttnName($order = Criteria::ASC) Order by the customer_invoice_attn_name column
 * @method     ChildSaleOrderQuery orderByCustomerInvoiceStreet($order = Criteria::ASC) Order by the customer_invoice_street column
 * @method     ChildSaleOrderQuery orderByCustomerInvoiceNumber($order = Criteria::ASC) Order by the customer_invoice_number column
 * @method     ChildSaleOrderQuery orderByCustomerInvoiceNumberAdd($order = Criteria::ASC) Order by the customer_invoice_number_add column
 * @method     ChildSaleOrderQuery orderByCustomerInvoicePostal($order = Criteria::ASC) Order by the customer_invoice_postal column
 * @method     ChildSaleOrderQuery orderByCustomerInvoiceCity($order = Criteria::ASC) Order by the customer_invoice_city column
 * @method     ChildSaleOrderQuery orderByCustomerInvoiceCountry($order = Criteria::ASC) Order by the customer_invoice_country column
 * @method     ChildSaleOrderQuery orderByCustomerInvoiceUsaState($order = Criteria::ASC) Order by the customer_invoice_usa_state column
 * @method     ChildSaleOrderQuery orderByCustomerInvoiceAddressL1($order = Criteria::ASC) Order by the customer_invoice_address_l1 column
 * @method     ChildSaleOrderQuery orderByCustomerInvoiceAddressL2($order = Criteria::ASC) Order by the customer_invoice_address_l2 column
 * @method     ChildSaleOrderQuery orderByCustomerDeliveryAddressId($order = Criteria::ASC) Order by the customer_delivery_address_id column
 * @method     ChildSaleOrderQuery orderByCustomerDeliveryCompanyName($order = Criteria::ASC) Order by the customer_delivery_company_name column
 * @method     ChildSaleOrderQuery orderByCustomerDeliveryAttnName($order = Criteria::ASC) Order by the customer_delivery_attn_name column
 * @method     ChildSaleOrderQuery orderByCustomerDeliveryStreet($order = Criteria::ASC) Order by the customer_delivery_street column
 * @method     ChildSaleOrderQuery orderByCustomerDeliveryNumber($order = Criteria::ASC) Order by the customer_delivery_number column
 * @method     ChildSaleOrderQuery orderByCustomerDeliveryNumberAdd($order = Criteria::ASC) Order by the customer_delivery_number_add column
 * @method     ChildSaleOrderQuery orderByCustomerDeliveryPostal($order = Criteria::ASC) Order by the customer_delivery_postal column
 * @method     ChildSaleOrderQuery orderByCustomerDeliveryCity($order = Criteria::ASC) Order by the customer_delivery_city column
 * @method     ChildSaleOrderQuery orderByCustomerDeliveryCountry($order = Criteria::ASC) Order by the customer_delivery_country column
 * @method     ChildSaleOrderQuery orderByCustomerDeliveryUsaState($order = Criteria::ASC) Order by the customer_delivery_usa_state column
 * @method     ChildSaleOrderQuery orderByCustomerDeliveryAddressL1($order = Criteria::ASC) Order by the customer_delivery_address_l1 column
 * @method     ChildSaleOrderQuery orderByCustomerDeliveryAddressL2($order = Criteria::ASC) Order by the customer_delivery_address_l2 column
 * @method     ChildSaleOrderQuery orderByCpFirstName($order = Criteria::ASC) Order by the cp_first_name column
 * @method     ChildSaleOrderQuery orderByCpLastName($order = Criteria::ASC) Order by the cp_last_name column
 * @method     ChildSaleOrderQuery orderByCpSallutation($order = Criteria::ASC) Order by the cp_sallutation column
 * @method     ChildSaleOrderQuery orderByReference($order = Criteria::ASC) Order by the reference column
 * @method     ChildSaleOrderQuery orderByShippingNote($order = Criteria::ASC) Order by the shipping_note column
 * @method     ChildSaleOrderQuery orderByCustomerOrderNote($order = Criteria::ASC) Order by the customer_order_note column
 * @method     ChildSaleOrderQuery orderByShippingOrderNote($order = Criteria::ASC) Order by the shipping_order_note column
 * @method     ChildSaleOrderQuery orderByHasWrap($order = Criteria::ASC) Order by the has_wrap column
 * @method     ChildSaleOrderQuery orderByWrappingPrice($order = Criteria::ASC) Order by the wrapping_price column
 * @method     ChildSaleOrderQuery orderByShippingCarrier($order = Criteria::ASC) Order by the shipping_carrier column
 * @method     ChildSaleOrderQuery orderByShippingPrice($order = Criteria::ASC) Order by the shipping_price column
 * @method     ChildSaleOrderQuery orderByShippingMethodId($order = Criteria::ASC) Order by the shipping_method_id column
 * @method     ChildSaleOrderQuery orderByShippingHasTrackTrace($order = Criteria::ASC) Order by the shipping_has_track_trace column
 * @method     ChildSaleOrderQuery orderByShippingId($order = Criteria::ASC) Order by the shipping_id column
 * @method     ChildSaleOrderQuery orderByTrackingCode($order = Criteria::ASC) Order by the tracking_code column
 * @method     ChildSaleOrderQuery orderByTrackingUrl($order = Criteria::ASC) Order by the tracking_url column
 * @method     ChildSaleOrderQuery orderByPaymethodName($order = Criteria::ASC) Order by the paymethod_name column
 * @method     ChildSaleOrderQuery orderByPaymethodCode($order = Criteria::ASC) Order by the paymethod_code column
 * @method     ChildSaleOrderQuery orderByPaymethodId($order = Criteria::ASC) Order by the paymethod_id column
 * @method     ChildSaleOrderQuery orderByShippingRegistrationDate($order = Criteria::ASC) Order by the shipping_registration_date column
 * @method     ChildSaleOrderQuery orderByInvoiceByPostal($order = Criteria::ASC) Order by the invoice_by_postal column
 * @method     ChildSaleOrderQuery orderByPackingSlipType($order = Criteria::ASC) Order by the packing_slip_type column
 * @method     ChildSaleOrderQuery orderByWorkInProgress($order = Criteria::ASC) Order by the work_in_progress column
 * @method     ChildSaleOrderQuery orderByPackageAmount($order = Criteria::ASC) Order by the package_amount column
 * @method     ChildSaleOrderQuery orderByCouponCode($order = Criteria::ASC) Order by the coupon_code column
 * @method     ChildSaleOrderQuery orderByCouponDiscountAmount($order = Criteria::ASC) Order by the coupon_discount_amount column
 * @method     ChildSaleOrderQuery orderByCouponDiscountPercentage($order = Criteria::ASC) Order by the coupon_discount_percentage column
 * @method     ChildSaleOrderQuery orderByHideUntill($order = Criteria::ASC) Order by the hide_untill column
 *
 * @method     ChildSaleOrderQuery groupById() Group by the id column
 * @method     ChildSaleOrderQuery groupBySiteId() Group by the site_id column
 * @method     ChildSaleOrderQuery groupByLanguageId() Group by the language_id column
 * @method     ChildSaleOrderQuery groupByIsClosed() Group by the is_closed column
 * @method     ChildSaleOrderQuery groupByIsFullyPaid() Group by the is_fully_paid column
 * @method     ChildSaleOrderQuery groupByPayDate() Group by the pay_date column
 * @method     ChildSaleOrderQuery groupByIsReadyForShipping() Group by the is_ready_for_shipping column
 * @method     ChildSaleOrderQuery groupByIsShippingChecked() Group by the is_shipping_checked column
 * @method     ChildSaleOrderQuery groupByIsFullyShipped() Group by the is_fully_shipped column
 * @method     ChildSaleOrderQuery groupByIsPushedToAccounting() Group by the is_pushed_to_accounting column
 * @method     ChildSaleOrderQuery groupByShipDate() Group by the ship_date column
 * @method     ChildSaleOrderQuery groupBySaleOrderStatusId() Group by the sale_order_status_id column
 * @method     ChildSaleOrderQuery groupByHandledBy() Group by the handled_by column
 * @method     ChildSaleOrderQuery groupBySource() Group by the source column
 * @method     ChildSaleOrderQuery groupByIsCompleted() Group by the is_completed column
 * @method     ChildSaleOrderQuery groupByCreatedByUserId() Group by the created_by_user_id column
 * @method     ChildSaleOrderQuery groupByCreatedOn() Group by the created_on column
 * @method     ChildSaleOrderQuery groupByEstimatedDeliveryDate() Group by the estimated_delivery_date column
 * @method     ChildSaleOrderQuery groupByCustomerId() Group by the customer_id column
 * @method     ChildSaleOrderQuery groupByEnvelopeWeightGrams() Group by the envelope_weight_grams column
 * @method     ChildSaleOrderQuery groupByWebshopUserSessionId() Group by the webshop_user_session_id column
 * @method     ChildSaleOrderQuery groupByInvoiceNumber() Group by the invoice_number column
 * @method     ChildSaleOrderQuery groupByInvoiceDate() Group by the invoice_date column
 * @method     ChildSaleOrderQuery groupByPaytermDays() Group by the payterm_days column
 * @method     ChildSaleOrderQuery groupByPaytermOriginalId() Group by the payterm_original_id column
 * @method     ChildSaleOrderQuery groupByPaytermString() Group by the payterm_string column
 * @method     ChildSaleOrderQuery groupByOrderNumber() Group by the order_number column
 * @method     ChildSaleOrderQuery groupByItemDeleted() Group by the is_deleted column
 * @method     ChildSaleOrderQuery groupByCompanyName() Group by the company_name column
 * @method     ChildSaleOrderQuery groupByDebitor() Group by the debitor column
 * @method     ChildSaleOrderQuery groupByOurCustomFolder() Group by the our_custom_folder column
 * @method     ChildSaleOrderQuery groupByOurSlogan() Group by the our_slogan column
 * @method     ChildSaleOrderQuery groupByOurPhone() Group by the our_phone column
 * @method     ChildSaleOrderQuery groupByOurFax() Group by the our_fax column
 * @method     ChildSaleOrderQuery groupByOurWebsite() Group by the our_website column
 * @method     ChildSaleOrderQuery groupByOurEmail() Group by the our_email column
 * @method     ChildSaleOrderQuery groupByOurVatNumber() Group by the our_vat_number column
 * @method     ChildSaleOrderQuery groupByOurChamberOfCommerce() Group by the our_chamber_of_commerce column
 * @method     ChildSaleOrderQuery groupByOurIbanNumber() Group by the our_iban_number column
 * @method     ChildSaleOrderQuery groupByOurBicNumber() Group by the our_bic_number column
 * @method     ChildSaleOrderQuery groupByOurGeneralCompanyName() Group by the our_general_company_name column
 * @method     ChildSaleOrderQuery groupByOurGeneralStreet() Group by the our_general_street column
 * @method     ChildSaleOrderQuery groupByOurGeneralNumber() Group by the our_general_number column
 * @method     ChildSaleOrderQuery groupByOurGeneralNumberAdd() Group by the our_general_number_add column
 * @method     ChildSaleOrderQuery groupByOurGeneralPostal() Group by the our_general_postal column
 * @method     ChildSaleOrderQuery groupByOurGeneralPoBox() Group by the our_general_po_box column
 * @method     ChildSaleOrderQuery groupByOurGeneralCity() Group by the our_general_city column
 * @method     ChildSaleOrderQuery groupByOurGeneralCountry() Group by the our_general_country column
 * @method     ChildSaleOrderQuery groupByCustomerPhone() Group by the customer_phone column
 * @method     ChildSaleOrderQuery groupByCustomerFax() Group by the customer_fax column
 * @method     ChildSaleOrderQuery groupByCustomerEmail() Group by the customer_email column
 * @method     ChildSaleOrderQuery groupByCustomerChamberOfCommerce() Group by the customer_chamber_of_commerce column
 * @method     ChildSaleOrderQuery groupByCustomerVatNumber() Group by the customer_vat_number column
 * @method     ChildSaleOrderQuery groupByCustomerOrderReference() Group by the customer_order_reference column
 * @method     ChildSaleOrderQuery groupByCustomerOrderPlacedBy() Group by the customer_order_placed_by column
 * @method     ChildSaleOrderQuery groupByCustomerInvoiceAddressId() Group by the customer_invoice_address_id column
 * @method     ChildSaleOrderQuery groupByCustomerInvoiceCompanyName() Group by the customer_invoice_company_name column
 * @method     ChildSaleOrderQuery groupByCustomerInvoiceAttnName() Group by the customer_invoice_attn_name column
 * @method     ChildSaleOrderQuery groupByCustomerInvoiceStreet() Group by the customer_invoice_street column
 * @method     ChildSaleOrderQuery groupByCustomerInvoiceNumber() Group by the customer_invoice_number column
 * @method     ChildSaleOrderQuery groupByCustomerInvoiceNumberAdd() Group by the customer_invoice_number_add column
 * @method     ChildSaleOrderQuery groupByCustomerInvoicePostal() Group by the customer_invoice_postal column
 * @method     ChildSaleOrderQuery groupByCustomerInvoiceCity() Group by the customer_invoice_city column
 * @method     ChildSaleOrderQuery groupByCustomerInvoiceCountry() Group by the customer_invoice_country column
 * @method     ChildSaleOrderQuery groupByCustomerInvoiceUsaState() Group by the customer_invoice_usa_state column
 * @method     ChildSaleOrderQuery groupByCustomerInvoiceAddressL1() Group by the customer_invoice_address_l1 column
 * @method     ChildSaleOrderQuery groupByCustomerInvoiceAddressL2() Group by the customer_invoice_address_l2 column
 * @method     ChildSaleOrderQuery groupByCustomerDeliveryAddressId() Group by the customer_delivery_address_id column
 * @method     ChildSaleOrderQuery groupByCustomerDeliveryCompanyName() Group by the customer_delivery_company_name column
 * @method     ChildSaleOrderQuery groupByCustomerDeliveryAttnName() Group by the customer_delivery_attn_name column
 * @method     ChildSaleOrderQuery groupByCustomerDeliveryStreet() Group by the customer_delivery_street column
 * @method     ChildSaleOrderQuery groupByCustomerDeliveryNumber() Group by the customer_delivery_number column
 * @method     ChildSaleOrderQuery groupByCustomerDeliveryNumberAdd() Group by the customer_delivery_number_add column
 * @method     ChildSaleOrderQuery groupByCustomerDeliveryPostal() Group by the customer_delivery_postal column
 * @method     ChildSaleOrderQuery groupByCustomerDeliveryCity() Group by the customer_delivery_city column
 * @method     ChildSaleOrderQuery groupByCustomerDeliveryCountry() Group by the customer_delivery_country column
 * @method     ChildSaleOrderQuery groupByCustomerDeliveryUsaState() Group by the customer_delivery_usa_state column
 * @method     ChildSaleOrderQuery groupByCustomerDeliveryAddressL1() Group by the customer_delivery_address_l1 column
 * @method     ChildSaleOrderQuery groupByCustomerDeliveryAddressL2() Group by the customer_delivery_address_l2 column
 * @method     ChildSaleOrderQuery groupByCpFirstName() Group by the cp_first_name column
 * @method     ChildSaleOrderQuery groupByCpLastName() Group by the cp_last_name column
 * @method     ChildSaleOrderQuery groupByCpSallutation() Group by the cp_sallutation column
 * @method     ChildSaleOrderQuery groupByReference() Group by the reference column
 * @method     ChildSaleOrderQuery groupByShippingNote() Group by the shipping_note column
 * @method     ChildSaleOrderQuery groupByCustomerOrderNote() Group by the customer_order_note column
 * @method     ChildSaleOrderQuery groupByShippingOrderNote() Group by the shipping_order_note column
 * @method     ChildSaleOrderQuery groupByHasWrap() Group by the has_wrap column
 * @method     ChildSaleOrderQuery groupByWrappingPrice() Group by the wrapping_price column
 * @method     ChildSaleOrderQuery groupByShippingCarrier() Group by the shipping_carrier column
 * @method     ChildSaleOrderQuery groupByShippingPrice() Group by the shipping_price column
 * @method     ChildSaleOrderQuery groupByShippingMethodId() Group by the shipping_method_id column
 * @method     ChildSaleOrderQuery groupByShippingHasTrackTrace() Group by the shipping_has_track_trace column
 * @method     ChildSaleOrderQuery groupByShippingId() Group by the shipping_id column
 * @method     ChildSaleOrderQuery groupByTrackingCode() Group by the tracking_code column
 * @method     ChildSaleOrderQuery groupByTrackingUrl() Group by the tracking_url column
 * @method     ChildSaleOrderQuery groupByPaymethodName() Group by the paymethod_name column
 * @method     ChildSaleOrderQuery groupByPaymethodCode() Group by the paymethod_code column
 * @method     ChildSaleOrderQuery groupByPaymethodId() Group by the paymethod_id column
 * @method     ChildSaleOrderQuery groupByShippingRegistrationDate() Group by the shipping_registration_date column
 * @method     ChildSaleOrderQuery groupByInvoiceByPostal() Group by the invoice_by_postal column
 * @method     ChildSaleOrderQuery groupByPackingSlipType() Group by the packing_slip_type column
 * @method     ChildSaleOrderQuery groupByWorkInProgress() Group by the work_in_progress column
 * @method     ChildSaleOrderQuery groupByPackageAmount() Group by the package_amount column
 * @method     ChildSaleOrderQuery groupByCouponCode() Group by the coupon_code column
 * @method     ChildSaleOrderQuery groupByCouponDiscountAmount() Group by the coupon_discount_amount column
 * @method     ChildSaleOrderQuery groupByCouponDiscountPercentage() Group by the coupon_discount_percentage column
 * @method     ChildSaleOrderQuery groupByHideUntill() Group by the hide_untill column
 *
 * @method     ChildSaleOrderQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSaleOrderQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSaleOrderQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSaleOrderQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSaleOrderQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSaleOrderQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSaleOrderQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildSaleOrderQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildSaleOrderQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildSaleOrderQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildSaleOrderQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildSaleOrderQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildSaleOrderQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildSaleOrderQuery leftJoinSaleOrderStatus($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrderStatus relation
 * @method     ChildSaleOrderQuery rightJoinSaleOrderStatus($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrderStatus relation
 * @method     ChildSaleOrderQuery innerJoinSaleOrderStatus($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrderStatus relation
 *
 * @method     ChildSaleOrderQuery joinWithSaleOrderStatus($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrderStatus relation
 *
 * @method     ChildSaleOrderQuery leftJoinWithSaleOrderStatus() Adds a LEFT JOIN clause and with to the query using the SaleOrderStatus relation
 * @method     ChildSaleOrderQuery rightJoinWithSaleOrderStatus() Adds a RIGHT JOIN clause and with to the query using the SaleOrderStatus relation
 * @method     ChildSaleOrderQuery innerJoinWithSaleOrderStatus() Adds a INNER JOIN clause and with to the query using the SaleOrderStatus relation
 *
 * @method     ChildSaleOrderQuery leftJoinShippingMethod($relationAlias = null) Adds a LEFT JOIN clause to the query using the ShippingMethod relation
 * @method     ChildSaleOrderQuery rightJoinShippingMethod($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ShippingMethod relation
 * @method     ChildSaleOrderQuery innerJoinShippingMethod($relationAlias = null) Adds a INNER JOIN clause to the query using the ShippingMethod relation
 *
 * @method     ChildSaleOrderQuery joinWithShippingMethod($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ShippingMethod relation
 *
 * @method     ChildSaleOrderQuery leftJoinWithShippingMethod() Adds a LEFT JOIN clause and with to the query using the ShippingMethod relation
 * @method     ChildSaleOrderQuery rightJoinWithShippingMethod() Adds a RIGHT JOIN clause and with to the query using the ShippingMethod relation
 * @method     ChildSaleOrderQuery innerJoinWithShippingMethod() Adds a INNER JOIN clause and with to the query using the ShippingMethod relation
 *
 * @method     ChildSaleOrderQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildSaleOrderQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildSaleOrderQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildSaleOrderQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildSaleOrderQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildSaleOrderQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildSaleOrderQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     ChildSaleOrderQuery leftJoinSaleOrderPayment($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrderPayment relation
 * @method     ChildSaleOrderQuery rightJoinSaleOrderPayment($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrderPayment relation
 * @method     ChildSaleOrderQuery innerJoinSaleOrderPayment($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrderPayment relation
 *
 * @method     ChildSaleOrderQuery joinWithSaleOrderPayment($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrderPayment relation
 *
 * @method     ChildSaleOrderQuery leftJoinWithSaleOrderPayment() Adds a LEFT JOIN clause and with to the query using the SaleOrderPayment relation
 * @method     ChildSaleOrderQuery rightJoinWithSaleOrderPayment() Adds a RIGHT JOIN clause and with to the query using the SaleOrderPayment relation
 * @method     ChildSaleOrderQuery innerJoinWithSaleOrderPayment() Adds a INNER JOIN clause and with to the query using the SaleOrderPayment relation
 *
 * @method     ChildSaleOrderQuery leftJoinPayment($relationAlias = null) Adds a LEFT JOIN clause to the query using the Payment relation
 * @method     ChildSaleOrderQuery rightJoinPayment($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Payment relation
 * @method     ChildSaleOrderQuery innerJoinPayment($relationAlias = null) Adds a INNER JOIN clause to the query using the Payment relation
 *
 * @method     ChildSaleOrderQuery joinWithPayment($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Payment relation
 *
 * @method     ChildSaleOrderQuery leftJoinWithPayment() Adds a LEFT JOIN clause and with to the query using the Payment relation
 * @method     ChildSaleOrderQuery rightJoinWithPayment() Adds a RIGHT JOIN clause and with to the query using the Payment relation
 * @method     ChildSaleOrderQuery innerJoinWithPayment() Adds a INNER JOIN clause and with to the query using the Payment relation
 *
 * @method     ChildSaleOrderQuery leftJoinSaleOrderEmail($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrderEmail relation
 * @method     ChildSaleOrderQuery rightJoinSaleOrderEmail($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrderEmail relation
 * @method     ChildSaleOrderQuery innerJoinSaleOrderEmail($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrderEmail relation
 *
 * @method     ChildSaleOrderQuery joinWithSaleOrderEmail($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrderEmail relation
 *
 * @method     ChildSaleOrderQuery leftJoinWithSaleOrderEmail() Adds a LEFT JOIN clause and with to the query using the SaleOrderEmail relation
 * @method     ChildSaleOrderQuery rightJoinWithSaleOrderEmail() Adds a RIGHT JOIN clause and with to the query using the SaleOrderEmail relation
 * @method     ChildSaleOrderQuery innerJoinWithSaleOrderEmail() Adds a INNER JOIN clause and with to the query using the SaleOrderEmail relation
 *
 * @method     ChildSaleOrderQuery leftJoinSaleOrderItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrderItem relation
 * @method     ChildSaleOrderQuery rightJoinSaleOrderItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrderItem relation
 * @method     ChildSaleOrderQuery innerJoinSaleOrderItem($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrderItem relation
 *
 * @method     ChildSaleOrderQuery joinWithSaleOrderItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrderItem relation
 *
 * @method     ChildSaleOrderQuery leftJoinWithSaleOrderItem() Adds a LEFT JOIN clause and with to the query using the SaleOrderItem relation
 * @method     ChildSaleOrderQuery rightJoinWithSaleOrderItem() Adds a RIGHT JOIN clause and with to the query using the SaleOrderItem relation
 * @method     ChildSaleOrderQuery innerJoinWithSaleOrderItem() Adds a INNER JOIN clause and with to the query using the SaleOrderItem relation
 *
 * @method     ChildSaleOrderQuery leftJoinSaleOrderNotification($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrderNotification relation
 * @method     ChildSaleOrderQuery rightJoinSaleOrderNotification($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrderNotification relation
 * @method     ChildSaleOrderQuery innerJoinSaleOrderNotification($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrderNotification relation
 *
 * @method     ChildSaleOrderQuery joinWithSaleOrderNotification($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrderNotification relation
 *
 * @method     ChildSaleOrderQuery leftJoinWithSaleOrderNotification() Adds a LEFT JOIN clause and with to the query using the SaleOrderNotification relation
 * @method     ChildSaleOrderQuery rightJoinWithSaleOrderNotification() Adds a RIGHT JOIN clause and with to the query using the SaleOrderNotification relation
 * @method     ChildSaleOrderQuery innerJoinWithSaleOrderNotification() Adds a INNER JOIN clause and with to the query using the SaleOrderNotification relation
 *
 * @method     \Model\Cms\SiteQuery|\Model\Setting\MasterTable\SaleOrderStatusQuery|\Model\Setting\MasterTable\ShippingMethodQuery|\Model\Account\UserQuery|\Model\Sale\SaleOrderPaymentQuery|\Model\Finance\PaymentQuery|\Model\Sale\SaleOrderEmailQuery|\Model\Sale\SaleOrderItemQuery|\Model\SaleOrderNotification\SaleOrderNotificationQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSaleOrder findOne(ConnectionInterface $con = null) Return the first ChildSaleOrder matching the query
 * @method     ChildSaleOrder findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSaleOrder matching the query, or a new ChildSaleOrder object populated from the query conditions when no match is found
 *
 * @method     ChildSaleOrder findOneById(int $id) Return the first ChildSaleOrder filtered by the id column
 * @method     ChildSaleOrder findOneBySiteId(int $site_id) Return the first ChildSaleOrder filtered by the site_id column
 * @method     ChildSaleOrder findOneByLanguageId(int $language_id) Return the first ChildSaleOrder filtered by the language_id column
 * @method     ChildSaleOrder findOneByIsClosed(boolean $is_closed) Return the first ChildSaleOrder filtered by the is_closed column
 * @method     ChildSaleOrder findOneByIsFullyPaid(boolean $is_fully_paid) Return the first ChildSaleOrder filtered by the is_fully_paid column
 * @method     ChildSaleOrder findOneByPayDate(string $pay_date) Return the first ChildSaleOrder filtered by the pay_date column
 * @method     ChildSaleOrder findOneByIsReadyForShipping(boolean $is_ready_for_shipping) Return the first ChildSaleOrder filtered by the is_ready_for_shipping column
 * @method     ChildSaleOrder findOneByIsShippingChecked(boolean $is_shipping_checked) Return the first ChildSaleOrder filtered by the is_shipping_checked column
 * @method     ChildSaleOrder findOneByIsFullyShipped(boolean $is_fully_shipped) Return the first ChildSaleOrder filtered by the is_fully_shipped column
 * @method     ChildSaleOrder findOneByIsPushedToAccounting(boolean $is_pushed_to_accounting) Return the first ChildSaleOrder filtered by the is_pushed_to_accounting column
 * @method     ChildSaleOrder findOneByShipDate(string $ship_date) Return the first ChildSaleOrder filtered by the ship_date column
 * @method     ChildSaleOrder findOneBySaleOrderStatusId(int $sale_order_status_id) Return the first ChildSaleOrder filtered by the sale_order_status_id column
 * @method     ChildSaleOrder findOneByHandledBy(string $handled_by) Return the first ChildSaleOrder filtered by the handled_by column
 * @method     ChildSaleOrder findOneBySource(string $source) Return the first ChildSaleOrder filtered by the source column
 * @method     ChildSaleOrder findOneByIsCompleted(boolean $is_completed) Return the first ChildSaleOrder filtered by the is_completed column
 * @method     ChildSaleOrder findOneByCreatedByUserId(int $created_by_user_id) Return the first ChildSaleOrder filtered by the created_by_user_id column
 * @method     ChildSaleOrder findOneByCreatedOn(string $created_on) Return the first ChildSaleOrder filtered by the created_on column
 * @method     ChildSaleOrder findOneByEstimatedDeliveryDate(string $estimated_delivery_date) Return the first ChildSaleOrder filtered by the estimated_delivery_date column
 * @method     ChildSaleOrder findOneByCustomerId(int $customer_id) Return the first ChildSaleOrder filtered by the customer_id column
 * @method     ChildSaleOrder findOneByEnvelopeWeightGrams(int $envelope_weight_grams) Return the first ChildSaleOrder filtered by the envelope_weight_grams column
 * @method     ChildSaleOrder findOneByWebshopUserSessionId(int $webshop_user_session_id) Return the first ChildSaleOrder filtered by the webshop_user_session_id column
 * @method     ChildSaleOrder findOneByInvoiceNumber(string $invoice_number) Return the first ChildSaleOrder filtered by the invoice_number column
 * @method     ChildSaleOrder findOneByInvoiceDate(string $invoice_date) Return the first ChildSaleOrder filtered by the invoice_date column
 * @method     ChildSaleOrder findOneByPaytermDays(int $payterm_days) Return the first ChildSaleOrder filtered by the payterm_days column
 * @method     ChildSaleOrder findOneByPaytermOriginalId(int $payterm_original_id) Return the first ChildSaleOrder filtered by the payterm_original_id column
 * @method     ChildSaleOrder findOneByPaytermString(string $payterm_string) Return the first ChildSaleOrder filtered by the payterm_string column
 * @method     ChildSaleOrder findOneByOrderNumber(string $order_number) Return the first ChildSaleOrder filtered by the order_number column
 * @method     ChildSaleOrder findOneByItemDeleted(boolean $is_deleted) Return the first ChildSaleOrder filtered by the is_deleted column
 * @method     ChildSaleOrder findOneByCompanyName(string $company_name) Return the first ChildSaleOrder filtered by the company_name column
 * @method     ChildSaleOrder findOneByDebitor(string $debitor) Return the first ChildSaleOrder filtered by the debitor column
 * @method     ChildSaleOrder findOneByOurCustomFolder(string $our_custom_folder) Return the first ChildSaleOrder filtered by the our_custom_folder column
 * @method     ChildSaleOrder findOneByOurSlogan(string $our_slogan) Return the first ChildSaleOrder filtered by the our_slogan column
 * @method     ChildSaleOrder findOneByOurPhone(string $our_phone) Return the first ChildSaleOrder filtered by the our_phone column
 * @method     ChildSaleOrder findOneByOurFax(string $our_fax) Return the first ChildSaleOrder filtered by the our_fax column
 * @method     ChildSaleOrder findOneByOurWebsite(string $our_website) Return the first ChildSaleOrder filtered by the our_website column
 * @method     ChildSaleOrder findOneByOurEmail(string $our_email) Return the first ChildSaleOrder filtered by the our_email column
 * @method     ChildSaleOrder findOneByOurVatNumber(string $our_vat_number) Return the first ChildSaleOrder filtered by the our_vat_number column
 * @method     ChildSaleOrder findOneByOurChamberOfCommerce(string $our_chamber_of_commerce) Return the first ChildSaleOrder filtered by the our_chamber_of_commerce column
 * @method     ChildSaleOrder findOneByOurIbanNumber(string $our_iban_number) Return the first ChildSaleOrder filtered by the our_iban_number column
 * @method     ChildSaleOrder findOneByOurBicNumber(string $our_bic_number) Return the first ChildSaleOrder filtered by the our_bic_number column
 * @method     ChildSaleOrder findOneByOurGeneralCompanyName(string $our_general_company_name) Return the first ChildSaleOrder filtered by the our_general_company_name column
 * @method     ChildSaleOrder findOneByOurGeneralStreet(string $our_general_street) Return the first ChildSaleOrder filtered by the our_general_street column
 * @method     ChildSaleOrder findOneByOurGeneralNumber(string $our_general_number) Return the first ChildSaleOrder filtered by the our_general_number column
 * @method     ChildSaleOrder findOneByOurGeneralNumberAdd(string $our_general_number_add) Return the first ChildSaleOrder filtered by the our_general_number_add column
 * @method     ChildSaleOrder findOneByOurGeneralPostal(string $our_general_postal) Return the first ChildSaleOrder filtered by the our_general_postal column
 * @method     ChildSaleOrder findOneByOurGeneralPoBox(string $our_general_po_box) Return the first ChildSaleOrder filtered by the our_general_po_box column
 * @method     ChildSaleOrder findOneByOurGeneralCity(string $our_general_city) Return the first ChildSaleOrder filtered by the our_general_city column
 * @method     ChildSaleOrder findOneByOurGeneralCountry(string $our_general_country) Return the first ChildSaleOrder filtered by the our_general_country column
 * @method     ChildSaleOrder findOneByCustomerPhone(string $customer_phone) Return the first ChildSaleOrder filtered by the customer_phone column
 * @method     ChildSaleOrder findOneByCustomerFax(string $customer_fax) Return the first ChildSaleOrder filtered by the customer_fax column
 * @method     ChildSaleOrder findOneByCustomerEmail(string $customer_email) Return the first ChildSaleOrder filtered by the customer_email column
 * @method     ChildSaleOrder findOneByCustomerChamberOfCommerce(string $customer_chamber_of_commerce) Return the first ChildSaleOrder filtered by the customer_chamber_of_commerce column
 * @method     ChildSaleOrder findOneByCustomerVatNumber(string $customer_vat_number) Return the first ChildSaleOrder filtered by the customer_vat_number column
 * @method     ChildSaleOrder findOneByCustomerOrderReference(string $customer_order_reference) Return the first ChildSaleOrder filtered by the customer_order_reference column
 * @method     ChildSaleOrder findOneByCustomerOrderPlacedBy(string $customer_order_placed_by) Return the first ChildSaleOrder filtered by the customer_order_placed_by column
 * @method     ChildSaleOrder findOneByCustomerInvoiceAddressId(int $customer_invoice_address_id) Return the first ChildSaleOrder filtered by the customer_invoice_address_id column
 * @method     ChildSaleOrder findOneByCustomerInvoiceCompanyName(string $customer_invoice_company_name) Return the first ChildSaleOrder filtered by the customer_invoice_company_name column
 * @method     ChildSaleOrder findOneByCustomerInvoiceAttnName(string $customer_invoice_attn_name) Return the first ChildSaleOrder filtered by the customer_invoice_attn_name column
 * @method     ChildSaleOrder findOneByCustomerInvoiceStreet(string $customer_invoice_street) Return the first ChildSaleOrder filtered by the customer_invoice_street column
 * @method     ChildSaleOrder findOneByCustomerInvoiceNumber(string $customer_invoice_number) Return the first ChildSaleOrder filtered by the customer_invoice_number column
 * @method     ChildSaleOrder findOneByCustomerInvoiceNumberAdd(string $customer_invoice_number_add) Return the first ChildSaleOrder filtered by the customer_invoice_number_add column
 * @method     ChildSaleOrder findOneByCustomerInvoicePostal(string $customer_invoice_postal) Return the first ChildSaleOrder filtered by the customer_invoice_postal column
 * @method     ChildSaleOrder findOneByCustomerInvoiceCity(string $customer_invoice_city) Return the first ChildSaleOrder filtered by the customer_invoice_city column
 * @method     ChildSaleOrder findOneByCustomerInvoiceCountry(string $customer_invoice_country) Return the first ChildSaleOrder filtered by the customer_invoice_country column
 * @method     ChildSaleOrder findOneByCustomerInvoiceUsaState(string $customer_invoice_usa_state) Return the first ChildSaleOrder filtered by the customer_invoice_usa_state column
 * @method     ChildSaleOrder findOneByCustomerInvoiceAddressL1(string $customer_invoice_address_l1) Return the first ChildSaleOrder filtered by the customer_invoice_address_l1 column
 * @method     ChildSaleOrder findOneByCustomerInvoiceAddressL2(string $customer_invoice_address_l2) Return the first ChildSaleOrder filtered by the customer_invoice_address_l2 column
 * @method     ChildSaleOrder findOneByCustomerDeliveryAddressId(int $customer_delivery_address_id) Return the first ChildSaleOrder filtered by the customer_delivery_address_id column
 * @method     ChildSaleOrder findOneByCustomerDeliveryCompanyName(string $customer_delivery_company_name) Return the first ChildSaleOrder filtered by the customer_delivery_company_name column
 * @method     ChildSaleOrder findOneByCustomerDeliveryAttnName(string $customer_delivery_attn_name) Return the first ChildSaleOrder filtered by the customer_delivery_attn_name column
 * @method     ChildSaleOrder findOneByCustomerDeliveryStreet(string $customer_delivery_street) Return the first ChildSaleOrder filtered by the customer_delivery_street column
 * @method     ChildSaleOrder findOneByCustomerDeliveryNumber(string $customer_delivery_number) Return the first ChildSaleOrder filtered by the customer_delivery_number column
 * @method     ChildSaleOrder findOneByCustomerDeliveryNumberAdd(string $customer_delivery_number_add) Return the first ChildSaleOrder filtered by the customer_delivery_number_add column
 * @method     ChildSaleOrder findOneByCustomerDeliveryPostal(string $customer_delivery_postal) Return the first ChildSaleOrder filtered by the customer_delivery_postal column
 * @method     ChildSaleOrder findOneByCustomerDeliveryCity(string $customer_delivery_city) Return the first ChildSaleOrder filtered by the customer_delivery_city column
 * @method     ChildSaleOrder findOneByCustomerDeliveryCountry(string $customer_delivery_country) Return the first ChildSaleOrder filtered by the customer_delivery_country column
 * @method     ChildSaleOrder findOneByCustomerDeliveryUsaState(string $customer_delivery_usa_state) Return the first ChildSaleOrder filtered by the customer_delivery_usa_state column
 * @method     ChildSaleOrder findOneByCustomerDeliveryAddressL1(string $customer_delivery_address_l1) Return the first ChildSaleOrder filtered by the customer_delivery_address_l1 column
 * @method     ChildSaleOrder findOneByCustomerDeliveryAddressL2(string $customer_delivery_address_l2) Return the first ChildSaleOrder filtered by the customer_delivery_address_l2 column
 * @method     ChildSaleOrder findOneByCpFirstName(string $cp_first_name) Return the first ChildSaleOrder filtered by the cp_first_name column
 * @method     ChildSaleOrder findOneByCpLastName(string $cp_last_name) Return the first ChildSaleOrder filtered by the cp_last_name column
 * @method     ChildSaleOrder findOneByCpSallutation(string $cp_sallutation) Return the first ChildSaleOrder filtered by the cp_sallutation column
 * @method     ChildSaleOrder findOneByReference(string $reference) Return the first ChildSaleOrder filtered by the reference column
 * @method     ChildSaleOrder findOneByShippingNote(string $shipping_note) Return the first ChildSaleOrder filtered by the shipping_note column
 * @method     ChildSaleOrder findOneByCustomerOrderNote(string $customer_order_note) Return the first ChildSaleOrder filtered by the customer_order_note column
 * @method     ChildSaleOrder findOneByShippingOrderNote(string $shipping_order_note) Return the first ChildSaleOrder filtered by the shipping_order_note column
 * @method     ChildSaleOrder findOneByHasWrap(boolean $has_wrap) Return the first ChildSaleOrder filtered by the has_wrap column
 * @method     ChildSaleOrder findOneByWrappingPrice(double $wrapping_price) Return the first ChildSaleOrder filtered by the wrapping_price column
 * @method     ChildSaleOrder findOneByShippingCarrier(string $shipping_carrier) Return the first ChildSaleOrder filtered by the shipping_carrier column
 * @method     ChildSaleOrder findOneByShippingPrice(double $shipping_price) Return the first ChildSaleOrder filtered by the shipping_price column
 * @method     ChildSaleOrder findOneByShippingMethodId(int $shipping_method_id) Return the first ChildSaleOrder filtered by the shipping_method_id column
 * @method     ChildSaleOrder findOneByShippingHasTrackTrace(boolean $shipping_has_track_trace) Return the first ChildSaleOrder filtered by the shipping_has_track_trace column
 * @method     ChildSaleOrder findOneByShippingId(double $shipping_id) Return the first ChildSaleOrder filtered by the shipping_id column
 * @method     ChildSaleOrder findOneByTrackingCode(string $tracking_code) Return the first ChildSaleOrder filtered by the tracking_code column
 * @method     ChildSaleOrder findOneByTrackingUrl(string $tracking_url) Return the first ChildSaleOrder filtered by the tracking_url column
 * @method     ChildSaleOrder findOneByPaymethodName(string $paymethod_name) Return the first ChildSaleOrder filtered by the paymethod_name column
 * @method     ChildSaleOrder findOneByPaymethodCode(string $paymethod_code) Return the first ChildSaleOrder filtered by the paymethod_code column
 * @method     ChildSaleOrder findOneByPaymethodId(int $paymethod_id) Return the first ChildSaleOrder filtered by the paymethod_id column
 * @method     ChildSaleOrder findOneByShippingRegistrationDate(string $shipping_registration_date) Return the first ChildSaleOrder filtered by the shipping_registration_date column
 * @method     ChildSaleOrder findOneByInvoiceByPostal(boolean $invoice_by_postal) Return the first ChildSaleOrder filtered by the invoice_by_postal column
 * @method     ChildSaleOrder findOneByPackingSlipType(string $packing_slip_type) Return the first ChildSaleOrder filtered by the packing_slip_type column
 * @method     ChildSaleOrder findOneByWorkInProgress(boolean $work_in_progress) Return the first ChildSaleOrder filtered by the work_in_progress column
 * @method     ChildSaleOrder findOneByPackageAmount(int $package_amount) Return the first ChildSaleOrder filtered by the package_amount column
 * @method     ChildSaleOrder findOneByCouponCode(string $coupon_code) Return the first ChildSaleOrder filtered by the coupon_code column
 * @method     ChildSaleOrder findOneByCouponDiscountAmount(double $coupon_discount_amount) Return the first ChildSaleOrder filtered by the coupon_discount_amount column
 * @method     ChildSaleOrder findOneByCouponDiscountPercentage(double $coupon_discount_percentage) Return the first ChildSaleOrder filtered by the coupon_discount_percentage column
 * @method     ChildSaleOrder findOneByHideUntill(string $hide_untill) Return the first ChildSaleOrder filtered by the hide_untill column *

 * @method     ChildSaleOrder requirePk($key, ConnectionInterface $con = null) Return the ChildSaleOrder by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOne(ConnectionInterface $con = null) Return the first ChildSaleOrder matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleOrder requireOneById(int $id) Return the first ChildSaleOrder filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneBySiteId(int $site_id) Return the first ChildSaleOrder filtered by the site_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByLanguageId(int $language_id) Return the first ChildSaleOrder filtered by the language_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByIsClosed(boolean $is_closed) Return the first ChildSaleOrder filtered by the is_closed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByIsFullyPaid(boolean $is_fully_paid) Return the first ChildSaleOrder filtered by the is_fully_paid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByPayDate(string $pay_date) Return the first ChildSaleOrder filtered by the pay_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByIsReadyForShipping(boolean $is_ready_for_shipping) Return the first ChildSaleOrder filtered by the is_ready_for_shipping column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByIsShippingChecked(boolean $is_shipping_checked) Return the first ChildSaleOrder filtered by the is_shipping_checked column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByIsFullyShipped(boolean $is_fully_shipped) Return the first ChildSaleOrder filtered by the is_fully_shipped column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByIsPushedToAccounting(boolean $is_pushed_to_accounting) Return the first ChildSaleOrder filtered by the is_pushed_to_accounting column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByShipDate(string $ship_date) Return the first ChildSaleOrder filtered by the ship_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneBySaleOrderStatusId(int $sale_order_status_id) Return the first ChildSaleOrder filtered by the sale_order_status_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByHandledBy(string $handled_by) Return the first ChildSaleOrder filtered by the handled_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneBySource(string $source) Return the first ChildSaleOrder filtered by the source column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByIsCompleted(boolean $is_completed) Return the first ChildSaleOrder filtered by the is_completed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCreatedByUserId(int $created_by_user_id) Return the first ChildSaleOrder filtered by the created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCreatedOn(string $created_on) Return the first ChildSaleOrder filtered by the created_on column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByEstimatedDeliveryDate(string $estimated_delivery_date) Return the first ChildSaleOrder filtered by the estimated_delivery_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerId(int $customer_id) Return the first ChildSaleOrder filtered by the customer_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByEnvelopeWeightGrams(int $envelope_weight_grams) Return the first ChildSaleOrder filtered by the envelope_weight_grams column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByWebshopUserSessionId(int $webshop_user_session_id) Return the first ChildSaleOrder filtered by the webshop_user_session_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByInvoiceNumber(string $invoice_number) Return the first ChildSaleOrder filtered by the invoice_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByInvoiceDate(string $invoice_date) Return the first ChildSaleOrder filtered by the invoice_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByPaytermDays(int $payterm_days) Return the first ChildSaleOrder filtered by the payterm_days column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByPaytermOriginalId(int $payterm_original_id) Return the first ChildSaleOrder filtered by the payterm_original_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByPaytermString(string $payterm_string) Return the first ChildSaleOrder filtered by the payterm_string column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOrderNumber(string $order_number) Return the first ChildSaleOrder filtered by the order_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByItemDeleted(boolean $is_deleted) Return the first ChildSaleOrder filtered by the is_deleted column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCompanyName(string $company_name) Return the first ChildSaleOrder filtered by the company_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByDebitor(string $debitor) Return the first ChildSaleOrder filtered by the debitor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurCustomFolder(string $our_custom_folder) Return the first ChildSaleOrder filtered by the our_custom_folder column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurSlogan(string $our_slogan) Return the first ChildSaleOrder filtered by the our_slogan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurPhone(string $our_phone) Return the first ChildSaleOrder filtered by the our_phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurFax(string $our_fax) Return the first ChildSaleOrder filtered by the our_fax column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurWebsite(string $our_website) Return the first ChildSaleOrder filtered by the our_website column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurEmail(string $our_email) Return the first ChildSaleOrder filtered by the our_email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurVatNumber(string $our_vat_number) Return the first ChildSaleOrder filtered by the our_vat_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurChamberOfCommerce(string $our_chamber_of_commerce) Return the first ChildSaleOrder filtered by the our_chamber_of_commerce column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurIbanNumber(string $our_iban_number) Return the first ChildSaleOrder filtered by the our_iban_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurBicNumber(string $our_bic_number) Return the first ChildSaleOrder filtered by the our_bic_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurGeneralCompanyName(string $our_general_company_name) Return the first ChildSaleOrder filtered by the our_general_company_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurGeneralStreet(string $our_general_street) Return the first ChildSaleOrder filtered by the our_general_street column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurGeneralNumber(string $our_general_number) Return the first ChildSaleOrder filtered by the our_general_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurGeneralNumberAdd(string $our_general_number_add) Return the first ChildSaleOrder filtered by the our_general_number_add column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurGeneralPostal(string $our_general_postal) Return the first ChildSaleOrder filtered by the our_general_postal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurGeneralPoBox(string $our_general_po_box) Return the first ChildSaleOrder filtered by the our_general_po_box column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurGeneralCity(string $our_general_city) Return the first ChildSaleOrder filtered by the our_general_city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByOurGeneralCountry(string $our_general_country) Return the first ChildSaleOrder filtered by the our_general_country column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerPhone(string $customer_phone) Return the first ChildSaleOrder filtered by the customer_phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerFax(string $customer_fax) Return the first ChildSaleOrder filtered by the customer_fax column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerEmail(string $customer_email) Return the first ChildSaleOrder filtered by the customer_email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerChamberOfCommerce(string $customer_chamber_of_commerce) Return the first ChildSaleOrder filtered by the customer_chamber_of_commerce column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerVatNumber(string $customer_vat_number) Return the first ChildSaleOrder filtered by the customer_vat_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerOrderReference(string $customer_order_reference) Return the first ChildSaleOrder filtered by the customer_order_reference column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerOrderPlacedBy(string $customer_order_placed_by) Return the first ChildSaleOrder filtered by the customer_order_placed_by column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerInvoiceAddressId(int $customer_invoice_address_id) Return the first ChildSaleOrder filtered by the customer_invoice_address_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerInvoiceCompanyName(string $customer_invoice_company_name) Return the first ChildSaleOrder filtered by the customer_invoice_company_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerInvoiceAttnName(string $customer_invoice_attn_name) Return the first ChildSaleOrder filtered by the customer_invoice_attn_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerInvoiceStreet(string $customer_invoice_street) Return the first ChildSaleOrder filtered by the customer_invoice_street column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerInvoiceNumber(string $customer_invoice_number) Return the first ChildSaleOrder filtered by the customer_invoice_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerInvoiceNumberAdd(string $customer_invoice_number_add) Return the first ChildSaleOrder filtered by the customer_invoice_number_add column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerInvoicePostal(string $customer_invoice_postal) Return the first ChildSaleOrder filtered by the customer_invoice_postal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerInvoiceCity(string $customer_invoice_city) Return the first ChildSaleOrder filtered by the customer_invoice_city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerInvoiceCountry(string $customer_invoice_country) Return the first ChildSaleOrder filtered by the customer_invoice_country column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerInvoiceUsaState(string $customer_invoice_usa_state) Return the first ChildSaleOrder filtered by the customer_invoice_usa_state column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerInvoiceAddressL1(string $customer_invoice_address_l1) Return the first ChildSaleOrder filtered by the customer_invoice_address_l1 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerInvoiceAddressL2(string $customer_invoice_address_l2) Return the first ChildSaleOrder filtered by the customer_invoice_address_l2 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerDeliveryAddressId(int $customer_delivery_address_id) Return the first ChildSaleOrder filtered by the customer_delivery_address_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerDeliveryCompanyName(string $customer_delivery_company_name) Return the first ChildSaleOrder filtered by the customer_delivery_company_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerDeliveryAttnName(string $customer_delivery_attn_name) Return the first ChildSaleOrder filtered by the customer_delivery_attn_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerDeliveryStreet(string $customer_delivery_street) Return the first ChildSaleOrder filtered by the customer_delivery_street column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerDeliveryNumber(string $customer_delivery_number) Return the first ChildSaleOrder filtered by the customer_delivery_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerDeliveryNumberAdd(string $customer_delivery_number_add) Return the first ChildSaleOrder filtered by the customer_delivery_number_add column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerDeliveryPostal(string $customer_delivery_postal) Return the first ChildSaleOrder filtered by the customer_delivery_postal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerDeliveryCity(string $customer_delivery_city) Return the first ChildSaleOrder filtered by the customer_delivery_city column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerDeliveryCountry(string $customer_delivery_country) Return the first ChildSaleOrder filtered by the customer_delivery_country column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerDeliveryUsaState(string $customer_delivery_usa_state) Return the first ChildSaleOrder filtered by the customer_delivery_usa_state column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerDeliveryAddressL1(string $customer_delivery_address_l1) Return the first ChildSaleOrder filtered by the customer_delivery_address_l1 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerDeliveryAddressL2(string $customer_delivery_address_l2) Return the first ChildSaleOrder filtered by the customer_delivery_address_l2 column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCpFirstName(string $cp_first_name) Return the first ChildSaleOrder filtered by the cp_first_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCpLastName(string $cp_last_name) Return the first ChildSaleOrder filtered by the cp_last_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCpSallutation(string $cp_sallutation) Return the first ChildSaleOrder filtered by the cp_sallutation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByReference(string $reference) Return the first ChildSaleOrder filtered by the reference column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByShippingNote(string $shipping_note) Return the first ChildSaleOrder filtered by the shipping_note column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCustomerOrderNote(string $customer_order_note) Return the first ChildSaleOrder filtered by the customer_order_note column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByShippingOrderNote(string $shipping_order_note) Return the first ChildSaleOrder filtered by the shipping_order_note column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByHasWrap(boolean $has_wrap) Return the first ChildSaleOrder filtered by the has_wrap column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByWrappingPrice(double $wrapping_price) Return the first ChildSaleOrder filtered by the wrapping_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByShippingCarrier(string $shipping_carrier) Return the first ChildSaleOrder filtered by the shipping_carrier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByShippingPrice(double $shipping_price) Return the first ChildSaleOrder filtered by the shipping_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByShippingMethodId(int $shipping_method_id) Return the first ChildSaleOrder filtered by the shipping_method_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByShippingHasTrackTrace(boolean $shipping_has_track_trace) Return the first ChildSaleOrder filtered by the shipping_has_track_trace column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByShippingId(double $shipping_id) Return the first ChildSaleOrder filtered by the shipping_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByTrackingCode(string $tracking_code) Return the first ChildSaleOrder filtered by the tracking_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByTrackingUrl(string $tracking_url) Return the first ChildSaleOrder filtered by the tracking_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByPaymethodName(string $paymethod_name) Return the first ChildSaleOrder filtered by the paymethod_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByPaymethodCode(string $paymethod_code) Return the first ChildSaleOrder filtered by the paymethod_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByPaymethodId(int $paymethod_id) Return the first ChildSaleOrder filtered by the paymethod_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByShippingRegistrationDate(string $shipping_registration_date) Return the first ChildSaleOrder filtered by the shipping_registration_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByInvoiceByPostal(boolean $invoice_by_postal) Return the first ChildSaleOrder filtered by the invoice_by_postal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByPackingSlipType(string $packing_slip_type) Return the first ChildSaleOrder filtered by the packing_slip_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByWorkInProgress(boolean $work_in_progress) Return the first ChildSaleOrder filtered by the work_in_progress column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByPackageAmount(int $package_amount) Return the first ChildSaleOrder filtered by the package_amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCouponCode(string $coupon_code) Return the first ChildSaleOrder filtered by the coupon_code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCouponDiscountAmount(double $coupon_discount_amount) Return the first ChildSaleOrder filtered by the coupon_discount_amount column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByCouponDiscountPercentage(double $coupon_discount_percentage) Return the first ChildSaleOrder filtered by the coupon_discount_percentage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrder requireOneByHideUntill(string $hide_untill) Return the first ChildSaleOrder filtered by the hide_untill column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleOrder[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSaleOrder objects based on current ModelCriteria
 * @method     ChildSaleOrder[]|ObjectCollection findById(int $id) Return ChildSaleOrder objects filtered by the id column
 * @method     ChildSaleOrder[]|ObjectCollection findBySiteId(int $site_id) Return ChildSaleOrder objects filtered by the site_id column
 * @method     ChildSaleOrder[]|ObjectCollection findByLanguageId(int $language_id) Return ChildSaleOrder objects filtered by the language_id column
 * @method     ChildSaleOrder[]|ObjectCollection findByIsClosed(boolean $is_closed) Return ChildSaleOrder objects filtered by the is_closed column
 * @method     ChildSaleOrder[]|ObjectCollection findByIsFullyPaid(boolean $is_fully_paid) Return ChildSaleOrder objects filtered by the is_fully_paid column
 * @method     ChildSaleOrder[]|ObjectCollection findByPayDate(string $pay_date) Return ChildSaleOrder objects filtered by the pay_date column
 * @method     ChildSaleOrder[]|ObjectCollection findByIsReadyForShipping(boolean $is_ready_for_shipping) Return ChildSaleOrder objects filtered by the is_ready_for_shipping column
 * @method     ChildSaleOrder[]|ObjectCollection findByIsShippingChecked(boolean $is_shipping_checked) Return ChildSaleOrder objects filtered by the is_shipping_checked column
 * @method     ChildSaleOrder[]|ObjectCollection findByIsFullyShipped(boolean $is_fully_shipped) Return ChildSaleOrder objects filtered by the is_fully_shipped column
 * @method     ChildSaleOrder[]|ObjectCollection findByIsPushedToAccounting(boolean $is_pushed_to_accounting) Return ChildSaleOrder objects filtered by the is_pushed_to_accounting column
 * @method     ChildSaleOrder[]|ObjectCollection findByShipDate(string $ship_date) Return ChildSaleOrder objects filtered by the ship_date column
 * @method     ChildSaleOrder[]|ObjectCollection findBySaleOrderStatusId(int $sale_order_status_id) Return ChildSaleOrder objects filtered by the sale_order_status_id column
 * @method     ChildSaleOrder[]|ObjectCollection findByHandledBy(string $handled_by) Return ChildSaleOrder objects filtered by the handled_by column
 * @method     ChildSaleOrder[]|ObjectCollection findBySource(string $source) Return ChildSaleOrder objects filtered by the source column
 * @method     ChildSaleOrder[]|ObjectCollection findByIsCompleted(boolean $is_completed) Return ChildSaleOrder objects filtered by the is_completed column
 * @method     ChildSaleOrder[]|ObjectCollection findByCreatedByUserId(int $created_by_user_id) Return ChildSaleOrder objects filtered by the created_by_user_id column
 * @method     ChildSaleOrder[]|ObjectCollection findByCreatedOn(string $created_on) Return ChildSaleOrder objects filtered by the created_on column
 * @method     ChildSaleOrder[]|ObjectCollection findByEstimatedDeliveryDate(string $estimated_delivery_date) Return ChildSaleOrder objects filtered by the estimated_delivery_date column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerId(int $customer_id) Return ChildSaleOrder objects filtered by the customer_id column
 * @method     ChildSaleOrder[]|ObjectCollection findByEnvelopeWeightGrams(int $envelope_weight_grams) Return ChildSaleOrder objects filtered by the envelope_weight_grams column
 * @method     ChildSaleOrder[]|ObjectCollection findByWebshopUserSessionId(int $webshop_user_session_id) Return ChildSaleOrder objects filtered by the webshop_user_session_id column
 * @method     ChildSaleOrder[]|ObjectCollection findByInvoiceNumber(string $invoice_number) Return ChildSaleOrder objects filtered by the invoice_number column
 * @method     ChildSaleOrder[]|ObjectCollection findByInvoiceDate(string $invoice_date) Return ChildSaleOrder objects filtered by the invoice_date column
 * @method     ChildSaleOrder[]|ObjectCollection findByPaytermDays(int $payterm_days) Return ChildSaleOrder objects filtered by the payterm_days column
 * @method     ChildSaleOrder[]|ObjectCollection findByPaytermOriginalId(int $payterm_original_id) Return ChildSaleOrder objects filtered by the payterm_original_id column
 * @method     ChildSaleOrder[]|ObjectCollection findByPaytermString(string $payterm_string) Return ChildSaleOrder objects filtered by the payterm_string column
 * @method     ChildSaleOrder[]|ObjectCollection findByOrderNumber(string $order_number) Return ChildSaleOrder objects filtered by the order_number column
 * @method     ChildSaleOrder[]|ObjectCollection findByItemDeleted(boolean $is_deleted) Return ChildSaleOrder objects filtered by the is_deleted column
 * @method     ChildSaleOrder[]|ObjectCollection findByCompanyName(string $company_name) Return ChildSaleOrder objects filtered by the company_name column
 * @method     ChildSaleOrder[]|ObjectCollection findByDebitor(string $debitor) Return ChildSaleOrder objects filtered by the debitor column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurCustomFolder(string $our_custom_folder) Return ChildSaleOrder objects filtered by the our_custom_folder column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurSlogan(string $our_slogan) Return ChildSaleOrder objects filtered by the our_slogan column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurPhone(string $our_phone) Return ChildSaleOrder objects filtered by the our_phone column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurFax(string $our_fax) Return ChildSaleOrder objects filtered by the our_fax column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurWebsite(string $our_website) Return ChildSaleOrder objects filtered by the our_website column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurEmail(string $our_email) Return ChildSaleOrder objects filtered by the our_email column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurVatNumber(string $our_vat_number) Return ChildSaleOrder objects filtered by the our_vat_number column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurChamberOfCommerce(string $our_chamber_of_commerce) Return ChildSaleOrder objects filtered by the our_chamber_of_commerce column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurIbanNumber(string $our_iban_number) Return ChildSaleOrder objects filtered by the our_iban_number column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurBicNumber(string $our_bic_number) Return ChildSaleOrder objects filtered by the our_bic_number column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurGeneralCompanyName(string $our_general_company_name) Return ChildSaleOrder objects filtered by the our_general_company_name column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurGeneralStreet(string $our_general_street) Return ChildSaleOrder objects filtered by the our_general_street column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurGeneralNumber(string $our_general_number) Return ChildSaleOrder objects filtered by the our_general_number column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurGeneralNumberAdd(string $our_general_number_add) Return ChildSaleOrder objects filtered by the our_general_number_add column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurGeneralPostal(string $our_general_postal) Return ChildSaleOrder objects filtered by the our_general_postal column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurGeneralPoBox(string $our_general_po_box) Return ChildSaleOrder objects filtered by the our_general_po_box column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurGeneralCity(string $our_general_city) Return ChildSaleOrder objects filtered by the our_general_city column
 * @method     ChildSaleOrder[]|ObjectCollection findByOurGeneralCountry(string $our_general_country) Return ChildSaleOrder objects filtered by the our_general_country column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerPhone(string $customer_phone) Return ChildSaleOrder objects filtered by the customer_phone column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerFax(string $customer_fax) Return ChildSaleOrder objects filtered by the customer_fax column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerEmail(string $customer_email) Return ChildSaleOrder objects filtered by the customer_email column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerChamberOfCommerce(string $customer_chamber_of_commerce) Return ChildSaleOrder objects filtered by the customer_chamber_of_commerce column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerVatNumber(string $customer_vat_number) Return ChildSaleOrder objects filtered by the customer_vat_number column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerOrderReference(string $customer_order_reference) Return ChildSaleOrder objects filtered by the customer_order_reference column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerOrderPlacedBy(string $customer_order_placed_by) Return ChildSaleOrder objects filtered by the customer_order_placed_by column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerInvoiceAddressId(int $customer_invoice_address_id) Return ChildSaleOrder objects filtered by the customer_invoice_address_id column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerInvoiceCompanyName(string $customer_invoice_company_name) Return ChildSaleOrder objects filtered by the customer_invoice_company_name column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerInvoiceAttnName(string $customer_invoice_attn_name) Return ChildSaleOrder objects filtered by the customer_invoice_attn_name column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerInvoiceStreet(string $customer_invoice_street) Return ChildSaleOrder objects filtered by the customer_invoice_street column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerInvoiceNumber(string $customer_invoice_number) Return ChildSaleOrder objects filtered by the customer_invoice_number column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerInvoiceNumberAdd(string $customer_invoice_number_add) Return ChildSaleOrder objects filtered by the customer_invoice_number_add column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerInvoicePostal(string $customer_invoice_postal) Return ChildSaleOrder objects filtered by the customer_invoice_postal column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerInvoiceCity(string $customer_invoice_city) Return ChildSaleOrder objects filtered by the customer_invoice_city column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerInvoiceCountry(string $customer_invoice_country) Return ChildSaleOrder objects filtered by the customer_invoice_country column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerInvoiceUsaState(string $customer_invoice_usa_state) Return ChildSaleOrder objects filtered by the customer_invoice_usa_state column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerInvoiceAddressL1(string $customer_invoice_address_l1) Return ChildSaleOrder objects filtered by the customer_invoice_address_l1 column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerInvoiceAddressL2(string $customer_invoice_address_l2) Return ChildSaleOrder objects filtered by the customer_invoice_address_l2 column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerDeliveryAddressId(int $customer_delivery_address_id) Return ChildSaleOrder objects filtered by the customer_delivery_address_id column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerDeliveryCompanyName(string $customer_delivery_company_name) Return ChildSaleOrder objects filtered by the customer_delivery_company_name column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerDeliveryAttnName(string $customer_delivery_attn_name) Return ChildSaleOrder objects filtered by the customer_delivery_attn_name column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerDeliveryStreet(string $customer_delivery_street) Return ChildSaleOrder objects filtered by the customer_delivery_street column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerDeliveryNumber(string $customer_delivery_number) Return ChildSaleOrder objects filtered by the customer_delivery_number column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerDeliveryNumberAdd(string $customer_delivery_number_add) Return ChildSaleOrder objects filtered by the customer_delivery_number_add column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerDeliveryPostal(string $customer_delivery_postal) Return ChildSaleOrder objects filtered by the customer_delivery_postal column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerDeliveryCity(string $customer_delivery_city) Return ChildSaleOrder objects filtered by the customer_delivery_city column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerDeliveryCountry(string $customer_delivery_country) Return ChildSaleOrder objects filtered by the customer_delivery_country column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerDeliveryUsaState(string $customer_delivery_usa_state) Return ChildSaleOrder objects filtered by the customer_delivery_usa_state column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerDeliveryAddressL1(string $customer_delivery_address_l1) Return ChildSaleOrder objects filtered by the customer_delivery_address_l1 column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerDeliveryAddressL2(string $customer_delivery_address_l2) Return ChildSaleOrder objects filtered by the customer_delivery_address_l2 column
 * @method     ChildSaleOrder[]|ObjectCollection findByCpFirstName(string $cp_first_name) Return ChildSaleOrder objects filtered by the cp_first_name column
 * @method     ChildSaleOrder[]|ObjectCollection findByCpLastName(string $cp_last_name) Return ChildSaleOrder objects filtered by the cp_last_name column
 * @method     ChildSaleOrder[]|ObjectCollection findByCpSallutation(string $cp_sallutation) Return ChildSaleOrder objects filtered by the cp_sallutation column
 * @method     ChildSaleOrder[]|ObjectCollection findByReference(string $reference) Return ChildSaleOrder objects filtered by the reference column
 * @method     ChildSaleOrder[]|ObjectCollection findByShippingNote(string $shipping_note) Return ChildSaleOrder objects filtered by the shipping_note column
 * @method     ChildSaleOrder[]|ObjectCollection findByCustomerOrderNote(string $customer_order_note) Return ChildSaleOrder objects filtered by the customer_order_note column
 * @method     ChildSaleOrder[]|ObjectCollection findByShippingOrderNote(string $shipping_order_note) Return ChildSaleOrder objects filtered by the shipping_order_note column
 * @method     ChildSaleOrder[]|ObjectCollection findByHasWrap(boolean $has_wrap) Return ChildSaleOrder objects filtered by the has_wrap column
 * @method     ChildSaleOrder[]|ObjectCollection findByWrappingPrice(double $wrapping_price) Return ChildSaleOrder objects filtered by the wrapping_price column
 * @method     ChildSaleOrder[]|ObjectCollection findByShippingCarrier(string $shipping_carrier) Return ChildSaleOrder objects filtered by the shipping_carrier column
 * @method     ChildSaleOrder[]|ObjectCollection findByShippingPrice(double $shipping_price) Return ChildSaleOrder objects filtered by the shipping_price column
 * @method     ChildSaleOrder[]|ObjectCollection findByShippingMethodId(int $shipping_method_id) Return ChildSaleOrder objects filtered by the shipping_method_id column
 * @method     ChildSaleOrder[]|ObjectCollection findByShippingHasTrackTrace(boolean $shipping_has_track_trace) Return ChildSaleOrder objects filtered by the shipping_has_track_trace column
 * @method     ChildSaleOrder[]|ObjectCollection findByShippingId(double $shipping_id) Return ChildSaleOrder objects filtered by the shipping_id column
 * @method     ChildSaleOrder[]|ObjectCollection findByTrackingCode(string $tracking_code) Return ChildSaleOrder objects filtered by the tracking_code column
 * @method     ChildSaleOrder[]|ObjectCollection findByTrackingUrl(string $tracking_url) Return ChildSaleOrder objects filtered by the tracking_url column
 * @method     ChildSaleOrder[]|ObjectCollection findByPaymethodName(string $paymethod_name) Return ChildSaleOrder objects filtered by the paymethod_name column
 * @method     ChildSaleOrder[]|ObjectCollection findByPaymethodCode(string $paymethod_code) Return ChildSaleOrder objects filtered by the paymethod_code column
 * @method     ChildSaleOrder[]|ObjectCollection findByPaymethodId(int $paymethod_id) Return ChildSaleOrder objects filtered by the paymethod_id column
 * @method     ChildSaleOrder[]|ObjectCollection findByShippingRegistrationDate(string $shipping_registration_date) Return ChildSaleOrder objects filtered by the shipping_registration_date column
 * @method     ChildSaleOrder[]|ObjectCollection findByInvoiceByPostal(boolean $invoice_by_postal) Return ChildSaleOrder objects filtered by the invoice_by_postal column
 * @method     ChildSaleOrder[]|ObjectCollection findByPackingSlipType(string $packing_slip_type) Return ChildSaleOrder objects filtered by the packing_slip_type column
 * @method     ChildSaleOrder[]|ObjectCollection findByWorkInProgress(boolean $work_in_progress) Return ChildSaleOrder objects filtered by the work_in_progress column
 * @method     ChildSaleOrder[]|ObjectCollection findByPackageAmount(int $package_amount) Return ChildSaleOrder objects filtered by the package_amount column
 * @method     ChildSaleOrder[]|ObjectCollection findByCouponCode(string $coupon_code) Return ChildSaleOrder objects filtered by the coupon_code column
 * @method     ChildSaleOrder[]|ObjectCollection findByCouponDiscountAmount(double $coupon_discount_amount) Return ChildSaleOrder objects filtered by the coupon_discount_amount column
 * @method     ChildSaleOrder[]|ObjectCollection findByCouponDiscountPercentage(double $coupon_discount_percentage) Return ChildSaleOrder objects filtered by the coupon_discount_percentage column
 * @method     ChildSaleOrder[]|ObjectCollection findByHideUntill(string $hide_untill) Return ChildSaleOrder objects filtered by the hide_untill column
 * @method     ChildSaleOrder[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SaleOrderQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Sale\Base\SaleOrderQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Sale\\SaleOrder', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSaleOrderQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSaleOrderQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSaleOrderQuery) {
            return $criteria;
        }
        $query = new ChildSaleOrderQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSaleOrder|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SaleOrderTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SaleOrderTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrder A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, site_id, language_id, is_closed, is_fully_paid, pay_date, is_ready_for_shipping, is_shipping_checked, is_fully_shipped, is_pushed_to_accounting, ship_date, sale_order_status_id, handled_by, source, is_completed, created_by_user_id, created_on, estimated_delivery_date, customer_id, envelope_weight_grams, webshop_user_session_id, invoice_number, invoice_date, payterm_days, payterm_original_id, payterm_string, order_number, is_deleted, company_name, debitor, our_custom_folder, our_slogan, our_phone, our_fax, our_website, our_email, our_vat_number, our_chamber_of_commerce, our_iban_number, our_bic_number, our_general_company_name, our_general_street, our_general_number, our_general_number_add, our_general_postal, our_general_po_box, our_general_city, our_general_country, customer_phone, customer_fax, customer_email, customer_chamber_of_commerce, customer_vat_number, customer_order_reference, customer_order_placed_by, customer_invoice_address_id, customer_invoice_company_name, customer_invoice_attn_name, customer_invoice_street, customer_invoice_number, customer_invoice_number_add, customer_invoice_postal, customer_invoice_city, customer_invoice_country, customer_invoice_usa_state, customer_invoice_address_l1, customer_invoice_address_l2, customer_delivery_address_id, customer_delivery_company_name, customer_delivery_attn_name, customer_delivery_street, customer_delivery_number, customer_delivery_number_add, customer_delivery_postal, customer_delivery_city, customer_delivery_country, customer_delivery_usa_state, customer_delivery_address_l1, customer_delivery_address_l2, cp_first_name, cp_last_name, cp_sallutation, reference, shipping_note, customer_order_note, shipping_order_note, has_wrap, wrapping_price, shipping_carrier, shipping_price, shipping_method_id, shipping_has_track_trace, shipping_id, tracking_code, tracking_url, paymethod_name, paymethod_code, paymethod_id, shipping_registration_date, invoice_by_postal, packing_slip_type, work_in_progress, package_amount, coupon_code, coupon_discount_amount, coupon_discount_percentage, hide_untill FROM sale_order WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSaleOrder $obj */
            $obj = new ChildSaleOrder();
            $obj->hydrate($row);
            SaleOrderTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSaleOrder|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SaleOrderTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SaleOrderTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the site_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE site_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE site_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE site_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_SITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the language_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguageId(1234); // WHERE language_id = 1234
     * $query->filterByLanguageId(array(12, 34)); // WHERE language_id IN (12, 34)
     * $query->filterByLanguageId(array('min' => 12)); // WHERE language_id > 12
     * </code>
     *
     * @param     mixed $languageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByLanguageId($languageId = null, $comparison = null)
    {
        if (is_array($languageId)) {
            $useMinMax = false;
            if (isset($languageId['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_LANGUAGE_ID, $languageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($languageId['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_LANGUAGE_ID, $languageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_LANGUAGE_ID, $languageId, $comparison);
    }

    /**
     * Filter the query on the is_closed column
     *
     * Example usage:
     * <code>
     * $query->filterByIsClosed(true); // WHERE is_closed = true
     * $query->filterByIsClosed('yes'); // WHERE is_closed = true
     * </code>
     *
     * @param     boolean|string $isClosed The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByIsClosed($isClosed = null, $comparison = null)
    {
        if (is_string($isClosed)) {
            $isClosed = in_array(strtolower($isClosed), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_IS_CLOSED, $isClosed, $comparison);
    }

    /**
     * Filter the query on the is_fully_paid column
     *
     * Example usage:
     * <code>
     * $query->filterByIsFullyPaid(true); // WHERE is_fully_paid = true
     * $query->filterByIsFullyPaid('yes'); // WHERE is_fully_paid = true
     * </code>
     *
     * @param     boolean|string $isFullyPaid The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByIsFullyPaid($isFullyPaid = null, $comparison = null)
    {
        if (is_string($isFullyPaid)) {
            $isFullyPaid = in_array(strtolower($isFullyPaid), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_IS_FULLY_PAID, $isFullyPaid, $comparison);
    }

    /**
     * Filter the query on the pay_date column
     *
     * Example usage:
     * <code>
     * $query->filterByPayDate('2011-03-14'); // WHERE pay_date = '2011-03-14'
     * $query->filterByPayDate('now'); // WHERE pay_date = '2011-03-14'
     * $query->filterByPayDate(array('max' => 'yesterday')); // WHERE pay_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $payDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByPayDate($payDate = null, $comparison = null)
    {
        if (is_array($payDate)) {
            $useMinMax = false;
            if (isset($payDate['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_PAY_DATE, $payDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($payDate['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_PAY_DATE, $payDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_PAY_DATE, $payDate, $comparison);
    }

    /**
     * Filter the query on the is_ready_for_shipping column
     *
     * Example usage:
     * <code>
     * $query->filterByIsReadyForShipping(true); // WHERE is_ready_for_shipping = true
     * $query->filterByIsReadyForShipping('yes'); // WHERE is_ready_for_shipping = true
     * </code>
     *
     * @param     boolean|string $isReadyForShipping The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByIsReadyForShipping($isReadyForShipping = null, $comparison = null)
    {
        if (is_string($isReadyForShipping)) {
            $isReadyForShipping = in_array(strtolower($isReadyForShipping), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_IS_READY_FOR_SHIPPING, $isReadyForShipping, $comparison);
    }

    /**
     * Filter the query on the is_shipping_checked column
     *
     * Example usage:
     * <code>
     * $query->filterByIsShippingChecked(true); // WHERE is_shipping_checked = true
     * $query->filterByIsShippingChecked('yes'); // WHERE is_shipping_checked = true
     * </code>
     *
     * @param     boolean|string $isShippingChecked The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByIsShippingChecked($isShippingChecked = null, $comparison = null)
    {
        if (is_string($isShippingChecked)) {
            $isShippingChecked = in_array(strtolower($isShippingChecked), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_IS_SHIPPING_CHECKED, $isShippingChecked, $comparison);
    }

    /**
     * Filter the query on the is_fully_shipped column
     *
     * Example usage:
     * <code>
     * $query->filterByIsFullyShipped(true); // WHERE is_fully_shipped = true
     * $query->filterByIsFullyShipped('yes'); // WHERE is_fully_shipped = true
     * </code>
     *
     * @param     boolean|string $isFullyShipped The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByIsFullyShipped($isFullyShipped = null, $comparison = null)
    {
        if (is_string($isFullyShipped)) {
            $isFullyShipped = in_array(strtolower($isFullyShipped), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_IS_FULLY_SHIPPED, $isFullyShipped, $comparison);
    }

    /**
     * Filter the query on the is_pushed_to_accounting column
     *
     * Example usage:
     * <code>
     * $query->filterByIsPushedToAccounting(true); // WHERE is_pushed_to_accounting = true
     * $query->filterByIsPushedToAccounting('yes'); // WHERE is_pushed_to_accounting = true
     * </code>
     *
     * @param     boolean|string $isPushedToAccounting The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByIsPushedToAccounting($isPushedToAccounting = null, $comparison = null)
    {
        if (is_string($isPushedToAccounting)) {
            $isPushedToAccounting = in_array(strtolower($isPushedToAccounting), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_IS_PUSHED_TO_ACCOUNTING, $isPushedToAccounting, $comparison);
    }

    /**
     * Filter the query on the ship_date column
     *
     * Example usage:
     * <code>
     * $query->filterByShipDate('2011-03-14'); // WHERE ship_date = '2011-03-14'
     * $query->filterByShipDate('now'); // WHERE ship_date = '2011-03-14'
     * $query->filterByShipDate(array('max' => 'yesterday')); // WHERE ship_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $shipDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByShipDate($shipDate = null, $comparison = null)
    {
        if (is_array($shipDate)) {
            $useMinMax = false;
            if (isset($shipDate['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SHIP_DATE, $shipDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shipDate['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SHIP_DATE, $shipDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_SHIP_DATE, $shipDate, $comparison);
    }

    /**
     * Filter the query on the sale_order_status_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySaleOrderStatusId(1234); // WHERE sale_order_status_id = 1234
     * $query->filterBySaleOrderStatusId(array(12, 34)); // WHERE sale_order_status_id IN (12, 34)
     * $query->filterBySaleOrderStatusId(array('min' => 12)); // WHERE sale_order_status_id > 12
     * </code>
     *
     * @see       filterBySaleOrderStatus()
     *
     * @param     mixed $saleOrderStatusId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterBySaleOrderStatusId($saleOrderStatusId = null, $comparison = null)
    {
        if (is_array($saleOrderStatusId)) {
            $useMinMax = false;
            if (isset($saleOrderStatusId['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SALE_ORDER_STATUS_ID, $saleOrderStatusId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($saleOrderStatusId['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SALE_ORDER_STATUS_ID, $saleOrderStatusId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_SALE_ORDER_STATUS_ID, $saleOrderStatusId, $comparison);
    }

    /**
     * Filter the query on the handled_by column
     *
     * Example usage:
     * <code>
     * $query->filterByHandledBy('fooValue');   // WHERE handled_by = 'fooValue'
     * $query->filterByHandledBy('%fooValue%', Criteria::LIKE); // WHERE handled_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $handledBy The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByHandledBy($handledBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($handledBy)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_HANDLED_BY, $handledBy, $comparison);
    }

    /**
     * Filter the query on the source column
     *
     * Example usage:
     * <code>
     * $query->filterBySource('fooValue');   // WHERE source = 'fooValue'
     * $query->filterBySource('%fooValue%', Criteria::LIKE); // WHERE source LIKE '%fooValue%'
     * </code>
     *
     * @param     string $source The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterBySource($source = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($source)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_SOURCE, $source, $comparison);
    }

    /**
     * Filter the query on the is_completed column
     *
     * Example usage:
     * <code>
     * $query->filterByIsCompleted(true); // WHERE is_completed = true
     * $query->filterByIsCompleted('yes'); // WHERE is_completed = true
     * </code>
     *
     * @param     boolean|string $isCompleted The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByIsCompleted($isCompleted = null, $comparison = null)
    {
        if (is_string($isCompleted)) {
            $isCompleted = in_array(strtolower($isCompleted), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_IS_COMPLETED, $isCompleted, $comparison);
    }

    /**
     * Filter the query on the created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE created_by_user_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the created_on column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedOn('2011-03-14'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn('now'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn(array('max' => 'yesterday')); // WHERE created_on > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdOn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCreatedOn($createdOn = null, $comparison = null)
    {
        if (is_array($createdOn)) {
            $useMinMax = false;
            if (isset($createdOn['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_CREATED_ON, $createdOn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdOn['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_CREATED_ON, $createdOn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CREATED_ON, $createdOn, $comparison);
    }

    /**
     * Filter the query on the estimated_delivery_date column
     *
     * Example usage:
     * <code>
     * $query->filterByEstimatedDeliveryDate('2011-03-14'); // WHERE estimated_delivery_date = '2011-03-14'
     * $query->filterByEstimatedDeliveryDate('now'); // WHERE estimated_delivery_date = '2011-03-14'
     * $query->filterByEstimatedDeliveryDate(array('max' => 'yesterday')); // WHERE estimated_delivery_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $estimatedDeliveryDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByEstimatedDeliveryDate($estimatedDeliveryDate = null, $comparison = null)
    {
        if (is_array($estimatedDeliveryDate)) {
            $useMinMax = false;
            if (isset($estimatedDeliveryDate['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_ESTIMATED_DELIVERY_DATE, $estimatedDeliveryDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($estimatedDeliveryDate['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_ESTIMATED_DELIVERY_DATE, $estimatedDeliveryDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_ESTIMATED_DELIVERY_DATE, $estimatedDeliveryDate, $comparison);
    }

    /**
     * Filter the query on the customer_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerId(1234); // WHERE customer_id = 1234
     * $query->filterByCustomerId(array(12, 34)); // WHERE customer_id IN (12, 34)
     * $query->filterByCustomerId(array('min' => 12)); // WHERE customer_id > 12
     * </code>
     *
     * @param     mixed $customerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerId($customerId = null, $comparison = null)
    {
        if (is_array($customerId)) {
            $useMinMax = false;
            if (isset($customerId['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_ID, $customerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerId['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_ID, $customerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_ID, $customerId, $comparison);
    }

    /**
     * Filter the query on the envelope_weight_grams column
     *
     * Example usage:
     * <code>
     * $query->filterByEnvelopeWeightGrams(1234); // WHERE envelope_weight_grams = 1234
     * $query->filterByEnvelopeWeightGrams(array(12, 34)); // WHERE envelope_weight_grams IN (12, 34)
     * $query->filterByEnvelopeWeightGrams(array('min' => 12)); // WHERE envelope_weight_grams > 12
     * </code>
     *
     * @param     mixed $envelopeWeightGrams The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByEnvelopeWeightGrams($envelopeWeightGrams = null, $comparison = null)
    {
        if (is_array($envelopeWeightGrams)) {
            $useMinMax = false;
            if (isset($envelopeWeightGrams['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_ENVELOPE_WEIGHT_GRAMS, $envelopeWeightGrams['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($envelopeWeightGrams['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_ENVELOPE_WEIGHT_GRAMS, $envelopeWeightGrams['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_ENVELOPE_WEIGHT_GRAMS, $envelopeWeightGrams, $comparison);
    }

    /**
     * Filter the query on the webshop_user_session_id column
     *
     * Example usage:
     * <code>
     * $query->filterByWebshopUserSessionId(1234); // WHERE webshop_user_session_id = 1234
     * $query->filterByWebshopUserSessionId(array(12, 34)); // WHERE webshop_user_session_id IN (12, 34)
     * $query->filterByWebshopUserSessionId(array('min' => 12)); // WHERE webshop_user_session_id > 12
     * </code>
     *
     * @param     mixed $webshopUserSessionId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByWebshopUserSessionId($webshopUserSessionId = null, $comparison = null)
    {
        if (is_array($webshopUserSessionId)) {
            $useMinMax = false;
            if (isset($webshopUserSessionId['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_WEBSHOP_USER_SESSION_ID, $webshopUserSessionId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($webshopUserSessionId['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_WEBSHOP_USER_SESSION_ID, $webshopUserSessionId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_WEBSHOP_USER_SESSION_ID, $webshopUserSessionId, $comparison);
    }

    /**
     * Filter the query on the invoice_number column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceNumber('fooValue');   // WHERE invoice_number = 'fooValue'
     * $query->filterByInvoiceNumber('%fooValue%', Criteria::LIKE); // WHERE invoice_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $invoiceNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByInvoiceNumber($invoiceNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($invoiceNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_INVOICE_NUMBER, $invoiceNumber, $comparison);
    }

    /**
     * Filter the query on the invoice_date column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceDate('2011-03-14'); // WHERE invoice_date = '2011-03-14'
     * $query->filterByInvoiceDate('now'); // WHERE invoice_date = '2011-03-14'
     * $query->filterByInvoiceDate(array('max' => 'yesterday')); // WHERE invoice_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $invoiceDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByInvoiceDate($invoiceDate = null, $comparison = null)
    {
        if (is_array($invoiceDate)) {
            $useMinMax = false;
            if (isset($invoiceDate['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_INVOICE_DATE, $invoiceDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($invoiceDate['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_INVOICE_DATE, $invoiceDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_INVOICE_DATE, $invoiceDate, $comparison);
    }

    /**
     * Filter the query on the payterm_days column
     *
     * Example usage:
     * <code>
     * $query->filterByPaytermDays(1234); // WHERE payterm_days = 1234
     * $query->filterByPaytermDays(array(12, 34)); // WHERE payterm_days IN (12, 34)
     * $query->filterByPaytermDays(array('min' => 12)); // WHERE payterm_days > 12
     * </code>
     *
     * @param     mixed $paytermDays The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByPaytermDays($paytermDays = null, $comparison = null)
    {
        if (is_array($paytermDays)) {
            $useMinMax = false;
            if (isset($paytermDays['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_PAYTERM_DAYS, $paytermDays['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($paytermDays['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_PAYTERM_DAYS, $paytermDays['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_PAYTERM_DAYS, $paytermDays, $comparison);
    }

    /**
     * Filter the query on the payterm_original_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPaytermOriginalId(1234); // WHERE payterm_original_id = 1234
     * $query->filterByPaytermOriginalId(array(12, 34)); // WHERE payterm_original_id IN (12, 34)
     * $query->filterByPaytermOriginalId(array('min' => 12)); // WHERE payterm_original_id > 12
     * </code>
     *
     * @param     mixed $paytermOriginalId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByPaytermOriginalId($paytermOriginalId = null, $comparison = null)
    {
        if (is_array($paytermOriginalId)) {
            $useMinMax = false;
            if (isset($paytermOriginalId['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_PAYTERM_ORIGINAL_ID, $paytermOriginalId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($paytermOriginalId['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_PAYTERM_ORIGINAL_ID, $paytermOriginalId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_PAYTERM_ORIGINAL_ID, $paytermOriginalId, $comparison);
    }

    /**
     * Filter the query on the payterm_string column
     *
     * Example usage:
     * <code>
     * $query->filterByPaytermString('fooValue');   // WHERE payterm_string = 'fooValue'
     * $query->filterByPaytermString('%fooValue%', Criteria::LIKE); // WHERE payterm_string LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paytermString The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByPaytermString($paytermString = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paytermString)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_PAYTERM_STRING, $paytermString, $comparison);
    }

    /**
     * Filter the query on the order_number column
     *
     * Example usage:
     * <code>
     * $query->filterByOrderNumber('fooValue');   // WHERE order_number = 'fooValue'
     * $query->filterByOrderNumber('%fooValue%', Criteria::LIKE); // WHERE order_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $orderNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOrderNumber($orderNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($orderNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_ORDER_NUMBER, $orderNumber, $comparison);
    }

    /**
     * Filter the query on the is_deleted column
     *
     * Example usage:
     * <code>
     * $query->filterByItemDeleted(true); // WHERE is_deleted = true
     * $query->filterByItemDeleted('yes'); // WHERE is_deleted = true
     * </code>
     *
     * @param     boolean|string $itemDeleted The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByItemDeleted($itemDeleted = null, $comparison = null)
    {
        if (is_string($itemDeleted)) {
            $itemDeleted = in_array(strtolower($itemDeleted), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_IS_DELETED, $itemDeleted, $comparison);
    }

    /**
     * Filter the query on the company_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCompanyName('fooValue');   // WHERE company_name = 'fooValue'
     * $query->filterByCompanyName('%fooValue%', Criteria::LIKE); // WHERE company_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $companyName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCompanyName($companyName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($companyName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_COMPANY_NAME, $companyName, $comparison);
    }

    /**
     * Filter the query on the debitor column
     *
     * Example usage:
     * <code>
     * $query->filterByDebitor('fooValue');   // WHERE debitor = 'fooValue'
     * $query->filterByDebitor('%fooValue%', Criteria::LIKE); // WHERE debitor LIKE '%fooValue%'
     * </code>
     *
     * @param     string $debitor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByDebitor($debitor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($debitor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_DEBITOR, $debitor, $comparison);
    }

    /**
     * Filter the query on the our_custom_folder column
     *
     * Example usage:
     * <code>
     * $query->filterByOurCustomFolder('fooValue');   // WHERE our_custom_folder = 'fooValue'
     * $query->filterByOurCustomFolder('%fooValue%', Criteria::LIKE); // WHERE our_custom_folder LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourCustomFolder The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurCustomFolder($ourCustomFolder = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourCustomFolder)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_CUSTOM_FOLDER, $ourCustomFolder, $comparison);
    }

    /**
     * Filter the query on the our_slogan column
     *
     * Example usage:
     * <code>
     * $query->filterByOurSlogan('fooValue');   // WHERE our_slogan = 'fooValue'
     * $query->filterByOurSlogan('%fooValue%', Criteria::LIKE); // WHERE our_slogan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourSlogan The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurSlogan($ourSlogan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourSlogan)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_SLOGAN, $ourSlogan, $comparison);
    }

    /**
     * Filter the query on the our_phone column
     *
     * Example usage:
     * <code>
     * $query->filterByOurPhone('fooValue');   // WHERE our_phone = 'fooValue'
     * $query->filterByOurPhone('%fooValue%', Criteria::LIKE); // WHERE our_phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourPhone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurPhone($ourPhone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourPhone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_PHONE, $ourPhone, $comparison);
    }

    /**
     * Filter the query on the our_fax column
     *
     * Example usage:
     * <code>
     * $query->filterByOurFax('fooValue');   // WHERE our_fax = 'fooValue'
     * $query->filterByOurFax('%fooValue%', Criteria::LIKE); // WHERE our_fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourFax The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurFax($ourFax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourFax)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_FAX, $ourFax, $comparison);
    }

    /**
     * Filter the query on the our_website column
     *
     * Example usage:
     * <code>
     * $query->filterByOurWebsite('fooValue');   // WHERE our_website = 'fooValue'
     * $query->filterByOurWebsite('%fooValue%', Criteria::LIKE); // WHERE our_website LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourWebsite The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurWebsite($ourWebsite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourWebsite)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_WEBSITE, $ourWebsite, $comparison);
    }

    /**
     * Filter the query on the our_email column
     *
     * Example usage:
     * <code>
     * $query->filterByOurEmail('fooValue');   // WHERE our_email = 'fooValue'
     * $query->filterByOurEmail('%fooValue%', Criteria::LIKE); // WHERE our_email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourEmail The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurEmail($ourEmail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourEmail)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_EMAIL, $ourEmail, $comparison);
    }

    /**
     * Filter the query on the our_vat_number column
     *
     * Example usage:
     * <code>
     * $query->filterByOurVatNumber('fooValue');   // WHERE our_vat_number = 'fooValue'
     * $query->filterByOurVatNumber('%fooValue%', Criteria::LIKE); // WHERE our_vat_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourVatNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurVatNumber($ourVatNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourVatNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_VAT_NUMBER, $ourVatNumber, $comparison);
    }

    /**
     * Filter the query on the our_chamber_of_commerce column
     *
     * Example usage:
     * <code>
     * $query->filterByOurChamberOfCommerce('fooValue');   // WHERE our_chamber_of_commerce = 'fooValue'
     * $query->filterByOurChamberOfCommerce('%fooValue%', Criteria::LIKE); // WHERE our_chamber_of_commerce LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourChamberOfCommerce The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurChamberOfCommerce($ourChamberOfCommerce = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourChamberOfCommerce)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_CHAMBER_OF_COMMERCE, $ourChamberOfCommerce, $comparison);
    }

    /**
     * Filter the query on the our_iban_number column
     *
     * Example usage:
     * <code>
     * $query->filterByOurIbanNumber('fooValue');   // WHERE our_iban_number = 'fooValue'
     * $query->filterByOurIbanNumber('%fooValue%', Criteria::LIKE); // WHERE our_iban_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourIbanNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurIbanNumber($ourIbanNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourIbanNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_IBAN_NUMBER, $ourIbanNumber, $comparison);
    }

    /**
     * Filter the query on the our_bic_number column
     *
     * Example usage:
     * <code>
     * $query->filterByOurBicNumber('fooValue');   // WHERE our_bic_number = 'fooValue'
     * $query->filterByOurBicNumber('%fooValue%', Criteria::LIKE); // WHERE our_bic_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourBicNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurBicNumber($ourBicNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourBicNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_BIC_NUMBER, $ourBicNumber, $comparison);
    }

    /**
     * Filter the query on the our_general_company_name column
     *
     * Example usage:
     * <code>
     * $query->filterByOurGeneralCompanyName('fooValue');   // WHERE our_general_company_name = 'fooValue'
     * $query->filterByOurGeneralCompanyName('%fooValue%', Criteria::LIKE); // WHERE our_general_company_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourGeneralCompanyName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurGeneralCompanyName($ourGeneralCompanyName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourGeneralCompanyName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_GENERAL_COMPANY_NAME, $ourGeneralCompanyName, $comparison);
    }

    /**
     * Filter the query on the our_general_street column
     *
     * Example usage:
     * <code>
     * $query->filterByOurGeneralStreet('fooValue');   // WHERE our_general_street = 'fooValue'
     * $query->filterByOurGeneralStreet('%fooValue%', Criteria::LIKE); // WHERE our_general_street LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourGeneralStreet The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurGeneralStreet($ourGeneralStreet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourGeneralStreet)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_GENERAL_STREET, $ourGeneralStreet, $comparison);
    }

    /**
     * Filter the query on the our_general_number column
     *
     * Example usage:
     * <code>
     * $query->filterByOurGeneralNumber('fooValue');   // WHERE our_general_number = 'fooValue'
     * $query->filterByOurGeneralNumber('%fooValue%', Criteria::LIKE); // WHERE our_general_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourGeneralNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurGeneralNumber($ourGeneralNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourGeneralNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_GENERAL_NUMBER, $ourGeneralNumber, $comparison);
    }

    /**
     * Filter the query on the our_general_number_add column
     *
     * Example usage:
     * <code>
     * $query->filterByOurGeneralNumberAdd('fooValue');   // WHERE our_general_number_add = 'fooValue'
     * $query->filterByOurGeneralNumberAdd('%fooValue%', Criteria::LIKE); // WHERE our_general_number_add LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourGeneralNumberAdd The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurGeneralNumberAdd($ourGeneralNumberAdd = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourGeneralNumberAdd)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_GENERAL_NUMBER_ADD, $ourGeneralNumberAdd, $comparison);
    }

    /**
     * Filter the query on the our_general_postal column
     *
     * Example usage:
     * <code>
     * $query->filterByOurGeneralPostal('fooValue');   // WHERE our_general_postal = 'fooValue'
     * $query->filterByOurGeneralPostal('%fooValue%', Criteria::LIKE); // WHERE our_general_postal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourGeneralPostal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurGeneralPostal($ourGeneralPostal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourGeneralPostal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_GENERAL_POSTAL, $ourGeneralPostal, $comparison);
    }

    /**
     * Filter the query on the our_general_po_box column
     *
     * Example usage:
     * <code>
     * $query->filterByOurGeneralPoBox('fooValue');   // WHERE our_general_po_box = 'fooValue'
     * $query->filterByOurGeneralPoBox('%fooValue%', Criteria::LIKE); // WHERE our_general_po_box LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourGeneralPoBox The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurGeneralPoBox($ourGeneralPoBox = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourGeneralPoBox)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_GENERAL_PO_BOX, $ourGeneralPoBox, $comparison);
    }

    /**
     * Filter the query on the our_general_city column
     *
     * Example usage:
     * <code>
     * $query->filterByOurGeneralCity('fooValue');   // WHERE our_general_city = 'fooValue'
     * $query->filterByOurGeneralCity('%fooValue%', Criteria::LIKE); // WHERE our_general_city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourGeneralCity The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurGeneralCity($ourGeneralCity = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourGeneralCity)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_GENERAL_CITY, $ourGeneralCity, $comparison);
    }

    /**
     * Filter the query on the our_general_country column
     *
     * Example usage:
     * <code>
     * $query->filterByOurGeneralCountry('fooValue');   // WHERE our_general_country = 'fooValue'
     * $query->filterByOurGeneralCountry('%fooValue%', Criteria::LIKE); // WHERE our_general_country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourGeneralCountry The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByOurGeneralCountry($ourGeneralCountry = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourGeneralCountry)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_OUR_GENERAL_COUNTRY, $ourGeneralCountry, $comparison);
    }

    /**
     * Filter the query on the customer_phone column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerPhone('fooValue');   // WHERE customer_phone = 'fooValue'
     * $query->filterByCustomerPhone('%fooValue%', Criteria::LIKE); // WHERE customer_phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerPhone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerPhone($customerPhone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerPhone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_PHONE, $customerPhone, $comparison);
    }

    /**
     * Filter the query on the customer_fax column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerFax('fooValue');   // WHERE customer_fax = 'fooValue'
     * $query->filterByCustomerFax('%fooValue%', Criteria::LIKE); // WHERE customer_fax LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerFax The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerFax($customerFax = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerFax)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_FAX, $customerFax, $comparison);
    }

    /**
     * Filter the query on the customer_email column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerEmail('fooValue');   // WHERE customer_email = 'fooValue'
     * $query->filterByCustomerEmail('%fooValue%', Criteria::LIKE); // WHERE customer_email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerEmail The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerEmail($customerEmail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerEmail)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_EMAIL, $customerEmail, $comparison);
    }

    /**
     * Filter the query on the customer_chamber_of_commerce column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerChamberOfCommerce('fooValue');   // WHERE customer_chamber_of_commerce = 'fooValue'
     * $query->filterByCustomerChamberOfCommerce('%fooValue%', Criteria::LIKE); // WHERE customer_chamber_of_commerce LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerChamberOfCommerce The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerChamberOfCommerce($customerChamberOfCommerce = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerChamberOfCommerce)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_CHAMBER_OF_COMMERCE, $customerChamberOfCommerce, $comparison);
    }

    /**
     * Filter the query on the customer_vat_number column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerVatNumber('fooValue');   // WHERE customer_vat_number = 'fooValue'
     * $query->filterByCustomerVatNumber('%fooValue%', Criteria::LIKE); // WHERE customer_vat_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerVatNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerVatNumber($customerVatNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerVatNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_VAT_NUMBER, $customerVatNumber, $comparison);
    }

    /**
     * Filter the query on the customer_order_reference column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerOrderReference('fooValue');   // WHERE customer_order_reference = 'fooValue'
     * $query->filterByCustomerOrderReference('%fooValue%', Criteria::LIKE); // WHERE customer_order_reference LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerOrderReference The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerOrderReference($customerOrderReference = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerOrderReference)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_ORDER_REFERENCE, $customerOrderReference, $comparison);
    }

    /**
     * Filter the query on the customer_order_placed_by column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerOrderPlacedBy('fooValue');   // WHERE customer_order_placed_by = 'fooValue'
     * $query->filterByCustomerOrderPlacedBy('%fooValue%', Criteria::LIKE); // WHERE customer_order_placed_by LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerOrderPlacedBy The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerOrderPlacedBy($customerOrderPlacedBy = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerOrderPlacedBy)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_ORDER_PLACED_BY, $customerOrderPlacedBy, $comparison);
    }

    /**
     * Filter the query on the customer_invoice_address_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerInvoiceAddressId(1234); // WHERE customer_invoice_address_id = 1234
     * $query->filterByCustomerInvoiceAddressId(array(12, 34)); // WHERE customer_invoice_address_id IN (12, 34)
     * $query->filterByCustomerInvoiceAddressId(array('min' => 12)); // WHERE customer_invoice_address_id > 12
     * </code>
     *
     * @param     mixed $customerInvoiceAddressId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerInvoiceAddressId($customerInvoiceAddressId = null, $comparison = null)
    {
        if (is_array($customerInvoiceAddressId)) {
            $useMinMax = false;
            if (isset($customerInvoiceAddressId['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_ID, $customerInvoiceAddressId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerInvoiceAddressId['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_ID, $customerInvoiceAddressId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_ID, $customerInvoiceAddressId, $comparison);
    }

    /**
     * Filter the query on the customer_invoice_company_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerInvoiceCompanyName('fooValue');   // WHERE customer_invoice_company_name = 'fooValue'
     * $query->filterByCustomerInvoiceCompanyName('%fooValue%', Criteria::LIKE); // WHERE customer_invoice_company_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerInvoiceCompanyName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerInvoiceCompanyName($customerInvoiceCompanyName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerInvoiceCompanyName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_COMPANY_NAME, $customerInvoiceCompanyName, $comparison);
    }

    /**
     * Filter the query on the customer_invoice_attn_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerInvoiceAttnName('fooValue');   // WHERE customer_invoice_attn_name = 'fooValue'
     * $query->filterByCustomerInvoiceAttnName('%fooValue%', Criteria::LIKE); // WHERE customer_invoice_attn_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerInvoiceAttnName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerInvoiceAttnName($customerInvoiceAttnName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerInvoiceAttnName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ATTN_NAME, $customerInvoiceAttnName, $comparison);
    }

    /**
     * Filter the query on the customer_invoice_street column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerInvoiceStreet('fooValue');   // WHERE customer_invoice_street = 'fooValue'
     * $query->filterByCustomerInvoiceStreet('%fooValue%', Criteria::LIKE); // WHERE customer_invoice_street LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerInvoiceStreet The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerInvoiceStreet($customerInvoiceStreet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerInvoiceStreet)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_STREET, $customerInvoiceStreet, $comparison);
    }

    /**
     * Filter the query on the customer_invoice_number column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerInvoiceNumber('fooValue');   // WHERE customer_invoice_number = 'fooValue'
     * $query->filterByCustomerInvoiceNumber('%fooValue%', Criteria::LIKE); // WHERE customer_invoice_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerInvoiceNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerInvoiceNumber($customerInvoiceNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerInvoiceNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER, $customerInvoiceNumber, $comparison);
    }

    /**
     * Filter the query on the customer_invoice_number_add column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerInvoiceNumberAdd('fooValue');   // WHERE customer_invoice_number_add = 'fooValue'
     * $query->filterByCustomerInvoiceNumberAdd('%fooValue%', Criteria::LIKE); // WHERE customer_invoice_number_add LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerInvoiceNumberAdd The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerInvoiceNumberAdd($customerInvoiceNumberAdd = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerInvoiceNumberAdd)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER_ADD, $customerInvoiceNumberAdd, $comparison);
    }

    /**
     * Filter the query on the customer_invoice_postal column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerInvoicePostal('fooValue');   // WHERE customer_invoice_postal = 'fooValue'
     * $query->filterByCustomerInvoicePostal('%fooValue%', Criteria::LIKE); // WHERE customer_invoice_postal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerInvoicePostal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerInvoicePostal($customerInvoicePostal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerInvoicePostal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_POSTAL, $customerInvoicePostal, $comparison);
    }

    /**
     * Filter the query on the customer_invoice_city column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerInvoiceCity('fooValue');   // WHERE customer_invoice_city = 'fooValue'
     * $query->filterByCustomerInvoiceCity('%fooValue%', Criteria::LIKE); // WHERE customer_invoice_city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerInvoiceCity The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerInvoiceCity($customerInvoiceCity = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerInvoiceCity)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_CITY, $customerInvoiceCity, $comparison);
    }

    /**
     * Filter the query on the customer_invoice_country column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerInvoiceCountry('fooValue');   // WHERE customer_invoice_country = 'fooValue'
     * $query->filterByCustomerInvoiceCountry('%fooValue%', Criteria::LIKE); // WHERE customer_invoice_country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerInvoiceCountry The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerInvoiceCountry($customerInvoiceCountry = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerInvoiceCountry)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_COUNTRY, $customerInvoiceCountry, $comparison);
    }

    /**
     * Filter the query on the customer_invoice_usa_state column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerInvoiceUsaState('fooValue');   // WHERE customer_invoice_usa_state = 'fooValue'
     * $query->filterByCustomerInvoiceUsaState('%fooValue%', Criteria::LIKE); // WHERE customer_invoice_usa_state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerInvoiceUsaState The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerInvoiceUsaState($customerInvoiceUsaState = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerInvoiceUsaState)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_USA_STATE, $customerInvoiceUsaState, $comparison);
    }

    /**
     * Filter the query on the customer_invoice_address_l1 column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerInvoiceAddressL1('fooValue');   // WHERE customer_invoice_address_l1 = 'fooValue'
     * $query->filterByCustomerInvoiceAddressL1('%fooValue%', Criteria::LIKE); // WHERE customer_invoice_address_l1 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerInvoiceAddressL1 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerInvoiceAddressL1($customerInvoiceAddressL1 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerInvoiceAddressL1)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L1, $customerInvoiceAddressL1, $comparison);
    }

    /**
     * Filter the query on the customer_invoice_address_l2 column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerInvoiceAddressL2('fooValue');   // WHERE customer_invoice_address_l2 = 'fooValue'
     * $query->filterByCustomerInvoiceAddressL2('%fooValue%', Criteria::LIKE); // WHERE customer_invoice_address_l2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerInvoiceAddressL2 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerInvoiceAddressL2($customerInvoiceAddressL2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerInvoiceAddressL2)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L2, $customerInvoiceAddressL2, $comparison);
    }

    /**
     * Filter the query on the customer_delivery_address_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerDeliveryAddressId(1234); // WHERE customer_delivery_address_id = 1234
     * $query->filterByCustomerDeliveryAddressId(array(12, 34)); // WHERE customer_delivery_address_id IN (12, 34)
     * $query->filterByCustomerDeliveryAddressId(array('min' => 12)); // WHERE customer_delivery_address_id > 12
     * </code>
     *
     * @param     mixed $customerDeliveryAddressId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerDeliveryAddressId($customerDeliveryAddressId = null, $comparison = null)
    {
        if (is_array($customerDeliveryAddressId)) {
            $useMinMax = false;
            if (isset($customerDeliveryAddressId['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_ID, $customerDeliveryAddressId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerDeliveryAddressId['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_ID, $customerDeliveryAddressId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_ID, $customerDeliveryAddressId, $comparison);
    }

    /**
     * Filter the query on the customer_delivery_company_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerDeliveryCompanyName('fooValue');   // WHERE customer_delivery_company_name = 'fooValue'
     * $query->filterByCustomerDeliveryCompanyName('%fooValue%', Criteria::LIKE); // WHERE customer_delivery_company_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerDeliveryCompanyName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerDeliveryCompanyName($customerDeliveryCompanyName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerDeliveryCompanyName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COMPANY_NAME, $customerDeliveryCompanyName, $comparison);
    }

    /**
     * Filter the query on the customer_delivery_attn_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerDeliveryAttnName('fooValue');   // WHERE customer_delivery_attn_name = 'fooValue'
     * $query->filterByCustomerDeliveryAttnName('%fooValue%', Criteria::LIKE); // WHERE customer_delivery_attn_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerDeliveryAttnName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerDeliveryAttnName($customerDeliveryAttnName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerDeliveryAttnName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ATTN_NAME, $customerDeliveryAttnName, $comparison);
    }

    /**
     * Filter the query on the customer_delivery_street column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerDeliveryStreet('fooValue');   // WHERE customer_delivery_street = 'fooValue'
     * $query->filterByCustomerDeliveryStreet('%fooValue%', Criteria::LIKE); // WHERE customer_delivery_street LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerDeliveryStreet The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerDeliveryStreet($customerDeliveryStreet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerDeliveryStreet)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_STREET, $customerDeliveryStreet, $comparison);
    }

    /**
     * Filter the query on the customer_delivery_number column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerDeliveryNumber('fooValue');   // WHERE customer_delivery_number = 'fooValue'
     * $query->filterByCustomerDeliveryNumber('%fooValue%', Criteria::LIKE); // WHERE customer_delivery_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerDeliveryNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerDeliveryNumber($customerDeliveryNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerDeliveryNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER, $customerDeliveryNumber, $comparison);
    }

    /**
     * Filter the query on the customer_delivery_number_add column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerDeliveryNumberAdd('fooValue');   // WHERE customer_delivery_number_add = 'fooValue'
     * $query->filterByCustomerDeliveryNumberAdd('%fooValue%', Criteria::LIKE); // WHERE customer_delivery_number_add LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerDeliveryNumberAdd The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerDeliveryNumberAdd($customerDeliveryNumberAdd = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerDeliveryNumberAdd)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER_ADD, $customerDeliveryNumberAdd, $comparison);
    }

    /**
     * Filter the query on the customer_delivery_postal column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerDeliveryPostal('fooValue');   // WHERE customer_delivery_postal = 'fooValue'
     * $query->filterByCustomerDeliveryPostal('%fooValue%', Criteria::LIKE); // WHERE customer_delivery_postal LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerDeliveryPostal The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerDeliveryPostal($customerDeliveryPostal = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerDeliveryPostal)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_POSTAL, $customerDeliveryPostal, $comparison);
    }

    /**
     * Filter the query on the customer_delivery_city column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerDeliveryCity('fooValue');   // WHERE customer_delivery_city = 'fooValue'
     * $query->filterByCustomerDeliveryCity('%fooValue%', Criteria::LIKE); // WHERE customer_delivery_city LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerDeliveryCity The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerDeliveryCity($customerDeliveryCity = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerDeliveryCity)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_CITY, $customerDeliveryCity, $comparison);
    }

    /**
     * Filter the query on the customer_delivery_country column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerDeliveryCountry('fooValue');   // WHERE customer_delivery_country = 'fooValue'
     * $query->filterByCustomerDeliveryCountry('%fooValue%', Criteria::LIKE); // WHERE customer_delivery_country LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerDeliveryCountry The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerDeliveryCountry($customerDeliveryCountry = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerDeliveryCountry)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COUNTRY, $customerDeliveryCountry, $comparison);
    }

    /**
     * Filter the query on the customer_delivery_usa_state column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerDeliveryUsaState('fooValue');   // WHERE customer_delivery_usa_state = 'fooValue'
     * $query->filterByCustomerDeliveryUsaState('%fooValue%', Criteria::LIKE); // WHERE customer_delivery_usa_state LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerDeliveryUsaState The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerDeliveryUsaState($customerDeliveryUsaState = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerDeliveryUsaState)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_USA_STATE, $customerDeliveryUsaState, $comparison);
    }

    /**
     * Filter the query on the customer_delivery_address_l1 column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerDeliveryAddressL1('fooValue');   // WHERE customer_delivery_address_l1 = 'fooValue'
     * $query->filterByCustomerDeliveryAddressL1('%fooValue%', Criteria::LIKE); // WHERE customer_delivery_address_l1 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerDeliveryAddressL1 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerDeliveryAddressL1($customerDeliveryAddressL1 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerDeliveryAddressL1)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L1, $customerDeliveryAddressL1, $comparison);
    }

    /**
     * Filter the query on the customer_delivery_address_l2 column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerDeliveryAddressL2('fooValue');   // WHERE customer_delivery_address_l2 = 'fooValue'
     * $query->filterByCustomerDeliveryAddressL2('%fooValue%', Criteria::LIKE); // WHERE customer_delivery_address_l2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerDeliveryAddressL2 The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerDeliveryAddressL2($customerDeliveryAddressL2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerDeliveryAddressL2)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L2, $customerDeliveryAddressL2, $comparison);
    }

    /**
     * Filter the query on the cp_first_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCpFirstName('fooValue');   // WHERE cp_first_name = 'fooValue'
     * $query->filterByCpFirstName('%fooValue%', Criteria::LIKE); // WHERE cp_first_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cpFirstName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCpFirstName($cpFirstName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cpFirstName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CP_FIRST_NAME, $cpFirstName, $comparison);
    }

    /**
     * Filter the query on the cp_last_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCpLastName('fooValue');   // WHERE cp_last_name = 'fooValue'
     * $query->filterByCpLastName('%fooValue%', Criteria::LIKE); // WHERE cp_last_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cpLastName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCpLastName($cpLastName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cpLastName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CP_LAST_NAME, $cpLastName, $comparison);
    }

    /**
     * Filter the query on the cp_sallutation column
     *
     * Example usage:
     * <code>
     * $query->filterByCpSallutation('fooValue');   // WHERE cp_sallutation = 'fooValue'
     * $query->filterByCpSallutation('%fooValue%', Criteria::LIKE); // WHERE cp_sallutation LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cpSallutation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCpSallutation($cpSallutation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cpSallutation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CP_SALLUTATION, $cpSallutation, $comparison);
    }

    /**
     * Filter the query on the reference column
     *
     * Example usage:
     * <code>
     * $query->filterByReference('fooValue');   // WHERE reference = 'fooValue'
     * $query->filterByReference('%fooValue%', Criteria::LIKE); // WHERE reference LIKE '%fooValue%'
     * </code>
     *
     * @param     string $reference The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByReference($reference = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($reference)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_REFERENCE, $reference, $comparison);
    }

    /**
     * Filter the query on the shipping_note column
     *
     * Example usage:
     * <code>
     * $query->filterByShippingNote('fooValue');   // WHERE shipping_note = 'fooValue'
     * $query->filterByShippingNote('%fooValue%', Criteria::LIKE); // WHERE shipping_note LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shippingNote The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByShippingNote($shippingNote = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shippingNote)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_NOTE, $shippingNote, $comparison);
    }

    /**
     * Filter the query on the customer_order_note column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerOrderNote('fooValue');   // WHERE customer_order_note = 'fooValue'
     * $query->filterByCustomerOrderNote('%fooValue%', Criteria::LIKE); // WHERE customer_order_note LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customerOrderNote The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCustomerOrderNote($customerOrderNote = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customerOrderNote)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_CUSTOMER_ORDER_NOTE, $customerOrderNote, $comparison);
    }

    /**
     * Filter the query on the shipping_order_note column
     *
     * Example usage:
     * <code>
     * $query->filterByShippingOrderNote('fooValue');   // WHERE shipping_order_note = 'fooValue'
     * $query->filterByShippingOrderNote('%fooValue%', Criteria::LIKE); // WHERE shipping_order_note LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shippingOrderNote The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByShippingOrderNote($shippingOrderNote = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shippingOrderNote)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_ORDER_NOTE, $shippingOrderNote, $comparison);
    }

    /**
     * Filter the query on the has_wrap column
     *
     * Example usage:
     * <code>
     * $query->filterByHasWrap(true); // WHERE has_wrap = true
     * $query->filterByHasWrap('yes'); // WHERE has_wrap = true
     * </code>
     *
     * @param     boolean|string $hasWrap The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByHasWrap($hasWrap = null, $comparison = null)
    {
        if (is_string($hasWrap)) {
            $hasWrap = in_array(strtolower($hasWrap), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_HAS_WRAP, $hasWrap, $comparison);
    }

    /**
     * Filter the query on the wrapping_price column
     *
     * Example usage:
     * <code>
     * $query->filterByWrappingPrice(1234); // WHERE wrapping_price = 1234
     * $query->filterByWrappingPrice(array(12, 34)); // WHERE wrapping_price IN (12, 34)
     * $query->filterByWrappingPrice(array('min' => 12)); // WHERE wrapping_price > 12
     * </code>
     *
     * @param     mixed $wrappingPrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByWrappingPrice($wrappingPrice = null, $comparison = null)
    {
        if (is_array($wrappingPrice)) {
            $useMinMax = false;
            if (isset($wrappingPrice['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_WRAPPING_PRICE, $wrappingPrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($wrappingPrice['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_WRAPPING_PRICE, $wrappingPrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_WRAPPING_PRICE, $wrappingPrice, $comparison);
    }

    /**
     * Filter the query on the shipping_carrier column
     *
     * Example usage:
     * <code>
     * $query->filterByShippingCarrier('fooValue');   // WHERE shipping_carrier = 'fooValue'
     * $query->filterByShippingCarrier('%fooValue%', Criteria::LIKE); // WHERE shipping_carrier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shippingCarrier The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByShippingCarrier($shippingCarrier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shippingCarrier)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_CARRIER, $shippingCarrier, $comparison);
    }

    /**
     * Filter the query on the shipping_price column
     *
     * Example usage:
     * <code>
     * $query->filterByShippingPrice(1234); // WHERE shipping_price = 1234
     * $query->filterByShippingPrice(array(12, 34)); // WHERE shipping_price IN (12, 34)
     * $query->filterByShippingPrice(array('min' => 12)); // WHERE shipping_price > 12
     * </code>
     *
     * @param     mixed $shippingPrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByShippingPrice($shippingPrice = null, $comparison = null)
    {
        if (is_array($shippingPrice)) {
            $useMinMax = false;
            if (isset($shippingPrice['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_PRICE, $shippingPrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shippingPrice['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_PRICE, $shippingPrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_PRICE, $shippingPrice, $comparison);
    }

    /**
     * Filter the query on the shipping_method_id column
     *
     * Example usage:
     * <code>
     * $query->filterByShippingMethodId(1234); // WHERE shipping_method_id = 1234
     * $query->filterByShippingMethodId(array(12, 34)); // WHERE shipping_method_id IN (12, 34)
     * $query->filterByShippingMethodId(array('min' => 12)); // WHERE shipping_method_id > 12
     * </code>
     *
     * @see       filterByShippingMethod()
     *
     * @param     mixed $shippingMethodId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByShippingMethodId($shippingMethodId = null, $comparison = null)
    {
        if (is_array($shippingMethodId)) {
            $useMinMax = false;
            if (isset($shippingMethodId['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_METHOD_ID, $shippingMethodId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shippingMethodId['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_METHOD_ID, $shippingMethodId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_METHOD_ID, $shippingMethodId, $comparison);
    }

    /**
     * Filter the query on the shipping_has_track_trace column
     *
     * Example usage:
     * <code>
     * $query->filterByShippingHasTrackTrace(true); // WHERE shipping_has_track_trace = true
     * $query->filterByShippingHasTrackTrace('yes'); // WHERE shipping_has_track_trace = true
     * </code>
     *
     * @param     boolean|string $shippingHasTrackTrace The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByShippingHasTrackTrace($shippingHasTrackTrace = null, $comparison = null)
    {
        if (is_string($shippingHasTrackTrace)) {
            $shippingHasTrackTrace = in_array(strtolower($shippingHasTrackTrace), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_HAS_TRACK_TRACE, $shippingHasTrackTrace, $comparison);
    }

    /**
     * Filter the query on the shipping_id column
     *
     * Example usage:
     * <code>
     * $query->filterByShippingId(1234); // WHERE shipping_id = 1234
     * $query->filterByShippingId(array(12, 34)); // WHERE shipping_id IN (12, 34)
     * $query->filterByShippingId(array('min' => 12)); // WHERE shipping_id > 12
     * </code>
     *
     * @param     mixed $shippingId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByShippingId($shippingId = null, $comparison = null)
    {
        if (is_array($shippingId)) {
            $useMinMax = false;
            if (isset($shippingId['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_ID, $shippingId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shippingId['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_ID, $shippingId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_ID, $shippingId, $comparison);
    }

    /**
     * Filter the query on the tracking_code column
     *
     * Example usage:
     * <code>
     * $query->filterByTrackingCode('fooValue');   // WHERE tracking_code = 'fooValue'
     * $query->filterByTrackingCode('%fooValue%', Criteria::LIKE); // WHERE tracking_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $trackingCode The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByTrackingCode($trackingCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($trackingCode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_TRACKING_CODE, $trackingCode, $comparison);
    }

    /**
     * Filter the query on the tracking_url column
     *
     * Example usage:
     * <code>
     * $query->filterByTrackingUrl('fooValue');   // WHERE tracking_url = 'fooValue'
     * $query->filterByTrackingUrl('%fooValue%', Criteria::LIKE); // WHERE tracking_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $trackingUrl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByTrackingUrl($trackingUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($trackingUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_TRACKING_URL, $trackingUrl, $comparison);
    }

    /**
     * Filter the query on the paymethod_name column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymethodName('fooValue');   // WHERE paymethod_name = 'fooValue'
     * $query->filterByPaymethodName('%fooValue%', Criteria::LIKE); // WHERE paymethod_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymethodName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByPaymethodName($paymethodName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymethodName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_PAYMETHOD_NAME, $paymethodName, $comparison);
    }

    /**
     * Filter the query on the paymethod_code column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymethodCode('fooValue');   // WHERE paymethod_code = 'fooValue'
     * $query->filterByPaymethodCode('%fooValue%', Criteria::LIKE); // WHERE paymethod_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymethodCode The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByPaymethodCode($paymethodCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymethodCode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_PAYMETHOD_CODE, $paymethodCode, $comparison);
    }

    /**
     * Filter the query on the paymethod_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymethodId(1234); // WHERE paymethod_id = 1234
     * $query->filterByPaymethodId(array(12, 34)); // WHERE paymethod_id IN (12, 34)
     * $query->filterByPaymethodId(array('min' => 12)); // WHERE paymethod_id > 12
     * </code>
     *
     * @param     mixed $paymethodId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByPaymethodId($paymethodId = null, $comparison = null)
    {
        if (is_array($paymethodId)) {
            $useMinMax = false;
            if (isset($paymethodId['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_PAYMETHOD_ID, $paymethodId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($paymethodId['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_PAYMETHOD_ID, $paymethodId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_PAYMETHOD_ID, $paymethodId, $comparison);
    }

    /**
     * Filter the query on the shipping_registration_date column
     *
     * Example usage:
     * <code>
     * $query->filterByShippingRegistrationDate('2011-03-14'); // WHERE shipping_registration_date = '2011-03-14'
     * $query->filterByShippingRegistrationDate('now'); // WHERE shipping_registration_date = '2011-03-14'
     * $query->filterByShippingRegistrationDate(array('max' => 'yesterday')); // WHERE shipping_registration_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $shippingRegistrationDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByShippingRegistrationDate($shippingRegistrationDate = null, $comparison = null)
    {
        if (is_array($shippingRegistrationDate)) {
            $useMinMax = false;
            if (isset($shippingRegistrationDate['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_REGISTRATION_DATE, $shippingRegistrationDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($shippingRegistrationDate['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_REGISTRATION_DATE, $shippingRegistrationDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_REGISTRATION_DATE, $shippingRegistrationDate, $comparison);
    }

    /**
     * Filter the query on the invoice_by_postal column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoiceByPostal(true); // WHERE invoice_by_postal = true
     * $query->filterByInvoiceByPostal('yes'); // WHERE invoice_by_postal = true
     * </code>
     *
     * @param     boolean|string $invoiceByPostal The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByInvoiceByPostal($invoiceByPostal = null, $comparison = null)
    {
        if (is_string($invoiceByPostal)) {
            $invoiceByPostal = in_array(strtolower($invoiceByPostal), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_INVOICE_BY_POSTAL, $invoiceByPostal, $comparison);
    }

    /**
     * Filter the query on the packing_slip_type column
     *
     * Example usage:
     * <code>
     * $query->filterByPackingSlipType('fooValue');   // WHERE packing_slip_type = 'fooValue'
     * $query->filterByPackingSlipType('%fooValue%', Criteria::LIKE); // WHERE packing_slip_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $packingSlipType The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByPackingSlipType($packingSlipType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($packingSlipType)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_PACKING_SLIP_TYPE, $packingSlipType, $comparison);
    }

    /**
     * Filter the query on the work_in_progress column
     *
     * Example usage:
     * <code>
     * $query->filterByWorkInProgress(true); // WHERE work_in_progress = true
     * $query->filterByWorkInProgress('yes'); // WHERE work_in_progress = true
     * </code>
     *
     * @param     boolean|string $workInProgress The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByWorkInProgress($workInProgress = null, $comparison = null)
    {
        if (is_string($workInProgress)) {
            $workInProgress = in_array(strtolower($workInProgress), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_WORK_IN_PROGRESS, $workInProgress, $comparison);
    }

    /**
     * Filter the query on the package_amount column
     *
     * Example usage:
     * <code>
     * $query->filterByPackageAmount(1234); // WHERE package_amount = 1234
     * $query->filterByPackageAmount(array(12, 34)); // WHERE package_amount IN (12, 34)
     * $query->filterByPackageAmount(array('min' => 12)); // WHERE package_amount > 12
     * </code>
     *
     * @param     mixed $packageAmount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByPackageAmount($packageAmount = null, $comparison = null)
    {
        if (is_array($packageAmount)) {
            $useMinMax = false;
            if (isset($packageAmount['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_PACKAGE_AMOUNT, $packageAmount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($packageAmount['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_PACKAGE_AMOUNT, $packageAmount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_PACKAGE_AMOUNT, $packageAmount, $comparison);
    }

    /**
     * Filter the query on the coupon_code column
     *
     * Example usage:
     * <code>
     * $query->filterByCouponCode('fooValue');   // WHERE coupon_code = 'fooValue'
     * $query->filterByCouponCode('%fooValue%', Criteria::LIKE); // WHERE coupon_code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $couponCode The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCouponCode($couponCode = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($couponCode)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_COUPON_CODE, $couponCode, $comparison);
    }

    /**
     * Filter the query on the coupon_discount_amount column
     *
     * Example usage:
     * <code>
     * $query->filterByCouponDiscountAmount(1234); // WHERE coupon_discount_amount = 1234
     * $query->filterByCouponDiscountAmount(array(12, 34)); // WHERE coupon_discount_amount IN (12, 34)
     * $query->filterByCouponDiscountAmount(array('min' => 12)); // WHERE coupon_discount_amount > 12
     * </code>
     *
     * @param     mixed $couponDiscountAmount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCouponDiscountAmount($couponDiscountAmount = null, $comparison = null)
    {
        if (is_array($couponDiscountAmount)) {
            $useMinMax = false;
            if (isset($couponDiscountAmount['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_COUPON_DISCOUNT_AMOUNT, $couponDiscountAmount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($couponDiscountAmount['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_COUPON_DISCOUNT_AMOUNT, $couponDiscountAmount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_COUPON_DISCOUNT_AMOUNT, $couponDiscountAmount, $comparison);
    }

    /**
     * Filter the query on the coupon_discount_percentage column
     *
     * Example usage:
     * <code>
     * $query->filterByCouponDiscountPercentage(1234); // WHERE coupon_discount_percentage = 1234
     * $query->filterByCouponDiscountPercentage(array(12, 34)); // WHERE coupon_discount_percentage IN (12, 34)
     * $query->filterByCouponDiscountPercentage(array('min' => 12)); // WHERE coupon_discount_percentage > 12
     * </code>
     *
     * @param     mixed $couponDiscountPercentage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByCouponDiscountPercentage($couponDiscountPercentage = null, $comparison = null)
    {
        if (is_array($couponDiscountPercentage)) {
            $useMinMax = false;
            if (isset($couponDiscountPercentage['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_COUPON_DISCOUNT_PERCENTAGE, $couponDiscountPercentage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($couponDiscountPercentage['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_COUPON_DISCOUNT_PERCENTAGE, $couponDiscountPercentage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_COUPON_DISCOUNT_PERCENTAGE, $couponDiscountPercentage, $comparison);
    }

    /**
     * Filter the query on the hide_untill column
     *
     * Example usage:
     * <code>
     * $query->filterByHideUntill('2011-03-14'); // WHERE hide_untill = '2011-03-14'
     * $query->filterByHideUntill('now'); // WHERE hide_untill = '2011-03-14'
     * $query->filterByHideUntill(array('max' => 'yesterday')); // WHERE hide_untill > '2011-03-13'
     * </code>
     *
     * @param     mixed $hideUntill The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByHideUntill($hideUntill = null, $comparison = null)
    {
        if (is_array($hideUntill)) {
            $useMinMax = false;
            if (isset($hideUntill['min'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_HIDE_UNTILL, $hideUntill['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($hideUntill['max'])) {
                $this->addUsingAlias(SaleOrderTableMap::COL_HIDE_UNTILL, $hideUntill['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderTableMap::COL_HIDE_UNTILL, $hideUntill, $comparison);
    }

    /**
     * Filter the query by a related \Model\Cms\Site object
     *
     * @param \Model\Cms\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \Model\Cms\Site) {
            return $this
                ->addUsingAlias(SaleOrderTableMap::COL_SITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaleOrderTableMap::COL_SITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \Model\Cms\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\Model\Cms\SiteQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\SaleOrderStatus object
     *
     * @param \Model\Setting\MasterTable\SaleOrderStatus|ObjectCollection $saleOrderStatus The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterBySaleOrderStatus($saleOrderStatus, $comparison = null)
    {
        if ($saleOrderStatus instanceof \Model\Setting\MasterTable\SaleOrderStatus) {
            return $this
                ->addUsingAlias(SaleOrderTableMap::COL_SALE_ORDER_STATUS_ID, $saleOrderStatus->getId(), $comparison);
        } elseif ($saleOrderStatus instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaleOrderTableMap::COL_SALE_ORDER_STATUS_ID, $saleOrderStatus->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySaleOrderStatus() only accepts arguments of type \Model\Setting\MasterTable\SaleOrderStatus or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrderStatus relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function joinSaleOrderStatus($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrderStatus');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrderStatus');
        }

        return $this;
    }

    /**
     * Use the SaleOrderStatus relation SaleOrderStatus object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\SaleOrderStatusQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderStatusQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaleOrderStatus($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrderStatus', '\Model\Setting\MasterTable\SaleOrderStatusQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\ShippingMethod object
     *
     * @param \Model\Setting\MasterTable\ShippingMethod|ObjectCollection $shippingMethod The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByShippingMethod($shippingMethod, $comparison = null)
    {
        if ($shippingMethod instanceof \Model\Setting\MasterTable\ShippingMethod) {
            return $this
                ->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_METHOD_ID, $shippingMethod->getId(), $comparison);
        } elseif ($shippingMethod instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaleOrderTableMap::COL_SHIPPING_METHOD_ID, $shippingMethod->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByShippingMethod() only accepts arguments of type \Model\Setting\MasterTable\ShippingMethod or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ShippingMethod relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function joinShippingMethod($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ShippingMethod');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ShippingMethod');
        }

        return $this;
    }

    /**
     * Use the ShippingMethod relation ShippingMethod object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\ShippingMethodQuery A secondary query class using the current class as primary query
     */
    public function useShippingMethodQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinShippingMethod($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ShippingMethod', '\Model\Setting\MasterTable\ShippingMethodQuery');
    }

    /**
     * Filter the query by a related \Model\Account\User object
     *
     * @param \Model\Account\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof \Model\Account\User) {
            return $this
                ->addUsingAlias(SaleOrderTableMap::COL_CREATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaleOrderTableMap::COL_CREATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \Model\Account\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Account\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\Model\Account\UserQuery');
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrderPayment object
     *
     * @param \Model\Sale\SaleOrderPayment|ObjectCollection $saleOrderPayment the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterBySaleOrderPayment($saleOrderPayment, $comparison = null)
    {
        if ($saleOrderPayment instanceof \Model\Sale\SaleOrderPayment) {
            return $this
                ->addUsingAlias(SaleOrderTableMap::COL_ID, $saleOrderPayment->getSaleOrderId(), $comparison);
        } elseif ($saleOrderPayment instanceof ObjectCollection) {
            return $this
                ->useSaleOrderPaymentQuery()
                ->filterByPrimaryKeys($saleOrderPayment->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleOrderPayment() only accepts arguments of type \Model\Sale\SaleOrderPayment or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrderPayment relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function joinSaleOrderPayment($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrderPayment');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrderPayment');
        }

        return $this;
    }

    /**
     * Use the SaleOrderPayment relation SaleOrderPayment object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderPaymentQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderPaymentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaleOrderPayment($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrderPayment', '\Model\Sale\SaleOrderPaymentQuery');
    }

    /**
     * Filter the query by a related \Model\Finance\Payment object
     *
     * @param \Model\Finance\Payment|ObjectCollection $payment the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterByPayment($payment, $comparison = null)
    {
        if ($payment instanceof \Model\Finance\Payment) {
            return $this
                ->addUsingAlias(SaleOrderTableMap::COL_ID, $payment->getSaleOrderId(), $comparison);
        } elseif ($payment instanceof ObjectCollection) {
            return $this
                ->usePaymentQuery()
                ->filterByPrimaryKeys($payment->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPayment() only accepts arguments of type \Model\Finance\Payment or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Payment relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function joinPayment($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Payment');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Payment');
        }

        return $this;
    }

    /**
     * Use the Payment relation Payment object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Finance\PaymentQuery A secondary query class using the current class as primary query
     */
    public function usePaymentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPayment($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Payment', '\Model\Finance\PaymentQuery');
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrderEmail object
     *
     * @param \Model\Sale\SaleOrderEmail|ObjectCollection $saleOrderEmail the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterBySaleOrderEmail($saleOrderEmail, $comparison = null)
    {
        if ($saleOrderEmail instanceof \Model\Sale\SaleOrderEmail) {
            return $this
                ->addUsingAlias(SaleOrderTableMap::COL_ID, $saleOrderEmail->getSaleOrderId(), $comparison);
        } elseif ($saleOrderEmail instanceof ObjectCollection) {
            return $this
                ->useSaleOrderEmailQuery()
                ->filterByPrimaryKeys($saleOrderEmail->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleOrderEmail() only accepts arguments of type \Model\Sale\SaleOrderEmail or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrderEmail relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function joinSaleOrderEmail($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrderEmail');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrderEmail');
        }

        return $this;
    }

    /**
     * Use the SaleOrderEmail relation SaleOrderEmail object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderEmailQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderEmailQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaleOrderEmail($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrderEmail', '\Model\Sale\SaleOrderEmailQuery');
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrderItem object
     *
     * @param \Model\Sale\SaleOrderItem|ObjectCollection $saleOrderItem the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterBySaleOrderItem($saleOrderItem, $comparison = null)
    {
        if ($saleOrderItem instanceof \Model\Sale\SaleOrderItem) {
            return $this
                ->addUsingAlias(SaleOrderTableMap::COL_ID, $saleOrderItem->getSaleOrderId(), $comparison);
        } elseif ($saleOrderItem instanceof ObjectCollection) {
            return $this
                ->useSaleOrderItemQuery()
                ->filterByPrimaryKeys($saleOrderItem->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleOrderItem() only accepts arguments of type \Model\Sale\SaleOrderItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrderItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function joinSaleOrderItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrderItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrderItem');
        }

        return $this;
    }

    /**
     * Use the SaleOrderItem relation SaleOrderItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderItemQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaleOrderItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrderItem', '\Model\Sale\SaleOrderItemQuery');
    }

    /**
     * Filter the query by a related \Model\SaleOrderNotification\SaleOrderNotification object
     *
     * @param \Model\SaleOrderNotification\SaleOrderNotification|ObjectCollection $saleOrderNotification the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSaleOrderQuery The current query, for fluid interface
     */
    public function filterBySaleOrderNotification($saleOrderNotification, $comparison = null)
    {
        if ($saleOrderNotification instanceof \Model\SaleOrderNotification\SaleOrderNotification) {
            return $this
                ->addUsingAlias(SaleOrderTableMap::COL_ID, $saleOrderNotification->getSaleOrderId(), $comparison);
        } elseif ($saleOrderNotification instanceof ObjectCollection) {
            return $this
                ->useSaleOrderNotificationQuery()
                ->filterByPrimaryKeys($saleOrderNotification->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleOrderNotification() only accepts arguments of type \Model\SaleOrderNotification\SaleOrderNotification or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrderNotification relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function joinSaleOrderNotification($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrderNotification');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrderNotification');
        }

        return $this;
    }

    /**
     * Use the SaleOrderNotification relation SaleOrderNotification object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\SaleOrderNotification\SaleOrderNotificationQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderNotificationQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaleOrderNotification($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrderNotification', '\Model\SaleOrderNotification\SaleOrderNotificationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSaleOrder $saleOrder Object to remove from the list of results
     *
     * @return $this|ChildSaleOrderQuery The current query, for fluid interface
     */
    public function prune($saleOrder = null)
    {
        if ($saleOrder) {
            $this->addUsingAlias(SaleOrderTableMap::COL_ID, $saleOrder->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the sale_order table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SaleOrderTableMap::clearInstancePool();
            SaleOrderTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SaleOrderTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SaleOrderTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SaleOrderTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 3600)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SaleOrderTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(SaleOrderTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

} // SaleOrderQuery
