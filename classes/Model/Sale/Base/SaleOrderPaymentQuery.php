<?php

namespace Model\Sale\Base;

use \Exception;
use \PDO;
use Model\Sale\SaleOrderPayment as ChildSaleOrderPayment;
use Model\Sale\SaleOrderPaymentQuery as ChildSaleOrderPaymentQuery;
use Model\Sale\Map\SaleOrderPaymentTableMap;
use Model\Setting\MasterTable\PaymentMethod;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'sale_order_payment' table.
 *
 *
 *
 * @method     ChildSaleOrderPaymentQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSaleOrderPaymentQuery orderBySaleOrderId($order = Criteria::ASC) Order by the sale_order_id column
 * @method     ChildSaleOrderPaymentQuery orderByPaymentMethodId($order = Criteria::ASC) Order by the payment_method_id column
 * @method     ChildSaleOrderPaymentQuery orderByTotalPrice($order = Criteria::ASC) Order by the total_price column
 * @method     ChildSaleOrderPaymentQuery orderByOurSecretHash($order = Criteria::ASC) Order by the our_secret_hash column
 * @method     ChildSaleOrderPaymentQuery orderByTrxid($order = Criteria::ASC) Order by the trxid column
 * @method     ChildSaleOrderPaymentQuery orderByTraceReference($order = Criteria::ASC) Order by the trace_reference column
 * @method     ChildSaleOrderPaymentQuery orderByPaymentReference($order = Criteria::ASC) Order by the payment_reference column
 * @method     ChildSaleOrderPaymentQuery orderByTransactionReference($order = Criteria::ASC) Order by the transaction_reference column
 * @method     ChildSaleOrderPaymentQuery orderByPayUrl($order = Criteria::ASC) Order by the pay_url column
 * @method     ChildSaleOrderPaymentQuery orderByStartedOn($order = Criteria::ASC) Order by the started_on column
 * @method     ChildSaleOrderPaymentQuery orderByIsPaid($order = Criteria::ASC) Order by the is_paid column
 *
 * @method     ChildSaleOrderPaymentQuery groupById() Group by the id column
 * @method     ChildSaleOrderPaymentQuery groupBySaleOrderId() Group by the sale_order_id column
 * @method     ChildSaleOrderPaymentQuery groupByPaymentMethodId() Group by the payment_method_id column
 * @method     ChildSaleOrderPaymentQuery groupByTotalPrice() Group by the total_price column
 * @method     ChildSaleOrderPaymentQuery groupByOurSecretHash() Group by the our_secret_hash column
 * @method     ChildSaleOrderPaymentQuery groupByTrxid() Group by the trxid column
 * @method     ChildSaleOrderPaymentQuery groupByTraceReference() Group by the trace_reference column
 * @method     ChildSaleOrderPaymentQuery groupByPaymentReference() Group by the payment_reference column
 * @method     ChildSaleOrderPaymentQuery groupByTransactionReference() Group by the transaction_reference column
 * @method     ChildSaleOrderPaymentQuery groupByPayUrl() Group by the pay_url column
 * @method     ChildSaleOrderPaymentQuery groupByStartedOn() Group by the started_on column
 * @method     ChildSaleOrderPaymentQuery groupByIsPaid() Group by the is_paid column
 *
 * @method     ChildSaleOrderPaymentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSaleOrderPaymentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSaleOrderPaymentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSaleOrderPaymentQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSaleOrderPaymentQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSaleOrderPaymentQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSaleOrderPaymentQuery leftJoinSaleOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrder relation
 * @method     ChildSaleOrderPaymentQuery rightJoinSaleOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrder relation
 * @method     ChildSaleOrderPaymentQuery innerJoinSaleOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrder relation
 *
 * @method     ChildSaleOrderPaymentQuery joinWithSaleOrder($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrder relation
 *
 * @method     ChildSaleOrderPaymentQuery leftJoinWithSaleOrder() Adds a LEFT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildSaleOrderPaymentQuery rightJoinWithSaleOrder() Adds a RIGHT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildSaleOrderPaymentQuery innerJoinWithSaleOrder() Adds a INNER JOIN clause and with to the query using the SaleOrder relation
 *
 * @method     ChildSaleOrderPaymentQuery leftJoinPaymentMethod($relationAlias = null) Adds a LEFT JOIN clause to the query using the PaymentMethod relation
 * @method     ChildSaleOrderPaymentQuery rightJoinPaymentMethod($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PaymentMethod relation
 * @method     ChildSaleOrderPaymentQuery innerJoinPaymentMethod($relationAlias = null) Adds a INNER JOIN clause to the query using the PaymentMethod relation
 *
 * @method     ChildSaleOrderPaymentQuery joinWithPaymentMethod($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PaymentMethod relation
 *
 * @method     ChildSaleOrderPaymentQuery leftJoinWithPaymentMethod() Adds a LEFT JOIN clause and with to the query using the PaymentMethod relation
 * @method     ChildSaleOrderPaymentQuery rightJoinWithPaymentMethod() Adds a RIGHT JOIN clause and with to the query using the PaymentMethod relation
 * @method     ChildSaleOrderPaymentQuery innerJoinWithPaymentMethod() Adds a INNER JOIN clause and with to the query using the PaymentMethod relation
 *
 * @method     \Model\Sale\SaleOrderQuery|\Model\Setting\MasterTable\PaymentMethodQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSaleOrderPayment findOne(ConnectionInterface $con = null) Return the first ChildSaleOrderPayment matching the query
 * @method     ChildSaleOrderPayment findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSaleOrderPayment matching the query, or a new ChildSaleOrderPayment object populated from the query conditions when no match is found
 *
 * @method     ChildSaleOrderPayment findOneById(int $id) Return the first ChildSaleOrderPayment filtered by the id column
 * @method     ChildSaleOrderPayment findOneBySaleOrderId(int $sale_order_id) Return the first ChildSaleOrderPayment filtered by the sale_order_id column
 * @method     ChildSaleOrderPayment findOneByPaymentMethodId(int $payment_method_id) Return the first ChildSaleOrderPayment filtered by the payment_method_id column
 * @method     ChildSaleOrderPayment findOneByTotalPrice(double $total_price) Return the first ChildSaleOrderPayment filtered by the total_price column
 * @method     ChildSaleOrderPayment findOneByOurSecretHash(string $our_secret_hash) Return the first ChildSaleOrderPayment filtered by the our_secret_hash column
 * @method     ChildSaleOrderPayment findOneByTrxid(string $trxid) Return the first ChildSaleOrderPayment filtered by the trxid column
 * @method     ChildSaleOrderPayment findOneByTraceReference(string $trace_reference) Return the first ChildSaleOrderPayment filtered by the trace_reference column
 * @method     ChildSaleOrderPayment findOneByPaymentReference(string $payment_reference) Return the first ChildSaleOrderPayment filtered by the payment_reference column
 * @method     ChildSaleOrderPayment findOneByTransactionReference(string $transaction_reference) Return the first ChildSaleOrderPayment filtered by the transaction_reference column
 * @method     ChildSaleOrderPayment findOneByPayUrl(string $pay_url) Return the first ChildSaleOrderPayment filtered by the pay_url column
 * @method     ChildSaleOrderPayment findOneByStartedOn(string $started_on) Return the first ChildSaleOrderPayment filtered by the started_on column
 * @method     ChildSaleOrderPayment findOneByIsPaid(boolean $is_paid) Return the first ChildSaleOrderPayment filtered by the is_paid column *

 * @method     ChildSaleOrderPayment requirePk($key, ConnectionInterface $con = null) Return the ChildSaleOrderPayment by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderPayment requireOne(ConnectionInterface $con = null) Return the first ChildSaleOrderPayment matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleOrderPayment requireOneById(int $id) Return the first ChildSaleOrderPayment filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderPayment requireOneBySaleOrderId(int $sale_order_id) Return the first ChildSaleOrderPayment filtered by the sale_order_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderPayment requireOneByPaymentMethodId(int $payment_method_id) Return the first ChildSaleOrderPayment filtered by the payment_method_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderPayment requireOneByTotalPrice(double $total_price) Return the first ChildSaleOrderPayment filtered by the total_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderPayment requireOneByOurSecretHash(string $our_secret_hash) Return the first ChildSaleOrderPayment filtered by the our_secret_hash column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderPayment requireOneByTrxid(string $trxid) Return the first ChildSaleOrderPayment filtered by the trxid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderPayment requireOneByTraceReference(string $trace_reference) Return the first ChildSaleOrderPayment filtered by the trace_reference column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderPayment requireOneByPaymentReference(string $payment_reference) Return the first ChildSaleOrderPayment filtered by the payment_reference column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderPayment requireOneByTransactionReference(string $transaction_reference) Return the first ChildSaleOrderPayment filtered by the transaction_reference column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderPayment requireOneByPayUrl(string $pay_url) Return the first ChildSaleOrderPayment filtered by the pay_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderPayment requireOneByStartedOn(string $started_on) Return the first ChildSaleOrderPayment filtered by the started_on column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderPayment requireOneByIsPaid(boolean $is_paid) Return the first ChildSaleOrderPayment filtered by the is_paid column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleOrderPayment[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSaleOrderPayment objects based on current ModelCriteria
 * @method     ChildSaleOrderPayment[]|ObjectCollection findById(int $id) Return ChildSaleOrderPayment objects filtered by the id column
 * @method     ChildSaleOrderPayment[]|ObjectCollection findBySaleOrderId(int $sale_order_id) Return ChildSaleOrderPayment objects filtered by the sale_order_id column
 * @method     ChildSaleOrderPayment[]|ObjectCollection findByPaymentMethodId(int $payment_method_id) Return ChildSaleOrderPayment objects filtered by the payment_method_id column
 * @method     ChildSaleOrderPayment[]|ObjectCollection findByTotalPrice(double $total_price) Return ChildSaleOrderPayment objects filtered by the total_price column
 * @method     ChildSaleOrderPayment[]|ObjectCollection findByOurSecretHash(string $our_secret_hash) Return ChildSaleOrderPayment objects filtered by the our_secret_hash column
 * @method     ChildSaleOrderPayment[]|ObjectCollection findByTrxid(string $trxid) Return ChildSaleOrderPayment objects filtered by the trxid column
 * @method     ChildSaleOrderPayment[]|ObjectCollection findByTraceReference(string $trace_reference) Return ChildSaleOrderPayment objects filtered by the trace_reference column
 * @method     ChildSaleOrderPayment[]|ObjectCollection findByPaymentReference(string $payment_reference) Return ChildSaleOrderPayment objects filtered by the payment_reference column
 * @method     ChildSaleOrderPayment[]|ObjectCollection findByTransactionReference(string $transaction_reference) Return ChildSaleOrderPayment objects filtered by the transaction_reference column
 * @method     ChildSaleOrderPayment[]|ObjectCollection findByPayUrl(string $pay_url) Return ChildSaleOrderPayment objects filtered by the pay_url column
 * @method     ChildSaleOrderPayment[]|ObjectCollection findByStartedOn(string $started_on) Return ChildSaleOrderPayment objects filtered by the started_on column
 * @method     ChildSaleOrderPayment[]|ObjectCollection findByIsPaid(boolean $is_paid) Return ChildSaleOrderPayment objects filtered by the is_paid column
 * @method     ChildSaleOrderPayment[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SaleOrderPaymentQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Sale\Base\SaleOrderPaymentQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Sale\\SaleOrderPayment', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSaleOrderPaymentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSaleOrderPaymentQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSaleOrderPaymentQuery) {
            return $criteria;
        }
        $query = new ChildSaleOrderPaymentQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSaleOrderPayment|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SaleOrderPaymentTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SaleOrderPaymentTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderPayment A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, sale_order_id, payment_method_id, total_price, our_secret_hash, trxid, trace_reference, payment_reference, transaction_reference, pay_url, started_on, is_paid FROM sale_order_payment WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSaleOrderPayment $obj */
            $obj = new ChildSaleOrderPayment();
            $obj->hydrate($row);
            SaleOrderPaymentTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSaleOrderPayment|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SaleOrderPaymentTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SaleOrderPaymentTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the sale_order_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySaleOrderId(1234); // WHERE sale_order_id = 1234
     * $query->filterBySaleOrderId(array(12, 34)); // WHERE sale_order_id IN (12, 34)
     * $query->filterBySaleOrderId(array('min' => 12)); // WHERE sale_order_id > 12
     * </code>
     *
     * @see       filterBySaleOrder()
     *
     * @param     mixed $saleOrderId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterBySaleOrderId($saleOrderId = null, $comparison = null)
    {
        if (is_array($saleOrderId)) {
            $useMinMax = false;
            if (isset($saleOrderId['min'])) {
                $this->addUsingAlias(SaleOrderPaymentTableMap::COL_SALE_ORDER_ID, $saleOrderId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($saleOrderId['max'])) {
                $this->addUsingAlias(SaleOrderPaymentTableMap::COL_SALE_ORDER_ID, $saleOrderId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_SALE_ORDER_ID, $saleOrderId, $comparison);
    }

    /**
     * Filter the query on the payment_method_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentMethodId(1234); // WHERE payment_method_id = 1234
     * $query->filterByPaymentMethodId(array(12, 34)); // WHERE payment_method_id IN (12, 34)
     * $query->filterByPaymentMethodId(array('min' => 12)); // WHERE payment_method_id > 12
     * </code>
     *
     * @see       filterByPaymentMethod()
     *
     * @param     mixed $paymentMethodId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterByPaymentMethodId($paymentMethodId = null, $comparison = null)
    {
        if (is_array($paymentMethodId)) {
            $useMinMax = false;
            if (isset($paymentMethodId['min'])) {
                $this->addUsingAlias(SaleOrderPaymentTableMap::COL_PAYMENT_METHOD_ID, $paymentMethodId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($paymentMethodId['max'])) {
                $this->addUsingAlias(SaleOrderPaymentTableMap::COL_PAYMENT_METHOD_ID, $paymentMethodId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_PAYMENT_METHOD_ID, $paymentMethodId, $comparison);
    }

    /**
     * Filter the query on the total_price column
     *
     * Example usage:
     * <code>
     * $query->filterByTotalPrice(1234); // WHERE total_price = 1234
     * $query->filterByTotalPrice(array(12, 34)); // WHERE total_price IN (12, 34)
     * $query->filterByTotalPrice(array('min' => 12)); // WHERE total_price > 12
     * </code>
     *
     * @param     mixed $totalPrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterByTotalPrice($totalPrice = null, $comparison = null)
    {
        if (is_array($totalPrice)) {
            $useMinMax = false;
            if (isset($totalPrice['min'])) {
                $this->addUsingAlias(SaleOrderPaymentTableMap::COL_TOTAL_PRICE, $totalPrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($totalPrice['max'])) {
                $this->addUsingAlias(SaleOrderPaymentTableMap::COL_TOTAL_PRICE, $totalPrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_TOTAL_PRICE, $totalPrice, $comparison);
    }

    /**
     * Filter the query on the our_secret_hash column
     *
     * Example usage:
     * <code>
     * $query->filterByOurSecretHash('fooValue');   // WHERE our_secret_hash = 'fooValue'
     * $query->filterByOurSecretHash('%fooValue%', Criteria::LIKE); // WHERE our_secret_hash LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ourSecretHash The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterByOurSecretHash($ourSecretHash = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ourSecretHash)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_OUR_SECRET_HASH, $ourSecretHash, $comparison);
    }

    /**
     * Filter the query on the trxid column
     *
     * Example usage:
     * <code>
     * $query->filterByTrxid('fooValue');   // WHERE trxid = 'fooValue'
     * $query->filterByTrxid('%fooValue%', Criteria::LIKE); // WHERE trxid LIKE '%fooValue%'
     * </code>
     *
     * @param     string $trxid The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterByTrxid($trxid = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($trxid)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_TRXID, $trxid, $comparison);
    }

    /**
     * Filter the query on the trace_reference column
     *
     * Example usage:
     * <code>
     * $query->filterByTraceReference('fooValue');   // WHERE trace_reference = 'fooValue'
     * $query->filterByTraceReference('%fooValue%', Criteria::LIKE); // WHERE trace_reference LIKE '%fooValue%'
     * </code>
     *
     * @param     string $traceReference The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterByTraceReference($traceReference = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($traceReference)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_TRACE_REFERENCE, $traceReference, $comparison);
    }

    /**
     * Filter the query on the payment_reference column
     *
     * Example usage:
     * <code>
     * $query->filterByPaymentReference('fooValue');   // WHERE payment_reference = 'fooValue'
     * $query->filterByPaymentReference('%fooValue%', Criteria::LIKE); // WHERE payment_reference LIKE '%fooValue%'
     * </code>
     *
     * @param     string $paymentReference The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterByPaymentReference($paymentReference = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($paymentReference)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_PAYMENT_REFERENCE, $paymentReference, $comparison);
    }

    /**
     * Filter the query on the transaction_reference column
     *
     * Example usage:
     * <code>
     * $query->filterByTransactionReference('fooValue');   // WHERE transaction_reference = 'fooValue'
     * $query->filterByTransactionReference('%fooValue%', Criteria::LIKE); // WHERE transaction_reference LIKE '%fooValue%'
     * </code>
     *
     * @param     string $transactionReference The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterByTransactionReference($transactionReference = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($transactionReference)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_TRANSACTION_REFERENCE, $transactionReference, $comparison);
    }

    /**
     * Filter the query on the pay_url column
     *
     * Example usage:
     * <code>
     * $query->filterByPayUrl('fooValue');   // WHERE pay_url = 'fooValue'
     * $query->filterByPayUrl('%fooValue%', Criteria::LIKE); // WHERE pay_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $payUrl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterByPayUrl($payUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($payUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_PAY_URL, $payUrl, $comparison);
    }

    /**
     * Filter the query on the started_on column
     *
     * Example usage:
     * <code>
     * $query->filterByStartedOn('2011-03-14'); // WHERE started_on = '2011-03-14'
     * $query->filterByStartedOn('now'); // WHERE started_on = '2011-03-14'
     * $query->filterByStartedOn(array('max' => 'yesterday')); // WHERE started_on > '2011-03-13'
     * </code>
     *
     * @param     mixed $startedOn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterByStartedOn($startedOn = null, $comparison = null)
    {
        if (is_array($startedOn)) {
            $useMinMax = false;
            if (isset($startedOn['min'])) {
                $this->addUsingAlias(SaleOrderPaymentTableMap::COL_STARTED_ON, $startedOn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($startedOn['max'])) {
                $this->addUsingAlias(SaleOrderPaymentTableMap::COL_STARTED_ON, $startedOn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_STARTED_ON, $startedOn, $comparison);
    }

    /**
     * Filter the query on the is_paid column
     *
     * Example usage:
     * <code>
     * $query->filterByIsPaid(true); // WHERE is_paid = true
     * $query->filterByIsPaid('yes'); // WHERE is_paid = true
     * </code>
     *
     * @param     boolean|string $isPaid The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterByIsPaid($isPaid = null, $comparison = null)
    {
        if (is_string($isPaid)) {
            $isPaid = in_array(strtolower($isPaid), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderPaymentTableMap::COL_IS_PAID, $isPaid, $comparison);
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrder object
     *
     * @param \Model\Sale\SaleOrder|ObjectCollection $saleOrder The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterBySaleOrder($saleOrder, $comparison = null)
    {
        if ($saleOrder instanceof \Model\Sale\SaleOrder) {
            return $this
                ->addUsingAlias(SaleOrderPaymentTableMap::COL_SALE_ORDER_ID, $saleOrder->getId(), $comparison);
        } elseif ($saleOrder instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaleOrderPaymentTableMap::COL_SALE_ORDER_ID, $saleOrder->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySaleOrder() only accepts arguments of type \Model\Sale\SaleOrder or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function joinSaleOrder($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrder');
        }

        return $this;
    }

    /**
     * Use the SaleOrder relation SaleOrder object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaleOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrder', '\Model\Sale\SaleOrderQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\PaymentMethod object
     *
     * @param \Model\Setting\MasterTable\PaymentMethod|ObjectCollection $paymentMethod The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function filterByPaymentMethod($paymentMethod, $comparison = null)
    {
        if ($paymentMethod instanceof \Model\Setting\MasterTable\PaymentMethod) {
            return $this
                ->addUsingAlias(SaleOrderPaymentTableMap::COL_PAYMENT_METHOD_ID, $paymentMethod->getId(), $comparison);
        } elseif ($paymentMethod instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaleOrderPaymentTableMap::COL_PAYMENT_METHOD_ID, $paymentMethod->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPaymentMethod() only accepts arguments of type \Model\Setting\MasterTable\PaymentMethod or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PaymentMethod relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function joinPaymentMethod($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PaymentMethod');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PaymentMethod');
        }

        return $this;
    }

    /**
     * Use the PaymentMethod relation PaymentMethod object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\PaymentMethodQuery A secondary query class using the current class as primary query
     */
    public function usePaymentMethodQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPaymentMethod($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PaymentMethod', '\Model\Setting\MasterTable\PaymentMethodQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSaleOrderPayment $saleOrderPayment Object to remove from the list of results
     *
     * @return $this|ChildSaleOrderPaymentQuery The current query, for fluid interface
     */
    public function prune($saleOrderPayment = null)
    {
        if ($saleOrderPayment) {
            $this->addUsingAlias(SaleOrderPaymentTableMap::COL_ID, $saleOrderPayment->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the sale_order_payment table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderPaymentTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SaleOrderPaymentTableMap::clearInstancePool();
            SaleOrderPaymentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderPaymentTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SaleOrderPaymentTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SaleOrderPaymentTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SaleOrderPaymentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SaleOrderPaymentQuery
