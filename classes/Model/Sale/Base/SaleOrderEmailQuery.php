<?php

namespace Model\Sale\Base;

use \Exception;
use \PDO;
use Model\Sale\SaleOrderEmail as ChildSaleOrderEmail;
use Model\Sale\SaleOrderEmailQuery as ChildSaleOrderEmailQuery;
use Model\Sale\Map\SaleOrderEmailTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'sale_order_email' table.
 *
 *
 *
 * @method     ChildSaleOrderEmailQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSaleOrderEmailQuery orderByCreatedOn($order = Criteria::ASC) Order by the created_on column
 * @method     ChildSaleOrderEmailQuery orderBySaleOrderId($order = Criteria::ASC) Order by the sale_order_id column
 * @method     ChildSaleOrderEmailQuery orderBySendFrom($order = Criteria::ASC) Order by the send_from column
 * @method     ChildSaleOrderEmailQuery orderBySendTo($order = Criteria::ASC) Order by the send_to column
 * @method     ChildSaleOrderEmailQuery orderBySubject($order = Criteria::ASC) Order by the subject column
 * @method     ChildSaleOrderEmailQuery orderByContent($order = Criteria::ASC) Order by the content column
 * @method     ChildSaleOrderEmailQuery orderByAttachedFiles($order = Criteria::ASC) Order by the attached_files column
 *
 * @method     ChildSaleOrderEmailQuery groupById() Group by the id column
 * @method     ChildSaleOrderEmailQuery groupByCreatedOn() Group by the created_on column
 * @method     ChildSaleOrderEmailQuery groupBySaleOrderId() Group by the sale_order_id column
 * @method     ChildSaleOrderEmailQuery groupBySendFrom() Group by the send_from column
 * @method     ChildSaleOrderEmailQuery groupBySendTo() Group by the send_to column
 * @method     ChildSaleOrderEmailQuery groupBySubject() Group by the subject column
 * @method     ChildSaleOrderEmailQuery groupByContent() Group by the content column
 * @method     ChildSaleOrderEmailQuery groupByAttachedFiles() Group by the attached_files column
 *
 * @method     ChildSaleOrderEmailQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSaleOrderEmailQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSaleOrderEmailQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSaleOrderEmailQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSaleOrderEmailQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSaleOrderEmailQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSaleOrderEmailQuery leftJoinSaleOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrder relation
 * @method     ChildSaleOrderEmailQuery rightJoinSaleOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrder relation
 * @method     ChildSaleOrderEmailQuery innerJoinSaleOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrder relation
 *
 * @method     ChildSaleOrderEmailQuery joinWithSaleOrder($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrder relation
 *
 * @method     ChildSaleOrderEmailQuery leftJoinWithSaleOrder() Adds a LEFT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildSaleOrderEmailQuery rightJoinWithSaleOrder() Adds a RIGHT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildSaleOrderEmailQuery innerJoinWithSaleOrder() Adds a INNER JOIN clause and with to the query using the SaleOrder relation
 *
 * @method     \Model\Sale\SaleOrderQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSaleOrderEmail findOne(ConnectionInterface $con = null) Return the first ChildSaleOrderEmail matching the query
 * @method     ChildSaleOrderEmail findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSaleOrderEmail matching the query, or a new ChildSaleOrderEmail object populated from the query conditions when no match is found
 *
 * @method     ChildSaleOrderEmail findOneById(int $id) Return the first ChildSaleOrderEmail filtered by the id column
 * @method     ChildSaleOrderEmail findOneByCreatedOn(string $created_on) Return the first ChildSaleOrderEmail filtered by the created_on column
 * @method     ChildSaleOrderEmail findOneBySaleOrderId(int $sale_order_id) Return the first ChildSaleOrderEmail filtered by the sale_order_id column
 * @method     ChildSaleOrderEmail findOneBySendFrom(string $send_from) Return the first ChildSaleOrderEmail filtered by the send_from column
 * @method     ChildSaleOrderEmail findOneBySendTo(string $send_to) Return the first ChildSaleOrderEmail filtered by the send_to column
 * @method     ChildSaleOrderEmail findOneBySubject(string $subject) Return the first ChildSaleOrderEmail filtered by the subject column
 * @method     ChildSaleOrderEmail findOneByContent(string $content) Return the first ChildSaleOrderEmail filtered by the content column
 * @method     ChildSaleOrderEmail findOneByAttachedFiles(string $attached_files) Return the first ChildSaleOrderEmail filtered by the attached_files column *

 * @method     ChildSaleOrderEmail requirePk($key, ConnectionInterface $con = null) Return the ChildSaleOrderEmail by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderEmail requireOne(ConnectionInterface $con = null) Return the first ChildSaleOrderEmail matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleOrderEmail requireOneById(int $id) Return the first ChildSaleOrderEmail filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderEmail requireOneByCreatedOn(string $created_on) Return the first ChildSaleOrderEmail filtered by the created_on column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderEmail requireOneBySaleOrderId(int $sale_order_id) Return the first ChildSaleOrderEmail filtered by the sale_order_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderEmail requireOneBySendFrom(string $send_from) Return the first ChildSaleOrderEmail filtered by the send_from column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderEmail requireOneBySendTo(string $send_to) Return the first ChildSaleOrderEmail filtered by the send_to column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderEmail requireOneBySubject(string $subject) Return the first ChildSaleOrderEmail filtered by the subject column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderEmail requireOneByContent(string $content) Return the first ChildSaleOrderEmail filtered by the content column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderEmail requireOneByAttachedFiles(string $attached_files) Return the first ChildSaleOrderEmail filtered by the attached_files column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleOrderEmail[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSaleOrderEmail objects based on current ModelCriteria
 * @method     ChildSaleOrderEmail[]|ObjectCollection findById(int $id) Return ChildSaleOrderEmail objects filtered by the id column
 * @method     ChildSaleOrderEmail[]|ObjectCollection findByCreatedOn(string $created_on) Return ChildSaleOrderEmail objects filtered by the created_on column
 * @method     ChildSaleOrderEmail[]|ObjectCollection findBySaleOrderId(int $sale_order_id) Return ChildSaleOrderEmail objects filtered by the sale_order_id column
 * @method     ChildSaleOrderEmail[]|ObjectCollection findBySendFrom(string $send_from) Return ChildSaleOrderEmail objects filtered by the send_from column
 * @method     ChildSaleOrderEmail[]|ObjectCollection findBySendTo(string $send_to) Return ChildSaleOrderEmail objects filtered by the send_to column
 * @method     ChildSaleOrderEmail[]|ObjectCollection findBySubject(string $subject) Return ChildSaleOrderEmail objects filtered by the subject column
 * @method     ChildSaleOrderEmail[]|ObjectCollection findByContent(string $content) Return ChildSaleOrderEmail objects filtered by the content column
 * @method     ChildSaleOrderEmail[]|ObjectCollection findByAttachedFiles(string $attached_files) Return ChildSaleOrderEmail objects filtered by the attached_files column
 * @method     ChildSaleOrderEmail[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SaleOrderEmailQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Sale\Base\SaleOrderEmailQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Sale\\SaleOrderEmail', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSaleOrderEmailQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSaleOrderEmailQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSaleOrderEmailQuery) {
            return $criteria;
        }
        $query = new ChildSaleOrderEmailQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSaleOrderEmail|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SaleOrderEmailTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SaleOrderEmailTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderEmail A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, created_on, sale_order_id, send_from, send_to, subject, content, attached_files FROM sale_order_email WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSaleOrderEmail $obj */
            $obj = new ChildSaleOrderEmail();
            $obj->hydrate($row);
            SaleOrderEmailTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSaleOrderEmail|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSaleOrderEmailQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SaleOrderEmailTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSaleOrderEmailQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SaleOrderEmailTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderEmailQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SaleOrderEmailTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SaleOrderEmailTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderEmailTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the created_on column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedOn('2011-03-14'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn('now'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn(array('max' => 'yesterday')); // WHERE created_on > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdOn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderEmailQuery The current query, for fluid interface
     */
    public function filterByCreatedOn($createdOn = null, $comparison = null)
    {
        if (is_array($createdOn)) {
            $useMinMax = false;
            if (isset($createdOn['min'])) {
                $this->addUsingAlias(SaleOrderEmailTableMap::COL_CREATED_ON, $createdOn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdOn['max'])) {
                $this->addUsingAlias(SaleOrderEmailTableMap::COL_CREATED_ON, $createdOn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderEmailTableMap::COL_CREATED_ON, $createdOn, $comparison);
    }

    /**
     * Filter the query on the sale_order_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySaleOrderId(1234); // WHERE sale_order_id = 1234
     * $query->filterBySaleOrderId(array(12, 34)); // WHERE sale_order_id IN (12, 34)
     * $query->filterBySaleOrderId(array('min' => 12)); // WHERE sale_order_id > 12
     * </code>
     *
     * @see       filterBySaleOrder()
     *
     * @param     mixed $saleOrderId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderEmailQuery The current query, for fluid interface
     */
    public function filterBySaleOrderId($saleOrderId = null, $comparison = null)
    {
        if (is_array($saleOrderId)) {
            $useMinMax = false;
            if (isset($saleOrderId['min'])) {
                $this->addUsingAlias(SaleOrderEmailTableMap::COL_SALE_ORDER_ID, $saleOrderId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($saleOrderId['max'])) {
                $this->addUsingAlias(SaleOrderEmailTableMap::COL_SALE_ORDER_ID, $saleOrderId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderEmailTableMap::COL_SALE_ORDER_ID, $saleOrderId, $comparison);
    }

    /**
     * Filter the query on the send_from column
     *
     * Example usage:
     * <code>
     * $query->filterBySendFrom('fooValue');   // WHERE send_from = 'fooValue'
     * $query->filterBySendFrom('%fooValue%', Criteria::LIKE); // WHERE send_from LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sendFrom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderEmailQuery The current query, for fluid interface
     */
    public function filterBySendFrom($sendFrom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sendFrom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderEmailTableMap::COL_SEND_FROM, $sendFrom, $comparison);
    }

    /**
     * Filter the query on the send_to column
     *
     * Example usage:
     * <code>
     * $query->filterBySendTo('fooValue');   // WHERE send_to = 'fooValue'
     * $query->filterBySendTo('%fooValue%', Criteria::LIKE); // WHERE send_to LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sendTo The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderEmailQuery The current query, for fluid interface
     */
    public function filterBySendTo($sendTo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sendTo)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderEmailTableMap::COL_SEND_TO, $sendTo, $comparison);
    }

    /**
     * Filter the query on the subject column
     *
     * Example usage:
     * <code>
     * $query->filterBySubject('fooValue');   // WHERE subject = 'fooValue'
     * $query->filterBySubject('%fooValue%', Criteria::LIKE); // WHERE subject LIKE '%fooValue%'
     * </code>
     *
     * @param     string $subject The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderEmailQuery The current query, for fluid interface
     */
    public function filterBySubject($subject = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($subject)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderEmailTableMap::COL_SUBJECT, $subject, $comparison);
    }

    /**
     * Filter the query on the content column
     *
     * Example usage:
     * <code>
     * $query->filterByContent('fooValue');   // WHERE content = 'fooValue'
     * $query->filterByContent('%fooValue%', Criteria::LIKE); // WHERE content LIKE '%fooValue%'
     * </code>
     *
     * @param     string $content The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderEmailQuery The current query, for fluid interface
     */
    public function filterByContent($content = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($content)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderEmailTableMap::COL_CONTENT, $content, $comparison);
    }

    /**
     * Filter the query on the attached_files column
     *
     * Example usage:
     * <code>
     * $query->filterByAttachedFiles('fooValue');   // WHERE attached_files = 'fooValue'
     * $query->filterByAttachedFiles('%fooValue%', Criteria::LIKE); // WHERE attached_files LIKE '%fooValue%'
     * </code>
     *
     * @param     string $attachedFiles The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderEmailQuery The current query, for fluid interface
     */
    public function filterByAttachedFiles($attachedFiles = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($attachedFiles)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderEmailTableMap::COL_ATTACHED_FILES, $attachedFiles, $comparison);
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrder object
     *
     * @param \Model\Sale\SaleOrder|ObjectCollection $saleOrder The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderEmailQuery The current query, for fluid interface
     */
    public function filterBySaleOrder($saleOrder, $comparison = null)
    {
        if ($saleOrder instanceof \Model\Sale\SaleOrder) {
            return $this
                ->addUsingAlias(SaleOrderEmailTableMap::COL_SALE_ORDER_ID, $saleOrder->getId(), $comparison);
        } elseif ($saleOrder instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaleOrderEmailTableMap::COL_SALE_ORDER_ID, $saleOrder->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySaleOrder() only accepts arguments of type \Model\Sale\SaleOrder or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderEmailQuery The current query, for fluid interface
     */
    public function joinSaleOrder($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrder');
        }

        return $this;
    }

    /**
     * Use the SaleOrder relation SaleOrder object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaleOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrder', '\Model\Sale\SaleOrderQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSaleOrderEmail $saleOrderEmail Object to remove from the list of results
     *
     * @return $this|ChildSaleOrderEmailQuery The current query, for fluid interface
     */
    public function prune($saleOrderEmail = null)
    {
        if ($saleOrderEmail) {
            $this->addUsingAlias(SaleOrderEmailTableMap::COL_ID, $saleOrderEmail->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the sale_order_email table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderEmailTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SaleOrderEmailTableMap::clearInstancePool();
            SaleOrderEmailTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderEmailTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SaleOrderEmailTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SaleOrderEmailTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SaleOrderEmailTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SaleOrderEmailQuery
