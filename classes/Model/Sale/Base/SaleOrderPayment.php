<?php

namespace Model\Sale\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Sale\SaleOrder as ChildSaleOrder;
use Model\Sale\SaleOrderPaymentQuery as ChildSaleOrderPaymentQuery;
use Model\Sale\SaleOrderQuery as ChildSaleOrderQuery;
use Model\Sale\Map\SaleOrderPaymentTableMap;
use Model\Setting\MasterTable\PaymentMethod;
use Model\Setting\MasterTable\PaymentMethodQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'sale_order_payment' table.
 *
 *
 *
 * @package    propel.generator.Model.Sale.Base
 */
abstract class SaleOrderPayment implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Sale\\Map\\SaleOrderPaymentTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the sale_order_id field.
     *
     * @var        int|null
     */
    protected $sale_order_id;

    /**
     * The value for the payment_method_id field.
     *
     * @var        int|null
     */
    protected $payment_method_id;

    /**
     * The value for the total_price field.
     *
     * @var        double|null
     */
    protected $total_price;

    /**
     * The value for the our_secret_hash field.
     *
     * @var        string|null
     */
    protected $our_secret_hash;

    /**
     * The value for the trxid field.
     *
     * @var        string|null
     */
    protected $trxid;

    /**
     * The value for the trace_reference field.
     *
     * @var        string|null
     */
    protected $trace_reference;

    /**
     * The value for the payment_reference field.
     *
     * @var        string|null
     */
    protected $payment_reference;

    /**
     * The value for the transaction_reference field.
     *
     * @var        string|null
     */
    protected $transaction_reference;

    /**
     * The value for the pay_url field.
     *
     * @var        string|null
     */
    protected $pay_url;

    /**
     * The value for the started_on field.
     *
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        DateTime|null
     */
    protected $started_on;

    /**
     * The value for the is_paid field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_paid;

    /**
     * @var        ChildSaleOrder
     */
    protected $aSaleOrder;

    /**
     * @var        PaymentMethod
     */
    protected $aPaymentMethod;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_paid = false;
    }

    /**
     * Initializes internal state of Model\Sale\Base\SaleOrderPayment object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>SaleOrderPayment</code> instance.  If
     * <code>obj</code> is an instance of <code>SaleOrderPayment</code>, delegates to
     * <code>equals(SaleOrderPayment)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [sale_order_id] column value.
     *
     * @return int|null
     */
    public function getSaleOrderId()
    {
        return $this->sale_order_id;
    }

    /**
     * Get the [payment_method_id] column value.
     *
     * @return int|null
     */
    public function getPaymentMethodId()
    {
        return $this->payment_method_id;
    }

    /**
     * Get the [total_price] column value.
     *
     * @return double|null
     */
    public function getTotalPrice()
    {
        return $this->total_price;
    }

    /**
     * Get the [our_secret_hash] column value.
     *
     * @return string|null
     */
    public function getOurSecretHash()
    {
        return $this->our_secret_hash;
    }

    /**
     * Get the [trxid] column value.
     *
     * @return string|null
     */
    public function getTrxid()
    {
        return $this->trxid;
    }

    /**
     * Get the [trace_reference] column value.
     *
     * @return string|null
     */
    public function getTraceReference()
    {
        return $this->trace_reference;
    }

    /**
     * Get the [payment_reference] column value.
     *
     * @return string|null
     */
    public function getPaymentReference()
    {
        return $this->payment_reference;
    }

    /**
     * Get the [transaction_reference] column value.
     *
     * @return string|null
     */
    public function getTransactionReference()
    {
        return $this->transaction_reference;
    }

    /**
     * Get the [pay_url] column value.
     *
     * @return string|null
     */
    public function getPayUrl()
    {
        return $this->pay_url;
    }

    /**
     * Get the [optionally formatted] temporal [started_on] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getStartedOn($format = null)
    {
        if ($format === null) {
            return $this->started_on;
        } else {
            return $this->started_on instanceof \DateTimeInterface ? $this->started_on->format($format) : null;
        }
    }

    /**
     * Get the [is_paid] column value.
     *
     * @return boolean|null
     */
    public function getIsPaid()
    {
        return $this->is_paid;
    }

    /**
     * Get the [is_paid] column value.
     *
     * @return boolean|null
     */
    public function isPaid()
    {
        return $this->getIsPaid();
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[SaleOrderPaymentTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [sale_order_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     */
    public function setSaleOrderId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->sale_order_id !== $v) {
            $this->sale_order_id = $v;
            $this->modifiedColumns[SaleOrderPaymentTableMap::COL_SALE_ORDER_ID] = true;
        }

        if ($this->aSaleOrder !== null && $this->aSaleOrder->getId() !== $v) {
            $this->aSaleOrder = null;
        }

        return $this;
    } // setSaleOrderId()

    /**
     * Set the value of [payment_method_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     */
    public function setPaymentMethodId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->payment_method_id !== $v) {
            $this->payment_method_id = $v;
            $this->modifiedColumns[SaleOrderPaymentTableMap::COL_PAYMENT_METHOD_ID] = true;
        }

        if ($this->aPaymentMethod !== null && $this->aPaymentMethod->getId() !== $v) {
            $this->aPaymentMethod = null;
        }

        return $this;
    } // setPaymentMethodId()

    /**
     * Set the value of [total_price] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     */
    public function setTotalPrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->total_price !== $v) {
            $this->total_price = $v;
            $this->modifiedColumns[SaleOrderPaymentTableMap::COL_TOTAL_PRICE] = true;
        }

        return $this;
    } // setTotalPrice()

    /**
     * Set the value of [our_secret_hash] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     */
    public function setOurSecretHash($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->our_secret_hash !== $v) {
            $this->our_secret_hash = $v;
            $this->modifiedColumns[SaleOrderPaymentTableMap::COL_OUR_SECRET_HASH] = true;
        }

        return $this;
    } // setOurSecretHash()

    /**
     * Set the value of [trxid] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     */
    public function setTrxid($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->trxid !== $v) {
            $this->trxid = $v;
            $this->modifiedColumns[SaleOrderPaymentTableMap::COL_TRXID] = true;
        }

        return $this;
    } // setTrxid()

    /**
     * Set the value of [trace_reference] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     */
    public function setTraceReference($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->trace_reference !== $v) {
            $this->trace_reference = $v;
            $this->modifiedColumns[SaleOrderPaymentTableMap::COL_TRACE_REFERENCE] = true;
        }

        return $this;
    } // setTraceReference()

    /**
     * Set the value of [payment_reference] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     */
    public function setPaymentReference($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->payment_reference !== $v) {
            $this->payment_reference = $v;
            $this->modifiedColumns[SaleOrderPaymentTableMap::COL_PAYMENT_REFERENCE] = true;
        }

        return $this;
    } // setPaymentReference()

    /**
     * Set the value of [transaction_reference] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     */
    public function setTransactionReference($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->transaction_reference !== $v) {
            $this->transaction_reference = $v;
            $this->modifiedColumns[SaleOrderPaymentTableMap::COL_TRANSACTION_REFERENCE] = true;
        }

        return $this;
    } // setTransactionReference()

    /**
     * Set the value of [pay_url] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     */
    public function setPayUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->pay_url !== $v) {
            $this->pay_url = $v;
            $this->modifiedColumns[SaleOrderPaymentTableMap::COL_PAY_URL] = true;
        }

        return $this;
    } // setPayUrl()

    /**
     * Sets the value of [started_on] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     */
    public function setStartedOn($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->started_on !== null || $dt !== null) {
            if ($this->started_on === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->started_on->format("Y-m-d H:i:s.u")) {
                $this->started_on = $dt === null ? null : clone $dt;
                $this->modifiedColumns[SaleOrderPaymentTableMap::COL_STARTED_ON] = true;
            }
        } // if either are not null

        return $this;
    } // setStartedOn()

    /**
     * Sets the value of the [is_paid] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     */
    public function setIsPaid($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_paid !== $v) {
            $this->is_paid = $v;
            $this->modifiedColumns[SaleOrderPaymentTableMap::COL_IS_PAID] = true;
        }

        return $this;
    } // setIsPaid()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_paid !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SaleOrderPaymentTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SaleOrderPaymentTableMap::translateFieldName('SaleOrderId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sale_order_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SaleOrderPaymentTableMap::translateFieldName('PaymentMethodId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->payment_method_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SaleOrderPaymentTableMap::translateFieldName('TotalPrice', TableMap::TYPE_PHPNAME, $indexType)];
            $this->total_price = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : SaleOrderPaymentTableMap::translateFieldName('OurSecretHash', TableMap::TYPE_PHPNAME, $indexType)];
            $this->our_secret_hash = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : SaleOrderPaymentTableMap::translateFieldName('Trxid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->trxid = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : SaleOrderPaymentTableMap::translateFieldName('TraceReference', TableMap::TYPE_PHPNAME, $indexType)];
            $this->trace_reference = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : SaleOrderPaymentTableMap::translateFieldName('PaymentReference', TableMap::TYPE_PHPNAME, $indexType)];
            $this->payment_reference = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : SaleOrderPaymentTableMap::translateFieldName('TransactionReference', TableMap::TYPE_PHPNAME, $indexType)];
            $this->transaction_reference = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : SaleOrderPaymentTableMap::translateFieldName('PayUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->pay_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : SaleOrderPaymentTableMap::translateFieldName('StartedOn', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->started_on = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : SaleOrderPaymentTableMap::translateFieldName('IsPaid', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_paid = (null !== $col) ? (boolean) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 12; // 12 = SaleOrderPaymentTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Sale\\SaleOrderPayment'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aSaleOrder !== null && $this->sale_order_id !== $this->aSaleOrder->getId()) {
            $this->aSaleOrder = null;
        }
        if ($this->aPaymentMethod !== null && $this->payment_method_id !== $this->aPaymentMethod->getId()) {
            $this->aPaymentMethod = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SaleOrderPaymentTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSaleOrderPaymentQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSaleOrder = null;
            $this->aPaymentMethod = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see SaleOrderPayment::setDeleted()
     * @see SaleOrderPayment::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderPaymentTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSaleOrderPaymentQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderPaymentTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SaleOrderPaymentTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSaleOrder !== null) {
                if ($this->aSaleOrder->isModified() || $this->aSaleOrder->isNew()) {
                    $affectedRows += $this->aSaleOrder->save($con);
                }
                $this->setSaleOrder($this->aSaleOrder);
            }

            if ($this->aPaymentMethod !== null) {
                if ($this->aPaymentMethod->isModified() || $this->aPaymentMethod->isNew()) {
                    $affectedRows += $this->aPaymentMethod->save($con);
                }
                $this->setPaymentMethod($this->aPaymentMethod);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[SaleOrderPaymentTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SaleOrderPaymentTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_SALE_ORDER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'sale_order_id';
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_PAYMENT_METHOD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'payment_method_id';
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_TOTAL_PRICE)) {
            $modifiedColumns[':p' . $index++]  = 'total_price';
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_OUR_SECRET_HASH)) {
            $modifiedColumns[':p' . $index++]  = 'our_secret_hash';
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_TRXID)) {
            $modifiedColumns[':p' . $index++]  = 'trxid';
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_TRACE_REFERENCE)) {
            $modifiedColumns[':p' . $index++]  = 'trace_reference';
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_PAYMENT_REFERENCE)) {
            $modifiedColumns[':p' . $index++]  = 'payment_reference';
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_TRANSACTION_REFERENCE)) {
            $modifiedColumns[':p' . $index++]  = 'transaction_reference';
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_PAY_URL)) {
            $modifiedColumns[':p' . $index++]  = 'pay_url';
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_STARTED_ON)) {
            $modifiedColumns[':p' . $index++]  = 'started_on';
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_IS_PAID)) {
            $modifiedColumns[':p' . $index++]  = 'is_paid';
        }

        $sql = sprintf(
            'INSERT INTO sale_order_payment (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'sale_order_id':
                        $stmt->bindValue($identifier, $this->sale_order_id, PDO::PARAM_INT);
                        break;
                    case 'payment_method_id':
                        $stmt->bindValue($identifier, $this->payment_method_id, PDO::PARAM_INT);
                        break;
                    case 'total_price':
                        $stmt->bindValue($identifier, $this->total_price, PDO::PARAM_STR);
                        break;
                    case 'our_secret_hash':
                        $stmt->bindValue($identifier, $this->our_secret_hash, PDO::PARAM_STR);
                        break;
                    case 'trxid':
                        $stmt->bindValue($identifier, $this->trxid, PDO::PARAM_STR);
                        break;
                    case 'trace_reference':
                        $stmt->bindValue($identifier, $this->trace_reference, PDO::PARAM_STR);
                        break;
                    case 'payment_reference':
                        $stmt->bindValue($identifier, $this->payment_reference, PDO::PARAM_STR);
                        break;
                    case 'transaction_reference':
                        $stmt->bindValue($identifier, $this->transaction_reference, PDO::PARAM_STR);
                        break;
                    case 'pay_url':
                        $stmt->bindValue($identifier, $this->pay_url, PDO::PARAM_STR);
                        break;
                    case 'started_on':
                        $stmt->bindValue($identifier, $this->started_on ? $this->started_on->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'is_paid':
                        $stmt->bindValue($identifier, (int) $this->is_paid, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SaleOrderPaymentTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getSaleOrderId();
                break;
            case 2:
                return $this->getPaymentMethodId();
                break;
            case 3:
                return $this->getTotalPrice();
                break;
            case 4:
                return $this->getOurSecretHash();
                break;
            case 5:
                return $this->getTrxid();
                break;
            case 6:
                return $this->getTraceReference();
                break;
            case 7:
                return $this->getPaymentReference();
                break;
            case 8:
                return $this->getTransactionReference();
                break;
            case 9:
                return $this->getPayUrl();
                break;
            case 10:
                return $this->getStartedOn();
                break;
            case 11:
                return $this->getIsPaid();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['SaleOrderPayment'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SaleOrderPayment'][$this->hashCode()] = true;
        $keys = SaleOrderPaymentTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getSaleOrderId(),
            $keys[2] => $this->getPaymentMethodId(),
            $keys[3] => $this->getTotalPrice(),
            $keys[4] => $this->getOurSecretHash(),
            $keys[5] => $this->getTrxid(),
            $keys[6] => $this->getTraceReference(),
            $keys[7] => $this->getPaymentReference(),
            $keys[8] => $this->getTransactionReference(),
            $keys[9] => $this->getPayUrl(),
            $keys[10] => $this->getStartedOn(),
            $keys[11] => $this->getIsPaid(),
        );
        if ($result[$keys[10]] instanceof \DateTimeInterface) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSaleOrder) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'saleOrder';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_order';
                        break;
                    default:
                        $key = 'SaleOrder';
                }

                $result[$key] = $this->aSaleOrder->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aPaymentMethod) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'paymentMethod';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'payment_method';
                        break;
                    default:
                        $key = 'PaymentMethod';
                }

                $result[$key] = $this->aPaymentMethod->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Sale\SaleOrderPayment
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SaleOrderPaymentTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Sale\SaleOrderPayment
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setSaleOrderId($value);
                break;
            case 2:
                $this->setPaymentMethodId($value);
                break;
            case 3:
                $this->setTotalPrice($value);
                break;
            case 4:
                $this->setOurSecretHash($value);
                break;
            case 5:
                $this->setTrxid($value);
                break;
            case 6:
                $this->setTraceReference($value);
                break;
            case 7:
                $this->setPaymentReference($value);
                break;
            case 8:
                $this->setTransactionReference($value);
                break;
            case 9:
                $this->setPayUrl($value);
                break;
            case 10:
                $this->setStartedOn($value);
                break;
            case 11:
                $this->setIsPaid($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SaleOrderPaymentTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setSaleOrderId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setPaymentMethodId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setTotalPrice($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setOurSecretHash($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setTrxid($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setTraceReference($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setPaymentReference($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTransactionReference($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setPayUrl($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setStartedOn($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setIsPaid($arr[$keys[11]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Sale\SaleOrderPayment The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SaleOrderPaymentTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_ID)) {
            $criteria->add(SaleOrderPaymentTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_SALE_ORDER_ID)) {
            $criteria->add(SaleOrderPaymentTableMap::COL_SALE_ORDER_ID, $this->sale_order_id);
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_PAYMENT_METHOD_ID)) {
            $criteria->add(SaleOrderPaymentTableMap::COL_PAYMENT_METHOD_ID, $this->payment_method_id);
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_TOTAL_PRICE)) {
            $criteria->add(SaleOrderPaymentTableMap::COL_TOTAL_PRICE, $this->total_price);
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_OUR_SECRET_HASH)) {
            $criteria->add(SaleOrderPaymentTableMap::COL_OUR_SECRET_HASH, $this->our_secret_hash);
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_TRXID)) {
            $criteria->add(SaleOrderPaymentTableMap::COL_TRXID, $this->trxid);
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_TRACE_REFERENCE)) {
            $criteria->add(SaleOrderPaymentTableMap::COL_TRACE_REFERENCE, $this->trace_reference);
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_PAYMENT_REFERENCE)) {
            $criteria->add(SaleOrderPaymentTableMap::COL_PAYMENT_REFERENCE, $this->payment_reference);
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_TRANSACTION_REFERENCE)) {
            $criteria->add(SaleOrderPaymentTableMap::COL_TRANSACTION_REFERENCE, $this->transaction_reference);
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_PAY_URL)) {
            $criteria->add(SaleOrderPaymentTableMap::COL_PAY_URL, $this->pay_url);
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_STARTED_ON)) {
            $criteria->add(SaleOrderPaymentTableMap::COL_STARTED_ON, $this->started_on);
        }
        if ($this->isColumnModified(SaleOrderPaymentTableMap::COL_IS_PAID)) {
            $criteria->add(SaleOrderPaymentTableMap::COL_IS_PAID, $this->is_paid);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSaleOrderPaymentQuery::create();
        $criteria->add(SaleOrderPaymentTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Sale\SaleOrderPayment (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSaleOrderId($this->getSaleOrderId());
        $copyObj->setPaymentMethodId($this->getPaymentMethodId());
        $copyObj->setTotalPrice($this->getTotalPrice());
        $copyObj->setOurSecretHash($this->getOurSecretHash());
        $copyObj->setTrxid($this->getTrxid());
        $copyObj->setTraceReference($this->getTraceReference());
        $copyObj->setPaymentReference($this->getPaymentReference());
        $copyObj->setTransactionReference($this->getTransactionReference());
        $copyObj->setPayUrl($this->getPayUrl());
        $copyObj->setStartedOn($this->getStartedOn());
        $copyObj->setIsPaid($this->getIsPaid());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Sale\SaleOrderPayment Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildSaleOrder object.
     *
     * @param  ChildSaleOrder|null $v
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSaleOrder(ChildSaleOrder $v = null)
    {
        if ($v === null) {
            $this->setSaleOrderId(NULL);
        } else {
            $this->setSaleOrderId($v->getId());
        }

        $this->aSaleOrder = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSaleOrder object, it will not be re-added.
        if ($v !== null) {
            $v->addSaleOrderPayment($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSaleOrder object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSaleOrder|null The associated ChildSaleOrder object.
     * @throws PropelException
     */
    public function getSaleOrder(ConnectionInterface $con = null)
    {
        if ($this->aSaleOrder === null && ($this->sale_order_id != 0)) {
            $this->aSaleOrder = ChildSaleOrderQuery::create()->findPk($this->sale_order_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSaleOrder->addSaleOrderPayments($this);
             */
        }

        return $this->aSaleOrder;
    }

    /**
     * Declares an association between this object and a PaymentMethod object.
     *
     * @param  PaymentMethod|null $v
     * @return $this|\Model\Sale\SaleOrderPayment The current object (for fluent API support)
     * @throws PropelException
     */
    public function setPaymentMethod(PaymentMethod $v = null)
    {
        if ($v === null) {
            $this->setPaymentMethodId(NULL);
        } else {
            $this->setPaymentMethodId($v->getId());
        }

        $this->aPaymentMethod = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the PaymentMethod object, it will not be re-added.
        if ($v !== null) {
            $v->addSaleOrderPayment($this);
        }


        return $this;
    }


    /**
     * Get the associated PaymentMethod object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return PaymentMethod|null The associated PaymentMethod object.
     * @throws PropelException
     */
    public function getPaymentMethod(ConnectionInterface $con = null)
    {
        if ($this->aPaymentMethod === null && ($this->payment_method_id != 0)) {
            $this->aPaymentMethod = PaymentMethodQuery::create()->findPk($this->payment_method_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aPaymentMethod->addSaleOrderPayments($this);
             */
        }

        return $this->aPaymentMethod;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aSaleOrder) {
            $this->aSaleOrder->removeSaleOrderPayment($this);
        }
        if (null !== $this->aPaymentMethod) {
            $this->aPaymentMethod->removeSaleOrderPayment($this);
        }
        $this->id = null;
        $this->sale_order_id = null;
        $this->payment_method_id = null;
        $this->total_price = null;
        $this->our_secret_hash = null;
        $this->trxid = null;
        $this->trace_reference = null;
        $this->payment_reference = null;
        $this->transaction_reference = null;
        $this->pay_url = null;
        $this->started_on = null;
        $this->is_paid = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aSaleOrder = null;
        $this->aPaymentMethod = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SaleOrderPaymentTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
