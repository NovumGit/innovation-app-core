<?php

namespace Model\Sale\Base;

use \Exception;
use \PDO;
use Model\Sale\SaleOrderItem as ChildSaleOrderItem;
use Model\Sale\SaleOrderItemQuery as ChildSaleOrderItemQuery;
use Model\Sale\Map\SaleOrderItemTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'sale_order_item' table.
 *
 *
 *
 * @method     ChildSaleOrderItemQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSaleOrderItemQuery orderByCreatedOn($order = Criteria::ASC) Order by the created_on column
 * @method     ChildSaleOrderItemQuery orderBySaleOrderId($order = Criteria::ASC) Order by the sale_order_id column
 * @method     ChildSaleOrderItemQuery orderByProductNumber($order = Criteria::ASC) Order by the product_number column
 * @method     ChildSaleOrderItemQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildSaleOrderItemQuery orderByQuantity($order = Criteria::ASC) Order by the quantity column
 * @method     ChildSaleOrderItemQuery orderBySingleItemWeight($order = Criteria::ASC) Order by the single_item_weight column
 * @method     ChildSaleOrderItemQuery orderByPrice($order = Criteria::ASC) Order by the price column
 * @method     ChildSaleOrderItemQuery orderByVatPercentage($order = Criteria::ASC) Order by the vat_percentage column
 * @method     ChildSaleOrderItemQuery orderByManualEntry($order = Criteria::ASC) Order by the manual_entry column
 *
 * @method     ChildSaleOrderItemQuery groupById() Group by the id column
 * @method     ChildSaleOrderItemQuery groupByCreatedOn() Group by the created_on column
 * @method     ChildSaleOrderItemQuery groupBySaleOrderId() Group by the sale_order_id column
 * @method     ChildSaleOrderItemQuery groupByProductNumber() Group by the product_number column
 * @method     ChildSaleOrderItemQuery groupByDescription() Group by the description column
 * @method     ChildSaleOrderItemQuery groupByQuantity() Group by the quantity column
 * @method     ChildSaleOrderItemQuery groupBySingleItemWeight() Group by the single_item_weight column
 * @method     ChildSaleOrderItemQuery groupByPrice() Group by the price column
 * @method     ChildSaleOrderItemQuery groupByVatPercentage() Group by the vat_percentage column
 * @method     ChildSaleOrderItemQuery groupByManualEntry() Group by the manual_entry column
 *
 * @method     ChildSaleOrderItemQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSaleOrderItemQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSaleOrderItemQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSaleOrderItemQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSaleOrderItemQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSaleOrderItemQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSaleOrderItemQuery leftJoinSaleOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrder relation
 * @method     ChildSaleOrderItemQuery rightJoinSaleOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrder relation
 * @method     ChildSaleOrderItemQuery innerJoinSaleOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrder relation
 *
 * @method     ChildSaleOrderItemQuery joinWithSaleOrder($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrder relation
 *
 * @method     ChildSaleOrderItemQuery leftJoinWithSaleOrder() Adds a LEFT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildSaleOrderItemQuery rightJoinWithSaleOrder() Adds a RIGHT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildSaleOrderItemQuery innerJoinWithSaleOrder() Adds a INNER JOIN clause and with to the query using the SaleOrder relation
 *
 * @method     ChildSaleOrderItemQuery leftJoinSaleOrderItemProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrderItemProduct relation
 * @method     ChildSaleOrderItemQuery rightJoinSaleOrderItemProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrderItemProduct relation
 * @method     ChildSaleOrderItemQuery innerJoinSaleOrderItemProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrderItemProduct relation
 *
 * @method     ChildSaleOrderItemQuery joinWithSaleOrderItemProduct($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrderItemProduct relation
 *
 * @method     ChildSaleOrderItemQuery leftJoinWithSaleOrderItemProduct() Adds a LEFT JOIN clause and with to the query using the SaleOrderItemProduct relation
 * @method     ChildSaleOrderItemQuery rightJoinWithSaleOrderItemProduct() Adds a RIGHT JOIN clause and with to the query using the SaleOrderItemProduct relation
 * @method     ChildSaleOrderItemQuery innerJoinWithSaleOrderItemProduct() Adds a INNER JOIN clause and with to the query using the SaleOrderItemProduct relation
 *
 * @method     ChildSaleOrderItemQuery leftJoinSaleOrderItemStockClaimed($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrderItemStockClaimed relation
 * @method     ChildSaleOrderItemQuery rightJoinSaleOrderItemStockClaimed($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrderItemStockClaimed relation
 * @method     ChildSaleOrderItemQuery innerJoinSaleOrderItemStockClaimed($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrderItemStockClaimed relation
 *
 * @method     ChildSaleOrderItemQuery joinWithSaleOrderItemStockClaimed($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrderItemStockClaimed relation
 *
 * @method     ChildSaleOrderItemQuery leftJoinWithSaleOrderItemStockClaimed() Adds a LEFT JOIN clause and with to the query using the SaleOrderItemStockClaimed relation
 * @method     ChildSaleOrderItemQuery rightJoinWithSaleOrderItemStockClaimed() Adds a RIGHT JOIN clause and with to the query using the SaleOrderItemStockClaimed relation
 * @method     ChildSaleOrderItemQuery innerJoinWithSaleOrderItemStockClaimed() Adds a INNER JOIN clause and with to the query using the SaleOrderItemStockClaimed relation
 *
 * @method     \Model\Sale\SaleOrderQuery|\Model\Sale\OrderItemProductQuery|\Model\Sale\SaleOrderItemStockClaimedQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSaleOrderItem findOne(ConnectionInterface $con = null) Return the first ChildSaleOrderItem matching the query
 * @method     ChildSaleOrderItem findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSaleOrderItem matching the query, or a new ChildSaleOrderItem object populated from the query conditions when no match is found
 *
 * @method     ChildSaleOrderItem findOneById(int $id) Return the first ChildSaleOrderItem filtered by the id column
 * @method     ChildSaleOrderItem findOneByCreatedOn(string $created_on) Return the first ChildSaleOrderItem filtered by the created_on column
 * @method     ChildSaleOrderItem findOneBySaleOrderId(int $sale_order_id) Return the first ChildSaleOrderItem filtered by the sale_order_id column
 * @method     ChildSaleOrderItem findOneByProductNumber(string $product_number) Return the first ChildSaleOrderItem filtered by the product_number column
 * @method     ChildSaleOrderItem findOneByDescription(string $description) Return the first ChildSaleOrderItem filtered by the description column
 * @method     ChildSaleOrderItem findOneByQuantity(double $quantity) Return the first ChildSaleOrderItem filtered by the quantity column
 * @method     ChildSaleOrderItem findOneBySingleItemWeight(int $single_item_weight) Return the first ChildSaleOrderItem filtered by the single_item_weight column
 * @method     ChildSaleOrderItem findOneByPrice(double $price) Return the first ChildSaleOrderItem filtered by the price column
 * @method     ChildSaleOrderItem findOneByVatPercentage(int $vat_percentage) Return the first ChildSaleOrderItem filtered by the vat_percentage column
 * @method     ChildSaleOrderItem findOneByManualEntry(boolean $manual_entry) Return the first ChildSaleOrderItem filtered by the manual_entry column *

 * @method     ChildSaleOrderItem requirePk($key, ConnectionInterface $con = null) Return the ChildSaleOrderItem by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItem requireOne(ConnectionInterface $con = null) Return the first ChildSaleOrderItem matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleOrderItem requireOneById(int $id) Return the first ChildSaleOrderItem filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItem requireOneByCreatedOn(string $created_on) Return the first ChildSaleOrderItem filtered by the created_on column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItem requireOneBySaleOrderId(int $sale_order_id) Return the first ChildSaleOrderItem filtered by the sale_order_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItem requireOneByProductNumber(string $product_number) Return the first ChildSaleOrderItem filtered by the product_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItem requireOneByDescription(string $description) Return the first ChildSaleOrderItem filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItem requireOneByQuantity(double $quantity) Return the first ChildSaleOrderItem filtered by the quantity column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItem requireOneBySingleItemWeight(int $single_item_weight) Return the first ChildSaleOrderItem filtered by the single_item_weight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItem requireOneByPrice(double $price) Return the first ChildSaleOrderItem filtered by the price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItem requireOneByVatPercentage(int $vat_percentage) Return the first ChildSaleOrderItem filtered by the vat_percentage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItem requireOneByManualEntry(boolean $manual_entry) Return the first ChildSaleOrderItem filtered by the manual_entry column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleOrderItem[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSaleOrderItem objects based on current ModelCriteria
 * @method     ChildSaleOrderItem[]|ObjectCollection findById(int $id) Return ChildSaleOrderItem objects filtered by the id column
 * @method     ChildSaleOrderItem[]|ObjectCollection findByCreatedOn(string $created_on) Return ChildSaleOrderItem objects filtered by the created_on column
 * @method     ChildSaleOrderItem[]|ObjectCollection findBySaleOrderId(int $sale_order_id) Return ChildSaleOrderItem objects filtered by the sale_order_id column
 * @method     ChildSaleOrderItem[]|ObjectCollection findByProductNumber(string $product_number) Return ChildSaleOrderItem objects filtered by the product_number column
 * @method     ChildSaleOrderItem[]|ObjectCollection findByDescription(string $description) Return ChildSaleOrderItem objects filtered by the description column
 * @method     ChildSaleOrderItem[]|ObjectCollection findByQuantity(double $quantity) Return ChildSaleOrderItem objects filtered by the quantity column
 * @method     ChildSaleOrderItem[]|ObjectCollection findBySingleItemWeight(int $single_item_weight) Return ChildSaleOrderItem objects filtered by the single_item_weight column
 * @method     ChildSaleOrderItem[]|ObjectCollection findByPrice(double $price) Return ChildSaleOrderItem objects filtered by the price column
 * @method     ChildSaleOrderItem[]|ObjectCollection findByVatPercentage(int $vat_percentage) Return ChildSaleOrderItem objects filtered by the vat_percentage column
 * @method     ChildSaleOrderItem[]|ObjectCollection findByManualEntry(boolean $manual_entry) Return ChildSaleOrderItem objects filtered by the manual_entry column
 * @method     ChildSaleOrderItem[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SaleOrderItemQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Sale\Base\SaleOrderItemQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Sale\\SaleOrderItem', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSaleOrderItemQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSaleOrderItemQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSaleOrderItemQuery) {
            return $criteria;
        }
        $query = new ChildSaleOrderItemQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSaleOrderItem|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SaleOrderItemTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SaleOrderItemTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderItem A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, created_on, sale_order_id, product_number, description, quantity, single_item_weight, price, vat_percentage, manual_entry FROM sale_order_item WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSaleOrderItem $obj */
            $obj = new ChildSaleOrderItem();
            $obj->hydrate($row);
            SaleOrderItemTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSaleOrderItem|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SaleOrderItemTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SaleOrderItemTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the created_on column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedOn('2011-03-14'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn('now'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn(array('max' => 'yesterday')); // WHERE created_on > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdOn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterByCreatedOn($createdOn = null, $comparison = null)
    {
        if (is_array($createdOn)) {
            $useMinMax = false;
            if (isset($createdOn['min'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_CREATED_ON, $createdOn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdOn['max'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_CREATED_ON, $createdOn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemTableMap::COL_CREATED_ON, $createdOn, $comparison);
    }

    /**
     * Filter the query on the sale_order_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySaleOrderId(1234); // WHERE sale_order_id = 1234
     * $query->filterBySaleOrderId(array(12, 34)); // WHERE sale_order_id IN (12, 34)
     * $query->filterBySaleOrderId(array('min' => 12)); // WHERE sale_order_id > 12
     * </code>
     *
     * @see       filterBySaleOrder()
     *
     * @param     mixed $saleOrderId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterBySaleOrderId($saleOrderId = null, $comparison = null)
    {
        if (is_array($saleOrderId)) {
            $useMinMax = false;
            if (isset($saleOrderId['min'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_SALE_ORDER_ID, $saleOrderId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($saleOrderId['max'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_SALE_ORDER_ID, $saleOrderId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemTableMap::COL_SALE_ORDER_ID, $saleOrderId, $comparison);
    }

    /**
     * Filter the query on the product_number column
     *
     * Example usage:
     * <code>
     * $query->filterByProductNumber('fooValue');   // WHERE product_number = 'fooValue'
     * $query->filterByProductNumber('%fooValue%', Criteria::LIKE); // WHERE product_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $productNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterByProductNumber($productNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($productNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemTableMap::COL_PRODUCT_NUMBER, $productNumber, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the quantity column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantity(1234); // WHERE quantity = 1234
     * $query->filterByQuantity(array(12, 34)); // WHERE quantity IN (12, 34)
     * $query->filterByQuantity(array('min' => 12)); // WHERE quantity > 12
     * </code>
     *
     * @param     mixed $quantity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterByQuantity($quantity = null, $comparison = null)
    {
        if (is_array($quantity)) {
            $useMinMax = false;
            if (isset($quantity['min'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_QUANTITY, $quantity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quantity['max'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_QUANTITY, $quantity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemTableMap::COL_QUANTITY, $quantity, $comparison);
    }

    /**
     * Filter the query on the single_item_weight column
     *
     * Example usage:
     * <code>
     * $query->filterBySingleItemWeight(1234); // WHERE single_item_weight = 1234
     * $query->filterBySingleItemWeight(array(12, 34)); // WHERE single_item_weight IN (12, 34)
     * $query->filterBySingleItemWeight(array('min' => 12)); // WHERE single_item_weight > 12
     * </code>
     *
     * @param     mixed $singleItemWeight The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterBySingleItemWeight($singleItemWeight = null, $comparison = null)
    {
        if (is_array($singleItemWeight)) {
            $useMinMax = false;
            if (isset($singleItemWeight['min'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_SINGLE_ITEM_WEIGHT, $singleItemWeight['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($singleItemWeight['max'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_SINGLE_ITEM_WEIGHT, $singleItemWeight['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemTableMap::COL_SINGLE_ITEM_WEIGHT, $singleItemWeight, $comparison);
    }

    /**
     * Filter the query on the price column
     *
     * Example usage:
     * <code>
     * $query->filterByPrice(1234); // WHERE price = 1234
     * $query->filterByPrice(array(12, 34)); // WHERE price IN (12, 34)
     * $query->filterByPrice(array('min' => 12)); // WHERE price > 12
     * </code>
     *
     * @param     mixed $price The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterByPrice($price = null, $comparison = null)
    {
        if (is_array($price)) {
            $useMinMax = false;
            if (isset($price['min'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_PRICE, $price['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($price['max'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_PRICE, $price['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemTableMap::COL_PRICE, $price, $comparison);
    }

    /**
     * Filter the query on the vat_percentage column
     *
     * Example usage:
     * <code>
     * $query->filterByVatPercentage(1234); // WHERE vat_percentage = 1234
     * $query->filterByVatPercentage(array(12, 34)); // WHERE vat_percentage IN (12, 34)
     * $query->filterByVatPercentage(array('min' => 12)); // WHERE vat_percentage > 12
     * </code>
     *
     * @param     mixed $vatPercentage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterByVatPercentage($vatPercentage = null, $comparison = null)
    {
        if (is_array($vatPercentage)) {
            $useMinMax = false;
            if (isset($vatPercentage['min'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_VAT_PERCENTAGE, $vatPercentage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($vatPercentage['max'])) {
                $this->addUsingAlias(SaleOrderItemTableMap::COL_VAT_PERCENTAGE, $vatPercentage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemTableMap::COL_VAT_PERCENTAGE, $vatPercentage, $comparison);
    }

    /**
     * Filter the query on the manual_entry column
     *
     * Example usage:
     * <code>
     * $query->filterByManualEntry(true); // WHERE manual_entry = true
     * $query->filterByManualEntry('yes'); // WHERE manual_entry = true
     * </code>
     *
     * @param     boolean|string $manualEntry The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterByManualEntry($manualEntry = null, $comparison = null)
    {
        if (is_string($manualEntry)) {
            $manualEntry = in_array(strtolower($manualEntry), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderItemTableMap::COL_MANUAL_ENTRY, $manualEntry, $comparison);
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrder object
     *
     * @param \Model\Sale\SaleOrder|ObjectCollection $saleOrder The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterBySaleOrder($saleOrder, $comparison = null)
    {
        if ($saleOrder instanceof \Model\Sale\SaleOrder) {
            return $this
                ->addUsingAlias(SaleOrderItemTableMap::COL_SALE_ORDER_ID, $saleOrder->getId(), $comparison);
        } elseif ($saleOrder instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaleOrderItemTableMap::COL_SALE_ORDER_ID, $saleOrder->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySaleOrder() only accepts arguments of type \Model\Sale\SaleOrder or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function joinSaleOrder($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrder');
        }

        return $this;
    }

    /**
     * Use the SaleOrder relation SaleOrder object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaleOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrder', '\Model\Sale\SaleOrderQuery');
    }

    /**
     * Filter the query by a related \Model\Sale\OrderItemProduct object
     *
     * @param \Model\Sale\OrderItemProduct|ObjectCollection $orderItemProduct the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterBySaleOrderItemProduct($orderItemProduct, $comparison = null)
    {
        if ($orderItemProduct instanceof \Model\Sale\OrderItemProduct) {
            return $this
                ->addUsingAlias(SaleOrderItemTableMap::COL_ID, $orderItemProduct->getSaleOrderItemId(), $comparison);
        } elseif ($orderItemProduct instanceof ObjectCollection) {
            return $this
                ->useSaleOrderItemProductQuery()
                ->filterByPrimaryKeys($orderItemProduct->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleOrderItemProduct() only accepts arguments of type \Model\Sale\OrderItemProduct or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrderItemProduct relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function joinSaleOrderItemProduct($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrderItemProduct');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrderItemProduct');
        }

        return $this;
    }

    /**
     * Use the SaleOrderItemProduct relation OrderItemProduct object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\OrderItemProductQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderItemProductQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaleOrderItemProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrderItemProduct', '\Model\Sale\OrderItemProductQuery');
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrderItemStockClaimed object
     *
     * @param \Model\Sale\SaleOrderItemStockClaimed|ObjectCollection $saleOrderItemStockClaimed the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function filterBySaleOrderItemStockClaimed($saleOrderItemStockClaimed, $comparison = null)
    {
        if ($saleOrderItemStockClaimed instanceof \Model\Sale\SaleOrderItemStockClaimed) {
            return $this
                ->addUsingAlias(SaleOrderItemTableMap::COL_ID, $saleOrderItemStockClaimed->getSaleOrderItemId(), $comparison);
        } elseif ($saleOrderItemStockClaimed instanceof ObjectCollection) {
            return $this
                ->useSaleOrderItemStockClaimedQuery()
                ->filterByPrimaryKeys($saleOrderItemStockClaimed->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleOrderItemStockClaimed() only accepts arguments of type \Model\Sale\SaleOrderItemStockClaimed or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrderItemStockClaimed relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function joinSaleOrderItemStockClaimed($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrderItemStockClaimed');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrderItemStockClaimed');
        }

        return $this;
    }

    /**
     * Use the SaleOrderItemStockClaimed relation SaleOrderItemStockClaimed object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderItemStockClaimedQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderItemStockClaimedQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaleOrderItemStockClaimed($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrderItemStockClaimed', '\Model\Sale\SaleOrderItemStockClaimedQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSaleOrderItem $saleOrderItem Object to remove from the list of results
     *
     * @return $this|ChildSaleOrderItemQuery The current query, for fluid interface
     */
    public function prune($saleOrderItem = null)
    {
        if ($saleOrderItem) {
            $this->addUsingAlias(SaleOrderItemTableMap::COL_ID, $saleOrderItem->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the sale_order_item table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderItemTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SaleOrderItemTableMap::clearInstancePool();
            SaleOrderItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderItemTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SaleOrderItemTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SaleOrderItemTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SaleOrderItemTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 3600)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SaleOrderItemTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(SaleOrderItemTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

} // SaleOrderItemQuery
