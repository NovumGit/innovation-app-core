<?php

namespace Model\Sale\Base;

use \Exception;
use \PDO;
use Model\Product;
use Model\Sale\SaleOrderItemStockClaimed as ChildSaleOrderItemStockClaimed;
use Model\Sale\SaleOrderItemStockClaimedQuery as ChildSaleOrderItemStockClaimedQuery;
use Model\Sale\Map\SaleOrderItemStockClaimedTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'sale_order_item_stock_claimed' table.
 *
 *
 *
 * @method     ChildSaleOrderItemStockClaimedQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSaleOrderItemStockClaimedQuery orderBySaleOrderItemId($order = Criteria::ASC) Order by the sale_order_item_id column
 * @method     ChildSaleOrderItemStockClaimedQuery orderByProductId($order = Criteria::ASC) Order by the product_id column
 * @method     ChildSaleOrderItemStockClaimedQuery orderByCreatedOn($order = Criteria::ASC) Order by the created_on column
 * @method     ChildSaleOrderItemStockClaimedQuery orderByQuantity($order = Criteria::ASC) Order by the quantity column
 *
 * @method     ChildSaleOrderItemStockClaimedQuery groupById() Group by the id column
 * @method     ChildSaleOrderItemStockClaimedQuery groupBySaleOrderItemId() Group by the sale_order_item_id column
 * @method     ChildSaleOrderItemStockClaimedQuery groupByProductId() Group by the product_id column
 * @method     ChildSaleOrderItemStockClaimedQuery groupByCreatedOn() Group by the created_on column
 * @method     ChildSaleOrderItemStockClaimedQuery groupByQuantity() Group by the quantity column
 *
 * @method     ChildSaleOrderItemStockClaimedQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSaleOrderItemStockClaimedQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSaleOrderItemStockClaimedQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSaleOrderItemStockClaimedQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSaleOrderItemStockClaimedQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSaleOrderItemStockClaimedQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSaleOrderItemStockClaimedQuery leftJoinProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the Product relation
 * @method     ChildSaleOrderItemStockClaimedQuery rightJoinProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Product relation
 * @method     ChildSaleOrderItemStockClaimedQuery innerJoinProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the Product relation
 *
 * @method     ChildSaleOrderItemStockClaimedQuery joinWithProduct($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Product relation
 *
 * @method     ChildSaleOrderItemStockClaimedQuery leftJoinWithProduct() Adds a LEFT JOIN clause and with to the query using the Product relation
 * @method     ChildSaleOrderItemStockClaimedQuery rightJoinWithProduct() Adds a RIGHT JOIN clause and with to the query using the Product relation
 * @method     ChildSaleOrderItemStockClaimedQuery innerJoinWithProduct() Adds a INNER JOIN clause and with to the query using the Product relation
 *
 * @method     ChildSaleOrderItemStockClaimedQuery leftJoinSaleOrderItem($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrderItem relation
 * @method     ChildSaleOrderItemStockClaimedQuery rightJoinSaleOrderItem($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrderItem relation
 * @method     ChildSaleOrderItemStockClaimedQuery innerJoinSaleOrderItem($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrderItem relation
 *
 * @method     ChildSaleOrderItemStockClaimedQuery joinWithSaleOrderItem($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrderItem relation
 *
 * @method     ChildSaleOrderItemStockClaimedQuery leftJoinWithSaleOrderItem() Adds a LEFT JOIN clause and with to the query using the SaleOrderItem relation
 * @method     ChildSaleOrderItemStockClaimedQuery rightJoinWithSaleOrderItem() Adds a RIGHT JOIN clause and with to the query using the SaleOrderItem relation
 * @method     ChildSaleOrderItemStockClaimedQuery innerJoinWithSaleOrderItem() Adds a INNER JOIN clause and with to the query using the SaleOrderItem relation
 *
 * @method     \Model\ProductQuery|\Model\Sale\SaleOrderItemQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSaleOrderItemStockClaimed findOne(ConnectionInterface $con = null) Return the first ChildSaleOrderItemStockClaimed matching the query
 * @method     ChildSaleOrderItemStockClaimed findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSaleOrderItemStockClaimed matching the query, or a new ChildSaleOrderItemStockClaimed object populated from the query conditions when no match is found
 *
 * @method     ChildSaleOrderItemStockClaimed findOneById(int $id) Return the first ChildSaleOrderItemStockClaimed filtered by the id column
 * @method     ChildSaleOrderItemStockClaimed findOneBySaleOrderItemId(int $sale_order_item_id) Return the first ChildSaleOrderItemStockClaimed filtered by the sale_order_item_id column
 * @method     ChildSaleOrderItemStockClaimed findOneByProductId(int $product_id) Return the first ChildSaleOrderItemStockClaimed filtered by the product_id column
 * @method     ChildSaleOrderItemStockClaimed findOneByCreatedOn(string $created_on) Return the first ChildSaleOrderItemStockClaimed filtered by the created_on column
 * @method     ChildSaleOrderItemStockClaimed findOneByQuantity(double $quantity) Return the first ChildSaleOrderItemStockClaimed filtered by the quantity column *

 * @method     ChildSaleOrderItemStockClaimed requirePk($key, ConnectionInterface $con = null) Return the ChildSaleOrderItemStockClaimed by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItemStockClaimed requireOne(ConnectionInterface $con = null) Return the first ChildSaleOrderItemStockClaimed matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleOrderItemStockClaimed requireOneById(int $id) Return the first ChildSaleOrderItemStockClaimed filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItemStockClaimed requireOneBySaleOrderItemId(int $sale_order_item_id) Return the first ChildSaleOrderItemStockClaimed filtered by the sale_order_item_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItemStockClaimed requireOneByProductId(int $product_id) Return the first ChildSaleOrderItemStockClaimed filtered by the product_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItemStockClaimed requireOneByCreatedOn(string $created_on) Return the first ChildSaleOrderItemStockClaimed filtered by the created_on column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderItemStockClaimed requireOneByQuantity(double $quantity) Return the first ChildSaleOrderItemStockClaimed filtered by the quantity column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleOrderItemStockClaimed[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSaleOrderItemStockClaimed objects based on current ModelCriteria
 * @method     ChildSaleOrderItemStockClaimed[]|ObjectCollection findById(int $id) Return ChildSaleOrderItemStockClaimed objects filtered by the id column
 * @method     ChildSaleOrderItemStockClaimed[]|ObjectCollection findBySaleOrderItemId(int $sale_order_item_id) Return ChildSaleOrderItemStockClaimed objects filtered by the sale_order_item_id column
 * @method     ChildSaleOrderItemStockClaimed[]|ObjectCollection findByProductId(int $product_id) Return ChildSaleOrderItemStockClaimed objects filtered by the product_id column
 * @method     ChildSaleOrderItemStockClaimed[]|ObjectCollection findByCreatedOn(string $created_on) Return ChildSaleOrderItemStockClaimed objects filtered by the created_on column
 * @method     ChildSaleOrderItemStockClaimed[]|ObjectCollection findByQuantity(double $quantity) Return ChildSaleOrderItemStockClaimed objects filtered by the quantity column
 * @method     ChildSaleOrderItemStockClaimed[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SaleOrderItemStockClaimedQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Sale\Base\SaleOrderItemStockClaimedQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Sale\\SaleOrderItemStockClaimed', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSaleOrderItemStockClaimedQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSaleOrderItemStockClaimedQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSaleOrderItemStockClaimedQuery) {
            return $criteria;
        }
        $query = new ChildSaleOrderItemStockClaimedQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSaleOrderItemStockClaimed|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SaleOrderItemStockClaimedTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SaleOrderItemStockClaimedTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderItemStockClaimed A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, sale_order_item_id, product_id, created_on, quantity FROM sale_order_item_stock_claimed WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSaleOrderItemStockClaimed $obj */
            $obj = new ChildSaleOrderItemStockClaimed();
            $obj->hydrate($row);
            SaleOrderItemStockClaimedTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSaleOrderItemStockClaimed|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSaleOrderItemStockClaimedQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSaleOrderItemStockClaimedQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemStockClaimedQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the sale_order_item_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySaleOrderItemId(1234); // WHERE sale_order_item_id = 1234
     * $query->filterBySaleOrderItemId(array(12, 34)); // WHERE sale_order_item_id IN (12, 34)
     * $query->filterBySaleOrderItemId(array('min' => 12)); // WHERE sale_order_item_id > 12
     * </code>
     *
     * @see       filterBySaleOrderItem()
     *
     * @param     mixed $saleOrderItemId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemStockClaimedQuery The current query, for fluid interface
     */
    public function filterBySaleOrderItemId($saleOrderItemId = null, $comparison = null)
    {
        if (is_array($saleOrderItemId)) {
            $useMinMax = false;
            if (isset($saleOrderItemId['min'])) {
                $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_SALE_ORDER_ITEM_ID, $saleOrderItemId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($saleOrderItemId['max'])) {
                $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_SALE_ORDER_ITEM_ID, $saleOrderItemId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_SALE_ORDER_ITEM_ID, $saleOrderItemId, $comparison);
    }

    /**
     * Filter the query on the product_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProductId(1234); // WHERE product_id = 1234
     * $query->filterByProductId(array(12, 34)); // WHERE product_id IN (12, 34)
     * $query->filterByProductId(array('min' => 12)); // WHERE product_id > 12
     * </code>
     *
     * @see       filterByProduct()
     *
     * @param     mixed $productId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemStockClaimedQuery The current query, for fluid interface
     */
    public function filterByProductId($productId = null, $comparison = null)
    {
        if (is_array($productId)) {
            $useMinMax = false;
            if (isset($productId['min'])) {
                $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_PRODUCT_ID, $productId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productId['max'])) {
                $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_PRODUCT_ID, $productId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_PRODUCT_ID, $productId, $comparison);
    }

    /**
     * Filter the query on the created_on column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedOn('2011-03-14'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn('now'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn(array('max' => 'yesterday')); // WHERE created_on > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdOn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemStockClaimedQuery The current query, for fluid interface
     */
    public function filterByCreatedOn($createdOn = null, $comparison = null)
    {
        if (is_array($createdOn)) {
            $useMinMax = false;
            if (isset($createdOn['min'])) {
                $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_CREATED_ON, $createdOn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdOn['max'])) {
                $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_CREATED_ON, $createdOn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_CREATED_ON, $createdOn, $comparison);
    }

    /**
     * Filter the query on the quantity column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantity(1234); // WHERE quantity = 1234
     * $query->filterByQuantity(array(12, 34)); // WHERE quantity IN (12, 34)
     * $query->filterByQuantity(array('min' => 12)); // WHERE quantity > 12
     * </code>
     *
     * @param     mixed $quantity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderItemStockClaimedQuery The current query, for fluid interface
     */
    public function filterByQuantity($quantity = null, $comparison = null)
    {
        if (is_array($quantity)) {
            $useMinMax = false;
            if (isset($quantity['min'])) {
                $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_QUANTITY, $quantity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quantity['max'])) {
                $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_QUANTITY, $quantity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_QUANTITY, $quantity, $comparison);
    }

    /**
     * Filter the query by a related \Model\Product object
     *
     * @param \Model\Product|ObjectCollection $product The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderItemStockClaimedQuery The current query, for fluid interface
     */
    public function filterByProduct($product, $comparison = null)
    {
        if ($product instanceof \Model\Product) {
            return $this
                ->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_PRODUCT_ID, $product->getId(), $comparison);
        } elseif ($product instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_PRODUCT_ID, $product->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProduct() only accepts arguments of type \Model\Product or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Product relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderItemStockClaimedQuery The current query, for fluid interface
     */
    public function joinProduct($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Product');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Product');
        }

        return $this;
    }

    /**
     * Use the Product relation Product object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ProductQuery A secondary query class using the current class as primary query
     */
    public function useProductQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Product', '\Model\ProductQuery');
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrderItem object
     *
     * @param \Model\Sale\SaleOrderItem|ObjectCollection $saleOrderItem The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderItemStockClaimedQuery The current query, for fluid interface
     */
    public function filterBySaleOrderItem($saleOrderItem, $comparison = null)
    {
        if ($saleOrderItem instanceof \Model\Sale\SaleOrderItem) {
            return $this
                ->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_SALE_ORDER_ITEM_ID, $saleOrderItem->getId(), $comparison);
        } elseif ($saleOrderItem instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_SALE_ORDER_ITEM_ID, $saleOrderItem->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySaleOrderItem() only accepts arguments of type \Model\Sale\SaleOrderItem or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrderItem relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderItemStockClaimedQuery The current query, for fluid interface
     */
    public function joinSaleOrderItem($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrderItem');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrderItem');
        }

        return $this;
    }

    /**
     * Use the SaleOrderItem relation SaleOrderItem object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderItemQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderItemQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaleOrderItem($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrderItem', '\Model\Sale\SaleOrderItemQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSaleOrderItemStockClaimed $saleOrderItemStockClaimed Object to remove from the list of results
     *
     * @return $this|ChildSaleOrderItemStockClaimedQuery The current query, for fluid interface
     */
    public function prune($saleOrderItemStockClaimed = null)
    {
        if ($saleOrderItemStockClaimed) {
            $this->addUsingAlias(SaleOrderItemStockClaimedTableMap::COL_ID, $saleOrderItemStockClaimed->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the sale_order_item_stock_claimed table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderItemStockClaimedTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SaleOrderItemStockClaimedTableMap::clearInstancePool();
            SaleOrderItemStockClaimedTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderItemStockClaimedTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SaleOrderItemStockClaimedTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SaleOrderItemStockClaimedTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SaleOrderItemStockClaimedTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SaleOrderItemStockClaimedQuery
