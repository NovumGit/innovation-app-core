<?php

namespace Model\Sale;

use Exception\LogicException;
use Model\Sale\Base\DhlShipment as BaseDhlShipment;

/**
 * Skeleton subclass for representing a row from the 'dhl_shipment' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DhlShipment extends BaseDhlShipment
{


	function __construct()
	{
		$this->setWeight(1000);
		$this->setDeliveryMethod('door_delivery');

	}

	function setDeliveryMethod($v)
	{
		if(!in_array($v, $aAllowed = ['picked_up', 'door_delivery']))
		{
			throw new LogicException("Deliverymethod should be one of " . join(', ', $aAllowed));
		}
		return parent::setDeliveryMethod($v);
	}
}
