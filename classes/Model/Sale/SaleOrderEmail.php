<?php

namespace Model\Sale;

use Model\Sale\Base\SaleOrderEmail as BaseSaleOrderEmail;
use Core\MailMessage;

/**
 * Skeleton subclass for representing a row from the 'sale_order_email' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SaleOrderEmail extends BaseSaleOrderEmail
{
    public static function register($iSaleOrderId, MailMessage $oMailMessage, $aAttachments)
    {
        $oSaleOrderEmail = new SaleOrderEmail();
        $oSaleOrderEmail->setSendFrom($oMailMessage->getFrom());
        $oSaleOrderEmail->setSendTo($oMailMessage->getTo());
        $oSaleOrderEmail->setSubject($oMailMessage->getSubject());
        $oSaleOrderEmail->setContent($oMailMessage->getBody());
        $oSaleOrderEmail->setSaleOrderId($iSaleOrderId);

        if(!empty($aAttachments))
        {
            $aAttachmentFilenames = [];
            foreach($aAttachments as $oAttachment)
            {
                if($oAttachment instanceof \Swift_Attachment)
                {
                    $aAttachmentFilenames[] = $oAttachment->getFilename();
                }
            }
            $oSaleOrderEmail->setAttachedFiles(join(", ", $aAttachmentFilenames));
        }
        else
        {
            $oSaleOrderEmail->setAttachedFiles('');
        }

        $oSaleOrderEmail->save();
    }
}
