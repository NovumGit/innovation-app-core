<?php

namespace Model\Sale;

use Core\PropelSqlTrait;
use Model\Sale\Base\SaleOrderItemQuery as BaseSaleOrderItemQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'sale_order_item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SaleOrderItemQuery extends BaseSaleOrderItemQuery
{
	use PropelSqlTrait;
}
