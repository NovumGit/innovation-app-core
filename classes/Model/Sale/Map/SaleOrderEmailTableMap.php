<?php

namespace Model\Sale\Map;

use Model\Sale\SaleOrderEmail;
use Model\Sale\SaleOrderEmailQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'sale_order_email' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class SaleOrderEmailTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Sale.Map.SaleOrderEmailTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'sale_order_email';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Sale\\SaleOrderEmail';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Sale.SaleOrderEmail';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    const COL_ID = 'sale_order_email.id';

    /**
     * the column name for the created_on field
     */
    const COL_CREATED_ON = 'sale_order_email.created_on';

    /**
     * the column name for the sale_order_id field
     */
    const COL_SALE_ORDER_ID = 'sale_order_email.sale_order_id';

    /**
     * the column name for the send_from field
     */
    const COL_SEND_FROM = 'sale_order_email.send_from';

    /**
     * the column name for the send_to field
     */
    const COL_SEND_TO = 'sale_order_email.send_to';

    /**
     * the column name for the subject field
     */
    const COL_SUBJECT = 'sale_order_email.subject';

    /**
     * the column name for the content field
     */
    const COL_CONTENT = 'sale_order_email.content';

    /**
     * the column name for the attached_files field
     */
    const COL_ATTACHED_FILES = 'sale_order_email.attached_files';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'CreatedOn', 'SaleOrderId', 'SendFrom', 'SendTo', 'Subject', 'Content', 'AttachedFiles', ),
        self::TYPE_CAMELNAME     => array('id', 'createdOn', 'saleOrderId', 'sendFrom', 'sendTo', 'subject', 'content', 'attachedFiles', ),
        self::TYPE_COLNAME       => array(SaleOrderEmailTableMap::COL_ID, SaleOrderEmailTableMap::COL_CREATED_ON, SaleOrderEmailTableMap::COL_SALE_ORDER_ID, SaleOrderEmailTableMap::COL_SEND_FROM, SaleOrderEmailTableMap::COL_SEND_TO, SaleOrderEmailTableMap::COL_SUBJECT, SaleOrderEmailTableMap::COL_CONTENT, SaleOrderEmailTableMap::COL_ATTACHED_FILES, ),
        self::TYPE_FIELDNAME     => array('id', 'created_on', 'sale_order_id', 'send_from', 'send_to', 'subject', 'content', 'attached_files', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'CreatedOn' => 1, 'SaleOrderId' => 2, 'SendFrom' => 3, 'SendTo' => 4, 'Subject' => 5, 'Content' => 6, 'AttachedFiles' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'createdOn' => 1, 'saleOrderId' => 2, 'sendFrom' => 3, 'sendTo' => 4, 'subject' => 5, 'content' => 6, 'attachedFiles' => 7, ),
        self::TYPE_COLNAME       => array(SaleOrderEmailTableMap::COL_ID => 0, SaleOrderEmailTableMap::COL_CREATED_ON => 1, SaleOrderEmailTableMap::COL_SALE_ORDER_ID => 2, SaleOrderEmailTableMap::COL_SEND_FROM => 3, SaleOrderEmailTableMap::COL_SEND_TO => 4, SaleOrderEmailTableMap::COL_SUBJECT => 5, SaleOrderEmailTableMap::COL_CONTENT => 6, SaleOrderEmailTableMap::COL_ATTACHED_FILES => 7, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'created_on' => 1, 'sale_order_id' => 2, 'send_from' => 3, 'send_to' => 4, 'subject' => 5, 'content' => 6, 'attached_files' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sale_order_email');
        $this->setPhpName('SaleOrderEmail');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Sale\\SaleOrderEmail');
        $this->setPackage('Model.Sale');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('created_on', 'CreatedOn', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
        $this->addForeignKey('sale_order_id', 'SaleOrderId', 'INTEGER', 'sale_order', 'id', true, null, null);
        $this->addColumn('send_from', 'SendFrom', 'VARCHAR', false, 255, null);
        $this->addColumn('send_to', 'SendTo', 'VARCHAR', false, 255, null);
        $this->addColumn('subject', 'Subject', 'VARCHAR', false, 255, null);
        $this->addColumn('content', 'Content', 'VARCHAR', false, 255, null);
        $this->addColumn('attached_files', 'AttachedFiles', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SaleOrder', '\\Model\\Sale\\SaleOrder', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':sale_order_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SaleOrderEmailTableMap::CLASS_DEFAULT : SaleOrderEmailTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SaleOrderEmail object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SaleOrderEmailTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SaleOrderEmailTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SaleOrderEmailTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SaleOrderEmailTableMap::OM_CLASS;
            /** @var SaleOrderEmail $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SaleOrderEmailTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SaleOrderEmailTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SaleOrderEmailTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SaleOrderEmail $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SaleOrderEmailTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SaleOrderEmailTableMap::COL_ID);
            $criteria->addSelectColumn(SaleOrderEmailTableMap::COL_CREATED_ON);
            $criteria->addSelectColumn(SaleOrderEmailTableMap::COL_SALE_ORDER_ID);
            $criteria->addSelectColumn(SaleOrderEmailTableMap::COL_SEND_FROM);
            $criteria->addSelectColumn(SaleOrderEmailTableMap::COL_SEND_TO);
            $criteria->addSelectColumn(SaleOrderEmailTableMap::COL_SUBJECT);
            $criteria->addSelectColumn(SaleOrderEmailTableMap::COL_CONTENT);
            $criteria->addSelectColumn(SaleOrderEmailTableMap::COL_ATTACHED_FILES);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.created_on');
            $criteria->addSelectColumn($alias . '.sale_order_id');
            $criteria->addSelectColumn($alias . '.send_from');
            $criteria->addSelectColumn($alias . '.send_to');
            $criteria->addSelectColumn($alias . '.subject');
            $criteria->addSelectColumn($alias . '.content');
            $criteria->addSelectColumn($alias . '.attached_files');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SaleOrderEmailTableMap::DATABASE_NAME)->getTable(SaleOrderEmailTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SaleOrderEmailTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SaleOrderEmailTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SaleOrderEmailTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SaleOrderEmail or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SaleOrderEmail object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderEmailTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Sale\SaleOrderEmail) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SaleOrderEmailTableMap::DATABASE_NAME);
            $criteria->add(SaleOrderEmailTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SaleOrderEmailQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SaleOrderEmailTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SaleOrderEmailTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the sale_order_email table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SaleOrderEmailQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SaleOrderEmail or Criteria object.
     *
     * @param mixed               $criteria Criteria or SaleOrderEmail object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderEmailTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SaleOrderEmail object
        }

        if ($criteria->containsKey(SaleOrderEmailTableMap::COL_ID) && $criteria->keyContainsValue(SaleOrderEmailTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SaleOrderEmailTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SaleOrderEmailQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SaleOrderEmailTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SaleOrderEmailTableMap::buildTableMap();
