<?php

namespace Model\Sale\Map;

use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderQuery;
use Model\SaleOrderNotification\Map\SaleOrderNotificationTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'sale_order' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class SaleOrderTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Sale.Map.SaleOrderTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'sale_order';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Sale\\SaleOrder';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Sale.SaleOrder';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 107;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 107;

    /**
     * the column name for the id field
     */
    const COL_ID = 'sale_order.id';

    /**
     * the column name for the site_id field
     */
    const COL_SITE_ID = 'sale_order.site_id';

    /**
     * the column name for the language_id field
     */
    const COL_LANGUAGE_ID = 'sale_order.language_id';

    /**
     * the column name for the is_closed field
     */
    const COL_IS_CLOSED = 'sale_order.is_closed';

    /**
     * the column name for the is_fully_paid field
     */
    const COL_IS_FULLY_PAID = 'sale_order.is_fully_paid';

    /**
     * the column name for the pay_date field
     */
    const COL_PAY_DATE = 'sale_order.pay_date';

    /**
     * the column name for the is_ready_for_shipping field
     */
    const COL_IS_READY_FOR_SHIPPING = 'sale_order.is_ready_for_shipping';

    /**
     * the column name for the is_shipping_checked field
     */
    const COL_IS_SHIPPING_CHECKED = 'sale_order.is_shipping_checked';

    /**
     * the column name for the is_fully_shipped field
     */
    const COL_IS_FULLY_SHIPPED = 'sale_order.is_fully_shipped';

    /**
     * the column name for the is_pushed_to_accounting field
     */
    const COL_IS_PUSHED_TO_ACCOUNTING = 'sale_order.is_pushed_to_accounting';

    /**
     * the column name for the ship_date field
     */
    const COL_SHIP_DATE = 'sale_order.ship_date';

    /**
     * the column name for the sale_order_status_id field
     */
    const COL_SALE_ORDER_STATUS_ID = 'sale_order.sale_order_status_id';

    /**
     * the column name for the handled_by field
     */
    const COL_HANDLED_BY = 'sale_order.handled_by';

    /**
     * the column name for the source field
     */
    const COL_SOURCE = 'sale_order.source';

    /**
     * the column name for the is_completed field
     */
    const COL_IS_COMPLETED = 'sale_order.is_completed';

    /**
     * the column name for the created_by_user_id field
     */
    const COL_CREATED_BY_USER_ID = 'sale_order.created_by_user_id';

    /**
     * the column name for the created_on field
     */
    const COL_CREATED_ON = 'sale_order.created_on';

    /**
     * the column name for the estimated_delivery_date field
     */
    const COL_ESTIMATED_DELIVERY_DATE = 'sale_order.estimated_delivery_date';

    /**
     * the column name for the customer_id field
     */
    const COL_CUSTOMER_ID = 'sale_order.customer_id';

    /**
     * the column name for the envelope_weight_grams field
     */
    const COL_ENVELOPE_WEIGHT_GRAMS = 'sale_order.envelope_weight_grams';

    /**
     * the column name for the webshop_user_session_id field
     */
    const COL_WEBSHOP_USER_SESSION_ID = 'sale_order.webshop_user_session_id';

    /**
     * the column name for the invoice_number field
     */
    const COL_INVOICE_NUMBER = 'sale_order.invoice_number';

    /**
     * the column name for the invoice_date field
     */
    const COL_INVOICE_DATE = 'sale_order.invoice_date';

    /**
     * the column name for the payterm_days field
     */
    const COL_PAYTERM_DAYS = 'sale_order.payterm_days';

    /**
     * the column name for the payterm_original_id field
     */
    const COL_PAYTERM_ORIGINAL_ID = 'sale_order.payterm_original_id';

    /**
     * the column name for the payterm_string field
     */
    const COL_PAYTERM_STRING = 'sale_order.payterm_string';

    /**
     * the column name for the order_number field
     */
    const COL_ORDER_NUMBER = 'sale_order.order_number';

    /**
     * the column name for the is_deleted field
     */
    const COL_IS_DELETED = 'sale_order.is_deleted';

    /**
     * the column name for the company_name field
     */
    const COL_COMPANY_NAME = 'sale_order.company_name';

    /**
     * the column name for the debitor field
     */
    const COL_DEBITOR = 'sale_order.debitor';

    /**
     * the column name for the our_custom_folder field
     */
    const COL_OUR_CUSTOM_FOLDER = 'sale_order.our_custom_folder';

    /**
     * the column name for the our_slogan field
     */
    const COL_OUR_SLOGAN = 'sale_order.our_slogan';

    /**
     * the column name for the our_phone field
     */
    const COL_OUR_PHONE = 'sale_order.our_phone';

    /**
     * the column name for the our_fax field
     */
    const COL_OUR_FAX = 'sale_order.our_fax';

    /**
     * the column name for the our_website field
     */
    const COL_OUR_WEBSITE = 'sale_order.our_website';

    /**
     * the column name for the our_email field
     */
    const COL_OUR_EMAIL = 'sale_order.our_email';

    /**
     * the column name for the our_vat_number field
     */
    const COL_OUR_VAT_NUMBER = 'sale_order.our_vat_number';

    /**
     * the column name for the our_chamber_of_commerce field
     */
    const COL_OUR_CHAMBER_OF_COMMERCE = 'sale_order.our_chamber_of_commerce';

    /**
     * the column name for the our_iban_number field
     */
    const COL_OUR_IBAN_NUMBER = 'sale_order.our_iban_number';

    /**
     * the column name for the our_bic_number field
     */
    const COL_OUR_BIC_NUMBER = 'sale_order.our_bic_number';

    /**
     * the column name for the our_general_company_name field
     */
    const COL_OUR_GENERAL_COMPANY_NAME = 'sale_order.our_general_company_name';

    /**
     * the column name for the our_general_street field
     */
    const COL_OUR_GENERAL_STREET = 'sale_order.our_general_street';

    /**
     * the column name for the our_general_number field
     */
    const COL_OUR_GENERAL_NUMBER = 'sale_order.our_general_number';

    /**
     * the column name for the our_general_number_add field
     */
    const COL_OUR_GENERAL_NUMBER_ADD = 'sale_order.our_general_number_add';

    /**
     * the column name for the our_general_postal field
     */
    const COL_OUR_GENERAL_POSTAL = 'sale_order.our_general_postal';

    /**
     * the column name for the our_general_po_box field
     */
    const COL_OUR_GENERAL_PO_BOX = 'sale_order.our_general_po_box';

    /**
     * the column name for the our_general_city field
     */
    const COL_OUR_GENERAL_CITY = 'sale_order.our_general_city';

    /**
     * the column name for the our_general_country field
     */
    const COL_OUR_GENERAL_COUNTRY = 'sale_order.our_general_country';

    /**
     * the column name for the customer_phone field
     */
    const COL_CUSTOMER_PHONE = 'sale_order.customer_phone';

    /**
     * the column name for the customer_fax field
     */
    const COL_CUSTOMER_FAX = 'sale_order.customer_fax';

    /**
     * the column name for the customer_email field
     */
    const COL_CUSTOMER_EMAIL = 'sale_order.customer_email';

    /**
     * the column name for the customer_chamber_of_commerce field
     */
    const COL_CUSTOMER_CHAMBER_OF_COMMERCE = 'sale_order.customer_chamber_of_commerce';

    /**
     * the column name for the customer_vat_number field
     */
    const COL_CUSTOMER_VAT_NUMBER = 'sale_order.customer_vat_number';

    /**
     * the column name for the customer_order_reference field
     */
    const COL_CUSTOMER_ORDER_REFERENCE = 'sale_order.customer_order_reference';

    /**
     * the column name for the customer_order_placed_by field
     */
    const COL_CUSTOMER_ORDER_PLACED_BY = 'sale_order.customer_order_placed_by';

    /**
     * the column name for the customer_invoice_address_id field
     */
    const COL_CUSTOMER_INVOICE_ADDRESS_ID = 'sale_order.customer_invoice_address_id';

    /**
     * the column name for the customer_invoice_company_name field
     */
    const COL_CUSTOMER_INVOICE_COMPANY_NAME = 'sale_order.customer_invoice_company_name';

    /**
     * the column name for the customer_invoice_attn_name field
     */
    const COL_CUSTOMER_INVOICE_ATTN_NAME = 'sale_order.customer_invoice_attn_name';

    /**
     * the column name for the customer_invoice_street field
     */
    const COL_CUSTOMER_INVOICE_STREET = 'sale_order.customer_invoice_street';

    /**
     * the column name for the customer_invoice_number field
     */
    const COL_CUSTOMER_INVOICE_NUMBER = 'sale_order.customer_invoice_number';

    /**
     * the column name for the customer_invoice_number_add field
     */
    const COL_CUSTOMER_INVOICE_NUMBER_ADD = 'sale_order.customer_invoice_number_add';

    /**
     * the column name for the customer_invoice_postal field
     */
    const COL_CUSTOMER_INVOICE_POSTAL = 'sale_order.customer_invoice_postal';

    /**
     * the column name for the customer_invoice_city field
     */
    const COL_CUSTOMER_INVOICE_CITY = 'sale_order.customer_invoice_city';

    /**
     * the column name for the customer_invoice_country field
     */
    const COL_CUSTOMER_INVOICE_COUNTRY = 'sale_order.customer_invoice_country';

    /**
     * the column name for the customer_invoice_usa_state field
     */
    const COL_CUSTOMER_INVOICE_USA_STATE = 'sale_order.customer_invoice_usa_state';

    /**
     * the column name for the customer_invoice_address_l1 field
     */
    const COL_CUSTOMER_INVOICE_ADDRESS_L1 = 'sale_order.customer_invoice_address_l1';

    /**
     * the column name for the customer_invoice_address_l2 field
     */
    const COL_CUSTOMER_INVOICE_ADDRESS_L2 = 'sale_order.customer_invoice_address_l2';

    /**
     * the column name for the customer_delivery_address_id field
     */
    const COL_CUSTOMER_DELIVERY_ADDRESS_ID = 'sale_order.customer_delivery_address_id';

    /**
     * the column name for the customer_delivery_company_name field
     */
    const COL_CUSTOMER_DELIVERY_COMPANY_NAME = 'sale_order.customer_delivery_company_name';

    /**
     * the column name for the customer_delivery_attn_name field
     */
    const COL_CUSTOMER_DELIVERY_ATTN_NAME = 'sale_order.customer_delivery_attn_name';

    /**
     * the column name for the customer_delivery_street field
     */
    const COL_CUSTOMER_DELIVERY_STREET = 'sale_order.customer_delivery_street';

    /**
     * the column name for the customer_delivery_number field
     */
    const COL_CUSTOMER_DELIVERY_NUMBER = 'sale_order.customer_delivery_number';

    /**
     * the column name for the customer_delivery_number_add field
     */
    const COL_CUSTOMER_DELIVERY_NUMBER_ADD = 'sale_order.customer_delivery_number_add';

    /**
     * the column name for the customer_delivery_postal field
     */
    const COL_CUSTOMER_DELIVERY_POSTAL = 'sale_order.customer_delivery_postal';

    /**
     * the column name for the customer_delivery_city field
     */
    const COL_CUSTOMER_DELIVERY_CITY = 'sale_order.customer_delivery_city';

    /**
     * the column name for the customer_delivery_country field
     */
    const COL_CUSTOMER_DELIVERY_COUNTRY = 'sale_order.customer_delivery_country';

    /**
     * the column name for the customer_delivery_usa_state field
     */
    const COL_CUSTOMER_DELIVERY_USA_STATE = 'sale_order.customer_delivery_usa_state';

    /**
     * the column name for the customer_delivery_address_l1 field
     */
    const COL_CUSTOMER_DELIVERY_ADDRESS_L1 = 'sale_order.customer_delivery_address_l1';

    /**
     * the column name for the customer_delivery_address_l2 field
     */
    const COL_CUSTOMER_DELIVERY_ADDRESS_L2 = 'sale_order.customer_delivery_address_l2';

    /**
     * the column name for the cp_first_name field
     */
    const COL_CP_FIRST_NAME = 'sale_order.cp_first_name';

    /**
     * the column name for the cp_last_name field
     */
    const COL_CP_LAST_NAME = 'sale_order.cp_last_name';

    /**
     * the column name for the cp_sallutation field
     */
    const COL_CP_SALLUTATION = 'sale_order.cp_sallutation';

    /**
     * the column name for the reference field
     */
    const COL_REFERENCE = 'sale_order.reference';

    /**
     * the column name for the shipping_note field
     */
    const COL_SHIPPING_NOTE = 'sale_order.shipping_note';

    /**
     * the column name for the customer_order_note field
     */
    const COL_CUSTOMER_ORDER_NOTE = 'sale_order.customer_order_note';

    /**
     * the column name for the shipping_order_note field
     */
    const COL_SHIPPING_ORDER_NOTE = 'sale_order.shipping_order_note';

    /**
     * the column name for the has_wrap field
     */
    const COL_HAS_WRAP = 'sale_order.has_wrap';

    /**
     * the column name for the wrapping_price field
     */
    const COL_WRAPPING_PRICE = 'sale_order.wrapping_price';

    /**
     * the column name for the shipping_carrier field
     */
    const COL_SHIPPING_CARRIER = 'sale_order.shipping_carrier';

    /**
     * the column name for the shipping_price field
     */
    const COL_SHIPPING_PRICE = 'sale_order.shipping_price';

    /**
     * the column name for the shipping_method_id field
     */
    const COL_SHIPPING_METHOD_ID = 'sale_order.shipping_method_id';

    /**
     * the column name for the shipping_has_track_trace field
     */
    const COL_SHIPPING_HAS_TRACK_TRACE = 'sale_order.shipping_has_track_trace';

    /**
     * the column name for the shipping_id field
     */
    const COL_SHIPPING_ID = 'sale_order.shipping_id';

    /**
     * the column name for the tracking_code field
     */
    const COL_TRACKING_CODE = 'sale_order.tracking_code';

    /**
     * the column name for the tracking_url field
     */
    const COL_TRACKING_URL = 'sale_order.tracking_url';

    /**
     * the column name for the paymethod_name field
     */
    const COL_PAYMETHOD_NAME = 'sale_order.paymethod_name';

    /**
     * the column name for the paymethod_code field
     */
    const COL_PAYMETHOD_CODE = 'sale_order.paymethod_code';

    /**
     * the column name for the paymethod_id field
     */
    const COL_PAYMETHOD_ID = 'sale_order.paymethod_id';

    /**
     * the column name for the shipping_registration_date field
     */
    const COL_SHIPPING_REGISTRATION_DATE = 'sale_order.shipping_registration_date';

    /**
     * the column name for the invoice_by_postal field
     */
    const COL_INVOICE_BY_POSTAL = 'sale_order.invoice_by_postal';

    /**
     * the column name for the packing_slip_type field
     */
    const COL_PACKING_SLIP_TYPE = 'sale_order.packing_slip_type';

    /**
     * the column name for the work_in_progress field
     */
    const COL_WORK_IN_PROGRESS = 'sale_order.work_in_progress';

    /**
     * the column name for the package_amount field
     */
    const COL_PACKAGE_AMOUNT = 'sale_order.package_amount';

    /**
     * the column name for the coupon_code field
     */
    const COL_COUPON_CODE = 'sale_order.coupon_code';

    /**
     * the column name for the coupon_discount_amount field
     */
    const COL_COUPON_DISCOUNT_AMOUNT = 'sale_order.coupon_discount_amount';

    /**
     * the column name for the coupon_discount_percentage field
     */
    const COL_COUPON_DISCOUNT_PERCENTAGE = 'sale_order.coupon_discount_percentage';

    /**
     * the column name for the hide_untill field
     */
    const COL_HIDE_UNTILL = 'sale_order.hide_untill';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'SiteId', 'LanguageId', 'IsClosed', 'IsFullyPaid', 'PayDate', 'IsReadyForShipping', 'IsShippingChecked', 'IsFullyShipped', 'IsPushedToAccounting', 'ShipDate', 'SaleOrderStatusId', 'HandledBy', 'Source', 'IsCompleted', 'CreatedByUserId', 'CreatedOn', 'EstimatedDeliveryDate', 'CustomerId', 'EnvelopeWeightGrams', 'WebshopUserSessionId', 'InvoiceNumber', 'InvoiceDate', 'PaytermDays', 'PaytermOriginalId', 'PaytermString', 'OrderNumber', 'ItemDeleted', 'CompanyName', 'Debitor', 'OurCustomFolder', 'OurSlogan', 'OurPhone', 'OurFax', 'OurWebsite', 'OurEmail', 'OurVatNumber', 'OurChamberOfCommerce', 'OurIbanNumber', 'OurBicNumber', 'OurGeneralCompanyName', 'OurGeneralStreet', 'OurGeneralNumber', 'OurGeneralNumberAdd', 'OurGeneralPostal', 'OurGeneralPoBox', 'OurGeneralCity', 'OurGeneralCountry', 'CustomerPhone', 'CustomerFax', 'CustomerEmail', 'CustomerChamberOfCommerce', 'CustomerVatNumber', 'CustomerOrderReference', 'CustomerOrderPlacedBy', 'CustomerInvoiceAddressId', 'CustomerInvoiceCompanyName', 'CustomerInvoiceAttnName', 'CustomerInvoiceStreet', 'CustomerInvoiceNumber', 'CustomerInvoiceNumberAdd', 'CustomerInvoicePostal', 'CustomerInvoiceCity', 'CustomerInvoiceCountry', 'CustomerInvoiceUsaState', 'CustomerInvoiceAddressL1', 'CustomerInvoiceAddressL2', 'CustomerDeliveryAddressId', 'CustomerDeliveryCompanyName', 'CustomerDeliveryAttnName', 'CustomerDeliveryStreet', 'CustomerDeliveryNumber', 'CustomerDeliveryNumberAdd', 'CustomerDeliveryPostal', 'CustomerDeliveryCity', 'CustomerDeliveryCountry', 'CustomerDeliveryUsaState', 'CustomerDeliveryAddressL1', 'CustomerDeliveryAddressL2', 'CpFirstName', 'CpLastName', 'CpSallutation', 'Reference', 'ShippingNote', 'CustomerOrderNote', 'ShippingOrderNote', 'HasWrap', 'WrappingPrice', 'ShippingCarrier', 'ShippingPrice', 'ShippingMethodId', 'ShippingHasTrackTrace', 'ShippingId', 'TrackingCode', 'TrackingUrl', 'PaymethodName', 'PaymethodCode', 'PaymethodId', 'ShippingRegistrationDate', 'InvoiceByPostal', 'PackingSlipType', 'WorkInProgress', 'PackageAmount', 'CouponCode', 'CouponDiscountAmount', 'CouponDiscountPercentage', 'HideUntill', ),
        self::TYPE_CAMELNAME     => array('id', 'siteId', 'languageId', 'isClosed', 'isFullyPaid', 'payDate', 'isReadyForShipping', 'isShippingChecked', 'isFullyShipped', 'isPushedToAccounting', 'shipDate', 'saleOrderStatusId', 'handledBy', 'source', 'isCompleted', 'createdByUserId', 'createdOn', 'estimatedDeliveryDate', 'customerId', 'envelopeWeightGrams', 'webshopUserSessionId', 'invoiceNumber', 'invoiceDate', 'paytermDays', 'paytermOriginalId', 'paytermString', 'orderNumber', 'itemDeleted', 'companyName', 'debitor', 'ourCustomFolder', 'ourSlogan', 'ourPhone', 'ourFax', 'ourWebsite', 'ourEmail', 'ourVatNumber', 'ourChamberOfCommerce', 'ourIbanNumber', 'ourBicNumber', 'ourGeneralCompanyName', 'ourGeneralStreet', 'ourGeneralNumber', 'ourGeneralNumberAdd', 'ourGeneralPostal', 'ourGeneralPoBox', 'ourGeneralCity', 'ourGeneralCountry', 'customerPhone', 'customerFax', 'customerEmail', 'customerChamberOfCommerce', 'customerVatNumber', 'customerOrderReference', 'customerOrderPlacedBy', 'customerInvoiceAddressId', 'customerInvoiceCompanyName', 'customerInvoiceAttnName', 'customerInvoiceStreet', 'customerInvoiceNumber', 'customerInvoiceNumberAdd', 'customerInvoicePostal', 'customerInvoiceCity', 'customerInvoiceCountry', 'customerInvoiceUsaState', 'customerInvoiceAddressL1', 'customerInvoiceAddressL2', 'customerDeliveryAddressId', 'customerDeliveryCompanyName', 'customerDeliveryAttnName', 'customerDeliveryStreet', 'customerDeliveryNumber', 'customerDeliveryNumberAdd', 'customerDeliveryPostal', 'customerDeliveryCity', 'customerDeliveryCountry', 'customerDeliveryUsaState', 'customerDeliveryAddressL1', 'customerDeliveryAddressL2', 'cpFirstName', 'cpLastName', 'cpSallutation', 'reference', 'shippingNote', 'customerOrderNote', 'shippingOrderNote', 'hasWrap', 'wrappingPrice', 'shippingCarrier', 'shippingPrice', 'shippingMethodId', 'shippingHasTrackTrace', 'shippingId', 'trackingCode', 'trackingUrl', 'paymethodName', 'paymethodCode', 'paymethodId', 'shippingRegistrationDate', 'invoiceByPostal', 'packingSlipType', 'workInProgress', 'packageAmount', 'couponCode', 'couponDiscountAmount', 'couponDiscountPercentage', 'hideUntill', ),
        self::TYPE_COLNAME       => array(SaleOrderTableMap::COL_ID, SaleOrderTableMap::COL_SITE_ID, SaleOrderTableMap::COL_LANGUAGE_ID, SaleOrderTableMap::COL_IS_CLOSED, SaleOrderTableMap::COL_IS_FULLY_PAID, SaleOrderTableMap::COL_PAY_DATE, SaleOrderTableMap::COL_IS_READY_FOR_SHIPPING, SaleOrderTableMap::COL_IS_SHIPPING_CHECKED, SaleOrderTableMap::COL_IS_FULLY_SHIPPED, SaleOrderTableMap::COL_IS_PUSHED_TO_ACCOUNTING, SaleOrderTableMap::COL_SHIP_DATE, SaleOrderTableMap::COL_SALE_ORDER_STATUS_ID, SaleOrderTableMap::COL_HANDLED_BY, SaleOrderTableMap::COL_SOURCE, SaleOrderTableMap::COL_IS_COMPLETED, SaleOrderTableMap::COL_CREATED_BY_USER_ID, SaleOrderTableMap::COL_CREATED_ON, SaleOrderTableMap::COL_ESTIMATED_DELIVERY_DATE, SaleOrderTableMap::COL_CUSTOMER_ID, SaleOrderTableMap::COL_ENVELOPE_WEIGHT_GRAMS, SaleOrderTableMap::COL_WEBSHOP_USER_SESSION_ID, SaleOrderTableMap::COL_INVOICE_NUMBER, SaleOrderTableMap::COL_INVOICE_DATE, SaleOrderTableMap::COL_PAYTERM_DAYS, SaleOrderTableMap::COL_PAYTERM_ORIGINAL_ID, SaleOrderTableMap::COL_PAYTERM_STRING, SaleOrderTableMap::COL_ORDER_NUMBER, SaleOrderTableMap::COL_IS_DELETED, SaleOrderTableMap::COL_COMPANY_NAME, SaleOrderTableMap::COL_DEBITOR, SaleOrderTableMap::COL_OUR_CUSTOM_FOLDER, SaleOrderTableMap::COL_OUR_SLOGAN, SaleOrderTableMap::COL_OUR_PHONE, SaleOrderTableMap::COL_OUR_FAX, SaleOrderTableMap::COL_OUR_WEBSITE, SaleOrderTableMap::COL_OUR_EMAIL, SaleOrderTableMap::COL_OUR_VAT_NUMBER, SaleOrderTableMap::COL_OUR_CHAMBER_OF_COMMERCE, SaleOrderTableMap::COL_OUR_IBAN_NUMBER, SaleOrderTableMap::COL_OUR_BIC_NUMBER, SaleOrderTableMap::COL_OUR_GENERAL_COMPANY_NAME, SaleOrderTableMap::COL_OUR_GENERAL_STREET, SaleOrderTableMap::COL_OUR_GENERAL_NUMBER, SaleOrderTableMap::COL_OUR_GENERAL_NUMBER_ADD, SaleOrderTableMap::COL_OUR_GENERAL_POSTAL, SaleOrderTableMap::COL_OUR_GENERAL_PO_BOX, SaleOrderTableMap::COL_OUR_GENERAL_CITY, SaleOrderTableMap::COL_OUR_GENERAL_COUNTRY, SaleOrderTableMap::COL_CUSTOMER_PHONE, SaleOrderTableMap::COL_CUSTOMER_FAX, SaleOrderTableMap::COL_CUSTOMER_EMAIL, SaleOrderTableMap::COL_CUSTOMER_CHAMBER_OF_COMMERCE, SaleOrderTableMap::COL_CUSTOMER_VAT_NUMBER, SaleOrderTableMap::COL_CUSTOMER_ORDER_REFERENCE, SaleOrderTableMap::COL_CUSTOMER_ORDER_PLACED_BY, SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_ID, SaleOrderTableMap::COL_CUSTOMER_INVOICE_COMPANY_NAME, SaleOrderTableMap::COL_CUSTOMER_INVOICE_ATTN_NAME, SaleOrderTableMap::COL_CUSTOMER_INVOICE_STREET, SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER, SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER_ADD, SaleOrderTableMap::COL_CUSTOMER_INVOICE_POSTAL, SaleOrderTableMap::COL_CUSTOMER_INVOICE_CITY, SaleOrderTableMap::COL_CUSTOMER_INVOICE_COUNTRY, SaleOrderTableMap::COL_CUSTOMER_INVOICE_USA_STATE, SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L1, SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L2, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_ID, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COMPANY_NAME, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ATTN_NAME, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_STREET, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER_ADD, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_POSTAL, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_CITY, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COUNTRY, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_USA_STATE, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L1, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L2, SaleOrderTableMap::COL_CP_FIRST_NAME, SaleOrderTableMap::COL_CP_LAST_NAME, SaleOrderTableMap::COL_CP_SALLUTATION, SaleOrderTableMap::COL_REFERENCE, SaleOrderTableMap::COL_SHIPPING_NOTE, SaleOrderTableMap::COL_CUSTOMER_ORDER_NOTE, SaleOrderTableMap::COL_SHIPPING_ORDER_NOTE, SaleOrderTableMap::COL_HAS_WRAP, SaleOrderTableMap::COL_WRAPPING_PRICE, SaleOrderTableMap::COL_SHIPPING_CARRIER, SaleOrderTableMap::COL_SHIPPING_PRICE, SaleOrderTableMap::COL_SHIPPING_METHOD_ID, SaleOrderTableMap::COL_SHIPPING_HAS_TRACK_TRACE, SaleOrderTableMap::COL_SHIPPING_ID, SaleOrderTableMap::COL_TRACKING_CODE, SaleOrderTableMap::COL_TRACKING_URL, SaleOrderTableMap::COL_PAYMETHOD_NAME, SaleOrderTableMap::COL_PAYMETHOD_CODE, SaleOrderTableMap::COL_PAYMETHOD_ID, SaleOrderTableMap::COL_SHIPPING_REGISTRATION_DATE, SaleOrderTableMap::COL_INVOICE_BY_POSTAL, SaleOrderTableMap::COL_PACKING_SLIP_TYPE, SaleOrderTableMap::COL_WORK_IN_PROGRESS, SaleOrderTableMap::COL_PACKAGE_AMOUNT, SaleOrderTableMap::COL_COUPON_CODE, SaleOrderTableMap::COL_COUPON_DISCOUNT_AMOUNT, SaleOrderTableMap::COL_COUPON_DISCOUNT_PERCENTAGE, SaleOrderTableMap::COL_HIDE_UNTILL, ),
        self::TYPE_FIELDNAME     => array('id', 'site_id', 'language_id', 'is_closed', 'is_fully_paid', 'pay_date', 'is_ready_for_shipping', 'is_shipping_checked', 'is_fully_shipped', 'is_pushed_to_accounting', 'ship_date', 'sale_order_status_id', 'handled_by', 'source', 'is_completed', 'created_by_user_id', 'created_on', 'estimated_delivery_date', 'customer_id', 'envelope_weight_grams', 'webshop_user_session_id', 'invoice_number', 'invoice_date', 'payterm_days', 'payterm_original_id', 'payterm_string', 'order_number', 'is_deleted', 'company_name', 'debitor', 'our_custom_folder', 'our_slogan', 'our_phone', 'our_fax', 'our_website', 'our_email', 'our_vat_number', 'our_chamber_of_commerce', 'our_iban_number', 'our_bic_number', 'our_general_company_name', 'our_general_street', 'our_general_number', 'our_general_number_add', 'our_general_postal', 'our_general_po_box', 'our_general_city', 'our_general_country', 'customer_phone', 'customer_fax', 'customer_email', 'customer_chamber_of_commerce', 'customer_vat_number', 'customer_order_reference', 'customer_order_placed_by', 'customer_invoice_address_id', 'customer_invoice_company_name', 'customer_invoice_attn_name', 'customer_invoice_street', 'customer_invoice_number', 'customer_invoice_number_add', 'customer_invoice_postal', 'customer_invoice_city', 'customer_invoice_country', 'customer_invoice_usa_state', 'customer_invoice_address_l1', 'customer_invoice_address_l2', 'customer_delivery_address_id', 'customer_delivery_company_name', 'customer_delivery_attn_name', 'customer_delivery_street', 'customer_delivery_number', 'customer_delivery_number_add', 'customer_delivery_postal', 'customer_delivery_city', 'customer_delivery_country', 'customer_delivery_usa_state', 'customer_delivery_address_l1', 'customer_delivery_address_l2', 'cp_first_name', 'cp_last_name', 'cp_sallutation', 'reference', 'shipping_note', 'customer_order_note', 'shipping_order_note', 'has_wrap', 'wrapping_price', 'shipping_carrier', 'shipping_price', 'shipping_method_id', 'shipping_has_track_trace', 'shipping_id', 'tracking_code', 'tracking_url', 'paymethod_name', 'paymethod_code', 'paymethod_id', 'shipping_registration_date', 'invoice_by_postal', 'packing_slip_type', 'work_in_progress', 'package_amount', 'coupon_code', 'coupon_discount_amount', 'coupon_discount_percentage', 'hide_untill', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'SiteId' => 1, 'LanguageId' => 2, 'IsClosed' => 3, 'IsFullyPaid' => 4, 'PayDate' => 5, 'IsReadyForShipping' => 6, 'IsShippingChecked' => 7, 'IsFullyShipped' => 8, 'IsPushedToAccounting' => 9, 'ShipDate' => 10, 'SaleOrderStatusId' => 11, 'HandledBy' => 12, 'Source' => 13, 'IsCompleted' => 14, 'CreatedByUserId' => 15, 'CreatedOn' => 16, 'EstimatedDeliveryDate' => 17, 'CustomerId' => 18, 'EnvelopeWeightGrams' => 19, 'WebshopUserSessionId' => 20, 'InvoiceNumber' => 21, 'InvoiceDate' => 22, 'PaytermDays' => 23, 'PaytermOriginalId' => 24, 'PaytermString' => 25, 'OrderNumber' => 26, 'ItemDeleted' => 27, 'CompanyName' => 28, 'Debitor' => 29, 'OurCustomFolder' => 30, 'OurSlogan' => 31, 'OurPhone' => 32, 'OurFax' => 33, 'OurWebsite' => 34, 'OurEmail' => 35, 'OurVatNumber' => 36, 'OurChamberOfCommerce' => 37, 'OurIbanNumber' => 38, 'OurBicNumber' => 39, 'OurGeneralCompanyName' => 40, 'OurGeneralStreet' => 41, 'OurGeneralNumber' => 42, 'OurGeneralNumberAdd' => 43, 'OurGeneralPostal' => 44, 'OurGeneralPoBox' => 45, 'OurGeneralCity' => 46, 'OurGeneralCountry' => 47, 'CustomerPhone' => 48, 'CustomerFax' => 49, 'CustomerEmail' => 50, 'CustomerChamberOfCommerce' => 51, 'CustomerVatNumber' => 52, 'CustomerOrderReference' => 53, 'CustomerOrderPlacedBy' => 54, 'CustomerInvoiceAddressId' => 55, 'CustomerInvoiceCompanyName' => 56, 'CustomerInvoiceAttnName' => 57, 'CustomerInvoiceStreet' => 58, 'CustomerInvoiceNumber' => 59, 'CustomerInvoiceNumberAdd' => 60, 'CustomerInvoicePostal' => 61, 'CustomerInvoiceCity' => 62, 'CustomerInvoiceCountry' => 63, 'CustomerInvoiceUsaState' => 64, 'CustomerInvoiceAddressL1' => 65, 'CustomerInvoiceAddressL2' => 66, 'CustomerDeliveryAddressId' => 67, 'CustomerDeliveryCompanyName' => 68, 'CustomerDeliveryAttnName' => 69, 'CustomerDeliveryStreet' => 70, 'CustomerDeliveryNumber' => 71, 'CustomerDeliveryNumberAdd' => 72, 'CustomerDeliveryPostal' => 73, 'CustomerDeliveryCity' => 74, 'CustomerDeliveryCountry' => 75, 'CustomerDeliveryUsaState' => 76, 'CustomerDeliveryAddressL1' => 77, 'CustomerDeliveryAddressL2' => 78, 'CpFirstName' => 79, 'CpLastName' => 80, 'CpSallutation' => 81, 'Reference' => 82, 'ShippingNote' => 83, 'CustomerOrderNote' => 84, 'ShippingOrderNote' => 85, 'HasWrap' => 86, 'WrappingPrice' => 87, 'ShippingCarrier' => 88, 'ShippingPrice' => 89, 'ShippingMethodId' => 90, 'ShippingHasTrackTrace' => 91, 'ShippingId' => 92, 'TrackingCode' => 93, 'TrackingUrl' => 94, 'PaymethodName' => 95, 'PaymethodCode' => 96, 'PaymethodId' => 97, 'ShippingRegistrationDate' => 98, 'InvoiceByPostal' => 99, 'PackingSlipType' => 100, 'WorkInProgress' => 101, 'PackageAmount' => 102, 'CouponCode' => 103, 'CouponDiscountAmount' => 104, 'CouponDiscountPercentage' => 105, 'HideUntill' => 106, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'siteId' => 1, 'languageId' => 2, 'isClosed' => 3, 'isFullyPaid' => 4, 'payDate' => 5, 'isReadyForShipping' => 6, 'isShippingChecked' => 7, 'isFullyShipped' => 8, 'isPushedToAccounting' => 9, 'shipDate' => 10, 'saleOrderStatusId' => 11, 'handledBy' => 12, 'source' => 13, 'isCompleted' => 14, 'createdByUserId' => 15, 'createdOn' => 16, 'estimatedDeliveryDate' => 17, 'customerId' => 18, 'envelopeWeightGrams' => 19, 'webshopUserSessionId' => 20, 'invoiceNumber' => 21, 'invoiceDate' => 22, 'paytermDays' => 23, 'paytermOriginalId' => 24, 'paytermString' => 25, 'orderNumber' => 26, 'itemDeleted' => 27, 'companyName' => 28, 'debitor' => 29, 'ourCustomFolder' => 30, 'ourSlogan' => 31, 'ourPhone' => 32, 'ourFax' => 33, 'ourWebsite' => 34, 'ourEmail' => 35, 'ourVatNumber' => 36, 'ourChamberOfCommerce' => 37, 'ourIbanNumber' => 38, 'ourBicNumber' => 39, 'ourGeneralCompanyName' => 40, 'ourGeneralStreet' => 41, 'ourGeneralNumber' => 42, 'ourGeneralNumberAdd' => 43, 'ourGeneralPostal' => 44, 'ourGeneralPoBox' => 45, 'ourGeneralCity' => 46, 'ourGeneralCountry' => 47, 'customerPhone' => 48, 'customerFax' => 49, 'customerEmail' => 50, 'customerChamberOfCommerce' => 51, 'customerVatNumber' => 52, 'customerOrderReference' => 53, 'customerOrderPlacedBy' => 54, 'customerInvoiceAddressId' => 55, 'customerInvoiceCompanyName' => 56, 'customerInvoiceAttnName' => 57, 'customerInvoiceStreet' => 58, 'customerInvoiceNumber' => 59, 'customerInvoiceNumberAdd' => 60, 'customerInvoicePostal' => 61, 'customerInvoiceCity' => 62, 'customerInvoiceCountry' => 63, 'customerInvoiceUsaState' => 64, 'customerInvoiceAddressL1' => 65, 'customerInvoiceAddressL2' => 66, 'customerDeliveryAddressId' => 67, 'customerDeliveryCompanyName' => 68, 'customerDeliveryAttnName' => 69, 'customerDeliveryStreet' => 70, 'customerDeliveryNumber' => 71, 'customerDeliveryNumberAdd' => 72, 'customerDeliveryPostal' => 73, 'customerDeliveryCity' => 74, 'customerDeliveryCountry' => 75, 'customerDeliveryUsaState' => 76, 'customerDeliveryAddressL1' => 77, 'customerDeliveryAddressL2' => 78, 'cpFirstName' => 79, 'cpLastName' => 80, 'cpSallutation' => 81, 'reference' => 82, 'shippingNote' => 83, 'customerOrderNote' => 84, 'shippingOrderNote' => 85, 'hasWrap' => 86, 'wrappingPrice' => 87, 'shippingCarrier' => 88, 'shippingPrice' => 89, 'shippingMethodId' => 90, 'shippingHasTrackTrace' => 91, 'shippingId' => 92, 'trackingCode' => 93, 'trackingUrl' => 94, 'paymethodName' => 95, 'paymethodCode' => 96, 'paymethodId' => 97, 'shippingRegistrationDate' => 98, 'invoiceByPostal' => 99, 'packingSlipType' => 100, 'workInProgress' => 101, 'packageAmount' => 102, 'couponCode' => 103, 'couponDiscountAmount' => 104, 'couponDiscountPercentage' => 105, 'hideUntill' => 106, ),
        self::TYPE_COLNAME       => array(SaleOrderTableMap::COL_ID => 0, SaleOrderTableMap::COL_SITE_ID => 1, SaleOrderTableMap::COL_LANGUAGE_ID => 2, SaleOrderTableMap::COL_IS_CLOSED => 3, SaleOrderTableMap::COL_IS_FULLY_PAID => 4, SaleOrderTableMap::COL_PAY_DATE => 5, SaleOrderTableMap::COL_IS_READY_FOR_SHIPPING => 6, SaleOrderTableMap::COL_IS_SHIPPING_CHECKED => 7, SaleOrderTableMap::COL_IS_FULLY_SHIPPED => 8, SaleOrderTableMap::COL_IS_PUSHED_TO_ACCOUNTING => 9, SaleOrderTableMap::COL_SHIP_DATE => 10, SaleOrderTableMap::COL_SALE_ORDER_STATUS_ID => 11, SaleOrderTableMap::COL_HANDLED_BY => 12, SaleOrderTableMap::COL_SOURCE => 13, SaleOrderTableMap::COL_IS_COMPLETED => 14, SaleOrderTableMap::COL_CREATED_BY_USER_ID => 15, SaleOrderTableMap::COL_CREATED_ON => 16, SaleOrderTableMap::COL_ESTIMATED_DELIVERY_DATE => 17, SaleOrderTableMap::COL_CUSTOMER_ID => 18, SaleOrderTableMap::COL_ENVELOPE_WEIGHT_GRAMS => 19, SaleOrderTableMap::COL_WEBSHOP_USER_SESSION_ID => 20, SaleOrderTableMap::COL_INVOICE_NUMBER => 21, SaleOrderTableMap::COL_INVOICE_DATE => 22, SaleOrderTableMap::COL_PAYTERM_DAYS => 23, SaleOrderTableMap::COL_PAYTERM_ORIGINAL_ID => 24, SaleOrderTableMap::COL_PAYTERM_STRING => 25, SaleOrderTableMap::COL_ORDER_NUMBER => 26, SaleOrderTableMap::COL_IS_DELETED => 27, SaleOrderTableMap::COL_COMPANY_NAME => 28, SaleOrderTableMap::COL_DEBITOR => 29, SaleOrderTableMap::COL_OUR_CUSTOM_FOLDER => 30, SaleOrderTableMap::COL_OUR_SLOGAN => 31, SaleOrderTableMap::COL_OUR_PHONE => 32, SaleOrderTableMap::COL_OUR_FAX => 33, SaleOrderTableMap::COL_OUR_WEBSITE => 34, SaleOrderTableMap::COL_OUR_EMAIL => 35, SaleOrderTableMap::COL_OUR_VAT_NUMBER => 36, SaleOrderTableMap::COL_OUR_CHAMBER_OF_COMMERCE => 37, SaleOrderTableMap::COL_OUR_IBAN_NUMBER => 38, SaleOrderTableMap::COL_OUR_BIC_NUMBER => 39, SaleOrderTableMap::COL_OUR_GENERAL_COMPANY_NAME => 40, SaleOrderTableMap::COL_OUR_GENERAL_STREET => 41, SaleOrderTableMap::COL_OUR_GENERAL_NUMBER => 42, SaleOrderTableMap::COL_OUR_GENERAL_NUMBER_ADD => 43, SaleOrderTableMap::COL_OUR_GENERAL_POSTAL => 44, SaleOrderTableMap::COL_OUR_GENERAL_PO_BOX => 45, SaleOrderTableMap::COL_OUR_GENERAL_CITY => 46, SaleOrderTableMap::COL_OUR_GENERAL_COUNTRY => 47, SaleOrderTableMap::COL_CUSTOMER_PHONE => 48, SaleOrderTableMap::COL_CUSTOMER_FAX => 49, SaleOrderTableMap::COL_CUSTOMER_EMAIL => 50, SaleOrderTableMap::COL_CUSTOMER_CHAMBER_OF_COMMERCE => 51, SaleOrderTableMap::COL_CUSTOMER_VAT_NUMBER => 52, SaleOrderTableMap::COL_CUSTOMER_ORDER_REFERENCE => 53, SaleOrderTableMap::COL_CUSTOMER_ORDER_PLACED_BY => 54, SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_ID => 55, SaleOrderTableMap::COL_CUSTOMER_INVOICE_COMPANY_NAME => 56, SaleOrderTableMap::COL_CUSTOMER_INVOICE_ATTN_NAME => 57, SaleOrderTableMap::COL_CUSTOMER_INVOICE_STREET => 58, SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER => 59, SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER_ADD => 60, SaleOrderTableMap::COL_CUSTOMER_INVOICE_POSTAL => 61, SaleOrderTableMap::COL_CUSTOMER_INVOICE_CITY => 62, SaleOrderTableMap::COL_CUSTOMER_INVOICE_COUNTRY => 63, SaleOrderTableMap::COL_CUSTOMER_INVOICE_USA_STATE => 64, SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L1 => 65, SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L2 => 66, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_ID => 67, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COMPANY_NAME => 68, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ATTN_NAME => 69, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_STREET => 70, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER => 71, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER_ADD => 72, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_POSTAL => 73, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_CITY => 74, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COUNTRY => 75, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_USA_STATE => 76, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L1 => 77, SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L2 => 78, SaleOrderTableMap::COL_CP_FIRST_NAME => 79, SaleOrderTableMap::COL_CP_LAST_NAME => 80, SaleOrderTableMap::COL_CP_SALLUTATION => 81, SaleOrderTableMap::COL_REFERENCE => 82, SaleOrderTableMap::COL_SHIPPING_NOTE => 83, SaleOrderTableMap::COL_CUSTOMER_ORDER_NOTE => 84, SaleOrderTableMap::COL_SHIPPING_ORDER_NOTE => 85, SaleOrderTableMap::COL_HAS_WRAP => 86, SaleOrderTableMap::COL_WRAPPING_PRICE => 87, SaleOrderTableMap::COL_SHIPPING_CARRIER => 88, SaleOrderTableMap::COL_SHIPPING_PRICE => 89, SaleOrderTableMap::COL_SHIPPING_METHOD_ID => 90, SaleOrderTableMap::COL_SHIPPING_HAS_TRACK_TRACE => 91, SaleOrderTableMap::COL_SHIPPING_ID => 92, SaleOrderTableMap::COL_TRACKING_CODE => 93, SaleOrderTableMap::COL_TRACKING_URL => 94, SaleOrderTableMap::COL_PAYMETHOD_NAME => 95, SaleOrderTableMap::COL_PAYMETHOD_CODE => 96, SaleOrderTableMap::COL_PAYMETHOD_ID => 97, SaleOrderTableMap::COL_SHIPPING_REGISTRATION_DATE => 98, SaleOrderTableMap::COL_INVOICE_BY_POSTAL => 99, SaleOrderTableMap::COL_PACKING_SLIP_TYPE => 100, SaleOrderTableMap::COL_WORK_IN_PROGRESS => 101, SaleOrderTableMap::COL_PACKAGE_AMOUNT => 102, SaleOrderTableMap::COL_COUPON_CODE => 103, SaleOrderTableMap::COL_COUPON_DISCOUNT_AMOUNT => 104, SaleOrderTableMap::COL_COUPON_DISCOUNT_PERCENTAGE => 105, SaleOrderTableMap::COL_HIDE_UNTILL => 106, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'site_id' => 1, 'language_id' => 2, 'is_closed' => 3, 'is_fully_paid' => 4, 'pay_date' => 5, 'is_ready_for_shipping' => 6, 'is_shipping_checked' => 7, 'is_fully_shipped' => 8, 'is_pushed_to_accounting' => 9, 'ship_date' => 10, 'sale_order_status_id' => 11, 'handled_by' => 12, 'source' => 13, 'is_completed' => 14, 'created_by_user_id' => 15, 'created_on' => 16, 'estimated_delivery_date' => 17, 'customer_id' => 18, 'envelope_weight_grams' => 19, 'webshop_user_session_id' => 20, 'invoice_number' => 21, 'invoice_date' => 22, 'payterm_days' => 23, 'payterm_original_id' => 24, 'payterm_string' => 25, 'order_number' => 26, 'is_deleted' => 27, 'company_name' => 28, 'debitor' => 29, 'our_custom_folder' => 30, 'our_slogan' => 31, 'our_phone' => 32, 'our_fax' => 33, 'our_website' => 34, 'our_email' => 35, 'our_vat_number' => 36, 'our_chamber_of_commerce' => 37, 'our_iban_number' => 38, 'our_bic_number' => 39, 'our_general_company_name' => 40, 'our_general_street' => 41, 'our_general_number' => 42, 'our_general_number_add' => 43, 'our_general_postal' => 44, 'our_general_po_box' => 45, 'our_general_city' => 46, 'our_general_country' => 47, 'customer_phone' => 48, 'customer_fax' => 49, 'customer_email' => 50, 'customer_chamber_of_commerce' => 51, 'customer_vat_number' => 52, 'customer_order_reference' => 53, 'customer_order_placed_by' => 54, 'customer_invoice_address_id' => 55, 'customer_invoice_company_name' => 56, 'customer_invoice_attn_name' => 57, 'customer_invoice_street' => 58, 'customer_invoice_number' => 59, 'customer_invoice_number_add' => 60, 'customer_invoice_postal' => 61, 'customer_invoice_city' => 62, 'customer_invoice_country' => 63, 'customer_invoice_usa_state' => 64, 'customer_invoice_address_l1' => 65, 'customer_invoice_address_l2' => 66, 'customer_delivery_address_id' => 67, 'customer_delivery_company_name' => 68, 'customer_delivery_attn_name' => 69, 'customer_delivery_street' => 70, 'customer_delivery_number' => 71, 'customer_delivery_number_add' => 72, 'customer_delivery_postal' => 73, 'customer_delivery_city' => 74, 'customer_delivery_country' => 75, 'customer_delivery_usa_state' => 76, 'customer_delivery_address_l1' => 77, 'customer_delivery_address_l2' => 78, 'cp_first_name' => 79, 'cp_last_name' => 80, 'cp_sallutation' => 81, 'reference' => 82, 'shipping_note' => 83, 'customer_order_note' => 84, 'shipping_order_note' => 85, 'has_wrap' => 86, 'wrapping_price' => 87, 'shipping_carrier' => 88, 'shipping_price' => 89, 'shipping_method_id' => 90, 'shipping_has_track_trace' => 91, 'shipping_id' => 92, 'tracking_code' => 93, 'tracking_url' => 94, 'paymethod_name' => 95, 'paymethod_code' => 96, 'paymethod_id' => 97, 'shipping_registration_date' => 98, 'invoice_by_postal' => 99, 'packing_slip_type' => 100, 'work_in_progress' => 101, 'package_amount' => 102, 'coupon_code' => 103, 'coupon_discount_amount' => 104, 'coupon_discount_percentage' => 105, 'hide_untill' => 106, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sale_order');
        $this->setPhpName('SaleOrder');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Sale\\SaleOrder');
        $this->setPackage('Model.Sale');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('site_id', 'SiteId', 'INTEGER', 'site', 'id', false, null, null);
        $this->addColumn('language_id', 'LanguageId', 'INTEGER', false, null, null);
        $this->addColumn('is_closed', 'IsClosed', 'BOOLEAN', false, 1, false);
        $this->addColumn('is_fully_paid', 'IsFullyPaid', 'BOOLEAN', false, 1, false);
        $this->addColumn('pay_date', 'PayDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('is_ready_for_shipping', 'IsReadyForShipping', 'BOOLEAN', false, 1, false);
        $this->addColumn('is_shipping_checked', 'IsShippingChecked', 'BOOLEAN', false, 1, false);
        $this->addColumn('is_fully_shipped', 'IsFullyShipped', 'BOOLEAN', false, 1, false);
        $this->addColumn('is_pushed_to_accounting', 'IsPushedToAccounting', 'BOOLEAN', false, 1, false);
        $this->addColumn('ship_date', 'ShipDate', 'TIMESTAMP', false, null, null);
        $this->addForeignKey('sale_order_status_id', 'SaleOrderStatusId', 'INTEGER', 'mt_sale_order_status', 'id', false, null, false);
        $this->addColumn('handled_by', 'HandledBy', 'VARCHAR', false, 255, null);
        $this->addColumn('source', 'Source', 'VARCHAR', false, 255, 'intern');
        $this->addColumn('is_completed', 'IsCompleted', 'BOOLEAN', false, 1, false);
        $this->addForeignKey('created_by_user_id', 'CreatedByUserId', 'INTEGER', 'user', 'id', false, null, null);
        $this->addColumn('created_on', 'CreatedOn', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('estimated_delivery_date', 'EstimatedDeliveryDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('customer_id', 'CustomerId', 'INTEGER', false, null, null);
        $this->addColumn('envelope_weight_grams', 'EnvelopeWeightGrams', 'INTEGER', false, null, null);
        $this->addColumn('webshop_user_session_id', 'WebshopUserSessionId', 'INTEGER', false, null, null);
        $this->addColumn('invoice_number', 'InvoiceNumber', 'VARCHAR', false, 255, null);
        $this->addColumn('invoice_date', 'InvoiceDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('payterm_days', 'PaytermDays', 'INTEGER', false, null, null);
        $this->addColumn('payterm_original_id', 'PaytermOriginalId', 'INTEGER', false, null, null);
        $this->addColumn('payterm_string', 'PaytermString', 'VARCHAR', false, 255, null);
        $this->addColumn('order_number', 'OrderNumber', 'VARCHAR', false, 255, 'false');
        $this->addColumn('is_deleted', 'ItemDeleted', 'BOOLEAN', false, 1, false);
        $this->addColumn('company_name', 'CompanyName', 'VARCHAR', false, 120, null);
        $this->addColumn('debitor', 'Debitor', 'VARCHAR', false, 120, null);
        $this->addColumn('our_custom_folder', 'OurCustomFolder', 'VARCHAR', false, 255, null);
        $this->addColumn('our_slogan', 'OurSlogan', 'VARCHAR', false, 255, null);
        $this->addColumn('our_phone', 'OurPhone', 'VARCHAR', false, 255, null);
        $this->addColumn('our_fax', 'OurFax', 'VARCHAR', false, 255, null);
        $this->addColumn('our_website', 'OurWebsite', 'VARCHAR', false, 255, null);
        $this->addColumn('our_email', 'OurEmail', 'VARCHAR', false, 150, null);
        $this->addColumn('our_vat_number', 'OurVatNumber', 'VARCHAR', false, 150, null);
        $this->addColumn('our_chamber_of_commerce', 'OurChamberOfCommerce', 'VARCHAR', false, 120, null);
        $this->addColumn('our_iban_number', 'OurIbanNumber', 'VARCHAR', false, 120, null);
        $this->addColumn('our_bic_number', 'OurBicNumber', 'VARCHAR', false, 120, null);
        $this->addColumn('our_general_company_name', 'OurGeneralCompanyName', 'VARCHAR', false, 255, null);
        $this->addColumn('our_general_street', 'OurGeneralStreet', 'VARCHAR', false, 255, null);
        $this->addColumn('our_general_number', 'OurGeneralNumber', 'VARCHAR', false, 255, null);
        $this->addColumn('our_general_number_add', 'OurGeneralNumberAdd', 'VARCHAR', false, 255, null);
        $this->addColumn('our_general_postal', 'OurGeneralPostal', 'VARCHAR', false, 255, null);
        $this->addColumn('our_general_po_box', 'OurGeneralPoBox', 'VARCHAR', false, 255, null);
        $this->addColumn('our_general_city', 'OurGeneralCity', 'VARCHAR', false, 255, null);
        $this->addColumn('our_general_country', 'OurGeneralCountry', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_phone', 'CustomerPhone', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_fax', 'CustomerFax', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_email', 'CustomerEmail', 'VARCHAR', false, 150, null);
        $this->addColumn('customer_chamber_of_commerce', 'CustomerChamberOfCommerce', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_vat_number', 'CustomerVatNumber', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_order_reference', 'CustomerOrderReference', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_order_placed_by', 'CustomerOrderPlacedBy', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_invoice_address_id', 'CustomerInvoiceAddressId', 'INTEGER', false, null, null);
        $this->addColumn('customer_invoice_company_name', 'CustomerInvoiceCompanyName', 'VARCHAR', false, 120, null);
        $this->addColumn('customer_invoice_attn_name', 'CustomerInvoiceAttnName', 'VARCHAR', false, 120, null);
        $this->addColumn('customer_invoice_street', 'CustomerInvoiceStreet', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_invoice_number', 'CustomerInvoiceNumber', 'VARCHAR', false, 150, null);
        $this->addColumn('customer_invoice_number_add', 'CustomerInvoiceNumberAdd', 'VARCHAR', false, 150, null);
        $this->addColumn('customer_invoice_postal', 'CustomerInvoicePostal', 'VARCHAR', false, 150, null);
        $this->addColumn('customer_invoice_city', 'CustomerInvoiceCity', 'VARCHAR', false, 150, null);
        $this->addColumn('customer_invoice_country', 'CustomerInvoiceCountry', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_invoice_usa_state', 'CustomerInvoiceUsaState', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_invoice_address_l1', 'CustomerInvoiceAddressL1', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_invoice_address_l2', 'CustomerInvoiceAddressL2', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_delivery_address_id', 'CustomerDeliveryAddressId', 'INTEGER', false, null, null);
        $this->addColumn('customer_delivery_company_name', 'CustomerDeliveryCompanyName', 'VARCHAR', false, 120, null);
        $this->addColumn('customer_delivery_attn_name', 'CustomerDeliveryAttnName', 'VARCHAR', false, 120, null);
        $this->addColumn('customer_delivery_street', 'CustomerDeliveryStreet', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_delivery_number', 'CustomerDeliveryNumber', 'VARCHAR', false, 150, null);
        $this->addColumn('customer_delivery_number_add', 'CustomerDeliveryNumberAdd', 'VARCHAR', false, 150, null);
        $this->addColumn('customer_delivery_postal', 'CustomerDeliveryPostal', 'VARCHAR', false, 150, null);
        $this->addColumn('customer_delivery_city', 'CustomerDeliveryCity', 'VARCHAR', false, 150, null);
        $this->addColumn('customer_delivery_country', 'CustomerDeliveryCountry', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_delivery_usa_state', 'CustomerDeliveryUsaState', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_delivery_address_l1', 'CustomerDeliveryAddressL1', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_delivery_address_l2', 'CustomerDeliveryAddressL2', 'VARCHAR', false, 255, null);
        $this->addColumn('cp_first_name', 'CpFirstName', 'VARCHAR', false, 75, null);
        $this->addColumn('cp_last_name', 'CpLastName', 'VARCHAR', false, 75, null);
        $this->addColumn('cp_sallutation', 'CpSallutation', 'VARCHAR', false, 25, null);
        $this->addColumn('reference', 'Reference', 'LONGVARCHAR', false, null, null);
        $this->addColumn('shipping_note', 'ShippingNote', 'LONGVARCHAR', false, null, null);
        $this->addColumn('customer_order_note', 'CustomerOrderNote', 'LONGVARCHAR', false, null, null);
        $this->addColumn('shipping_order_note', 'ShippingOrderNote', 'LONGVARCHAR', false, null, null);
        $this->addColumn('has_wrap', 'HasWrap', 'BOOLEAN', false, 1, false);
        $this->addColumn('wrapping_price', 'WrappingPrice', 'FLOAT', false, null, 0);
        $this->addColumn('shipping_carrier', 'ShippingCarrier', 'VARCHAR', false, 255, null);
        $this->addColumn('shipping_price', 'ShippingPrice', 'FLOAT', false, null, null);
        $this->addForeignKey('shipping_method_id', 'ShippingMethodId', 'INTEGER', 'mt_shipping_method', 'id', false, null, null);
        $this->addColumn('shipping_has_track_trace', 'ShippingHasTrackTrace', 'BOOLEAN', false, 1, null);
        $this->addColumn('shipping_id', 'ShippingId', 'FLOAT', false, null, null);
        $this->addColumn('tracking_code', 'TrackingCode', 'VARCHAR', false, 255, null);
        $this->addColumn('tracking_url', 'TrackingUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('paymethod_name', 'PaymethodName', 'VARCHAR', false, 255, null);
        $this->addColumn('paymethod_code', 'PaymethodCode', 'VARCHAR', false, 255, null);
        $this->addColumn('paymethod_id', 'PaymethodId', 'INTEGER', false, null, null);
        $this->addColumn('shipping_registration_date', 'ShippingRegistrationDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('invoice_by_postal', 'InvoiceByPostal', 'BOOLEAN', false, 1, null);
        $this->addColumn('packing_slip_type', 'PackingSlipType', 'VARCHAR', false, 255, null);
        $this->addColumn('work_in_progress', 'WorkInProgress', 'BOOLEAN', false, 1, false);
        $this->addColumn('package_amount', 'PackageAmount', 'INTEGER', false, null, null);
        $this->addColumn('coupon_code', 'CouponCode', 'VARCHAR', false, 255, null);
        $this->addColumn('coupon_discount_amount', 'CouponDiscountAmount', 'FLOAT', false, null, null);
        $this->addColumn('coupon_discount_percentage', 'CouponDiscountPercentage', 'FLOAT', false, null, null);
        $this->addColumn('hide_untill', 'HideUntill', 'DATE', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Site', '\\Model\\Cms\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':site_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('SaleOrderStatus', '\\Model\\Setting\\MasterTable\\SaleOrderStatus', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':sale_order_status_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('ShippingMethod', '\\Model\\Setting\\MasterTable\\ShippingMethod', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':shipping_method_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('User', '\\Model\\Account\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':created_by_user_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('SaleOrderPayment', '\\Model\\Sale\\SaleOrderPayment', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sale_order_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'SaleOrderPayments', false);
        $this->addRelation('Payment', '\\Model\\Finance\\Payment', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sale_order_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'Payments', false);
        $this->addRelation('SaleOrderEmail', '\\Model\\Sale\\SaleOrderEmail', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sale_order_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'SaleOrderEmails', false);
        $this->addRelation('SaleOrderItem', '\\Model\\Sale\\SaleOrderItem', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sale_order_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'SaleOrderItems', false);
        $this->addRelation('SaleOrderNotification', '\\Model\\SaleOrderNotification\\SaleOrderNotification', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sale_order_id',
    1 => ':id',
  ),
), 'SET NULL', null, 'SaleOrderNotifications', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '3600', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to sale_order     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        SaleOrderPaymentTableMap::clearInstancePool();
        SaleOrderEmailTableMap::clearInstancePool();
        SaleOrderNotificationTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SaleOrderTableMap::CLASS_DEFAULT : SaleOrderTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SaleOrder object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SaleOrderTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SaleOrderTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SaleOrderTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SaleOrderTableMap::OM_CLASS;
            /** @var SaleOrder $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SaleOrderTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SaleOrderTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SaleOrderTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SaleOrder $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SaleOrderTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SaleOrderTableMap::COL_ID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_SITE_ID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_LANGUAGE_ID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_IS_CLOSED);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_IS_FULLY_PAID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_PAY_DATE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_IS_READY_FOR_SHIPPING);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_IS_SHIPPING_CHECKED);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_IS_FULLY_SHIPPED);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_IS_PUSHED_TO_ACCOUNTING);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_SHIP_DATE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_SALE_ORDER_STATUS_ID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_HANDLED_BY);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_SOURCE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_IS_COMPLETED);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CREATED_BY_USER_ID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CREATED_ON);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_ESTIMATED_DELIVERY_DATE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_ID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_ENVELOPE_WEIGHT_GRAMS);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_WEBSHOP_USER_SESSION_ID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_INVOICE_NUMBER);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_INVOICE_DATE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_PAYTERM_DAYS);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_PAYTERM_ORIGINAL_ID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_PAYTERM_STRING);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_ORDER_NUMBER);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_IS_DELETED);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_COMPANY_NAME);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_DEBITOR);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_CUSTOM_FOLDER);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_SLOGAN);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_PHONE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_FAX);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_WEBSITE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_EMAIL);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_VAT_NUMBER);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_CHAMBER_OF_COMMERCE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_IBAN_NUMBER);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_BIC_NUMBER);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_GENERAL_COMPANY_NAME);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_GENERAL_STREET);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_GENERAL_NUMBER);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_GENERAL_NUMBER_ADD);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_GENERAL_POSTAL);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_GENERAL_PO_BOX);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_GENERAL_CITY);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_OUR_GENERAL_COUNTRY);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_PHONE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_FAX);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_EMAIL);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_CHAMBER_OF_COMMERCE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_VAT_NUMBER);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_ORDER_REFERENCE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_ORDER_PLACED_BY);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_ID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_INVOICE_COMPANY_NAME);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ATTN_NAME);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_INVOICE_STREET);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_INVOICE_NUMBER_ADD);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_INVOICE_POSTAL);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_INVOICE_CITY);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_INVOICE_COUNTRY);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_INVOICE_USA_STATE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L1);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_INVOICE_ADDRESS_L2);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_ID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COMPANY_NAME);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ATTN_NAME);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_STREET);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_NUMBER_ADD);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_POSTAL);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_CITY);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_COUNTRY);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_USA_STATE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L1);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_DELIVERY_ADDRESS_L2);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CP_FIRST_NAME);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CP_LAST_NAME);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CP_SALLUTATION);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_REFERENCE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_SHIPPING_NOTE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_CUSTOMER_ORDER_NOTE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_SHIPPING_ORDER_NOTE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_HAS_WRAP);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_WRAPPING_PRICE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_SHIPPING_CARRIER);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_SHIPPING_PRICE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_SHIPPING_METHOD_ID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_SHIPPING_HAS_TRACK_TRACE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_SHIPPING_ID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_TRACKING_CODE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_TRACKING_URL);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_PAYMETHOD_NAME);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_PAYMETHOD_CODE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_PAYMETHOD_ID);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_SHIPPING_REGISTRATION_DATE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_INVOICE_BY_POSTAL);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_PACKING_SLIP_TYPE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_WORK_IN_PROGRESS);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_PACKAGE_AMOUNT);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_COUPON_CODE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_COUPON_DISCOUNT_AMOUNT);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_COUPON_DISCOUNT_PERCENTAGE);
            $criteria->addSelectColumn(SaleOrderTableMap::COL_HIDE_UNTILL);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.site_id');
            $criteria->addSelectColumn($alias . '.language_id');
            $criteria->addSelectColumn($alias . '.is_closed');
            $criteria->addSelectColumn($alias . '.is_fully_paid');
            $criteria->addSelectColumn($alias . '.pay_date');
            $criteria->addSelectColumn($alias . '.is_ready_for_shipping');
            $criteria->addSelectColumn($alias . '.is_shipping_checked');
            $criteria->addSelectColumn($alias . '.is_fully_shipped');
            $criteria->addSelectColumn($alias . '.is_pushed_to_accounting');
            $criteria->addSelectColumn($alias . '.ship_date');
            $criteria->addSelectColumn($alias . '.sale_order_status_id');
            $criteria->addSelectColumn($alias . '.handled_by');
            $criteria->addSelectColumn($alias . '.source');
            $criteria->addSelectColumn($alias . '.is_completed');
            $criteria->addSelectColumn($alias . '.created_by_user_id');
            $criteria->addSelectColumn($alias . '.created_on');
            $criteria->addSelectColumn($alias . '.estimated_delivery_date');
            $criteria->addSelectColumn($alias . '.customer_id');
            $criteria->addSelectColumn($alias . '.envelope_weight_grams');
            $criteria->addSelectColumn($alias . '.webshop_user_session_id');
            $criteria->addSelectColumn($alias . '.invoice_number');
            $criteria->addSelectColumn($alias . '.invoice_date');
            $criteria->addSelectColumn($alias . '.payterm_days');
            $criteria->addSelectColumn($alias . '.payterm_original_id');
            $criteria->addSelectColumn($alias . '.payterm_string');
            $criteria->addSelectColumn($alias . '.order_number');
            $criteria->addSelectColumn($alias . '.is_deleted');
            $criteria->addSelectColumn($alias . '.company_name');
            $criteria->addSelectColumn($alias . '.debitor');
            $criteria->addSelectColumn($alias . '.our_custom_folder');
            $criteria->addSelectColumn($alias . '.our_slogan');
            $criteria->addSelectColumn($alias . '.our_phone');
            $criteria->addSelectColumn($alias . '.our_fax');
            $criteria->addSelectColumn($alias . '.our_website');
            $criteria->addSelectColumn($alias . '.our_email');
            $criteria->addSelectColumn($alias . '.our_vat_number');
            $criteria->addSelectColumn($alias . '.our_chamber_of_commerce');
            $criteria->addSelectColumn($alias . '.our_iban_number');
            $criteria->addSelectColumn($alias . '.our_bic_number');
            $criteria->addSelectColumn($alias . '.our_general_company_name');
            $criteria->addSelectColumn($alias . '.our_general_street');
            $criteria->addSelectColumn($alias . '.our_general_number');
            $criteria->addSelectColumn($alias . '.our_general_number_add');
            $criteria->addSelectColumn($alias . '.our_general_postal');
            $criteria->addSelectColumn($alias . '.our_general_po_box');
            $criteria->addSelectColumn($alias . '.our_general_city');
            $criteria->addSelectColumn($alias . '.our_general_country');
            $criteria->addSelectColumn($alias . '.customer_phone');
            $criteria->addSelectColumn($alias . '.customer_fax');
            $criteria->addSelectColumn($alias . '.customer_email');
            $criteria->addSelectColumn($alias . '.customer_chamber_of_commerce');
            $criteria->addSelectColumn($alias . '.customer_vat_number');
            $criteria->addSelectColumn($alias . '.customer_order_reference');
            $criteria->addSelectColumn($alias . '.customer_order_placed_by');
            $criteria->addSelectColumn($alias . '.customer_invoice_address_id');
            $criteria->addSelectColumn($alias . '.customer_invoice_company_name');
            $criteria->addSelectColumn($alias . '.customer_invoice_attn_name');
            $criteria->addSelectColumn($alias . '.customer_invoice_street');
            $criteria->addSelectColumn($alias . '.customer_invoice_number');
            $criteria->addSelectColumn($alias . '.customer_invoice_number_add');
            $criteria->addSelectColumn($alias . '.customer_invoice_postal');
            $criteria->addSelectColumn($alias . '.customer_invoice_city');
            $criteria->addSelectColumn($alias . '.customer_invoice_country');
            $criteria->addSelectColumn($alias . '.customer_invoice_usa_state');
            $criteria->addSelectColumn($alias . '.customer_invoice_address_l1');
            $criteria->addSelectColumn($alias . '.customer_invoice_address_l2');
            $criteria->addSelectColumn($alias . '.customer_delivery_address_id');
            $criteria->addSelectColumn($alias . '.customer_delivery_company_name');
            $criteria->addSelectColumn($alias . '.customer_delivery_attn_name');
            $criteria->addSelectColumn($alias . '.customer_delivery_street');
            $criteria->addSelectColumn($alias . '.customer_delivery_number');
            $criteria->addSelectColumn($alias . '.customer_delivery_number_add');
            $criteria->addSelectColumn($alias . '.customer_delivery_postal');
            $criteria->addSelectColumn($alias . '.customer_delivery_city');
            $criteria->addSelectColumn($alias . '.customer_delivery_country');
            $criteria->addSelectColumn($alias . '.customer_delivery_usa_state');
            $criteria->addSelectColumn($alias . '.customer_delivery_address_l1');
            $criteria->addSelectColumn($alias . '.customer_delivery_address_l2');
            $criteria->addSelectColumn($alias . '.cp_first_name');
            $criteria->addSelectColumn($alias . '.cp_last_name');
            $criteria->addSelectColumn($alias . '.cp_sallutation');
            $criteria->addSelectColumn($alias . '.reference');
            $criteria->addSelectColumn($alias . '.shipping_note');
            $criteria->addSelectColumn($alias . '.customer_order_note');
            $criteria->addSelectColumn($alias . '.shipping_order_note');
            $criteria->addSelectColumn($alias . '.has_wrap');
            $criteria->addSelectColumn($alias . '.wrapping_price');
            $criteria->addSelectColumn($alias . '.shipping_carrier');
            $criteria->addSelectColumn($alias . '.shipping_price');
            $criteria->addSelectColumn($alias . '.shipping_method_id');
            $criteria->addSelectColumn($alias . '.shipping_has_track_trace');
            $criteria->addSelectColumn($alias . '.shipping_id');
            $criteria->addSelectColumn($alias . '.tracking_code');
            $criteria->addSelectColumn($alias . '.tracking_url');
            $criteria->addSelectColumn($alias . '.paymethod_name');
            $criteria->addSelectColumn($alias . '.paymethod_code');
            $criteria->addSelectColumn($alias . '.paymethod_id');
            $criteria->addSelectColumn($alias . '.shipping_registration_date');
            $criteria->addSelectColumn($alias . '.invoice_by_postal');
            $criteria->addSelectColumn($alias . '.packing_slip_type');
            $criteria->addSelectColumn($alias . '.work_in_progress');
            $criteria->addSelectColumn($alias . '.package_amount');
            $criteria->addSelectColumn($alias . '.coupon_code');
            $criteria->addSelectColumn($alias . '.coupon_discount_amount');
            $criteria->addSelectColumn($alias . '.coupon_discount_percentage');
            $criteria->addSelectColumn($alias . '.hide_untill');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SaleOrderTableMap::DATABASE_NAME)->getTable(SaleOrderTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SaleOrderTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SaleOrderTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SaleOrderTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SaleOrder or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SaleOrder object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Sale\SaleOrder) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SaleOrderTableMap::DATABASE_NAME);
            $criteria->add(SaleOrderTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SaleOrderQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SaleOrderTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SaleOrderTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the sale_order table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SaleOrderQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SaleOrder or Criteria object.
     *
     * @param mixed               $criteria Criteria or SaleOrder object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SaleOrder object
        }

        if ($criteria->containsKey(SaleOrderTableMap::COL_ID) && $criteria->keyContainsValue(SaleOrderTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SaleOrderTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SaleOrderQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SaleOrderTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SaleOrderTableMap::buildTableMap();
