<?php

namespace Model\Sale\Map;

use Model\Sale\DhlShipment;
use Model\Sale\DhlShipmentQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'dhl_shipment' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class DhlShipmentTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Sale.Map.DhlShipmentTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'dhl_shipment';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Sale\\DhlShipment';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Sale.DhlShipment';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'dhl_shipment.id';

    /**
     * the column name for the sale_order_id field
     */
    const COL_SALE_ORDER_ID = 'dhl_shipment.sale_order_id';

    /**
     * the column name for the pickup_point field
     */
    const COL_PICKUP_POINT = 'dhl_shipment.pickup_point';

    /**
     * the column name for the delivery_method field
     */
    const COL_DELIVERY_METHOD = 'dhl_shipment.delivery_method';

    /**
     * the column name for the delivery_method_dhl_name field
     */
    const COL_DELIVERY_METHOD_DHL_NAME = 'dhl_shipment.delivery_method_dhl_name';

    /**
     * the column name for the weight field
     */
    const COL_WEIGHT = 'dhl_shipment.weight';

    /**
     * the column name for the attn_to_firstname field
     */
    const COL_ATTN_TO_FIRSTNAME = 'dhl_shipment.attn_to_firstname';

    /**
     * the column name for the attn_to_lastname field
     */
    const COL_ATTN_TO_LASTNAME = 'dhl_shipment.attn_to_lastname';

    /**
     * the column name for the reference field
     */
    const COL_REFERENCE = 'dhl_shipment.reference';

    /**
     * the column name for the email_note field
     */
    const COL_EMAIL_NOTE = 'dhl_shipment.email_note';

    /**
     * the column name for the shipment_option field
     */
    const COL_SHIPMENT_OPTION = 'dhl_shipment.shipment_option';

    /**
     * the column name for the additional_features field
     */
    const COL_ADDITIONAL_FEATURES = 'dhl_shipment.additional_features';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'SaleOrderId', 'PickupPoint', 'DeliveryMethod', 'DeliveryMethodDhlName', 'Weight', 'AttnToFirstname', 'AttnToLastname', 'Reference', 'EmailNote', 'ShipmentOption', 'AdditionalFeatures', ),
        self::TYPE_CAMELNAME     => array('id', 'saleOrderId', 'pickupPoint', 'deliveryMethod', 'deliveryMethodDhlName', 'weight', 'attnToFirstname', 'attnToLastname', 'reference', 'emailNote', 'shipmentOption', 'additionalFeatures', ),
        self::TYPE_COLNAME       => array(DhlShipmentTableMap::COL_ID, DhlShipmentTableMap::COL_SALE_ORDER_ID, DhlShipmentTableMap::COL_PICKUP_POINT, DhlShipmentTableMap::COL_DELIVERY_METHOD, DhlShipmentTableMap::COL_DELIVERY_METHOD_DHL_NAME, DhlShipmentTableMap::COL_WEIGHT, DhlShipmentTableMap::COL_ATTN_TO_FIRSTNAME, DhlShipmentTableMap::COL_ATTN_TO_LASTNAME, DhlShipmentTableMap::COL_REFERENCE, DhlShipmentTableMap::COL_EMAIL_NOTE, DhlShipmentTableMap::COL_SHIPMENT_OPTION, DhlShipmentTableMap::COL_ADDITIONAL_FEATURES, ),
        self::TYPE_FIELDNAME     => array('id', 'sale_order_id', 'pickup_point', 'delivery_method', 'delivery_method_dhl_name', 'weight', 'attn_to_firstname', 'attn_to_lastname', 'reference', 'email_note', 'shipment_option', 'additional_features', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'SaleOrderId' => 1, 'PickupPoint' => 2, 'DeliveryMethod' => 3, 'DeliveryMethodDhlName' => 4, 'Weight' => 5, 'AttnToFirstname' => 6, 'AttnToLastname' => 7, 'Reference' => 8, 'EmailNote' => 9, 'ShipmentOption' => 10, 'AdditionalFeatures' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'saleOrderId' => 1, 'pickupPoint' => 2, 'deliveryMethod' => 3, 'deliveryMethodDhlName' => 4, 'weight' => 5, 'attnToFirstname' => 6, 'attnToLastname' => 7, 'reference' => 8, 'emailNote' => 9, 'shipmentOption' => 10, 'additionalFeatures' => 11, ),
        self::TYPE_COLNAME       => array(DhlShipmentTableMap::COL_ID => 0, DhlShipmentTableMap::COL_SALE_ORDER_ID => 1, DhlShipmentTableMap::COL_PICKUP_POINT => 2, DhlShipmentTableMap::COL_DELIVERY_METHOD => 3, DhlShipmentTableMap::COL_DELIVERY_METHOD_DHL_NAME => 4, DhlShipmentTableMap::COL_WEIGHT => 5, DhlShipmentTableMap::COL_ATTN_TO_FIRSTNAME => 6, DhlShipmentTableMap::COL_ATTN_TO_LASTNAME => 7, DhlShipmentTableMap::COL_REFERENCE => 8, DhlShipmentTableMap::COL_EMAIL_NOTE => 9, DhlShipmentTableMap::COL_SHIPMENT_OPTION => 10, DhlShipmentTableMap::COL_ADDITIONAL_FEATURES => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'sale_order_id' => 1, 'pickup_point' => 2, 'delivery_method' => 3, 'delivery_method_dhl_name' => 4, 'weight' => 5, 'attn_to_firstname' => 6, 'attn_to_lastname' => 7, 'reference' => 8, 'email_note' => 9, 'shipment_option' => 10, 'additional_features' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('dhl_shipment');
        $this->setPhpName('DhlShipment');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Sale\\DhlShipment');
        $this->setPackage('Model.Sale');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('sale_order_id', 'SaleOrderId', 'INTEGER', 'sale_order', 'id', true, null, null);
        $this->addColumn('pickup_point', 'PickupPoint', 'VARCHAR', false, 255, null);
        $this->addColumn('delivery_method', 'DeliveryMethod', 'VARCHAR', false, 255, null);
        $this->addColumn('delivery_method_dhl_name', 'DeliveryMethodDhlName', 'VARCHAR', false, 255, null);
        $this->addColumn('weight', 'Weight', 'FLOAT', false, null, null);
        $this->addColumn('attn_to_firstname', 'AttnToFirstname', 'VARCHAR', false, 255, null);
        $this->addColumn('attn_to_lastname', 'AttnToLastname', 'VARCHAR', false, 255, null);
        $this->addColumn('reference', 'Reference', 'VARCHAR', false, 255, null);
        $this->addColumn('email_note', 'EmailNote', 'LONGVARCHAR', false, null, null);
        $this->addColumn('shipment_option', 'ShipmentOption', 'VARCHAR', false, 255, null);
        $this->addColumn('additional_features', 'AdditionalFeatures', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SaleOrder', '\\Model\\Sale\\SaleOrder', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':sale_order_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? DhlShipmentTableMap::CLASS_DEFAULT : DhlShipmentTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (DhlShipment object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = DhlShipmentTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = DhlShipmentTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + DhlShipmentTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = DhlShipmentTableMap::OM_CLASS;
            /** @var DhlShipment $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            DhlShipmentTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = DhlShipmentTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = DhlShipmentTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var DhlShipment $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                DhlShipmentTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(DhlShipmentTableMap::COL_ID);
            $criteria->addSelectColumn(DhlShipmentTableMap::COL_SALE_ORDER_ID);
            $criteria->addSelectColumn(DhlShipmentTableMap::COL_PICKUP_POINT);
            $criteria->addSelectColumn(DhlShipmentTableMap::COL_DELIVERY_METHOD);
            $criteria->addSelectColumn(DhlShipmentTableMap::COL_DELIVERY_METHOD_DHL_NAME);
            $criteria->addSelectColumn(DhlShipmentTableMap::COL_WEIGHT);
            $criteria->addSelectColumn(DhlShipmentTableMap::COL_ATTN_TO_FIRSTNAME);
            $criteria->addSelectColumn(DhlShipmentTableMap::COL_ATTN_TO_LASTNAME);
            $criteria->addSelectColumn(DhlShipmentTableMap::COL_REFERENCE);
            $criteria->addSelectColumn(DhlShipmentTableMap::COL_EMAIL_NOTE);
            $criteria->addSelectColumn(DhlShipmentTableMap::COL_SHIPMENT_OPTION);
            $criteria->addSelectColumn(DhlShipmentTableMap::COL_ADDITIONAL_FEATURES);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.sale_order_id');
            $criteria->addSelectColumn($alias . '.pickup_point');
            $criteria->addSelectColumn($alias . '.delivery_method');
            $criteria->addSelectColumn($alias . '.delivery_method_dhl_name');
            $criteria->addSelectColumn($alias . '.weight');
            $criteria->addSelectColumn($alias . '.attn_to_firstname');
            $criteria->addSelectColumn($alias . '.attn_to_lastname');
            $criteria->addSelectColumn($alias . '.reference');
            $criteria->addSelectColumn($alias . '.email_note');
            $criteria->addSelectColumn($alias . '.shipment_option');
            $criteria->addSelectColumn($alias . '.additional_features');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(DhlShipmentTableMap::DATABASE_NAME)->getTable(DhlShipmentTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(DhlShipmentTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(DhlShipmentTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new DhlShipmentTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a DhlShipment or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or DhlShipment object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DhlShipmentTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Sale\DhlShipment) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(DhlShipmentTableMap::DATABASE_NAME);
            $criteria->add(DhlShipmentTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = DhlShipmentQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            DhlShipmentTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                DhlShipmentTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the dhl_shipment table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return DhlShipmentQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a DhlShipment or Criteria object.
     *
     * @param mixed               $criteria Criteria or DhlShipment object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DhlShipmentTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from DhlShipment object
        }

        if ($criteria->containsKey(DhlShipmentTableMap::COL_ID) && $criteria->keyContainsValue(DhlShipmentTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.DhlShipmentTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = DhlShipmentQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // DhlShipmentTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
DhlShipmentTableMap::buildTableMap();
