<?php

namespace Model\Sale\Map;

use Model\Sale\SaleOrderPayment;
use Model\Sale\SaleOrderPaymentQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'sale_order_payment' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class SaleOrderPaymentTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Sale.Map.SaleOrderPaymentTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'sale_order_payment';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Sale\\SaleOrderPayment';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Sale.SaleOrderPayment';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'sale_order_payment.id';

    /**
     * the column name for the sale_order_id field
     */
    const COL_SALE_ORDER_ID = 'sale_order_payment.sale_order_id';

    /**
     * the column name for the payment_method_id field
     */
    const COL_PAYMENT_METHOD_ID = 'sale_order_payment.payment_method_id';

    /**
     * the column name for the total_price field
     */
    const COL_TOTAL_PRICE = 'sale_order_payment.total_price';

    /**
     * the column name for the our_secret_hash field
     */
    const COL_OUR_SECRET_HASH = 'sale_order_payment.our_secret_hash';

    /**
     * the column name for the trxid field
     */
    const COL_TRXID = 'sale_order_payment.trxid';

    /**
     * the column name for the trace_reference field
     */
    const COL_TRACE_REFERENCE = 'sale_order_payment.trace_reference';

    /**
     * the column name for the payment_reference field
     */
    const COL_PAYMENT_REFERENCE = 'sale_order_payment.payment_reference';

    /**
     * the column name for the transaction_reference field
     */
    const COL_TRANSACTION_REFERENCE = 'sale_order_payment.transaction_reference';

    /**
     * the column name for the pay_url field
     */
    const COL_PAY_URL = 'sale_order_payment.pay_url';

    /**
     * the column name for the started_on field
     */
    const COL_STARTED_ON = 'sale_order_payment.started_on';

    /**
     * the column name for the is_paid field
     */
    const COL_IS_PAID = 'sale_order_payment.is_paid';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'SaleOrderId', 'PaymentMethodId', 'TotalPrice', 'OurSecretHash', 'Trxid', 'TraceReference', 'PaymentReference', 'TransactionReference', 'PayUrl', 'StartedOn', 'IsPaid', ),
        self::TYPE_CAMELNAME     => array('id', 'saleOrderId', 'paymentMethodId', 'totalPrice', 'ourSecretHash', 'trxid', 'traceReference', 'paymentReference', 'transactionReference', 'payUrl', 'startedOn', 'isPaid', ),
        self::TYPE_COLNAME       => array(SaleOrderPaymentTableMap::COL_ID, SaleOrderPaymentTableMap::COL_SALE_ORDER_ID, SaleOrderPaymentTableMap::COL_PAYMENT_METHOD_ID, SaleOrderPaymentTableMap::COL_TOTAL_PRICE, SaleOrderPaymentTableMap::COL_OUR_SECRET_HASH, SaleOrderPaymentTableMap::COL_TRXID, SaleOrderPaymentTableMap::COL_TRACE_REFERENCE, SaleOrderPaymentTableMap::COL_PAYMENT_REFERENCE, SaleOrderPaymentTableMap::COL_TRANSACTION_REFERENCE, SaleOrderPaymentTableMap::COL_PAY_URL, SaleOrderPaymentTableMap::COL_STARTED_ON, SaleOrderPaymentTableMap::COL_IS_PAID, ),
        self::TYPE_FIELDNAME     => array('id', 'sale_order_id', 'payment_method_id', 'total_price', 'our_secret_hash', 'trxid', 'trace_reference', 'payment_reference', 'transaction_reference', 'pay_url', 'started_on', 'is_paid', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'SaleOrderId' => 1, 'PaymentMethodId' => 2, 'TotalPrice' => 3, 'OurSecretHash' => 4, 'Trxid' => 5, 'TraceReference' => 6, 'PaymentReference' => 7, 'TransactionReference' => 8, 'PayUrl' => 9, 'StartedOn' => 10, 'IsPaid' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'saleOrderId' => 1, 'paymentMethodId' => 2, 'totalPrice' => 3, 'ourSecretHash' => 4, 'trxid' => 5, 'traceReference' => 6, 'paymentReference' => 7, 'transactionReference' => 8, 'payUrl' => 9, 'startedOn' => 10, 'isPaid' => 11, ),
        self::TYPE_COLNAME       => array(SaleOrderPaymentTableMap::COL_ID => 0, SaleOrderPaymentTableMap::COL_SALE_ORDER_ID => 1, SaleOrderPaymentTableMap::COL_PAYMENT_METHOD_ID => 2, SaleOrderPaymentTableMap::COL_TOTAL_PRICE => 3, SaleOrderPaymentTableMap::COL_OUR_SECRET_HASH => 4, SaleOrderPaymentTableMap::COL_TRXID => 5, SaleOrderPaymentTableMap::COL_TRACE_REFERENCE => 6, SaleOrderPaymentTableMap::COL_PAYMENT_REFERENCE => 7, SaleOrderPaymentTableMap::COL_TRANSACTION_REFERENCE => 8, SaleOrderPaymentTableMap::COL_PAY_URL => 9, SaleOrderPaymentTableMap::COL_STARTED_ON => 10, SaleOrderPaymentTableMap::COL_IS_PAID => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'sale_order_id' => 1, 'payment_method_id' => 2, 'total_price' => 3, 'our_secret_hash' => 4, 'trxid' => 5, 'trace_reference' => 6, 'payment_reference' => 7, 'transaction_reference' => 8, 'pay_url' => 9, 'started_on' => 10, 'is_paid' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sale_order_payment');
        $this->setPhpName('SaleOrderPayment');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Sale\\SaleOrderPayment');
        $this->setPackage('Model.Sale');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('sale_order_id', 'SaleOrderId', 'INTEGER', 'sale_order', 'id', false, null, null);
        $this->addForeignKey('payment_method_id', 'PaymentMethodId', 'INTEGER', 'payment_method', 'id', false, null, null);
        $this->addColumn('total_price', 'TotalPrice', 'FLOAT', false, null, null);
        $this->addColumn('our_secret_hash', 'OurSecretHash', 'VARCHAR', false, 255, null);
        $this->addColumn('trxid', 'Trxid', 'VARCHAR', false, 255, null);
        $this->addColumn('trace_reference', 'TraceReference', 'VARCHAR', false, 255, null);
        $this->addColumn('payment_reference', 'PaymentReference', 'VARCHAR', false, 255, null);
        $this->addColumn('transaction_reference', 'TransactionReference', 'VARCHAR', false, 255, null);
        $this->addColumn('pay_url', 'PayUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('started_on', 'StartedOn', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('is_paid', 'IsPaid', 'BOOLEAN', false, 1, false);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SaleOrder', '\\Model\\Sale\\SaleOrder', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':sale_order_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('PaymentMethod', '\\Model\\Setting\\MasterTable\\PaymentMethod', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':payment_method_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SaleOrderPaymentTableMap::CLASS_DEFAULT : SaleOrderPaymentTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SaleOrderPayment object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SaleOrderPaymentTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SaleOrderPaymentTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SaleOrderPaymentTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SaleOrderPaymentTableMap::OM_CLASS;
            /** @var SaleOrderPayment $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SaleOrderPaymentTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SaleOrderPaymentTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SaleOrderPaymentTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SaleOrderPayment $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SaleOrderPaymentTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SaleOrderPaymentTableMap::COL_ID);
            $criteria->addSelectColumn(SaleOrderPaymentTableMap::COL_SALE_ORDER_ID);
            $criteria->addSelectColumn(SaleOrderPaymentTableMap::COL_PAYMENT_METHOD_ID);
            $criteria->addSelectColumn(SaleOrderPaymentTableMap::COL_TOTAL_PRICE);
            $criteria->addSelectColumn(SaleOrderPaymentTableMap::COL_OUR_SECRET_HASH);
            $criteria->addSelectColumn(SaleOrderPaymentTableMap::COL_TRXID);
            $criteria->addSelectColumn(SaleOrderPaymentTableMap::COL_TRACE_REFERENCE);
            $criteria->addSelectColumn(SaleOrderPaymentTableMap::COL_PAYMENT_REFERENCE);
            $criteria->addSelectColumn(SaleOrderPaymentTableMap::COL_TRANSACTION_REFERENCE);
            $criteria->addSelectColumn(SaleOrderPaymentTableMap::COL_PAY_URL);
            $criteria->addSelectColumn(SaleOrderPaymentTableMap::COL_STARTED_ON);
            $criteria->addSelectColumn(SaleOrderPaymentTableMap::COL_IS_PAID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.sale_order_id');
            $criteria->addSelectColumn($alias . '.payment_method_id');
            $criteria->addSelectColumn($alias . '.total_price');
            $criteria->addSelectColumn($alias . '.our_secret_hash');
            $criteria->addSelectColumn($alias . '.trxid');
            $criteria->addSelectColumn($alias . '.trace_reference');
            $criteria->addSelectColumn($alias . '.payment_reference');
            $criteria->addSelectColumn($alias . '.transaction_reference');
            $criteria->addSelectColumn($alias . '.pay_url');
            $criteria->addSelectColumn($alias . '.started_on');
            $criteria->addSelectColumn($alias . '.is_paid');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SaleOrderPaymentTableMap::DATABASE_NAME)->getTable(SaleOrderPaymentTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SaleOrderPaymentTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SaleOrderPaymentTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SaleOrderPaymentTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SaleOrderPayment or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SaleOrderPayment object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderPaymentTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Sale\SaleOrderPayment) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SaleOrderPaymentTableMap::DATABASE_NAME);
            $criteria->add(SaleOrderPaymentTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SaleOrderPaymentQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SaleOrderPaymentTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SaleOrderPaymentTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the sale_order_payment table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SaleOrderPaymentQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SaleOrderPayment or Criteria object.
     *
     * @param mixed               $criteria Criteria or SaleOrderPayment object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderPaymentTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SaleOrderPayment object
        }

        if ($criteria->containsKey(SaleOrderPaymentTableMap::COL_ID) && $criteria->keyContainsValue(SaleOrderPaymentTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SaleOrderPaymentTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SaleOrderPaymentQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SaleOrderPaymentTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SaleOrderPaymentTableMap::buildTableMap();
