<?php

namespace Model\Sale\Map;

use Model\Sale\SaleOrderItem;
use Model\Sale\SaleOrderItemQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'sale_order_item' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class SaleOrderItemTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Sale.Map.SaleOrderItemTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'sale_order_item';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Sale\\SaleOrderItem';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Sale.SaleOrderItem';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id field
     */
    const COL_ID = 'sale_order_item.id';

    /**
     * the column name for the created_on field
     */
    const COL_CREATED_ON = 'sale_order_item.created_on';

    /**
     * the column name for the sale_order_id field
     */
    const COL_SALE_ORDER_ID = 'sale_order_item.sale_order_id';

    /**
     * the column name for the product_number field
     */
    const COL_PRODUCT_NUMBER = 'sale_order_item.product_number';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'sale_order_item.description';

    /**
     * the column name for the quantity field
     */
    const COL_QUANTITY = 'sale_order_item.quantity';

    /**
     * the column name for the single_item_weight field
     */
    const COL_SINGLE_ITEM_WEIGHT = 'sale_order_item.single_item_weight';

    /**
     * the column name for the price field
     */
    const COL_PRICE = 'sale_order_item.price';

    /**
     * the column name for the vat_percentage field
     */
    const COL_VAT_PERCENTAGE = 'sale_order_item.vat_percentage';

    /**
     * the column name for the manual_entry field
     */
    const COL_MANUAL_ENTRY = 'sale_order_item.manual_entry';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'CreatedOn', 'SaleOrderId', 'ProductNumber', 'Description', 'Quantity', 'SingleItemWeight', 'Price', 'VatPercentage', 'ManualEntry', ),
        self::TYPE_CAMELNAME     => array('id', 'createdOn', 'saleOrderId', 'productNumber', 'description', 'quantity', 'singleItemWeight', 'price', 'vatPercentage', 'manualEntry', ),
        self::TYPE_COLNAME       => array(SaleOrderItemTableMap::COL_ID, SaleOrderItemTableMap::COL_CREATED_ON, SaleOrderItemTableMap::COL_SALE_ORDER_ID, SaleOrderItemTableMap::COL_PRODUCT_NUMBER, SaleOrderItemTableMap::COL_DESCRIPTION, SaleOrderItemTableMap::COL_QUANTITY, SaleOrderItemTableMap::COL_SINGLE_ITEM_WEIGHT, SaleOrderItemTableMap::COL_PRICE, SaleOrderItemTableMap::COL_VAT_PERCENTAGE, SaleOrderItemTableMap::COL_MANUAL_ENTRY, ),
        self::TYPE_FIELDNAME     => array('id', 'created_on', 'sale_order_id', 'product_number', 'description', 'quantity', 'single_item_weight', 'price', 'vat_percentage', 'manual_entry', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'CreatedOn' => 1, 'SaleOrderId' => 2, 'ProductNumber' => 3, 'Description' => 4, 'Quantity' => 5, 'SingleItemWeight' => 6, 'Price' => 7, 'VatPercentage' => 8, 'ManualEntry' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'createdOn' => 1, 'saleOrderId' => 2, 'productNumber' => 3, 'description' => 4, 'quantity' => 5, 'singleItemWeight' => 6, 'price' => 7, 'vatPercentage' => 8, 'manualEntry' => 9, ),
        self::TYPE_COLNAME       => array(SaleOrderItemTableMap::COL_ID => 0, SaleOrderItemTableMap::COL_CREATED_ON => 1, SaleOrderItemTableMap::COL_SALE_ORDER_ID => 2, SaleOrderItemTableMap::COL_PRODUCT_NUMBER => 3, SaleOrderItemTableMap::COL_DESCRIPTION => 4, SaleOrderItemTableMap::COL_QUANTITY => 5, SaleOrderItemTableMap::COL_SINGLE_ITEM_WEIGHT => 6, SaleOrderItemTableMap::COL_PRICE => 7, SaleOrderItemTableMap::COL_VAT_PERCENTAGE => 8, SaleOrderItemTableMap::COL_MANUAL_ENTRY => 9, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'created_on' => 1, 'sale_order_id' => 2, 'product_number' => 3, 'description' => 4, 'quantity' => 5, 'single_item_weight' => 6, 'price' => 7, 'vat_percentage' => 8, 'manual_entry' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sale_order_item');
        $this->setPhpName('SaleOrderItem');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Sale\\SaleOrderItem');
        $this->setPackage('Model.Sale');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('created_on', 'CreatedOn', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
        $this->addForeignKey('sale_order_id', 'SaleOrderId', 'INTEGER', 'sale_order', 'id', true, null, null);
        $this->addColumn('product_number', 'ProductNumber', 'VARCHAR', false, 255, null);
        $this->addColumn('description', 'Description', 'VARCHAR', true, 255, null);
        $this->addColumn('quantity', 'Quantity', 'FLOAT', true, null, null);
        $this->addColumn('single_item_weight', 'SingleItemWeight', 'INTEGER', false, null, null);
        $this->addColumn('price', 'Price', 'FLOAT', true, null, null);
        $this->addColumn('vat_percentage', 'VatPercentage', 'INTEGER', false, null, null);
        $this->addColumn('manual_entry', 'ManualEntry', 'BOOLEAN', false, 1, false);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SaleOrder', '\\Model\\Sale\\SaleOrder', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':sale_order_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('SaleOrderItemProduct', '\\Model\\Sale\\OrderItemProduct', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sale_order_item_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'SaleOrderItemProducts', false);
        $this->addRelation('SaleOrderItemStockClaimed', '\\Model\\Sale\\SaleOrderItemStockClaimed', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':sale_order_item_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'SaleOrderItemStockClaimeds', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '3600', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to sale_order_item     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        OrderItemProductTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SaleOrderItemTableMap::CLASS_DEFAULT : SaleOrderItemTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SaleOrderItem object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SaleOrderItemTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SaleOrderItemTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SaleOrderItemTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SaleOrderItemTableMap::OM_CLASS;
            /** @var SaleOrderItem $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SaleOrderItemTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SaleOrderItemTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SaleOrderItemTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SaleOrderItem $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SaleOrderItemTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SaleOrderItemTableMap::COL_ID);
            $criteria->addSelectColumn(SaleOrderItemTableMap::COL_CREATED_ON);
            $criteria->addSelectColumn(SaleOrderItemTableMap::COL_SALE_ORDER_ID);
            $criteria->addSelectColumn(SaleOrderItemTableMap::COL_PRODUCT_NUMBER);
            $criteria->addSelectColumn(SaleOrderItemTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(SaleOrderItemTableMap::COL_QUANTITY);
            $criteria->addSelectColumn(SaleOrderItemTableMap::COL_SINGLE_ITEM_WEIGHT);
            $criteria->addSelectColumn(SaleOrderItemTableMap::COL_PRICE);
            $criteria->addSelectColumn(SaleOrderItemTableMap::COL_VAT_PERCENTAGE);
            $criteria->addSelectColumn(SaleOrderItemTableMap::COL_MANUAL_ENTRY);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.created_on');
            $criteria->addSelectColumn($alias . '.sale_order_id');
            $criteria->addSelectColumn($alias . '.product_number');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.quantity');
            $criteria->addSelectColumn($alias . '.single_item_weight');
            $criteria->addSelectColumn($alias . '.price');
            $criteria->addSelectColumn($alias . '.vat_percentage');
            $criteria->addSelectColumn($alias . '.manual_entry');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SaleOrderItemTableMap::DATABASE_NAME)->getTable(SaleOrderItemTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SaleOrderItemTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SaleOrderItemTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SaleOrderItemTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SaleOrderItem or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SaleOrderItem object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderItemTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Sale\SaleOrderItem) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SaleOrderItemTableMap::DATABASE_NAME);
            $criteria->add(SaleOrderItemTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SaleOrderItemQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SaleOrderItemTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SaleOrderItemTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the sale_order_item table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SaleOrderItemQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SaleOrderItem or Criteria object.
     *
     * @param mixed               $criteria Criteria or SaleOrderItem object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderItemTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SaleOrderItem object
        }

        if ($criteria->containsKey(SaleOrderItemTableMap::COL_ID) && $criteria->keyContainsValue(SaleOrderItemTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SaleOrderItemTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SaleOrderItemQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SaleOrderItemTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SaleOrderItemTableMap::buildTableMap();
