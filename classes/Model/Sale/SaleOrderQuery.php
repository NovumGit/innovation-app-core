<?php

namespace Model\Sale;

use Core\PropelSqlTrait;
use Core\QueryMapper;
use Model\Sale\Base\SaleOrderQuery as BaseSaleOrderQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'sale_order' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SaleOrderQuery extends BaseSaleOrderQuery
{
	use PropelSqlTrait;

	public static function getOneRandom()
    {
        $aRow = QueryMapper::fetchRow('SELECT id as item_id FROM sale_order ORDER BY RAND() LIMIT 1');
        $iSaleOrderItemId = $aRow['item_id'];
        return self::create()->findOneById($iSaleOrderItemId);
    }
}
