<?php

namespace Model\Sale;

use Model\Sale\Base\SaleOrderItemStockClaimedQuery as BaseSaleOrderItemStockClaimedQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'sale_order_item_stock_claimed' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SaleOrderItemStockClaimedQuery extends BaseSaleOrderItemStockClaimedQuery
{

}
