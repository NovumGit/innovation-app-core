<?php

namespace Model\Sale;

use Core\PropelSqlTrait;
use Model\Product;
use Model\CustomerQuery;
use Model\Sale\Base\SaleOrderItem as BaseSaleOrderItem;

/**
 * Skeleton subclass for representing a row from the 'sale_order_item' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SaleOrderItem extends BaseSaleOrderItem
{

	use PropelSqlTrait;

	static $aFirstProducts = null;

    function getFirstProduct():Product
    {
	    if(isset(self::$aFirstProducts[$this->getId()]))
	    {
	    	return self::$aFirstProducts[$this->getId()];
	    }
	    $aSaleOrderItemProducts = $this->getSaleOrderItemProducts();
        foreach($aSaleOrderItemProducts as $oSaleOrderItemProduct)
        {
            if($oSaleOrderItemProduct instanceof OrderItemProduct)
            {
                $iProductId = $oSaleOrderItemProduct->getProductId();
                $oProduct = CustomerQuery::create()->findOneById($iProductId);
                if($oProduct instanceof Product)
                {
	                self::$aFirstProducts[$this->getId()] = $oProduct;
                    return $oProduct;
                }
            }
        }
        return new Product();
    }

}
