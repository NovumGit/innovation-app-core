<?php

namespace Model\Sale;

use Model\Sale\Base\CitymailShipment as BaseCitymailShipment;

/**
 * Skeleton subclass for representing a row from the 'citymail_sale_order_shipment' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CitymailShipment extends BaseCitymailShipment
{

}
