<?php

namespace Model\Eenoverheid;

use Model\Eenoverheid\Base\Formulier as BaseFormulier;

/**
 * Skeleton subclass for representing a row from the 'formulieren' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Formulier extends BaseFormulier
{

}
