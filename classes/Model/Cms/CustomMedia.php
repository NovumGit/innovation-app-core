<?php

namespace Model\Cms;
use Core\Config;
use Core\Utils;
use Exception\LogicException;
use Model\Cms\Base\CustomMedia as BaseCustomMedia;

/**
 * Skeleton subclass for representing a row from the 'custom_media' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class CustomMedia extends BaseCustomMedia
{

    function getFileMime()
    {
        $sExt = strtolower(pathinfo($this->getName(), PATHINFO_EXTENSION));
        if(in_array($sExt, ['png', 'jpg', 'jpeg', 'gif', 'svg', 'bmp']))
        {
            return 'image';
        }
        else
        {
            return 'file';
        }
    }
    function deleteWithFiles()
    {
        $sCustomFileDir = Config::getDataDir().'/custom';
        $sFile = $sCustomFileDir.'/'.$this->getNameOnDisk();
        if(file_exists($sFile))
        {
            unlink($sFile);
        }
        $this->delete();
    }
    function isDeletableAndHasNoChildren()
    {
        $bHasChildren = (CustomMediaQuery::create()->findByParentId($this->getId())->count() > 0);

        $bIsDeletable = $this->isDeletable();
        return (!$bHasChildren && $bIsDeletable);
    }
    function getFileUrl()
    {
        return '/img/media/'.$this->getNameOnDisk();
    }
    function getUrl()
    {
        $sSite = $_GET['site'];
        $sType = $this->getCustomMediaNodeType()->getType();
        if($sType == 'folder')
        {
            return '/cms/media/structure?site='.$sSite.'&parent_id='.$this->getId();
        }
        else
        {
            return '/cms/media/view?site='.$sSite.'&id='.$this->getId();
        }
    }
    function getRootPath()
    {
        $sRoot = '';
        if($this->getParentId())
        {
            $oCustomMedia = CustomMediaQuery::create()->findOneById($this->getParentId());
            $sRoot = $oCustomMedia->getRootPath();
        }

        return /** @lang text */
            $sRoot.'/<a href="/cms/media/structure?site='.$_GET['site'].'&parent_id='.$this->getId().'" class="crumble-nav">'.$this->getName().'</a>';
    }
    public function getNameOnDisk()
    {
        return $this->getId().'-'.Utils::slugify($this->getName());
    }
    public function saveFile($tmp_name)
    {
        if(empty($this->getId()))
        {
            throw new LogicException("You must first call save on the CustomMedia object it self");
        }

        $sCustomFileDir = Config::getDataDir().'/custom';

        if(!is_writable(Config::getDataDir()))
        {
            throw new LogicException('Custom datadir is not writable.');
        }
        if(!is_dir($sCustomFileDir))
        {
            mkdir($sCustomFileDir);
        }

        move_uploaded_file($tmp_name, $sCustomFileDir.'/'.$this->getNameOnDisk());
    }
}
