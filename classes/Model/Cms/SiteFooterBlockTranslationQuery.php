<?php

namespace Model\Cms;

use Core\PropelSqlTrait;
use Model\Cms\Base\SiteFooterBlockTranslationQuery as BaseSiteFooterBlockTranslationQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'site_footer_block_translation' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SiteFooterBlockTranslationQuery extends BaseSiteFooterBlockTranslationQuery
{
	use PropelSqlTrait;
}
