<?php

namespace Model\Cms\Mail;

use Model\Cms\Mail\Base\MailTypeQuery as BaseMailTypeQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'mail_type' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MailTypeQuery extends BaseMailTypeQuery
{

}
