<?php

namespace Model\Cms\Mail\Base;

use \Exception;
use \PDO;
use Model\Cms\Site;
use Model\Cms\Mail\MailTemplate as ChildMailTemplate;
use Model\Cms\Mail\MailTemplateQuery as ChildMailTemplateQuery;
use Model\Cms\Mail\Map\MailTemplateTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'mail_template' table.
 *
 *
 *
 * @method     ChildMailTemplateQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildMailTemplateQuery orderByEmailFrom($order = Criteria::ASC) Order by the email_from column
 * @method     ChildMailTemplateQuery orderByEmailCc($order = Criteria::ASC) Order by the email_cc column
 * @method     ChildMailTemplateQuery orderByEmailBcc($order = Criteria::ASC) Order by the email_bcc column
 * @method     ChildMailTemplateQuery orderByEmailSubject($order = Criteria::ASC) Order by the email_subject column
 * @method     ChildMailTemplateQuery orderByContent($order = Criteria::ASC) Order by the content column
 * @method     ChildMailTemplateQuery orderBySiteId($order = Criteria::ASC) Order by the site_id column
 * @method     ChildMailTemplateQuery orderByMailTypeId($order = Criteria::ASC) Order by the mail_type_id column
 *
 * @method     ChildMailTemplateQuery groupById() Group by the id column
 * @method     ChildMailTemplateQuery groupByEmailFrom() Group by the email_from column
 * @method     ChildMailTemplateQuery groupByEmailCc() Group by the email_cc column
 * @method     ChildMailTemplateQuery groupByEmailBcc() Group by the email_bcc column
 * @method     ChildMailTemplateQuery groupByEmailSubject() Group by the email_subject column
 * @method     ChildMailTemplateQuery groupByContent() Group by the content column
 * @method     ChildMailTemplateQuery groupBySiteId() Group by the site_id column
 * @method     ChildMailTemplateQuery groupByMailTypeId() Group by the mail_type_id column
 *
 * @method     ChildMailTemplateQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildMailTemplateQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildMailTemplateQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildMailTemplateQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildMailTemplateQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildMailTemplateQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildMailTemplateQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildMailTemplateQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildMailTemplateQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildMailTemplateQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildMailTemplateQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildMailTemplateQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildMailTemplateQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildMailTemplateQuery leftJoinMailType($relationAlias = null) Adds a LEFT JOIN clause to the query using the MailType relation
 * @method     ChildMailTemplateQuery rightJoinMailType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MailType relation
 * @method     ChildMailTemplateQuery innerJoinMailType($relationAlias = null) Adds a INNER JOIN clause to the query using the MailType relation
 *
 * @method     ChildMailTemplateQuery joinWithMailType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MailType relation
 *
 * @method     ChildMailTemplateQuery leftJoinWithMailType() Adds a LEFT JOIN clause and with to the query using the MailType relation
 * @method     ChildMailTemplateQuery rightJoinWithMailType() Adds a RIGHT JOIN clause and with to the query using the MailType relation
 * @method     ChildMailTemplateQuery innerJoinWithMailType() Adds a INNER JOIN clause and with to the query using the MailType relation
 *
 * @method     \Model\Cms\SiteQuery|\Model\Cms\Mail\MailTypeQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildMailTemplate findOne(ConnectionInterface $con = null) Return the first ChildMailTemplate matching the query
 * @method     ChildMailTemplate findOneOrCreate(ConnectionInterface $con = null) Return the first ChildMailTemplate matching the query, or a new ChildMailTemplate object populated from the query conditions when no match is found
 *
 * @method     ChildMailTemplate findOneById(int $id) Return the first ChildMailTemplate filtered by the id column
 * @method     ChildMailTemplate findOneByEmailFrom(string $email_from) Return the first ChildMailTemplate filtered by the email_from column
 * @method     ChildMailTemplate findOneByEmailCc(string $email_cc) Return the first ChildMailTemplate filtered by the email_cc column
 * @method     ChildMailTemplate findOneByEmailBcc(string $email_bcc) Return the first ChildMailTemplate filtered by the email_bcc column
 * @method     ChildMailTemplate findOneByEmailSubject(string $email_subject) Return the first ChildMailTemplate filtered by the email_subject column
 * @method     ChildMailTemplate findOneByContent(string $content) Return the first ChildMailTemplate filtered by the content column
 * @method     ChildMailTemplate findOneBySiteId(int $site_id) Return the first ChildMailTemplate filtered by the site_id column
 * @method     ChildMailTemplate findOneByMailTypeId(int $mail_type_id) Return the first ChildMailTemplate filtered by the mail_type_id column *

 * @method     ChildMailTemplate requirePk($key, ConnectionInterface $con = null) Return the ChildMailTemplate by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailTemplate requireOne(ConnectionInterface $con = null) Return the first ChildMailTemplate matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMailTemplate requireOneById(int $id) Return the first ChildMailTemplate filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailTemplate requireOneByEmailFrom(string $email_from) Return the first ChildMailTemplate filtered by the email_from column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailTemplate requireOneByEmailCc(string $email_cc) Return the first ChildMailTemplate filtered by the email_cc column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailTemplate requireOneByEmailBcc(string $email_bcc) Return the first ChildMailTemplate filtered by the email_bcc column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailTemplate requireOneByEmailSubject(string $email_subject) Return the first ChildMailTemplate filtered by the email_subject column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailTemplate requireOneByContent(string $content) Return the first ChildMailTemplate filtered by the content column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailTemplate requireOneBySiteId(int $site_id) Return the first ChildMailTemplate filtered by the site_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildMailTemplate requireOneByMailTypeId(int $mail_type_id) Return the first ChildMailTemplate filtered by the mail_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildMailTemplate[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildMailTemplate objects based on current ModelCriteria
 * @method     ChildMailTemplate[]|ObjectCollection findById(int $id) Return ChildMailTemplate objects filtered by the id column
 * @method     ChildMailTemplate[]|ObjectCollection findByEmailFrom(string $email_from) Return ChildMailTemplate objects filtered by the email_from column
 * @method     ChildMailTemplate[]|ObjectCollection findByEmailCc(string $email_cc) Return ChildMailTemplate objects filtered by the email_cc column
 * @method     ChildMailTemplate[]|ObjectCollection findByEmailBcc(string $email_bcc) Return ChildMailTemplate objects filtered by the email_bcc column
 * @method     ChildMailTemplate[]|ObjectCollection findByEmailSubject(string $email_subject) Return ChildMailTemplate objects filtered by the email_subject column
 * @method     ChildMailTemplate[]|ObjectCollection findByContent(string $content) Return ChildMailTemplate objects filtered by the content column
 * @method     ChildMailTemplate[]|ObjectCollection findBySiteId(int $site_id) Return ChildMailTemplate objects filtered by the site_id column
 * @method     ChildMailTemplate[]|ObjectCollection findByMailTypeId(int $mail_type_id) Return ChildMailTemplate objects filtered by the mail_type_id column
 * @method     ChildMailTemplate[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class MailTemplateQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Cms\Mail\Base\MailTemplateQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Cms\\Mail\\MailTemplate', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildMailTemplateQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildMailTemplateQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildMailTemplateQuery) {
            return $criteria;
        }
        $query = new ChildMailTemplateQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildMailTemplate|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(MailTemplateTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = MailTemplateTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMailTemplate A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, email_from, email_cc, email_bcc, email_subject, content, site_id, mail_type_id FROM mail_template WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildMailTemplate $obj */
            $obj = new ChildMailTemplate();
            $obj->hydrate($row);
            MailTemplateTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildMailTemplate|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildMailTemplateQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MailTemplateTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildMailTemplateQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MailTemplateTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailTemplateQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(MailTemplateTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(MailTemplateTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailTemplateTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the email_from column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailFrom('fooValue');   // WHERE email_from = 'fooValue'
     * $query->filterByEmailFrom('%fooValue%', Criteria::LIKE); // WHERE email_from LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailFrom The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailTemplateQuery The current query, for fluid interface
     */
    public function filterByEmailFrom($emailFrom = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailFrom)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailTemplateTableMap::COL_EMAIL_FROM, $emailFrom, $comparison);
    }

    /**
     * Filter the query on the email_cc column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailCc('fooValue');   // WHERE email_cc = 'fooValue'
     * $query->filterByEmailCc('%fooValue%', Criteria::LIKE); // WHERE email_cc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailCc The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailTemplateQuery The current query, for fluid interface
     */
    public function filterByEmailCc($emailCc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailCc)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailTemplateTableMap::COL_EMAIL_CC, $emailCc, $comparison);
    }

    /**
     * Filter the query on the email_bcc column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailBcc('fooValue');   // WHERE email_bcc = 'fooValue'
     * $query->filterByEmailBcc('%fooValue%', Criteria::LIKE); // WHERE email_bcc LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailBcc The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailTemplateQuery The current query, for fluid interface
     */
    public function filterByEmailBcc($emailBcc = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailBcc)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailTemplateTableMap::COL_EMAIL_BCC, $emailBcc, $comparison);
    }

    /**
     * Filter the query on the email_subject column
     *
     * Example usage:
     * <code>
     * $query->filterByEmailSubject('fooValue');   // WHERE email_subject = 'fooValue'
     * $query->filterByEmailSubject('%fooValue%', Criteria::LIKE); // WHERE email_subject LIKE '%fooValue%'
     * </code>
     *
     * @param     string $emailSubject The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailTemplateQuery The current query, for fluid interface
     */
    public function filterByEmailSubject($emailSubject = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($emailSubject)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailTemplateTableMap::COL_EMAIL_SUBJECT, $emailSubject, $comparison);
    }

    /**
     * Filter the query on the content column
     *
     * Example usage:
     * <code>
     * $query->filterByContent('fooValue');   // WHERE content = 'fooValue'
     * $query->filterByContent('%fooValue%', Criteria::LIKE); // WHERE content LIKE '%fooValue%'
     * </code>
     *
     * @param     string $content The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailTemplateQuery The current query, for fluid interface
     */
    public function filterByContent($content = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($content)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailTemplateTableMap::COL_CONTENT, $content, $comparison);
    }

    /**
     * Filter the query on the site_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE site_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE site_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE site_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailTemplateQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(MailTemplateTableMap::COL_SITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(MailTemplateTableMap::COL_SITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailTemplateTableMap::COL_SITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the mail_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMailTypeId(1234); // WHERE mail_type_id = 1234
     * $query->filterByMailTypeId(array(12, 34)); // WHERE mail_type_id IN (12, 34)
     * $query->filterByMailTypeId(array('min' => 12)); // WHERE mail_type_id > 12
     * </code>
     *
     * @see       filterByMailType()
     *
     * @param     mixed $mailTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildMailTemplateQuery The current query, for fluid interface
     */
    public function filterByMailTypeId($mailTypeId = null, $comparison = null)
    {
        if (is_array($mailTypeId)) {
            $useMinMax = false;
            if (isset($mailTypeId['min'])) {
                $this->addUsingAlias(MailTemplateTableMap::COL_MAIL_TYPE_ID, $mailTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($mailTypeId['max'])) {
                $this->addUsingAlias(MailTemplateTableMap::COL_MAIL_TYPE_ID, $mailTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MailTemplateTableMap::COL_MAIL_TYPE_ID, $mailTypeId, $comparison);
    }

    /**
     * Filter the query by a related \Model\Cms\Site object
     *
     * @param \Model\Cms\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMailTemplateQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \Model\Cms\Site) {
            return $this
                ->addUsingAlias(MailTemplateTableMap::COL_SITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MailTemplateTableMap::COL_SITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \Model\Cms\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMailTemplateQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\Model\Cms\SiteQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\Mail\MailType object
     *
     * @param \Model\Cms\Mail\MailType|ObjectCollection $mailType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildMailTemplateQuery The current query, for fluid interface
     */
    public function filterByMailType($mailType, $comparison = null)
    {
        if ($mailType instanceof \Model\Cms\Mail\MailType) {
            return $this
                ->addUsingAlias(MailTemplateTableMap::COL_MAIL_TYPE_ID, $mailType->getId(), $comparison);
        } elseif ($mailType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(MailTemplateTableMap::COL_MAIL_TYPE_ID, $mailType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByMailType() only accepts arguments of type \Model\Cms\Mail\MailType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MailType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildMailTemplateQuery The current query, for fluid interface
     */
    public function joinMailType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MailType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MailType');
        }

        return $this;
    }

    /**
     * Use the MailType relation MailType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\Mail\MailTypeQuery A secondary query class using the current class as primary query
     */
    public function useMailTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMailType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MailType', '\Model\Cms\Mail\MailTypeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildMailTemplate $mailTemplate Object to remove from the list of results
     *
     * @return $this|ChildMailTemplateQuery The current query, for fluid interface
     */
    public function prune($mailTemplate = null)
    {
        if ($mailTemplate) {
            $this->addUsingAlias(MailTemplateTableMap::COL_ID, $mailTemplate->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the mail_template table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MailTemplateTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MailTemplateTableMap::clearInstancePool();
            MailTemplateTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(MailTemplateTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(MailTemplateTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            MailTemplateTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            MailTemplateTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // MailTemplateQuery
