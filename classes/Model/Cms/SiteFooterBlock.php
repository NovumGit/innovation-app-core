<?php

namespace Model\Cms;

use Model\Cms\Base\SiteFooterBlock as BaseSiteFooterBlock;

/**
 * Skeleton subclass for representing a row from the 'site_footer_block' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SiteFooterBlock extends BaseSiteFooterBlock
{

}
