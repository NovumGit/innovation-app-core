<?php

namespace Model\Cms;

use Model\Cms\Base\Site_navigation as BaseSite_navigation;

/**
 * Skeleton subclass for representing a row from the 'site_navigation' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Site_navigation extends BaseSite_navigation
{

}
