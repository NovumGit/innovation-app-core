<?php

namespace Model\Cms;

use Model\Cms\Base\SiteBannerQuery as BaseSiteBannerQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'site_banner' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SiteBannerQuery extends BaseSiteBannerQuery
{

}
