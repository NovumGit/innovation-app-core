<?php

namespace Model\Cms;

use Model\Cms\Base\SiteFAQCategory as BaseSiteFAQCategory;
use Core\Config;

/**
 * Skeleton subclass for representing a row from the 'site_faq_category' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SiteFAQCategory extends BaseSiteFAQCategory
{

    function getImagePath()
    {
        return '/img/faq/'.$this->getId().'.'.$this->getImageExt();
    }
    function getImageFileLocation()
    {
        return Config::getDataDir().'/img/faq/'.$this->getId().'.'.$this->getImageExt();
    }

}
