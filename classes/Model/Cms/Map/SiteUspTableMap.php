<?php

namespace Model\Cms\Map;

use Model\Cms\SiteUsp;
use Model\Cms\SiteUspQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'site_usp' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class SiteUspTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Cms.Map.SiteUspTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'site_usp';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Cms\\SiteUsp';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Cms.SiteUsp';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'site_usp.id';

    /**
     * the column name for the site_id field
     */
    const COL_SITE_ID = 'site_usp.site_id';

    /**
     * the column name for the language_id field
     */
    const COL_LANGUAGE_ID = 'site_usp.language_id';

    /**
     * the column name for the site_hook field
     */
    const COL_SITE_HOOK = 'site_usp.site_hook';

    /**
     * the column name for the about_txt field
     */
    const COL_ABOUT_TXT = 'site_usp.about_txt';

    /**
     * the column name for the short_txt field
     */
    const COL_SHORT_TXT = 'site_usp.short_txt';

    /**
     * the column name for the long_txt field
     */
    const COL_LONG_TXT = 'site_usp.long_txt';

    /**
     * the column name for the slogan field
     */
    const COL_SLOGAN = 'site_usp.slogan';

    /**
     * the column name for the icon field
     */
    const COL_ICON = 'site_usp.icon';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'SiteId', 'LanguageId', 'SiteHook', 'AboutTxt', 'ShortTxt', 'LongTxt', 'Slogan', 'Icon', ),
        self::TYPE_CAMELNAME     => array('id', 'siteId', 'languageId', 'siteHook', 'aboutTxt', 'shortTxt', 'longTxt', 'slogan', 'icon', ),
        self::TYPE_COLNAME       => array(SiteUspTableMap::COL_ID, SiteUspTableMap::COL_SITE_ID, SiteUspTableMap::COL_LANGUAGE_ID, SiteUspTableMap::COL_SITE_HOOK, SiteUspTableMap::COL_ABOUT_TXT, SiteUspTableMap::COL_SHORT_TXT, SiteUspTableMap::COL_LONG_TXT, SiteUspTableMap::COL_SLOGAN, SiteUspTableMap::COL_ICON, ),
        self::TYPE_FIELDNAME     => array('id', 'site_id', 'language_id', 'site_hook', 'about_txt', 'short_txt', 'long_txt', 'slogan', 'icon', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'SiteId' => 1, 'LanguageId' => 2, 'SiteHook' => 3, 'AboutTxt' => 4, 'ShortTxt' => 5, 'LongTxt' => 6, 'Slogan' => 7, 'Icon' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'siteId' => 1, 'languageId' => 2, 'siteHook' => 3, 'aboutTxt' => 4, 'shortTxt' => 5, 'longTxt' => 6, 'slogan' => 7, 'icon' => 8, ),
        self::TYPE_COLNAME       => array(SiteUspTableMap::COL_ID => 0, SiteUspTableMap::COL_SITE_ID => 1, SiteUspTableMap::COL_LANGUAGE_ID => 2, SiteUspTableMap::COL_SITE_HOOK => 3, SiteUspTableMap::COL_ABOUT_TXT => 4, SiteUspTableMap::COL_SHORT_TXT => 5, SiteUspTableMap::COL_LONG_TXT => 6, SiteUspTableMap::COL_SLOGAN => 7, SiteUspTableMap::COL_ICON => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'site_id' => 1, 'language_id' => 2, 'site_hook' => 3, 'about_txt' => 4, 'short_txt' => 5, 'long_txt' => 6, 'slogan' => 7, 'icon' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('site_usp');
        $this->setPhpName('SiteUsp');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Cms\\SiteUsp');
        $this->setPackage('Model.Cms');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('site_id', 'SiteId', 'INTEGER', 'site', 'id', true, null, null);
        $this->addForeignKey('language_id', 'LanguageId', 'INTEGER', 'mt_language', 'id', false, null, null);
        $this->addColumn('site_hook', 'SiteHook', 'VARCHAR', false, 120, null);
        $this->addColumn('about_txt', 'AboutTxt', 'VARCHAR', false, 120, null);
        $this->addColumn('short_txt', 'ShortTxt', 'VARCHAR', false, 120, null);
        $this->addColumn('long_txt', 'LongTxt', 'LONGVARCHAR', false, null, null);
        $this->addColumn('slogan', 'Slogan', 'VARCHAR', false, 120, null);
        $this->addColumn('icon', 'Icon', 'VARCHAR', false, 25, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Site', '\\Model\\Cms\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':site_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Language', '\\Model\\Setting\\MasterTable\\Language', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SiteUspTableMap::CLASS_DEFAULT : SiteUspTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SiteUsp object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SiteUspTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SiteUspTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SiteUspTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SiteUspTableMap::OM_CLASS;
            /** @var SiteUsp $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SiteUspTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SiteUspTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SiteUspTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SiteUsp $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SiteUspTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SiteUspTableMap::COL_ID);
            $criteria->addSelectColumn(SiteUspTableMap::COL_SITE_ID);
            $criteria->addSelectColumn(SiteUspTableMap::COL_LANGUAGE_ID);
            $criteria->addSelectColumn(SiteUspTableMap::COL_SITE_HOOK);
            $criteria->addSelectColumn(SiteUspTableMap::COL_ABOUT_TXT);
            $criteria->addSelectColumn(SiteUspTableMap::COL_SHORT_TXT);
            $criteria->addSelectColumn(SiteUspTableMap::COL_LONG_TXT);
            $criteria->addSelectColumn(SiteUspTableMap::COL_SLOGAN);
            $criteria->addSelectColumn(SiteUspTableMap::COL_ICON);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.site_id');
            $criteria->addSelectColumn($alias . '.language_id');
            $criteria->addSelectColumn($alias . '.site_hook');
            $criteria->addSelectColumn($alias . '.about_txt');
            $criteria->addSelectColumn($alias . '.short_txt');
            $criteria->addSelectColumn($alias . '.long_txt');
            $criteria->addSelectColumn($alias . '.slogan');
            $criteria->addSelectColumn($alias . '.icon');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SiteUspTableMap::DATABASE_NAME)->getTable(SiteUspTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SiteUspTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SiteUspTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SiteUspTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SiteUsp or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SiteUsp object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteUspTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Cms\SiteUsp) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SiteUspTableMap::DATABASE_NAME);
            $criteria->add(SiteUspTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SiteUspQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SiteUspTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SiteUspTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the site_usp table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SiteUspQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SiteUsp or Criteria object.
     *
     * @param mixed               $criteria Criteria or SiteUsp object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteUspTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SiteUsp object
        }

        if ($criteria->containsKey(SiteUspTableMap::COL_ID) && $criteria->keyContainsValue(SiteUspTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SiteUspTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SiteUspQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SiteUspTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SiteUspTableMap::buildTableMap();
