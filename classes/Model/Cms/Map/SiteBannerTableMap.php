<?php

namespace Model\Cms\Map;

use Model\Cms\SiteBanner;
use Model\Cms\SiteBannerQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'site_banner' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class SiteBannerTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Cms.Map.SiteBannerTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'site_banner';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Cms\\SiteBanner';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Cms.SiteBanner';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 11;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 11;

    /**
     * the column name for the id field
     */
    const COL_ID = 'site_banner.id';

    /**
     * the column name for the site_id field
     */
    const COL_SITE_ID = 'site_banner.site_id';

    /**
     * the column name for the about_txt field
     */
    const COL_ABOUT_TXT = 'site_banner.about_txt';

    /**
     * the column name for the alignment field
     */
    const COL_ALIGNMENT = 'site_banner.alignment';

    /**
     * the column name for the slogan field
     */
    const COL_SLOGAN = 'site_banner.slogan';

    /**
     * the column name for the sub_text field
     */
    const COL_SUB_TEXT = 'site_banner.sub_text';

    /**
     * the column name for the has_info_button field
     */
    const COL_HAS_INFO_BUTTON = 'site_banner.has_info_button';

    /**
     * the column name for the info_link field
     */
    const COL_INFO_LINK = 'site_banner.info_link';

    /**
     * the column name for the has_buy_now field
     */
    const COL_HAS_BUY_NOW = 'site_banner.has_buy_now';

    /**
     * the column name for the buy_now_link field
     */
    const COL_BUY_NOW_LINK = 'site_banner.buy_now_link';

    /**
     * the column name for the file_ext field
     */
    const COL_FILE_EXT = 'site_banner.file_ext';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'SiteId', 'AboutTxt', 'Alignment', 'Slogan', 'SubText', 'HasInfoButton', 'InfoLink', 'HasBuyNow', 'BuyNowLink', 'FileExt', ),
        self::TYPE_CAMELNAME     => array('id', 'siteId', 'aboutTxt', 'alignment', 'slogan', 'subText', 'hasInfoButton', 'infoLink', 'hasBuyNow', 'buyNowLink', 'fileExt', ),
        self::TYPE_COLNAME       => array(SiteBannerTableMap::COL_ID, SiteBannerTableMap::COL_SITE_ID, SiteBannerTableMap::COL_ABOUT_TXT, SiteBannerTableMap::COL_ALIGNMENT, SiteBannerTableMap::COL_SLOGAN, SiteBannerTableMap::COL_SUB_TEXT, SiteBannerTableMap::COL_HAS_INFO_BUTTON, SiteBannerTableMap::COL_INFO_LINK, SiteBannerTableMap::COL_HAS_BUY_NOW, SiteBannerTableMap::COL_BUY_NOW_LINK, SiteBannerTableMap::COL_FILE_EXT, ),
        self::TYPE_FIELDNAME     => array('id', 'site_id', 'about_txt', 'alignment', 'slogan', 'sub_text', 'has_info_button', 'info_link', 'has_buy_now', 'buy_now_link', 'file_ext', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'SiteId' => 1, 'AboutTxt' => 2, 'Alignment' => 3, 'Slogan' => 4, 'SubText' => 5, 'HasInfoButton' => 6, 'InfoLink' => 7, 'HasBuyNow' => 8, 'BuyNowLink' => 9, 'FileExt' => 10, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'siteId' => 1, 'aboutTxt' => 2, 'alignment' => 3, 'slogan' => 4, 'subText' => 5, 'hasInfoButton' => 6, 'infoLink' => 7, 'hasBuyNow' => 8, 'buyNowLink' => 9, 'fileExt' => 10, ),
        self::TYPE_COLNAME       => array(SiteBannerTableMap::COL_ID => 0, SiteBannerTableMap::COL_SITE_ID => 1, SiteBannerTableMap::COL_ABOUT_TXT => 2, SiteBannerTableMap::COL_ALIGNMENT => 3, SiteBannerTableMap::COL_SLOGAN => 4, SiteBannerTableMap::COL_SUB_TEXT => 5, SiteBannerTableMap::COL_HAS_INFO_BUTTON => 6, SiteBannerTableMap::COL_INFO_LINK => 7, SiteBannerTableMap::COL_HAS_BUY_NOW => 8, SiteBannerTableMap::COL_BUY_NOW_LINK => 9, SiteBannerTableMap::COL_FILE_EXT => 10, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'site_id' => 1, 'about_txt' => 2, 'alignment' => 3, 'slogan' => 4, 'sub_text' => 5, 'has_info_button' => 6, 'info_link' => 7, 'has_buy_now' => 8, 'buy_now_link' => 9, 'file_ext' => 10, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('site_banner');
        $this->setPhpName('SiteBanner');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Cms\\SiteBanner');
        $this->setPackage('Model.Cms');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('site_id', 'SiteId', 'INTEGER', 'site', 'id', true, null, null);
        $this->addColumn('about_txt', 'AboutTxt', 'VARCHAR', false, 250, null);
        $this->addColumn('alignment', 'Alignment', 'VARCHAR', false, 250, null);
        $this->addColumn('slogan', 'Slogan', 'VARCHAR', false, 250, null);
        $this->addColumn('sub_text', 'SubText', 'VARCHAR', false, 250, null);
        $this->addColumn('has_info_button', 'HasInfoButton', 'BOOLEAN', false, 1, true);
        $this->addColumn('info_link', 'InfoLink', 'VARCHAR', false, 250, null);
        $this->addColumn('has_buy_now', 'HasBuyNow', 'BOOLEAN', false, 1, true);
        $this->addColumn('buy_now_link', 'BuyNowLink', 'VARCHAR', false, 255, null);
        $this->addColumn('file_ext', 'FileExt', 'VARCHAR', false, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Site', '\\Model\\Cms\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':site_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SiteBannerTableMap::CLASS_DEFAULT : SiteBannerTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SiteBanner object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SiteBannerTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SiteBannerTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SiteBannerTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SiteBannerTableMap::OM_CLASS;
            /** @var SiteBanner $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SiteBannerTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SiteBannerTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SiteBannerTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SiteBanner $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SiteBannerTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SiteBannerTableMap::COL_ID);
            $criteria->addSelectColumn(SiteBannerTableMap::COL_SITE_ID);
            $criteria->addSelectColumn(SiteBannerTableMap::COL_ABOUT_TXT);
            $criteria->addSelectColumn(SiteBannerTableMap::COL_ALIGNMENT);
            $criteria->addSelectColumn(SiteBannerTableMap::COL_SLOGAN);
            $criteria->addSelectColumn(SiteBannerTableMap::COL_SUB_TEXT);
            $criteria->addSelectColumn(SiteBannerTableMap::COL_HAS_INFO_BUTTON);
            $criteria->addSelectColumn(SiteBannerTableMap::COL_INFO_LINK);
            $criteria->addSelectColumn(SiteBannerTableMap::COL_HAS_BUY_NOW);
            $criteria->addSelectColumn(SiteBannerTableMap::COL_BUY_NOW_LINK);
            $criteria->addSelectColumn(SiteBannerTableMap::COL_FILE_EXT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.site_id');
            $criteria->addSelectColumn($alias . '.about_txt');
            $criteria->addSelectColumn($alias . '.alignment');
            $criteria->addSelectColumn($alias . '.slogan');
            $criteria->addSelectColumn($alias . '.sub_text');
            $criteria->addSelectColumn($alias . '.has_info_button');
            $criteria->addSelectColumn($alias . '.info_link');
            $criteria->addSelectColumn($alias . '.has_buy_now');
            $criteria->addSelectColumn($alias . '.buy_now_link');
            $criteria->addSelectColumn($alias . '.file_ext');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SiteBannerTableMap::DATABASE_NAME)->getTable(SiteBannerTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SiteBannerTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SiteBannerTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SiteBannerTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SiteBanner or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SiteBanner object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteBannerTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Cms\SiteBanner) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SiteBannerTableMap::DATABASE_NAME);
            $criteria->add(SiteBannerTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SiteBannerQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SiteBannerTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SiteBannerTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the site_banner table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SiteBannerQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SiteBanner or Criteria object.
     *
     * @param mixed               $criteria Criteria or SiteBanner object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteBannerTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SiteBanner object
        }

        if ($criteria->containsKey(SiteBannerTableMap::COL_ID) && $criteria->keyContainsValue(SiteBannerTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SiteBannerTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SiteBannerQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SiteBannerTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SiteBannerTableMap::buildTableMap();
