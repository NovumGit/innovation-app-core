<?php

namespace Model\Cms\Map;

use Model\Cms\SitePage;
use Model\Cms\SitePageQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'site_page' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class SitePageTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Cms.Map.SitePageTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'site_page';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Cms\\SitePage';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Cms.SitePage';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 33;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 33;

    /**
     * the column name for the id field
     */
    const COL_ID = 'site_page.id';

    /**
     * the column name for the old_id field
     */
    const COL_OLD_ID = 'site_page.old_id';

    /**
     * the column name for the language_id field
     */
    const COL_LANGUAGE_ID = 'site_page.language_id';

    /**
     * the column name for the navigation_id field
     */
    const COL_NAVIGATION_ID = 'site_page.navigation_id';

    /**
     * the column name for the site_id field
     */
    const COL_SITE_ID = 'site_page.site_id';

    /**
     * the column name for the tag field
     */
    const COL_TAG = 'site_page.tag';

    /**
     * the column name for the url field
     */
    const COL_URL = 'site_page.url';

    /**
     * the column name for the has_changeable_url field
     */
    const COL_HAS_CHANGEABLE_URL = 'site_page.has_changeable_url';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'site_page.title';

    /**
     * the column name for the title_color field
     */
    const COL_TITLE_COLOR = 'site_page.title_color';

    /**
     * the column name for the title_background_color field
     */
    const COL_TITLE_BACKGROUND_COLOR = 'site_page.title_background_color';

    /**
     * the column name for the meta_description field
     */
    const COL_META_DESCRIPTION = 'site_page.meta_description';

    /**
     * the column name for the meta_keywords field
     */
    const COL_META_KEYWORDS = 'site_page.meta_keywords';

    /**
     * the column name for the content field
     */
    const COL_CONTENT = 'site_page.content';

    /**
     * the column name for the about field
     */
    const COL_ABOUT = 'site_page.about';

    /**
     * the column name for the min_pictures field
     */
    const COL_MIN_PICTURES = 'site_page.min_pictures';

    /**
     * the column name for the max_pictures field
     */
    const COL_MAX_PICTURES = 'site_page.max_pictures';

    /**
     * the column name for the title_pos_x field
     */
    const COL_TITLE_POS_X = 'site_page.title_pos_x';

    /**
     * the column name for the title_pos_y field
     */
    const COL_TITLE_POS_Y = 'site_page.title_pos_y';

    /**
     * the column name for the has_title_color_editor field
     */
    const COL_HAS_TITLE_COLOR_EDITOR = 'site_page.has_title_color_editor';

    /**
     * the column name for the has_title_backgroundcolor_editor field
     */
    const COL_HAS_TITLE_BACKGROUNDCOLOR_EDITOR = 'site_page.has_title_backgroundcolor_editor';

    /**
     * the column name for the has_navigation field
     */
    const COL_HAS_NAVIGATION = 'site_page.has_navigation';

    /**
     * the column name for the has_title field
     */
    const COL_HAS_TITLE = 'site_page.has_title';

    /**
     * the column name for the has_title_positioning_fields field
     */
    const COL_HAS_TITLE_POSITIONING_FIELDS = 'site_page.has_title_positioning_fields';

    /**
     * the column name for the has_content field
     */
    const COL_HAS_CONTENT = 'site_page.has_content';

    /**
     * the column name for the has_url field
     */
    const COL_HAS_URL = 'site_page.has_url';

    /**
     * the column name for the has_meta_description field
     */
    const COL_HAS_META_DESCRIPTION = 'site_page.has_meta_description';

    /**
     * the column name for the has_meta_keywords field
     */
    const COL_HAS_META_KEYWORDS = 'site_page.has_meta_keywords';

    /**
     * the column name for the is_deletable field
     */
    const COL_IS_DELETABLE = 'site_page.is_deletable';

    /**
     * the column name for the is_editable field
     */
    const COL_IS_EDITABLE = 'site_page.is_editable';

    /**
     * the column name for the is_visible field
     */
    const COL_IS_VISIBLE = 'site_page.is_visible';

    /**
     * the column name for the has_cover_image field
     */
    const COL_HAS_COVER_IMAGE = 'site_page.has_cover_image';

    /**
     * the column name for the cover_image_ext field
     */
    const COL_COVER_IMAGE_EXT = 'site_page.cover_image_ext';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'OldId', 'LanguageId', 'NavigationId', 'SiteId', 'Tag', 'Url', 'HasChangeableUrl', 'Title', 'TitleColor', 'TitleBackgroundColor', 'MetaDescription', 'MetaKeywords', 'Content', 'About', 'MinPictures', 'MaxPictures', 'TitlePosX', 'TitlePosY', 'HasTitleColorEditor', 'HasTitleBackgroundcolorEditor', 'HasNavigation', 'HasTitle', 'HasTitlePositioningFields', 'HasContent', 'HasUrl', 'HasMetaDescription', 'HasMetaKeywords', 'IsDeletable', 'IsEditable', 'IsVisible', 'HasCoverImage', 'CoverImageExt', ),
        self::TYPE_CAMELNAME     => array('id', 'oldId', 'languageId', 'navigationId', 'siteId', 'tag', 'url', 'hasChangeableUrl', 'title', 'titleColor', 'titleBackgroundColor', 'metaDescription', 'metaKeywords', 'content', 'about', 'minPictures', 'maxPictures', 'titlePosX', 'titlePosY', 'hasTitleColorEditor', 'hasTitleBackgroundcolorEditor', 'hasNavigation', 'hasTitle', 'hasTitlePositioningFields', 'hasContent', 'hasUrl', 'hasMetaDescription', 'hasMetaKeywords', 'isDeletable', 'isEditable', 'isVisible', 'hasCoverImage', 'coverImageExt', ),
        self::TYPE_COLNAME       => array(SitePageTableMap::COL_ID, SitePageTableMap::COL_OLD_ID, SitePageTableMap::COL_LANGUAGE_ID, SitePageTableMap::COL_NAVIGATION_ID, SitePageTableMap::COL_SITE_ID, SitePageTableMap::COL_TAG, SitePageTableMap::COL_URL, SitePageTableMap::COL_HAS_CHANGEABLE_URL, SitePageTableMap::COL_TITLE, SitePageTableMap::COL_TITLE_COLOR, SitePageTableMap::COL_TITLE_BACKGROUND_COLOR, SitePageTableMap::COL_META_DESCRIPTION, SitePageTableMap::COL_META_KEYWORDS, SitePageTableMap::COL_CONTENT, SitePageTableMap::COL_ABOUT, SitePageTableMap::COL_MIN_PICTURES, SitePageTableMap::COL_MAX_PICTURES, SitePageTableMap::COL_TITLE_POS_X, SitePageTableMap::COL_TITLE_POS_Y, SitePageTableMap::COL_HAS_TITLE_COLOR_EDITOR, SitePageTableMap::COL_HAS_TITLE_BACKGROUNDCOLOR_EDITOR, SitePageTableMap::COL_HAS_NAVIGATION, SitePageTableMap::COL_HAS_TITLE, SitePageTableMap::COL_HAS_TITLE_POSITIONING_FIELDS, SitePageTableMap::COL_HAS_CONTENT, SitePageTableMap::COL_HAS_URL, SitePageTableMap::COL_HAS_META_DESCRIPTION, SitePageTableMap::COL_HAS_META_KEYWORDS, SitePageTableMap::COL_IS_DELETABLE, SitePageTableMap::COL_IS_EDITABLE, SitePageTableMap::COL_IS_VISIBLE, SitePageTableMap::COL_HAS_COVER_IMAGE, SitePageTableMap::COL_COVER_IMAGE_EXT, ),
        self::TYPE_FIELDNAME     => array('id', 'old_id', 'language_id', 'navigation_id', 'site_id', 'tag', 'url', 'has_changeable_url', 'title', 'title_color', 'title_background_color', 'meta_description', 'meta_keywords', 'content', 'about', 'min_pictures', 'max_pictures', 'title_pos_x', 'title_pos_y', 'has_title_color_editor', 'has_title_backgroundcolor_editor', 'has_navigation', 'has_title', 'has_title_positioning_fields', 'has_content', 'has_url', 'has_meta_description', 'has_meta_keywords', 'is_deletable', 'is_editable', 'is_visible', 'has_cover_image', 'cover_image_ext', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'OldId' => 1, 'LanguageId' => 2, 'NavigationId' => 3, 'SiteId' => 4, 'Tag' => 5, 'Url' => 6, 'HasChangeableUrl' => 7, 'Title' => 8, 'TitleColor' => 9, 'TitleBackgroundColor' => 10, 'MetaDescription' => 11, 'MetaKeywords' => 12, 'Content' => 13, 'About' => 14, 'MinPictures' => 15, 'MaxPictures' => 16, 'TitlePosX' => 17, 'TitlePosY' => 18, 'HasTitleColorEditor' => 19, 'HasTitleBackgroundcolorEditor' => 20, 'HasNavigation' => 21, 'HasTitle' => 22, 'HasTitlePositioningFields' => 23, 'HasContent' => 24, 'HasUrl' => 25, 'HasMetaDescription' => 26, 'HasMetaKeywords' => 27, 'IsDeletable' => 28, 'IsEditable' => 29, 'IsVisible' => 30, 'HasCoverImage' => 31, 'CoverImageExt' => 32, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'oldId' => 1, 'languageId' => 2, 'navigationId' => 3, 'siteId' => 4, 'tag' => 5, 'url' => 6, 'hasChangeableUrl' => 7, 'title' => 8, 'titleColor' => 9, 'titleBackgroundColor' => 10, 'metaDescription' => 11, 'metaKeywords' => 12, 'content' => 13, 'about' => 14, 'minPictures' => 15, 'maxPictures' => 16, 'titlePosX' => 17, 'titlePosY' => 18, 'hasTitleColorEditor' => 19, 'hasTitleBackgroundcolorEditor' => 20, 'hasNavigation' => 21, 'hasTitle' => 22, 'hasTitlePositioningFields' => 23, 'hasContent' => 24, 'hasUrl' => 25, 'hasMetaDescription' => 26, 'hasMetaKeywords' => 27, 'isDeletable' => 28, 'isEditable' => 29, 'isVisible' => 30, 'hasCoverImage' => 31, 'coverImageExt' => 32, ),
        self::TYPE_COLNAME       => array(SitePageTableMap::COL_ID => 0, SitePageTableMap::COL_OLD_ID => 1, SitePageTableMap::COL_LANGUAGE_ID => 2, SitePageTableMap::COL_NAVIGATION_ID => 3, SitePageTableMap::COL_SITE_ID => 4, SitePageTableMap::COL_TAG => 5, SitePageTableMap::COL_URL => 6, SitePageTableMap::COL_HAS_CHANGEABLE_URL => 7, SitePageTableMap::COL_TITLE => 8, SitePageTableMap::COL_TITLE_COLOR => 9, SitePageTableMap::COL_TITLE_BACKGROUND_COLOR => 10, SitePageTableMap::COL_META_DESCRIPTION => 11, SitePageTableMap::COL_META_KEYWORDS => 12, SitePageTableMap::COL_CONTENT => 13, SitePageTableMap::COL_ABOUT => 14, SitePageTableMap::COL_MIN_PICTURES => 15, SitePageTableMap::COL_MAX_PICTURES => 16, SitePageTableMap::COL_TITLE_POS_X => 17, SitePageTableMap::COL_TITLE_POS_Y => 18, SitePageTableMap::COL_HAS_TITLE_COLOR_EDITOR => 19, SitePageTableMap::COL_HAS_TITLE_BACKGROUNDCOLOR_EDITOR => 20, SitePageTableMap::COL_HAS_NAVIGATION => 21, SitePageTableMap::COL_HAS_TITLE => 22, SitePageTableMap::COL_HAS_TITLE_POSITIONING_FIELDS => 23, SitePageTableMap::COL_HAS_CONTENT => 24, SitePageTableMap::COL_HAS_URL => 25, SitePageTableMap::COL_HAS_META_DESCRIPTION => 26, SitePageTableMap::COL_HAS_META_KEYWORDS => 27, SitePageTableMap::COL_IS_DELETABLE => 28, SitePageTableMap::COL_IS_EDITABLE => 29, SitePageTableMap::COL_IS_VISIBLE => 30, SitePageTableMap::COL_HAS_COVER_IMAGE => 31, SitePageTableMap::COL_COVER_IMAGE_EXT => 32, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'old_id' => 1, 'language_id' => 2, 'navigation_id' => 3, 'site_id' => 4, 'tag' => 5, 'url' => 6, 'has_changeable_url' => 7, 'title' => 8, 'title_color' => 9, 'title_background_color' => 10, 'meta_description' => 11, 'meta_keywords' => 12, 'content' => 13, 'about' => 14, 'min_pictures' => 15, 'max_pictures' => 16, 'title_pos_x' => 17, 'title_pos_y' => 18, 'has_title_color_editor' => 19, 'has_title_backgroundcolor_editor' => 20, 'has_navigation' => 21, 'has_title' => 22, 'has_title_positioning_fields' => 23, 'has_content' => 24, 'has_url' => 25, 'has_meta_description' => 26, 'has_meta_keywords' => 27, 'is_deletable' => 28, 'is_editable' => 29, 'is_visible' => 30, 'has_cover_image' => 31, 'cover_image_ext' => 32, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('site_page');
        $this->setPhpName('SitePage');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Cms\\SitePage');
        $this->setPackage('Model.Cms');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('old_id', 'OldId', 'VARCHAR', false, 255, null);
        $this->addForeignKey('language_id', 'LanguageId', 'INTEGER', 'mt_language', 'id', false, null, null);
        $this->addForeignKey('navigation_id', 'NavigationId', 'INTEGER', 'site_navigation', 'id', false, null, null);
        $this->addForeignKey('site_id', 'SiteId', 'INTEGER', 'site', 'id', true, null, null);
        $this->addColumn('tag', 'Tag', 'VARCHAR', false, 255, null);
        $this->addColumn('url', 'Url', 'VARCHAR', false, 255, null);
        $this->addColumn('has_changeable_url', 'HasChangeableUrl', 'BOOLEAN', false, 1, true);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 255, null);
        $this->addColumn('title_color', 'TitleColor', 'VARCHAR', false, 255, null);
        $this->addColumn('title_background_color', 'TitleBackgroundColor', 'VARCHAR', false, 255, null);
        $this->addColumn('meta_description', 'MetaDescription', 'VARCHAR', false, 255, null);
        $this->addColumn('meta_keywords', 'MetaKeywords', 'VARCHAR', false, 255, null);
        $this->addColumn('content', 'Content', 'LONGVARCHAR', false, null, null);
        $this->addColumn('about', 'About', 'VARCHAR', false, 255, null);
        $this->addColumn('min_pictures', 'MinPictures', 'INTEGER', true, null, 0);
        $this->addColumn('max_pictures', 'MaxPictures', 'INTEGER', true, null, 0);
        $this->addColumn('title_pos_x', 'TitlePosX', 'INTEGER', true, null, 0);
        $this->addColumn('title_pos_y', 'TitlePosY', 'INTEGER', true, null, 0);
        $this->addColumn('has_title_color_editor', 'HasTitleColorEditor', 'BOOLEAN', true, 1, true);
        $this->addColumn('has_title_backgroundcolor_editor', 'HasTitleBackgroundcolorEditor', 'BOOLEAN', true, 1, true);
        $this->addColumn('has_navigation', 'HasNavigation', 'BOOLEAN', true, 1, true);
        $this->addColumn('has_title', 'HasTitle', 'BOOLEAN', true, 1, true);
        $this->addColumn('has_title_positioning_fields', 'HasTitlePositioningFields', 'BOOLEAN', true, 1, false);
        $this->addColumn('has_content', 'HasContent', 'BOOLEAN', true, 1, true);
        $this->addColumn('has_url', 'HasUrl', 'BOOLEAN', true, 1, true);
        $this->addColumn('has_meta_description', 'HasMetaDescription', 'BOOLEAN', true, 1, true);
        $this->addColumn('has_meta_keywords', 'HasMetaKeywords', 'BOOLEAN', true, 1, true);
        $this->addColumn('is_deletable', 'IsDeletable', 'BOOLEAN', true, 1, true);
        $this->addColumn('is_editable', 'IsEditable', 'BOOLEAN', true, 1, true);
        $this->addColumn('is_visible', 'IsVisible', 'BOOLEAN', true, 1, true);
        $this->addColumn('has_cover_image', 'HasCoverImage', 'BOOLEAN', false, 1, null);
        $this->addColumn('cover_image_ext', 'CoverImageExt', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Site', '\\Model\\Cms\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':site_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('SiteNavigaton', '\\Model\\Cms\\Site_navigation', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':navigation_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('Language', '\\Model\\Setting\\MasterTable\\Language', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('SiteFooterBlockTranslationSitePage', '\\Model\\Cms\\SiteFooterBlockTranslationSitePage', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':site_page_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'SiteFooterBlockTranslationSitePages', false);
        $this->addRelation('SitePagePicture', '\\Model\\Cms\\SitePagePicture', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':site_page_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'SitePagePictures', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '3600', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to site_page     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        SiteFooterBlockTranslationSitePageTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SitePageTableMap::CLASS_DEFAULT : SitePageTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SitePage object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SitePageTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SitePageTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SitePageTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SitePageTableMap::OM_CLASS;
            /** @var SitePage $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SitePageTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SitePageTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SitePageTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SitePage $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SitePageTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SitePageTableMap::COL_ID);
            $criteria->addSelectColumn(SitePageTableMap::COL_OLD_ID);
            $criteria->addSelectColumn(SitePageTableMap::COL_LANGUAGE_ID);
            $criteria->addSelectColumn(SitePageTableMap::COL_NAVIGATION_ID);
            $criteria->addSelectColumn(SitePageTableMap::COL_SITE_ID);
            $criteria->addSelectColumn(SitePageTableMap::COL_TAG);
            $criteria->addSelectColumn(SitePageTableMap::COL_URL);
            $criteria->addSelectColumn(SitePageTableMap::COL_HAS_CHANGEABLE_URL);
            $criteria->addSelectColumn(SitePageTableMap::COL_TITLE);
            $criteria->addSelectColumn(SitePageTableMap::COL_TITLE_COLOR);
            $criteria->addSelectColumn(SitePageTableMap::COL_TITLE_BACKGROUND_COLOR);
            $criteria->addSelectColumn(SitePageTableMap::COL_META_DESCRIPTION);
            $criteria->addSelectColumn(SitePageTableMap::COL_META_KEYWORDS);
            $criteria->addSelectColumn(SitePageTableMap::COL_CONTENT);
            $criteria->addSelectColumn(SitePageTableMap::COL_ABOUT);
            $criteria->addSelectColumn(SitePageTableMap::COL_MIN_PICTURES);
            $criteria->addSelectColumn(SitePageTableMap::COL_MAX_PICTURES);
            $criteria->addSelectColumn(SitePageTableMap::COL_TITLE_POS_X);
            $criteria->addSelectColumn(SitePageTableMap::COL_TITLE_POS_Y);
            $criteria->addSelectColumn(SitePageTableMap::COL_HAS_TITLE_COLOR_EDITOR);
            $criteria->addSelectColumn(SitePageTableMap::COL_HAS_TITLE_BACKGROUNDCOLOR_EDITOR);
            $criteria->addSelectColumn(SitePageTableMap::COL_HAS_NAVIGATION);
            $criteria->addSelectColumn(SitePageTableMap::COL_HAS_TITLE);
            $criteria->addSelectColumn(SitePageTableMap::COL_HAS_TITLE_POSITIONING_FIELDS);
            $criteria->addSelectColumn(SitePageTableMap::COL_HAS_CONTENT);
            $criteria->addSelectColumn(SitePageTableMap::COL_HAS_URL);
            $criteria->addSelectColumn(SitePageTableMap::COL_HAS_META_DESCRIPTION);
            $criteria->addSelectColumn(SitePageTableMap::COL_HAS_META_KEYWORDS);
            $criteria->addSelectColumn(SitePageTableMap::COL_IS_DELETABLE);
            $criteria->addSelectColumn(SitePageTableMap::COL_IS_EDITABLE);
            $criteria->addSelectColumn(SitePageTableMap::COL_IS_VISIBLE);
            $criteria->addSelectColumn(SitePageTableMap::COL_HAS_COVER_IMAGE);
            $criteria->addSelectColumn(SitePageTableMap::COL_COVER_IMAGE_EXT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.old_id');
            $criteria->addSelectColumn($alias . '.language_id');
            $criteria->addSelectColumn($alias . '.navigation_id');
            $criteria->addSelectColumn($alias . '.site_id');
            $criteria->addSelectColumn($alias . '.tag');
            $criteria->addSelectColumn($alias . '.url');
            $criteria->addSelectColumn($alias . '.has_changeable_url');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.title_color');
            $criteria->addSelectColumn($alias . '.title_background_color');
            $criteria->addSelectColumn($alias . '.meta_description');
            $criteria->addSelectColumn($alias . '.meta_keywords');
            $criteria->addSelectColumn($alias . '.content');
            $criteria->addSelectColumn($alias . '.about');
            $criteria->addSelectColumn($alias . '.min_pictures');
            $criteria->addSelectColumn($alias . '.max_pictures');
            $criteria->addSelectColumn($alias . '.title_pos_x');
            $criteria->addSelectColumn($alias . '.title_pos_y');
            $criteria->addSelectColumn($alias . '.has_title_color_editor');
            $criteria->addSelectColumn($alias . '.has_title_backgroundcolor_editor');
            $criteria->addSelectColumn($alias . '.has_navigation');
            $criteria->addSelectColumn($alias . '.has_title');
            $criteria->addSelectColumn($alias . '.has_title_positioning_fields');
            $criteria->addSelectColumn($alias . '.has_content');
            $criteria->addSelectColumn($alias . '.has_url');
            $criteria->addSelectColumn($alias . '.has_meta_description');
            $criteria->addSelectColumn($alias . '.has_meta_keywords');
            $criteria->addSelectColumn($alias . '.is_deletable');
            $criteria->addSelectColumn($alias . '.is_editable');
            $criteria->addSelectColumn($alias . '.is_visible');
            $criteria->addSelectColumn($alias . '.has_cover_image');
            $criteria->addSelectColumn($alias . '.cover_image_ext');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SitePageTableMap::DATABASE_NAME)->getTable(SitePageTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SitePageTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SitePageTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SitePageTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SitePage or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SitePage object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SitePageTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Cms\SitePage) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SitePageTableMap::DATABASE_NAME);
            $criteria->add(SitePageTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SitePageQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SitePageTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SitePageTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the site_page table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SitePageQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SitePage or Criteria object.
     *
     * @param mixed               $criteria Criteria or SitePage object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SitePageTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SitePage object
        }

        if ($criteria->containsKey(SitePageTableMap::COL_ID) && $criteria->keyContainsValue(SitePageTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SitePageTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SitePageQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SitePageTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SitePageTableMap::buildTableMap();
