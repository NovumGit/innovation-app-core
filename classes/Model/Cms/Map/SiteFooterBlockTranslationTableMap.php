<?php

namespace Model\Cms\Map;

use Model\Cms\SiteFooterBlockTranslation;
use Model\Cms\SiteFooterBlockTranslationQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'site_footer_block_translation' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class SiteFooterBlockTranslationTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Cms.Map.SiteFooterBlockTranslationTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'site_footer_block_translation';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Cms\\SiteFooterBlockTranslation';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Cms.SiteFooterBlockTranslation';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 5;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 5;

    /**
     * the column name for the id field
     */
    const COL_ID = 'site_footer_block_translation.id';

    /**
     * the column name for the site_footer_block_id field
     */
    const COL_SITE_FOOTER_BLOCK_ID = 'site_footer_block_translation.site_footer_block_id';

    /**
     * the column name for the language_id field
     */
    const COL_LANGUAGE_ID = 'site_footer_block_translation.language_id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'site_footer_block_translation.title';

    /**
     * the column name for the html_contents field
     */
    const COL_HTML_CONTENTS = 'site_footer_block_translation.html_contents';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'SiteFooterBlockId', 'LanguageId', 'Title', 'HtmlContents', ),
        self::TYPE_CAMELNAME     => array('id', 'siteFooterBlockId', 'languageId', 'title', 'htmlContents', ),
        self::TYPE_COLNAME       => array(SiteFooterBlockTranslationTableMap::COL_ID, SiteFooterBlockTranslationTableMap::COL_SITE_FOOTER_BLOCK_ID, SiteFooterBlockTranslationTableMap::COL_LANGUAGE_ID, SiteFooterBlockTranslationTableMap::COL_TITLE, SiteFooterBlockTranslationTableMap::COL_HTML_CONTENTS, ),
        self::TYPE_FIELDNAME     => array('id', 'site_footer_block_id', 'language_id', 'title', 'html_contents', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'SiteFooterBlockId' => 1, 'LanguageId' => 2, 'Title' => 3, 'HtmlContents' => 4, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'siteFooterBlockId' => 1, 'languageId' => 2, 'title' => 3, 'htmlContents' => 4, ),
        self::TYPE_COLNAME       => array(SiteFooterBlockTranslationTableMap::COL_ID => 0, SiteFooterBlockTranslationTableMap::COL_SITE_FOOTER_BLOCK_ID => 1, SiteFooterBlockTranslationTableMap::COL_LANGUAGE_ID => 2, SiteFooterBlockTranslationTableMap::COL_TITLE => 3, SiteFooterBlockTranslationTableMap::COL_HTML_CONTENTS => 4, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'site_footer_block_id' => 1, 'language_id' => 2, 'title' => 3, 'html_contents' => 4, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('site_footer_block_translation');
        $this->setPhpName('SiteFooterBlockTranslation');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Cms\\SiteFooterBlockTranslation');
        $this->setPackage('Model.Cms');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('site_footer_block_id', 'SiteFooterBlockId', 'INTEGER', 'site_footer_block', 'id', true, null, null);
        $this->addForeignKey('language_id', 'LanguageId', 'INTEGER', 'mt_language', 'id', true, null, null);
        $this->addColumn('title', 'Title', 'LONGVARCHAR', false, null, null);
        $this->addColumn('html_contents', 'HtmlContents', 'LONGVARCHAR', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Language', '\\Model\\Setting\\MasterTable\\Language', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('SiteFooterBlock', '\\Model\\Cms\\SiteFooterBlock', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':site_footer_block_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('SiteFooterBlockTranslationSitePage', '\\Model\\Cms\\SiteFooterBlockTranslationSitePage', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':site_footer_block_translation_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'SiteFooterBlockTranslationSitePages', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '3600', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to site_footer_block_translation     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        SiteFooterBlockTranslationSitePageTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SiteFooterBlockTranslationTableMap::CLASS_DEFAULT : SiteFooterBlockTranslationTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SiteFooterBlockTranslation object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SiteFooterBlockTranslationTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SiteFooterBlockTranslationTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SiteFooterBlockTranslationTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SiteFooterBlockTranslationTableMap::OM_CLASS;
            /** @var SiteFooterBlockTranslation $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SiteFooterBlockTranslationTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SiteFooterBlockTranslationTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SiteFooterBlockTranslationTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SiteFooterBlockTranslation $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SiteFooterBlockTranslationTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SiteFooterBlockTranslationTableMap::COL_ID);
            $criteria->addSelectColumn(SiteFooterBlockTranslationTableMap::COL_SITE_FOOTER_BLOCK_ID);
            $criteria->addSelectColumn(SiteFooterBlockTranslationTableMap::COL_LANGUAGE_ID);
            $criteria->addSelectColumn(SiteFooterBlockTranslationTableMap::COL_TITLE);
            $criteria->addSelectColumn(SiteFooterBlockTranslationTableMap::COL_HTML_CONTENTS);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.site_footer_block_id');
            $criteria->addSelectColumn($alias . '.language_id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.html_contents');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SiteFooterBlockTranslationTableMap::DATABASE_NAME)->getTable(SiteFooterBlockTranslationTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SiteFooterBlockTranslationTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SiteFooterBlockTranslationTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SiteFooterBlockTranslationTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SiteFooterBlockTranslation or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SiteFooterBlockTranslation object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteFooterBlockTranslationTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Cms\SiteFooterBlockTranslation) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SiteFooterBlockTranslationTableMap::DATABASE_NAME);
            $criteria->add(SiteFooterBlockTranslationTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SiteFooterBlockTranslationQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SiteFooterBlockTranslationTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SiteFooterBlockTranslationTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the site_footer_block_translation table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SiteFooterBlockTranslationQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SiteFooterBlockTranslation or Criteria object.
     *
     * @param mixed               $criteria Criteria or SiteFooterBlockTranslation object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteFooterBlockTranslationTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SiteFooterBlockTranslation object
        }

        if ($criteria->containsKey(SiteFooterBlockTranslationTableMap::COL_ID) && $criteria->keyContainsValue(SiteFooterBlockTranslationTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SiteFooterBlockTranslationTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SiteFooterBlockTranslationQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SiteFooterBlockTranslationTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SiteFooterBlockTranslationTableMap::buildTableMap();
