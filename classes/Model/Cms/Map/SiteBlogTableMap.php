<?php

namespace Model\Cms\Map;

use Model\Cms\SiteBlog;
use Model\Cms\SiteBlogQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'site_blog' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class SiteBlogTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Cms.Map.SiteBlogTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'site_blog';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Cms\\SiteBlog';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Cms.SiteBlog';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 13;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 13;

    /**
     * the column name for the id field
     */
    const COL_ID = 'site_blog.id';

    /**
     * the column name for the site_id field
     */
    const COL_SITE_ID = 'site_blog.site_id';

    /**
     * the column name for the language_id field
     */
    const COL_LANGUAGE_ID = 'site_blog.language_id';

    /**
     * the column name for the created_on field
     */
    const COL_CREATED_ON = 'site_blog.created_on';

    /**
     * the column name for the created_by_user_id field
     */
    const COL_CREATED_BY_USER_ID = 'site_blog.created_by_user_id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'site_blog.title';

    /**
     * the column name for the content field
     */
    const COL_CONTENT = 'site_blog.content';

    /**
     * the column name for the teaser field
     */
    const COL_TEASER = 'site_blog.teaser';

    /**
     * the column name for the tag field
     */
    const COL_TAG = 'site_blog.tag';

    /**
     * the column name for the file_original_name field
     */
    const COL_FILE_ORIGINAL_NAME = 'site_blog.file_original_name';

    /**
     * the column name for the file_ext field
     */
    const COL_FILE_EXT = 'site_blog.file_ext';

    /**
     * the column name for the file_size field
     */
    const COL_FILE_SIZE = 'site_blog.file_size';

    /**
     * the column name for the file_mime field
     */
    const COL_FILE_MIME = 'site_blog.file_mime';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'SiteId', 'LanguageId', 'CreatedOn', 'CreatedByUserId', 'Title', 'Content', 'Teaser', 'Tag', 'FileOriginalName', 'FileExt', 'FileSize', 'FileMime', ),
        self::TYPE_CAMELNAME     => array('id', 'siteId', 'languageId', 'createdOn', 'createdByUserId', 'title', 'content', 'teaser', 'tag', 'fileOriginalName', 'fileExt', 'fileSize', 'fileMime', ),
        self::TYPE_COLNAME       => array(SiteBlogTableMap::COL_ID, SiteBlogTableMap::COL_SITE_ID, SiteBlogTableMap::COL_LANGUAGE_ID, SiteBlogTableMap::COL_CREATED_ON, SiteBlogTableMap::COL_CREATED_BY_USER_ID, SiteBlogTableMap::COL_TITLE, SiteBlogTableMap::COL_CONTENT, SiteBlogTableMap::COL_TEASER, SiteBlogTableMap::COL_TAG, SiteBlogTableMap::COL_FILE_ORIGINAL_NAME, SiteBlogTableMap::COL_FILE_EXT, SiteBlogTableMap::COL_FILE_SIZE, SiteBlogTableMap::COL_FILE_MIME, ),
        self::TYPE_FIELDNAME     => array('id', 'site_id', 'language_id', 'created_on', 'created_by_user_id', 'title', 'content', 'teaser', 'tag', 'file_original_name', 'file_ext', 'file_size', 'file_mime', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'SiteId' => 1, 'LanguageId' => 2, 'CreatedOn' => 3, 'CreatedByUserId' => 4, 'Title' => 5, 'Content' => 6, 'Teaser' => 7, 'Tag' => 8, 'FileOriginalName' => 9, 'FileExt' => 10, 'FileSize' => 11, 'FileMime' => 12, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'siteId' => 1, 'languageId' => 2, 'createdOn' => 3, 'createdByUserId' => 4, 'title' => 5, 'content' => 6, 'teaser' => 7, 'tag' => 8, 'fileOriginalName' => 9, 'fileExt' => 10, 'fileSize' => 11, 'fileMime' => 12, ),
        self::TYPE_COLNAME       => array(SiteBlogTableMap::COL_ID => 0, SiteBlogTableMap::COL_SITE_ID => 1, SiteBlogTableMap::COL_LANGUAGE_ID => 2, SiteBlogTableMap::COL_CREATED_ON => 3, SiteBlogTableMap::COL_CREATED_BY_USER_ID => 4, SiteBlogTableMap::COL_TITLE => 5, SiteBlogTableMap::COL_CONTENT => 6, SiteBlogTableMap::COL_TEASER => 7, SiteBlogTableMap::COL_TAG => 8, SiteBlogTableMap::COL_FILE_ORIGINAL_NAME => 9, SiteBlogTableMap::COL_FILE_EXT => 10, SiteBlogTableMap::COL_FILE_SIZE => 11, SiteBlogTableMap::COL_FILE_MIME => 12, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'site_id' => 1, 'language_id' => 2, 'created_on' => 3, 'created_by_user_id' => 4, 'title' => 5, 'content' => 6, 'teaser' => 7, 'tag' => 8, 'file_original_name' => 9, 'file_ext' => 10, 'file_size' => 11, 'file_mime' => 12, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('site_blog');
        $this->setPhpName('SiteBlog');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Cms\\SiteBlog');
        $this->setPackage('Model.Cms');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('site_id', 'SiteId', 'INTEGER', 'site', 'id', true, null, null);
        $this->addForeignKey('language_id', 'LanguageId', 'INTEGER', 'mt_language', 'id', true, null, null);
        $this->addColumn('created_on', 'CreatedOn', 'TIMESTAMP', true, null, null);
        $this->addForeignKey('created_by_user_id', 'CreatedByUserId', 'INTEGER', 'user', 'id', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', true, 255, null);
        $this->addColumn('content', 'Content', 'LONGVARCHAR', false, null, null);
        $this->addColumn('teaser', 'Teaser', 'VARCHAR', false, 255, null);
        $this->addColumn('tag', 'Tag', 'VARCHAR', false, 255, null);
        $this->addColumn('file_original_name', 'FileOriginalName', 'VARCHAR', false, 255, null);
        $this->addColumn('file_ext', 'FileExt', 'VARCHAR', false, 255, null);
        $this->addColumn('file_size', 'FileSize', 'INTEGER', false, null, null);
        $this->addColumn('file_mime', 'FileMime', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Language', '\\Model\\Setting\\MasterTable\\Language', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Site', '\\Model\\Cms\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':site_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('User', '\\Model\\Account\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':created_by_user_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SiteBlogTableMap::CLASS_DEFAULT : SiteBlogTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SiteBlog object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SiteBlogTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SiteBlogTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SiteBlogTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SiteBlogTableMap::OM_CLASS;
            /** @var SiteBlog $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SiteBlogTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SiteBlogTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SiteBlogTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SiteBlog $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SiteBlogTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SiteBlogTableMap::COL_ID);
            $criteria->addSelectColumn(SiteBlogTableMap::COL_SITE_ID);
            $criteria->addSelectColumn(SiteBlogTableMap::COL_LANGUAGE_ID);
            $criteria->addSelectColumn(SiteBlogTableMap::COL_CREATED_ON);
            $criteria->addSelectColumn(SiteBlogTableMap::COL_CREATED_BY_USER_ID);
            $criteria->addSelectColumn(SiteBlogTableMap::COL_TITLE);
            $criteria->addSelectColumn(SiteBlogTableMap::COL_CONTENT);
            $criteria->addSelectColumn(SiteBlogTableMap::COL_TEASER);
            $criteria->addSelectColumn(SiteBlogTableMap::COL_TAG);
            $criteria->addSelectColumn(SiteBlogTableMap::COL_FILE_ORIGINAL_NAME);
            $criteria->addSelectColumn(SiteBlogTableMap::COL_FILE_EXT);
            $criteria->addSelectColumn(SiteBlogTableMap::COL_FILE_SIZE);
            $criteria->addSelectColumn(SiteBlogTableMap::COL_FILE_MIME);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.site_id');
            $criteria->addSelectColumn($alias . '.language_id');
            $criteria->addSelectColumn($alias . '.created_on');
            $criteria->addSelectColumn($alias . '.created_by_user_id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.content');
            $criteria->addSelectColumn($alias . '.teaser');
            $criteria->addSelectColumn($alias . '.tag');
            $criteria->addSelectColumn($alias . '.file_original_name');
            $criteria->addSelectColumn($alias . '.file_ext');
            $criteria->addSelectColumn($alias . '.file_size');
            $criteria->addSelectColumn($alias . '.file_mime');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SiteBlogTableMap::DATABASE_NAME)->getTable(SiteBlogTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SiteBlogTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SiteBlogTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SiteBlogTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SiteBlog or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SiteBlog object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteBlogTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Cms\SiteBlog) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SiteBlogTableMap::DATABASE_NAME);
            $criteria->add(SiteBlogTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SiteBlogQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SiteBlogTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SiteBlogTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the site_blog table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SiteBlogQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SiteBlog or Criteria object.
     *
     * @param mixed               $criteria Criteria or SiteBlog object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteBlogTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SiteBlog object
        }

        if ($criteria->containsKey(SiteBlogTableMap::COL_ID) && $criteria->keyContainsValue(SiteBlogTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SiteBlogTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SiteBlogQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SiteBlogTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SiteBlogTableMap::buildTableMap();
