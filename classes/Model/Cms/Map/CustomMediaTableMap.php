<?php

namespace Model\Cms\Map;

use Model\Cms\CustomMedia;
use Model\Cms\CustomMediaQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'custom_media' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class CustomMediaTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Cms.Map.CustomMediaTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'custom_media';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Cms\\CustomMedia';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Cms.CustomMedia';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the id field
     */
    const COL_ID = 'custom_media.id';

    /**
     * the column name for the parent_id field
     */
    const COL_PARENT_ID = 'custom_media.parent_id';

    /**
     * the column name for the site_id field
     */
    const COL_SITE_ID = 'custom_media.site_id';

    /**
     * the column name for the node_type_id field
     */
    const COL_NODE_TYPE_ID = 'custom_media.node_type_id';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'custom_media.name';

    /**
     * the column name for the is_deletable field
     */
    const COL_IS_DELETABLE = 'custom_media.is_deletable';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'custom_media.created_date';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ParentId', 'SiteId', 'NodeTypeId', 'Name', 'IsDeletable', 'CreatedDate', ),
        self::TYPE_CAMELNAME     => array('id', 'parentId', 'siteId', 'nodeTypeId', 'name', 'isDeletable', 'createdDate', ),
        self::TYPE_COLNAME       => array(CustomMediaTableMap::COL_ID, CustomMediaTableMap::COL_PARENT_ID, CustomMediaTableMap::COL_SITE_ID, CustomMediaTableMap::COL_NODE_TYPE_ID, CustomMediaTableMap::COL_NAME, CustomMediaTableMap::COL_IS_DELETABLE, CustomMediaTableMap::COL_CREATED_DATE, ),
        self::TYPE_FIELDNAME     => array('id', 'parent_id', 'site_id', 'node_type_id', 'name', 'is_deletable', 'created_date', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ParentId' => 1, 'SiteId' => 2, 'NodeTypeId' => 3, 'Name' => 4, 'IsDeletable' => 5, 'CreatedDate' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'parentId' => 1, 'siteId' => 2, 'nodeTypeId' => 3, 'name' => 4, 'isDeletable' => 5, 'createdDate' => 6, ),
        self::TYPE_COLNAME       => array(CustomMediaTableMap::COL_ID => 0, CustomMediaTableMap::COL_PARENT_ID => 1, CustomMediaTableMap::COL_SITE_ID => 2, CustomMediaTableMap::COL_NODE_TYPE_ID => 3, CustomMediaTableMap::COL_NAME => 4, CustomMediaTableMap::COL_IS_DELETABLE => 5, CustomMediaTableMap::COL_CREATED_DATE => 6, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'parent_id' => 1, 'site_id' => 2, 'node_type_id' => 3, 'name' => 4, 'is_deletable' => 5, 'created_date' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('custom_media');
        $this->setPhpName('CustomMedia');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Cms\\CustomMedia');
        $this->setPackage('Model.Cms');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('parent_id', 'ParentId', 'INTEGER', false, null, null);
        $this->addForeignKey('site_id', 'SiteId', 'INTEGER', 'site', 'id', true, null, null);
        $this->addForeignKey('node_type_id', 'NodeTypeId', 'INTEGER', 'custom_media_node_type', 'id', true, null, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('is_deletable', 'IsDeletable', 'BOOLEAN', false, 1, false);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Site', '\\Model\\Cms\\Site', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':site_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('CustomMediaNodeType', '\\Model\\Cms\\CustomMediaNodeType', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':node_type_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? CustomMediaTableMap::CLASS_DEFAULT : CustomMediaTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (CustomMedia object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = CustomMediaTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = CustomMediaTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + CustomMediaTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = CustomMediaTableMap::OM_CLASS;
            /** @var CustomMedia $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            CustomMediaTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = CustomMediaTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = CustomMediaTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var CustomMedia $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                CustomMediaTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(CustomMediaTableMap::COL_ID);
            $criteria->addSelectColumn(CustomMediaTableMap::COL_PARENT_ID);
            $criteria->addSelectColumn(CustomMediaTableMap::COL_SITE_ID);
            $criteria->addSelectColumn(CustomMediaTableMap::COL_NODE_TYPE_ID);
            $criteria->addSelectColumn(CustomMediaTableMap::COL_NAME);
            $criteria->addSelectColumn(CustomMediaTableMap::COL_IS_DELETABLE);
            $criteria->addSelectColumn(CustomMediaTableMap::COL_CREATED_DATE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.parent_id');
            $criteria->addSelectColumn($alias . '.site_id');
            $criteria->addSelectColumn($alias . '.node_type_id');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.is_deletable');
            $criteria->addSelectColumn($alias . '.created_date');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(CustomMediaTableMap::DATABASE_NAME)->getTable(CustomMediaTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(CustomMediaTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(CustomMediaTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new CustomMediaTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a CustomMedia or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or CustomMedia object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomMediaTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Cms\CustomMedia) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(CustomMediaTableMap::DATABASE_NAME);
            $criteria->add(CustomMediaTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = CustomMediaQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            CustomMediaTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                CustomMediaTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the custom_media table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return CustomMediaQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a CustomMedia or Criteria object.
     *
     * @param mixed               $criteria Criteria or CustomMedia object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomMediaTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from CustomMedia object
        }

        if ($criteria->containsKey(CustomMediaTableMap::COL_ID) && $criteria->keyContainsValue(CustomMediaTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.CustomMediaTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = CustomMediaQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // CustomMediaTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
CustomMediaTableMap::buildTableMap();
