<?php

namespace Model\Cms\Map;

use Model\Cms\SitePagePicture;
use Model\Cms\SitePagePictureQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'site_page_picture' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class SitePagePictureTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Cms.Map.SitePagePictureTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'site_page_picture';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Cms\\SitePagePicture';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Cms.SitePagePicture';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    const COL_ID = 'site_page_picture.id';

    /**
     * the column name for the old_id field
     */
    const COL_OLD_ID = 'site_page_picture.old_id';

    /**
     * the column name for the site_page_id field
     */
    const COL_SITE_PAGE_ID = 'site_page_picture.site_page_id';

    /**
     * the column name for the original_name field
     */
    const COL_ORIGINAL_NAME = 'site_page_picture.original_name';

    /**
     * the column name for the file_ext field
     */
    const COL_FILE_EXT = 'site_page_picture.file_ext';

    /**
     * the column name for the file_size field
     */
    const COL_FILE_SIZE = 'site_page_picture.file_size';

    /**
     * the column name for the file_mime field
     */
    const COL_FILE_MIME = 'site_page_picture.file_mime';

    /**
     * the column name for the sorting field
     */
    const COL_SORTING = 'site_page_picture.sorting';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'OldId', 'SitePageId', 'OriginalName', 'FileExt', 'FileSize', 'FileMime', 'Sorting', ),
        self::TYPE_CAMELNAME     => array('id', 'oldId', 'sitePageId', 'originalName', 'fileExt', 'fileSize', 'fileMime', 'sorting', ),
        self::TYPE_COLNAME       => array(SitePagePictureTableMap::COL_ID, SitePagePictureTableMap::COL_OLD_ID, SitePagePictureTableMap::COL_SITE_PAGE_ID, SitePagePictureTableMap::COL_ORIGINAL_NAME, SitePagePictureTableMap::COL_FILE_EXT, SitePagePictureTableMap::COL_FILE_SIZE, SitePagePictureTableMap::COL_FILE_MIME, SitePagePictureTableMap::COL_SORTING, ),
        self::TYPE_FIELDNAME     => array('id', 'old_id', 'site_page_id', 'original_name', 'file_ext', 'file_size', 'file_mime', 'sorting', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'OldId' => 1, 'SitePageId' => 2, 'OriginalName' => 3, 'FileExt' => 4, 'FileSize' => 5, 'FileMime' => 6, 'Sorting' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'oldId' => 1, 'sitePageId' => 2, 'originalName' => 3, 'fileExt' => 4, 'fileSize' => 5, 'fileMime' => 6, 'sorting' => 7, ),
        self::TYPE_COLNAME       => array(SitePagePictureTableMap::COL_ID => 0, SitePagePictureTableMap::COL_OLD_ID => 1, SitePagePictureTableMap::COL_SITE_PAGE_ID => 2, SitePagePictureTableMap::COL_ORIGINAL_NAME => 3, SitePagePictureTableMap::COL_FILE_EXT => 4, SitePagePictureTableMap::COL_FILE_SIZE => 5, SitePagePictureTableMap::COL_FILE_MIME => 6, SitePagePictureTableMap::COL_SORTING => 7, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'old_id' => 1, 'site_page_id' => 2, 'original_name' => 3, 'file_ext' => 4, 'file_size' => 5, 'file_mime' => 6, 'sorting' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('site_page_picture');
        $this->setPhpName('SitePagePicture');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Cms\\SitePagePicture');
        $this->setPackage('Model.Cms');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('old_id', 'OldId', 'VARCHAR', false, 255, null);
        $this->addForeignKey('site_page_id', 'SitePageId', 'INTEGER', 'site_page', 'id', true, null, null);
        $this->addColumn('original_name', 'OriginalName', 'VARCHAR', false, 255, null);
        $this->addColumn('file_ext', 'FileExt', 'VARCHAR', true, 255, null);
        $this->addColumn('file_size', 'FileSize', 'INTEGER', true, null, null);
        $this->addColumn('file_mime', 'FileMime', 'VARCHAR', true, 255, null);
        $this->addColumn('sorting', 'Sorting', 'INTEGER', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SitePage', '\\Model\\Cms\\SitePage', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':site_page_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? SitePagePictureTableMap::CLASS_DEFAULT : SitePagePictureTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (SitePagePicture object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = SitePagePictureTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = SitePagePictureTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + SitePagePictureTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = SitePagePictureTableMap::OM_CLASS;
            /** @var SitePagePicture $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            SitePagePictureTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = SitePagePictureTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = SitePagePictureTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var SitePagePicture $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                SitePagePictureTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(SitePagePictureTableMap::COL_ID);
            $criteria->addSelectColumn(SitePagePictureTableMap::COL_OLD_ID);
            $criteria->addSelectColumn(SitePagePictureTableMap::COL_SITE_PAGE_ID);
            $criteria->addSelectColumn(SitePagePictureTableMap::COL_ORIGINAL_NAME);
            $criteria->addSelectColumn(SitePagePictureTableMap::COL_FILE_EXT);
            $criteria->addSelectColumn(SitePagePictureTableMap::COL_FILE_SIZE);
            $criteria->addSelectColumn(SitePagePictureTableMap::COL_FILE_MIME);
            $criteria->addSelectColumn(SitePagePictureTableMap::COL_SORTING);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.old_id');
            $criteria->addSelectColumn($alias . '.site_page_id');
            $criteria->addSelectColumn($alias . '.original_name');
            $criteria->addSelectColumn($alias . '.file_ext');
            $criteria->addSelectColumn($alias . '.file_size');
            $criteria->addSelectColumn($alias . '.file_mime');
            $criteria->addSelectColumn($alias . '.sorting');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(SitePagePictureTableMap::DATABASE_NAME)->getTable(SitePagePictureTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SitePagePictureTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(SitePagePictureTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new SitePagePictureTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a SitePagePicture or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or SitePagePicture object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SitePagePictureTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Cms\SitePagePicture) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(SitePagePictureTableMap::DATABASE_NAME);
            $criteria->add(SitePagePictureTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = SitePagePictureQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            SitePagePictureTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                SitePagePictureTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the site_page_picture table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return SitePagePictureQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a SitePagePicture or Criteria object.
     *
     * @param mixed               $criteria Criteria or SitePagePicture object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SitePagePictureTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from SitePagePicture object
        }

        if ($criteria->containsKey(SitePagePictureTableMap::COL_ID) && $criteria->keyContainsValue(SitePagePictureTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.SitePagePictureTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = SitePagePictureQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // SitePagePictureTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
SitePagePictureTableMap::buildTableMap();
