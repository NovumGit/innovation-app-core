<?php

namespace Model\Cms;

use Core\Utils;
use Model\Account\User;
use Model\Account\UserQuery;
use Model\Cms\Base\SiteBlog as BaseSiteBlog;
use Core\Config;

/**
 * Skeleton subclass for representing a row from the 'site_blog' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SiteBlog extends BaseSiteBlog
{
    function getImageDir()
    {
        return Config::getDataDir().'/img/blog/';

    }
    function getAuthorName()
    {

        $oUser = new User();
        if($this->getCreatedByUserId())
        {
            $oUser = UserQuery::create()->findOneById($this->getCreatedByUserId());
        }
        return $oUser->getFirstName();
    }
    function getUrl()
    {

        return '/b/'.Utils::slugify($this->getTitle()).'-'.$this->getId();
    }
}
