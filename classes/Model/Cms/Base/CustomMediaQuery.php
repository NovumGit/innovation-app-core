<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\Cms\CustomMedia as ChildCustomMedia;
use Model\Cms\CustomMediaQuery as ChildCustomMediaQuery;
use Model\Cms\Map\CustomMediaTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'custom_media' table.
 *
 *
 *
 * @method     ChildCustomMediaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCustomMediaQuery orderByParentId($order = Criteria::ASC) Order by the parent_id column
 * @method     ChildCustomMediaQuery orderBySiteId($order = Criteria::ASC) Order by the site_id column
 * @method     ChildCustomMediaQuery orderByNodeTypeId($order = Criteria::ASC) Order by the node_type_id column
 * @method     ChildCustomMediaQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildCustomMediaQuery orderByIsDeletable($order = Criteria::ASC) Order by the is_deletable column
 * @method     ChildCustomMediaQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 *
 * @method     ChildCustomMediaQuery groupById() Group by the id column
 * @method     ChildCustomMediaQuery groupByParentId() Group by the parent_id column
 * @method     ChildCustomMediaQuery groupBySiteId() Group by the site_id column
 * @method     ChildCustomMediaQuery groupByNodeTypeId() Group by the node_type_id column
 * @method     ChildCustomMediaQuery groupByName() Group by the name column
 * @method     ChildCustomMediaQuery groupByIsDeletable() Group by the is_deletable column
 * @method     ChildCustomMediaQuery groupByCreatedDate() Group by the created_date column
 *
 * @method     ChildCustomMediaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCustomMediaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCustomMediaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCustomMediaQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCustomMediaQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCustomMediaQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCustomMediaQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildCustomMediaQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildCustomMediaQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildCustomMediaQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildCustomMediaQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildCustomMediaQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildCustomMediaQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildCustomMediaQuery leftJoinCustomMediaNodeType($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomMediaNodeType relation
 * @method     ChildCustomMediaQuery rightJoinCustomMediaNodeType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomMediaNodeType relation
 * @method     ChildCustomMediaQuery innerJoinCustomMediaNodeType($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomMediaNodeType relation
 *
 * @method     ChildCustomMediaQuery joinWithCustomMediaNodeType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomMediaNodeType relation
 *
 * @method     ChildCustomMediaQuery leftJoinWithCustomMediaNodeType() Adds a LEFT JOIN clause and with to the query using the CustomMediaNodeType relation
 * @method     ChildCustomMediaQuery rightJoinWithCustomMediaNodeType() Adds a RIGHT JOIN clause and with to the query using the CustomMediaNodeType relation
 * @method     ChildCustomMediaQuery innerJoinWithCustomMediaNodeType() Adds a INNER JOIN clause and with to the query using the CustomMediaNodeType relation
 *
 * @method     \Model\Cms\SiteQuery|\Model\Cms\CustomMediaNodeTypeQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCustomMedia findOne(ConnectionInterface $con = null) Return the first ChildCustomMedia matching the query
 * @method     ChildCustomMedia findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCustomMedia matching the query, or a new ChildCustomMedia object populated from the query conditions when no match is found
 *
 * @method     ChildCustomMedia findOneById(int $id) Return the first ChildCustomMedia filtered by the id column
 * @method     ChildCustomMedia findOneByParentId(int $parent_id) Return the first ChildCustomMedia filtered by the parent_id column
 * @method     ChildCustomMedia findOneBySiteId(int $site_id) Return the first ChildCustomMedia filtered by the site_id column
 * @method     ChildCustomMedia findOneByNodeTypeId(int $node_type_id) Return the first ChildCustomMedia filtered by the node_type_id column
 * @method     ChildCustomMedia findOneByName(string $name) Return the first ChildCustomMedia filtered by the name column
 * @method     ChildCustomMedia findOneByIsDeletable(boolean $is_deletable) Return the first ChildCustomMedia filtered by the is_deletable column
 * @method     ChildCustomMedia findOneByCreatedDate(string $created_date) Return the first ChildCustomMedia filtered by the created_date column *

 * @method     ChildCustomMedia requirePk($key, ConnectionInterface $con = null) Return the ChildCustomMedia by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomMedia requireOne(ConnectionInterface $con = null) Return the first ChildCustomMedia matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomMedia requireOneById(int $id) Return the first ChildCustomMedia filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomMedia requireOneByParentId(int $parent_id) Return the first ChildCustomMedia filtered by the parent_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomMedia requireOneBySiteId(int $site_id) Return the first ChildCustomMedia filtered by the site_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomMedia requireOneByNodeTypeId(int $node_type_id) Return the first ChildCustomMedia filtered by the node_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomMedia requireOneByName(string $name) Return the first ChildCustomMedia filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomMedia requireOneByIsDeletable(boolean $is_deletable) Return the first ChildCustomMedia filtered by the is_deletable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomMedia requireOneByCreatedDate(string $created_date) Return the first ChildCustomMedia filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomMedia[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCustomMedia objects based on current ModelCriteria
 * @method     ChildCustomMedia[]|ObjectCollection findById(int $id) Return ChildCustomMedia objects filtered by the id column
 * @method     ChildCustomMedia[]|ObjectCollection findByParentId(int $parent_id) Return ChildCustomMedia objects filtered by the parent_id column
 * @method     ChildCustomMedia[]|ObjectCollection findBySiteId(int $site_id) Return ChildCustomMedia objects filtered by the site_id column
 * @method     ChildCustomMedia[]|ObjectCollection findByNodeTypeId(int $node_type_id) Return ChildCustomMedia objects filtered by the node_type_id column
 * @method     ChildCustomMedia[]|ObjectCollection findByName(string $name) Return ChildCustomMedia objects filtered by the name column
 * @method     ChildCustomMedia[]|ObjectCollection findByIsDeletable(boolean $is_deletable) Return ChildCustomMedia objects filtered by the is_deletable column
 * @method     ChildCustomMedia[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildCustomMedia objects filtered by the created_date column
 * @method     ChildCustomMedia[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CustomMediaQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Cms\Base\CustomMediaQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Cms\\CustomMedia', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCustomMediaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCustomMediaQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCustomMediaQuery) {
            return $criteria;
        }
        $query = new ChildCustomMediaQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCustomMedia|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CustomMediaTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CustomMediaTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomMedia A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, parent_id, site_id, node_type_id, name, is_deletable, created_date FROM custom_media WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCustomMedia $obj */
            $obj = new ChildCustomMedia();
            $obj->hydrate($row);
            CustomMediaTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCustomMedia|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCustomMediaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CustomMediaTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCustomMediaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CustomMediaTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomMediaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CustomMediaTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CustomMediaTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomMediaTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE parent_id > 12
     * </code>
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomMediaQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(CustomMediaTableMap::COL_PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(CustomMediaTableMap::COL_PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomMediaTableMap::COL_PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the site_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE site_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE site_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE site_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomMediaQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(CustomMediaTableMap::COL_SITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(CustomMediaTableMap::COL_SITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomMediaTableMap::COL_SITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the node_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByNodeTypeId(1234); // WHERE node_type_id = 1234
     * $query->filterByNodeTypeId(array(12, 34)); // WHERE node_type_id IN (12, 34)
     * $query->filterByNodeTypeId(array('min' => 12)); // WHERE node_type_id > 12
     * </code>
     *
     * @see       filterByCustomMediaNodeType()
     *
     * @param     mixed $nodeTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomMediaQuery The current query, for fluid interface
     */
    public function filterByNodeTypeId($nodeTypeId = null, $comparison = null)
    {
        if (is_array($nodeTypeId)) {
            $useMinMax = false;
            if (isset($nodeTypeId['min'])) {
                $this->addUsingAlias(CustomMediaTableMap::COL_NODE_TYPE_ID, $nodeTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($nodeTypeId['max'])) {
                $this->addUsingAlias(CustomMediaTableMap::COL_NODE_TYPE_ID, $nodeTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomMediaTableMap::COL_NODE_TYPE_ID, $nodeTypeId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomMediaQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomMediaTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the is_deletable column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDeletable(true); // WHERE is_deletable = true
     * $query->filterByIsDeletable('yes'); // WHERE is_deletable = true
     * </code>
     *
     * @param     boolean|string $isDeletable The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomMediaQuery The current query, for fluid interface
     */
    public function filterByIsDeletable($isDeletable = null, $comparison = null)
    {
        if (is_string($isDeletable)) {
            $isDeletable = in_array(strtolower($isDeletable), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(CustomMediaTableMap::COL_IS_DELETABLE, $isDeletable, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomMediaQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(CustomMediaTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(CustomMediaTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomMediaTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query by a related \Model\Cms\Site object
     *
     * @param \Model\Cms\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomMediaQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \Model\Cms\Site) {
            return $this
                ->addUsingAlias(CustomMediaTableMap::COL_SITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomMediaTableMap::COL_SITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \Model\Cms\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomMediaQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\Model\Cms\SiteQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\CustomMediaNodeType object
     *
     * @param \Model\Cms\CustomMediaNodeType|ObjectCollection $customMediaNodeType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomMediaQuery The current query, for fluid interface
     */
    public function filterByCustomMediaNodeType($customMediaNodeType, $comparison = null)
    {
        if ($customMediaNodeType instanceof \Model\Cms\CustomMediaNodeType) {
            return $this
                ->addUsingAlias(CustomMediaTableMap::COL_NODE_TYPE_ID, $customMediaNodeType->getId(), $comparison);
        } elseif ($customMediaNodeType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(CustomMediaTableMap::COL_NODE_TYPE_ID, $customMediaNodeType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCustomMediaNodeType() only accepts arguments of type \Model\Cms\CustomMediaNodeType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomMediaNodeType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomMediaQuery The current query, for fluid interface
     */
    public function joinCustomMediaNodeType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomMediaNodeType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomMediaNodeType');
        }

        return $this;
    }

    /**
     * Use the CustomMediaNodeType relation CustomMediaNodeType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\CustomMediaNodeTypeQuery A secondary query class using the current class as primary query
     */
    public function useCustomMediaNodeTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCustomMediaNodeType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomMediaNodeType', '\Model\Cms\CustomMediaNodeTypeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCustomMedia $customMedia Object to remove from the list of results
     *
     * @return $this|ChildCustomMediaQuery The current query, for fluid interface
     */
    public function prune($customMedia = null)
    {
        if ($customMedia) {
            $this->addUsingAlias(CustomMediaTableMap::COL_ID, $customMedia->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the custom_media table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomMediaTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CustomMediaTableMap::clearInstancePool();
            CustomMediaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomMediaTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CustomMediaTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CustomMediaTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CustomMediaTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CustomMediaQuery
