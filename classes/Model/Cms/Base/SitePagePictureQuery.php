<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\Cms\SitePagePicture as ChildSitePagePicture;
use Model\Cms\SitePagePictureQuery as ChildSitePagePictureQuery;
use Model\Cms\Map\SitePagePictureTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'site_page_picture' table.
 *
 *
 *
 * @method     ChildSitePagePictureQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSitePagePictureQuery orderByOldId($order = Criteria::ASC) Order by the old_id column
 * @method     ChildSitePagePictureQuery orderBySitePageId($order = Criteria::ASC) Order by the site_page_id column
 * @method     ChildSitePagePictureQuery orderByOriginalName($order = Criteria::ASC) Order by the original_name column
 * @method     ChildSitePagePictureQuery orderByFileExt($order = Criteria::ASC) Order by the file_ext column
 * @method     ChildSitePagePictureQuery orderByFileSize($order = Criteria::ASC) Order by the file_size column
 * @method     ChildSitePagePictureQuery orderByFileMime($order = Criteria::ASC) Order by the file_mime column
 * @method     ChildSitePagePictureQuery orderBySorting($order = Criteria::ASC) Order by the sorting column
 *
 * @method     ChildSitePagePictureQuery groupById() Group by the id column
 * @method     ChildSitePagePictureQuery groupByOldId() Group by the old_id column
 * @method     ChildSitePagePictureQuery groupBySitePageId() Group by the site_page_id column
 * @method     ChildSitePagePictureQuery groupByOriginalName() Group by the original_name column
 * @method     ChildSitePagePictureQuery groupByFileExt() Group by the file_ext column
 * @method     ChildSitePagePictureQuery groupByFileSize() Group by the file_size column
 * @method     ChildSitePagePictureQuery groupByFileMime() Group by the file_mime column
 * @method     ChildSitePagePictureQuery groupBySorting() Group by the sorting column
 *
 * @method     ChildSitePagePictureQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSitePagePictureQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSitePagePictureQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSitePagePictureQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSitePagePictureQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSitePagePictureQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSitePagePictureQuery leftJoinSitePage($relationAlias = null) Adds a LEFT JOIN clause to the query using the SitePage relation
 * @method     ChildSitePagePictureQuery rightJoinSitePage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SitePage relation
 * @method     ChildSitePagePictureQuery innerJoinSitePage($relationAlias = null) Adds a INNER JOIN clause to the query using the SitePage relation
 *
 * @method     ChildSitePagePictureQuery joinWithSitePage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SitePage relation
 *
 * @method     ChildSitePagePictureQuery leftJoinWithSitePage() Adds a LEFT JOIN clause and with to the query using the SitePage relation
 * @method     ChildSitePagePictureQuery rightJoinWithSitePage() Adds a RIGHT JOIN clause and with to the query using the SitePage relation
 * @method     ChildSitePagePictureQuery innerJoinWithSitePage() Adds a INNER JOIN clause and with to the query using the SitePage relation
 *
 * @method     \Model\Cms\SitePageQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSitePagePicture findOne(ConnectionInterface $con = null) Return the first ChildSitePagePicture matching the query
 * @method     ChildSitePagePicture findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSitePagePicture matching the query, or a new ChildSitePagePicture object populated from the query conditions when no match is found
 *
 * @method     ChildSitePagePicture findOneById(int $id) Return the first ChildSitePagePicture filtered by the id column
 * @method     ChildSitePagePicture findOneByOldId(string $old_id) Return the first ChildSitePagePicture filtered by the old_id column
 * @method     ChildSitePagePicture findOneBySitePageId(int $site_page_id) Return the first ChildSitePagePicture filtered by the site_page_id column
 * @method     ChildSitePagePicture findOneByOriginalName(string $original_name) Return the first ChildSitePagePicture filtered by the original_name column
 * @method     ChildSitePagePicture findOneByFileExt(string $file_ext) Return the first ChildSitePagePicture filtered by the file_ext column
 * @method     ChildSitePagePicture findOneByFileSize(int $file_size) Return the first ChildSitePagePicture filtered by the file_size column
 * @method     ChildSitePagePicture findOneByFileMime(string $file_mime) Return the first ChildSitePagePicture filtered by the file_mime column
 * @method     ChildSitePagePicture findOneBySorting(int $sorting) Return the first ChildSitePagePicture filtered by the sorting column *

 * @method     ChildSitePagePicture requirePk($key, ConnectionInterface $con = null) Return the ChildSitePagePicture by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePagePicture requireOne(ConnectionInterface $con = null) Return the first ChildSitePagePicture matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSitePagePicture requireOneById(int $id) Return the first ChildSitePagePicture filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePagePicture requireOneByOldId(string $old_id) Return the first ChildSitePagePicture filtered by the old_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePagePicture requireOneBySitePageId(int $site_page_id) Return the first ChildSitePagePicture filtered by the site_page_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePagePicture requireOneByOriginalName(string $original_name) Return the first ChildSitePagePicture filtered by the original_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePagePicture requireOneByFileExt(string $file_ext) Return the first ChildSitePagePicture filtered by the file_ext column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePagePicture requireOneByFileSize(int $file_size) Return the first ChildSitePagePicture filtered by the file_size column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePagePicture requireOneByFileMime(string $file_mime) Return the first ChildSitePagePicture filtered by the file_mime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePagePicture requireOneBySorting(int $sorting) Return the first ChildSitePagePicture filtered by the sorting column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSitePagePicture[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSitePagePicture objects based on current ModelCriteria
 * @method     ChildSitePagePicture[]|ObjectCollection findById(int $id) Return ChildSitePagePicture objects filtered by the id column
 * @method     ChildSitePagePicture[]|ObjectCollection findByOldId(string $old_id) Return ChildSitePagePicture objects filtered by the old_id column
 * @method     ChildSitePagePicture[]|ObjectCollection findBySitePageId(int $site_page_id) Return ChildSitePagePicture objects filtered by the site_page_id column
 * @method     ChildSitePagePicture[]|ObjectCollection findByOriginalName(string $original_name) Return ChildSitePagePicture objects filtered by the original_name column
 * @method     ChildSitePagePicture[]|ObjectCollection findByFileExt(string $file_ext) Return ChildSitePagePicture objects filtered by the file_ext column
 * @method     ChildSitePagePicture[]|ObjectCollection findByFileSize(int $file_size) Return ChildSitePagePicture objects filtered by the file_size column
 * @method     ChildSitePagePicture[]|ObjectCollection findByFileMime(string $file_mime) Return ChildSitePagePicture objects filtered by the file_mime column
 * @method     ChildSitePagePicture[]|ObjectCollection findBySorting(int $sorting) Return ChildSitePagePicture objects filtered by the sorting column
 * @method     ChildSitePagePicture[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SitePagePictureQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Cms\Base\SitePagePictureQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Cms\\SitePagePicture', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSitePagePictureQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSitePagePictureQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSitePagePictureQuery) {
            return $criteria;
        }
        $query = new ChildSitePagePictureQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSitePagePicture|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SitePagePictureTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SitePagePictureTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSitePagePicture A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, old_id, site_page_id, original_name, file_ext, file_size, file_mime, sorting FROM site_page_picture WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSitePagePicture $obj */
            $obj = new ChildSitePagePicture();
            $obj->hydrate($row);
            SitePagePictureTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSitePagePicture|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSitePagePictureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SitePagePictureTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSitePagePictureQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SitePagePictureTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePagePictureQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SitePagePictureTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SitePagePictureTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePagePictureTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the old_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldId('fooValue');   // WHERE old_id = 'fooValue'
     * $query->filterByOldId('%fooValue%', Criteria::LIKE); // WHERE old_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $oldId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePagePictureQuery The current query, for fluid interface
     */
    public function filterByOldId($oldId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($oldId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePagePictureTableMap::COL_OLD_ID, $oldId, $comparison);
    }

    /**
     * Filter the query on the site_page_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySitePageId(1234); // WHERE site_page_id = 1234
     * $query->filterBySitePageId(array(12, 34)); // WHERE site_page_id IN (12, 34)
     * $query->filterBySitePageId(array('min' => 12)); // WHERE site_page_id > 12
     * </code>
     *
     * @see       filterBySitePage()
     *
     * @param     mixed $sitePageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePagePictureQuery The current query, for fluid interface
     */
    public function filterBySitePageId($sitePageId = null, $comparison = null)
    {
        if (is_array($sitePageId)) {
            $useMinMax = false;
            if (isset($sitePageId['min'])) {
                $this->addUsingAlias(SitePagePictureTableMap::COL_SITE_PAGE_ID, $sitePageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sitePageId['max'])) {
                $this->addUsingAlias(SitePagePictureTableMap::COL_SITE_PAGE_ID, $sitePageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePagePictureTableMap::COL_SITE_PAGE_ID, $sitePageId, $comparison);
    }

    /**
     * Filter the query on the original_name column
     *
     * Example usage:
     * <code>
     * $query->filterByOriginalName('fooValue');   // WHERE original_name = 'fooValue'
     * $query->filterByOriginalName('%fooValue%', Criteria::LIKE); // WHERE original_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $originalName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePagePictureQuery The current query, for fluid interface
     */
    public function filterByOriginalName($originalName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($originalName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePagePictureTableMap::COL_ORIGINAL_NAME, $originalName, $comparison);
    }

    /**
     * Filter the query on the file_ext column
     *
     * Example usage:
     * <code>
     * $query->filterByFileExt('fooValue');   // WHERE file_ext = 'fooValue'
     * $query->filterByFileExt('%fooValue%', Criteria::LIKE); // WHERE file_ext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fileExt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePagePictureQuery The current query, for fluid interface
     */
    public function filterByFileExt($fileExt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fileExt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePagePictureTableMap::COL_FILE_EXT, $fileExt, $comparison);
    }

    /**
     * Filter the query on the file_size column
     *
     * Example usage:
     * <code>
     * $query->filterByFileSize(1234); // WHERE file_size = 1234
     * $query->filterByFileSize(array(12, 34)); // WHERE file_size IN (12, 34)
     * $query->filterByFileSize(array('min' => 12)); // WHERE file_size > 12
     * </code>
     *
     * @param     mixed $fileSize The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePagePictureQuery The current query, for fluid interface
     */
    public function filterByFileSize($fileSize = null, $comparison = null)
    {
        if (is_array($fileSize)) {
            $useMinMax = false;
            if (isset($fileSize['min'])) {
                $this->addUsingAlias(SitePagePictureTableMap::COL_FILE_SIZE, $fileSize['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fileSize['max'])) {
                $this->addUsingAlias(SitePagePictureTableMap::COL_FILE_SIZE, $fileSize['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePagePictureTableMap::COL_FILE_SIZE, $fileSize, $comparison);
    }

    /**
     * Filter the query on the file_mime column
     *
     * Example usage:
     * <code>
     * $query->filterByFileMime('fooValue');   // WHERE file_mime = 'fooValue'
     * $query->filterByFileMime('%fooValue%', Criteria::LIKE); // WHERE file_mime LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fileMime The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePagePictureQuery The current query, for fluid interface
     */
    public function filterByFileMime($fileMime = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fileMime)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePagePictureTableMap::COL_FILE_MIME, $fileMime, $comparison);
    }

    /**
     * Filter the query on the sorting column
     *
     * Example usage:
     * <code>
     * $query->filterBySorting(1234); // WHERE sorting = 1234
     * $query->filterBySorting(array(12, 34)); // WHERE sorting IN (12, 34)
     * $query->filterBySorting(array('min' => 12)); // WHERE sorting > 12
     * </code>
     *
     * @param     mixed $sorting The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePagePictureQuery The current query, for fluid interface
     */
    public function filterBySorting($sorting = null, $comparison = null)
    {
        if (is_array($sorting)) {
            $useMinMax = false;
            if (isset($sorting['min'])) {
                $this->addUsingAlias(SitePagePictureTableMap::COL_SORTING, $sorting['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sorting['max'])) {
                $this->addUsingAlias(SitePagePictureTableMap::COL_SORTING, $sorting['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePagePictureTableMap::COL_SORTING, $sorting, $comparison);
    }

    /**
     * Filter the query by a related \Model\Cms\SitePage object
     *
     * @param \Model\Cms\SitePage|ObjectCollection $sitePage The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSitePagePictureQuery The current query, for fluid interface
     */
    public function filterBySitePage($sitePage, $comparison = null)
    {
        if ($sitePage instanceof \Model\Cms\SitePage) {
            return $this
                ->addUsingAlias(SitePagePictureTableMap::COL_SITE_PAGE_ID, $sitePage->getId(), $comparison);
        } elseif ($sitePage instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SitePagePictureTableMap::COL_SITE_PAGE_ID, $sitePage->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySitePage() only accepts arguments of type \Model\Cms\SitePage or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SitePage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSitePagePictureQuery The current query, for fluid interface
     */
    public function joinSitePage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SitePage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SitePage');
        }

        return $this;
    }

    /**
     * Use the SitePage relation SitePage object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SitePageQuery A secondary query class using the current class as primary query
     */
    public function useSitePageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSitePage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SitePage', '\Model\Cms\SitePageQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSitePagePicture $sitePagePicture Object to remove from the list of results
     *
     * @return $this|ChildSitePagePictureQuery The current query, for fluid interface
     */
    public function prune($sitePagePicture = null)
    {
        if ($sitePagePicture) {
            $this->addUsingAlias(SitePagePictureTableMap::COL_ID, $sitePagePicture->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the site_page_picture table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SitePagePictureTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SitePagePictureTableMap::clearInstancePool();
            SitePagePictureTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SitePagePictureTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SitePagePictureTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SitePagePictureTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SitePagePictureTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SitePagePictureQuery
