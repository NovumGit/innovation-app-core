<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\PromoProduct;
use Model\Cms\Site as ChildSite;
use Model\Cms\SiteQuery as ChildSiteQuery;
use Model\Cms\Mail\MailTemplate;
use Model\Cms\Map\SiteTableMap;
use Model\Company\Company;
use Model\Sale\SaleOrder;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'site' table.
 *
 *
 *
 * @method     ChildSiteQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSiteQuery orderByDomain($order = Criteria::ASC) Order by the domain column
 * @method     ChildSiteQuery orderByCustomFolder($order = Criteria::ASC) Order by the custom_folder column
 * @method     ChildSiteQuery orderByFromEmail($order = Criteria::ASC) Order by the from_email column
 * @method     ChildSiteQuery orderByContactFormToEmail($order = Criteria::ASC) Order by the contact_form_to_email column
 * @method     ChildSiteQuery orderByNumFooterBlocks($order = Criteria::ASC) Order by the num_footer_blocks column
 * @method     ChildSiteQuery orderByGeneralPhone($order = Criteria::ASC) Order by the general_phone column
 * @method     ChildSiteQuery orderByInvoicingCompanyId($order = Criteria::ASC) Order by the invoicing_company_id column
 *
 * @method     ChildSiteQuery groupById() Group by the id column
 * @method     ChildSiteQuery groupByDomain() Group by the domain column
 * @method     ChildSiteQuery groupByCustomFolder() Group by the custom_folder column
 * @method     ChildSiteQuery groupByFromEmail() Group by the from_email column
 * @method     ChildSiteQuery groupByContactFormToEmail() Group by the contact_form_to_email column
 * @method     ChildSiteQuery groupByNumFooterBlocks() Group by the num_footer_blocks column
 * @method     ChildSiteQuery groupByGeneralPhone() Group by the general_phone column
 * @method     ChildSiteQuery groupByInvoicingCompanyId() Group by the invoicing_company_id column
 *
 * @method     ChildSiteQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteQuery leftJoinOwnCompany($relationAlias = null) Adds a LEFT JOIN clause to the query using the OwnCompany relation
 * @method     ChildSiteQuery rightJoinOwnCompany($relationAlias = null) Adds a RIGHT JOIN clause to the query using the OwnCompany relation
 * @method     ChildSiteQuery innerJoinOwnCompany($relationAlias = null) Adds a INNER JOIN clause to the query using the OwnCompany relation
 *
 * @method     ChildSiteQuery joinWithOwnCompany($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the OwnCompany relation
 *
 * @method     ChildSiteQuery leftJoinWithOwnCompany() Adds a LEFT JOIN clause and with to the query using the OwnCompany relation
 * @method     ChildSiteQuery rightJoinWithOwnCompany() Adds a RIGHT JOIN clause and with to the query using the OwnCompany relation
 * @method     ChildSiteQuery innerJoinWithOwnCompany() Adds a INNER JOIN clause and with to the query using the OwnCompany relation
 *
 * @method     ChildSiteQuery leftJoinCustomMedia($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomMedia relation
 * @method     ChildSiteQuery rightJoinCustomMedia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomMedia relation
 * @method     ChildSiteQuery innerJoinCustomMedia($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomMedia relation
 *
 * @method     ChildSiteQuery joinWithCustomMedia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomMedia relation
 *
 * @method     ChildSiteQuery leftJoinWithCustomMedia() Adds a LEFT JOIN clause and with to the query using the CustomMedia relation
 * @method     ChildSiteQuery rightJoinWithCustomMedia() Adds a RIGHT JOIN clause and with to the query using the CustomMedia relation
 * @method     ChildSiteQuery innerJoinWithCustomMedia() Adds a INNER JOIN clause and with to the query using the CustomMedia relation
 *
 * @method     ChildSiteQuery leftJoinPromoProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the PromoProduct relation
 * @method     ChildSiteQuery rightJoinPromoProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PromoProduct relation
 * @method     ChildSiteQuery innerJoinPromoProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the PromoProduct relation
 *
 * @method     ChildSiteQuery joinWithPromoProduct($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PromoProduct relation
 *
 * @method     ChildSiteQuery leftJoinWithPromoProduct() Adds a LEFT JOIN clause and with to the query using the PromoProduct relation
 * @method     ChildSiteQuery rightJoinWithPromoProduct() Adds a RIGHT JOIN clause and with to the query using the PromoProduct relation
 * @method     ChildSiteQuery innerJoinWithPromoProduct() Adds a INNER JOIN clause and with to the query using the PromoProduct relation
 *
 * @method     ChildSiteQuery leftJoinSaleOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrder relation
 * @method     ChildSiteQuery rightJoinSaleOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrder relation
 * @method     ChildSiteQuery innerJoinSaleOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrder relation
 *
 * @method     ChildSiteQuery joinWithSaleOrder($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrder relation
 *
 * @method     ChildSiteQuery leftJoinWithSaleOrder() Adds a LEFT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildSiteQuery rightJoinWithSaleOrder() Adds a RIGHT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildSiteQuery innerJoinWithSaleOrder() Adds a INNER JOIN clause and with to the query using the SaleOrder relation
 *
 * @method     ChildSiteQuery leftJoinSiteUsp($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteUsp relation
 * @method     ChildSiteQuery rightJoinSiteUsp($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteUsp relation
 * @method     ChildSiteQuery innerJoinSiteUsp($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteUsp relation
 *
 * @method     ChildSiteQuery joinWithSiteUsp($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteUsp relation
 *
 * @method     ChildSiteQuery leftJoinWithSiteUsp() Adds a LEFT JOIN clause and with to the query using the SiteUsp relation
 * @method     ChildSiteQuery rightJoinWithSiteUsp() Adds a RIGHT JOIN clause and with to the query using the SiteUsp relation
 * @method     ChildSiteQuery innerJoinWithSiteUsp() Adds a INNER JOIN clause and with to the query using the SiteUsp relation
 *
 * @method     ChildSiteQuery leftJoinSiteFooterBlock($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteFooterBlock relation
 * @method     ChildSiteQuery rightJoinSiteFooterBlock($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteFooterBlock relation
 * @method     ChildSiteQuery innerJoinSiteFooterBlock($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteFooterBlock relation
 *
 * @method     ChildSiteQuery joinWithSiteFooterBlock($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteFooterBlock relation
 *
 * @method     ChildSiteQuery leftJoinWithSiteFooterBlock() Adds a LEFT JOIN clause and with to the query using the SiteFooterBlock relation
 * @method     ChildSiteQuery rightJoinWithSiteFooterBlock() Adds a RIGHT JOIN clause and with to the query using the SiteFooterBlock relation
 * @method     ChildSiteQuery innerJoinWithSiteFooterBlock() Adds a INNER JOIN clause and with to the query using the SiteFooterBlock relation
 *
 * @method     ChildSiteQuery leftJoinSiteBlog($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteBlog relation
 * @method     ChildSiteQuery rightJoinSiteBlog($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteBlog relation
 * @method     ChildSiteQuery innerJoinSiteBlog($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteBlog relation
 *
 * @method     ChildSiteQuery joinWithSiteBlog($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteBlog relation
 *
 * @method     ChildSiteQuery leftJoinWithSiteBlog() Adds a LEFT JOIN clause and with to the query using the SiteBlog relation
 * @method     ChildSiteQuery rightJoinWithSiteBlog() Adds a RIGHT JOIN clause and with to the query using the SiteBlog relation
 * @method     ChildSiteQuery innerJoinWithSiteBlog() Adds a INNER JOIN clause and with to the query using the SiteBlog relation
 *
 * @method     ChildSiteQuery leftJoinSiteBanner($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteBanner relation
 * @method     ChildSiteQuery rightJoinSiteBanner($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteBanner relation
 * @method     ChildSiteQuery innerJoinSiteBanner($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteBanner relation
 *
 * @method     ChildSiteQuery joinWithSiteBanner($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteBanner relation
 *
 * @method     ChildSiteQuery leftJoinWithSiteBanner() Adds a LEFT JOIN clause and with to the query using the SiteBanner relation
 * @method     ChildSiteQuery rightJoinWithSiteBanner() Adds a RIGHT JOIN clause and with to the query using the SiteBanner relation
 * @method     ChildSiteQuery innerJoinWithSiteBanner() Adds a INNER JOIN clause and with to the query using the SiteBanner relation
 *
 * @method     ChildSiteQuery leftJoinSiteFAQCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteFAQCategory relation
 * @method     ChildSiteQuery rightJoinSiteFAQCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteFAQCategory relation
 * @method     ChildSiteQuery innerJoinSiteFAQCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteFAQCategory relation
 *
 * @method     ChildSiteQuery joinWithSiteFAQCategory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteFAQCategory relation
 *
 * @method     ChildSiteQuery leftJoinWithSiteFAQCategory() Adds a LEFT JOIN clause and with to the query using the SiteFAQCategory relation
 * @method     ChildSiteQuery rightJoinWithSiteFAQCategory() Adds a RIGHT JOIN clause and with to the query using the SiteFAQCategory relation
 * @method     ChildSiteQuery innerJoinWithSiteFAQCategory() Adds a INNER JOIN clause and with to the query using the SiteFAQCategory relation
 *
 * @method     ChildSiteQuery leftJoinSiteFAQ($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteFAQ relation
 * @method     ChildSiteQuery rightJoinSiteFAQ($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteFAQ relation
 * @method     ChildSiteQuery innerJoinSiteFAQ($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteFAQ relation
 *
 * @method     ChildSiteQuery joinWithSiteFAQ($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteFAQ relation
 *
 * @method     ChildSiteQuery leftJoinWithSiteFAQ() Adds a LEFT JOIN clause and with to the query using the SiteFAQ relation
 * @method     ChildSiteQuery rightJoinWithSiteFAQ() Adds a RIGHT JOIN clause and with to the query using the SiteFAQ relation
 * @method     ChildSiteQuery innerJoinWithSiteFAQ() Adds a INNER JOIN clause and with to the query using the SiteFAQ relation
 *
 * @method     ChildSiteQuery leftJoinSitePage($relationAlias = null) Adds a LEFT JOIN clause to the query using the SitePage relation
 * @method     ChildSiteQuery rightJoinSitePage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SitePage relation
 * @method     ChildSiteQuery innerJoinSitePage($relationAlias = null) Adds a INNER JOIN clause to the query using the SitePage relation
 *
 * @method     ChildSiteQuery joinWithSitePage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SitePage relation
 *
 * @method     ChildSiteQuery leftJoinWithSitePage() Adds a LEFT JOIN clause and with to the query using the SitePage relation
 * @method     ChildSiteQuery rightJoinWithSitePage() Adds a RIGHT JOIN clause and with to the query using the SitePage relation
 * @method     ChildSiteQuery innerJoinWithSitePage() Adds a INNER JOIN clause and with to the query using the SitePage relation
 *
 * @method     ChildSiteQuery leftJoinMailTemplate($relationAlias = null) Adds a LEFT JOIN clause to the query using the MailTemplate relation
 * @method     ChildSiteQuery rightJoinMailTemplate($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MailTemplate relation
 * @method     ChildSiteQuery innerJoinMailTemplate($relationAlias = null) Adds a INNER JOIN clause to the query using the MailTemplate relation
 *
 * @method     ChildSiteQuery joinWithMailTemplate($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MailTemplate relation
 *
 * @method     ChildSiteQuery leftJoinWithMailTemplate() Adds a LEFT JOIN clause and with to the query using the MailTemplate relation
 * @method     ChildSiteQuery rightJoinWithMailTemplate() Adds a RIGHT JOIN clause and with to the query using the MailTemplate relation
 * @method     ChildSiteQuery innerJoinWithMailTemplate() Adds a INNER JOIN clause and with to the query using the MailTemplate relation
 *
 * @method     \Model\Company\CompanyQuery|\Model\Cms\CustomMediaQuery|\Model\PromoProductQuery|\Model\Sale\SaleOrderQuery|\Model\Cms\SiteUspQuery|\Model\Cms\SiteFooterBlockQuery|\Model\Cms\SiteBlogQuery|\Model\Cms\SiteBannerQuery|\Model\Cms\SiteFAQCategoryQuery|\Model\Cms\SiteFAQQuery|\Model\Cms\SitePageQuery|\Model\Cms\Mail\MailTemplateQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSite findOne(ConnectionInterface $con = null) Return the first ChildSite matching the query
 * @method     ChildSite findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSite matching the query, or a new ChildSite object populated from the query conditions when no match is found
 *
 * @method     ChildSite findOneById(int $id) Return the first ChildSite filtered by the id column
 * @method     ChildSite findOneByDomain(string $domain) Return the first ChildSite filtered by the domain column
 * @method     ChildSite findOneByCustomFolder(string $custom_folder) Return the first ChildSite filtered by the custom_folder column
 * @method     ChildSite findOneByFromEmail(string $from_email) Return the first ChildSite filtered by the from_email column
 * @method     ChildSite findOneByContactFormToEmail(string $contact_form_to_email) Return the first ChildSite filtered by the contact_form_to_email column
 * @method     ChildSite findOneByNumFooterBlocks(int $num_footer_blocks) Return the first ChildSite filtered by the num_footer_blocks column
 * @method     ChildSite findOneByGeneralPhone(string $general_phone) Return the first ChildSite filtered by the general_phone column
 * @method     ChildSite findOneByInvoicingCompanyId(int $invoicing_company_id) Return the first ChildSite filtered by the invoicing_company_id column *

 * @method     ChildSite requirePk($key, ConnectionInterface $con = null) Return the ChildSite by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOne(ConnectionInterface $con = null) Return the first ChildSite matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSite requireOneById(int $id) Return the first ChildSite filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByDomain(string $domain) Return the first ChildSite filtered by the domain column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByCustomFolder(string $custom_folder) Return the first ChildSite filtered by the custom_folder column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByFromEmail(string $from_email) Return the first ChildSite filtered by the from_email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByContactFormToEmail(string $contact_form_to_email) Return the first ChildSite filtered by the contact_form_to_email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByNumFooterBlocks(int $num_footer_blocks) Return the first ChildSite filtered by the num_footer_blocks column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByGeneralPhone(string $general_phone) Return the first ChildSite filtered by the general_phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSite requireOneByInvoicingCompanyId(int $invoicing_company_id) Return the first ChildSite filtered by the invoicing_company_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSite[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSite objects based on current ModelCriteria
 * @method     ChildSite[]|ObjectCollection findById(int $id) Return ChildSite objects filtered by the id column
 * @method     ChildSite[]|ObjectCollection findByDomain(string $domain) Return ChildSite objects filtered by the domain column
 * @method     ChildSite[]|ObjectCollection findByCustomFolder(string $custom_folder) Return ChildSite objects filtered by the custom_folder column
 * @method     ChildSite[]|ObjectCollection findByFromEmail(string $from_email) Return ChildSite objects filtered by the from_email column
 * @method     ChildSite[]|ObjectCollection findByContactFormToEmail(string $contact_form_to_email) Return ChildSite objects filtered by the contact_form_to_email column
 * @method     ChildSite[]|ObjectCollection findByNumFooterBlocks(int $num_footer_blocks) Return ChildSite objects filtered by the num_footer_blocks column
 * @method     ChildSite[]|ObjectCollection findByGeneralPhone(string $general_phone) Return ChildSite objects filtered by the general_phone column
 * @method     ChildSite[]|ObjectCollection findByInvoicingCompanyId(int $invoicing_company_id) Return ChildSite objects filtered by the invoicing_company_id column
 * @method     ChildSite[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Cms\Base\SiteQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Cms\\Site', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteQuery) {
            return $criteria;
        }
        $query = new ChildSiteQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSite|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSite A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, domain, custom_folder, from_email, contact_form_to_email, num_footer_blocks, general_phone, invoicing_company_id FROM site WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSite $obj */
            $obj = new ChildSite();
            $obj->hydrate($row);
            SiteTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSite|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the domain column
     *
     * Example usage:
     * <code>
     * $query->filterByDomain('fooValue');   // WHERE domain = 'fooValue'
     * $query->filterByDomain('%fooValue%', Criteria::LIKE); // WHERE domain LIKE '%fooValue%'
     * </code>
     *
     * @param     string $domain The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByDomain($domain = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($domain)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_DOMAIN, $domain, $comparison);
    }

    /**
     * Filter the query on the custom_folder column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomFolder('fooValue');   // WHERE custom_folder = 'fooValue'
     * $query->filterByCustomFolder('%fooValue%', Criteria::LIKE); // WHERE custom_folder LIKE '%fooValue%'
     * </code>
     *
     * @param     string $customFolder The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByCustomFolder($customFolder = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($customFolder)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_CUSTOM_FOLDER, $customFolder, $comparison);
    }

    /**
     * Filter the query on the from_email column
     *
     * Example usage:
     * <code>
     * $query->filterByFromEmail('fooValue');   // WHERE from_email = 'fooValue'
     * $query->filterByFromEmail('%fooValue%', Criteria::LIKE); // WHERE from_email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fromEmail The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByFromEmail($fromEmail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fromEmail)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_FROM_EMAIL, $fromEmail, $comparison);
    }

    /**
     * Filter the query on the contact_form_to_email column
     *
     * Example usage:
     * <code>
     * $query->filterByContactFormToEmail('fooValue');   // WHERE contact_form_to_email = 'fooValue'
     * $query->filterByContactFormToEmail('%fooValue%', Criteria::LIKE); // WHERE contact_form_to_email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $contactFormToEmail The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByContactFormToEmail($contactFormToEmail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($contactFormToEmail)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_CONTACT_FORM_TO_EMAIL, $contactFormToEmail, $comparison);
    }

    /**
     * Filter the query on the num_footer_blocks column
     *
     * Example usage:
     * <code>
     * $query->filterByNumFooterBlocks(1234); // WHERE num_footer_blocks = 1234
     * $query->filterByNumFooterBlocks(array(12, 34)); // WHERE num_footer_blocks IN (12, 34)
     * $query->filterByNumFooterBlocks(array('min' => 12)); // WHERE num_footer_blocks > 12
     * </code>
     *
     * @param     mixed $numFooterBlocks The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByNumFooterBlocks($numFooterBlocks = null, $comparison = null)
    {
        if (is_array($numFooterBlocks)) {
            $useMinMax = false;
            if (isset($numFooterBlocks['min'])) {
                $this->addUsingAlias(SiteTableMap::COL_NUM_FOOTER_BLOCKS, $numFooterBlocks['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($numFooterBlocks['max'])) {
                $this->addUsingAlias(SiteTableMap::COL_NUM_FOOTER_BLOCKS, $numFooterBlocks['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_NUM_FOOTER_BLOCKS, $numFooterBlocks, $comparison);
    }

    /**
     * Filter the query on the general_phone column
     *
     * Example usage:
     * <code>
     * $query->filterByGeneralPhone('fooValue');   // WHERE general_phone = 'fooValue'
     * $query->filterByGeneralPhone('%fooValue%', Criteria::LIKE); // WHERE general_phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $generalPhone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByGeneralPhone($generalPhone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($generalPhone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_GENERAL_PHONE, $generalPhone, $comparison);
    }

    /**
     * Filter the query on the invoicing_company_id column
     *
     * Example usage:
     * <code>
     * $query->filterByInvoicingCompanyId(1234); // WHERE invoicing_company_id = 1234
     * $query->filterByInvoicingCompanyId(array(12, 34)); // WHERE invoicing_company_id IN (12, 34)
     * $query->filterByInvoicingCompanyId(array('min' => 12)); // WHERE invoicing_company_id > 12
     * </code>
     *
     * @see       filterByOwnCompany()
     *
     * @param     mixed $invoicingCompanyId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function filterByInvoicingCompanyId($invoicingCompanyId = null, $comparison = null)
    {
        if (is_array($invoicingCompanyId)) {
            $useMinMax = false;
            if (isset($invoicingCompanyId['min'])) {
                $this->addUsingAlias(SiteTableMap::COL_INVOICING_COMPANY_ID, $invoicingCompanyId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($invoicingCompanyId['max'])) {
                $this->addUsingAlias(SiteTableMap::COL_INVOICING_COMPANY_ID, $invoicingCompanyId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteTableMap::COL_INVOICING_COMPANY_ID, $invoicingCompanyId, $comparison);
    }

    /**
     * Filter the query by a related \Model\Company\Company object
     *
     * @param \Model\Company\Company|ObjectCollection $company The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByOwnCompany($company, $comparison = null)
    {
        if ($company instanceof \Model\Company\Company) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_INVOICING_COMPANY_ID, $company->getId(), $comparison);
        } elseif ($company instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteTableMap::COL_INVOICING_COMPANY_ID, $company->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByOwnCompany() only accepts arguments of type \Model\Company\Company or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the OwnCompany relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinOwnCompany($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('OwnCompany');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'OwnCompany');
        }

        return $this;
    }

    /**
     * Use the OwnCompany relation Company object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Company\CompanyQuery A secondary query class using the current class as primary query
     */
    public function useOwnCompanyQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinOwnCompany($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'OwnCompany', '\Model\Company\CompanyQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\CustomMedia object
     *
     * @param \Model\Cms\CustomMedia|ObjectCollection $customMedia the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByCustomMedia($customMedia, $comparison = null)
    {
        if ($customMedia instanceof \Model\Cms\CustomMedia) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_ID, $customMedia->getSiteId(), $comparison);
        } elseif ($customMedia instanceof ObjectCollection) {
            return $this
                ->useCustomMediaQuery()
                ->filterByPrimaryKeys($customMedia->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomMedia() only accepts arguments of type \Model\Cms\CustomMedia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomMedia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinCustomMedia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomMedia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomMedia');
        }

        return $this;
    }

    /**
     * Use the CustomMedia relation CustomMedia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\CustomMediaQuery A secondary query class using the current class as primary query
     */
    public function useCustomMediaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCustomMedia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomMedia', '\Model\Cms\CustomMediaQuery');
    }

    /**
     * Filter the query by a related \Model\PromoProduct object
     *
     * @param \Model\PromoProduct|ObjectCollection $promoProduct the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByPromoProduct($promoProduct, $comparison = null)
    {
        if ($promoProduct instanceof \Model\PromoProduct) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_ID, $promoProduct->getSiteId(), $comparison);
        } elseif ($promoProduct instanceof ObjectCollection) {
            return $this
                ->usePromoProductQuery()
                ->filterByPrimaryKeys($promoProduct->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPromoProduct() only accepts arguments of type \Model\PromoProduct or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PromoProduct relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinPromoProduct($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PromoProduct');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PromoProduct');
        }

        return $this;
    }

    /**
     * Use the PromoProduct relation PromoProduct object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\PromoProductQuery A secondary query class using the current class as primary query
     */
    public function usePromoProductQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPromoProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PromoProduct', '\Model\PromoProductQuery');
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrder object
     *
     * @param \Model\Sale\SaleOrder|ObjectCollection $saleOrder the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterBySaleOrder($saleOrder, $comparison = null)
    {
        if ($saleOrder instanceof \Model\Sale\SaleOrder) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_ID, $saleOrder->getSiteId(), $comparison);
        } elseif ($saleOrder instanceof ObjectCollection) {
            return $this
                ->useSaleOrderQuery()
                ->filterByPrimaryKeys($saleOrder->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleOrder() only accepts arguments of type \Model\Sale\SaleOrder or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinSaleOrder($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrder');
        }

        return $this;
    }

    /**
     * Use the SaleOrder relation SaleOrder object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaleOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrder', '\Model\Sale\SaleOrderQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteUsp object
     *
     * @param \Model\Cms\SiteUsp|ObjectCollection $siteUsp the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterBySiteUsp($siteUsp, $comparison = null)
    {
        if ($siteUsp instanceof \Model\Cms\SiteUsp) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_ID, $siteUsp->getSiteId(), $comparison);
        } elseif ($siteUsp instanceof ObjectCollection) {
            return $this
                ->useSiteUspQuery()
                ->filterByPrimaryKeys($siteUsp->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteUsp() only accepts arguments of type \Model\Cms\SiteUsp or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteUsp relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinSiteUsp($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteUsp');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteUsp');
        }

        return $this;
    }

    /**
     * Use the SiteUsp relation SiteUsp object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteUspQuery A secondary query class using the current class as primary query
     */
    public function useSiteUspQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteUsp($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteUsp', '\Model\Cms\SiteUspQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteFooterBlock object
     *
     * @param \Model\Cms\SiteFooterBlock|ObjectCollection $siteFooterBlock the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterBySiteFooterBlock($siteFooterBlock, $comparison = null)
    {
        if ($siteFooterBlock instanceof \Model\Cms\SiteFooterBlock) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_ID, $siteFooterBlock->getSiteId(), $comparison);
        } elseif ($siteFooterBlock instanceof ObjectCollection) {
            return $this
                ->useSiteFooterBlockQuery()
                ->filterByPrimaryKeys($siteFooterBlock->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteFooterBlock() only accepts arguments of type \Model\Cms\SiteFooterBlock or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteFooterBlock relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinSiteFooterBlock($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteFooterBlock');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteFooterBlock');
        }

        return $this;
    }

    /**
     * Use the SiteFooterBlock relation SiteFooterBlock object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteFooterBlockQuery A secondary query class using the current class as primary query
     */
    public function useSiteFooterBlockQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteFooterBlock($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteFooterBlock', '\Model\Cms\SiteFooterBlockQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteBlog object
     *
     * @param \Model\Cms\SiteBlog|ObjectCollection $siteBlog the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterBySiteBlog($siteBlog, $comparison = null)
    {
        if ($siteBlog instanceof \Model\Cms\SiteBlog) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_ID, $siteBlog->getSiteId(), $comparison);
        } elseif ($siteBlog instanceof ObjectCollection) {
            return $this
                ->useSiteBlogQuery()
                ->filterByPrimaryKeys($siteBlog->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteBlog() only accepts arguments of type \Model\Cms\SiteBlog or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteBlog relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinSiteBlog($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteBlog');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteBlog');
        }

        return $this;
    }

    /**
     * Use the SiteBlog relation SiteBlog object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteBlogQuery A secondary query class using the current class as primary query
     */
    public function useSiteBlogQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteBlog($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteBlog', '\Model\Cms\SiteBlogQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteBanner object
     *
     * @param \Model\Cms\SiteBanner|ObjectCollection $siteBanner the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterBySiteBanner($siteBanner, $comparison = null)
    {
        if ($siteBanner instanceof \Model\Cms\SiteBanner) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_ID, $siteBanner->getSiteId(), $comparison);
        } elseif ($siteBanner instanceof ObjectCollection) {
            return $this
                ->useSiteBannerQuery()
                ->filterByPrimaryKeys($siteBanner->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteBanner() only accepts arguments of type \Model\Cms\SiteBanner or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteBanner relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinSiteBanner($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteBanner');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteBanner');
        }

        return $this;
    }

    /**
     * Use the SiteBanner relation SiteBanner object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteBannerQuery A secondary query class using the current class as primary query
     */
    public function useSiteBannerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteBanner($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteBanner', '\Model\Cms\SiteBannerQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteFAQCategory object
     *
     * @param \Model\Cms\SiteFAQCategory|ObjectCollection $siteFAQCategory the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterBySiteFAQCategory($siteFAQCategory, $comparison = null)
    {
        if ($siteFAQCategory instanceof \Model\Cms\SiteFAQCategory) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_ID, $siteFAQCategory->getSiteId(), $comparison);
        } elseif ($siteFAQCategory instanceof ObjectCollection) {
            return $this
                ->useSiteFAQCategoryQuery()
                ->filterByPrimaryKeys($siteFAQCategory->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteFAQCategory() only accepts arguments of type \Model\Cms\SiteFAQCategory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteFAQCategory relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinSiteFAQCategory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteFAQCategory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteFAQCategory');
        }

        return $this;
    }

    /**
     * Use the SiteFAQCategory relation SiteFAQCategory object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteFAQCategoryQuery A secondary query class using the current class as primary query
     */
    public function useSiteFAQCategoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteFAQCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteFAQCategory', '\Model\Cms\SiteFAQCategoryQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteFAQ object
     *
     * @param \Model\Cms\SiteFAQ|ObjectCollection $siteFAQ the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterBySiteFAQ($siteFAQ, $comparison = null)
    {
        if ($siteFAQ instanceof \Model\Cms\SiteFAQ) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_ID, $siteFAQ->getSiteId(), $comparison);
        } elseif ($siteFAQ instanceof ObjectCollection) {
            return $this
                ->useSiteFAQQuery()
                ->filterByPrimaryKeys($siteFAQ->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteFAQ() only accepts arguments of type \Model\Cms\SiteFAQ or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteFAQ relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinSiteFAQ($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteFAQ');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteFAQ');
        }

        return $this;
    }

    /**
     * Use the SiteFAQ relation SiteFAQ object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteFAQQuery A secondary query class using the current class as primary query
     */
    public function useSiteFAQQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteFAQ($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteFAQ', '\Model\Cms\SiteFAQQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SitePage object
     *
     * @param \Model\Cms\SitePage|ObjectCollection $sitePage the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterBySitePage($sitePage, $comparison = null)
    {
        if ($sitePage instanceof \Model\Cms\SitePage) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_ID, $sitePage->getSiteId(), $comparison);
        } elseif ($sitePage instanceof ObjectCollection) {
            return $this
                ->useSitePageQuery()
                ->filterByPrimaryKeys($sitePage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySitePage() only accepts arguments of type \Model\Cms\SitePage or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SitePage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinSitePage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SitePage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SitePage');
        }

        return $this;
    }

    /**
     * Use the SitePage relation SitePage object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SitePageQuery A secondary query class using the current class as primary query
     */
    public function useSitePageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSitePage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SitePage', '\Model\Cms\SitePageQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\Mail\MailTemplate object
     *
     * @param \Model\Cms\Mail\MailTemplate|ObjectCollection $mailTemplate the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteQuery The current query, for fluid interface
     */
    public function filterByMailTemplate($mailTemplate, $comparison = null)
    {
        if ($mailTemplate instanceof \Model\Cms\Mail\MailTemplate) {
            return $this
                ->addUsingAlias(SiteTableMap::COL_ID, $mailTemplate->getSiteId(), $comparison);
        } elseif ($mailTemplate instanceof ObjectCollection) {
            return $this
                ->useMailTemplateQuery()
                ->filterByPrimaryKeys($mailTemplate->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMailTemplate() only accepts arguments of type \Model\Cms\Mail\MailTemplate or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MailTemplate relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function joinMailTemplate($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MailTemplate');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MailTemplate');
        }

        return $this;
    }

    /**
     * Use the MailTemplate relation MailTemplate object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\Mail\MailTemplateQuery A secondary query class using the current class as primary query
     */
    public function useMailTemplateQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMailTemplate($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MailTemplate', '\Model\Cms\Mail\MailTemplateQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSite $site Object to remove from the list of results
     *
     * @return $this|ChildSiteQuery The current query, for fluid interface
     */
    public function prune($site = null)
    {
        if ($site) {
            $this->addUsingAlias(SiteTableMap::COL_ID, $site->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the site table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteTableMap::clearInstancePool();
            SiteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 3600)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SiteTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(SiteTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

} // SiteQuery
