<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\Cms\SiteUsp as ChildSiteUsp;
use Model\Cms\SiteUspQuery as ChildSiteUspQuery;
use Model\Cms\Map\SiteUspTableMap;
use Model\Setting\MasterTable\Language;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'site_usp' table.
 *
 *
 *
 * @method     ChildSiteUspQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSiteUspQuery orderBySiteId($order = Criteria::ASC) Order by the site_id column
 * @method     ChildSiteUspQuery orderByLanguageId($order = Criteria::ASC) Order by the language_id column
 * @method     ChildSiteUspQuery orderBySiteHook($order = Criteria::ASC) Order by the site_hook column
 * @method     ChildSiteUspQuery orderByAboutTxt($order = Criteria::ASC) Order by the about_txt column
 * @method     ChildSiteUspQuery orderByShortTxt($order = Criteria::ASC) Order by the short_txt column
 * @method     ChildSiteUspQuery orderByLongTxt($order = Criteria::ASC) Order by the long_txt column
 * @method     ChildSiteUspQuery orderBySlogan($order = Criteria::ASC) Order by the slogan column
 * @method     ChildSiteUspQuery orderByIcon($order = Criteria::ASC) Order by the icon column
 *
 * @method     ChildSiteUspQuery groupById() Group by the id column
 * @method     ChildSiteUspQuery groupBySiteId() Group by the site_id column
 * @method     ChildSiteUspQuery groupByLanguageId() Group by the language_id column
 * @method     ChildSiteUspQuery groupBySiteHook() Group by the site_hook column
 * @method     ChildSiteUspQuery groupByAboutTxt() Group by the about_txt column
 * @method     ChildSiteUspQuery groupByShortTxt() Group by the short_txt column
 * @method     ChildSiteUspQuery groupByLongTxt() Group by the long_txt column
 * @method     ChildSiteUspQuery groupBySlogan() Group by the slogan column
 * @method     ChildSiteUspQuery groupByIcon() Group by the icon column
 *
 * @method     ChildSiteUspQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteUspQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteUspQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteUspQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteUspQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteUspQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteUspQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildSiteUspQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildSiteUspQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildSiteUspQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildSiteUspQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildSiteUspQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildSiteUspQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildSiteUspQuery leftJoinLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Language relation
 * @method     ChildSiteUspQuery rightJoinLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Language relation
 * @method     ChildSiteUspQuery innerJoinLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the Language relation
 *
 * @method     ChildSiteUspQuery joinWithLanguage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Language relation
 *
 * @method     ChildSiteUspQuery leftJoinWithLanguage() Adds a LEFT JOIN clause and with to the query using the Language relation
 * @method     ChildSiteUspQuery rightJoinWithLanguage() Adds a RIGHT JOIN clause and with to the query using the Language relation
 * @method     ChildSiteUspQuery innerJoinWithLanguage() Adds a INNER JOIN clause and with to the query using the Language relation
 *
 * @method     \Model\Cms\SiteQuery|\Model\Setting\MasterTable\LanguageQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSiteUsp findOne(ConnectionInterface $con = null) Return the first ChildSiteUsp matching the query
 * @method     ChildSiteUsp findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSiteUsp matching the query, or a new ChildSiteUsp object populated from the query conditions when no match is found
 *
 * @method     ChildSiteUsp findOneById(int $id) Return the first ChildSiteUsp filtered by the id column
 * @method     ChildSiteUsp findOneBySiteId(int $site_id) Return the first ChildSiteUsp filtered by the site_id column
 * @method     ChildSiteUsp findOneByLanguageId(int $language_id) Return the first ChildSiteUsp filtered by the language_id column
 * @method     ChildSiteUsp findOneBySiteHook(string $site_hook) Return the first ChildSiteUsp filtered by the site_hook column
 * @method     ChildSiteUsp findOneByAboutTxt(string $about_txt) Return the first ChildSiteUsp filtered by the about_txt column
 * @method     ChildSiteUsp findOneByShortTxt(string $short_txt) Return the first ChildSiteUsp filtered by the short_txt column
 * @method     ChildSiteUsp findOneByLongTxt(string $long_txt) Return the first ChildSiteUsp filtered by the long_txt column
 * @method     ChildSiteUsp findOneBySlogan(string $slogan) Return the first ChildSiteUsp filtered by the slogan column
 * @method     ChildSiteUsp findOneByIcon(string $icon) Return the first ChildSiteUsp filtered by the icon column *

 * @method     ChildSiteUsp requirePk($key, ConnectionInterface $con = null) Return the ChildSiteUsp by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteUsp requireOne(ConnectionInterface $con = null) Return the first ChildSiteUsp matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteUsp requireOneById(int $id) Return the first ChildSiteUsp filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteUsp requireOneBySiteId(int $site_id) Return the first ChildSiteUsp filtered by the site_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteUsp requireOneByLanguageId(int $language_id) Return the first ChildSiteUsp filtered by the language_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteUsp requireOneBySiteHook(string $site_hook) Return the first ChildSiteUsp filtered by the site_hook column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteUsp requireOneByAboutTxt(string $about_txt) Return the first ChildSiteUsp filtered by the about_txt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteUsp requireOneByShortTxt(string $short_txt) Return the first ChildSiteUsp filtered by the short_txt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteUsp requireOneByLongTxt(string $long_txt) Return the first ChildSiteUsp filtered by the long_txt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteUsp requireOneBySlogan(string $slogan) Return the first ChildSiteUsp filtered by the slogan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteUsp requireOneByIcon(string $icon) Return the first ChildSiteUsp filtered by the icon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteUsp[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSiteUsp objects based on current ModelCriteria
 * @method     ChildSiteUsp[]|ObjectCollection findById(int $id) Return ChildSiteUsp objects filtered by the id column
 * @method     ChildSiteUsp[]|ObjectCollection findBySiteId(int $site_id) Return ChildSiteUsp objects filtered by the site_id column
 * @method     ChildSiteUsp[]|ObjectCollection findByLanguageId(int $language_id) Return ChildSiteUsp objects filtered by the language_id column
 * @method     ChildSiteUsp[]|ObjectCollection findBySiteHook(string $site_hook) Return ChildSiteUsp objects filtered by the site_hook column
 * @method     ChildSiteUsp[]|ObjectCollection findByAboutTxt(string $about_txt) Return ChildSiteUsp objects filtered by the about_txt column
 * @method     ChildSiteUsp[]|ObjectCollection findByShortTxt(string $short_txt) Return ChildSiteUsp objects filtered by the short_txt column
 * @method     ChildSiteUsp[]|ObjectCollection findByLongTxt(string $long_txt) Return ChildSiteUsp objects filtered by the long_txt column
 * @method     ChildSiteUsp[]|ObjectCollection findBySlogan(string $slogan) Return ChildSiteUsp objects filtered by the slogan column
 * @method     ChildSiteUsp[]|ObjectCollection findByIcon(string $icon) Return ChildSiteUsp objects filtered by the icon column
 * @method     ChildSiteUsp[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteUspQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Cms\Base\SiteUspQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Cms\\SiteUsp', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteUspQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteUspQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteUspQuery) {
            return $criteria;
        }
        $query = new ChildSiteUspQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSiteUsp|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteUspTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteUspTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteUsp A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, site_id, language_id, site_hook, about_txt, short_txt, long_txt, slogan, icon FROM site_usp WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSiteUsp $obj */
            $obj = new ChildSiteUsp();
            $obj->hydrate($row);
            SiteUspTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSiteUsp|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteUspTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteUspTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteUspTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteUspTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteUspTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the site_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE site_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE site_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE site_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(SiteUspTableMap::COL_SITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(SiteUspTableMap::COL_SITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteUspTableMap::COL_SITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the language_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguageId(1234); // WHERE language_id = 1234
     * $query->filterByLanguageId(array(12, 34)); // WHERE language_id IN (12, 34)
     * $query->filterByLanguageId(array('min' => 12)); // WHERE language_id > 12
     * </code>
     *
     * @see       filterByLanguage()
     *
     * @param     mixed $languageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function filterByLanguageId($languageId = null, $comparison = null)
    {
        if (is_array($languageId)) {
            $useMinMax = false;
            if (isset($languageId['min'])) {
                $this->addUsingAlias(SiteUspTableMap::COL_LANGUAGE_ID, $languageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($languageId['max'])) {
                $this->addUsingAlias(SiteUspTableMap::COL_LANGUAGE_ID, $languageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteUspTableMap::COL_LANGUAGE_ID, $languageId, $comparison);
    }

    /**
     * Filter the query on the site_hook column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteHook('fooValue');   // WHERE site_hook = 'fooValue'
     * $query->filterBySiteHook('%fooValue%', Criteria::LIKE); // WHERE site_hook LIKE '%fooValue%'
     * </code>
     *
     * @param     string $siteHook The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function filterBySiteHook($siteHook = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($siteHook)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteUspTableMap::COL_SITE_HOOK, $siteHook, $comparison);
    }

    /**
     * Filter the query on the about_txt column
     *
     * Example usage:
     * <code>
     * $query->filterByAboutTxt('fooValue');   // WHERE about_txt = 'fooValue'
     * $query->filterByAboutTxt('%fooValue%', Criteria::LIKE); // WHERE about_txt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $aboutTxt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function filterByAboutTxt($aboutTxt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($aboutTxt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteUspTableMap::COL_ABOUT_TXT, $aboutTxt, $comparison);
    }

    /**
     * Filter the query on the short_txt column
     *
     * Example usage:
     * <code>
     * $query->filterByShortTxt('fooValue');   // WHERE short_txt = 'fooValue'
     * $query->filterByShortTxt('%fooValue%', Criteria::LIKE); // WHERE short_txt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $shortTxt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function filterByShortTxt($shortTxt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($shortTxt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteUspTableMap::COL_SHORT_TXT, $shortTxt, $comparison);
    }

    /**
     * Filter the query on the long_txt column
     *
     * Example usage:
     * <code>
     * $query->filterByLongTxt('fooValue');   // WHERE long_txt = 'fooValue'
     * $query->filterByLongTxt('%fooValue%', Criteria::LIKE); // WHERE long_txt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $longTxt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function filterByLongTxt($longTxt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($longTxt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteUspTableMap::COL_LONG_TXT, $longTxt, $comparison);
    }

    /**
     * Filter the query on the slogan column
     *
     * Example usage:
     * <code>
     * $query->filterBySlogan('fooValue');   // WHERE slogan = 'fooValue'
     * $query->filterBySlogan('%fooValue%', Criteria::LIKE); // WHERE slogan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slogan The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function filterBySlogan($slogan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slogan)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteUspTableMap::COL_SLOGAN, $slogan, $comparison);
    }

    /**
     * Filter the query on the icon column
     *
     * Example usage:
     * <code>
     * $query->filterByIcon('fooValue');   // WHERE icon = 'fooValue'
     * $query->filterByIcon('%fooValue%', Criteria::LIKE); // WHERE icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $icon The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function filterByIcon($icon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($icon)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteUspTableMap::COL_ICON, $icon, $comparison);
    }

    /**
     * Filter the query by a related \Model\Cms\Site object
     *
     * @param \Model\Cms\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteUspQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \Model\Cms\Site) {
            return $this
                ->addUsingAlias(SiteUspTableMap::COL_SITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteUspTableMap::COL_SITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \Model\Cms\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\Model\Cms\SiteQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Language object
     *
     * @param \Model\Setting\MasterTable\Language|ObjectCollection $language The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteUspQuery The current query, for fluid interface
     */
    public function filterByLanguage($language, $comparison = null)
    {
        if ($language instanceof \Model\Setting\MasterTable\Language) {
            return $this
                ->addUsingAlias(SiteUspTableMap::COL_LANGUAGE_ID, $language->getId(), $comparison);
        } elseif ($language instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteUspTableMap::COL_LANGUAGE_ID, $language->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLanguage() only accepts arguments of type \Model\Setting\MasterTable\Language or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Language relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function joinLanguage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Language');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Language');
        }

        return $this;
    }

    /**
     * Use the Language relation Language object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\LanguageQuery A secondary query class using the current class as primary query
     */
    public function useLanguageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLanguage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Language', '\Model\Setting\MasterTable\LanguageQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSiteUsp $siteUsp Object to remove from the list of results
     *
     * @return $this|ChildSiteUspQuery The current query, for fluid interface
     */
    public function prune($siteUsp = null)
    {
        if ($siteUsp) {
            $this->addUsingAlias(SiteUspTableMap::COL_ID, $siteUsp->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the site_usp table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteUspTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteUspTableMap::clearInstancePool();
            SiteUspTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteUspTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteUspTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteUspTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteUspTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SiteUspQuery
