<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\Cms\Site as ChildSite;
use Model\Cms\SiteFooterBlockTranslationSitePage as ChildSiteFooterBlockTranslationSitePage;
use Model\Cms\SiteFooterBlockTranslationSitePageQuery as ChildSiteFooterBlockTranslationSitePageQuery;
use Model\Cms\SitePage as ChildSitePage;
use Model\Cms\SitePagePicture as ChildSitePagePicture;
use Model\Cms\SitePagePictureQuery as ChildSitePagePictureQuery;
use Model\Cms\SitePageQuery as ChildSitePageQuery;
use Model\Cms\SiteQuery as ChildSiteQuery;
use Model\Cms\Site_navigation as ChildSite_navigation;
use Model\Cms\Site_navigationQuery as ChildSite_navigationQuery;
use Model\Cms\Map\SiteFooterBlockTranslationSitePageTableMap;
use Model\Cms\Map\SitePagePictureTableMap;
use Model\Cms\Map\SitePageTableMap;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'site_page' table.
 *
 *
 *
 * @package    propel.generator.Model.Cms.Base
 */
abstract class SitePage implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Cms\\Map\\SitePageTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the old_id field.
     *
     * @var        string|null
     */
    protected $old_id;

    /**
     * The value for the language_id field.
     *
     * @var        int|null
     */
    protected $language_id;

    /**
     * The value for the navigation_id field.
     *
     * @var        int|null
     */
    protected $navigation_id;

    /**
     * The value for the site_id field.
     *
     * @var        int
     */
    protected $site_id;

    /**
     * The value for the tag field.
     *
     * @var        string|null
     */
    protected $tag;

    /**
     * The value for the url field.
     *
     * @var        string|null
     */
    protected $url;

    /**
     * The value for the has_changeable_url field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean|null
     */
    protected $has_changeable_url;

    /**
     * The value for the title field.
     *
     * @var        string|null
     */
    protected $title;

    /**
     * The value for the title_color field.
     *
     * @var        string|null
     */
    protected $title_color;

    /**
     * The value for the title_background_color field.
     *
     * @var        string|null
     */
    protected $title_background_color;

    /**
     * The value for the meta_description field.
     *
     * @var        string|null
     */
    protected $meta_description;

    /**
     * The value for the meta_keywords field.
     *
     * @var        string|null
     */
    protected $meta_keywords;

    /**
     * The value for the content field.
     *
     * @var        string|null
     */
    protected $content;

    /**
     * The value for the about field.
     *
     * @var        string|null
     */
    protected $about;

    /**
     * The value for the min_pictures field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $min_pictures;

    /**
     * The value for the max_pictures field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $max_pictures;

    /**
     * The value for the title_pos_x field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $title_pos_x;

    /**
     * The value for the title_pos_y field.
     *
     * Note: this column has a database default value of: 0
     * @var        int
     */
    protected $title_pos_y;

    /**
     * The value for the has_title_color_editor field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $has_title_color_editor;

    /**
     * The value for the has_title_backgroundcolor_editor field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $has_title_backgroundcolor_editor;

    /**
     * The value for the has_navigation field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $has_navigation;

    /**
     * The value for the has_title field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $has_title;

    /**
     * The value for the has_title_positioning_fields field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $has_title_positioning_fields;

    /**
     * The value for the has_content field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $has_content;

    /**
     * The value for the has_url field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $has_url;

    /**
     * The value for the has_meta_description field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $has_meta_description;

    /**
     * The value for the has_meta_keywords field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $has_meta_keywords;

    /**
     * The value for the is_deletable field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $is_deletable;

    /**
     * The value for the is_editable field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $is_editable;

    /**
     * The value for the is_visible field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean
     */
    protected $is_visible;

    /**
     * The value for the has_cover_image field.
     *
     * @var        boolean|null
     */
    protected $has_cover_image;

    /**
     * The value for the cover_image_ext field.
     *
     * @var        string|null
     */
    protected $cover_image_ext;

    /**
     * @var        ChildSite
     */
    protected $aSite;

    /**
     * @var        ChildSite_navigation
     */
    protected $aSiteNavigaton;

    /**
     * @var        Language
     */
    protected $aLanguage;

    /**
     * @var        ObjectCollection|ChildSiteFooterBlockTranslationSitePage[] Collection to store aggregation of ChildSiteFooterBlockTranslationSitePage objects.
     */
    protected $collSiteFooterBlockTranslationSitePages;
    protected $collSiteFooterBlockTranslationSitePagesPartial;

    /**
     * @var        ObjectCollection|ChildSitePagePicture[] Collection to store aggregation of ChildSitePagePicture objects.
     */
    protected $collSitePagePictures;
    protected $collSitePagePicturesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSiteFooterBlockTranslationSitePage[]
     */
    protected $siteFooterBlockTranslationSitePagesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSitePagePicture[]
     */
    protected $sitePagePicturesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->has_changeable_url = true;
        $this->min_pictures = 0;
        $this->max_pictures = 0;
        $this->title_pos_x = 0;
        $this->title_pos_y = 0;
        $this->has_title_color_editor = true;
        $this->has_title_backgroundcolor_editor = true;
        $this->has_navigation = true;
        $this->has_title = true;
        $this->has_title_positioning_fields = false;
        $this->has_content = true;
        $this->has_url = true;
        $this->has_meta_description = true;
        $this->has_meta_keywords = true;
        $this->is_deletable = true;
        $this->is_editable = true;
        $this->is_visible = true;
    }

    /**
     * Initializes internal state of Model\Cms\Base\SitePage object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>SitePage</code> instance.  If
     * <code>obj</code> is an instance of <code>SitePage</code>, delegates to
     * <code>equals(SitePage)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [old_id] column value.
     *
     * @return string|null
     */
    public function getOldId()
    {
        return $this->old_id;
    }

    /**
     * Get the [language_id] column value.
     *
     * @return int|null
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * Get the [navigation_id] column value.
     *
     * @return int|null
     */
    public function getNavigationId()
    {
        return $this->navigation_id;
    }

    /**
     * Get the [site_id] column value.
     *
     * @return int
     */
    public function getSiteId()
    {
        return $this->site_id;
    }

    /**
     * Get the [tag] column value.
     *
     * @return string|null
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Get the [url] column value.
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the [has_changeable_url] column value.
     *
     * @return boolean|null
     */
    public function getHasChangeableUrl()
    {
        return $this->has_changeable_url;
    }

    /**
     * Get the [has_changeable_url] column value.
     *
     * @return boolean|null
     */
    public function hasChangeableUrl()
    {
        return $this->getHasChangeableUrl();
    }

    /**
     * Get the [title] column value.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [title_color] column value.
     *
     * @return string|null
     */
    public function getTitleColor()
    {
        return $this->title_color;
    }

    /**
     * Get the [title_background_color] column value.
     *
     * @return string|null
     */
    public function getTitleBackgroundColor()
    {
        return $this->title_background_color;
    }

    /**
     * Get the [meta_description] column value.
     *
     * @return string|null
     */
    public function getMetaDescription()
    {
        return $this->meta_description;
    }

    /**
     * Get the [meta_keywords] column value.
     *
     * @return string|null
     */
    public function getMetaKeywords()
    {
        return $this->meta_keywords;
    }

    /**
     * Get the [content] column value.
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Get the [about] column value.
     *
     * @return string|null
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Get the [min_pictures] column value.
     *
     * @return int
     */
    public function getMinPictures()
    {
        return $this->min_pictures;
    }

    /**
     * Get the [max_pictures] column value.
     *
     * @return int
     */
    public function getMaxPictures()
    {
        return $this->max_pictures;
    }

    /**
     * Get the [title_pos_x] column value.
     *
     * @return int
     */
    public function getTitlePosX()
    {
        return $this->title_pos_x;
    }

    /**
     * Get the [title_pos_y] column value.
     *
     * @return int
     */
    public function getTitlePosY()
    {
        return $this->title_pos_y;
    }

    /**
     * Get the [has_title_color_editor] column value.
     *
     * @return boolean
     */
    public function getHasTitleColorEditor()
    {
        return $this->has_title_color_editor;
    }

    /**
     * Get the [has_title_color_editor] column value.
     *
     * @return boolean
     */
    public function hasTitleColorEditor()
    {
        return $this->getHasTitleColorEditor();
    }

    /**
     * Get the [has_title_backgroundcolor_editor] column value.
     *
     * @return boolean
     */
    public function getHasTitleBackgroundcolorEditor()
    {
        return $this->has_title_backgroundcolor_editor;
    }

    /**
     * Get the [has_title_backgroundcolor_editor] column value.
     *
     * @return boolean
     */
    public function hasTitleBackgroundcolorEditor()
    {
        return $this->getHasTitleBackgroundcolorEditor();
    }

    /**
     * Get the [has_navigation] column value.
     *
     * @return boolean
     */
    public function getHasNavigation()
    {
        return $this->has_navigation;
    }

    /**
     * Get the [has_navigation] column value.
     *
     * @return boolean
     */
    public function hasNavigation()
    {
        return $this->getHasNavigation();
    }

    /**
     * Get the [has_title] column value.
     *
     * @return boolean
     */
    public function getHasTitle()
    {
        return $this->has_title;
    }

    /**
     * Get the [has_title] column value.
     *
     * @return boolean
     */
    public function hasTitle()
    {
        return $this->getHasTitle();
    }

    /**
     * Get the [has_title_positioning_fields] column value.
     *
     * @return boolean
     */
    public function getHasTitlePositioningFields()
    {
        return $this->has_title_positioning_fields;
    }

    /**
     * Get the [has_title_positioning_fields] column value.
     *
     * @return boolean
     */
    public function hasTitlePositioningFields()
    {
        return $this->getHasTitlePositioningFields();
    }

    /**
     * Get the [has_content] column value.
     *
     * @return boolean
     */
    public function getHasContent()
    {
        return $this->has_content;
    }

    /**
     * Get the [has_content] column value.
     *
     * @return boolean
     */
    public function hasContent()
    {
        return $this->getHasContent();
    }

    /**
     * Get the [has_url] column value.
     *
     * @return boolean
     */
    public function getHasUrl()
    {
        return $this->has_url;
    }

    /**
     * Get the [has_url] column value.
     *
     * @return boolean
     */
    public function hasUrl()
    {
        return $this->getHasUrl();
    }

    /**
     * Get the [has_meta_description] column value.
     *
     * @return boolean
     */
    public function getHasMetaDescription()
    {
        return $this->has_meta_description;
    }

    /**
     * Get the [has_meta_description] column value.
     *
     * @return boolean
     */
    public function hasMetaDescription()
    {
        return $this->getHasMetaDescription();
    }

    /**
     * Get the [has_meta_keywords] column value.
     *
     * @return boolean
     */
    public function getHasMetaKeywords()
    {
        return $this->has_meta_keywords;
    }

    /**
     * Get the [has_meta_keywords] column value.
     *
     * @return boolean
     */
    public function hasMetaKeywords()
    {
        return $this->getHasMetaKeywords();
    }

    /**
     * Get the [is_deletable] column value.
     *
     * @return boolean
     */
    public function getIsDeletable()
    {
        return $this->is_deletable;
    }

    /**
     * Get the [is_deletable] column value.
     *
     * @return boolean
     */
    public function isDeletable()
    {
        return $this->getIsDeletable();
    }

    /**
     * Get the [is_editable] column value.
     *
     * @return boolean
     */
    public function getIsEditable()
    {
        return $this->is_editable;
    }

    /**
     * Get the [is_editable] column value.
     *
     * @return boolean
     */
    public function isEditable()
    {
        return $this->getIsEditable();
    }

    /**
     * Get the [is_visible] column value.
     *
     * @return boolean
     */
    public function getIsVisible()
    {
        return $this->is_visible;
    }

    /**
     * Get the [is_visible] column value.
     *
     * @return boolean
     */
    public function isVisible()
    {
        return $this->getIsVisible();
    }

    /**
     * Get the [has_cover_image] column value.
     *
     * @return boolean|null
     */
    public function getHasCoverImage()
    {
        return $this->has_cover_image;
    }

    /**
     * Get the [has_cover_image] column value.
     *
     * @return boolean|null
     */
    public function hasCoverImage()
    {
        return $this->getHasCoverImage();
    }

    /**
     * Get the [cover_image_ext] column value.
     *
     * @return string|null
     */
    public function getCoverImageExt()
    {
        return $this->cover_image_ext;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[SitePageTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [old_id] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setOldId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->old_id !== $v) {
            $this->old_id = $v;
            $this->modifiedColumns[SitePageTableMap::COL_OLD_ID] = true;
        }

        return $this;
    } // setOldId()

    /**
     * Set the value of [language_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setLanguageId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->language_id !== $v) {
            $this->language_id = $v;
            $this->modifiedColumns[SitePageTableMap::COL_LANGUAGE_ID] = true;
        }

        if ($this->aLanguage !== null && $this->aLanguage->getId() !== $v) {
            $this->aLanguage = null;
        }

        return $this;
    } // setLanguageId()

    /**
     * Set the value of [navigation_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setNavigationId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->navigation_id !== $v) {
            $this->navigation_id = $v;
            $this->modifiedColumns[SitePageTableMap::COL_NAVIGATION_ID] = true;
        }

        if ($this->aSiteNavigaton !== null && $this->aSiteNavigaton->getId() !== $v) {
            $this->aSiteNavigaton = null;
        }

        return $this;
    } // setNavigationId()

    /**
     * Set the value of [site_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setSiteId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->site_id !== $v) {
            $this->site_id = $v;
            $this->modifiedColumns[SitePageTableMap::COL_SITE_ID] = true;
        }

        if ($this->aSite !== null && $this->aSite->getId() !== $v) {
            $this->aSite = null;
        }

        return $this;
    } // setSiteId()

    /**
     * Set the value of [tag] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setTag($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->tag !== $v) {
            $this->tag = $v;
            $this->modifiedColumns[SitePageTableMap::COL_TAG] = true;
        }

        return $this;
    } // setTag()

    /**
     * Set the value of [url] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[SitePageTableMap::COL_URL] = true;
        }

        return $this;
    } // setUrl()

    /**
     * Sets the value of the [has_changeable_url] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setHasChangeableUrl($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_changeable_url !== $v) {
            $this->has_changeable_url = $v;
            $this->modifiedColumns[SitePageTableMap::COL_HAS_CHANGEABLE_URL] = true;
        }

        return $this;
    } // setHasChangeableUrl()

    /**
     * Set the value of [title] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[SitePageTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [title_color] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setTitleColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title_color !== $v) {
            $this->title_color = $v;
            $this->modifiedColumns[SitePageTableMap::COL_TITLE_COLOR] = true;
        }

        return $this;
    } // setTitleColor()

    /**
     * Set the value of [title_background_color] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setTitleBackgroundColor($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title_background_color !== $v) {
            $this->title_background_color = $v;
            $this->modifiedColumns[SitePageTableMap::COL_TITLE_BACKGROUND_COLOR] = true;
        }

        return $this;
    } // setTitleBackgroundColor()

    /**
     * Set the value of [meta_description] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setMetaDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->meta_description !== $v) {
            $this->meta_description = $v;
            $this->modifiedColumns[SitePageTableMap::COL_META_DESCRIPTION] = true;
        }

        return $this;
    } // setMetaDescription()

    /**
     * Set the value of [meta_keywords] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setMetaKeywords($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->meta_keywords !== $v) {
            $this->meta_keywords = $v;
            $this->modifiedColumns[SitePageTableMap::COL_META_KEYWORDS] = true;
        }

        return $this;
    } // setMetaKeywords()

    /**
     * Set the value of [content] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setContent($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->content !== $v) {
            $this->content = $v;
            $this->modifiedColumns[SitePageTableMap::COL_CONTENT] = true;
        }

        return $this;
    } // setContent()

    /**
     * Set the value of [about] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setAbout($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->about !== $v) {
            $this->about = $v;
            $this->modifiedColumns[SitePageTableMap::COL_ABOUT] = true;
        }

        return $this;
    } // setAbout()

    /**
     * Set the value of [min_pictures] column.
     *
     * @param int $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setMinPictures($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->min_pictures !== $v) {
            $this->min_pictures = $v;
            $this->modifiedColumns[SitePageTableMap::COL_MIN_PICTURES] = true;
        }

        return $this;
    } // setMinPictures()

    /**
     * Set the value of [max_pictures] column.
     *
     * @param int $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setMaxPictures($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->max_pictures !== $v) {
            $this->max_pictures = $v;
            $this->modifiedColumns[SitePageTableMap::COL_MAX_PICTURES] = true;
        }

        return $this;
    } // setMaxPictures()

    /**
     * Set the value of [title_pos_x] column.
     *
     * @param int $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setTitlePosX($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->title_pos_x !== $v) {
            $this->title_pos_x = $v;
            $this->modifiedColumns[SitePageTableMap::COL_TITLE_POS_X] = true;
        }

        return $this;
    } // setTitlePosX()

    /**
     * Set the value of [title_pos_y] column.
     *
     * @param int $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setTitlePosY($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->title_pos_y !== $v) {
            $this->title_pos_y = $v;
            $this->modifiedColumns[SitePageTableMap::COL_TITLE_POS_Y] = true;
        }

        return $this;
    } // setTitlePosY()

    /**
     * Sets the value of the [has_title_color_editor] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setHasTitleColorEditor($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_title_color_editor !== $v) {
            $this->has_title_color_editor = $v;
            $this->modifiedColumns[SitePageTableMap::COL_HAS_TITLE_COLOR_EDITOR] = true;
        }

        return $this;
    } // setHasTitleColorEditor()

    /**
     * Sets the value of the [has_title_backgroundcolor_editor] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setHasTitleBackgroundcolorEditor($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_title_backgroundcolor_editor !== $v) {
            $this->has_title_backgroundcolor_editor = $v;
            $this->modifiedColumns[SitePageTableMap::COL_HAS_TITLE_BACKGROUNDCOLOR_EDITOR] = true;
        }

        return $this;
    } // setHasTitleBackgroundcolorEditor()

    /**
     * Sets the value of the [has_navigation] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setHasNavigation($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_navigation !== $v) {
            $this->has_navigation = $v;
            $this->modifiedColumns[SitePageTableMap::COL_HAS_NAVIGATION] = true;
        }

        return $this;
    } // setHasNavigation()

    /**
     * Sets the value of the [has_title] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setHasTitle($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_title !== $v) {
            $this->has_title = $v;
            $this->modifiedColumns[SitePageTableMap::COL_HAS_TITLE] = true;
        }

        return $this;
    } // setHasTitle()

    /**
     * Sets the value of the [has_title_positioning_fields] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setHasTitlePositioningFields($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_title_positioning_fields !== $v) {
            $this->has_title_positioning_fields = $v;
            $this->modifiedColumns[SitePageTableMap::COL_HAS_TITLE_POSITIONING_FIELDS] = true;
        }

        return $this;
    } // setHasTitlePositioningFields()

    /**
     * Sets the value of the [has_content] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setHasContent($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_content !== $v) {
            $this->has_content = $v;
            $this->modifiedColumns[SitePageTableMap::COL_HAS_CONTENT] = true;
        }

        return $this;
    } // setHasContent()

    /**
     * Sets the value of the [has_url] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setHasUrl($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_url !== $v) {
            $this->has_url = $v;
            $this->modifiedColumns[SitePageTableMap::COL_HAS_URL] = true;
        }

        return $this;
    } // setHasUrl()

    /**
     * Sets the value of the [has_meta_description] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setHasMetaDescription($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_meta_description !== $v) {
            $this->has_meta_description = $v;
            $this->modifiedColumns[SitePageTableMap::COL_HAS_META_DESCRIPTION] = true;
        }

        return $this;
    } // setHasMetaDescription()

    /**
     * Sets the value of the [has_meta_keywords] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setHasMetaKeywords($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_meta_keywords !== $v) {
            $this->has_meta_keywords = $v;
            $this->modifiedColumns[SitePageTableMap::COL_HAS_META_KEYWORDS] = true;
        }

        return $this;
    } // setHasMetaKeywords()

    /**
     * Sets the value of the [is_deletable] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setIsDeletable($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_deletable !== $v) {
            $this->is_deletable = $v;
            $this->modifiedColumns[SitePageTableMap::COL_IS_DELETABLE] = true;
        }

        return $this;
    } // setIsDeletable()

    /**
     * Sets the value of the [is_editable] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setIsEditable($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_editable !== $v) {
            $this->is_editable = $v;
            $this->modifiedColumns[SitePageTableMap::COL_IS_EDITABLE] = true;
        }

        return $this;
    } // setIsEditable()

    /**
     * Sets the value of the [is_visible] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setIsVisible($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_visible !== $v) {
            $this->is_visible = $v;
            $this->modifiedColumns[SitePageTableMap::COL_IS_VISIBLE] = true;
        }

        return $this;
    } // setIsVisible()

    /**
     * Sets the value of the [has_cover_image] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setHasCoverImage($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_cover_image !== $v) {
            $this->has_cover_image = $v;
            $this->modifiedColumns[SitePageTableMap::COL_HAS_COVER_IMAGE] = true;
        }

        return $this;
    } // setHasCoverImage()

    /**
     * Set the value of [cover_image_ext] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function setCoverImageExt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cover_image_ext !== $v) {
            $this->cover_image_ext = $v;
            $this->modifiedColumns[SitePageTableMap::COL_COVER_IMAGE_EXT] = true;
        }

        return $this;
    } // setCoverImageExt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->has_changeable_url !== true) {
                return false;
            }

            if ($this->min_pictures !== 0) {
                return false;
            }

            if ($this->max_pictures !== 0) {
                return false;
            }

            if ($this->title_pos_x !== 0) {
                return false;
            }

            if ($this->title_pos_y !== 0) {
                return false;
            }

            if ($this->has_title_color_editor !== true) {
                return false;
            }

            if ($this->has_title_backgroundcolor_editor !== true) {
                return false;
            }

            if ($this->has_navigation !== true) {
                return false;
            }

            if ($this->has_title !== true) {
                return false;
            }

            if ($this->has_title_positioning_fields !== false) {
                return false;
            }

            if ($this->has_content !== true) {
                return false;
            }

            if ($this->has_url !== true) {
                return false;
            }

            if ($this->has_meta_description !== true) {
                return false;
            }

            if ($this->has_meta_keywords !== true) {
                return false;
            }

            if ($this->is_deletable !== true) {
                return false;
            }

            if ($this->is_editable !== true) {
                return false;
            }

            if ($this->is_visible !== true) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SitePageTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SitePageTableMap::translateFieldName('OldId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->old_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SitePageTableMap::translateFieldName('LanguageId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->language_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SitePageTableMap::translateFieldName('NavigationId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->navigation_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : SitePageTableMap::translateFieldName('SiteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->site_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : SitePageTableMap::translateFieldName('Tag', TableMap::TYPE_PHPNAME, $indexType)];
            $this->tag = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : SitePageTableMap::translateFieldName('Url', TableMap::TYPE_PHPNAME, $indexType)];
            $this->url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : SitePageTableMap::translateFieldName('HasChangeableUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_changeable_url = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : SitePageTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : SitePageTableMap::translateFieldName('TitleColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : SitePageTableMap::translateFieldName('TitleBackgroundColor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title_background_color = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : SitePageTableMap::translateFieldName('MetaDescription', TableMap::TYPE_PHPNAME, $indexType)];
            $this->meta_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : SitePageTableMap::translateFieldName('MetaKeywords', TableMap::TYPE_PHPNAME, $indexType)];
            $this->meta_keywords = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : SitePageTableMap::translateFieldName('Content', TableMap::TYPE_PHPNAME, $indexType)];
            $this->content = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : SitePageTableMap::translateFieldName('About', TableMap::TYPE_PHPNAME, $indexType)];
            $this->about = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : SitePageTableMap::translateFieldName('MinPictures', TableMap::TYPE_PHPNAME, $indexType)];
            $this->min_pictures = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : SitePageTableMap::translateFieldName('MaxPictures', TableMap::TYPE_PHPNAME, $indexType)];
            $this->max_pictures = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : SitePageTableMap::translateFieldName('TitlePosX', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title_pos_x = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : SitePageTableMap::translateFieldName('TitlePosY', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title_pos_y = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : SitePageTableMap::translateFieldName('HasTitleColorEditor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_title_color_editor = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : SitePageTableMap::translateFieldName('HasTitleBackgroundcolorEditor', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_title_backgroundcolor_editor = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : SitePageTableMap::translateFieldName('HasNavigation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_navigation = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : SitePageTableMap::translateFieldName('HasTitle', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_title = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : SitePageTableMap::translateFieldName('HasTitlePositioningFields', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_title_positioning_fields = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : SitePageTableMap::translateFieldName('HasContent', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_content = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : SitePageTableMap::translateFieldName('HasUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_url = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : SitePageTableMap::translateFieldName('HasMetaDescription', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_meta_description = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : SitePageTableMap::translateFieldName('HasMetaKeywords', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_meta_keywords = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : SitePageTableMap::translateFieldName('IsDeletable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_deletable = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : SitePageTableMap::translateFieldName('IsEditable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_editable = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : SitePageTableMap::translateFieldName('IsVisible', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_visible = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 31 + $startcol : SitePageTableMap::translateFieldName('HasCoverImage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_cover_image = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 32 + $startcol : SitePageTableMap::translateFieldName('CoverImageExt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cover_image_ext = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 33; // 33 = SitePageTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Cms\\SitePage'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aLanguage !== null && $this->language_id !== $this->aLanguage->getId()) {
            $this->aLanguage = null;
        }
        if ($this->aSiteNavigaton !== null && $this->navigation_id !== $this->aSiteNavigaton->getId()) {
            $this->aSiteNavigaton = null;
        }
        if ($this->aSite !== null && $this->site_id !== $this->aSite->getId()) {
            $this->aSite = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SitePageTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSitePageQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSite = null;
            $this->aSiteNavigaton = null;
            $this->aLanguage = null;
            $this->collSiteFooterBlockTranslationSitePages = null;

            $this->collSitePagePictures = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see SitePage::setDeleted()
     * @see SitePage::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SitePageTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSitePageQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SitePageTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SitePageTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSite !== null) {
                if ($this->aSite->isModified() || $this->aSite->isNew()) {
                    $affectedRows += $this->aSite->save($con);
                }
                $this->setSite($this->aSite);
            }

            if ($this->aSiteNavigaton !== null) {
                if ($this->aSiteNavigaton->isModified() || $this->aSiteNavigaton->isNew()) {
                    $affectedRows += $this->aSiteNavigaton->save($con);
                }
                $this->setSiteNavigaton($this->aSiteNavigaton);
            }

            if ($this->aLanguage !== null) {
                if ($this->aLanguage->isModified() || $this->aLanguage->isNew()) {
                    $affectedRows += $this->aLanguage->save($con);
                }
                $this->setLanguage($this->aLanguage);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->siteFooterBlockTranslationSitePagesScheduledForDeletion !== null) {
                if (!$this->siteFooterBlockTranslationSitePagesScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SiteFooterBlockTranslationSitePageQuery::create()
                        ->filterByPrimaryKeys($this->siteFooterBlockTranslationSitePagesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteFooterBlockTranslationSitePagesScheduledForDeletion = null;
                }
            }

            if ($this->collSiteFooterBlockTranslationSitePages !== null) {
                foreach ($this->collSiteFooterBlockTranslationSitePages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sitePagePicturesScheduledForDeletion !== null) {
                if (!$this->sitePagePicturesScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SitePagePictureQuery::create()
                        ->filterByPrimaryKeys($this->sitePagePicturesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sitePagePicturesScheduledForDeletion = null;
                }
            }

            if ($this->collSitePagePictures !== null) {
                foreach ($this->collSitePagePictures as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[SitePageTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SitePageTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SitePageTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_OLD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'old_id';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_LANGUAGE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'language_id';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_NAVIGATION_ID)) {
            $modifiedColumns[':p' . $index++]  = 'navigation_id';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_SITE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'site_id';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_TAG)) {
            $modifiedColumns[':p' . $index++]  = 'tag';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'url';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_CHANGEABLE_URL)) {
            $modifiedColumns[':p' . $index++]  = 'has_changeable_url';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_TITLE_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'title_color';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_TITLE_BACKGROUND_COLOR)) {
            $modifiedColumns[':p' . $index++]  = 'title_background_color';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_META_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'meta_description';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_META_KEYWORDS)) {
            $modifiedColumns[':p' . $index++]  = 'meta_keywords';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_CONTENT)) {
            $modifiedColumns[':p' . $index++]  = 'content';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_ABOUT)) {
            $modifiedColumns[':p' . $index++]  = 'about';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_MIN_PICTURES)) {
            $modifiedColumns[':p' . $index++]  = 'min_pictures';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_MAX_PICTURES)) {
            $modifiedColumns[':p' . $index++]  = 'max_pictures';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_TITLE_POS_X)) {
            $modifiedColumns[':p' . $index++]  = 'title_pos_x';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_TITLE_POS_Y)) {
            $modifiedColumns[':p' . $index++]  = 'title_pos_y';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_TITLE_COLOR_EDITOR)) {
            $modifiedColumns[':p' . $index++]  = 'has_title_color_editor';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_TITLE_BACKGROUNDCOLOR_EDITOR)) {
            $modifiedColumns[':p' . $index++]  = 'has_title_backgroundcolor_editor';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_NAVIGATION)) {
            $modifiedColumns[':p' . $index++]  = 'has_navigation';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'has_title';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_TITLE_POSITIONING_FIELDS)) {
            $modifiedColumns[':p' . $index++]  = 'has_title_positioning_fields';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_CONTENT)) {
            $modifiedColumns[':p' . $index++]  = 'has_content';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_URL)) {
            $modifiedColumns[':p' . $index++]  = 'has_url';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_META_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'has_meta_description';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_META_KEYWORDS)) {
            $modifiedColumns[':p' . $index++]  = 'has_meta_keywords';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_IS_DELETABLE)) {
            $modifiedColumns[':p' . $index++]  = 'is_deletable';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_IS_EDITABLE)) {
            $modifiedColumns[':p' . $index++]  = 'is_editable';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_IS_VISIBLE)) {
            $modifiedColumns[':p' . $index++]  = 'is_visible';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_COVER_IMAGE)) {
            $modifiedColumns[':p' . $index++]  = 'has_cover_image';
        }
        if ($this->isColumnModified(SitePageTableMap::COL_COVER_IMAGE_EXT)) {
            $modifiedColumns[':p' . $index++]  = 'cover_image_ext';
        }

        $sql = sprintf(
            'INSERT INTO site_page (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'old_id':
                        $stmt->bindValue($identifier, $this->old_id, PDO::PARAM_STR);
                        break;
                    case 'language_id':
                        $stmt->bindValue($identifier, $this->language_id, PDO::PARAM_INT);
                        break;
                    case 'navigation_id':
                        $stmt->bindValue($identifier, $this->navigation_id, PDO::PARAM_INT);
                        break;
                    case 'site_id':
                        $stmt->bindValue($identifier, $this->site_id, PDO::PARAM_INT);
                        break;
                    case 'tag':
                        $stmt->bindValue($identifier, $this->tag, PDO::PARAM_STR);
                        break;
                    case 'url':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case 'has_changeable_url':
                        $stmt->bindValue($identifier, (int) $this->has_changeable_url, PDO::PARAM_INT);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'title_color':
                        $stmt->bindValue($identifier, $this->title_color, PDO::PARAM_STR);
                        break;
                    case 'title_background_color':
                        $stmt->bindValue($identifier, $this->title_background_color, PDO::PARAM_STR);
                        break;
                    case 'meta_description':
                        $stmt->bindValue($identifier, $this->meta_description, PDO::PARAM_STR);
                        break;
                    case 'meta_keywords':
                        $stmt->bindValue($identifier, $this->meta_keywords, PDO::PARAM_STR);
                        break;
                    case 'content':
                        $stmt->bindValue($identifier, $this->content, PDO::PARAM_STR);
                        break;
                    case 'about':
                        $stmt->bindValue($identifier, $this->about, PDO::PARAM_STR);
                        break;
                    case 'min_pictures':
                        $stmt->bindValue($identifier, $this->min_pictures, PDO::PARAM_INT);
                        break;
                    case 'max_pictures':
                        $stmt->bindValue($identifier, $this->max_pictures, PDO::PARAM_INT);
                        break;
                    case 'title_pos_x':
                        $stmt->bindValue($identifier, $this->title_pos_x, PDO::PARAM_INT);
                        break;
                    case 'title_pos_y':
                        $stmt->bindValue($identifier, $this->title_pos_y, PDO::PARAM_INT);
                        break;
                    case 'has_title_color_editor':
                        $stmt->bindValue($identifier, (int) $this->has_title_color_editor, PDO::PARAM_INT);
                        break;
                    case 'has_title_backgroundcolor_editor':
                        $stmt->bindValue($identifier, (int) $this->has_title_backgroundcolor_editor, PDO::PARAM_INT);
                        break;
                    case 'has_navigation':
                        $stmt->bindValue($identifier, (int) $this->has_navigation, PDO::PARAM_INT);
                        break;
                    case 'has_title':
                        $stmt->bindValue($identifier, (int) $this->has_title, PDO::PARAM_INT);
                        break;
                    case 'has_title_positioning_fields':
                        $stmt->bindValue($identifier, (int) $this->has_title_positioning_fields, PDO::PARAM_INT);
                        break;
                    case 'has_content':
                        $stmt->bindValue($identifier, (int) $this->has_content, PDO::PARAM_INT);
                        break;
                    case 'has_url':
                        $stmt->bindValue($identifier, (int) $this->has_url, PDO::PARAM_INT);
                        break;
                    case 'has_meta_description':
                        $stmt->bindValue($identifier, (int) $this->has_meta_description, PDO::PARAM_INT);
                        break;
                    case 'has_meta_keywords':
                        $stmt->bindValue($identifier, (int) $this->has_meta_keywords, PDO::PARAM_INT);
                        break;
                    case 'is_deletable':
                        $stmt->bindValue($identifier, (int) $this->is_deletable, PDO::PARAM_INT);
                        break;
                    case 'is_editable':
                        $stmt->bindValue($identifier, (int) $this->is_editable, PDO::PARAM_INT);
                        break;
                    case 'is_visible':
                        $stmt->bindValue($identifier, (int) $this->is_visible, PDO::PARAM_INT);
                        break;
                    case 'has_cover_image':
                        $stmt->bindValue($identifier, (int) $this->has_cover_image, PDO::PARAM_INT);
                        break;
                    case 'cover_image_ext':
                        $stmt->bindValue($identifier, $this->cover_image_ext, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SitePageTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getOldId();
                break;
            case 2:
                return $this->getLanguageId();
                break;
            case 3:
                return $this->getNavigationId();
                break;
            case 4:
                return $this->getSiteId();
                break;
            case 5:
                return $this->getTag();
                break;
            case 6:
                return $this->getUrl();
                break;
            case 7:
                return $this->getHasChangeableUrl();
                break;
            case 8:
                return $this->getTitle();
                break;
            case 9:
                return $this->getTitleColor();
                break;
            case 10:
                return $this->getTitleBackgroundColor();
                break;
            case 11:
                return $this->getMetaDescription();
                break;
            case 12:
                return $this->getMetaKeywords();
                break;
            case 13:
                return $this->getContent();
                break;
            case 14:
                return $this->getAbout();
                break;
            case 15:
                return $this->getMinPictures();
                break;
            case 16:
                return $this->getMaxPictures();
                break;
            case 17:
                return $this->getTitlePosX();
                break;
            case 18:
                return $this->getTitlePosY();
                break;
            case 19:
                return $this->getHasTitleColorEditor();
                break;
            case 20:
                return $this->getHasTitleBackgroundcolorEditor();
                break;
            case 21:
                return $this->getHasNavigation();
                break;
            case 22:
                return $this->getHasTitle();
                break;
            case 23:
                return $this->getHasTitlePositioningFields();
                break;
            case 24:
                return $this->getHasContent();
                break;
            case 25:
                return $this->getHasUrl();
                break;
            case 26:
                return $this->getHasMetaDescription();
                break;
            case 27:
                return $this->getHasMetaKeywords();
                break;
            case 28:
                return $this->getIsDeletable();
                break;
            case 29:
                return $this->getIsEditable();
                break;
            case 30:
                return $this->getIsVisible();
                break;
            case 31:
                return $this->getHasCoverImage();
                break;
            case 32:
                return $this->getCoverImageExt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['SitePage'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SitePage'][$this->hashCode()] = true;
        $keys = SitePageTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getOldId(),
            $keys[2] => $this->getLanguageId(),
            $keys[3] => $this->getNavigationId(),
            $keys[4] => $this->getSiteId(),
            $keys[5] => $this->getTag(),
            $keys[6] => $this->getUrl(),
            $keys[7] => $this->getHasChangeableUrl(),
            $keys[8] => $this->getTitle(),
            $keys[9] => $this->getTitleColor(),
            $keys[10] => $this->getTitleBackgroundColor(),
            $keys[11] => $this->getMetaDescription(),
            $keys[12] => $this->getMetaKeywords(),
            $keys[13] => $this->getContent(),
            $keys[14] => $this->getAbout(),
            $keys[15] => $this->getMinPictures(),
            $keys[16] => $this->getMaxPictures(),
            $keys[17] => $this->getTitlePosX(),
            $keys[18] => $this->getTitlePosY(),
            $keys[19] => $this->getHasTitleColorEditor(),
            $keys[20] => $this->getHasTitleBackgroundcolorEditor(),
            $keys[21] => $this->getHasNavigation(),
            $keys[22] => $this->getHasTitle(),
            $keys[23] => $this->getHasTitlePositioningFields(),
            $keys[24] => $this->getHasContent(),
            $keys[25] => $this->getHasUrl(),
            $keys[26] => $this->getHasMetaDescription(),
            $keys[27] => $this->getHasMetaKeywords(),
            $keys[28] => $this->getIsDeletable(),
            $keys[29] => $this->getIsEditable(),
            $keys[30] => $this->getIsVisible(),
            $keys[31] => $this->getHasCoverImage(),
            $keys[32] => $this->getCoverImageExt(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSite) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'site';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site';
                        break;
                    default:
                        $key = 'Site';
                }

                $result[$key] = $this->aSite->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSiteNavigaton) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'site_navigation';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_navigation';
                        break;
                    default:
                        $key = 'SiteNavigaton';
                }

                $result[$key] = $this->aSiteNavigaton->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aLanguage) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'language';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_language';
                        break;
                    default:
                        $key = 'Language';
                }

                $result[$key] = $this->aLanguage->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSiteFooterBlockTranslationSitePages) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteFooterBlockTranslationSitePages';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_footer_block_translation_site_pages';
                        break;
                    default:
                        $key = 'SiteFooterBlockTranslationSitePages';
                }

                $result[$key] = $this->collSiteFooterBlockTranslationSitePages->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSitePagePictures) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sitePagePictures';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_page_pictures';
                        break;
                    default:
                        $key = 'SitePagePictures';
                }

                $result[$key] = $this->collSitePagePictures->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Cms\SitePage
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SitePageTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Cms\SitePage
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setOldId($value);
                break;
            case 2:
                $this->setLanguageId($value);
                break;
            case 3:
                $this->setNavigationId($value);
                break;
            case 4:
                $this->setSiteId($value);
                break;
            case 5:
                $this->setTag($value);
                break;
            case 6:
                $this->setUrl($value);
                break;
            case 7:
                $this->setHasChangeableUrl($value);
                break;
            case 8:
                $this->setTitle($value);
                break;
            case 9:
                $this->setTitleColor($value);
                break;
            case 10:
                $this->setTitleBackgroundColor($value);
                break;
            case 11:
                $this->setMetaDescription($value);
                break;
            case 12:
                $this->setMetaKeywords($value);
                break;
            case 13:
                $this->setContent($value);
                break;
            case 14:
                $this->setAbout($value);
                break;
            case 15:
                $this->setMinPictures($value);
                break;
            case 16:
                $this->setMaxPictures($value);
                break;
            case 17:
                $this->setTitlePosX($value);
                break;
            case 18:
                $this->setTitlePosY($value);
                break;
            case 19:
                $this->setHasTitleColorEditor($value);
                break;
            case 20:
                $this->setHasTitleBackgroundcolorEditor($value);
                break;
            case 21:
                $this->setHasNavigation($value);
                break;
            case 22:
                $this->setHasTitle($value);
                break;
            case 23:
                $this->setHasTitlePositioningFields($value);
                break;
            case 24:
                $this->setHasContent($value);
                break;
            case 25:
                $this->setHasUrl($value);
                break;
            case 26:
                $this->setHasMetaDescription($value);
                break;
            case 27:
                $this->setHasMetaKeywords($value);
                break;
            case 28:
                $this->setIsDeletable($value);
                break;
            case 29:
                $this->setIsEditable($value);
                break;
            case 30:
                $this->setIsVisible($value);
                break;
            case 31:
                $this->setHasCoverImage($value);
                break;
            case 32:
                $this->setCoverImageExt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SitePageTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setOldId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setLanguageId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setNavigationId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSiteId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setTag($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setUrl($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setHasChangeableUrl($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setTitle($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setTitleColor($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setTitleBackgroundColor($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setMetaDescription($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setMetaKeywords($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setContent($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setAbout($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setMinPictures($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setMaxPictures($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setTitlePosX($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setTitlePosY($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setHasTitleColorEditor($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setHasTitleBackgroundcolorEditor($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setHasNavigation($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setHasTitle($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setHasTitlePositioningFields($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setHasContent($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setHasUrl($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setHasMetaDescription($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setHasMetaKeywords($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setIsDeletable($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setIsEditable($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setIsVisible($arr[$keys[30]]);
        }
        if (array_key_exists($keys[31], $arr)) {
            $this->setHasCoverImage($arr[$keys[31]]);
        }
        if (array_key_exists($keys[32], $arr)) {
            $this->setCoverImageExt($arr[$keys[32]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Cms\SitePage The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SitePageTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SitePageTableMap::COL_ID)) {
            $criteria->add(SitePageTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_OLD_ID)) {
            $criteria->add(SitePageTableMap::COL_OLD_ID, $this->old_id);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_LANGUAGE_ID)) {
            $criteria->add(SitePageTableMap::COL_LANGUAGE_ID, $this->language_id);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_NAVIGATION_ID)) {
            $criteria->add(SitePageTableMap::COL_NAVIGATION_ID, $this->navigation_id);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_SITE_ID)) {
            $criteria->add(SitePageTableMap::COL_SITE_ID, $this->site_id);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_TAG)) {
            $criteria->add(SitePageTableMap::COL_TAG, $this->tag);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_URL)) {
            $criteria->add(SitePageTableMap::COL_URL, $this->url);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_CHANGEABLE_URL)) {
            $criteria->add(SitePageTableMap::COL_HAS_CHANGEABLE_URL, $this->has_changeable_url);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_TITLE)) {
            $criteria->add(SitePageTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_TITLE_COLOR)) {
            $criteria->add(SitePageTableMap::COL_TITLE_COLOR, $this->title_color);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_TITLE_BACKGROUND_COLOR)) {
            $criteria->add(SitePageTableMap::COL_TITLE_BACKGROUND_COLOR, $this->title_background_color);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_META_DESCRIPTION)) {
            $criteria->add(SitePageTableMap::COL_META_DESCRIPTION, $this->meta_description);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_META_KEYWORDS)) {
            $criteria->add(SitePageTableMap::COL_META_KEYWORDS, $this->meta_keywords);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_CONTENT)) {
            $criteria->add(SitePageTableMap::COL_CONTENT, $this->content);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_ABOUT)) {
            $criteria->add(SitePageTableMap::COL_ABOUT, $this->about);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_MIN_PICTURES)) {
            $criteria->add(SitePageTableMap::COL_MIN_PICTURES, $this->min_pictures);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_MAX_PICTURES)) {
            $criteria->add(SitePageTableMap::COL_MAX_PICTURES, $this->max_pictures);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_TITLE_POS_X)) {
            $criteria->add(SitePageTableMap::COL_TITLE_POS_X, $this->title_pos_x);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_TITLE_POS_Y)) {
            $criteria->add(SitePageTableMap::COL_TITLE_POS_Y, $this->title_pos_y);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_TITLE_COLOR_EDITOR)) {
            $criteria->add(SitePageTableMap::COL_HAS_TITLE_COLOR_EDITOR, $this->has_title_color_editor);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_TITLE_BACKGROUNDCOLOR_EDITOR)) {
            $criteria->add(SitePageTableMap::COL_HAS_TITLE_BACKGROUNDCOLOR_EDITOR, $this->has_title_backgroundcolor_editor);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_NAVIGATION)) {
            $criteria->add(SitePageTableMap::COL_HAS_NAVIGATION, $this->has_navigation);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_TITLE)) {
            $criteria->add(SitePageTableMap::COL_HAS_TITLE, $this->has_title);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_TITLE_POSITIONING_FIELDS)) {
            $criteria->add(SitePageTableMap::COL_HAS_TITLE_POSITIONING_FIELDS, $this->has_title_positioning_fields);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_CONTENT)) {
            $criteria->add(SitePageTableMap::COL_HAS_CONTENT, $this->has_content);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_URL)) {
            $criteria->add(SitePageTableMap::COL_HAS_URL, $this->has_url);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_META_DESCRIPTION)) {
            $criteria->add(SitePageTableMap::COL_HAS_META_DESCRIPTION, $this->has_meta_description);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_META_KEYWORDS)) {
            $criteria->add(SitePageTableMap::COL_HAS_META_KEYWORDS, $this->has_meta_keywords);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_IS_DELETABLE)) {
            $criteria->add(SitePageTableMap::COL_IS_DELETABLE, $this->is_deletable);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_IS_EDITABLE)) {
            $criteria->add(SitePageTableMap::COL_IS_EDITABLE, $this->is_editable);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_IS_VISIBLE)) {
            $criteria->add(SitePageTableMap::COL_IS_VISIBLE, $this->is_visible);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_HAS_COVER_IMAGE)) {
            $criteria->add(SitePageTableMap::COL_HAS_COVER_IMAGE, $this->has_cover_image);
        }
        if ($this->isColumnModified(SitePageTableMap::COL_COVER_IMAGE_EXT)) {
            $criteria->add(SitePageTableMap::COL_COVER_IMAGE_EXT, $this->cover_image_ext);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSitePageQuery::create();
        $criteria->add(SitePageTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Cms\SitePage (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setOldId($this->getOldId());
        $copyObj->setLanguageId($this->getLanguageId());
        $copyObj->setNavigationId($this->getNavigationId());
        $copyObj->setSiteId($this->getSiteId());
        $copyObj->setTag($this->getTag());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setHasChangeableUrl($this->getHasChangeableUrl());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setTitleColor($this->getTitleColor());
        $copyObj->setTitleBackgroundColor($this->getTitleBackgroundColor());
        $copyObj->setMetaDescription($this->getMetaDescription());
        $copyObj->setMetaKeywords($this->getMetaKeywords());
        $copyObj->setContent($this->getContent());
        $copyObj->setAbout($this->getAbout());
        $copyObj->setMinPictures($this->getMinPictures());
        $copyObj->setMaxPictures($this->getMaxPictures());
        $copyObj->setTitlePosX($this->getTitlePosX());
        $copyObj->setTitlePosY($this->getTitlePosY());
        $copyObj->setHasTitleColorEditor($this->getHasTitleColorEditor());
        $copyObj->setHasTitleBackgroundcolorEditor($this->getHasTitleBackgroundcolorEditor());
        $copyObj->setHasNavigation($this->getHasNavigation());
        $copyObj->setHasTitle($this->getHasTitle());
        $copyObj->setHasTitlePositioningFields($this->getHasTitlePositioningFields());
        $copyObj->setHasContent($this->getHasContent());
        $copyObj->setHasUrl($this->getHasUrl());
        $copyObj->setHasMetaDescription($this->getHasMetaDescription());
        $copyObj->setHasMetaKeywords($this->getHasMetaKeywords());
        $copyObj->setIsDeletable($this->getIsDeletable());
        $copyObj->setIsEditable($this->getIsEditable());
        $copyObj->setIsVisible($this->getIsVisible());
        $copyObj->setHasCoverImage($this->getHasCoverImage());
        $copyObj->setCoverImageExt($this->getCoverImageExt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getSiteFooterBlockTranslationSitePages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteFooterBlockTranslationSitePage($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSitePagePictures() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSitePagePicture($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Cms\SitePage Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildSite object.
     *
     * @param  ChildSite $v
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSite(ChildSite $v = null)
    {
        if ($v === null) {
            $this->setSiteId(NULL);
        } else {
            $this->setSiteId($v->getId());
        }

        $this->aSite = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSite object, it will not be re-added.
        if ($v !== null) {
            $v->addSitePage($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSite object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSite The associated ChildSite object.
     * @throws PropelException
     */
    public function getSite(ConnectionInterface $con = null)
    {
        if ($this->aSite === null && ($this->site_id != 0)) {
            $this->aSite = ChildSiteQuery::create()->findPk($this->site_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSite->addSitePages($this);
             */
        }

        return $this->aSite;
    }

    /**
     * Declares an association between this object and a ChildSite_navigation object.
     *
     * @param  ChildSite_navigation|null $v
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSiteNavigaton(ChildSite_navigation $v = null)
    {
        if ($v === null) {
            $this->setNavigationId(NULL);
        } else {
            $this->setNavigationId($v->getId());
        }

        $this->aSiteNavigaton = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSite_navigation object, it will not be re-added.
        if ($v !== null) {
            $v->addSitePage($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSite_navigation object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSite_navigation|null The associated ChildSite_navigation object.
     * @throws PropelException
     */
    public function getSiteNavigaton(ConnectionInterface $con = null)
    {
        if ($this->aSiteNavigaton === null && ($this->navigation_id != 0)) {
            $this->aSiteNavigaton = ChildSite_navigationQuery::create()->findPk($this->navigation_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSiteNavigaton->addSitePages($this);
             */
        }

        return $this->aSiteNavigaton;
    }

    /**
     * Declares an association between this object and a Language object.
     *
     * @param  Language|null $v
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLanguage(Language $v = null)
    {
        if ($v === null) {
            $this->setLanguageId(NULL);
        } else {
            $this->setLanguageId($v->getId());
        }

        $this->aLanguage = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Language object, it will not be re-added.
        if ($v !== null) {
            $v->addSitePage($this);
        }


        return $this;
    }


    /**
     * Get the associated Language object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Language|null The associated Language object.
     * @throws PropelException
     */
    public function getLanguage(ConnectionInterface $con = null)
    {
        if ($this->aLanguage === null && ($this->language_id != 0)) {
            $this->aLanguage = LanguageQuery::create()->findPk($this->language_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLanguage->addSitePages($this);
             */
        }

        return $this->aLanguage;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SiteFooterBlockTranslationSitePage' === $relationName) {
            $this->initSiteFooterBlockTranslationSitePages();
            return;
        }
        if ('SitePagePicture' === $relationName) {
            $this->initSitePagePictures();
            return;
        }
    }

    /**
     * Clears out the collSiteFooterBlockTranslationSitePages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteFooterBlockTranslationSitePages()
     */
    public function clearSiteFooterBlockTranslationSitePages()
    {
        $this->collSiteFooterBlockTranslationSitePages = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteFooterBlockTranslationSitePages collection loaded partially.
     */
    public function resetPartialSiteFooterBlockTranslationSitePages($v = true)
    {
        $this->collSiteFooterBlockTranslationSitePagesPartial = $v;
    }

    /**
     * Initializes the collSiteFooterBlockTranslationSitePages collection.
     *
     * By default this just sets the collSiteFooterBlockTranslationSitePages collection to an empty array (like clearcollSiteFooterBlockTranslationSitePages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteFooterBlockTranslationSitePages($overrideExisting = true)
    {
        if (null !== $this->collSiteFooterBlockTranslationSitePages && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteFooterBlockTranslationSitePageTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteFooterBlockTranslationSitePages = new $collectionClassName;
        $this->collSiteFooterBlockTranslationSitePages->setModel('\Model\Cms\SiteFooterBlockTranslationSitePage');
    }

    /**
     * Gets an array of ChildSiteFooterBlockTranslationSitePage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSitePage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSiteFooterBlockTranslationSitePage[] List of ChildSiteFooterBlockTranslationSitePage objects
     * @throws PropelException
     */
    public function getSiteFooterBlockTranslationSitePages(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFooterBlockTranslationSitePagesPartial && !$this->isNew();
        if (null === $this->collSiteFooterBlockTranslationSitePages || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteFooterBlockTranslationSitePages) {
                    $this->initSiteFooterBlockTranslationSitePages();
                } else {
                    $collectionClassName = SiteFooterBlockTranslationSitePageTableMap::getTableMap()->getCollectionClassName();

                    $collSiteFooterBlockTranslationSitePages = new $collectionClassName;
                    $collSiteFooterBlockTranslationSitePages->setModel('\Model\Cms\SiteFooterBlockTranslationSitePage');

                    return $collSiteFooterBlockTranslationSitePages;
                }
            } else {
                $collSiteFooterBlockTranslationSitePages = ChildSiteFooterBlockTranslationSitePageQuery::create(null, $criteria)
                    ->filterBySitePage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteFooterBlockTranslationSitePagesPartial && count($collSiteFooterBlockTranslationSitePages)) {
                        $this->initSiteFooterBlockTranslationSitePages(false);

                        foreach ($collSiteFooterBlockTranslationSitePages as $obj) {
                            if (false == $this->collSiteFooterBlockTranslationSitePages->contains($obj)) {
                                $this->collSiteFooterBlockTranslationSitePages->append($obj);
                            }
                        }

                        $this->collSiteFooterBlockTranslationSitePagesPartial = true;
                    }

                    return $collSiteFooterBlockTranslationSitePages;
                }

                if ($partial && $this->collSiteFooterBlockTranslationSitePages) {
                    foreach ($this->collSiteFooterBlockTranslationSitePages as $obj) {
                        if ($obj->isNew()) {
                            $collSiteFooterBlockTranslationSitePages[] = $obj;
                        }
                    }
                }

                $this->collSiteFooterBlockTranslationSitePages = $collSiteFooterBlockTranslationSitePages;
                $this->collSiteFooterBlockTranslationSitePagesPartial = false;
            }
        }

        return $this->collSiteFooterBlockTranslationSitePages;
    }

    /**
     * Sets a collection of ChildSiteFooterBlockTranslationSitePage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteFooterBlockTranslationSitePages A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSitePage The current object (for fluent API support)
     */
    public function setSiteFooterBlockTranslationSitePages(Collection $siteFooterBlockTranslationSitePages, ConnectionInterface $con = null)
    {
        /** @var ChildSiteFooterBlockTranslationSitePage[] $siteFooterBlockTranslationSitePagesToDelete */
        $siteFooterBlockTranslationSitePagesToDelete = $this->getSiteFooterBlockTranslationSitePages(new Criteria(), $con)->diff($siteFooterBlockTranslationSitePages);


        $this->siteFooterBlockTranslationSitePagesScheduledForDeletion = $siteFooterBlockTranslationSitePagesToDelete;

        foreach ($siteFooterBlockTranslationSitePagesToDelete as $siteFooterBlockTranslationSitePageRemoved) {
            $siteFooterBlockTranslationSitePageRemoved->setSitePage(null);
        }

        $this->collSiteFooterBlockTranslationSitePages = null;
        foreach ($siteFooterBlockTranslationSitePages as $siteFooterBlockTranslationSitePage) {
            $this->addSiteFooterBlockTranslationSitePage($siteFooterBlockTranslationSitePage);
        }

        $this->collSiteFooterBlockTranslationSitePages = $siteFooterBlockTranslationSitePages;
        $this->collSiteFooterBlockTranslationSitePagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SiteFooterBlockTranslationSitePage objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SiteFooterBlockTranslationSitePage objects.
     * @throws PropelException
     */
    public function countSiteFooterBlockTranslationSitePages(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFooterBlockTranslationSitePagesPartial && !$this->isNew();
        if (null === $this->collSiteFooterBlockTranslationSitePages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteFooterBlockTranslationSitePages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteFooterBlockTranslationSitePages());
            }

            $query = ChildSiteFooterBlockTranslationSitePageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySitePage($this)
                ->count($con);
        }

        return count($this->collSiteFooterBlockTranslationSitePages);
    }

    /**
     * Method called to associate a ChildSiteFooterBlockTranslationSitePage object to this object
     * through the ChildSiteFooterBlockTranslationSitePage foreign key attribute.
     *
     * @param  ChildSiteFooterBlockTranslationSitePage $l ChildSiteFooterBlockTranslationSitePage
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function addSiteFooterBlockTranslationSitePage(ChildSiteFooterBlockTranslationSitePage $l)
    {
        if ($this->collSiteFooterBlockTranslationSitePages === null) {
            $this->initSiteFooterBlockTranslationSitePages();
            $this->collSiteFooterBlockTranslationSitePagesPartial = true;
        }

        if (!$this->collSiteFooterBlockTranslationSitePages->contains($l)) {
            $this->doAddSiteFooterBlockTranslationSitePage($l);

            if ($this->siteFooterBlockTranslationSitePagesScheduledForDeletion and $this->siteFooterBlockTranslationSitePagesScheduledForDeletion->contains($l)) {
                $this->siteFooterBlockTranslationSitePagesScheduledForDeletion->remove($this->siteFooterBlockTranslationSitePagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSiteFooterBlockTranslationSitePage $siteFooterBlockTranslationSitePage The ChildSiteFooterBlockTranslationSitePage object to add.
     */
    protected function doAddSiteFooterBlockTranslationSitePage(ChildSiteFooterBlockTranslationSitePage $siteFooterBlockTranslationSitePage)
    {
        $this->collSiteFooterBlockTranslationSitePages[]= $siteFooterBlockTranslationSitePage;
        $siteFooterBlockTranslationSitePage->setSitePage($this);
    }

    /**
     * @param  ChildSiteFooterBlockTranslationSitePage $siteFooterBlockTranslationSitePage The ChildSiteFooterBlockTranslationSitePage object to remove.
     * @return $this|ChildSitePage The current object (for fluent API support)
     */
    public function removeSiteFooterBlockTranslationSitePage(ChildSiteFooterBlockTranslationSitePage $siteFooterBlockTranslationSitePage)
    {
        if ($this->getSiteFooterBlockTranslationSitePages()->contains($siteFooterBlockTranslationSitePage)) {
            $pos = $this->collSiteFooterBlockTranslationSitePages->search($siteFooterBlockTranslationSitePage);
            $this->collSiteFooterBlockTranslationSitePages->remove($pos);
            if (null === $this->siteFooterBlockTranslationSitePagesScheduledForDeletion) {
                $this->siteFooterBlockTranslationSitePagesScheduledForDeletion = clone $this->collSiteFooterBlockTranslationSitePages;
                $this->siteFooterBlockTranslationSitePagesScheduledForDeletion->clear();
            }
            $this->siteFooterBlockTranslationSitePagesScheduledForDeletion[]= clone $siteFooterBlockTranslationSitePage;
            $siteFooterBlockTranslationSitePage->setSitePage(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SitePage is new, it will return
     * an empty collection; or if this SitePage has previously
     * been saved, it will retrieve related SiteFooterBlockTranslationSitePages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SitePage.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSiteFooterBlockTranslationSitePage[] List of ChildSiteFooterBlockTranslationSitePage objects
     */
    public function getSiteFooterBlockTranslationSitePagesJoinSiteFooterBlockTranslation(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSiteFooterBlockTranslationSitePageQuery::create(null, $criteria);
        $query->joinWith('SiteFooterBlockTranslation', $joinBehavior);

        return $this->getSiteFooterBlockTranslationSitePages($query, $con);
    }

    /**
     * Clears out the collSitePagePictures collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSitePagePictures()
     */
    public function clearSitePagePictures()
    {
        $this->collSitePagePictures = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSitePagePictures collection loaded partially.
     */
    public function resetPartialSitePagePictures($v = true)
    {
        $this->collSitePagePicturesPartial = $v;
    }

    /**
     * Initializes the collSitePagePictures collection.
     *
     * By default this just sets the collSitePagePictures collection to an empty array (like clearcollSitePagePictures());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSitePagePictures($overrideExisting = true)
    {
        if (null !== $this->collSitePagePictures && !$overrideExisting) {
            return;
        }

        $collectionClassName = SitePagePictureTableMap::getTableMap()->getCollectionClassName();

        $this->collSitePagePictures = new $collectionClassName;
        $this->collSitePagePictures->setModel('\Model\Cms\SitePagePicture');
    }

    /**
     * Gets an array of ChildSitePagePicture objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSitePage is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSitePagePicture[] List of ChildSitePagePicture objects
     * @throws PropelException
     */
    public function getSitePagePictures(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSitePagePicturesPartial && !$this->isNew();
        if (null === $this->collSitePagePictures || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSitePagePictures) {
                    $this->initSitePagePictures();
                } else {
                    $collectionClassName = SitePagePictureTableMap::getTableMap()->getCollectionClassName();

                    $collSitePagePictures = new $collectionClassName;
                    $collSitePagePictures->setModel('\Model\Cms\SitePagePicture');

                    return $collSitePagePictures;
                }
            } else {
                $collSitePagePictures = ChildSitePagePictureQuery::create(null, $criteria)
                    ->filterBySitePage($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSitePagePicturesPartial && count($collSitePagePictures)) {
                        $this->initSitePagePictures(false);

                        foreach ($collSitePagePictures as $obj) {
                            if (false == $this->collSitePagePictures->contains($obj)) {
                                $this->collSitePagePictures->append($obj);
                            }
                        }

                        $this->collSitePagePicturesPartial = true;
                    }

                    return $collSitePagePictures;
                }

                if ($partial && $this->collSitePagePictures) {
                    foreach ($this->collSitePagePictures as $obj) {
                        if ($obj->isNew()) {
                            $collSitePagePictures[] = $obj;
                        }
                    }
                }

                $this->collSitePagePictures = $collSitePagePictures;
                $this->collSitePagePicturesPartial = false;
            }
        }

        return $this->collSitePagePictures;
    }

    /**
     * Sets a collection of ChildSitePagePicture objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sitePagePictures A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSitePage The current object (for fluent API support)
     */
    public function setSitePagePictures(Collection $sitePagePictures, ConnectionInterface $con = null)
    {
        /** @var ChildSitePagePicture[] $sitePagePicturesToDelete */
        $sitePagePicturesToDelete = $this->getSitePagePictures(new Criteria(), $con)->diff($sitePagePictures);


        $this->sitePagePicturesScheduledForDeletion = $sitePagePicturesToDelete;

        foreach ($sitePagePicturesToDelete as $sitePagePictureRemoved) {
            $sitePagePictureRemoved->setSitePage(null);
        }

        $this->collSitePagePictures = null;
        foreach ($sitePagePictures as $sitePagePicture) {
            $this->addSitePagePicture($sitePagePicture);
        }

        $this->collSitePagePictures = $sitePagePictures;
        $this->collSitePagePicturesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SitePagePicture objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SitePagePicture objects.
     * @throws PropelException
     */
    public function countSitePagePictures(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSitePagePicturesPartial && !$this->isNew();
        if (null === $this->collSitePagePictures || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSitePagePictures) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSitePagePictures());
            }

            $query = ChildSitePagePictureQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySitePage($this)
                ->count($con);
        }

        return count($this->collSitePagePictures);
    }

    /**
     * Method called to associate a ChildSitePagePicture object to this object
     * through the ChildSitePagePicture foreign key attribute.
     *
     * @param  ChildSitePagePicture $l ChildSitePagePicture
     * @return $this|\Model\Cms\SitePage The current object (for fluent API support)
     */
    public function addSitePagePicture(ChildSitePagePicture $l)
    {
        if ($this->collSitePagePictures === null) {
            $this->initSitePagePictures();
            $this->collSitePagePicturesPartial = true;
        }

        if (!$this->collSitePagePictures->contains($l)) {
            $this->doAddSitePagePicture($l);

            if ($this->sitePagePicturesScheduledForDeletion and $this->sitePagePicturesScheduledForDeletion->contains($l)) {
                $this->sitePagePicturesScheduledForDeletion->remove($this->sitePagePicturesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSitePagePicture $sitePagePicture The ChildSitePagePicture object to add.
     */
    protected function doAddSitePagePicture(ChildSitePagePicture $sitePagePicture)
    {
        $this->collSitePagePictures[]= $sitePagePicture;
        $sitePagePicture->setSitePage($this);
    }

    /**
     * @param  ChildSitePagePicture $sitePagePicture The ChildSitePagePicture object to remove.
     * @return $this|ChildSitePage The current object (for fluent API support)
     */
    public function removeSitePagePicture(ChildSitePagePicture $sitePagePicture)
    {
        if ($this->getSitePagePictures()->contains($sitePagePicture)) {
            $pos = $this->collSitePagePictures->search($sitePagePicture);
            $this->collSitePagePictures->remove($pos);
            if (null === $this->sitePagePicturesScheduledForDeletion) {
                $this->sitePagePicturesScheduledForDeletion = clone $this->collSitePagePictures;
                $this->sitePagePicturesScheduledForDeletion->clear();
            }
            $this->sitePagePicturesScheduledForDeletion[]= clone $sitePagePicture;
            $sitePagePicture->setSitePage(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aSite) {
            $this->aSite->removeSitePage($this);
        }
        if (null !== $this->aSiteNavigaton) {
            $this->aSiteNavigaton->removeSitePage($this);
        }
        if (null !== $this->aLanguage) {
            $this->aLanguage->removeSitePage($this);
        }
        $this->id = null;
        $this->old_id = null;
        $this->language_id = null;
        $this->navigation_id = null;
        $this->site_id = null;
        $this->tag = null;
        $this->url = null;
        $this->has_changeable_url = null;
        $this->title = null;
        $this->title_color = null;
        $this->title_background_color = null;
        $this->meta_description = null;
        $this->meta_keywords = null;
        $this->content = null;
        $this->about = null;
        $this->min_pictures = null;
        $this->max_pictures = null;
        $this->title_pos_x = null;
        $this->title_pos_y = null;
        $this->has_title_color_editor = null;
        $this->has_title_backgroundcolor_editor = null;
        $this->has_navigation = null;
        $this->has_title = null;
        $this->has_title_positioning_fields = null;
        $this->has_content = null;
        $this->has_url = null;
        $this->has_meta_description = null;
        $this->has_meta_keywords = null;
        $this->is_deletable = null;
        $this->is_editable = null;
        $this->is_visible = null;
        $this->has_cover_image = null;
        $this->cover_image_ext = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSiteFooterBlockTranslationSitePages) {
                foreach ($this->collSiteFooterBlockTranslationSitePages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSitePagePictures) {
                foreach ($this->collSitePagePictures as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collSiteFooterBlockTranslationSitePages = null;
        $this->collSitePagePictures = null;
        $this->aSite = null;
        $this->aSiteNavigaton = null;
        $this->aLanguage = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SitePageTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
