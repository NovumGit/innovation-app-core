<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\Account\User;
use Model\Cms\SiteBlog as ChildSiteBlog;
use Model\Cms\SiteBlogQuery as ChildSiteBlogQuery;
use Model\Cms\Map\SiteBlogTableMap;
use Model\Setting\MasterTable\Language;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'site_blog' table.
 *
 *
 *
 * @method     ChildSiteBlogQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSiteBlogQuery orderBySiteId($order = Criteria::ASC) Order by the site_id column
 * @method     ChildSiteBlogQuery orderByLanguageId($order = Criteria::ASC) Order by the language_id column
 * @method     ChildSiteBlogQuery orderByCreatedOn($order = Criteria::ASC) Order by the created_on column
 * @method     ChildSiteBlogQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the created_by_user_id column
 * @method     ChildSiteBlogQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildSiteBlogQuery orderByContent($order = Criteria::ASC) Order by the content column
 * @method     ChildSiteBlogQuery orderByTeaser($order = Criteria::ASC) Order by the teaser column
 * @method     ChildSiteBlogQuery orderByTag($order = Criteria::ASC) Order by the tag column
 * @method     ChildSiteBlogQuery orderByFileOriginalName($order = Criteria::ASC) Order by the file_original_name column
 * @method     ChildSiteBlogQuery orderByFileExt($order = Criteria::ASC) Order by the file_ext column
 * @method     ChildSiteBlogQuery orderByFileSize($order = Criteria::ASC) Order by the file_size column
 * @method     ChildSiteBlogQuery orderByFileMime($order = Criteria::ASC) Order by the file_mime column
 *
 * @method     ChildSiteBlogQuery groupById() Group by the id column
 * @method     ChildSiteBlogQuery groupBySiteId() Group by the site_id column
 * @method     ChildSiteBlogQuery groupByLanguageId() Group by the language_id column
 * @method     ChildSiteBlogQuery groupByCreatedOn() Group by the created_on column
 * @method     ChildSiteBlogQuery groupByCreatedByUserId() Group by the created_by_user_id column
 * @method     ChildSiteBlogQuery groupByTitle() Group by the title column
 * @method     ChildSiteBlogQuery groupByContent() Group by the content column
 * @method     ChildSiteBlogQuery groupByTeaser() Group by the teaser column
 * @method     ChildSiteBlogQuery groupByTag() Group by the tag column
 * @method     ChildSiteBlogQuery groupByFileOriginalName() Group by the file_original_name column
 * @method     ChildSiteBlogQuery groupByFileExt() Group by the file_ext column
 * @method     ChildSiteBlogQuery groupByFileSize() Group by the file_size column
 * @method     ChildSiteBlogQuery groupByFileMime() Group by the file_mime column
 *
 * @method     ChildSiteBlogQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteBlogQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteBlogQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteBlogQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteBlogQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteBlogQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteBlogQuery leftJoinLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Language relation
 * @method     ChildSiteBlogQuery rightJoinLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Language relation
 * @method     ChildSiteBlogQuery innerJoinLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the Language relation
 *
 * @method     ChildSiteBlogQuery joinWithLanguage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Language relation
 *
 * @method     ChildSiteBlogQuery leftJoinWithLanguage() Adds a LEFT JOIN clause and with to the query using the Language relation
 * @method     ChildSiteBlogQuery rightJoinWithLanguage() Adds a RIGHT JOIN clause and with to the query using the Language relation
 * @method     ChildSiteBlogQuery innerJoinWithLanguage() Adds a INNER JOIN clause and with to the query using the Language relation
 *
 * @method     ChildSiteBlogQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildSiteBlogQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildSiteBlogQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildSiteBlogQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildSiteBlogQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildSiteBlogQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildSiteBlogQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildSiteBlogQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildSiteBlogQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildSiteBlogQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildSiteBlogQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildSiteBlogQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildSiteBlogQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildSiteBlogQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     \Model\Setting\MasterTable\LanguageQuery|\Model\Cms\SiteQuery|\Model\Account\UserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSiteBlog findOne(ConnectionInterface $con = null) Return the first ChildSiteBlog matching the query
 * @method     ChildSiteBlog findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSiteBlog matching the query, or a new ChildSiteBlog object populated from the query conditions when no match is found
 *
 * @method     ChildSiteBlog findOneById(int $id) Return the first ChildSiteBlog filtered by the id column
 * @method     ChildSiteBlog findOneBySiteId(int $site_id) Return the first ChildSiteBlog filtered by the site_id column
 * @method     ChildSiteBlog findOneByLanguageId(int $language_id) Return the first ChildSiteBlog filtered by the language_id column
 * @method     ChildSiteBlog findOneByCreatedOn(string $created_on) Return the first ChildSiteBlog filtered by the created_on column
 * @method     ChildSiteBlog findOneByCreatedByUserId(int $created_by_user_id) Return the first ChildSiteBlog filtered by the created_by_user_id column
 * @method     ChildSiteBlog findOneByTitle(string $title) Return the first ChildSiteBlog filtered by the title column
 * @method     ChildSiteBlog findOneByContent(string $content) Return the first ChildSiteBlog filtered by the content column
 * @method     ChildSiteBlog findOneByTeaser(string $teaser) Return the first ChildSiteBlog filtered by the teaser column
 * @method     ChildSiteBlog findOneByTag(string $tag) Return the first ChildSiteBlog filtered by the tag column
 * @method     ChildSiteBlog findOneByFileOriginalName(string $file_original_name) Return the first ChildSiteBlog filtered by the file_original_name column
 * @method     ChildSiteBlog findOneByFileExt(string $file_ext) Return the first ChildSiteBlog filtered by the file_ext column
 * @method     ChildSiteBlog findOneByFileSize(int $file_size) Return the first ChildSiteBlog filtered by the file_size column
 * @method     ChildSiteBlog findOneByFileMime(string $file_mime) Return the first ChildSiteBlog filtered by the file_mime column *

 * @method     ChildSiteBlog requirePk($key, ConnectionInterface $con = null) Return the ChildSiteBlog by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBlog requireOne(ConnectionInterface $con = null) Return the first ChildSiteBlog matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteBlog requireOneById(int $id) Return the first ChildSiteBlog filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBlog requireOneBySiteId(int $site_id) Return the first ChildSiteBlog filtered by the site_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBlog requireOneByLanguageId(int $language_id) Return the first ChildSiteBlog filtered by the language_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBlog requireOneByCreatedOn(string $created_on) Return the first ChildSiteBlog filtered by the created_on column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBlog requireOneByCreatedByUserId(int $created_by_user_id) Return the first ChildSiteBlog filtered by the created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBlog requireOneByTitle(string $title) Return the first ChildSiteBlog filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBlog requireOneByContent(string $content) Return the first ChildSiteBlog filtered by the content column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBlog requireOneByTeaser(string $teaser) Return the first ChildSiteBlog filtered by the teaser column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBlog requireOneByTag(string $tag) Return the first ChildSiteBlog filtered by the tag column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBlog requireOneByFileOriginalName(string $file_original_name) Return the first ChildSiteBlog filtered by the file_original_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBlog requireOneByFileExt(string $file_ext) Return the first ChildSiteBlog filtered by the file_ext column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBlog requireOneByFileSize(int $file_size) Return the first ChildSiteBlog filtered by the file_size column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBlog requireOneByFileMime(string $file_mime) Return the first ChildSiteBlog filtered by the file_mime column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteBlog[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSiteBlog objects based on current ModelCriteria
 * @method     ChildSiteBlog[]|ObjectCollection findById(int $id) Return ChildSiteBlog objects filtered by the id column
 * @method     ChildSiteBlog[]|ObjectCollection findBySiteId(int $site_id) Return ChildSiteBlog objects filtered by the site_id column
 * @method     ChildSiteBlog[]|ObjectCollection findByLanguageId(int $language_id) Return ChildSiteBlog objects filtered by the language_id column
 * @method     ChildSiteBlog[]|ObjectCollection findByCreatedOn(string $created_on) Return ChildSiteBlog objects filtered by the created_on column
 * @method     ChildSiteBlog[]|ObjectCollection findByCreatedByUserId(int $created_by_user_id) Return ChildSiteBlog objects filtered by the created_by_user_id column
 * @method     ChildSiteBlog[]|ObjectCollection findByTitle(string $title) Return ChildSiteBlog objects filtered by the title column
 * @method     ChildSiteBlog[]|ObjectCollection findByContent(string $content) Return ChildSiteBlog objects filtered by the content column
 * @method     ChildSiteBlog[]|ObjectCollection findByTeaser(string $teaser) Return ChildSiteBlog objects filtered by the teaser column
 * @method     ChildSiteBlog[]|ObjectCollection findByTag(string $tag) Return ChildSiteBlog objects filtered by the tag column
 * @method     ChildSiteBlog[]|ObjectCollection findByFileOriginalName(string $file_original_name) Return ChildSiteBlog objects filtered by the file_original_name column
 * @method     ChildSiteBlog[]|ObjectCollection findByFileExt(string $file_ext) Return ChildSiteBlog objects filtered by the file_ext column
 * @method     ChildSiteBlog[]|ObjectCollection findByFileSize(int $file_size) Return ChildSiteBlog objects filtered by the file_size column
 * @method     ChildSiteBlog[]|ObjectCollection findByFileMime(string $file_mime) Return ChildSiteBlog objects filtered by the file_mime column
 * @method     ChildSiteBlog[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteBlogQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Cms\Base\SiteBlogQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Cms\\SiteBlog', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteBlogQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteBlogQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteBlogQuery) {
            return $criteria;
        }
        $query = new ChildSiteBlogQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSiteBlog|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteBlogTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteBlogTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteBlog A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, site_id, language_id, created_on, created_by_user_id, title, content, teaser, tag, file_original_name, file_ext, file_size, file_mime FROM site_blog WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSiteBlog $obj */
            $obj = new ChildSiteBlog();
            $obj->hydrate($row);
            SiteBlogTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSiteBlog|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteBlogTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteBlogTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteBlogTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteBlogTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBlogTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the site_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE site_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE site_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE site_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(SiteBlogTableMap::COL_SITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(SiteBlogTableMap::COL_SITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBlogTableMap::COL_SITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the language_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguageId(1234); // WHERE language_id = 1234
     * $query->filterByLanguageId(array(12, 34)); // WHERE language_id IN (12, 34)
     * $query->filterByLanguageId(array('min' => 12)); // WHERE language_id > 12
     * </code>
     *
     * @see       filterByLanguage()
     *
     * @param     mixed $languageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByLanguageId($languageId = null, $comparison = null)
    {
        if (is_array($languageId)) {
            $useMinMax = false;
            if (isset($languageId['min'])) {
                $this->addUsingAlias(SiteBlogTableMap::COL_LANGUAGE_ID, $languageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($languageId['max'])) {
                $this->addUsingAlias(SiteBlogTableMap::COL_LANGUAGE_ID, $languageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBlogTableMap::COL_LANGUAGE_ID, $languageId, $comparison);
    }

    /**
     * Filter the query on the created_on column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedOn('2011-03-14'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn('now'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn(array('max' => 'yesterday')); // WHERE created_on > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdOn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByCreatedOn($createdOn = null, $comparison = null)
    {
        if (is_array($createdOn)) {
            $useMinMax = false;
            if (isset($createdOn['min'])) {
                $this->addUsingAlias(SiteBlogTableMap::COL_CREATED_ON, $createdOn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdOn['max'])) {
                $this->addUsingAlias(SiteBlogTableMap::COL_CREATED_ON, $createdOn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBlogTableMap::COL_CREATED_ON, $createdOn, $comparison);
    }

    /**
     * Filter the query on the created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE created_by_user_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(SiteBlogTableMap::COL_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(SiteBlogTableMap::COL_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBlogTableMap::COL_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBlogTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the content column
     *
     * Example usage:
     * <code>
     * $query->filterByContent('fooValue');   // WHERE content = 'fooValue'
     * $query->filterByContent('%fooValue%', Criteria::LIKE); // WHERE content LIKE '%fooValue%'
     * </code>
     *
     * @param     string $content The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByContent($content = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($content)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBlogTableMap::COL_CONTENT, $content, $comparison);
    }

    /**
     * Filter the query on the teaser column
     *
     * Example usage:
     * <code>
     * $query->filterByTeaser('fooValue');   // WHERE teaser = 'fooValue'
     * $query->filterByTeaser('%fooValue%', Criteria::LIKE); // WHERE teaser LIKE '%fooValue%'
     * </code>
     *
     * @param     string $teaser The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByTeaser($teaser = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($teaser)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBlogTableMap::COL_TEASER, $teaser, $comparison);
    }

    /**
     * Filter the query on the tag column
     *
     * Example usage:
     * <code>
     * $query->filterByTag('fooValue');   // WHERE tag = 'fooValue'
     * $query->filterByTag('%fooValue%', Criteria::LIKE); // WHERE tag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tag The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByTag($tag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tag)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBlogTableMap::COL_TAG, $tag, $comparison);
    }

    /**
     * Filter the query on the file_original_name column
     *
     * Example usage:
     * <code>
     * $query->filterByFileOriginalName('fooValue');   // WHERE file_original_name = 'fooValue'
     * $query->filterByFileOriginalName('%fooValue%', Criteria::LIKE); // WHERE file_original_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fileOriginalName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByFileOriginalName($fileOriginalName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fileOriginalName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBlogTableMap::COL_FILE_ORIGINAL_NAME, $fileOriginalName, $comparison);
    }

    /**
     * Filter the query on the file_ext column
     *
     * Example usage:
     * <code>
     * $query->filterByFileExt('fooValue');   // WHERE file_ext = 'fooValue'
     * $query->filterByFileExt('%fooValue%', Criteria::LIKE); // WHERE file_ext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fileExt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByFileExt($fileExt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fileExt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBlogTableMap::COL_FILE_EXT, $fileExt, $comparison);
    }

    /**
     * Filter the query on the file_size column
     *
     * Example usage:
     * <code>
     * $query->filterByFileSize(1234); // WHERE file_size = 1234
     * $query->filterByFileSize(array(12, 34)); // WHERE file_size IN (12, 34)
     * $query->filterByFileSize(array('min' => 12)); // WHERE file_size > 12
     * </code>
     *
     * @param     mixed $fileSize The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByFileSize($fileSize = null, $comparison = null)
    {
        if (is_array($fileSize)) {
            $useMinMax = false;
            if (isset($fileSize['min'])) {
                $this->addUsingAlias(SiteBlogTableMap::COL_FILE_SIZE, $fileSize['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fileSize['max'])) {
                $this->addUsingAlias(SiteBlogTableMap::COL_FILE_SIZE, $fileSize['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBlogTableMap::COL_FILE_SIZE, $fileSize, $comparison);
    }

    /**
     * Filter the query on the file_mime column
     *
     * Example usage:
     * <code>
     * $query->filterByFileMime('fooValue');   // WHERE file_mime = 'fooValue'
     * $query->filterByFileMime('%fooValue%', Criteria::LIKE); // WHERE file_mime LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fileMime The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByFileMime($fileMime = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fileMime)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBlogTableMap::COL_FILE_MIME, $fileMime, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Language object
     *
     * @param \Model\Setting\MasterTable\Language|ObjectCollection $language The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByLanguage($language, $comparison = null)
    {
        if ($language instanceof \Model\Setting\MasterTable\Language) {
            return $this
                ->addUsingAlias(SiteBlogTableMap::COL_LANGUAGE_ID, $language->getId(), $comparison);
        } elseif ($language instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteBlogTableMap::COL_LANGUAGE_ID, $language->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLanguage() only accepts arguments of type \Model\Setting\MasterTable\Language or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Language relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function joinLanguage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Language');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Language');
        }

        return $this;
    }

    /**
     * Use the Language relation Language object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\LanguageQuery A secondary query class using the current class as primary query
     */
    public function useLanguageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLanguage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Language', '\Model\Setting\MasterTable\LanguageQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\Site object
     *
     * @param \Model\Cms\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \Model\Cms\Site) {
            return $this
                ->addUsingAlias(SiteBlogTableMap::COL_SITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteBlogTableMap::COL_SITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \Model\Cms\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\Model\Cms\SiteQuery');
    }

    /**
     * Filter the query by a related \Model\Account\User object
     *
     * @param \Model\Account\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteBlogQuery The current query, for fluid interface
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof \Model\Account\User) {
            return $this
                ->addUsingAlias(SiteBlogTableMap::COL_CREATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteBlogTableMap::COL_CREATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \Model\Account\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Account\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\Model\Account\UserQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSiteBlog $siteBlog Object to remove from the list of results
     *
     * @return $this|ChildSiteBlogQuery The current query, for fluid interface
     */
    public function prune($siteBlog = null)
    {
        if ($siteBlog) {
            $this->addUsingAlias(SiteBlogTableMap::COL_ID, $siteBlog->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the site_blog table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteBlogTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteBlogTableMap::clearInstancePool();
            SiteBlogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteBlogTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteBlogTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteBlogTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteBlogTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SiteBlogQuery
