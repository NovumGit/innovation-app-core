<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\Cms\SiteBanner as ChildSiteBanner;
use Model\Cms\SiteBannerQuery as ChildSiteBannerQuery;
use Model\Cms\Map\SiteBannerTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'site_banner' table.
 *
 *
 *
 * @method     ChildSiteBannerQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSiteBannerQuery orderBySiteId($order = Criteria::ASC) Order by the site_id column
 * @method     ChildSiteBannerQuery orderByAboutTxt($order = Criteria::ASC) Order by the about_txt column
 * @method     ChildSiteBannerQuery orderByAlignment($order = Criteria::ASC) Order by the alignment column
 * @method     ChildSiteBannerQuery orderBySlogan($order = Criteria::ASC) Order by the slogan column
 * @method     ChildSiteBannerQuery orderBySubText($order = Criteria::ASC) Order by the sub_text column
 * @method     ChildSiteBannerQuery orderByHasInfoButton($order = Criteria::ASC) Order by the has_info_button column
 * @method     ChildSiteBannerQuery orderByInfoLink($order = Criteria::ASC) Order by the info_link column
 * @method     ChildSiteBannerQuery orderByHasBuyNow($order = Criteria::ASC) Order by the has_buy_now column
 * @method     ChildSiteBannerQuery orderByBuyNowLink($order = Criteria::ASC) Order by the buy_now_link column
 * @method     ChildSiteBannerQuery orderByFileExt($order = Criteria::ASC) Order by the file_ext column
 *
 * @method     ChildSiteBannerQuery groupById() Group by the id column
 * @method     ChildSiteBannerQuery groupBySiteId() Group by the site_id column
 * @method     ChildSiteBannerQuery groupByAboutTxt() Group by the about_txt column
 * @method     ChildSiteBannerQuery groupByAlignment() Group by the alignment column
 * @method     ChildSiteBannerQuery groupBySlogan() Group by the slogan column
 * @method     ChildSiteBannerQuery groupBySubText() Group by the sub_text column
 * @method     ChildSiteBannerQuery groupByHasInfoButton() Group by the has_info_button column
 * @method     ChildSiteBannerQuery groupByInfoLink() Group by the info_link column
 * @method     ChildSiteBannerQuery groupByHasBuyNow() Group by the has_buy_now column
 * @method     ChildSiteBannerQuery groupByBuyNowLink() Group by the buy_now_link column
 * @method     ChildSiteBannerQuery groupByFileExt() Group by the file_ext column
 *
 * @method     ChildSiteBannerQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteBannerQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteBannerQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteBannerQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteBannerQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteBannerQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteBannerQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildSiteBannerQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildSiteBannerQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildSiteBannerQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildSiteBannerQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildSiteBannerQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildSiteBannerQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     \Model\Cms\SiteQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSiteBanner findOne(ConnectionInterface $con = null) Return the first ChildSiteBanner matching the query
 * @method     ChildSiteBanner findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSiteBanner matching the query, or a new ChildSiteBanner object populated from the query conditions when no match is found
 *
 * @method     ChildSiteBanner findOneById(int $id) Return the first ChildSiteBanner filtered by the id column
 * @method     ChildSiteBanner findOneBySiteId(int $site_id) Return the first ChildSiteBanner filtered by the site_id column
 * @method     ChildSiteBanner findOneByAboutTxt(string $about_txt) Return the first ChildSiteBanner filtered by the about_txt column
 * @method     ChildSiteBanner findOneByAlignment(string $alignment) Return the first ChildSiteBanner filtered by the alignment column
 * @method     ChildSiteBanner findOneBySlogan(string $slogan) Return the first ChildSiteBanner filtered by the slogan column
 * @method     ChildSiteBanner findOneBySubText(string $sub_text) Return the first ChildSiteBanner filtered by the sub_text column
 * @method     ChildSiteBanner findOneByHasInfoButton(boolean $has_info_button) Return the first ChildSiteBanner filtered by the has_info_button column
 * @method     ChildSiteBanner findOneByInfoLink(string $info_link) Return the first ChildSiteBanner filtered by the info_link column
 * @method     ChildSiteBanner findOneByHasBuyNow(boolean $has_buy_now) Return the first ChildSiteBanner filtered by the has_buy_now column
 * @method     ChildSiteBanner findOneByBuyNowLink(string $buy_now_link) Return the first ChildSiteBanner filtered by the buy_now_link column
 * @method     ChildSiteBanner findOneByFileExt(string $file_ext) Return the first ChildSiteBanner filtered by the file_ext column *

 * @method     ChildSiteBanner requirePk($key, ConnectionInterface $con = null) Return the ChildSiteBanner by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBanner requireOne(ConnectionInterface $con = null) Return the first ChildSiteBanner matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteBanner requireOneById(int $id) Return the first ChildSiteBanner filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBanner requireOneBySiteId(int $site_id) Return the first ChildSiteBanner filtered by the site_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBanner requireOneByAboutTxt(string $about_txt) Return the first ChildSiteBanner filtered by the about_txt column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBanner requireOneByAlignment(string $alignment) Return the first ChildSiteBanner filtered by the alignment column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBanner requireOneBySlogan(string $slogan) Return the first ChildSiteBanner filtered by the slogan column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBanner requireOneBySubText(string $sub_text) Return the first ChildSiteBanner filtered by the sub_text column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBanner requireOneByHasInfoButton(boolean $has_info_button) Return the first ChildSiteBanner filtered by the has_info_button column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBanner requireOneByInfoLink(string $info_link) Return the first ChildSiteBanner filtered by the info_link column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBanner requireOneByHasBuyNow(boolean $has_buy_now) Return the first ChildSiteBanner filtered by the has_buy_now column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBanner requireOneByBuyNowLink(string $buy_now_link) Return the first ChildSiteBanner filtered by the buy_now_link column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteBanner requireOneByFileExt(string $file_ext) Return the first ChildSiteBanner filtered by the file_ext column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteBanner[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSiteBanner objects based on current ModelCriteria
 * @method     ChildSiteBanner[]|ObjectCollection findById(int $id) Return ChildSiteBanner objects filtered by the id column
 * @method     ChildSiteBanner[]|ObjectCollection findBySiteId(int $site_id) Return ChildSiteBanner objects filtered by the site_id column
 * @method     ChildSiteBanner[]|ObjectCollection findByAboutTxt(string $about_txt) Return ChildSiteBanner objects filtered by the about_txt column
 * @method     ChildSiteBanner[]|ObjectCollection findByAlignment(string $alignment) Return ChildSiteBanner objects filtered by the alignment column
 * @method     ChildSiteBanner[]|ObjectCollection findBySlogan(string $slogan) Return ChildSiteBanner objects filtered by the slogan column
 * @method     ChildSiteBanner[]|ObjectCollection findBySubText(string $sub_text) Return ChildSiteBanner objects filtered by the sub_text column
 * @method     ChildSiteBanner[]|ObjectCollection findByHasInfoButton(boolean $has_info_button) Return ChildSiteBanner objects filtered by the has_info_button column
 * @method     ChildSiteBanner[]|ObjectCollection findByInfoLink(string $info_link) Return ChildSiteBanner objects filtered by the info_link column
 * @method     ChildSiteBanner[]|ObjectCollection findByHasBuyNow(boolean $has_buy_now) Return ChildSiteBanner objects filtered by the has_buy_now column
 * @method     ChildSiteBanner[]|ObjectCollection findByBuyNowLink(string $buy_now_link) Return ChildSiteBanner objects filtered by the buy_now_link column
 * @method     ChildSiteBanner[]|ObjectCollection findByFileExt(string $file_ext) Return ChildSiteBanner objects filtered by the file_ext column
 * @method     ChildSiteBanner[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteBannerQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Cms\Base\SiteBannerQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Cms\\SiteBanner', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteBannerQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteBannerQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteBannerQuery) {
            return $criteria;
        }
        $query = new ChildSiteBannerQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSiteBanner|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteBannerTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteBannerTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteBanner A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, site_id, about_txt, alignment, slogan, sub_text, has_info_button, info_link, has_buy_now, buy_now_link, file_ext FROM site_banner WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSiteBanner $obj */
            $obj = new ChildSiteBanner();
            $obj->hydrate($row);
            SiteBannerTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSiteBanner|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteBannerTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteBannerTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteBannerTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteBannerTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBannerTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the site_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE site_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE site_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE site_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(SiteBannerTableMap::COL_SITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(SiteBannerTableMap::COL_SITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBannerTableMap::COL_SITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the about_txt column
     *
     * Example usage:
     * <code>
     * $query->filterByAboutTxt('fooValue');   // WHERE about_txt = 'fooValue'
     * $query->filterByAboutTxt('%fooValue%', Criteria::LIKE); // WHERE about_txt LIKE '%fooValue%'
     * </code>
     *
     * @param     string $aboutTxt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterByAboutTxt($aboutTxt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($aboutTxt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBannerTableMap::COL_ABOUT_TXT, $aboutTxt, $comparison);
    }

    /**
     * Filter the query on the alignment column
     *
     * Example usage:
     * <code>
     * $query->filterByAlignment('fooValue');   // WHERE alignment = 'fooValue'
     * $query->filterByAlignment('%fooValue%', Criteria::LIKE); // WHERE alignment LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alignment The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterByAlignment($alignment = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alignment)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBannerTableMap::COL_ALIGNMENT, $alignment, $comparison);
    }

    /**
     * Filter the query on the slogan column
     *
     * Example usage:
     * <code>
     * $query->filterBySlogan('fooValue');   // WHERE slogan = 'fooValue'
     * $query->filterBySlogan('%fooValue%', Criteria::LIKE); // WHERE slogan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $slogan The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterBySlogan($slogan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($slogan)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBannerTableMap::COL_SLOGAN, $slogan, $comparison);
    }

    /**
     * Filter the query on the sub_text column
     *
     * Example usage:
     * <code>
     * $query->filterBySubText('fooValue');   // WHERE sub_text = 'fooValue'
     * $query->filterBySubText('%fooValue%', Criteria::LIKE); // WHERE sub_text LIKE '%fooValue%'
     * </code>
     *
     * @param     string $subText The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterBySubText($subText = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($subText)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBannerTableMap::COL_SUB_TEXT, $subText, $comparison);
    }

    /**
     * Filter the query on the has_info_button column
     *
     * Example usage:
     * <code>
     * $query->filterByHasInfoButton(true); // WHERE has_info_button = true
     * $query->filterByHasInfoButton('yes'); // WHERE has_info_button = true
     * </code>
     *
     * @param     boolean|string $hasInfoButton The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterByHasInfoButton($hasInfoButton = null, $comparison = null)
    {
        if (is_string($hasInfoButton)) {
            $hasInfoButton = in_array(strtolower($hasInfoButton), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SiteBannerTableMap::COL_HAS_INFO_BUTTON, $hasInfoButton, $comparison);
    }

    /**
     * Filter the query on the info_link column
     *
     * Example usage:
     * <code>
     * $query->filterByInfoLink('fooValue');   // WHERE info_link = 'fooValue'
     * $query->filterByInfoLink('%fooValue%', Criteria::LIKE); // WHERE info_link LIKE '%fooValue%'
     * </code>
     *
     * @param     string $infoLink The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterByInfoLink($infoLink = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($infoLink)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBannerTableMap::COL_INFO_LINK, $infoLink, $comparison);
    }

    /**
     * Filter the query on the has_buy_now column
     *
     * Example usage:
     * <code>
     * $query->filterByHasBuyNow(true); // WHERE has_buy_now = true
     * $query->filterByHasBuyNow('yes'); // WHERE has_buy_now = true
     * </code>
     *
     * @param     boolean|string $hasBuyNow The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterByHasBuyNow($hasBuyNow = null, $comparison = null)
    {
        if (is_string($hasBuyNow)) {
            $hasBuyNow = in_array(strtolower($hasBuyNow), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SiteBannerTableMap::COL_HAS_BUY_NOW, $hasBuyNow, $comparison);
    }

    /**
     * Filter the query on the buy_now_link column
     *
     * Example usage:
     * <code>
     * $query->filterByBuyNowLink('fooValue');   // WHERE buy_now_link = 'fooValue'
     * $query->filterByBuyNowLink('%fooValue%', Criteria::LIKE); // WHERE buy_now_link LIKE '%fooValue%'
     * </code>
     *
     * @param     string $buyNowLink The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterByBuyNowLink($buyNowLink = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($buyNowLink)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBannerTableMap::COL_BUY_NOW_LINK, $buyNowLink, $comparison);
    }

    /**
     * Filter the query on the file_ext column
     *
     * Example usage:
     * <code>
     * $query->filterByFileExt('fooValue');   // WHERE file_ext = 'fooValue'
     * $query->filterByFileExt('%fooValue%', Criteria::LIKE); // WHERE file_ext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fileExt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterByFileExt($fileExt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fileExt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteBannerTableMap::COL_FILE_EXT, $fileExt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Cms\Site object
     *
     * @param \Model\Cms\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteBannerQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \Model\Cms\Site) {
            return $this
                ->addUsingAlias(SiteBannerTableMap::COL_SITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteBannerTableMap::COL_SITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \Model\Cms\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\Model\Cms\SiteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSiteBanner $siteBanner Object to remove from the list of results
     *
     * @return $this|ChildSiteBannerQuery The current query, for fluid interface
     */
    public function prune($siteBanner = null)
    {
        if ($siteBanner) {
            $this->addUsingAlias(SiteBannerTableMap::COL_ID, $siteBanner->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the site_banner table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteBannerTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteBannerTableMap::clearInstancePool();
            SiteBannerTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteBannerTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteBannerTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteBannerTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteBannerTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SiteBannerQuery
