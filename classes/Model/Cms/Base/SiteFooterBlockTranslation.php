<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\Cms\SiteFooterBlock as ChildSiteFooterBlock;
use Model\Cms\SiteFooterBlockQuery as ChildSiteFooterBlockQuery;
use Model\Cms\SiteFooterBlockTranslation as ChildSiteFooterBlockTranslation;
use Model\Cms\SiteFooterBlockTranslationQuery as ChildSiteFooterBlockTranslationQuery;
use Model\Cms\SiteFooterBlockTranslationSitePage as ChildSiteFooterBlockTranslationSitePage;
use Model\Cms\SiteFooterBlockTranslationSitePageQuery as ChildSiteFooterBlockTranslationSitePageQuery;
use Model\Cms\Map\SiteFooterBlockTranslationSitePageTableMap;
use Model\Cms\Map\SiteFooterBlockTranslationTableMap;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'site_footer_block_translation' table.
 *
 *
 *
 * @package    propel.generator.Model.Cms.Base
 */
abstract class SiteFooterBlockTranslation implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Cms\\Map\\SiteFooterBlockTranslationTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the site_footer_block_id field.
     *
     * @var        int
     */
    protected $site_footer_block_id;

    /**
     * The value for the language_id field.
     *
     * @var        int
     */
    protected $language_id;

    /**
     * The value for the title field.
     *
     * @var        string|null
     */
    protected $title;

    /**
     * The value for the html_contents field.
     *
     * @var        string|null
     */
    protected $html_contents;

    /**
     * @var        Language
     */
    protected $aLanguage;

    /**
     * @var        ChildSiteFooterBlock
     */
    protected $aSiteFooterBlock;

    /**
     * @var        ObjectCollection|ChildSiteFooterBlockTranslationSitePage[] Collection to store aggregation of ChildSiteFooterBlockTranslationSitePage objects.
     */
    protected $collSiteFooterBlockTranslationSitePages;
    protected $collSiteFooterBlockTranslationSitePagesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSiteFooterBlockTranslationSitePage[]
     */
    protected $siteFooterBlockTranslationSitePagesScheduledForDeletion = null;

    /**
     * Initializes internal state of Model\Cms\Base\SiteFooterBlockTranslation object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>SiteFooterBlockTranslation</code> instance.  If
     * <code>obj</code> is an instance of <code>SiteFooterBlockTranslation</code>, delegates to
     * <code>equals(SiteFooterBlockTranslation)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [site_footer_block_id] column value.
     *
     * @return int
     */
    public function getSiteFooterBlockId()
    {
        return $this->site_footer_block_id;
    }

    /**
     * Get the [language_id] column value.
     *
     * @return int
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * Get the [title] column value.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [html_contents] column value.
     *
     * @return string|null
     */
    public function getHtmlContents()
    {
        return $this->html_contents;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Cms\SiteFooterBlockTranslation The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[SiteFooterBlockTranslationTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [site_footer_block_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Cms\SiteFooterBlockTranslation The current object (for fluent API support)
     */
    public function setSiteFooterBlockId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->site_footer_block_id !== $v) {
            $this->site_footer_block_id = $v;
            $this->modifiedColumns[SiteFooterBlockTranslationTableMap::COL_SITE_FOOTER_BLOCK_ID] = true;
        }

        if ($this->aSiteFooterBlock !== null && $this->aSiteFooterBlock->getId() !== $v) {
            $this->aSiteFooterBlock = null;
        }

        return $this;
    } // setSiteFooterBlockId()

    /**
     * Set the value of [language_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Cms\SiteFooterBlockTranslation The current object (for fluent API support)
     */
    public function setLanguageId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->language_id !== $v) {
            $this->language_id = $v;
            $this->modifiedColumns[SiteFooterBlockTranslationTableMap::COL_LANGUAGE_ID] = true;
        }

        if ($this->aLanguage !== null && $this->aLanguage->getId() !== $v) {
            $this->aLanguage = null;
        }

        return $this;
    } // setLanguageId()

    /**
     * Set the value of [title] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SiteFooterBlockTranslation The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[SiteFooterBlockTranslationTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [html_contents] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SiteFooterBlockTranslation The current object (for fluent API support)
     */
    public function setHtmlContents($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->html_contents !== $v) {
            $this->html_contents = $v;
            $this->modifiedColumns[SiteFooterBlockTranslationTableMap::COL_HTML_CONTENTS] = true;
        }

        return $this;
    } // setHtmlContents()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SiteFooterBlockTranslationTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SiteFooterBlockTranslationTableMap::translateFieldName('SiteFooterBlockId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->site_footer_block_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SiteFooterBlockTranslationTableMap::translateFieldName('LanguageId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->language_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SiteFooterBlockTranslationTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : SiteFooterBlockTranslationTableMap::translateFieldName('HtmlContents', TableMap::TYPE_PHPNAME, $indexType)];
            $this->html_contents = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 5; // 5 = SiteFooterBlockTranslationTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Cms\\SiteFooterBlockTranslation'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aSiteFooterBlock !== null && $this->site_footer_block_id !== $this->aSiteFooterBlock->getId()) {
            $this->aSiteFooterBlock = null;
        }
        if ($this->aLanguage !== null && $this->language_id !== $this->aLanguage->getId()) {
            $this->aLanguage = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteFooterBlockTranslationTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSiteFooterBlockTranslationQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aLanguage = null;
            $this->aSiteFooterBlock = null;
            $this->collSiteFooterBlockTranslationSitePages = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see SiteFooterBlockTranslation::setDeleted()
     * @see SiteFooterBlockTranslation::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteFooterBlockTranslationTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSiteFooterBlockTranslationQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteFooterBlockTranslationTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SiteFooterBlockTranslationTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aLanguage !== null) {
                if ($this->aLanguage->isModified() || $this->aLanguage->isNew()) {
                    $affectedRows += $this->aLanguage->save($con);
                }
                $this->setLanguage($this->aLanguage);
            }

            if ($this->aSiteFooterBlock !== null) {
                if ($this->aSiteFooterBlock->isModified() || $this->aSiteFooterBlock->isNew()) {
                    $affectedRows += $this->aSiteFooterBlock->save($con);
                }
                $this->setSiteFooterBlock($this->aSiteFooterBlock);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->siteFooterBlockTranslationSitePagesScheduledForDeletion !== null) {
                if (!$this->siteFooterBlockTranslationSitePagesScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SiteFooterBlockTranslationSitePageQuery::create()
                        ->filterByPrimaryKeys($this->siteFooterBlockTranslationSitePagesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteFooterBlockTranslationSitePagesScheduledForDeletion = null;
                }
            }

            if ($this->collSiteFooterBlockTranslationSitePages !== null) {
                foreach ($this->collSiteFooterBlockTranslationSitePages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[SiteFooterBlockTranslationTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SiteFooterBlockTranslationTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SiteFooterBlockTranslationTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(SiteFooterBlockTranslationTableMap::COL_SITE_FOOTER_BLOCK_ID)) {
            $modifiedColumns[':p' . $index++]  = 'site_footer_block_id';
        }
        if ($this->isColumnModified(SiteFooterBlockTranslationTableMap::COL_LANGUAGE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'language_id';
        }
        if ($this->isColumnModified(SiteFooterBlockTranslationTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(SiteFooterBlockTranslationTableMap::COL_HTML_CONTENTS)) {
            $modifiedColumns[':p' . $index++]  = 'html_contents';
        }

        $sql = sprintf(
            'INSERT INTO site_footer_block_translation (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'site_footer_block_id':
                        $stmt->bindValue($identifier, $this->site_footer_block_id, PDO::PARAM_INT);
                        break;
                    case 'language_id':
                        $stmt->bindValue($identifier, $this->language_id, PDO::PARAM_INT);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'html_contents':
                        $stmt->bindValue($identifier, $this->html_contents, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SiteFooterBlockTranslationTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getSiteFooterBlockId();
                break;
            case 2:
                return $this->getLanguageId();
                break;
            case 3:
                return $this->getTitle();
                break;
            case 4:
                return $this->getHtmlContents();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['SiteFooterBlockTranslation'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SiteFooterBlockTranslation'][$this->hashCode()] = true;
        $keys = SiteFooterBlockTranslationTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getSiteFooterBlockId(),
            $keys[2] => $this->getLanguageId(),
            $keys[3] => $this->getTitle(),
            $keys[4] => $this->getHtmlContents(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aLanguage) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'language';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_language';
                        break;
                    default:
                        $key = 'Language';
                }

                $result[$key] = $this->aLanguage->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSiteFooterBlock) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteFooterBlock';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_footer_block';
                        break;
                    default:
                        $key = 'SiteFooterBlock';
                }

                $result[$key] = $this->aSiteFooterBlock->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collSiteFooterBlockTranslationSitePages) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteFooterBlockTranslationSitePages';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_footer_block_translation_site_pages';
                        break;
                    default:
                        $key = 'SiteFooterBlockTranslationSitePages';
                }

                $result[$key] = $this->collSiteFooterBlockTranslationSitePages->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Cms\SiteFooterBlockTranslation
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SiteFooterBlockTranslationTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Cms\SiteFooterBlockTranslation
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setSiteFooterBlockId($value);
                break;
            case 2:
                $this->setLanguageId($value);
                break;
            case 3:
                $this->setTitle($value);
                break;
            case 4:
                $this->setHtmlContents($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SiteFooterBlockTranslationTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setSiteFooterBlockId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setLanguageId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setTitle($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setHtmlContents($arr[$keys[4]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Cms\SiteFooterBlockTranslation The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SiteFooterBlockTranslationTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SiteFooterBlockTranslationTableMap::COL_ID)) {
            $criteria->add(SiteFooterBlockTranslationTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(SiteFooterBlockTranslationTableMap::COL_SITE_FOOTER_BLOCK_ID)) {
            $criteria->add(SiteFooterBlockTranslationTableMap::COL_SITE_FOOTER_BLOCK_ID, $this->site_footer_block_id);
        }
        if ($this->isColumnModified(SiteFooterBlockTranslationTableMap::COL_LANGUAGE_ID)) {
            $criteria->add(SiteFooterBlockTranslationTableMap::COL_LANGUAGE_ID, $this->language_id);
        }
        if ($this->isColumnModified(SiteFooterBlockTranslationTableMap::COL_TITLE)) {
            $criteria->add(SiteFooterBlockTranslationTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(SiteFooterBlockTranslationTableMap::COL_HTML_CONTENTS)) {
            $criteria->add(SiteFooterBlockTranslationTableMap::COL_HTML_CONTENTS, $this->html_contents);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSiteFooterBlockTranslationQuery::create();
        $criteria->add(SiteFooterBlockTranslationTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Cms\SiteFooterBlockTranslation (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSiteFooterBlockId($this->getSiteFooterBlockId());
        $copyObj->setLanguageId($this->getLanguageId());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setHtmlContents($this->getHtmlContents());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getSiteFooterBlockTranslationSitePages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteFooterBlockTranslationSitePage($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Cms\SiteFooterBlockTranslation Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a Language object.
     *
     * @param  Language $v
     * @return $this|\Model\Cms\SiteFooterBlockTranslation The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLanguage(Language $v = null)
    {
        if ($v === null) {
            $this->setLanguageId(NULL);
        } else {
            $this->setLanguageId($v->getId());
        }

        $this->aLanguage = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Language object, it will not be re-added.
        if ($v !== null) {
            $v->addSiteFooterBlockTranslation($this);
        }


        return $this;
    }


    /**
     * Get the associated Language object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Language The associated Language object.
     * @throws PropelException
     */
    public function getLanguage(ConnectionInterface $con = null)
    {
        if ($this->aLanguage === null && ($this->language_id != 0)) {
            $this->aLanguage = LanguageQuery::create()->findPk($this->language_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLanguage->addSiteFooterBlockTranslations($this);
             */
        }

        return $this->aLanguage;
    }

    /**
     * Declares an association between this object and a ChildSiteFooterBlock object.
     *
     * @param  ChildSiteFooterBlock $v
     * @return $this|\Model\Cms\SiteFooterBlockTranslation The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSiteFooterBlock(ChildSiteFooterBlock $v = null)
    {
        if ($v === null) {
            $this->setSiteFooterBlockId(NULL);
        } else {
            $this->setSiteFooterBlockId($v->getId());
        }

        $this->aSiteFooterBlock = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSiteFooterBlock object, it will not be re-added.
        if ($v !== null) {
            $v->addSiteFooterBlockTranslation($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSiteFooterBlock object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSiteFooterBlock The associated ChildSiteFooterBlock object.
     * @throws PropelException
     */
    public function getSiteFooterBlock(ConnectionInterface $con = null)
    {
        if ($this->aSiteFooterBlock === null && ($this->site_footer_block_id != 0)) {
            $this->aSiteFooterBlock = ChildSiteFooterBlockQuery::create()->findPk($this->site_footer_block_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSiteFooterBlock->addSiteFooterBlockTranslations($this);
             */
        }

        return $this->aSiteFooterBlock;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('SiteFooterBlockTranslationSitePage' === $relationName) {
            $this->initSiteFooterBlockTranslationSitePages();
            return;
        }
    }

    /**
     * Clears out the collSiteFooterBlockTranslationSitePages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteFooterBlockTranslationSitePages()
     */
    public function clearSiteFooterBlockTranslationSitePages()
    {
        $this->collSiteFooterBlockTranslationSitePages = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteFooterBlockTranslationSitePages collection loaded partially.
     */
    public function resetPartialSiteFooterBlockTranslationSitePages($v = true)
    {
        $this->collSiteFooterBlockTranslationSitePagesPartial = $v;
    }

    /**
     * Initializes the collSiteFooterBlockTranslationSitePages collection.
     *
     * By default this just sets the collSiteFooterBlockTranslationSitePages collection to an empty array (like clearcollSiteFooterBlockTranslationSitePages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteFooterBlockTranslationSitePages($overrideExisting = true)
    {
        if (null !== $this->collSiteFooterBlockTranslationSitePages && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteFooterBlockTranslationSitePageTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteFooterBlockTranslationSitePages = new $collectionClassName;
        $this->collSiteFooterBlockTranslationSitePages->setModel('\Model\Cms\SiteFooterBlockTranslationSitePage');
    }

    /**
     * Gets an array of ChildSiteFooterBlockTranslationSitePage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSiteFooterBlockTranslation is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSiteFooterBlockTranslationSitePage[] List of ChildSiteFooterBlockTranslationSitePage objects
     * @throws PropelException
     */
    public function getSiteFooterBlockTranslationSitePages(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFooterBlockTranslationSitePagesPartial && !$this->isNew();
        if (null === $this->collSiteFooterBlockTranslationSitePages || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteFooterBlockTranslationSitePages) {
                    $this->initSiteFooterBlockTranslationSitePages();
                } else {
                    $collectionClassName = SiteFooterBlockTranslationSitePageTableMap::getTableMap()->getCollectionClassName();

                    $collSiteFooterBlockTranslationSitePages = new $collectionClassName;
                    $collSiteFooterBlockTranslationSitePages->setModel('\Model\Cms\SiteFooterBlockTranslationSitePage');

                    return $collSiteFooterBlockTranslationSitePages;
                }
            } else {
                $collSiteFooterBlockTranslationSitePages = ChildSiteFooterBlockTranslationSitePageQuery::create(null, $criteria)
                    ->filterBySiteFooterBlockTranslation($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteFooterBlockTranslationSitePagesPartial && count($collSiteFooterBlockTranslationSitePages)) {
                        $this->initSiteFooterBlockTranslationSitePages(false);

                        foreach ($collSiteFooterBlockTranslationSitePages as $obj) {
                            if (false == $this->collSiteFooterBlockTranslationSitePages->contains($obj)) {
                                $this->collSiteFooterBlockTranslationSitePages->append($obj);
                            }
                        }

                        $this->collSiteFooterBlockTranslationSitePagesPartial = true;
                    }

                    return $collSiteFooterBlockTranslationSitePages;
                }

                if ($partial && $this->collSiteFooterBlockTranslationSitePages) {
                    foreach ($this->collSiteFooterBlockTranslationSitePages as $obj) {
                        if ($obj->isNew()) {
                            $collSiteFooterBlockTranslationSitePages[] = $obj;
                        }
                    }
                }

                $this->collSiteFooterBlockTranslationSitePages = $collSiteFooterBlockTranslationSitePages;
                $this->collSiteFooterBlockTranslationSitePagesPartial = false;
            }
        }

        return $this->collSiteFooterBlockTranslationSitePages;
    }

    /**
     * Sets a collection of ChildSiteFooterBlockTranslationSitePage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteFooterBlockTranslationSitePages A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSiteFooterBlockTranslation The current object (for fluent API support)
     */
    public function setSiteFooterBlockTranslationSitePages(Collection $siteFooterBlockTranslationSitePages, ConnectionInterface $con = null)
    {
        /** @var ChildSiteFooterBlockTranslationSitePage[] $siteFooterBlockTranslationSitePagesToDelete */
        $siteFooterBlockTranslationSitePagesToDelete = $this->getSiteFooterBlockTranslationSitePages(new Criteria(), $con)->diff($siteFooterBlockTranslationSitePages);


        $this->siteFooterBlockTranslationSitePagesScheduledForDeletion = $siteFooterBlockTranslationSitePagesToDelete;

        foreach ($siteFooterBlockTranslationSitePagesToDelete as $siteFooterBlockTranslationSitePageRemoved) {
            $siteFooterBlockTranslationSitePageRemoved->setSiteFooterBlockTranslation(null);
        }

        $this->collSiteFooterBlockTranslationSitePages = null;
        foreach ($siteFooterBlockTranslationSitePages as $siteFooterBlockTranslationSitePage) {
            $this->addSiteFooterBlockTranslationSitePage($siteFooterBlockTranslationSitePage);
        }

        $this->collSiteFooterBlockTranslationSitePages = $siteFooterBlockTranslationSitePages;
        $this->collSiteFooterBlockTranslationSitePagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SiteFooterBlockTranslationSitePage objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SiteFooterBlockTranslationSitePage objects.
     * @throws PropelException
     */
    public function countSiteFooterBlockTranslationSitePages(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFooterBlockTranslationSitePagesPartial && !$this->isNew();
        if (null === $this->collSiteFooterBlockTranslationSitePages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteFooterBlockTranslationSitePages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteFooterBlockTranslationSitePages());
            }

            $query = ChildSiteFooterBlockTranslationSitePageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySiteFooterBlockTranslation($this)
                ->count($con);
        }

        return count($this->collSiteFooterBlockTranslationSitePages);
    }

    /**
     * Method called to associate a ChildSiteFooterBlockTranslationSitePage object to this object
     * through the ChildSiteFooterBlockTranslationSitePage foreign key attribute.
     *
     * @param  ChildSiteFooterBlockTranslationSitePage $l ChildSiteFooterBlockTranslationSitePage
     * @return $this|\Model\Cms\SiteFooterBlockTranslation The current object (for fluent API support)
     */
    public function addSiteFooterBlockTranslationSitePage(ChildSiteFooterBlockTranslationSitePage $l)
    {
        if ($this->collSiteFooterBlockTranslationSitePages === null) {
            $this->initSiteFooterBlockTranslationSitePages();
            $this->collSiteFooterBlockTranslationSitePagesPartial = true;
        }

        if (!$this->collSiteFooterBlockTranslationSitePages->contains($l)) {
            $this->doAddSiteFooterBlockTranslationSitePage($l);

            if ($this->siteFooterBlockTranslationSitePagesScheduledForDeletion and $this->siteFooterBlockTranslationSitePagesScheduledForDeletion->contains($l)) {
                $this->siteFooterBlockTranslationSitePagesScheduledForDeletion->remove($this->siteFooterBlockTranslationSitePagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSiteFooterBlockTranslationSitePage $siteFooterBlockTranslationSitePage The ChildSiteFooterBlockTranslationSitePage object to add.
     */
    protected function doAddSiteFooterBlockTranslationSitePage(ChildSiteFooterBlockTranslationSitePage $siteFooterBlockTranslationSitePage)
    {
        $this->collSiteFooterBlockTranslationSitePages[]= $siteFooterBlockTranslationSitePage;
        $siteFooterBlockTranslationSitePage->setSiteFooterBlockTranslation($this);
    }

    /**
     * @param  ChildSiteFooterBlockTranslationSitePage $siteFooterBlockTranslationSitePage The ChildSiteFooterBlockTranslationSitePage object to remove.
     * @return $this|ChildSiteFooterBlockTranslation The current object (for fluent API support)
     */
    public function removeSiteFooterBlockTranslationSitePage(ChildSiteFooterBlockTranslationSitePage $siteFooterBlockTranslationSitePage)
    {
        if ($this->getSiteFooterBlockTranslationSitePages()->contains($siteFooterBlockTranslationSitePage)) {
            $pos = $this->collSiteFooterBlockTranslationSitePages->search($siteFooterBlockTranslationSitePage);
            $this->collSiteFooterBlockTranslationSitePages->remove($pos);
            if (null === $this->siteFooterBlockTranslationSitePagesScheduledForDeletion) {
                $this->siteFooterBlockTranslationSitePagesScheduledForDeletion = clone $this->collSiteFooterBlockTranslationSitePages;
                $this->siteFooterBlockTranslationSitePagesScheduledForDeletion->clear();
            }
            $this->siteFooterBlockTranslationSitePagesScheduledForDeletion[]= clone $siteFooterBlockTranslationSitePage;
            $siteFooterBlockTranslationSitePage->setSiteFooterBlockTranslation(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this SiteFooterBlockTranslation is new, it will return
     * an empty collection; or if this SiteFooterBlockTranslation has previously
     * been saved, it will retrieve related SiteFooterBlockTranslationSitePages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in SiteFooterBlockTranslation.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSiteFooterBlockTranslationSitePage[] List of ChildSiteFooterBlockTranslationSitePage objects
     */
    public function getSiteFooterBlockTranslationSitePagesJoinSitePage(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSiteFooterBlockTranslationSitePageQuery::create(null, $criteria);
        $query->joinWith('SitePage', $joinBehavior);

        return $this->getSiteFooterBlockTranslationSitePages($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aLanguage) {
            $this->aLanguage->removeSiteFooterBlockTranslation($this);
        }
        if (null !== $this->aSiteFooterBlock) {
            $this->aSiteFooterBlock->removeSiteFooterBlockTranslation($this);
        }
        $this->id = null;
        $this->site_footer_block_id = null;
        $this->language_id = null;
        $this->title = null;
        $this->html_contents = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collSiteFooterBlockTranslationSitePages) {
                foreach ($this->collSiteFooterBlockTranslationSitePages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collSiteFooterBlockTranslationSitePages = null;
        $this->aLanguage = null;
        $this->aSiteFooterBlock = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SiteFooterBlockTranslationTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
