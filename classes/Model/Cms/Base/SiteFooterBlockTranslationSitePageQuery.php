<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\Cms\SiteFooterBlockTranslationSitePage as ChildSiteFooterBlockTranslationSitePage;
use Model\Cms\SiteFooterBlockTranslationSitePageQuery as ChildSiteFooterBlockTranslationSitePageQuery;
use Model\Cms\Map\SiteFooterBlockTranslationSitePageTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'site_footer_block_translation_site_page' table.
 *
 *
 *
 * @method     ChildSiteFooterBlockTranslationSitePageQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSiteFooterBlockTranslationSitePageQuery orderBySiteFooterBlockTranslationId($order = Criteria::ASC) Order by the site_footer_block_translation_id column
 * @method     ChildSiteFooterBlockTranslationSitePageQuery orderBySitePageId($order = Criteria::ASC) Order by the site_page_id column
 *
 * @method     ChildSiteFooterBlockTranslationSitePageQuery groupById() Group by the id column
 * @method     ChildSiteFooterBlockTranslationSitePageQuery groupBySiteFooterBlockTranslationId() Group by the site_footer_block_translation_id column
 * @method     ChildSiteFooterBlockTranslationSitePageQuery groupBySitePageId() Group by the site_page_id column
 *
 * @method     ChildSiteFooterBlockTranslationSitePageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteFooterBlockTranslationSitePageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteFooterBlockTranslationSitePageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteFooterBlockTranslationSitePageQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteFooterBlockTranslationSitePageQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteFooterBlockTranslationSitePageQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteFooterBlockTranslationSitePageQuery leftJoinSiteFooterBlockTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteFooterBlockTranslation relation
 * @method     ChildSiteFooterBlockTranslationSitePageQuery rightJoinSiteFooterBlockTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteFooterBlockTranslation relation
 * @method     ChildSiteFooterBlockTranslationSitePageQuery innerJoinSiteFooterBlockTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteFooterBlockTranslation relation
 *
 * @method     ChildSiteFooterBlockTranslationSitePageQuery joinWithSiteFooterBlockTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteFooterBlockTranslation relation
 *
 * @method     ChildSiteFooterBlockTranslationSitePageQuery leftJoinWithSiteFooterBlockTranslation() Adds a LEFT JOIN clause and with to the query using the SiteFooterBlockTranslation relation
 * @method     ChildSiteFooterBlockTranslationSitePageQuery rightJoinWithSiteFooterBlockTranslation() Adds a RIGHT JOIN clause and with to the query using the SiteFooterBlockTranslation relation
 * @method     ChildSiteFooterBlockTranslationSitePageQuery innerJoinWithSiteFooterBlockTranslation() Adds a INNER JOIN clause and with to the query using the SiteFooterBlockTranslation relation
 *
 * @method     ChildSiteFooterBlockTranslationSitePageQuery leftJoinSitePage($relationAlias = null) Adds a LEFT JOIN clause to the query using the SitePage relation
 * @method     ChildSiteFooterBlockTranslationSitePageQuery rightJoinSitePage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SitePage relation
 * @method     ChildSiteFooterBlockTranslationSitePageQuery innerJoinSitePage($relationAlias = null) Adds a INNER JOIN clause to the query using the SitePage relation
 *
 * @method     ChildSiteFooterBlockTranslationSitePageQuery joinWithSitePage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SitePage relation
 *
 * @method     ChildSiteFooterBlockTranslationSitePageQuery leftJoinWithSitePage() Adds a LEFT JOIN clause and with to the query using the SitePage relation
 * @method     ChildSiteFooterBlockTranslationSitePageQuery rightJoinWithSitePage() Adds a RIGHT JOIN clause and with to the query using the SitePage relation
 * @method     ChildSiteFooterBlockTranslationSitePageQuery innerJoinWithSitePage() Adds a INNER JOIN clause and with to the query using the SitePage relation
 *
 * @method     \Model\Cms\SiteFooterBlockTranslationQuery|\Model\Cms\SitePageQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSiteFooterBlockTranslationSitePage findOne(ConnectionInterface $con = null) Return the first ChildSiteFooterBlockTranslationSitePage matching the query
 * @method     ChildSiteFooterBlockTranslationSitePage findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSiteFooterBlockTranslationSitePage matching the query, or a new ChildSiteFooterBlockTranslationSitePage object populated from the query conditions when no match is found
 *
 * @method     ChildSiteFooterBlockTranslationSitePage findOneById(int $id) Return the first ChildSiteFooterBlockTranslationSitePage filtered by the id column
 * @method     ChildSiteFooterBlockTranslationSitePage findOneBySiteFooterBlockTranslationId(int $site_footer_block_translation_id) Return the first ChildSiteFooterBlockTranslationSitePage filtered by the site_footer_block_translation_id column
 * @method     ChildSiteFooterBlockTranslationSitePage findOneBySitePageId(int $site_page_id) Return the first ChildSiteFooterBlockTranslationSitePage filtered by the site_page_id column *

 * @method     ChildSiteFooterBlockTranslationSitePage requirePk($key, ConnectionInterface $con = null) Return the ChildSiteFooterBlockTranslationSitePage by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFooterBlockTranslationSitePage requireOne(ConnectionInterface $con = null) Return the first ChildSiteFooterBlockTranslationSitePage matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteFooterBlockTranslationSitePage requireOneById(int $id) Return the first ChildSiteFooterBlockTranslationSitePage filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFooterBlockTranslationSitePage requireOneBySiteFooterBlockTranslationId(int $site_footer_block_translation_id) Return the first ChildSiteFooterBlockTranslationSitePage filtered by the site_footer_block_translation_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFooterBlockTranslationSitePage requireOneBySitePageId(int $site_page_id) Return the first ChildSiteFooterBlockTranslationSitePage filtered by the site_page_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteFooterBlockTranslationSitePage[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSiteFooterBlockTranslationSitePage objects based on current ModelCriteria
 * @method     ChildSiteFooterBlockTranslationSitePage[]|ObjectCollection findById(int $id) Return ChildSiteFooterBlockTranslationSitePage objects filtered by the id column
 * @method     ChildSiteFooterBlockTranslationSitePage[]|ObjectCollection findBySiteFooterBlockTranslationId(int $site_footer_block_translation_id) Return ChildSiteFooterBlockTranslationSitePage objects filtered by the site_footer_block_translation_id column
 * @method     ChildSiteFooterBlockTranslationSitePage[]|ObjectCollection findBySitePageId(int $site_page_id) Return ChildSiteFooterBlockTranslationSitePage objects filtered by the site_page_id column
 * @method     ChildSiteFooterBlockTranslationSitePage[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteFooterBlockTranslationSitePageQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Cms\Base\SiteFooterBlockTranslationSitePageQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Cms\\SiteFooterBlockTranslationSitePage', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteFooterBlockTranslationSitePageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteFooterBlockTranslationSitePageQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteFooterBlockTranslationSitePageQuery) {
            return $criteria;
        }
        $query = new ChildSiteFooterBlockTranslationSitePageQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSiteFooterBlockTranslationSitePage|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteFooterBlockTranslationSitePageTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteFooterBlockTranslationSitePageTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteFooterBlockTranslationSitePage A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, site_footer_block_translation_id, site_page_id FROM site_footer_block_translation_site_page WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSiteFooterBlockTranslationSitePage $obj */
            $obj = new ChildSiteFooterBlockTranslationSitePage();
            $obj->hydrate($row);
            SiteFooterBlockTranslationSitePageTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSiteFooterBlockTranslationSitePage|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteFooterBlockTranslationSitePageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteFooterBlockTranslationSitePageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFooterBlockTranslationSitePageQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the site_footer_block_translation_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteFooterBlockTranslationId(1234); // WHERE site_footer_block_translation_id = 1234
     * $query->filterBySiteFooterBlockTranslationId(array(12, 34)); // WHERE site_footer_block_translation_id IN (12, 34)
     * $query->filterBySiteFooterBlockTranslationId(array('min' => 12)); // WHERE site_footer_block_translation_id > 12
     * </code>
     *
     * @see       filterBySiteFooterBlockTranslation()
     *
     * @param     mixed $siteFooterBlockTranslationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFooterBlockTranslationSitePageQuery The current query, for fluid interface
     */
    public function filterBySiteFooterBlockTranslationId($siteFooterBlockTranslationId = null, $comparison = null)
    {
        if (is_array($siteFooterBlockTranslationId)) {
            $useMinMax = false;
            if (isset($siteFooterBlockTranslationId['min'])) {
                $this->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_SITE_FOOTER_BLOCK_TRANSLATION_ID, $siteFooterBlockTranslationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteFooterBlockTranslationId['max'])) {
                $this->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_SITE_FOOTER_BLOCK_TRANSLATION_ID, $siteFooterBlockTranslationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_SITE_FOOTER_BLOCK_TRANSLATION_ID, $siteFooterBlockTranslationId, $comparison);
    }

    /**
     * Filter the query on the site_page_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySitePageId(1234); // WHERE site_page_id = 1234
     * $query->filterBySitePageId(array(12, 34)); // WHERE site_page_id IN (12, 34)
     * $query->filterBySitePageId(array('min' => 12)); // WHERE site_page_id > 12
     * </code>
     *
     * @see       filterBySitePage()
     *
     * @param     mixed $sitePageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFooterBlockTranslationSitePageQuery The current query, for fluid interface
     */
    public function filterBySitePageId($sitePageId = null, $comparison = null)
    {
        if (is_array($sitePageId)) {
            $useMinMax = false;
            if (isset($sitePageId['min'])) {
                $this->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_SITE_PAGE_ID, $sitePageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sitePageId['max'])) {
                $this->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_SITE_PAGE_ID, $sitePageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_SITE_PAGE_ID, $sitePageId, $comparison);
    }

    /**
     * Filter the query by a related \Model\Cms\SiteFooterBlockTranslation object
     *
     * @param \Model\Cms\SiteFooterBlockTranslation|ObjectCollection $siteFooterBlockTranslation The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteFooterBlockTranslationSitePageQuery The current query, for fluid interface
     */
    public function filterBySiteFooterBlockTranslation($siteFooterBlockTranslation, $comparison = null)
    {
        if ($siteFooterBlockTranslation instanceof \Model\Cms\SiteFooterBlockTranslation) {
            return $this
                ->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_SITE_FOOTER_BLOCK_TRANSLATION_ID, $siteFooterBlockTranslation->getId(), $comparison);
        } elseif ($siteFooterBlockTranslation instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_SITE_FOOTER_BLOCK_TRANSLATION_ID, $siteFooterBlockTranslation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySiteFooterBlockTranslation() only accepts arguments of type \Model\Cms\SiteFooterBlockTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteFooterBlockTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteFooterBlockTranslationSitePageQuery The current query, for fluid interface
     */
    public function joinSiteFooterBlockTranslation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteFooterBlockTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteFooterBlockTranslation');
        }

        return $this;
    }

    /**
     * Use the SiteFooterBlockTranslation relation SiteFooterBlockTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteFooterBlockTranslationQuery A secondary query class using the current class as primary query
     */
    public function useSiteFooterBlockTranslationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteFooterBlockTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteFooterBlockTranslation', '\Model\Cms\SiteFooterBlockTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SitePage object
     *
     * @param \Model\Cms\SitePage|ObjectCollection $sitePage The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteFooterBlockTranslationSitePageQuery The current query, for fluid interface
     */
    public function filterBySitePage($sitePage, $comparison = null)
    {
        if ($sitePage instanceof \Model\Cms\SitePage) {
            return $this
                ->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_SITE_PAGE_ID, $sitePage->getId(), $comparison);
        } elseif ($sitePage instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_SITE_PAGE_ID, $sitePage->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySitePage() only accepts arguments of type \Model\Cms\SitePage or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SitePage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteFooterBlockTranslationSitePageQuery The current query, for fluid interface
     */
    public function joinSitePage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SitePage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SitePage');
        }

        return $this;
    }

    /**
     * Use the SitePage relation SitePage object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SitePageQuery A secondary query class using the current class as primary query
     */
    public function useSitePageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSitePage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SitePage', '\Model\Cms\SitePageQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSiteFooterBlockTranslationSitePage $siteFooterBlockTranslationSitePage Object to remove from the list of results
     *
     * @return $this|ChildSiteFooterBlockTranslationSitePageQuery The current query, for fluid interface
     */
    public function prune($siteFooterBlockTranslationSitePage = null)
    {
        if ($siteFooterBlockTranslationSitePage) {
            $this->addUsingAlias(SiteFooterBlockTranslationSitePageTableMap::COL_ID, $siteFooterBlockTranslationSitePage->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the site_footer_block_translation_site_page table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteFooterBlockTranslationSitePageTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteFooterBlockTranslationSitePageTableMap::clearInstancePool();
            SiteFooterBlockTranslationSitePageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteFooterBlockTranslationSitePageTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteFooterBlockTranslationSitePageTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteFooterBlockTranslationSitePageTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteFooterBlockTranslationSitePageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 3600)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SiteFooterBlockTranslationSitePageTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(SiteFooterBlockTranslationSitePageTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

} // SiteFooterBlockTranslationSitePageQuery
