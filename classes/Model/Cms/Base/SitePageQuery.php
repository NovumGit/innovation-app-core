<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\Cms\SitePage as ChildSitePage;
use Model\Cms\SitePageQuery as ChildSitePageQuery;
use Model\Cms\Map\SitePageTableMap;
use Model\Setting\MasterTable\Language;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'site_page' table.
 *
 *
 *
 * @method     ChildSitePageQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSitePageQuery orderByOldId($order = Criteria::ASC) Order by the old_id column
 * @method     ChildSitePageQuery orderByLanguageId($order = Criteria::ASC) Order by the language_id column
 * @method     ChildSitePageQuery orderByNavigationId($order = Criteria::ASC) Order by the navigation_id column
 * @method     ChildSitePageQuery orderBySiteId($order = Criteria::ASC) Order by the site_id column
 * @method     ChildSitePageQuery orderByTag($order = Criteria::ASC) Order by the tag column
 * @method     ChildSitePageQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method     ChildSitePageQuery orderByHasChangeableUrl($order = Criteria::ASC) Order by the has_changeable_url column
 * @method     ChildSitePageQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildSitePageQuery orderByTitleColor($order = Criteria::ASC) Order by the title_color column
 * @method     ChildSitePageQuery orderByTitleBackgroundColor($order = Criteria::ASC) Order by the title_background_color column
 * @method     ChildSitePageQuery orderByMetaDescription($order = Criteria::ASC) Order by the meta_description column
 * @method     ChildSitePageQuery orderByMetaKeywords($order = Criteria::ASC) Order by the meta_keywords column
 * @method     ChildSitePageQuery orderByContent($order = Criteria::ASC) Order by the content column
 * @method     ChildSitePageQuery orderByAbout($order = Criteria::ASC) Order by the about column
 * @method     ChildSitePageQuery orderByMinPictures($order = Criteria::ASC) Order by the min_pictures column
 * @method     ChildSitePageQuery orderByMaxPictures($order = Criteria::ASC) Order by the max_pictures column
 * @method     ChildSitePageQuery orderByTitlePosX($order = Criteria::ASC) Order by the title_pos_x column
 * @method     ChildSitePageQuery orderByTitlePosY($order = Criteria::ASC) Order by the title_pos_y column
 * @method     ChildSitePageQuery orderByHasTitleColorEditor($order = Criteria::ASC) Order by the has_title_color_editor column
 * @method     ChildSitePageQuery orderByHasTitleBackgroundcolorEditor($order = Criteria::ASC) Order by the has_title_backgroundcolor_editor column
 * @method     ChildSitePageQuery orderByHasNavigation($order = Criteria::ASC) Order by the has_navigation column
 * @method     ChildSitePageQuery orderByHasTitle($order = Criteria::ASC) Order by the has_title column
 * @method     ChildSitePageQuery orderByHasTitlePositioningFields($order = Criteria::ASC) Order by the has_title_positioning_fields column
 * @method     ChildSitePageQuery orderByHasContent($order = Criteria::ASC) Order by the has_content column
 * @method     ChildSitePageQuery orderByHasUrl($order = Criteria::ASC) Order by the has_url column
 * @method     ChildSitePageQuery orderByHasMetaDescription($order = Criteria::ASC) Order by the has_meta_description column
 * @method     ChildSitePageQuery orderByHasMetaKeywords($order = Criteria::ASC) Order by the has_meta_keywords column
 * @method     ChildSitePageQuery orderByIsDeletable($order = Criteria::ASC) Order by the is_deletable column
 * @method     ChildSitePageQuery orderByIsEditable($order = Criteria::ASC) Order by the is_editable column
 * @method     ChildSitePageQuery orderByIsVisible($order = Criteria::ASC) Order by the is_visible column
 * @method     ChildSitePageQuery orderByHasCoverImage($order = Criteria::ASC) Order by the has_cover_image column
 * @method     ChildSitePageQuery orderByCoverImageExt($order = Criteria::ASC) Order by the cover_image_ext column
 *
 * @method     ChildSitePageQuery groupById() Group by the id column
 * @method     ChildSitePageQuery groupByOldId() Group by the old_id column
 * @method     ChildSitePageQuery groupByLanguageId() Group by the language_id column
 * @method     ChildSitePageQuery groupByNavigationId() Group by the navigation_id column
 * @method     ChildSitePageQuery groupBySiteId() Group by the site_id column
 * @method     ChildSitePageQuery groupByTag() Group by the tag column
 * @method     ChildSitePageQuery groupByUrl() Group by the url column
 * @method     ChildSitePageQuery groupByHasChangeableUrl() Group by the has_changeable_url column
 * @method     ChildSitePageQuery groupByTitle() Group by the title column
 * @method     ChildSitePageQuery groupByTitleColor() Group by the title_color column
 * @method     ChildSitePageQuery groupByTitleBackgroundColor() Group by the title_background_color column
 * @method     ChildSitePageQuery groupByMetaDescription() Group by the meta_description column
 * @method     ChildSitePageQuery groupByMetaKeywords() Group by the meta_keywords column
 * @method     ChildSitePageQuery groupByContent() Group by the content column
 * @method     ChildSitePageQuery groupByAbout() Group by the about column
 * @method     ChildSitePageQuery groupByMinPictures() Group by the min_pictures column
 * @method     ChildSitePageQuery groupByMaxPictures() Group by the max_pictures column
 * @method     ChildSitePageQuery groupByTitlePosX() Group by the title_pos_x column
 * @method     ChildSitePageQuery groupByTitlePosY() Group by the title_pos_y column
 * @method     ChildSitePageQuery groupByHasTitleColorEditor() Group by the has_title_color_editor column
 * @method     ChildSitePageQuery groupByHasTitleBackgroundcolorEditor() Group by the has_title_backgroundcolor_editor column
 * @method     ChildSitePageQuery groupByHasNavigation() Group by the has_navigation column
 * @method     ChildSitePageQuery groupByHasTitle() Group by the has_title column
 * @method     ChildSitePageQuery groupByHasTitlePositioningFields() Group by the has_title_positioning_fields column
 * @method     ChildSitePageQuery groupByHasContent() Group by the has_content column
 * @method     ChildSitePageQuery groupByHasUrl() Group by the has_url column
 * @method     ChildSitePageQuery groupByHasMetaDescription() Group by the has_meta_description column
 * @method     ChildSitePageQuery groupByHasMetaKeywords() Group by the has_meta_keywords column
 * @method     ChildSitePageQuery groupByIsDeletable() Group by the is_deletable column
 * @method     ChildSitePageQuery groupByIsEditable() Group by the is_editable column
 * @method     ChildSitePageQuery groupByIsVisible() Group by the is_visible column
 * @method     ChildSitePageQuery groupByHasCoverImage() Group by the has_cover_image column
 * @method     ChildSitePageQuery groupByCoverImageExt() Group by the cover_image_ext column
 *
 * @method     ChildSitePageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSitePageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSitePageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSitePageQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSitePageQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSitePageQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSitePageQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildSitePageQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildSitePageQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildSitePageQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildSitePageQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildSitePageQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildSitePageQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildSitePageQuery leftJoinSiteNavigaton($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteNavigaton relation
 * @method     ChildSitePageQuery rightJoinSiteNavigaton($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteNavigaton relation
 * @method     ChildSitePageQuery innerJoinSiteNavigaton($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteNavigaton relation
 *
 * @method     ChildSitePageQuery joinWithSiteNavigaton($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteNavigaton relation
 *
 * @method     ChildSitePageQuery leftJoinWithSiteNavigaton() Adds a LEFT JOIN clause and with to the query using the SiteNavigaton relation
 * @method     ChildSitePageQuery rightJoinWithSiteNavigaton() Adds a RIGHT JOIN clause and with to the query using the SiteNavigaton relation
 * @method     ChildSitePageQuery innerJoinWithSiteNavigaton() Adds a INNER JOIN clause and with to the query using the SiteNavigaton relation
 *
 * @method     ChildSitePageQuery leftJoinLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Language relation
 * @method     ChildSitePageQuery rightJoinLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Language relation
 * @method     ChildSitePageQuery innerJoinLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the Language relation
 *
 * @method     ChildSitePageQuery joinWithLanguage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Language relation
 *
 * @method     ChildSitePageQuery leftJoinWithLanguage() Adds a LEFT JOIN clause and with to the query using the Language relation
 * @method     ChildSitePageQuery rightJoinWithLanguage() Adds a RIGHT JOIN clause and with to the query using the Language relation
 * @method     ChildSitePageQuery innerJoinWithLanguage() Adds a INNER JOIN clause and with to the query using the Language relation
 *
 * @method     ChildSitePageQuery leftJoinSiteFooterBlockTranslationSitePage($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteFooterBlockTranslationSitePage relation
 * @method     ChildSitePageQuery rightJoinSiteFooterBlockTranslationSitePage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteFooterBlockTranslationSitePage relation
 * @method     ChildSitePageQuery innerJoinSiteFooterBlockTranslationSitePage($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteFooterBlockTranslationSitePage relation
 *
 * @method     ChildSitePageQuery joinWithSiteFooterBlockTranslationSitePage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteFooterBlockTranslationSitePage relation
 *
 * @method     ChildSitePageQuery leftJoinWithSiteFooterBlockTranslationSitePage() Adds a LEFT JOIN clause and with to the query using the SiteFooterBlockTranslationSitePage relation
 * @method     ChildSitePageQuery rightJoinWithSiteFooterBlockTranslationSitePage() Adds a RIGHT JOIN clause and with to the query using the SiteFooterBlockTranslationSitePage relation
 * @method     ChildSitePageQuery innerJoinWithSiteFooterBlockTranslationSitePage() Adds a INNER JOIN clause and with to the query using the SiteFooterBlockTranslationSitePage relation
 *
 * @method     ChildSitePageQuery leftJoinSitePagePicture($relationAlias = null) Adds a LEFT JOIN clause to the query using the SitePagePicture relation
 * @method     ChildSitePageQuery rightJoinSitePagePicture($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SitePagePicture relation
 * @method     ChildSitePageQuery innerJoinSitePagePicture($relationAlias = null) Adds a INNER JOIN clause to the query using the SitePagePicture relation
 *
 * @method     ChildSitePageQuery joinWithSitePagePicture($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SitePagePicture relation
 *
 * @method     ChildSitePageQuery leftJoinWithSitePagePicture() Adds a LEFT JOIN clause and with to the query using the SitePagePicture relation
 * @method     ChildSitePageQuery rightJoinWithSitePagePicture() Adds a RIGHT JOIN clause and with to the query using the SitePagePicture relation
 * @method     ChildSitePageQuery innerJoinWithSitePagePicture() Adds a INNER JOIN clause and with to the query using the SitePagePicture relation
 *
 * @method     \Model\Cms\SiteQuery|\Model\Cms\Site_navigationQuery|\Model\Setting\MasterTable\LanguageQuery|\Model\Cms\SiteFooterBlockTranslationSitePageQuery|\Model\Cms\SitePagePictureQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSitePage findOne(ConnectionInterface $con = null) Return the first ChildSitePage matching the query
 * @method     ChildSitePage findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSitePage matching the query, or a new ChildSitePage object populated from the query conditions when no match is found
 *
 * @method     ChildSitePage findOneById(int $id) Return the first ChildSitePage filtered by the id column
 * @method     ChildSitePage findOneByOldId(string $old_id) Return the first ChildSitePage filtered by the old_id column
 * @method     ChildSitePage findOneByLanguageId(int $language_id) Return the first ChildSitePage filtered by the language_id column
 * @method     ChildSitePage findOneByNavigationId(int $navigation_id) Return the first ChildSitePage filtered by the navigation_id column
 * @method     ChildSitePage findOneBySiteId(int $site_id) Return the first ChildSitePage filtered by the site_id column
 * @method     ChildSitePage findOneByTag(string $tag) Return the first ChildSitePage filtered by the tag column
 * @method     ChildSitePage findOneByUrl(string $url) Return the first ChildSitePage filtered by the url column
 * @method     ChildSitePage findOneByHasChangeableUrl(boolean $has_changeable_url) Return the first ChildSitePage filtered by the has_changeable_url column
 * @method     ChildSitePage findOneByTitle(string $title) Return the first ChildSitePage filtered by the title column
 * @method     ChildSitePage findOneByTitleColor(string $title_color) Return the first ChildSitePage filtered by the title_color column
 * @method     ChildSitePage findOneByTitleBackgroundColor(string $title_background_color) Return the first ChildSitePage filtered by the title_background_color column
 * @method     ChildSitePage findOneByMetaDescription(string $meta_description) Return the first ChildSitePage filtered by the meta_description column
 * @method     ChildSitePage findOneByMetaKeywords(string $meta_keywords) Return the first ChildSitePage filtered by the meta_keywords column
 * @method     ChildSitePage findOneByContent(string $content) Return the first ChildSitePage filtered by the content column
 * @method     ChildSitePage findOneByAbout(string $about) Return the first ChildSitePage filtered by the about column
 * @method     ChildSitePage findOneByMinPictures(int $min_pictures) Return the first ChildSitePage filtered by the min_pictures column
 * @method     ChildSitePage findOneByMaxPictures(int $max_pictures) Return the first ChildSitePage filtered by the max_pictures column
 * @method     ChildSitePage findOneByTitlePosX(int $title_pos_x) Return the first ChildSitePage filtered by the title_pos_x column
 * @method     ChildSitePage findOneByTitlePosY(int $title_pos_y) Return the first ChildSitePage filtered by the title_pos_y column
 * @method     ChildSitePage findOneByHasTitleColorEditor(boolean $has_title_color_editor) Return the first ChildSitePage filtered by the has_title_color_editor column
 * @method     ChildSitePage findOneByHasTitleBackgroundcolorEditor(boolean $has_title_backgroundcolor_editor) Return the first ChildSitePage filtered by the has_title_backgroundcolor_editor column
 * @method     ChildSitePage findOneByHasNavigation(boolean $has_navigation) Return the first ChildSitePage filtered by the has_navigation column
 * @method     ChildSitePage findOneByHasTitle(boolean $has_title) Return the first ChildSitePage filtered by the has_title column
 * @method     ChildSitePage findOneByHasTitlePositioningFields(boolean $has_title_positioning_fields) Return the first ChildSitePage filtered by the has_title_positioning_fields column
 * @method     ChildSitePage findOneByHasContent(boolean $has_content) Return the first ChildSitePage filtered by the has_content column
 * @method     ChildSitePage findOneByHasUrl(boolean $has_url) Return the first ChildSitePage filtered by the has_url column
 * @method     ChildSitePage findOneByHasMetaDescription(boolean $has_meta_description) Return the first ChildSitePage filtered by the has_meta_description column
 * @method     ChildSitePage findOneByHasMetaKeywords(boolean $has_meta_keywords) Return the first ChildSitePage filtered by the has_meta_keywords column
 * @method     ChildSitePage findOneByIsDeletable(boolean $is_deletable) Return the first ChildSitePage filtered by the is_deletable column
 * @method     ChildSitePage findOneByIsEditable(boolean $is_editable) Return the first ChildSitePage filtered by the is_editable column
 * @method     ChildSitePage findOneByIsVisible(boolean $is_visible) Return the first ChildSitePage filtered by the is_visible column
 * @method     ChildSitePage findOneByHasCoverImage(boolean $has_cover_image) Return the first ChildSitePage filtered by the has_cover_image column
 * @method     ChildSitePage findOneByCoverImageExt(string $cover_image_ext) Return the first ChildSitePage filtered by the cover_image_ext column *

 * @method     ChildSitePage requirePk($key, ConnectionInterface $con = null) Return the ChildSitePage by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOne(ConnectionInterface $con = null) Return the first ChildSitePage matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSitePage requireOneById(int $id) Return the first ChildSitePage filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByOldId(string $old_id) Return the first ChildSitePage filtered by the old_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByLanguageId(int $language_id) Return the first ChildSitePage filtered by the language_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByNavigationId(int $navigation_id) Return the first ChildSitePage filtered by the navigation_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneBySiteId(int $site_id) Return the first ChildSitePage filtered by the site_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByTag(string $tag) Return the first ChildSitePage filtered by the tag column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByUrl(string $url) Return the first ChildSitePage filtered by the url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByHasChangeableUrl(boolean $has_changeable_url) Return the first ChildSitePage filtered by the has_changeable_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByTitle(string $title) Return the first ChildSitePage filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByTitleColor(string $title_color) Return the first ChildSitePage filtered by the title_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByTitleBackgroundColor(string $title_background_color) Return the first ChildSitePage filtered by the title_background_color column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByMetaDescription(string $meta_description) Return the first ChildSitePage filtered by the meta_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByMetaKeywords(string $meta_keywords) Return the first ChildSitePage filtered by the meta_keywords column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByContent(string $content) Return the first ChildSitePage filtered by the content column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByAbout(string $about) Return the first ChildSitePage filtered by the about column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByMinPictures(int $min_pictures) Return the first ChildSitePage filtered by the min_pictures column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByMaxPictures(int $max_pictures) Return the first ChildSitePage filtered by the max_pictures column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByTitlePosX(int $title_pos_x) Return the first ChildSitePage filtered by the title_pos_x column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByTitlePosY(int $title_pos_y) Return the first ChildSitePage filtered by the title_pos_y column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByHasTitleColorEditor(boolean $has_title_color_editor) Return the first ChildSitePage filtered by the has_title_color_editor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByHasTitleBackgroundcolorEditor(boolean $has_title_backgroundcolor_editor) Return the first ChildSitePage filtered by the has_title_backgroundcolor_editor column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByHasNavigation(boolean $has_navigation) Return the first ChildSitePage filtered by the has_navigation column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByHasTitle(boolean $has_title) Return the first ChildSitePage filtered by the has_title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByHasTitlePositioningFields(boolean $has_title_positioning_fields) Return the first ChildSitePage filtered by the has_title_positioning_fields column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByHasContent(boolean $has_content) Return the first ChildSitePage filtered by the has_content column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByHasUrl(boolean $has_url) Return the first ChildSitePage filtered by the has_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByHasMetaDescription(boolean $has_meta_description) Return the first ChildSitePage filtered by the has_meta_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByHasMetaKeywords(boolean $has_meta_keywords) Return the first ChildSitePage filtered by the has_meta_keywords column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByIsDeletable(boolean $is_deletable) Return the first ChildSitePage filtered by the is_deletable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByIsEditable(boolean $is_editable) Return the first ChildSitePage filtered by the is_editable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByIsVisible(boolean $is_visible) Return the first ChildSitePage filtered by the is_visible column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByHasCoverImage(boolean $has_cover_image) Return the first ChildSitePage filtered by the has_cover_image column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSitePage requireOneByCoverImageExt(string $cover_image_ext) Return the first ChildSitePage filtered by the cover_image_ext column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSitePage[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSitePage objects based on current ModelCriteria
 * @method     ChildSitePage[]|ObjectCollection findById(int $id) Return ChildSitePage objects filtered by the id column
 * @method     ChildSitePage[]|ObjectCollection findByOldId(string $old_id) Return ChildSitePage objects filtered by the old_id column
 * @method     ChildSitePage[]|ObjectCollection findByLanguageId(int $language_id) Return ChildSitePage objects filtered by the language_id column
 * @method     ChildSitePage[]|ObjectCollection findByNavigationId(int $navigation_id) Return ChildSitePage objects filtered by the navigation_id column
 * @method     ChildSitePage[]|ObjectCollection findBySiteId(int $site_id) Return ChildSitePage objects filtered by the site_id column
 * @method     ChildSitePage[]|ObjectCollection findByTag(string $tag) Return ChildSitePage objects filtered by the tag column
 * @method     ChildSitePage[]|ObjectCollection findByUrl(string $url) Return ChildSitePage objects filtered by the url column
 * @method     ChildSitePage[]|ObjectCollection findByHasChangeableUrl(boolean $has_changeable_url) Return ChildSitePage objects filtered by the has_changeable_url column
 * @method     ChildSitePage[]|ObjectCollection findByTitle(string $title) Return ChildSitePage objects filtered by the title column
 * @method     ChildSitePage[]|ObjectCollection findByTitleColor(string $title_color) Return ChildSitePage objects filtered by the title_color column
 * @method     ChildSitePage[]|ObjectCollection findByTitleBackgroundColor(string $title_background_color) Return ChildSitePage objects filtered by the title_background_color column
 * @method     ChildSitePage[]|ObjectCollection findByMetaDescription(string $meta_description) Return ChildSitePage objects filtered by the meta_description column
 * @method     ChildSitePage[]|ObjectCollection findByMetaKeywords(string $meta_keywords) Return ChildSitePage objects filtered by the meta_keywords column
 * @method     ChildSitePage[]|ObjectCollection findByContent(string $content) Return ChildSitePage objects filtered by the content column
 * @method     ChildSitePage[]|ObjectCollection findByAbout(string $about) Return ChildSitePage objects filtered by the about column
 * @method     ChildSitePage[]|ObjectCollection findByMinPictures(int $min_pictures) Return ChildSitePage objects filtered by the min_pictures column
 * @method     ChildSitePage[]|ObjectCollection findByMaxPictures(int $max_pictures) Return ChildSitePage objects filtered by the max_pictures column
 * @method     ChildSitePage[]|ObjectCollection findByTitlePosX(int $title_pos_x) Return ChildSitePage objects filtered by the title_pos_x column
 * @method     ChildSitePage[]|ObjectCollection findByTitlePosY(int $title_pos_y) Return ChildSitePage objects filtered by the title_pos_y column
 * @method     ChildSitePage[]|ObjectCollection findByHasTitleColorEditor(boolean $has_title_color_editor) Return ChildSitePage objects filtered by the has_title_color_editor column
 * @method     ChildSitePage[]|ObjectCollection findByHasTitleBackgroundcolorEditor(boolean $has_title_backgroundcolor_editor) Return ChildSitePage objects filtered by the has_title_backgroundcolor_editor column
 * @method     ChildSitePage[]|ObjectCollection findByHasNavigation(boolean $has_navigation) Return ChildSitePage objects filtered by the has_navigation column
 * @method     ChildSitePage[]|ObjectCollection findByHasTitle(boolean $has_title) Return ChildSitePage objects filtered by the has_title column
 * @method     ChildSitePage[]|ObjectCollection findByHasTitlePositioningFields(boolean $has_title_positioning_fields) Return ChildSitePage objects filtered by the has_title_positioning_fields column
 * @method     ChildSitePage[]|ObjectCollection findByHasContent(boolean $has_content) Return ChildSitePage objects filtered by the has_content column
 * @method     ChildSitePage[]|ObjectCollection findByHasUrl(boolean $has_url) Return ChildSitePage objects filtered by the has_url column
 * @method     ChildSitePage[]|ObjectCollection findByHasMetaDescription(boolean $has_meta_description) Return ChildSitePage objects filtered by the has_meta_description column
 * @method     ChildSitePage[]|ObjectCollection findByHasMetaKeywords(boolean $has_meta_keywords) Return ChildSitePage objects filtered by the has_meta_keywords column
 * @method     ChildSitePage[]|ObjectCollection findByIsDeletable(boolean $is_deletable) Return ChildSitePage objects filtered by the is_deletable column
 * @method     ChildSitePage[]|ObjectCollection findByIsEditable(boolean $is_editable) Return ChildSitePage objects filtered by the is_editable column
 * @method     ChildSitePage[]|ObjectCollection findByIsVisible(boolean $is_visible) Return ChildSitePage objects filtered by the is_visible column
 * @method     ChildSitePage[]|ObjectCollection findByHasCoverImage(boolean $has_cover_image) Return ChildSitePage objects filtered by the has_cover_image column
 * @method     ChildSitePage[]|ObjectCollection findByCoverImageExt(string $cover_image_ext) Return ChildSitePage objects filtered by the cover_image_ext column
 * @method     ChildSitePage[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SitePageQuery extends ModelCriteria
{

    // query_cache behavior
    protected $queryKey = '';
protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Cms\Base\SitePageQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Cms\\SitePage', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSitePageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSitePageQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSitePageQuery) {
            return $criteria;
        }
        $query = new ChildSitePageQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSitePage|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SitePageTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SitePageTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSitePage A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, old_id, language_id, navigation_id, site_id, tag, url, has_changeable_url, title, title_color, title_background_color, meta_description, meta_keywords, content, about, min_pictures, max_pictures, title_pos_x, title_pos_y, has_title_color_editor, has_title_backgroundcolor_editor, has_navigation, has_title, has_title_positioning_fields, has_content, has_url, has_meta_description, has_meta_keywords, is_deletable, is_editable, is_visible, has_cover_image, cover_image_ext FROM site_page WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSitePage $obj */
            $obj = new ChildSitePage();
            $obj->hydrate($row);
            SitePageTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSitePage|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SitePageTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SitePageTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SitePageTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SitePageTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the old_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldId('fooValue');   // WHERE old_id = 'fooValue'
     * $query->filterByOldId('%fooValue%', Criteria::LIKE); // WHERE old_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $oldId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByOldId($oldId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($oldId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_OLD_ID, $oldId, $comparison);
    }

    /**
     * Filter the query on the language_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguageId(1234); // WHERE language_id = 1234
     * $query->filterByLanguageId(array(12, 34)); // WHERE language_id IN (12, 34)
     * $query->filterByLanguageId(array('min' => 12)); // WHERE language_id > 12
     * </code>
     *
     * @see       filterByLanguage()
     *
     * @param     mixed $languageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByLanguageId($languageId = null, $comparison = null)
    {
        if (is_array($languageId)) {
            $useMinMax = false;
            if (isset($languageId['min'])) {
                $this->addUsingAlias(SitePageTableMap::COL_LANGUAGE_ID, $languageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($languageId['max'])) {
                $this->addUsingAlias(SitePageTableMap::COL_LANGUAGE_ID, $languageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_LANGUAGE_ID, $languageId, $comparison);
    }

    /**
     * Filter the query on the navigation_id column
     *
     * Example usage:
     * <code>
     * $query->filterByNavigationId(1234); // WHERE navigation_id = 1234
     * $query->filterByNavigationId(array(12, 34)); // WHERE navigation_id IN (12, 34)
     * $query->filterByNavigationId(array('min' => 12)); // WHERE navigation_id > 12
     * </code>
     *
     * @see       filterBySiteNavigaton()
     *
     * @param     mixed $navigationId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByNavigationId($navigationId = null, $comparison = null)
    {
        if (is_array($navigationId)) {
            $useMinMax = false;
            if (isset($navigationId['min'])) {
                $this->addUsingAlias(SitePageTableMap::COL_NAVIGATION_ID, $navigationId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($navigationId['max'])) {
                $this->addUsingAlias(SitePageTableMap::COL_NAVIGATION_ID, $navigationId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_NAVIGATION_ID, $navigationId, $comparison);
    }

    /**
     * Filter the query on the site_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE site_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE site_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE site_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(SitePageTableMap::COL_SITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(SitePageTableMap::COL_SITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_SITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the tag column
     *
     * Example usage:
     * <code>
     * $query->filterByTag('fooValue');   // WHERE tag = 'fooValue'
     * $query->filterByTag('%fooValue%', Criteria::LIKE); // WHERE tag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tag The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByTag($tag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tag)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_TAG, $tag, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%', Criteria::LIKE); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_URL, $url, $comparison);
    }

    /**
     * Filter the query on the has_changeable_url column
     *
     * Example usage:
     * <code>
     * $query->filterByHasChangeableUrl(true); // WHERE has_changeable_url = true
     * $query->filterByHasChangeableUrl('yes'); // WHERE has_changeable_url = true
     * </code>
     *
     * @param     boolean|string $hasChangeableUrl The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByHasChangeableUrl($hasChangeableUrl = null, $comparison = null)
    {
        if (is_string($hasChangeableUrl)) {
            $hasChangeableUrl = in_array(strtolower($hasChangeableUrl), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_HAS_CHANGEABLE_URL, $hasChangeableUrl, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the title_color column
     *
     * Example usage:
     * <code>
     * $query->filterByTitleColor('fooValue');   // WHERE title_color = 'fooValue'
     * $query->filterByTitleColor('%fooValue%', Criteria::LIKE); // WHERE title_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $titleColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByTitleColor($titleColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($titleColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_TITLE_COLOR, $titleColor, $comparison);
    }

    /**
     * Filter the query on the title_background_color column
     *
     * Example usage:
     * <code>
     * $query->filterByTitleBackgroundColor('fooValue');   // WHERE title_background_color = 'fooValue'
     * $query->filterByTitleBackgroundColor('%fooValue%', Criteria::LIKE); // WHERE title_background_color LIKE '%fooValue%'
     * </code>
     *
     * @param     string $titleBackgroundColor The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByTitleBackgroundColor($titleBackgroundColor = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($titleBackgroundColor)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_TITLE_BACKGROUND_COLOR, $titleBackgroundColor, $comparison);
    }

    /**
     * Filter the query on the meta_description column
     *
     * Example usage:
     * <code>
     * $query->filterByMetaDescription('fooValue');   // WHERE meta_description = 'fooValue'
     * $query->filterByMetaDescription('%fooValue%', Criteria::LIKE); // WHERE meta_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metaDescription The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByMetaDescription($metaDescription = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metaDescription)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_META_DESCRIPTION, $metaDescription, $comparison);
    }

    /**
     * Filter the query on the meta_keywords column
     *
     * Example usage:
     * <code>
     * $query->filterByMetaKeywords('fooValue');   // WHERE meta_keywords = 'fooValue'
     * $query->filterByMetaKeywords('%fooValue%', Criteria::LIKE); // WHERE meta_keywords LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metaKeywords The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByMetaKeywords($metaKeywords = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metaKeywords)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_META_KEYWORDS, $metaKeywords, $comparison);
    }

    /**
     * Filter the query on the content column
     *
     * Example usage:
     * <code>
     * $query->filterByContent('fooValue');   // WHERE content = 'fooValue'
     * $query->filterByContent('%fooValue%', Criteria::LIKE); // WHERE content LIKE '%fooValue%'
     * </code>
     *
     * @param     string $content The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByContent($content = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($content)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_CONTENT, $content, $comparison);
    }

    /**
     * Filter the query on the about column
     *
     * Example usage:
     * <code>
     * $query->filterByAbout('fooValue');   // WHERE about = 'fooValue'
     * $query->filterByAbout('%fooValue%', Criteria::LIKE); // WHERE about LIKE '%fooValue%'
     * </code>
     *
     * @param     string $about The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByAbout($about = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($about)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_ABOUT, $about, $comparison);
    }

    /**
     * Filter the query on the min_pictures column
     *
     * Example usage:
     * <code>
     * $query->filterByMinPictures(1234); // WHERE min_pictures = 1234
     * $query->filterByMinPictures(array(12, 34)); // WHERE min_pictures IN (12, 34)
     * $query->filterByMinPictures(array('min' => 12)); // WHERE min_pictures > 12
     * </code>
     *
     * @param     mixed $minPictures The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByMinPictures($minPictures = null, $comparison = null)
    {
        if (is_array($minPictures)) {
            $useMinMax = false;
            if (isset($minPictures['min'])) {
                $this->addUsingAlias(SitePageTableMap::COL_MIN_PICTURES, $minPictures['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($minPictures['max'])) {
                $this->addUsingAlias(SitePageTableMap::COL_MIN_PICTURES, $minPictures['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_MIN_PICTURES, $minPictures, $comparison);
    }

    /**
     * Filter the query on the max_pictures column
     *
     * Example usage:
     * <code>
     * $query->filterByMaxPictures(1234); // WHERE max_pictures = 1234
     * $query->filterByMaxPictures(array(12, 34)); // WHERE max_pictures IN (12, 34)
     * $query->filterByMaxPictures(array('min' => 12)); // WHERE max_pictures > 12
     * </code>
     *
     * @param     mixed $maxPictures The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByMaxPictures($maxPictures = null, $comparison = null)
    {
        if (is_array($maxPictures)) {
            $useMinMax = false;
            if (isset($maxPictures['min'])) {
                $this->addUsingAlias(SitePageTableMap::COL_MAX_PICTURES, $maxPictures['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($maxPictures['max'])) {
                $this->addUsingAlias(SitePageTableMap::COL_MAX_PICTURES, $maxPictures['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_MAX_PICTURES, $maxPictures, $comparison);
    }

    /**
     * Filter the query on the title_pos_x column
     *
     * Example usage:
     * <code>
     * $query->filterByTitlePosX(1234); // WHERE title_pos_x = 1234
     * $query->filterByTitlePosX(array(12, 34)); // WHERE title_pos_x IN (12, 34)
     * $query->filterByTitlePosX(array('min' => 12)); // WHERE title_pos_x > 12
     * </code>
     *
     * @param     mixed $titlePosX The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByTitlePosX($titlePosX = null, $comparison = null)
    {
        if (is_array($titlePosX)) {
            $useMinMax = false;
            if (isset($titlePosX['min'])) {
                $this->addUsingAlias(SitePageTableMap::COL_TITLE_POS_X, $titlePosX['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($titlePosX['max'])) {
                $this->addUsingAlias(SitePageTableMap::COL_TITLE_POS_X, $titlePosX['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_TITLE_POS_X, $titlePosX, $comparison);
    }

    /**
     * Filter the query on the title_pos_y column
     *
     * Example usage:
     * <code>
     * $query->filterByTitlePosY(1234); // WHERE title_pos_y = 1234
     * $query->filterByTitlePosY(array(12, 34)); // WHERE title_pos_y IN (12, 34)
     * $query->filterByTitlePosY(array('min' => 12)); // WHERE title_pos_y > 12
     * </code>
     *
     * @param     mixed $titlePosY The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByTitlePosY($titlePosY = null, $comparison = null)
    {
        if (is_array($titlePosY)) {
            $useMinMax = false;
            if (isset($titlePosY['min'])) {
                $this->addUsingAlias(SitePageTableMap::COL_TITLE_POS_Y, $titlePosY['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($titlePosY['max'])) {
                $this->addUsingAlias(SitePageTableMap::COL_TITLE_POS_Y, $titlePosY['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_TITLE_POS_Y, $titlePosY, $comparison);
    }

    /**
     * Filter the query on the has_title_color_editor column
     *
     * Example usage:
     * <code>
     * $query->filterByHasTitleColorEditor(true); // WHERE has_title_color_editor = true
     * $query->filterByHasTitleColorEditor('yes'); // WHERE has_title_color_editor = true
     * </code>
     *
     * @param     boolean|string $hasTitleColorEditor The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByHasTitleColorEditor($hasTitleColorEditor = null, $comparison = null)
    {
        if (is_string($hasTitleColorEditor)) {
            $hasTitleColorEditor = in_array(strtolower($hasTitleColorEditor), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_HAS_TITLE_COLOR_EDITOR, $hasTitleColorEditor, $comparison);
    }

    /**
     * Filter the query on the has_title_backgroundcolor_editor column
     *
     * Example usage:
     * <code>
     * $query->filterByHasTitleBackgroundcolorEditor(true); // WHERE has_title_backgroundcolor_editor = true
     * $query->filterByHasTitleBackgroundcolorEditor('yes'); // WHERE has_title_backgroundcolor_editor = true
     * </code>
     *
     * @param     boolean|string $hasTitleBackgroundcolorEditor The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByHasTitleBackgroundcolorEditor($hasTitleBackgroundcolorEditor = null, $comparison = null)
    {
        if (is_string($hasTitleBackgroundcolorEditor)) {
            $hasTitleBackgroundcolorEditor = in_array(strtolower($hasTitleBackgroundcolorEditor), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_HAS_TITLE_BACKGROUNDCOLOR_EDITOR, $hasTitleBackgroundcolorEditor, $comparison);
    }

    /**
     * Filter the query on the has_navigation column
     *
     * Example usage:
     * <code>
     * $query->filterByHasNavigation(true); // WHERE has_navigation = true
     * $query->filterByHasNavigation('yes'); // WHERE has_navigation = true
     * </code>
     *
     * @param     boolean|string $hasNavigation The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByHasNavigation($hasNavigation = null, $comparison = null)
    {
        if (is_string($hasNavigation)) {
            $hasNavigation = in_array(strtolower($hasNavigation), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_HAS_NAVIGATION, $hasNavigation, $comparison);
    }

    /**
     * Filter the query on the has_title column
     *
     * Example usage:
     * <code>
     * $query->filterByHasTitle(true); // WHERE has_title = true
     * $query->filterByHasTitle('yes'); // WHERE has_title = true
     * </code>
     *
     * @param     boolean|string $hasTitle The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByHasTitle($hasTitle = null, $comparison = null)
    {
        if (is_string($hasTitle)) {
            $hasTitle = in_array(strtolower($hasTitle), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_HAS_TITLE, $hasTitle, $comparison);
    }

    /**
     * Filter the query on the has_title_positioning_fields column
     *
     * Example usage:
     * <code>
     * $query->filterByHasTitlePositioningFields(true); // WHERE has_title_positioning_fields = true
     * $query->filterByHasTitlePositioningFields('yes'); // WHERE has_title_positioning_fields = true
     * </code>
     *
     * @param     boolean|string $hasTitlePositioningFields The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByHasTitlePositioningFields($hasTitlePositioningFields = null, $comparison = null)
    {
        if (is_string($hasTitlePositioningFields)) {
            $hasTitlePositioningFields = in_array(strtolower($hasTitlePositioningFields), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_HAS_TITLE_POSITIONING_FIELDS, $hasTitlePositioningFields, $comparison);
    }

    /**
     * Filter the query on the has_content column
     *
     * Example usage:
     * <code>
     * $query->filterByHasContent(true); // WHERE has_content = true
     * $query->filterByHasContent('yes'); // WHERE has_content = true
     * </code>
     *
     * @param     boolean|string $hasContent The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByHasContent($hasContent = null, $comparison = null)
    {
        if (is_string($hasContent)) {
            $hasContent = in_array(strtolower($hasContent), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_HAS_CONTENT, $hasContent, $comparison);
    }

    /**
     * Filter the query on the has_url column
     *
     * Example usage:
     * <code>
     * $query->filterByHasUrl(true); // WHERE has_url = true
     * $query->filterByHasUrl('yes'); // WHERE has_url = true
     * </code>
     *
     * @param     boolean|string $hasUrl The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByHasUrl($hasUrl = null, $comparison = null)
    {
        if (is_string($hasUrl)) {
            $hasUrl = in_array(strtolower($hasUrl), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_HAS_URL, $hasUrl, $comparison);
    }

    /**
     * Filter the query on the has_meta_description column
     *
     * Example usage:
     * <code>
     * $query->filterByHasMetaDescription(true); // WHERE has_meta_description = true
     * $query->filterByHasMetaDescription('yes'); // WHERE has_meta_description = true
     * </code>
     *
     * @param     boolean|string $hasMetaDescription The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByHasMetaDescription($hasMetaDescription = null, $comparison = null)
    {
        if (is_string($hasMetaDescription)) {
            $hasMetaDescription = in_array(strtolower($hasMetaDescription), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_HAS_META_DESCRIPTION, $hasMetaDescription, $comparison);
    }

    /**
     * Filter the query on the has_meta_keywords column
     *
     * Example usage:
     * <code>
     * $query->filterByHasMetaKeywords(true); // WHERE has_meta_keywords = true
     * $query->filterByHasMetaKeywords('yes'); // WHERE has_meta_keywords = true
     * </code>
     *
     * @param     boolean|string $hasMetaKeywords The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByHasMetaKeywords($hasMetaKeywords = null, $comparison = null)
    {
        if (is_string($hasMetaKeywords)) {
            $hasMetaKeywords = in_array(strtolower($hasMetaKeywords), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_HAS_META_KEYWORDS, $hasMetaKeywords, $comparison);
    }

    /**
     * Filter the query on the is_deletable column
     *
     * Example usage:
     * <code>
     * $query->filterByIsDeletable(true); // WHERE is_deletable = true
     * $query->filterByIsDeletable('yes'); // WHERE is_deletable = true
     * </code>
     *
     * @param     boolean|string $isDeletable The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByIsDeletable($isDeletable = null, $comparison = null)
    {
        if (is_string($isDeletable)) {
            $isDeletable = in_array(strtolower($isDeletable), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_IS_DELETABLE, $isDeletable, $comparison);
    }

    /**
     * Filter the query on the is_editable column
     *
     * Example usage:
     * <code>
     * $query->filterByIsEditable(true); // WHERE is_editable = true
     * $query->filterByIsEditable('yes'); // WHERE is_editable = true
     * </code>
     *
     * @param     boolean|string $isEditable The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByIsEditable($isEditable = null, $comparison = null)
    {
        if (is_string($isEditable)) {
            $isEditable = in_array(strtolower($isEditable), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_IS_EDITABLE, $isEditable, $comparison);
    }

    /**
     * Filter the query on the is_visible column
     *
     * Example usage:
     * <code>
     * $query->filterByIsVisible(true); // WHERE is_visible = true
     * $query->filterByIsVisible('yes'); // WHERE is_visible = true
     * </code>
     *
     * @param     boolean|string $isVisible The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByIsVisible($isVisible = null, $comparison = null)
    {
        if (is_string($isVisible)) {
            $isVisible = in_array(strtolower($isVisible), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_IS_VISIBLE, $isVisible, $comparison);
    }

    /**
     * Filter the query on the has_cover_image column
     *
     * Example usage:
     * <code>
     * $query->filterByHasCoverImage(true); // WHERE has_cover_image = true
     * $query->filterByHasCoverImage('yes'); // WHERE has_cover_image = true
     * </code>
     *
     * @param     boolean|string $hasCoverImage The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByHasCoverImage($hasCoverImage = null, $comparison = null)
    {
        if (is_string($hasCoverImage)) {
            $hasCoverImage = in_array(strtolower($hasCoverImage), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SitePageTableMap::COL_HAS_COVER_IMAGE, $hasCoverImage, $comparison);
    }

    /**
     * Filter the query on the cover_image_ext column
     *
     * Example usage:
     * <code>
     * $query->filterByCoverImageExt('fooValue');   // WHERE cover_image_ext = 'fooValue'
     * $query->filterByCoverImageExt('%fooValue%', Criteria::LIKE); // WHERE cover_image_ext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $coverImageExt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByCoverImageExt($coverImageExt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($coverImageExt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SitePageTableMap::COL_COVER_IMAGE_EXT, $coverImageExt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Cms\Site object
     *
     * @param \Model\Cms\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSitePageQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \Model\Cms\Site) {
            return $this
                ->addUsingAlias(SitePageTableMap::COL_SITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SitePageTableMap::COL_SITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \Model\Cms\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\Model\Cms\SiteQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\Site_navigation object
     *
     * @param \Model\Cms\Site_navigation|ObjectCollection $site_navigation The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSitePageQuery The current query, for fluid interface
     */
    public function filterBySiteNavigaton($site_navigation, $comparison = null)
    {
        if ($site_navigation instanceof \Model\Cms\Site_navigation) {
            return $this
                ->addUsingAlias(SitePageTableMap::COL_NAVIGATION_ID, $site_navigation->getId(), $comparison);
        } elseif ($site_navigation instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SitePageTableMap::COL_NAVIGATION_ID, $site_navigation->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySiteNavigaton() only accepts arguments of type \Model\Cms\Site_navigation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteNavigaton relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function joinSiteNavigaton($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteNavigaton');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteNavigaton');
        }

        return $this;
    }

    /**
     * Use the SiteNavigaton relation Site_navigation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\Site_navigationQuery A secondary query class using the current class as primary query
     */
    public function useSiteNavigatonQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSiteNavigaton($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteNavigaton', '\Model\Cms\Site_navigationQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Language object
     *
     * @param \Model\Setting\MasterTable\Language|ObjectCollection $language The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSitePageQuery The current query, for fluid interface
     */
    public function filterByLanguage($language, $comparison = null)
    {
        if ($language instanceof \Model\Setting\MasterTable\Language) {
            return $this
                ->addUsingAlias(SitePageTableMap::COL_LANGUAGE_ID, $language->getId(), $comparison);
        } elseif ($language instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SitePageTableMap::COL_LANGUAGE_ID, $language->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLanguage() only accepts arguments of type \Model\Setting\MasterTable\Language or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Language relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function joinLanguage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Language');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Language');
        }

        return $this;
    }

    /**
     * Use the Language relation Language object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\LanguageQuery A secondary query class using the current class as primary query
     */
    public function useLanguageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLanguage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Language', '\Model\Setting\MasterTable\LanguageQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteFooterBlockTranslationSitePage object
     *
     * @param \Model\Cms\SiteFooterBlockTranslationSitePage|ObjectCollection $siteFooterBlockTranslationSitePage the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSitePageQuery The current query, for fluid interface
     */
    public function filterBySiteFooterBlockTranslationSitePage($siteFooterBlockTranslationSitePage, $comparison = null)
    {
        if ($siteFooterBlockTranslationSitePage instanceof \Model\Cms\SiteFooterBlockTranslationSitePage) {
            return $this
                ->addUsingAlias(SitePageTableMap::COL_ID, $siteFooterBlockTranslationSitePage->getSitePageId(), $comparison);
        } elseif ($siteFooterBlockTranslationSitePage instanceof ObjectCollection) {
            return $this
                ->useSiteFooterBlockTranslationSitePageQuery()
                ->filterByPrimaryKeys($siteFooterBlockTranslationSitePage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteFooterBlockTranslationSitePage() only accepts arguments of type \Model\Cms\SiteFooterBlockTranslationSitePage or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteFooterBlockTranslationSitePage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function joinSiteFooterBlockTranslationSitePage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteFooterBlockTranslationSitePage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteFooterBlockTranslationSitePage');
        }

        return $this;
    }

    /**
     * Use the SiteFooterBlockTranslationSitePage relation SiteFooterBlockTranslationSitePage object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteFooterBlockTranslationSitePageQuery A secondary query class using the current class as primary query
     */
    public function useSiteFooterBlockTranslationSitePageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteFooterBlockTranslationSitePage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteFooterBlockTranslationSitePage', '\Model\Cms\SiteFooterBlockTranslationSitePageQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SitePagePicture object
     *
     * @param \Model\Cms\SitePagePicture|ObjectCollection $sitePagePicture the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSitePageQuery The current query, for fluid interface
     */
    public function filterBySitePagePicture($sitePagePicture, $comparison = null)
    {
        if ($sitePagePicture instanceof \Model\Cms\SitePagePicture) {
            return $this
                ->addUsingAlias(SitePageTableMap::COL_ID, $sitePagePicture->getSitePageId(), $comparison);
        } elseif ($sitePagePicture instanceof ObjectCollection) {
            return $this
                ->useSitePagePictureQuery()
                ->filterByPrimaryKeys($sitePagePicture->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySitePagePicture() only accepts arguments of type \Model\Cms\SitePagePicture or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SitePagePicture relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function joinSitePagePicture($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SitePagePicture');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SitePagePicture');
        }

        return $this;
    }

    /**
     * Use the SitePagePicture relation SitePagePicture object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SitePagePictureQuery A secondary query class using the current class as primary query
     */
    public function useSitePagePictureQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSitePagePicture($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SitePagePicture', '\Model\Cms\SitePagePictureQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSitePage $sitePage Object to remove from the list of results
     *
     * @return $this|ChildSitePageQuery The current query, for fluid interface
     */
    public function prune($sitePage = null)
    {
        if ($sitePage) {
            $this->addUsingAlias(SitePageTableMap::COL_ID, $sitePage->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the site_page table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SitePageTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SitePageTableMap::clearInstancePool();
            SitePageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SitePageTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SitePageTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SitePageTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SitePageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // query_cache behavior

    public function setQueryKey($key)
    {
        $this->queryKey = $key;

        return $this;
    }

    public function getQueryKey()
    {
        return $this->queryKey;
    }

    public function cacheContains($key)
    {

        return apc_fetch($key);
    }

    public function cacheFetch($key)
    {

        return apc_fetch($key);
    }

    public function cacheStore($key, $value, $lifetime = 3600)
    {
        apc_store($key, $value, $lifetime);
    }

    public function doSelect(ConnectionInterface $con = null)
    {
        // check that the columns of the main class are already added (if this is the primary ModelCriteria)
        if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
            $this->addSelfSelectColumns();
        }
        $this->configureSelectColumns();

        $dbMap = Propel::getServiceContainer()->getDatabaseMap(SitePageTableMap::DATABASE_NAME);
        $db = Propel::getServiceContainer()->getAdapter(SitePageTableMap::DATABASE_NAME);

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            $params = array();
            $sql = $this->createSelectSql($params);
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
            } catch (Exception $e) {
                Propel::log($e->getMessage(), Propel::LOG_ERR);
                throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
            }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

    public function doCount(ConnectionInterface $con = null)
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap($this->getDbName());
        $db = Propel::getServiceContainer()->getAdapter($this->getDbName());

        $key = $this->getQueryKey();
        if ($key && $this->cacheContains($key)) {
            $params = $this->getParams();
            $sql = $this->cacheFetch($key);
        } else {
            // check that the columns of the main class are already added (if this is the primary ModelCriteria)
            if (!$this->hasSelectClause() && !$this->getPrimaryCriteria()) {
                $this->addSelfSelectColumns();
            }

            $this->configureSelectColumns();

            $needsComplexCount = $this->getGroupByColumns()
                || $this->getOffset()
                || $this->getLimit() >= 0
                || $this->getHaving()
                || in_array(Criteria::DISTINCT, $this->getSelectModifiers())
                || count($this->selectQueries) > 0
            ;

            $params = array();
            if ($needsComplexCount) {
                if ($this->needsSelectAliases()) {
                    if ($this->getHaving()) {
                        throw new PropelException('Propel cannot create a COUNT query when using HAVING and  duplicate column names in the SELECT part');
                    }
                    $db->turnSelectColumnsToAliases($this);
                }
                $selectSql = $this->createSelectSql($params);
                $sql = 'SELECT COUNT(*) FROM (' . $selectSql . ') propelmatch4cnt';
            } else {
                // Replace SELECT columns with COUNT(*)
                $this->clearSelectColumns()->addSelectColumn('COUNT(*)');
                $sql = $this->createSelectSql($params);
            }
        }

        try {
            $stmt = $con->prepare($sql);
            $db->bindValues($stmt, $params, $dbMap);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute COUNT statement [%s]', $sql), 0, $e);
        }

        if ($key && !$this->cacheContains($key)) {
                $this->cacheStore($key, $sql);
        }

        return $con->getDataFetcher($stmt);
    }

} // SitePageQuery
