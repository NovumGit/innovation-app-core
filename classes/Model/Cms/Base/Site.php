<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\PromoProduct;
use Model\PromoProductQuery;
use Model\Base\PromoProduct as BasePromoProduct;
use Model\Cms\CustomMedia as ChildCustomMedia;
use Model\Cms\CustomMediaQuery as ChildCustomMediaQuery;
use Model\Cms\Site as ChildSite;
use Model\Cms\SiteBanner as ChildSiteBanner;
use Model\Cms\SiteBannerQuery as ChildSiteBannerQuery;
use Model\Cms\SiteBlog as ChildSiteBlog;
use Model\Cms\SiteBlogQuery as ChildSiteBlogQuery;
use Model\Cms\SiteFAQ as ChildSiteFAQ;
use Model\Cms\SiteFAQCategory as ChildSiteFAQCategory;
use Model\Cms\SiteFAQCategoryQuery as ChildSiteFAQCategoryQuery;
use Model\Cms\SiteFAQQuery as ChildSiteFAQQuery;
use Model\Cms\SiteFooterBlock as ChildSiteFooterBlock;
use Model\Cms\SiteFooterBlockQuery as ChildSiteFooterBlockQuery;
use Model\Cms\SitePage as ChildSitePage;
use Model\Cms\SitePageQuery as ChildSitePageQuery;
use Model\Cms\SiteQuery as ChildSiteQuery;
use Model\Cms\SiteUsp as ChildSiteUsp;
use Model\Cms\SiteUspQuery as ChildSiteUspQuery;
use Model\Cms\Mail\MailTemplate;
use Model\Cms\Mail\MailTemplateQuery;
use Model\Cms\Mail\Base\MailTemplate as BaseMailTemplate;
use Model\Cms\Mail\Map\MailTemplateTableMap;
use Model\Cms\Map\CustomMediaTableMap;
use Model\Cms\Map\SiteBannerTableMap;
use Model\Cms\Map\SiteBlogTableMap;
use Model\Cms\Map\SiteFAQCategoryTableMap;
use Model\Cms\Map\SiteFAQTableMap;
use Model\Cms\Map\SiteFooterBlockTableMap;
use Model\Cms\Map\SitePageTableMap;
use Model\Cms\Map\SiteTableMap;
use Model\Cms\Map\SiteUspTableMap;
use Model\Company\Company;
use Model\Company\CompanyQuery;
use Model\Map\PromoProductTableMap;
use Model\Sale\SaleOrder;
use Model\Sale\SaleOrderQuery;
use Model\Sale\Base\SaleOrder as BaseSaleOrder;
use Model\Sale\Map\SaleOrderTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'site' table.
 *
 *
 *
 * @package    propel.generator.Model.Cms.Base
 */
abstract class Site implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Cms\\Map\\SiteTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the domain field.
     *
     * @var        string|null
     */
    protected $domain;

    /**
     * The value for the custom_folder field.
     *
     * @var        string|null
     */
    protected $custom_folder;

    /**
     * The value for the from_email field.
     *
     * @var        string|null
     */
    protected $from_email;

    /**
     * The value for the contact_form_to_email field.
     *
     * @var        string|null
     */
    protected $contact_form_to_email;

    /**
     * The value for the num_footer_blocks field.
     *
     * @var        int|null
     */
    protected $num_footer_blocks;

    /**
     * The value for the general_phone field.
     *
     * @var        string|null
     */
    protected $general_phone;

    /**
     * The value for the invoicing_company_id field.
     *
     * @var        int|null
     */
    protected $invoicing_company_id;

    /**
     * @var        Company
     */
    protected $aOwnCompany;

    /**
     * @var        ObjectCollection|ChildCustomMedia[] Collection to store aggregation of ChildCustomMedia objects.
     */
    protected $collCustomMedias;
    protected $collCustomMediasPartial;

    /**
     * @var        ObjectCollection|PromoProduct[] Collection to store aggregation of PromoProduct objects.
     */
    protected $collPromoProducts;
    protected $collPromoProductsPartial;

    /**
     * @var        ObjectCollection|SaleOrder[] Collection to store aggregation of SaleOrder objects.
     */
    protected $collSaleOrders;
    protected $collSaleOrdersPartial;

    /**
     * @var        ObjectCollection|ChildSiteUsp[] Collection to store aggregation of ChildSiteUsp objects.
     */
    protected $collSiteUsps;
    protected $collSiteUspsPartial;

    /**
     * @var        ObjectCollection|ChildSiteFooterBlock[] Collection to store aggregation of ChildSiteFooterBlock objects.
     */
    protected $collSiteFooterBlocks;
    protected $collSiteFooterBlocksPartial;

    /**
     * @var        ObjectCollection|ChildSiteBlog[] Collection to store aggregation of ChildSiteBlog objects.
     */
    protected $collSiteBlogs;
    protected $collSiteBlogsPartial;

    /**
     * @var        ObjectCollection|ChildSiteBanner[] Collection to store aggregation of ChildSiteBanner objects.
     */
    protected $collSiteBanners;
    protected $collSiteBannersPartial;

    /**
     * @var        ObjectCollection|ChildSiteFAQCategory[] Collection to store aggregation of ChildSiteFAQCategory objects.
     */
    protected $collSiteFAQCategories;
    protected $collSiteFAQCategoriesPartial;

    /**
     * @var        ObjectCollection|ChildSiteFAQ[] Collection to store aggregation of ChildSiteFAQ objects.
     */
    protected $collSiteFAQs;
    protected $collSiteFAQsPartial;

    /**
     * @var        ObjectCollection|ChildSitePage[] Collection to store aggregation of ChildSitePage objects.
     */
    protected $collSitePages;
    protected $collSitePagesPartial;

    /**
     * @var        ObjectCollection|MailTemplate[] Collection to store aggregation of MailTemplate objects.
     */
    protected $collMailTemplates;
    protected $collMailTemplatesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildCustomMedia[]
     */
    protected $customMediasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|PromoProduct[]
     */
    protected $promoProductsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|SaleOrder[]
     */
    protected $saleOrdersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSiteUsp[]
     */
    protected $siteUspsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSiteFooterBlock[]
     */
    protected $siteFooterBlocksScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSiteBlog[]
     */
    protected $siteBlogsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSiteBanner[]
     */
    protected $siteBannersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSiteFAQCategory[]
     */
    protected $siteFAQCategoriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSiteFAQ[]
     */
    protected $siteFAQsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildSitePage[]
     */
    protected $sitePagesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|MailTemplate[]
     */
    protected $mailTemplatesScheduledForDeletion = null;

    /**
     * Initializes internal state of Model\Cms\Base\Site object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Site</code> instance.  If
     * <code>obj</code> is an instance of <code>Site</code>, delegates to
     * <code>equals(Site)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [domain] column value.
     *
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Get the [custom_folder] column value.
     *
     * @return string|null
     */
    public function getCustomFolder()
    {
        return $this->custom_folder;
    }

    /**
     * Get the [from_email] column value.
     *
     * @return string|null
     */
    public function getFromEmail()
    {
        return $this->from_email;
    }

    /**
     * Get the [contact_form_to_email] column value.
     *
     * @return string|null
     */
    public function getContactFormToEmail()
    {
        return $this->contact_form_to_email;
    }

    /**
     * Get the [num_footer_blocks] column value.
     *
     * @return int|null
     */
    public function getNumFooterBlocks()
    {
        return $this->num_footer_blocks;
    }

    /**
     * Get the [general_phone] column value.
     *
     * @return string|null
     */
    public function getGeneralPhone()
    {
        return $this->general_phone;
    }

    /**
     * Get the [invoicing_company_id] column value.
     *
     * @return int|null
     */
    public function getInvoicingCompanyId()
    {
        return $this->invoicing_company_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[SiteTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [domain] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function setDomain($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->domain !== $v) {
            $this->domain = $v;
            $this->modifiedColumns[SiteTableMap::COL_DOMAIN] = true;
        }

        return $this;
    } // setDomain()

    /**
     * Set the value of [custom_folder] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function setCustomFolder($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->custom_folder !== $v) {
            $this->custom_folder = $v;
            $this->modifiedColumns[SiteTableMap::COL_CUSTOM_FOLDER] = true;
        }

        return $this;
    } // setCustomFolder()

    /**
     * Set the value of [from_email] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function setFromEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->from_email !== $v) {
            $this->from_email = $v;
            $this->modifiedColumns[SiteTableMap::COL_FROM_EMAIL] = true;
        }

        return $this;
    } // setFromEmail()

    /**
     * Set the value of [contact_form_to_email] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function setContactFormToEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->contact_form_to_email !== $v) {
            $this->contact_form_to_email = $v;
            $this->modifiedColumns[SiteTableMap::COL_CONTACT_FORM_TO_EMAIL] = true;
        }

        return $this;
    } // setContactFormToEmail()

    /**
     * Set the value of [num_footer_blocks] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function setNumFooterBlocks($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->num_footer_blocks !== $v) {
            $this->num_footer_blocks = $v;
            $this->modifiedColumns[SiteTableMap::COL_NUM_FOOTER_BLOCKS] = true;
        }

        return $this;
    } // setNumFooterBlocks()

    /**
     * Set the value of [general_phone] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function setGeneralPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->general_phone !== $v) {
            $this->general_phone = $v;
            $this->modifiedColumns[SiteTableMap::COL_GENERAL_PHONE] = true;
        }

        return $this;
    } // setGeneralPhone()

    /**
     * Set the value of [invoicing_company_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function setInvoicingCompanyId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->invoicing_company_id !== $v) {
            $this->invoicing_company_id = $v;
            $this->modifiedColumns[SiteTableMap::COL_INVOICING_COMPANY_ID] = true;
        }

        if ($this->aOwnCompany !== null && $this->aOwnCompany->getId() !== $v) {
            $this->aOwnCompany = null;
        }

        return $this;
    } // setInvoicingCompanyId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SiteTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SiteTableMap::translateFieldName('Domain', TableMap::TYPE_PHPNAME, $indexType)];
            $this->domain = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SiteTableMap::translateFieldName('CustomFolder', TableMap::TYPE_PHPNAME, $indexType)];
            $this->custom_folder = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SiteTableMap::translateFieldName('FromEmail', TableMap::TYPE_PHPNAME, $indexType)];
            $this->from_email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : SiteTableMap::translateFieldName('ContactFormToEmail', TableMap::TYPE_PHPNAME, $indexType)];
            $this->contact_form_to_email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : SiteTableMap::translateFieldName('NumFooterBlocks', TableMap::TYPE_PHPNAME, $indexType)];
            $this->num_footer_blocks = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : SiteTableMap::translateFieldName('GeneralPhone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->general_phone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : SiteTableMap::translateFieldName('InvoicingCompanyId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->invoicing_company_id = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 8; // 8 = SiteTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Cms\\Site'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aOwnCompany !== null && $this->invoicing_company_id !== $this->aOwnCompany->getId()) {
            $this->aOwnCompany = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSiteQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aOwnCompany = null;
            $this->collCustomMedias = null;

            $this->collPromoProducts = null;

            $this->collSaleOrders = null;

            $this->collSiteUsps = null;

            $this->collSiteFooterBlocks = null;

            $this->collSiteBlogs = null;

            $this->collSiteBanners = null;

            $this->collSiteFAQCategories = null;

            $this->collSiteFAQs = null;

            $this->collSitePages = null;

            $this->collMailTemplates = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Site::setDeleted()
     * @see Site::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSiteQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SiteTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aOwnCompany !== null) {
                if ($this->aOwnCompany->isModified() || $this->aOwnCompany->isNew()) {
                    $affectedRows += $this->aOwnCompany->save($con);
                }
                $this->setOwnCompany($this->aOwnCompany);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->customMediasScheduledForDeletion !== null) {
                if (!$this->customMediasScheduledForDeletion->isEmpty()) {
                    \Model\Cms\CustomMediaQuery::create()
                        ->filterByPrimaryKeys($this->customMediasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->customMediasScheduledForDeletion = null;
                }
            }

            if ($this->collCustomMedias !== null) {
                foreach ($this->collCustomMedias as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->promoProductsScheduledForDeletion !== null) {
                if (!$this->promoProductsScheduledForDeletion->isEmpty()) {
                    \Model\PromoProductQuery::create()
                        ->filterByPrimaryKeys($this->promoProductsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->promoProductsScheduledForDeletion = null;
                }
            }

            if ($this->collPromoProducts !== null) {
                foreach ($this->collPromoProducts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saleOrdersScheduledForDeletion !== null) {
                if (!$this->saleOrdersScheduledForDeletion->isEmpty()) {
                    foreach ($this->saleOrdersScheduledForDeletion as $saleOrder) {
                        // need to save related object because we set the relation to null
                        $saleOrder->save($con);
                    }
                    $this->saleOrdersScheduledForDeletion = null;
                }
            }

            if ($this->collSaleOrders !== null) {
                foreach ($this->collSaleOrders as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteUspsScheduledForDeletion !== null) {
                if (!$this->siteUspsScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SiteUspQuery::create()
                        ->filterByPrimaryKeys($this->siteUspsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteUspsScheduledForDeletion = null;
                }
            }

            if ($this->collSiteUsps !== null) {
                foreach ($this->collSiteUsps as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteFooterBlocksScheduledForDeletion !== null) {
                if (!$this->siteFooterBlocksScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SiteFooterBlockQuery::create()
                        ->filterByPrimaryKeys($this->siteFooterBlocksScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteFooterBlocksScheduledForDeletion = null;
                }
            }

            if ($this->collSiteFooterBlocks !== null) {
                foreach ($this->collSiteFooterBlocks as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteBlogsScheduledForDeletion !== null) {
                if (!$this->siteBlogsScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SiteBlogQuery::create()
                        ->filterByPrimaryKeys($this->siteBlogsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteBlogsScheduledForDeletion = null;
                }
            }

            if ($this->collSiteBlogs !== null) {
                foreach ($this->collSiteBlogs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteBannersScheduledForDeletion !== null) {
                if (!$this->siteBannersScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SiteBannerQuery::create()
                        ->filterByPrimaryKeys($this->siteBannersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteBannersScheduledForDeletion = null;
                }
            }

            if ($this->collSiteBanners !== null) {
                foreach ($this->collSiteBanners as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteFAQCategoriesScheduledForDeletion !== null) {
                if (!$this->siteFAQCategoriesScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SiteFAQCategoryQuery::create()
                        ->filterByPrimaryKeys($this->siteFAQCategoriesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteFAQCategoriesScheduledForDeletion = null;
                }
            }

            if ($this->collSiteFAQCategories !== null) {
                foreach ($this->collSiteFAQCategories as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->siteFAQsScheduledForDeletion !== null) {
                if (!$this->siteFAQsScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SiteFAQQuery::create()
                        ->filterByPrimaryKeys($this->siteFAQsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->siteFAQsScheduledForDeletion = null;
                }
            }

            if ($this->collSiteFAQs !== null) {
                foreach ($this->collSiteFAQs as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->sitePagesScheduledForDeletion !== null) {
                if (!$this->sitePagesScheduledForDeletion->isEmpty()) {
                    \Model\Cms\SitePageQuery::create()
                        ->filterByPrimaryKeys($this->sitePagesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->sitePagesScheduledForDeletion = null;
                }
            }

            if ($this->collSitePages !== null) {
                foreach ($this->collSitePages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->mailTemplatesScheduledForDeletion !== null) {
                if (!$this->mailTemplatesScheduledForDeletion->isEmpty()) {
                    \Model\Cms\Mail\MailTemplateQuery::create()
                        ->filterByPrimaryKeys($this->mailTemplatesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->mailTemplatesScheduledForDeletion = null;
                }
            }

            if ($this->collMailTemplates !== null) {
                foreach ($this->collMailTemplates as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[SiteTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SiteTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SiteTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(SiteTableMap::COL_DOMAIN)) {
            $modifiedColumns[':p' . $index++]  = 'domain';
        }
        if ($this->isColumnModified(SiteTableMap::COL_CUSTOM_FOLDER)) {
            $modifiedColumns[':p' . $index++]  = 'custom_folder';
        }
        if ($this->isColumnModified(SiteTableMap::COL_FROM_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'from_email';
        }
        if ($this->isColumnModified(SiteTableMap::COL_CONTACT_FORM_TO_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'contact_form_to_email';
        }
        if ($this->isColumnModified(SiteTableMap::COL_NUM_FOOTER_BLOCKS)) {
            $modifiedColumns[':p' . $index++]  = 'num_footer_blocks';
        }
        if ($this->isColumnModified(SiteTableMap::COL_GENERAL_PHONE)) {
            $modifiedColumns[':p' . $index++]  = 'general_phone';
        }
        if ($this->isColumnModified(SiteTableMap::COL_INVOICING_COMPANY_ID)) {
            $modifiedColumns[':p' . $index++]  = 'invoicing_company_id';
        }

        $sql = sprintf(
            'INSERT INTO site (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'domain':
                        $stmt->bindValue($identifier, $this->domain, PDO::PARAM_STR);
                        break;
                    case 'custom_folder':
                        $stmt->bindValue($identifier, $this->custom_folder, PDO::PARAM_STR);
                        break;
                    case 'from_email':
                        $stmt->bindValue($identifier, $this->from_email, PDO::PARAM_STR);
                        break;
                    case 'contact_form_to_email':
                        $stmt->bindValue($identifier, $this->contact_form_to_email, PDO::PARAM_STR);
                        break;
                    case 'num_footer_blocks':
                        $stmt->bindValue($identifier, $this->num_footer_blocks, PDO::PARAM_INT);
                        break;
                    case 'general_phone':
                        $stmt->bindValue($identifier, $this->general_phone, PDO::PARAM_STR);
                        break;
                    case 'invoicing_company_id':
                        $stmt->bindValue($identifier, $this->invoicing_company_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SiteTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getDomain();
                break;
            case 2:
                return $this->getCustomFolder();
                break;
            case 3:
                return $this->getFromEmail();
                break;
            case 4:
                return $this->getContactFormToEmail();
                break;
            case 5:
                return $this->getNumFooterBlocks();
                break;
            case 6:
                return $this->getGeneralPhone();
                break;
            case 7:
                return $this->getInvoicingCompanyId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Site'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Site'][$this->hashCode()] = true;
        $keys = SiteTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getDomain(),
            $keys[2] => $this->getCustomFolder(),
            $keys[3] => $this->getFromEmail(),
            $keys[4] => $this->getContactFormToEmail(),
            $keys[5] => $this->getNumFooterBlocks(),
            $keys[6] => $this->getGeneralPhone(),
            $keys[7] => $this->getInvoicingCompanyId(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aOwnCompany) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'company';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'own_company';
                        break;
                    default:
                        $key = 'OwnCompany';
                }

                $result[$key] = $this->aOwnCompany->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collCustomMedias) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customMedias';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'custom_medias';
                        break;
                    default:
                        $key = 'CustomMedias';
                }

                $result[$key] = $this->collCustomMedias->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPromoProducts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'promoProducts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'promo_products';
                        break;
                    default:
                        $key = 'PromoProducts';
                }

                $result[$key] = $this->collPromoProducts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaleOrders) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'saleOrders';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_orders';
                        break;
                    default:
                        $key = 'SaleOrders';
                }

                $result[$key] = $this->collSaleOrders->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteUsps) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteUsps';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_usps';
                        break;
                    default:
                        $key = 'SiteUsps';
                }

                $result[$key] = $this->collSiteUsps->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteFooterBlocks) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteFooterBlocks';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_footer_blocks';
                        break;
                    default:
                        $key = 'SiteFooterBlocks';
                }

                $result[$key] = $this->collSiteFooterBlocks->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteBlogs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteBlogs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_blogs';
                        break;
                    default:
                        $key = 'SiteBlogs';
                }

                $result[$key] = $this->collSiteBlogs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteBanners) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteBanners';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_banners';
                        break;
                    default:
                        $key = 'SiteBanners';
                }

                $result[$key] = $this->collSiteBanners->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteFAQCategories) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteFAQCategories';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_faq_categories';
                        break;
                    default:
                        $key = 'SiteFAQCategories';
                }

                $result[$key] = $this->collSiteFAQCategories->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSiteFAQs) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'siteFAQs';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_faqs';
                        break;
                    default:
                        $key = 'SiteFAQs';
                }

                $result[$key] = $this->collSiteFAQs->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSitePages) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'sitePages';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site_pages';
                        break;
                    default:
                        $key = 'SitePages';
                }

                $result[$key] = $this->collSitePages->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMailTemplates) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'mailTemplates';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mail_templates';
                        break;
                    default:
                        $key = 'MailTemplates';
                }

                $result[$key] = $this->collMailTemplates->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Cms\Site
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SiteTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Cms\Site
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setDomain($value);
                break;
            case 2:
                $this->setCustomFolder($value);
                break;
            case 3:
                $this->setFromEmail($value);
                break;
            case 4:
                $this->setContactFormToEmail($value);
                break;
            case 5:
                $this->setNumFooterBlocks($value);
                break;
            case 6:
                $this->setGeneralPhone($value);
                break;
            case 7:
                $this->setInvoicingCompanyId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SiteTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setDomain($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setCustomFolder($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setFromEmail($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setContactFormToEmail($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setNumFooterBlocks($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setGeneralPhone($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setInvoicingCompanyId($arr[$keys[7]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Cms\Site The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SiteTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SiteTableMap::COL_ID)) {
            $criteria->add(SiteTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(SiteTableMap::COL_DOMAIN)) {
            $criteria->add(SiteTableMap::COL_DOMAIN, $this->domain);
        }
        if ($this->isColumnModified(SiteTableMap::COL_CUSTOM_FOLDER)) {
            $criteria->add(SiteTableMap::COL_CUSTOM_FOLDER, $this->custom_folder);
        }
        if ($this->isColumnModified(SiteTableMap::COL_FROM_EMAIL)) {
            $criteria->add(SiteTableMap::COL_FROM_EMAIL, $this->from_email);
        }
        if ($this->isColumnModified(SiteTableMap::COL_CONTACT_FORM_TO_EMAIL)) {
            $criteria->add(SiteTableMap::COL_CONTACT_FORM_TO_EMAIL, $this->contact_form_to_email);
        }
        if ($this->isColumnModified(SiteTableMap::COL_NUM_FOOTER_BLOCKS)) {
            $criteria->add(SiteTableMap::COL_NUM_FOOTER_BLOCKS, $this->num_footer_blocks);
        }
        if ($this->isColumnModified(SiteTableMap::COL_GENERAL_PHONE)) {
            $criteria->add(SiteTableMap::COL_GENERAL_PHONE, $this->general_phone);
        }
        if ($this->isColumnModified(SiteTableMap::COL_INVOICING_COMPANY_ID)) {
            $criteria->add(SiteTableMap::COL_INVOICING_COMPANY_ID, $this->invoicing_company_id);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSiteQuery::create();
        $criteria->add(SiteTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Cms\Site (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setDomain($this->getDomain());
        $copyObj->setCustomFolder($this->getCustomFolder());
        $copyObj->setFromEmail($this->getFromEmail());
        $copyObj->setContactFormToEmail($this->getContactFormToEmail());
        $copyObj->setNumFooterBlocks($this->getNumFooterBlocks());
        $copyObj->setGeneralPhone($this->getGeneralPhone());
        $copyObj->setInvoicingCompanyId($this->getInvoicingCompanyId());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getCustomMedias() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomMedia($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPromoProducts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPromoProduct($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaleOrders() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaleOrder($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteUsps() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteUsp($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteFooterBlocks() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteFooterBlock($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteBlogs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteBlog($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteBanners() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteBanner($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteFAQCategories() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteFAQCategory($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSiteFAQs() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSiteFAQ($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSitePages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSitePage($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMailTemplates() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMailTemplate($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Cms\Site Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a Company object.
     *
     * @param  Company|null $v
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     * @throws PropelException
     */
    public function setOwnCompany(Company $v = null)
    {
        if ($v === null) {
            $this->setInvoicingCompanyId(NULL);
        } else {
            $this->setInvoicingCompanyId($v->getId());
        }

        $this->aOwnCompany = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Company object, it will not be re-added.
        if ($v !== null) {
            $v->addSite($this);
        }


        return $this;
    }


    /**
     * Get the associated Company object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Company|null The associated Company object.
     * @throws PropelException
     */
    public function getOwnCompany(ConnectionInterface $con = null)
    {
        if ($this->aOwnCompany === null && ($this->invoicing_company_id != 0)) {
            $this->aOwnCompany = CompanyQuery::create()->findPk($this->invoicing_company_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aOwnCompany->addSites($this);
             */
        }

        return $this->aOwnCompany;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('CustomMedia' === $relationName) {
            $this->initCustomMedias();
            return;
        }
        if ('PromoProduct' === $relationName) {
            $this->initPromoProducts();
            return;
        }
        if ('SaleOrder' === $relationName) {
            $this->initSaleOrders();
            return;
        }
        if ('SiteUsp' === $relationName) {
            $this->initSiteUsps();
            return;
        }
        if ('SiteFooterBlock' === $relationName) {
            $this->initSiteFooterBlocks();
            return;
        }
        if ('SiteBlog' === $relationName) {
            $this->initSiteBlogs();
            return;
        }
        if ('SiteBanner' === $relationName) {
            $this->initSiteBanners();
            return;
        }
        if ('SiteFAQCategory' === $relationName) {
            $this->initSiteFAQCategories();
            return;
        }
        if ('SiteFAQ' === $relationName) {
            $this->initSiteFAQs();
            return;
        }
        if ('SitePage' === $relationName) {
            $this->initSitePages();
            return;
        }
        if ('MailTemplate' === $relationName) {
            $this->initMailTemplates();
            return;
        }
    }

    /**
     * Clears out the collCustomMedias collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomMedias()
     */
    public function clearCustomMedias()
    {
        $this->collCustomMedias = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomMedias collection loaded partially.
     */
    public function resetPartialCustomMedias($v = true)
    {
        $this->collCustomMediasPartial = $v;
    }

    /**
     * Initializes the collCustomMedias collection.
     *
     * By default this just sets the collCustomMedias collection to an empty array (like clearcollCustomMedias());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomMedias($overrideExisting = true)
    {
        if (null !== $this->collCustomMedias && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomMediaTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomMedias = new $collectionClassName;
        $this->collCustomMedias->setModel('\Model\Cms\CustomMedia');
    }

    /**
     * Gets an array of ChildCustomMedia objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildCustomMedia[] List of ChildCustomMedia objects
     * @throws PropelException
     */
    public function getCustomMedias(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomMediasPartial && !$this->isNew();
        if (null === $this->collCustomMedias || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCustomMedias) {
                    $this->initCustomMedias();
                } else {
                    $collectionClassName = CustomMediaTableMap::getTableMap()->getCollectionClassName();

                    $collCustomMedias = new $collectionClassName;
                    $collCustomMedias->setModel('\Model\Cms\CustomMedia');

                    return $collCustomMedias;
                }
            } else {
                $collCustomMedias = ChildCustomMediaQuery::create(null, $criteria)
                    ->filterBySite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomMediasPartial && count($collCustomMedias)) {
                        $this->initCustomMedias(false);

                        foreach ($collCustomMedias as $obj) {
                            if (false == $this->collCustomMedias->contains($obj)) {
                                $this->collCustomMedias->append($obj);
                            }
                        }

                        $this->collCustomMediasPartial = true;
                    }

                    return $collCustomMedias;
                }

                if ($partial && $this->collCustomMedias) {
                    foreach ($this->collCustomMedias as $obj) {
                        if ($obj->isNew()) {
                            $collCustomMedias[] = $obj;
                        }
                    }
                }

                $this->collCustomMedias = $collCustomMedias;
                $this->collCustomMediasPartial = false;
            }
        }

        return $this->collCustomMedias;
    }

    /**
     * Sets a collection of ChildCustomMedia objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customMedias A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function setCustomMedias(Collection $customMedias, ConnectionInterface $con = null)
    {
        /** @var ChildCustomMedia[] $customMediasToDelete */
        $customMediasToDelete = $this->getCustomMedias(new Criteria(), $con)->diff($customMedias);


        $this->customMediasScheduledForDeletion = $customMediasToDelete;

        foreach ($customMediasToDelete as $customMediaRemoved) {
            $customMediaRemoved->setSite(null);
        }

        $this->collCustomMedias = null;
        foreach ($customMedias as $customMedia) {
            $this->addCustomMedia($customMedia);
        }

        $this->collCustomMedias = $customMedias;
        $this->collCustomMediasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related CustomMedia objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related CustomMedia objects.
     * @throws PropelException
     */
    public function countCustomMedias(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomMediasPartial && !$this->isNew();
        if (null === $this->collCustomMedias || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomMedias) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomMedias());
            }

            $query = ChildCustomMediaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySite($this)
                ->count($con);
        }

        return count($this->collCustomMedias);
    }

    /**
     * Method called to associate a ChildCustomMedia object to this object
     * through the ChildCustomMedia foreign key attribute.
     *
     * @param  ChildCustomMedia $l ChildCustomMedia
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function addCustomMedia(ChildCustomMedia $l)
    {
        if ($this->collCustomMedias === null) {
            $this->initCustomMedias();
            $this->collCustomMediasPartial = true;
        }

        if (!$this->collCustomMedias->contains($l)) {
            $this->doAddCustomMedia($l);

            if ($this->customMediasScheduledForDeletion and $this->customMediasScheduledForDeletion->contains($l)) {
                $this->customMediasScheduledForDeletion->remove($this->customMediasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildCustomMedia $customMedia The ChildCustomMedia object to add.
     */
    protected function doAddCustomMedia(ChildCustomMedia $customMedia)
    {
        $this->collCustomMedias[]= $customMedia;
        $customMedia->setSite($this);
    }

    /**
     * @param  ChildCustomMedia $customMedia The ChildCustomMedia object to remove.
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function removeCustomMedia(ChildCustomMedia $customMedia)
    {
        if ($this->getCustomMedias()->contains($customMedia)) {
            $pos = $this->collCustomMedias->search($customMedia);
            $this->collCustomMedias->remove($pos);
            if (null === $this->customMediasScheduledForDeletion) {
                $this->customMediasScheduledForDeletion = clone $this->collCustomMedias;
                $this->customMediasScheduledForDeletion->clear();
            }
            $this->customMediasScheduledForDeletion[]= clone $customMedia;
            $customMedia->setSite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Site is new, it will return
     * an empty collection; or if this Site has previously
     * been saved, it will retrieve related CustomMedias from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Site.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildCustomMedia[] List of ChildCustomMedia objects
     */
    public function getCustomMediasJoinCustomMediaNodeType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildCustomMediaQuery::create(null, $criteria);
        $query->joinWith('CustomMediaNodeType', $joinBehavior);

        return $this->getCustomMedias($query, $con);
    }

    /**
     * Clears out the collPromoProducts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPromoProducts()
     */
    public function clearPromoProducts()
    {
        $this->collPromoProducts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPromoProducts collection loaded partially.
     */
    public function resetPartialPromoProducts($v = true)
    {
        $this->collPromoProductsPartial = $v;
    }

    /**
     * Initializes the collPromoProducts collection.
     *
     * By default this just sets the collPromoProducts collection to an empty array (like clearcollPromoProducts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPromoProducts($overrideExisting = true)
    {
        if (null !== $this->collPromoProducts && !$overrideExisting) {
            return;
        }

        $collectionClassName = PromoProductTableMap::getTableMap()->getCollectionClassName();

        $this->collPromoProducts = new $collectionClassName;
        $this->collPromoProducts->setModel('\Model\PromoProduct');
    }

    /**
     * Gets an array of PromoProduct objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|PromoProduct[] List of PromoProduct objects
     * @throws PropelException
     */
    public function getPromoProducts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPromoProductsPartial && !$this->isNew();
        if (null === $this->collPromoProducts || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPromoProducts) {
                    $this->initPromoProducts();
                } else {
                    $collectionClassName = PromoProductTableMap::getTableMap()->getCollectionClassName();

                    $collPromoProducts = new $collectionClassName;
                    $collPromoProducts->setModel('\Model\PromoProduct');

                    return $collPromoProducts;
                }
            } else {
                $collPromoProducts = PromoProductQuery::create(null, $criteria)
                    ->filterBySite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPromoProductsPartial && count($collPromoProducts)) {
                        $this->initPromoProducts(false);

                        foreach ($collPromoProducts as $obj) {
                            if (false == $this->collPromoProducts->contains($obj)) {
                                $this->collPromoProducts->append($obj);
                            }
                        }

                        $this->collPromoProductsPartial = true;
                    }

                    return $collPromoProducts;
                }

                if ($partial && $this->collPromoProducts) {
                    foreach ($this->collPromoProducts as $obj) {
                        if ($obj->isNew()) {
                            $collPromoProducts[] = $obj;
                        }
                    }
                }

                $this->collPromoProducts = $collPromoProducts;
                $this->collPromoProductsPartial = false;
            }
        }

        return $this->collPromoProducts;
    }

    /**
     * Sets a collection of PromoProduct objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $promoProducts A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function setPromoProducts(Collection $promoProducts, ConnectionInterface $con = null)
    {
        /** @var PromoProduct[] $promoProductsToDelete */
        $promoProductsToDelete = $this->getPromoProducts(new Criteria(), $con)->diff($promoProducts);


        $this->promoProductsScheduledForDeletion = $promoProductsToDelete;

        foreach ($promoProductsToDelete as $promoProductRemoved) {
            $promoProductRemoved->setSite(null);
        }

        $this->collPromoProducts = null;
        foreach ($promoProducts as $promoProduct) {
            $this->addPromoProduct($promoProduct);
        }

        $this->collPromoProducts = $promoProducts;
        $this->collPromoProductsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BasePromoProduct objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BasePromoProduct objects.
     * @throws PropelException
     */
    public function countPromoProducts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPromoProductsPartial && !$this->isNew();
        if (null === $this->collPromoProducts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPromoProducts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPromoProducts());
            }

            $query = PromoProductQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySite($this)
                ->count($con);
        }

        return count($this->collPromoProducts);
    }

    /**
     * Method called to associate a PromoProduct object to this object
     * through the PromoProduct foreign key attribute.
     *
     * @param  PromoProduct $l PromoProduct
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function addPromoProduct(PromoProduct $l)
    {
        if ($this->collPromoProducts === null) {
            $this->initPromoProducts();
            $this->collPromoProductsPartial = true;
        }

        if (!$this->collPromoProducts->contains($l)) {
            $this->doAddPromoProduct($l);

            if ($this->promoProductsScheduledForDeletion and $this->promoProductsScheduledForDeletion->contains($l)) {
                $this->promoProductsScheduledForDeletion->remove($this->promoProductsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param PromoProduct $promoProduct The PromoProduct object to add.
     */
    protected function doAddPromoProduct(PromoProduct $promoProduct)
    {
        $this->collPromoProducts[]= $promoProduct;
        $promoProduct->setSite($this);
    }

    /**
     * @param  PromoProduct $promoProduct The PromoProduct object to remove.
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function removePromoProduct(PromoProduct $promoProduct)
    {
        if ($this->getPromoProducts()->contains($promoProduct)) {
            $pos = $this->collPromoProducts->search($promoProduct);
            $this->collPromoProducts->remove($pos);
            if (null === $this->promoProductsScheduledForDeletion) {
                $this->promoProductsScheduledForDeletion = clone $this->collPromoProducts;
                $this->promoProductsScheduledForDeletion->clear();
            }
            $this->promoProductsScheduledForDeletion[]= $promoProduct;
            $promoProduct->setSite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Site is new, it will return
     * an empty collection; or if this Site has previously
     * been saved, it will retrieve related PromoProducts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Site.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|PromoProduct[] List of PromoProduct objects
     */
    public function getPromoProductsJoinProduct(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = PromoProductQuery::create(null, $criteria);
        $query->joinWith('Product', $joinBehavior);

        return $this->getPromoProducts($query, $con);
    }

    /**
     * Clears out the collSaleOrders collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSaleOrders()
     */
    public function clearSaleOrders()
    {
        $this->collSaleOrders = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSaleOrders collection loaded partially.
     */
    public function resetPartialSaleOrders($v = true)
    {
        $this->collSaleOrdersPartial = $v;
    }

    /**
     * Initializes the collSaleOrders collection.
     *
     * By default this just sets the collSaleOrders collection to an empty array (like clearcollSaleOrders());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaleOrders($overrideExisting = true)
    {
        if (null !== $this->collSaleOrders && !$overrideExisting) {
            return;
        }

        $collectionClassName = SaleOrderTableMap::getTableMap()->getCollectionClassName();

        $this->collSaleOrders = new $collectionClassName;
        $this->collSaleOrders->setModel('\Model\Sale\SaleOrder');
    }

    /**
     * Gets an array of SaleOrder objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|SaleOrder[] List of SaleOrder objects
     * @throws PropelException
     */
    public function getSaleOrders(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrdersPartial && !$this->isNew();
        if (null === $this->collSaleOrders || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSaleOrders) {
                    $this->initSaleOrders();
                } else {
                    $collectionClassName = SaleOrderTableMap::getTableMap()->getCollectionClassName();

                    $collSaleOrders = new $collectionClassName;
                    $collSaleOrders->setModel('\Model\Sale\SaleOrder');

                    return $collSaleOrders;
                }
            } else {
                $collSaleOrders = SaleOrderQuery::create(null, $criteria)
                    ->filterBySite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSaleOrdersPartial && count($collSaleOrders)) {
                        $this->initSaleOrders(false);

                        foreach ($collSaleOrders as $obj) {
                            if (false == $this->collSaleOrders->contains($obj)) {
                                $this->collSaleOrders->append($obj);
                            }
                        }

                        $this->collSaleOrdersPartial = true;
                    }

                    return $collSaleOrders;
                }

                if ($partial && $this->collSaleOrders) {
                    foreach ($this->collSaleOrders as $obj) {
                        if ($obj->isNew()) {
                            $collSaleOrders[] = $obj;
                        }
                    }
                }

                $this->collSaleOrders = $collSaleOrders;
                $this->collSaleOrdersPartial = false;
            }
        }

        return $this->collSaleOrders;
    }

    /**
     * Sets a collection of SaleOrder objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $saleOrders A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function setSaleOrders(Collection $saleOrders, ConnectionInterface $con = null)
    {
        /** @var SaleOrder[] $saleOrdersToDelete */
        $saleOrdersToDelete = $this->getSaleOrders(new Criteria(), $con)->diff($saleOrders);


        $this->saleOrdersScheduledForDeletion = $saleOrdersToDelete;

        foreach ($saleOrdersToDelete as $saleOrderRemoved) {
            $saleOrderRemoved->setSite(null);
        }

        $this->collSaleOrders = null;
        foreach ($saleOrders as $saleOrder) {
            $this->addSaleOrder($saleOrder);
        }

        $this->collSaleOrders = $saleOrders;
        $this->collSaleOrdersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSaleOrder objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSaleOrder objects.
     * @throws PropelException
     */
    public function countSaleOrders(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrdersPartial && !$this->isNew();
        if (null === $this->collSaleOrders || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaleOrders) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSaleOrders());
            }

            $query = SaleOrderQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySite($this)
                ->count($con);
        }

        return count($this->collSaleOrders);
    }

    /**
     * Method called to associate a SaleOrder object to this object
     * through the SaleOrder foreign key attribute.
     *
     * @param  SaleOrder $l SaleOrder
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function addSaleOrder(SaleOrder $l)
    {
        if ($this->collSaleOrders === null) {
            $this->initSaleOrders();
            $this->collSaleOrdersPartial = true;
        }

        if (!$this->collSaleOrders->contains($l)) {
            $this->doAddSaleOrder($l);

            if ($this->saleOrdersScheduledForDeletion and $this->saleOrdersScheduledForDeletion->contains($l)) {
                $this->saleOrdersScheduledForDeletion->remove($this->saleOrdersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param SaleOrder $saleOrder The SaleOrder object to add.
     */
    protected function doAddSaleOrder(SaleOrder $saleOrder)
    {
        $this->collSaleOrders[]= $saleOrder;
        $saleOrder->setSite($this);
    }

    /**
     * @param  SaleOrder $saleOrder The SaleOrder object to remove.
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function removeSaleOrder(SaleOrder $saleOrder)
    {
        if ($this->getSaleOrders()->contains($saleOrder)) {
            $pos = $this->collSaleOrders->search($saleOrder);
            $this->collSaleOrders->remove($pos);
            if (null === $this->saleOrdersScheduledForDeletion) {
                $this->saleOrdersScheduledForDeletion = clone $this->collSaleOrders;
                $this->saleOrdersScheduledForDeletion->clear();
            }
            $this->saleOrdersScheduledForDeletion[]= $saleOrder;
            $saleOrder->setSite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Site is new, it will return
     * an empty collection; or if this Site has previously
     * been saved, it will retrieve related SaleOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Site.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SaleOrder[] List of SaleOrder objects
     */
    public function getSaleOrdersJoinSaleOrderStatus(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SaleOrderQuery::create(null, $criteria);
        $query->joinWith('SaleOrderStatus', $joinBehavior);

        return $this->getSaleOrders($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Site is new, it will return
     * an empty collection; or if this Site has previously
     * been saved, it will retrieve related SaleOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Site.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SaleOrder[] List of SaleOrder objects
     */
    public function getSaleOrdersJoinShippingMethod(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SaleOrderQuery::create(null, $criteria);
        $query->joinWith('ShippingMethod', $joinBehavior);

        return $this->getSaleOrders($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Site is new, it will return
     * an empty collection; or if this Site has previously
     * been saved, it will retrieve related SaleOrders from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Site.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SaleOrder[] List of SaleOrder objects
     */
    public function getSaleOrdersJoinUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SaleOrderQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getSaleOrders($query, $con);
    }

    /**
     * Clears out the collSiteUsps collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteUsps()
     */
    public function clearSiteUsps()
    {
        $this->collSiteUsps = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteUsps collection loaded partially.
     */
    public function resetPartialSiteUsps($v = true)
    {
        $this->collSiteUspsPartial = $v;
    }

    /**
     * Initializes the collSiteUsps collection.
     *
     * By default this just sets the collSiteUsps collection to an empty array (like clearcollSiteUsps());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteUsps($overrideExisting = true)
    {
        if (null !== $this->collSiteUsps && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteUspTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteUsps = new $collectionClassName;
        $this->collSiteUsps->setModel('\Model\Cms\SiteUsp');
    }

    /**
     * Gets an array of ChildSiteUsp objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSiteUsp[] List of ChildSiteUsp objects
     * @throws PropelException
     */
    public function getSiteUsps(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteUspsPartial && !$this->isNew();
        if (null === $this->collSiteUsps || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteUsps) {
                    $this->initSiteUsps();
                } else {
                    $collectionClassName = SiteUspTableMap::getTableMap()->getCollectionClassName();

                    $collSiteUsps = new $collectionClassName;
                    $collSiteUsps->setModel('\Model\Cms\SiteUsp');

                    return $collSiteUsps;
                }
            } else {
                $collSiteUsps = ChildSiteUspQuery::create(null, $criteria)
                    ->filterBySite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteUspsPartial && count($collSiteUsps)) {
                        $this->initSiteUsps(false);

                        foreach ($collSiteUsps as $obj) {
                            if (false == $this->collSiteUsps->contains($obj)) {
                                $this->collSiteUsps->append($obj);
                            }
                        }

                        $this->collSiteUspsPartial = true;
                    }

                    return $collSiteUsps;
                }

                if ($partial && $this->collSiteUsps) {
                    foreach ($this->collSiteUsps as $obj) {
                        if ($obj->isNew()) {
                            $collSiteUsps[] = $obj;
                        }
                    }
                }

                $this->collSiteUsps = $collSiteUsps;
                $this->collSiteUspsPartial = false;
            }
        }

        return $this->collSiteUsps;
    }

    /**
     * Sets a collection of ChildSiteUsp objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteUsps A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function setSiteUsps(Collection $siteUsps, ConnectionInterface $con = null)
    {
        /** @var ChildSiteUsp[] $siteUspsToDelete */
        $siteUspsToDelete = $this->getSiteUsps(new Criteria(), $con)->diff($siteUsps);


        $this->siteUspsScheduledForDeletion = $siteUspsToDelete;

        foreach ($siteUspsToDelete as $siteUspRemoved) {
            $siteUspRemoved->setSite(null);
        }

        $this->collSiteUsps = null;
        foreach ($siteUsps as $siteUsp) {
            $this->addSiteUsp($siteUsp);
        }

        $this->collSiteUsps = $siteUsps;
        $this->collSiteUspsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SiteUsp objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SiteUsp objects.
     * @throws PropelException
     */
    public function countSiteUsps(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteUspsPartial && !$this->isNew();
        if (null === $this->collSiteUsps || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteUsps) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteUsps());
            }

            $query = ChildSiteUspQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySite($this)
                ->count($con);
        }

        return count($this->collSiteUsps);
    }

    /**
     * Method called to associate a ChildSiteUsp object to this object
     * through the ChildSiteUsp foreign key attribute.
     *
     * @param  ChildSiteUsp $l ChildSiteUsp
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function addSiteUsp(ChildSiteUsp $l)
    {
        if ($this->collSiteUsps === null) {
            $this->initSiteUsps();
            $this->collSiteUspsPartial = true;
        }

        if (!$this->collSiteUsps->contains($l)) {
            $this->doAddSiteUsp($l);

            if ($this->siteUspsScheduledForDeletion and $this->siteUspsScheduledForDeletion->contains($l)) {
                $this->siteUspsScheduledForDeletion->remove($this->siteUspsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSiteUsp $siteUsp The ChildSiteUsp object to add.
     */
    protected function doAddSiteUsp(ChildSiteUsp $siteUsp)
    {
        $this->collSiteUsps[]= $siteUsp;
        $siteUsp->setSite($this);
    }

    /**
     * @param  ChildSiteUsp $siteUsp The ChildSiteUsp object to remove.
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function removeSiteUsp(ChildSiteUsp $siteUsp)
    {
        if ($this->getSiteUsps()->contains($siteUsp)) {
            $pos = $this->collSiteUsps->search($siteUsp);
            $this->collSiteUsps->remove($pos);
            if (null === $this->siteUspsScheduledForDeletion) {
                $this->siteUspsScheduledForDeletion = clone $this->collSiteUsps;
                $this->siteUspsScheduledForDeletion->clear();
            }
            $this->siteUspsScheduledForDeletion[]= clone $siteUsp;
            $siteUsp->setSite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Site is new, it will return
     * an empty collection; or if this Site has previously
     * been saved, it will retrieve related SiteUsps from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Site.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSiteUsp[] List of ChildSiteUsp objects
     */
    public function getSiteUspsJoinLanguage(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSiteUspQuery::create(null, $criteria);
        $query->joinWith('Language', $joinBehavior);

        return $this->getSiteUsps($query, $con);
    }

    /**
     * Clears out the collSiteFooterBlocks collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteFooterBlocks()
     */
    public function clearSiteFooterBlocks()
    {
        $this->collSiteFooterBlocks = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteFooterBlocks collection loaded partially.
     */
    public function resetPartialSiteFooterBlocks($v = true)
    {
        $this->collSiteFooterBlocksPartial = $v;
    }

    /**
     * Initializes the collSiteFooterBlocks collection.
     *
     * By default this just sets the collSiteFooterBlocks collection to an empty array (like clearcollSiteFooterBlocks());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteFooterBlocks($overrideExisting = true)
    {
        if (null !== $this->collSiteFooterBlocks && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteFooterBlockTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteFooterBlocks = new $collectionClassName;
        $this->collSiteFooterBlocks->setModel('\Model\Cms\SiteFooterBlock');
    }

    /**
     * Gets an array of ChildSiteFooterBlock objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSiteFooterBlock[] List of ChildSiteFooterBlock objects
     * @throws PropelException
     */
    public function getSiteFooterBlocks(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFooterBlocksPartial && !$this->isNew();
        if (null === $this->collSiteFooterBlocks || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteFooterBlocks) {
                    $this->initSiteFooterBlocks();
                } else {
                    $collectionClassName = SiteFooterBlockTableMap::getTableMap()->getCollectionClassName();

                    $collSiteFooterBlocks = new $collectionClassName;
                    $collSiteFooterBlocks->setModel('\Model\Cms\SiteFooterBlock');

                    return $collSiteFooterBlocks;
                }
            } else {
                $collSiteFooterBlocks = ChildSiteFooterBlockQuery::create(null, $criteria)
                    ->filterBySite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteFooterBlocksPartial && count($collSiteFooterBlocks)) {
                        $this->initSiteFooterBlocks(false);

                        foreach ($collSiteFooterBlocks as $obj) {
                            if (false == $this->collSiteFooterBlocks->contains($obj)) {
                                $this->collSiteFooterBlocks->append($obj);
                            }
                        }

                        $this->collSiteFooterBlocksPartial = true;
                    }

                    return $collSiteFooterBlocks;
                }

                if ($partial && $this->collSiteFooterBlocks) {
                    foreach ($this->collSiteFooterBlocks as $obj) {
                        if ($obj->isNew()) {
                            $collSiteFooterBlocks[] = $obj;
                        }
                    }
                }

                $this->collSiteFooterBlocks = $collSiteFooterBlocks;
                $this->collSiteFooterBlocksPartial = false;
            }
        }

        return $this->collSiteFooterBlocks;
    }

    /**
     * Sets a collection of ChildSiteFooterBlock objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteFooterBlocks A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function setSiteFooterBlocks(Collection $siteFooterBlocks, ConnectionInterface $con = null)
    {
        /** @var ChildSiteFooterBlock[] $siteFooterBlocksToDelete */
        $siteFooterBlocksToDelete = $this->getSiteFooterBlocks(new Criteria(), $con)->diff($siteFooterBlocks);


        $this->siteFooterBlocksScheduledForDeletion = $siteFooterBlocksToDelete;

        foreach ($siteFooterBlocksToDelete as $siteFooterBlockRemoved) {
            $siteFooterBlockRemoved->setSite(null);
        }

        $this->collSiteFooterBlocks = null;
        foreach ($siteFooterBlocks as $siteFooterBlock) {
            $this->addSiteFooterBlock($siteFooterBlock);
        }

        $this->collSiteFooterBlocks = $siteFooterBlocks;
        $this->collSiteFooterBlocksPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SiteFooterBlock objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SiteFooterBlock objects.
     * @throws PropelException
     */
    public function countSiteFooterBlocks(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFooterBlocksPartial && !$this->isNew();
        if (null === $this->collSiteFooterBlocks || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteFooterBlocks) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteFooterBlocks());
            }

            $query = ChildSiteFooterBlockQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySite($this)
                ->count($con);
        }

        return count($this->collSiteFooterBlocks);
    }

    /**
     * Method called to associate a ChildSiteFooterBlock object to this object
     * through the ChildSiteFooterBlock foreign key attribute.
     *
     * @param  ChildSiteFooterBlock $l ChildSiteFooterBlock
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function addSiteFooterBlock(ChildSiteFooterBlock $l)
    {
        if ($this->collSiteFooterBlocks === null) {
            $this->initSiteFooterBlocks();
            $this->collSiteFooterBlocksPartial = true;
        }

        if (!$this->collSiteFooterBlocks->contains($l)) {
            $this->doAddSiteFooterBlock($l);

            if ($this->siteFooterBlocksScheduledForDeletion and $this->siteFooterBlocksScheduledForDeletion->contains($l)) {
                $this->siteFooterBlocksScheduledForDeletion->remove($this->siteFooterBlocksScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSiteFooterBlock $siteFooterBlock The ChildSiteFooterBlock object to add.
     */
    protected function doAddSiteFooterBlock(ChildSiteFooterBlock $siteFooterBlock)
    {
        $this->collSiteFooterBlocks[]= $siteFooterBlock;
        $siteFooterBlock->setSite($this);
    }

    /**
     * @param  ChildSiteFooterBlock $siteFooterBlock The ChildSiteFooterBlock object to remove.
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function removeSiteFooterBlock(ChildSiteFooterBlock $siteFooterBlock)
    {
        if ($this->getSiteFooterBlocks()->contains($siteFooterBlock)) {
            $pos = $this->collSiteFooterBlocks->search($siteFooterBlock);
            $this->collSiteFooterBlocks->remove($pos);
            if (null === $this->siteFooterBlocksScheduledForDeletion) {
                $this->siteFooterBlocksScheduledForDeletion = clone $this->collSiteFooterBlocks;
                $this->siteFooterBlocksScheduledForDeletion->clear();
            }
            $this->siteFooterBlocksScheduledForDeletion[]= clone $siteFooterBlock;
            $siteFooterBlock->setSite(null);
        }

        return $this;
    }

    /**
     * Clears out the collSiteBlogs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteBlogs()
     */
    public function clearSiteBlogs()
    {
        $this->collSiteBlogs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteBlogs collection loaded partially.
     */
    public function resetPartialSiteBlogs($v = true)
    {
        $this->collSiteBlogsPartial = $v;
    }

    /**
     * Initializes the collSiteBlogs collection.
     *
     * By default this just sets the collSiteBlogs collection to an empty array (like clearcollSiteBlogs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteBlogs($overrideExisting = true)
    {
        if (null !== $this->collSiteBlogs && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteBlogTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteBlogs = new $collectionClassName;
        $this->collSiteBlogs->setModel('\Model\Cms\SiteBlog');
    }

    /**
     * Gets an array of ChildSiteBlog objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSiteBlog[] List of ChildSiteBlog objects
     * @throws PropelException
     */
    public function getSiteBlogs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteBlogsPartial && !$this->isNew();
        if (null === $this->collSiteBlogs || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteBlogs) {
                    $this->initSiteBlogs();
                } else {
                    $collectionClassName = SiteBlogTableMap::getTableMap()->getCollectionClassName();

                    $collSiteBlogs = new $collectionClassName;
                    $collSiteBlogs->setModel('\Model\Cms\SiteBlog');

                    return $collSiteBlogs;
                }
            } else {
                $collSiteBlogs = ChildSiteBlogQuery::create(null, $criteria)
                    ->filterBySite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteBlogsPartial && count($collSiteBlogs)) {
                        $this->initSiteBlogs(false);

                        foreach ($collSiteBlogs as $obj) {
                            if (false == $this->collSiteBlogs->contains($obj)) {
                                $this->collSiteBlogs->append($obj);
                            }
                        }

                        $this->collSiteBlogsPartial = true;
                    }

                    return $collSiteBlogs;
                }

                if ($partial && $this->collSiteBlogs) {
                    foreach ($this->collSiteBlogs as $obj) {
                        if ($obj->isNew()) {
                            $collSiteBlogs[] = $obj;
                        }
                    }
                }

                $this->collSiteBlogs = $collSiteBlogs;
                $this->collSiteBlogsPartial = false;
            }
        }

        return $this->collSiteBlogs;
    }

    /**
     * Sets a collection of ChildSiteBlog objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteBlogs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function setSiteBlogs(Collection $siteBlogs, ConnectionInterface $con = null)
    {
        /** @var ChildSiteBlog[] $siteBlogsToDelete */
        $siteBlogsToDelete = $this->getSiteBlogs(new Criteria(), $con)->diff($siteBlogs);


        $this->siteBlogsScheduledForDeletion = $siteBlogsToDelete;

        foreach ($siteBlogsToDelete as $siteBlogRemoved) {
            $siteBlogRemoved->setSite(null);
        }

        $this->collSiteBlogs = null;
        foreach ($siteBlogs as $siteBlog) {
            $this->addSiteBlog($siteBlog);
        }

        $this->collSiteBlogs = $siteBlogs;
        $this->collSiteBlogsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SiteBlog objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SiteBlog objects.
     * @throws PropelException
     */
    public function countSiteBlogs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteBlogsPartial && !$this->isNew();
        if (null === $this->collSiteBlogs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteBlogs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteBlogs());
            }

            $query = ChildSiteBlogQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySite($this)
                ->count($con);
        }

        return count($this->collSiteBlogs);
    }

    /**
     * Method called to associate a ChildSiteBlog object to this object
     * through the ChildSiteBlog foreign key attribute.
     *
     * @param  ChildSiteBlog $l ChildSiteBlog
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function addSiteBlog(ChildSiteBlog $l)
    {
        if ($this->collSiteBlogs === null) {
            $this->initSiteBlogs();
            $this->collSiteBlogsPartial = true;
        }

        if (!$this->collSiteBlogs->contains($l)) {
            $this->doAddSiteBlog($l);

            if ($this->siteBlogsScheduledForDeletion and $this->siteBlogsScheduledForDeletion->contains($l)) {
                $this->siteBlogsScheduledForDeletion->remove($this->siteBlogsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSiteBlog $siteBlog The ChildSiteBlog object to add.
     */
    protected function doAddSiteBlog(ChildSiteBlog $siteBlog)
    {
        $this->collSiteBlogs[]= $siteBlog;
        $siteBlog->setSite($this);
    }

    /**
     * @param  ChildSiteBlog $siteBlog The ChildSiteBlog object to remove.
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function removeSiteBlog(ChildSiteBlog $siteBlog)
    {
        if ($this->getSiteBlogs()->contains($siteBlog)) {
            $pos = $this->collSiteBlogs->search($siteBlog);
            $this->collSiteBlogs->remove($pos);
            if (null === $this->siteBlogsScheduledForDeletion) {
                $this->siteBlogsScheduledForDeletion = clone $this->collSiteBlogs;
                $this->siteBlogsScheduledForDeletion->clear();
            }
            $this->siteBlogsScheduledForDeletion[]= clone $siteBlog;
            $siteBlog->setSite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Site is new, it will return
     * an empty collection; or if this Site has previously
     * been saved, it will retrieve related SiteBlogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Site.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSiteBlog[] List of ChildSiteBlog objects
     */
    public function getSiteBlogsJoinLanguage(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSiteBlogQuery::create(null, $criteria);
        $query->joinWith('Language', $joinBehavior);

        return $this->getSiteBlogs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Site is new, it will return
     * an empty collection; or if this Site has previously
     * been saved, it will retrieve related SiteBlogs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Site.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSiteBlog[] List of ChildSiteBlog objects
     */
    public function getSiteBlogsJoinUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSiteBlogQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getSiteBlogs($query, $con);
    }

    /**
     * Clears out the collSiteBanners collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteBanners()
     */
    public function clearSiteBanners()
    {
        $this->collSiteBanners = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteBanners collection loaded partially.
     */
    public function resetPartialSiteBanners($v = true)
    {
        $this->collSiteBannersPartial = $v;
    }

    /**
     * Initializes the collSiteBanners collection.
     *
     * By default this just sets the collSiteBanners collection to an empty array (like clearcollSiteBanners());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteBanners($overrideExisting = true)
    {
        if (null !== $this->collSiteBanners && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteBannerTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteBanners = new $collectionClassName;
        $this->collSiteBanners->setModel('\Model\Cms\SiteBanner');
    }

    /**
     * Gets an array of ChildSiteBanner objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSiteBanner[] List of ChildSiteBanner objects
     * @throws PropelException
     */
    public function getSiteBanners(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteBannersPartial && !$this->isNew();
        if (null === $this->collSiteBanners || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteBanners) {
                    $this->initSiteBanners();
                } else {
                    $collectionClassName = SiteBannerTableMap::getTableMap()->getCollectionClassName();

                    $collSiteBanners = new $collectionClassName;
                    $collSiteBanners->setModel('\Model\Cms\SiteBanner');

                    return $collSiteBanners;
                }
            } else {
                $collSiteBanners = ChildSiteBannerQuery::create(null, $criteria)
                    ->filterBySite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteBannersPartial && count($collSiteBanners)) {
                        $this->initSiteBanners(false);

                        foreach ($collSiteBanners as $obj) {
                            if (false == $this->collSiteBanners->contains($obj)) {
                                $this->collSiteBanners->append($obj);
                            }
                        }

                        $this->collSiteBannersPartial = true;
                    }

                    return $collSiteBanners;
                }

                if ($partial && $this->collSiteBanners) {
                    foreach ($this->collSiteBanners as $obj) {
                        if ($obj->isNew()) {
                            $collSiteBanners[] = $obj;
                        }
                    }
                }

                $this->collSiteBanners = $collSiteBanners;
                $this->collSiteBannersPartial = false;
            }
        }

        return $this->collSiteBanners;
    }

    /**
     * Sets a collection of ChildSiteBanner objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteBanners A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function setSiteBanners(Collection $siteBanners, ConnectionInterface $con = null)
    {
        /** @var ChildSiteBanner[] $siteBannersToDelete */
        $siteBannersToDelete = $this->getSiteBanners(new Criteria(), $con)->diff($siteBanners);


        $this->siteBannersScheduledForDeletion = $siteBannersToDelete;

        foreach ($siteBannersToDelete as $siteBannerRemoved) {
            $siteBannerRemoved->setSite(null);
        }

        $this->collSiteBanners = null;
        foreach ($siteBanners as $siteBanner) {
            $this->addSiteBanner($siteBanner);
        }

        $this->collSiteBanners = $siteBanners;
        $this->collSiteBannersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SiteBanner objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SiteBanner objects.
     * @throws PropelException
     */
    public function countSiteBanners(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteBannersPartial && !$this->isNew();
        if (null === $this->collSiteBanners || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteBanners) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteBanners());
            }

            $query = ChildSiteBannerQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySite($this)
                ->count($con);
        }

        return count($this->collSiteBanners);
    }

    /**
     * Method called to associate a ChildSiteBanner object to this object
     * through the ChildSiteBanner foreign key attribute.
     *
     * @param  ChildSiteBanner $l ChildSiteBanner
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function addSiteBanner(ChildSiteBanner $l)
    {
        if ($this->collSiteBanners === null) {
            $this->initSiteBanners();
            $this->collSiteBannersPartial = true;
        }

        if (!$this->collSiteBanners->contains($l)) {
            $this->doAddSiteBanner($l);

            if ($this->siteBannersScheduledForDeletion and $this->siteBannersScheduledForDeletion->contains($l)) {
                $this->siteBannersScheduledForDeletion->remove($this->siteBannersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSiteBanner $siteBanner The ChildSiteBanner object to add.
     */
    protected function doAddSiteBanner(ChildSiteBanner $siteBanner)
    {
        $this->collSiteBanners[]= $siteBanner;
        $siteBanner->setSite($this);
    }

    /**
     * @param  ChildSiteBanner $siteBanner The ChildSiteBanner object to remove.
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function removeSiteBanner(ChildSiteBanner $siteBanner)
    {
        if ($this->getSiteBanners()->contains($siteBanner)) {
            $pos = $this->collSiteBanners->search($siteBanner);
            $this->collSiteBanners->remove($pos);
            if (null === $this->siteBannersScheduledForDeletion) {
                $this->siteBannersScheduledForDeletion = clone $this->collSiteBanners;
                $this->siteBannersScheduledForDeletion->clear();
            }
            $this->siteBannersScheduledForDeletion[]= clone $siteBanner;
            $siteBanner->setSite(null);
        }

        return $this;
    }

    /**
     * Clears out the collSiteFAQCategories collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteFAQCategories()
     */
    public function clearSiteFAQCategories()
    {
        $this->collSiteFAQCategories = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteFAQCategories collection loaded partially.
     */
    public function resetPartialSiteFAQCategories($v = true)
    {
        $this->collSiteFAQCategoriesPartial = $v;
    }

    /**
     * Initializes the collSiteFAQCategories collection.
     *
     * By default this just sets the collSiteFAQCategories collection to an empty array (like clearcollSiteFAQCategories());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteFAQCategories($overrideExisting = true)
    {
        if (null !== $this->collSiteFAQCategories && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteFAQCategoryTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteFAQCategories = new $collectionClassName;
        $this->collSiteFAQCategories->setModel('\Model\Cms\SiteFAQCategory');
    }

    /**
     * Gets an array of ChildSiteFAQCategory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSiteFAQCategory[] List of ChildSiteFAQCategory objects
     * @throws PropelException
     */
    public function getSiteFAQCategories(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFAQCategoriesPartial && !$this->isNew();
        if (null === $this->collSiteFAQCategories || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteFAQCategories) {
                    $this->initSiteFAQCategories();
                } else {
                    $collectionClassName = SiteFAQCategoryTableMap::getTableMap()->getCollectionClassName();

                    $collSiteFAQCategories = new $collectionClassName;
                    $collSiteFAQCategories->setModel('\Model\Cms\SiteFAQCategory');

                    return $collSiteFAQCategories;
                }
            } else {
                $collSiteFAQCategories = ChildSiteFAQCategoryQuery::create(null, $criteria)
                    ->filterBySite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteFAQCategoriesPartial && count($collSiteFAQCategories)) {
                        $this->initSiteFAQCategories(false);

                        foreach ($collSiteFAQCategories as $obj) {
                            if (false == $this->collSiteFAQCategories->contains($obj)) {
                                $this->collSiteFAQCategories->append($obj);
                            }
                        }

                        $this->collSiteFAQCategoriesPartial = true;
                    }

                    return $collSiteFAQCategories;
                }

                if ($partial && $this->collSiteFAQCategories) {
                    foreach ($this->collSiteFAQCategories as $obj) {
                        if ($obj->isNew()) {
                            $collSiteFAQCategories[] = $obj;
                        }
                    }
                }

                $this->collSiteFAQCategories = $collSiteFAQCategories;
                $this->collSiteFAQCategoriesPartial = false;
            }
        }

        return $this->collSiteFAQCategories;
    }

    /**
     * Sets a collection of ChildSiteFAQCategory objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteFAQCategories A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function setSiteFAQCategories(Collection $siteFAQCategories, ConnectionInterface $con = null)
    {
        /** @var ChildSiteFAQCategory[] $siteFAQCategoriesToDelete */
        $siteFAQCategoriesToDelete = $this->getSiteFAQCategories(new Criteria(), $con)->diff($siteFAQCategories);


        $this->siteFAQCategoriesScheduledForDeletion = $siteFAQCategoriesToDelete;

        foreach ($siteFAQCategoriesToDelete as $siteFAQCategoryRemoved) {
            $siteFAQCategoryRemoved->setSite(null);
        }

        $this->collSiteFAQCategories = null;
        foreach ($siteFAQCategories as $siteFAQCategory) {
            $this->addSiteFAQCategory($siteFAQCategory);
        }

        $this->collSiteFAQCategories = $siteFAQCategories;
        $this->collSiteFAQCategoriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SiteFAQCategory objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SiteFAQCategory objects.
     * @throws PropelException
     */
    public function countSiteFAQCategories(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFAQCategoriesPartial && !$this->isNew();
        if (null === $this->collSiteFAQCategories || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteFAQCategories) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteFAQCategories());
            }

            $query = ChildSiteFAQCategoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySite($this)
                ->count($con);
        }

        return count($this->collSiteFAQCategories);
    }

    /**
     * Method called to associate a ChildSiteFAQCategory object to this object
     * through the ChildSiteFAQCategory foreign key attribute.
     *
     * @param  ChildSiteFAQCategory $l ChildSiteFAQCategory
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function addSiteFAQCategory(ChildSiteFAQCategory $l)
    {
        if ($this->collSiteFAQCategories === null) {
            $this->initSiteFAQCategories();
            $this->collSiteFAQCategoriesPartial = true;
        }

        if (!$this->collSiteFAQCategories->contains($l)) {
            $this->doAddSiteFAQCategory($l);

            if ($this->siteFAQCategoriesScheduledForDeletion and $this->siteFAQCategoriesScheduledForDeletion->contains($l)) {
                $this->siteFAQCategoriesScheduledForDeletion->remove($this->siteFAQCategoriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSiteFAQCategory $siteFAQCategory The ChildSiteFAQCategory object to add.
     */
    protected function doAddSiteFAQCategory(ChildSiteFAQCategory $siteFAQCategory)
    {
        $this->collSiteFAQCategories[]= $siteFAQCategory;
        $siteFAQCategory->setSite($this);
    }

    /**
     * @param  ChildSiteFAQCategory $siteFAQCategory The ChildSiteFAQCategory object to remove.
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function removeSiteFAQCategory(ChildSiteFAQCategory $siteFAQCategory)
    {
        if ($this->getSiteFAQCategories()->contains($siteFAQCategory)) {
            $pos = $this->collSiteFAQCategories->search($siteFAQCategory);
            $this->collSiteFAQCategories->remove($pos);
            if (null === $this->siteFAQCategoriesScheduledForDeletion) {
                $this->siteFAQCategoriesScheduledForDeletion = clone $this->collSiteFAQCategories;
                $this->siteFAQCategoriesScheduledForDeletion->clear();
            }
            $this->siteFAQCategoriesScheduledForDeletion[]= clone $siteFAQCategory;
            $siteFAQCategory->setSite(null);
        }

        return $this;
    }

    /**
     * Clears out the collSiteFAQs collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSiteFAQs()
     */
    public function clearSiteFAQs()
    {
        $this->collSiteFAQs = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSiteFAQs collection loaded partially.
     */
    public function resetPartialSiteFAQs($v = true)
    {
        $this->collSiteFAQsPartial = $v;
    }

    /**
     * Initializes the collSiteFAQs collection.
     *
     * By default this just sets the collSiteFAQs collection to an empty array (like clearcollSiteFAQs());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSiteFAQs($overrideExisting = true)
    {
        if (null !== $this->collSiteFAQs && !$overrideExisting) {
            return;
        }

        $collectionClassName = SiteFAQTableMap::getTableMap()->getCollectionClassName();

        $this->collSiteFAQs = new $collectionClassName;
        $this->collSiteFAQs->setModel('\Model\Cms\SiteFAQ');
    }

    /**
     * Gets an array of ChildSiteFAQ objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSiteFAQ[] List of ChildSiteFAQ objects
     * @throws PropelException
     */
    public function getSiteFAQs(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFAQsPartial && !$this->isNew();
        if (null === $this->collSiteFAQs || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSiteFAQs) {
                    $this->initSiteFAQs();
                } else {
                    $collectionClassName = SiteFAQTableMap::getTableMap()->getCollectionClassName();

                    $collSiteFAQs = new $collectionClassName;
                    $collSiteFAQs->setModel('\Model\Cms\SiteFAQ');

                    return $collSiteFAQs;
                }
            } else {
                $collSiteFAQs = ChildSiteFAQQuery::create(null, $criteria)
                    ->filterBySite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSiteFAQsPartial && count($collSiteFAQs)) {
                        $this->initSiteFAQs(false);

                        foreach ($collSiteFAQs as $obj) {
                            if (false == $this->collSiteFAQs->contains($obj)) {
                                $this->collSiteFAQs->append($obj);
                            }
                        }

                        $this->collSiteFAQsPartial = true;
                    }

                    return $collSiteFAQs;
                }

                if ($partial && $this->collSiteFAQs) {
                    foreach ($this->collSiteFAQs as $obj) {
                        if ($obj->isNew()) {
                            $collSiteFAQs[] = $obj;
                        }
                    }
                }

                $this->collSiteFAQs = $collSiteFAQs;
                $this->collSiteFAQsPartial = false;
            }
        }

        return $this->collSiteFAQs;
    }

    /**
     * Sets a collection of ChildSiteFAQ objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $siteFAQs A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function setSiteFAQs(Collection $siteFAQs, ConnectionInterface $con = null)
    {
        /** @var ChildSiteFAQ[] $siteFAQsToDelete */
        $siteFAQsToDelete = $this->getSiteFAQs(new Criteria(), $con)->diff($siteFAQs);


        $this->siteFAQsScheduledForDeletion = $siteFAQsToDelete;

        foreach ($siteFAQsToDelete as $siteFAQRemoved) {
            $siteFAQRemoved->setSite(null);
        }

        $this->collSiteFAQs = null;
        foreach ($siteFAQs as $siteFAQ) {
            $this->addSiteFAQ($siteFAQ);
        }

        $this->collSiteFAQs = $siteFAQs;
        $this->collSiteFAQsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SiteFAQ objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SiteFAQ objects.
     * @throws PropelException
     */
    public function countSiteFAQs(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSiteFAQsPartial && !$this->isNew();
        if (null === $this->collSiteFAQs || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSiteFAQs) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSiteFAQs());
            }

            $query = ChildSiteFAQQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySite($this)
                ->count($con);
        }

        return count($this->collSiteFAQs);
    }

    /**
     * Method called to associate a ChildSiteFAQ object to this object
     * through the ChildSiteFAQ foreign key attribute.
     *
     * @param  ChildSiteFAQ $l ChildSiteFAQ
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function addSiteFAQ(ChildSiteFAQ $l)
    {
        if ($this->collSiteFAQs === null) {
            $this->initSiteFAQs();
            $this->collSiteFAQsPartial = true;
        }

        if (!$this->collSiteFAQs->contains($l)) {
            $this->doAddSiteFAQ($l);

            if ($this->siteFAQsScheduledForDeletion and $this->siteFAQsScheduledForDeletion->contains($l)) {
                $this->siteFAQsScheduledForDeletion->remove($this->siteFAQsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSiteFAQ $siteFAQ The ChildSiteFAQ object to add.
     */
    protected function doAddSiteFAQ(ChildSiteFAQ $siteFAQ)
    {
        $this->collSiteFAQs[]= $siteFAQ;
        $siteFAQ->setSite($this);
    }

    /**
     * @param  ChildSiteFAQ $siteFAQ The ChildSiteFAQ object to remove.
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function removeSiteFAQ(ChildSiteFAQ $siteFAQ)
    {
        if ($this->getSiteFAQs()->contains($siteFAQ)) {
            $pos = $this->collSiteFAQs->search($siteFAQ);
            $this->collSiteFAQs->remove($pos);
            if (null === $this->siteFAQsScheduledForDeletion) {
                $this->siteFAQsScheduledForDeletion = clone $this->collSiteFAQs;
                $this->siteFAQsScheduledForDeletion->clear();
            }
            $this->siteFAQsScheduledForDeletion[]= clone $siteFAQ;
            $siteFAQ->setSite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Site is new, it will return
     * an empty collection; or if this Site has previously
     * been saved, it will retrieve related SiteFAQs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Site.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSiteFAQ[] List of ChildSiteFAQ objects
     */
    public function getSiteFAQsJoinLanguage(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSiteFAQQuery::create(null, $criteria);
        $query->joinWith('Language', $joinBehavior);

        return $this->getSiteFAQs($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Site is new, it will return
     * an empty collection; or if this Site has previously
     * been saved, it will retrieve related SiteFAQs from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Site.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSiteFAQ[] List of ChildSiteFAQ objects
     */
    public function getSiteFAQsJoinFaqCategory(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSiteFAQQuery::create(null, $criteria);
        $query->joinWith('FaqCategory', $joinBehavior);

        return $this->getSiteFAQs($query, $con);
    }

    /**
     * Clears out the collSitePages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSitePages()
     */
    public function clearSitePages()
    {
        $this->collSitePages = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSitePages collection loaded partially.
     */
    public function resetPartialSitePages($v = true)
    {
        $this->collSitePagesPartial = $v;
    }

    /**
     * Initializes the collSitePages collection.
     *
     * By default this just sets the collSitePages collection to an empty array (like clearcollSitePages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSitePages($overrideExisting = true)
    {
        if (null !== $this->collSitePages && !$overrideExisting) {
            return;
        }

        $collectionClassName = SitePageTableMap::getTableMap()->getCollectionClassName();

        $this->collSitePages = new $collectionClassName;
        $this->collSitePages->setModel('\Model\Cms\SitePage');
    }

    /**
     * Gets an array of ChildSitePage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildSitePage[] List of ChildSitePage objects
     * @throws PropelException
     */
    public function getSitePages(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSitePagesPartial && !$this->isNew();
        if (null === $this->collSitePages || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSitePages) {
                    $this->initSitePages();
                } else {
                    $collectionClassName = SitePageTableMap::getTableMap()->getCollectionClassName();

                    $collSitePages = new $collectionClassName;
                    $collSitePages->setModel('\Model\Cms\SitePage');

                    return $collSitePages;
                }
            } else {
                $collSitePages = ChildSitePageQuery::create(null, $criteria)
                    ->filterBySite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSitePagesPartial && count($collSitePages)) {
                        $this->initSitePages(false);

                        foreach ($collSitePages as $obj) {
                            if (false == $this->collSitePages->contains($obj)) {
                                $this->collSitePages->append($obj);
                            }
                        }

                        $this->collSitePagesPartial = true;
                    }

                    return $collSitePages;
                }

                if ($partial && $this->collSitePages) {
                    foreach ($this->collSitePages as $obj) {
                        if ($obj->isNew()) {
                            $collSitePages[] = $obj;
                        }
                    }
                }

                $this->collSitePages = $collSitePages;
                $this->collSitePagesPartial = false;
            }
        }

        return $this->collSitePages;
    }

    /**
     * Sets a collection of ChildSitePage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $sitePages A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function setSitePages(Collection $sitePages, ConnectionInterface $con = null)
    {
        /** @var ChildSitePage[] $sitePagesToDelete */
        $sitePagesToDelete = $this->getSitePages(new Criteria(), $con)->diff($sitePages);


        $this->sitePagesScheduledForDeletion = $sitePagesToDelete;

        foreach ($sitePagesToDelete as $sitePageRemoved) {
            $sitePageRemoved->setSite(null);
        }

        $this->collSitePages = null;
        foreach ($sitePages as $sitePage) {
            $this->addSitePage($sitePage);
        }

        $this->collSitePages = $sitePages;
        $this->collSitePagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related SitePage objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related SitePage objects.
     * @throws PropelException
     */
    public function countSitePages(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSitePagesPartial && !$this->isNew();
        if (null === $this->collSitePages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSitePages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSitePages());
            }

            $query = ChildSitePageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySite($this)
                ->count($con);
        }

        return count($this->collSitePages);
    }

    /**
     * Method called to associate a ChildSitePage object to this object
     * through the ChildSitePage foreign key attribute.
     *
     * @param  ChildSitePage $l ChildSitePage
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function addSitePage(ChildSitePage $l)
    {
        if ($this->collSitePages === null) {
            $this->initSitePages();
            $this->collSitePagesPartial = true;
        }

        if (!$this->collSitePages->contains($l)) {
            $this->doAddSitePage($l);

            if ($this->sitePagesScheduledForDeletion and $this->sitePagesScheduledForDeletion->contains($l)) {
                $this->sitePagesScheduledForDeletion->remove($this->sitePagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildSitePage $sitePage The ChildSitePage object to add.
     */
    protected function doAddSitePage(ChildSitePage $sitePage)
    {
        $this->collSitePages[]= $sitePage;
        $sitePage->setSite($this);
    }

    /**
     * @param  ChildSitePage $sitePage The ChildSitePage object to remove.
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function removeSitePage(ChildSitePage $sitePage)
    {
        if ($this->getSitePages()->contains($sitePage)) {
            $pos = $this->collSitePages->search($sitePage);
            $this->collSitePages->remove($pos);
            if (null === $this->sitePagesScheduledForDeletion) {
                $this->sitePagesScheduledForDeletion = clone $this->collSitePages;
                $this->sitePagesScheduledForDeletion->clear();
            }
            $this->sitePagesScheduledForDeletion[]= clone $sitePage;
            $sitePage->setSite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Site is new, it will return
     * an empty collection; or if this Site has previously
     * been saved, it will retrieve related SitePages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Site.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSitePage[] List of ChildSitePage objects
     */
    public function getSitePagesJoinSiteNavigaton(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSitePageQuery::create(null, $criteria);
        $query->joinWith('SiteNavigaton', $joinBehavior);

        return $this->getSitePages($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Site is new, it will return
     * an empty collection; or if this Site has previously
     * been saved, it will retrieve related SitePages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Site.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildSitePage[] List of ChildSitePage objects
     */
    public function getSitePagesJoinLanguage(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildSitePageQuery::create(null, $criteria);
        $query->joinWith('Language', $joinBehavior);

        return $this->getSitePages($query, $con);
    }

    /**
     * Clears out the collMailTemplates collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMailTemplates()
     */
    public function clearMailTemplates()
    {
        $this->collMailTemplates = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMailTemplates collection loaded partially.
     */
    public function resetPartialMailTemplates($v = true)
    {
        $this->collMailTemplatesPartial = $v;
    }

    /**
     * Initializes the collMailTemplates collection.
     *
     * By default this just sets the collMailTemplates collection to an empty array (like clearcollMailTemplates());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMailTemplates($overrideExisting = true)
    {
        if (null !== $this->collMailTemplates && !$overrideExisting) {
            return;
        }

        $collectionClassName = MailTemplateTableMap::getTableMap()->getCollectionClassName();

        $this->collMailTemplates = new $collectionClassName;
        $this->collMailTemplates->setModel('\Model\Cms\Mail\MailTemplate');
    }

    /**
     * Gets an array of MailTemplate objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildSite is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|MailTemplate[] List of MailTemplate objects
     * @throws PropelException
     */
    public function getMailTemplates(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMailTemplatesPartial && !$this->isNew();
        if (null === $this->collMailTemplates || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collMailTemplates) {
                    $this->initMailTemplates();
                } else {
                    $collectionClassName = MailTemplateTableMap::getTableMap()->getCollectionClassName();

                    $collMailTemplates = new $collectionClassName;
                    $collMailTemplates->setModel('\Model\Cms\Mail\MailTemplate');

                    return $collMailTemplates;
                }
            } else {
                $collMailTemplates = MailTemplateQuery::create(null, $criteria)
                    ->filterBySite($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMailTemplatesPartial && count($collMailTemplates)) {
                        $this->initMailTemplates(false);

                        foreach ($collMailTemplates as $obj) {
                            if (false == $this->collMailTemplates->contains($obj)) {
                                $this->collMailTemplates->append($obj);
                            }
                        }

                        $this->collMailTemplatesPartial = true;
                    }

                    return $collMailTemplates;
                }

                if ($partial && $this->collMailTemplates) {
                    foreach ($this->collMailTemplates as $obj) {
                        if ($obj->isNew()) {
                            $collMailTemplates[] = $obj;
                        }
                    }
                }

                $this->collMailTemplates = $collMailTemplates;
                $this->collMailTemplatesPartial = false;
            }
        }

        return $this->collMailTemplates;
    }

    /**
     * Sets a collection of MailTemplate objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $mailTemplates A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function setMailTemplates(Collection $mailTemplates, ConnectionInterface $con = null)
    {
        /** @var MailTemplate[] $mailTemplatesToDelete */
        $mailTemplatesToDelete = $this->getMailTemplates(new Criteria(), $con)->diff($mailTemplates);


        $this->mailTemplatesScheduledForDeletion = $mailTemplatesToDelete;

        foreach ($mailTemplatesToDelete as $mailTemplateRemoved) {
            $mailTemplateRemoved->setSite(null);
        }

        $this->collMailTemplates = null;
        foreach ($mailTemplates as $mailTemplate) {
            $this->addMailTemplate($mailTemplate);
        }

        $this->collMailTemplates = $mailTemplates;
        $this->collMailTemplatesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseMailTemplate objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseMailTemplate objects.
     * @throws PropelException
     */
    public function countMailTemplates(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMailTemplatesPartial && !$this->isNew();
        if (null === $this->collMailTemplates || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMailTemplates) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMailTemplates());
            }

            $query = MailTemplateQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySite($this)
                ->count($con);
        }

        return count($this->collMailTemplates);
    }

    /**
     * Method called to associate a MailTemplate object to this object
     * through the MailTemplate foreign key attribute.
     *
     * @param  MailTemplate $l MailTemplate
     * @return $this|\Model\Cms\Site The current object (for fluent API support)
     */
    public function addMailTemplate(MailTemplate $l)
    {
        if ($this->collMailTemplates === null) {
            $this->initMailTemplates();
            $this->collMailTemplatesPartial = true;
        }

        if (!$this->collMailTemplates->contains($l)) {
            $this->doAddMailTemplate($l);

            if ($this->mailTemplatesScheduledForDeletion and $this->mailTemplatesScheduledForDeletion->contains($l)) {
                $this->mailTemplatesScheduledForDeletion->remove($this->mailTemplatesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param MailTemplate $mailTemplate The MailTemplate object to add.
     */
    protected function doAddMailTemplate(MailTemplate $mailTemplate)
    {
        $this->collMailTemplates[]= $mailTemplate;
        $mailTemplate->setSite($this);
    }

    /**
     * @param  MailTemplate $mailTemplate The MailTemplate object to remove.
     * @return $this|ChildSite The current object (for fluent API support)
     */
    public function removeMailTemplate(MailTemplate $mailTemplate)
    {
        if ($this->getMailTemplates()->contains($mailTemplate)) {
            $pos = $this->collMailTemplates->search($mailTemplate);
            $this->collMailTemplates->remove($pos);
            if (null === $this->mailTemplatesScheduledForDeletion) {
                $this->mailTemplatesScheduledForDeletion = clone $this->collMailTemplates;
                $this->mailTemplatesScheduledForDeletion->clear();
            }
            $this->mailTemplatesScheduledForDeletion[]= clone $mailTemplate;
            $mailTemplate->setSite(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Site is new, it will return
     * an empty collection; or if this Site has previously
     * been saved, it will retrieve related MailTemplates from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Site.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|MailTemplate[] List of MailTemplate objects
     */
    public function getMailTemplatesJoinMailType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = MailTemplateQuery::create(null, $criteria);
        $query->joinWith('MailType', $joinBehavior);

        return $this->getMailTemplates($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aOwnCompany) {
            $this->aOwnCompany->removeSite($this);
        }
        $this->id = null;
        $this->domain = null;
        $this->custom_folder = null;
        $this->from_email = null;
        $this->contact_form_to_email = null;
        $this->num_footer_blocks = null;
        $this->general_phone = null;
        $this->invoicing_company_id = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collCustomMedias) {
                foreach ($this->collCustomMedias as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPromoProducts) {
                foreach ($this->collPromoProducts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaleOrders) {
                foreach ($this->collSaleOrders as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteUsps) {
                foreach ($this->collSiteUsps as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteFooterBlocks) {
                foreach ($this->collSiteFooterBlocks as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteBlogs) {
                foreach ($this->collSiteBlogs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteBanners) {
                foreach ($this->collSiteBanners as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteFAQCategories) {
                foreach ($this->collSiteFAQCategories as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSiteFAQs) {
                foreach ($this->collSiteFAQs as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSitePages) {
                foreach ($this->collSitePages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMailTemplates) {
                foreach ($this->collMailTemplates as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collCustomMedias = null;
        $this->collPromoProducts = null;
        $this->collSaleOrders = null;
        $this->collSiteUsps = null;
        $this->collSiteFooterBlocks = null;
        $this->collSiteBlogs = null;
        $this->collSiteBanners = null;
        $this->collSiteFAQCategories = null;
        $this->collSiteFAQs = null;
        $this->collSitePages = null;
        $this->collMailTemplates = null;
        $this->aOwnCompany = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SiteTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
