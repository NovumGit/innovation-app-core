<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\Cms\SiteFAQCategory as ChildSiteFAQCategory;
use Model\Cms\SiteFAQCategoryQuery as ChildSiteFAQCategoryQuery;
use Model\Cms\Map\SiteFAQCategoryTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'site_faq_category' table.
 *
 *
 *
 * @method     ChildSiteFAQCategoryQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSiteFAQCategoryQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildSiteFAQCategoryQuery orderBySiteId($order = Criteria::ASC) Order by the site_id column
 * @method     ChildSiteFAQCategoryQuery orderByHasImage($order = Criteria::ASC) Order by the has_image column
 * @method     ChildSiteFAQCategoryQuery orderByImageExt($order = Criteria::ASC) Order by the image_ext column
 *
 * @method     ChildSiteFAQCategoryQuery groupById() Group by the id column
 * @method     ChildSiteFAQCategoryQuery groupByName() Group by the name column
 * @method     ChildSiteFAQCategoryQuery groupBySiteId() Group by the site_id column
 * @method     ChildSiteFAQCategoryQuery groupByHasImage() Group by the has_image column
 * @method     ChildSiteFAQCategoryQuery groupByImageExt() Group by the image_ext column
 *
 * @method     ChildSiteFAQCategoryQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteFAQCategoryQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteFAQCategoryQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteFAQCategoryQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteFAQCategoryQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteFAQCategoryQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteFAQCategoryQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildSiteFAQCategoryQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildSiteFAQCategoryQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildSiteFAQCategoryQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildSiteFAQCategoryQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildSiteFAQCategoryQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildSiteFAQCategoryQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     ChildSiteFAQCategoryQuery leftJoinSiteFAQCategoryTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteFAQCategoryTranslation relation
 * @method     ChildSiteFAQCategoryQuery rightJoinSiteFAQCategoryTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteFAQCategoryTranslation relation
 * @method     ChildSiteFAQCategoryQuery innerJoinSiteFAQCategoryTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteFAQCategoryTranslation relation
 *
 * @method     ChildSiteFAQCategoryQuery joinWithSiteFAQCategoryTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteFAQCategoryTranslation relation
 *
 * @method     ChildSiteFAQCategoryQuery leftJoinWithSiteFAQCategoryTranslation() Adds a LEFT JOIN clause and with to the query using the SiteFAQCategoryTranslation relation
 * @method     ChildSiteFAQCategoryQuery rightJoinWithSiteFAQCategoryTranslation() Adds a RIGHT JOIN clause and with to the query using the SiteFAQCategoryTranslation relation
 * @method     ChildSiteFAQCategoryQuery innerJoinWithSiteFAQCategoryTranslation() Adds a INNER JOIN clause and with to the query using the SiteFAQCategoryTranslation relation
 *
 * @method     ChildSiteFAQCategoryQuery leftJoinSiteFAQ($relationAlias = null) Adds a LEFT JOIN clause to the query using the SiteFAQ relation
 * @method     ChildSiteFAQCategoryQuery rightJoinSiteFAQ($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SiteFAQ relation
 * @method     ChildSiteFAQCategoryQuery innerJoinSiteFAQ($relationAlias = null) Adds a INNER JOIN clause to the query using the SiteFAQ relation
 *
 * @method     ChildSiteFAQCategoryQuery joinWithSiteFAQ($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SiteFAQ relation
 *
 * @method     ChildSiteFAQCategoryQuery leftJoinWithSiteFAQ() Adds a LEFT JOIN clause and with to the query using the SiteFAQ relation
 * @method     ChildSiteFAQCategoryQuery rightJoinWithSiteFAQ() Adds a RIGHT JOIN clause and with to the query using the SiteFAQ relation
 * @method     ChildSiteFAQCategoryQuery innerJoinWithSiteFAQ() Adds a INNER JOIN clause and with to the query using the SiteFAQ relation
 *
 * @method     \Model\Cms\SiteQuery|\Model\Cms\SiteFAQCategoryTranslationQuery|\Model\Cms\SiteFAQQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSiteFAQCategory findOne(ConnectionInterface $con = null) Return the first ChildSiteFAQCategory matching the query
 * @method     ChildSiteFAQCategory findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSiteFAQCategory matching the query, or a new ChildSiteFAQCategory object populated from the query conditions when no match is found
 *
 * @method     ChildSiteFAQCategory findOneById(int $id) Return the first ChildSiteFAQCategory filtered by the id column
 * @method     ChildSiteFAQCategory findOneByName(string $name) Return the first ChildSiteFAQCategory filtered by the name column
 * @method     ChildSiteFAQCategory findOneBySiteId(int $site_id) Return the first ChildSiteFAQCategory filtered by the site_id column
 * @method     ChildSiteFAQCategory findOneByHasImage(boolean $has_image) Return the first ChildSiteFAQCategory filtered by the has_image column
 * @method     ChildSiteFAQCategory findOneByImageExt(string $image_ext) Return the first ChildSiteFAQCategory filtered by the image_ext column *

 * @method     ChildSiteFAQCategory requirePk($key, ConnectionInterface $con = null) Return the ChildSiteFAQCategory by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFAQCategory requireOne(ConnectionInterface $con = null) Return the first ChildSiteFAQCategory matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteFAQCategory requireOneById(int $id) Return the first ChildSiteFAQCategory filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFAQCategory requireOneByName(string $name) Return the first ChildSiteFAQCategory filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFAQCategory requireOneBySiteId(int $site_id) Return the first ChildSiteFAQCategory filtered by the site_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFAQCategory requireOneByHasImage(boolean $has_image) Return the first ChildSiteFAQCategory filtered by the has_image column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFAQCategory requireOneByImageExt(string $image_ext) Return the first ChildSiteFAQCategory filtered by the image_ext column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteFAQCategory[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSiteFAQCategory objects based on current ModelCriteria
 * @method     ChildSiteFAQCategory[]|ObjectCollection findById(int $id) Return ChildSiteFAQCategory objects filtered by the id column
 * @method     ChildSiteFAQCategory[]|ObjectCollection findByName(string $name) Return ChildSiteFAQCategory objects filtered by the name column
 * @method     ChildSiteFAQCategory[]|ObjectCollection findBySiteId(int $site_id) Return ChildSiteFAQCategory objects filtered by the site_id column
 * @method     ChildSiteFAQCategory[]|ObjectCollection findByHasImage(boolean $has_image) Return ChildSiteFAQCategory objects filtered by the has_image column
 * @method     ChildSiteFAQCategory[]|ObjectCollection findByImageExt(string $image_ext) Return ChildSiteFAQCategory objects filtered by the image_ext column
 * @method     ChildSiteFAQCategory[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteFAQCategoryQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Cms\Base\SiteFAQCategoryQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Cms\\SiteFAQCategory', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteFAQCategoryQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteFAQCategoryQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteFAQCategoryQuery) {
            return $criteria;
        }
        $query = new ChildSiteFAQCategoryQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSiteFAQCategory|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteFAQCategoryTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteFAQCategoryTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteFAQCategory A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, site_id, has_image, image_ext FROM site_faq_category WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSiteFAQCategory $obj */
            $obj = new ChildSiteFAQCategory();
            $obj->hydrate($row);
            SiteFAQCategoryTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSiteFAQCategory|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteFAQCategoryTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteFAQCategoryTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteFAQCategoryTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteFAQCategoryTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteFAQCategoryTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteFAQCategoryTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the site_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE site_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE site_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE site_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(SiteFAQCategoryTableMap::COL_SITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(SiteFAQCategoryTableMap::COL_SITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteFAQCategoryTableMap::COL_SITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the has_image column
     *
     * Example usage:
     * <code>
     * $query->filterByHasImage(true); // WHERE has_image = true
     * $query->filterByHasImage('yes'); // WHERE has_image = true
     * </code>
     *
     * @param     boolean|string $hasImage The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function filterByHasImage($hasImage = null, $comparison = null)
    {
        if (is_string($hasImage)) {
            $hasImage = in_array(strtolower($hasImage), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SiteFAQCategoryTableMap::COL_HAS_IMAGE, $hasImage, $comparison);
    }

    /**
     * Filter the query on the image_ext column
     *
     * Example usage:
     * <code>
     * $query->filterByImageExt('fooValue');   // WHERE image_ext = 'fooValue'
     * $query->filterByImageExt('%fooValue%', Criteria::LIKE); // WHERE image_ext LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imageExt The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function filterByImageExt($imageExt = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imageExt)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteFAQCategoryTableMap::COL_IMAGE_EXT, $imageExt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Cms\Site object
     *
     * @param \Model\Cms\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \Model\Cms\Site) {
            return $this
                ->addUsingAlias(SiteFAQCategoryTableMap::COL_SITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteFAQCategoryTableMap::COL_SITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \Model\Cms\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\Model\Cms\SiteQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteFAQCategoryTranslation object
     *
     * @param \Model\Cms\SiteFAQCategoryTranslation|ObjectCollection $siteFAQCategoryTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function filterBySiteFAQCategoryTranslation($siteFAQCategoryTranslation, $comparison = null)
    {
        if ($siteFAQCategoryTranslation instanceof \Model\Cms\SiteFAQCategoryTranslation) {
            return $this
                ->addUsingAlias(SiteFAQCategoryTableMap::COL_ID, $siteFAQCategoryTranslation->getSiteFaqCategoryId(), $comparison);
        } elseif ($siteFAQCategoryTranslation instanceof ObjectCollection) {
            return $this
                ->useSiteFAQCategoryTranslationQuery()
                ->filterByPrimaryKeys($siteFAQCategoryTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteFAQCategoryTranslation() only accepts arguments of type \Model\Cms\SiteFAQCategoryTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteFAQCategoryTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function joinSiteFAQCategoryTranslation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteFAQCategoryTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteFAQCategoryTranslation');
        }

        return $this;
    }

    /**
     * Use the SiteFAQCategoryTranslation relation SiteFAQCategoryTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteFAQCategoryTranslationQuery A secondary query class using the current class as primary query
     */
    public function useSiteFAQCategoryTranslationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSiteFAQCategoryTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteFAQCategoryTranslation', '\Model\Cms\SiteFAQCategoryTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteFAQ object
     *
     * @param \Model\Cms\SiteFAQ|ObjectCollection $siteFAQ the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function filterBySiteFAQ($siteFAQ, $comparison = null)
    {
        if ($siteFAQ instanceof \Model\Cms\SiteFAQ) {
            return $this
                ->addUsingAlias(SiteFAQCategoryTableMap::COL_ID, $siteFAQ->getSiteFaqCategoryId(), $comparison);
        } elseif ($siteFAQ instanceof ObjectCollection) {
            return $this
                ->useSiteFAQQuery()
                ->filterByPrimaryKeys($siteFAQ->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySiteFAQ() only accepts arguments of type \Model\Cms\SiteFAQ or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SiteFAQ relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function joinSiteFAQ($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SiteFAQ');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SiteFAQ');
        }

        return $this;
    }

    /**
     * Use the SiteFAQ relation SiteFAQ object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteFAQQuery A secondary query class using the current class as primary query
     */
    public function useSiteFAQQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSiteFAQ($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SiteFAQ', '\Model\Cms\SiteFAQQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSiteFAQCategory $siteFAQCategory Object to remove from the list of results
     *
     * @return $this|ChildSiteFAQCategoryQuery The current query, for fluid interface
     */
    public function prune($siteFAQCategory = null)
    {
        if ($siteFAQCategory) {
            $this->addUsingAlias(SiteFAQCategoryTableMap::COL_ID, $siteFAQCategory->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the site_faq_category table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteFAQCategoryTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteFAQCategoryTableMap::clearInstancePool();
            SiteFAQCategoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteFAQCategoryTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteFAQCategoryTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteFAQCategoryTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteFAQCategoryTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SiteFAQCategoryQuery
