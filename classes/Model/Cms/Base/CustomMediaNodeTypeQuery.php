<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\Cms\CustomMediaNodeType as ChildCustomMediaNodeType;
use Model\Cms\CustomMediaNodeTypeQuery as ChildCustomMediaNodeTypeQuery;
use Model\Cms\Map\CustomMediaNodeTypeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'custom_media_node_type' table.
 *
 *
 *
 * @method     ChildCustomMediaNodeTypeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildCustomMediaNodeTypeQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildCustomMediaNodeTypeQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 *
 * @method     ChildCustomMediaNodeTypeQuery groupById() Group by the id column
 * @method     ChildCustomMediaNodeTypeQuery groupByType() Group by the type column
 * @method     ChildCustomMediaNodeTypeQuery groupByCreatedDate() Group by the created_date column
 *
 * @method     ChildCustomMediaNodeTypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildCustomMediaNodeTypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildCustomMediaNodeTypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildCustomMediaNodeTypeQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildCustomMediaNodeTypeQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildCustomMediaNodeTypeQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildCustomMediaNodeTypeQuery leftJoinCustomMedia($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomMedia relation
 * @method     ChildCustomMediaNodeTypeQuery rightJoinCustomMedia($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomMedia relation
 * @method     ChildCustomMediaNodeTypeQuery innerJoinCustomMedia($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomMedia relation
 *
 * @method     ChildCustomMediaNodeTypeQuery joinWithCustomMedia($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomMedia relation
 *
 * @method     ChildCustomMediaNodeTypeQuery leftJoinWithCustomMedia() Adds a LEFT JOIN clause and with to the query using the CustomMedia relation
 * @method     ChildCustomMediaNodeTypeQuery rightJoinWithCustomMedia() Adds a RIGHT JOIN clause and with to the query using the CustomMedia relation
 * @method     ChildCustomMediaNodeTypeQuery innerJoinWithCustomMedia() Adds a INNER JOIN clause and with to the query using the CustomMedia relation
 *
 * @method     \Model\Cms\CustomMediaQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildCustomMediaNodeType findOne(ConnectionInterface $con = null) Return the first ChildCustomMediaNodeType matching the query
 * @method     ChildCustomMediaNodeType findOneOrCreate(ConnectionInterface $con = null) Return the first ChildCustomMediaNodeType matching the query, or a new ChildCustomMediaNodeType object populated from the query conditions when no match is found
 *
 * @method     ChildCustomMediaNodeType findOneById(int $id) Return the first ChildCustomMediaNodeType filtered by the id column
 * @method     ChildCustomMediaNodeType findOneByType(string $type) Return the first ChildCustomMediaNodeType filtered by the type column
 * @method     ChildCustomMediaNodeType findOneByCreatedDate(string $created_date) Return the first ChildCustomMediaNodeType filtered by the created_date column *

 * @method     ChildCustomMediaNodeType requirePk($key, ConnectionInterface $con = null) Return the ChildCustomMediaNodeType by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomMediaNodeType requireOne(ConnectionInterface $con = null) Return the first ChildCustomMediaNodeType matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomMediaNodeType requireOneById(int $id) Return the first ChildCustomMediaNodeType filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomMediaNodeType requireOneByType(string $type) Return the first ChildCustomMediaNodeType filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildCustomMediaNodeType requireOneByCreatedDate(string $created_date) Return the first ChildCustomMediaNodeType filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildCustomMediaNodeType[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildCustomMediaNodeType objects based on current ModelCriteria
 * @method     ChildCustomMediaNodeType[]|ObjectCollection findById(int $id) Return ChildCustomMediaNodeType objects filtered by the id column
 * @method     ChildCustomMediaNodeType[]|ObjectCollection findByType(string $type) Return ChildCustomMediaNodeType objects filtered by the type column
 * @method     ChildCustomMediaNodeType[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildCustomMediaNodeType objects filtered by the created_date column
 * @method     ChildCustomMediaNodeType[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class CustomMediaNodeTypeQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Cms\Base\CustomMediaNodeTypeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Cms\\CustomMediaNodeType', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildCustomMediaNodeTypeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildCustomMediaNodeTypeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildCustomMediaNodeTypeQuery) {
            return $criteria;
        }
        $query = new ChildCustomMediaNodeTypeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildCustomMediaNodeType|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(CustomMediaNodeTypeTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = CustomMediaNodeTypeTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildCustomMediaNodeType A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, type, created_date FROM custom_media_node_type WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildCustomMediaNodeType $obj */
            $obj = new ChildCustomMediaNodeType();
            $obj->hydrate($row);
            CustomMediaNodeTypeTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildCustomMediaNodeType|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildCustomMediaNodeTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(CustomMediaNodeTypeTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildCustomMediaNodeTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(CustomMediaNodeTypeTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomMediaNodeTypeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(CustomMediaNodeTypeTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(CustomMediaNodeTypeTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomMediaNodeTypeTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%', Criteria::LIKE); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomMediaNodeTypeQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomMediaNodeTypeTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildCustomMediaNodeTypeQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(CustomMediaNodeTypeTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(CustomMediaNodeTypeTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(CustomMediaNodeTypeTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query by a related \Model\Cms\CustomMedia object
     *
     * @param \Model\Cms\CustomMedia|ObjectCollection $customMedia the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildCustomMediaNodeTypeQuery The current query, for fluid interface
     */
    public function filterByCustomMedia($customMedia, $comparison = null)
    {
        if ($customMedia instanceof \Model\Cms\CustomMedia) {
            return $this
                ->addUsingAlias(CustomMediaNodeTypeTableMap::COL_ID, $customMedia->getNodeTypeId(), $comparison);
        } elseif ($customMedia instanceof ObjectCollection) {
            return $this
                ->useCustomMediaQuery()
                ->filterByPrimaryKeys($customMedia->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomMedia() only accepts arguments of type \Model\Cms\CustomMedia or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomMedia relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildCustomMediaNodeTypeQuery The current query, for fluid interface
     */
    public function joinCustomMedia($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomMedia');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomMedia');
        }

        return $this;
    }

    /**
     * Use the CustomMedia relation CustomMedia object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\CustomMediaQuery A secondary query class using the current class as primary query
     */
    public function useCustomMediaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCustomMedia($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomMedia', '\Model\Cms\CustomMediaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildCustomMediaNodeType $customMediaNodeType Object to remove from the list of results
     *
     * @return $this|ChildCustomMediaNodeTypeQuery The current query, for fluid interface
     */
    public function prune($customMediaNodeType = null)
    {
        if ($customMediaNodeType) {
            $this->addUsingAlias(CustomMediaNodeTypeTableMap::COL_ID, $customMediaNodeType->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the custom_media_node_type table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomMediaNodeTypeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            CustomMediaNodeTypeTableMap::clearInstancePool();
            CustomMediaNodeTypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(CustomMediaNodeTypeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(CustomMediaNodeTypeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            CustomMediaNodeTypeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            CustomMediaNodeTypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // CustomMediaNodeTypeQuery
