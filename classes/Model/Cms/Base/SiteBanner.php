<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\Cms\Site as ChildSite;
use Model\Cms\SiteBannerQuery as ChildSiteBannerQuery;
use Model\Cms\SiteQuery as ChildSiteQuery;
use Model\Cms\Map\SiteBannerTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'site_banner' table.
 *
 *
 *
 * @package    propel.generator.Model.Cms.Base
 */
abstract class SiteBanner implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Cms\\Map\\SiteBannerTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the site_id field.
     *
     * @var        int
     */
    protected $site_id;

    /**
     * The value for the about_txt field.
     *
     * @var        string|null
     */
    protected $about_txt;

    /**
     * The value for the alignment field.
     *
     * @var        string|null
     */
    protected $alignment;

    /**
     * The value for the slogan field.
     *
     * @var        string|null
     */
    protected $slogan;

    /**
     * The value for the sub_text field.
     *
     * @var        string|null
     */
    protected $sub_text;

    /**
     * The value for the has_info_button field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean|null
     */
    protected $has_info_button;

    /**
     * The value for the info_link field.
     *
     * @var        string|null
     */
    protected $info_link;

    /**
     * The value for the has_buy_now field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean|null
     */
    protected $has_buy_now;

    /**
     * The value for the buy_now_link field.
     *
     * @var        string|null
     */
    protected $buy_now_link;

    /**
     * The value for the file_ext field.
     *
     * @var        string|null
     */
    protected $file_ext;

    /**
     * @var        ChildSite
     */
    protected $aSite;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->has_info_button = true;
        $this->has_buy_now = true;
    }

    /**
     * Initializes internal state of Model\Cms\Base\SiteBanner object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>SiteBanner</code> instance.  If
     * <code>obj</code> is an instance of <code>SiteBanner</code>, delegates to
     * <code>equals(SiteBanner)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [site_id] column value.
     *
     * @return int
     */
    public function getSiteId()
    {
        return $this->site_id;
    }

    /**
     * Get the [about_txt] column value.
     *
     * @return string|null
     */
    public function getAboutTxt()
    {
        return $this->about_txt;
    }

    /**
     * Get the [alignment] column value.
     *
     * @return string|null
     */
    public function getAlignment()
    {
        return $this->alignment;
    }

    /**
     * Get the [slogan] column value.
     *
     * @return string|null
     */
    public function getSlogan()
    {
        return $this->slogan;
    }

    /**
     * Get the [sub_text] column value.
     *
     * @return string|null
     */
    public function getSubText()
    {
        return $this->sub_text;
    }

    /**
     * Get the [has_info_button] column value.
     *
     * @return boolean|null
     */
    public function getHasInfoButton()
    {
        return $this->has_info_button;
    }

    /**
     * Get the [has_info_button] column value.
     *
     * @return boolean|null
     */
    public function hasInfoButton()
    {
        return $this->getHasInfoButton();
    }

    /**
     * Get the [info_link] column value.
     *
     * @return string|null
     */
    public function getInfoLink()
    {
        return $this->info_link;
    }

    /**
     * Get the [has_buy_now] column value.
     *
     * @return boolean|null
     */
    public function getHasBuyNow()
    {
        return $this->has_buy_now;
    }

    /**
     * Get the [has_buy_now] column value.
     *
     * @return boolean|null
     */
    public function hasBuyNow()
    {
        return $this->getHasBuyNow();
    }

    /**
     * Get the [buy_now_link] column value.
     *
     * @return string|null
     */
    public function getBuyNowLink()
    {
        return $this->buy_now_link;
    }

    /**
     * Get the [file_ext] column value.
     *
     * @return string|null
     */
    public function getFileExt()
    {
        return $this->file_ext;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Cms\SiteBanner The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[SiteBannerTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [site_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Cms\SiteBanner The current object (for fluent API support)
     */
    public function setSiteId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->site_id !== $v) {
            $this->site_id = $v;
            $this->modifiedColumns[SiteBannerTableMap::COL_SITE_ID] = true;
        }

        if ($this->aSite !== null && $this->aSite->getId() !== $v) {
            $this->aSite = null;
        }

        return $this;
    } // setSiteId()

    /**
     * Set the value of [about_txt] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SiteBanner The current object (for fluent API support)
     */
    public function setAboutTxt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->about_txt !== $v) {
            $this->about_txt = $v;
            $this->modifiedColumns[SiteBannerTableMap::COL_ABOUT_TXT] = true;
        }

        return $this;
    } // setAboutTxt()

    /**
     * Set the value of [alignment] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SiteBanner The current object (for fluent API support)
     */
    public function setAlignment($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->alignment !== $v) {
            $this->alignment = $v;
            $this->modifiedColumns[SiteBannerTableMap::COL_ALIGNMENT] = true;
        }

        return $this;
    } // setAlignment()

    /**
     * Set the value of [slogan] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SiteBanner The current object (for fluent API support)
     */
    public function setSlogan($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->slogan !== $v) {
            $this->slogan = $v;
            $this->modifiedColumns[SiteBannerTableMap::COL_SLOGAN] = true;
        }

        return $this;
    } // setSlogan()

    /**
     * Set the value of [sub_text] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SiteBanner The current object (for fluent API support)
     */
    public function setSubText($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sub_text !== $v) {
            $this->sub_text = $v;
            $this->modifiedColumns[SiteBannerTableMap::COL_SUB_TEXT] = true;
        }

        return $this;
    } // setSubText()

    /**
     * Sets the value of the [has_info_button] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Cms\SiteBanner The current object (for fluent API support)
     */
    public function setHasInfoButton($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_info_button !== $v) {
            $this->has_info_button = $v;
            $this->modifiedColumns[SiteBannerTableMap::COL_HAS_INFO_BUTTON] = true;
        }

        return $this;
    } // setHasInfoButton()

    /**
     * Set the value of [info_link] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SiteBanner The current object (for fluent API support)
     */
    public function setInfoLink($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->info_link !== $v) {
            $this->info_link = $v;
            $this->modifiedColumns[SiteBannerTableMap::COL_INFO_LINK] = true;
        }

        return $this;
    } // setInfoLink()

    /**
     * Sets the value of the [has_buy_now] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Cms\SiteBanner The current object (for fluent API support)
     */
    public function setHasBuyNow($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_buy_now !== $v) {
            $this->has_buy_now = $v;
            $this->modifiedColumns[SiteBannerTableMap::COL_HAS_BUY_NOW] = true;
        }

        return $this;
    } // setHasBuyNow()

    /**
     * Set the value of [buy_now_link] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SiteBanner The current object (for fluent API support)
     */
    public function setBuyNowLink($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->buy_now_link !== $v) {
            $this->buy_now_link = $v;
            $this->modifiedColumns[SiteBannerTableMap::COL_BUY_NOW_LINK] = true;
        }

        return $this;
    } // setBuyNowLink()

    /**
     * Set the value of [file_ext] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Cms\SiteBanner The current object (for fluent API support)
     */
    public function setFileExt($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->file_ext !== $v) {
            $this->file_ext = $v;
            $this->modifiedColumns[SiteBannerTableMap::COL_FILE_EXT] = true;
        }

        return $this;
    } // setFileExt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->has_info_button !== true) {
                return false;
            }

            if ($this->has_buy_now !== true) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : SiteBannerTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : SiteBannerTableMap::translateFieldName('SiteId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->site_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : SiteBannerTableMap::translateFieldName('AboutTxt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->about_txt = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : SiteBannerTableMap::translateFieldName('Alignment', TableMap::TYPE_PHPNAME, $indexType)];
            $this->alignment = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : SiteBannerTableMap::translateFieldName('Slogan', TableMap::TYPE_PHPNAME, $indexType)];
            $this->slogan = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : SiteBannerTableMap::translateFieldName('SubText', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sub_text = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : SiteBannerTableMap::translateFieldName('HasInfoButton', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_info_button = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : SiteBannerTableMap::translateFieldName('InfoLink', TableMap::TYPE_PHPNAME, $indexType)];
            $this->info_link = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : SiteBannerTableMap::translateFieldName('HasBuyNow', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_buy_now = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : SiteBannerTableMap::translateFieldName('BuyNowLink', TableMap::TYPE_PHPNAME, $indexType)];
            $this->buy_now_link = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : SiteBannerTableMap::translateFieldName('FileExt', TableMap::TYPE_PHPNAME, $indexType)];
            $this->file_ext = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 11; // 11 = SiteBannerTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Cms\\SiteBanner'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aSite !== null && $this->site_id !== $this->aSite->getId()) {
            $this->aSite = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteBannerTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildSiteBannerQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aSite = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see SiteBanner::setDeleted()
     * @see SiteBanner::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteBannerTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildSiteBannerQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteBannerTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                SiteBannerTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aSite !== null) {
                if ($this->aSite->isModified() || $this->aSite->isNew()) {
                    $affectedRows += $this->aSite->save($con);
                }
                $this->setSite($this->aSite);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[SiteBannerTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . SiteBannerTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(SiteBannerTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_SITE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'site_id';
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_ABOUT_TXT)) {
            $modifiedColumns[':p' . $index++]  = 'about_txt';
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_ALIGNMENT)) {
            $modifiedColumns[':p' . $index++]  = 'alignment';
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_SLOGAN)) {
            $modifiedColumns[':p' . $index++]  = 'slogan';
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_SUB_TEXT)) {
            $modifiedColumns[':p' . $index++]  = 'sub_text';
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_HAS_INFO_BUTTON)) {
            $modifiedColumns[':p' . $index++]  = 'has_info_button';
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_INFO_LINK)) {
            $modifiedColumns[':p' . $index++]  = 'info_link';
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_HAS_BUY_NOW)) {
            $modifiedColumns[':p' . $index++]  = 'has_buy_now';
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_BUY_NOW_LINK)) {
            $modifiedColumns[':p' . $index++]  = 'buy_now_link';
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_FILE_EXT)) {
            $modifiedColumns[':p' . $index++]  = 'file_ext';
        }

        $sql = sprintf(
            'INSERT INTO site_banner (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'site_id':
                        $stmt->bindValue($identifier, $this->site_id, PDO::PARAM_INT);
                        break;
                    case 'about_txt':
                        $stmt->bindValue($identifier, $this->about_txt, PDO::PARAM_STR);
                        break;
                    case 'alignment':
                        $stmt->bindValue($identifier, $this->alignment, PDO::PARAM_STR);
                        break;
                    case 'slogan':
                        $stmt->bindValue($identifier, $this->slogan, PDO::PARAM_STR);
                        break;
                    case 'sub_text':
                        $stmt->bindValue($identifier, $this->sub_text, PDO::PARAM_STR);
                        break;
                    case 'has_info_button':
                        $stmt->bindValue($identifier, (int) $this->has_info_button, PDO::PARAM_INT);
                        break;
                    case 'info_link':
                        $stmt->bindValue($identifier, $this->info_link, PDO::PARAM_STR);
                        break;
                    case 'has_buy_now':
                        $stmt->bindValue($identifier, (int) $this->has_buy_now, PDO::PARAM_INT);
                        break;
                    case 'buy_now_link':
                        $stmt->bindValue($identifier, $this->buy_now_link, PDO::PARAM_STR);
                        break;
                    case 'file_ext':
                        $stmt->bindValue($identifier, $this->file_ext, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SiteBannerTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getSiteId();
                break;
            case 2:
                return $this->getAboutTxt();
                break;
            case 3:
                return $this->getAlignment();
                break;
            case 4:
                return $this->getSlogan();
                break;
            case 5:
                return $this->getSubText();
                break;
            case 6:
                return $this->getHasInfoButton();
                break;
            case 7:
                return $this->getInfoLink();
                break;
            case 8:
                return $this->getHasBuyNow();
                break;
            case 9:
                return $this->getBuyNowLink();
                break;
            case 10:
                return $this->getFileExt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['SiteBanner'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['SiteBanner'][$this->hashCode()] = true;
        $keys = SiteBannerTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getSiteId(),
            $keys[2] => $this->getAboutTxt(),
            $keys[3] => $this->getAlignment(),
            $keys[4] => $this->getSlogan(),
            $keys[5] => $this->getSubText(),
            $keys[6] => $this->getHasInfoButton(),
            $keys[7] => $this->getInfoLink(),
            $keys[8] => $this->getHasBuyNow(),
            $keys[9] => $this->getBuyNowLink(),
            $keys[10] => $this->getFileExt(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aSite) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'site';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'site';
                        break;
                    default:
                        $key = 'Site';
                }

                $result[$key] = $this->aSite->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Cms\SiteBanner
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = SiteBannerTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Cms\SiteBanner
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setSiteId($value);
                break;
            case 2:
                $this->setAboutTxt($value);
                break;
            case 3:
                $this->setAlignment($value);
                break;
            case 4:
                $this->setSlogan($value);
                break;
            case 5:
                $this->setSubText($value);
                break;
            case 6:
                $this->setHasInfoButton($value);
                break;
            case 7:
                $this->setInfoLink($value);
                break;
            case 8:
                $this->setHasBuyNow($value);
                break;
            case 9:
                $this->setBuyNowLink($value);
                break;
            case 10:
                $this->setFileExt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = SiteBannerTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setSiteId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setAboutTxt($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setAlignment($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSlogan($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setSubText($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setHasInfoButton($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setInfoLink($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setHasBuyNow($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setBuyNowLink($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setFileExt($arr[$keys[10]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Cms\SiteBanner The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(SiteBannerTableMap::DATABASE_NAME);

        if ($this->isColumnModified(SiteBannerTableMap::COL_ID)) {
            $criteria->add(SiteBannerTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_SITE_ID)) {
            $criteria->add(SiteBannerTableMap::COL_SITE_ID, $this->site_id);
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_ABOUT_TXT)) {
            $criteria->add(SiteBannerTableMap::COL_ABOUT_TXT, $this->about_txt);
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_ALIGNMENT)) {
            $criteria->add(SiteBannerTableMap::COL_ALIGNMENT, $this->alignment);
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_SLOGAN)) {
            $criteria->add(SiteBannerTableMap::COL_SLOGAN, $this->slogan);
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_SUB_TEXT)) {
            $criteria->add(SiteBannerTableMap::COL_SUB_TEXT, $this->sub_text);
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_HAS_INFO_BUTTON)) {
            $criteria->add(SiteBannerTableMap::COL_HAS_INFO_BUTTON, $this->has_info_button);
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_INFO_LINK)) {
            $criteria->add(SiteBannerTableMap::COL_INFO_LINK, $this->info_link);
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_HAS_BUY_NOW)) {
            $criteria->add(SiteBannerTableMap::COL_HAS_BUY_NOW, $this->has_buy_now);
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_BUY_NOW_LINK)) {
            $criteria->add(SiteBannerTableMap::COL_BUY_NOW_LINK, $this->buy_now_link);
        }
        if ($this->isColumnModified(SiteBannerTableMap::COL_FILE_EXT)) {
            $criteria->add(SiteBannerTableMap::COL_FILE_EXT, $this->file_ext);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildSiteBannerQuery::create();
        $criteria->add(SiteBannerTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Cms\SiteBanner (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setSiteId($this->getSiteId());
        $copyObj->setAboutTxt($this->getAboutTxt());
        $copyObj->setAlignment($this->getAlignment());
        $copyObj->setSlogan($this->getSlogan());
        $copyObj->setSubText($this->getSubText());
        $copyObj->setHasInfoButton($this->getHasInfoButton());
        $copyObj->setInfoLink($this->getInfoLink());
        $copyObj->setHasBuyNow($this->getHasBuyNow());
        $copyObj->setBuyNowLink($this->getBuyNowLink());
        $copyObj->setFileExt($this->getFileExt());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Cms\SiteBanner Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildSite object.
     *
     * @param  ChildSite $v
     * @return $this|\Model\Cms\SiteBanner The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSite(ChildSite $v = null)
    {
        if ($v === null) {
            $this->setSiteId(NULL);
        } else {
            $this->setSiteId($v->getId());
        }

        $this->aSite = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildSite object, it will not be re-added.
        if ($v !== null) {
            $v->addSiteBanner($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildSite object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildSite The associated ChildSite object.
     * @throws PropelException
     */
    public function getSite(ConnectionInterface $con = null)
    {
        if ($this->aSite === null && ($this->site_id != 0)) {
            $this->aSite = ChildSiteQuery::create()->findPk($this->site_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSite->addSiteBanners($this);
             */
        }

        return $this->aSite;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aSite) {
            $this->aSite->removeSiteBanner($this);
        }
        $this->id = null;
        $this->site_id = null;
        $this->about_txt = null;
        $this->alignment = null;
        $this->slogan = null;
        $this->sub_text = null;
        $this->has_info_button = null;
        $this->info_link = null;
        $this->has_buy_now = null;
        $this->buy_now_link = null;
        $this->file_ext = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aSite = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(SiteBannerTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
