<?php

namespace Model\Cms\Base;

use \Exception;
use \PDO;
use Model\Cms\SiteFAQ as ChildSiteFAQ;
use Model\Cms\SiteFAQQuery as ChildSiteFAQQuery;
use Model\Cms\Map\SiteFAQTableMap;
use Model\Setting\MasterTable\Language;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'site_faq' table.
 *
 *
 *
 * @method     ChildSiteFAQQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSiteFAQQuery orderBySiteId($order = Criteria::ASC) Order by the site_id column
 * @method     ChildSiteFAQQuery orderBySiteFaqCategoryId($order = Criteria::ASC) Order by the site_faq_category_id column
 * @method     ChildSiteFAQQuery orderByLanguageId($order = Criteria::ASC) Order by the language_id column
 * @method     ChildSiteFAQQuery orderByQuestion($order = Criteria::ASC) Order by the question column
 * @method     ChildSiteFAQQuery orderByAnswer($order = Criteria::ASC) Order by the answer column
 *
 * @method     ChildSiteFAQQuery groupById() Group by the id column
 * @method     ChildSiteFAQQuery groupBySiteId() Group by the site_id column
 * @method     ChildSiteFAQQuery groupBySiteFaqCategoryId() Group by the site_faq_category_id column
 * @method     ChildSiteFAQQuery groupByLanguageId() Group by the language_id column
 * @method     ChildSiteFAQQuery groupByQuestion() Group by the question column
 * @method     ChildSiteFAQQuery groupByAnswer() Group by the answer column
 *
 * @method     ChildSiteFAQQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSiteFAQQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSiteFAQQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSiteFAQQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSiteFAQQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSiteFAQQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSiteFAQQuery leftJoinLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Language relation
 * @method     ChildSiteFAQQuery rightJoinLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Language relation
 * @method     ChildSiteFAQQuery innerJoinLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the Language relation
 *
 * @method     ChildSiteFAQQuery joinWithLanguage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Language relation
 *
 * @method     ChildSiteFAQQuery leftJoinWithLanguage() Adds a LEFT JOIN clause and with to the query using the Language relation
 * @method     ChildSiteFAQQuery rightJoinWithLanguage() Adds a RIGHT JOIN clause and with to the query using the Language relation
 * @method     ChildSiteFAQQuery innerJoinWithLanguage() Adds a INNER JOIN clause and with to the query using the Language relation
 *
 * @method     ChildSiteFAQQuery leftJoinFaqCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the FaqCategory relation
 * @method     ChildSiteFAQQuery rightJoinFaqCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FaqCategory relation
 * @method     ChildSiteFAQQuery innerJoinFaqCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the FaqCategory relation
 *
 * @method     ChildSiteFAQQuery joinWithFaqCategory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FaqCategory relation
 *
 * @method     ChildSiteFAQQuery leftJoinWithFaqCategory() Adds a LEFT JOIN clause and with to the query using the FaqCategory relation
 * @method     ChildSiteFAQQuery rightJoinWithFaqCategory() Adds a RIGHT JOIN clause and with to the query using the FaqCategory relation
 * @method     ChildSiteFAQQuery innerJoinWithFaqCategory() Adds a INNER JOIN clause and with to the query using the FaqCategory relation
 *
 * @method     ChildSiteFAQQuery leftJoinSite($relationAlias = null) Adds a LEFT JOIN clause to the query using the Site relation
 * @method     ChildSiteFAQQuery rightJoinSite($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Site relation
 * @method     ChildSiteFAQQuery innerJoinSite($relationAlias = null) Adds a INNER JOIN clause to the query using the Site relation
 *
 * @method     ChildSiteFAQQuery joinWithSite($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Site relation
 *
 * @method     ChildSiteFAQQuery leftJoinWithSite() Adds a LEFT JOIN clause and with to the query using the Site relation
 * @method     ChildSiteFAQQuery rightJoinWithSite() Adds a RIGHT JOIN clause and with to the query using the Site relation
 * @method     ChildSiteFAQQuery innerJoinWithSite() Adds a INNER JOIN clause and with to the query using the Site relation
 *
 * @method     \Model\Setting\MasterTable\LanguageQuery|\Model\Cms\SiteFAQCategoryQuery|\Model\Cms\SiteQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSiteFAQ findOne(ConnectionInterface $con = null) Return the first ChildSiteFAQ matching the query
 * @method     ChildSiteFAQ findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSiteFAQ matching the query, or a new ChildSiteFAQ object populated from the query conditions when no match is found
 *
 * @method     ChildSiteFAQ findOneById(int $id) Return the first ChildSiteFAQ filtered by the id column
 * @method     ChildSiteFAQ findOneBySiteId(int $site_id) Return the first ChildSiteFAQ filtered by the site_id column
 * @method     ChildSiteFAQ findOneBySiteFaqCategoryId(int $site_faq_category_id) Return the first ChildSiteFAQ filtered by the site_faq_category_id column
 * @method     ChildSiteFAQ findOneByLanguageId(int $language_id) Return the first ChildSiteFAQ filtered by the language_id column
 * @method     ChildSiteFAQ findOneByQuestion(string $question) Return the first ChildSiteFAQ filtered by the question column
 * @method     ChildSiteFAQ findOneByAnswer(string $answer) Return the first ChildSiteFAQ filtered by the answer column *

 * @method     ChildSiteFAQ requirePk($key, ConnectionInterface $con = null) Return the ChildSiteFAQ by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFAQ requireOne(ConnectionInterface $con = null) Return the first ChildSiteFAQ matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteFAQ requireOneById(int $id) Return the first ChildSiteFAQ filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFAQ requireOneBySiteId(int $site_id) Return the first ChildSiteFAQ filtered by the site_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFAQ requireOneBySiteFaqCategoryId(int $site_faq_category_id) Return the first ChildSiteFAQ filtered by the site_faq_category_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFAQ requireOneByLanguageId(int $language_id) Return the first ChildSiteFAQ filtered by the language_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFAQ requireOneByQuestion(string $question) Return the first ChildSiteFAQ filtered by the question column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSiteFAQ requireOneByAnswer(string $answer) Return the first ChildSiteFAQ filtered by the answer column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSiteFAQ[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSiteFAQ objects based on current ModelCriteria
 * @method     ChildSiteFAQ[]|ObjectCollection findById(int $id) Return ChildSiteFAQ objects filtered by the id column
 * @method     ChildSiteFAQ[]|ObjectCollection findBySiteId(int $site_id) Return ChildSiteFAQ objects filtered by the site_id column
 * @method     ChildSiteFAQ[]|ObjectCollection findBySiteFaqCategoryId(int $site_faq_category_id) Return ChildSiteFAQ objects filtered by the site_faq_category_id column
 * @method     ChildSiteFAQ[]|ObjectCollection findByLanguageId(int $language_id) Return ChildSiteFAQ objects filtered by the language_id column
 * @method     ChildSiteFAQ[]|ObjectCollection findByQuestion(string $question) Return ChildSiteFAQ objects filtered by the question column
 * @method     ChildSiteFAQ[]|ObjectCollection findByAnswer(string $answer) Return ChildSiteFAQ objects filtered by the answer column
 * @method     ChildSiteFAQ[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SiteFAQQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Cms\Base\SiteFAQQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Cms\\SiteFAQ', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSiteFAQQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSiteFAQQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSiteFAQQuery) {
            return $criteria;
        }
        $query = new ChildSiteFAQQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSiteFAQ|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SiteFAQTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SiteFAQTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteFAQ A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, site_id, site_faq_category_id, language_id, question, answer FROM site_faq WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSiteFAQ $obj */
            $obj = new ChildSiteFAQ();
            $obj->hydrate($row);
            SiteFAQTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSiteFAQ|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSiteFAQQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SiteFAQTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSiteFAQQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SiteFAQTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFAQQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SiteFAQTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SiteFAQTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteFAQTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the site_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteId(1234); // WHERE site_id = 1234
     * $query->filterBySiteId(array(12, 34)); // WHERE site_id IN (12, 34)
     * $query->filterBySiteId(array('min' => 12)); // WHERE site_id > 12
     * </code>
     *
     * @see       filterBySite()
     *
     * @param     mixed $siteId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFAQQuery The current query, for fluid interface
     */
    public function filterBySiteId($siteId = null, $comparison = null)
    {
        if (is_array($siteId)) {
            $useMinMax = false;
            if (isset($siteId['min'])) {
                $this->addUsingAlias(SiteFAQTableMap::COL_SITE_ID, $siteId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteId['max'])) {
                $this->addUsingAlias(SiteFAQTableMap::COL_SITE_ID, $siteId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteFAQTableMap::COL_SITE_ID, $siteId, $comparison);
    }

    /**
     * Filter the query on the site_faq_category_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySiteFaqCategoryId(1234); // WHERE site_faq_category_id = 1234
     * $query->filterBySiteFaqCategoryId(array(12, 34)); // WHERE site_faq_category_id IN (12, 34)
     * $query->filterBySiteFaqCategoryId(array('min' => 12)); // WHERE site_faq_category_id > 12
     * </code>
     *
     * @see       filterByFaqCategory()
     *
     * @param     mixed $siteFaqCategoryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFAQQuery The current query, for fluid interface
     */
    public function filterBySiteFaqCategoryId($siteFaqCategoryId = null, $comparison = null)
    {
        if (is_array($siteFaqCategoryId)) {
            $useMinMax = false;
            if (isset($siteFaqCategoryId['min'])) {
                $this->addUsingAlias(SiteFAQTableMap::COL_SITE_FAQ_CATEGORY_ID, $siteFaqCategoryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($siteFaqCategoryId['max'])) {
                $this->addUsingAlias(SiteFAQTableMap::COL_SITE_FAQ_CATEGORY_ID, $siteFaqCategoryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteFAQTableMap::COL_SITE_FAQ_CATEGORY_ID, $siteFaqCategoryId, $comparison);
    }

    /**
     * Filter the query on the language_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguageId(1234); // WHERE language_id = 1234
     * $query->filterByLanguageId(array(12, 34)); // WHERE language_id IN (12, 34)
     * $query->filterByLanguageId(array('min' => 12)); // WHERE language_id > 12
     * </code>
     *
     * @see       filterByLanguage()
     *
     * @param     mixed $languageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFAQQuery The current query, for fluid interface
     */
    public function filterByLanguageId($languageId = null, $comparison = null)
    {
        if (is_array($languageId)) {
            $useMinMax = false;
            if (isset($languageId['min'])) {
                $this->addUsingAlias(SiteFAQTableMap::COL_LANGUAGE_ID, $languageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($languageId['max'])) {
                $this->addUsingAlias(SiteFAQTableMap::COL_LANGUAGE_ID, $languageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteFAQTableMap::COL_LANGUAGE_ID, $languageId, $comparison);
    }

    /**
     * Filter the query on the question column
     *
     * Example usage:
     * <code>
     * $query->filterByQuestion('fooValue');   // WHERE question = 'fooValue'
     * $query->filterByQuestion('%fooValue%', Criteria::LIKE); // WHERE question LIKE '%fooValue%'
     * </code>
     *
     * @param     string $question The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFAQQuery The current query, for fluid interface
     */
    public function filterByQuestion($question = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($question)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteFAQTableMap::COL_QUESTION, $question, $comparison);
    }

    /**
     * Filter the query on the answer column
     *
     * Example usage:
     * <code>
     * $query->filterByAnswer('fooValue');   // WHERE answer = 'fooValue'
     * $query->filterByAnswer('%fooValue%', Criteria::LIKE); // WHERE answer LIKE '%fooValue%'
     * </code>
     *
     * @param     string $answer The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSiteFAQQuery The current query, for fluid interface
     */
    public function filterByAnswer($answer = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($answer)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SiteFAQTableMap::COL_ANSWER, $answer, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Language object
     *
     * @param \Model\Setting\MasterTable\Language|ObjectCollection $language The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteFAQQuery The current query, for fluid interface
     */
    public function filterByLanguage($language, $comparison = null)
    {
        if ($language instanceof \Model\Setting\MasterTable\Language) {
            return $this
                ->addUsingAlias(SiteFAQTableMap::COL_LANGUAGE_ID, $language->getId(), $comparison);
        } elseif ($language instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteFAQTableMap::COL_LANGUAGE_ID, $language->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLanguage() only accepts arguments of type \Model\Setting\MasterTable\Language or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Language relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteFAQQuery The current query, for fluid interface
     */
    public function joinLanguage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Language');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Language');
        }

        return $this;
    }

    /**
     * Use the Language relation Language object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\LanguageQuery A secondary query class using the current class as primary query
     */
    public function useLanguageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLanguage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Language', '\Model\Setting\MasterTable\LanguageQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\SiteFAQCategory object
     *
     * @param \Model\Cms\SiteFAQCategory|ObjectCollection $siteFAQCategory The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteFAQQuery The current query, for fluid interface
     */
    public function filterByFaqCategory($siteFAQCategory, $comparison = null)
    {
        if ($siteFAQCategory instanceof \Model\Cms\SiteFAQCategory) {
            return $this
                ->addUsingAlias(SiteFAQTableMap::COL_SITE_FAQ_CATEGORY_ID, $siteFAQCategory->getId(), $comparison);
        } elseif ($siteFAQCategory instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteFAQTableMap::COL_SITE_FAQ_CATEGORY_ID, $siteFAQCategory->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFaqCategory() only accepts arguments of type \Model\Cms\SiteFAQCategory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FaqCategory relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteFAQQuery The current query, for fluid interface
     */
    public function joinFaqCategory($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FaqCategory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FaqCategory');
        }

        return $this;
    }

    /**
     * Use the FaqCategory relation SiteFAQCategory object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteFAQCategoryQuery A secondary query class using the current class as primary query
     */
    public function useFaqCategoryQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFaqCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FaqCategory', '\Model\Cms\SiteFAQCategoryQuery');
    }

    /**
     * Filter the query by a related \Model\Cms\Site object
     *
     * @param \Model\Cms\Site|ObjectCollection $site The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSiteFAQQuery The current query, for fluid interface
     */
    public function filterBySite($site, $comparison = null)
    {
        if ($site instanceof \Model\Cms\Site) {
            return $this
                ->addUsingAlias(SiteFAQTableMap::COL_SITE_ID, $site->getId(), $comparison);
        } elseif ($site instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SiteFAQTableMap::COL_SITE_ID, $site->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySite() only accepts arguments of type \Model\Cms\Site or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Site relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSiteFAQQuery The current query, for fluid interface
     */
    public function joinSite($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Site');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Site');
        }

        return $this;
    }

    /**
     * Use the Site relation Site object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Cms\SiteQuery A secondary query class using the current class as primary query
     */
    public function useSiteQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSite($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Site', '\Model\Cms\SiteQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSiteFAQ $siteFAQ Object to remove from the list of results
     *
     * @return $this|ChildSiteFAQQuery The current query, for fluid interface
     */
    public function prune($siteFAQ = null)
    {
        if ($siteFAQ) {
            $this->addUsingAlias(SiteFAQTableMap::COL_ID, $siteFAQ->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the site_faq table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteFAQTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SiteFAQTableMap::clearInstancePool();
            SiteFAQTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SiteFAQTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SiteFAQTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SiteFAQTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SiteFAQTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SiteFAQQuery
