<?php

namespace Model\Cms;

use Model\Cms\Base\SiteFAQ as BaseSiteFAQ;

/**
 * Skeleton subclass for representing a row from the 'site_faq' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SiteFAQ extends BaseSiteFAQ
{

}
