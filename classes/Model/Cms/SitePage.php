<?php

namespace Model\Cms;

use Core\Utils;
use Model\Cms\Base\SitePage as BaseSitePage;

/**
 * Skeleton subclass for representing a row from the 'site_page' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SitePage extends BaseSitePage
{
    function hasCustomUrl()
    {
        return (bool) parent::getUrl();
    }
    function getUrl()
    {

        if(!$this->hasUrl())
        {
            return null;
        }
        if(!parent::getUrl())
        {
            return '/s/'.$this->getId().'/'.Utils::slugify($this->getTitle());
        }
        return parent::getUrl();
    }
    function getCoverImagePath()
    {
        return null;
    }
}
