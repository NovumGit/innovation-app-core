<?php

namespace Model\Cms;

use Core\Config;
use Model\Cms\Base\SitePagePicture as BaseSitePagePicture;

/**
 * Skeleton subclass for representing a row from the 'site_page_picture' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SitePagePicture extends BaseSitePagePicture
{

    function getImageDir()
    {
        $sDir = Config::getDataDir().'/img/cms/'.$this->getSitePageId().'/';
        if(!is_dir($sDir))
        {
            mkdir($sDir, 0777, true);
            chmod($sDir, 0777);
        }
        return $sDir;
    }
}
