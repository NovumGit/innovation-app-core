<?php

namespace Model\Cms;

use Core\PropelSqlTrait;
use Model\Cms\Base\SiteFooterBlockTranslationSitePageQuery as BaseSiteFooterBlockTranslationSitePageQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'site_footer_block_translation_site_page' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SiteFooterBlockTranslationSitePageQuery extends BaseSiteFooterBlockTranslationSitePageQuery
{

	use PropelSqlTrait;
}
