<?php

namespace Model;

use Model\Base\BulkStock as BaseBulkStock;

/**
 * Skeleton subclass for representing a row from the 'bulk_stock' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BulkStock extends BaseBulkStock
{

}
