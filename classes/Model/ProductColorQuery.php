<?php

namespace Model;

use Model\Base\ProductColorQuery as BaseProductColorQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'product_color' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProductColorQuery extends BaseProductColorQuery
{

}
