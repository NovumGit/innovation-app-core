<?php

namespace Model\Map;

use Model\ProductReview;
use Model\ProductReviewQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'product_review' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class ProductReviewTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Map.ProductReviewTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'product_review';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\ProductReview';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.ProductReview';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 10;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 10;

    /**
     * the column name for the id field
     */
    const COL_ID = 'product_review.id';

    /**
     * the column name for the product_id field
     */
    const COL_PRODUCT_ID = 'product_review.product_id';

    /**
     * the column name for the old_id field
     */
    const COL_OLD_ID = 'product_review.old_id';

    /**
     * the column name for the customer_id field
     */
    const COL_CUSTOMER_ID = 'product_review.customer_id';

    /**
     * the column name for the first_name field
     */
    const COL_FIRST_NAME = 'product_review.first_name';

    /**
     * the column name for the from_city field
     */
    const COL_FROM_CITY = 'product_review.from_city';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'product_review.description';

    /**
     * the column name for the rating field
     */
    const COL_RATING = 'product_review.rating';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'product_review.created_date';

    /**
     * the column name for the approved field
     */
    const COL_APPROVED = 'product_review.approved';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProductId', 'OldId', 'CustomerId', 'FirstName', 'FromCity', 'Description', 'Rating', 'CreatedDate', 'Approved', ),
        self::TYPE_CAMELNAME     => array('id', 'productId', 'oldId', 'customerId', 'firstName', 'fromCity', 'description', 'rating', 'createdDate', 'approved', ),
        self::TYPE_COLNAME       => array(ProductReviewTableMap::COL_ID, ProductReviewTableMap::COL_PRODUCT_ID, ProductReviewTableMap::COL_OLD_ID, ProductReviewTableMap::COL_CUSTOMER_ID, ProductReviewTableMap::COL_FIRST_NAME, ProductReviewTableMap::COL_FROM_CITY, ProductReviewTableMap::COL_DESCRIPTION, ProductReviewTableMap::COL_RATING, ProductReviewTableMap::COL_CREATED_DATE, ProductReviewTableMap::COL_APPROVED, ),
        self::TYPE_FIELDNAME     => array('id', 'product_id', 'old_id', 'customer_id', 'first_name', 'from_city', 'description', 'rating', 'created_date', 'approved', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProductId' => 1, 'OldId' => 2, 'CustomerId' => 3, 'FirstName' => 4, 'FromCity' => 5, 'Description' => 6, 'Rating' => 7, 'CreatedDate' => 8, 'Approved' => 9, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'productId' => 1, 'oldId' => 2, 'customerId' => 3, 'firstName' => 4, 'fromCity' => 5, 'description' => 6, 'rating' => 7, 'createdDate' => 8, 'approved' => 9, ),
        self::TYPE_COLNAME       => array(ProductReviewTableMap::COL_ID => 0, ProductReviewTableMap::COL_PRODUCT_ID => 1, ProductReviewTableMap::COL_OLD_ID => 2, ProductReviewTableMap::COL_CUSTOMER_ID => 3, ProductReviewTableMap::COL_FIRST_NAME => 4, ProductReviewTableMap::COL_FROM_CITY => 5, ProductReviewTableMap::COL_DESCRIPTION => 6, ProductReviewTableMap::COL_RATING => 7, ProductReviewTableMap::COL_CREATED_DATE => 8, ProductReviewTableMap::COL_APPROVED => 9, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'product_id' => 1, 'old_id' => 2, 'customer_id' => 3, 'first_name' => 4, 'from_city' => 5, 'description' => 6, 'rating' => 7, 'created_date' => 8, 'approved' => 9, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('product_review');
        $this->setPhpName('ProductReview');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\ProductReview');
        $this->setPackage('Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('product_id', 'ProductId', 'INTEGER', 'product', 'id', true, null, null);
        $this->addColumn('old_id', 'OldId', 'VARCHAR', false, 255, null);
        $this->addColumn('customer_id', 'CustomerId', 'INTEGER', true, null, null);
        $this->addColumn('first_name', 'FirstName', 'LONGVARCHAR', false, null, null);
        $this->addColumn('from_city', 'FromCity', 'LONGVARCHAR', false, null, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('rating', 'Rating', 'INTEGER', false, 10, 0);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('approved', 'Approved', 'INTEGER', false, null, 0);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Product', '\\Model\\Product', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ProductReviewTableMap::CLASS_DEFAULT : ProductReviewTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ProductReview object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ProductReviewTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ProductReviewTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ProductReviewTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ProductReviewTableMap::OM_CLASS;
            /** @var ProductReview $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ProductReviewTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ProductReviewTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ProductReviewTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ProductReview $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ProductReviewTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ProductReviewTableMap::COL_ID);
            $criteria->addSelectColumn(ProductReviewTableMap::COL_PRODUCT_ID);
            $criteria->addSelectColumn(ProductReviewTableMap::COL_OLD_ID);
            $criteria->addSelectColumn(ProductReviewTableMap::COL_CUSTOMER_ID);
            $criteria->addSelectColumn(ProductReviewTableMap::COL_FIRST_NAME);
            $criteria->addSelectColumn(ProductReviewTableMap::COL_FROM_CITY);
            $criteria->addSelectColumn(ProductReviewTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(ProductReviewTableMap::COL_RATING);
            $criteria->addSelectColumn(ProductReviewTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(ProductReviewTableMap::COL_APPROVED);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.product_id');
            $criteria->addSelectColumn($alias . '.old_id');
            $criteria->addSelectColumn($alias . '.customer_id');
            $criteria->addSelectColumn($alias . '.first_name');
            $criteria->addSelectColumn($alias . '.from_city');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.rating');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.approved');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ProductReviewTableMap::DATABASE_NAME)->getTable(ProductReviewTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ProductReviewTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ProductReviewTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ProductReviewTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ProductReview or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ProductReview object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductReviewTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\ProductReview) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ProductReviewTableMap::DATABASE_NAME);
            $criteria->add(ProductReviewTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ProductReviewQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ProductReviewTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ProductReviewTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the product_review table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ProductReviewQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ProductReview or Criteria object.
     *
     * @param mixed               $criteria Criteria or ProductReview object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductReviewTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ProductReview object
        }

        if ($criteria->containsKey(ProductReviewTableMap::COL_ID) && $criteria->keyContainsValue(ProductReviewTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ProductReviewTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ProductReviewQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ProductReviewTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ProductReviewTableMap::buildTableMap();
