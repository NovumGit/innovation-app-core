<?php

namespace Model\Map;

use Model\ContactMessage;
use Model\ContactMessageQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'contact_message' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class ContactMessageTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Map.ContactMessageTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'contact_message';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\ContactMessage';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.ContactMessage';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 21;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 21;

    /**
     * the column name for the id field
     */
    const COL_ID = 'contact_message.id';

    /**
     * the column name for the is_new field
     */
    const COL_IS_NEW = 'contact_message.is_new';

    /**
     * the column name for the language_id field
     */
    const COL_LANGUAGE_ID = 'contact_message.language_id';

    /**
     * the column name for the customer_id field
     */
    const COL_CUSTOMER_ID = 'contact_message.customer_id';

    /**
     * the column name for the type_id field
     */
    const COL_TYPE_ID = 'contact_message.type_id';

    /**
     * the column name for the status_id field
     */
    const COL_STATUS_ID = 'contact_message.status_id';

    /**
     * the column name for the company_name field
     */
    const COL_COMPANY_NAME = 'contact_message.company_name';

    /**
     * the column name for the name field
     */
    const COL_NAME = 'contact_message.name';

    /**
     * the column name for the email field
     */
    const COL_EMAIL = 'contact_message.email';

    /**
     * the column name for the phone field
     */
    const COL_PHONE = 'contact_message.phone';

    /**
     * the column name for the subject field
     */
    const COL_SUBJECT = 'contact_message.subject';

    /**
     * the column name for the message field
     */
    const COL_MESSAGE = 'contact_message.message';

    /**
     * the column name for the form_url field
     */
    const COL_FORM_URL = 'contact_message.form_url';

    /**
     * the column name for the referrer_site field
     */
    const COL_REFERRER_SITE = 'contact_message.referrer_site';

    /**
     * the column name for the http_referrer field
     */
    const COL_HTTP_REFERRER = 'contact_message.http_referrer';

    /**
     * the column name for the remote_addr field
     */
    const COL_REMOTE_ADDR = 'contact_message.remote_addr';

    /**
     * the column name for the http_user_agent field
     */
    const COL_HTTP_USER_AGENT = 'contact_message.http_user_agent';

    /**
     * the column name for the note field
     */
    const COL_NOTE = 'contact_message.note';

    /**
     * the column name for the created_date field
     */
    const COL_CREATED_DATE = 'contact_message.created_date';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'contact_message.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'contact_message.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'IsNew', 'LanguageId', 'CustomerId', 'TypeId', 'StatusId', 'CompanyName', 'Name', 'Email', 'Phone', 'Subject', 'Message', 'FormUrl', 'ReferrerSite', 'HttpReferrer', 'RemoteAddr', 'HttpUserAgent', 'Note', 'CreatedDate', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'isNew', 'languageId', 'customerId', 'typeId', 'statusId', 'companyName', 'name', 'email', 'phone', 'subject', 'message', 'formUrl', 'referrerSite', 'httpReferrer', 'remoteAddr', 'httpUserAgent', 'note', 'createdDate', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ContactMessageTableMap::COL_ID, ContactMessageTableMap::COL_IS_NEW, ContactMessageTableMap::COL_LANGUAGE_ID, ContactMessageTableMap::COL_CUSTOMER_ID, ContactMessageTableMap::COL_TYPE_ID, ContactMessageTableMap::COL_STATUS_ID, ContactMessageTableMap::COL_COMPANY_NAME, ContactMessageTableMap::COL_NAME, ContactMessageTableMap::COL_EMAIL, ContactMessageTableMap::COL_PHONE, ContactMessageTableMap::COL_SUBJECT, ContactMessageTableMap::COL_MESSAGE, ContactMessageTableMap::COL_FORM_URL, ContactMessageTableMap::COL_REFERRER_SITE, ContactMessageTableMap::COL_HTTP_REFERRER, ContactMessageTableMap::COL_REMOTE_ADDR, ContactMessageTableMap::COL_HTTP_USER_AGENT, ContactMessageTableMap::COL_NOTE, ContactMessageTableMap::COL_CREATED_DATE, ContactMessageTableMap::COL_CREATED_AT, ContactMessageTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'is_new', 'language_id', 'customer_id', 'type_id', 'status_id', 'company_name', 'name', 'email', 'phone', 'subject', 'message', 'form_url', 'referrer_site', 'http_referrer', 'remote_addr', 'http_user_agent', 'note', 'created_date', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'IsNew' => 1, 'LanguageId' => 2, 'CustomerId' => 3, 'TypeId' => 4, 'StatusId' => 5, 'CompanyName' => 6, 'Name' => 7, 'Email' => 8, 'Phone' => 9, 'Subject' => 10, 'Message' => 11, 'FormUrl' => 12, 'ReferrerSite' => 13, 'HttpReferrer' => 14, 'RemoteAddr' => 15, 'HttpUserAgent' => 16, 'Note' => 17, 'CreatedDate' => 18, 'CreatedAt' => 19, 'UpdatedAt' => 20, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'isNew' => 1, 'languageId' => 2, 'customerId' => 3, 'typeId' => 4, 'statusId' => 5, 'companyName' => 6, 'name' => 7, 'email' => 8, 'phone' => 9, 'subject' => 10, 'message' => 11, 'formUrl' => 12, 'referrerSite' => 13, 'httpReferrer' => 14, 'remoteAddr' => 15, 'httpUserAgent' => 16, 'note' => 17, 'createdDate' => 18, 'createdAt' => 19, 'updatedAt' => 20, ),
        self::TYPE_COLNAME       => array(ContactMessageTableMap::COL_ID => 0, ContactMessageTableMap::COL_IS_NEW => 1, ContactMessageTableMap::COL_LANGUAGE_ID => 2, ContactMessageTableMap::COL_CUSTOMER_ID => 3, ContactMessageTableMap::COL_TYPE_ID => 4, ContactMessageTableMap::COL_STATUS_ID => 5, ContactMessageTableMap::COL_COMPANY_NAME => 6, ContactMessageTableMap::COL_NAME => 7, ContactMessageTableMap::COL_EMAIL => 8, ContactMessageTableMap::COL_PHONE => 9, ContactMessageTableMap::COL_SUBJECT => 10, ContactMessageTableMap::COL_MESSAGE => 11, ContactMessageTableMap::COL_FORM_URL => 12, ContactMessageTableMap::COL_REFERRER_SITE => 13, ContactMessageTableMap::COL_HTTP_REFERRER => 14, ContactMessageTableMap::COL_REMOTE_ADDR => 15, ContactMessageTableMap::COL_HTTP_USER_AGENT => 16, ContactMessageTableMap::COL_NOTE => 17, ContactMessageTableMap::COL_CREATED_DATE => 18, ContactMessageTableMap::COL_CREATED_AT => 19, ContactMessageTableMap::COL_UPDATED_AT => 20, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'is_new' => 1, 'language_id' => 2, 'customer_id' => 3, 'type_id' => 4, 'status_id' => 5, 'company_name' => 6, 'name' => 7, 'email' => 8, 'phone' => 9, 'subject' => 10, 'message' => 11, 'form_url' => 12, 'referrer_site' => 13, 'http_referrer' => 14, 'remote_addr' => 15, 'http_user_agent' => 16, 'note' => 17, 'created_date' => 18, 'created_at' => 19, 'updated_at' => 20, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('contact_message');
        $this->setPhpName('ContactMessage');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\ContactMessage');
        $this->setPackage('Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('is_new', 'IsNew', 'BOOLEAN', false, 1, true);
        $this->addForeignKey('language_id', 'LanguageId', 'INTEGER', 'mt_language', 'id', false, null, null);
        $this->addForeignKey('customer_id', 'CustomerId', 'INTEGER', 'customer', 'id', false, null, null);
        $this->addForeignKey('type_id', 'TypeId', 'INTEGER', 'contact_message_type', 'id', false, null, null);
        $this->addForeignKey('status_id', 'StatusId', 'INTEGER', 'contact_message_status', 'id', false, null, null);
        $this->addColumn('company_name', 'CompanyName', 'VARCHAR', true, 255, null);
        $this->addColumn('name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 255, null);
        $this->addColumn('phone', 'Phone', 'VARCHAR', false, 255, null);
        $this->addColumn('subject', 'Subject', 'VARCHAR', true, 255, null);
        $this->addColumn('message', 'Message', 'LONGVARCHAR', false, null, null);
        $this->addColumn('form_url', 'FormUrl', 'VARCHAR', true, 255, null);
        $this->addColumn('referrer_site', 'ReferrerSite', 'LONGVARCHAR', true, null, null);
        $this->addColumn('http_referrer', 'HttpReferrer', 'LONGVARCHAR', true, null, null);
        $this->addColumn('remote_addr', 'RemoteAddr', 'LONGVARCHAR', true, null, null);
        $this->addColumn('http_user_agent', 'HttpUserAgent', 'LONGVARCHAR', true, null, null);
        $this->addColumn('note', 'Note', 'LONGVARCHAR', false, null, null);
        $this->addColumn('created_date', 'CreatedDate', 'TIMESTAMP', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Language', '\\Model\\Setting\\MasterTable\\Language', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('Customer', '\\Model\\Crm\\Customer', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':customer_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('ContactMessageType', '\\Model\\ContactMessageType', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':type_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('ContactMessageStatus', '\\Model\\ContactMessageStatus', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':status_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ContactMessageTableMap::CLASS_DEFAULT : ContactMessageTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ContactMessage object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ContactMessageTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ContactMessageTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ContactMessageTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ContactMessageTableMap::OM_CLASS;
            /** @var ContactMessage $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ContactMessageTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ContactMessageTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ContactMessageTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ContactMessage $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ContactMessageTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ContactMessageTableMap::COL_ID);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_IS_NEW);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_LANGUAGE_ID);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_CUSTOMER_ID);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_TYPE_ID);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_STATUS_ID);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_COMPANY_NAME);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_NAME);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_EMAIL);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_PHONE);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_SUBJECT);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_MESSAGE);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_FORM_URL);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_REFERRER_SITE);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_HTTP_REFERRER);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_REMOTE_ADDR);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_HTTP_USER_AGENT);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_NOTE);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_CREATED_DATE);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(ContactMessageTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.is_new');
            $criteria->addSelectColumn($alias . '.language_id');
            $criteria->addSelectColumn($alias . '.customer_id');
            $criteria->addSelectColumn($alias . '.type_id');
            $criteria->addSelectColumn($alias . '.status_id');
            $criteria->addSelectColumn($alias . '.company_name');
            $criteria->addSelectColumn($alias . '.name');
            $criteria->addSelectColumn($alias . '.email');
            $criteria->addSelectColumn($alias . '.phone');
            $criteria->addSelectColumn($alias . '.subject');
            $criteria->addSelectColumn($alias . '.message');
            $criteria->addSelectColumn($alias . '.form_url');
            $criteria->addSelectColumn($alias . '.referrer_site');
            $criteria->addSelectColumn($alias . '.http_referrer');
            $criteria->addSelectColumn($alias . '.remote_addr');
            $criteria->addSelectColumn($alias . '.http_user_agent');
            $criteria->addSelectColumn($alias . '.note');
            $criteria->addSelectColumn($alias . '.created_date');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ContactMessageTableMap::DATABASE_NAME)->getTable(ContactMessageTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ContactMessageTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ContactMessageTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ContactMessageTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ContactMessage or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ContactMessage object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContactMessageTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\ContactMessage) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ContactMessageTableMap::DATABASE_NAME);
            $criteria->add(ContactMessageTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ContactMessageQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ContactMessageTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ContactMessageTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the contact_message table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ContactMessageQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ContactMessage or Criteria object.
     *
     * @param mixed               $criteria Criteria or ContactMessage object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContactMessageTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ContactMessage object
        }

        if ($criteria->containsKey(ContactMessageTableMap::COL_ID) && $criteria->keyContainsValue(ContactMessageTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ContactMessageTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ContactMessageQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ContactMessageTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ContactMessageTableMap::buildTableMap();
