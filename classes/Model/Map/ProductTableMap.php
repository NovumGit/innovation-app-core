<?php

namespace Model\Map;

use Model\Product;
use Model\ProductQuery;
use Model\Category\Map\CustomerWishlistTableMap;
use Model\Category\Map\ProductMultiCategoryTableMap;
use Model\Product\Map\ProductTagTableMap;
use Model\Product\Map\RelatedProductTableMap;
use Model\Sale\Map\OrderItemProductTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'product' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class ProductTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Map.ProductTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'product';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Product';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Product';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 56;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 56;

    /**
     * the column name for the id field
     */
    const COL_ID = 'product.id';

    /**
     * the column name for the derrived_from_id field
     */
    const COL_DERRIVED_FROM_ID = 'product.derrived_from_id';

    /**
     * the column name for the old_id field
     */
    const COL_OLD_ID = 'product.old_id';

    /**
     * the column name for the import_tag field
     */
    const COL_IMPORT_TAG = 'product.import_tag';

    /**
     * the column name for the sku field
     */
    const COL_SKU = 'product.sku';

    /**
     * the column name for the ean field
     */
    const COL_EAN = 'product.ean';

    /**
     * the column name for the deleted_on field
     */
    const COL_DELETED_ON = 'product.deleted_on';

    /**
     * the column name for the deleted_by_user_id field
     */
    const COL_DELETED_BY_USER_ID = 'product.deleted_by_user_id';

    /**
     * the column name for the popularity field
     */
    const COL_POPULARITY = 'product.popularity';

    /**
     * the column name for the avg_rating field
     */
    const COL_AVG_RATING = 'product.avg_rating';

    /**
     * the column name for the supplier field
     */
    const COL_SUPPLIER = 'product.supplier';

    /**
     * the column name for the supplier_id field
     */
    const COL_SUPPLIER_ID = 'product.supplier_id';

    /**
     * the column name for the supplier_product_number field
     */
    const COL_SUPPLIER_PRODUCT_NUMBER = 'product.supplier_product_number';

    /**
     * the column name for the in_sale field
     */
    const COL_IN_SALE = 'product.in_sale';

    /**
     * the column name for the in_spotlight field
     */
    const COL_IN_SPOTLIGHT = 'product.in_spotlight';

    /**
     * the column name for the in_webshop field
     */
    const COL_IN_WEBSHOP = 'product.in_webshop';

    /**
     * the column name for the is_offer field
     */
    const COL_IS_OFFER = 'product.is_offer';

    /**
     * the column name for the is_bestseller field
     */
    const COL_IS_BESTSELLER = 'product.is_bestseller';

    /**
     * the column name for the is_out_of_stock field
     */
    const COL_IS_OUT_OF_STOCK = 'product.is_out_of_stock';

    /**
     * the column name for the has_images field
     */
    const COL_HAS_IMAGES = 'product.has_images';

    /**
     * the column name for the category_id field
     */
    const COL_CATEGORY_ID = 'product.category_id';

    /**
     * the column name for the number field
     */
    const COL_NUMBER = 'product.number';

    /**
     * the column name for the advertiser_user_id field
     */
    const COL_ADVERTISER_USER_ID = 'product.advertiser_user_id';

    /**
     * the column name for the delivery_time_id field
     */
    const COL_DELIVERY_TIME_ID = 'product.delivery_time_id';

    /**
     * the column name for the thickness field
     */
    const COL_THICKNESS = 'product.thickness';

    /**
     * the column name for the height field
     */
    const COL_HEIGHT = 'product.height';

    /**
     * the column name for the width field
     */
    const COL_WIDTH = 'product.width';

    /**
     * the column name for the length field
     */
    const COL_LENGTH = 'product.length';

    /**
     * the column name for the weight field
     */
    const COL_WEIGHT = 'product.weight';

    /**
     * the column name for the composition field
     */
    const COL_COMPOSITION = 'product.composition';

    /**
     * the column name for the material_id field
     */
    const COL_MATERIAL_ID = 'product.material_id';

    /**
     * the column name for the brand_id field
     */
    const COL_BRAND_ID = 'product.brand_id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'product.title';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'product.description';

    /**
     * the column name for the note field
     */
    const COL_NOTE = 'product.note';

    /**
     * the column name for the created_on field
     */
    const COL_CREATED_ON = 'product.created_on';

    /**
     * the column name for the day_price field
     */
    const COL_DAY_PRICE = 'product.day_price';

    /**
     * the column name for the sale_price field
     */
    const COL_SALE_PRICE = 'product.sale_price';

    /**
     * the column name for the discount_percentage field
     */
    const COL_DISCOUNT_PERCENTAGE = 'product.discount_percentage';

    /**
     * the column name for the purchase_price field
     */
    const COL_PURCHASE_PRICE = 'product.purchase_price';

    /**
     * the column name for the reseller_price field
     */
    const COL_RESELLER_PRICE = 'product.reseller_price';

    /**
     * the column name for the advice_price field
     */
    const COL_ADVICE_PRICE = 'product.advice_price';

    /**
     * the column name for the bulk_stock_id field
     */
    const COL_BULK_STOCK_ID = 'product.bulk_stock_id';

    /**
     * the column name for the stock_quantity field
     */
    const COL_STOCK_QUANTITY = 'product.stock_quantity';

    /**
     * the column name for the virtual_stock field
     */
    const COL_VIRTUAL_STOCK = 'product.virtual_stock';

    /**
     * the column name for the quantity_per_pack field
     */
    const COL_QUANTITY_PER_PACK = 'product.quantity_per_pack';

    /**
     * the column name for the youtube_url field
     */
    const COL_YOUTUBE_URL = 'product.youtube_url';

    /**
     * the column name for the meta_title field
     */
    const COL_META_TITLE = 'product.meta_title';

    /**
     * the column name for the meta_description field
     */
    const COL_META_DESCRIPTION = 'product.meta_description';

    /**
     * the column name for the meta_keyword field
     */
    const COL_META_KEYWORD = 'product.meta_keyword';

    /**
     * the column name for the hashtag field
     */
    const COL_HASHTAG = 'product.hashtag';

    /**
     * the column name for the calculated_average_rating field
     */
    const COL_CALCULATED_AVERAGE_RATING = 'product.calculated_average_rating';

    /**
     * the column name for the vat_id field
     */
    const COL_VAT_ID = 'product.vat_id';

    /**
     * the column name for the unit_id field
     */
    const COL_UNIT_ID = 'product.unit_id';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'product.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'product.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'DerrivedFromId', 'OldId', 'ImportTag', 'Sku', 'Ean', 'DeletedOn', 'DeletedByUserId', 'Popularity', 'AvgRating', 'Supplier', 'SupplierId', 'SupplierProductNumber', 'InSale', 'InSpotlight', 'InWebshop', 'IsOffer', 'IsBestseller', 'IsOutOfStock', 'HasImages', 'CategoryId', 'Number', 'AdvertiserUserId', 'DeliveryTimeId', 'Thickness', 'Height', 'Width', 'Length', 'Weight', 'Composition', 'MaterialId', 'BrandId', 'Title', 'Description', 'Note', 'CreatedOn', 'DayPrice', 'SalePrice', 'DiscountPercentage', 'PurchasePrice', 'ResellerPrice', 'AdvicePrice', 'BulkStockId', 'StockQuantity', 'VirtualStock', 'QuantityPerPack', 'YoutubeUrl', 'MetaTitle', 'MetaDescription', 'MetaKeyword', 'Hashtag', 'CalculatedAverageRating', 'VatId', 'UnitId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'derrivedFromId', 'oldId', 'importTag', 'sku', 'ean', 'deletedOn', 'deletedByUserId', 'popularity', 'avgRating', 'supplier', 'supplierId', 'supplierProductNumber', 'inSale', 'inSpotlight', 'inWebshop', 'isOffer', 'isBestseller', 'isOutOfStock', 'hasImages', 'categoryId', 'number', 'advertiserUserId', 'deliveryTimeId', 'thickness', 'height', 'width', 'length', 'weight', 'composition', 'materialId', 'brandId', 'title', 'description', 'note', 'createdOn', 'dayPrice', 'salePrice', 'discountPercentage', 'purchasePrice', 'resellerPrice', 'advicePrice', 'bulkStockId', 'stockQuantity', 'virtualStock', 'quantityPerPack', 'youtubeUrl', 'metaTitle', 'metaDescription', 'metaKeyword', 'hashtag', 'calculatedAverageRating', 'vatId', 'unitId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ProductTableMap::COL_ID, ProductTableMap::COL_DERRIVED_FROM_ID, ProductTableMap::COL_OLD_ID, ProductTableMap::COL_IMPORT_TAG, ProductTableMap::COL_SKU, ProductTableMap::COL_EAN, ProductTableMap::COL_DELETED_ON, ProductTableMap::COL_DELETED_BY_USER_ID, ProductTableMap::COL_POPULARITY, ProductTableMap::COL_AVG_RATING, ProductTableMap::COL_SUPPLIER, ProductTableMap::COL_SUPPLIER_ID, ProductTableMap::COL_SUPPLIER_PRODUCT_NUMBER, ProductTableMap::COL_IN_SALE, ProductTableMap::COL_IN_SPOTLIGHT, ProductTableMap::COL_IN_WEBSHOP, ProductTableMap::COL_IS_OFFER, ProductTableMap::COL_IS_BESTSELLER, ProductTableMap::COL_IS_OUT_OF_STOCK, ProductTableMap::COL_HAS_IMAGES, ProductTableMap::COL_CATEGORY_ID, ProductTableMap::COL_NUMBER, ProductTableMap::COL_ADVERTISER_USER_ID, ProductTableMap::COL_DELIVERY_TIME_ID, ProductTableMap::COL_THICKNESS, ProductTableMap::COL_HEIGHT, ProductTableMap::COL_WIDTH, ProductTableMap::COL_LENGTH, ProductTableMap::COL_WEIGHT, ProductTableMap::COL_COMPOSITION, ProductTableMap::COL_MATERIAL_ID, ProductTableMap::COL_BRAND_ID, ProductTableMap::COL_TITLE, ProductTableMap::COL_DESCRIPTION, ProductTableMap::COL_NOTE, ProductTableMap::COL_CREATED_ON, ProductTableMap::COL_DAY_PRICE, ProductTableMap::COL_SALE_PRICE, ProductTableMap::COL_DISCOUNT_PERCENTAGE, ProductTableMap::COL_PURCHASE_PRICE, ProductTableMap::COL_RESELLER_PRICE, ProductTableMap::COL_ADVICE_PRICE, ProductTableMap::COL_BULK_STOCK_ID, ProductTableMap::COL_STOCK_QUANTITY, ProductTableMap::COL_VIRTUAL_STOCK, ProductTableMap::COL_QUANTITY_PER_PACK, ProductTableMap::COL_YOUTUBE_URL, ProductTableMap::COL_META_TITLE, ProductTableMap::COL_META_DESCRIPTION, ProductTableMap::COL_META_KEYWORD, ProductTableMap::COL_HASHTAG, ProductTableMap::COL_CALCULATED_AVERAGE_RATING, ProductTableMap::COL_VAT_ID, ProductTableMap::COL_UNIT_ID, ProductTableMap::COL_CREATED_AT, ProductTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'derrived_from_id', 'old_id', 'import_tag', 'sku', 'ean', 'deleted_on', 'deleted_by_user_id', 'popularity', 'avg_rating', 'supplier', 'supplier_id', 'supplier_product_number', 'in_sale', 'in_spotlight', 'in_webshop', 'is_offer', 'is_bestseller', 'is_out_of_stock', 'has_images', 'category_id', 'number', 'advertiser_user_id', 'delivery_time_id', 'thickness', 'height', 'width', 'length', 'weight', 'composition', 'material_id', 'brand_id', 'title', 'description', 'note', 'created_on', 'day_price', 'sale_price', 'discount_percentage', 'purchase_price', 'reseller_price', 'advice_price', 'bulk_stock_id', 'stock_quantity', 'virtual_stock', 'quantity_per_pack', 'youtube_url', 'meta_title', 'meta_description', 'meta_keyword', 'hashtag', 'calculated_average_rating', 'vat_id', 'unit_id', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'DerrivedFromId' => 1, 'OldId' => 2, 'ImportTag' => 3, 'Sku' => 4, 'Ean' => 5, 'DeletedOn' => 6, 'DeletedByUserId' => 7, 'Popularity' => 8, 'AvgRating' => 9, 'Supplier' => 10, 'SupplierId' => 11, 'SupplierProductNumber' => 12, 'InSale' => 13, 'InSpotlight' => 14, 'InWebshop' => 15, 'IsOffer' => 16, 'IsBestseller' => 17, 'IsOutOfStock' => 18, 'HasImages' => 19, 'CategoryId' => 20, 'Number' => 21, 'AdvertiserUserId' => 22, 'DeliveryTimeId' => 23, 'Thickness' => 24, 'Height' => 25, 'Width' => 26, 'Length' => 27, 'Weight' => 28, 'Composition' => 29, 'MaterialId' => 30, 'BrandId' => 31, 'Title' => 32, 'Description' => 33, 'Note' => 34, 'CreatedOn' => 35, 'DayPrice' => 36, 'SalePrice' => 37, 'DiscountPercentage' => 38, 'PurchasePrice' => 39, 'ResellerPrice' => 40, 'AdvicePrice' => 41, 'BulkStockId' => 42, 'StockQuantity' => 43, 'VirtualStock' => 44, 'QuantityPerPack' => 45, 'YoutubeUrl' => 46, 'MetaTitle' => 47, 'MetaDescription' => 48, 'MetaKeyword' => 49, 'Hashtag' => 50, 'CalculatedAverageRating' => 51, 'VatId' => 52, 'UnitId' => 53, 'CreatedAt' => 54, 'UpdatedAt' => 55, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'derrivedFromId' => 1, 'oldId' => 2, 'importTag' => 3, 'sku' => 4, 'ean' => 5, 'deletedOn' => 6, 'deletedByUserId' => 7, 'popularity' => 8, 'avgRating' => 9, 'supplier' => 10, 'supplierId' => 11, 'supplierProductNumber' => 12, 'inSale' => 13, 'inSpotlight' => 14, 'inWebshop' => 15, 'isOffer' => 16, 'isBestseller' => 17, 'isOutOfStock' => 18, 'hasImages' => 19, 'categoryId' => 20, 'number' => 21, 'advertiserUserId' => 22, 'deliveryTimeId' => 23, 'thickness' => 24, 'height' => 25, 'width' => 26, 'length' => 27, 'weight' => 28, 'composition' => 29, 'materialId' => 30, 'brandId' => 31, 'title' => 32, 'description' => 33, 'note' => 34, 'createdOn' => 35, 'dayPrice' => 36, 'salePrice' => 37, 'discountPercentage' => 38, 'purchasePrice' => 39, 'resellerPrice' => 40, 'advicePrice' => 41, 'bulkStockId' => 42, 'stockQuantity' => 43, 'virtualStock' => 44, 'quantityPerPack' => 45, 'youtubeUrl' => 46, 'metaTitle' => 47, 'metaDescription' => 48, 'metaKeyword' => 49, 'hashtag' => 50, 'calculatedAverageRating' => 51, 'vatId' => 52, 'unitId' => 53, 'createdAt' => 54, 'updatedAt' => 55, ),
        self::TYPE_COLNAME       => array(ProductTableMap::COL_ID => 0, ProductTableMap::COL_DERRIVED_FROM_ID => 1, ProductTableMap::COL_OLD_ID => 2, ProductTableMap::COL_IMPORT_TAG => 3, ProductTableMap::COL_SKU => 4, ProductTableMap::COL_EAN => 5, ProductTableMap::COL_DELETED_ON => 6, ProductTableMap::COL_DELETED_BY_USER_ID => 7, ProductTableMap::COL_POPULARITY => 8, ProductTableMap::COL_AVG_RATING => 9, ProductTableMap::COL_SUPPLIER => 10, ProductTableMap::COL_SUPPLIER_ID => 11, ProductTableMap::COL_SUPPLIER_PRODUCT_NUMBER => 12, ProductTableMap::COL_IN_SALE => 13, ProductTableMap::COL_IN_SPOTLIGHT => 14, ProductTableMap::COL_IN_WEBSHOP => 15, ProductTableMap::COL_IS_OFFER => 16, ProductTableMap::COL_IS_BESTSELLER => 17, ProductTableMap::COL_IS_OUT_OF_STOCK => 18, ProductTableMap::COL_HAS_IMAGES => 19, ProductTableMap::COL_CATEGORY_ID => 20, ProductTableMap::COL_NUMBER => 21, ProductTableMap::COL_ADVERTISER_USER_ID => 22, ProductTableMap::COL_DELIVERY_TIME_ID => 23, ProductTableMap::COL_THICKNESS => 24, ProductTableMap::COL_HEIGHT => 25, ProductTableMap::COL_WIDTH => 26, ProductTableMap::COL_LENGTH => 27, ProductTableMap::COL_WEIGHT => 28, ProductTableMap::COL_COMPOSITION => 29, ProductTableMap::COL_MATERIAL_ID => 30, ProductTableMap::COL_BRAND_ID => 31, ProductTableMap::COL_TITLE => 32, ProductTableMap::COL_DESCRIPTION => 33, ProductTableMap::COL_NOTE => 34, ProductTableMap::COL_CREATED_ON => 35, ProductTableMap::COL_DAY_PRICE => 36, ProductTableMap::COL_SALE_PRICE => 37, ProductTableMap::COL_DISCOUNT_PERCENTAGE => 38, ProductTableMap::COL_PURCHASE_PRICE => 39, ProductTableMap::COL_RESELLER_PRICE => 40, ProductTableMap::COL_ADVICE_PRICE => 41, ProductTableMap::COL_BULK_STOCK_ID => 42, ProductTableMap::COL_STOCK_QUANTITY => 43, ProductTableMap::COL_VIRTUAL_STOCK => 44, ProductTableMap::COL_QUANTITY_PER_PACK => 45, ProductTableMap::COL_YOUTUBE_URL => 46, ProductTableMap::COL_META_TITLE => 47, ProductTableMap::COL_META_DESCRIPTION => 48, ProductTableMap::COL_META_KEYWORD => 49, ProductTableMap::COL_HASHTAG => 50, ProductTableMap::COL_CALCULATED_AVERAGE_RATING => 51, ProductTableMap::COL_VAT_ID => 52, ProductTableMap::COL_UNIT_ID => 53, ProductTableMap::COL_CREATED_AT => 54, ProductTableMap::COL_UPDATED_AT => 55, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'derrived_from_id' => 1, 'old_id' => 2, 'import_tag' => 3, 'sku' => 4, 'ean' => 5, 'deleted_on' => 6, 'deleted_by_user_id' => 7, 'popularity' => 8, 'avg_rating' => 9, 'supplier' => 10, 'supplier_id' => 11, 'supplier_product_number' => 12, 'in_sale' => 13, 'in_spotlight' => 14, 'in_webshop' => 15, 'is_offer' => 16, 'is_bestseller' => 17, 'is_out_of_stock' => 18, 'has_images' => 19, 'category_id' => 20, 'number' => 21, 'advertiser_user_id' => 22, 'delivery_time_id' => 23, 'thickness' => 24, 'height' => 25, 'width' => 26, 'length' => 27, 'weight' => 28, 'composition' => 29, 'material_id' => 30, 'brand_id' => 31, 'title' => 32, 'description' => 33, 'note' => 34, 'created_on' => 35, 'day_price' => 36, 'sale_price' => 37, 'discount_percentage' => 38, 'purchase_price' => 39, 'reseller_price' => 40, 'advice_price' => 41, 'bulk_stock_id' => 42, 'stock_quantity' => 43, 'virtual_stock' => 44, 'quantity_per_pack' => 45, 'youtube_url' => 46, 'meta_title' => 47, 'meta_description' => 48, 'meta_keyword' => 49, 'hashtag' => 50, 'calculated_average_rating' => 51, 'vat_id' => 52, 'unit_id' => 53, 'created_at' => 54, 'updated_at' => 55, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('product');
        $this->setPhpName('Product');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Product');
        $this->setPackage('Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('derrived_from_id', 'DerrivedFromId', 'INTEGER', false, null, null);
        $this->addColumn('old_id', 'OldId', 'VARCHAR', false, 255, null);
        $this->addColumn('import_tag', 'ImportTag', 'VARCHAR', false, 255, null);
        $this->addColumn('sku', 'Sku', 'VARCHAR', false, 30, null);
        $this->addColumn('ean', 'Ean', 'VARCHAR', false, 30, null);
        $this->addColumn('deleted_on', 'DeletedOn', 'TIMESTAMP', false, null, null);
        $this->addColumn('deleted_by_user_id', 'DeletedByUserId', 'INTEGER', false, null, null);
        $this->addColumn('popularity', 'Popularity', 'INTEGER', false, null, null);
        $this->addColumn('avg_rating', 'AvgRating', 'INTEGER', false, null, null);
        $this->addColumn('supplier', 'Supplier', 'VARCHAR', false, 255, null);
        $this->addForeignKey('supplier_id', 'SupplierId', 'INTEGER', 'supplier', 'id', false, null, null);
        $this->addColumn('supplier_product_number', 'SupplierProductNumber', 'VARCHAR', false, 250, null);
        $this->addColumn('in_sale', 'InSale', 'BOOLEAN', false, 1, false);
        $this->addColumn('in_spotlight', 'InSpotlight', 'BOOLEAN', false, 1, false);
        $this->addColumn('in_webshop', 'InWebshop', 'BOOLEAN', false, 1, true);
        $this->addColumn('is_offer', 'IsOffer', 'BOOLEAN', false, 1, false);
        $this->addColumn('is_bestseller', 'IsBestseller', 'BOOLEAN', false, 1, false);
        $this->addColumn('is_out_of_stock', 'IsOutOfStock', 'BOOLEAN', false, 1, false);
        $this->addColumn('has_images', 'HasImages', 'BOOLEAN', false, 1, true);
        $this->addForeignKey('category_id', 'CategoryId', 'INTEGER', 'category', 'id', true, null, null);
        $this->addColumn('number', 'Number', 'VARCHAR', false, 150, null);
        $this->addForeignKey('advertiser_user_id', 'AdvertiserUserId', 'INTEGER', 'user', 'id', false, null, null);
        $this->addColumn('delivery_time_id', 'DeliveryTimeId', 'INTEGER', false, null, null);
        $this->addColumn('thickness', 'Thickness', 'DOUBLE', false, null, null);
        $this->addColumn('height', 'Height', 'VARCHAR', false, 50, null);
        $this->addColumn('width', 'Width', 'VARCHAR', false, 50, null);
        $this->addColumn('length', 'Length', 'VARCHAR', false, 50, null);
        $this->addColumn('weight', 'Weight', 'VARCHAR', false, 50, null);
        $this->addColumn('composition', 'Composition', 'VARCHAR', false, 150, null);
        $this->addColumn('material_id', 'MaterialId', 'VARCHAR', false, 50, null);
        $this->addForeignKey('brand_id', 'BrandId', 'INTEGER', 'mt_brand', 'id', false, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 250, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('note', 'Note', 'VARCHAR', false, 255, null);
        $this->addColumn('created_on', 'CreatedOn', 'TIMESTAMP', false, null, 'CURRENT_TIMESTAMP');
        $this->addColumn('day_price', 'DayPrice', 'FLOAT', false, null, null);
        $this->addColumn('sale_price', 'SalePrice', 'FLOAT', false, null, null);
        $this->addColumn('discount_percentage', 'DiscountPercentage', 'FLOAT', false, null, null);
        $this->addColumn('purchase_price', 'PurchasePrice', 'DECIMAL', false, 8, null);
        $this->addColumn('reseller_price', 'ResellerPrice', 'FLOAT', false, null, null);
        $this->addColumn('advice_price', 'AdvicePrice', 'FLOAT', false, null, null);
        $this->addColumn('bulk_stock_id', 'BulkStockId', 'INTEGER', false, null, null);
        $this->addColumn('stock_quantity', 'StockQuantity', 'INTEGER', false, null, null);
        $this->addColumn('virtual_stock', 'VirtualStock', 'BOOLEAN', false, 1, false);
        $this->addColumn('quantity_per_pack', 'QuantityPerPack', 'INTEGER', false, null, null);
        $this->addColumn('youtube_url', 'YoutubeUrl', 'LONGVARCHAR', false, null, null);
        $this->addColumn('meta_title', 'MetaTitle', 'VARCHAR', false, 255, null);
        $this->addColumn('meta_description', 'MetaDescription', 'LONGVARCHAR', false, null, null);
        $this->addColumn('meta_keyword', 'MetaKeyword', 'VARCHAR', false, 255, null);
        $this->addColumn('hashtag', 'Hashtag', 'VARCHAR', false, 255, null);
        $this->addColumn('calculated_average_rating', 'CalculatedAverageRating', 'INTEGER', false, 1, null);
        $this->addForeignKey('vat_id', 'VatId', 'INTEGER', 'mt_vat', 'id', false, null, null);
        $this->addForeignKey('unit_id', 'UnitId', 'INTEGER', 'mt_unit', 'id', false, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Brand', '\\Model\\Setting\\MasterTable\\Brand', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':brand_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('ProductUnit', '\\Model\\Setting\\MasterTable\\ProductUnit', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':unit_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('SupplierRef', '\\Model\\Supplier\\Supplier', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':supplier_id',
    1 => ':id',
  ),
), 'SET NULL', null, null, false);
        $this->addRelation('Category', '\\Model\\Category\\Category', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':category_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('User', '\\Model\\Account\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':advertiser_user_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('Vat', '\\Model\\Setting\\MasterTable\\Vat', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':vat_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('ProductMultiCategory', '\\Model\\Category\\ProductMultiCategory', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'ProductMultiCategories', false);
        $this->addRelation('SaleOrderItemProduct', '\\Model\\Sale\\OrderItemProduct', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'SaleOrderItemProducts', false);
        $this->addRelation('ProductReview', '\\Model\\ProductReview', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'ProductReviews', false);
        $this->addRelation('PromoProduct', '\\Model\\PromoProduct', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'PromoProducts', false);
        $this->addRelation('ProductProperty', '\\Model\\ProductProperty', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'ProductProperties', false);
        $this->addRelation('ProductColor', '\\Model\\ProductColor', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'ProductColors', false);
        $this->addRelation('ProductList', '\\Model\\ProductList', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'ProductLists', false);
        $this->addRelation('ProductUsage', '\\Model\\ProductUsage', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'ProductUsages', false);
        $this->addRelation('ProductTranslation', '\\Model\\ProductTranslation', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'ProductTranslations', false);
        $this->addRelation('RelatedProductRelatedByProductId', '\\Model\\Product\\RelatedProduct', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'RelatedProductsRelatedByProductId', false);
        $this->addRelation('RelatedProductRelatedByOtherProductId', '\\Model\\Product\\RelatedProduct', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':other_product_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'RelatedProductsRelatedByOtherProductId', false);
        $this->addRelation('ProductImage', '\\Model\\Product\\Image\\ProductImage', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'ProductImages', false);
        $this->addRelation('ProductTag', '\\Model\\Product\\ProductTag', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'ProductTags', false);
        $this->addRelation('SaleOrderItemStockClaimed', '\\Model\\Sale\\SaleOrderItemStockClaimed', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'SaleOrderItemStockClaimeds', false);
        $this->addRelation('CustomerWishlist', '\\Model\\Category\\CustomerWishlist', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'CASCADE', null, 'CustomerWishlists', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to product     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ProductMultiCategoryTableMap::clearInstancePool();
        OrderItemProductTableMap::clearInstancePool();
        ProductReviewTableMap::clearInstancePool();
        PromoProductTableMap::clearInstancePool();
        ProductPropertyTableMap::clearInstancePool();
        ProductColorTableMap::clearInstancePool();
        ProductListTableMap::clearInstancePool();
        ProductUsageTableMap::clearInstancePool();
        ProductTranslationTableMap::clearInstancePool();
        RelatedProductTableMap::clearInstancePool();
        ProductTagTableMap::clearInstancePool();
        CustomerWishlistTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ProductTableMap::CLASS_DEFAULT : ProductTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Product object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ProductTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ProductTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ProductTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ProductTableMap::OM_CLASS;
            /** @var Product $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ProductTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ProductTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ProductTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Product $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ProductTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ProductTableMap::COL_ID);
            $criteria->addSelectColumn(ProductTableMap::COL_DERRIVED_FROM_ID);
            $criteria->addSelectColumn(ProductTableMap::COL_OLD_ID);
            $criteria->addSelectColumn(ProductTableMap::COL_IMPORT_TAG);
            $criteria->addSelectColumn(ProductTableMap::COL_SKU);
            $criteria->addSelectColumn(ProductTableMap::COL_EAN);
            $criteria->addSelectColumn(ProductTableMap::COL_DELETED_ON);
            $criteria->addSelectColumn(ProductTableMap::COL_DELETED_BY_USER_ID);
            $criteria->addSelectColumn(ProductTableMap::COL_POPULARITY);
            $criteria->addSelectColumn(ProductTableMap::COL_AVG_RATING);
            $criteria->addSelectColumn(ProductTableMap::COL_SUPPLIER);
            $criteria->addSelectColumn(ProductTableMap::COL_SUPPLIER_ID);
            $criteria->addSelectColumn(ProductTableMap::COL_SUPPLIER_PRODUCT_NUMBER);
            $criteria->addSelectColumn(ProductTableMap::COL_IN_SALE);
            $criteria->addSelectColumn(ProductTableMap::COL_IN_SPOTLIGHT);
            $criteria->addSelectColumn(ProductTableMap::COL_IN_WEBSHOP);
            $criteria->addSelectColumn(ProductTableMap::COL_IS_OFFER);
            $criteria->addSelectColumn(ProductTableMap::COL_IS_BESTSELLER);
            $criteria->addSelectColumn(ProductTableMap::COL_IS_OUT_OF_STOCK);
            $criteria->addSelectColumn(ProductTableMap::COL_HAS_IMAGES);
            $criteria->addSelectColumn(ProductTableMap::COL_CATEGORY_ID);
            $criteria->addSelectColumn(ProductTableMap::COL_NUMBER);
            $criteria->addSelectColumn(ProductTableMap::COL_ADVERTISER_USER_ID);
            $criteria->addSelectColumn(ProductTableMap::COL_DELIVERY_TIME_ID);
            $criteria->addSelectColumn(ProductTableMap::COL_THICKNESS);
            $criteria->addSelectColumn(ProductTableMap::COL_HEIGHT);
            $criteria->addSelectColumn(ProductTableMap::COL_WIDTH);
            $criteria->addSelectColumn(ProductTableMap::COL_LENGTH);
            $criteria->addSelectColumn(ProductTableMap::COL_WEIGHT);
            $criteria->addSelectColumn(ProductTableMap::COL_COMPOSITION);
            $criteria->addSelectColumn(ProductTableMap::COL_MATERIAL_ID);
            $criteria->addSelectColumn(ProductTableMap::COL_BRAND_ID);
            $criteria->addSelectColumn(ProductTableMap::COL_TITLE);
            $criteria->addSelectColumn(ProductTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(ProductTableMap::COL_NOTE);
            $criteria->addSelectColumn(ProductTableMap::COL_CREATED_ON);
            $criteria->addSelectColumn(ProductTableMap::COL_DAY_PRICE);
            $criteria->addSelectColumn(ProductTableMap::COL_SALE_PRICE);
            $criteria->addSelectColumn(ProductTableMap::COL_DISCOUNT_PERCENTAGE);
            $criteria->addSelectColumn(ProductTableMap::COL_PURCHASE_PRICE);
            $criteria->addSelectColumn(ProductTableMap::COL_RESELLER_PRICE);
            $criteria->addSelectColumn(ProductTableMap::COL_ADVICE_PRICE);
            $criteria->addSelectColumn(ProductTableMap::COL_BULK_STOCK_ID);
            $criteria->addSelectColumn(ProductTableMap::COL_STOCK_QUANTITY);
            $criteria->addSelectColumn(ProductTableMap::COL_VIRTUAL_STOCK);
            $criteria->addSelectColumn(ProductTableMap::COL_QUANTITY_PER_PACK);
            $criteria->addSelectColumn(ProductTableMap::COL_YOUTUBE_URL);
            $criteria->addSelectColumn(ProductTableMap::COL_META_TITLE);
            $criteria->addSelectColumn(ProductTableMap::COL_META_DESCRIPTION);
            $criteria->addSelectColumn(ProductTableMap::COL_META_KEYWORD);
            $criteria->addSelectColumn(ProductTableMap::COL_HASHTAG);
            $criteria->addSelectColumn(ProductTableMap::COL_CALCULATED_AVERAGE_RATING);
            $criteria->addSelectColumn(ProductTableMap::COL_VAT_ID);
            $criteria->addSelectColumn(ProductTableMap::COL_UNIT_ID);
            $criteria->addSelectColumn(ProductTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(ProductTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.derrived_from_id');
            $criteria->addSelectColumn($alias . '.old_id');
            $criteria->addSelectColumn($alias . '.import_tag');
            $criteria->addSelectColumn($alias . '.sku');
            $criteria->addSelectColumn($alias . '.ean');
            $criteria->addSelectColumn($alias . '.deleted_on');
            $criteria->addSelectColumn($alias . '.deleted_by_user_id');
            $criteria->addSelectColumn($alias . '.popularity');
            $criteria->addSelectColumn($alias . '.avg_rating');
            $criteria->addSelectColumn($alias . '.supplier');
            $criteria->addSelectColumn($alias . '.supplier_id');
            $criteria->addSelectColumn($alias . '.supplier_product_number');
            $criteria->addSelectColumn($alias . '.in_sale');
            $criteria->addSelectColumn($alias . '.in_spotlight');
            $criteria->addSelectColumn($alias . '.in_webshop');
            $criteria->addSelectColumn($alias . '.is_offer');
            $criteria->addSelectColumn($alias . '.is_bestseller');
            $criteria->addSelectColumn($alias . '.is_out_of_stock');
            $criteria->addSelectColumn($alias . '.has_images');
            $criteria->addSelectColumn($alias . '.category_id');
            $criteria->addSelectColumn($alias . '.number');
            $criteria->addSelectColumn($alias . '.advertiser_user_id');
            $criteria->addSelectColumn($alias . '.delivery_time_id');
            $criteria->addSelectColumn($alias . '.thickness');
            $criteria->addSelectColumn($alias . '.height');
            $criteria->addSelectColumn($alias . '.width');
            $criteria->addSelectColumn($alias . '.length');
            $criteria->addSelectColumn($alias . '.weight');
            $criteria->addSelectColumn($alias . '.composition');
            $criteria->addSelectColumn($alias . '.material_id');
            $criteria->addSelectColumn($alias . '.brand_id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.note');
            $criteria->addSelectColumn($alias . '.created_on');
            $criteria->addSelectColumn($alias . '.day_price');
            $criteria->addSelectColumn($alias . '.sale_price');
            $criteria->addSelectColumn($alias . '.discount_percentage');
            $criteria->addSelectColumn($alias . '.purchase_price');
            $criteria->addSelectColumn($alias . '.reseller_price');
            $criteria->addSelectColumn($alias . '.advice_price');
            $criteria->addSelectColumn($alias . '.bulk_stock_id');
            $criteria->addSelectColumn($alias . '.stock_quantity');
            $criteria->addSelectColumn($alias . '.virtual_stock');
            $criteria->addSelectColumn($alias . '.quantity_per_pack');
            $criteria->addSelectColumn($alias . '.youtube_url');
            $criteria->addSelectColumn($alias . '.meta_title');
            $criteria->addSelectColumn($alias . '.meta_description');
            $criteria->addSelectColumn($alias . '.meta_keyword');
            $criteria->addSelectColumn($alias . '.hashtag');
            $criteria->addSelectColumn($alias . '.calculated_average_rating');
            $criteria->addSelectColumn($alias . '.vat_id');
            $criteria->addSelectColumn($alias . '.unit_id');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ProductTableMap::DATABASE_NAME)->getTable(ProductTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ProductTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ProductTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ProductTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Product or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Product object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Product) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ProductTableMap::DATABASE_NAME);
            $criteria->add(ProductTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ProductQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ProductTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ProductTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the product table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ProductQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Product or Criteria object.
     *
     * @param mixed               $criteria Criteria or Product object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Product object
        }

        if ($criteria->containsKey(ProductTableMap::COL_ID) && $criteria->keyContainsValue(ProductTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ProductTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ProductQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ProductTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ProductTableMap::buildTableMap();
