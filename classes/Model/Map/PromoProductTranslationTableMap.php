<?php

namespace Model\Map;

use Model\PromoProductTranslation;
use Model\PromoProductTranslationQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'promo_product_translation' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class PromoProductTranslationTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Map.PromoProductTranslationTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'promo_product_translation';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\PromoProductTranslation';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.PromoProductTranslation';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    const COL_ID = 'promo_product_translation.id';

    /**
     * the column name for the promo_product_id field
     */
    const COL_PROMO_PRODUCT_ID = 'promo_product_translation.promo_product_id';

    /**
     * the column name for the alt_url field
     */
    const COL_ALT_URL = 'promo_product_translation.alt_url';

    /**
     * the column name for the language_id field
     */
    const COL_LANGUAGE_ID = 'promo_product_translation.language_id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'promo_product_translation.title';

    /**
     * the column name for the description field
     */
    const COL_DESCRIPTION = 'promo_product_translation.description';

    /**
     * the column name for the has_image field
     */
    const COL_HAS_IMAGE = 'promo_product_translation.has_image';

    /**
     * the column name for the image_ext field
     */
    const COL_IMAGE_EXT = 'promo_product_translation.image_ext';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'PromoProductId', 'AltUrl', 'LanguageId', 'Title', 'Description', 'HasImage', 'ImageExt', ),
        self::TYPE_CAMELNAME     => array('id', 'promoProductId', 'altUrl', 'languageId', 'title', 'description', 'hasImage', 'imageExt', ),
        self::TYPE_COLNAME       => array(PromoProductTranslationTableMap::COL_ID, PromoProductTranslationTableMap::COL_PROMO_PRODUCT_ID, PromoProductTranslationTableMap::COL_ALT_URL, PromoProductTranslationTableMap::COL_LANGUAGE_ID, PromoProductTranslationTableMap::COL_TITLE, PromoProductTranslationTableMap::COL_DESCRIPTION, PromoProductTranslationTableMap::COL_HAS_IMAGE, PromoProductTranslationTableMap::COL_IMAGE_EXT, ),
        self::TYPE_FIELDNAME     => array('id', 'promo_product_id', 'alt_url', 'language_id', 'title', 'description', 'has_image', 'image_ext', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'PromoProductId' => 1, 'AltUrl' => 2, 'LanguageId' => 3, 'Title' => 4, 'Description' => 5, 'HasImage' => 6, 'ImageExt' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'promoProductId' => 1, 'altUrl' => 2, 'languageId' => 3, 'title' => 4, 'description' => 5, 'hasImage' => 6, 'imageExt' => 7, ),
        self::TYPE_COLNAME       => array(PromoProductTranslationTableMap::COL_ID => 0, PromoProductTranslationTableMap::COL_PROMO_PRODUCT_ID => 1, PromoProductTranslationTableMap::COL_ALT_URL => 2, PromoProductTranslationTableMap::COL_LANGUAGE_ID => 3, PromoProductTranslationTableMap::COL_TITLE => 4, PromoProductTranslationTableMap::COL_DESCRIPTION => 5, PromoProductTranslationTableMap::COL_HAS_IMAGE => 6, PromoProductTranslationTableMap::COL_IMAGE_EXT => 7, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'promo_product_id' => 1, 'alt_url' => 2, 'language_id' => 3, 'title' => 4, 'description' => 5, 'has_image' => 6, 'image_ext' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('promo_product_translation');
        $this->setPhpName('PromoProductTranslation');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\PromoProductTranslation');
        $this->setPackage('Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('promo_product_id', 'PromoProductId', 'INTEGER', 'promo_product', 'id', false, null, null);
        $this->addColumn('alt_url', 'AltUrl', 'VARCHAR', false, 250, null);
        $this->addForeignKey('language_id', 'LanguageId', 'INTEGER', 'mt_language', 'id', false, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 250, null);
        $this->addColumn('description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addColumn('has_image', 'HasImage', 'BOOLEAN', false, 1, null);
        $this->addColumn('image_ext', 'ImageExt', 'VARCHAR', false, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PromoProduct', '\\Model\\PromoProduct', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':promo_product_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
        $this->addRelation('Language', '\\Model\\Setting\\MasterTable\\Language', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':language_id',
    1 => ':id',
  ),
), 'CASCADE', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? PromoProductTranslationTableMap::CLASS_DEFAULT : PromoProductTranslationTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (PromoProductTranslation object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = PromoProductTranslationTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = PromoProductTranslationTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + PromoProductTranslationTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = PromoProductTranslationTableMap::OM_CLASS;
            /** @var PromoProductTranslation $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            PromoProductTranslationTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = PromoProductTranslationTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = PromoProductTranslationTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var PromoProductTranslation $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                PromoProductTranslationTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(PromoProductTranslationTableMap::COL_ID);
            $criteria->addSelectColumn(PromoProductTranslationTableMap::COL_PROMO_PRODUCT_ID);
            $criteria->addSelectColumn(PromoProductTranslationTableMap::COL_ALT_URL);
            $criteria->addSelectColumn(PromoProductTranslationTableMap::COL_LANGUAGE_ID);
            $criteria->addSelectColumn(PromoProductTranslationTableMap::COL_TITLE);
            $criteria->addSelectColumn(PromoProductTranslationTableMap::COL_DESCRIPTION);
            $criteria->addSelectColumn(PromoProductTranslationTableMap::COL_HAS_IMAGE);
            $criteria->addSelectColumn(PromoProductTranslationTableMap::COL_IMAGE_EXT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.promo_product_id');
            $criteria->addSelectColumn($alias . '.alt_url');
            $criteria->addSelectColumn($alias . '.language_id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.description');
            $criteria->addSelectColumn($alias . '.has_image');
            $criteria->addSelectColumn($alias . '.image_ext');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(PromoProductTranslationTableMap::DATABASE_NAME)->getTable(PromoProductTranslationTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(PromoProductTranslationTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(PromoProductTranslationTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new PromoProductTranslationTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a PromoProductTranslation or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or PromoProductTranslation object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PromoProductTranslationTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\PromoProductTranslation) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(PromoProductTranslationTableMap::DATABASE_NAME);
            $criteria->add(PromoProductTranslationTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = PromoProductTranslationQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            PromoProductTranslationTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                PromoProductTranslationTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the promo_product_translation table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return PromoProductTranslationQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a PromoProductTranslation or Criteria object.
     *
     * @param mixed               $criteria Criteria or PromoProductTranslation object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(PromoProductTranslationTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from PromoProductTranslation object
        }

        if ($criteria->containsKey(PromoProductTranslationTableMap::COL_ID) && $criteria->keyContainsValue(PromoProductTranslationTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.PromoProductTranslationTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = PromoProductTranslationQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // PromoProductTranslationTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
PromoProductTranslationTableMap::buildTableMap();
