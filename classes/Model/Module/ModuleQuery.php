<?php

namespace Model\Module;

use Model\Module\Base\ModuleQuery as BaseModuleQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'module' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ModuleQuery extends BaseModuleQuery
{

    public static function getUiComponentModule():Module
    {
        $oModuleQuery = ModuleQuery::create();
        $oModuleQuery->findOneByName('AdminModules\\System\\Ui_component_type\\Config');
        $oModule =  $oModuleQuery->findOneOrCreate();

        if(empty($sTitle = $oModule->getTitle()))
        {
            $oModule->setTitle('Ui components');
        }
        
        $oModule->save();
        return $oModule;
    }
}
