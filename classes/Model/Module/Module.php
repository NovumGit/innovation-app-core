<?php

namespace Model\Module;

use Model\Module\Base\Module as BaseModule;

/**
 * Skeleton subclass for representing a row from the 'module' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Module extends BaseModule
{

    function getTopLevelNamespace():string
    {
        $aNamespaceParts = array_reverse(explode('\\', $this->getName()));
        return $aNamespaceParts[1];
    }

}
