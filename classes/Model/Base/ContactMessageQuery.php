<?php

namespace Model\Base;

use \Exception;
use \PDO;
use Model\ContactMessage as ChildContactMessage;
use Model\ContactMessageQuery as ChildContactMessageQuery;
use Model\Crm\Customer;
use Model\Map\ContactMessageTableMap;
use Model\Setting\MasterTable\Language;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'contact_message' table.
 *
 *
 *
 * @method     ChildContactMessageQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildContactMessageQuery orderByIsNew($order = Criteria::ASC) Order by the is_new column
 * @method     ChildContactMessageQuery orderByLanguageId($order = Criteria::ASC) Order by the language_id column
 * @method     ChildContactMessageQuery orderByCustomerId($order = Criteria::ASC) Order by the customer_id column
 * @method     ChildContactMessageQuery orderByTypeId($order = Criteria::ASC) Order by the type_id column
 * @method     ChildContactMessageQuery orderByStatusId($order = Criteria::ASC) Order by the status_id column
 * @method     ChildContactMessageQuery orderByCompanyName($order = Criteria::ASC) Order by the company_name column
 * @method     ChildContactMessageQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildContactMessageQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method     ChildContactMessageQuery orderByPhone($order = Criteria::ASC) Order by the phone column
 * @method     ChildContactMessageQuery orderBySubject($order = Criteria::ASC) Order by the subject column
 * @method     ChildContactMessageQuery orderByMessage($order = Criteria::ASC) Order by the message column
 * @method     ChildContactMessageQuery orderByFormUrl($order = Criteria::ASC) Order by the form_url column
 * @method     ChildContactMessageQuery orderByReferrerSite($order = Criteria::ASC) Order by the referrer_site column
 * @method     ChildContactMessageQuery orderByHttpReferrer($order = Criteria::ASC) Order by the http_referrer column
 * @method     ChildContactMessageQuery orderByRemoteAddr($order = Criteria::ASC) Order by the remote_addr column
 * @method     ChildContactMessageQuery orderByHttpUserAgent($order = Criteria::ASC) Order by the http_user_agent column
 * @method     ChildContactMessageQuery orderByNote($order = Criteria::ASC) Order by the note column
 * @method     ChildContactMessageQuery orderByCreatedDate($order = Criteria::ASC) Order by the created_date column
 *
 * @method     ChildContactMessageQuery groupById() Group by the id column
 * @method     ChildContactMessageQuery groupByIsNew() Group by the is_new column
 * @method     ChildContactMessageQuery groupByLanguageId() Group by the language_id column
 * @method     ChildContactMessageQuery groupByCustomerId() Group by the customer_id column
 * @method     ChildContactMessageQuery groupByTypeId() Group by the type_id column
 * @method     ChildContactMessageQuery groupByStatusId() Group by the status_id column
 * @method     ChildContactMessageQuery groupByCompanyName() Group by the company_name column
 * @method     ChildContactMessageQuery groupByName() Group by the name column
 * @method     ChildContactMessageQuery groupByEmail() Group by the email column
 * @method     ChildContactMessageQuery groupByPhone() Group by the phone column
 * @method     ChildContactMessageQuery groupBySubject() Group by the subject column
 * @method     ChildContactMessageQuery groupByMessage() Group by the message column
 * @method     ChildContactMessageQuery groupByFormUrl() Group by the form_url column
 * @method     ChildContactMessageQuery groupByReferrerSite() Group by the referrer_site column
 * @method     ChildContactMessageQuery groupByHttpReferrer() Group by the http_referrer column
 * @method     ChildContactMessageQuery groupByRemoteAddr() Group by the remote_addr column
 * @method     ChildContactMessageQuery groupByHttpUserAgent() Group by the http_user_agent column
 * @method     ChildContactMessageQuery groupByNote() Group by the note column
 * @method     ChildContactMessageQuery groupByCreatedDate() Group by the created_date column
 *
 * @method     ChildContactMessageQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildContactMessageQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildContactMessageQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildContactMessageQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildContactMessageQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildContactMessageQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildContactMessageQuery leftJoinCustomer($relationAlias = null) Adds a LEFT JOIN clause to the query using the Customer relation
 * @method     ChildContactMessageQuery rightJoinCustomer($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Customer relation
 * @method     ChildContactMessageQuery innerJoinCustomer($relationAlias = null) Adds a INNER JOIN clause to the query using the Customer relation
 *
 * @method     ChildContactMessageQuery joinWithCustomer($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Customer relation
 *
 * @method     ChildContactMessageQuery leftJoinWithCustomer() Adds a LEFT JOIN clause and with to the query using the Customer relation
 * @method     ChildContactMessageQuery rightJoinWithCustomer() Adds a RIGHT JOIN clause and with to the query using the Customer relation
 * @method     ChildContactMessageQuery innerJoinWithCustomer() Adds a INNER JOIN clause and with to the query using the Customer relation
 *
 * @method     ChildContactMessageQuery leftJoinLanguage($relationAlias = null) Adds a LEFT JOIN clause to the query using the Language relation
 * @method     ChildContactMessageQuery rightJoinLanguage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Language relation
 * @method     ChildContactMessageQuery innerJoinLanguage($relationAlias = null) Adds a INNER JOIN clause to the query using the Language relation
 *
 * @method     ChildContactMessageQuery joinWithLanguage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Language relation
 *
 * @method     ChildContactMessageQuery leftJoinWithLanguage() Adds a LEFT JOIN clause and with to the query using the Language relation
 * @method     ChildContactMessageQuery rightJoinWithLanguage() Adds a RIGHT JOIN clause and with to the query using the Language relation
 * @method     ChildContactMessageQuery innerJoinWithLanguage() Adds a INNER JOIN clause and with to the query using the Language relation
 *
 * @method     ChildContactMessageQuery leftJoinContactMessageType($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContactMessageType relation
 * @method     ChildContactMessageQuery rightJoinContactMessageType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContactMessageType relation
 * @method     ChildContactMessageQuery innerJoinContactMessageType($relationAlias = null) Adds a INNER JOIN clause to the query using the ContactMessageType relation
 *
 * @method     ChildContactMessageQuery joinWithContactMessageType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContactMessageType relation
 *
 * @method     ChildContactMessageQuery leftJoinWithContactMessageType() Adds a LEFT JOIN clause and with to the query using the ContactMessageType relation
 * @method     ChildContactMessageQuery rightJoinWithContactMessageType() Adds a RIGHT JOIN clause and with to the query using the ContactMessageType relation
 * @method     ChildContactMessageQuery innerJoinWithContactMessageType() Adds a INNER JOIN clause and with to the query using the ContactMessageType relation
 *
 * @method     ChildContactMessageQuery leftJoinContactMessageStatus($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContactMessageStatus relation
 * @method     ChildContactMessageQuery rightJoinContactMessageStatus($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContactMessageStatus relation
 * @method     ChildContactMessageQuery innerJoinContactMessageStatus($relationAlias = null) Adds a INNER JOIN clause to the query using the ContactMessageStatus relation
 *
 * @method     ChildContactMessageQuery joinWithContactMessageStatus($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContactMessageStatus relation
 *
 * @method     ChildContactMessageQuery leftJoinWithContactMessageStatus() Adds a LEFT JOIN clause and with to the query using the ContactMessageStatus relation
 * @method     ChildContactMessageQuery rightJoinWithContactMessageStatus() Adds a RIGHT JOIN clause and with to the query using the ContactMessageStatus relation
 * @method     ChildContactMessageQuery innerJoinWithContactMessageStatus() Adds a INNER JOIN clause and with to the query using the ContactMessageStatus relation
 *
 * @method     \Model\Crm\CustomerQuery|\Model\Setting\MasterTable\LanguageQuery|\Model\ContactMessageTypeQuery|\Model\ContactMessageStatusQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildContactMessage findOne(ConnectionInterface $con = null) Return the first ChildContactMessage matching the query
 * @method     ChildContactMessage findOneOrCreate(ConnectionInterface $con = null) Return the first ChildContactMessage matching the query, or a new ChildContactMessage object populated from the query conditions when no match is found
 *
 * @method     ChildContactMessage findOneById(int $id) Return the first ChildContactMessage filtered by the id column
 * @method     ChildContactMessage findOneByIsNew(boolean $is_new) Return the first ChildContactMessage filtered by the is_new column
 * @method     ChildContactMessage findOneByLanguageId(int $language_id) Return the first ChildContactMessage filtered by the language_id column
 * @method     ChildContactMessage findOneByCustomerId(int $customer_id) Return the first ChildContactMessage filtered by the customer_id column
 * @method     ChildContactMessage findOneByTypeId(int $type_id) Return the first ChildContactMessage filtered by the type_id column
 * @method     ChildContactMessage findOneByStatusId(int $status_id) Return the first ChildContactMessage filtered by the status_id column
 * @method     ChildContactMessage findOneByCompanyName(string $company_name) Return the first ChildContactMessage filtered by the company_name column
 * @method     ChildContactMessage findOneByName(string $name) Return the first ChildContactMessage filtered by the name column
 * @method     ChildContactMessage findOneByEmail(string $email) Return the first ChildContactMessage filtered by the email column
 * @method     ChildContactMessage findOneByPhone(string $phone) Return the first ChildContactMessage filtered by the phone column
 * @method     ChildContactMessage findOneBySubject(string $subject) Return the first ChildContactMessage filtered by the subject column
 * @method     ChildContactMessage findOneByMessage(string $message) Return the first ChildContactMessage filtered by the message column
 * @method     ChildContactMessage findOneByFormUrl(string $form_url) Return the first ChildContactMessage filtered by the form_url column
 * @method     ChildContactMessage findOneByReferrerSite(string $referrer_site) Return the first ChildContactMessage filtered by the referrer_site column
 * @method     ChildContactMessage findOneByHttpReferrer(string $http_referrer) Return the first ChildContactMessage filtered by the http_referrer column
 * @method     ChildContactMessage findOneByRemoteAddr(string $remote_addr) Return the first ChildContactMessage filtered by the remote_addr column
 * @method     ChildContactMessage findOneByHttpUserAgent(string $http_user_agent) Return the first ChildContactMessage filtered by the http_user_agent column
 * @method     ChildContactMessage findOneByNote(string $note) Return the first ChildContactMessage filtered by the note column
 * @method     ChildContactMessage findOneByCreatedDate(string $created_date) Return the first ChildContactMessage filtered by the created_date column *

 * @method     ChildContactMessage requirePk($key, ConnectionInterface $con = null) Return the ChildContactMessage by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOne(ConnectionInterface $con = null) Return the first ChildContactMessage matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContactMessage requireOneById(int $id) Return the first ChildContactMessage filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByIsNew(boolean $is_new) Return the first ChildContactMessage filtered by the is_new column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByLanguageId(int $language_id) Return the first ChildContactMessage filtered by the language_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByCustomerId(int $customer_id) Return the first ChildContactMessage filtered by the customer_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByTypeId(int $type_id) Return the first ChildContactMessage filtered by the type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByStatusId(int $status_id) Return the first ChildContactMessage filtered by the status_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByCompanyName(string $company_name) Return the first ChildContactMessage filtered by the company_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByName(string $name) Return the first ChildContactMessage filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByEmail(string $email) Return the first ChildContactMessage filtered by the email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByPhone(string $phone) Return the first ChildContactMessage filtered by the phone column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneBySubject(string $subject) Return the first ChildContactMessage filtered by the subject column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByMessage(string $message) Return the first ChildContactMessage filtered by the message column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByFormUrl(string $form_url) Return the first ChildContactMessage filtered by the form_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByReferrerSite(string $referrer_site) Return the first ChildContactMessage filtered by the referrer_site column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByHttpReferrer(string $http_referrer) Return the first ChildContactMessage filtered by the http_referrer column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByRemoteAddr(string $remote_addr) Return the first ChildContactMessage filtered by the remote_addr column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByHttpUserAgent(string $http_user_agent) Return the first ChildContactMessage filtered by the http_user_agent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByNote(string $note) Return the first ChildContactMessage filtered by the note column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildContactMessage requireOneByCreatedDate(string $created_date) Return the first ChildContactMessage filtered by the created_date column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildContactMessage[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildContactMessage objects based on current ModelCriteria
 * @method     ChildContactMessage[]|ObjectCollection findById(int $id) Return ChildContactMessage objects filtered by the id column
 * @method     ChildContactMessage[]|ObjectCollection findByIsNew(boolean $is_new) Return ChildContactMessage objects filtered by the is_new column
 * @method     ChildContactMessage[]|ObjectCollection findByLanguageId(int $language_id) Return ChildContactMessage objects filtered by the language_id column
 * @method     ChildContactMessage[]|ObjectCollection findByCustomerId(int $customer_id) Return ChildContactMessage objects filtered by the customer_id column
 * @method     ChildContactMessage[]|ObjectCollection findByTypeId(int $type_id) Return ChildContactMessage objects filtered by the type_id column
 * @method     ChildContactMessage[]|ObjectCollection findByStatusId(int $status_id) Return ChildContactMessage objects filtered by the status_id column
 * @method     ChildContactMessage[]|ObjectCollection findByCompanyName(string $company_name) Return ChildContactMessage objects filtered by the company_name column
 * @method     ChildContactMessage[]|ObjectCollection findByName(string $name) Return ChildContactMessage objects filtered by the name column
 * @method     ChildContactMessage[]|ObjectCollection findByEmail(string $email) Return ChildContactMessage objects filtered by the email column
 * @method     ChildContactMessage[]|ObjectCollection findByPhone(string $phone) Return ChildContactMessage objects filtered by the phone column
 * @method     ChildContactMessage[]|ObjectCollection findBySubject(string $subject) Return ChildContactMessage objects filtered by the subject column
 * @method     ChildContactMessage[]|ObjectCollection findByMessage(string $message) Return ChildContactMessage objects filtered by the message column
 * @method     ChildContactMessage[]|ObjectCollection findByFormUrl(string $form_url) Return ChildContactMessage objects filtered by the form_url column
 * @method     ChildContactMessage[]|ObjectCollection findByReferrerSite(string $referrer_site) Return ChildContactMessage objects filtered by the referrer_site column
 * @method     ChildContactMessage[]|ObjectCollection findByHttpReferrer(string $http_referrer) Return ChildContactMessage objects filtered by the http_referrer column
 * @method     ChildContactMessage[]|ObjectCollection findByRemoteAddr(string $remote_addr) Return ChildContactMessage objects filtered by the remote_addr column
 * @method     ChildContactMessage[]|ObjectCollection findByHttpUserAgent(string $http_user_agent) Return ChildContactMessage objects filtered by the http_user_agent column
 * @method     ChildContactMessage[]|ObjectCollection findByNote(string $note) Return ChildContactMessage objects filtered by the note column
 * @method     ChildContactMessage[]|ObjectCollection findByCreatedDate(string $created_date) Return ChildContactMessage objects filtered by the created_date column
 * @method     ChildContactMessage[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ContactMessageQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Base\ContactMessageQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\ContactMessage', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildContactMessageQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildContactMessageQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildContactMessageQuery) {
            return $criteria;
        }
        $query = new ChildContactMessageQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildContactMessage|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContactMessageTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ContactMessageTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContactMessage A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, is_new, language_id, customer_id, type_id, status_id, company_name, name, email, phone, subject, message, form_url, referrer_site, http_referrer, remote_addr, http_user_agent, note, created_date FROM contact_message WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildContactMessage $obj */
            $obj = new ChildContactMessage();
            $obj->hydrate($row);
            ContactMessageTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildContactMessage|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ContactMessageTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ContactMessageTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ContactMessageTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ContactMessageTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the is_new column
     *
     * Example usage:
     * <code>
     * $query->filterByIsNew(true); // WHERE is_new = true
     * $query->filterByIsNew('yes'); // WHERE is_new = true
     * </code>
     *
     * @param     boolean|string $isNew The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByIsNew($isNew = null, $comparison = null)
    {
        if (is_string($isNew)) {
            $isNew = in_array(strtolower($isNew), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_IS_NEW, $isNew, $comparison);
    }

    /**
     * Filter the query on the language_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLanguageId(1234); // WHERE language_id = 1234
     * $query->filterByLanguageId(array(12, 34)); // WHERE language_id IN (12, 34)
     * $query->filterByLanguageId(array('min' => 12)); // WHERE language_id > 12
     * </code>
     *
     * @see       filterByLanguage()
     *
     * @param     mixed $languageId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByLanguageId($languageId = null, $comparison = null)
    {
        if (is_array($languageId)) {
            $useMinMax = false;
            if (isset($languageId['min'])) {
                $this->addUsingAlias(ContactMessageTableMap::COL_LANGUAGE_ID, $languageId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($languageId['max'])) {
                $this->addUsingAlias(ContactMessageTableMap::COL_LANGUAGE_ID, $languageId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_LANGUAGE_ID, $languageId, $comparison);
    }

    /**
     * Filter the query on the customer_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCustomerId(1234); // WHERE customer_id = 1234
     * $query->filterByCustomerId(array(12, 34)); // WHERE customer_id IN (12, 34)
     * $query->filterByCustomerId(array('min' => 12)); // WHERE customer_id > 12
     * </code>
     *
     * @see       filterByCustomer()
     *
     * @param     mixed $customerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByCustomerId($customerId = null, $comparison = null)
    {
        if (is_array($customerId)) {
            $useMinMax = false;
            if (isset($customerId['min'])) {
                $this->addUsingAlias(ContactMessageTableMap::COL_CUSTOMER_ID, $customerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($customerId['max'])) {
                $this->addUsingAlias(ContactMessageTableMap::COL_CUSTOMER_ID, $customerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_CUSTOMER_ID, $customerId, $comparison);
    }

    /**
     * Filter the query on the type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeId(1234); // WHERE type_id = 1234
     * $query->filterByTypeId(array(12, 34)); // WHERE type_id IN (12, 34)
     * $query->filterByTypeId(array('min' => 12)); // WHERE type_id > 12
     * </code>
     *
     * @see       filterByContactMessageType()
     *
     * @param     mixed $typeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByTypeId($typeId = null, $comparison = null)
    {
        if (is_array($typeId)) {
            $useMinMax = false;
            if (isset($typeId['min'])) {
                $this->addUsingAlias(ContactMessageTableMap::COL_TYPE_ID, $typeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($typeId['max'])) {
                $this->addUsingAlias(ContactMessageTableMap::COL_TYPE_ID, $typeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_TYPE_ID, $typeId, $comparison);
    }

    /**
     * Filter the query on the status_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusId(1234); // WHERE status_id = 1234
     * $query->filterByStatusId(array(12, 34)); // WHERE status_id IN (12, 34)
     * $query->filterByStatusId(array('min' => 12)); // WHERE status_id > 12
     * </code>
     *
     * @see       filterByContactMessageStatus()
     *
     * @param     mixed $statusId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByStatusId($statusId = null, $comparison = null)
    {
        if (is_array($statusId)) {
            $useMinMax = false;
            if (isset($statusId['min'])) {
                $this->addUsingAlias(ContactMessageTableMap::COL_STATUS_ID, $statusId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusId['max'])) {
                $this->addUsingAlias(ContactMessageTableMap::COL_STATUS_ID, $statusId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_STATUS_ID, $statusId, $comparison);
    }

    /**
     * Filter the query on the company_name column
     *
     * Example usage:
     * <code>
     * $query->filterByCompanyName('fooValue');   // WHERE company_name = 'fooValue'
     * $query->filterByCompanyName('%fooValue%', Criteria::LIKE); // WHERE company_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $companyName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByCompanyName($companyName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($companyName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_COMPANY_NAME, $companyName, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%', Criteria::LIKE); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the phone column
     *
     * Example usage:
     * <code>
     * $query->filterByPhone('fooValue');   // WHERE phone = 'fooValue'
     * $query->filterByPhone('%fooValue%', Criteria::LIKE); // WHERE phone LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phone The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByPhone($phone = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phone)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_PHONE, $phone, $comparison);
    }

    /**
     * Filter the query on the subject column
     *
     * Example usage:
     * <code>
     * $query->filterBySubject('fooValue');   // WHERE subject = 'fooValue'
     * $query->filterBySubject('%fooValue%', Criteria::LIKE); // WHERE subject LIKE '%fooValue%'
     * </code>
     *
     * @param     string $subject The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterBySubject($subject = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($subject)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_SUBJECT, $subject, $comparison);
    }

    /**
     * Filter the query on the message column
     *
     * Example usage:
     * <code>
     * $query->filterByMessage('fooValue');   // WHERE message = 'fooValue'
     * $query->filterByMessage('%fooValue%', Criteria::LIKE); // WHERE message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $message The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByMessage($message = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($message)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_MESSAGE, $message, $comparison);
    }

    /**
     * Filter the query on the form_url column
     *
     * Example usage:
     * <code>
     * $query->filterByFormUrl('fooValue');   // WHERE form_url = 'fooValue'
     * $query->filterByFormUrl('%fooValue%', Criteria::LIKE); // WHERE form_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $formUrl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByFormUrl($formUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($formUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_FORM_URL, $formUrl, $comparison);
    }

    /**
     * Filter the query on the referrer_site column
     *
     * Example usage:
     * <code>
     * $query->filterByReferrerSite('fooValue');   // WHERE referrer_site = 'fooValue'
     * $query->filterByReferrerSite('%fooValue%', Criteria::LIKE); // WHERE referrer_site LIKE '%fooValue%'
     * </code>
     *
     * @param     string $referrerSite The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByReferrerSite($referrerSite = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($referrerSite)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_REFERRER_SITE, $referrerSite, $comparison);
    }

    /**
     * Filter the query on the http_referrer column
     *
     * Example usage:
     * <code>
     * $query->filterByHttpReferrer('fooValue');   // WHERE http_referrer = 'fooValue'
     * $query->filterByHttpReferrer('%fooValue%', Criteria::LIKE); // WHERE http_referrer LIKE '%fooValue%'
     * </code>
     *
     * @param     string $httpReferrer The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByHttpReferrer($httpReferrer = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($httpReferrer)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_HTTP_REFERRER, $httpReferrer, $comparison);
    }

    /**
     * Filter the query on the remote_addr column
     *
     * Example usage:
     * <code>
     * $query->filterByRemoteAddr('fooValue');   // WHERE remote_addr = 'fooValue'
     * $query->filterByRemoteAddr('%fooValue%', Criteria::LIKE); // WHERE remote_addr LIKE '%fooValue%'
     * </code>
     *
     * @param     string $remoteAddr The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByRemoteAddr($remoteAddr = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($remoteAddr)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_REMOTE_ADDR, $remoteAddr, $comparison);
    }

    /**
     * Filter the query on the http_user_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByHttpUserAgent('fooValue');   // WHERE http_user_agent = 'fooValue'
     * $query->filterByHttpUserAgent('%fooValue%', Criteria::LIKE); // WHERE http_user_agent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $httpUserAgent The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByHttpUserAgent($httpUserAgent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($httpUserAgent)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_HTTP_USER_AGENT, $httpUserAgent, $comparison);
    }

    /**
     * Filter the query on the note column
     *
     * Example usage:
     * <code>
     * $query->filterByNote('fooValue');   // WHERE note = 'fooValue'
     * $query->filterByNote('%fooValue%', Criteria::LIKE); // WHERE note LIKE '%fooValue%'
     * </code>
     *
     * @param     string $note The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByNote($note = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($note)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_NOTE, $note, $comparison);
    }

    /**
     * Filter the query on the created_date column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedDate('2011-03-14'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate('now'); // WHERE created_date = '2011-03-14'
     * $query->filterByCreatedDate(array('max' => 'yesterday')); // WHERE created_date > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdDate The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByCreatedDate($createdDate = null, $comparison = null)
    {
        if (is_array($createdDate)) {
            $useMinMax = false;
            if (isset($createdDate['min'])) {
                $this->addUsingAlias(ContactMessageTableMap::COL_CREATED_DATE, $createdDate['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdDate['max'])) {
                $this->addUsingAlias(ContactMessageTableMap::COL_CREATED_DATE, $createdDate['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ContactMessageTableMap::COL_CREATED_DATE, $createdDate, $comparison);
    }

    /**
     * Filter the query by a related \Model\Crm\Customer object
     *
     * @param \Model\Crm\Customer|ObjectCollection $customer The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByCustomer($customer, $comparison = null)
    {
        if ($customer instanceof \Model\Crm\Customer) {
            return $this
                ->addUsingAlias(ContactMessageTableMap::COL_CUSTOMER_ID, $customer->getId(), $comparison);
        } elseif ($customer instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContactMessageTableMap::COL_CUSTOMER_ID, $customer->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCustomer() only accepts arguments of type \Model\Crm\Customer or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Customer relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function joinCustomer($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Customer');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Customer');
        }

        return $this;
    }

    /**
     * Use the Customer relation Customer object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Crm\CustomerQuery A secondary query class using the current class as primary query
     */
    public function useCustomerQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCustomer($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Customer', '\Model\Crm\CustomerQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Language object
     *
     * @param \Model\Setting\MasterTable\Language|ObjectCollection $language The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByLanguage($language, $comparison = null)
    {
        if ($language instanceof \Model\Setting\MasterTable\Language) {
            return $this
                ->addUsingAlias(ContactMessageTableMap::COL_LANGUAGE_ID, $language->getId(), $comparison);
        } elseif ($language instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContactMessageTableMap::COL_LANGUAGE_ID, $language->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByLanguage() only accepts arguments of type \Model\Setting\MasterTable\Language or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Language relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function joinLanguage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Language');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Language');
        }

        return $this;
    }

    /**
     * Use the Language relation Language object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\LanguageQuery A secondary query class using the current class as primary query
     */
    public function useLanguageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinLanguage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Language', '\Model\Setting\MasterTable\LanguageQuery');
    }

    /**
     * Filter the query by a related \Model\ContactMessageType object
     *
     * @param \Model\ContactMessageType|ObjectCollection $contactMessageType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByContactMessageType($contactMessageType, $comparison = null)
    {
        if ($contactMessageType instanceof \Model\ContactMessageType) {
            return $this
                ->addUsingAlias(ContactMessageTableMap::COL_TYPE_ID, $contactMessageType->getId(), $comparison);
        } elseif ($contactMessageType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContactMessageTableMap::COL_TYPE_ID, $contactMessageType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByContactMessageType() only accepts arguments of type \Model\ContactMessageType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContactMessageType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function joinContactMessageType($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContactMessageType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContactMessageType');
        }

        return $this;
    }

    /**
     * Use the ContactMessageType relation ContactMessageType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ContactMessageTypeQuery A secondary query class using the current class as primary query
     */
    public function useContactMessageTypeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinContactMessageType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContactMessageType', '\Model\ContactMessageTypeQuery');
    }

    /**
     * Filter the query by a related \Model\ContactMessageStatus object
     *
     * @param \Model\ContactMessageStatus|ObjectCollection $contactMessageStatus The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildContactMessageQuery The current query, for fluid interface
     */
    public function filterByContactMessageStatus($contactMessageStatus, $comparison = null)
    {
        if ($contactMessageStatus instanceof \Model\ContactMessageStatus) {
            return $this
                ->addUsingAlias(ContactMessageTableMap::COL_STATUS_ID, $contactMessageStatus->getId(), $comparison);
        } elseif ($contactMessageStatus instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ContactMessageTableMap::COL_STATUS_ID, $contactMessageStatus->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByContactMessageStatus() only accepts arguments of type \Model\ContactMessageStatus or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContactMessageStatus relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function joinContactMessageStatus($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContactMessageStatus');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContactMessageStatus');
        }

        return $this;
    }

    /**
     * Use the ContactMessageStatus relation ContactMessageStatus object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ContactMessageStatusQuery A secondary query class using the current class as primary query
     */
    public function useContactMessageStatusQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinContactMessageStatus($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContactMessageStatus', '\Model\ContactMessageStatusQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildContactMessage $contactMessage Object to remove from the list of results
     *
     * @return $this|ChildContactMessageQuery The current query, for fluid interface
     */
    public function prune($contactMessage = null)
    {
        if ($contactMessage) {
            $this->addUsingAlias(ContactMessageTableMap::COL_ID, $contactMessage->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the contact_message table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContactMessageTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ContactMessageTableMap::clearInstancePool();
            ContactMessageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContactMessageTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ContactMessageTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ContactMessageTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ContactMessageTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ContactMessageQuery
