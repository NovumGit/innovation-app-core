<?php

namespace Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\ContactMessage as ChildContactMessage;
use Model\ContactMessageQuery as ChildContactMessageQuery;
use Model\ContactMessageStatus as ChildContactMessageStatus;
use Model\ContactMessageStatusQuery as ChildContactMessageStatusQuery;
use Model\ContactMessageType as ChildContactMessageType;
use Model\ContactMessageTypeQuery as ChildContactMessageTypeQuery;
use Model\Crm\Customer;
use Model\Crm\CustomerQuery;
use Model\Map\ContactMessageTableMap;
use Model\Setting\MasterTable\Language;
use Model\Setting\MasterTable\LanguageQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'contact_message' table.
 *
 *
 *
 * @package    propel.generator.Model.Base
 */
abstract class ContactMessage implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Map\\ContactMessageTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the is_new field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean|null
     */
    protected $is_new;

    /**
     * The value for the language_id field.
     *
     * @var        int|null
     */
    protected $language_id;

    /**
     * The value for the customer_id field.
     *
     * @var        int|null
     */
    protected $customer_id;

    /**
     * The value for the type_id field.
     *
     * @var        int|null
     */
    protected $type_id;

    /**
     * The value for the status_id field.
     *
     * @var        int|null
     */
    protected $status_id;

    /**
     * The value for the company_name field.
     *
     * @var        string
     */
    protected $company_name;

    /**
     * The value for the name field.
     *
     * @var        string
     */
    protected $name;

    /**
     * The value for the email field.
     *
     * @var        string|null
     */
    protected $email;

    /**
     * The value for the phone field.
     *
     * @var        string|null
     */
    protected $phone;

    /**
     * The value for the subject field.
     *
     * @var        string
     */
    protected $subject;

    /**
     * The value for the message field.
     *
     * @var        string|null
     */
    protected $message;

    /**
     * The value for the form_url field.
     *
     * @var        string
     */
    protected $form_url;

    /**
     * The value for the referrer_site field.
     *
     * @var        string
     */
    protected $referrer_site;

    /**
     * The value for the http_referrer field.
     *
     * @var        string
     */
    protected $http_referrer;

    /**
     * The value for the remote_addr field.
     *
     * @var        string
     */
    protected $remote_addr;

    /**
     * The value for the http_user_agent field.
     *
     * @var        string
     */
    protected $http_user_agent;

    /**
     * The value for the note field.
     *
     * @var        string|null
     */
    protected $note;

    /**
     * The value for the created_date field.
     *
     * @var        DateTime|null
     */
    protected $created_date;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        Language
     */
    protected $aLanguage;

    /**
     * @var        Customer
     */
    protected $aCustomer;

    /**
     * @var        ChildContactMessageType
     */
    protected $aContactMessageType;

    /**
     * @var        ChildContactMessageStatus
     */
    protected $aContactMessageStatus;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_new = true;
    }

    /**
     * Initializes internal state of Model\Base\ContactMessage object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ContactMessage</code> instance.  If
     * <code>obj</code> is an instance of <code>ContactMessage</code>, delegates to
     * <code>equals(ContactMessage)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [is_new] column value.
     *
     * @return boolean|null
     */
    public function getIsNew()
    {
        return $this->is_new;
    }

    /**
     * Get the [language_id] column value.
     *
     * @return int|null
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * Get the [customer_id] column value.
     *
     * @return int|null
     */
    public function getCustomerId()
    {
        return $this->customer_id;
    }

    /**
     * Get the [type_id] column value.
     *
     * @return int|null
     */
    public function getTypeId()
    {
        return $this->type_id;
    }

    /**
     * Get the [status_id] column value.
     *
     * @return int|null
     */
    public function getStatusId()
    {
        return $this->status_id;
    }

    /**
     * Get the [company_name] column value.
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [email] column value.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get the [phone] column value.
     *
     * @return string|null
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Get the [subject] column value.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Get the [message] column value.
     *
     * @return string|null
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Get the [form_url] column value.
     *
     * @return string
     */
    public function getFormUrl()
    {
        return $this->form_url;
    }

    /**
     * Get the [referrer_site] column value.
     *
     * @return string
     */
    public function getReferrerSite()
    {
        return $this->referrer_site;
    }

    /**
     * Get the [http_referrer] column value.
     *
     * @return string
     */
    public function getHttpReferrer()
    {
        return $this->http_referrer;
    }

    /**
     * Get the [remote_addr] column value.
     *
     * @return string
     */
    public function getRemoteAddr()
    {
        return $this->remote_addr;
    }

    /**
     * Get the [http_user_agent] column value.
     *
     * @return string
     */
    public function getHttpUserAgent()
    {
        return $this->http_user_agent;
    }

    /**
     * Get the [note] column value.
     *
     * @return string|null
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Get the [optionally formatted] temporal [created_date] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedDate($format = null)
    {
        if ($format === null) {
            return $this->created_date;
        } else {
            return $this->created_date instanceof \DateTimeInterface ? $this->created_date->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Sets the value of the [is_new] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setIsNew($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_new !== $v) {
            $this->is_new = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_IS_NEW] = true;
        }

        return $this;
    } // setIsNew()

    /**
     * Set the value of [language_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setLanguageId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->language_id !== $v) {
            $this->language_id = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_LANGUAGE_ID] = true;
        }

        if ($this->aLanguage !== null && $this->aLanguage->getId() !== $v) {
            $this->aLanguage = null;
        }

        return $this;
    } // setLanguageId()

    /**
     * Set the value of [customer_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setCustomerId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->customer_id !== $v) {
            $this->customer_id = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_CUSTOMER_ID] = true;
        }

        if ($this->aCustomer !== null && $this->aCustomer->getId() !== $v) {
            $this->aCustomer = null;
        }

        return $this;
    } // setCustomerId()

    /**
     * Set the value of [type_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setTypeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->type_id !== $v) {
            $this->type_id = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_TYPE_ID] = true;
        }

        if ($this->aContactMessageType !== null && $this->aContactMessageType->getId() !== $v) {
            $this->aContactMessageType = null;
        }

        return $this;
    } // setTypeId()

    /**
     * Set the value of [status_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setStatusId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->status_id !== $v) {
            $this->status_id = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_STATUS_ID] = true;
        }

        if ($this->aContactMessageStatus !== null && $this->aContactMessageStatus->getId() !== $v) {
            $this->aContactMessageStatus = null;
        }

        return $this;
    } // setStatusId()

    /**
     * Set the value of [company_name] column.
     *
     * @param string $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setCompanyName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->company_name !== $v) {
            $this->company_name = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_COMPANY_NAME] = true;
        }

        return $this;
    } // setCompanyName()

    /**
     * Set the value of [name] column.
     *
     * @param string $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [email] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_EMAIL] = true;
        }

        return $this;
    } // setEmail()

    /**
     * Set the value of [phone] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setPhone($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->phone !== $v) {
            $this->phone = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_PHONE] = true;
        }

        return $this;
    } // setPhone()

    /**
     * Set the value of [subject] column.
     *
     * @param string $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setSubject($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->subject !== $v) {
            $this->subject = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_SUBJECT] = true;
        }

        return $this;
    } // setSubject()

    /**
     * Set the value of [message] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setMessage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->message !== $v) {
            $this->message = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_MESSAGE] = true;
        }

        return $this;
    } // setMessage()

    /**
     * Set the value of [form_url] column.
     *
     * @param string $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setFormUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->form_url !== $v) {
            $this->form_url = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_FORM_URL] = true;
        }

        return $this;
    } // setFormUrl()

    /**
     * Set the value of [referrer_site] column.
     *
     * @param string $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setReferrerSite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->referrer_site !== $v) {
            $this->referrer_site = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_REFERRER_SITE] = true;
        }

        return $this;
    } // setReferrerSite()

    /**
     * Set the value of [http_referrer] column.
     *
     * @param string $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setHttpReferrer($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->http_referrer !== $v) {
            $this->http_referrer = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_HTTP_REFERRER] = true;
        }

        return $this;
    } // setHttpReferrer()

    /**
     * Set the value of [remote_addr] column.
     *
     * @param string $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setRemoteAddr($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->remote_addr !== $v) {
            $this->remote_addr = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_REMOTE_ADDR] = true;
        }

        return $this;
    } // setRemoteAddr()

    /**
     * Set the value of [http_user_agent] column.
     *
     * @param string $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setHttpUserAgent($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->http_user_agent !== $v) {
            $this->http_user_agent = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_HTTP_USER_AGENT] = true;
        }

        return $this;
    } // setHttpUserAgent()

    /**
     * Set the value of [note] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setNote($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->note !== $v) {
            $this->note = $v;
            $this->modifiedColumns[ContactMessageTableMap::COL_NOTE] = true;
        }

        return $this;
    } // setNote()

    /**
     * Sets the value of [created_date] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setCreatedDate($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_date !== null || $dt !== null) {
            if ($this->created_date === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_date->format("Y-m-d H:i:s.u")) {
                $this->created_date = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContactMessageTableMap::COL_CREATED_DATE] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedDate()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContactMessageTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ContactMessageTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_new !== true) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ContactMessageTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ContactMessageTableMap::translateFieldName('IsNew', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_new = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ContactMessageTableMap::translateFieldName('LanguageId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->language_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ContactMessageTableMap::translateFieldName('CustomerId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->customer_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ContactMessageTableMap::translateFieldName('TypeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->type_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ContactMessageTableMap::translateFieldName('StatusId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->status_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ContactMessageTableMap::translateFieldName('CompanyName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->company_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ContactMessageTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ContactMessageTableMap::translateFieldName('Email', TableMap::TYPE_PHPNAME, $indexType)];
            $this->email = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ContactMessageTableMap::translateFieldName('Phone', TableMap::TYPE_PHPNAME, $indexType)];
            $this->phone = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ContactMessageTableMap::translateFieldName('Subject', TableMap::TYPE_PHPNAME, $indexType)];
            $this->subject = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ContactMessageTableMap::translateFieldName('Message', TableMap::TYPE_PHPNAME, $indexType)];
            $this->message = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ContactMessageTableMap::translateFieldName('FormUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->form_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ContactMessageTableMap::translateFieldName('ReferrerSite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->referrer_site = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ContactMessageTableMap::translateFieldName('HttpReferrer', TableMap::TYPE_PHPNAME, $indexType)];
            $this->http_referrer = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ContactMessageTableMap::translateFieldName('RemoteAddr', TableMap::TYPE_PHPNAME, $indexType)];
            $this->remote_addr = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ContactMessageTableMap::translateFieldName('HttpUserAgent', TableMap::TYPE_PHPNAME, $indexType)];
            $this->http_user_agent = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ContactMessageTableMap::translateFieldName('Note', TableMap::TYPE_PHPNAME, $indexType)];
            $this->note = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ContactMessageTableMap::translateFieldName('CreatedDate', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_date = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ContactMessageTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : ContactMessageTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 21; // 21 = ContactMessageTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\ContactMessage'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aLanguage !== null && $this->language_id !== $this->aLanguage->getId()) {
            $this->aLanguage = null;
        }
        if ($this->aCustomer !== null && $this->customer_id !== $this->aCustomer->getId()) {
            $this->aCustomer = null;
        }
        if ($this->aContactMessageType !== null && $this->type_id !== $this->aContactMessageType->getId()) {
            $this->aContactMessageType = null;
        }
        if ($this->aContactMessageStatus !== null && $this->status_id !== $this->aContactMessageStatus->getId()) {
            $this->aContactMessageStatus = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ContactMessageTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildContactMessageQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aLanguage = null;
            $this->aCustomer = null;
            $this->aContactMessageType = null;
            $this->aContactMessageStatus = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ContactMessage::setDeleted()
     * @see ContactMessage::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContactMessageTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildContactMessageQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ContactMessageTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(ContactMessageTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(ContactMessageTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(ContactMessageTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ContactMessageTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aLanguage !== null) {
                if ($this->aLanguage->isModified() || $this->aLanguage->isNew()) {
                    $affectedRows += $this->aLanguage->save($con);
                }
                $this->setLanguage($this->aLanguage);
            }

            if ($this->aCustomer !== null) {
                if ($this->aCustomer->isModified() || $this->aCustomer->isNew()) {
                    $affectedRows += $this->aCustomer->save($con);
                }
                $this->setCustomer($this->aCustomer);
            }

            if ($this->aContactMessageType !== null) {
                if ($this->aContactMessageType->isModified() || $this->aContactMessageType->isNew()) {
                    $affectedRows += $this->aContactMessageType->save($con);
                }
                $this->setContactMessageType($this->aContactMessageType);
            }

            if ($this->aContactMessageStatus !== null) {
                if ($this->aContactMessageStatus->isModified() || $this->aContactMessageStatus->isNew()) {
                    $affectedRows += $this->aContactMessageStatus->save($con);
                }
                $this->setContactMessageStatus($this->aContactMessageStatus);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ContactMessageTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ContactMessageTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ContactMessageTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_IS_NEW)) {
            $modifiedColumns[':p' . $index++]  = 'is_new';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_LANGUAGE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'language_id';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_CUSTOMER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'customer_id';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_TYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'type_id';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_STATUS_ID)) {
            $modifiedColumns[':p' . $index++]  = 'status_id';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_COMPANY_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'company_name';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_EMAIL)) {
            $modifiedColumns[':p' . $index++]  = 'email';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_PHONE)) {
            $modifiedColumns[':p' . $index++]  = 'phone';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_SUBJECT)) {
            $modifiedColumns[':p' . $index++]  = 'subject';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_MESSAGE)) {
            $modifiedColumns[':p' . $index++]  = 'message';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_FORM_URL)) {
            $modifiedColumns[':p' . $index++]  = 'form_url';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_REFERRER_SITE)) {
            $modifiedColumns[':p' . $index++]  = 'referrer_site';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_HTTP_REFERRER)) {
            $modifiedColumns[':p' . $index++]  = 'http_referrer';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_REMOTE_ADDR)) {
            $modifiedColumns[':p' . $index++]  = 'remote_addr';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_HTTP_USER_AGENT)) {
            $modifiedColumns[':p' . $index++]  = 'http_user_agent';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_NOTE)) {
            $modifiedColumns[':p' . $index++]  = 'note';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_CREATED_DATE)) {
            $modifiedColumns[':p' . $index++]  = 'created_date';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO contact_message (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'is_new':
                        $stmt->bindValue($identifier, (int) $this->is_new, PDO::PARAM_INT);
                        break;
                    case 'language_id':
                        $stmt->bindValue($identifier, $this->language_id, PDO::PARAM_INT);
                        break;
                    case 'customer_id':
                        $stmt->bindValue($identifier, $this->customer_id, PDO::PARAM_INT);
                        break;
                    case 'type_id':
                        $stmt->bindValue($identifier, $this->type_id, PDO::PARAM_INT);
                        break;
                    case 'status_id':
                        $stmt->bindValue($identifier, $this->status_id, PDO::PARAM_INT);
                        break;
                    case 'company_name':
                        $stmt->bindValue($identifier, $this->company_name, PDO::PARAM_STR);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'email':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case 'phone':
                        $stmt->bindValue($identifier, $this->phone, PDO::PARAM_STR);
                        break;
                    case 'subject':
                        $stmt->bindValue($identifier, $this->subject, PDO::PARAM_STR);
                        break;
                    case 'message':
                        $stmt->bindValue($identifier, $this->message, PDO::PARAM_STR);
                        break;
                    case 'form_url':
                        $stmt->bindValue($identifier, $this->form_url, PDO::PARAM_STR);
                        break;
                    case 'referrer_site':
                        $stmt->bindValue($identifier, $this->referrer_site, PDO::PARAM_STR);
                        break;
                    case 'http_referrer':
                        $stmt->bindValue($identifier, $this->http_referrer, PDO::PARAM_STR);
                        break;
                    case 'remote_addr':
                        $stmt->bindValue($identifier, $this->remote_addr, PDO::PARAM_STR);
                        break;
                    case 'http_user_agent':
                        $stmt->bindValue($identifier, $this->http_user_agent, PDO::PARAM_STR);
                        break;
                    case 'note':
                        $stmt->bindValue($identifier, $this->note, PDO::PARAM_STR);
                        break;
                    case 'created_date':
                        $stmt->bindValue($identifier, $this->created_date ? $this->created_date->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ContactMessageTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getIsNew();
                break;
            case 2:
                return $this->getLanguageId();
                break;
            case 3:
                return $this->getCustomerId();
                break;
            case 4:
                return $this->getTypeId();
                break;
            case 5:
                return $this->getStatusId();
                break;
            case 6:
                return $this->getCompanyName();
                break;
            case 7:
                return $this->getName();
                break;
            case 8:
                return $this->getEmail();
                break;
            case 9:
                return $this->getPhone();
                break;
            case 10:
                return $this->getSubject();
                break;
            case 11:
                return $this->getMessage();
                break;
            case 12:
                return $this->getFormUrl();
                break;
            case 13:
                return $this->getReferrerSite();
                break;
            case 14:
                return $this->getHttpReferrer();
                break;
            case 15:
                return $this->getRemoteAddr();
                break;
            case 16:
                return $this->getHttpUserAgent();
                break;
            case 17:
                return $this->getNote();
                break;
            case 18:
                return $this->getCreatedDate();
                break;
            case 19:
                return $this->getCreatedAt();
                break;
            case 20:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ContactMessage'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ContactMessage'][$this->hashCode()] = true;
        $keys = ContactMessageTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getIsNew(),
            $keys[2] => $this->getLanguageId(),
            $keys[3] => $this->getCustomerId(),
            $keys[4] => $this->getTypeId(),
            $keys[5] => $this->getStatusId(),
            $keys[6] => $this->getCompanyName(),
            $keys[7] => $this->getName(),
            $keys[8] => $this->getEmail(),
            $keys[9] => $this->getPhone(),
            $keys[10] => $this->getSubject(),
            $keys[11] => $this->getMessage(),
            $keys[12] => $this->getFormUrl(),
            $keys[13] => $this->getReferrerSite(),
            $keys[14] => $this->getHttpReferrer(),
            $keys[15] => $this->getRemoteAddr(),
            $keys[16] => $this->getHttpUserAgent(),
            $keys[17] => $this->getNote(),
            $keys[18] => $this->getCreatedDate(),
            $keys[19] => $this->getCreatedAt(),
            $keys[20] => $this->getUpdatedAt(),
        );
        if ($result[$keys[18]] instanceof \DateTimeInterface) {
            $result[$keys[18]] = $result[$keys[18]]->format('c');
        }

        if ($result[$keys[19]] instanceof \DateTimeInterface) {
            $result[$keys[19]] = $result[$keys[19]]->format('c');
        }

        if ($result[$keys[20]] instanceof \DateTimeInterface) {
            $result[$keys[20]] = $result[$keys[20]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aLanguage) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'language';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_language';
                        break;
                    default:
                        $key = 'Language';
                }

                $result[$key] = $this->aLanguage->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCustomer) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customer';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'customer';
                        break;
                    default:
                        $key = 'Customer';
                }

                $result[$key] = $this->aCustomer->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aContactMessageType) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contactMessageType';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contact_message_type';
                        break;
                    default:
                        $key = 'ContactMessageType';
                }

                $result[$key] = $this->aContactMessageType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aContactMessageStatus) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'contactMessageStatus';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'contact_message_status';
                        break;
                    default:
                        $key = 'ContactMessageStatus';
                }

                $result[$key] = $this->aContactMessageStatus->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\ContactMessage
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ContactMessageTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\ContactMessage
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setIsNew($value);
                break;
            case 2:
                $this->setLanguageId($value);
                break;
            case 3:
                $this->setCustomerId($value);
                break;
            case 4:
                $this->setTypeId($value);
                break;
            case 5:
                $this->setStatusId($value);
                break;
            case 6:
                $this->setCompanyName($value);
                break;
            case 7:
                $this->setName($value);
                break;
            case 8:
                $this->setEmail($value);
                break;
            case 9:
                $this->setPhone($value);
                break;
            case 10:
                $this->setSubject($value);
                break;
            case 11:
                $this->setMessage($value);
                break;
            case 12:
                $this->setFormUrl($value);
                break;
            case 13:
                $this->setReferrerSite($value);
                break;
            case 14:
                $this->setHttpReferrer($value);
                break;
            case 15:
                $this->setRemoteAddr($value);
                break;
            case 16:
                $this->setHttpUserAgent($value);
                break;
            case 17:
                $this->setNote($value);
                break;
            case 18:
                $this->setCreatedDate($value);
                break;
            case 19:
                $this->setCreatedAt($value);
                break;
            case 20:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ContactMessageTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setIsNew($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setLanguageId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setCustomerId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setTypeId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setStatusId($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setCompanyName($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setName($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setEmail($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setPhone($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setSubject($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setMessage($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setFormUrl($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setReferrerSite($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setHttpReferrer($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setRemoteAddr($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setHttpUserAgent($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setNote($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setCreatedDate($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setCreatedAt($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setUpdatedAt($arr[$keys[20]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\ContactMessage The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ContactMessageTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ContactMessageTableMap::COL_ID)) {
            $criteria->add(ContactMessageTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_IS_NEW)) {
            $criteria->add(ContactMessageTableMap::COL_IS_NEW, $this->is_new);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_LANGUAGE_ID)) {
            $criteria->add(ContactMessageTableMap::COL_LANGUAGE_ID, $this->language_id);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_CUSTOMER_ID)) {
            $criteria->add(ContactMessageTableMap::COL_CUSTOMER_ID, $this->customer_id);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_TYPE_ID)) {
            $criteria->add(ContactMessageTableMap::COL_TYPE_ID, $this->type_id);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_STATUS_ID)) {
            $criteria->add(ContactMessageTableMap::COL_STATUS_ID, $this->status_id);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_COMPANY_NAME)) {
            $criteria->add(ContactMessageTableMap::COL_COMPANY_NAME, $this->company_name);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_NAME)) {
            $criteria->add(ContactMessageTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_EMAIL)) {
            $criteria->add(ContactMessageTableMap::COL_EMAIL, $this->email);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_PHONE)) {
            $criteria->add(ContactMessageTableMap::COL_PHONE, $this->phone);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_SUBJECT)) {
            $criteria->add(ContactMessageTableMap::COL_SUBJECT, $this->subject);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_MESSAGE)) {
            $criteria->add(ContactMessageTableMap::COL_MESSAGE, $this->message);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_FORM_URL)) {
            $criteria->add(ContactMessageTableMap::COL_FORM_URL, $this->form_url);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_REFERRER_SITE)) {
            $criteria->add(ContactMessageTableMap::COL_REFERRER_SITE, $this->referrer_site);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_HTTP_REFERRER)) {
            $criteria->add(ContactMessageTableMap::COL_HTTP_REFERRER, $this->http_referrer);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_REMOTE_ADDR)) {
            $criteria->add(ContactMessageTableMap::COL_REMOTE_ADDR, $this->remote_addr);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_HTTP_USER_AGENT)) {
            $criteria->add(ContactMessageTableMap::COL_HTTP_USER_AGENT, $this->http_user_agent);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_NOTE)) {
            $criteria->add(ContactMessageTableMap::COL_NOTE, $this->note);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_CREATED_DATE)) {
            $criteria->add(ContactMessageTableMap::COL_CREATED_DATE, $this->created_date);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_CREATED_AT)) {
            $criteria->add(ContactMessageTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(ContactMessageTableMap::COL_UPDATED_AT)) {
            $criteria->add(ContactMessageTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildContactMessageQuery::create();
        $criteria->add(ContactMessageTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\ContactMessage (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setIsNew($this->getIsNew());
        $copyObj->setLanguageId($this->getLanguageId());
        $copyObj->setCustomerId($this->getCustomerId());
        $copyObj->setTypeId($this->getTypeId());
        $copyObj->setStatusId($this->getStatusId());
        $copyObj->setCompanyName($this->getCompanyName());
        $copyObj->setName($this->getName());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setPhone($this->getPhone());
        $copyObj->setSubject($this->getSubject());
        $copyObj->setMessage($this->getMessage());
        $copyObj->setFormUrl($this->getFormUrl());
        $copyObj->setReferrerSite($this->getReferrerSite());
        $copyObj->setHttpReferrer($this->getHttpReferrer());
        $copyObj->setRemoteAddr($this->getRemoteAddr());
        $copyObj->setHttpUserAgent($this->getHttpUserAgent());
        $copyObj->setNote($this->getNote());
        $copyObj->setCreatedDate($this->getCreatedDate());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\ContactMessage Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a Language object.
     *
     * @param  Language|null $v
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     * @throws PropelException
     */
    public function setLanguage(Language $v = null)
    {
        if ($v === null) {
            $this->setLanguageId(NULL);
        } else {
            $this->setLanguageId($v->getId());
        }

        $this->aLanguage = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Language object, it will not be re-added.
        if ($v !== null) {
            $v->addContactMessage($this);
        }


        return $this;
    }


    /**
     * Get the associated Language object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Language|null The associated Language object.
     * @throws PropelException
     */
    public function getLanguage(ConnectionInterface $con = null)
    {
        if ($this->aLanguage === null && ($this->language_id != 0)) {
            $this->aLanguage = LanguageQuery::create()->findPk($this->language_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aLanguage->addContactMessages($this);
             */
        }

        return $this->aLanguage;
    }

    /**
     * Declares an association between this object and a Customer object.
     *
     * @param  Customer|null $v
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCustomer(Customer $v = null)
    {
        if ($v === null) {
            $this->setCustomerId(NULL);
        } else {
            $this->setCustomerId($v->getId());
        }

        $this->aCustomer = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Customer object, it will not be re-added.
        if ($v !== null) {
            $v->addContactMessage($this);
        }


        return $this;
    }


    /**
     * Get the associated Customer object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Customer|null The associated Customer object.
     * @throws PropelException
     */
    public function getCustomer(ConnectionInterface $con = null)
    {
        if ($this->aCustomer === null && ($this->customer_id != 0)) {
            $this->aCustomer = CustomerQuery::create()->findPk($this->customer_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCustomer->addContactMessages($this);
             */
        }

        return $this->aCustomer;
    }

    /**
     * Declares an association between this object and a ChildContactMessageType object.
     *
     * @param  ChildContactMessageType|null $v
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     * @throws PropelException
     */
    public function setContactMessageType(ChildContactMessageType $v = null)
    {
        if ($v === null) {
            $this->setTypeId(NULL);
        } else {
            $this->setTypeId($v->getId());
        }

        $this->aContactMessageType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildContactMessageType object, it will not be re-added.
        if ($v !== null) {
            $v->addContactMessage($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildContactMessageType object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildContactMessageType|null The associated ChildContactMessageType object.
     * @throws PropelException
     */
    public function getContactMessageType(ConnectionInterface $con = null)
    {
        if ($this->aContactMessageType === null && ($this->type_id != 0)) {
            $this->aContactMessageType = ChildContactMessageTypeQuery::create()->findPk($this->type_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aContactMessageType->addContactMessages($this);
             */
        }

        return $this->aContactMessageType;
    }

    /**
     * Declares an association between this object and a ChildContactMessageStatus object.
     *
     * @param  ChildContactMessageStatus|null $v
     * @return $this|\Model\ContactMessage The current object (for fluent API support)
     * @throws PropelException
     */
    public function setContactMessageStatus(ChildContactMessageStatus $v = null)
    {
        if ($v === null) {
            $this->setStatusId(NULL);
        } else {
            $this->setStatusId($v->getId());
        }

        $this->aContactMessageStatus = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildContactMessageStatus object, it will not be re-added.
        if ($v !== null) {
            $v->addContactMessage($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildContactMessageStatus object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildContactMessageStatus|null The associated ChildContactMessageStatus object.
     * @throws PropelException
     */
    public function getContactMessageStatus(ConnectionInterface $con = null)
    {
        if ($this->aContactMessageStatus === null && ($this->status_id != 0)) {
            $this->aContactMessageStatus = ChildContactMessageStatusQuery::create()->findPk($this->status_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aContactMessageStatus->addContactMessages($this);
             */
        }

        return $this->aContactMessageStatus;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aLanguage) {
            $this->aLanguage->removeContactMessage($this);
        }
        if (null !== $this->aCustomer) {
            $this->aCustomer->removeContactMessage($this);
        }
        if (null !== $this->aContactMessageType) {
            $this->aContactMessageType->removeContactMessage($this);
        }
        if (null !== $this->aContactMessageStatus) {
            $this->aContactMessageStatus->removeContactMessage($this);
        }
        $this->id = null;
        $this->is_new = null;
        $this->language_id = null;
        $this->customer_id = null;
        $this->type_id = null;
        $this->status_id = null;
        $this->company_name = null;
        $this->name = null;
        $this->email = null;
        $this->phone = null;
        $this->subject = null;
        $this->message = null;
        $this->form_url = null;
        $this->referrer_site = null;
        $this->http_referrer = null;
        $this->remote_addr = null;
        $this->http_user_agent = null;
        $this->note = null;
        $this->created_date = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aLanguage = null;
        $this->aCustomer = null;
        $this->aContactMessageType = null;
        $this->aContactMessageStatus = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ContactMessageTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildContactMessage The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[ContactMessageTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
