<?php

namespace Model\Base;

use \Exception;
use \PDO;
use Model\Product as ChildProduct;
use Model\ProductQuery as ChildProductQuery;
use Model\Account\User;
use Model\Category\Category;
use Model\Category\CustomerWishlist;
use Model\Category\ProductMultiCategory;
use Model\Map\ProductTableMap;
use Model\Product\ProductTag;
use Model\Product\RelatedProduct;
use Model\Product\Image\ProductImage;
use Model\Sale\OrderItemProduct;
use Model\Sale\SaleOrderItemStockClaimed;
use Model\Setting\MasterTable\Brand;
use Model\Setting\MasterTable\ProductUnit;
use Model\Setting\MasterTable\Vat;
use Model\Supplier\Supplier;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'product' table.
 *
 *
 *
 * @method     ChildProductQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildProductQuery orderByDerrivedFromId($order = Criteria::ASC) Order by the derrived_from_id column
 * @method     ChildProductQuery orderByOldId($order = Criteria::ASC) Order by the old_id column
 * @method     ChildProductQuery orderByImportTag($order = Criteria::ASC) Order by the import_tag column
 * @method     ChildProductQuery orderBySku($order = Criteria::ASC) Order by the sku column
 * @method     ChildProductQuery orderByEan($order = Criteria::ASC) Order by the ean column
 * @method     ChildProductQuery orderByDeletedOn($order = Criteria::ASC) Order by the deleted_on column
 * @method     ChildProductQuery orderByDeletedByUserId($order = Criteria::ASC) Order by the deleted_by_user_id column
 * @method     ChildProductQuery orderByPopularity($order = Criteria::ASC) Order by the popularity column
 * @method     ChildProductQuery orderByAvgRating($order = Criteria::ASC) Order by the avg_rating column
 * @method     ChildProductQuery orderBySupplier($order = Criteria::ASC) Order by the supplier column
 * @method     ChildProductQuery orderBySupplierId($order = Criteria::ASC) Order by the supplier_id column
 * @method     ChildProductQuery orderBySupplierProductNumber($order = Criteria::ASC) Order by the supplier_product_number column
 * @method     ChildProductQuery orderByInSale($order = Criteria::ASC) Order by the in_sale column
 * @method     ChildProductQuery orderByInSpotlight($order = Criteria::ASC) Order by the in_spotlight column
 * @method     ChildProductQuery orderByInWebshop($order = Criteria::ASC) Order by the in_webshop column
 * @method     ChildProductQuery orderByIsOffer($order = Criteria::ASC) Order by the is_offer column
 * @method     ChildProductQuery orderByIsBestseller($order = Criteria::ASC) Order by the is_bestseller column
 * @method     ChildProductQuery orderByIsOutOfStock($order = Criteria::ASC) Order by the is_out_of_stock column
 * @method     ChildProductQuery orderByHasImages($order = Criteria::ASC) Order by the has_images column
 * @method     ChildProductQuery orderByCategoryId($order = Criteria::ASC) Order by the category_id column
 * @method     ChildProductQuery orderByNumber($order = Criteria::ASC) Order by the number column
 * @method     ChildProductQuery orderByAdvertiserUserId($order = Criteria::ASC) Order by the advertiser_user_id column
 * @method     ChildProductQuery orderByDeliveryTimeId($order = Criteria::ASC) Order by the delivery_time_id column
 * @method     ChildProductQuery orderByThickness($order = Criteria::ASC) Order by the thickness column
 * @method     ChildProductQuery orderByHeight($order = Criteria::ASC) Order by the height column
 * @method     ChildProductQuery orderByWidth($order = Criteria::ASC) Order by the width column
 * @method     ChildProductQuery orderByLength($order = Criteria::ASC) Order by the length column
 * @method     ChildProductQuery orderByWeight($order = Criteria::ASC) Order by the weight column
 * @method     ChildProductQuery orderByComposition($order = Criteria::ASC) Order by the composition column
 * @method     ChildProductQuery orderByMaterialId($order = Criteria::ASC) Order by the material_id column
 * @method     ChildProductQuery orderByBrandId($order = Criteria::ASC) Order by the brand_id column
 * @method     ChildProductQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildProductQuery orderByDescription($order = Criteria::ASC) Order by the description column
 * @method     ChildProductQuery orderByNote($order = Criteria::ASC) Order by the note column
 * @method     ChildProductQuery orderByCreatedOn($order = Criteria::ASC) Order by the created_on column
 * @method     ChildProductQuery orderByDayPrice($order = Criteria::ASC) Order by the day_price column
 * @method     ChildProductQuery orderBySalePrice($order = Criteria::ASC) Order by the sale_price column
 * @method     ChildProductQuery orderByDiscountPercentage($order = Criteria::ASC) Order by the discount_percentage column
 * @method     ChildProductQuery orderByPurchasePrice($order = Criteria::ASC) Order by the purchase_price column
 * @method     ChildProductQuery orderByResellerPrice($order = Criteria::ASC) Order by the reseller_price column
 * @method     ChildProductQuery orderByAdvicePrice($order = Criteria::ASC) Order by the advice_price column
 * @method     ChildProductQuery orderByBulkStockId($order = Criteria::ASC) Order by the bulk_stock_id column
 * @method     ChildProductQuery orderByStockQuantity($order = Criteria::ASC) Order by the stock_quantity column
 * @method     ChildProductQuery orderByVirtualStock($order = Criteria::ASC) Order by the virtual_stock column
 * @method     ChildProductQuery orderByQuantityPerPack($order = Criteria::ASC) Order by the quantity_per_pack column
 * @method     ChildProductQuery orderByYoutubeUrl($order = Criteria::ASC) Order by the youtube_url column
 * @method     ChildProductQuery orderByMetaTitle($order = Criteria::ASC) Order by the meta_title column
 * @method     ChildProductQuery orderByMetaDescription($order = Criteria::ASC) Order by the meta_description column
 * @method     ChildProductQuery orderByMetaKeyword($order = Criteria::ASC) Order by the meta_keyword column
 * @method     ChildProductQuery orderByHashtag($order = Criteria::ASC) Order by the hashtag column
 * @method     ChildProductQuery orderByCalculatedAverageRating($order = Criteria::ASC) Order by the calculated_average_rating column
 * @method     ChildProductQuery orderByVatId($order = Criteria::ASC) Order by the vat_id column
 * @method     ChildProductQuery orderByUnitId($order = Criteria::ASC) Order by the unit_id column
 * @method     ChildProductQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildProductQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildProductQuery groupById() Group by the id column
 * @method     ChildProductQuery groupByDerrivedFromId() Group by the derrived_from_id column
 * @method     ChildProductQuery groupByOldId() Group by the old_id column
 * @method     ChildProductQuery groupByImportTag() Group by the import_tag column
 * @method     ChildProductQuery groupBySku() Group by the sku column
 * @method     ChildProductQuery groupByEan() Group by the ean column
 * @method     ChildProductQuery groupByDeletedOn() Group by the deleted_on column
 * @method     ChildProductQuery groupByDeletedByUserId() Group by the deleted_by_user_id column
 * @method     ChildProductQuery groupByPopularity() Group by the popularity column
 * @method     ChildProductQuery groupByAvgRating() Group by the avg_rating column
 * @method     ChildProductQuery groupBySupplier() Group by the supplier column
 * @method     ChildProductQuery groupBySupplierId() Group by the supplier_id column
 * @method     ChildProductQuery groupBySupplierProductNumber() Group by the supplier_product_number column
 * @method     ChildProductQuery groupByInSale() Group by the in_sale column
 * @method     ChildProductQuery groupByInSpotlight() Group by the in_spotlight column
 * @method     ChildProductQuery groupByInWebshop() Group by the in_webshop column
 * @method     ChildProductQuery groupByIsOffer() Group by the is_offer column
 * @method     ChildProductQuery groupByIsBestseller() Group by the is_bestseller column
 * @method     ChildProductQuery groupByIsOutOfStock() Group by the is_out_of_stock column
 * @method     ChildProductQuery groupByHasImages() Group by the has_images column
 * @method     ChildProductQuery groupByCategoryId() Group by the category_id column
 * @method     ChildProductQuery groupByNumber() Group by the number column
 * @method     ChildProductQuery groupByAdvertiserUserId() Group by the advertiser_user_id column
 * @method     ChildProductQuery groupByDeliveryTimeId() Group by the delivery_time_id column
 * @method     ChildProductQuery groupByThickness() Group by the thickness column
 * @method     ChildProductQuery groupByHeight() Group by the height column
 * @method     ChildProductQuery groupByWidth() Group by the width column
 * @method     ChildProductQuery groupByLength() Group by the length column
 * @method     ChildProductQuery groupByWeight() Group by the weight column
 * @method     ChildProductQuery groupByComposition() Group by the composition column
 * @method     ChildProductQuery groupByMaterialId() Group by the material_id column
 * @method     ChildProductQuery groupByBrandId() Group by the brand_id column
 * @method     ChildProductQuery groupByTitle() Group by the title column
 * @method     ChildProductQuery groupByDescription() Group by the description column
 * @method     ChildProductQuery groupByNote() Group by the note column
 * @method     ChildProductQuery groupByCreatedOn() Group by the created_on column
 * @method     ChildProductQuery groupByDayPrice() Group by the day_price column
 * @method     ChildProductQuery groupBySalePrice() Group by the sale_price column
 * @method     ChildProductQuery groupByDiscountPercentage() Group by the discount_percentage column
 * @method     ChildProductQuery groupByPurchasePrice() Group by the purchase_price column
 * @method     ChildProductQuery groupByResellerPrice() Group by the reseller_price column
 * @method     ChildProductQuery groupByAdvicePrice() Group by the advice_price column
 * @method     ChildProductQuery groupByBulkStockId() Group by the bulk_stock_id column
 * @method     ChildProductQuery groupByStockQuantity() Group by the stock_quantity column
 * @method     ChildProductQuery groupByVirtualStock() Group by the virtual_stock column
 * @method     ChildProductQuery groupByQuantityPerPack() Group by the quantity_per_pack column
 * @method     ChildProductQuery groupByYoutubeUrl() Group by the youtube_url column
 * @method     ChildProductQuery groupByMetaTitle() Group by the meta_title column
 * @method     ChildProductQuery groupByMetaDescription() Group by the meta_description column
 * @method     ChildProductQuery groupByMetaKeyword() Group by the meta_keyword column
 * @method     ChildProductQuery groupByHashtag() Group by the hashtag column
 * @method     ChildProductQuery groupByCalculatedAverageRating() Group by the calculated_average_rating column
 * @method     ChildProductQuery groupByVatId() Group by the vat_id column
 * @method     ChildProductQuery groupByUnitId() Group by the unit_id column
 * @method     ChildProductQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildProductQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildProductQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProductQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProductQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProductQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildProductQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildProductQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildProductQuery leftJoinBrand($relationAlias = null) Adds a LEFT JOIN clause to the query using the Brand relation
 * @method     ChildProductQuery rightJoinBrand($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Brand relation
 * @method     ChildProductQuery innerJoinBrand($relationAlias = null) Adds a INNER JOIN clause to the query using the Brand relation
 *
 * @method     ChildProductQuery joinWithBrand($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Brand relation
 *
 * @method     ChildProductQuery leftJoinWithBrand() Adds a LEFT JOIN clause and with to the query using the Brand relation
 * @method     ChildProductQuery rightJoinWithBrand() Adds a RIGHT JOIN clause and with to the query using the Brand relation
 * @method     ChildProductQuery innerJoinWithBrand() Adds a INNER JOIN clause and with to the query using the Brand relation
 *
 * @method     ChildProductQuery leftJoinProductUnit($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductUnit relation
 * @method     ChildProductQuery rightJoinProductUnit($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductUnit relation
 * @method     ChildProductQuery innerJoinProductUnit($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductUnit relation
 *
 * @method     ChildProductQuery joinWithProductUnit($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductUnit relation
 *
 * @method     ChildProductQuery leftJoinWithProductUnit() Adds a LEFT JOIN clause and with to the query using the ProductUnit relation
 * @method     ChildProductQuery rightJoinWithProductUnit() Adds a RIGHT JOIN clause and with to the query using the ProductUnit relation
 * @method     ChildProductQuery innerJoinWithProductUnit() Adds a INNER JOIN clause and with to the query using the ProductUnit relation
 *
 * @method     ChildProductQuery leftJoinSupplierRef($relationAlias = null) Adds a LEFT JOIN clause to the query using the SupplierRef relation
 * @method     ChildProductQuery rightJoinSupplierRef($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SupplierRef relation
 * @method     ChildProductQuery innerJoinSupplierRef($relationAlias = null) Adds a INNER JOIN clause to the query using the SupplierRef relation
 *
 * @method     ChildProductQuery joinWithSupplierRef($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SupplierRef relation
 *
 * @method     ChildProductQuery leftJoinWithSupplierRef() Adds a LEFT JOIN clause and with to the query using the SupplierRef relation
 * @method     ChildProductQuery rightJoinWithSupplierRef() Adds a RIGHT JOIN clause and with to the query using the SupplierRef relation
 * @method     ChildProductQuery innerJoinWithSupplierRef() Adds a INNER JOIN clause and with to the query using the SupplierRef relation
 *
 * @method     ChildProductQuery leftJoinCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the Category relation
 * @method     ChildProductQuery rightJoinCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Category relation
 * @method     ChildProductQuery innerJoinCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the Category relation
 *
 * @method     ChildProductQuery joinWithCategory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Category relation
 *
 * @method     ChildProductQuery leftJoinWithCategory() Adds a LEFT JOIN clause and with to the query using the Category relation
 * @method     ChildProductQuery rightJoinWithCategory() Adds a RIGHT JOIN clause and with to the query using the Category relation
 * @method     ChildProductQuery innerJoinWithCategory() Adds a INNER JOIN clause and with to the query using the Category relation
 *
 * @method     ChildProductQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildProductQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildProductQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildProductQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildProductQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildProductQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildProductQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     ChildProductQuery leftJoinVat($relationAlias = null) Adds a LEFT JOIN clause to the query using the Vat relation
 * @method     ChildProductQuery rightJoinVat($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Vat relation
 * @method     ChildProductQuery innerJoinVat($relationAlias = null) Adds a INNER JOIN clause to the query using the Vat relation
 *
 * @method     ChildProductQuery joinWithVat($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Vat relation
 *
 * @method     ChildProductQuery leftJoinWithVat() Adds a LEFT JOIN clause and with to the query using the Vat relation
 * @method     ChildProductQuery rightJoinWithVat() Adds a RIGHT JOIN clause and with to the query using the Vat relation
 * @method     ChildProductQuery innerJoinWithVat() Adds a INNER JOIN clause and with to the query using the Vat relation
 *
 * @method     ChildProductQuery leftJoinProductMultiCategory($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductMultiCategory relation
 * @method     ChildProductQuery rightJoinProductMultiCategory($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductMultiCategory relation
 * @method     ChildProductQuery innerJoinProductMultiCategory($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductMultiCategory relation
 *
 * @method     ChildProductQuery joinWithProductMultiCategory($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductMultiCategory relation
 *
 * @method     ChildProductQuery leftJoinWithProductMultiCategory() Adds a LEFT JOIN clause and with to the query using the ProductMultiCategory relation
 * @method     ChildProductQuery rightJoinWithProductMultiCategory() Adds a RIGHT JOIN clause and with to the query using the ProductMultiCategory relation
 * @method     ChildProductQuery innerJoinWithProductMultiCategory() Adds a INNER JOIN clause and with to the query using the ProductMultiCategory relation
 *
 * @method     ChildProductQuery leftJoinSaleOrderItemProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrderItemProduct relation
 * @method     ChildProductQuery rightJoinSaleOrderItemProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrderItemProduct relation
 * @method     ChildProductQuery innerJoinSaleOrderItemProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrderItemProduct relation
 *
 * @method     ChildProductQuery joinWithSaleOrderItemProduct($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrderItemProduct relation
 *
 * @method     ChildProductQuery leftJoinWithSaleOrderItemProduct() Adds a LEFT JOIN clause and with to the query using the SaleOrderItemProduct relation
 * @method     ChildProductQuery rightJoinWithSaleOrderItemProduct() Adds a RIGHT JOIN clause and with to the query using the SaleOrderItemProduct relation
 * @method     ChildProductQuery innerJoinWithSaleOrderItemProduct() Adds a INNER JOIN clause and with to the query using the SaleOrderItemProduct relation
 *
 * @method     ChildProductQuery leftJoinProductReview($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductReview relation
 * @method     ChildProductQuery rightJoinProductReview($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductReview relation
 * @method     ChildProductQuery innerJoinProductReview($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductReview relation
 *
 * @method     ChildProductQuery joinWithProductReview($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductReview relation
 *
 * @method     ChildProductQuery leftJoinWithProductReview() Adds a LEFT JOIN clause and with to the query using the ProductReview relation
 * @method     ChildProductQuery rightJoinWithProductReview() Adds a RIGHT JOIN clause and with to the query using the ProductReview relation
 * @method     ChildProductQuery innerJoinWithProductReview() Adds a INNER JOIN clause and with to the query using the ProductReview relation
 *
 * @method     ChildProductQuery leftJoinPromoProduct($relationAlias = null) Adds a LEFT JOIN clause to the query using the PromoProduct relation
 * @method     ChildProductQuery rightJoinPromoProduct($relationAlias = null) Adds a RIGHT JOIN clause to the query using the PromoProduct relation
 * @method     ChildProductQuery innerJoinPromoProduct($relationAlias = null) Adds a INNER JOIN clause to the query using the PromoProduct relation
 *
 * @method     ChildProductQuery joinWithPromoProduct($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the PromoProduct relation
 *
 * @method     ChildProductQuery leftJoinWithPromoProduct() Adds a LEFT JOIN clause and with to the query using the PromoProduct relation
 * @method     ChildProductQuery rightJoinWithPromoProduct() Adds a RIGHT JOIN clause and with to the query using the PromoProduct relation
 * @method     ChildProductQuery innerJoinWithPromoProduct() Adds a INNER JOIN clause and with to the query using the PromoProduct relation
 *
 * @method     ChildProductQuery leftJoinProductProperty($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductProperty relation
 * @method     ChildProductQuery rightJoinProductProperty($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductProperty relation
 * @method     ChildProductQuery innerJoinProductProperty($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductProperty relation
 *
 * @method     ChildProductQuery joinWithProductProperty($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductProperty relation
 *
 * @method     ChildProductQuery leftJoinWithProductProperty() Adds a LEFT JOIN clause and with to the query using the ProductProperty relation
 * @method     ChildProductQuery rightJoinWithProductProperty() Adds a RIGHT JOIN clause and with to the query using the ProductProperty relation
 * @method     ChildProductQuery innerJoinWithProductProperty() Adds a INNER JOIN clause and with to the query using the ProductProperty relation
 *
 * @method     ChildProductQuery leftJoinProductColor($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductColor relation
 * @method     ChildProductQuery rightJoinProductColor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductColor relation
 * @method     ChildProductQuery innerJoinProductColor($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductColor relation
 *
 * @method     ChildProductQuery joinWithProductColor($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductColor relation
 *
 * @method     ChildProductQuery leftJoinWithProductColor() Adds a LEFT JOIN clause and with to the query using the ProductColor relation
 * @method     ChildProductQuery rightJoinWithProductColor() Adds a RIGHT JOIN clause and with to the query using the ProductColor relation
 * @method     ChildProductQuery innerJoinWithProductColor() Adds a INNER JOIN clause and with to the query using the ProductColor relation
 *
 * @method     ChildProductQuery leftJoinProductList($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductList relation
 * @method     ChildProductQuery rightJoinProductList($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductList relation
 * @method     ChildProductQuery innerJoinProductList($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductList relation
 *
 * @method     ChildProductQuery joinWithProductList($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductList relation
 *
 * @method     ChildProductQuery leftJoinWithProductList() Adds a LEFT JOIN clause and with to the query using the ProductList relation
 * @method     ChildProductQuery rightJoinWithProductList() Adds a RIGHT JOIN clause and with to the query using the ProductList relation
 * @method     ChildProductQuery innerJoinWithProductList() Adds a INNER JOIN clause and with to the query using the ProductList relation
 *
 * @method     ChildProductQuery leftJoinProductUsage($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductUsage relation
 * @method     ChildProductQuery rightJoinProductUsage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductUsage relation
 * @method     ChildProductQuery innerJoinProductUsage($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductUsage relation
 *
 * @method     ChildProductQuery joinWithProductUsage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductUsage relation
 *
 * @method     ChildProductQuery leftJoinWithProductUsage() Adds a LEFT JOIN clause and with to the query using the ProductUsage relation
 * @method     ChildProductQuery rightJoinWithProductUsage() Adds a RIGHT JOIN clause and with to the query using the ProductUsage relation
 * @method     ChildProductQuery innerJoinWithProductUsage() Adds a INNER JOIN clause and with to the query using the ProductUsage relation
 *
 * @method     ChildProductQuery leftJoinProductTranslation($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductTranslation relation
 * @method     ChildProductQuery rightJoinProductTranslation($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductTranslation relation
 * @method     ChildProductQuery innerJoinProductTranslation($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductTranslation relation
 *
 * @method     ChildProductQuery joinWithProductTranslation($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductTranslation relation
 *
 * @method     ChildProductQuery leftJoinWithProductTranslation() Adds a LEFT JOIN clause and with to the query using the ProductTranslation relation
 * @method     ChildProductQuery rightJoinWithProductTranslation() Adds a RIGHT JOIN clause and with to the query using the ProductTranslation relation
 * @method     ChildProductQuery innerJoinWithProductTranslation() Adds a INNER JOIN clause and with to the query using the ProductTranslation relation
 *
 * @method     ChildProductQuery leftJoinRelatedProductRelatedByProductId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RelatedProductRelatedByProductId relation
 * @method     ChildProductQuery rightJoinRelatedProductRelatedByProductId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RelatedProductRelatedByProductId relation
 * @method     ChildProductQuery innerJoinRelatedProductRelatedByProductId($relationAlias = null) Adds a INNER JOIN clause to the query using the RelatedProductRelatedByProductId relation
 *
 * @method     ChildProductQuery joinWithRelatedProductRelatedByProductId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RelatedProductRelatedByProductId relation
 *
 * @method     ChildProductQuery leftJoinWithRelatedProductRelatedByProductId() Adds a LEFT JOIN clause and with to the query using the RelatedProductRelatedByProductId relation
 * @method     ChildProductQuery rightJoinWithRelatedProductRelatedByProductId() Adds a RIGHT JOIN clause and with to the query using the RelatedProductRelatedByProductId relation
 * @method     ChildProductQuery innerJoinWithRelatedProductRelatedByProductId() Adds a INNER JOIN clause and with to the query using the RelatedProductRelatedByProductId relation
 *
 * @method     ChildProductQuery leftJoinRelatedProductRelatedByOtherProductId($relationAlias = null) Adds a LEFT JOIN clause to the query using the RelatedProductRelatedByOtherProductId relation
 * @method     ChildProductQuery rightJoinRelatedProductRelatedByOtherProductId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RelatedProductRelatedByOtherProductId relation
 * @method     ChildProductQuery innerJoinRelatedProductRelatedByOtherProductId($relationAlias = null) Adds a INNER JOIN clause to the query using the RelatedProductRelatedByOtherProductId relation
 *
 * @method     ChildProductQuery joinWithRelatedProductRelatedByOtherProductId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the RelatedProductRelatedByOtherProductId relation
 *
 * @method     ChildProductQuery leftJoinWithRelatedProductRelatedByOtherProductId() Adds a LEFT JOIN clause and with to the query using the RelatedProductRelatedByOtherProductId relation
 * @method     ChildProductQuery rightJoinWithRelatedProductRelatedByOtherProductId() Adds a RIGHT JOIN clause and with to the query using the RelatedProductRelatedByOtherProductId relation
 * @method     ChildProductQuery innerJoinWithRelatedProductRelatedByOtherProductId() Adds a INNER JOIN clause and with to the query using the RelatedProductRelatedByOtherProductId relation
 *
 * @method     ChildProductQuery leftJoinProductImage($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductImage relation
 * @method     ChildProductQuery rightJoinProductImage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductImage relation
 * @method     ChildProductQuery innerJoinProductImage($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductImage relation
 *
 * @method     ChildProductQuery joinWithProductImage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductImage relation
 *
 * @method     ChildProductQuery leftJoinWithProductImage() Adds a LEFT JOIN clause and with to the query using the ProductImage relation
 * @method     ChildProductQuery rightJoinWithProductImage() Adds a RIGHT JOIN clause and with to the query using the ProductImage relation
 * @method     ChildProductQuery innerJoinWithProductImage() Adds a INNER JOIN clause and with to the query using the ProductImage relation
 *
 * @method     ChildProductQuery leftJoinProductTag($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProductTag relation
 * @method     ChildProductQuery rightJoinProductTag($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProductTag relation
 * @method     ChildProductQuery innerJoinProductTag($relationAlias = null) Adds a INNER JOIN clause to the query using the ProductTag relation
 *
 * @method     ChildProductQuery joinWithProductTag($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProductTag relation
 *
 * @method     ChildProductQuery leftJoinWithProductTag() Adds a LEFT JOIN clause and with to the query using the ProductTag relation
 * @method     ChildProductQuery rightJoinWithProductTag() Adds a RIGHT JOIN clause and with to the query using the ProductTag relation
 * @method     ChildProductQuery innerJoinWithProductTag() Adds a INNER JOIN clause and with to the query using the ProductTag relation
 *
 * @method     ChildProductQuery leftJoinSaleOrderItemStockClaimed($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrderItemStockClaimed relation
 * @method     ChildProductQuery rightJoinSaleOrderItemStockClaimed($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrderItemStockClaimed relation
 * @method     ChildProductQuery innerJoinSaleOrderItemStockClaimed($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrderItemStockClaimed relation
 *
 * @method     ChildProductQuery joinWithSaleOrderItemStockClaimed($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrderItemStockClaimed relation
 *
 * @method     ChildProductQuery leftJoinWithSaleOrderItemStockClaimed() Adds a LEFT JOIN clause and with to the query using the SaleOrderItemStockClaimed relation
 * @method     ChildProductQuery rightJoinWithSaleOrderItemStockClaimed() Adds a RIGHT JOIN clause and with to the query using the SaleOrderItemStockClaimed relation
 * @method     ChildProductQuery innerJoinWithSaleOrderItemStockClaimed() Adds a INNER JOIN clause and with to the query using the SaleOrderItemStockClaimed relation
 *
 * @method     ChildProductQuery leftJoinCustomerWishlist($relationAlias = null) Adds a LEFT JOIN clause to the query using the CustomerWishlist relation
 * @method     ChildProductQuery rightJoinCustomerWishlist($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CustomerWishlist relation
 * @method     ChildProductQuery innerJoinCustomerWishlist($relationAlias = null) Adds a INNER JOIN clause to the query using the CustomerWishlist relation
 *
 * @method     ChildProductQuery joinWithCustomerWishlist($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CustomerWishlist relation
 *
 * @method     ChildProductQuery leftJoinWithCustomerWishlist() Adds a LEFT JOIN clause and with to the query using the CustomerWishlist relation
 * @method     ChildProductQuery rightJoinWithCustomerWishlist() Adds a RIGHT JOIN clause and with to the query using the CustomerWishlist relation
 * @method     ChildProductQuery innerJoinWithCustomerWishlist() Adds a INNER JOIN clause and with to the query using the CustomerWishlist relation
 *
 * @method     \Model\Setting\MasterTable\BrandQuery|\Model\Setting\MasterTable\ProductUnitQuery|\Model\Supplier\SupplierQuery|\Model\Category\CategoryQuery|\Model\Account\UserQuery|\Model\Setting\MasterTable\VatQuery|\Model\Category\ProductMultiCategoryQuery|\Model\Sale\OrderItemProductQuery|\Model\ProductReviewQuery|\Model\PromoProductQuery|\Model\ProductPropertyQuery|\Model\ProductColorQuery|\Model\ProductListQuery|\Model\ProductUsageQuery|\Model\ProductTranslationQuery|\Model\Product\RelatedProductQuery|\Model\Product\Image\ProductImageQuery|\Model\Product\ProductTagQuery|\Model\Sale\SaleOrderItemStockClaimedQuery|\Model\Category\CustomerWishlistQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildProduct findOne(ConnectionInterface $con = null) Return the first ChildProduct matching the query
 * @method     ChildProduct findOneOrCreate(ConnectionInterface $con = null) Return the first ChildProduct matching the query, or a new ChildProduct object populated from the query conditions when no match is found
 *
 * @method     ChildProduct findOneById(int $id) Return the first ChildProduct filtered by the id column
 * @method     ChildProduct findOneByDerrivedFromId(int $derrived_from_id) Return the first ChildProduct filtered by the derrived_from_id column
 * @method     ChildProduct findOneByOldId(string $old_id) Return the first ChildProduct filtered by the old_id column
 * @method     ChildProduct findOneByImportTag(string $import_tag) Return the first ChildProduct filtered by the import_tag column
 * @method     ChildProduct findOneBySku(string $sku) Return the first ChildProduct filtered by the sku column
 * @method     ChildProduct findOneByEan(string $ean) Return the first ChildProduct filtered by the ean column
 * @method     ChildProduct findOneByDeletedOn(string $deleted_on) Return the first ChildProduct filtered by the deleted_on column
 * @method     ChildProduct findOneByDeletedByUserId(int $deleted_by_user_id) Return the first ChildProduct filtered by the deleted_by_user_id column
 * @method     ChildProduct findOneByPopularity(int $popularity) Return the first ChildProduct filtered by the popularity column
 * @method     ChildProduct findOneByAvgRating(int $avg_rating) Return the first ChildProduct filtered by the avg_rating column
 * @method     ChildProduct findOneBySupplier(string $supplier) Return the first ChildProduct filtered by the supplier column
 * @method     ChildProduct findOneBySupplierId(int $supplier_id) Return the first ChildProduct filtered by the supplier_id column
 * @method     ChildProduct findOneBySupplierProductNumber(string $supplier_product_number) Return the first ChildProduct filtered by the supplier_product_number column
 * @method     ChildProduct findOneByInSale(boolean $in_sale) Return the first ChildProduct filtered by the in_sale column
 * @method     ChildProduct findOneByInSpotlight(boolean $in_spotlight) Return the first ChildProduct filtered by the in_spotlight column
 * @method     ChildProduct findOneByInWebshop(boolean $in_webshop) Return the first ChildProduct filtered by the in_webshop column
 * @method     ChildProduct findOneByIsOffer(boolean $is_offer) Return the first ChildProduct filtered by the is_offer column
 * @method     ChildProduct findOneByIsBestseller(boolean $is_bestseller) Return the first ChildProduct filtered by the is_bestseller column
 * @method     ChildProduct findOneByIsOutOfStock(boolean $is_out_of_stock) Return the first ChildProduct filtered by the is_out_of_stock column
 * @method     ChildProduct findOneByHasImages(boolean $has_images) Return the first ChildProduct filtered by the has_images column
 * @method     ChildProduct findOneByCategoryId(int $category_id) Return the first ChildProduct filtered by the category_id column
 * @method     ChildProduct findOneByNumber(string $number) Return the first ChildProduct filtered by the number column
 * @method     ChildProduct findOneByAdvertiserUserId(int $advertiser_user_id) Return the first ChildProduct filtered by the advertiser_user_id column
 * @method     ChildProduct findOneByDeliveryTimeId(int $delivery_time_id) Return the first ChildProduct filtered by the delivery_time_id column
 * @method     ChildProduct findOneByThickness(double $thickness) Return the first ChildProduct filtered by the thickness column
 * @method     ChildProduct findOneByHeight(string $height) Return the first ChildProduct filtered by the height column
 * @method     ChildProduct findOneByWidth(string $width) Return the first ChildProduct filtered by the width column
 * @method     ChildProduct findOneByLength(string $length) Return the first ChildProduct filtered by the length column
 * @method     ChildProduct findOneByWeight(string $weight) Return the first ChildProduct filtered by the weight column
 * @method     ChildProduct findOneByComposition(string $composition) Return the first ChildProduct filtered by the composition column
 * @method     ChildProduct findOneByMaterialId(string $material_id) Return the first ChildProduct filtered by the material_id column
 * @method     ChildProduct findOneByBrandId(int $brand_id) Return the first ChildProduct filtered by the brand_id column
 * @method     ChildProduct findOneByTitle(string $title) Return the first ChildProduct filtered by the title column
 * @method     ChildProduct findOneByDescription(string $description) Return the first ChildProduct filtered by the description column
 * @method     ChildProduct findOneByNote(string $note) Return the first ChildProduct filtered by the note column
 * @method     ChildProduct findOneByCreatedOn(string $created_on) Return the first ChildProduct filtered by the created_on column
 * @method     ChildProduct findOneByDayPrice(double $day_price) Return the first ChildProduct filtered by the day_price column
 * @method     ChildProduct findOneBySalePrice(double $sale_price) Return the first ChildProduct filtered by the sale_price column
 * @method     ChildProduct findOneByDiscountPercentage(double $discount_percentage) Return the first ChildProduct filtered by the discount_percentage column
 * @method     ChildProduct findOneByPurchasePrice(string $purchase_price) Return the first ChildProduct filtered by the purchase_price column
 * @method     ChildProduct findOneByResellerPrice(double $reseller_price) Return the first ChildProduct filtered by the reseller_price column
 * @method     ChildProduct findOneByAdvicePrice(double $advice_price) Return the first ChildProduct filtered by the advice_price column
 * @method     ChildProduct findOneByBulkStockId(int $bulk_stock_id) Return the first ChildProduct filtered by the bulk_stock_id column
 * @method     ChildProduct findOneByStockQuantity(int $stock_quantity) Return the first ChildProduct filtered by the stock_quantity column
 * @method     ChildProduct findOneByVirtualStock(boolean $virtual_stock) Return the first ChildProduct filtered by the virtual_stock column
 * @method     ChildProduct findOneByQuantityPerPack(int $quantity_per_pack) Return the first ChildProduct filtered by the quantity_per_pack column
 * @method     ChildProduct findOneByYoutubeUrl(string $youtube_url) Return the first ChildProduct filtered by the youtube_url column
 * @method     ChildProduct findOneByMetaTitle(string $meta_title) Return the first ChildProduct filtered by the meta_title column
 * @method     ChildProduct findOneByMetaDescription(string $meta_description) Return the first ChildProduct filtered by the meta_description column
 * @method     ChildProduct findOneByMetaKeyword(string $meta_keyword) Return the first ChildProduct filtered by the meta_keyword column
 * @method     ChildProduct findOneByHashtag(string $hashtag) Return the first ChildProduct filtered by the hashtag column
 * @method     ChildProduct findOneByCalculatedAverageRating(int $calculated_average_rating) Return the first ChildProduct filtered by the calculated_average_rating column
 * @method     ChildProduct findOneByVatId(int $vat_id) Return the first ChildProduct filtered by the vat_id column
 * @method     ChildProduct findOneByUnitId(int $unit_id) Return the first ChildProduct filtered by the unit_id column
 * @method     ChildProduct findOneByCreatedAt(string $created_at) Return the first ChildProduct filtered by the created_at column
 * @method     ChildProduct findOneByUpdatedAt(string $updated_at) Return the first ChildProduct filtered by the updated_at column *

 * @method     ChildProduct requirePk($key, ConnectionInterface $con = null) Return the ChildProduct by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOne(ConnectionInterface $con = null) Return the first ChildProduct matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProduct requireOneById(int $id) Return the first ChildProduct filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByDerrivedFromId(int $derrived_from_id) Return the first ChildProduct filtered by the derrived_from_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByOldId(string $old_id) Return the first ChildProduct filtered by the old_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByImportTag(string $import_tag) Return the first ChildProduct filtered by the import_tag column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneBySku(string $sku) Return the first ChildProduct filtered by the sku column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByEan(string $ean) Return the first ChildProduct filtered by the ean column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByDeletedOn(string $deleted_on) Return the first ChildProduct filtered by the deleted_on column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByDeletedByUserId(int $deleted_by_user_id) Return the first ChildProduct filtered by the deleted_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByPopularity(int $popularity) Return the first ChildProduct filtered by the popularity column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByAvgRating(int $avg_rating) Return the first ChildProduct filtered by the avg_rating column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneBySupplier(string $supplier) Return the first ChildProduct filtered by the supplier column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneBySupplierId(int $supplier_id) Return the first ChildProduct filtered by the supplier_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneBySupplierProductNumber(string $supplier_product_number) Return the first ChildProduct filtered by the supplier_product_number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByInSale(boolean $in_sale) Return the first ChildProduct filtered by the in_sale column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByInSpotlight(boolean $in_spotlight) Return the first ChildProduct filtered by the in_spotlight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByInWebshop(boolean $in_webshop) Return the first ChildProduct filtered by the in_webshop column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByIsOffer(boolean $is_offer) Return the first ChildProduct filtered by the is_offer column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByIsBestseller(boolean $is_bestseller) Return the first ChildProduct filtered by the is_bestseller column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByIsOutOfStock(boolean $is_out_of_stock) Return the first ChildProduct filtered by the is_out_of_stock column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByHasImages(boolean $has_images) Return the first ChildProduct filtered by the has_images column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByCategoryId(int $category_id) Return the first ChildProduct filtered by the category_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByNumber(string $number) Return the first ChildProduct filtered by the number column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByAdvertiserUserId(int $advertiser_user_id) Return the first ChildProduct filtered by the advertiser_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByDeliveryTimeId(int $delivery_time_id) Return the first ChildProduct filtered by the delivery_time_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByThickness(double $thickness) Return the first ChildProduct filtered by the thickness column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByHeight(string $height) Return the first ChildProduct filtered by the height column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByWidth(string $width) Return the first ChildProduct filtered by the width column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByLength(string $length) Return the first ChildProduct filtered by the length column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByWeight(string $weight) Return the first ChildProduct filtered by the weight column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByComposition(string $composition) Return the first ChildProduct filtered by the composition column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByMaterialId(string $material_id) Return the first ChildProduct filtered by the material_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByBrandId(int $brand_id) Return the first ChildProduct filtered by the brand_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByTitle(string $title) Return the first ChildProduct filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByDescription(string $description) Return the first ChildProduct filtered by the description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByNote(string $note) Return the first ChildProduct filtered by the note column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByCreatedOn(string $created_on) Return the first ChildProduct filtered by the created_on column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByDayPrice(double $day_price) Return the first ChildProduct filtered by the day_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneBySalePrice(double $sale_price) Return the first ChildProduct filtered by the sale_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByDiscountPercentage(double $discount_percentage) Return the first ChildProduct filtered by the discount_percentage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByPurchasePrice(string $purchase_price) Return the first ChildProduct filtered by the purchase_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByResellerPrice(double $reseller_price) Return the first ChildProduct filtered by the reseller_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByAdvicePrice(double $advice_price) Return the first ChildProduct filtered by the advice_price column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByBulkStockId(int $bulk_stock_id) Return the first ChildProduct filtered by the bulk_stock_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByStockQuantity(int $stock_quantity) Return the first ChildProduct filtered by the stock_quantity column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByVirtualStock(boolean $virtual_stock) Return the first ChildProduct filtered by the virtual_stock column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByQuantityPerPack(int $quantity_per_pack) Return the first ChildProduct filtered by the quantity_per_pack column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByYoutubeUrl(string $youtube_url) Return the first ChildProduct filtered by the youtube_url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByMetaTitle(string $meta_title) Return the first ChildProduct filtered by the meta_title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByMetaDescription(string $meta_description) Return the first ChildProduct filtered by the meta_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByMetaKeyword(string $meta_keyword) Return the first ChildProduct filtered by the meta_keyword column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByHashtag(string $hashtag) Return the first ChildProduct filtered by the hashtag column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByCalculatedAverageRating(int $calculated_average_rating) Return the first ChildProduct filtered by the calculated_average_rating column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByVatId(int $vat_id) Return the first ChildProduct filtered by the vat_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByUnitId(int $unit_id) Return the first ChildProduct filtered by the unit_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByCreatedAt(string $created_at) Return the first ChildProduct filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProduct requireOneByUpdatedAt(string $updated_at) Return the first ChildProduct filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProduct[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildProduct objects based on current ModelCriteria
 * @method     ChildProduct[]|ObjectCollection findById(int $id) Return ChildProduct objects filtered by the id column
 * @method     ChildProduct[]|ObjectCollection findByDerrivedFromId(int $derrived_from_id) Return ChildProduct objects filtered by the derrived_from_id column
 * @method     ChildProduct[]|ObjectCollection findByOldId(string $old_id) Return ChildProduct objects filtered by the old_id column
 * @method     ChildProduct[]|ObjectCollection findByImportTag(string $import_tag) Return ChildProduct objects filtered by the import_tag column
 * @method     ChildProduct[]|ObjectCollection findBySku(string $sku) Return ChildProduct objects filtered by the sku column
 * @method     ChildProduct[]|ObjectCollection findByEan(string $ean) Return ChildProduct objects filtered by the ean column
 * @method     ChildProduct[]|ObjectCollection findByDeletedOn(string $deleted_on) Return ChildProduct objects filtered by the deleted_on column
 * @method     ChildProduct[]|ObjectCollection findByDeletedByUserId(int $deleted_by_user_id) Return ChildProduct objects filtered by the deleted_by_user_id column
 * @method     ChildProduct[]|ObjectCollection findByPopularity(int $popularity) Return ChildProduct objects filtered by the popularity column
 * @method     ChildProduct[]|ObjectCollection findByAvgRating(int $avg_rating) Return ChildProduct objects filtered by the avg_rating column
 * @method     ChildProduct[]|ObjectCollection findBySupplier(string $supplier) Return ChildProduct objects filtered by the supplier column
 * @method     ChildProduct[]|ObjectCollection findBySupplierId(int $supplier_id) Return ChildProduct objects filtered by the supplier_id column
 * @method     ChildProduct[]|ObjectCollection findBySupplierProductNumber(string $supplier_product_number) Return ChildProduct objects filtered by the supplier_product_number column
 * @method     ChildProduct[]|ObjectCollection findByInSale(boolean $in_sale) Return ChildProduct objects filtered by the in_sale column
 * @method     ChildProduct[]|ObjectCollection findByInSpotlight(boolean $in_spotlight) Return ChildProduct objects filtered by the in_spotlight column
 * @method     ChildProduct[]|ObjectCollection findByInWebshop(boolean $in_webshop) Return ChildProduct objects filtered by the in_webshop column
 * @method     ChildProduct[]|ObjectCollection findByIsOffer(boolean $is_offer) Return ChildProduct objects filtered by the is_offer column
 * @method     ChildProduct[]|ObjectCollection findByIsBestseller(boolean $is_bestseller) Return ChildProduct objects filtered by the is_bestseller column
 * @method     ChildProduct[]|ObjectCollection findByIsOutOfStock(boolean $is_out_of_stock) Return ChildProduct objects filtered by the is_out_of_stock column
 * @method     ChildProduct[]|ObjectCollection findByHasImages(boolean $has_images) Return ChildProduct objects filtered by the has_images column
 * @method     ChildProduct[]|ObjectCollection findByCategoryId(int $category_id) Return ChildProduct objects filtered by the category_id column
 * @method     ChildProduct[]|ObjectCollection findByNumber(string $number) Return ChildProduct objects filtered by the number column
 * @method     ChildProduct[]|ObjectCollection findByAdvertiserUserId(int $advertiser_user_id) Return ChildProduct objects filtered by the advertiser_user_id column
 * @method     ChildProduct[]|ObjectCollection findByDeliveryTimeId(int $delivery_time_id) Return ChildProduct objects filtered by the delivery_time_id column
 * @method     ChildProduct[]|ObjectCollection findByThickness(double $thickness) Return ChildProduct objects filtered by the thickness column
 * @method     ChildProduct[]|ObjectCollection findByHeight(string $height) Return ChildProduct objects filtered by the height column
 * @method     ChildProduct[]|ObjectCollection findByWidth(string $width) Return ChildProduct objects filtered by the width column
 * @method     ChildProduct[]|ObjectCollection findByLength(string $length) Return ChildProduct objects filtered by the length column
 * @method     ChildProduct[]|ObjectCollection findByWeight(string $weight) Return ChildProduct objects filtered by the weight column
 * @method     ChildProduct[]|ObjectCollection findByComposition(string $composition) Return ChildProduct objects filtered by the composition column
 * @method     ChildProduct[]|ObjectCollection findByMaterialId(string $material_id) Return ChildProduct objects filtered by the material_id column
 * @method     ChildProduct[]|ObjectCollection findByBrandId(int $brand_id) Return ChildProduct objects filtered by the brand_id column
 * @method     ChildProduct[]|ObjectCollection findByTitle(string $title) Return ChildProduct objects filtered by the title column
 * @method     ChildProduct[]|ObjectCollection findByDescription(string $description) Return ChildProduct objects filtered by the description column
 * @method     ChildProduct[]|ObjectCollection findByNote(string $note) Return ChildProduct objects filtered by the note column
 * @method     ChildProduct[]|ObjectCollection findByCreatedOn(string $created_on) Return ChildProduct objects filtered by the created_on column
 * @method     ChildProduct[]|ObjectCollection findByDayPrice(double $day_price) Return ChildProduct objects filtered by the day_price column
 * @method     ChildProduct[]|ObjectCollection findBySalePrice(double $sale_price) Return ChildProduct objects filtered by the sale_price column
 * @method     ChildProduct[]|ObjectCollection findByDiscountPercentage(double $discount_percentage) Return ChildProduct objects filtered by the discount_percentage column
 * @method     ChildProduct[]|ObjectCollection findByPurchasePrice(string $purchase_price) Return ChildProduct objects filtered by the purchase_price column
 * @method     ChildProduct[]|ObjectCollection findByResellerPrice(double $reseller_price) Return ChildProduct objects filtered by the reseller_price column
 * @method     ChildProduct[]|ObjectCollection findByAdvicePrice(double $advice_price) Return ChildProduct objects filtered by the advice_price column
 * @method     ChildProduct[]|ObjectCollection findByBulkStockId(int $bulk_stock_id) Return ChildProduct objects filtered by the bulk_stock_id column
 * @method     ChildProduct[]|ObjectCollection findByStockQuantity(int $stock_quantity) Return ChildProduct objects filtered by the stock_quantity column
 * @method     ChildProduct[]|ObjectCollection findByVirtualStock(boolean $virtual_stock) Return ChildProduct objects filtered by the virtual_stock column
 * @method     ChildProduct[]|ObjectCollection findByQuantityPerPack(int $quantity_per_pack) Return ChildProduct objects filtered by the quantity_per_pack column
 * @method     ChildProduct[]|ObjectCollection findByYoutubeUrl(string $youtube_url) Return ChildProduct objects filtered by the youtube_url column
 * @method     ChildProduct[]|ObjectCollection findByMetaTitle(string $meta_title) Return ChildProduct objects filtered by the meta_title column
 * @method     ChildProduct[]|ObjectCollection findByMetaDescription(string $meta_description) Return ChildProduct objects filtered by the meta_description column
 * @method     ChildProduct[]|ObjectCollection findByMetaKeyword(string $meta_keyword) Return ChildProduct objects filtered by the meta_keyword column
 * @method     ChildProduct[]|ObjectCollection findByHashtag(string $hashtag) Return ChildProduct objects filtered by the hashtag column
 * @method     ChildProduct[]|ObjectCollection findByCalculatedAverageRating(int $calculated_average_rating) Return ChildProduct objects filtered by the calculated_average_rating column
 * @method     ChildProduct[]|ObjectCollection findByVatId(int $vat_id) Return ChildProduct objects filtered by the vat_id column
 * @method     ChildProduct[]|ObjectCollection findByUnitId(int $unit_id) Return ChildProduct objects filtered by the unit_id column
 * @method     ChildProduct[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildProduct objects filtered by the created_at column
 * @method     ChildProduct[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildProduct objects filtered by the updated_at column
 * @method     ChildProduct[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProductQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Base\ProductQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Product', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProductQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProductQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProductQuery) {
            return $criteria;
        }
        $query = new ChildProductQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildProduct|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProductTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ProductTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProduct A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, derrived_from_id, old_id, import_tag, sku, ean, deleted_on, deleted_by_user_id, popularity, avg_rating, supplier, supplier_id, supplier_product_number, in_sale, in_spotlight, in_webshop, is_offer, is_bestseller, is_out_of_stock, has_images, category_id, number, advertiser_user_id, delivery_time_id, thickness, height, width, length, weight, composition, material_id, brand_id, title, description, note, created_on, day_price, sale_price, discount_percentage, purchase_price, reseller_price, advice_price, bulk_stock_id, stock_quantity, virtual_stock, quantity_per_pack, youtube_url, meta_title, meta_description, meta_keyword, hashtag, calculated_average_rating, vat_id, unit_id, created_at, updated_at FROM product WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildProduct $obj */
            $obj = new ChildProduct();
            $obj->hydrate($row);
            ProductTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProduct|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProductTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProductTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the derrived_from_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDerrivedFromId(1234); // WHERE derrived_from_id = 1234
     * $query->filterByDerrivedFromId(array(12, 34)); // WHERE derrived_from_id IN (12, 34)
     * $query->filterByDerrivedFromId(array('min' => 12)); // WHERE derrived_from_id > 12
     * </code>
     *
     * @param     mixed $derrivedFromId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByDerrivedFromId($derrivedFromId = null, $comparison = null)
    {
        if (is_array($derrivedFromId)) {
            $useMinMax = false;
            if (isset($derrivedFromId['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_DERRIVED_FROM_ID, $derrivedFromId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($derrivedFromId['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_DERRIVED_FROM_ID, $derrivedFromId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_DERRIVED_FROM_ID, $derrivedFromId, $comparison);
    }

    /**
     * Filter the query on the old_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOldId('fooValue');   // WHERE old_id = 'fooValue'
     * $query->filterByOldId('%fooValue%', Criteria::LIKE); // WHERE old_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $oldId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByOldId($oldId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($oldId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_OLD_ID, $oldId, $comparison);
    }

    /**
     * Filter the query on the import_tag column
     *
     * Example usage:
     * <code>
     * $query->filterByImportTag('fooValue');   // WHERE import_tag = 'fooValue'
     * $query->filterByImportTag('%fooValue%', Criteria::LIKE); // WHERE import_tag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $importTag The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByImportTag($importTag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($importTag)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_IMPORT_TAG, $importTag, $comparison);
    }

    /**
     * Filter the query on the sku column
     *
     * Example usage:
     * <code>
     * $query->filterBySku('fooValue');   // WHERE sku = 'fooValue'
     * $query->filterBySku('%fooValue%', Criteria::LIKE); // WHERE sku LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sku The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterBySku($sku = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sku)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_SKU, $sku, $comparison);
    }

    /**
     * Filter the query on the ean column
     *
     * Example usage:
     * <code>
     * $query->filterByEan('fooValue');   // WHERE ean = 'fooValue'
     * $query->filterByEan('%fooValue%', Criteria::LIKE); // WHERE ean LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ean The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByEan($ean = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ean)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_EAN, $ean, $comparison);
    }

    /**
     * Filter the query on the deleted_on column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedOn('2011-03-14'); // WHERE deleted_on = '2011-03-14'
     * $query->filterByDeletedOn('now'); // WHERE deleted_on = '2011-03-14'
     * $query->filterByDeletedOn(array('max' => 'yesterday')); // WHERE deleted_on > '2011-03-13'
     * </code>
     *
     * @param     mixed $deletedOn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByDeletedOn($deletedOn = null, $comparison = null)
    {
        if (is_array($deletedOn)) {
            $useMinMax = false;
            if (isset($deletedOn['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_DELETED_ON, $deletedOn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedOn['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_DELETED_ON, $deletedOn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_DELETED_ON, $deletedOn, $comparison);
    }

    /**
     * Filter the query on the deleted_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDeletedByUserId(1234); // WHERE deleted_by_user_id = 1234
     * $query->filterByDeletedByUserId(array(12, 34)); // WHERE deleted_by_user_id IN (12, 34)
     * $query->filterByDeletedByUserId(array('min' => 12)); // WHERE deleted_by_user_id > 12
     * </code>
     *
     * @param     mixed $deletedByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByDeletedByUserId($deletedByUserId = null, $comparison = null)
    {
        if (is_array($deletedByUserId)) {
            $useMinMax = false;
            if (isset($deletedByUserId['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_DELETED_BY_USER_ID, $deletedByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deletedByUserId['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_DELETED_BY_USER_ID, $deletedByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_DELETED_BY_USER_ID, $deletedByUserId, $comparison);
    }

    /**
     * Filter the query on the popularity column
     *
     * Example usage:
     * <code>
     * $query->filterByPopularity(1234); // WHERE popularity = 1234
     * $query->filterByPopularity(array(12, 34)); // WHERE popularity IN (12, 34)
     * $query->filterByPopularity(array('min' => 12)); // WHERE popularity > 12
     * </code>
     *
     * @param     mixed $popularity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByPopularity($popularity = null, $comparison = null)
    {
        if (is_array($popularity)) {
            $useMinMax = false;
            if (isset($popularity['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_POPULARITY, $popularity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($popularity['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_POPULARITY, $popularity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_POPULARITY, $popularity, $comparison);
    }

    /**
     * Filter the query on the avg_rating column
     *
     * Example usage:
     * <code>
     * $query->filterByAvgRating(1234); // WHERE avg_rating = 1234
     * $query->filterByAvgRating(array(12, 34)); // WHERE avg_rating IN (12, 34)
     * $query->filterByAvgRating(array('min' => 12)); // WHERE avg_rating > 12
     * </code>
     *
     * @param     mixed $avgRating The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByAvgRating($avgRating = null, $comparison = null)
    {
        if (is_array($avgRating)) {
            $useMinMax = false;
            if (isset($avgRating['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_AVG_RATING, $avgRating['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($avgRating['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_AVG_RATING, $avgRating['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_AVG_RATING, $avgRating, $comparison);
    }

    /**
     * Filter the query on the supplier column
     *
     * Example usage:
     * <code>
     * $query->filterBySupplier('fooValue');   // WHERE supplier = 'fooValue'
     * $query->filterBySupplier('%fooValue%', Criteria::LIKE); // WHERE supplier LIKE '%fooValue%'
     * </code>
     *
     * @param     string $supplier The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterBySupplier($supplier = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($supplier)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_SUPPLIER, $supplier, $comparison);
    }

    /**
     * Filter the query on the supplier_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySupplierId(1234); // WHERE supplier_id = 1234
     * $query->filterBySupplierId(array(12, 34)); // WHERE supplier_id IN (12, 34)
     * $query->filterBySupplierId(array('min' => 12)); // WHERE supplier_id > 12
     * </code>
     *
     * @see       filterBySupplierRef()
     *
     * @param     mixed $supplierId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterBySupplierId($supplierId = null, $comparison = null)
    {
        if (is_array($supplierId)) {
            $useMinMax = false;
            if (isset($supplierId['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_SUPPLIER_ID, $supplierId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($supplierId['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_SUPPLIER_ID, $supplierId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_SUPPLIER_ID, $supplierId, $comparison);
    }

    /**
     * Filter the query on the supplier_product_number column
     *
     * Example usage:
     * <code>
     * $query->filterBySupplierProductNumber('fooValue');   // WHERE supplier_product_number = 'fooValue'
     * $query->filterBySupplierProductNumber('%fooValue%', Criteria::LIKE); // WHERE supplier_product_number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $supplierProductNumber The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterBySupplierProductNumber($supplierProductNumber = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($supplierProductNumber)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_SUPPLIER_PRODUCT_NUMBER, $supplierProductNumber, $comparison);
    }

    /**
     * Filter the query on the in_sale column
     *
     * Example usage:
     * <code>
     * $query->filterByInSale(true); // WHERE in_sale = true
     * $query->filterByInSale('yes'); // WHERE in_sale = true
     * </code>
     *
     * @param     boolean|string $inSale The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByInSale($inSale = null, $comparison = null)
    {
        if (is_string($inSale)) {
            $inSale = in_array(strtolower($inSale), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ProductTableMap::COL_IN_SALE, $inSale, $comparison);
    }

    /**
     * Filter the query on the in_spotlight column
     *
     * Example usage:
     * <code>
     * $query->filterByInSpotlight(true); // WHERE in_spotlight = true
     * $query->filterByInSpotlight('yes'); // WHERE in_spotlight = true
     * </code>
     *
     * @param     boolean|string $inSpotlight The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByInSpotlight($inSpotlight = null, $comparison = null)
    {
        if (is_string($inSpotlight)) {
            $inSpotlight = in_array(strtolower($inSpotlight), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ProductTableMap::COL_IN_SPOTLIGHT, $inSpotlight, $comparison);
    }

    /**
     * Filter the query on the in_webshop column
     *
     * Example usage:
     * <code>
     * $query->filterByInWebshop(true); // WHERE in_webshop = true
     * $query->filterByInWebshop('yes'); // WHERE in_webshop = true
     * </code>
     *
     * @param     boolean|string $inWebshop The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByInWebshop($inWebshop = null, $comparison = null)
    {
        if (is_string($inWebshop)) {
            $inWebshop = in_array(strtolower($inWebshop), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ProductTableMap::COL_IN_WEBSHOP, $inWebshop, $comparison);
    }

    /**
     * Filter the query on the is_offer column
     *
     * Example usage:
     * <code>
     * $query->filterByIsOffer(true); // WHERE is_offer = true
     * $query->filterByIsOffer('yes'); // WHERE is_offer = true
     * </code>
     *
     * @param     boolean|string $isOffer The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByIsOffer($isOffer = null, $comparison = null)
    {
        if (is_string($isOffer)) {
            $isOffer = in_array(strtolower($isOffer), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ProductTableMap::COL_IS_OFFER, $isOffer, $comparison);
    }

    /**
     * Filter the query on the is_bestseller column
     *
     * Example usage:
     * <code>
     * $query->filterByIsBestseller(true); // WHERE is_bestseller = true
     * $query->filterByIsBestseller('yes'); // WHERE is_bestseller = true
     * </code>
     *
     * @param     boolean|string $isBestseller The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByIsBestseller($isBestseller = null, $comparison = null)
    {
        if (is_string($isBestseller)) {
            $isBestseller = in_array(strtolower($isBestseller), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ProductTableMap::COL_IS_BESTSELLER, $isBestseller, $comparison);
    }

    /**
     * Filter the query on the is_out_of_stock column
     *
     * Example usage:
     * <code>
     * $query->filterByIsOutOfStock(true); // WHERE is_out_of_stock = true
     * $query->filterByIsOutOfStock('yes'); // WHERE is_out_of_stock = true
     * </code>
     *
     * @param     boolean|string $isOutOfStock The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByIsOutOfStock($isOutOfStock = null, $comparison = null)
    {
        if (is_string($isOutOfStock)) {
            $isOutOfStock = in_array(strtolower($isOutOfStock), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ProductTableMap::COL_IS_OUT_OF_STOCK, $isOutOfStock, $comparison);
    }

    /**
     * Filter the query on the has_images column
     *
     * Example usage:
     * <code>
     * $query->filterByHasImages(true); // WHERE has_images = true
     * $query->filterByHasImages('yes'); // WHERE has_images = true
     * </code>
     *
     * @param     boolean|string $hasImages The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByHasImages($hasImages = null, $comparison = null)
    {
        if (is_string($hasImages)) {
            $hasImages = in_array(strtolower($hasImages), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ProductTableMap::COL_HAS_IMAGES, $hasImages, $comparison);
    }

    /**
     * Filter the query on the category_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCategoryId(1234); // WHERE category_id = 1234
     * $query->filterByCategoryId(array(12, 34)); // WHERE category_id IN (12, 34)
     * $query->filterByCategoryId(array('min' => 12)); // WHERE category_id > 12
     * </code>
     *
     * @see       filterByCategory()
     *
     * @param     mixed $categoryId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByCategoryId($categoryId = null, $comparison = null)
    {
        if (is_array($categoryId)) {
            $useMinMax = false;
            if (isset($categoryId['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_CATEGORY_ID, $categoryId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($categoryId['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_CATEGORY_ID, $categoryId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_CATEGORY_ID, $categoryId, $comparison);
    }

    /**
     * Filter the query on the number column
     *
     * Example usage:
     * <code>
     * $query->filterByNumber('fooValue');   // WHERE number = 'fooValue'
     * $query->filterByNumber('%fooValue%', Criteria::LIKE); // WHERE number LIKE '%fooValue%'
     * </code>
     *
     * @param     string $number The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByNumber($number = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($number)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_NUMBER, $number, $comparison);
    }

    /**
     * Filter the query on the advertiser_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAdvertiserUserId(1234); // WHERE advertiser_user_id = 1234
     * $query->filterByAdvertiserUserId(array(12, 34)); // WHERE advertiser_user_id IN (12, 34)
     * $query->filterByAdvertiserUserId(array('min' => 12)); // WHERE advertiser_user_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $advertiserUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByAdvertiserUserId($advertiserUserId = null, $comparison = null)
    {
        if (is_array($advertiserUserId)) {
            $useMinMax = false;
            if (isset($advertiserUserId['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_ADVERTISER_USER_ID, $advertiserUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($advertiserUserId['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_ADVERTISER_USER_ID, $advertiserUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_ADVERTISER_USER_ID, $advertiserUserId, $comparison);
    }

    /**
     * Filter the query on the delivery_time_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDeliveryTimeId(1234); // WHERE delivery_time_id = 1234
     * $query->filterByDeliveryTimeId(array(12, 34)); // WHERE delivery_time_id IN (12, 34)
     * $query->filterByDeliveryTimeId(array('min' => 12)); // WHERE delivery_time_id > 12
     * </code>
     *
     * @param     mixed $deliveryTimeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByDeliveryTimeId($deliveryTimeId = null, $comparison = null)
    {
        if (is_array($deliveryTimeId)) {
            $useMinMax = false;
            if (isset($deliveryTimeId['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_DELIVERY_TIME_ID, $deliveryTimeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($deliveryTimeId['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_DELIVERY_TIME_ID, $deliveryTimeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_DELIVERY_TIME_ID, $deliveryTimeId, $comparison);
    }

    /**
     * Filter the query on the thickness column
     *
     * Example usage:
     * <code>
     * $query->filterByThickness(1234); // WHERE thickness = 1234
     * $query->filterByThickness(array(12, 34)); // WHERE thickness IN (12, 34)
     * $query->filterByThickness(array('min' => 12)); // WHERE thickness > 12
     * </code>
     *
     * @param     mixed $thickness The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByThickness($thickness = null, $comparison = null)
    {
        if (is_array($thickness)) {
            $useMinMax = false;
            if (isset($thickness['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_THICKNESS, $thickness['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($thickness['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_THICKNESS, $thickness['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_THICKNESS, $thickness, $comparison);
    }

    /**
     * Filter the query on the height column
     *
     * Example usage:
     * <code>
     * $query->filterByHeight('fooValue');   // WHERE height = 'fooValue'
     * $query->filterByHeight('%fooValue%', Criteria::LIKE); // WHERE height LIKE '%fooValue%'
     * </code>
     *
     * @param     string $height The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByHeight($height = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($height)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_HEIGHT, $height, $comparison);
    }

    /**
     * Filter the query on the width column
     *
     * Example usage:
     * <code>
     * $query->filterByWidth('fooValue');   // WHERE width = 'fooValue'
     * $query->filterByWidth('%fooValue%', Criteria::LIKE); // WHERE width LIKE '%fooValue%'
     * </code>
     *
     * @param     string $width The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByWidth($width = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($width)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_WIDTH, $width, $comparison);
    }

    /**
     * Filter the query on the length column
     *
     * Example usage:
     * <code>
     * $query->filterByLength('fooValue');   // WHERE length = 'fooValue'
     * $query->filterByLength('%fooValue%', Criteria::LIKE); // WHERE length LIKE '%fooValue%'
     * </code>
     *
     * @param     string $length The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByLength($length = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($length)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_LENGTH, $length, $comparison);
    }

    /**
     * Filter the query on the weight column
     *
     * Example usage:
     * <code>
     * $query->filterByWeight('fooValue');   // WHERE weight = 'fooValue'
     * $query->filterByWeight('%fooValue%', Criteria::LIKE); // WHERE weight LIKE '%fooValue%'
     * </code>
     *
     * @param     string $weight The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByWeight($weight = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($weight)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_WEIGHT, $weight, $comparison);
    }

    /**
     * Filter the query on the composition column
     *
     * Example usage:
     * <code>
     * $query->filterByComposition('fooValue');   // WHERE composition = 'fooValue'
     * $query->filterByComposition('%fooValue%', Criteria::LIKE); // WHERE composition LIKE '%fooValue%'
     * </code>
     *
     * @param     string $composition The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByComposition($composition = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($composition)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_COMPOSITION, $composition, $comparison);
    }

    /**
     * Filter the query on the material_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMaterialId('fooValue');   // WHERE material_id = 'fooValue'
     * $query->filterByMaterialId('%fooValue%', Criteria::LIKE); // WHERE material_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $materialId The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByMaterialId($materialId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($materialId)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_MATERIAL_ID, $materialId, $comparison);
    }

    /**
     * Filter the query on the brand_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBrandId(1234); // WHERE brand_id = 1234
     * $query->filterByBrandId(array(12, 34)); // WHERE brand_id IN (12, 34)
     * $query->filterByBrandId(array('min' => 12)); // WHERE brand_id > 12
     * </code>
     *
     * @see       filterByBrand()
     *
     * @param     mixed $brandId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByBrandId($brandId = null, $comparison = null)
    {
        if (is_array($brandId)) {
            $useMinMax = false;
            if (isset($brandId['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_BRAND_ID, $brandId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($brandId['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_BRAND_ID, $brandId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_BRAND_ID, $brandId, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE description = 'fooValue'
     * $query->filterByDescription('%fooValue%', Criteria::LIKE); // WHERE description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the note column
     *
     * Example usage:
     * <code>
     * $query->filterByNote('fooValue');   // WHERE note = 'fooValue'
     * $query->filterByNote('%fooValue%', Criteria::LIKE); // WHERE note LIKE '%fooValue%'
     * </code>
     *
     * @param     string $note The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByNote($note = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($note)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_NOTE, $note, $comparison);
    }

    /**
     * Filter the query on the created_on column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedOn('2011-03-14'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn('now'); // WHERE created_on = '2011-03-14'
     * $query->filterByCreatedOn(array('max' => 'yesterday')); // WHERE created_on > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdOn The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByCreatedOn($createdOn = null, $comparison = null)
    {
        if (is_array($createdOn)) {
            $useMinMax = false;
            if (isset($createdOn['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_CREATED_ON, $createdOn['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdOn['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_CREATED_ON, $createdOn['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_CREATED_ON, $createdOn, $comparison);
    }

    /**
     * Filter the query on the day_price column
     *
     * Example usage:
     * <code>
     * $query->filterByDayPrice(1234); // WHERE day_price = 1234
     * $query->filterByDayPrice(array(12, 34)); // WHERE day_price IN (12, 34)
     * $query->filterByDayPrice(array('min' => 12)); // WHERE day_price > 12
     * </code>
     *
     * @param     mixed $dayPrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByDayPrice($dayPrice = null, $comparison = null)
    {
        if (is_array($dayPrice)) {
            $useMinMax = false;
            if (isset($dayPrice['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_DAY_PRICE, $dayPrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dayPrice['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_DAY_PRICE, $dayPrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_DAY_PRICE, $dayPrice, $comparison);
    }

    /**
     * Filter the query on the sale_price column
     *
     * Example usage:
     * <code>
     * $query->filterBySalePrice(1234); // WHERE sale_price = 1234
     * $query->filterBySalePrice(array(12, 34)); // WHERE sale_price IN (12, 34)
     * $query->filterBySalePrice(array('min' => 12)); // WHERE sale_price > 12
     * </code>
     *
     * @param     mixed $salePrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterBySalePrice($salePrice = null, $comparison = null)
    {
        if (is_array($salePrice)) {
            $useMinMax = false;
            if (isset($salePrice['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_SALE_PRICE, $salePrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($salePrice['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_SALE_PRICE, $salePrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_SALE_PRICE, $salePrice, $comparison);
    }

    /**
     * Filter the query on the discount_percentage column
     *
     * Example usage:
     * <code>
     * $query->filterByDiscountPercentage(1234); // WHERE discount_percentage = 1234
     * $query->filterByDiscountPercentage(array(12, 34)); // WHERE discount_percentage IN (12, 34)
     * $query->filterByDiscountPercentage(array('min' => 12)); // WHERE discount_percentage > 12
     * </code>
     *
     * @param     mixed $discountPercentage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByDiscountPercentage($discountPercentage = null, $comparison = null)
    {
        if (is_array($discountPercentage)) {
            $useMinMax = false;
            if (isset($discountPercentage['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_DISCOUNT_PERCENTAGE, $discountPercentage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($discountPercentage['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_DISCOUNT_PERCENTAGE, $discountPercentage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_DISCOUNT_PERCENTAGE, $discountPercentage, $comparison);
    }

    /**
     * Filter the query on the purchase_price column
     *
     * Example usage:
     * <code>
     * $query->filterByPurchasePrice(1234); // WHERE purchase_price = 1234
     * $query->filterByPurchasePrice(array(12, 34)); // WHERE purchase_price IN (12, 34)
     * $query->filterByPurchasePrice(array('min' => 12)); // WHERE purchase_price > 12
     * </code>
     *
     * @param     mixed $purchasePrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByPurchasePrice($purchasePrice = null, $comparison = null)
    {
        if (is_array($purchasePrice)) {
            $useMinMax = false;
            if (isset($purchasePrice['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_PURCHASE_PRICE, $purchasePrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($purchasePrice['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_PURCHASE_PRICE, $purchasePrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_PURCHASE_PRICE, $purchasePrice, $comparison);
    }

    /**
     * Filter the query on the reseller_price column
     *
     * Example usage:
     * <code>
     * $query->filterByResellerPrice(1234); // WHERE reseller_price = 1234
     * $query->filterByResellerPrice(array(12, 34)); // WHERE reseller_price IN (12, 34)
     * $query->filterByResellerPrice(array('min' => 12)); // WHERE reseller_price > 12
     * </code>
     *
     * @param     mixed $resellerPrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByResellerPrice($resellerPrice = null, $comparison = null)
    {
        if (is_array($resellerPrice)) {
            $useMinMax = false;
            if (isset($resellerPrice['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_RESELLER_PRICE, $resellerPrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($resellerPrice['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_RESELLER_PRICE, $resellerPrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_RESELLER_PRICE, $resellerPrice, $comparison);
    }

    /**
     * Filter the query on the advice_price column
     *
     * Example usage:
     * <code>
     * $query->filterByAdvicePrice(1234); // WHERE advice_price = 1234
     * $query->filterByAdvicePrice(array(12, 34)); // WHERE advice_price IN (12, 34)
     * $query->filterByAdvicePrice(array('min' => 12)); // WHERE advice_price > 12
     * </code>
     *
     * @param     mixed $advicePrice The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByAdvicePrice($advicePrice = null, $comparison = null)
    {
        if (is_array($advicePrice)) {
            $useMinMax = false;
            if (isset($advicePrice['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_ADVICE_PRICE, $advicePrice['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($advicePrice['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_ADVICE_PRICE, $advicePrice['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_ADVICE_PRICE, $advicePrice, $comparison);
    }

    /**
     * Filter the query on the bulk_stock_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBulkStockId(1234); // WHERE bulk_stock_id = 1234
     * $query->filterByBulkStockId(array(12, 34)); // WHERE bulk_stock_id IN (12, 34)
     * $query->filterByBulkStockId(array('min' => 12)); // WHERE bulk_stock_id > 12
     * </code>
     *
     * @param     mixed $bulkStockId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByBulkStockId($bulkStockId = null, $comparison = null)
    {
        if (is_array($bulkStockId)) {
            $useMinMax = false;
            if (isset($bulkStockId['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_BULK_STOCK_ID, $bulkStockId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bulkStockId['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_BULK_STOCK_ID, $bulkStockId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_BULK_STOCK_ID, $bulkStockId, $comparison);
    }

    /**
     * Filter the query on the stock_quantity column
     *
     * Example usage:
     * <code>
     * $query->filterByStockQuantity(1234); // WHERE stock_quantity = 1234
     * $query->filterByStockQuantity(array(12, 34)); // WHERE stock_quantity IN (12, 34)
     * $query->filterByStockQuantity(array('min' => 12)); // WHERE stock_quantity > 12
     * </code>
     *
     * @param     mixed $stockQuantity The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByStockQuantity($stockQuantity = null, $comparison = null)
    {
        if (is_array($stockQuantity)) {
            $useMinMax = false;
            if (isset($stockQuantity['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_STOCK_QUANTITY, $stockQuantity['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($stockQuantity['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_STOCK_QUANTITY, $stockQuantity['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_STOCK_QUANTITY, $stockQuantity, $comparison);
    }

    /**
     * Filter the query on the virtual_stock column
     *
     * Example usage:
     * <code>
     * $query->filterByVirtualStock(true); // WHERE virtual_stock = true
     * $query->filterByVirtualStock('yes'); // WHERE virtual_stock = true
     * </code>
     *
     * @param     boolean|string $virtualStock The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByVirtualStock($virtualStock = null, $comparison = null)
    {
        if (is_string($virtualStock)) {
            $virtualStock = in_array(strtolower($virtualStock), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ProductTableMap::COL_VIRTUAL_STOCK, $virtualStock, $comparison);
    }

    /**
     * Filter the query on the quantity_per_pack column
     *
     * Example usage:
     * <code>
     * $query->filterByQuantityPerPack(1234); // WHERE quantity_per_pack = 1234
     * $query->filterByQuantityPerPack(array(12, 34)); // WHERE quantity_per_pack IN (12, 34)
     * $query->filterByQuantityPerPack(array('min' => 12)); // WHERE quantity_per_pack > 12
     * </code>
     *
     * @param     mixed $quantityPerPack The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByQuantityPerPack($quantityPerPack = null, $comparison = null)
    {
        if (is_array($quantityPerPack)) {
            $useMinMax = false;
            if (isset($quantityPerPack['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_QUANTITY_PER_PACK, $quantityPerPack['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($quantityPerPack['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_QUANTITY_PER_PACK, $quantityPerPack['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_QUANTITY_PER_PACK, $quantityPerPack, $comparison);
    }

    /**
     * Filter the query on the youtube_url column
     *
     * Example usage:
     * <code>
     * $query->filterByYoutubeUrl('fooValue');   // WHERE youtube_url = 'fooValue'
     * $query->filterByYoutubeUrl('%fooValue%', Criteria::LIKE); // WHERE youtube_url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $youtubeUrl The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByYoutubeUrl($youtubeUrl = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($youtubeUrl)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_YOUTUBE_URL, $youtubeUrl, $comparison);
    }

    /**
     * Filter the query on the meta_title column
     *
     * Example usage:
     * <code>
     * $query->filterByMetaTitle('fooValue');   // WHERE meta_title = 'fooValue'
     * $query->filterByMetaTitle('%fooValue%', Criteria::LIKE); // WHERE meta_title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metaTitle The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByMetaTitle($metaTitle = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metaTitle)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_META_TITLE, $metaTitle, $comparison);
    }

    /**
     * Filter the query on the meta_description column
     *
     * Example usage:
     * <code>
     * $query->filterByMetaDescription('fooValue');   // WHERE meta_description = 'fooValue'
     * $query->filterByMetaDescription('%fooValue%', Criteria::LIKE); // WHERE meta_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metaDescription The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByMetaDescription($metaDescription = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metaDescription)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_META_DESCRIPTION, $metaDescription, $comparison);
    }

    /**
     * Filter the query on the meta_keyword column
     *
     * Example usage:
     * <code>
     * $query->filterByMetaKeyword('fooValue');   // WHERE meta_keyword = 'fooValue'
     * $query->filterByMetaKeyword('%fooValue%', Criteria::LIKE); // WHERE meta_keyword LIKE '%fooValue%'
     * </code>
     *
     * @param     string $metaKeyword The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByMetaKeyword($metaKeyword = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($metaKeyword)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_META_KEYWORD, $metaKeyword, $comparison);
    }

    /**
     * Filter the query on the hashtag column
     *
     * Example usage:
     * <code>
     * $query->filterByHashtag('fooValue');   // WHERE hashtag = 'fooValue'
     * $query->filterByHashtag('%fooValue%', Criteria::LIKE); // WHERE hashtag LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hashtag The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByHashtag($hashtag = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hashtag)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_HASHTAG, $hashtag, $comparison);
    }

    /**
     * Filter the query on the calculated_average_rating column
     *
     * Example usage:
     * <code>
     * $query->filterByCalculatedAverageRating(1234); // WHERE calculated_average_rating = 1234
     * $query->filterByCalculatedAverageRating(array(12, 34)); // WHERE calculated_average_rating IN (12, 34)
     * $query->filterByCalculatedAverageRating(array('min' => 12)); // WHERE calculated_average_rating > 12
     * </code>
     *
     * @param     mixed $calculatedAverageRating The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByCalculatedAverageRating($calculatedAverageRating = null, $comparison = null)
    {
        if (is_array($calculatedAverageRating)) {
            $useMinMax = false;
            if (isset($calculatedAverageRating['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_CALCULATED_AVERAGE_RATING, $calculatedAverageRating['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($calculatedAverageRating['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_CALCULATED_AVERAGE_RATING, $calculatedAverageRating['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_CALCULATED_AVERAGE_RATING, $calculatedAverageRating, $comparison);
    }

    /**
     * Filter the query on the vat_id column
     *
     * Example usage:
     * <code>
     * $query->filterByVatId(1234); // WHERE vat_id = 1234
     * $query->filterByVatId(array(12, 34)); // WHERE vat_id IN (12, 34)
     * $query->filterByVatId(array('min' => 12)); // WHERE vat_id > 12
     * </code>
     *
     * @see       filterByVat()
     *
     * @param     mixed $vatId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByVatId($vatId = null, $comparison = null)
    {
        if (is_array($vatId)) {
            $useMinMax = false;
            if (isset($vatId['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_VAT_ID, $vatId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($vatId['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_VAT_ID, $vatId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_VAT_ID, $vatId, $comparison);
    }

    /**
     * Filter the query on the unit_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUnitId(1234); // WHERE unit_id = 1234
     * $query->filterByUnitId(array(12, 34)); // WHERE unit_id IN (12, 34)
     * $query->filterByUnitId(array('min' => 12)); // WHERE unit_id > 12
     * </code>
     *
     * @see       filterByProductUnit()
     *
     * @param     mixed $unitId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByUnitId($unitId = null, $comparison = null)
    {
        if (is_array($unitId)) {
            $useMinMax = false;
            if (isset($unitId['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_UNIT_ID, $unitId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($unitId['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_UNIT_ID, $unitId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_UNIT_ID, $unitId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ProductTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ProductTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Brand object
     *
     * @param \Model\Setting\MasterTable\Brand|ObjectCollection $brand The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByBrand($brand, $comparison = null)
    {
        if ($brand instanceof \Model\Setting\MasterTable\Brand) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_BRAND_ID, $brand->getId(), $comparison);
        } elseif ($brand instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProductTableMap::COL_BRAND_ID, $brand->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByBrand() only accepts arguments of type \Model\Setting\MasterTable\Brand or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Brand relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinBrand($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Brand');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Brand');
        }

        return $this;
    }

    /**
     * Use the Brand relation Brand object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\BrandQuery A secondary query class using the current class as primary query
     */
    public function useBrandQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinBrand($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Brand', '\Model\Setting\MasterTable\BrandQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\ProductUnit object
     *
     * @param \Model\Setting\MasterTable\ProductUnit|ObjectCollection $productUnit The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByProductUnit($productUnit, $comparison = null)
    {
        if ($productUnit instanceof \Model\Setting\MasterTable\ProductUnit) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_UNIT_ID, $productUnit->getId(), $comparison);
        } elseif ($productUnit instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProductTableMap::COL_UNIT_ID, $productUnit->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProductUnit() only accepts arguments of type \Model\Setting\MasterTable\ProductUnit or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductUnit relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinProductUnit($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductUnit');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductUnit');
        }

        return $this;
    }

    /**
     * Use the ProductUnit relation ProductUnit object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\ProductUnitQuery A secondary query class using the current class as primary query
     */
    public function useProductUnitQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinProductUnit($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductUnit', '\Model\Setting\MasterTable\ProductUnitQuery');
    }

    /**
     * Filter the query by a related \Model\Supplier\Supplier object
     *
     * @param \Model\Supplier\Supplier|ObjectCollection $supplier The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterBySupplierRef($supplier, $comparison = null)
    {
        if ($supplier instanceof \Model\Supplier\Supplier) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_SUPPLIER_ID, $supplier->getId(), $comparison);
        } elseif ($supplier instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProductTableMap::COL_SUPPLIER_ID, $supplier->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySupplierRef() only accepts arguments of type \Model\Supplier\Supplier or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SupplierRef relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinSupplierRef($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SupplierRef');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SupplierRef');
        }

        return $this;
    }

    /**
     * Use the SupplierRef relation Supplier object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Supplier\SupplierQuery A secondary query class using the current class as primary query
     */
    public function useSupplierRefQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSupplierRef($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SupplierRef', '\Model\Supplier\SupplierQuery');
    }

    /**
     * Filter the query by a related \Model\Category\Category object
     *
     * @param \Model\Category\Category|ObjectCollection $category The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByCategory($category, $comparison = null)
    {
        if ($category instanceof \Model\Category\Category) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_CATEGORY_ID, $category->getId(), $comparison);
        } elseif ($category instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProductTableMap::COL_CATEGORY_ID, $category->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCategory() only accepts arguments of type \Model\Category\Category or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Category relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinCategory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Category');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Category');
        }

        return $this;
    }

    /**
     * Use the Category relation Category object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Category\CategoryQuery A secondary query class using the current class as primary query
     */
    public function useCategoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Category', '\Model\Category\CategoryQuery');
    }

    /**
     * Filter the query by a related \Model\Account\User object
     *
     * @param \Model\Account\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof \Model\Account\User) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ADVERTISER_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProductTableMap::COL_ADVERTISER_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \Model\Account\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Account\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\Model\Account\UserQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Vat object
     *
     * @param \Model\Setting\MasterTable\Vat|ObjectCollection $vat The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByVat($vat, $comparison = null)
    {
        if ($vat instanceof \Model\Setting\MasterTable\Vat) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_VAT_ID, $vat->getId(), $comparison);
        } elseif ($vat instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProductTableMap::COL_VAT_ID, $vat->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByVat() only accepts arguments of type \Model\Setting\MasterTable\Vat or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Vat relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinVat($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Vat');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Vat');
        }

        return $this;
    }

    /**
     * Use the Vat relation Vat object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\VatQuery A secondary query class using the current class as primary query
     */
    public function useVatQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinVat($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Vat', '\Model\Setting\MasterTable\VatQuery');
    }

    /**
     * Filter the query by a related \Model\Category\ProductMultiCategory object
     *
     * @param \Model\Category\ProductMultiCategory|ObjectCollection $productMultiCategory the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByProductMultiCategory($productMultiCategory, $comparison = null)
    {
        if ($productMultiCategory instanceof \Model\Category\ProductMultiCategory) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $productMultiCategory->getProductId(), $comparison);
        } elseif ($productMultiCategory instanceof ObjectCollection) {
            return $this
                ->useProductMultiCategoryQuery()
                ->filterByPrimaryKeys($productMultiCategory->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductMultiCategory() only accepts arguments of type \Model\Category\ProductMultiCategory or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductMultiCategory relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinProductMultiCategory($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductMultiCategory');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductMultiCategory');
        }

        return $this;
    }

    /**
     * Use the ProductMultiCategory relation ProductMultiCategory object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Category\ProductMultiCategoryQuery A secondary query class using the current class as primary query
     */
    public function useProductMultiCategoryQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProductMultiCategory($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductMultiCategory', '\Model\Category\ProductMultiCategoryQuery');
    }

    /**
     * Filter the query by a related \Model\Sale\OrderItemProduct object
     *
     * @param \Model\Sale\OrderItemProduct|ObjectCollection $orderItemProduct the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterBySaleOrderItemProduct($orderItemProduct, $comparison = null)
    {
        if ($orderItemProduct instanceof \Model\Sale\OrderItemProduct) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $orderItemProduct->getProductId(), $comparison);
        } elseif ($orderItemProduct instanceof ObjectCollection) {
            return $this
                ->useSaleOrderItemProductQuery()
                ->filterByPrimaryKeys($orderItemProduct->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleOrderItemProduct() only accepts arguments of type \Model\Sale\OrderItemProduct or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrderItemProduct relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinSaleOrderItemProduct($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrderItemProduct');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrderItemProduct');
        }

        return $this;
    }

    /**
     * Use the SaleOrderItemProduct relation OrderItemProduct object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\OrderItemProductQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderItemProductQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaleOrderItemProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrderItemProduct', '\Model\Sale\OrderItemProductQuery');
    }

    /**
     * Filter the query by a related \Model\ProductReview object
     *
     * @param \Model\ProductReview|ObjectCollection $productReview the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByProductReview($productReview, $comparison = null)
    {
        if ($productReview instanceof \Model\ProductReview) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $productReview->getProductId(), $comparison);
        } elseif ($productReview instanceof ObjectCollection) {
            return $this
                ->useProductReviewQuery()
                ->filterByPrimaryKeys($productReview->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductReview() only accepts arguments of type \Model\ProductReview or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductReview relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinProductReview($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductReview');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductReview');
        }

        return $this;
    }

    /**
     * Use the ProductReview relation ProductReview object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ProductReviewQuery A secondary query class using the current class as primary query
     */
    public function useProductReviewQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProductReview($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductReview', '\Model\ProductReviewQuery');
    }

    /**
     * Filter the query by a related \Model\PromoProduct object
     *
     * @param \Model\PromoProduct|ObjectCollection $promoProduct the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByPromoProduct($promoProduct, $comparison = null)
    {
        if ($promoProduct instanceof \Model\PromoProduct) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $promoProduct->getProductId(), $comparison);
        } elseif ($promoProduct instanceof ObjectCollection) {
            return $this
                ->usePromoProductQuery()
                ->filterByPrimaryKeys($promoProduct->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPromoProduct() only accepts arguments of type \Model\PromoProduct or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the PromoProduct relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinPromoProduct($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('PromoProduct');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'PromoProduct');
        }

        return $this;
    }

    /**
     * Use the PromoProduct relation PromoProduct object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\PromoProductQuery A secondary query class using the current class as primary query
     */
    public function usePromoProductQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinPromoProduct($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'PromoProduct', '\Model\PromoProductQuery');
    }

    /**
     * Filter the query by a related \Model\ProductProperty object
     *
     * @param \Model\ProductProperty|ObjectCollection $productProperty the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByProductProperty($productProperty, $comparison = null)
    {
        if ($productProperty instanceof \Model\ProductProperty) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $productProperty->getProductId(), $comparison);
        } elseif ($productProperty instanceof ObjectCollection) {
            return $this
                ->useProductPropertyQuery()
                ->filterByPrimaryKeys($productProperty->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductProperty() only accepts arguments of type \Model\ProductProperty or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductProperty relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinProductProperty($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductProperty');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductProperty');
        }

        return $this;
    }

    /**
     * Use the ProductProperty relation ProductProperty object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ProductPropertyQuery A secondary query class using the current class as primary query
     */
    public function useProductPropertyQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinProductProperty($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductProperty', '\Model\ProductPropertyQuery');
    }

    /**
     * Filter the query by a related \Model\ProductColor object
     *
     * @param \Model\ProductColor|ObjectCollection $productColor the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByProductColor($productColor, $comparison = null)
    {
        if ($productColor instanceof \Model\ProductColor) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $productColor->getProductId(), $comparison);
        } elseif ($productColor instanceof ObjectCollection) {
            return $this
                ->useProductColorQuery()
                ->filterByPrimaryKeys($productColor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductColor() only accepts arguments of type \Model\ProductColor or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductColor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinProductColor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductColor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductColor');
        }

        return $this;
    }

    /**
     * Use the ProductColor relation ProductColor object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ProductColorQuery A secondary query class using the current class as primary query
     */
    public function useProductColorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProductColor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductColor', '\Model\ProductColorQuery');
    }

    /**
     * Filter the query by a related \Model\ProductList object
     *
     * @param \Model\ProductList|ObjectCollection $productList the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByProductList($productList, $comparison = null)
    {
        if ($productList instanceof \Model\ProductList) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $productList->getProductId(), $comparison);
        } elseif ($productList instanceof ObjectCollection) {
            return $this
                ->useProductListQuery()
                ->filterByPrimaryKeys($productList->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductList() only accepts arguments of type \Model\ProductList or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductList relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinProductList($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductList');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductList');
        }

        return $this;
    }

    /**
     * Use the ProductList relation ProductList object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ProductListQuery A secondary query class using the current class as primary query
     */
    public function useProductListQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProductList($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductList', '\Model\ProductListQuery');
    }

    /**
     * Filter the query by a related \Model\ProductUsage object
     *
     * @param \Model\ProductUsage|ObjectCollection $productUsage the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByProductUsage($productUsage, $comparison = null)
    {
        if ($productUsage instanceof \Model\ProductUsage) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $productUsage->getProductId(), $comparison);
        } elseif ($productUsage instanceof ObjectCollection) {
            return $this
                ->useProductUsageQuery()
                ->filterByPrimaryKeys($productUsage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductUsage() only accepts arguments of type \Model\ProductUsage or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductUsage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinProductUsage($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductUsage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductUsage');
        }

        return $this;
    }

    /**
     * Use the ProductUsage relation ProductUsage object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ProductUsageQuery A secondary query class using the current class as primary query
     */
    public function useProductUsageQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProductUsage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductUsage', '\Model\ProductUsageQuery');
    }

    /**
     * Filter the query by a related \Model\ProductTranslation object
     *
     * @param \Model\ProductTranslation|ObjectCollection $productTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByProductTranslation($productTranslation, $comparison = null)
    {
        if ($productTranslation instanceof \Model\ProductTranslation) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $productTranslation->getProductId(), $comparison);
        } elseif ($productTranslation instanceof ObjectCollection) {
            return $this
                ->useProductTranslationQuery()
                ->filterByPrimaryKeys($productTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductTranslation() only accepts arguments of type \Model\ProductTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductTranslation relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinProductTranslation($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductTranslation');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductTranslation');
        }

        return $this;
    }

    /**
     * Use the ProductTranslation relation ProductTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\ProductTranslationQuery A secondary query class using the current class as primary query
     */
    public function useProductTranslationQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProductTranslation($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductTranslation', '\Model\ProductTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\Product\RelatedProduct object
     *
     * @param \Model\Product\RelatedProduct|ObjectCollection $relatedProduct the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByRelatedProductRelatedByProductId($relatedProduct, $comparison = null)
    {
        if ($relatedProduct instanceof \Model\Product\RelatedProduct) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $relatedProduct->getProductId(), $comparison);
        } elseif ($relatedProduct instanceof ObjectCollection) {
            return $this
                ->useRelatedProductRelatedByProductIdQuery()
                ->filterByPrimaryKeys($relatedProduct->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRelatedProductRelatedByProductId() only accepts arguments of type \Model\Product\RelatedProduct or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RelatedProductRelatedByProductId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinRelatedProductRelatedByProductId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RelatedProductRelatedByProductId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RelatedProductRelatedByProductId');
        }

        return $this;
    }

    /**
     * Use the RelatedProductRelatedByProductId relation RelatedProduct object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Product\RelatedProductQuery A secondary query class using the current class as primary query
     */
    public function useRelatedProductRelatedByProductIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRelatedProductRelatedByProductId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RelatedProductRelatedByProductId', '\Model\Product\RelatedProductQuery');
    }

    /**
     * Filter the query by a related \Model\Product\RelatedProduct object
     *
     * @param \Model\Product\RelatedProduct|ObjectCollection $relatedProduct the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByRelatedProductRelatedByOtherProductId($relatedProduct, $comparison = null)
    {
        if ($relatedProduct instanceof \Model\Product\RelatedProduct) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $relatedProduct->getOtherProductId(), $comparison);
        } elseif ($relatedProduct instanceof ObjectCollection) {
            return $this
                ->useRelatedProductRelatedByOtherProductIdQuery()
                ->filterByPrimaryKeys($relatedProduct->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRelatedProductRelatedByOtherProductId() only accepts arguments of type \Model\Product\RelatedProduct or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RelatedProductRelatedByOtherProductId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinRelatedProductRelatedByOtherProductId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RelatedProductRelatedByOtherProductId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RelatedProductRelatedByOtherProductId');
        }

        return $this;
    }

    /**
     * Use the RelatedProductRelatedByOtherProductId relation RelatedProduct object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Product\RelatedProductQuery A secondary query class using the current class as primary query
     */
    public function useRelatedProductRelatedByOtherProductIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRelatedProductRelatedByOtherProductId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RelatedProductRelatedByOtherProductId', '\Model\Product\RelatedProductQuery');
    }

    /**
     * Filter the query by a related \Model\Product\Image\ProductImage object
     *
     * @param \Model\Product\Image\ProductImage|ObjectCollection $productImage the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByProductImage($productImage, $comparison = null)
    {
        if ($productImage instanceof \Model\Product\Image\ProductImage) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $productImage->getProductId(), $comparison);
        } elseif ($productImage instanceof ObjectCollection) {
            return $this
                ->useProductImageQuery()
                ->filterByPrimaryKeys($productImage->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductImage() only accepts arguments of type \Model\Product\Image\ProductImage or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductImage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinProductImage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductImage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductImage');
        }

        return $this;
    }

    /**
     * Use the ProductImage relation ProductImage object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Product\Image\ProductImageQuery A secondary query class using the current class as primary query
     */
    public function useProductImageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinProductImage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductImage', '\Model\Product\Image\ProductImageQuery');
    }

    /**
     * Filter the query by a related \Model\Product\ProductTag object
     *
     * @param \Model\Product\ProductTag|ObjectCollection $productTag the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByProductTag($productTag, $comparison = null)
    {
        if ($productTag instanceof \Model\Product\ProductTag) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $productTag->getProductId(), $comparison);
        } elseif ($productTag instanceof ObjectCollection) {
            return $this
                ->useProductTagQuery()
                ->filterByPrimaryKeys($productTag->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProductTag() only accepts arguments of type \Model\Product\ProductTag or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProductTag relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinProductTag($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProductTag');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProductTag');
        }

        return $this;
    }

    /**
     * Use the ProductTag relation ProductTag object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Product\ProductTagQuery A secondary query class using the current class as primary query
     */
    public function useProductTagQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProductTag($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProductTag', '\Model\Product\ProductTagQuery');
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrderItemStockClaimed object
     *
     * @param \Model\Sale\SaleOrderItemStockClaimed|ObjectCollection $saleOrderItemStockClaimed the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterBySaleOrderItemStockClaimed($saleOrderItemStockClaimed, $comparison = null)
    {
        if ($saleOrderItemStockClaimed instanceof \Model\Sale\SaleOrderItemStockClaimed) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $saleOrderItemStockClaimed->getProductId(), $comparison);
        } elseif ($saleOrderItemStockClaimed instanceof ObjectCollection) {
            return $this
                ->useSaleOrderItemStockClaimedQuery()
                ->filterByPrimaryKeys($saleOrderItemStockClaimed->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterBySaleOrderItemStockClaimed() only accepts arguments of type \Model\Sale\SaleOrderItemStockClaimed or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrderItemStockClaimed relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinSaleOrderItemStockClaimed($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrderItemStockClaimed');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrderItemStockClaimed');
        }

        return $this;
    }

    /**
     * Use the SaleOrderItemStockClaimed relation SaleOrderItemStockClaimed object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderItemStockClaimedQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderItemStockClaimedQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinSaleOrderItemStockClaimed($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrderItemStockClaimed', '\Model\Sale\SaleOrderItemStockClaimedQuery');
    }

    /**
     * Filter the query by a related \Model\Category\CustomerWishlist object
     *
     * @param \Model\Category\CustomerWishlist|ObjectCollection $customerWishlist the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProductQuery The current query, for fluid interface
     */
    public function filterByCustomerWishlist($customerWishlist, $comparison = null)
    {
        if ($customerWishlist instanceof \Model\Category\CustomerWishlist) {
            return $this
                ->addUsingAlias(ProductTableMap::COL_ID, $customerWishlist->getProductId(), $comparison);
        } elseif ($customerWishlist instanceof ObjectCollection) {
            return $this
                ->useCustomerWishlistQuery()
                ->filterByPrimaryKeys($customerWishlist->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByCustomerWishlist() only accepts arguments of type \Model\Category\CustomerWishlist or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CustomerWishlist relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function joinCustomerWishlist($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CustomerWishlist');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CustomerWishlist');
        }

        return $this;
    }

    /**
     * Use the CustomerWishlist relation CustomerWishlist object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Category\CustomerWishlistQuery A secondary query class using the current class as primary query
     */
    public function useCustomerWishlistQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCustomerWishlist($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CustomerWishlist', '\Model\Category\CustomerWishlistQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildProduct $product Object to remove from the list of results
     *
     * @return $this|ChildProductQuery The current query, for fluid interface
     */
    public function prune($product = null)
    {
        if ($product) {
            $this->addUsingAlias(ProductTableMap::COL_ID, $product->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the product table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProductTableMap::clearInstancePool();
            ProductTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProductTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProductTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProductTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildProductQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ProductTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildProductQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ProductTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildProductQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ProductTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildProductQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ProductTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildProductQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ProductTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildProductQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ProductTableMap::COL_CREATED_AT);
    }

} // ProductQuery
