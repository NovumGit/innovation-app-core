<?php

namespace Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Product as ChildProduct;
use Model\ProductColor as ChildProductColor;
use Model\ProductColorQuery as ChildProductColorQuery;
use Model\ProductList as ChildProductList;
use Model\ProductListQuery as ChildProductListQuery;
use Model\ProductProperty as ChildProductProperty;
use Model\ProductPropertyQuery as ChildProductPropertyQuery;
use Model\ProductQuery as ChildProductQuery;
use Model\ProductReview as ChildProductReview;
use Model\ProductReviewQuery as ChildProductReviewQuery;
use Model\ProductTranslation as ChildProductTranslation;
use Model\ProductTranslationQuery as ChildProductTranslationQuery;
use Model\ProductUsage as ChildProductUsage;
use Model\ProductUsageQuery as ChildProductUsageQuery;
use Model\PromoProduct as ChildPromoProduct;
use Model\PromoProductQuery as ChildPromoProductQuery;
use Model\Account\User;
use Model\Account\UserQuery;
use Model\Category\Category;
use Model\Category\CategoryQuery;
use Model\Category\CustomerWishlist;
use Model\Category\CustomerWishlistQuery;
use Model\Category\ProductMultiCategory;
use Model\Category\ProductMultiCategoryQuery;
use Model\Category\Base\CustomerWishlist as BaseCustomerWishlist;
use Model\Category\Base\ProductMultiCategory as BaseProductMultiCategory;
use Model\Category\Map\CustomerWishlistTableMap;
use Model\Category\Map\ProductMultiCategoryTableMap;
use Model\Map\ProductColorTableMap;
use Model\Map\ProductListTableMap;
use Model\Map\ProductPropertyTableMap;
use Model\Map\ProductReviewTableMap;
use Model\Map\ProductTableMap;
use Model\Map\ProductTranslationTableMap;
use Model\Map\ProductUsageTableMap;
use Model\Map\PromoProductTableMap;
use Model\Product\ProductTag;
use Model\Product\ProductTagQuery;
use Model\Product\RelatedProduct;
use Model\Product\RelatedProductQuery;
use Model\Product\Base\ProductTag as BaseProductTag;
use Model\Product\Base\RelatedProduct as BaseRelatedProduct;
use Model\Product\Image\ProductImage;
use Model\Product\Image\ProductImageQuery;
use Model\Product\Image\Base\ProductImage as BaseProductImage;
use Model\Product\Image\Map\ProductImageTableMap;
use Model\Product\Map\ProductTagTableMap;
use Model\Product\Map\RelatedProductTableMap;
use Model\Sale\OrderItemProduct;
use Model\Sale\OrderItemProductQuery;
use Model\Sale\SaleOrderItemStockClaimed;
use Model\Sale\SaleOrderItemStockClaimedQuery;
use Model\Sale\Base\OrderItemProduct as BaseOrderItemProduct;
use Model\Sale\Base\SaleOrderItemStockClaimed as BaseSaleOrderItemStockClaimed;
use Model\Sale\Map\OrderItemProductTableMap;
use Model\Sale\Map\SaleOrderItemStockClaimedTableMap;
use Model\Setting\MasterTable\Brand;
use Model\Setting\MasterTable\BrandQuery;
use Model\Setting\MasterTable\ProductUnit;
use Model\Setting\MasterTable\ProductUnitQuery;
use Model\Setting\MasterTable\Vat;
use Model\Setting\MasterTable\VatQuery;
use Model\Supplier\Supplier;
use Model\Supplier\SupplierQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'product' table.
 *
 *
 *
 * @package    propel.generator.Model.Base
 */
abstract class Product implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Map\\ProductTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the derrived_from_id field.
     *
     * @var        int|null
     */
    protected $derrived_from_id;

    /**
     * The value for the old_id field.
     *
     * @var        string|null
     */
    protected $old_id;

    /**
     * The value for the import_tag field.
     *
     * @var        string|null
     */
    protected $import_tag;

    /**
     * The value for the sku field.
     *
     * @var        string|null
     */
    protected $sku;

    /**
     * The value for the ean field.
     *
     * @var        string|null
     */
    protected $ean;

    /**
     * The value for the deleted_on field.
     *
     * @var        DateTime|null
     */
    protected $deleted_on;

    /**
     * The value for the deleted_by_user_id field.
     *
     * @var        int|null
     */
    protected $deleted_by_user_id;

    /**
     * The value for the popularity field.
     *
     * @var        int|null
     */
    protected $popularity;

    /**
     * The value for the avg_rating field.
     *
     * @var        int|null
     */
    protected $avg_rating;

    /**
     * The value for the supplier field.
     *
     * @var        string|null
     */
    protected $supplier;

    /**
     * The value for the supplier_id field.
     *
     * @var        int|null
     */
    protected $supplier_id;

    /**
     * The value for the supplier_product_number field.
     *
     * @var        string|null
     */
    protected $supplier_product_number;

    /**
     * The value for the in_sale field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $in_sale;

    /**
     * The value for the in_spotlight field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $in_spotlight;

    /**
     * The value for the in_webshop field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean|null
     */
    protected $in_webshop;

    /**
     * The value for the is_offer field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_offer;

    /**
     * The value for the is_bestseller field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_bestseller;

    /**
     * The value for the is_out_of_stock field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_out_of_stock;

    /**
     * The value for the has_images field.
     *
     * Note: this column has a database default value of: true
     * @var        boolean|null
     */
    protected $has_images;

    /**
     * The value for the category_id field.
     *
     * @var        int
     */
    protected $category_id;

    /**
     * The value for the number field.
     *
     * @var        string|null
     */
    protected $number;

    /**
     * The value for the advertiser_user_id field.
     *
     * @var        int|null
     */
    protected $advertiser_user_id;

    /**
     * The value for the delivery_time_id field.
     *
     * @var        int|null
     */
    protected $delivery_time_id;

    /**
     * The value for the thickness field.
     *
     * @var        double|null
     */
    protected $thickness;

    /**
     * The value for the height field.
     *
     * @var        string|null
     */
    protected $height;

    /**
     * The value for the width field.
     *
     * @var        string|null
     */
    protected $width;

    /**
     * The value for the length field.
     *
     * @var        string|null
     */
    protected $length;

    /**
     * The value for the weight field.
     *
     * @var        string|null
     */
    protected $weight;

    /**
     * The value for the composition field.
     *
     * @var        string|null
     */
    protected $composition;

    /**
     * The value for the material_id field.
     *
     * @var        string|null
     */
    protected $material_id;

    /**
     * The value for the brand_id field.
     *
     * @var        int|null
     */
    protected $brand_id;

    /**
     * The value for the title field.
     *
     * @var        string|null
     */
    protected $title;

    /**
     * The value for the description field.
     *
     * @var        string|null
     */
    protected $description;

    /**
     * The value for the note field.
     *
     * @var        string|null
     */
    protected $note;

    /**
     * The value for the created_on field.
     *
     * Note: this column has a database default value of: (expression) CURRENT_TIMESTAMP
     * @var        DateTime|null
     */
    protected $created_on;

    /**
     * The value for the day_price field.
     *
     * @var        double|null
     */
    protected $day_price;

    /**
     * The value for the sale_price field.
     *
     * @var        double|null
     */
    protected $sale_price;

    /**
     * The value for the discount_percentage field.
     *
     * @var        double|null
     */
    protected $discount_percentage;

    /**
     * The value for the purchase_price field.
     *
     * @var        string|null
     */
    protected $purchase_price;

    /**
     * The value for the reseller_price field.
     *
     * @var        double|null
     */
    protected $reseller_price;

    /**
     * The value for the advice_price field.
     *
     * @var        double|null
     */
    protected $advice_price;

    /**
     * The value for the bulk_stock_id field.
     *
     * @var        int|null
     */
    protected $bulk_stock_id;

    /**
     * The value for the stock_quantity field.
     *
     * @var        int|null
     */
    protected $stock_quantity;

    /**
     * The value for the virtual_stock field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $virtual_stock;

    /**
     * The value for the quantity_per_pack field.
     *
     * @var        int|null
     */
    protected $quantity_per_pack;

    /**
     * The value for the youtube_url field.
     *
     * @var        string|null
     */
    protected $youtube_url;

    /**
     * The value for the meta_title field.
     *
     * @var        string|null
     */
    protected $meta_title;

    /**
     * The value for the meta_description field.
     *
     * @var        string|null
     */
    protected $meta_description;

    /**
     * The value for the meta_keyword field.
     *
     * @var        string|null
     */
    protected $meta_keyword;

    /**
     * The value for the hashtag field.
     *
     * @var        string|null
     */
    protected $hashtag;

    /**
     * The value for the calculated_average_rating field.
     *
     * @var        int|null
     */
    protected $calculated_average_rating;

    /**
     * The value for the vat_id field.
     *
     * @var        int|null
     */
    protected $vat_id;

    /**
     * The value for the unit_id field.
     *
     * @var        int|null
     */
    protected $unit_id;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        Brand
     */
    protected $aBrand;

    /**
     * @var        ProductUnit
     */
    protected $aProductUnit;

    /**
     * @var        Supplier
     */
    protected $aSupplierRef;

    /**
     * @var        Category
     */
    protected $aCategory;

    /**
     * @var        User
     */
    protected $aUser;

    /**
     * @var        Vat
     */
    protected $aVat;

    /**
     * @var        ObjectCollection|ProductMultiCategory[] Collection to store aggregation of ProductMultiCategory objects.
     */
    protected $collProductMultiCategories;
    protected $collProductMultiCategoriesPartial;

    /**
     * @var        ObjectCollection|OrderItemProduct[] Collection to store aggregation of OrderItemProduct objects.
     */
    protected $collSaleOrderItemProducts;
    protected $collSaleOrderItemProductsPartial;

    /**
     * @var        ObjectCollection|ChildProductReview[] Collection to store aggregation of ChildProductReview objects.
     */
    protected $collProductReviews;
    protected $collProductReviewsPartial;

    /**
     * @var        ObjectCollection|ChildPromoProduct[] Collection to store aggregation of ChildPromoProduct objects.
     */
    protected $collPromoProducts;
    protected $collPromoProductsPartial;

    /**
     * @var        ObjectCollection|ChildProductProperty[] Collection to store aggregation of ChildProductProperty objects.
     */
    protected $collProductProperties;
    protected $collProductPropertiesPartial;

    /**
     * @var        ObjectCollection|ChildProductColor[] Collection to store aggregation of ChildProductColor objects.
     */
    protected $collProductColors;
    protected $collProductColorsPartial;

    /**
     * @var        ObjectCollection|ChildProductList[] Collection to store aggregation of ChildProductList objects.
     */
    protected $collProductLists;
    protected $collProductListsPartial;

    /**
     * @var        ObjectCollection|ChildProductUsage[] Collection to store aggregation of ChildProductUsage objects.
     */
    protected $collProductUsages;
    protected $collProductUsagesPartial;

    /**
     * @var        ObjectCollection|ChildProductTranslation[] Collection to store aggregation of ChildProductTranslation objects.
     */
    protected $collProductTranslations;
    protected $collProductTranslationsPartial;

    /**
     * @var        ObjectCollection|RelatedProduct[] Collection to store aggregation of RelatedProduct objects.
     */
    protected $collRelatedProductsRelatedByProductId;
    protected $collRelatedProductsRelatedByProductIdPartial;

    /**
     * @var        ObjectCollection|RelatedProduct[] Collection to store aggregation of RelatedProduct objects.
     */
    protected $collRelatedProductsRelatedByOtherProductId;
    protected $collRelatedProductsRelatedByOtherProductIdPartial;

    /**
     * @var        ObjectCollection|ProductImage[] Collection to store aggregation of ProductImage objects.
     */
    protected $collProductImages;
    protected $collProductImagesPartial;

    /**
     * @var        ObjectCollection|ProductTag[] Collection to store aggregation of ProductTag objects.
     */
    protected $collProductTags;
    protected $collProductTagsPartial;

    /**
     * @var        ObjectCollection|SaleOrderItemStockClaimed[] Collection to store aggregation of SaleOrderItemStockClaimed objects.
     */
    protected $collSaleOrderItemStockClaimeds;
    protected $collSaleOrderItemStockClaimedsPartial;

    /**
     * @var        ObjectCollection|CustomerWishlist[] Collection to store aggregation of CustomerWishlist objects.
     */
    protected $collCustomerWishlists;
    protected $collCustomerWishlistsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ProductMultiCategory[]
     */
    protected $productMultiCategoriesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|OrderItemProduct[]
     */
    protected $saleOrderItemProductsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProductReview[]
     */
    protected $productReviewsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildPromoProduct[]
     */
    protected $promoProductsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProductProperty[]
     */
    protected $productPropertiesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProductColor[]
     */
    protected $productColorsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProductList[]
     */
    protected $productListsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProductUsage[]
     */
    protected $productUsagesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProductTranslation[]
     */
    protected $productTranslationsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|RelatedProduct[]
     */
    protected $relatedProductsRelatedByProductIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|RelatedProduct[]
     */
    protected $relatedProductsRelatedByOtherProductIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ProductImage[]
     */
    protected $productImagesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ProductTag[]
     */
    protected $productTagsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|SaleOrderItemStockClaimed[]
     */
    protected $saleOrderItemStockClaimedsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|CustomerWishlist[]
     */
    protected $customerWishlistsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->in_sale = false;
        $this->in_spotlight = false;
        $this->in_webshop = true;
        $this->is_offer = false;
        $this->is_bestseller = false;
        $this->is_out_of_stock = false;
        $this->has_images = true;
        $this->virtual_stock = false;
    }

    /**
     * Initializes internal state of Model\Base\Product object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Product</code> instance.  If
     * <code>obj</code> is an instance of <code>Product</code>, delegates to
     * <code>equals(Product)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [derrived_from_id] column value.
     *
     * @return int|null
     */
    public function getDerrivedFromId()
    {
        return $this->derrived_from_id;
    }

    /**
     * Get the [old_id] column value.
     *
     * @return string|null
     */
    public function getOldId()
    {
        return $this->old_id;
    }

    /**
     * Get the [import_tag] column value.
     *
     * @return string|null
     */
    public function getImportTag()
    {
        return $this->import_tag;
    }

    /**
     * Get the [sku] column value.
     *
     * @return string|null
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Get the [ean] column value.
     *
     * @return string|null
     */
    public function getEan()
    {
        return $this->ean;
    }

    /**
     * Get the [optionally formatted] temporal [deleted_on] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getDeletedOn($format = null)
    {
        if ($format === null) {
            return $this->deleted_on;
        } else {
            return $this->deleted_on instanceof \DateTimeInterface ? $this->deleted_on->format($format) : null;
        }
    }

    /**
     * Get the [deleted_by_user_id] column value.
     *
     * @return int|null
     */
    public function getDeletedByUserId()
    {
        return $this->deleted_by_user_id;
    }

    /**
     * Get the [popularity] column value.
     *
     * @return int|null
     */
    public function getPopularity()
    {
        return $this->popularity;
    }

    /**
     * Get the [avg_rating] column value.
     *
     * @return int|null
     */
    public function getAvgRating()
    {
        return $this->avg_rating;
    }

    /**
     * Get the [supplier] column value.
     *
     * @return string|null
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Get the [supplier_id] column value.
     *
     * @return int|null
     */
    public function getSupplierId()
    {
        return $this->supplier_id;
    }

    /**
     * Get the [supplier_product_number] column value.
     *
     * @return string|null
     */
    public function getSupplierProductNumber()
    {
        return $this->supplier_product_number;
    }

    /**
     * Get the [in_sale] column value.
     *
     * @return boolean|null
     */
    public function getInSale()
    {
        return $this->in_sale;
    }

    /**
     * Get the [in_sale] column value.
     *
     * @return boolean|null
     */
    public function isInSale()
    {
        return $this->getInSale();
    }

    /**
     * Get the [in_spotlight] column value.
     *
     * @return boolean|null
     */
    public function getInSpotlight()
    {
        return $this->in_spotlight;
    }

    /**
     * Get the [in_spotlight] column value.
     *
     * @return boolean|null
     */
    public function isInSpotlight()
    {
        return $this->getInSpotlight();
    }

    /**
     * Get the [in_webshop] column value.
     *
     * @return boolean|null
     */
    public function getInWebshop()
    {
        return $this->in_webshop;
    }

    /**
     * Get the [in_webshop] column value.
     *
     * @return boolean|null
     */
    public function isInWebshop()
    {
        return $this->getInWebshop();
    }

    /**
     * Get the [is_offer] column value.
     *
     * @return boolean|null
     */
    public function getIsOffer()
    {
        return $this->is_offer;
    }

    /**
     * Get the [is_offer] column value.
     *
     * @return boolean|null
     */
    public function isOffer()
    {
        return $this->getIsOffer();
    }

    /**
     * Get the [is_bestseller] column value.
     *
     * @return boolean|null
     */
    public function getIsBestseller()
    {
        return $this->is_bestseller;
    }

    /**
     * Get the [is_bestseller] column value.
     *
     * @return boolean|null
     */
    public function isBestseller()
    {
        return $this->getIsBestseller();
    }

    /**
     * Get the [is_out_of_stock] column value.
     *
     * @return boolean|null
     */
    public function getIsOutOfStock()
    {
        return $this->is_out_of_stock;
    }

    /**
     * Get the [is_out_of_stock] column value.
     *
     * @return boolean|null
     */
    public function isOutOfStock()
    {
        return $this->getIsOutOfStock();
    }

    /**
     * Get the [has_images] column value.
     *
     * @return boolean|null
     */
    public function getHasImages()
    {
        return $this->has_images;
    }

    /**
     * Get the [has_images] column value.
     *
     * @return boolean|null
     */
    public function hasImages()
    {
        return $this->getHasImages();
    }

    /**
     * Get the [category_id] column value.
     *
     * @return int
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * Get the [number] column value.
     *
     * @return string|null
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Get the [advertiser_user_id] column value.
     *
     * @return int|null
     */
    public function getAdvertiserUserId()
    {
        return $this->advertiser_user_id;
    }

    /**
     * Get the [delivery_time_id] column value.
     *
     * @return int|null
     */
    public function getDeliveryTimeId()
    {
        return $this->delivery_time_id;
    }

    /**
     * Get the [thickness] column value.
     *
     * @return double|null
     */
    public function getThickness()
    {
        return $this->thickness;
    }

    /**
     * Get the [height] column value.
     *
     * @return string|null
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Get the [width] column value.
     *
     * @return string|null
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Get the [length] column value.
     *
     * @return string|null
     */
    public function getLength()
    {
        return $this->length;
    }

    /**
     * Get the [weight] column value.
     *
     * @return string|null
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Get the [composition] column value.
     *
     * @return string|null
     */
    public function getComposition()
    {
        return $this->composition;
    }

    /**
     * Get the [material_id] column value.
     *
     * @return string|null
     */
    public function getMaterialId()
    {
        return $this->material_id;
    }

    /**
     * Get the [brand_id] column value.
     *
     * @return int|null
     */
    public function getBrandId()
    {
        return $this->brand_id;
    }

    /**
     * Get the [title] column value.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [description] column value.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get the [note] column value.
     *
     * @return string|null
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Get the [optionally formatted] temporal [created_on] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedOn($format = null)
    {
        if ($format === null) {
            return $this->created_on;
        } else {
            return $this->created_on instanceof \DateTimeInterface ? $this->created_on->format($format) : null;
        }
    }

    /**
     * Get the [day_price] column value.
     *
     * @return double|null
     */
    public function getDayPrice()
    {
        return $this->day_price;
    }

    /**
     * Get the [sale_price] column value.
     *
     * @return double|null
     */
    public function getSalePrice()
    {
        return $this->sale_price;
    }

    /**
     * Get the [discount_percentage] column value.
     *
     * @return double|null
     */
    public function getDiscountPercentage()
    {
        return $this->discount_percentage;
    }

    /**
     * Get the [purchase_price] column value.
     *
     * @return string|null
     */
    public function getPurchasePrice()
    {
        return $this->purchase_price;
    }

    /**
     * Get the [reseller_price] column value.
     *
     * @return double|null
     */
    public function getResellerPrice()
    {
        return $this->reseller_price;
    }

    /**
     * Get the [advice_price] column value.
     *
     * @return double|null
     */
    public function getAdvicePrice()
    {
        return $this->advice_price;
    }

    /**
     * Get the [bulk_stock_id] column value.
     *
     * @return int|null
     */
    public function getBulkStockId()
    {
        return $this->bulk_stock_id;
    }

    /**
     * Get the [stock_quantity] column value.
     *
     * @return int|null
     */
    public function getStockQuantity()
    {
        return $this->stock_quantity;
    }

    /**
     * Get the [virtual_stock] column value.
     *
     * @return boolean|null
     */
    public function getVirtualStock()
    {
        return $this->virtual_stock;
    }

    /**
     * Get the [virtual_stock] column value.
     *
     * @return boolean|null
     */
    public function isVirtualStock()
    {
        return $this->getVirtualStock();
    }

    /**
     * Get the [quantity_per_pack] column value.
     *
     * @return int|null
     */
    public function getQuantityPerPack()
    {
        return $this->quantity_per_pack;
    }

    /**
     * Get the [youtube_url] column value.
     *
     * @return string|null
     */
    public function getYoutubeUrl()
    {
        return $this->youtube_url;
    }

    /**
     * Get the [meta_title] column value.
     *
     * @return string|null
     */
    public function getMetaTitle()
    {
        return $this->meta_title;
    }

    /**
     * Get the [meta_description] column value.
     *
     * @return string|null
     */
    public function getMetaDescription()
    {
        return $this->meta_description;
    }

    /**
     * Get the [meta_keyword] column value.
     *
     * @return string|null
     */
    public function getMetaKeyword()
    {
        return $this->meta_keyword;
    }

    /**
     * Get the [hashtag] column value.
     *
     * @return string|null
     */
    public function getHashtag()
    {
        return $this->hashtag;
    }

    /**
     * Get the [calculated_average_rating] column value.
     *
     * @return int|null
     */
    public function getCalculatedAverageRating()
    {
        return $this->calculated_average_rating;
    }

    /**
     * Get the [vat_id] column value.
     *
     * @return int|null
     */
    public function getVatId()
    {
        return $this->vat_id;
    }

    /**
     * Get the [unit_id] column value.
     *
     * @return int|null
     */
    public function getUnitId()
    {
        return $this->unit_id;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[ProductTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [derrived_from_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setDerrivedFromId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->derrived_from_id !== $v) {
            $this->derrived_from_id = $v;
            $this->modifiedColumns[ProductTableMap::COL_DERRIVED_FROM_ID] = true;
        }

        return $this;
    } // setDerrivedFromId()

    /**
     * Set the value of [old_id] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setOldId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->old_id !== $v) {
            $this->old_id = $v;
            $this->modifiedColumns[ProductTableMap::COL_OLD_ID] = true;
        }

        return $this;
    } // setOldId()

    /**
     * Set the value of [import_tag] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setImportTag($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->import_tag !== $v) {
            $this->import_tag = $v;
            $this->modifiedColumns[ProductTableMap::COL_IMPORT_TAG] = true;
        }

        return $this;
    } // setImportTag()

    /**
     * Set the value of [sku] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setSku($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->sku !== $v) {
            $this->sku = $v;
            $this->modifiedColumns[ProductTableMap::COL_SKU] = true;
        }

        return $this;
    } // setSku()

    /**
     * Set the value of [ean] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setEan($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ean !== $v) {
            $this->ean = $v;
            $this->modifiedColumns[ProductTableMap::COL_EAN] = true;
        }

        return $this;
    } // setEan()

    /**
     * Sets the value of [deleted_on] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setDeletedOn($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->deleted_on !== null || $dt !== null) {
            if ($this->deleted_on === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->deleted_on->format("Y-m-d H:i:s.u")) {
                $this->deleted_on = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProductTableMap::COL_DELETED_ON] = true;
            }
        } // if either are not null

        return $this;
    } // setDeletedOn()

    /**
     * Set the value of [deleted_by_user_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setDeletedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->deleted_by_user_id !== $v) {
            $this->deleted_by_user_id = $v;
            $this->modifiedColumns[ProductTableMap::COL_DELETED_BY_USER_ID] = true;
        }

        return $this;
    } // setDeletedByUserId()

    /**
     * Set the value of [popularity] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setPopularity($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->popularity !== $v) {
            $this->popularity = $v;
            $this->modifiedColumns[ProductTableMap::COL_POPULARITY] = true;
        }

        return $this;
    } // setPopularity()

    /**
     * Set the value of [avg_rating] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setAvgRating($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->avg_rating !== $v) {
            $this->avg_rating = $v;
            $this->modifiedColumns[ProductTableMap::COL_AVG_RATING] = true;
        }

        return $this;
    } // setAvgRating()

    /**
     * Set the value of [supplier] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setSupplier($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->supplier !== $v) {
            $this->supplier = $v;
            $this->modifiedColumns[ProductTableMap::COL_SUPPLIER] = true;
        }

        return $this;
    } // setSupplier()

    /**
     * Set the value of [supplier_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setSupplierId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->supplier_id !== $v) {
            $this->supplier_id = $v;
            $this->modifiedColumns[ProductTableMap::COL_SUPPLIER_ID] = true;
        }

        if ($this->aSupplierRef !== null && $this->aSupplierRef->getId() !== $v) {
            $this->aSupplierRef = null;
        }

        return $this;
    } // setSupplierId()

    /**
     * Set the value of [supplier_product_number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setSupplierProductNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->supplier_product_number !== $v) {
            $this->supplier_product_number = $v;
            $this->modifiedColumns[ProductTableMap::COL_SUPPLIER_PRODUCT_NUMBER] = true;
        }

        return $this;
    } // setSupplierProductNumber()

    /**
     * Sets the value of the [in_sale] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setInSale($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->in_sale !== $v) {
            $this->in_sale = $v;
            $this->modifiedColumns[ProductTableMap::COL_IN_SALE] = true;
        }

        return $this;
    } // setInSale()

    /**
     * Sets the value of the [in_spotlight] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setInSpotlight($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->in_spotlight !== $v) {
            $this->in_spotlight = $v;
            $this->modifiedColumns[ProductTableMap::COL_IN_SPOTLIGHT] = true;
        }

        return $this;
    } // setInSpotlight()

    /**
     * Sets the value of the [in_webshop] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setInWebshop($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->in_webshop !== $v) {
            $this->in_webshop = $v;
            $this->modifiedColumns[ProductTableMap::COL_IN_WEBSHOP] = true;
        }

        return $this;
    } // setInWebshop()

    /**
     * Sets the value of the [is_offer] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setIsOffer($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_offer !== $v) {
            $this->is_offer = $v;
            $this->modifiedColumns[ProductTableMap::COL_IS_OFFER] = true;
        }

        return $this;
    } // setIsOffer()

    /**
     * Sets the value of the [is_bestseller] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setIsBestseller($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_bestseller !== $v) {
            $this->is_bestseller = $v;
            $this->modifiedColumns[ProductTableMap::COL_IS_BESTSELLER] = true;
        }

        return $this;
    } // setIsBestseller()

    /**
     * Sets the value of the [is_out_of_stock] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setIsOutOfStock($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_out_of_stock !== $v) {
            $this->is_out_of_stock = $v;
            $this->modifiedColumns[ProductTableMap::COL_IS_OUT_OF_STOCK] = true;
        }

        return $this;
    } // setIsOutOfStock()

    /**
     * Sets the value of the [has_images] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setHasImages($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->has_images !== $v) {
            $this->has_images = $v;
            $this->modifiedColumns[ProductTableMap::COL_HAS_IMAGES] = true;
        }

        return $this;
    } // setHasImages()

    /**
     * Set the value of [category_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setCategoryId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->category_id !== $v) {
            $this->category_id = $v;
            $this->modifiedColumns[ProductTableMap::COL_CATEGORY_ID] = true;
        }

        if ($this->aCategory !== null && $this->aCategory->getId() !== $v) {
            $this->aCategory = null;
        }

        return $this;
    } // setCategoryId()

    /**
     * Set the value of [number] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setNumber($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->number !== $v) {
            $this->number = $v;
            $this->modifiedColumns[ProductTableMap::COL_NUMBER] = true;
        }

        return $this;
    } // setNumber()

    /**
     * Set the value of [advertiser_user_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setAdvertiserUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->advertiser_user_id !== $v) {
            $this->advertiser_user_id = $v;
            $this->modifiedColumns[ProductTableMap::COL_ADVERTISER_USER_ID] = true;
        }

        if ($this->aUser !== null && $this->aUser->getId() !== $v) {
            $this->aUser = null;
        }

        return $this;
    } // setAdvertiserUserId()

    /**
     * Set the value of [delivery_time_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setDeliveryTimeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->delivery_time_id !== $v) {
            $this->delivery_time_id = $v;
            $this->modifiedColumns[ProductTableMap::COL_DELIVERY_TIME_ID] = true;
        }

        return $this;
    } // setDeliveryTimeId()

    /**
     * Set the value of [thickness] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setThickness($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->thickness !== $v) {
            $this->thickness = $v;
            $this->modifiedColumns[ProductTableMap::COL_THICKNESS] = true;
        }

        return $this;
    } // setThickness()

    /**
     * Set the value of [height] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setHeight($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->height !== $v) {
            $this->height = $v;
            $this->modifiedColumns[ProductTableMap::COL_HEIGHT] = true;
        }

        return $this;
    } // setHeight()

    /**
     * Set the value of [width] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setWidth($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->width !== $v) {
            $this->width = $v;
            $this->modifiedColumns[ProductTableMap::COL_WIDTH] = true;
        }

        return $this;
    } // setWidth()

    /**
     * Set the value of [length] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setLength($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->length !== $v) {
            $this->length = $v;
            $this->modifiedColumns[ProductTableMap::COL_LENGTH] = true;
        }

        return $this;
    } // setLength()

    /**
     * Set the value of [weight] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setWeight($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->weight !== $v) {
            $this->weight = $v;
            $this->modifiedColumns[ProductTableMap::COL_WEIGHT] = true;
        }

        return $this;
    } // setWeight()

    /**
     * Set the value of [composition] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setComposition($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->composition !== $v) {
            $this->composition = $v;
            $this->modifiedColumns[ProductTableMap::COL_COMPOSITION] = true;
        }

        return $this;
    } // setComposition()

    /**
     * Set the value of [material_id] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setMaterialId($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->material_id !== $v) {
            $this->material_id = $v;
            $this->modifiedColumns[ProductTableMap::COL_MATERIAL_ID] = true;
        }

        return $this;
    } // setMaterialId()

    /**
     * Set the value of [brand_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setBrandId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->brand_id !== $v) {
            $this->brand_id = $v;
            $this->modifiedColumns[ProductTableMap::COL_BRAND_ID] = true;
        }

        if ($this->aBrand !== null && $this->aBrand->getId() !== $v) {
            $this->aBrand = null;
        }

        return $this;
    } // setBrandId()

    /**
     * Set the value of [title] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[ProductTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [description] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->description !== $v) {
            $this->description = $v;
            $this->modifiedColumns[ProductTableMap::COL_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [note] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setNote($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->note !== $v) {
            $this->note = $v;
            $this->modifiedColumns[ProductTableMap::COL_NOTE] = true;
        }

        return $this;
    } // setNote()

    /**
     * Sets the value of [created_on] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setCreatedOn($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_on !== null || $dt !== null) {
            if ($this->created_on === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_on->format("Y-m-d H:i:s.u")) {
                $this->created_on = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProductTableMap::COL_CREATED_ON] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedOn()

    /**
     * Set the value of [day_price] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setDayPrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->day_price !== $v) {
            $this->day_price = $v;
            $this->modifiedColumns[ProductTableMap::COL_DAY_PRICE] = true;
        }

        return $this;
    } // setDayPrice()

    /**
     * Set the value of [sale_price] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setSalePrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->sale_price !== $v) {
            $this->sale_price = $v;
            $this->modifiedColumns[ProductTableMap::COL_SALE_PRICE] = true;
        }

        return $this;
    } // setSalePrice()

    /**
     * Set the value of [discount_percentage] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setDiscountPercentage($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->discount_percentage !== $v) {
            $this->discount_percentage = $v;
            $this->modifiedColumns[ProductTableMap::COL_DISCOUNT_PERCENTAGE] = true;
        }

        return $this;
    } // setDiscountPercentage()

    /**
     * Set the value of [purchase_price] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setPurchasePrice($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->purchase_price !== $v) {
            $this->purchase_price = $v;
            $this->modifiedColumns[ProductTableMap::COL_PURCHASE_PRICE] = true;
        }

        return $this;
    } // setPurchasePrice()

    /**
     * Set the value of [reseller_price] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setResellerPrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->reseller_price !== $v) {
            $this->reseller_price = $v;
            $this->modifiedColumns[ProductTableMap::COL_RESELLER_PRICE] = true;
        }

        return $this;
    } // setResellerPrice()

    /**
     * Set the value of [advice_price] column.
     *
     * @param double|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setAdvicePrice($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->advice_price !== $v) {
            $this->advice_price = $v;
            $this->modifiedColumns[ProductTableMap::COL_ADVICE_PRICE] = true;
        }

        return $this;
    } // setAdvicePrice()

    /**
     * Set the value of [bulk_stock_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setBulkStockId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->bulk_stock_id !== $v) {
            $this->bulk_stock_id = $v;
            $this->modifiedColumns[ProductTableMap::COL_BULK_STOCK_ID] = true;
        }

        return $this;
    } // setBulkStockId()

    /**
     * Set the value of [stock_quantity] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setStockQuantity($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->stock_quantity !== $v) {
            $this->stock_quantity = $v;
            $this->modifiedColumns[ProductTableMap::COL_STOCK_QUANTITY] = true;
        }

        return $this;
    } // setStockQuantity()

    /**
     * Sets the value of the [virtual_stock] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setVirtualStock($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->virtual_stock !== $v) {
            $this->virtual_stock = $v;
            $this->modifiedColumns[ProductTableMap::COL_VIRTUAL_STOCK] = true;
        }

        return $this;
    } // setVirtualStock()

    /**
     * Set the value of [quantity_per_pack] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setQuantityPerPack($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->quantity_per_pack !== $v) {
            $this->quantity_per_pack = $v;
            $this->modifiedColumns[ProductTableMap::COL_QUANTITY_PER_PACK] = true;
        }

        return $this;
    } // setQuantityPerPack()

    /**
     * Set the value of [youtube_url] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setYoutubeUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->youtube_url !== $v) {
            $this->youtube_url = $v;
            $this->modifiedColumns[ProductTableMap::COL_YOUTUBE_URL] = true;
        }

        return $this;
    } // setYoutubeUrl()

    /**
     * Set the value of [meta_title] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setMetaTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->meta_title !== $v) {
            $this->meta_title = $v;
            $this->modifiedColumns[ProductTableMap::COL_META_TITLE] = true;
        }

        return $this;
    } // setMetaTitle()

    /**
     * Set the value of [meta_description] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setMetaDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->meta_description !== $v) {
            $this->meta_description = $v;
            $this->modifiedColumns[ProductTableMap::COL_META_DESCRIPTION] = true;
        }

        return $this;
    } // setMetaDescription()

    /**
     * Set the value of [meta_keyword] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setMetaKeyword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->meta_keyword !== $v) {
            $this->meta_keyword = $v;
            $this->modifiedColumns[ProductTableMap::COL_META_KEYWORD] = true;
        }

        return $this;
    } // setMetaKeyword()

    /**
     * Set the value of [hashtag] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setHashtag($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->hashtag !== $v) {
            $this->hashtag = $v;
            $this->modifiedColumns[ProductTableMap::COL_HASHTAG] = true;
        }

        return $this;
    } // setHashtag()

    /**
     * Set the value of [calculated_average_rating] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setCalculatedAverageRating($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->calculated_average_rating !== $v) {
            $this->calculated_average_rating = $v;
            $this->modifiedColumns[ProductTableMap::COL_CALCULATED_AVERAGE_RATING] = true;
        }

        return $this;
    } // setCalculatedAverageRating()

    /**
     * Set the value of [vat_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setVatId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->vat_id !== $v) {
            $this->vat_id = $v;
            $this->modifiedColumns[ProductTableMap::COL_VAT_ID] = true;
        }

        if ($this->aVat !== null && $this->aVat->getId() !== $v) {
            $this->aVat = null;
        }

        return $this;
    } // setVatId()

    /**
     * Set the value of [unit_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setUnitId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->unit_id !== $v) {
            $this->unit_id = $v;
            $this->modifiedColumns[ProductTableMap::COL_UNIT_ID] = true;
        }

        if ($this->aProductUnit !== null && $this->aProductUnit->getId() !== $v) {
            $this->aProductUnit = null;
        }

        return $this;
    } // setUnitId()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProductTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProductTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->in_sale !== false) {
                return false;
            }

            if ($this->in_spotlight !== false) {
                return false;
            }

            if ($this->in_webshop !== true) {
                return false;
            }

            if ($this->is_offer !== false) {
                return false;
            }

            if ($this->is_bestseller !== false) {
                return false;
            }

            if ($this->is_out_of_stock !== false) {
                return false;
            }

            if ($this->has_images !== true) {
                return false;
            }

            if ($this->virtual_stock !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ProductTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ProductTableMap::translateFieldName('DerrivedFromId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->derrived_from_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ProductTableMap::translateFieldName('OldId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->old_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ProductTableMap::translateFieldName('ImportTag', TableMap::TYPE_PHPNAME, $indexType)];
            $this->import_tag = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ProductTableMap::translateFieldName('Sku', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sku = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ProductTableMap::translateFieldName('Ean', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ean = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ProductTableMap::translateFieldName('DeletedOn', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->deleted_on = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ProductTableMap::translateFieldName('DeletedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->deleted_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ProductTableMap::translateFieldName('Popularity', TableMap::TYPE_PHPNAME, $indexType)];
            $this->popularity = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ProductTableMap::translateFieldName('AvgRating', TableMap::TYPE_PHPNAME, $indexType)];
            $this->avg_rating = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ProductTableMap::translateFieldName('Supplier', TableMap::TYPE_PHPNAME, $indexType)];
            $this->supplier = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ProductTableMap::translateFieldName('SupplierId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->supplier_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ProductTableMap::translateFieldName('SupplierProductNumber', TableMap::TYPE_PHPNAME, $indexType)];
            $this->supplier_product_number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ProductTableMap::translateFieldName('InSale', TableMap::TYPE_PHPNAME, $indexType)];
            $this->in_sale = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ProductTableMap::translateFieldName('InSpotlight', TableMap::TYPE_PHPNAME, $indexType)];
            $this->in_spotlight = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ProductTableMap::translateFieldName('InWebshop', TableMap::TYPE_PHPNAME, $indexType)];
            $this->in_webshop = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ProductTableMap::translateFieldName('IsOffer', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_offer = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ProductTableMap::translateFieldName('IsBestseller', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_bestseller = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : ProductTableMap::translateFieldName('IsOutOfStock', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_out_of_stock = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : ProductTableMap::translateFieldName('HasImages', TableMap::TYPE_PHPNAME, $indexType)];
            $this->has_images = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : ProductTableMap::translateFieldName('CategoryId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->category_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : ProductTableMap::translateFieldName('Number', TableMap::TYPE_PHPNAME, $indexType)];
            $this->number = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 22 + $startcol : ProductTableMap::translateFieldName('AdvertiserUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->advertiser_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 23 + $startcol : ProductTableMap::translateFieldName('DeliveryTimeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->delivery_time_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 24 + $startcol : ProductTableMap::translateFieldName('Thickness', TableMap::TYPE_PHPNAME, $indexType)];
            $this->thickness = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 25 + $startcol : ProductTableMap::translateFieldName('Height', TableMap::TYPE_PHPNAME, $indexType)];
            $this->height = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 26 + $startcol : ProductTableMap::translateFieldName('Width', TableMap::TYPE_PHPNAME, $indexType)];
            $this->width = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 27 + $startcol : ProductTableMap::translateFieldName('Length', TableMap::TYPE_PHPNAME, $indexType)];
            $this->length = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 28 + $startcol : ProductTableMap::translateFieldName('Weight', TableMap::TYPE_PHPNAME, $indexType)];
            $this->weight = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 29 + $startcol : ProductTableMap::translateFieldName('Composition', TableMap::TYPE_PHPNAME, $indexType)];
            $this->composition = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 30 + $startcol : ProductTableMap::translateFieldName('MaterialId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->material_id = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 31 + $startcol : ProductTableMap::translateFieldName('BrandId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->brand_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 32 + $startcol : ProductTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 33 + $startcol : ProductTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 34 + $startcol : ProductTableMap::translateFieldName('Note', TableMap::TYPE_PHPNAME, $indexType)];
            $this->note = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 35 + $startcol : ProductTableMap::translateFieldName('CreatedOn', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_on = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 36 + $startcol : ProductTableMap::translateFieldName('DayPrice', TableMap::TYPE_PHPNAME, $indexType)];
            $this->day_price = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 37 + $startcol : ProductTableMap::translateFieldName('SalePrice', TableMap::TYPE_PHPNAME, $indexType)];
            $this->sale_price = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 38 + $startcol : ProductTableMap::translateFieldName('DiscountPercentage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->discount_percentage = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 39 + $startcol : ProductTableMap::translateFieldName('PurchasePrice', TableMap::TYPE_PHPNAME, $indexType)];
            $this->purchase_price = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 40 + $startcol : ProductTableMap::translateFieldName('ResellerPrice', TableMap::TYPE_PHPNAME, $indexType)];
            $this->reseller_price = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 41 + $startcol : ProductTableMap::translateFieldName('AdvicePrice', TableMap::TYPE_PHPNAME, $indexType)];
            $this->advice_price = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 42 + $startcol : ProductTableMap::translateFieldName('BulkStockId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->bulk_stock_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 43 + $startcol : ProductTableMap::translateFieldName('StockQuantity', TableMap::TYPE_PHPNAME, $indexType)];
            $this->stock_quantity = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 44 + $startcol : ProductTableMap::translateFieldName('VirtualStock', TableMap::TYPE_PHPNAME, $indexType)];
            $this->virtual_stock = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 45 + $startcol : ProductTableMap::translateFieldName('QuantityPerPack', TableMap::TYPE_PHPNAME, $indexType)];
            $this->quantity_per_pack = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 46 + $startcol : ProductTableMap::translateFieldName('YoutubeUrl', TableMap::TYPE_PHPNAME, $indexType)];
            $this->youtube_url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 47 + $startcol : ProductTableMap::translateFieldName('MetaTitle', TableMap::TYPE_PHPNAME, $indexType)];
            $this->meta_title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 48 + $startcol : ProductTableMap::translateFieldName('MetaDescription', TableMap::TYPE_PHPNAME, $indexType)];
            $this->meta_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 49 + $startcol : ProductTableMap::translateFieldName('MetaKeyword', TableMap::TYPE_PHPNAME, $indexType)];
            $this->meta_keyword = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 50 + $startcol : ProductTableMap::translateFieldName('Hashtag', TableMap::TYPE_PHPNAME, $indexType)];
            $this->hashtag = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 51 + $startcol : ProductTableMap::translateFieldName('CalculatedAverageRating', TableMap::TYPE_PHPNAME, $indexType)];
            $this->calculated_average_rating = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 52 + $startcol : ProductTableMap::translateFieldName('VatId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->vat_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 53 + $startcol : ProductTableMap::translateFieldName('UnitId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->unit_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 54 + $startcol : ProductTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 55 + $startcol : ProductTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 56; // 56 = ProductTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Product'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aSupplierRef !== null && $this->supplier_id !== $this->aSupplierRef->getId()) {
            $this->aSupplierRef = null;
        }
        if ($this->aCategory !== null && $this->category_id !== $this->aCategory->getId()) {
            $this->aCategory = null;
        }
        if ($this->aUser !== null && $this->advertiser_user_id !== $this->aUser->getId()) {
            $this->aUser = null;
        }
        if ($this->aBrand !== null && $this->brand_id !== $this->aBrand->getId()) {
            $this->aBrand = null;
        }
        if ($this->aVat !== null && $this->vat_id !== $this->aVat->getId()) {
            $this->aVat = null;
        }
        if ($this->aProductUnit !== null && $this->unit_id !== $this->aProductUnit->getId()) {
            $this->aProductUnit = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProductTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildProductQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aBrand = null;
            $this->aProductUnit = null;
            $this->aSupplierRef = null;
            $this->aCategory = null;
            $this->aUser = null;
            $this->aVat = null;
            $this->collProductMultiCategories = null;

            $this->collSaleOrderItemProducts = null;

            $this->collProductReviews = null;

            $this->collPromoProducts = null;

            $this->collProductProperties = null;

            $this->collProductColors = null;

            $this->collProductLists = null;

            $this->collProductUsages = null;

            $this->collProductTranslations = null;

            $this->collRelatedProductsRelatedByProductId = null;

            $this->collRelatedProductsRelatedByOtherProductId = null;

            $this->collProductImages = null;

            $this->collProductTags = null;

            $this->collSaleOrderItemStockClaimeds = null;

            $this->collCustomerWishlists = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Product::setDeleted()
     * @see Product::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildProductQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(ProductTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(ProductTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(ProductTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ProductTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aBrand !== null) {
                if ($this->aBrand->isModified() || $this->aBrand->isNew()) {
                    $affectedRows += $this->aBrand->save($con);
                }
                $this->setBrand($this->aBrand);
            }

            if ($this->aProductUnit !== null) {
                if ($this->aProductUnit->isModified() || $this->aProductUnit->isNew()) {
                    $affectedRows += $this->aProductUnit->save($con);
                }
                $this->setProductUnit($this->aProductUnit);
            }

            if ($this->aSupplierRef !== null) {
                if ($this->aSupplierRef->isModified() || $this->aSupplierRef->isNew()) {
                    $affectedRows += $this->aSupplierRef->save($con);
                }
                $this->setSupplierRef($this->aSupplierRef);
            }

            if ($this->aCategory !== null) {
                if ($this->aCategory->isModified() || $this->aCategory->isNew()) {
                    $affectedRows += $this->aCategory->save($con);
                }
                $this->setCategory($this->aCategory);
            }

            if ($this->aUser !== null) {
                if ($this->aUser->isModified() || $this->aUser->isNew()) {
                    $affectedRows += $this->aUser->save($con);
                }
                $this->setUser($this->aUser);
            }

            if ($this->aVat !== null) {
                if ($this->aVat->isModified() || $this->aVat->isNew()) {
                    $affectedRows += $this->aVat->save($con);
                }
                $this->setVat($this->aVat);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->productMultiCategoriesScheduledForDeletion !== null) {
                if (!$this->productMultiCategoriesScheduledForDeletion->isEmpty()) {
                    \Model\Category\ProductMultiCategoryQuery::create()
                        ->filterByPrimaryKeys($this->productMultiCategoriesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productMultiCategoriesScheduledForDeletion = null;
                }
            }

            if ($this->collProductMultiCategories !== null) {
                foreach ($this->collProductMultiCategories as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saleOrderItemProductsScheduledForDeletion !== null) {
                if (!$this->saleOrderItemProductsScheduledForDeletion->isEmpty()) {
                    \Model\Sale\OrderItemProductQuery::create()
                        ->filterByPrimaryKeys($this->saleOrderItemProductsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->saleOrderItemProductsScheduledForDeletion = null;
                }
            }

            if ($this->collSaleOrderItemProducts !== null) {
                foreach ($this->collSaleOrderItemProducts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productReviewsScheduledForDeletion !== null) {
                if (!$this->productReviewsScheduledForDeletion->isEmpty()) {
                    \Model\ProductReviewQuery::create()
                        ->filterByPrimaryKeys($this->productReviewsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productReviewsScheduledForDeletion = null;
                }
            }

            if ($this->collProductReviews !== null) {
                foreach ($this->collProductReviews as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->promoProductsScheduledForDeletion !== null) {
                if (!$this->promoProductsScheduledForDeletion->isEmpty()) {
                    \Model\PromoProductQuery::create()
                        ->filterByPrimaryKeys($this->promoProductsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->promoProductsScheduledForDeletion = null;
                }
            }

            if ($this->collPromoProducts !== null) {
                foreach ($this->collPromoProducts as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productPropertiesScheduledForDeletion !== null) {
                if (!$this->productPropertiesScheduledForDeletion->isEmpty()) {
                    \Model\ProductPropertyQuery::create()
                        ->filterByPrimaryKeys($this->productPropertiesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productPropertiesScheduledForDeletion = null;
                }
            }

            if ($this->collProductProperties !== null) {
                foreach ($this->collProductProperties as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productColorsScheduledForDeletion !== null) {
                if (!$this->productColorsScheduledForDeletion->isEmpty()) {
                    \Model\ProductColorQuery::create()
                        ->filterByPrimaryKeys($this->productColorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productColorsScheduledForDeletion = null;
                }
            }

            if ($this->collProductColors !== null) {
                foreach ($this->collProductColors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productListsScheduledForDeletion !== null) {
                if (!$this->productListsScheduledForDeletion->isEmpty()) {
                    \Model\ProductListQuery::create()
                        ->filterByPrimaryKeys($this->productListsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productListsScheduledForDeletion = null;
                }
            }

            if ($this->collProductLists !== null) {
                foreach ($this->collProductLists as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productUsagesScheduledForDeletion !== null) {
                if (!$this->productUsagesScheduledForDeletion->isEmpty()) {
                    \Model\ProductUsageQuery::create()
                        ->filterByPrimaryKeys($this->productUsagesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productUsagesScheduledForDeletion = null;
                }
            }

            if ($this->collProductUsages !== null) {
                foreach ($this->collProductUsages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productTranslationsScheduledForDeletion !== null) {
                if (!$this->productTranslationsScheduledForDeletion->isEmpty()) {
                    \Model\ProductTranslationQuery::create()
                        ->filterByPrimaryKeys($this->productTranslationsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productTranslationsScheduledForDeletion = null;
                }
            }

            if ($this->collProductTranslations !== null) {
                foreach ($this->collProductTranslations as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->relatedProductsRelatedByProductIdScheduledForDeletion !== null) {
                if (!$this->relatedProductsRelatedByProductIdScheduledForDeletion->isEmpty()) {
                    \Model\Product\RelatedProductQuery::create()
                        ->filterByPrimaryKeys($this->relatedProductsRelatedByProductIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->relatedProductsRelatedByProductIdScheduledForDeletion = null;
                }
            }

            if ($this->collRelatedProductsRelatedByProductId !== null) {
                foreach ($this->collRelatedProductsRelatedByProductId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->relatedProductsRelatedByOtherProductIdScheduledForDeletion !== null) {
                if (!$this->relatedProductsRelatedByOtherProductIdScheduledForDeletion->isEmpty()) {
                    \Model\Product\RelatedProductQuery::create()
                        ->filterByPrimaryKeys($this->relatedProductsRelatedByOtherProductIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->relatedProductsRelatedByOtherProductIdScheduledForDeletion = null;
                }
            }

            if ($this->collRelatedProductsRelatedByOtherProductId !== null) {
                foreach ($this->collRelatedProductsRelatedByOtherProductId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productImagesScheduledForDeletion !== null) {
                if (!$this->productImagesScheduledForDeletion->isEmpty()) {
                    foreach ($this->productImagesScheduledForDeletion as $productImage) {
                        // need to save related object because we set the relation to null
                        $productImage->save($con);
                    }
                    $this->productImagesScheduledForDeletion = null;
                }
            }

            if ($this->collProductImages !== null) {
                foreach ($this->collProductImages as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->productTagsScheduledForDeletion !== null) {
                if (!$this->productTagsScheduledForDeletion->isEmpty()) {
                    \Model\Product\ProductTagQuery::create()
                        ->filterByPrimaryKeys($this->productTagsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->productTagsScheduledForDeletion = null;
                }
            }

            if ($this->collProductTags !== null) {
                foreach ($this->collProductTags as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->saleOrderItemStockClaimedsScheduledForDeletion !== null) {
                if (!$this->saleOrderItemStockClaimedsScheduledForDeletion->isEmpty()) {
                    \Model\Sale\SaleOrderItemStockClaimedQuery::create()
                        ->filterByPrimaryKeys($this->saleOrderItemStockClaimedsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->saleOrderItemStockClaimedsScheduledForDeletion = null;
                }
            }

            if ($this->collSaleOrderItemStockClaimeds !== null) {
                foreach ($this->collSaleOrderItemStockClaimeds as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->customerWishlistsScheduledForDeletion !== null) {
                if (!$this->customerWishlistsScheduledForDeletion->isEmpty()) {
                    \Model\Category\CustomerWishlistQuery::create()
                        ->filterByPrimaryKeys($this->customerWishlistsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->customerWishlistsScheduledForDeletion = null;
                }
            }

            if ($this->collCustomerWishlists !== null) {
                foreach ($this->collCustomerWishlists as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ProductTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ProductTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ProductTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_DERRIVED_FROM_ID)) {
            $modifiedColumns[':p' . $index++]  = 'derrived_from_id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_OLD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'old_id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_IMPORT_TAG)) {
            $modifiedColumns[':p' . $index++]  = 'import_tag';
        }
        if ($this->isColumnModified(ProductTableMap::COL_SKU)) {
            $modifiedColumns[':p' . $index++]  = 'sku';
        }
        if ($this->isColumnModified(ProductTableMap::COL_EAN)) {
            $modifiedColumns[':p' . $index++]  = 'ean';
        }
        if ($this->isColumnModified(ProductTableMap::COL_DELETED_ON)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_on';
        }
        if ($this->isColumnModified(ProductTableMap::COL_DELETED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'deleted_by_user_id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_POPULARITY)) {
            $modifiedColumns[':p' . $index++]  = 'popularity';
        }
        if ($this->isColumnModified(ProductTableMap::COL_AVG_RATING)) {
            $modifiedColumns[':p' . $index++]  = 'avg_rating';
        }
        if ($this->isColumnModified(ProductTableMap::COL_SUPPLIER)) {
            $modifiedColumns[':p' . $index++]  = 'supplier';
        }
        if ($this->isColumnModified(ProductTableMap::COL_SUPPLIER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'supplier_id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_SUPPLIER_PRODUCT_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'supplier_product_number';
        }
        if ($this->isColumnModified(ProductTableMap::COL_IN_SALE)) {
            $modifiedColumns[':p' . $index++]  = 'in_sale';
        }
        if ($this->isColumnModified(ProductTableMap::COL_IN_SPOTLIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'in_spotlight';
        }
        if ($this->isColumnModified(ProductTableMap::COL_IN_WEBSHOP)) {
            $modifiedColumns[':p' . $index++]  = 'in_webshop';
        }
        if ($this->isColumnModified(ProductTableMap::COL_IS_OFFER)) {
            $modifiedColumns[':p' . $index++]  = 'is_offer';
        }
        if ($this->isColumnModified(ProductTableMap::COL_IS_BESTSELLER)) {
            $modifiedColumns[':p' . $index++]  = 'is_bestseller';
        }
        if ($this->isColumnModified(ProductTableMap::COL_IS_OUT_OF_STOCK)) {
            $modifiedColumns[':p' . $index++]  = 'is_out_of_stock';
        }
        if ($this->isColumnModified(ProductTableMap::COL_HAS_IMAGES)) {
            $modifiedColumns[':p' . $index++]  = 'has_images';
        }
        if ($this->isColumnModified(ProductTableMap::COL_CATEGORY_ID)) {
            $modifiedColumns[':p' . $index++]  = 'category_id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_NUMBER)) {
            $modifiedColumns[':p' . $index++]  = 'number';
        }
        if ($this->isColumnModified(ProductTableMap::COL_ADVERTISER_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'advertiser_user_id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_DELIVERY_TIME_ID)) {
            $modifiedColumns[':p' . $index++]  = 'delivery_time_id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_THICKNESS)) {
            $modifiedColumns[':p' . $index++]  = 'thickness';
        }
        if ($this->isColumnModified(ProductTableMap::COL_HEIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'height';
        }
        if ($this->isColumnModified(ProductTableMap::COL_WIDTH)) {
            $modifiedColumns[':p' . $index++]  = 'width';
        }
        if ($this->isColumnModified(ProductTableMap::COL_LENGTH)) {
            $modifiedColumns[':p' . $index++]  = 'length';
        }
        if ($this->isColumnModified(ProductTableMap::COL_WEIGHT)) {
            $modifiedColumns[':p' . $index++]  = 'weight';
        }
        if ($this->isColumnModified(ProductTableMap::COL_COMPOSITION)) {
            $modifiedColumns[':p' . $index++]  = 'composition';
        }
        if ($this->isColumnModified(ProductTableMap::COL_MATERIAL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'material_id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_BRAND_ID)) {
            $modifiedColumns[':p' . $index++]  = 'brand_id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(ProductTableMap::COL_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'description';
        }
        if ($this->isColumnModified(ProductTableMap::COL_NOTE)) {
            $modifiedColumns[':p' . $index++]  = 'note';
        }
        if ($this->isColumnModified(ProductTableMap::COL_CREATED_ON)) {
            $modifiedColumns[':p' . $index++]  = 'created_on';
        }
        if ($this->isColumnModified(ProductTableMap::COL_DAY_PRICE)) {
            $modifiedColumns[':p' . $index++]  = 'day_price';
        }
        if ($this->isColumnModified(ProductTableMap::COL_SALE_PRICE)) {
            $modifiedColumns[':p' . $index++]  = 'sale_price';
        }
        if ($this->isColumnModified(ProductTableMap::COL_DISCOUNT_PERCENTAGE)) {
            $modifiedColumns[':p' . $index++]  = 'discount_percentage';
        }
        if ($this->isColumnModified(ProductTableMap::COL_PURCHASE_PRICE)) {
            $modifiedColumns[':p' . $index++]  = 'purchase_price';
        }
        if ($this->isColumnModified(ProductTableMap::COL_RESELLER_PRICE)) {
            $modifiedColumns[':p' . $index++]  = 'reseller_price';
        }
        if ($this->isColumnModified(ProductTableMap::COL_ADVICE_PRICE)) {
            $modifiedColumns[':p' . $index++]  = 'advice_price';
        }
        if ($this->isColumnModified(ProductTableMap::COL_BULK_STOCK_ID)) {
            $modifiedColumns[':p' . $index++]  = 'bulk_stock_id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_STOCK_QUANTITY)) {
            $modifiedColumns[':p' . $index++]  = 'stock_quantity';
        }
        if ($this->isColumnModified(ProductTableMap::COL_VIRTUAL_STOCK)) {
            $modifiedColumns[':p' . $index++]  = 'virtual_stock';
        }
        if ($this->isColumnModified(ProductTableMap::COL_QUANTITY_PER_PACK)) {
            $modifiedColumns[':p' . $index++]  = 'quantity_per_pack';
        }
        if ($this->isColumnModified(ProductTableMap::COL_YOUTUBE_URL)) {
            $modifiedColumns[':p' . $index++]  = 'youtube_url';
        }
        if ($this->isColumnModified(ProductTableMap::COL_META_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'meta_title';
        }
        if ($this->isColumnModified(ProductTableMap::COL_META_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'meta_description';
        }
        if ($this->isColumnModified(ProductTableMap::COL_META_KEYWORD)) {
            $modifiedColumns[':p' . $index++]  = 'meta_keyword';
        }
        if ($this->isColumnModified(ProductTableMap::COL_HASHTAG)) {
            $modifiedColumns[':p' . $index++]  = 'hashtag';
        }
        if ($this->isColumnModified(ProductTableMap::COL_CALCULATED_AVERAGE_RATING)) {
            $modifiedColumns[':p' . $index++]  = 'calculated_average_rating';
        }
        if ($this->isColumnModified(ProductTableMap::COL_VAT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'vat_id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_UNIT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'unit_id';
        }
        if ($this->isColumnModified(ProductTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(ProductTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO product (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'derrived_from_id':
                        $stmt->bindValue($identifier, $this->derrived_from_id, PDO::PARAM_INT);
                        break;
                    case 'old_id':
                        $stmt->bindValue($identifier, $this->old_id, PDO::PARAM_STR);
                        break;
                    case 'import_tag':
                        $stmt->bindValue($identifier, $this->import_tag, PDO::PARAM_STR);
                        break;
                    case 'sku':
                        $stmt->bindValue($identifier, $this->sku, PDO::PARAM_STR);
                        break;
                    case 'ean':
                        $stmt->bindValue($identifier, $this->ean, PDO::PARAM_STR);
                        break;
                    case 'deleted_on':
                        $stmt->bindValue($identifier, $this->deleted_on ? $this->deleted_on->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'deleted_by_user_id':
                        $stmt->bindValue($identifier, $this->deleted_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'popularity':
                        $stmt->bindValue($identifier, $this->popularity, PDO::PARAM_INT);
                        break;
                    case 'avg_rating':
                        $stmt->bindValue($identifier, $this->avg_rating, PDO::PARAM_INT);
                        break;
                    case 'supplier':
                        $stmt->bindValue($identifier, $this->supplier, PDO::PARAM_STR);
                        break;
                    case 'supplier_id':
                        $stmt->bindValue($identifier, $this->supplier_id, PDO::PARAM_INT);
                        break;
                    case 'supplier_product_number':
                        $stmt->bindValue($identifier, $this->supplier_product_number, PDO::PARAM_STR);
                        break;
                    case 'in_sale':
                        $stmt->bindValue($identifier, (int) $this->in_sale, PDO::PARAM_INT);
                        break;
                    case 'in_spotlight':
                        $stmt->bindValue($identifier, (int) $this->in_spotlight, PDO::PARAM_INT);
                        break;
                    case 'in_webshop':
                        $stmt->bindValue($identifier, (int) $this->in_webshop, PDO::PARAM_INT);
                        break;
                    case 'is_offer':
                        $stmt->bindValue($identifier, (int) $this->is_offer, PDO::PARAM_INT);
                        break;
                    case 'is_bestseller':
                        $stmt->bindValue($identifier, (int) $this->is_bestseller, PDO::PARAM_INT);
                        break;
                    case 'is_out_of_stock':
                        $stmt->bindValue($identifier, (int) $this->is_out_of_stock, PDO::PARAM_INT);
                        break;
                    case 'has_images':
                        $stmt->bindValue($identifier, (int) $this->has_images, PDO::PARAM_INT);
                        break;
                    case 'category_id':
                        $stmt->bindValue($identifier, $this->category_id, PDO::PARAM_INT);
                        break;
                    case 'number':
                        $stmt->bindValue($identifier, $this->number, PDO::PARAM_STR);
                        break;
                    case 'advertiser_user_id':
                        $stmt->bindValue($identifier, $this->advertiser_user_id, PDO::PARAM_INT);
                        break;
                    case 'delivery_time_id':
                        $stmt->bindValue($identifier, $this->delivery_time_id, PDO::PARAM_INT);
                        break;
                    case 'thickness':
                        $stmt->bindValue($identifier, $this->thickness, PDO::PARAM_STR);
                        break;
                    case 'height':
                        $stmt->bindValue($identifier, $this->height, PDO::PARAM_STR);
                        break;
                    case 'width':
                        $stmt->bindValue($identifier, $this->width, PDO::PARAM_STR);
                        break;
                    case 'length':
                        $stmt->bindValue($identifier, $this->length, PDO::PARAM_STR);
                        break;
                    case 'weight':
                        $stmt->bindValue($identifier, $this->weight, PDO::PARAM_STR);
                        break;
                    case 'composition':
                        $stmt->bindValue($identifier, $this->composition, PDO::PARAM_STR);
                        break;
                    case 'material_id':
                        $stmt->bindValue($identifier, $this->material_id, PDO::PARAM_STR);
                        break;
                    case 'brand_id':
                        $stmt->bindValue($identifier, $this->brand_id, PDO::PARAM_INT);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'description':
                        $stmt->bindValue($identifier, $this->description, PDO::PARAM_STR);
                        break;
                    case 'note':
                        $stmt->bindValue($identifier, $this->note, PDO::PARAM_STR);
                        break;
                    case 'created_on':
                        $stmt->bindValue($identifier, $this->created_on ? $this->created_on->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'day_price':
                        $stmt->bindValue($identifier, $this->day_price, PDO::PARAM_STR);
                        break;
                    case 'sale_price':
                        $stmt->bindValue($identifier, $this->sale_price, PDO::PARAM_STR);
                        break;
                    case 'discount_percentage':
                        $stmt->bindValue($identifier, $this->discount_percentage, PDO::PARAM_STR);
                        break;
                    case 'purchase_price':
                        $stmt->bindValue($identifier, $this->purchase_price, PDO::PARAM_STR);
                        break;
                    case 'reseller_price':
                        $stmt->bindValue($identifier, $this->reseller_price, PDO::PARAM_STR);
                        break;
                    case 'advice_price':
                        $stmt->bindValue($identifier, $this->advice_price, PDO::PARAM_STR);
                        break;
                    case 'bulk_stock_id':
                        $stmt->bindValue($identifier, $this->bulk_stock_id, PDO::PARAM_INT);
                        break;
                    case 'stock_quantity':
                        $stmt->bindValue($identifier, $this->stock_quantity, PDO::PARAM_INT);
                        break;
                    case 'virtual_stock':
                        $stmt->bindValue($identifier, (int) $this->virtual_stock, PDO::PARAM_INT);
                        break;
                    case 'quantity_per_pack':
                        $stmt->bindValue($identifier, $this->quantity_per_pack, PDO::PARAM_INT);
                        break;
                    case 'youtube_url':
                        $stmt->bindValue($identifier, $this->youtube_url, PDO::PARAM_STR);
                        break;
                    case 'meta_title':
                        $stmt->bindValue($identifier, $this->meta_title, PDO::PARAM_STR);
                        break;
                    case 'meta_description':
                        $stmt->bindValue($identifier, $this->meta_description, PDO::PARAM_STR);
                        break;
                    case 'meta_keyword':
                        $stmt->bindValue($identifier, $this->meta_keyword, PDO::PARAM_STR);
                        break;
                    case 'hashtag':
                        $stmt->bindValue($identifier, $this->hashtag, PDO::PARAM_STR);
                        break;
                    case 'calculated_average_rating':
                        $stmt->bindValue($identifier, $this->calculated_average_rating, PDO::PARAM_INT);
                        break;
                    case 'vat_id':
                        $stmt->bindValue($identifier, $this->vat_id, PDO::PARAM_INT);
                        break;
                    case 'unit_id':
                        $stmt->bindValue($identifier, $this->unit_id, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProductTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getDerrivedFromId();
                break;
            case 2:
                return $this->getOldId();
                break;
            case 3:
                return $this->getImportTag();
                break;
            case 4:
                return $this->getSku();
                break;
            case 5:
                return $this->getEan();
                break;
            case 6:
                return $this->getDeletedOn();
                break;
            case 7:
                return $this->getDeletedByUserId();
                break;
            case 8:
                return $this->getPopularity();
                break;
            case 9:
                return $this->getAvgRating();
                break;
            case 10:
                return $this->getSupplier();
                break;
            case 11:
                return $this->getSupplierId();
                break;
            case 12:
                return $this->getSupplierProductNumber();
                break;
            case 13:
                return $this->getInSale();
                break;
            case 14:
                return $this->getInSpotlight();
                break;
            case 15:
                return $this->getInWebshop();
                break;
            case 16:
                return $this->getIsOffer();
                break;
            case 17:
                return $this->getIsBestseller();
                break;
            case 18:
                return $this->getIsOutOfStock();
                break;
            case 19:
                return $this->getHasImages();
                break;
            case 20:
                return $this->getCategoryId();
                break;
            case 21:
                return $this->getNumber();
                break;
            case 22:
                return $this->getAdvertiserUserId();
                break;
            case 23:
                return $this->getDeliveryTimeId();
                break;
            case 24:
                return $this->getThickness();
                break;
            case 25:
                return $this->getHeight();
                break;
            case 26:
                return $this->getWidth();
                break;
            case 27:
                return $this->getLength();
                break;
            case 28:
                return $this->getWeight();
                break;
            case 29:
                return $this->getComposition();
                break;
            case 30:
                return $this->getMaterialId();
                break;
            case 31:
                return $this->getBrandId();
                break;
            case 32:
                return $this->getTitle();
                break;
            case 33:
                return $this->getDescription();
                break;
            case 34:
                return $this->getNote();
                break;
            case 35:
                return $this->getCreatedOn();
                break;
            case 36:
                return $this->getDayPrice();
                break;
            case 37:
                return $this->getSalePrice();
                break;
            case 38:
                return $this->getDiscountPercentage();
                break;
            case 39:
                return $this->getPurchasePrice();
                break;
            case 40:
                return $this->getResellerPrice();
                break;
            case 41:
                return $this->getAdvicePrice();
                break;
            case 42:
                return $this->getBulkStockId();
                break;
            case 43:
                return $this->getStockQuantity();
                break;
            case 44:
                return $this->getVirtualStock();
                break;
            case 45:
                return $this->getQuantityPerPack();
                break;
            case 46:
                return $this->getYoutubeUrl();
                break;
            case 47:
                return $this->getMetaTitle();
                break;
            case 48:
                return $this->getMetaDescription();
                break;
            case 49:
                return $this->getMetaKeyword();
                break;
            case 50:
                return $this->getHashtag();
                break;
            case 51:
                return $this->getCalculatedAverageRating();
                break;
            case 52:
                return $this->getVatId();
                break;
            case 53:
                return $this->getUnitId();
                break;
            case 54:
                return $this->getCreatedAt();
                break;
            case 55:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Product'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Product'][$this->hashCode()] = true;
        $keys = ProductTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getDerrivedFromId(),
            $keys[2] => $this->getOldId(),
            $keys[3] => $this->getImportTag(),
            $keys[4] => $this->getSku(),
            $keys[5] => $this->getEan(),
            $keys[6] => $this->getDeletedOn(),
            $keys[7] => $this->getDeletedByUserId(),
            $keys[8] => $this->getPopularity(),
            $keys[9] => $this->getAvgRating(),
            $keys[10] => $this->getSupplier(),
            $keys[11] => $this->getSupplierId(),
            $keys[12] => $this->getSupplierProductNumber(),
            $keys[13] => $this->getInSale(),
            $keys[14] => $this->getInSpotlight(),
            $keys[15] => $this->getInWebshop(),
            $keys[16] => $this->getIsOffer(),
            $keys[17] => $this->getIsBestseller(),
            $keys[18] => $this->getIsOutOfStock(),
            $keys[19] => $this->getHasImages(),
            $keys[20] => $this->getCategoryId(),
            $keys[21] => $this->getNumber(),
            $keys[22] => $this->getAdvertiserUserId(),
            $keys[23] => $this->getDeliveryTimeId(),
            $keys[24] => $this->getThickness(),
            $keys[25] => $this->getHeight(),
            $keys[26] => $this->getWidth(),
            $keys[27] => $this->getLength(),
            $keys[28] => $this->getWeight(),
            $keys[29] => $this->getComposition(),
            $keys[30] => $this->getMaterialId(),
            $keys[31] => $this->getBrandId(),
            $keys[32] => $this->getTitle(),
            $keys[33] => $this->getDescription(),
            $keys[34] => $this->getNote(),
            $keys[35] => $this->getCreatedOn(),
            $keys[36] => $this->getDayPrice(),
            $keys[37] => $this->getSalePrice(),
            $keys[38] => $this->getDiscountPercentage(),
            $keys[39] => $this->getPurchasePrice(),
            $keys[40] => $this->getResellerPrice(),
            $keys[41] => $this->getAdvicePrice(),
            $keys[42] => $this->getBulkStockId(),
            $keys[43] => $this->getStockQuantity(),
            $keys[44] => $this->getVirtualStock(),
            $keys[45] => $this->getQuantityPerPack(),
            $keys[46] => $this->getYoutubeUrl(),
            $keys[47] => $this->getMetaTitle(),
            $keys[48] => $this->getMetaDescription(),
            $keys[49] => $this->getMetaKeyword(),
            $keys[50] => $this->getHashtag(),
            $keys[51] => $this->getCalculatedAverageRating(),
            $keys[52] => $this->getVatId(),
            $keys[53] => $this->getUnitId(),
            $keys[54] => $this->getCreatedAt(),
            $keys[55] => $this->getUpdatedAt(),
        );
        if ($result[$keys[6]] instanceof \DateTimeInterface) {
            $result[$keys[6]] = $result[$keys[6]]->format('c');
        }

        if ($result[$keys[35]] instanceof \DateTimeInterface) {
            $result[$keys[35]] = $result[$keys[35]]->format('c');
        }

        if ($result[$keys[54]] instanceof \DateTimeInterface) {
            $result[$keys[54]] = $result[$keys[54]]->format('c');
        }

        if ($result[$keys[55]] instanceof \DateTimeInterface) {
            $result[$keys[55]] = $result[$keys[55]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aBrand) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'brand';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_brand';
                        break;
                    default:
                        $key = 'Brand';
                }

                $result[$key] = $this->aBrand->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aProductUnit) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productUnit';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_unit';
                        break;
                    default:
                        $key = 'ProductUnit';
                }

                $result[$key] = $this->aProductUnit->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aSupplierRef) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'supplier';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'supplier';
                        break;
                    default:
                        $key = 'SupplierRef';
                }

                $result[$key] = $this->aSupplierRef->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCategory) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'category';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'category';
                        break;
                    default:
                        $key = 'Category';
                }

                $result[$key] = $this->aCategory->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user';
                        break;
                    default:
                        $key = 'User';
                }

                $result[$key] = $this->aUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aVat) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'vat';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mt_vat';
                        break;
                    default:
                        $key = 'Vat';
                }

                $result[$key] = $this->aVat->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collProductMultiCategories) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productMultiCategories';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_multi_categories';
                        break;
                    default:
                        $key = 'ProductMultiCategories';
                }

                $result[$key] = $this->collProductMultiCategories->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaleOrderItemProducts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'orderItemProducts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_order_item_products';
                        break;
                    default:
                        $key = 'SaleOrderItemProducts';
                }

                $result[$key] = $this->collSaleOrderItemProducts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProductReviews) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productReviews';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_reviews';
                        break;
                    default:
                        $key = 'ProductReviews';
                }

                $result[$key] = $this->collProductReviews->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collPromoProducts) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'promoProducts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'promo_products';
                        break;
                    default:
                        $key = 'PromoProducts';
                }

                $result[$key] = $this->collPromoProducts->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProductProperties) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productProperties';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_properties';
                        break;
                    default:
                        $key = 'ProductProperties';
                }

                $result[$key] = $this->collProductProperties->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProductColors) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productColors';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_colors';
                        break;
                    default:
                        $key = 'ProductColors';
                }

                $result[$key] = $this->collProductColors->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProductLists) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productLists';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_lists';
                        break;
                    default:
                        $key = 'ProductLists';
                }

                $result[$key] = $this->collProductLists->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProductUsages) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productUsages';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_usages';
                        break;
                    default:
                        $key = 'ProductUsages';
                }

                $result[$key] = $this->collProductUsages->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProductTranslations) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_translations';
                        break;
                    default:
                        $key = 'ProductTranslations';
                }

                $result[$key] = $this->collProductTranslations->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRelatedProductsRelatedByProductId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'relatedProducts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'related_products';
                        break;
                    default:
                        $key = 'RelatedProducts';
                }

                $result[$key] = $this->collRelatedProductsRelatedByProductId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRelatedProductsRelatedByOtherProductId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'relatedProducts';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'related_products';
                        break;
                    default:
                        $key = 'RelatedProducts';
                }

                $result[$key] = $this->collRelatedProductsRelatedByOtherProductId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProductImages) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productImages';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_images';
                        break;
                    default:
                        $key = 'ProductImages';
                }

                $result[$key] = $this->collProductImages->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProductTags) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'productTags';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'product_tags';
                        break;
                    default:
                        $key = 'ProductTags';
                }

                $result[$key] = $this->collProductTags->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collSaleOrderItemStockClaimeds) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'saleOrderItemStockClaimeds';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'sale_order_item_stock_claimeds';
                        break;
                    default:
                        $key = 'SaleOrderItemStockClaimeds';
                }

                $result[$key] = $this->collSaleOrderItemStockClaimeds->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collCustomerWishlists) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'customerWishlists';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'customer_wishlists';
                        break;
                    default:
                        $key = 'CustomerWishlists';
                }

                $result[$key] = $this->collCustomerWishlists->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Product
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProductTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Product
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setDerrivedFromId($value);
                break;
            case 2:
                $this->setOldId($value);
                break;
            case 3:
                $this->setImportTag($value);
                break;
            case 4:
                $this->setSku($value);
                break;
            case 5:
                $this->setEan($value);
                break;
            case 6:
                $this->setDeletedOn($value);
                break;
            case 7:
                $this->setDeletedByUserId($value);
                break;
            case 8:
                $this->setPopularity($value);
                break;
            case 9:
                $this->setAvgRating($value);
                break;
            case 10:
                $this->setSupplier($value);
                break;
            case 11:
                $this->setSupplierId($value);
                break;
            case 12:
                $this->setSupplierProductNumber($value);
                break;
            case 13:
                $this->setInSale($value);
                break;
            case 14:
                $this->setInSpotlight($value);
                break;
            case 15:
                $this->setInWebshop($value);
                break;
            case 16:
                $this->setIsOffer($value);
                break;
            case 17:
                $this->setIsBestseller($value);
                break;
            case 18:
                $this->setIsOutOfStock($value);
                break;
            case 19:
                $this->setHasImages($value);
                break;
            case 20:
                $this->setCategoryId($value);
                break;
            case 21:
                $this->setNumber($value);
                break;
            case 22:
                $this->setAdvertiserUserId($value);
                break;
            case 23:
                $this->setDeliveryTimeId($value);
                break;
            case 24:
                $this->setThickness($value);
                break;
            case 25:
                $this->setHeight($value);
                break;
            case 26:
                $this->setWidth($value);
                break;
            case 27:
                $this->setLength($value);
                break;
            case 28:
                $this->setWeight($value);
                break;
            case 29:
                $this->setComposition($value);
                break;
            case 30:
                $this->setMaterialId($value);
                break;
            case 31:
                $this->setBrandId($value);
                break;
            case 32:
                $this->setTitle($value);
                break;
            case 33:
                $this->setDescription($value);
                break;
            case 34:
                $this->setNote($value);
                break;
            case 35:
                $this->setCreatedOn($value);
                break;
            case 36:
                $this->setDayPrice($value);
                break;
            case 37:
                $this->setSalePrice($value);
                break;
            case 38:
                $this->setDiscountPercentage($value);
                break;
            case 39:
                $this->setPurchasePrice($value);
                break;
            case 40:
                $this->setResellerPrice($value);
                break;
            case 41:
                $this->setAdvicePrice($value);
                break;
            case 42:
                $this->setBulkStockId($value);
                break;
            case 43:
                $this->setStockQuantity($value);
                break;
            case 44:
                $this->setVirtualStock($value);
                break;
            case 45:
                $this->setQuantityPerPack($value);
                break;
            case 46:
                $this->setYoutubeUrl($value);
                break;
            case 47:
                $this->setMetaTitle($value);
                break;
            case 48:
                $this->setMetaDescription($value);
                break;
            case 49:
                $this->setMetaKeyword($value);
                break;
            case 50:
                $this->setHashtag($value);
                break;
            case 51:
                $this->setCalculatedAverageRating($value);
                break;
            case 52:
                $this->setVatId($value);
                break;
            case 53:
                $this->setUnitId($value);
                break;
            case 54:
                $this->setCreatedAt($value);
                break;
            case 55:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ProductTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setDerrivedFromId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setOldId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setImportTag($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSku($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setEan($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setDeletedOn($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setDeletedByUserId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setPopularity($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setAvgRating($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setSupplier($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setSupplierId($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setSupplierProductNumber($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setInSale($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setInSpotlight($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setInWebshop($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setIsOffer($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setIsBestseller($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setIsOutOfStock($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setHasImages($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setCategoryId($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setNumber($arr[$keys[21]]);
        }
        if (array_key_exists($keys[22], $arr)) {
            $this->setAdvertiserUserId($arr[$keys[22]]);
        }
        if (array_key_exists($keys[23], $arr)) {
            $this->setDeliveryTimeId($arr[$keys[23]]);
        }
        if (array_key_exists($keys[24], $arr)) {
            $this->setThickness($arr[$keys[24]]);
        }
        if (array_key_exists($keys[25], $arr)) {
            $this->setHeight($arr[$keys[25]]);
        }
        if (array_key_exists($keys[26], $arr)) {
            $this->setWidth($arr[$keys[26]]);
        }
        if (array_key_exists($keys[27], $arr)) {
            $this->setLength($arr[$keys[27]]);
        }
        if (array_key_exists($keys[28], $arr)) {
            $this->setWeight($arr[$keys[28]]);
        }
        if (array_key_exists($keys[29], $arr)) {
            $this->setComposition($arr[$keys[29]]);
        }
        if (array_key_exists($keys[30], $arr)) {
            $this->setMaterialId($arr[$keys[30]]);
        }
        if (array_key_exists($keys[31], $arr)) {
            $this->setBrandId($arr[$keys[31]]);
        }
        if (array_key_exists($keys[32], $arr)) {
            $this->setTitle($arr[$keys[32]]);
        }
        if (array_key_exists($keys[33], $arr)) {
            $this->setDescription($arr[$keys[33]]);
        }
        if (array_key_exists($keys[34], $arr)) {
            $this->setNote($arr[$keys[34]]);
        }
        if (array_key_exists($keys[35], $arr)) {
            $this->setCreatedOn($arr[$keys[35]]);
        }
        if (array_key_exists($keys[36], $arr)) {
            $this->setDayPrice($arr[$keys[36]]);
        }
        if (array_key_exists($keys[37], $arr)) {
            $this->setSalePrice($arr[$keys[37]]);
        }
        if (array_key_exists($keys[38], $arr)) {
            $this->setDiscountPercentage($arr[$keys[38]]);
        }
        if (array_key_exists($keys[39], $arr)) {
            $this->setPurchasePrice($arr[$keys[39]]);
        }
        if (array_key_exists($keys[40], $arr)) {
            $this->setResellerPrice($arr[$keys[40]]);
        }
        if (array_key_exists($keys[41], $arr)) {
            $this->setAdvicePrice($arr[$keys[41]]);
        }
        if (array_key_exists($keys[42], $arr)) {
            $this->setBulkStockId($arr[$keys[42]]);
        }
        if (array_key_exists($keys[43], $arr)) {
            $this->setStockQuantity($arr[$keys[43]]);
        }
        if (array_key_exists($keys[44], $arr)) {
            $this->setVirtualStock($arr[$keys[44]]);
        }
        if (array_key_exists($keys[45], $arr)) {
            $this->setQuantityPerPack($arr[$keys[45]]);
        }
        if (array_key_exists($keys[46], $arr)) {
            $this->setYoutubeUrl($arr[$keys[46]]);
        }
        if (array_key_exists($keys[47], $arr)) {
            $this->setMetaTitle($arr[$keys[47]]);
        }
        if (array_key_exists($keys[48], $arr)) {
            $this->setMetaDescription($arr[$keys[48]]);
        }
        if (array_key_exists($keys[49], $arr)) {
            $this->setMetaKeyword($arr[$keys[49]]);
        }
        if (array_key_exists($keys[50], $arr)) {
            $this->setHashtag($arr[$keys[50]]);
        }
        if (array_key_exists($keys[51], $arr)) {
            $this->setCalculatedAverageRating($arr[$keys[51]]);
        }
        if (array_key_exists($keys[52], $arr)) {
            $this->setVatId($arr[$keys[52]]);
        }
        if (array_key_exists($keys[53], $arr)) {
            $this->setUnitId($arr[$keys[53]]);
        }
        if (array_key_exists($keys[54], $arr)) {
            $this->setCreatedAt($arr[$keys[54]]);
        }
        if (array_key_exists($keys[55], $arr)) {
            $this->setUpdatedAt($arr[$keys[55]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Product The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ProductTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ProductTableMap::COL_ID)) {
            $criteria->add(ProductTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_DERRIVED_FROM_ID)) {
            $criteria->add(ProductTableMap::COL_DERRIVED_FROM_ID, $this->derrived_from_id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_OLD_ID)) {
            $criteria->add(ProductTableMap::COL_OLD_ID, $this->old_id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_IMPORT_TAG)) {
            $criteria->add(ProductTableMap::COL_IMPORT_TAG, $this->import_tag);
        }
        if ($this->isColumnModified(ProductTableMap::COL_SKU)) {
            $criteria->add(ProductTableMap::COL_SKU, $this->sku);
        }
        if ($this->isColumnModified(ProductTableMap::COL_EAN)) {
            $criteria->add(ProductTableMap::COL_EAN, $this->ean);
        }
        if ($this->isColumnModified(ProductTableMap::COL_DELETED_ON)) {
            $criteria->add(ProductTableMap::COL_DELETED_ON, $this->deleted_on);
        }
        if ($this->isColumnModified(ProductTableMap::COL_DELETED_BY_USER_ID)) {
            $criteria->add(ProductTableMap::COL_DELETED_BY_USER_ID, $this->deleted_by_user_id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_POPULARITY)) {
            $criteria->add(ProductTableMap::COL_POPULARITY, $this->popularity);
        }
        if ($this->isColumnModified(ProductTableMap::COL_AVG_RATING)) {
            $criteria->add(ProductTableMap::COL_AVG_RATING, $this->avg_rating);
        }
        if ($this->isColumnModified(ProductTableMap::COL_SUPPLIER)) {
            $criteria->add(ProductTableMap::COL_SUPPLIER, $this->supplier);
        }
        if ($this->isColumnModified(ProductTableMap::COL_SUPPLIER_ID)) {
            $criteria->add(ProductTableMap::COL_SUPPLIER_ID, $this->supplier_id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_SUPPLIER_PRODUCT_NUMBER)) {
            $criteria->add(ProductTableMap::COL_SUPPLIER_PRODUCT_NUMBER, $this->supplier_product_number);
        }
        if ($this->isColumnModified(ProductTableMap::COL_IN_SALE)) {
            $criteria->add(ProductTableMap::COL_IN_SALE, $this->in_sale);
        }
        if ($this->isColumnModified(ProductTableMap::COL_IN_SPOTLIGHT)) {
            $criteria->add(ProductTableMap::COL_IN_SPOTLIGHT, $this->in_spotlight);
        }
        if ($this->isColumnModified(ProductTableMap::COL_IN_WEBSHOP)) {
            $criteria->add(ProductTableMap::COL_IN_WEBSHOP, $this->in_webshop);
        }
        if ($this->isColumnModified(ProductTableMap::COL_IS_OFFER)) {
            $criteria->add(ProductTableMap::COL_IS_OFFER, $this->is_offer);
        }
        if ($this->isColumnModified(ProductTableMap::COL_IS_BESTSELLER)) {
            $criteria->add(ProductTableMap::COL_IS_BESTSELLER, $this->is_bestseller);
        }
        if ($this->isColumnModified(ProductTableMap::COL_IS_OUT_OF_STOCK)) {
            $criteria->add(ProductTableMap::COL_IS_OUT_OF_STOCK, $this->is_out_of_stock);
        }
        if ($this->isColumnModified(ProductTableMap::COL_HAS_IMAGES)) {
            $criteria->add(ProductTableMap::COL_HAS_IMAGES, $this->has_images);
        }
        if ($this->isColumnModified(ProductTableMap::COL_CATEGORY_ID)) {
            $criteria->add(ProductTableMap::COL_CATEGORY_ID, $this->category_id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_NUMBER)) {
            $criteria->add(ProductTableMap::COL_NUMBER, $this->number);
        }
        if ($this->isColumnModified(ProductTableMap::COL_ADVERTISER_USER_ID)) {
            $criteria->add(ProductTableMap::COL_ADVERTISER_USER_ID, $this->advertiser_user_id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_DELIVERY_TIME_ID)) {
            $criteria->add(ProductTableMap::COL_DELIVERY_TIME_ID, $this->delivery_time_id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_THICKNESS)) {
            $criteria->add(ProductTableMap::COL_THICKNESS, $this->thickness);
        }
        if ($this->isColumnModified(ProductTableMap::COL_HEIGHT)) {
            $criteria->add(ProductTableMap::COL_HEIGHT, $this->height);
        }
        if ($this->isColumnModified(ProductTableMap::COL_WIDTH)) {
            $criteria->add(ProductTableMap::COL_WIDTH, $this->width);
        }
        if ($this->isColumnModified(ProductTableMap::COL_LENGTH)) {
            $criteria->add(ProductTableMap::COL_LENGTH, $this->length);
        }
        if ($this->isColumnModified(ProductTableMap::COL_WEIGHT)) {
            $criteria->add(ProductTableMap::COL_WEIGHT, $this->weight);
        }
        if ($this->isColumnModified(ProductTableMap::COL_COMPOSITION)) {
            $criteria->add(ProductTableMap::COL_COMPOSITION, $this->composition);
        }
        if ($this->isColumnModified(ProductTableMap::COL_MATERIAL_ID)) {
            $criteria->add(ProductTableMap::COL_MATERIAL_ID, $this->material_id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_BRAND_ID)) {
            $criteria->add(ProductTableMap::COL_BRAND_ID, $this->brand_id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_TITLE)) {
            $criteria->add(ProductTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(ProductTableMap::COL_DESCRIPTION)) {
            $criteria->add(ProductTableMap::COL_DESCRIPTION, $this->description);
        }
        if ($this->isColumnModified(ProductTableMap::COL_NOTE)) {
            $criteria->add(ProductTableMap::COL_NOTE, $this->note);
        }
        if ($this->isColumnModified(ProductTableMap::COL_CREATED_ON)) {
            $criteria->add(ProductTableMap::COL_CREATED_ON, $this->created_on);
        }
        if ($this->isColumnModified(ProductTableMap::COL_DAY_PRICE)) {
            $criteria->add(ProductTableMap::COL_DAY_PRICE, $this->day_price);
        }
        if ($this->isColumnModified(ProductTableMap::COL_SALE_PRICE)) {
            $criteria->add(ProductTableMap::COL_SALE_PRICE, $this->sale_price);
        }
        if ($this->isColumnModified(ProductTableMap::COL_DISCOUNT_PERCENTAGE)) {
            $criteria->add(ProductTableMap::COL_DISCOUNT_PERCENTAGE, $this->discount_percentage);
        }
        if ($this->isColumnModified(ProductTableMap::COL_PURCHASE_PRICE)) {
            $criteria->add(ProductTableMap::COL_PURCHASE_PRICE, $this->purchase_price);
        }
        if ($this->isColumnModified(ProductTableMap::COL_RESELLER_PRICE)) {
            $criteria->add(ProductTableMap::COL_RESELLER_PRICE, $this->reseller_price);
        }
        if ($this->isColumnModified(ProductTableMap::COL_ADVICE_PRICE)) {
            $criteria->add(ProductTableMap::COL_ADVICE_PRICE, $this->advice_price);
        }
        if ($this->isColumnModified(ProductTableMap::COL_BULK_STOCK_ID)) {
            $criteria->add(ProductTableMap::COL_BULK_STOCK_ID, $this->bulk_stock_id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_STOCK_QUANTITY)) {
            $criteria->add(ProductTableMap::COL_STOCK_QUANTITY, $this->stock_quantity);
        }
        if ($this->isColumnModified(ProductTableMap::COL_VIRTUAL_STOCK)) {
            $criteria->add(ProductTableMap::COL_VIRTUAL_STOCK, $this->virtual_stock);
        }
        if ($this->isColumnModified(ProductTableMap::COL_QUANTITY_PER_PACK)) {
            $criteria->add(ProductTableMap::COL_QUANTITY_PER_PACK, $this->quantity_per_pack);
        }
        if ($this->isColumnModified(ProductTableMap::COL_YOUTUBE_URL)) {
            $criteria->add(ProductTableMap::COL_YOUTUBE_URL, $this->youtube_url);
        }
        if ($this->isColumnModified(ProductTableMap::COL_META_TITLE)) {
            $criteria->add(ProductTableMap::COL_META_TITLE, $this->meta_title);
        }
        if ($this->isColumnModified(ProductTableMap::COL_META_DESCRIPTION)) {
            $criteria->add(ProductTableMap::COL_META_DESCRIPTION, $this->meta_description);
        }
        if ($this->isColumnModified(ProductTableMap::COL_META_KEYWORD)) {
            $criteria->add(ProductTableMap::COL_META_KEYWORD, $this->meta_keyword);
        }
        if ($this->isColumnModified(ProductTableMap::COL_HASHTAG)) {
            $criteria->add(ProductTableMap::COL_HASHTAG, $this->hashtag);
        }
        if ($this->isColumnModified(ProductTableMap::COL_CALCULATED_AVERAGE_RATING)) {
            $criteria->add(ProductTableMap::COL_CALCULATED_AVERAGE_RATING, $this->calculated_average_rating);
        }
        if ($this->isColumnModified(ProductTableMap::COL_VAT_ID)) {
            $criteria->add(ProductTableMap::COL_VAT_ID, $this->vat_id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_UNIT_ID)) {
            $criteria->add(ProductTableMap::COL_UNIT_ID, $this->unit_id);
        }
        if ($this->isColumnModified(ProductTableMap::COL_CREATED_AT)) {
            $criteria->add(ProductTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(ProductTableMap::COL_UPDATED_AT)) {
            $criteria->add(ProductTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildProductQuery::create();
        $criteria->add(ProductTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Product (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setDerrivedFromId($this->getDerrivedFromId());
        $copyObj->setOldId($this->getOldId());
        $copyObj->setImportTag($this->getImportTag());
        $copyObj->setSku($this->getSku());
        $copyObj->setEan($this->getEan());
        $copyObj->setDeletedOn($this->getDeletedOn());
        $copyObj->setDeletedByUserId($this->getDeletedByUserId());
        $copyObj->setPopularity($this->getPopularity());
        $copyObj->setAvgRating($this->getAvgRating());
        $copyObj->setSupplier($this->getSupplier());
        $copyObj->setSupplierId($this->getSupplierId());
        $copyObj->setSupplierProductNumber($this->getSupplierProductNumber());
        $copyObj->setInSale($this->getInSale());
        $copyObj->setInSpotlight($this->getInSpotlight());
        $copyObj->setInWebshop($this->getInWebshop());
        $copyObj->setIsOffer($this->getIsOffer());
        $copyObj->setIsBestseller($this->getIsBestseller());
        $copyObj->setIsOutOfStock($this->getIsOutOfStock());
        $copyObj->setHasImages($this->getHasImages());
        $copyObj->setCategoryId($this->getCategoryId());
        $copyObj->setNumber($this->getNumber());
        $copyObj->setAdvertiserUserId($this->getAdvertiserUserId());
        $copyObj->setDeliveryTimeId($this->getDeliveryTimeId());
        $copyObj->setThickness($this->getThickness());
        $copyObj->setHeight($this->getHeight());
        $copyObj->setWidth($this->getWidth());
        $copyObj->setLength($this->getLength());
        $copyObj->setWeight($this->getWeight());
        $copyObj->setComposition($this->getComposition());
        $copyObj->setMaterialId($this->getMaterialId());
        $copyObj->setBrandId($this->getBrandId());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setNote($this->getNote());
        $copyObj->setCreatedOn($this->getCreatedOn());
        $copyObj->setDayPrice($this->getDayPrice());
        $copyObj->setSalePrice($this->getSalePrice());
        $copyObj->setDiscountPercentage($this->getDiscountPercentage());
        $copyObj->setPurchasePrice($this->getPurchasePrice());
        $copyObj->setResellerPrice($this->getResellerPrice());
        $copyObj->setAdvicePrice($this->getAdvicePrice());
        $copyObj->setBulkStockId($this->getBulkStockId());
        $copyObj->setStockQuantity($this->getStockQuantity());
        $copyObj->setVirtualStock($this->getVirtualStock());
        $copyObj->setQuantityPerPack($this->getQuantityPerPack());
        $copyObj->setYoutubeUrl($this->getYoutubeUrl());
        $copyObj->setMetaTitle($this->getMetaTitle());
        $copyObj->setMetaDescription($this->getMetaDescription());
        $copyObj->setMetaKeyword($this->getMetaKeyword());
        $copyObj->setHashtag($this->getHashtag());
        $copyObj->setCalculatedAverageRating($this->getCalculatedAverageRating());
        $copyObj->setVatId($this->getVatId());
        $copyObj->setUnitId($this->getUnitId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getProductMultiCategories() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductMultiCategory($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaleOrderItemProducts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaleOrderItemProduct($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProductReviews() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductReview($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getPromoProducts() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addPromoProduct($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProductProperties() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductProperty($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProductColors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductColor($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProductLists() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductList($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProductUsages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductUsage($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProductTranslations() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductTranslation($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRelatedProductsRelatedByProductId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRelatedProductRelatedByProductId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRelatedProductsRelatedByOtherProductId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRelatedProductRelatedByOtherProductId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProductImages() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductImage($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProductTags() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProductTag($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getSaleOrderItemStockClaimeds() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addSaleOrderItemStockClaimed($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getCustomerWishlists() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addCustomerWishlist($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Product Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a Brand object.
     *
     * @param  Brand|null $v
     * @return $this|\Model\Product The current object (for fluent API support)
     * @throws PropelException
     */
    public function setBrand(Brand $v = null)
    {
        if ($v === null) {
            $this->setBrandId(NULL);
        } else {
            $this->setBrandId($v->getId());
        }

        $this->aBrand = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Brand object, it will not be re-added.
        if ($v !== null) {
            $v->addProduct($this);
        }


        return $this;
    }


    /**
     * Get the associated Brand object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Brand|null The associated Brand object.
     * @throws PropelException
     */
    public function getBrand(ConnectionInterface $con = null)
    {
        if ($this->aBrand === null && ($this->brand_id != 0)) {
            $this->aBrand = BrandQuery::create()->findPk($this->brand_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aBrand->addProducts($this);
             */
        }

        return $this->aBrand;
    }

    /**
     * Declares an association between this object and a ProductUnit object.
     *
     * @param  ProductUnit|null $v
     * @return $this|\Model\Product The current object (for fluent API support)
     * @throws PropelException
     */
    public function setProductUnit(ProductUnit $v = null)
    {
        if ($v === null) {
            $this->setUnitId(NULL);
        } else {
            $this->setUnitId($v->getId());
        }

        $this->aProductUnit = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ProductUnit object, it will not be re-added.
        if ($v !== null) {
            $v->addProduct($this);
        }


        return $this;
    }


    /**
     * Get the associated ProductUnit object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ProductUnit|null The associated ProductUnit object.
     * @throws PropelException
     */
    public function getProductUnit(ConnectionInterface $con = null)
    {
        if ($this->aProductUnit === null && ($this->unit_id != 0)) {
            $this->aProductUnit = ProductUnitQuery::create()->findPk($this->unit_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aProductUnit->addProducts($this);
             */
        }

        return $this->aProductUnit;
    }

    /**
     * Declares an association between this object and a Supplier object.
     *
     * @param  Supplier|null $v
     * @return $this|\Model\Product The current object (for fluent API support)
     * @throws PropelException
     */
    public function setSupplierRef(Supplier $v = null)
    {
        if ($v === null) {
            $this->setSupplierId(NULL);
        } else {
            $this->setSupplierId($v->getId());
        }

        $this->aSupplierRef = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Supplier object, it will not be re-added.
        if ($v !== null) {
            $v->addProduct($this);
        }


        return $this;
    }


    /**
     * Get the associated Supplier object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Supplier|null The associated Supplier object.
     * @throws PropelException
     */
    public function getSupplierRef(ConnectionInterface $con = null)
    {
        if ($this->aSupplierRef === null && ($this->supplier_id != 0)) {
            $this->aSupplierRef = SupplierQuery::create()->findPk($this->supplier_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aSupplierRef->addProducts($this);
             */
        }

        return $this->aSupplierRef;
    }

    /**
     * Declares an association between this object and a Category object.
     *
     * @param  Category $v
     * @return $this|\Model\Product The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCategory(Category $v = null)
    {
        if ($v === null) {
            $this->setCategoryId(NULL);
        } else {
            $this->setCategoryId($v->getId());
        }

        $this->aCategory = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Category object, it will not be re-added.
        if ($v !== null) {
            $v->addProduct($this);
        }


        return $this;
    }


    /**
     * Get the associated Category object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Category The associated Category object.
     * @throws PropelException
     */
    public function getCategory(ConnectionInterface $con = null)
    {
        if ($this->aCategory === null && ($this->category_id != 0)) {
            $this->aCategory = CategoryQuery::create()->findPk($this->category_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCategory->addProducts($this);
             */
        }

        return $this->aCategory;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User|null $v
     * @return $this|\Model\Product The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUser(User $v = null)
    {
        if ($v === null) {
            $this->setAdvertiserUserId(NULL);
        } else {
            $this->setAdvertiserUserId($v->getId());
        }

        $this->aUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addProduct($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User|null The associated User object.
     * @throws PropelException
     */
    public function getUser(ConnectionInterface $con = null)
    {
        if ($this->aUser === null && ($this->advertiser_user_id != 0)) {
            $this->aUser = UserQuery::create()->findPk($this->advertiser_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUser->addProducts($this);
             */
        }

        return $this->aUser;
    }

    /**
     * Declares an association between this object and a Vat object.
     *
     * @param  Vat|null $v
     * @return $this|\Model\Product The current object (for fluent API support)
     * @throws PropelException
     */
    public function setVat(Vat $v = null)
    {
        if ($v === null) {
            $this->setVatId(NULL);
        } else {
            $this->setVatId($v->getId());
        }

        $this->aVat = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Vat object, it will not be re-added.
        if ($v !== null) {
            $v->addProduct($this);
        }


        return $this;
    }


    /**
     * Get the associated Vat object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Vat|null The associated Vat object.
     * @throws PropelException
     */
    public function getVat(ConnectionInterface $con = null)
    {
        if ($this->aVat === null && ($this->vat_id != 0)) {
            $this->aVat = VatQuery::create()->findPk($this->vat_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aVat->addProducts($this);
             */
        }

        return $this->aVat;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ProductMultiCategory' === $relationName) {
            $this->initProductMultiCategories();
            return;
        }
        if ('SaleOrderItemProduct' === $relationName) {
            $this->initSaleOrderItemProducts();
            return;
        }
        if ('ProductReview' === $relationName) {
            $this->initProductReviews();
            return;
        }
        if ('PromoProduct' === $relationName) {
            $this->initPromoProducts();
            return;
        }
        if ('ProductProperty' === $relationName) {
            $this->initProductProperties();
            return;
        }
        if ('ProductColor' === $relationName) {
            $this->initProductColors();
            return;
        }
        if ('ProductList' === $relationName) {
            $this->initProductLists();
            return;
        }
        if ('ProductUsage' === $relationName) {
            $this->initProductUsages();
            return;
        }
        if ('ProductTranslation' === $relationName) {
            $this->initProductTranslations();
            return;
        }
        if ('RelatedProductRelatedByProductId' === $relationName) {
            $this->initRelatedProductsRelatedByProductId();
            return;
        }
        if ('RelatedProductRelatedByOtherProductId' === $relationName) {
            $this->initRelatedProductsRelatedByOtherProductId();
            return;
        }
        if ('ProductImage' === $relationName) {
            $this->initProductImages();
            return;
        }
        if ('ProductTag' === $relationName) {
            $this->initProductTags();
            return;
        }
        if ('SaleOrderItemStockClaimed' === $relationName) {
            $this->initSaleOrderItemStockClaimeds();
            return;
        }
        if ('CustomerWishlist' === $relationName) {
            $this->initCustomerWishlists();
            return;
        }
    }

    /**
     * Clears out the collProductMultiCategories collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductMultiCategories()
     */
    public function clearProductMultiCategories()
    {
        $this->collProductMultiCategories = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductMultiCategories collection loaded partially.
     */
    public function resetPartialProductMultiCategories($v = true)
    {
        $this->collProductMultiCategoriesPartial = $v;
    }

    /**
     * Initializes the collProductMultiCategories collection.
     *
     * By default this just sets the collProductMultiCategories collection to an empty array (like clearcollProductMultiCategories());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductMultiCategories($overrideExisting = true)
    {
        if (null !== $this->collProductMultiCategories && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductMultiCategoryTableMap::getTableMap()->getCollectionClassName();

        $this->collProductMultiCategories = new $collectionClassName;
        $this->collProductMultiCategories->setModel('\Model\Category\ProductMultiCategory');
    }

    /**
     * Gets an array of ProductMultiCategory objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ProductMultiCategory[] List of ProductMultiCategory objects
     * @throws PropelException
     */
    public function getProductMultiCategories(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductMultiCategoriesPartial && !$this->isNew();
        if (null === $this->collProductMultiCategories || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductMultiCategories) {
                    $this->initProductMultiCategories();
                } else {
                    $collectionClassName = ProductMultiCategoryTableMap::getTableMap()->getCollectionClassName();

                    $collProductMultiCategories = new $collectionClassName;
                    $collProductMultiCategories->setModel('\Model\Category\ProductMultiCategory');

                    return $collProductMultiCategories;
                }
            } else {
                $collProductMultiCategories = ProductMultiCategoryQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductMultiCategoriesPartial && count($collProductMultiCategories)) {
                        $this->initProductMultiCategories(false);

                        foreach ($collProductMultiCategories as $obj) {
                            if (false == $this->collProductMultiCategories->contains($obj)) {
                                $this->collProductMultiCategories->append($obj);
                            }
                        }

                        $this->collProductMultiCategoriesPartial = true;
                    }

                    return $collProductMultiCategories;
                }

                if ($partial && $this->collProductMultiCategories) {
                    foreach ($this->collProductMultiCategories as $obj) {
                        if ($obj->isNew()) {
                            $collProductMultiCategories[] = $obj;
                        }
                    }
                }

                $this->collProductMultiCategories = $collProductMultiCategories;
                $this->collProductMultiCategoriesPartial = false;
            }
        }

        return $this->collProductMultiCategories;
    }

    /**
     * Sets a collection of ProductMultiCategory objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productMultiCategories A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setProductMultiCategories(Collection $productMultiCategories, ConnectionInterface $con = null)
    {
        /** @var ProductMultiCategory[] $productMultiCategoriesToDelete */
        $productMultiCategoriesToDelete = $this->getProductMultiCategories(new Criteria(), $con)->diff($productMultiCategories);


        $this->productMultiCategoriesScheduledForDeletion = $productMultiCategoriesToDelete;

        foreach ($productMultiCategoriesToDelete as $productMultiCategoryRemoved) {
            $productMultiCategoryRemoved->setProduct(null);
        }

        $this->collProductMultiCategories = null;
        foreach ($productMultiCategories as $productMultiCategory) {
            $this->addProductMultiCategory($productMultiCategory);
        }

        $this->collProductMultiCategories = $productMultiCategories;
        $this->collProductMultiCategoriesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseProductMultiCategory objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseProductMultiCategory objects.
     * @throws PropelException
     */
    public function countProductMultiCategories(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductMultiCategoriesPartial && !$this->isNew();
        if (null === $this->collProductMultiCategories || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductMultiCategories) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductMultiCategories());
            }

            $query = ProductMultiCategoryQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collProductMultiCategories);
    }

    /**
     * Method called to associate a ProductMultiCategory object to this object
     * through the ProductMultiCategory foreign key attribute.
     *
     * @param  ProductMultiCategory $l ProductMultiCategory
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addProductMultiCategory(ProductMultiCategory $l)
    {
        if ($this->collProductMultiCategories === null) {
            $this->initProductMultiCategories();
            $this->collProductMultiCategoriesPartial = true;
        }

        if (!$this->collProductMultiCategories->contains($l)) {
            $this->doAddProductMultiCategory($l);

            if ($this->productMultiCategoriesScheduledForDeletion and $this->productMultiCategoriesScheduledForDeletion->contains($l)) {
                $this->productMultiCategoriesScheduledForDeletion->remove($this->productMultiCategoriesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ProductMultiCategory $productMultiCategory The ProductMultiCategory object to add.
     */
    protected function doAddProductMultiCategory(ProductMultiCategory $productMultiCategory)
    {
        $this->collProductMultiCategories[]= $productMultiCategory;
        $productMultiCategory->setProduct($this);
    }

    /**
     * @param  ProductMultiCategory $productMultiCategory The ProductMultiCategory object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeProductMultiCategory(ProductMultiCategory $productMultiCategory)
    {
        if ($this->getProductMultiCategories()->contains($productMultiCategory)) {
            $pos = $this->collProductMultiCategories->search($productMultiCategory);
            $this->collProductMultiCategories->remove($pos);
            if (null === $this->productMultiCategoriesScheduledForDeletion) {
                $this->productMultiCategoriesScheduledForDeletion = clone $this->collProductMultiCategories;
                $this->productMultiCategoriesScheduledForDeletion->clear();
            }
            $this->productMultiCategoriesScheduledForDeletion[]= clone $productMultiCategory;
            $productMultiCategory->setProduct(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Product is new, it will return
     * an empty collection; or if this Product has previously
     * been saved, it will retrieve related ProductMultiCategories from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Product.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ProductMultiCategory[] List of ProductMultiCategory objects
     */
    public function getProductMultiCategoriesJoinCategory(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductMultiCategoryQuery::create(null, $criteria);
        $query->joinWith('Category', $joinBehavior);

        return $this->getProductMultiCategories($query, $con);
    }

    /**
     * Clears out the collSaleOrderItemProducts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSaleOrderItemProducts()
     */
    public function clearSaleOrderItemProducts()
    {
        $this->collSaleOrderItemProducts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSaleOrderItemProducts collection loaded partially.
     */
    public function resetPartialSaleOrderItemProducts($v = true)
    {
        $this->collSaleOrderItemProductsPartial = $v;
    }

    /**
     * Initializes the collSaleOrderItemProducts collection.
     *
     * By default this just sets the collSaleOrderItemProducts collection to an empty array (like clearcollSaleOrderItemProducts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaleOrderItemProducts($overrideExisting = true)
    {
        if (null !== $this->collSaleOrderItemProducts && !$overrideExisting) {
            return;
        }

        $collectionClassName = OrderItemProductTableMap::getTableMap()->getCollectionClassName();

        $this->collSaleOrderItemProducts = new $collectionClassName;
        $this->collSaleOrderItemProducts->setModel('\Model\Sale\OrderItemProduct');
    }

    /**
     * Gets an array of OrderItemProduct objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|OrderItemProduct[] List of OrderItemProduct objects
     * @throws PropelException
     */
    public function getSaleOrderItemProducts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderItemProductsPartial && !$this->isNew();
        if (null === $this->collSaleOrderItemProducts || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSaleOrderItemProducts) {
                    $this->initSaleOrderItemProducts();
                } else {
                    $collectionClassName = OrderItemProductTableMap::getTableMap()->getCollectionClassName();

                    $collSaleOrderItemProducts = new $collectionClassName;
                    $collSaleOrderItemProducts->setModel('\Model\Sale\OrderItemProduct');

                    return $collSaleOrderItemProducts;
                }
            } else {
                $collSaleOrderItemProducts = OrderItemProductQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSaleOrderItemProductsPartial && count($collSaleOrderItemProducts)) {
                        $this->initSaleOrderItemProducts(false);

                        foreach ($collSaleOrderItemProducts as $obj) {
                            if (false == $this->collSaleOrderItemProducts->contains($obj)) {
                                $this->collSaleOrderItemProducts->append($obj);
                            }
                        }

                        $this->collSaleOrderItemProductsPartial = true;
                    }

                    return $collSaleOrderItemProducts;
                }

                if ($partial && $this->collSaleOrderItemProducts) {
                    foreach ($this->collSaleOrderItemProducts as $obj) {
                        if ($obj->isNew()) {
                            $collSaleOrderItemProducts[] = $obj;
                        }
                    }
                }

                $this->collSaleOrderItemProducts = $collSaleOrderItemProducts;
                $this->collSaleOrderItemProductsPartial = false;
            }
        }

        return $this->collSaleOrderItemProducts;
    }

    /**
     * Sets a collection of OrderItemProduct objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $saleOrderItemProducts A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setSaleOrderItemProducts(Collection $saleOrderItemProducts, ConnectionInterface $con = null)
    {
        /** @var OrderItemProduct[] $saleOrderItemProductsToDelete */
        $saleOrderItemProductsToDelete = $this->getSaleOrderItemProducts(new Criteria(), $con)->diff($saleOrderItemProducts);


        $this->saleOrderItemProductsScheduledForDeletion = $saleOrderItemProductsToDelete;

        foreach ($saleOrderItemProductsToDelete as $saleOrderItemProductRemoved) {
            $saleOrderItemProductRemoved->setProduct(null);
        }

        $this->collSaleOrderItemProducts = null;
        foreach ($saleOrderItemProducts as $saleOrderItemProduct) {
            $this->addSaleOrderItemProduct($saleOrderItemProduct);
        }

        $this->collSaleOrderItemProducts = $saleOrderItemProducts;
        $this->collSaleOrderItemProductsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseOrderItemProduct objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseOrderItemProduct objects.
     * @throws PropelException
     */
    public function countSaleOrderItemProducts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderItemProductsPartial && !$this->isNew();
        if (null === $this->collSaleOrderItemProducts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaleOrderItemProducts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSaleOrderItemProducts());
            }

            $query = OrderItemProductQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collSaleOrderItemProducts);
    }

    /**
     * Method called to associate a OrderItemProduct object to this object
     * through the OrderItemProduct foreign key attribute.
     *
     * @param  OrderItemProduct $l OrderItemProduct
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addSaleOrderItemProduct(OrderItemProduct $l)
    {
        if ($this->collSaleOrderItemProducts === null) {
            $this->initSaleOrderItemProducts();
            $this->collSaleOrderItemProductsPartial = true;
        }

        if (!$this->collSaleOrderItemProducts->contains($l)) {
            $this->doAddSaleOrderItemProduct($l);

            if ($this->saleOrderItemProductsScheduledForDeletion and $this->saleOrderItemProductsScheduledForDeletion->contains($l)) {
                $this->saleOrderItemProductsScheduledForDeletion->remove($this->saleOrderItemProductsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param OrderItemProduct $saleOrderItemProduct The OrderItemProduct object to add.
     */
    protected function doAddSaleOrderItemProduct(OrderItemProduct $saleOrderItemProduct)
    {
        $this->collSaleOrderItemProducts[]= $saleOrderItemProduct;
        $saleOrderItemProduct->setProduct($this);
    }

    /**
     * @param  OrderItemProduct $saleOrderItemProduct The OrderItemProduct object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeSaleOrderItemProduct(OrderItemProduct $saleOrderItemProduct)
    {
        if ($this->getSaleOrderItemProducts()->contains($saleOrderItemProduct)) {
            $pos = $this->collSaleOrderItemProducts->search($saleOrderItemProduct);
            $this->collSaleOrderItemProducts->remove($pos);
            if (null === $this->saleOrderItemProductsScheduledForDeletion) {
                $this->saleOrderItemProductsScheduledForDeletion = clone $this->collSaleOrderItemProducts;
                $this->saleOrderItemProductsScheduledForDeletion->clear();
            }
            $this->saleOrderItemProductsScheduledForDeletion[]= clone $saleOrderItemProduct;
            $saleOrderItemProduct->setProduct(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Product is new, it will return
     * an empty collection; or if this Product has previously
     * been saved, it will retrieve related SaleOrderItemProducts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Product.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|OrderItemProduct[] List of OrderItemProduct objects
     */
    public function getSaleOrderItemProductsJoinOrderItem(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = OrderItemProductQuery::create(null, $criteria);
        $query->joinWith('OrderItem', $joinBehavior);

        return $this->getSaleOrderItemProducts($query, $con);
    }

    /**
     * Clears out the collProductReviews collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductReviews()
     */
    public function clearProductReviews()
    {
        $this->collProductReviews = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductReviews collection loaded partially.
     */
    public function resetPartialProductReviews($v = true)
    {
        $this->collProductReviewsPartial = $v;
    }

    /**
     * Initializes the collProductReviews collection.
     *
     * By default this just sets the collProductReviews collection to an empty array (like clearcollProductReviews());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductReviews($overrideExisting = true)
    {
        if (null !== $this->collProductReviews && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductReviewTableMap::getTableMap()->getCollectionClassName();

        $this->collProductReviews = new $collectionClassName;
        $this->collProductReviews->setModel('\Model\ProductReview');
    }

    /**
     * Gets an array of ChildProductReview objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProductReview[] List of ChildProductReview objects
     * @throws PropelException
     */
    public function getProductReviews(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductReviewsPartial && !$this->isNew();
        if (null === $this->collProductReviews || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductReviews) {
                    $this->initProductReviews();
                } else {
                    $collectionClassName = ProductReviewTableMap::getTableMap()->getCollectionClassName();

                    $collProductReviews = new $collectionClassName;
                    $collProductReviews->setModel('\Model\ProductReview');

                    return $collProductReviews;
                }
            } else {
                $collProductReviews = ChildProductReviewQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductReviewsPartial && count($collProductReviews)) {
                        $this->initProductReviews(false);

                        foreach ($collProductReviews as $obj) {
                            if (false == $this->collProductReviews->contains($obj)) {
                                $this->collProductReviews->append($obj);
                            }
                        }

                        $this->collProductReviewsPartial = true;
                    }

                    return $collProductReviews;
                }

                if ($partial && $this->collProductReviews) {
                    foreach ($this->collProductReviews as $obj) {
                        if ($obj->isNew()) {
                            $collProductReviews[] = $obj;
                        }
                    }
                }

                $this->collProductReviews = $collProductReviews;
                $this->collProductReviewsPartial = false;
            }
        }

        return $this->collProductReviews;
    }

    /**
     * Sets a collection of ChildProductReview objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productReviews A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setProductReviews(Collection $productReviews, ConnectionInterface $con = null)
    {
        /** @var ChildProductReview[] $productReviewsToDelete */
        $productReviewsToDelete = $this->getProductReviews(new Criteria(), $con)->diff($productReviews);


        $this->productReviewsScheduledForDeletion = $productReviewsToDelete;

        foreach ($productReviewsToDelete as $productReviewRemoved) {
            $productReviewRemoved->setProduct(null);
        }

        $this->collProductReviews = null;
        foreach ($productReviews as $productReview) {
            $this->addProductReview($productReview);
        }

        $this->collProductReviews = $productReviews;
        $this->collProductReviewsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProductReview objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProductReview objects.
     * @throws PropelException
     */
    public function countProductReviews(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductReviewsPartial && !$this->isNew();
        if (null === $this->collProductReviews || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductReviews) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductReviews());
            }

            $query = ChildProductReviewQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collProductReviews);
    }

    /**
     * Method called to associate a ChildProductReview object to this object
     * through the ChildProductReview foreign key attribute.
     *
     * @param  ChildProductReview $l ChildProductReview
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addProductReview(ChildProductReview $l)
    {
        if ($this->collProductReviews === null) {
            $this->initProductReviews();
            $this->collProductReviewsPartial = true;
        }

        if (!$this->collProductReviews->contains($l)) {
            $this->doAddProductReview($l);

            if ($this->productReviewsScheduledForDeletion and $this->productReviewsScheduledForDeletion->contains($l)) {
                $this->productReviewsScheduledForDeletion->remove($this->productReviewsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProductReview $productReview The ChildProductReview object to add.
     */
    protected function doAddProductReview(ChildProductReview $productReview)
    {
        $this->collProductReviews[]= $productReview;
        $productReview->setProduct($this);
    }

    /**
     * @param  ChildProductReview $productReview The ChildProductReview object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeProductReview(ChildProductReview $productReview)
    {
        if ($this->getProductReviews()->contains($productReview)) {
            $pos = $this->collProductReviews->search($productReview);
            $this->collProductReviews->remove($pos);
            if (null === $this->productReviewsScheduledForDeletion) {
                $this->productReviewsScheduledForDeletion = clone $this->collProductReviews;
                $this->productReviewsScheduledForDeletion->clear();
            }
            $this->productReviewsScheduledForDeletion[]= clone $productReview;
            $productReview->setProduct(null);
        }

        return $this;
    }

    /**
     * Clears out the collPromoProducts collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addPromoProducts()
     */
    public function clearPromoProducts()
    {
        $this->collPromoProducts = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collPromoProducts collection loaded partially.
     */
    public function resetPartialPromoProducts($v = true)
    {
        $this->collPromoProductsPartial = $v;
    }

    /**
     * Initializes the collPromoProducts collection.
     *
     * By default this just sets the collPromoProducts collection to an empty array (like clearcollPromoProducts());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initPromoProducts($overrideExisting = true)
    {
        if (null !== $this->collPromoProducts && !$overrideExisting) {
            return;
        }

        $collectionClassName = PromoProductTableMap::getTableMap()->getCollectionClassName();

        $this->collPromoProducts = new $collectionClassName;
        $this->collPromoProducts->setModel('\Model\PromoProduct');
    }

    /**
     * Gets an array of ChildPromoProduct objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildPromoProduct[] List of ChildPromoProduct objects
     * @throws PropelException
     */
    public function getPromoProducts(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collPromoProductsPartial && !$this->isNew();
        if (null === $this->collPromoProducts || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collPromoProducts) {
                    $this->initPromoProducts();
                } else {
                    $collectionClassName = PromoProductTableMap::getTableMap()->getCollectionClassName();

                    $collPromoProducts = new $collectionClassName;
                    $collPromoProducts->setModel('\Model\PromoProduct');

                    return $collPromoProducts;
                }
            } else {
                $collPromoProducts = ChildPromoProductQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collPromoProductsPartial && count($collPromoProducts)) {
                        $this->initPromoProducts(false);

                        foreach ($collPromoProducts as $obj) {
                            if (false == $this->collPromoProducts->contains($obj)) {
                                $this->collPromoProducts->append($obj);
                            }
                        }

                        $this->collPromoProductsPartial = true;
                    }

                    return $collPromoProducts;
                }

                if ($partial && $this->collPromoProducts) {
                    foreach ($this->collPromoProducts as $obj) {
                        if ($obj->isNew()) {
                            $collPromoProducts[] = $obj;
                        }
                    }
                }

                $this->collPromoProducts = $collPromoProducts;
                $this->collPromoProductsPartial = false;
            }
        }

        return $this->collPromoProducts;
    }

    /**
     * Sets a collection of ChildPromoProduct objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $promoProducts A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setPromoProducts(Collection $promoProducts, ConnectionInterface $con = null)
    {
        /** @var ChildPromoProduct[] $promoProductsToDelete */
        $promoProductsToDelete = $this->getPromoProducts(new Criteria(), $con)->diff($promoProducts);


        $this->promoProductsScheduledForDeletion = $promoProductsToDelete;

        foreach ($promoProductsToDelete as $promoProductRemoved) {
            $promoProductRemoved->setProduct(null);
        }

        $this->collPromoProducts = null;
        foreach ($promoProducts as $promoProduct) {
            $this->addPromoProduct($promoProduct);
        }

        $this->collPromoProducts = $promoProducts;
        $this->collPromoProductsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related PromoProduct objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related PromoProduct objects.
     * @throws PropelException
     */
    public function countPromoProducts(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collPromoProductsPartial && !$this->isNew();
        if (null === $this->collPromoProducts || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collPromoProducts) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getPromoProducts());
            }

            $query = ChildPromoProductQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collPromoProducts);
    }

    /**
     * Method called to associate a ChildPromoProduct object to this object
     * through the ChildPromoProduct foreign key attribute.
     *
     * @param  ChildPromoProduct $l ChildPromoProduct
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addPromoProduct(ChildPromoProduct $l)
    {
        if ($this->collPromoProducts === null) {
            $this->initPromoProducts();
            $this->collPromoProductsPartial = true;
        }

        if (!$this->collPromoProducts->contains($l)) {
            $this->doAddPromoProduct($l);

            if ($this->promoProductsScheduledForDeletion and $this->promoProductsScheduledForDeletion->contains($l)) {
                $this->promoProductsScheduledForDeletion->remove($this->promoProductsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildPromoProduct $promoProduct The ChildPromoProduct object to add.
     */
    protected function doAddPromoProduct(ChildPromoProduct $promoProduct)
    {
        $this->collPromoProducts[]= $promoProduct;
        $promoProduct->setProduct($this);
    }

    /**
     * @param  ChildPromoProduct $promoProduct The ChildPromoProduct object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removePromoProduct(ChildPromoProduct $promoProduct)
    {
        if ($this->getPromoProducts()->contains($promoProduct)) {
            $pos = $this->collPromoProducts->search($promoProduct);
            $this->collPromoProducts->remove($pos);
            if (null === $this->promoProductsScheduledForDeletion) {
                $this->promoProductsScheduledForDeletion = clone $this->collPromoProducts;
                $this->promoProductsScheduledForDeletion->clear();
            }
            $this->promoProductsScheduledForDeletion[]= $promoProduct;
            $promoProduct->setProduct(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Product is new, it will return
     * an empty collection; or if this Product has previously
     * been saved, it will retrieve related PromoProducts from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Product.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildPromoProduct[] List of ChildPromoProduct objects
     */
    public function getPromoProductsJoinSite(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildPromoProductQuery::create(null, $criteria);
        $query->joinWith('Site', $joinBehavior);

        return $this->getPromoProducts($query, $con);
    }

    /**
     * Clears out the collProductProperties collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductProperties()
     */
    public function clearProductProperties()
    {
        $this->collProductProperties = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductProperties collection loaded partially.
     */
    public function resetPartialProductProperties($v = true)
    {
        $this->collProductPropertiesPartial = $v;
    }

    /**
     * Initializes the collProductProperties collection.
     *
     * By default this just sets the collProductProperties collection to an empty array (like clearcollProductProperties());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductProperties($overrideExisting = true)
    {
        if (null !== $this->collProductProperties && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductPropertyTableMap::getTableMap()->getCollectionClassName();

        $this->collProductProperties = new $collectionClassName;
        $this->collProductProperties->setModel('\Model\ProductProperty');
    }

    /**
     * Gets an array of ChildProductProperty objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProductProperty[] List of ChildProductProperty objects
     * @throws PropelException
     */
    public function getProductProperties(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductPropertiesPartial && !$this->isNew();
        if (null === $this->collProductProperties || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductProperties) {
                    $this->initProductProperties();
                } else {
                    $collectionClassName = ProductPropertyTableMap::getTableMap()->getCollectionClassName();

                    $collProductProperties = new $collectionClassName;
                    $collProductProperties->setModel('\Model\ProductProperty');

                    return $collProductProperties;
                }
            } else {
                $collProductProperties = ChildProductPropertyQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductPropertiesPartial && count($collProductProperties)) {
                        $this->initProductProperties(false);

                        foreach ($collProductProperties as $obj) {
                            if (false == $this->collProductProperties->contains($obj)) {
                                $this->collProductProperties->append($obj);
                            }
                        }

                        $this->collProductPropertiesPartial = true;
                    }

                    return $collProductProperties;
                }

                if ($partial && $this->collProductProperties) {
                    foreach ($this->collProductProperties as $obj) {
                        if ($obj->isNew()) {
                            $collProductProperties[] = $obj;
                        }
                    }
                }

                $this->collProductProperties = $collProductProperties;
                $this->collProductPropertiesPartial = false;
            }
        }

        return $this->collProductProperties;
    }

    /**
     * Sets a collection of ChildProductProperty objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productProperties A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setProductProperties(Collection $productProperties, ConnectionInterface $con = null)
    {
        /** @var ChildProductProperty[] $productPropertiesToDelete */
        $productPropertiesToDelete = $this->getProductProperties(new Criteria(), $con)->diff($productProperties);


        $this->productPropertiesScheduledForDeletion = $productPropertiesToDelete;

        foreach ($productPropertiesToDelete as $productPropertyRemoved) {
            $productPropertyRemoved->setProduct(null);
        }

        $this->collProductProperties = null;
        foreach ($productProperties as $productProperty) {
            $this->addProductProperty($productProperty);
        }

        $this->collProductProperties = $productProperties;
        $this->collProductPropertiesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProductProperty objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProductProperty objects.
     * @throws PropelException
     */
    public function countProductProperties(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductPropertiesPartial && !$this->isNew();
        if (null === $this->collProductProperties || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductProperties) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductProperties());
            }

            $query = ChildProductPropertyQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collProductProperties);
    }

    /**
     * Method called to associate a ChildProductProperty object to this object
     * through the ChildProductProperty foreign key attribute.
     *
     * @param  ChildProductProperty $l ChildProductProperty
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addProductProperty(ChildProductProperty $l)
    {
        if ($this->collProductProperties === null) {
            $this->initProductProperties();
            $this->collProductPropertiesPartial = true;
        }

        if (!$this->collProductProperties->contains($l)) {
            $this->doAddProductProperty($l);

            if ($this->productPropertiesScheduledForDeletion and $this->productPropertiesScheduledForDeletion->contains($l)) {
                $this->productPropertiesScheduledForDeletion->remove($this->productPropertiesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProductProperty $productProperty The ChildProductProperty object to add.
     */
    protected function doAddProductProperty(ChildProductProperty $productProperty)
    {
        $this->collProductProperties[]= $productProperty;
        $productProperty->setProduct($this);
    }

    /**
     * @param  ChildProductProperty $productProperty The ChildProductProperty object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeProductProperty(ChildProductProperty $productProperty)
    {
        if ($this->getProductProperties()->contains($productProperty)) {
            $pos = $this->collProductProperties->search($productProperty);
            $this->collProductProperties->remove($pos);
            if (null === $this->productPropertiesScheduledForDeletion) {
                $this->productPropertiesScheduledForDeletion = clone $this->collProductProperties;
                $this->productPropertiesScheduledForDeletion->clear();
            }
            $this->productPropertiesScheduledForDeletion[]= $productProperty;
            $productProperty->setProduct(null);
        }

        return $this;
    }

    /**
     * Clears out the collProductColors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductColors()
     */
    public function clearProductColors()
    {
        $this->collProductColors = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductColors collection loaded partially.
     */
    public function resetPartialProductColors($v = true)
    {
        $this->collProductColorsPartial = $v;
    }

    /**
     * Initializes the collProductColors collection.
     *
     * By default this just sets the collProductColors collection to an empty array (like clearcollProductColors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductColors($overrideExisting = true)
    {
        if (null !== $this->collProductColors && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductColorTableMap::getTableMap()->getCollectionClassName();

        $this->collProductColors = new $collectionClassName;
        $this->collProductColors->setModel('\Model\ProductColor');
    }

    /**
     * Gets an array of ChildProductColor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProductColor[] List of ChildProductColor objects
     * @throws PropelException
     */
    public function getProductColors(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductColorsPartial && !$this->isNew();
        if (null === $this->collProductColors || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductColors) {
                    $this->initProductColors();
                } else {
                    $collectionClassName = ProductColorTableMap::getTableMap()->getCollectionClassName();

                    $collProductColors = new $collectionClassName;
                    $collProductColors->setModel('\Model\ProductColor');

                    return $collProductColors;
                }
            } else {
                $collProductColors = ChildProductColorQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductColorsPartial && count($collProductColors)) {
                        $this->initProductColors(false);

                        foreach ($collProductColors as $obj) {
                            if (false == $this->collProductColors->contains($obj)) {
                                $this->collProductColors->append($obj);
                            }
                        }

                        $this->collProductColorsPartial = true;
                    }

                    return $collProductColors;
                }

                if ($partial && $this->collProductColors) {
                    foreach ($this->collProductColors as $obj) {
                        if ($obj->isNew()) {
                            $collProductColors[] = $obj;
                        }
                    }
                }

                $this->collProductColors = $collProductColors;
                $this->collProductColorsPartial = false;
            }
        }

        return $this->collProductColors;
    }

    /**
     * Sets a collection of ChildProductColor objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productColors A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setProductColors(Collection $productColors, ConnectionInterface $con = null)
    {
        /** @var ChildProductColor[] $productColorsToDelete */
        $productColorsToDelete = $this->getProductColors(new Criteria(), $con)->diff($productColors);


        $this->productColorsScheduledForDeletion = $productColorsToDelete;

        foreach ($productColorsToDelete as $productColorRemoved) {
            $productColorRemoved->setProduct(null);
        }

        $this->collProductColors = null;
        foreach ($productColors as $productColor) {
            $this->addProductColor($productColor);
        }

        $this->collProductColors = $productColors;
        $this->collProductColorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProductColor objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProductColor objects.
     * @throws PropelException
     */
    public function countProductColors(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductColorsPartial && !$this->isNew();
        if (null === $this->collProductColors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductColors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductColors());
            }

            $query = ChildProductColorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collProductColors);
    }

    /**
     * Method called to associate a ChildProductColor object to this object
     * through the ChildProductColor foreign key attribute.
     *
     * @param  ChildProductColor $l ChildProductColor
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addProductColor(ChildProductColor $l)
    {
        if ($this->collProductColors === null) {
            $this->initProductColors();
            $this->collProductColorsPartial = true;
        }

        if (!$this->collProductColors->contains($l)) {
            $this->doAddProductColor($l);

            if ($this->productColorsScheduledForDeletion and $this->productColorsScheduledForDeletion->contains($l)) {
                $this->productColorsScheduledForDeletion->remove($this->productColorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProductColor $productColor The ChildProductColor object to add.
     */
    protected function doAddProductColor(ChildProductColor $productColor)
    {
        $this->collProductColors[]= $productColor;
        $productColor->setProduct($this);
    }

    /**
     * @param  ChildProductColor $productColor The ChildProductColor object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeProductColor(ChildProductColor $productColor)
    {
        if ($this->getProductColors()->contains($productColor)) {
            $pos = $this->collProductColors->search($productColor);
            $this->collProductColors->remove($pos);
            if (null === $this->productColorsScheduledForDeletion) {
                $this->productColorsScheduledForDeletion = clone $this->collProductColors;
                $this->productColorsScheduledForDeletion->clear();
            }
            $this->productColorsScheduledForDeletion[]= clone $productColor;
            $productColor->setProduct(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Product is new, it will return
     * an empty collection; or if this Product has previously
     * been saved, it will retrieve related ProductColors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Product.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProductColor[] List of ChildProductColor objects
     */
    public function getProductColorsJoinColor(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProductColorQuery::create(null, $criteria);
        $query->joinWith('Color', $joinBehavior);

        return $this->getProductColors($query, $con);
    }

    /**
     * Clears out the collProductLists collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductLists()
     */
    public function clearProductLists()
    {
        $this->collProductLists = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductLists collection loaded partially.
     */
    public function resetPartialProductLists($v = true)
    {
        $this->collProductListsPartial = $v;
    }

    /**
     * Initializes the collProductLists collection.
     *
     * By default this just sets the collProductLists collection to an empty array (like clearcollProductLists());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductLists($overrideExisting = true)
    {
        if (null !== $this->collProductLists && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductListTableMap::getTableMap()->getCollectionClassName();

        $this->collProductLists = new $collectionClassName;
        $this->collProductLists->setModel('\Model\ProductList');
    }

    /**
     * Gets an array of ChildProductList objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProductList[] List of ChildProductList objects
     * @throws PropelException
     */
    public function getProductLists(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductListsPartial && !$this->isNew();
        if (null === $this->collProductLists || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductLists) {
                    $this->initProductLists();
                } else {
                    $collectionClassName = ProductListTableMap::getTableMap()->getCollectionClassName();

                    $collProductLists = new $collectionClassName;
                    $collProductLists->setModel('\Model\ProductList');

                    return $collProductLists;
                }
            } else {
                $collProductLists = ChildProductListQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductListsPartial && count($collProductLists)) {
                        $this->initProductLists(false);

                        foreach ($collProductLists as $obj) {
                            if (false == $this->collProductLists->contains($obj)) {
                                $this->collProductLists->append($obj);
                            }
                        }

                        $this->collProductListsPartial = true;
                    }

                    return $collProductLists;
                }

                if ($partial && $this->collProductLists) {
                    foreach ($this->collProductLists as $obj) {
                        if ($obj->isNew()) {
                            $collProductLists[] = $obj;
                        }
                    }
                }

                $this->collProductLists = $collProductLists;
                $this->collProductListsPartial = false;
            }
        }

        return $this->collProductLists;
    }

    /**
     * Sets a collection of ChildProductList objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productLists A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setProductLists(Collection $productLists, ConnectionInterface $con = null)
    {
        /** @var ChildProductList[] $productListsToDelete */
        $productListsToDelete = $this->getProductLists(new Criteria(), $con)->diff($productLists);


        $this->productListsScheduledForDeletion = $productListsToDelete;

        foreach ($productListsToDelete as $productListRemoved) {
            $productListRemoved->setProduct(null);
        }

        $this->collProductLists = null;
        foreach ($productLists as $productList) {
            $this->addProductList($productList);
        }

        $this->collProductLists = $productLists;
        $this->collProductListsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProductList objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProductList objects.
     * @throws PropelException
     */
    public function countProductLists(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductListsPartial && !$this->isNew();
        if (null === $this->collProductLists || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductLists) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductLists());
            }

            $query = ChildProductListQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collProductLists);
    }

    /**
     * Method called to associate a ChildProductList object to this object
     * through the ChildProductList foreign key attribute.
     *
     * @param  ChildProductList $l ChildProductList
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addProductList(ChildProductList $l)
    {
        if ($this->collProductLists === null) {
            $this->initProductLists();
            $this->collProductListsPartial = true;
        }

        if (!$this->collProductLists->contains($l)) {
            $this->doAddProductList($l);

            if ($this->productListsScheduledForDeletion and $this->productListsScheduledForDeletion->contains($l)) {
                $this->productListsScheduledForDeletion->remove($this->productListsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProductList $productList The ChildProductList object to add.
     */
    protected function doAddProductList(ChildProductList $productList)
    {
        $this->collProductLists[]= $productList;
        $productList->setProduct($this);
    }

    /**
     * @param  ChildProductList $productList The ChildProductList object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeProductList(ChildProductList $productList)
    {
        if ($this->getProductLists()->contains($productList)) {
            $pos = $this->collProductLists->search($productList);
            $this->collProductLists->remove($pos);
            if (null === $this->productListsScheduledForDeletion) {
                $this->productListsScheduledForDeletion = clone $this->collProductLists;
                $this->productListsScheduledForDeletion->clear();
            }
            $this->productListsScheduledForDeletion[]= clone $productList;
            $productList->setProduct(null);
        }

        return $this;
    }

    /**
     * Clears out the collProductUsages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductUsages()
     */
    public function clearProductUsages()
    {
        $this->collProductUsages = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductUsages collection loaded partially.
     */
    public function resetPartialProductUsages($v = true)
    {
        $this->collProductUsagesPartial = $v;
    }

    /**
     * Initializes the collProductUsages collection.
     *
     * By default this just sets the collProductUsages collection to an empty array (like clearcollProductUsages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductUsages($overrideExisting = true)
    {
        if (null !== $this->collProductUsages && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductUsageTableMap::getTableMap()->getCollectionClassName();

        $this->collProductUsages = new $collectionClassName;
        $this->collProductUsages->setModel('\Model\ProductUsage');
    }

    /**
     * Gets an array of ChildProductUsage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProductUsage[] List of ChildProductUsage objects
     * @throws PropelException
     */
    public function getProductUsages(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductUsagesPartial && !$this->isNew();
        if (null === $this->collProductUsages || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductUsages) {
                    $this->initProductUsages();
                } else {
                    $collectionClassName = ProductUsageTableMap::getTableMap()->getCollectionClassName();

                    $collProductUsages = new $collectionClassName;
                    $collProductUsages->setModel('\Model\ProductUsage');

                    return $collProductUsages;
                }
            } else {
                $collProductUsages = ChildProductUsageQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductUsagesPartial && count($collProductUsages)) {
                        $this->initProductUsages(false);

                        foreach ($collProductUsages as $obj) {
                            if (false == $this->collProductUsages->contains($obj)) {
                                $this->collProductUsages->append($obj);
                            }
                        }

                        $this->collProductUsagesPartial = true;
                    }

                    return $collProductUsages;
                }

                if ($partial && $this->collProductUsages) {
                    foreach ($this->collProductUsages as $obj) {
                        if ($obj->isNew()) {
                            $collProductUsages[] = $obj;
                        }
                    }
                }

                $this->collProductUsages = $collProductUsages;
                $this->collProductUsagesPartial = false;
            }
        }

        return $this->collProductUsages;
    }

    /**
     * Sets a collection of ChildProductUsage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productUsages A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setProductUsages(Collection $productUsages, ConnectionInterface $con = null)
    {
        /** @var ChildProductUsage[] $productUsagesToDelete */
        $productUsagesToDelete = $this->getProductUsages(new Criteria(), $con)->diff($productUsages);


        $this->productUsagesScheduledForDeletion = $productUsagesToDelete;

        foreach ($productUsagesToDelete as $productUsageRemoved) {
            $productUsageRemoved->setProduct(null);
        }

        $this->collProductUsages = null;
        foreach ($productUsages as $productUsage) {
            $this->addProductUsage($productUsage);
        }

        $this->collProductUsages = $productUsages;
        $this->collProductUsagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProductUsage objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProductUsage objects.
     * @throws PropelException
     */
    public function countProductUsages(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductUsagesPartial && !$this->isNew();
        if (null === $this->collProductUsages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductUsages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductUsages());
            }

            $query = ChildProductUsageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collProductUsages);
    }

    /**
     * Method called to associate a ChildProductUsage object to this object
     * through the ChildProductUsage foreign key attribute.
     *
     * @param  ChildProductUsage $l ChildProductUsage
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addProductUsage(ChildProductUsage $l)
    {
        if ($this->collProductUsages === null) {
            $this->initProductUsages();
            $this->collProductUsagesPartial = true;
        }

        if (!$this->collProductUsages->contains($l)) {
            $this->doAddProductUsage($l);

            if ($this->productUsagesScheduledForDeletion and $this->productUsagesScheduledForDeletion->contains($l)) {
                $this->productUsagesScheduledForDeletion->remove($this->productUsagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProductUsage $productUsage The ChildProductUsage object to add.
     */
    protected function doAddProductUsage(ChildProductUsage $productUsage)
    {
        $this->collProductUsages[]= $productUsage;
        $productUsage->setProduct($this);
    }

    /**
     * @param  ChildProductUsage $productUsage The ChildProductUsage object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeProductUsage(ChildProductUsage $productUsage)
    {
        if ($this->getProductUsages()->contains($productUsage)) {
            $pos = $this->collProductUsages->search($productUsage);
            $this->collProductUsages->remove($pos);
            if (null === $this->productUsagesScheduledForDeletion) {
                $this->productUsagesScheduledForDeletion = clone $this->collProductUsages;
                $this->productUsagesScheduledForDeletion->clear();
            }
            $this->productUsagesScheduledForDeletion[]= clone $productUsage;
            $productUsage->setProduct(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Product is new, it will return
     * an empty collection; or if this Product has previously
     * been saved, it will retrieve related ProductUsages from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Product.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProductUsage[] List of ChildProductUsage objects
     */
    public function getProductUsagesJoinUsage(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProductUsageQuery::create(null, $criteria);
        $query->joinWith('Usage', $joinBehavior);

        return $this->getProductUsages($query, $con);
    }

    /**
     * Clears out the collProductTranslations collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductTranslations()
     */
    public function clearProductTranslations()
    {
        $this->collProductTranslations = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductTranslations collection loaded partially.
     */
    public function resetPartialProductTranslations($v = true)
    {
        $this->collProductTranslationsPartial = $v;
    }

    /**
     * Initializes the collProductTranslations collection.
     *
     * By default this just sets the collProductTranslations collection to an empty array (like clearcollProductTranslations());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductTranslations($overrideExisting = true)
    {
        if (null !== $this->collProductTranslations && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collProductTranslations = new $collectionClassName;
        $this->collProductTranslations->setModel('\Model\ProductTranslation');
    }

    /**
     * Gets an array of ChildProductTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProductTranslation[] List of ChildProductTranslation objects
     * @throws PropelException
     */
    public function getProductTranslations(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductTranslationsPartial && !$this->isNew();
        if (null === $this->collProductTranslations || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductTranslations) {
                    $this->initProductTranslations();
                } else {
                    $collectionClassName = ProductTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collProductTranslations = new $collectionClassName;
                    $collProductTranslations->setModel('\Model\ProductTranslation');

                    return $collProductTranslations;
                }
            } else {
                $collProductTranslations = ChildProductTranslationQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductTranslationsPartial && count($collProductTranslations)) {
                        $this->initProductTranslations(false);

                        foreach ($collProductTranslations as $obj) {
                            if (false == $this->collProductTranslations->contains($obj)) {
                                $this->collProductTranslations->append($obj);
                            }
                        }

                        $this->collProductTranslationsPartial = true;
                    }

                    return $collProductTranslations;
                }

                if ($partial && $this->collProductTranslations) {
                    foreach ($this->collProductTranslations as $obj) {
                        if ($obj->isNew()) {
                            $collProductTranslations[] = $obj;
                        }
                    }
                }

                $this->collProductTranslations = $collProductTranslations;
                $this->collProductTranslationsPartial = false;
            }
        }

        return $this->collProductTranslations;
    }

    /**
     * Sets a collection of ChildProductTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productTranslations A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setProductTranslations(Collection $productTranslations, ConnectionInterface $con = null)
    {
        /** @var ChildProductTranslation[] $productTranslationsToDelete */
        $productTranslationsToDelete = $this->getProductTranslations(new Criteria(), $con)->diff($productTranslations);


        $this->productTranslationsScheduledForDeletion = $productTranslationsToDelete;

        foreach ($productTranslationsToDelete as $productTranslationRemoved) {
            $productTranslationRemoved->setProduct(null);
        }

        $this->collProductTranslations = null;
        foreach ($productTranslations as $productTranslation) {
            $this->addProductTranslation($productTranslation);
        }

        $this->collProductTranslations = $productTranslations;
        $this->collProductTranslationsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProductTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProductTranslation objects.
     * @throws PropelException
     */
    public function countProductTranslations(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductTranslationsPartial && !$this->isNew();
        if (null === $this->collProductTranslations || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductTranslations) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductTranslations());
            }

            $query = ChildProductTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collProductTranslations);
    }

    /**
     * Method called to associate a ChildProductTranslation object to this object
     * through the ChildProductTranslation foreign key attribute.
     *
     * @param  ChildProductTranslation $l ChildProductTranslation
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addProductTranslation(ChildProductTranslation $l)
    {
        if ($this->collProductTranslations === null) {
            $this->initProductTranslations();
            $this->collProductTranslationsPartial = true;
        }

        if (!$this->collProductTranslations->contains($l)) {
            $this->doAddProductTranslation($l);

            if ($this->productTranslationsScheduledForDeletion and $this->productTranslationsScheduledForDeletion->contains($l)) {
                $this->productTranslationsScheduledForDeletion->remove($this->productTranslationsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProductTranslation $productTranslation The ChildProductTranslation object to add.
     */
    protected function doAddProductTranslation(ChildProductTranslation $productTranslation)
    {
        $this->collProductTranslations[]= $productTranslation;
        $productTranslation->setProduct($this);
    }

    /**
     * @param  ChildProductTranslation $productTranslation The ChildProductTranslation object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeProductTranslation(ChildProductTranslation $productTranslation)
    {
        if ($this->getProductTranslations()->contains($productTranslation)) {
            $pos = $this->collProductTranslations->search($productTranslation);
            $this->collProductTranslations->remove($pos);
            if (null === $this->productTranslationsScheduledForDeletion) {
                $this->productTranslationsScheduledForDeletion = clone $this->collProductTranslations;
                $this->productTranslationsScheduledForDeletion->clear();
            }
            $this->productTranslationsScheduledForDeletion[]= clone $productTranslation;
            $productTranslation->setProduct(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Product is new, it will return
     * an empty collection; or if this Product has previously
     * been saved, it will retrieve related ProductTranslations from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Product.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProductTranslation[] List of ChildProductTranslation objects
     */
    public function getProductTranslationsJoinLanguage(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProductTranslationQuery::create(null, $criteria);
        $query->joinWith('Language', $joinBehavior);

        return $this->getProductTranslations($query, $con);
    }

    /**
     * Clears out the collRelatedProductsRelatedByProductId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addRelatedProductsRelatedByProductId()
     */
    public function clearRelatedProductsRelatedByProductId()
    {
        $this->collRelatedProductsRelatedByProductId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collRelatedProductsRelatedByProductId collection loaded partially.
     */
    public function resetPartialRelatedProductsRelatedByProductId($v = true)
    {
        $this->collRelatedProductsRelatedByProductIdPartial = $v;
    }

    /**
     * Initializes the collRelatedProductsRelatedByProductId collection.
     *
     * By default this just sets the collRelatedProductsRelatedByProductId collection to an empty array (like clearcollRelatedProductsRelatedByProductId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRelatedProductsRelatedByProductId($overrideExisting = true)
    {
        if (null !== $this->collRelatedProductsRelatedByProductId && !$overrideExisting) {
            return;
        }

        $collectionClassName = RelatedProductTableMap::getTableMap()->getCollectionClassName();

        $this->collRelatedProductsRelatedByProductId = new $collectionClassName;
        $this->collRelatedProductsRelatedByProductId->setModel('\Model\Product\RelatedProduct');
    }

    /**
     * Gets an array of RelatedProduct objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|RelatedProduct[] List of RelatedProduct objects
     * @throws PropelException
     */
    public function getRelatedProductsRelatedByProductId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collRelatedProductsRelatedByProductIdPartial && !$this->isNew();
        if (null === $this->collRelatedProductsRelatedByProductId || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collRelatedProductsRelatedByProductId) {
                    $this->initRelatedProductsRelatedByProductId();
                } else {
                    $collectionClassName = RelatedProductTableMap::getTableMap()->getCollectionClassName();

                    $collRelatedProductsRelatedByProductId = new $collectionClassName;
                    $collRelatedProductsRelatedByProductId->setModel('\Model\Product\RelatedProduct');

                    return $collRelatedProductsRelatedByProductId;
                }
            } else {
                $collRelatedProductsRelatedByProductId = RelatedProductQuery::create(null, $criteria)
                    ->filterBySourceProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collRelatedProductsRelatedByProductIdPartial && count($collRelatedProductsRelatedByProductId)) {
                        $this->initRelatedProductsRelatedByProductId(false);

                        foreach ($collRelatedProductsRelatedByProductId as $obj) {
                            if (false == $this->collRelatedProductsRelatedByProductId->contains($obj)) {
                                $this->collRelatedProductsRelatedByProductId->append($obj);
                            }
                        }

                        $this->collRelatedProductsRelatedByProductIdPartial = true;
                    }

                    return $collRelatedProductsRelatedByProductId;
                }

                if ($partial && $this->collRelatedProductsRelatedByProductId) {
                    foreach ($this->collRelatedProductsRelatedByProductId as $obj) {
                        if ($obj->isNew()) {
                            $collRelatedProductsRelatedByProductId[] = $obj;
                        }
                    }
                }

                $this->collRelatedProductsRelatedByProductId = $collRelatedProductsRelatedByProductId;
                $this->collRelatedProductsRelatedByProductIdPartial = false;
            }
        }

        return $this->collRelatedProductsRelatedByProductId;
    }

    /**
     * Sets a collection of RelatedProduct objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $relatedProductsRelatedByProductId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setRelatedProductsRelatedByProductId(Collection $relatedProductsRelatedByProductId, ConnectionInterface $con = null)
    {
        /** @var RelatedProduct[] $relatedProductsRelatedByProductIdToDelete */
        $relatedProductsRelatedByProductIdToDelete = $this->getRelatedProductsRelatedByProductId(new Criteria(), $con)->diff($relatedProductsRelatedByProductId);


        $this->relatedProductsRelatedByProductIdScheduledForDeletion = $relatedProductsRelatedByProductIdToDelete;

        foreach ($relatedProductsRelatedByProductIdToDelete as $relatedProductRelatedByProductIdRemoved) {
            $relatedProductRelatedByProductIdRemoved->setSourceProduct(null);
        }

        $this->collRelatedProductsRelatedByProductId = null;
        foreach ($relatedProductsRelatedByProductId as $relatedProductRelatedByProductId) {
            $this->addRelatedProductRelatedByProductId($relatedProductRelatedByProductId);
        }

        $this->collRelatedProductsRelatedByProductId = $relatedProductsRelatedByProductId;
        $this->collRelatedProductsRelatedByProductIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseRelatedProduct objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseRelatedProduct objects.
     * @throws PropelException
     */
    public function countRelatedProductsRelatedByProductId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collRelatedProductsRelatedByProductIdPartial && !$this->isNew();
        if (null === $this->collRelatedProductsRelatedByProductId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRelatedProductsRelatedByProductId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRelatedProductsRelatedByProductId());
            }

            $query = RelatedProductQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterBySourceProduct($this)
                ->count($con);
        }

        return count($this->collRelatedProductsRelatedByProductId);
    }

    /**
     * Method called to associate a RelatedProduct object to this object
     * through the RelatedProduct foreign key attribute.
     *
     * @param  RelatedProduct $l RelatedProduct
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addRelatedProductRelatedByProductId(RelatedProduct $l)
    {
        if ($this->collRelatedProductsRelatedByProductId === null) {
            $this->initRelatedProductsRelatedByProductId();
            $this->collRelatedProductsRelatedByProductIdPartial = true;
        }

        if (!$this->collRelatedProductsRelatedByProductId->contains($l)) {
            $this->doAddRelatedProductRelatedByProductId($l);

            if ($this->relatedProductsRelatedByProductIdScheduledForDeletion and $this->relatedProductsRelatedByProductIdScheduledForDeletion->contains($l)) {
                $this->relatedProductsRelatedByProductIdScheduledForDeletion->remove($this->relatedProductsRelatedByProductIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param RelatedProduct $relatedProductRelatedByProductId The RelatedProduct object to add.
     */
    protected function doAddRelatedProductRelatedByProductId(RelatedProduct $relatedProductRelatedByProductId)
    {
        $this->collRelatedProductsRelatedByProductId[]= $relatedProductRelatedByProductId;
        $relatedProductRelatedByProductId->setSourceProduct($this);
    }

    /**
     * @param  RelatedProduct $relatedProductRelatedByProductId The RelatedProduct object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeRelatedProductRelatedByProductId(RelatedProduct $relatedProductRelatedByProductId)
    {
        if ($this->getRelatedProductsRelatedByProductId()->contains($relatedProductRelatedByProductId)) {
            $pos = $this->collRelatedProductsRelatedByProductId->search($relatedProductRelatedByProductId);
            $this->collRelatedProductsRelatedByProductId->remove($pos);
            if (null === $this->relatedProductsRelatedByProductIdScheduledForDeletion) {
                $this->relatedProductsRelatedByProductIdScheduledForDeletion = clone $this->collRelatedProductsRelatedByProductId;
                $this->relatedProductsRelatedByProductIdScheduledForDeletion->clear();
            }
            $this->relatedProductsRelatedByProductIdScheduledForDeletion[]= clone $relatedProductRelatedByProductId;
            $relatedProductRelatedByProductId->setSourceProduct(null);
        }

        return $this;
    }

    /**
     * Clears out the collRelatedProductsRelatedByOtherProductId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addRelatedProductsRelatedByOtherProductId()
     */
    public function clearRelatedProductsRelatedByOtherProductId()
    {
        $this->collRelatedProductsRelatedByOtherProductId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collRelatedProductsRelatedByOtherProductId collection loaded partially.
     */
    public function resetPartialRelatedProductsRelatedByOtherProductId($v = true)
    {
        $this->collRelatedProductsRelatedByOtherProductIdPartial = $v;
    }

    /**
     * Initializes the collRelatedProductsRelatedByOtherProductId collection.
     *
     * By default this just sets the collRelatedProductsRelatedByOtherProductId collection to an empty array (like clearcollRelatedProductsRelatedByOtherProductId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRelatedProductsRelatedByOtherProductId($overrideExisting = true)
    {
        if (null !== $this->collRelatedProductsRelatedByOtherProductId && !$overrideExisting) {
            return;
        }

        $collectionClassName = RelatedProductTableMap::getTableMap()->getCollectionClassName();

        $this->collRelatedProductsRelatedByOtherProductId = new $collectionClassName;
        $this->collRelatedProductsRelatedByOtherProductId->setModel('\Model\Product\RelatedProduct');
    }

    /**
     * Gets an array of RelatedProduct objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|RelatedProduct[] List of RelatedProduct objects
     * @throws PropelException
     */
    public function getRelatedProductsRelatedByOtherProductId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collRelatedProductsRelatedByOtherProductIdPartial && !$this->isNew();
        if (null === $this->collRelatedProductsRelatedByOtherProductId || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collRelatedProductsRelatedByOtherProductId) {
                    $this->initRelatedProductsRelatedByOtherProductId();
                } else {
                    $collectionClassName = RelatedProductTableMap::getTableMap()->getCollectionClassName();

                    $collRelatedProductsRelatedByOtherProductId = new $collectionClassName;
                    $collRelatedProductsRelatedByOtherProductId->setModel('\Model\Product\RelatedProduct');

                    return $collRelatedProductsRelatedByOtherProductId;
                }
            } else {
                $collRelatedProductsRelatedByOtherProductId = RelatedProductQuery::create(null, $criteria)
                    ->filterByDestProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collRelatedProductsRelatedByOtherProductIdPartial && count($collRelatedProductsRelatedByOtherProductId)) {
                        $this->initRelatedProductsRelatedByOtherProductId(false);

                        foreach ($collRelatedProductsRelatedByOtherProductId as $obj) {
                            if (false == $this->collRelatedProductsRelatedByOtherProductId->contains($obj)) {
                                $this->collRelatedProductsRelatedByOtherProductId->append($obj);
                            }
                        }

                        $this->collRelatedProductsRelatedByOtherProductIdPartial = true;
                    }

                    return $collRelatedProductsRelatedByOtherProductId;
                }

                if ($partial && $this->collRelatedProductsRelatedByOtherProductId) {
                    foreach ($this->collRelatedProductsRelatedByOtherProductId as $obj) {
                        if ($obj->isNew()) {
                            $collRelatedProductsRelatedByOtherProductId[] = $obj;
                        }
                    }
                }

                $this->collRelatedProductsRelatedByOtherProductId = $collRelatedProductsRelatedByOtherProductId;
                $this->collRelatedProductsRelatedByOtherProductIdPartial = false;
            }
        }

        return $this->collRelatedProductsRelatedByOtherProductId;
    }

    /**
     * Sets a collection of RelatedProduct objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $relatedProductsRelatedByOtherProductId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setRelatedProductsRelatedByOtherProductId(Collection $relatedProductsRelatedByOtherProductId, ConnectionInterface $con = null)
    {
        /** @var RelatedProduct[] $relatedProductsRelatedByOtherProductIdToDelete */
        $relatedProductsRelatedByOtherProductIdToDelete = $this->getRelatedProductsRelatedByOtherProductId(new Criteria(), $con)->diff($relatedProductsRelatedByOtherProductId);


        $this->relatedProductsRelatedByOtherProductIdScheduledForDeletion = $relatedProductsRelatedByOtherProductIdToDelete;

        foreach ($relatedProductsRelatedByOtherProductIdToDelete as $relatedProductRelatedByOtherProductIdRemoved) {
            $relatedProductRelatedByOtherProductIdRemoved->setDestProduct(null);
        }

        $this->collRelatedProductsRelatedByOtherProductId = null;
        foreach ($relatedProductsRelatedByOtherProductId as $relatedProductRelatedByOtherProductId) {
            $this->addRelatedProductRelatedByOtherProductId($relatedProductRelatedByOtherProductId);
        }

        $this->collRelatedProductsRelatedByOtherProductId = $relatedProductsRelatedByOtherProductId;
        $this->collRelatedProductsRelatedByOtherProductIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseRelatedProduct objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseRelatedProduct objects.
     * @throws PropelException
     */
    public function countRelatedProductsRelatedByOtherProductId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collRelatedProductsRelatedByOtherProductIdPartial && !$this->isNew();
        if (null === $this->collRelatedProductsRelatedByOtherProductId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRelatedProductsRelatedByOtherProductId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRelatedProductsRelatedByOtherProductId());
            }

            $query = RelatedProductQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDestProduct($this)
                ->count($con);
        }

        return count($this->collRelatedProductsRelatedByOtherProductId);
    }

    /**
     * Method called to associate a RelatedProduct object to this object
     * through the RelatedProduct foreign key attribute.
     *
     * @param  RelatedProduct $l RelatedProduct
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addRelatedProductRelatedByOtherProductId(RelatedProduct $l)
    {
        if ($this->collRelatedProductsRelatedByOtherProductId === null) {
            $this->initRelatedProductsRelatedByOtherProductId();
            $this->collRelatedProductsRelatedByOtherProductIdPartial = true;
        }

        if (!$this->collRelatedProductsRelatedByOtherProductId->contains($l)) {
            $this->doAddRelatedProductRelatedByOtherProductId($l);

            if ($this->relatedProductsRelatedByOtherProductIdScheduledForDeletion and $this->relatedProductsRelatedByOtherProductIdScheduledForDeletion->contains($l)) {
                $this->relatedProductsRelatedByOtherProductIdScheduledForDeletion->remove($this->relatedProductsRelatedByOtherProductIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param RelatedProduct $relatedProductRelatedByOtherProductId The RelatedProduct object to add.
     */
    protected function doAddRelatedProductRelatedByOtherProductId(RelatedProduct $relatedProductRelatedByOtherProductId)
    {
        $this->collRelatedProductsRelatedByOtherProductId[]= $relatedProductRelatedByOtherProductId;
        $relatedProductRelatedByOtherProductId->setDestProduct($this);
    }

    /**
     * @param  RelatedProduct $relatedProductRelatedByOtherProductId The RelatedProduct object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeRelatedProductRelatedByOtherProductId(RelatedProduct $relatedProductRelatedByOtherProductId)
    {
        if ($this->getRelatedProductsRelatedByOtherProductId()->contains($relatedProductRelatedByOtherProductId)) {
            $pos = $this->collRelatedProductsRelatedByOtherProductId->search($relatedProductRelatedByOtherProductId);
            $this->collRelatedProductsRelatedByOtherProductId->remove($pos);
            if (null === $this->relatedProductsRelatedByOtherProductIdScheduledForDeletion) {
                $this->relatedProductsRelatedByOtherProductIdScheduledForDeletion = clone $this->collRelatedProductsRelatedByOtherProductId;
                $this->relatedProductsRelatedByOtherProductIdScheduledForDeletion->clear();
            }
            $this->relatedProductsRelatedByOtherProductIdScheduledForDeletion[]= clone $relatedProductRelatedByOtherProductId;
            $relatedProductRelatedByOtherProductId->setDestProduct(null);
        }

        return $this;
    }

    /**
     * Clears out the collProductImages collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductImages()
     */
    public function clearProductImages()
    {
        $this->collProductImages = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductImages collection loaded partially.
     */
    public function resetPartialProductImages($v = true)
    {
        $this->collProductImagesPartial = $v;
    }

    /**
     * Initializes the collProductImages collection.
     *
     * By default this just sets the collProductImages collection to an empty array (like clearcollProductImages());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductImages($overrideExisting = true)
    {
        if (null !== $this->collProductImages && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductImageTableMap::getTableMap()->getCollectionClassName();

        $this->collProductImages = new $collectionClassName;
        $this->collProductImages->setModel('\Model\Product\Image\ProductImage');
    }

    /**
     * Gets an array of ProductImage objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ProductImage[] List of ProductImage objects
     * @throws PropelException
     */
    public function getProductImages(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductImagesPartial && !$this->isNew();
        if (null === $this->collProductImages || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductImages) {
                    $this->initProductImages();
                } else {
                    $collectionClassName = ProductImageTableMap::getTableMap()->getCollectionClassName();

                    $collProductImages = new $collectionClassName;
                    $collProductImages->setModel('\Model\Product\Image\ProductImage');

                    return $collProductImages;
                }
            } else {
                $collProductImages = ProductImageQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductImagesPartial && count($collProductImages)) {
                        $this->initProductImages(false);

                        foreach ($collProductImages as $obj) {
                            if (false == $this->collProductImages->contains($obj)) {
                                $this->collProductImages->append($obj);
                            }
                        }

                        $this->collProductImagesPartial = true;
                    }

                    return $collProductImages;
                }

                if ($partial && $this->collProductImages) {
                    foreach ($this->collProductImages as $obj) {
                        if ($obj->isNew()) {
                            $collProductImages[] = $obj;
                        }
                    }
                }

                $this->collProductImages = $collProductImages;
                $this->collProductImagesPartial = false;
            }
        }

        return $this->collProductImages;
    }

    /**
     * Sets a collection of ProductImage objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productImages A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setProductImages(Collection $productImages, ConnectionInterface $con = null)
    {
        /** @var ProductImage[] $productImagesToDelete */
        $productImagesToDelete = $this->getProductImages(new Criteria(), $con)->diff($productImages);


        $this->productImagesScheduledForDeletion = $productImagesToDelete;

        foreach ($productImagesToDelete as $productImageRemoved) {
            $productImageRemoved->setProduct(null);
        }

        $this->collProductImages = null;
        foreach ($productImages as $productImage) {
            $this->addProductImage($productImage);
        }

        $this->collProductImages = $productImages;
        $this->collProductImagesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseProductImage objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseProductImage objects.
     * @throws PropelException
     */
    public function countProductImages(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductImagesPartial && !$this->isNew();
        if (null === $this->collProductImages || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductImages) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductImages());
            }

            $query = ProductImageQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collProductImages);
    }

    /**
     * Method called to associate a ProductImage object to this object
     * through the ProductImage foreign key attribute.
     *
     * @param  ProductImage $l ProductImage
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addProductImage(ProductImage $l)
    {
        if ($this->collProductImages === null) {
            $this->initProductImages();
            $this->collProductImagesPartial = true;
        }

        if (!$this->collProductImages->contains($l)) {
            $this->doAddProductImage($l);

            if ($this->productImagesScheduledForDeletion and $this->productImagesScheduledForDeletion->contains($l)) {
                $this->productImagesScheduledForDeletion->remove($this->productImagesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ProductImage $productImage The ProductImage object to add.
     */
    protected function doAddProductImage(ProductImage $productImage)
    {
        $this->collProductImages[]= $productImage;
        $productImage->setProduct($this);
    }

    /**
     * @param  ProductImage $productImage The ProductImage object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeProductImage(ProductImage $productImage)
    {
        if ($this->getProductImages()->contains($productImage)) {
            $pos = $this->collProductImages->search($productImage);
            $this->collProductImages->remove($pos);
            if (null === $this->productImagesScheduledForDeletion) {
                $this->productImagesScheduledForDeletion = clone $this->collProductImages;
                $this->productImagesScheduledForDeletion->clear();
            }
            $this->productImagesScheduledForDeletion[]= $productImage;
            $productImage->setProduct(null);
        }

        return $this;
    }

    /**
     * Clears out the collProductTags collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProductTags()
     */
    public function clearProductTags()
    {
        $this->collProductTags = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProductTags collection loaded partially.
     */
    public function resetPartialProductTags($v = true)
    {
        $this->collProductTagsPartial = $v;
    }

    /**
     * Initializes the collProductTags collection.
     *
     * By default this just sets the collProductTags collection to an empty array (like clearcollProductTags());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProductTags($overrideExisting = true)
    {
        if (null !== $this->collProductTags && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProductTagTableMap::getTableMap()->getCollectionClassName();

        $this->collProductTags = new $collectionClassName;
        $this->collProductTags->setModel('\Model\Product\ProductTag');
    }

    /**
     * Gets an array of ProductTag objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ProductTag[] List of ProductTag objects
     * @throws PropelException
     */
    public function getProductTags(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProductTagsPartial && !$this->isNew();
        if (null === $this->collProductTags || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collProductTags) {
                    $this->initProductTags();
                } else {
                    $collectionClassName = ProductTagTableMap::getTableMap()->getCollectionClassName();

                    $collProductTags = new $collectionClassName;
                    $collProductTags->setModel('\Model\Product\ProductTag');

                    return $collProductTags;
                }
            } else {
                $collProductTags = ProductTagQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProductTagsPartial && count($collProductTags)) {
                        $this->initProductTags(false);

                        foreach ($collProductTags as $obj) {
                            if (false == $this->collProductTags->contains($obj)) {
                                $this->collProductTags->append($obj);
                            }
                        }

                        $this->collProductTagsPartial = true;
                    }

                    return $collProductTags;
                }

                if ($partial && $this->collProductTags) {
                    foreach ($this->collProductTags as $obj) {
                        if ($obj->isNew()) {
                            $collProductTags[] = $obj;
                        }
                    }
                }

                $this->collProductTags = $collProductTags;
                $this->collProductTagsPartial = false;
            }
        }

        return $this->collProductTags;
    }

    /**
     * Sets a collection of ProductTag objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $productTags A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setProductTags(Collection $productTags, ConnectionInterface $con = null)
    {
        /** @var ProductTag[] $productTagsToDelete */
        $productTagsToDelete = $this->getProductTags(new Criteria(), $con)->diff($productTags);


        $this->productTagsScheduledForDeletion = $productTagsToDelete;

        foreach ($productTagsToDelete as $productTagRemoved) {
            $productTagRemoved->setProduct(null);
        }

        $this->collProductTags = null;
        foreach ($productTags as $productTag) {
            $this->addProductTag($productTag);
        }

        $this->collProductTags = $productTags;
        $this->collProductTagsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseProductTag objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseProductTag objects.
     * @throws PropelException
     */
    public function countProductTags(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProductTagsPartial && !$this->isNew();
        if (null === $this->collProductTags || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProductTags) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProductTags());
            }

            $query = ProductTagQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collProductTags);
    }

    /**
     * Method called to associate a ProductTag object to this object
     * through the ProductTag foreign key attribute.
     *
     * @param  ProductTag $l ProductTag
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addProductTag(ProductTag $l)
    {
        if ($this->collProductTags === null) {
            $this->initProductTags();
            $this->collProductTagsPartial = true;
        }

        if (!$this->collProductTags->contains($l)) {
            $this->doAddProductTag($l);

            if ($this->productTagsScheduledForDeletion and $this->productTagsScheduledForDeletion->contains($l)) {
                $this->productTagsScheduledForDeletion->remove($this->productTagsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ProductTag $productTag The ProductTag object to add.
     */
    protected function doAddProductTag(ProductTag $productTag)
    {
        $this->collProductTags[]= $productTag;
        $productTag->setProduct($this);
    }

    /**
     * @param  ProductTag $productTag The ProductTag object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeProductTag(ProductTag $productTag)
    {
        if ($this->getProductTags()->contains($productTag)) {
            $pos = $this->collProductTags->search($productTag);
            $this->collProductTags->remove($pos);
            if (null === $this->productTagsScheduledForDeletion) {
                $this->productTagsScheduledForDeletion = clone $this->collProductTags;
                $this->productTagsScheduledForDeletion->clear();
            }
            $this->productTagsScheduledForDeletion[]= clone $productTag;
            $productTag->setProduct(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Product is new, it will return
     * an empty collection; or if this Product has previously
     * been saved, it will retrieve related ProductTags from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Product.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ProductTag[] List of ProductTag objects
     */
    public function getProductTagsJoinTag(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ProductTagQuery::create(null, $criteria);
        $query->joinWith('Tag', $joinBehavior);

        return $this->getProductTags($query, $con);
    }

    /**
     * Clears out the collSaleOrderItemStockClaimeds collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addSaleOrderItemStockClaimeds()
     */
    public function clearSaleOrderItemStockClaimeds()
    {
        $this->collSaleOrderItemStockClaimeds = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collSaleOrderItemStockClaimeds collection loaded partially.
     */
    public function resetPartialSaleOrderItemStockClaimeds($v = true)
    {
        $this->collSaleOrderItemStockClaimedsPartial = $v;
    }

    /**
     * Initializes the collSaleOrderItemStockClaimeds collection.
     *
     * By default this just sets the collSaleOrderItemStockClaimeds collection to an empty array (like clearcollSaleOrderItemStockClaimeds());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initSaleOrderItemStockClaimeds($overrideExisting = true)
    {
        if (null !== $this->collSaleOrderItemStockClaimeds && !$overrideExisting) {
            return;
        }

        $collectionClassName = SaleOrderItemStockClaimedTableMap::getTableMap()->getCollectionClassName();

        $this->collSaleOrderItemStockClaimeds = new $collectionClassName;
        $this->collSaleOrderItemStockClaimeds->setModel('\Model\Sale\SaleOrderItemStockClaimed');
    }

    /**
     * Gets an array of SaleOrderItemStockClaimed objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|SaleOrderItemStockClaimed[] List of SaleOrderItemStockClaimed objects
     * @throws PropelException
     */
    public function getSaleOrderItemStockClaimeds(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderItemStockClaimedsPartial && !$this->isNew();
        if (null === $this->collSaleOrderItemStockClaimeds || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collSaleOrderItemStockClaimeds) {
                    $this->initSaleOrderItemStockClaimeds();
                } else {
                    $collectionClassName = SaleOrderItemStockClaimedTableMap::getTableMap()->getCollectionClassName();

                    $collSaleOrderItemStockClaimeds = new $collectionClassName;
                    $collSaleOrderItemStockClaimeds->setModel('\Model\Sale\SaleOrderItemStockClaimed');

                    return $collSaleOrderItemStockClaimeds;
                }
            } else {
                $collSaleOrderItemStockClaimeds = SaleOrderItemStockClaimedQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collSaleOrderItemStockClaimedsPartial && count($collSaleOrderItemStockClaimeds)) {
                        $this->initSaleOrderItemStockClaimeds(false);

                        foreach ($collSaleOrderItemStockClaimeds as $obj) {
                            if (false == $this->collSaleOrderItemStockClaimeds->contains($obj)) {
                                $this->collSaleOrderItemStockClaimeds->append($obj);
                            }
                        }

                        $this->collSaleOrderItemStockClaimedsPartial = true;
                    }

                    return $collSaleOrderItemStockClaimeds;
                }

                if ($partial && $this->collSaleOrderItemStockClaimeds) {
                    foreach ($this->collSaleOrderItemStockClaimeds as $obj) {
                        if ($obj->isNew()) {
                            $collSaleOrderItemStockClaimeds[] = $obj;
                        }
                    }
                }

                $this->collSaleOrderItemStockClaimeds = $collSaleOrderItemStockClaimeds;
                $this->collSaleOrderItemStockClaimedsPartial = false;
            }
        }

        return $this->collSaleOrderItemStockClaimeds;
    }

    /**
     * Sets a collection of SaleOrderItemStockClaimed objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $saleOrderItemStockClaimeds A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setSaleOrderItemStockClaimeds(Collection $saleOrderItemStockClaimeds, ConnectionInterface $con = null)
    {
        /** @var SaleOrderItemStockClaimed[] $saleOrderItemStockClaimedsToDelete */
        $saleOrderItemStockClaimedsToDelete = $this->getSaleOrderItemStockClaimeds(new Criteria(), $con)->diff($saleOrderItemStockClaimeds);


        $this->saleOrderItemStockClaimedsScheduledForDeletion = $saleOrderItemStockClaimedsToDelete;

        foreach ($saleOrderItemStockClaimedsToDelete as $saleOrderItemStockClaimedRemoved) {
            $saleOrderItemStockClaimedRemoved->setProduct(null);
        }

        $this->collSaleOrderItemStockClaimeds = null;
        foreach ($saleOrderItemStockClaimeds as $saleOrderItemStockClaimed) {
            $this->addSaleOrderItemStockClaimed($saleOrderItemStockClaimed);
        }

        $this->collSaleOrderItemStockClaimeds = $saleOrderItemStockClaimeds;
        $this->collSaleOrderItemStockClaimedsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseSaleOrderItemStockClaimed objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseSaleOrderItemStockClaimed objects.
     * @throws PropelException
     */
    public function countSaleOrderItemStockClaimeds(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collSaleOrderItemStockClaimedsPartial && !$this->isNew();
        if (null === $this->collSaleOrderItemStockClaimeds || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collSaleOrderItemStockClaimeds) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getSaleOrderItemStockClaimeds());
            }

            $query = SaleOrderItemStockClaimedQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collSaleOrderItemStockClaimeds);
    }

    /**
     * Method called to associate a SaleOrderItemStockClaimed object to this object
     * through the SaleOrderItemStockClaimed foreign key attribute.
     *
     * @param  SaleOrderItemStockClaimed $l SaleOrderItemStockClaimed
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addSaleOrderItemStockClaimed(SaleOrderItemStockClaimed $l)
    {
        if ($this->collSaleOrderItemStockClaimeds === null) {
            $this->initSaleOrderItemStockClaimeds();
            $this->collSaleOrderItemStockClaimedsPartial = true;
        }

        if (!$this->collSaleOrderItemStockClaimeds->contains($l)) {
            $this->doAddSaleOrderItemStockClaimed($l);

            if ($this->saleOrderItemStockClaimedsScheduledForDeletion and $this->saleOrderItemStockClaimedsScheduledForDeletion->contains($l)) {
                $this->saleOrderItemStockClaimedsScheduledForDeletion->remove($this->saleOrderItemStockClaimedsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param SaleOrderItemStockClaimed $saleOrderItemStockClaimed The SaleOrderItemStockClaimed object to add.
     */
    protected function doAddSaleOrderItemStockClaimed(SaleOrderItemStockClaimed $saleOrderItemStockClaimed)
    {
        $this->collSaleOrderItemStockClaimeds[]= $saleOrderItemStockClaimed;
        $saleOrderItemStockClaimed->setProduct($this);
    }

    /**
     * @param  SaleOrderItemStockClaimed $saleOrderItemStockClaimed The SaleOrderItemStockClaimed object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeSaleOrderItemStockClaimed(SaleOrderItemStockClaimed $saleOrderItemStockClaimed)
    {
        if ($this->getSaleOrderItemStockClaimeds()->contains($saleOrderItemStockClaimed)) {
            $pos = $this->collSaleOrderItemStockClaimeds->search($saleOrderItemStockClaimed);
            $this->collSaleOrderItemStockClaimeds->remove($pos);
            if (null === $this->saleOrderItemStockClaimedsScheduledForDeletion) {
                $this->saleOrderItemStockClaimedsScheduledForDeletion = clone $this->collSaleOrderItemStockClaimeds;
                $this->saleOrderItemStockClaimedsScheduledForDeletion->clear();
            }
            $this->saleOrderItemStockClaimedsScheduledForDeletion[]= clone $saleOrderItemStockClaimed;
            $saleOrderItemStockClaimed->setProduct(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Product is new, it will return
     * an empty collection; or if this Product has previously
     * been saved, it will retrieve related SaleOrderItemStockClaimeds from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Product.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|SaleOrderItemStockClaimed[] List of SaleOrderItemStockClaimed objects
     */
    public function getSaleOrderItemStockClaimedsJoinSaleOrderItem(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = SaleOrderItemStockClaimedQuery::create(null, $criteria);
        $query->joinWith('SaleOrderItem', $joinBehavior);

        return $this->getSaleOrderItemStockClaimeds($query, $con);
    }

    /**
     * Clears out the collCustomerWishlists collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addCustomerWishlists()
     */
    public function clearCustomerWishlists()
    {
        $this->collCustomerWishlists = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collCustomerWishlists collection loaded partially.
     */
    public function resetPartialCustomerWishlists($v = true)
    {
        $this->collCustomerWishlistsPartial = $v;
    }

    /**
     * Initializes the collCustomerWishlists collection.
     *
     * By default this just sets the collCustomerWishlists collection to an empty array (like clearcollCustomerWishlists());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initCustomerWishlists($overrideExisting = true)
    {
        if (null !== $this->collCustomerWishlists && !$overrideExisting) {
            return;
        }

        $collectionClassName = CustomerWishlistTableMap::getTableMap()->getCollectionClassName();

        $this->collCustomerWishlists = new $collectionClassName;
        $this->collCustomerWishlists->setModel('\Model\Category\CustomerWishlist');
    }

    /**
     * Gets an array of CustomerWishlist objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProduct is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|CustomerWishlist[] List of CustomerWishlist objects
     * @throws PropelException
     */
    public function getCustomerWishlists(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerWishlistsPartial && !$this->isNew();
        if (null === $this->collCustomerWishlists || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collCustomerWishlists) {
                    $this->initCustomerWishlists();
                } else {
                    $collectionClassName = CustomerWishlistTableMap::getTableMap()->getCollectionClassName();

                    $collCustomerWishlists = new $collectionClassName;
                    $collCustomerWishlists->setModel('\Model\Category\CustomerWishlist');

                    return $collCustomerWishlists;
                }
            } else {
                $collCustomerWishlists = CustomerWishlistQuery::create(null, $criteria)
                    ->filterByProduct($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collCustomerWishlistsPartial && count($collCustomerWishlists)) {
                        $this->initCustomerWishlists(false);

                        foreach ($collCustomerWishlists as $obj) {
                            if (false == $this->collCustomerWishlists->contains($obj)) {
                                $this->collCustomerWishlists->append($obj);
                            }
                        }

                        $this->collCustomerWishlistsPartial = true;
                    }

                    return $collCustomerWishlists;
                }

                if ($partial && $this->collCustomerWishlists) {
                    foreach ($this->collCustomerWishlists as $obj) {
                        if ($obj->isNew()) {
                            $collCustomerWishlists[] = $obj;
                        }
                    }
                }

                $this->collCustomerWishlists = $collCustomerWishlists;
                $this->collCustomerWishlistsPartial = false;
            }
        }

        return $this->collCustomerWishlists;
    }

    /**
     * Sets a collection of CustomerWishlist objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $customerWishlists A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function setCustomerWishlists(Collection $customerWishlists, ConnectionInterface $con = null)
    {
        /** @var CustomerWishlist[] $customerWishlistsToDelete */
        $customerWishlistsToDelete = $this->getCustomerWishlists(new Criteria(), $con)->diff($customerWishlists);


        $this->customerWishlistsScheduledForDeletion = $customerWishlistsToDelete;

        foreach ($customerWishlistsToDelete as $customerWishlistRemoved) {
            $customerWishlistRemoved->setProduct(null);
        }

        $this->collCustomerWishlists = null;
        foreach ($customerWishlists as $customerWishlist) {
            $this->addCustomerWishlist($customerWishlist);
        }

        $this->collCustomerWishlists = $customerWishlists;
        $this->collCustomerWishlistsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseCustomerWishlist objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseCustomerWishlist objects.
     * @throws PropelException
     */
    public function countCustomerWishlists(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collCustomerWishlistsPartial && !$this->isNew();
        if (null === $this->collCustomerWishlists || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collCustomerWishlists) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getCustomerWishlists());
            }

            $query = CustomerWishlistQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProduct($this)
                ->count($con);
        }

        return count($this->collCustomerWishlists);
    }

    /**
     * Method called to associate a CustomerWishlist object to this object
     * through the CustomerWishlist foreign key attribute.
     *
     * @param  CustomerWishlist $l CustomerWishlist
     * @return $this|\Model\Product The current object (for fluent API support)
     */
    public function addCustomerWishlist(CustomerWishlist $l)
    {
        if ($this->collCustomerWishlists === null) {
            $this->initCustomerWishlists();
            $this->collCustomerWishlistsPartial = true;
        }

        if (!$this->collCustomerWishlists->contains($l)) {
            $this->doAddCustomerWishlist($l);

            if ($this->customerWishlistsScheduledForDeletion and $this->customerWishlistsScheduledForDeletion->contains($l)) {
                $this->customerWishlistsScheduledForDeletion->remove($this->customerWishlistsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param CustomerWishlist $customerWishlist The CustomerWishlist object to add.
     */
    protected function doAddCustomerWishlist(CustomerWishlist $customerWishlist)
    {
        $this->collCustomerWishlists[]= $customerWishlist;
        $customerWishlist->setProduct($this);
    }

    /**
     * @param  CustomerWishlist $customerWishlist The CustomerWishlist object to remove.
     * @return $this|ChildProduct The current object (for fluent API support)
     */
    public function removeCustomerWishlist(CustomerWishlist $customerWishlist)
    {
        if ($this->getCustomerWishlists()->contains($customerWishlist)) {
            $pos = $this->collCustomerWishlists->search($customerWishlist);
            $this->collCustomerWishlists->remove($pos);
            if (null === $this->customerWishlistsScheduledForDeletion) {
                $this->customerWishlistsScheduledForDeletion = clone $this->collCustomerWishlists;
                $this->customerWishlistsScheduledForDeletion->clear();
            }
            $this->customerWishlistsScheduledForDeletion[]= clone $customerWishlist;
            $customerWishlist->setProduct(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Product is new, it will return
     * an empty collection; or if this Product has previously
     * been saved, it will retrieve related CustomerWishlists from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Product.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|CustomerWishlist[] List of CustomerWishlist objects
     */
    public function getCustomerWishlistsJoinCustomer(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = CustomerWishlistQuery::create(null, $criteria);
        $query->joinWith('Customer', $joinBehavior);

        return $this->getCustomerWishlists($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aBrand) {
            $this->aBrand->removeProduct($this);
        }
        if (null !== $this->aProductUnit) {
            $this->aProductUnit->removeProduct($this);
        }
        if (null !== $this->aSupplierRef) {
            $this->aSupplierRef->removeProduct($this);
        }
        if (null !== $this->aCategory) {
            $this->aCategory->removeProduct($this);
        }
        if (null !== $this->aUser) {
            $this->aUser->removeProduct($this);
        }
        if (null !== $this->aVat) {
            $this->aVat->removeProduct($this);
        }
        $this->id = null;
        $this->derrived_from_id = null;
        $this->old_id = null;
        $this->import_tag = null;
        $this->sku = null;
        $this->ean = null;
        $this->deleted_on = null;
        $this->deleted_by_user_id = null;
        $this->popularity = null;
        $this->avg_rating = null;
        $this->supplier = null;
        $this->supplier_id = null;
        $this->supplier_product_number = null;
        $this->in_sale = null;
        $this->in_spotlight = null;
        $this->in_webshop = null;
        $this->is_offer = null;
        $this->is_bestseller = null;
        $this->is_out_of_stock = null;
        $this->has_images = null;
        $this->category_id = null;
        $this->number = null;
        $this->advertiser_user_id = null;
        $this->delivery_time_id = null;
        $this->thickness = null;
        $this->height = null;
        $this->width = null;
        $this->length = null;
        $this->weight = null;
        $this->composition = null;
        $this->material_id = null;
        $this->brand_id = null;
        $this->title = null;
        $this->description = null;
        $this->note = null;
        $this->created_on = null;
        $this->day_price = null;
        $this->sale_price = null;
        $this->discount_percentage = null;
        $this->purchase_price = null;
        $this->reseller_price = null;
        $this->advice_price = null;
        $this->bulk_stock_id = null;
        $this->stock_quantity = null;
        $this->virtual_stock = null;
        $this->quantity_per_pack = null;
        $this->youtube_url = null;
        $this->meta_title = null;
        $this->meta_description = null;
        $this->meta_keyword = null;
        $this->hashtag = null;
        $this->calculated_average_rating = null;
        $this->vat_id = null;
        $this->unit_id = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collProductMultiCategories) {
                foreach ($this->collProductMultiCategories as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaleOrderItemProducts) {
                foreach ($this->collSaleOrderItemProducts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProductReviews) {
                foreach ($this->collProductReviews as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collPromoProducts) {
                foreach ($this->collPromoProducts as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProductProperties) {
                foreach ($this->collProductProperties as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProductColors) {
                foreach ($this->collProductColors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProductLists) {
                foreach ($this->collProductLists as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProductUsages) {
                foreach ($this->collProductUsages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProductTranslations) {
                foreach ($this->collProductTranslations as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRelatedProductsRelatedByProductId) {
                foreach ($this->collRelatedProductsRelatedByProductId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRelatedProductsRelatedByOtherProductId) {
                foreach ($this->collRelatedProductsRelatedByOtherProductId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProductImages) {
                foreach ($this->collProductImages as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProductTags) {
                foreach ($this->collProductTags as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collSaleOrderItemStockClaimeds) {
                foreach ($this->collSaleOrderItemStockClaimeds as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collCustomerWishlists) {
                foreach ($this->collCustomerWishlists as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collProductMultiCategories = null;
        $this->collSaleOrderItemProducts = null;
        $this->collProductReviews = null;
        $this->collPromoProducts = null;
        $this->collProductProperties = null;
        $this->collProductColors = null;
        $this->collProductLists = null;
        $this->collProductUsages = null;
        $this->collProductTranslations = null;
        $this->collRelatedProductsRelatedByProductId = null;
        $this->collRelatedProductsRelatedByOtherProductId = null;
        $this->collProductImages = null;
        $this->collProductTags = null;
        $this->collSaleOrderItemStockClaimeds = null;
        $this->collCustomerWishlists = null;
        $this->aBrand = null;
        $this->aProductUnit = null;
        $this->aSupplierRef = null;
        $this->aCategory = null;
        $this->aUser = null;
        $this->aVat = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ProductTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildProduct The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[ProductTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
