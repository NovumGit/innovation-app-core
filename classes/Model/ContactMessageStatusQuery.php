<?php

namespace Model;

use Model\Base\ContactMessageStatusQuery as BaseContactMessageStatusQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'contact_message_status' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ContactMessageStatusQuery extends BaseContactMessageStatusQuery
{

}
