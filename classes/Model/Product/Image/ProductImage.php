<?php

namespace Model\Product\Image;

use Core\Config;
use Model\Product\Image\Base\ProductImage as BaseProductImage;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * Skeleton subclass for representing a row from the 'product_image' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProductImage extends BaseProductImage
{

    function delete(ConnectionInterface $con = null)
    {
        $oProduct = $this->getProduct();
        parent::delete($con);
        // This takes care that the has_images field in the products table is maintained automatically
        $oProduct->save();
    }

    function deleteWithFile()
    {
        $aFilesTryToDelete = [];
        $aFilesTryToDelete[] = Config::getDataDir()."/img/product/{$this->getId()}.{$this->getFileExt()}";
        $aSlicedFolders = glob(Config::getDataDir()."/img/product/sliced/*");
        foreach($aSlicedFolders as $sFolder)
        {
            $aFilesTryToDelete[] = "$sFolder/product/{$this->getId()}.{$this->getFileExt()}";
        }
        foreach($aFilesTryToDelete as $sFile)
        {
            if(file_exists($sFile))
            {
                unlink($sFile);
            }
        }
        $this->delete();
    }
}
