<?php

namespace Model\Product\Image;

use Core\PropelSqlTrait;
use Model\Product\Image\Base\ProductImageQuery as BaseProductImageQuery;
use Propel\Runtime\Connection\ConnectionInterface;

/**
 * Skeleton subclass for performing query and update operations on the 'product_image' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProductImageQuery extends BaseProductImageQuery
{
	use PropelSqlTrait;

}
