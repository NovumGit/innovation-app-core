<?php

namespace Model\Product\Image\Map;

use Model\Product\Image\ProductImage;
use Model\Product\Image\ProductImageQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'product_image' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class ProductImageTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Product.Image.Map.ProductImageTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'product_image';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Product\\Image\\ProductImage';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Product.Image.ProductImage';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'product_image.id';

    /**
     * the column name for the product_id field
     */
    const COL_PRODUCT_ID = 'product_image.product_id';

    /**
     * the column name for the sorting field
     */
    const COL_SORTING = 'product_image.sorting';

    /**
     * the column name for the file_type field
     */
    const COL_FILE_TYPE = 'product_image.file_type';

    /**
     * the column name for the file_size field
     */
    const COL_FILE_SIZE = 'product_image.file_size';

    /**
     * the column name for the is_online field
     */
    const COL_IS_ONLINE = 'product_image.is_online';

    /**
     * the column name for the original_name field
     */
    const COL_ORIGINAL_NAME = 'product_image.original_name';

    /**
     * the column name for the source_url field
     */
    const COL_SOURCE_URL = 'product_image.source_url';

    /**
     * the column name for the file_ext field
     */
    const COL_FILE_EXT = 'product_image.file_ext';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProductId', 'Sorting', 'FileType', 'FileSize', 'IsOnline', 'OriginalName', 'SourceUrl', 'FileExt', ),
        self::TYPE_CAMELNAME     => array('id', 'productId', 'sorting', 'fileType', 'fileSize', 'isOnline', 'originalName', 'sourceUrl', 'fileExt', ),
        self::TYPE_COLNAME       => array(ProductImageTableMap::COL_ID, ProductImageTableMap::COL_PRODUCT_ID, ProductImageTableMap::COL_SORTING, ProductImageTableMap::COL_FILE_TYPE, ProductImageTableMap::COL_FILE_SIZE, ProductImageTableMap::COL_IS_ONLINE, ProductImageTableMap::COL_ORIGINAL_NAME, ProductImageTableMap::COL_SOURCE_URL, ProductImageTableMap::COL_FILE_EXT, ),
        self::TYPE_FIELDNAME     => array('id', 'product_id', 'sorting', 'file_type', 'file_size', 'is_online', 'original_name', 'source_url', 'file_ext', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProductId' => 1, 'Sorting' => 2, 'FileType' => 3, 'FileSize' => 4, 'IsOnline' => 5, 'OriginalName' => 6, 'SourceUrl' => 7, 'FileExt' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'productId' => 1, 'sorting' => 2, 'fileType' => 3, 'fileSize' => 4, 'isOnline' => 5, 'originalName' => 6, 'sourceUrl' => 7, 'fileExt' => 8, ),
        self::TYPE_COLNAME       => array(ProductImageTableMap::COL_ID => 0, ProductImageTableMap::COL_PRODUCT_ID => 1, ProductImageTableMap::COL_SORTING => 2, ProductImageTableMap::COL_FILE_TYPE => 3, ProductImageTableMap::COL_FILE_SIZE => 4, ProductImageTableMap::COL_IS_ONLINE => 5, ProductImageTableMap::COL_ORIGINAL_NAME => 6, ProductImageTableMap::COL_SOURCE_URL => 7, ProductImageTableMap::COL_FILE_EXT => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'product_id' => 1, 'sorting' => 2, 'file_type' => 3, 'file_size' => 4, 'is_online' => 5, 'original_name' => 6, 'source_url' => 7, 'file_ext' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('product_image');
        $this->setPhpName('ProductImage');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Product\\Image\\ProductImage');
        $this->setPackage('Model.Product.Image');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('product_id', 'ProductId', 'INTEGER', 'product', 'id', false, null, null);
        $this->addColumn('sorting', 'Sorting', 'INTEGER', true, null, null);
        $this->addColumn('file_type', 'FileType', 'INTEGER', true, null, null);
        $this->addColumn('file_size', 'FileSize', 'INTEGER', true, null, null);
        $this->addColumn('is_online', 'IsOnline', 'BOOLEAN', false, 1, true);
        $this->addColumn('original_name', 'OriginalName', 'VARCHAR', false, 255, null);
        $this->addColumn('source_url', 'SourceUrl', 'VARCHAR', false, 255, null);
        $this->addColumn('file_ext', 'FileExt', 'VARCHAR', false, 10, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Product', '\\Model\\Product', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':product_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'query_cache' => array('backend' => 'apc', 'lifetime' => '3600', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ProductImageTableMap::CLASS_DEFAULT : ProductImageTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ProductImage object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ProductImageTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ProductImageTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ProductImageTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ProductImageTableMap::OM_CLASS;
            /** @var ProductImage $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ProductImageTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ProductImageTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ProductImageTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ProductImage $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ProductImageTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ProductImageTableMap::COL_ID);
            $criteria->addSelectColumn(ProductImageTableMap::COL_PRODUCT_ID);
            $criteria->addSelectColumn(ProductImageTableMap::COL_SORTING);
            $criteria->addSelectColumn(ProductImageTableMap::COL_FILE_TYPE);
            $criteria->addSelectColumn(ProductImageTableMap::COL_FILE_SIZE);
            $criteria->addSelectColumn(ProductImageTableMap::COL_IS_ONLINE);
            $criteria->addSelectColumn(ProductImageTableMap::COL_ORIGINAL_NAME);
            $criteria->addSelectColumn(ProductImageTableMap::COL_SOURCE_URL);
            $criteria->addSelectColumn(ProductImageTableMap::COL_FILE_EXT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.product_id');
            $criteria->addSelectColumn($alias . '.sorting');
            $criteria->addSelectColumn($alias . '.file_type');
            $criteria->addSelectColumn($alias . '.file_size');
            $criteria->addSelectColumn($alias . '.is_online');
            $criteria->addSelectColumn($alias . '.original_name');
            $criteria->addSelectColumn($alias . '.source_url');
            $criteria->addSelectColumn($alias . '.file_ext');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ProductImageTableMap::DATABASE_NAME)->getTable(ProductImageTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ProductImageTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ProductImageTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ProductImageTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ProductImage or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ProductImage object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductImageTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Product\Image\ProductImage) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ProductImageTableMap::DATABASE_NAME);
            $criteria->add(ProductImageTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ProductImageQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ProductImageTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ProductImageTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the product_image table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ProductImageQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ProductImage or Criteria object.
     *
     * @param mixed               $criteria Criteria or ProductImage object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProductImageTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ProductImage object
        }

        if ($criteria->containsKey(ProductImageTableMap::COL_ID) && $criteria->keyContainsValue(ProductImageTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ProductImageTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ProductImageQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ProductImageTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ProductImageTableMap::buildTableMap();
