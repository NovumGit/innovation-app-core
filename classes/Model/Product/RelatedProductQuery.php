<?php

namespace Model\Product;

use Model\Product\Base\RelatedProductQuery as BaseRelatedProductQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'related_product' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class RelatedProductQuery extends BaseRelatedProductQuery
{


}
