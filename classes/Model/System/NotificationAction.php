<?php

namespace Model\System;

use Model\System\Base\NotificationAction as BaseNotificationAction;

/**
 * Skeleton subclass for representing a row from the 'notification_action' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class NotificationAction extends BaseNotificationAction
{

}
