<?php

namespace Model\System;

use Model\System\Base\DataSourceEndpoint as BaseDataSourceEndpoint;

/**
 * Skeleton subclass for representing a row from the 'datasource_endpoint' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DataSourceEndpoint extends BaseDataSourceEndpoint
{

}
