<?php

namespace Model\System\Base;

use \Exception;
use \PDO;
use Model\System\App as ChildApp;
use Model\System\AppQuery as ChildAppQuery;
use Model\System\Map\AppTableMap;
use Model\System\UI\UIComponent;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'app' table.
 *
 *
 *
 * @method     ChildAppQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildAppQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildAppQuery orderByAppTypeId($order = Criteria::ASC) Order by the app_type_id column
 * @method     ChildAppQuery orderByAppDescription($order = Criteria::ASC) Order by the app_description column
 * @method     ChildAppQuery orderByAppAudience($order = Criteria::ASC) Order by the app_audience column
 * @method     ChildAppQuery orderByAppIcon($order = Criteria::ASC) Order by the app_icon column
 * @method     ChildAppQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildAppQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildAppQuery groupById() Group by the id column
 * @method     ChildAppQuery groupByName() Group by the name column
 * @method     ChildAppQuery groupByAppTypeId() Group by the app_type_id column
 * @method     ChildAppQuery groupByAppDescription() Group by the app_description column
 * @method     ChildAppQuery groupByAppAudience() Group by the app_audience column
 * @method     ChildAppQuery groupByAppIcon() Group by the app_icon column
 * @method     ChildAppQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildAppQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildAppQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildAppQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildAppQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildAppQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildAppQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildAppQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildAppQuery leftJoinAppType($relationAlias = null) Adds a LEFT JOIN clause to the query using the AppType relation
 * @method     ChildAppQuery rightJoinAppType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AppType relation
 * @method     ChildAppQuery innerJoinAppType($relationAlias = null) Adds a INNER JOIN clause to the query using the AppType relation
 *
 * @method     ChildAppQuery joinWithAppType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the AppType relation
 *
 * @method     ChildAppQuery leftJoinWithAppType() Adds a LEFT JOIN clause and with to the query using the AppType relation
 * @method     ChildAppQuery rightJoinWithAppType() Adds a RIGHT JOIN clause and with to the query using the AppType relation
 * @method     ChildAppQuery innerJoinWithAppType() Adds a INNER JOIN clause and with to the query using the AppType relation
 *
 * @method     ChildAppQuery leftJoinUIComponent($relationAlias = null) Adds a LEFT JOIN clause to the query using the UIComponent relation
 * @method     ChildAppQuery rightJoinUIComponent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UIComponent relation
 * @method     ChildAppQuery innerJoinUIComponent($relationAlias = null) Adds a INNER JOIN clause to the query using the UIComponent relation
 *
 * @method     ChildAppQuery joinWithUIComponent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UIComponent relation
 *
 * @method     ChildAppQuery leftJoinWithUIComponent() Adds a LEFT JOIN clause and with to the query using the UIComponent relation
 * @method     ChildAppQuery rightJoinWithUIComponent() Adds a RIGHT JOIN clause and with to the query using the UIComponent relation
 * @method     ChildAppQuery innerJoinWithUIComponent() Adds a INNER JOIN clause and with to the query using the UIComponent relation
 *
 * @method     \Model\System\AppTypeQuery|\Model\System\UI\UIComponentQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildApp findOne(ConnectionInterface $con = null) Return the first ChildApp matching the query
 * @method     ChildApp findOneOrCreate(ConnectionInterface $con = null) Return the first ChildApp matching the query, or a new ChildApp object populated from the query conditions when no match is found
 *
 * @method     ChildApp findOneById(int $id) Return the first ChildApp filtered by the id column
 * @method     ChildApp findOneByName(string $name) Return the first ChildApp filtered by the name column
 * @method     ChildApp findOneByAppTypeId(int $app_type_id) Return the first ChildApp filtered by the app_type_id column
 * @method     ChildApp findOneByAppDescription(string $app_description) Return the first ChildApp filtered by the app_description column
 * @method     ChildApp findOneByAppAudience(string $app_audience) Return the first ChildApp filtered by the app_audience column
 * @method     ChildApp findOneByAppIcon(string $app_icon) Return the first ChildApp filtered by the app_icon column
 * @method     ChildApp findOneByCreatedAt(string $created_at) Return the first ChildApp filtered by the created_at column
 * @method     ChildApp findOneByUpdatedAt(string $updated_at) Return the first ChildApp filtered by the updated_at column *

 * @method     ChildApp requirePk($key, ConnectionInterface $con = null) Return the ChildApp by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApp requireOne(ConnectionInterface $con = null) Return the first ChildApp matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApp requireOneById(int $id) Return the first ChildApp filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApp requireOneByName(string $name) Return the first ChildApp filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApp requireOneByAppTypeId(int $app_type_id) Return the first ChildApp filtered by the app_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApp requireOneByAppDescription(string $app_description) Return the first ChildApp filtered by the app_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApp requireOneByAppAudience(string $app_audience) Return the first ChildApp filtered by the app_audience column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApp requireOneByAppIcon(string $app_icon) Return the first ChildApp filtered by the app_icon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApp requireOneByCreatedAt(string $created_at) Return the first ChildApp filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildApp requireOneByUpdatedAt(string $updated_at) Return the first ChildApp filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildApp[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildApp objects based on current ModelCriteria
 * @method     ChildApp[]|ObjectCollection findById(int $id) Return ChildApp objects filtered by the id column
 * @method     ChildApp[]|ObjectCollection findByName(string $name) Return ChildApp objects filtered by the name column
 * @method     ChildApp[]|ObjectCollection findByAppTypeId(int $app_type_id) Return ChildApp objects filtered by the app_type_id column
 * @method     ChildApp[]|ObjectCollection findByAppDescription(string $app_description) Return ChildApp objects filtered by the app_description column
 * @method     ChildApp[]|ObjectCollection findByAppAudience(string $app_audience) Return ChildApp objects filtered by the app_audience column
 * @method     ChildApp[]|ObjectCollection findByAppIcon(string $app_icon) Return ChildApp objects filtered by the app_icon column
 * @method     ChildApp[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildApp objects filtered by the created_at column
 * @method     ChildApp[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildApp objects filtered by the updated_at column
 * @method     ChildApp[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class AppQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\Base\AppQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\App', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildAppQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildAppQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildAppQuery) {
            return $criteria;
        }
        $query = new ChildAppQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildApp|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(AppTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = AppTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildApp A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, app_type_id, app_description, app_audience, app_icon, created_at, updated_at FROM app WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildApp $obj */
            $obj = new ChildApp();
            $obj->hydrate($row);
            AppTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildApp|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildAppQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(AppTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildAppQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(AppTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(AppTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(AppTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the app_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAppTypeId(1234); // WHERE app_type_id = 1234
     * $query->filterByAppTypeId(array(12, 34)); // WHERE app_type_id IN (12, 34)
     * $query->filterByAppTypeId(array('min' => 12)); // WHERE app_type_id > 12
     * </code>
     *
     * @see       filterByAppType()
     *
     * @param     mixed $appTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppQuery The current query, for fluid interface
     */
    public function filterByAppTypeId($appTypeId = null, $comparison = null)
    {
        if (is_array($appTypeId)) {
            $useMinMax = false;
            if (isset($appTypeId['min'])) {
                $this->addUsingAlias(AppTableMap::COL_APP_TYPE_ID, $appTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($appTypeId['max'])) {
                $this->addUsingAlias(AppTableMap::COL_APP_TYPE_ID, $appTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppTableMap::COL_APP_TYPE_ID, $appTypeId, $comparison);
    }

    /**
     * Filter the query on the app_description column
     *
     * Example usage:
     * <code>
     * $query->filterByAppDescription('fooValue');   // WHERE app_description = 'fooValue'
     * $query->filterByAppDescription('%fooValue%', Criteria::LIKE); // WHERE app_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appDescription The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppQuery The current query, for fluid interface
     */
    public function filterByAppDescription($appDescription = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appDescription)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppTableMap::COL_APP_DESCRIPTION, $appDescription, $comparison);
    }

    /**
     * Filter the query on the app_audience column
     *
     * Example usage:
     * <code>
     * $query->filterByAppAudience('fooValue');   // WHERE app_audience = 'fooValue'
     * $query->filterByAppAudience('%fooValue%', Criteria::LIKE); // WHERE app_audience LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appAudience The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppQuery The current query, for fluid interface
     */
    public function filterByAppAudience($appAudience = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appAudience)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppTableMap::COL_APP_AUDIENCE, $appAudience, $comparison);
    }

    /**
     * Filter the query on the app_icon column
     *
     * Example usage:
     * <code>
     * $query->filterByAppIcon('fooValue');   // WHERE app_icon = 'fooValue'
     * $query->filterByAppIcon('%fooValue%', Criteria::LIKE); // WHERE app_icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $appIcon The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppQuery The current query, for fluid interface
     */
    public function filterByAppIcon($appIcon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($appIcon)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppTableMap::COL_APP_ICON, $appIcon, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(AppTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(AppTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildAppQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(AppTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(AppTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(AppTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\AppType object
     *
     * @param \Model\System\AppType|ObjectCollection $appType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildAppQuery The current query, for fluid interface
     */
    public function filterByAppType($appType, $comparison = null)
    {
        if ($appType instanceof \Model\System\AppType) {
            return $this
                ->addUsingAlias(AppTableMap::COL_APP_TYPE_ID, $appType->getId(), $comparison);
        } elseif ($appType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(AppTableMap::COL_APP_TYPE_ID, $appType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAppType() only accepts arguments of type \Model\System\AppType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AppType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildAppQuery The current query, for fluid interface
     */
    public function joinAppType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AppType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AppType');
        }

        return $this;
    }

    /**
     * Use the AppType relation AppType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\AppTypeQuery A secondary query class using the current class as primary query
     */
    public function useAppTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinAppType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AppType', '\Model\System\AppTypeQuery');
    }

    /**
     * Filter the query by a related \Model\System\UI\UIComponent object
     *
     * @param \Model\System\UI\UIComponent|ObjectCollection $uIComponent the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildAppQuery The current query, for fluid interface
     */
    public function filterByUIComponent($uIComponent, $comparison = null)
    {
        if ($uIComponent instanceof \Model\System\UI\UIComponent) {
            return $this
                ->addUsingAlias(AppTableMap::COL_ID, $uIComponent->getAppId(), $comparison);
        } elseif ($uIComponent instanceof ObjectCollection) {
            return $this
                ->useUIComponentQuery()
                ->filterByPrimaryKeys($uIComponent->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUIComponent() only accepts arguments of type \Model\System\UI\UIComponent or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UIComponent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildAppQuery The current query, for fluid interface
     */
    public function joinUIComponent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UIComponent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UIComponent');
        }

        return $this;
    }

    /**
     * Use the UIComponent relation UIComponent object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\UI\UIComponentQuery A secondary query class using the current class as primary query
     */
    public function useUIComponentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUIComponent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UIComponent', '\Model\System\UI\UIComponentQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildApp $app Object to remove from the list of results
     *
     * @return $this|ChildAppQuery The current query, for fluid interface
     */
    public function prune($app = null)
    {
        if ($app) {
            $this->addUsingAlias(AppTableMap::COL_ID, $app->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the app table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            AppTableMap::clearInstancePool();
            AppTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(AppTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(AppTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            AppTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            AppTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildAppQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(AppTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildAppQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(AppTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildAppQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(AppTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildAppQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(AppTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildAppQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(AppTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildAppQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(AppTableMap::COL_CREATED_AT);
    }

} // AppQuery
