<?php

namespace Model\System\Base;

use \Exception;
use \PDO;
use Model\Custom\NovumOverheid\MessageTranslation;
use Model\System\DataSourceEndpoint as ChildDataSourceEndpoint;
use Model\System\DataSourceEndpointQuery as ChildDataSourceEndpointQuery;
use Model\System\Map\DataSourceEndpointTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'datasource_endpoint' table.
 *
 *
 *
 * @method     ChildDataSourceEndpointQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildDataSourceEndpointQuery orderByDatasourceId($order = Criteria::ASC) Order by the datasource_id column
 * @method     ChildDataSourceEndpointQuery orderByDatasourceEndpointMethodId($order = Criteria::ASC) Order by the datasource_endpoint_method_id column
 * @method     ChildDataSourceEndpointQuery orderByCode($order = Criteria::ASC) Order by the code column
 * @method     ChildDataSourceEndpointQuery orderBySummary($order = Criteria::ASC) Order by the summary column
 * @method     ChildDataSourceEndpointQuery orderByUrl($order = Criteria::ASC) Order by the url column
 *
 * @method     ChildDataSourceEndpointQuery groupById() Group by the id column
 * @method     ChildDataSourceEndpointQuery groupByDatasourceId() Group by the datasource_id column
 * @method     ChildDataSourceEndpointQuery groupByDatasourceEndpointMethodId() Group by the datasource_endpoint_method_id column
 * @method     ChildDataSourceEndpointQuery groupByCode() Group by the code column
 * @method     ChildDataSourceEndpointQuery groupBySummary() Group by the summary column
 * @method     ChildDataSourceEndpointQuery groupByUrl() Group by the url column
 *
 * @method     ChildDataSourceEndpointQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildDataSourceEndpointQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildDataSourceEndpointQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildDataSourceEndpointQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildDataSourceEndpointQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildDataSourceEndpointQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildDataSourceEndpointQuery leftJoinDataSourceEndpointMethod($relationAlias = null) Adds a LEFT JOIN clause to the query using the DataSourceEndpointMethod relation
 * @method     ChildDataSourceEndpointQuery rightJoinDataSourceEndpointMethod($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DataSourceEndpointMethod relation
 * @method     ChildDataSourceEndpointQuery innerJoinDataSourceEndpointMethod($relationAlias = null) Adds a INNER JOIN clause to the query using the DataSourceEndpointMethod relation
 *
 * @method     ChildDataSourceEndpointQuery joinWithDataSourceEndpointMethod($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DataSourceEndpointMethod relation
 *
 * @method     ChildDataSourceEndpointQuery leftJoinWithDataSourceEndpointMethod() Adds a LEFT JOIN clause and with to the query using the DataSourceEndpointMethod relation
 * @method     ChildDataSourceEndpointQuery rightJoinWithDataSourceEndpointMethod() Adds a RIGHT JOIN clause and with to the query using the DataSourceEndpointMethod relation
 * @method     ChildDataSourceEndpointQuery innerJoinWithDataSourceEndpointMethod() Adds a INNER JOIN clause and with to the query using the DataSourceEndpointMethod relation
 *
 * @method     ChildDataSourceEndpointQuery leftJoinDataSource($relationAlias = null) Adds a LEFT JOIN clause to the query using the DataSource relation
 * @method     ChildDataSourceEndpointQuery rightJoinDataSource($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DataSource relation
 * @method     ChildDataSourceEndpointQuery innerJoinDataSource($relationAlias = null) Adds a INNER JOIN clause to the query using the DataSource relation
 *
 * @method     ChildDataSourceEndpointQuery joinWithDataSource($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DataSource relation
 *
 * @method     ChildDataSourceEndpointQuery leftJoinWithDataSource() Adds a LEFT JOIN clause and with to the query using the DataSource relation
 * @method     ChildDataSourceEndpointQuery rightJoinWithDataSource() Adds a RIGHT JOIN clause and with to the query using the DataSource relation
 * @method     ChildDataSourceEndpointQuery innerJoinWithDataSource() Adds a INNER JOIN clause and with to the query using the DataSource relation
 *
 * @method     ChildDataSourceEndpointQuery leftJoinMessageTranslationRelatedByDatasourceEndpointFromId($relationAlias = null) Adds a LEFT JOIN clause to the query using the MessageTranslationRelatedByDatasourceEndpointFromId relation
 * @method     ChildDataSourceEndpointQuery rightJoinMessageTranslationRelatedByDatasourceEndpointFromId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MessageTranslationRelatedByDatasourceEndpointFromId relation
 * @method     ChildDataSourceEndpointQuery innerJoinMessageTranslationRelatedByDatasourceEndpointFromId($relationAlias = null) Adds a INNER JOIN clause to the query using the MessageTranslationRelatedByDatasourceEndpointFromId relation
 *
 * @method     ChildDataSourceEndpointQuery joinWithMessageTranslationRelatedByDatasourceEndpointFromId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MessageTranslationRelatedByDatasourceEndpointFromId relation
 *
 * @method     ChildDataSourceEndpointQuery leftJoinWithMessageTranslationRelatedByDatasourceEndpointFromId() Adds a LEFT JOIN clause and with to the query using the MessageTranslationRelatedByDatasourceEndpointFromId relation
 * @method     ChildDataSourceEndpointQuery rightJoinWithMessageTranslationRelatedByDatasourceEndpointFromId() Adds a RIGHT JOIN clause and with to the query using the MessageTranslationRelatedByDatasourceEndpointFromId relation
 * @method     ChildDataSourceEndpointQuery innerJoinWithMessageTranslationRelatedByDatasourceEndpointFromId() Adds a INNER JOIN clause and with to the query using the MessageTranslationRelatedByDatasourceEndpointFromId relation
 *
 * @method     ChildDataSourceEndpointQuery leftJoinMessageTranslationRelatedByDatasourceEndpointToId($relationAlias = null) Adds a LEFT JOIN clause to the query using the MessageTranslationRelatedByDatasourceEndpointToId relation
 * @method     ChildDataSourceEndpointQuery rightJoinMessageTranslationRelatedByDatasourceEndpointToId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the MessageTranslationRelatedByDatasourceEndpointToId relation
 * @method     ChildDataSourceEndpointQuery innerJoinMessageTranslationRelatedByDatasourceEndpointToId($relationAlias = null) Adds a INNER JOIN clause to the query using the MessageTranslationRelatedByDatasourceEndpointToId relation
 *
 * @method     ChildDataSourceEndpointQuery joinWithMessageTranslationRelatedByDatasourceEndpointToId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the MessageTranslationRelatedByDatasourceEndpointToId relation
 *
 * @method     ChildDataSourceEndpointQuery leftJoinWithMessageTranslationRelatedByDatasourceEndpointToId() Adds a LEFT JOIN clause and with to the query using the MessageTranslationRelatedByDatasourceEndpointToId relation
 * @method     ChildDataSourceEndpointQuery rightJoinWithMessageTranslationRelatedByDatasourceEndpointToId() Adds a RIGHT JOIN clause and with to the query using the MessageTranslationRelatedByDatasourceEndpointToId relation
 * @method     ChildDataSourceEndpointQuery innerJoinWithMessageTranslationRelatedByDatasourceEndpointToId() Adds a INNER JOIN clause and with to the query using the MessageTranslationRelatedByDatasourceEndpointToId relation
 *
 * @method     \Model\System\DataSourceEndpointMethodQuery|\Model\System\DataSourceQuery|\Model\Custom\NovumOverheid\MessageTranslationQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildDataSourceEndpoint findOne(ConnectionInterface $con = null) Return the first ChildDataSourceEndpoint matching the query
 * @method     ChildDataSourceEndpoint findOneOrCreate(ConnectionInterface $con = null) Return the first ChildDataSourceEndpoint matching the query, or a new ChildDataSourceEndpoint object populated from the query conditions when no match is found
 *
 * @method     ChildDataSourceEndpoint findOneById(int $id) Return the first ChildDataSourceEndpoint filtered by the id column
 * @method     ChildDataSourceEndpoint findOneByDatasourceId(int $datasource_id) Return the first ChildDataSourceEndpoint filtered by the datasource_id column
 * @method     ChildDataSourceEndpoint findOneByDatasourceEndpointMethodId(int $datasource_endpoint_method_id) Return the first ChildDataSourceEndpoint filtered by the datasource_endpoint_method_id column
 * @method     ChildDataSourceEndpoint findOneByCode(string $code) Return the first ChildDataSourceEndpoint filtered by the code column
 * @method     ChildDataSourceEndpoint findOneBySummary(string $summary) Return the first ChildDataSourceEndpoint filtered by the summary column
 * @method     ChildDataSourceEndpoint findOneByUrl(string $url) Return the first ChildDataSourceEndpoint filtered by the url column *

 * @method     ChildDataSourceEndpoint requirePk($key, ConnectionInterface $con = null) Return the ChildDataSourceEndpoint by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataSourceEndpoint requireOne(ConnectionInterface $con = null) Return the first ChildDataSourceEndpoint matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDataSourceEndpoint requireOneById(int $id) Return the first ChildDataSourceEndpoint filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataSourceEndpoint requireOneByDatasourceId(int $datasource_id) Return the first ChildDataSourceEndpoint filtered by the datasource_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataSourceEndpoint requireOneByDatasourceEndpointMethodId(int $datasource_endpoint_method_id) Return the first ChildDataSourceEndpoint filtered by the datasource_endpoint_method_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataSourceEndpoint requireOneByCode(string $code) Return the first ChildDataSourceEndpoint filtered by the code column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataSourceEndpoint requireOneBySummary(string $summary) Return the first ChildDataSourceEndpoint filtered by the summary column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataSourceEndpoint requireOneByUrl(string $url) Return the first ChildDataSourceEndpoint filtered by the url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDataSourceEndpoint[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildDataSourceEndpoint objects based on current ModelCriteria
 * @method     ChildDataSourceEndpoint[]|ObjectCollection findById(int $id) Return ChildDataSourceEndpoint objects filtered by the id column
 * @method     ChildDataSourceEndpoint[]|ObjectCollection findByDatasourceId(int $datasource_id) Return ChildDataSourceEndpoint objects filtered by the datasource_id column
 * @method     ChildDataSourceEndpoint[]|ObjectCollection findByDatasourceEndpointMethodId(int $datasource_endpoint_method_id) Return ChildDataSourceEndpoint objects filtered by the datasource_endpoint_method_id column
 * @method     ChildDataSourceEndpoint[]|ObjectCollection findByCode(string $code) Return ChildDataSourceEndpoint objects filtered by the code column
 * @method     ChildDataSourceEndpoint[]|ObjectCollection findBySummary(string $summary) Return ChildDataSourceEndpoint objects filtered by the summary column
 * @method     ChildDataSourceEndpoint[]|ObjectCollection findByUrl(string $url) Return ChildDataSourceEndpoint objects filtered by the url column
 * @method     ChildDataSourceEndpoint[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class DataSourceEndpointQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\Base\DataSourceEndpointQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\DataSourceEndpoint', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildDataSourceEndpointQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildDataSourceEndpointQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildDataSourceEndpointQuery) {
            return $criteria;
        }
        $query = new ChildDataSourceEndpointQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildDataSourceEndpoint|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DataSourceEndpointTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = DataSourceEndpointTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDataSourceEndpoint A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, datasource_id, datasource_endpoint_method_id, code, summary, url FROM datasource_endpoint WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildDataSourceEndpoint $obj */
            $obj = new ChildDataSourceEndpoint();
            $obj->hydrate($row);
            DataSourceEndpointTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildDataSourceEndpoint|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DataSourceEndpointTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DataSourceEndpointTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DataSourceEndpointTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DataSourceEndpointTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataSourceEndpointTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the datasource_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDatasourceId(1234); // WHERE datasource_id = 1234
     * $query->filterByDatasourceId(array(12, 34)); // WHERE datasource_id IN (12, 34)
     * $query->filterByDatasourceId(array('min' => 12)); // WHERE datasource_id > 12
     * </code>
     *
     * @see       filterByDataSource()
     *
     * @param     mixed $datasourceId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function filterByDatasourceId($datasourceId = null, $comparison = null)
    {
        if (is_array($datasourceId)) {
            $useMinMax = false;
            if (isset($datasourceId['min'])) {
                $this->addUsingAlias(DataSourceEndpointTableMap::COL_DATASOURCE_ID, $datasourceId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datasourceId['max'])) {
                $this->addUsingAlias(DataSourceEndpointTableMap::COL_DATASOURCE_ID, $datasourceId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataSourceEndpointTableMap::COL_DATASOURCE_ID, $datasourceId, $comparison);
    }

    /**
     * Filter the query on the datasource_endpoint_method_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDatasourceEndpointMethodId(1234); // WHERE datasource_endpoint_method_id = 1234
     * $query->filterByDatasourceEndpointMethodId(array(12, 34)); // WHERE datasource_endpoint_method_id IN (12, 34)
     * $query->filterByDatasourceEndpointMethodId(array('min' => 12)); // WHERE datasource_endpoint_method_id > 12
     * </code>
     *
     * @see       filterByDataSourceEndpointMethod()
     *
     * @param     mixed $datasourceEndpointMethodId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function filterByDatasourceEndpointMethodId($datasourceEndpointMethodId = null, $comparison = null)
    {
        if (is_array($datasourceEndpointMethodId)) {
            $useMinMax = false;
            if (isset($datasourceEndpointMethodId['min'])) {
                $this->addUsingAlias(DataSourceEndpointTableMap::COL_DATASOURCE_ENDPOINT_METHOD_ID, $datasourceEndpointMethodId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($datasourceEndpointMethodId['max'])) {
                $this->addUsingAlias(DataSourceEndpointTableMap::COL_DATASOURCE_ENDPOINT_METHOD_ID, $datasourceEndpointMethodId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataSourceEndpointTableMap::COL_DATASOURCE_ENDPOINT_METHOD_ID, $datasourceEndpointMethodId, $comparison);
    }

    /**
     * Filter the query on the code column
     *
     * Example usage:
     * <code>
     * $query->filterByCode('fooValue');   // WHERE code = 'fooValue'
     * $query->filterByCode('%fooValue%', Criteria::LIKE); // WHERE code LIKE '%fooValue%'
     * </code>
     *
     * @param     string $code The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function filterByCode($code = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($code)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataSourceEndpointTableMap::COL_CODE, $code, $comparison);
    }

    /**
     * Filter the query on the summary column
     *
     * Example usage:
     * <code>
     * $query->filterBySummary('fooValue');   // WHERE summary = 'fooValue'
     * $query->filterBySummary('%fooValue%', Criteria::LIKE); // WHERE summary LIKE '%fooValue%'
     * </code>
     *
     * @param     string $summary The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function filterBySummary($summary = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($summary)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataSourceEndpointTableMap::COL_SUMMARY, $summary, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%', Criteria::LIKE); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataSourceEndpointTableMap::COL_URL, $url, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\DataSourceEndpointMethod object
     *
     * @param \Model\System\DataSourceEndpointMethod|ObjectCollection $dataSourceEndpointMethod The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function filterByDataSourceEndpointMethod($dataSourceEndpointMethod, $comparison = null)
    {
        if ($dataSourceEndpointMethod instanceof \Model\System\DataSourceEndpointMethod) {
            return $this
                ->addUsingAlias(DataSourceEndpointTableMap::COL_DATASOURCE_ENDPOINT_METHOD_ID, $dataSourceEndpointMethod->getId(), $comparison);
        } elseif ($dataSourceEndpointMethod instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DataSourceEndpointTableMap::COL_DATASOURCE_ENDPOINT_METHOD_ID, $dataSourceEndpointMethod->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDataSourceEndpointMethod() only accepts arguments of type \Model\System\DataSourceEndpointMethod or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DataSourceEndpointMethod relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function joinDataSourceEndpointMethod($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DataSourceEndpointMethod');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DataSourceEndpointMethod');
        }

        return $this;
    }

    /**
     * Use the DataSourceEndpointMethod relation DataSourceEndpointMethod object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataSourceEndpointMethodQuery A secondary query class using the current class as primary query
     */
    public function useDataSourceEndpointMethodQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDataSourceEndpointMethod($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DataSourceEndpointMethod', '\Model\System\DataSourceEndpointMethodQuery');
    }

    /**
     * Filter the query by a related \Model\System\DataSource object
     *
     * @param \Model\System\DataSource|ObjectCollection $dataSource The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function filterByDataSource($dataSource, $comparison = null)
    {
        if ($dataSource instanceof \Model\System\DataSource) {
            return $this
                ->addUsingAlias(DataSourceEndpointTableMap::COL_DATASOURCE_ID, $dataSource->getId(), $comparison);
        } elseif ($dataSource instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DataSourceEndpointTableMap::COL_DATASOURCE_ID, $dataSource->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDataSource() only accepts arguments of type \Model\System\DataSource or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DataSource relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function joinDataSource($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DataSource');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DataSource');
        }

        return $this;
    }

    /**
     * Use the DataSource relation DataSource object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataSourceQuery A secondary query class using the current class as primary query
     */
    public function useDataSourceQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDataSource($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DataSource', '\Model\System\DataSourceQuery');
    }

    /**
     * Filter the query by a related \Model\Custom\NovumOverheid\MessageTranslation object
     *
     * @param \Model\Custom\NovumOverheid\MessageTranslation|ObjectCollection $messageTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function filterByMessageTranslationRelatedByDatasourceEndpointFromId($messageTranslation, $comparison = null)
    {
        if ($messageTranslation instanceof \Model\Custom\NovumOverheid\MessageTranslation) {
            return $this
                ->addUsingAlias(DataSourceEndpointTableMap::COL_ID, $messageTranslation->getDatasourceEndpointFromId(), $comparison);
        } elseif ($messageTranslation instanceof ObjectCollection) {
            return $this
                ->useMessageTranslationRelatedByDatasourceEndpointFromIdQuery()
                ->filterByPrimaryKeys($messageTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMessageTranslationRelatedByDatasourceEndpointFromId() only accepts arguments of type \Model\Custom\NovumOverheid\MessageTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MessageTranslationRelatedByDatasourceEndpointFromId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function joinMessageTranslationRelatedByDatasourceEndpointFromId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MessageTranslationRelatedByDatasourceEndpointFromId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MessageTranslationRelatedByDatasourceEndpointFromId');
        }

        return $this;
    }

    /**
     * Use the MessageTranslationRelatedByDatasourceEndpointFromId relation MessageTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Custom\NovumOverheid\MessageTranslationQuery A secondary query class using the current class as primary query
     */
    public function useMessageTranslationRelatedByDatasourceEndpointFromIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMessageTranslationRelatedByDatasourceEndpointFromId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MessageTranslationRelatedByDatasourceEndpointFromId', '\Model\Custom\NovumOverheid\MessageTranslationQuery');
    }

    /**
     * Filter the query by a related \Model\Custom\NovumOverheid\MessageTranslation object
     *
     * @param \Model\Custom\NovumOverheid\MessageTranslation|ObjectCollection $messageTranslation the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function filterByMessageTranslationRelatedByDatasourceEndpointToId($messageTranslation, $comparison = null)
    {
        if ($messageTranslation instanceof \Model\Custom\NovumOverheid\MessageTranslation) {
            return $this
                ->addUsingAlias(DataSourceEndpointTableMap::COL_ID, $messageTranslation->getDatasourceEndpointToId(), $comparison);
        } elseif ($messageTranslation instanceof ObjectCollection) {
            return $this
                ->useMessageTranslationRelatedByDatasourceEndpointToIdQuery()
                ->filterByPrimaryKeys($messageTranslation->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByMessageTranslationRelatedByDatasourceEndpointToId() only accepts arguments of type \Model\Custom\NovumOverheid\MessageTranslation or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the MessageTranslationRelatedByDatasourceEndpointToId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function joinMessageTranslationRelatedByDatasourceEndpointToId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('MessageTranslationRelatedByDatasourceEndpointToId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'MessageTranslationRelatedByDatasourceEndpointToId');
        }

        return $this;
    }

    /**
     * Use the MessageTranslationRelatedByDatasourceEndpointToId relation MessageTranslation object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Custom\NovumOverheid\MessageTranslationQuery A secondary query class using the current class as primary query
     */
    public function useMessageTranslationRelatedByDatasourceEndpointToIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinMessageTranslationRelatedByDatasourceEndpointToId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'MessageTranslationRelatedByDatasourceEndpointToId', '\Model\Custom\NovumOverheid\MessageTranslationQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildDataSourceEndpoint $dataSourceEndpoint Object to remove from the list of results
     *
     * @return $this|ChildDataSourceEndpointQuery The current query, for fluid interface
     */
    public function prune($dataSourceEndpoint = null)
    {
        if ($dataSourceEndpoint) {
            $this->addUsingAlias(DataSourceEndpointTableMap::COL_ID, $dataSourceEndpoint->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the datasource_endpoint table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DataSourceEndpointTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            DataSourceEndpointTableMap::clearInstancePool();
            DataSourceEndpointTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DataSourceEndpointTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(DataSourceEndpointTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            DataSourceEndpointTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            DataSourceEndpointTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // DataSourceEndpointQuery
