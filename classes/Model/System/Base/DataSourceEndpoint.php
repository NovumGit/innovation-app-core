<?php

namespace Model\System\Base;

use \Exception;
use \PDO;
use Model\Custom\NovumOverheid\MessageTranslation;
use Model\Custom\NovumOverheid\MessageTranslationQuery;
use Model\Custom\NovumOverheid\Base\MessageTranslation as BaseMessageTranslation;
use Model\Custom\NovumOverheid\Map\MessageTranslationTableMap;
use Model\System\DataSource as ChildDataSource;
use Model\System\DataSourceEndpoint as ChildDataSourceEndpoint;
use Model\System\DataSourceEndpointMethod as ChildDataSourceEndpointMethod;
use Model\System\DataSourceEndpointMethodQuery as ChildDataSourceEndpointMethodQuery;
use Model\System\DataSourceEndpointQuery as ChildDataSourceEndpointQuery;
use Model\System\DataSourceQuery as ChildDataSourceQuery;
use Model\System\Map\DataSourceEndpointTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'datasource_endpoint' table.
 *
 *
 *
 * @package    propel.generator.Model.System.Base
 */
abstract class DataSourceEndpoint implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\System\\Map\\DataSourceEndpointTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the datasource_id field.
     *
     * @var        int
     */
    protected $datasource_id;

    /**
     * The value for the datasource_endpoint_method_id field.
     *
     * @var        int
     */
    protected $datasource_endpoint_method_id;

    /**
     * The value for the code field.
     *
     * @var        string
     */
    protected $code;

    /**
     * The value for the summary field.
     *
     * @var        string
     */
    protected $summary;

    /**
     * The value for the url field.
     *
     * @var        string
     */
    protected $url;

    /**
     * @var        ChildDataSourceEndpointMethod
     */
    protected $aDataSourceEndpointMethod;

    /**
     * @var        ChildDataSource
     */
    protected $aDataSource;

    /**
     * @var        ObjectCollection|MessageTranslation[] Collection to store aggregation of MessageTranslation objects.
     */
    protected $collMessageTranslationsRelatedByDatasourceEndpointFromId;
    protected $collMessageTranslationsRelatedByDatasourceEndpointFromIdPartial;

    /**
     * @var        ObjectCollection|MessageTranslation[] Collection to store aggregation of MessageTranslation objects.
     */
    protected $collMessageTranslationsRelatedByDatasourceEndpointToId;
    protected $collMessageTranslationsRelatedByDatasourceEndpointToIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|MessageTranslation[]
     */
    protected $messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|MessageTranslation[]
     */
    protected $messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion = null;

    /**
     * Initializes internal state of Model\System\Base\DataSourceEndpoint object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>DataSourceEndpoint</code> instance.  If
     * <code>obj</code> is an instance of <code>DataSourceEndpoint</code>, delegates to
     * <code>equals(DataSourceEndpoint)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [datasource_id] column value.
     *
     * @return int
     */
    public function getDatasourceId()
    {
        return $this->datasource_id;
    }

    /**
     * Get the [datasource_endpoint_method_id] column value.
     *
     * @return int
     */
    public function getDatasourceEndpointMethodId()
    {
        return $this->datasource_endpoint_method_id;
    }

    /**
     * Get the [code] column value.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get the [summary] column value.
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Get the [url] column value.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\DataSourceEndpoint The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[DataSourceEndpointTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [datasource_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\DataSourceEndpoint The current object (for fluent API support)
     */
    public function setDatasourceId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->datasource_id !== $v) {
            $this->datasource_id = $v;
            $this->modifiedColumns[DataSourceEndpointTableMap::COL_DATASOURCE_ID] = true;
        }

        if ($this->aDataSource !== null && $this->aDataSource->getId() !== $v) {
            $this->aDataSource = null;
        }

        return $this;
    } // setDatasourceId()

    /**
     * Set the value of [datasource_endpoint_method_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\DataSourceEndpoint The current object (for fluent API support)
     */
    public function setDatasourceEndpointMethodId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->datasource_endpoint_method_id !== $v) {
            $this->datasource_endpoint_method_id = $v;
            $this->modifiedColumns[DataSourceEndpointTableMap::COL_DATASOURCE_ENDPOINT_METHOD_ID] = true;
        }

        if ($this->aDataSourceEndpointMethod !== null && $this->aDataSourceEndpointMethod->getId() !== $v) {
            $this->aDataSourceEndpointMethod = null;
        }

        return $this;
    } // setDatasourceEndpointMethodId()

    /**
     * Set the value of [code] column.
     *
     * @param string $v New value
     * @return $this|\Model\System\DataSourceEndpoint The current object (for fluent API support)
     */
    public function setCode($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->code !== $v) {
            $this->code = $v;
            $this->modifiedColumns[DataSourceEndpointTableMap::COL_CODE] = true;
        }

        return $this;
    } // setCode()

    /**
     * Set the value of [summary] column.
     *
     * @param string $v New value
     * @return $this|\Model\System\DataSourceEndpoint The current object (for fluent API support)
     */
    public function setSummary($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->summary !== $v) {
            $this->summary = $v;
            $this->modifiedColumns[DataSourceEndpointTableMap::COL_SUMMARY] = true;
        }

        return $this;
    } // setSummary()

    /**
     * Set the value of [url] column.
     *
     * @param string $v New value
     * @return $this|\Model\System\DataSourceEndpoint The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[DataSourceEndpointTableMap::COL_URL] = true;
        }

        return $this;
    } // setUrl()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : DataSourceEndpointTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : DataSourceEndpointTableMap::translateFieldName('DatasourceId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->datasource_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : DataSourceEndpointTableMap::translateFieldName('DatasourceEndpointMethodId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->datasource_endpoint_method_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : DataSourceEndpointTableMap::translateFieldName('Code', TableMap::TYPE_PHPNAME, $indexType)];
            $this->code = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : DataSourceEndpointTableMap::translateFieldName('Summary', TableMap::TYPE_PHPNAME, $indexType)];
            $this->summary = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : DataSourceEndpointTableMap::translateFieldName('Url', TableMap::TYPE_PHPNAME, $indexType)];
            $this->url = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 6; // 6 = DataSourceEndpointTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\System\\DataSourceEndpoint'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aDataSource !== null && $this->datasource_id !== $this->aDataSource->getId()) {
            $this->aDataSource = null;
        }
        if ($this->aDataSourceEndpointMethod !== null && $this->datasource_endpoint_method_id !== $this->aDataSourceEndpointMethod->getId()) {
            $this->aDataSourceEndpointMethod = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DataSourceEndpointTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildDataSourceEndpointQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aDataSourceEndpointMethod = null;
            $this->aDataSource = null;
            $this->collMessageTranslationsRelatedByDatasourceEndpointFromId = null;

            $this->collMessageTranslationsRelatedByDatasourceEndpointToId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see DataSourceEndpoint::setDeleted()
     * @see DataSourceEndpoint::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(DataSourceEndpointTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildDataSourceEndpointQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(DataSourceEndpointTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                DataSourceEndpointTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aDataSourceEndpointMethod !== null) {
                if ($this->aDataSourceEndpointMethod->isModified() || $this->aDataSourceEndpointMethod->isNew()) {
                    $affectedRows += $this->aDataSourceEndpointMethod->save($con);
                }
                $this->setDataSourceEndpointMethod($this->aDataSourceEndpointMethod);
            }

            if ($this->aDataSource !== null) {
                if ($this->aDataSource->isModified() || $this->aDataSource->isNew()) {
                    $affectedRows += $this->aDataSource->save($con);
                }
                $this->setDataSource($this->aDataSource);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion !== null) {
                if (!$this->messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion->isEmpty()) {
                    \Model\Custom\NovumOverheid\MessageTranslationQuery::create()
                        ->filterByPrimaryKeys($this->messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion = null;
                }
            }

            if ($this->collMessageTranslationsRelatedByDatasourceEndpointFromId !== null) {
                foreach ($this->collMessageTranslationsRelatedByDatasourceEndpointFromId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion !== null) {
                if (!$this->messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion->isEmpty()) {
                    \Model\Custom\NovumOverheid\MessageTranslationQuery::create()
                        ->filterByPrimaryKeys($this->messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion = null;
                }
            }

            if ($this->collMessageTranslationsRelatedByDatasourceEndpointToId !== null) {
                foreach ($this->collMessageTranslationsRelatedByDatasourceEndpointToId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[DataSourceEndpointTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . DataSourceEndpointTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(DataSourceEndpointTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(DataSourceEndpointTableMap::COL_DATASOURCE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'datasource_id';
        }
        if ($this->isColumnModified(DataSourceEndpointTableMap::COL_DATASOURCE_ENDPOINT_METHOD_ID)) {
            $modifiedColumns[':p' . $index++]  = 'datasource_endpoint_method_id';
        }
        if ($this->isColumnModified(DataSourceEndpointTableMap::COL_CODE)) {
            $modifiedColumns[':p' . $index++]  = 'code';
        }
        if ($this->isColumnModified(DataSourceEndpointTableMap::COL_SUMMARY)) {
            $modifiedColumns[':p' . $index++]  = 'summary';
        }
        if ($this->isColumnModified(DataSourceEndpointTableMap::COL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'url';
        }

        $sql = sprintf(
            'INSERT INTO datasource_endpoint (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'datasource_id':
                        $stmt->bindValue($identifier, $this->datasource_id, PDO::PARAM_INT);
                        break;
                    case 'datasource_endpoint_method_id':
                        $stmt->bindValue($identifier, $this->datasource_endpoint_method_id, PDO::PARAM_INT);
                        break;
                    case 'code':
                        $stmt->bindValue($identifier, $this->code, PDO::PARAM_STR);
                        break;
                    case 'summary':
                        $stmt->bindValue($identifier, $this->summary, PDO::PARAM_STR);
                        break;
                    case 'url':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = DataSourceEndpointTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getDatasourceId();
                break;
            case 2:
                return $this->getDatasourceEndpointMethodId();
                break;
            case 3:
                return $this->getCode();
                break;
            case 4:
                return $this->getSummary();
                break;
            case 5:
                return $this->getUrl();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['DataSourceEndpoint'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['DataSourceEndpoint'][$this->hashCode()] = true;
        $keys = DataSourceEndpointTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getDatasourceId(),
            $keys[2] => $this->getDatasourceEndpointMethodId(),
            $keys[3] => $this->getCode(),
            $keys[4] => $this->getSummary(),
            $keys[5] => $this->getUrl(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aDataSourceEndpointMethod) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dataSourceEndpointMethod';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'datasource_endpoint_method';
                        break;
                    default:
                        $key = 'DataSourceEndpointMethod';
                }

                $result[$key] = $this->aDataSourceEndpointMethod->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aDataSource) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dataSource';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'datasource';
                        break;
                    default:
                        $key = 'DataSource';
                }

                $result[$key] = $this->aDataSource->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collMessageTranslationsRelatedByDatasourceEndpointFromId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'messageTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'message_translations';
                        break;
                    default:
                        $key = 'MessageTranslations';
                }

                $result[$key] = $this->collMessageTranslationsRelatedByDatasourceEndpointFromId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collMessageTranslationsRelatedByDatasourceEndpointToId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'messageTranslations';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'message_translations';
                        break;
                    default:
                        $key = 'MessageTranslations';
                }

                $result[$key] = $this->collMessageTranslationsRelatedByDatasourceEndpointToId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\System\DataSourceEndpoint
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = DataSourceEndpointTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\System\DataSourceEndpoint
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setDatasourceId($value);
                break;
            case 2:
                $this->setDatasourceEndpointMethodId($value);
                break;
            case 3:
                $this->setCode($value);
                break;
            case 4:
                $this->setSummary($value);
                break;
            case 5:
                $this->setUrl($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = DataSourceEndpointTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setDatasourceId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setDatasourceEndpointMethodId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setCode($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSummary($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setUrl($arr[$keys[5]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\System\DataSourceEndpoint The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(DataSourceEndpointTableMap::DATABASE_NAME);

        if ($this->isColumnModified(DataSourceEndpointTableMap::COL_ID)) {
            $criteria->add(DataSourceEndpointTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(DataSourceEndpointTableMap::COL_DATASOURCE_ID)) {
            $criteria->add(DataSourceEndpointTableMap::COL_DATASOURCE_ID, $this->datasource_id);
        }
        if ($this->isColumnModified(DataSourceEndpointTableMap::COL_DATASOURCE_ENDPOINT_METHOD_ID)) {
            $criteria->add(DataSourceEndpointTableMap::COL_DATASOURCE_ENDPOINT_METHOD_ID, $this->datasource_endpoint_method_id);
        }
        if ($this->isColumnModified(DataSourceEndpointTableMap::COL_CODE)) {
            $criteria->add(DataSourceEndpointTableMap::COL_CODE, $this->code);
        }
        if ($this->isColumnModified(DataSourceEndpointTableMap::COL_SUMMARY)) {
            $criteria->add(DataSourceEndpointTableMap::COL_SUMMARY, $this->summary);
        }
        if ($this->isColumnModified(DataSourceEndpointTableMap::COL_URL)) {
            $criteria->add(DataSourceEndpointTableMap::COL_URL, $this->url);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildDataSourceEndpointQuery::create();
        $criteria->add(DataSourceEndpointTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\System\DataSourceEndpoint (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setDatasourceId($this->getDatasourceId());
        $copyObj->setDatasourceEndpointMethodId($this->getDatasourceEndpointMethodId());
        $copyObj->setCode($this->getCode());
        $copyObj->setSummary($this->getSummary());
        $copyObj->setUrl($this->getUrl());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getMessageTranslationsRelatedByDatasourceEndpointFromId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMessageTranslationRelatedByDatasourceEndpointFromId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getMessageTranslationsRelatedByDatasourceEndpointToId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addMessageTranslationRelatedByDatasourceEndpointToId($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\System\DataSourceEndpoint Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildDataSourceEndpointMethod object.
     *
     * @param  ChildDataSourceEndpointMethod $v
     * @return $this|\Model\System\DataSourceEndpoint The current object (for fluent API support)
     * @throws PropelException
     */
    public function setDataSourceEndpointMethod(ChildDataSourceEndpointMethod $v = null)
    {
        if ($v === null) {
            $this->setDatasourceEndpointMethodId(NULL);
        } else {
            $this->setDatasourceEndpointMethodId($v->getId());
        }

        $this->aDataSourceEndpointMethod = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildDataSourceEndpointMethod object, it will not be re-added.
        if ($v !== null) {
            $v->addDataSourceEndpoint($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildDataSourceEndpointMethod object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildDataSourceEndpointMethod The associated ChildDataSourceEndpointMethod object.
     * @throws PropelException
     */
    public function getDataSourceEndpointMethod(ConnectionInterface $con = null)
    {
        if ($this->aDataSourceEndpointMethod === null && ($this->datasource_endpoint_method_id != 0)) {
            $this->aDataSourceEndpointMethod = ChildDataSourceEndpointMethodQuery::create()->findPk($this->datasource_endpoint_method_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aDataSourceEndpointMethod->addDataSourceEndpoints($this);
             */
        }

        return $this->aDataSourceEndpointMethod;
    }

    /**
     * Declares an association between this object and a ChildDataSource object.
     *
     * @param  ChildDataSource $v
     * @return $this|\Model\System\DataSourceEndpoint The current object (for fluent API support)
     * @throws PropelException
     */
    public function setDataSource(ChildDataSource $v = null)
    {
        if ($v === null) {
            $this->setDatasourceId(NULL);
        } else {
            $this->setDatasourceId($v->getId());
        }

        $this->aDataSource = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildDataSource object, it will not be re-added.
        if ($v !== null) {
            $v->addDataSourceEndpoint($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildDataSource object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildDataSource The associated ChildDataSource object.
     * @throws PropelException
     */
    public function getDataSource(ConnectionInterface $con = null)
    {
        if ($this->aDataSource === null && ($this->datasource_id != 0)) {
            $this->aDataSource = ChildDataSourceQuery::create()->findPk($this->datasource_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aDataSource->addDataSourceEndpoints($this);
             */
        }

        return $this->aDataSource;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('MessageTranslationRelatedByDatasourceEndpointFromId' === $relationName) {
            $this->initMessageTranslationsRelatedByDatasourceEndpointFromId();
            return;
        }
        if ('MessageTranslationRelatedByDatasourceEndpointToId' === $relationName) {
            $this->initMessageTranslationsRelatedByDatasourceEndpointToId();
            return;
        }
    }

    /**
     * Clears out the collMessageTranslationsRelatedByDatasourceEndpointFromId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMessageTranslationsRelatedByDatasourceEndpointFromId()
     */
    public function clearMessageTranslationsRelatedByDatasourceEndpointFromId()
    {
        $this->collMessageTranslationsRelatedByDatasourceEndpointFromId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMessageTranslationsRelatedByDatasourceEndpointFromId collection loaded partially.
     */
    public function resetPartialMessageTranslationsRelatedByDatasourceEndpointFromId($v = true)
    {
        $this->collMessageTranslationsRelatedByDatasourceEndpointFromIdPartial = $v;
    }

    /**
     * Initializes the collMessageTranslationsRelatedByDatasourceEndpointFromId collection.
     *
     * By default this just sets the collMessageTranslationsRelatedByDatasourceEndpointFromId collection to an empty array (like clearcollMessageTranslationsRelatedByDatasourceEndpointFromId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMessageTranslationsRelatedByDatasourceEndpointFromId($overrideExisting = true)
    {
        if (null !== $this->collMessageTranslationsRelatedByDatasourceEndpointFromId && !$overrideExisting) {
            return;
        }

        $collectionClassName = MessageTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collMessageTranslationsRelatedByDatasourceEndpointFromId = new $collectionClassName;
        $this->collMessageTranslationsRelatedByDatasourceEndpointFromId->setModel('\Model\Custom\NovumOverheid\MessageTranslation');
    }

    /**
     * Gets an array of MessageTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDataSourceEndpoint is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|MessageTranslation[] List of MessageTranslation objects
     * @throws PropelException
     */
    public function getMessageTranslationsRelatedByDatasourceEndpointFromId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMessageTranslationsRelatedByDatasourceEndpointFromIdPartial && !$this->isNew();
        if (null === $this->collMessageTranslationsRelatedByDatasourceEndpointFromId || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collMessageTranslationsRelatedByDatasourceEndpointFromId) {
                    $this->initMessageTranslationsRelatedByDatasourceEndpointFromId();
                } else {
                    $collectionClassName = MessageTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collMessageTranslationsRelatedByDatasourceEndpointFromId = new $collectionClassName;
                    $collMessageTranslationsRelatedByDatasourceEndpointFromId->setModel('\Model\Custom\NovumOverheid\MessageTranslation');

                    return $collMessageTranslationsRelatedByDatasourceEndpointFromId;
                }
            } else {
                $collMessageTranslationsRelatedByDatasourceEndpointFromId = MessageTranslationQuery::create(null, $criteria)
                    ->filterByDataSourceEndpointFrom($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMessageTranslationsRelatedByDatasourceEndpointFromIdPartial && count($collMessageTranslationsRelatedByDatasourceEndpointFromId)) {
                        $this->initMessageTranslationsRelatedByDatasourceEndpointFromId(false);

                        foreach ($collMessageTranslationsRelatedByDatasourceEndpointFromId as $obj) {
                            if (false == $this->collMessageTranslationsRelatedByDatasourceEndpointFromId->contains($obj)) {
                                $this->collMessageTranslationsRelatedByDatasourceEndpointFromId->append($obj);
                            }
                        }

                        $this->collMessageTranslationsRelatedByDatasourceEndpointFromIdPartial = true;
                    }

                    return $collMessageTranslationsRelatedByDatasourceEndpointFromId;
                }

                if ($partial && $this->collMessageTranslationsRelatedByDatasourceEndpointFromId) {
                    foreach ($this->collMessageTranslationsRelatedByDatasourceEndpointFromId as $obj) {
                        if ($obj->isNew()) {
                            $collMessageTranslationsRelatedByDatasourceEndpointFromId[] = $obj;
                        }
                    }
                }

                $this->collMessageTranslationsRelatedByDatasourceEndpointFromId = $collMessageTranslationsRelatedByDatasourceEndpointFromId;
                $this->collMessageTranslationsRelatedByDatasourceEndpointFromIdPartial = false;
            }
        }

        return $this->collMessageTranslationsRelatedByDatasourceEndpointFromId;
    }

    /**
     * Sets a collection of MessageTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $messageTranslationsRelatedByDatasourceEndpointFromId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDataSourceEndpoint The current object (for fluent API support)
     */
    public function setMessageTranslationsRelatedByDatasourceEndpointFromId(Collection $messageTranslationsRelatedByDatasourceEndpointFromId, ConnectionInterface $con = null)
    {
        /** @var MessageTranslation[] $messageTranslationsRelatedByDatasourceEndpointFromIdToDelete */
        $messageTranslationsRelatedByDatasourceEndpointFromIdToDelete = $this->getMessageTranslationsRelatedByDatasourceEndpointFromId(new Criteria(), $con)->diff($messageTranslationsRelatedByDatasourceEndpointFromId);


        $this->messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion = $messageTranslationsRelatedByDatasourceEndpointFromIdToDelete;

        foreach ($messageTranslationsRelatedByDatasourceEndpointFromIdToDelete as $messageTranslationRelatedByDatasourceEndpointFromIdRemoved) {
            $messageTranslationRelatedByDatasourceEndpointFromIdRemoved->setDataSourceEndpointFrom(null);
        }

        $this->collMessageTranslationsRelatedByDatasourceEndpointFromId = null;
        foreach ($messageTranslationsRelatedByDatasourceEndpointFromId as $messageTranslationRelatedByDatasourceEndpointFromId) {
            $this->addMessageTranslationRelatedByDatasourceEndpointFromId($messageTranslationRelatedByDatasourceEndpointFromId);
        }

        $this->collMessageTranslationsRelatedByDatasourceEndpointFromId = $messageTranslationsRelatedByDatasourceEndpointFromId;
        $this->collMessageTranslationsRelatedByDatasourceEndpointFromIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseMessageTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseMessageTranslation objects.
     * @throws PropelException
     */
    public function countMessageTranslationsRelatedByDatasourceEndpointFromId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMessageTranslationsRelatedByDatasourceEndpointFromIdPartial && !$this->isNew();
        if (null === $this->collMessageTranslationsRelatedByDatasourceEndpointFromId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMessageTranslationsRelatedByDatasourceEndpointFromId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMessageTranslationsRelatedByDatasourceEndpointFromId());
            }

            $query = MessageTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDataSourceEndpointFrom($this)
                ->count($con);
        }

        return count($this->collMessageTranslationsRelatedByDatasourceEndpointFromId);
    }

    /**
     * Method called to associate a MessageTranslation object to this object
     * through the MessageTranslation foreign key attribute.
     *
     * @param  MessageTranslation $l MessageTranslation
     * @return $this|\Model\System\DataSourceEndpoint The current object (for fluent API support)
     */
    public function addMessageTranslationRelatedByDatasourceEndpointFromId(MessageTranslation $l)
    {
        if ($this->collMessageTranslationsRelatedByDatasourceEndpointFromId === null) {
            $this->initMessageTranslationsRelatedByDatasourceEndpointFromId();
            $this->collMessageTranslationsRelatedByDatasourceEndpointFromIdPartial = true;
        }

        if (!$this->collMessageTranslationsRelatedByDatasourceEndpointFromId->contains($l)) {
            $this->doAddMessageTranslationRelatedByDatasourceEndpointFromId($l);

            if ($this->messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion and $this->messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion->contains($l)) {
                $this->messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion->remove($this->messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param MessageTranslation $messageTranslationRelatedByDatasourceEndpointFromId The MessageTranslation object to add.
     */
    protected function doAddMessageTranslationRelatedByDatasourceEndpointFromId(MessageTranslation $messageTranslationRelatedByDatasourceEndpointFromId)
    {
        $this->collMessageTranslationsRelatedByDatasourceEndpointFromId[]= $messageTranslationRelatedByDatasourceEndpointFromId;
        $messageTranslationRelatedByDatasourceEndpointFromId->setDataSourceEndpointFrom($this);
    }

    /**
     * @param  MessageTranslation $messageTranslationRelatedByDatasourceEndpointFromId The MessageTranslation object to remove.
     * @return $this|ChildDataSourceEndpoint The current object (for fluent API support)
     */
    public function removeMessageTranslationRelatedByDatasourceEndpointFromId(MessageTranslation $messageTranslationRelatedByDatasourceEndpointFromId)
    {
        if ($this->getMessageTranslationsRelatedByDatasourceEndpointFromId()->contains($messageTranslationRelatedByDatasourceEndpointFromId)) {
            $pos = $this->collMessageTranslationsRelatedByDatasourceEndpointFromId->search($messageTranslationRelatedByDatasourceEndpointFromId);
            $this->collMessageTranslationsRelatedByDatasourceEndpointFromId->remove($pos);
            if (null === $this->messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion) {
                $this->messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion = clone $this->collMessageTranslationsRelatedByDatasourceEndpointFromId;
                $this->messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion->clear();
            }
            $this->messageTranslationsRelatedByDatasourceEndpointFromIdScheduledForDeletion[]= clone $messageTranslationRelatedByDatasourceEndpointFromId;
            $messageTranslationRelatedByDatasourceEndpointFromId->setDataSourceEndpointFrom(null);
        }

        return $this;
    }

    /**
     * Clears out the collMessageTranslationsRelatedByDatasourceEndpointToId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addMessageTranslationsRelatedByDatasourceEndpointToId()
     */
    public function clearMessageTranslationsRelatedByDatasourceEndpointToId()
    {
        $this->collMessageTranslationsRelatedByDatasourceEndpointToId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collMessageTranslationsRelatedByDatasourceEndpointToId collection loaded partially.
     */
    public function resetPartialMessageTranslationsRelatedByDatasourceEndpointToId($v = true)
    {
        $this->collMessageTranslationsRelatedByDatasourceEndpointToIdPartial = $v;
    }

    /**
     * Initializes the collMessageTranslationsRelatedByDatasourceEndpointToId collection.
     *
     * By default this just sets the collMessageTranslationsRelatedByDatasourceEndpointToId collection to an empty array (like clearcollMessageTranslationsRelatedByDatasourceEndpointToId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initMessageTranslationsRelatedByDatasourceEndpointToId($overrideExisting = true)
    {
        if (null !== $this->collMessageTranslationsRelatedByDatasourceEndpointToId && !$overrideExisting) {
            return;
        }

        $collectionClassName = MessageTranslationTableMap::getTableMap()->getCollectionClassName();

        $this->collMessageTranslationsRelatedByDatasourceEndpointToId = new $collectionClassName;
        $this->collMessageTranslationsRelatedByDatasourceEndpointToId->setModel('\Model\Custom\NovumOverheid\MessageTranslation');
    }

    /**
     * Gets an array of MessageTranslation objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDataSourceEndpoint is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|MessageTranslation[] List of MessageTranslation objects
     * @throws PropelException
     */
    public function getMessageTranslationsRelatedByDatasourceEndpointToId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collMessageTranslationsRelatedByDatasourceEndpointToIdPartial && !$this->isNew();
        if (null === $this->collMessageTranslationsRelatedByDatasourceEndpointToId || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collMessageTranslationsRelatedByDatasourceEndpointToId) {
                    $this->initMessageTranslationsRelatedByDatasourceEndpointToId();
                } else {
                    $collectionClassName = MessageTranslationTableMap::getTableMap()->getCollectionClassName();

                    $collMessageTranslationsRelatedByDatasourceEndpointToId = new $collectionClassName;
                    $collMessageTranslationsRelatedByDatasourceEndpointToId->setModel('\Model\Custom\NovumOverheid\MessageTranslation');

                    return $collMessageTranslationsRelatedByDatasourceEndpointToId;
                }
            } else {
                $collMessageTranslationsRelatedByDatasourceEndpointToId = MessageTranslationQuery::create(null, $criteria)
                    ->filterByDataSourceEndpointTo($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collMessageTranslationsRelatedByDatasourceEndpointToIdPartial && count($collMessageTranslationsRelatedByDatasourceEndpointToId)) {
                        $this->initMessageTranslationsRelatedByDatasourceEndpointToId(false);

                        foreach ($collMessageTranslationsRelatedByDatasourceEndpointToId as $obj) {
                            if (false == $this->collMessageTranslationsRelatedByDatasourceEndpointToId->contains($obj)) {
                                $this->collMessageTranslationsRelatedByDatasourceEndpointToId->append($obj);
                            }
                        }

                        $this->collMessageTranslationsRelatedByDatasourceEndpointToIdPartial = true;
                    }

                    return $collMessageTranslationsRelatedByDatasourceEndpointToId;
                }

                if ($partial && $this->collMessageTranslationsRelatedByDatasourceEndpointToId) {
                    foreach ($this->collMessageTranslationsRelatedByDatasourceEndpointToId as $obj) {
                        if ($obj->isNew()) {
                            $collMessageTranslationsRelatedByDatasourceEndpointToId[] = $obj;
                        }
                    }
                }

                $this->collMessageTranslationsRelatedByDatasourceEndpointToId = $collMessageTranslationsRelatedByDatasourceEndpointToId;
                $this->collMessageTranslationsRelatedByDatasourceEndpointToIdPartial = false;
            }
        }

        return $this->collMessageTranslationsRelatedByDatasourceEndpointToId;
    }

    /**
     * Sets a collection of MessageTranslation objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $messageTranslationsRelatedByDatasourceEndpointToId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDataSourceEndpoint The current object (for fluent API support)
     */
    public function setMessageTranslationsRelatedByDatasourceEndpointToId(Collection $messageTranslationsRelatedByDatasourceEndpointToId, ConnectionInterface $con = null)
    {
        /** @var MessageTranslation[] $messageTranslationsRelatedByDatasourceEndpointToIdToDelete */
        $messageTranslationsRelatedByDatasourceEndpointToIdToDelete = $this->getMessageTranslationsRelatedByDatasourceEndpointToId(new Criteria(), $con)->diff($messageTranslationsRelatedByDatasourceEndpointToId);


        $this->messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion = $messageTranslationsRelatedByDatasourceEndpointToIdToDelete;

        foreach ($messageTranslationsRelatedByDatasourceEndpointToIdToDelete as $messageTranslationRelatedByDatasourceEndpointToIdRemoved) {
            $messageTranslationRelatedByDatasourceEndpointToIdRemoved->setDataSourceEndpointTo(null);
        }

        $this->collMessageTranslationsRelatedByDatasourceEndpointToId = null;
        foreach ($messageTranslationsRelatedByDatasourceEndpointToId as $messageTranslationRelatedByDatasourceEndpointToId) {
            $this->addMessageTranslationRelatedByDatasourceEndpointToId($messageTranslationRelatedByDatasourceEndpointToId);
        }

        $this->collMessageTranslationsRelatedByDatasourceEndpointToId = $messageTranslationsRelatedByDatasourceEndpointToId;
        $this->collMessageTranslationsRelatedByDatasourceEndpointToIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseMessageTranslation objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseMessageTranslation objects.
     * @throws PropelException
     */
    public function countMessageTranslationsRelatedByDatasourceEndpointToId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collMessageTranslationsRelatedByDatasourceEndpointToIdPartial && !$this->isNew();
        if (null === $this->collMessageTranslationsRelatedByDatasourceEndpointToId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collMessageTranslationsRelatedByDatasourceEndpointToId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getMessageTranslationsRelatedByDatasourceEndpointToId());
            }

            $query = MessageTranslationQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDataSourceEndpointTo($this)
                ->count($con);
        }

        return count($this->collMessageTranslationsRelatedByDatasourceEndpointToId);
    }

    /**
     * Method called to associate a MessageTranslation object to this object
     * through the MessageTranslation foreign key attribute.
     *
     * @param  MessageTranslation $l MessageTranslation
     * @return $this|\Model\System\DataSourceEndpoint The current object (for fluent API support)
     */
    public function addMessageTranslationRelatedByDatasourceEndpointToId(MessageTranslation $l)
    {
        if ($this->collMessageTranslationsRelatedByDatasourceEndpointToId === null) {
            $this->initMessageTranslationsRelatedByDatasourceEndpointToId();
            $this->collMessageTranslationsRelatedByDatasourceEndpointToIdPartial = true;
        }

        if (!$this->collMessageTranslationsRelatedByDatasourceEndpointToId->contains($l)) {
            $this->doAddMessageTranslationRelatedByDatasourceEndpointToId($l);

            if ($this->messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion and $this->messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion->contains($l)) {
                $this->messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion->remove($this->messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param MessageTranslation $messageTranslationRelatedByDatasourceEndpointToId The MessageTranslation object to add.
     */
    protected function doAddMessageTranslationRelatedByDatasourceEndpointToId(MessageTranslation $messageTranslationRelatedByDatasourceEndpointToId)
    {
        $this->collMessageTranslationsRelatedByDatasourceEndpointToId[]= $messageTranslationRelatedByDatasourceEndpointToId;
        $messageTranslationRelatedByDatasourceEndpointToId->setDataSourceEndpointTo($this);
    }

    /**
     * @param  MessageTranslation $messageTranslationRelatedByDatasourceEndpointToId The MessageTranslation object to remove.
     * @return $this|ChildDataSourceEndpoint The current object (for fluent API support)
     */
    public function removeMessageTranslationRelatedByDatasourceEndpointToId(MessageTranslation $messageTranslationRelatedByDatasourceEndpointToId)
    {
        if ($this->getMessageTranslationsRelatedByDatasourceEndpointToId()->contains($messageTranslationRelatedByDatasourceEndpointToId)) {
            $pos = $this->collMessageTranslationsRelatedByDatasourceEndpointToId->search($messageTranslationRelatedByDatasourceEndpointToId);
            $this->collMessageTranslationsRelatedByDatasourceEndpointToId->remove($pos);
            if (null === $this->messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion) {
                $this->messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion = clone $this->collMessageTranslationsRelatedByDatasourceEndpointToId;
                $this->messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion->clear();
            }
            $this->messageTranslationsRelatedByDatasourceEndpointToIdScheduledForDeletion[]= clone $messageTranslationRelatedByDatasourceEndpointToId;
            $messageTranslationRelatedByDatasourceEndpointToId->setDataSourceEndpointTo(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aDataSourceEndpointMethod) {
            $this->aDataSourceEndpointMethod->removeDataSourceEndpoint($this);
        }
        if (null !== $this->aDataSource) {
            $this->aDataSource->removeDataSourceEndpoint($this);
        }
        $this->id = null;
        $this->datasource_id = null;
        $this->datasource_endpoint_method_id = null;
        $this->code = null;
        $this->summary = null;
        $this->url = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collMessageTranslationsRelatedByDatasourceEndpointFromId) {
                foreach ($this->collMessageTranslationsRelatedByDatasourceEndpointFromId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collMessageTranslationsRelatedByDatasourceEndpointToId) {
                foreach ($this->collMessageTranslationsRelatedByDatasourceEndpointToId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collMessageTranslationsRelatedByDatasourceEndpointFromId = null;
        $this->collMessageTranslationsRelatedByDatasourceEndpointToId = null;
        $this->aDataSourceEndpointMethod = null;
        $this->aDataSource = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(DataSourceEndpointTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
