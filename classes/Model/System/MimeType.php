<?php

namespace Model\System;

use Model\System\Base\MimeType as BaseMimeType;

/**
 * Skeleton subclass for representing a row from the 'mime_type' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class MimeType extends BaseMimeType
{

}
