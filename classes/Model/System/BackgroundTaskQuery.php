<?php

namespace Model\System;

use Model\System\Base\BackgroundTaskQuery as BaseBackgroundTaskQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'background_task' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BackgroundTaskQuery extends BaseBackgroundTaskQuery
{

}
