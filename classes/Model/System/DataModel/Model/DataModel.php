<?php

namespace Model\System\DataModel\Model;

use AdminModules\IModuleConfig;
use Core\Utils;
use Model\Module\Module;
use Model\Module\ModuleQuery;
use Model\System\DataModel\DataTypeQuery;
use Model\System\DataModel\Field\DataField;
use Model\System\DataModel\Field\DataFieldQuery;
use Model\System\DataModel\Model\Base\DataModel as BaseDataModel;
use LowCode\Component\IComponent;
use Model\System\FormFieldTypeQuery;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Skeleton subclass for representing a row from the 'data_model' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DataModel extends BaseDataModel
{
    /**
     * @param string $sTableName
     * @param string $sNamespace
     * @param string $sTitle
     * @param string $sApiDesc
     * @param bool $bIsUiComponent
     * @return DataModel
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public static function makeTable(string $sTableName, string $sNamespace, string $sTitle, string $sApiDesc, bool $bIsUiComponent = false)
    {
        $oDataModelQuery = DataModelQuery::create();
        $oDataModelQuery->filterByName($sTableName);
        $oDataModel = $oDataModelQuery->findOneOrCreate();
        $oDataModel->setNamespaceName($sNamespace);
        $oModule = ModuleQuery::getUiComponentModule();
        $oDataModel->setModuleId($oModule->getId());
        $oDataModel->setTitle($sTitle);

        $oDataModel->setPhpName(ucfirst($sTableName));
        $oDataModel->setApiExposed(true);
        $oDataModel->setApiDescription($sApiDesc);
        $oDataModel->setIsPersistent(true);
        $oDataModel->setIsUiComponent($bIsUiComponent);
        return $oDataModel;
    }

    /**
     * @param string $sTableName
     * @param string $sNamespace
     * @param bool $bIsUiComponent
     * @return DataModel
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public static function makeLookupTable(string $sTableName, string $sNamespace, bool $bIsUiComponent = false):DataModel
    {
        $sTitle = 'This table contains lookup values for the fk_' . $sTableName . ' property' ;
        $sApiDesc = 'Used for validation and to populate dropdown values.';

        $oDataModel = self::makeTable($sTableName, $sNamespace, $sTitle, $sApiDesc, $bIsUiComponent);

        return $oDataModel;
    }

    /**
     * @return ModelCriteria
     */
    public function getPropelQueryObject():ModelCriteria
    {
        $sFqn = $this->getNamespaceName() . '\\' . $this->getPhpName() . 'Query';
        return $sFqn::create();
    }
    public function getPropelModel() : ActiveRecordInterface
    {
        $sFqn = $this->getNamespaceName() . '\\' . $this->getPhpName();
        return new $sFqn;
    }

    /**
     * @param IComponent $oComponent
     * @return DataModel
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public static function fromUiComponent(IComponent $oComponent):DataModel
    {
        $oDataModelQuery = DataModelQuery::create();
        $oDataModelQuery->filterByName($oComponent->getModelName());
        $oDataModel = $oDataModelQuery->findOneOrCreate();
        $oDataModel->setNamespaceName('Model\\System\\LowCode\\' . Utils::camelCase($oComponent->getComponentXml()->getId()));
        $oModule = ModuleQuery::getUiComponentModule();
        $oDataModel->setModuleId($oModule->getId());
        $oDataModel->setTitle("Deze tabel bevat component configuraties voor " . $oComponent->getName());
        $oDataModel->setPhpName('Component_' . $oComponent->getComponentXml()->getId());
        $oDataModel->setApiExposed(true);
        $oDataModel->setApiDescription("Bevat configuratiegegevens van " . $oComponent->getName(). " component, gebruikt bij het weergeven van apps.");
        $oDataModel->setIsPersistent(true);
        $oDataModel->setIsUiComponent(true);
        return $oDataModel;
    }

    /**
     * @return DataField
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function makeUiComponentColumn(OutputInterface $output):DataField
    {
        $sColumnName = 'ui_component_id';
        $output->writeln("<info>{$this->getName()}</info> needs a <info>{$sColumnName}</info> column");

        $oDataFieldQuery = DataFieldQuery::create();
        $oDataFieldQuery->filterByDataModelId($this->getId());
        $oDataFieldQuery->filterByName('ui_component_id');
        $oDataField = $oDataFieldQuery->findOneOrCreate();

        if($oDataField->isNew())
        {
            $output->writeln("<info>{$this->getName()}</info> does not have a <info>{$sColumnName}</info> column", OutputInterface::VERBOSITY_VERBOSE);
        }
        else
        {
            $output->writeln("<info>{$this->getName()}</info> already has a <info>{$sColumnName}</info> column", OutputInterface::VERBOSITY_VERBOSE);
        }

        $output->writeln("Setting properties of <info>{$sColumnName}</info> column", OutputInterface::VERBOSITY_VERBOSE);

        $oDataField->setPhpName('UiComponentId');
        $oDataField->setIconId('cogs');
        $oDataField->setLabel('UiComponent');
        $oDataType = DataTypeQuery::create()->findOneByName('INTEGER');
        $oDataField->setDataTypeId($this->getId());
        $oFormFieldType = FormFieldTypeQuery::create()->findOneByName('Lookup');
        $oDataField->setFormFieldTypeId($oFormFieldType->getId());
        $oDataField->setDataTypeId($oDataType->getId());
        $oDataField->setFormFieldLookups('Model\System\UI\UIComponent.label');
        $oDataField->setRequired(true);
        $oDataField->setIsPrimaryKey(false);
        $oDataField->setAutoIncrement(false);
        $oDataField->save();
        $output->writeln("<comment>Saved</comment> <info>{$sColumnName}</info>, it has id <info>{$oDataField->getId()}</info>", OutputInterface::VERBOSITY_VERBOSE);


        $oUiComponentModel = DataModelQuery::create()->findOneByName('ui_component');
        $oDataFieldTo = DataFieldQuery::create()->filterByDataModelId($oUiComponentModel->getId())->filterByName('id')->findOne();

        $oModelConstraintQuery = ModelConstraintQuery::create();
        $oModelConstraintQuery->filterByLocalFieldId($oDataField->getId());
        $oModelConstraintQuery->filterByForeignFieldId($oDataFieldTo->getId());
        $oModelConstraint = $oModelConstraintQuery->findOneOrCreate();
        $oModelConstraint->setPhpName(Utils::camelCase($oDataFieldTo->getName()));
        $oModelConstraint->setRefPhpName(Utils::camelCase($oDataField->getName()));
        $oModelConstraint->setOnDelete('cascade');
        $oModelConstraint->setOnUpdate('cascade');

        try {
            $oModelConstraint->save();
        }
        catch (\Exception $e) {

            echo "Datafield to " . json_encode($oDataField->toArray());
            echo "Datafield from " . json_encode($oDataFieldTo->toArray());
            echo "Could not save dataset " . json_encode($oModelConstraint->toArray());
            echo $e->getMessage();
            exit();
        }


        return $oDataField;
    }

    /**
     * @return DataField
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function makeDefaultPrimaryKeyColumn():DataField
    {
        $oDataFieldQuery = DataFieldQuery::create();
        $oDataFieldQuery->filterByDataModelId($this->getId());
        $oDataFieldQuery->filterByName('id');
        $oDataField = $oDataFieldQuery->findOneOrCreate();

        $oDataField->setName('id');
        $oDataField->setPhpName('id');
        $oDataField->setIconId('tag');
        $oDataField->setLabel('Id');
        $oDataType = DataTypeQuery::create()->findOneByName('INTEGER');
        $oDataField->setDataModelId($this->getId());
        $oDataField->setDataTypeId($oDataType->getId());
        $oDataField->setRequired(true);
        $oDataField->setIsPrimaryKey(true);
        $oDataField->setAutoIncrement(true);
        return $oDataField;
    }

    public function hasIdField()
    {
        $oDataFieldQuery = DataFieldQuery::create();
        $oDataFieldQuery->filterByDataModelId($this->getId());
        $oDataFieldQuery->filterByName('id');
        $oDataField = $oDataFieldQuery->findOne();
        return $oDataField instanceof DataField;
    }

    public function getModule():Module
    {
        if($this->getModuleId() && $oModule = ModuleQuery::create()->findOneById($this->getModuleId()))
        {
            return $oModule;
        }

        return new Module();
    }

    public function getModuleConfig():IModuleConfig
    {
        $sFqn = $this->getModule()->getName();

        return new $sFqn;
    }

    /**
     * Returns an array of tables that need to be created before this table because it relies on them for foreign key
     * constraints.
     *
     * @return DataModel[]
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function getDependencies() : array {

        $aTables = [];
        $aFields = $this->getDataFields();
        foreach($aFields as $oField)
        {
            foreach($oField->getModelConstraintsRelatedByLocalFieldId() as $oModelConstraint)
            {
                $oDataModel = $oModelConstraint->getFkLocalField()->getFkDataModel();
                $aTables[] = $oDataModel;
            }
        }
        return $aTables;
    }
}
