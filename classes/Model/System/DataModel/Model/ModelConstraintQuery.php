<?php

namespace Model\System\DataModel\Model;

use Model\System\DataModel\Field\DataField;
use Model\System\DataModel\Model\Base\ModelConstraintQuery as BaseModelConstraintQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'model_constraint' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ModelConstraintQuery extends BaseModelConstraintQuery
{
    function filterByLocalModelField(string $sLocalTableName, string $sLocalColumnName):ModelConstraintQuery
    {
        echo 'From: ' . $sLocalTableName . '.' . $sLocalColumnName . PHP_EOL;
        $oDataField = DataField::findOneByName($sLocalTableName, $sLocalColumnName);
        $this->filterByLocalFieldId($oDataField->getId());
        return $this;
    }
    function filterByForeignModelField(string $sForeignTable, string $sForeignColumnName):ModelConstraintQuery
    {
        echo 'To: ' .  $sForeignTable . '.' . $sForeignColumnName . PHP_EOL;
        $oDataField = DataField::findOneByName($sForeignTable, $sForeignColumnName);
        $this->filterByForeignFieldId($oDataField->getId());
        return $this;
    }
}
