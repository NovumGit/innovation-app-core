<?php

namespace Model\System\DataModel\Model;

use Model\System\DataModel\Model\Base\UniqueFieldGroupQuery as BaseUniqueFieldGroupQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'unique_field_group' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class UniqueFieldGroupQuery extends BaseUniqueFieldGroupQuery
{

}
