<?php

namespace Model\System\DataModel\Model;

use Model\System\DataModel\Model\Base\UniqueFieldGroupFieldQuery as BaseUniqueFieldGroupFieldQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'unique_field_group_field' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class UniqueFieldGroupFieldQuery extends BaseUniqueFieldGroupFieldQuery
{

}
