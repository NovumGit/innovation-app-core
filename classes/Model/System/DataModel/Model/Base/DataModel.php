<?php

namespace Model\System\DataModel\Model\Base;

use \Exception;
use \PDO;
use Model\Module\Module;
use Model\Module\ModuleQuery;
use Model\System\DataModel\Behavior\Behavior;
use Model\System\DataModel\Behavior\BehaviorQuery;
use Model\System\DataModel\Behavior\Base\Behavior as BaseBehavior;
use Model\System\DataModel\Behavior\Map\BehaviorTableMap;
use Model\System\DataModel\Field\DataField;
use Model\System\DataModel\Field\DataFieldQuery;
use Model\System\DataModel\Field\Base\DataField as BaseDataField;
use Model\System\DataModel\Field\Map\DataFieldTableMap;
use Model\System\DataModel\Model\DataModel as ChildDataModel;
use Model\System\DataModel\Model\DataModelQuery as ChildDataModelQuery;
use Model\System\DataModel\Model\UniqueFieldGroup as ChildUniqueFieldGroup;
use Model\System\DataModel\Model\UniqueFieldGroupQuery as ChildUniqueFieldGroupQuery;
use Model\System\DataModel\Model\Map\DataModelTableMap;
use Model\System\DataModel\Model\Map\UniqueFieldGroupTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'data_model' table.
 *
 *
 *
 * @package    propel.generator.Model.System.DataModel.Model.Base
 */
abstract class DataModel implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\System\\DataModel\\Model\\Map\\DataModelTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the name field.
     *
     * @var        string
     */
    protected $name;

    /**
     * The value for the namespace field.
     *
     * @var        string
     */
    protected $namespace;

    /**
     * The value for the php_name field.
     *
     * @var        string|null
     */
    protected $php_name;

    /**
     * The value for the module_id field.
     *
     * @var        int|null
     */
    protected $module_id;

    /**
     * The value for the title field.
     *
     * @var        string
     */
    protected $title;

    /**
     * The value for the api_exposed field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $api_exposed;

    /**
     * The value for the api_description field.
     *
     * @var        string
     */
    protected $api_description;

    /**
     * The value for the is_persistent field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_persistent;

    /**
     * The value for the is_ui_component field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_ui_component;

    /**
     * @var        Module
     */
    protected $aFkDataModel;

    /**
     * @var        ObjectCollection|Behavior[] Collection to store aggregation of Behavior objects.
     */
    protected $collBehaviors;
    protected $collBehaviorsPartial;

    /**
     * @var        ObjectCollection|DataField[] Collection to store aggregation of DataField objects.
     */
    protected $collDataFields;
    protected $collDataFieldsPartial;

    /**
     * @var        ObjectCollection|ChildUniqueFieldGroup[] Collection to store aggregation of ChildUniqueFieldGroup objects.
     */
    protected $collUniqueFieldGroups;
    protected $collUniqueFieldGroupsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|Behavior[]
     */
    protected $behaviorsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|DataField[]
     */
    protected $dataFieldsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUniqueFieldGroup[]
     */
    protected $uniqueFieldGroupsScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->api_exposed = false;
        $this->is_persistent = false;
        $this->is_ui_component = false;
    }

    /**
     * Initializes internal state of Model\System\DataModel\Model\Base\DataModel object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>DataModel</code> instance.  If
     * <code>obj</code> is an instance of <code>DataModel</code>, delegates to
     * <code>equals(DataModel)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [namespace] column value.
     *
     * @return string
     */
    public function getNamespaceName()
    {
        return $this->namespace;
    }

    /**
     * Get the [php_name] column value.
     *
     * @return string|null
     */
    public function getPhpName()
    {
        return $this->php_name;
    }

    /**
     * Get the [module_id] column value.
     *
     * @return int|null
     */
    public function getModuleId()
    {
        return $this->module_id;
    }

    /**
     * Get the [title] column value.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [api_exposed] column value.
     *
     * @return boolean
     */
    public function getApiExposed()
    {
        return $this->api_exposed;
    }

    /**
     * Get the [api_exposed] column value.
     *
     * @return boolean
     */
    public function isApiExposed()
    {
        return $this->getApiExposed();
    }

    /**
     * Get the [api_description] column value.
     *
     * @return string
     */
    public function getApiDescription()
    {
        return $this->api_description;
    }

    /**
     * Get the [is_persistent] column value.
     *
     * @return boolean
     */
    public function getIsPersistent()
    {
        return $this->is_persistent;
    }

    /**
     * Get the [is_persistent] column value.
     *
     * @return boolean
     */
    public function isPersistent()
    {
        return $this->getIsPersistent();
    }

    /**
     * Get the [is_ui_component] column value.
     *
     * @return boolean|null
     */
    public function getIsUiComponent()
    {
        return $this->is_ui_component;
    }

    /**
     * Get the [is_ui_component] column value.
     *
     * @return boolean|null
     */
    public function isUiComponent()
    {
        return $this->getIsUiComponent();
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[DataModelTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [name] column.
     *
     * @param string $v New value
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[DataModelTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [namespace] column.
     *
     * @param string $v New value
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     */
    public function setNamespaceName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->namespace !== $v) {
            $this->namespace = $v;
            $this->modifiedColumns[DataModelTableMap::COL_NAMESPACE] = true;
        }

        return $this;
    } // setNamespaceName()

    /**
     * Set the value of [php_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     */
    public function setPhpName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->php_name !== $v) {
            $this->php_name = $v;
            $this->modifiedColumns[DataModelTableMap::COL_PHP_NAME] = true;
        }

        return $this;
    } // setPhpName()

    /**
     * Set the value of [module_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     */
    public function setModuleId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->module_id !== $v) {
            $this->module_id = $v;
            $this->modifiedColumns[DataModelTableMap::COL_MODULE_ID] = true;
        }

        if ($this->aFkDataModel !== null && $this->aFkDataModel->getId() !== $v) {
            $this->aFkDataModel = null;
        }

        return $this;
    } // setModuleId()

    /**
     * Set the value of [title] column.
     *
     * @param string $v New value
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[DataModelTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Sets the value of the [api_exposed] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     */
    public function setApiExposed($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->api_exposed !== $v) {
            $this->api_exposed = $v;
            $this->modifiedColumns[DataModelTableMap::COL_API_EXPOSED] = true;
        }

        return $this;
    } // setApiExposed()

    /**
     * Set the value of [api_description] column.
     *
     * @param string $v New value
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     */
    public function setApiDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->api_description !== $v) {
            $this->api_description = $v;
            $this->modifiedColumns[DataModelTableMap::COL_API_DESCRIPTION] = true;
        }

        return $this;
    } // setApiDescription()

    /**
     * Sets the value of the [is_persistent] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     */
    public function setIsPersistent($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_persistent !== $v) {
            $this->is_persistent = $v;
            $this->modifiedColumns[DataModelTableMap::COL_IS_PERSISTENT] = true;
        }

        return $this;
    } // setIsPersistent()

    /**
     * Sets the value of the [is_ui_component] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     */
    public function setIsUiComponent($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_ui_component !== $v) {
            $this->is_ui_component = $v;
            $this->modifiedColumns[DataModelTableMap::COL_IS_UI_COMPONENT] = true;
        }

        return $this;
    } // setIsUiComponent()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->api_exposed !== false) {
                return false;
            }

            if ($this->is_persistent !== false) {
                return false;
            }

            if ($this->is_ui_component !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : DataModelTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : DataModelTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : DataModelTableMap::translateFieldName('NamespaceName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->namespace = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : DataModelTableMap::translateFieldName('PhpName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->php_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : DataModelTableMap::translateFieldName('ModuleId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->module_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : DataModelTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : DataModelTableMap::translateFieldName('ApiExposed', TableMap::TYPE_PHPNAME, $indexType)];
            $this->api_exposed = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : DataModelTableMap::translateFieldName('ApiDescription', TableMap::TYPE_PHPNAME, $indexType)];
            $this->api_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : DataModelTableMap::translateFieldName('IsPersistent', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_persistent = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : DataModelTableMap::translateFieldName('IsUiComponent', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_ui_component = (null !== $col) ? (boolean) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = DataModelTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\System\\DataModel\\Model\\DataModel'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aFkDataModel !== null && $this->module_id !== $this->aFkDataModel->getId()) {
            $this->aFkDataModel = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DataModelTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildDataModelQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aFkDataModel = null;
            $this->collBehaviors = null;

            $this->collDataFields = null;

            $this->collUniqueFieldGroups = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see DataModel::setDeleted()
     * @see DataModel::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(DataModelTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildDataModelQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(DataModelTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                DataModelTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aFkDataModel !== null) {
                if ($this->aFkDataModel->isModified() || $this->aFkDataModel->isNew()) {
                    $affectedRows += $this->aFkDataModel->save($con);
                }
                $this->setFkDataModel($this->aFkDataModel);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->behaviorsScheduledForDeletion !== null) {
                if (!$this->behaviorsScheduledForDeletion->isEmpty()) {
                    \Model\System\DataModel\Behavior\BehaviorQuery::create()
                        ->filterByPrimaryKeys($this->behaviorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->behaviorsScheduledForDeletion = null;
                }
            }

            if ($this->collBehaviors !== null) {
                foreach ($this->collBehaviors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->dataFieldsScheduledForDeletion !== null) {
                if (!$this->dataFieldsScheduledForDeletion->isEmpty()) {
                    \Model\System\DataModel\Field\DataFieldQuery::create()
                        ->filterByPrimaryKeys($this->dataFieldsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->dataFieldsScheduledForDeletion = null;
                }
            }

            if ($this->collDataFields !== null) {
                foreach ($this->collDataFields as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->uniqueFieldGroupsScheduledForDeletion !== null) {
                if (!$this->uniqueFieldGroupsScheduledForDeletion->isEmpty()) {
                    \Model\System\DataModel\Model\UniqueFieldGroupQuery::create()
                        ->filterByPrimaryKeys($this->uniqueFieldGroupsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->uniqueFieldGroupsScheduledForDeletion = null;
                }
            }

            if ($this->collUniqueFieldGroups !== null) {
                foreach ($this->collUniqueFieldGroups as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[DataModelTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . DataModelTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(DataModelTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(DataModelTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(DataModelTableMap::COL_NAMESPACE)) {
            $modifiedColumns[':p' . $index++]  = 'namespace';
        }
        if ($this->isColumnModified(DataModelTableMap::COL_PHP_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'php_name';
        }
        if ($this->isColumnModified(DataModelTableMap::COL_MODULE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'module_id';
        }
        if ($this->isColumnModified(DataModelTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(DataModelTableMap::COL_API_EXPOSED)) {
            $modifiedColumns[':p' . $index++]  = 'api_exposed';
        }
        if ($this->isColumnModified(DataModelTableMap::COL_API_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'api_description';
        }
        if ($this->isColumnModified(DataModelTableMap::COL_IS_PERSISTENT)) {
            $modifiedColumns[':p' . $index++]  = 'is_persistent';
        }
        if ($this->isColumnModified(DataModelTableMap::COL_IS_UI_COMPONENT)) {
            $modifiedColumns[':p' . $index++]  = 'is_ui_component';
        }

        $sql = sprintf(
            'INSERT INTO data_model (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'namespace':
                        $stmt->bindValue($identifier, $this->namespace, PDO::PARAM_STR);
                        break;
                    case 'php_name':
                        $stmt->bindValue($identifier, $this->php_name, PDO::PARAM_STR);
                        break;
                    case 'module_id':
                        $stmt->bindValue($identifier, $this->module_id, PDO::PARAM_INT);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'api_exposed':
                        $stmt->bindValue($identifier, (int) $this->api_exposed, PDO::PARAM_INT);
                        break;
                    case 'api_description':
                        $stmt->bindValue($identifier, $this->api_description, PDO::PARAM_STR);
                        break;
                    case 'is_persistent':
                        $stmt->bindValue($identifier, (int) $this->is_persistent, PDO::PARAM_INT);
                        break;
                    case 'is_ui_component':
                        $stmt->bindValue($identifier, (int) $this->is_ui_component, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = DataModelTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getNamespaceName();
                break;
            case 3:
                return $this->getPhpName();
                break;
            case 4:
                return $this->getModuleId();
                break;
            case 5:
                return $this->getTitle();
                break;
            case 6:
                return $this->getApiExposed();
                break;
            case 7:
                return $this->getApiDescription();
                break;
            case 8:
                return $this->getIsPersistent();
                break;
            case 9:
                return $this->getIsUiComponent();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['DataModel'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['DataModel'][$this->hashCode()] = true;
        $keys = DataModelTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getNamespaceName(),
            $keys[3] => $this->getPhpName(),
            $keys[4] => $this->getModuleId(),
            $keys[5] => $this->getTitle(),
            $keys[6] => $this->getApiExposed(),
            $keys[7] => $this->getApiDescription(),
            $keys[8] => $this->getIsPersistent(),
            $keys[9] => $this->getIsUiComponent(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aFkDataModel) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'module';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'module';
                        break;
                    default:
                        $key = 'FkDataModel';
                }

                $result[$key] = $this->aFkDataModel->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collBehaviors) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'behaviors';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'behaviors';
                        break;
                    default:
                        $key = 'Behaviors';
                }

                $result[$key] = $this->collBehaviors->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collDataFields) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dataFields';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'data_fields';
                        break;
                    default:
                        $key = 'DataFields';
                }

                $result[$key] = $this->collDataFields->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUniqueFieldGroups) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'uniqueFieldGroups';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'unique_field_groups';
                        break;
                    default:
                        $key = 'UniqueFieldGroups';
                }

                $result[$key] = $this->collUniqueFieldGroups->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\System\DataModel\Model\DataModel
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = DataModelTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\System\DataModel\Model\DataModel
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setNamespaceName($value);
                break;
            case 3:
                $this->setPhpName($value);
                break;
            case 4:
                $this->setModuleId($value);
                break;
            case 5:
                $this->setTitle($value);
                break;
            case 6:
                $this->setApiExposed($value);
                break;
            case 7:
                $this->setApiDescription($value);
                break;
            case 8:
                $this->setIsPersistent($value);
                break;
            case 9:
                $this->setIsUiComponent($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = DataModelTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setNamespaceName($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setPhpName($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setModuleId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setTitle($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setApiExposed($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setApiDescription($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setIsPersistent($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setIsUiComponent($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\System\DataModel\Model\DataModel The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(DataModelTableMap::DATABASE_NAME);

        if ($this->isColumnModified(DataModelTableMap::COL_ID)) {
            $criteria->add(DataModelTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(DataModelTableMap::COL_NAME)) {
            $criteria->add(DataModelTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(DataModelTableMap::COL_NAMESPACE)) {
            $criteria->add(DataModelTableMap::COL_NAMESPACE, $this->namespace);
        }
        if ($this->isColumnModified(DataModelTableMap::COL_PHP_NAME)) {
            $criteria->add(DataModelTableMap::COL_PHP_NAME, $this->php_name);
        }
        if ($this->isColumnModified(DataModelTableMap::COL_MODULE_ID)) {
            $criteria->add(DataModelTableMap::COL_MODULE_ID, $this->module_id);
        }
        if ($this->isColumnModified(DataModelTableMap::COL_TITLE)) {
            $criteria->add(DataModelTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(DataModelTableMap::COL_API_EXPOSED)) {
            $criteria->add(DataModelTableMap::COL_API_EXPOSED, $this->api_exposed);
        }
        if ($this->isColumnModified(DataModelTableMap::COL_API_DESCRIPTION)) {
            $criteria->add(DataModelTableMap::COL_API_DESCRIPTION, $this->api_description);
        }
        if ($this->isColumnModified(DataModelTableMap::COL_IS_PERSISTENT)) {
            $criteria->add(DataModelTableMap::COL_IS_PERSISTENT, $this->is_persistent);
        }
        if ($this->isColumnModified(DataModelTableMap::COL_IS_UI_COMPONENT)) {
            $criteria->add(DataModelTableMap::COL_IS_UI_COMPONENT, $this->is_ui_component);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildDataModelQuery::create();
        $criteria->add(DataModelTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\System\DataModel\Model\DataModel (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setNamespaceName($this->getNamespaceName());
        $copyObj->setPhpName($this->getPhpName());
        $copyObj->setModuleId($this->getModuleId());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setApiExposed($this->getApiExposed());
        $copyObj->setApiDescription($this->getApiDescription());
        $copyObj->setIsPersistent($this->getIsPersistent());
        $copyObj->setIsUiComponent($this->getIsUiComponent());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getBehaviors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addBehavior($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getDataFields() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addDataField($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUniqueFieldGroups() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUniqueFieldGroup($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\System\DataModel\Model\DataModel Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a Module object.
     *
     * @param  Module|null $v
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     * @throws PropelException
     */
    public function setFkDataModel(Module $v = null)
    {
        if ($v === null) {
            $this->setModuleId(NULL);
        } else {
            $this->setModuleId($v->getId());
        }

        $this->aFkDataModel = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the Module object, it will not be re-added.
        if ($v !== null) {
            $v->addDataModel($this);
        }


        return $this;
    }


    /**
     * Get the associated Module object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return Module|null The associated Module object.
     * @throws PropelException
     */
    public function getFkDataModel(ConnectionInterface $con = null)
    {
        if ($this->aFkDataModel === null && ($this->module_id != 0)) {
            $this->aFkDataModel = ModuleQuery::create()->findPk($this->module_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aFkDataModel->addDataModels($this);
             */
        }

        return $this->aFkDataModel;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('Behavior' === $relationName) {
            $this->initBehaviors();
            return;
        }
        if ('DataField' === $relationName) {
            $this->initDataFields();
            return;
        }
        if ('UniqueFieldGroup' === $relationName) {
            $this->initUniqueFieldGroups();
            return;
        }
    }

    /**
     * Clears out the collBehaviors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addBehaviors()
     */
    public function clearBehaviors()
    {
        $this->collBehaviors = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collBehaviors collection loaded partially.
     */
    public function resetPartialBehaviors($v = true)
    {
        $this->collBehaviorsPartial = $v;
    }

    /**
     * Initializes the collBehaviors collection.
     *
     * By default this just sets the collBehaviors collection to an empty array (like clearcollBehaviors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initBehaviors($overrideExisting = true)
    {
        if (null !== $this->collBehaviors && !$overrideExisting) {
            return;
        }

        $collectionClassName = BehaviorTableMap::getTableMap()->getCollectionClassName();

        $this->collBehaviors = new $collectionClassName;
        $this->collBehaviors->setModel('\Model\System\DataModel\Behavior\Behavior');
    }

    /**
     * Gets an array of Behavior objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDataModel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|Behavior[] List of Behavior objects
     * @throws PropelException
     */
    public function getBehaviors(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collBehaviorsPartial && !$this->isNew();
        if (null === $this->collBehaviors || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collBehaviors) {
                    $this->initBehaviors();
                } else {
                    $collectionClassName = BehaviorTableMap::getTableMap()->getCollectionClassName();

                    $collBehaviors = new $collectionClassName;
                    $collBehaviors->setModel('\Model\System\DataModel\Behavior\Behavior');

                    return $collBehaviors;
                }
            } else {
                $collBehaviors = BehaviorQuery::create(null, $criteria)
                    ->filterByDataModel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collBehaviorsPartial && count($collBehaviors)) {
                        $this->initBehaviors(false);

                        foreach ($collBehaviors as $obj) {
                            if (false == $this->collBehaviors->contains($obj)) {
                                $this->collBehaviors->append($obj);
                            }
                        }

                        $this->collBehaviorsPartial = true;
                    }

                    return $collBehaviors;
                }

                if ($partial && $this->collBehaviors) {
                    foreach ($this->collBehaviors as $obj) {
                        if ($obj->isNew()) {
                            $collBehaviors[] = $obj;
                        }
                    }
                }

                $this->collBehaviors = $collBehaviors;
                $this->collBehaviorsPartial = false;
            }
        }

        return $this->collBehaviors;
    }

    /**
     * Sets a collection of Behavior objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $behaviors A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDataModel The current object (for fluent API support)
     */
    public function setBehaviors(Collection $behaviors, ConnectionInterface $con = null)
    {
        /** @var Behavior[] $behaviorsToDelete */
        $behaviorsToDelete = $this->getBehaviors(new Criteria(), $con)->diff($behaviors);


        $this->behaviorsScheduledForDeletion = $behaviorsToDelete;

        foreach ($behaviorsToDelete as $behaviorRemoved) {
            $behaviorRemoved->setDataModel(null);
        }

        $this->collBehaviors = null;
        foreach ($behaviors as $behavior) {
            $this->addBehavior($behavior);
        }

        $this->collBehaviors = $behaviors;
        $this->collBehaviorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseBehavior objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseBehavior objects.
     * @throws PropelException
     */
    public function countBehaviors(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collBehaviorsPartial && !$this->isNew();
        if (null === $this->collBehaviors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collBehaviors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getBehaviors());
            }

            $query = BehaviorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDataModel($this)
                ->count($con);
        }

        return count($this->collBehaviors);
    }

    /**
     * Method called to associate a Behavior object to this object
     * through the Behavior foreign key attribute.
     *
     * @param  Behavior $l Behavior
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     */
    public function addBehavior(Behavior $l)
    {
        if ($this->collBehaviors === null) {
            $this->initBehaviors();
            $this->collBehaviorsPartial = true;
        }

        if (!$this->collBehaviors->contains($l)) {
            $this->doAddBehavior($l);

            if ($this->behaviorsScheduledForDeletion and $this->behaviorsScheduledForDeletion->contains($l)) {
                $this->behaviorsScheduledForDeletion->remove($this->behaviorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param Behavior $behavior The Behavior object to add.
     */
    protected function doAddBehavior(Behavior $behavior)
    {
        $this->collBehaviors[]= $behavior;
        $behavior->setDataModel($this);
    }

    /**
     * @param  Behavior $behavior The Behavior object to remove.
     * @return $this|ChildDataModel The current object (for fluent API support)
     */
    public function removeBehavior(Behavior $behavior)
    {
        if ($this->getBehaviors()->contains($behavior)) {
            $pos = $this->collBehaviors->search($behavior);
            $this->collBehaviors->remove($pos);
            if (null === $this->behaviorsScheduledForDeletion) {
                $this->behaviorsScheduledForDeletion = clone $this->collBehaviors;
                $this->behaviorsScheduledForDeletion->clear();
            }
            $this->behaviorsScheduledForDeletion[]= clone $behavior;
            $behavior->setDataModel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this DataModel is new, it will return
     * an empty collection; or if this DataModel has previously
     * been saved, it will retrieve related Behaviors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in DataModel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|Behavior[] List of Behavior objects
     */
    public function getBehaviorsJoinBehaviorType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = BehaviorQuery::create(null, $criteria);
        $query->joinWith('BehaviorType', $joinBehavior);

        return $this->getBehaviors($query, $con);
    }

    /**
     * Clears out the collDataFields collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addDataFields()
     */
    public function clearDataFields()
    {
        $this->collDataFields = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collDataFields collection loaded partially.
     */
    public function resetPartialDataFields($v = true)
    {
        $this->collDataFieldsPartial = $v;
    }

    /**
     * Initializes the collDataFields collection.
     *
     * By default this just sets the collDataFields collection to an empty array (like clearcollDataFields());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initDataFields($overrideExisting = true)
    {
        if (null !== $this->collDataFields && !$overrideExisting) {
            return;
        }

        $collectionClassName = DataFieldTableMap::getTableMap()->getCollectionClassName();

        $this->collDataFields = new $collectionClassName;
        $this->collDataFields->setModel('\Model\System\DataModel\Field\DataField');
    }

    /**
     * Gets an array of DataField objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDataModel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|DataField[] List of DataField objects
     * @throws PropelException
     */
    public function getDataFields(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collDataFieldsPartial && !$this->isNew();
        if (null === $this->collDataFields || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collDataFields) {
                    $this->initDataFields();
                } else {
                    $collectionClassName = DataFieldTableMap::getTableMap()->getCollectionClassName();

                    $collDataFields = new $collectionClassName;
                    $collDataFields->setModel('\Model\System\DataModel\Field\DataField');

                    return $collDataFields;
                }
            } else {
                $collDataFields = DataFieldQuery::create(null, $criteria)
                    ->filterByFkDataModel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collDataFieldsPartial && count($collDataFields)) {
                        $this->initDataFields(false);

                        foreach ($collDataFields as $obj) {
                            if (false == $this->collDataFields->contains($obj)) {
                                $this->collDataFields->append($obj);
                            }
                        }

                        $this->collDataFieldsPartial = true;
                    }

                    return $collDataFields;
                }

                if ($partial && $this->collDataFields) {
                    foreach ($this->collDataFields as $obj) {
                        if ($obj->isNew()) {
                            $collDataFields[] = $obj;
                        }
                    }
                }

                $this->collDataFields = $collDataFields;
                $this->collDataFieldsPartial = false;
            }
        }

        return $this->collDataFields;
    }

    /**
     * Sets a collection of DataField objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $dataFields A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDataModel The current object (for fluent API support)
     */
    public function setDataFields(Collection $dataFields, ConnectionInterface $con = null)
    {
        /** @var DataField[] $dataFieldsToDelete */
        $dataFieldsToDelete = $this->getDataFields(new Criteria(), $con)->diff($dataFields);


        $this->dataFieldsScheduledForDeletion = $dataFieldsToDelete;

        foreach ($dataFieldsToDelete as $dataFieldRemoved) {
            $dataFieldRemoved->setFkDataModel(null);
        }

        $this->collDataFields = null;
        foreach ($dataFields as $dataField) {
            $this->addDataField($dataField);
        }

        $this->collDataFields = $dataFields;
        $this->collDataFieldsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseDataField objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseDataField objects.
     * @throws PropelException
     */
    public function countDataFields(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collDataFieldsPartial && !$this->isNew();
        if (null === $this->collDataFields || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collDataFields) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getDataFields());
            }

            $query = DataFieldQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFkDataModel($this)
                ->count($con);
        }

        return count($this->collDataFields);
    }

    /**
     * Method called to associate a DataField object to this object
     * through the DataField foreign key attribute.
     *
     * @param  DataField $l DataField
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     */
    public function addDataField(DataField $l)
    {
        if ($this->collDataFields === null) {
            $this->initDataFields();
            $this->collDataFieldsPartial = true;
        }

        if (!$this->collDataFields->contains($l)) {
            $this->doAddDataField($l);

            if ($this->dataFieldsScheduledForDeletion and $this->dataFieldsScheduledForDeletion->contains($l)) {
                $this->dataFieldsScheduledForDeletion->remove($this->dataFieldsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param DataField $dataField The DataField object to add.
     */
    protected function doAddDataField(DataField $dataField)
    {
        $this->collDataFields[]= $dataField;
        $dataField->setFkDataModel($this);
    }

    /**
     * @param  DataField $dataField The DataField object to remove.
     * @return $this|ChildDataModel The current object (for fluent API support)
     */
    public function removeDataField(DataField $dataField)
    {
        if ($this->getDataFields()->contains($dataField)) {
            $pos = $this->collDataFields->search($dataField);
            $this->collDataFields->remove($pos);
            if (null === $this->dataFieldsScheduledForDeletion) {
                $this->dataFieldsScheduledForDeletion = clone $this->collDataFields;
                $this->dataFieldsScheduledForDeletion->clear();
            }
            $this->dataFieldsScheduledForDeletion[]= clone $dataField;
            $dataField->setFkDataModel(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this DataModel is new, it will return
     * an empty collection; or if this DataModel has previously
     * been saved, it will retrieve related DataFields from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in DataModel.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|DataField[] List of DataField objects
     */
    public function getDataFieldsJoinFkDataType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = DataFieldQuery::create(null, $criteria);
        $query->joinWith('FkDataType', $joinBehavior);

        return $this->getDataFields($query, $con);
    }

    /**
     * Clears out the collUniqueFieldGroups collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUniqueFieldGroups()
     */
    public function clearUniqueFieldGroups()
    {
        $this->collUniqueFieldGroups = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUniqueFieldGroups collection loaded partially.
     */
    public function resetPartialUniqueFieldGroups($v = true)
    {
        $this->collUniqueFieldGroupsPartial = $v;
    }

    /**
     * Initializes the collUniqueFieldGroups collection.
     *
     * By default this just sets the collUniqueFieldGroups collection to an empty array (like clearcollUniqueFieldGroups());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUniqueFieldGroups($overrideExisting = true)
    {
        if (null !== $this->collUniqueFieldGroups && !$overrideExisting) {
            return;
        }

        $collectionClassName = UniqueFieldGroupTableMap::getTableMap()->getCollectionClassName();

        $this->collUniqueFieldGroups = new $collectionClassName;
        $this->collUniqueFieldGroups->setModel('\Model\System\DataModel\Model\UniqueFieldGroup');
    }

    /**
     * Gets an array of ChildUniqueFieldGroup objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDataModel is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUniqueFieldGroup[] List of ChildUniqueFieldGroup objects
     * @throws PropelException
     */
    public function getUniqueFieldGroups(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUniqueFieldGroupsPartial && !$this->isNew();
        if (null === $this->collUniqueFieldGroups || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUniqueFieldGroups) {
                    $this->initUniqueFieldGroups();
                } else {
                    $collectionClassName = UniqueFieldGroupTableMap::getTableMap()->getCollectionClassName();

                    $collUniqueFieldGroups = new $collectionClassName;
                    $collUniqueFieldGroups->setModel('\Model\System\DataModel\Model\UniqueFieldGroup');

                    return $collUniqueFieldGroups;
                }
            } else {
                $collUniqueFieldGroups = ChildUniqueFieldGroupQuery::create(null, $criteria)
                    ->filterByDataModel($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUniqueFieldGroupsPartial && count($collUniqueFieldGroups)) {
                        $this->initUniqueFieldGroups(false);

                        foreach ($collUniqueFieldGroups as $obj) {
                            if (false == $this->collUniqueFieldGroups->contains($obj)) {
                                $this->collUniqueFieldGroups->append($obj);
                            }
                        }

                        $this->collUniqueFieldGroupsPartial = true;
                    }

                    return $collUniqueFieldGroups;
                }

                if ($partial && $this->collUniqueFieldGroups) {
                    foreach ($this->collUniqueFieldGroups as $obj) {
                        if ($obj->isNew()) {
                            $collUniqueFieldGroups[] = $obj;
                        }
                    }
                }

                $this->collUniqueFieldGroups = $collUniqueFieldGroups;
                $this->collUniqueFieldGroupsPartial = false;
            }
        }

        return $this->collUniqueFieldGroups;
    }

    /**
     * Sets a collection of ChildUniqueFieldGroup objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $uniqueFieldGroups A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDataModel The current object (for fluent API support)
     */
    public function setUniqueFieldGroups(Collection $uniqueFieldGroups, ConnectionInterface $con = null)
    {
        /** @var ChildUniqueFieldGroup[] $uniqueFieldGroupsToDelete */
        $uniqueFieldGroupsToDelete = $this->getUniqueFieldGroups(new Criteria(), $con)->diff($uniqueFieldGroups);


        $this->uniqueFieldGroupsScheduledForDeletion = $uniqueFieldGroupsToDelete;

        foreach ($uniqueFieldGroupsToDelete as $uniqueFieldGroupRemoved) {
            $uniqueFieldGroupRemoved->setDataModel(null);
        }

        $this->collUniqueFieldGroups = null;
        foreach ($uniqueFieldGroups as $uniqueFieldGroup) {
            $this->addUniqueFieldGroup($uniqueFieldGroup);
        }

        $this->collUniqueFieldGroups = $uniqueFieldGroups;
        $this->collUniqueFieldGroupsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UniqueFieldGroup objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UniqueFieldGroup objects.
     * @throws PropelException
     */
    public function countUniqueFieldGroups(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUniqueFieldGroupsPartial && !$this->isNew();
        if (null === $this->collUniqueFieldGroups || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUniqueFieldGroups) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUniqueFieldGroups());
            }

            $query = ChildUniqueFieldGroupQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDataModel($this)
                ->count($con);
        }

        return count($this->collUniqueFieldGroups);
    }

    /**
     * Method called to associate a ChildUniqueFieldGroup object to this object
     * through the ChildUniqueFieldGroup foreign key attribute.
     *
     * @param  ChildUniqueFieldGroup $l ChildUniqueFieldGroup
     * @return $this|\Model\System\DataModel\Model\DataModel The current object (for fluent API support)
     */
    public function addUniqueFieldGroup(ChildUniqueFieldGroup $l)
    {
        if ($this->collUniqueFieldGroups === null) {
            $this->initUniqueFieldGroups();
            $this->collUniqueFieldGroupsPartial = true;
        }

        if (!$this->collUniqueFieldGroups->contains($l)) {
            $this->doAddUniqueFieldGroup($l);

            if ($this->uniqueFieldGroupsScheduledForDeletion and $this->uniqueFieldGroupsScheduledForDeletion->contains($l)) {
                $this->uniqueFieldGroupsScheduledForDeletion->remove($this->uniqueFieldGroupsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUniqueFieldGroup $uniqueFieldGroup The ChildUniqueFieldGroup object to add.
     */
    protected function doAddUniqueFieldGroup(ChildUniqueFieldGroup $uniqueFieldGroup)
    {
        $this->collUniqueFieldGroups[]= $uniqueFieldGroup;
        $uniqueFieldGroup->setDataModel($this);
    }

    /**
     * @param  ChildUniqueFieldGroup $uniqueFieldGroup The ChildUniqueFieldGroup object to remove.
     * @return $this|ChildDataModel The current object (for fluent API support)
     */
    public function removeUniqueFieldGroup(ChildUniqueFieldGroup $uniqueFieldGroup)
    {
        if ($this->getUniqueFieldGroups()->contains($uniqueFieldGroup)) {
            $pos = $this->collUniqueFieldGroups->search($uniqueFieldGroup);
            $this->collUniqueFieldGroups->remove($pos);
            if (null === $this->uniqueFieldGroupsScheduledForDeletion) {
                $this->uniqueFieldGroupsScheduledForDeletion = clone $this->collUniqueFieldGroups;
                $this->uniqueFieldGroupsScheduledForDeletion->clear();
            }
            $this->uniqueFieldGroupsScheduledForDeletion[]= clone $uniqueFieldGroup;
            $uniqueFieldGroup->setDataModel(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aFkDataModel) {
            $this->aFkDataModel->removeDataModel($this);
        }
        $this->id = null;
        $this->name = null;
        $this->namespace = null;
        $this->php_name = null;
        $this->module_id = null;
        $this->title = null;
        $this->api_exposed = null;
        $this->api_description = null;
        $this->is_persistent = null;
        $this->is_ui_component = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collBehaviors) {
                foreach ($this->collBehaviors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collDataFields) {
                foreach ($this->collDataFields as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUniqueFieldGroups) {
                foreach ($this->collUniqueFieldGroups as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collBehaviors = null;
        $this->collDataFields = null;
        $this->collUniqueFieldGroups = null;
        $this->aFkDataModel = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(DataModelTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
