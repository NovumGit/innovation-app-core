<?php

namespace Model\System\DataModel\Model\Base;

use \Exception;
use \PDO;
use Model\Module\Module;
use Model\System\DataModel\Behavior\Behavior;
use Model\System\DataModel\Field\DataField;
use Model\System\DataModel\Model\DataModel as ChildDataModel;
use Model\System\DataModel\Model\DataModelQuery as ChildDataModelQuery;
use Model\System\DataModel\Model\Map\DataModelTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'data_model' table.
 *
 *
 *
 * @method     ChildDataModelQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildDataModelQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildDataModelQuery orderByNamespaceName($order = Criteria::ASC) Order by the namespace column
 * @method     ChildDataModelQuery orderByPhpName($order = Criteria::ASC) Order by the php_name column
 * @method     ChildDataModelQuery orderByModuleId($order = Criteria::ASC) Order by the module_id column
 * @method     ChildDataModelQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildDataModelQuery orderByApiExposed($order = Criteria::ASC) Order by the api_exposed column
 * @method     ChildDataModelQuery orderByApiDescription($order = Criteria::ASC) Order by the api_description column
 * @method     ChildDataModelQuery orderByIsPersistent($order = Criteria::ASC) Order by the is_persistent column
 * @method     ChildDataModelQuery orderByIsUiComponent($order = Criteria::ASC) Order by the is_ui_component column
 *
 * @method     ChildDataModelQuery groupById() Group by the id column
 * @method     ChildDataModelQuery groupByName() Group by the name column
 * @method     ChildDataModelQuery groupByNamespaceName() Group by the namespace column
 * @method     ChildDataModelQuery groupByPhpName() Group by the php_name column
 * @method     ChildDataModelQuery groupByModuleId() Group by the module_id column
 * @method     ChildDataModelQuery groupByTitle() Group by the title column
 * @method     ChildDataModelQuery groupByApiExposed() Group by the api_exposed column
 * @method     ChildDataModelQuery groupByApiDescription() Group by the api_description column
 * @method     ChildDataModelQuery groupByIsPersistent() Group by the is_persistent column
 * @method     ChildDataModelQuery groupByIsUiComponent() Group by the is_ui_component column
 *
 * @method     ChildDataModelQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildDataModelQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildDataModelQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildDataModelQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildDataModelQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildDataModelQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildDataModelQuery leftJoinFkDataModel($relationAlias = null) Adds a LEFT JOIN clause to the query using the FkDataModel relation
 * @method     ChildDataModelQuery rightJoinFkDataModel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FkDataModel relation
 * @method     ChildDataModelQuery innerJoinFkDataModel($relationAlias = null) Adds a INNER JOIN clause to the query using the FkDataModel relation
 *
 * @method     ChildDataModelQuery joinWithFkDataModel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FkDataModel relation
 *
 * @method     ChildDataModelQuery leftJoinWithFkDataModel() Adds a LEFT JOIN clause and with to the query using the FkDataModel relation
 * @method     ChildDataModelQuery rightJoinWithFkDataModel() Adds a RIGHT JOIN clause and with to the query using the FkDataModel relation
 * @method     ChildDataModelQuery innerJoinWithFkDataModel() Adds a INNER JOIN clause and with to the query using the FkDataModel relation
 *
 * @method     ChildDataModelQuery leftJoinBehavior($relationAlias = null) Adds a LEFT JOIN clause to the query using the Behavior relation
 * @method     ChildDataModelQuery rightJoinBehavior($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Behavior relation
 * @method     ChildDataModelQuery innerJoinBehavior($relationAlias = null) Adds a INNER JOIN clause to the query using the Behavior relation
 *
 * @method     ChildDataModelQuery joinWithBehavior($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Behavior relation
 *
 * @method     ChildDataModelQuery leftJoinWithBehavior() Adds a LEFT JOIN clause and with to the query using the Behavior relation
 * @method     ChildDataModelQuery rightJoinWithBehavior() Adds a RIGHT JOIN clause and with to the query using the Behavior relation
 * @method     ChildDataModelQuery innerJoinWithBehavior() Adds a INNER JOIN clause and with to the query using the Behavior relation
 *
 * @method     ChildDataModelQuery leftJoinDataField($relationAlias = null) Adds a LEFT JOIN clause to the query using the DataField relation
 * @method     ChildDataModelQuery rightJoinDataField($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DataField relation
 * @method     ChildDataModelQuery innerJoinDataField($relationAlias = null) Adds a INNER JOIN clause to the query using the DataField relation
 *
 * @method     ChildDataModelQuery joinWithDataField($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DataField relation
 *
 * @method     ChildDataModelQuery leftJoinWithDataField() Adds a LEFT JOIN clause and with to the query using the DataField relation
 * @method     ChildDataModelQuery rightJoinWithDataField() Adds a RIGHT JOIN clause and with to the query using the DataField relation
 * @method     ChildDataModelQuery innerJoinWithDataField() Adds a INNER JOIN clause and with to the query using the DataField relation
 *
 * @method     ChildDataModelQuery leftJoinUniqueFieldGroup($relationAlias = null) Adds a LEFT JOIN clause to the query using the UniqueFieldGroup relation
 * @method     ChildDataModelQuery rightJoinUniqueFieldGroup($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UniqueFieldGroup relation
 * @method     ChildDataModelQuery innerJoinUniqueFieldGroup($relationAlias = null) Adds a INNER JOIN clause to the query using the UniqueFieldGroup relation
 *
 * @method     ChildDataModelQuery joinWithUniqueFieldGroup($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UniqueFieldGroup relation
 *
 * @method     ChildDataModelQuery leftJoinWithUniqueFieldGroup() Adds a LEFT JOIN clause and with to the query using the UniqueFieldGroup relation
 * @method     ChildDataModelQuery rightJoinWithUniqueFieldGroup() Adds a RIGHT JOIN clause and with to the query using the UniqueFieldGroup relation
 * @method     ChildDataModelQuery innerJoinWithUniqueFieldGroup() Adds a INNER JOIN clause and with to the query using the UniqueFieldGroup relation
 *
 * @method     \Model\Module\ModuleQuery|\Model\System\DataModel\Behavior\BehaviorQuery|\Model\System\DataModel\Field\DataFieldQuery|\Model\System\DataModel\Model\UniqueFieldGroupQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildDataModel findOne(ConnectionInterface $con = null) Return the first ChildDataModel matching the query
 * @method     ChildDataModel findOneOrCreate(ConnectionInterface $con = null) Return the first ChildDataModel matching the query, or a new ChildDataModel object populated from the query conditions when no match is found
 *
 * @method     ChildDataModel findOneById(int $id) Return the first ChildDataModel filtered by the id column
 * @method     ChildDataModel findOneByName(string $name) Return the first ChildDataModel filtered by the name column
 * @method     ChildDataModel findOneByNamespaceName(string $namespace) Return the first ChildDataModel filtered by the namespace column
 * @method     ChildDataModel findOneByPhpName(string $php_name) Return the first ChildDataModel filtered by the php_name column
 * @method     ChildDataModel findOneByModuleId(int $module_id) Return the first ChildDataModel filtered by the module_id column
 * @method     ChildDataModel findOneByTitle(string $title) Return the first ChildDataModel filtered by the title column
 * @method     ChildDataModel findOneByApiExposed(boolean $api_exposed) Return the first ChildDataModel filtered by the api_exposed column
 * @method     ChildDataModel findOneByApiDescription(string $api_description) Return the first ChildDataModel filtered by the api_description column
 * @method     ChildDataModel findOneByIsPersistent(boolean $is_persistent) Return the first ChildDataModel filtered by the is_persistent column
 * @method     ChildDataModel findOneByIsUiComponent(boolean $is_ui_component) Return the first ChildDataModel filtered by the is_ui_component column *

 * @method     ChildDataModel requirePk($key, ConnectionInterface $con = null) Return the ChildDataModel by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataModel requireOne(ConnectionInterface $con = null) Return the first ChildDataModel matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDataModel requireOneById(int $id) Return the first ChildDataModel filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataModel requireOneByName(string $name) Return the first ChildDataModel filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataModel requireOneByNamespaceName(string $namespace) Return the first ChildDataModel filtered by the namespace column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataModel requireOneByPhpName(string $php_name) Return the first ChildDataModel filtered by the php_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataModel requireOneByModuleId(int $module_id) Return the first ChildDataModel filtered by the module_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataModel requireOneByTitle(string $title) Return the first ChildDataModel filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataModel requireOneByApiExposed(boolean $api_exposed) Return the first ChildDataModel filtered by the api_exposed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataModel requireOneByApiDescription(string $api_description) Return the first ChildDataModel filtered by the api_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataModel requireOneByIsPersistent(boolean $is_persistent) Return the first ChildDataModel filtered by the is_persistent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataModel requireOneByIsUiComponent(boolean $is_ui_component) Return the first ChildDataModel filtered by the is_ui_component column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDataModel[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildDataModel objects based on current ModelCriteria
 * @method     ChildDataModel[]|ObjectCollection findById(int $id) Return ChildDataModel objects filtered by the id column
 * @method     ChildDataModel[]|ObjectCollection findByName(string $name) Return ChildDataModel objects filtered by the name column
 * @method     ChildDataModel[]|ObjectCollection findByNamespaceName(string $namespace) Return ChildDataModel objects filtered by the namespace column
 * @method     ChildDataModel[]|ObjectCollection findByPhpName(string $php_name) Return ChildDataModel objects filtered by the php_name column
 * @method     ChildDataModel[]|ObjectCollection findByModuleId(int $module_id) Return ChildDataModel objects filtered by the module_id column
 * @method     ChildDataModel[]|ObjectCollection findByTitle(string $title) Return ChildDataModel objects filtered by the title column
 * @method     ChildDataModel[]|ObjectCollection findByApiExposed(boolean $api_exposed) Return ChildDataModel objects filtered by the api_exposed column
 * @method     ChildDataModel[]|ObjectCollection findByApiDescription(string $api_description) Return ChildDataModel objects filtered by the api_description column
 * @method     ChildDataModel[]|ObjectCollection findByIsPersistent(boolean $is_persistent) Return ChildDataModel objects filtered by the is_persistent column
 * @method     ChildDataModel[]|ObjectCollection findByIsUiComponent(boolean $is_ui_component) Return ChildDataModel objects filtered by the is_ui_component column
 * @method     ChildDataModel[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class DataModelQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\DataModel\Model\Base\DataModelQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\DataModel\\Model\\DataModel', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildDataModelQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildDataModelQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildDataModelQuery) {
            return $criteria;
        }
        $query = new ChildDataModelQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildDataModel|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DataModelTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = DataModelTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDataModel A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, namespace, php_name, module_id, title, api_exposed, api_description, is_persistent, is_ui_component FROM data_model WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildDataModel $obj */
            $obj = new ChildDataModel();
            $obj->hydrate($row);
            DataModelTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildDataModel|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DataModelTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DataModelTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DataModelTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DataModelTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataModelTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataModelTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the namespace column
     *
     * Example usage:
     * <code>
     * $query->filterByNamespaceName('fooValue');   // WHERE namespace = 'fooValue'
     * $query->filterByNamespaceName('%fooValue%', Criteria::LIKE); // WHERE namespace LIKE '%fooValue%'
     * </code>
     *
     * @param     string $namespaceName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByNamespaceName($namespaceName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($namespaceName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataModelTableMap::COL_NAMESPACE, $namespaceName, $comparison);
    }

    /**
     * Filter the query on the php_name column
     *
     * Example usage:
     * <code>
     * $query->filterByPhpName('fooValue');   // WHERE php_name = 'fooValue'
     * $query->filterByPhpName('%fooValue%', Criteria::LIKE); // WHERE php_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phpName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByPhpName($phpName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phpName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataModelTableMap::COL_PHP_NAME, $phpName, $comparison);
    }

    /**
     * Filter the query on the module_id column
     *
     * Example usage:
     * <code>
     * $query->filterByModuleId(1234); // WHERE module_id = 1234
     * $query->filterByModuleId(array(12, 34)); // WHERE module_id IN (12, 34)
     * $query->filterByModuleId(array('min' => 12)); // WHERE module_id > 12
     * </code>
     *
     * @see       filterByFkDataModel()
     *
     * @param     mixed $moduleId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByModuleId($moduleId = null, $comparison = null)
    {
        if (is_array($moduleId)) {
            $useMinMax = false;
            if (isset($moduleId['min'])) {
                $this->addUsingAlias(DataModelTableMap::COL_MODULE_ID, $moduleId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($moduleId['max'])) {
                $this->addUsingAlias(DataModelTableMap::COL_MODULE_ID, $moduleId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataModelTableMap::COL_MODULE_ID, $moduleId, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataModelTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the api_exposed column
     *
     * Example usage:
     * <code>
     * $query->filterByApiExposed(true); // WHERE api_exposed = true
     * $query->filterByApiExposed('yes'); // WHERE api_exposed = true
     * </code>
     *
     * @param     boolean|string $apiExposed The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByApiExposed($apiExposed = null, $comparison = null)
    {
        if (is_string($apiExposed)) {
            $apiExposed = in_array(strtolower($apiExposed), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DataModelTableMap::COL_API_EXPOSED, $apiExposed, $comparison);
    }

    /**
     * Filter the query on the api_description column
     *
     * Example usage:
     * <code>
     * $query->filterByApiDescription('fooValue');   // WHERE api_description = 'fooValue'
     * $query->filterByApiDescription('%fooValue%', Criteria::LIKE); // WHERE api_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apiDescription The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByApiDescription($apiDescription = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apiDescription)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataModelTableMap::COL_API_DESCRIPTION, $apiDescription, $comparison);
    }

    /**
     * Filter the query on the is_persistent column
     *
     * Example usage:
     * <code>
     * $query->filterByIsPersistent(true); // WHERE is_persistent = true
     * $query->filterByIsPersistent('yes'); // WHERE is_persistent = true
     * </code>
     *
     * @param     boolean|string $isPersistent The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByIsPersistent($isPersistent = null, $comparison = null)
    {
        if (is_string($isPersistent)) {
            $isPersistent = in_array(strtolower($isPersistent), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DataModelTableMap::COL_IS_PERSISTENT, $isPersistent, $comparison);
    }

    /**
     * Filter the query on the is_ui_component column
     *
     * Example usage:
     * <code>
     * $query->filterByIsUiComponent(true); // WHERE is_ui_component = true
     * $query->filterByIsUiComponent('yes'); // WHERE is_ui_component = true
     * </code>
     *
     * @param     boolean|string $isUiComponent The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByIsUiComponent($isUiComponent = null, $comparison = null)
    {
        if (is_string($isUiComponent)) {
            $isUiComponent = in_array(strtolower($isUiComponent), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DataModelTableMap::COL_IS_UI_COMPONENT, $isUiComponent, $comparison);
    }

    /**
     * Filter the query by a related \Model\Module\Module object
     *
     * @param \Model\Module\Module|ObjectCollection $module The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByFkDataModel($module, $comparison = null)
    {
        if ($module instanceof \Model\Module\Module) {
            return $this
                ->addUsingAlias(DataModelTableMap::COL_MODULE_ID, $module->getId(), $comparison);
        } elseif ($module instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DataModelTableMap::COL_MODULE_ID, $module->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFkDataModel() only accepts arguments of type \Model\Module\Module or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FkDataModel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function joinFkDataModel($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FkDataModel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FkDataModel');
        }

        return $this;
    }

    /**
     * Use the FkDataModel relation Module object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Module\ModuleQuery A secondary query class using the current class as primary query
     */
    public function useFkDataModelQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinFkDataModel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FkDataModel', '\Model\Module\ModuleQuery');
    }

    /**
     * Filter the query by a related \Model\System\DataModel\Behavior\Behavior object
     *
     * @param \Model\System\DataModel\Behavior\Behavior|ObjectCollection $behavior the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByBehavior($behavior, $comparison = null)
    {
        if ($behavior instanceof \Model\System\DataModel\Behavior\Behavior) {
            return $this
                ->addUsingAlias(DataModelTableMap::COL_ID, $behavior->getDataModelId(), $comparison);
        } elseif ($behavior instanceof ObjectCollection) {
            return $this
                ->useBehaviorQuery()
                ->filterByPrimaryKeys($behavior->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBehavior() only accepts arguments of type \Model\System\DataModel\Behavior\Behavior or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Behavior relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function joinBehavior($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Behavior');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Behavior');
        }

        return $this;
    }

    /**
     * Use the Behavior relation Behavior object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\Behavior\BehaviorQuery A secondary query class using the current class as primary query
     */
    public function useBehaviorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBehavior($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Behavior', '\Model\System\DataModel\Behavior\BehaviorQuery');
    }

    /**
     * Filter the query by a related \Model\System\DataModel\Field\DataField object
     *
     * @param \Model\System\DataModel\Field\DataField|ObjectCollection $dataField the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByDataField($dataField, $comparison = null)
    {
        if ($dataField instanceof \Model\System\DataModel\Field\DataField) {
            return $this
                ->addUsingAlias(DataModelTableMap::COL_ID, $dataField->getDataModelId(), $comparison);
        } elseif ($dataField instanceof ObjectCollection) {
            return $this
                ->useDataFieldQuery()
                ->filterByPrimaryKeys($dataField->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByDataField() only accepts arguments of type \Model\System\DataModel\Field\DataField or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DataField relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function joinDataField($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DataField');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DataField');
        }

        return $this;
    }

    /**
     * Use the DataField relation DataField object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\Field\DataFieldQuery A secondary query class using the current class as primary query
     */
    public function useDataFieldQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDataField($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DataField', '\Model\System\DataModel\Field\DataFieldQuery');
    }

    /**
     * Filter the query by a related \Model\System\DataModel\Model\UniqueFieldGroup object
     *
     * @param \Model\System\DataModel\Model\UniqueFieldGroup|ObjectCollection $uniqueFieldGroup the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDataModelQuery The current query, for fluid interface
     */
    public function filterByUniqueFieldGroup($uniqueFieldGroup, $comparison = null)
    {
        if ($uniqueFieldGroup instanceof \Model\System\DataModel\Model\UniqueFieldGroup) {
            return $this
                ->addUsingAlias(DataModelTableMap::COL_ID, $uniqueFieldGroup->getDataModelId(), $comparison);
        } elseif ($uniqueFieldGroup instanceof ObjectCollection) {
            return $this
                ->useUniqueFieldGroupQuery()
                ->filterByPrimaryKeys($uniqueFieldGroup->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUniqueFieldGroup() only accepts arguments of type \Model\System\DataModel\Model\UniqueFieldGroup or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UniqueFieldGroup relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function joinUniqueFieldGroup($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UniqueFieldGroup');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UniqueFieldGroup');
        }

        return $this;
    }

    /**
     * Use the UniqueFieldGroup relation UniqueFieldGroup object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\Model\UniqueFieldGroupQuery A secondary query class using the current class as primary query
     */
    public function useUniqueFieldGroupQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUniqueFieldGroup($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UniqueFieldGroup', '\Model\System\DataModel\Model\UniqueFieldGroupQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildDataModel $dataModel Object to remove from the list of results
     *
     * @return $this|ChildDataModelQuery The current query, for fluid interface
     */
    public function prune($dataModel = null)
    {
        if ($dataModel) {
            $this->addUsingAlias(DataModelTableMap::COL_ID, $dataModel->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the data_model table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DataModelTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            DataModelTableMap::clearInstancePool();
            DataModelTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DataModelTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(DataModelTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            DataModelTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            DataModelTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // DataModelQuery
