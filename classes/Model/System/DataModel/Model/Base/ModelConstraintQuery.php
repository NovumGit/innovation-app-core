<?php

namespace Model\System\DataModel\Model\Base;

use \Exception;
use \PDO;
use Model\System\DataModel\Field\DataField;
use Model\System\DataModel\Model\ModelConstraint as ChildModelConstraint;
use Model\System\DataModel\Model\ModelConstraintQuery as ChildModelConstraintQuery;
use Model\System\DataModel\Model\Map\ModelConstraintTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'model_constraint' table.
 *
 *
 *
 * @method     ChildModelConstraintQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildModelConstraintQuery orderByPhpName($order = Criteria::ASC) Order by the php_name column
 * @method     ChildModelConstraintQuery orderByRefPhpName($order = Criteria::ASC) Order by the ref_php_name column
 * @method     ChildModelConstraintQuery orderByForeignFieldId($order = Criteria::ASC) Order by the foreign_field_id column
 * @method     ChildModelConstraintQuery orderByLocalFieldId($order = Criteria::ASC) Order by the local_field_id column
 * @method     ChildModelConstraintQuery orderByOnDelete($order = Criteria::ASC) Order by the on_delete column
 * @method     ChildModelConstraintQuery orderByOnUpdate($order = Criteria::ASC) Order by the on_update column
 *
 * @method     ChildModelConstraintQuery groupById() Group by the id column
 * @method     ChildModelConstraintQuery groupByPhpName() Group by the php_name column
 * @method     ChildModelConstraintQuery groupByRefPhpName() Group by the ref_php_name column
 * @method     ChildModelConstraintQuery groupByForeignFieldId() Group by the foreign_field_id column
 * @method     ChildModelConstraintQuery groupByLocalFieldId() Group by the local_field_id column
 * @method     ChildModelConstraintQuery groupByOnDelete() Group by the on_delete column
 * @method     ChildModelConstraintQuery groupByOnUpdate() Group by the on_update column
 *
 * @method     ChildModelConstraintQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildModelConstraintQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildModelConstraintQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildModelConstraintQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildModelConstraintQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildModelConstraintQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildModelConstraintQuery leftJoinFkLocalField($relationAlias = null) Adds a LEFT JOIN clause to the query using the FkLocalField relation
 * @method     ChildModelConstraintQuery rightJoinFkLocalField($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FkLocalField relation
 * @method     ChildModelConstraintQuery innerJoinFkLocalField($relationAlias = null) Adds a INNER JOIN clause to the query using the FkLocalField relation
 *
 * @method     ChildModelConstraintQuery joinWithFkLocalField($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FkLocalField relation
 *
 * @method     ChildModelConstraintQuery leftJoinWithFkLocalField() Adds a LEFT JOIN clause and with to the query using the FkLocalField relation
 * @method     ChildModelConstraintQuery rightJoinWithFkLocalField() Adds a RIGHT JOIN clause and with to the query using the FkLocalField relation
 * @method     ChildModelConstraintQuery innerJoinWithFkLocalField() Adds a INNER JOIN clause and with to the query using the FkLocalField relation
 *
 * @method     ChildModelConstraintQuery leftJoinFkForeignField($relationAlias = null) Adds a LEFT JOIN clause to the query using the FkForeignField relation
 * @method     ChildModelConstraintQuery rightJoinFkForeignField($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FkForeignField relation
 * @method     ChildModelConstraintQuery innerJoinFkForeignField($relationAlias = null) Adds a INNER JOIN clause to the query using the FkForeignField relation
 *
 * @method     ChildModelConstraintQuery joinWithFkForeignField($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FkForeignField relation
 *
 * @method     ChildModelConstraintQuery leftJoinWithFkForeignField() Adds a LEFT JOIN clause and with to the query using the FkForeignField relation
 * @method     ChildModelConstraintQuery rightJoinWithFkForeignField() Adds a RIGHT JOIN clause and with to the query using the FkForeignField relation
 * @method     ChildModelConstraintQuery innerJoinWithFkForeignField() Adds a INNER JOIN clause and with to the query using the FkForeignField relation
 *
 * @method     \Model\System\DataModel\Field\DataFieldQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildModelConstraint findOne(ConnectionInterface $con = null) Return the first ChildModelConstraint matching the query
 * @method     ChildModelConstraint findOneOrCreate(ConnectionInterface $con = null) Return the first ChildModelConstraint matching the query, or a new ChildModelConstraint object populated from the query conditions when no match is found
 *
 * @method     ChildModelConstraint findOneById(int $id) Return the first ChildModelConstraint filtered by the id column
 * @method     ChildModelConstraint findOneByPhpName(string $php_name) Return the first ChildModelConstraint filtered by the php_name column
 * @method     ChildModelConstraint findOneByRefPhpName(string $ref_php_name) Return the first ChildModelConstraint filtered by the ref_php_name column
 * @method     ChildModelConstraint findOneByForeignFieldId(int $foreign_field_id) Return the first ChildModelConstraint filtered by the foreign_field_id column
 * @method     ChildModelConstraint findOneByLocalFieldId(int $local_field_id) Return the first ChildModelConstraint filtered by the local_field_id column
 * @method     ChildModelConstraint findOneByOnDelete(string $on_delete) Return the first ChildModelConstraint filtered by the on_delete column
 * @method     ChildModelConstraint findOneByOnUpdate(string $on_update) Return the first ChildModelConstraint filtered by the on_update column *

 * @method     ChildModelConstraint requirePk($key, ConnectionInterface $con = null) Return the ChildModelConstraint by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildModelConstraint requireOne(ConnectionInterface $con = null) Return the first ChildModelConstraint matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildModelConstraint requireOneById(int $id) Return the first ChildModelConstraint filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildModelConstraint requireOneByPhpName(string $php_name) Return the first ChildModelConstraint filtered by the php_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildModelConstraint requireOneByRefPhpName(string $ref_php_name) Return the first ChildModelConstraint filtered by the ref_php_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildModelConstraint requireOneByForeignFieldId(int $foreign_field_id) Return the first ChildModelConstraint filtered by the foreign_field_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildModelConstraint requireOneByLocalFieldId(int $local_field_id) Return the first ChildModelConstraint filtered by the local_field_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildModelConstraint requireOneByOnDelete(string $on_delete) Return the first ChildModelConstraint filtered by the on_delete column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildModelConstraint requireOneByOnUpdate(string $on_update) Return the first ChildModelConstraint filtered by the on_update column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildModelConstraint[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildModelConstraint objects based on current ModelCriteria
 * @method     ChildModelConstraint[]|ObjectCollection findById(int $id) Return ChildModelConstraint objects filtered by the id column
 * @method     ChildModelConstraint[]|ObjectCollection findByPhpName(string $php_name) Return ChildModelConstraint objects filtered by the php_name column
 * @method     ChildModelConstraint[]|ObjectCollection findByRefPhpName(string $ref_php_name) Return ChildModelConstraint objects filtered by the ref_php_name column
 * @method     ChildModelConstraint[]|ObjectCollection findByForeignFieldId(int $foreign_field_id) Return ChildModelConstraint objects filtered by the foreign_field_id column
 * @method     ChildModelConstraint[]|ObjectCollection findByLocalFieldId(int $local_field_id) Return ChildModelConstraint objects filtered by the local_field_id column
 * @method     ChildModelConstraint[]|ObjectCollection findByOnDelete(string $on_delete) Return ChildModelConstraint objects filtered by the on_delete column
 * @method     ChildModelConstraint[]|ObjectCollection findByOnUpdate(string $on_update) Return ChildModelConstraint objects filtered by the on_update column
 * @method     ChildModelConstraint[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ModelConstraintQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\DataModel\Model\Base\ModelConstraintQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\DataModel\\Model\\ModelConstraint', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildModelConstraintQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildModelConstraintQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildModelConstraintQuery) {
            return $criteria;
        }
        $query = new ChildModelConstraintQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildModelConstraint|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ModelConstraintTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ModelConstraintTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildModelConstraint A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, php_name, ref_php_name, foreign_field_id, local_field_id, on_delete, on_update FROM model_constraint WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildModelConstraint $obj */
            $obj = new ChildModelConstraint();
            $obj->hydrate($row);
            ModelConstraintTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildModelConstraint|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildModelConstraintQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ModelConstraintTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildModelConstraintQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ModelConstraintTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildModelConstraintQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ModelConstraintTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ModelConstraintTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ModelConstraintTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the php_name column
     *
     * Example usage:
     * <code>
     * $query->filterByPhpName('fooValue');   // WHERE php_name = 'fooValue'
     * $query->filterByPhpName('%fooValue%', Criteria::LIKE); // WHERE php_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phpName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildModelConstraintQuery The current query, for fluid interface
     */
    public function filterByPhpName($phpName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phpName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ModelConstraintTableMap::COL_PHP_NAME, $phpName, $comparison);
    }

    /**
     * Filter the query on the ref_php_name column
     *
     * Example usage:
     * <code>
     * $query->filterByRefPhpName('fooValue');   // WHERE ref_php_name = 'fooValue'
     * $query->filterByRefPhpName('%fooValue%', Criteria::LIKE); // WHERE ref_php_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $refPhpName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildModelConstraintQuery The current query, for fluid interface
     */
    public function filterByRefPhpName($refPhpName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($refPhpName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ModelConstraintTableMap::COL_REF_PHP_NAME, $refPhpName, $comparison);
    }

    /**
     * Filter the query on the foreign_field_id column
     *
     * Example usage:
     * <code>
     * $query->filterByForeignFieldId(1234); // WHERE foreign_field_id = 1234
     * $query->filterByForeignFieldId(array(12, 34)); // WHERE foreign_field_id IN (12, 34)
     * $query->filterByForeignFieldId(array('min' => 12)); // WHERE foreign_field_id > 12
     * </code>
     *
     * @see       filterByFkForeignField()
     *
     * @param     mixed $foreignFieldId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildModelConstraintQuery The current query, for fluid interface
     */
    public function filterByForeignFieldId($foreignFieldId = null, $comparison = null)
    {
        if (is_array($foreignFieldId)) {
            $useMinMax = false;
            if (isset($foreignFieldId['min'])) {
                $this->addUsingAlias(ModelConstraintTableMap::COL_FOREIGN_FIELD_ID, $foreignFieldId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($foreignFieldId['max'])) {
                $this->addUsingAlias(ModelConstraintTableMap::COL_FOREIGN_FIELD_ID, $foreignFieldId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ModelConstraintTableMap::COL_FOREIGN_FIELD_ID, $foreignFieldId, $comparison);
    }

    /**
     * Filter the query on the local_field_id column
     *
     * Example usage:
     * <code>
     * $query->filterByLocalFieldId(1234); // WHERE local_field_id = 1234
     * $query->filterByLocalFieldId(array(12, 34)); // WHERE local_field_id IN (12, 34)
     * $query->filterByLocalFieldId(array('min' => 12)); // WHERE local_field_id > 12
     * </code>
     *
     * @see       filterByFkLocalField()
     *
     * @param     mixed $localFieldId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildModelConstraintQuery The current query, for fluid interface
     */
    public function filterByLocalFieldId($localFieldId = null, $comparison = null)
    {
        if (is_array($localFieldId)) {
            $useMinMax = false;
            if (isset($localFieldId['min'])) {
                $this->addUsingAlias(ModelConstraintTableMap::COL_LOCAL_FIELD_ID, $localFieldId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($localFieldId['max'])) {
                $this->addUsingAlias(ModelConstraintTableMap::COL_LOCAL_FIELD_ID, $localFieldId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ModelConstraintTableMap::COL_LOCAL_FIELD_ID, $localFieldId, $comparison);
    }

    /**
     * Filter the query on the on_delete column
     *
     * Example usage:
     * <code>
     * $query->filterByOnDelete('fooValue');   // WHERE on_delete = 'fooValue'
     * $query->filterByOnDelete('%fooValue%', Criteria::LIKE); // WHERE on_delete LIKE '%fooValue%'
     * </code>
     *
     * @param     string $onDelete The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildModelConstraintQuery The current query, for fluid interface
     */
    public function filterByOnDelete($onDelete = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($onDelete)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ModelConstraintTableMap::COL_ON_DELETE, $onDelete, $comparison);
    }

    /**
     * Filter the query on the on_update column
     *
     * Example usage:
     * <code>
     * $query->filterByOnUpdate('fooValue');   // WHERE on_update = 'fooValue'
     * $query->filterByOnUpdate('%fooValue%', Criteria::LIKE); // WHERE on_update LIKE '%fooValue%'
     * </code>
     *
     * @param     string $onUpdate The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildModelConstraintQuery The current query, for fluid interface
     */
    public function filterByOnUpdate($onUpdate = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($onUpdate)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ModelConstraintTableMap::COL_ON_UPDATE, $onUpdate, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\DataModel\Field\DataField object
     *
     * @param \Model\System\DataModel\Field\DataField|ObjectCollection $dataField The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildModelConstraintQuery The current query, for fluid interface
     */
    public function filterByFkLocalField($dataField, $comparison = null)
    {
        if ($dataField instanceof \Model\System\DataModel\Field\DataField) {
            return $this
                ->addUsingAlias(ModelConstraintTableMap::COL_LOCAL_FIELD_ID, $dataField->getId(), $comparison);
        } elseif ($dataField instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ModelConstraintTableMap::COL_LOCAL_FIELD_ID, $dataField->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFkLocalField() only accepts arguments of type \Model\System\DataModel\Field\DataField or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FkLocalField relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildModelConstraintQuery The current query, for fluid interface
     */
    public function joinFkLocalField($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FkLocalField');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FkLocalField');
        }

        return $this;
    }

    /**
     * Use the FkLocalField relation DataField object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\Field\DataFieldQuery A secondary query class using the current class as primary query
     */
    public function useFkLocalFieldQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFkLocalField($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FkLocalField', '\Model\System\DataModel\Field\DataFieldQuery');
    }

    /**
     * Filter the query by a related \Model\System\DataModel\Field\DataField object
     *
     * @param \Model\System\DataModel\Field\DataField|ObjectCollection $dataField The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildModelConstraintQuery The current query, for fluid interface
     */
    public function filterByFkForeignField($dataField, $comparison = null)
    {
        if ($dataField instanceof \Model\System\DataModel\Field\DataField) {
            return $this
                ->addUsingAlias(ModelConstraintTableMap::COL_FOREIGN_FIELD_ID, $dataField->getId(), $comparison);
        } elseif ($dataField instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ModelConstraintTableMap::COL_FOREIGN_FIELD_ID, $dataField->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFkForeignField() only accepts arguments of type \Model\System\DataModel\Field\DataField or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FkForeignField relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildModelConstraintQuery The current query, for fluid interface
     */
    public function joinFkForeignField($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FkForeignField');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FkForeignField');
        }

        return $this;
    }

    /**
     * Use the FkForeignField relation DataField object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\Field\DataFieldQuery A secondary query class using the current class as primary query
     */
    public function useFkForeignFieldQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFkForeignField($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FkForeignField', '\Model\System\DataModel\Field\DataFieldQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildModelConstraint $modelConstraint Object to remove from the list of results
     *
     * @return $this|ChildModelConstraintQuery The current query, for fluid interface
     */
    public function prune($modelConstraint = null)
    {
        if ($modelConstraint) {
            $this->addUsingAlias(ModelConstraintTableMap::COL_ID, $modelConstraint->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the model_constraint table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ModelConstraintTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ModelConstraintTableMap::clearInstancePool();
            ModelConstraintTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ModelConstraintTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ModelConstraintTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ModelConstraintTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ModelConstraintTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // ModelConstraintQuery
