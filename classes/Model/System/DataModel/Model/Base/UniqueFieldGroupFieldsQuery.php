<?php

namespace Model\System\DataModel\Model\Base;

use \Exception;
use \PDO;
use Model\System\DataModel\Field\DataField;
use Model\System\DataModel\Model\UniqueFieldGroupFields as ChildUniqueFieldGroupFields;
use Model\System\DataModel\Model\UniqueFieldGroupFieldsQuery as ChildUniqueFieldGroupFieldsQuery;
use Model\System\DataModel\Model\Map\UniqueFieldGroupFieldsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'unique_field_group_field' table.
 *
 *
 *
 * @method     ChildUniqueFieldGroupFieldsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUniqueFieldGroupFieldsQuery orderByUniqueFieldGroupId($order = Criteria::ASC) Order by the unique_field_group_id column
 * @method     ChildUniqueFieldGroupFieldsQuery orderByDataFieldId($order = Criteria::ASC) Order by the data_field_id column
 *
 * @method     ChildUniqueFieldGroupFieldsQuery groupById() Group by the id column
 * @method     ChildUniqueFieldGroupFieldsQuery groupByUniqueFieldGroupId() Group by the unique_field_group_id column
 * @method     ChildUniqueFieldGroupFieldsQuery groupByDataFieldId() Group by the data_field_id column
 *
 * @method     ChildUniqueFieldGroupFieldsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUniqueFieldGroupFieldsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUniqueFieldGroupFieldsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUniqueFieldGroupFieldsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUniqueFieldGroupFieldsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUniqueFieldGroupFieldsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUniqueFieldGroupFieldsQuery leftJoinModelUnique($relationAlias = null) Adds a LEFT JOIN clause to the query using the ModelUnique relation
 * @method     ChildUniqueFieldGroupFieldsQuery rightJoinModelUnique($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ModelUnique relation
 * @method     ChildUniqueFieldGroupFieldsQuery innerJoinModelUnique($relationAlias = null) Adds a INNER JOIN clause to the query using the ModelUnique relation
 *
 * @method     ChildUniqueFieldGroupFieldsQuery joinWithModelUnique($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ModelUnique relation
 *
 * @method     ChildUniqueFieldGroupFieldsQuery leftJoinWithModelUnique() Adds a LEFT JOIN clause and with to the query using the ModelUnique relation
 * @method     ChildUniqueFieldGroupFieldsQuery rightJoinWithModelUnique() Adds a RIGHT JOIN clause and with to the query using the ModelUnique relation
 * @method     ChildUniqueFieldGroupFieldsQuery innerJoinWithModelUnique() Adds a INNER JOIN clause and with to the query using the ModelUnique relation
 *
 * @method     ChildUniqueFieldGroupFieldsQuery leftJoinDataField($relationAlias = null) Adds a LEFT JOIN clause to the query using the DataField relation
 * @method     ChildUniqueFieldGroupFieldsQuery rightJoinDataField($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DataField relation
 * @method     ChildUniqueFieldGroupFieldsQuery innerJoinDataField($relationAlias = null) Adds a INNER JOIN clause to the query using the DataField relation
 *
 * @method     ChildUniqueFieldGroupFieldsQuery joinWithDataField($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DataField relation
 *
 * @method     ChildUniqueFieldGroupFieldsQuery leftJoinWithDataField() Adds a LEFT JOIN clause and with to the query using the DataField relation
 * @method     ChildUniqueFieldGroupFieldsQuery rightJoinWithDataField() Adds a RIGHT JOIN clause and with to the query using the DataField relation
 * @method     ChildUniqueFieldGroupFieldsQuery innerJoinWithDataField() Adds a INNER JOIN clause and with to the query using the DataField relation
 *
 * @method     \Model\System\DataModel\Model\UniqueFieldGroupQuery|\Model\System\DataModel\Field\DataFieldQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUniqueFieldGroupFields findOne(ConnectionInterface $con = null) Return the first ChildUniqueFieldGroupFields matching the query
 * @method     ChildUniqueFieldGroupFields findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUniqueFieldGroupFields matching the query, or a new ChildUniqueFieldGroupFields object populated from the query conditions when no match is found
 *
 * @method     ChildUniqueFieldGroupFields findOneById(int $id) Return the first ChildUniqueFieldGroupFields filtered by the id column
 * @method     ChildUniqueFieldGroupFields findOneByUniqueFieldGroupId(int $unique_field_group_id) Return the first ChildUniqueFieldGroupFields filtered by the unique_field_group_id column
 * @method     ChildUniqueFieldGroupFields findOneByDataFieldId(int $data_field_id) Return the first ChildUniqueFieldGroupFields filtered by the data_field_id column *

 * @method     ChildUniqueFieldGroupFields requirePk($key, ConnectionInterface $con = null) Return the ChildUniqueFieldGroupFields by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUniqueFieldGroupFields requireOne(ConnectionInterface $con = null) Return the first ChildUniqueFieldGroupFields matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUniqueFieldGroupFields requireOneById(int $id) Return the first ChildUniqueFieldGroupFields filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUniqueFieldGroupFields requireOneByUniqueFieldGroupId(int $unique_field_group_id) Return the first ChildUniqueFieldGroupFields filtered by the unique_field_group_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUniqueFieldGroupFields requireOneByDataFieldId(int $data_field_id) Return the first ChildUniqueFieldGroupFields filtered by the data_field_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUniqueFieldGroupFields[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUniqueFieldGroupFields objects based on current ModelCriteria
 * @method     ChildUniqueFieldGroupFields[]|ObjectCollection findById(int $id) Return ChildUniqueFieldGroupFields objects filtered by the id column
 * @method     ChildUniqueFieldGroupFields[]|ObjectCollection findByUniqueFieldGroupId(int $unique_field_group_id) Return ChildUniqueFieldGroupFields objects filtered by the unique_field_group_id column
 * @method     ChildUniqueFieldGroupFields[]|ObjectCollection findByDataFieldId(int $data_field_id) Return ChildUniqueFieldGroupFields objects filtered by the data_field_id column
 * @method     ChildUniqueFieldGroupFields[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UniqueFieldGroupFieldsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\DataModel\Model\Base\UniqueFieldGroupFieldsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\DataModel\\Model\\UniqueFieldGroupFields', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUniqueFieldGroupFieldsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUniqueFieldGroupFieldsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUniqueFieldGroupFieldsQuery) {
            return $criteria;
        }
        $query = new ChildUniqueFieldGroupFieldsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUniqueFieldGroupFields|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UniqueFieldGroupFieldsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UniqueFieldGroupFieldsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUniqueFieldGroupFields A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, unique_field_group_id, data_field_id FROM unique_field_group_field WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUniqueFieldGroupFields $obj */
            $obj = new ChildUniqueFieldGroupFields();
            $obj->hydrate($row);
            UniqueFieldGroupFieldsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUniqueFieldGroupFields|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUniqueFieldGroupFieldsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUniqueFieldGroupFieldsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUniqueFieldGroupFieldsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the unique_field_group_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUniqueFieldGroupId(1234); // WHERE unique_field_group_id = 1234
     * $query->filterByUniqueFieldGroupId(array(12, 34)); // WHERE unique_field_group_id IN (12, 34)
     * $query->filterByUniqueFieldGroupId(array('min' => 12)); // WHERE unique_field_group_id > 12
     * </code>
     *
     * @see       filterByModelUnique()
     *
     * @param     mixed $uniqueFieldGroupId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUniqueFieldGroupFieldsQuery The current query, for fluid interface
     */
    public function filterByUniqueFieldGroupId($uniqueFieldGroupId = null, $comparison = null)
    {
        if (is_array($uniqueFieldGroupId)) {
            $useMinMax = false;
            if (isset($uniqueFieldGroupId['min'])) {
                $this->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_UNIQUE_FIELD_GROUP_ID, $uniqueFieldGroupId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uniqueFieldGroupId['max'])) {
                $this->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_UNIQUE_FIELD_GROUP_ID, $uniqueFieldGroupId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_UNIQUE_FIELD_GROUP_ID, $uniqueFieldGroupId, $comparison);
    }

    /**
     * Filter the query on the data_field_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDataFieldId(1234); // WHERE data_field_id = 1234
     * $query->filterByDataFieldId(array(12, 34)); // WHERE data_field_id IN (12, 34)
     * $query->filterByDataFieldId(array('min' => 12)); // WHERE data_field_id > 12
     * </code>
     *
     * @see       filterByDataField()
     *
     * @param     mixed $dataFieldId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUniqueFieldGroupFieldsQuery The current query, for fluid interface
     */
    public function filterByDataFieldId($dataFieldId = null, $comparison = null)
    {
        if (is_array($dataFieldId)) {
            $useMinMax = false;
            if (isset($dataFieldId['min'])) {
                $this->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_DATA_FIELD_ID, $dataFieldId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataFieldId['max'])) {
                $this->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_DATA_FIELD_ID, $dataFieldId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_DATA_FIELD_ID, $dataFieldId, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\DataModel\Model\UniqueFieldGroup object
     *
     * @param \Model\System\DataModel\Model\UniqueFieldGroup|ObjectCollection $uniqueFieldGroup The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUniqueFieldGroupFieldsQuery The current query, for fluid interface
     */
    public function filterByModelUnique($uniqueFieldGroup, $comparison = null)
    {
        if ($uniqueFieldGroup instanceof \Model\System\DataModel\Model\UniqueFieldGroup) {
            return $this
                ->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_UNIQUE_FIELD_GROUP_ID, $uniqueFieldGroup->getId(), $comparison);
        } elseif ($uniqueFieldGroup instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_UNIQUE_FIELD_GROUP_ID, $uniqueFieldGroup->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByModelUnique() only accepts arguments of type \Model\System\DataModel\Model\UniqueFieldGroup or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ModelUnique relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUniqueFieldGroupFieldsQuery The current query, for fluid interface
     */
    public function joinModelUnique($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ModelUnique');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ModelUnique');
        }

        return $this;
    }

    /**
     * Use the ModelUnique relation UniqueFieldGroup object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\Model\UniqueFieldGroupQuery A secondary query class using the current class as primary query
     */
    public function useModelUniqueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinModelUnique($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ModelUnique', '\Model\System\DataModel\Model\UniqueFieldGroupQuery');
    }

    /**
     * Filter the query by a related \Model\System\DataModel\Field\DataField object
     *
     * @param \Model\System\DataModel\Field\DataField|ObjectCollection $dataField The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUniqueFieldGroupFieldsQuery The current query, for fluid interface
     */
    public function filterByDataField($dataField, $comparison = null)
    {
        if ($dataField instanceof \Model\System\DataModel\Field\DataField) {
            return $this
                ->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_DATA_FIELD_ID, $dataField->getId(), $comparison);
        } elseif ($dataField instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_DATA_FIELD_ID, $dataField->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDataField() only accepts arguments of type \Model\System\DataModel\Field\DataField or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DataField relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUniqueFieldGroupFieldsQuery The current query, for fluid interface
     */
    public function joinDataField($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DataField');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DataField');
        }

        return $this;
    }

    /**
     * Use the DataField relation DataField object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\Field\DataFieldQuery A secondary query class using the current class as primary query
     */
    public function useDataFieldQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDataField($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DataField', '\Model\System\DataModel\Field\DataFieldQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUniqueFieldGroupFields $uniqueFieldGroupFields Object to remove from the list of results
     *
     * @return $this|ChildUniqueFieldGroupFieldsQuery The current query, for fluid interface
     */
    public function prune($uniqueFieldGroupFields = null)
    {
        if ($uniqueFieldGroupFields) {
            $this->addUsingAlias(UniqueFieldGroupFieldsTableMap::COL_ID, $uniqueFieldGroupFields->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the unique_field_group_field table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UniqueFieldGroupFieldsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UniqueFieldGroupFieldsTableMap::clearInstancePool();
            UniqueFieldGroupFieldsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UniqueFieldGroupFieldsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UniqueFieldGroupFieldsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UniqueFieldGroupFieldsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UniqueFieldGroupFieldsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UniqueFieldGroupFieldsQuery
