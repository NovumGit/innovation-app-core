<?php

namespace Model\System\DataModel\Model\Base;

use \Exception;
use \PDO;
use Model\System\DataModel\Model\UniqueFieldGroup as ChildUniqueFieldGroup;
use Model\System\DataModel\Model\UniqueFieldGroupQuery as ChildUniqueFieldGroupQuery;
use Model\System\DataModel\Model\Map\UniqueFieldGroupTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'unique_field_group' table.
 *
 *
 *
 * @method     ChildUniqueFieldGroupQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUniqueFieldGroupQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildUniqueFieldGroupQuery orderByDataModelId($order = Criteria::ASC) Order by the data_model_id column
 *
 * @method     ChildUniqueFieldGroupQuery groupById() Group by the id column
 * @method     ChildUniqueFieldGroupQuery groupByName() Group by the name column
 * @method     ChildUniqueFieldGroupQuery groupByDataModelId() Group by the data_model_id column
 *
 * @method     ChildUniqueFieldGroupQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUniqueFieldGroupQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUniqueFieldGroupQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUniqueFieldGroupQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUniqueFieldGroupQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUniqueFieldGroupQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUniqueFieldGroupQuery leftJoinDataModel($relationAlias = null) Adds a LEFT JOIN clause to the query using the DataModel relation
 * @method     ChildUniqueFieldGroupQuery rightJoinDataModel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the DataModel relation
 * @method     ChildUniqueFieldGroupQuery innerJoinDataModel($relationAlias = null) Adds a INNER JOIN clause to the query using the DataModel relation
 *
 * @method     ChildUniqueFieldGroupQuery joinWithDataModel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the DataModel relation
 *
 * @method     ChildUniqueFieldGroupQuery leftJoinWithDataModel() Adds a LEFT JOIN clause and with to the query using the DataModel relation
 * @method     ChildUniqueFieldGroupQuery rightJoinWithDataModel() Adds a RIGHT JOIN clause and with to the query using the DataModel relation
 * @method     ChildUniqueFieldGroupQuery innerJoinWithDataModel() Adds a INNER JOIN clause and with to the query using the DataModel relation
 *
 * @method     ChildUniqueFieldGroupQuery leftJoinUniqueFieldGroupField($relationAlias = null) Adds a LEFT JOIN clause to the query using the UniqueFieldGroupField relation
 * @method     ChildUniqueFieldGroupQuery rightJoinUniqueFieldGroupField($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UniqueFieldGroupField relation
 * @method     ChildUniqueFieldGroupQuery innerJoinUniqueFieldGroupField($relationAlias = null) Adds a INNER JOIN clause to the query using the UniqueFieldGroupField relation
 *
 * @method     ChildUniqueFieldGroupQuery joinWithUniqueFieldGroupField($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UniqueFieldGroupField relation
 *
 * @method     ChildUniqueFieldGroupQuery leftJoinWithUniqueFieldGroupField() Adds a LEFT JOIN clause and with to the query using the UniqueFieldGroupField relation
 * @method     ChildUniqueFieldGroupQuery rightJoinWithUniqueFieldGroupField() Adds a RIGHT JOIN clause and with to the query using the UniqueFieldGroupField relation
 * @method     ChildUniqueFieldGroupQuery innerJoinWithUniqueFieldGroupField() Adds a INNER JOIN clause and with to the query using the UniqueFieldGroupField relation
 *
 * @method     \Model\System\DataModel\Model\DataModelQuery|\Model\System\DataModel\Model\UniqueFieldGroupFieldQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUniqueFieldGroup findOne(ConnectionInterface $con = null) Return the first ChildUniqueFieldGroup matching the query
 * @method     ChildUniqueFieldGroup findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUniqueFieldGroup matching the query, or a new ChildUniqueFieldGroup object populated from the query conditions when no match is found
 *
 * @method     ChildUniqueFieldGroup findOneById(int $id) Return the first ChildUniqueFieldGroup filtered by the id column
 * @method     ChildUniqueFieldGroup findOneByName(string $name) Return the first ChildUniqueFieldGroup filtered by the name column
 * @method     ChildUniqueFieldGroup findOneByDataModelId(int $data_model_id) Return the first ChildUniqueFieldGroup filtered by the data_model_id column *

 * @method     ChildUniqueFieldGroup requirePk($key, ConnectionInterface $con = null) Return the ChildUniqueFieldGroup by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUniqueFieldGroup requireOne(ConnectionInterface $con = null) Return the first ChildUniqueFieldGroup matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUniqueFieldGroup requireOneById(int $id) Return the first ChildUniqueFieldGroup filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUniqueFieldGroup requireOneByName(string $name) Return the first ChildUniqueFieldGroup filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUniqueFieldGroup requireOneByDataModelId(int $data_model_id) Return the first ChildUniqueFieldGroup filtered by the data_model_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUniqueFieldGroup[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUniqueFieldGroup objects based on current ModelCriteria
 * @method     ChildUniqueFieldGroup[]|ObjectCollection findById(int $id) Return ChildUniqueFieldGroup objects filtered by the id column
 * @method     ChildUniqueFieldGroup[]|ObjectCollection findByName(string $name) Return ChildUniqueFieldGroup objects filtered by the name column
 * @method     ChildUniqueFieldGroup[]|ObjectCollection findByDataModelId(int $data_model_id) Return ChildUniqueFieldGroup objects filtered by the data_model_id column
 * @method     ChildUniqueFieldGroup[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UniqueFieldGroupQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\DataModel\Model\Base\UniqueFieldGroupQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\DataModel\\Model\\UniqueFieldGroup', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUniqueFieldGroupQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUniqueFieldGroupQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUniqueFieldGroupQuery) {
            return $criteria;
        }
        $query = new ChildUniqueFieldGroupQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUniqueFieldGroup|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UniqueFieldGroupTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UniqueFieldGroupTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUniqueFieldGroup A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, data_model_id FROM unique_field_group WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUniqueFieldGroup $obj */
            $obj = new ChildUniqueFieldGroup();
            $obj->hydrate($row);
            UniqueFieldGroupTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUniqueFieldGroup|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUniqueFieldGroupQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UniqueFieldGroupTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUniqueFieldGroupQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UniqueFieldGroupTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUniqueFieldGroupQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UniqueFieldGroupTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UniqueFieldGroupTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UniqueFieldGroupTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUniqueFieldGroupQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UniqueFieldGroupTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the data_model_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDataModelId(1234); // WHERE data_model_id = 1234
     * $query->filterByDataModelId(array(12, 34)); // WHERE data_model_id IN (12, 34)
     * $query->filterByDataModelId(array('min' => 12)); // WHERE data_model_id > 12
     * </code>
     *
     * @see       filterByDataModel()
     *
     * @param     mixed $dataModelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUniqueFieldGroupQuery The current query, for fluid interface
     */
    public function filterByDataModelId($dataModelId = null, $comparison = null)
    {
        if (is_array($dataModelId)) {
            $useMinMax = false;
            if (isset($dataModelId['min'])) {
                $this->addUsingAlias(UniqueFieldGroupTableMap::COL_DATA_MODEL_ID, $dataModelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataModelId['max'])) {
                $this->addUsingAlias(UniqueFieldGroupTableMap::COL_DATA_MODEL_ID, $dataModelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UniqueFieldGroupTableMap::COL_DATA_MODEL_ID, $dataModelId, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\DataModel\Model\DataModel object
     *
     * @param \Model\System\DataModel\Model\DataModel|ObjectCollection $dataModel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUniqueFieldGroupQuery The current query, for fluid interface
     */
    public function filterByDataModel($dataModel, $comparison = null)
    {
        if ($dataModel instanceof \Model\System\DataModel\Model\DataModel) {
            return $this
                ->addUsingAlias(UniqueFieldGroupTableMap::COL_DATA_MODEL_ID, $dataModel->getId(), $comparison);
        } elseif ($dataModel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UniqueFieldGroupTableMap::COL_DATA_MODEL_ID, $dataModel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByDataModel() only accepts arguments of type \Model\System\DataModel\Model\DataModel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the DataModel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUniqueFieldGroupQuery The current query, for fluid interface
     */
    public function joinDataModel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('DataModel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'DataModel');
        }

        return $this;
    }

    /**
     * Use the DataModel relation DataModel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\Model\DataModelQuery A secondary query class using the current class as primary query
     */
    public function useDataModelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinDataModel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'DataModel', '\Model\System\DataModel\Model\DataModelQuery');
    }

    /**
     * Filter the query by a related \Model\System\DataModel\Model\UniqueFieldGroupField object
     *
     * @param \Model\System\DataModel\Model\UniqueFieldGroupField|ObjectCollection $uniqueFieldGroupField the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUniqueFieldGroupQuery The current query, for fluid interface
     */
    public function filterByUniqueFieldGroupField($uniqueFieldGroupField, $comparison = null)
    {
        if ($uniqueFieldGroupField instanceof \Model\System\DataModel\Model\UniqueFieldGroupField) {
            return $this
                ->addUsingAlias(UniqueFieldGroupTableMap::COL_ID, $uniqueFieldGroupField->getUniqueFieldGroupId(), $comparison);
        } elseif ($uniqueFieldGroupField instanceof ObjectCollection) {
            return $this
                ->useUniqueFieldGroupFieldQuery()
                ->filterByPrimaryKeys($uniqueFieldGroupField->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUniqueFieldGroupField() only accepts arguments of type \Model\System\DataModel\Model\UniqueFieldGroupField or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UniqueFieldGroupField relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUniqueFieldGroupQuery The current query, for fluid interface
     */
    public function joinUniqueFieldGroupField($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UniqueFieldGroupField');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UniqueFieldGroupField');
        }

        return $this;
    }

    /**
     * Use the UniqueFieldGroupField relation UniqueFieldGroupField object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\Model\UniqueFieldGroupFieldQuery A secondary query class using the current class as primary query
     */
    public function useUniqueFieldGroupFieldQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUniqueFieldGroupField($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UniqueFieldGroupField', '\Model\System\DataModel\Model\UniqueFieldGroupFieldQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUniqueFieldGroup $uniqueFieldGroup Object to remove from the list of results
     *
     * @return $this|ChildUniqueFieldGroupQuery The current query, for fluid interface
     */
    public function prune($uniqueFieldGroup = null)
    {
        if ($uniqueFieldGroup) {
            $this->addUsingAlias(UniqueFieldGroupTableMap::COL_ID, $uniqueFieldGroup->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the unique_field_group table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UniqueFieldGroupTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UniqueFieldGroupTableMap::clearInstancePool();
            UniqueFieldGroupTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UniqueFieldGroupTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UniqueFieldGroupTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UniqueFieldGroupTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UniqueFieldGroupTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UniqueFieldGroupQuery
