<?php

namespace Model\System\DataModel\Model;

use Model\System\DataModel\Model\Base\UniqueFieldGroup as BaseUniqueFieldGroup;

/**
 * Skeleton subclass for representing a row from the 'unique_field_group' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class UniqueFieldGroup extends BaseUniqueFieldGroup
{

}
