<?php

namespace Model\System\DataModel\Model\Map;

use Model\System\DataModel\Model\ModelConstraint;
use Model\System\DataModel\Model\ModelConstraintQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'model_constraint' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class ModelConstraintTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.System.DataModel.Model.Map.ModelConstraintTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'model_constraint';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\System\\DataModel\\Model\\ModelConstraint';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.System.DataModel.Model.ModelConstraint';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the id field
     */
    const COL_ID = 'model_constraint.id';

    /**
     * the column name for the php_name field
     */
    const COL_PHP_NAME = 'model_constraint.php_name';

    /**
     * the column name for the ref_php_name field
     */
    const COL_REF_PHP_NAME = 'model_constraint.ref_php_name';

    /**
     * the column name for the foreign_field_id field
     */
    const COL_FOREIGN_FIELD_ID = 'model_constraint.foreign_field_id';

    /**
     * the column name for the local_field_id field
     */
    const COL_LOCAL_FIELD_ID = 'model_constraint.local_field_id';

    /**
     * the column name for the on_delete field
     */
    const COL_ON_DELETE = 'model_constraint.on_delete';

    /**
     * the column name for the on_update field
     */
    const COL_ON_UPDATE = 'model_constraint.on_update';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'PhpName', 'RefPhpName', 'ForeignFieldId', 'LocalFieldId', 'OnDelete', 'OnUpdate', ),
        self::TYPE_CAMELNAME     => array('id', 'phpName', 'refPhpName', 'foreignFieldId', 'localFieldId', 'onDelete', 'onUpdate', ),
        self::TYPE_COLNAME       => array(ModelConstraintTableMap::COL_ID, ModelConstraintTableMap::COL_PHP_NAME, ModelConstraintTableMap::COL_REF_PHP_NAME, ModelConstraintTableMap::COL_FOREIGN_FIELD_ID, ModelConstraintTableMap::COL_LOCAL_FIELD_ID, ModelConstraintTableMap::COL_ON_DELETE, ModelConstraintTableMap::COL_ON_UPDATE, ),
        self::TYPE_FIELDNAME     => array('id', 'php_name', 'ref_php_name', 'foreign_field_id', 'local_field_id', 'on_delete', 'on_update', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'PhpName' => 1, 'RefPhpName' => 2, 'ForeignFieldId' => 3, 'LocalFieldId' => 4, 'OnDelete' => 5, 'OnUpdate' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'phpName' => 1, 'refPhpName' => 2, 'foreignFieldId' => 3, 'localFieldId' => 4, 'onDelete' => 5, 'onUpdate' => 6, ),
        self::TYPE_COLNAME       => array(ModelConstraintTableMap::COL_ID => 0, ModelConstraintTableMap::COL_PHP_NAME => 1, ModelConstraintTableMap::COL_REF_PHP_NAME => 2, ModelConstraintTableMap::COL_FOREIGN_FIELD_ID => 3, ModelConstraintTableMap::COL_LOCAL_FIELD_ID => 4, ModelConstraintTableMap::COL_ON_DELETE => 5, ModelConstraintTableMap::COL_ON_UPDATE => 6, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'php_name' => 1, 'ref_php_name' => 2, 'foreign_field_id' => 3, 'local_field_id' => 4, 'on_delete' => 5, 'on_update' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('model_constraint');
        $this->setPhpName('ModelConstraint');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\System\\DataModel\\Model\\ModelConstraint');
        $this->setPackage('Model.System.DataModel.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('php_name', 'PhpName', 'VARCHAR', true, 255, null);
        $this->addColumn('ref_php_name', 'RefPhpName', 'VARCHAR', true, 255, null);
        $this->addForeignKey('foreign_field_id', 'ForeignFieldId', 'INTEGER', 'data_field', 'id', true, null, null);
        $this->addForeignKey('local_field_id', 'LocalFieldId', 'INTEGER', 'data_field', 'id', true, null, null);
        $this->addColumn('on_delete', 'OnDelete', 'VARCHAR', true, 255, null);
        $this->addColumn('on_update', 'OnUpdate', 'VARCHAR', true, 255, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('FkLocalField', '\\Model\\System\\DataModel\\Field\\DataField', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':local_field_id',
    1 => ':id',
  ),
), 'RESTRICT', 'RESTRICT', null, false);
        $this->addRelation('FkForeignField', '\\Model\\System\\DataModel\\Field\\DataField', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':foreign_field_id',
    1 => ':id',
  ),
), 'RESTRICT', 'RESTRICT', null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ModelConstraintTableMap::CLASS_DEFAULT : ModelConstraintTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ModelConstraint object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ModelConstraintTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ModelConstraintTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ModelConstraintTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ModelConstraintTableMap::OM_CLASS;
            /** @var ModelConstraint $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ModelConstraintTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ModelConstraintTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ModelConstraintTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ModelConstraint $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ModelConstraintTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ModelConstraintTableMap::COL_ID);
            $criteria->addSelectColumn(ModelConstraintTableMap::COL_PHP_NAME);
            $criteria->addSelectColumn(ModelConstraintTableMap::COL_REF_PHP_NAME);
            $criteria->addSelectColumn(ModelConstraintTableMap::COL_FOREIGN_FIELD_ID);
            $criteria->addSelectColumn(ModelConstraintTableMap::COL_LOCAL_FIELD_ID);
            $criteria->addSelectColumn(ModelConstraintTableMap::COL_ON_DELETE);
            $criteria->addSelectColumn(ModelConstraintTableMap::COL_ON_UPDATE);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.php_name');
            $criteria->addSelectColumn($alias . '.ref_php_name');
            $criteria->addSelectColumn($alias . '.foreign_field_id');
            $criteria->addSelectColumn($alias . '.local_field_id');
            $criteria->addSelectColumn($alias . '.on_delete');
            $criteria->addSelectColumn($alias . '.on_update');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ModelConstraintTableMap::DATABASE_NAME)->getTable(ModelConstraintTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ModelConstraintTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ModelConstraintTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ModelConstraintTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ModelConstraint or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ModelConstraint object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ModelConstraintTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\System\DataModel\Model\ModelConstraint) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ModelConstraintTableMap::DATABASE_NAME);
            $criteria->add(ModelConstraintTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = ModelConstraintQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ModelConstraintTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ModelConstraintTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the model_constraint table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ModelConstraintQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ModelConstraint or Criteria object.
     *
     * @param mixed               $criteria Criteria or ModelConstraint object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ModelConstraintTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ModelConstraint object
        }

        if ($criteria->containsKey(ModelConstraintTableMap::COL_ID) && $criteria->keyContainsValue(ModelConstraintTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ModelConstraintTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = ModelConstraintQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ModelConstraintTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ModelConstraintTableMap::buildTableMap();
