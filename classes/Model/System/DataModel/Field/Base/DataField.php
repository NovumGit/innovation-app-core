<?php

namespace Model\System\DataModel\Field\Base;

use \Exception;
use \PDO;
use Model\System\DataModel\DataType;
use Model\System\DataModel\DataTypeQuery;
use Model\System\DataModel\Field\DataField as ChildDataField;
use Model\System\DataModel\Field\DataFieldQuery as ChildDataFieldQuery;
use Model\System\DataModel\Field\Map\DataFieldTableMap;
use Model\System\DataModel\Model\DataModel;
use Model\System\DataModel\Model\DataModelQuery;
use Model\System\DataModel\Model\ModelConstraint;
use Model\System\DataModel\Model\ModelConstraintQuery;
use Model\System\DataModel\Model\UniqueFieldGroupField;
use Model\System\DataModel\Model\UniqueFieldGroupFieldQuery;
use Model\System\DataModel\Model\Base\ModelConstraint as BaseModelConstraint;
use Model\System\DataModel\Model\Base\UniqueFieldGroupField as BaseUniqueFieldGroupField;
use Model\System\DataModel\Model\Map\ModelConstraintTableMap;
use Model\System\DataModel\Model\Map\UniqueFieldGroupFieldTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'data_field' table.
 *
 *
 *
 * @package    propel.generator.Model.System.DataModel.Field.Base
 */
abstract class DataField implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\System\\DataModel\\Field\\Map\\DataFieldTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the data_model_id field.
     *
     * @var        int
     */
    protected $data_model_id;

    /**
     * The value for the name field.
     *
     * @var        string
     */
    protected $name;

    /**
     * The value for the php_name field.
     *
     * @var        string|null
     */
    protected $php_name;

    /**
     * The value for the label field.
     *
     * @var        string
     */
    protected $label;

    /**
     * The value for the size field.
     *
     * @var        string|null
     */
    protected $size;

    /**
     * The value for the icon_id field.
     *
     * @var        int|null
     */
    protected $icon_id;

    /**
     * The value for the form_field_type_id field.
     *
     * @var        int|null
     */
    protected $form_field_type_id;

    /**
     * The value for the form_field_lookups field.
     *
     * @var        string|null
     */
    protected $form_field_lookups;

    /**
     * The value for the data_type_id field.
     *
     * @var        int
     */
    protected $data_type_id;

    /**
     * The value for the required field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $required;

    /**
     * The value for the is_primary_key field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $is_primary_key;

    /**
     * The value for the auto_increment field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean|null
     */
    protected $auto_increment;

    /**
     * The value for the default_value field.
     *
     * @var        string|null
     */
    protected $default_value;

    /**
     * @var        DataModel
     */
    protected $aFkDataModel;

    /**
     * @var        DataType
     */
    protected $aFkDataType;

    /**
     * @var        ObjectCollection|UniqueFieldGroupField[] Collection to store aggregation of UniqueFieldGroupField objects.
     */
    protected $collUniqueFieldGroupFields;
    protected $collUniqueFieldGroupFieldsPartial;

    /**
     * @var        ObjectCollection|ModelConstraint[] Collection to store aggregation of ModelConstraint objects.
     */
    protected $collModelConstraintsRelatedByLocalFieldId;
    protected $collModelConstraintsRelatedByLocalFieldIdPartial;

    /**
     * @var        ObjectCollection|ModelConstraint[] Collection to store aggregation of ModelConstraint objects.
     */
    protected $collModelConstraintsRelatedByForeignFieldId;
    protected $collModelConstraintsRelatedByForeignFieldIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|UniqueFieldGroupField[]
     */
    protected $uniqueFieldGroupFieldsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ModelConstraint[]
     */
    protected $modelConstraintsRelatedByLocalFieldIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ModelConstraint[]
     */
    protected $modelConstraintsRelatedByForeignFieldIdScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->required = false;
        $this->is_primary_key = false;
        $this->auto_increment = false;
    }

    /**
     * Initializes internal state of Model\System\DataModel\Field\Base\DataField object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>DataField</code> instance.  If
     * <code>obj</code> is an instance of <code>DataField</code>, delegates to
     * <code>equals(DataField)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [data_model_id] column value.
     *
     * @return int
     */
    public function getDataModelId()
    {
        return $this->data_model_id;
    }

    /**
     * Get the [name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the [php_name] column value.
     *
     * @return string|null
     */
    public function getPhpName()
    {
        return $this->php_name;
    }

    /**
     * Get the [label] column value.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Get the [size] column value.
     *
     * @return string|null
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Get the [icon_id] column value.
     *
     * @return int|null
     */
    public function getIconId()
    {
        return $this->icon_id;
    }

    /**
     * Get the [form_field_type_id] column value.
     *
     * @return int|null
     */
    public function getFormFieldTypeId()
    {
        return $this->form_field_type_id;
    }

    /**
     * Get the [form_field_lookups] column value.
     *
     * @return string|null
     */
    public function getFormFieldLookups()
    {
        return $this->form_field_lookups;
    }

    /**
     * Get the [data_type_id] column value.
     *
     * @return int
     */
    public function getDataTypeId()
    {
        return $this->data_type_id;
    }

    /**
     * Get the [required] column value.
     *
     * @return boolean
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Get the [required] column value.
     *
     * @return boolean
     */
    public function isRequired()
    {
        return $this->getRequired();
    }

    /**
     * Get the [is_primary_key] column value.
     *
     * @return boolean|null
     */
    public function getIsPrimaryKey()
    {
        return $this->is_primary_key;
    }

    /**
     * Get the [is_primary_key] column value.
     *
     * @return boolean|null
     */
    public function isPrimaryKey()
    {
        return $this->getIsPrimaryKey();
    }

    /**
     * Get the [auto_increment] column value.
     *
     * @return boolean|null
     */
    public function getAutoIncrement()
    {
        return $this->auto_increment;
    }

    /**
     * Get the [auto_increment] column value.
     *
     * @return boolean|null
     */
    public function isAutoIncrement()
    {
        return $this->getAutoIncrement();
    }

    /**
     * Get the [default_value] column value.
     *
     * @return string|null
     */
    public function getDefaultValue()
    {
        return $this->default_value;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [data_model_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setDataModelId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->data_model_id !== $v) {
            $this->data_model_id = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_DATA_MODEL_ID] = true;
        }

        if ($this->aFkDataModel !== null && $this->aFkDataModel->getId() !== $v) {
            $this->aFkDataModel = null;
        }

        return $this;
    } // setDataModelId()

    /**
     * Set the value of [name] column.
     *
     * @param string $v New value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->name !== $v) {
            $this->name = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [php_name] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setPhpName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->php_name !== $v) {
            $this->php_name = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_PHP_NAME] = true;
        }

        return $this;
    } // setPhpName()

    /**
     * Set the value of [label] column.
     *
     * @param string $v New value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setLabel($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->label !== $v) {
            $this->label = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_LABEL] = true;
        }

        return $this;
    } // setLabel()

    /**
     * Set the value of [size] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setSize($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->size !== $v) {
            $this->size = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_SIZE] = true;
        }

        return $this;
    } // setSize()

    /**
     * Set the value of [icon_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setIconId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->icon_id !== $v) {
            $this->icon_id = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_ICON_ID] = true;
        }

        return $this;
    } // setIconId()

    /**
     * Set the value of [form_field_type_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setFormFieldTypeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->form_field_type_id !== $v) {
            $this->form_field_type_id = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_FORM_FIELD_TYPE_ID] = true;
        }

        return $this;
    } // setFormFieldTypeId()

    /**
     * Set the value of [form_field_lookups] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setFormFieldLookups($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->form_field_lookups !== $v) {
            $this->form_field_lookups = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_FORM_FIELD_LOOKUPS] = true;
        }

        return $this;
    } // setFormFieldLookups()

    /**
     * Set the value of [data_type_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setDataTypeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->data_type_id !== $v) {
            $this->data_type_id = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_DATA_TYPE_ID] = true;
        }

        if ($this->aFkDataType !== null && $this->aFkDataType->getId() !== $v) {
            $this->aFkDataType = null;
        }

        return $this;
    } // setDataTypeId()

    /**
     * Sets the value of the [required] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setRequired($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->required !== $v) {
            $this->required = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_REQUIRED] = true;
        }

        return $this;
    } // setRequired()

    /**
     * Sets the value of the [is_primary_key] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setIsPrimaryKey($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_primary_key !== $v) {
            $this->is_primary_key = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_IS_PRIMARY_KEY] = true;
        }

        return $this;
    } // setIsPrimaryKey()

    /**
     * Sets the value of the [auto_increment] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setAutoIncrement($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->auto_increment !== $v) {
            $this->auto_increment = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_AUTO_INCREMENT] = true;
        }

        return $this;
    } // setAutoIncrement()

    /**
     * Set the value of [default_value] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function setDefaultValue($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->default_value !== $v) {
            $this->default_value = $v;
            $this->modifiedColumns[DataFieldTableMap::COL_DEFAULT_VALUE] = true;
        }

        return $this;
    } // setDefaultValue()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->required !== false) {
                return false;
            }

            if ($this->is_primary_key !== false) {
                return false;
            }

            if ($this->auto_increment !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : DataFieldTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : DataFieldTableMap::translateFieldName('DataModelId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->data_model_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : DataFieldTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : DataFieldTableMap::translateFieldName('PhpName', TableMap::TYPE_PHPNAME, $indexType)];
            $this->php_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : DataFieldTableMap::translateFieldName('Label', TableMap::TYPE_PHPNAME, $indexType)];
            $this->label = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : DataFieldTableMap::translateFieldName('Size', TableMap::TYPE_PHPNAME, $indexType)];
            $this->size = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : DataFieldTableMap::translateFieldName('IconId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->icon_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : DataFieldTableMap::translateFieldName('FormFieldTypeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->form_field_type_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : DataFieldTableMap::translateFieldName('FormFieldLookups', TableMap::TYPE_PHPNAME, $indexType)];
            $this->form_field_lookups = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : DataFieldTableMap::translateFieldName('DataTypeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->data_type_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : DataFieldTableMap::translateFieldName('Required', TableMap::TYPE_PHPNAME, $indexType)];
            $this->required = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : DataFieldTableMap::translateFieldName('IsPrimaryKey', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_primary_key = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : DataFieldTableMap::translateFieldName('AutoIncrement', TableMap::TYPE_PHPNAME, $indexType)];
            $this->auto_increment = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : DataFieldTableMap::translateFieldName('DefaultValue', TableMap::TYPE_PHPNAME, $indexType)];
            $this->default_value = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 14; // 14 = DataFieldTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\System\\DataModel\\Field\\DataField'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aFkDataModel !== null && $this->data_model_id !== $this->aFkDataModel->getId()) {
            $this->aFkDataModel = null;
        }
        if ($this->aFkDataType !== null && $this->data_type_id !== $this->aFkDataType->getId()) {
            $this->aFkDataType = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DataFieldTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildDataFieldQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aFkDataModel = null;
            $this->aFkDataType = null;
            $this->collUniqueFieldGroupFields = null;

            $this->collModelConstraintsRelatedByLocalFieldId = null;

            $this->collModelConstraintsRelatedByForeignFieldId = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see DataField::setDeleted()
     * @see DataField::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(DataFieldTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildDataFieldQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(DataFieldTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                DataFieldTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aFkDataModel !== null) {
                if ($this->aFkDataModel->isModified() || $this->aFkDataModel->isNew()) {
                    $affectedRows += $this->aFkDataModel->save($con);
                }
                $this->setFkDataModel($this->aFkDataModel);
            }

            if ($this->aFkDataType !== null) {
                if ($this->aFkDataType->isModified() || $this->aFkDataType->isNew()) {
                    $affectedRows += $this->aFkDataType->save($con);
                }
                $this->setFkDataType($this->aFkDataType);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->uniqueFieldGroupFieldsScheduledForDeletion !== null) {
                if (!$this->uniqueFieldGroupFieldsScheduledForDeletion->isEmpty()) {
                    \Model\System\DataModel\Model\UniqueFieldGroupFieldQuery::create()
                        ->filterByPrimaryKeys($this->uniqueFieldGroupFieldsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->uniqueFieldGroupFieldsScheduledForDeletion = null;
                }
            }

            if ($this->collUniqueFieldGroupFields !== null) {
                foreach ($this->collUniqueFieldGroupFields as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->modelConstraintsRelatedByLocalFieldIdScheduledForDeletion !== null) {
                if (!$this->modelConstraintsRelatedByLocalFieldIdScheduledForDeletion->isEmpty()) {
                    \Model\System\DataModel\Model\ModelConstraintQuery::create()
                        ->filterByPrimaryKeys($this->modelConstraintsRelatedByLocalFieldIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->modelConstraintsRelatedByLocalFieldIdScheduledForDeletion = null;
                }
            }

            if ($this->collModelConstraintsRelatedByLocalFieldId !== null) {
                foreach ($this->collModelConstraintsRelatedByLocalFieldId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->modelConstraintsRelatedByForeignFieldIdScheduledForDeletion !== null) {
                if (!$this->modelConstraintsRelatedByForeignFieldIdScheduledForDeletion->isEmpty()) {
                    \Model\System\DataModel\Model\ModelConstraintQuery::create()
                        ->filterByPrimaryKeys($this->modelConstraintsRelatedByForeignFieldIdScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->modelConstraintsRelatedByForeignFieldIdScheduledForDeletion = null;
                }
            }

            if ($this->collModelConstraintsRelatedByForeignFieldId !== null) {
                foreach ($this->collModelConstraintsRelatedByForeignFieldId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[DataFieldTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . DataFieldTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(DataFieldTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_DATA_MODEL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'data_model_id';
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'name';
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_PHP_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'php_name';
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_LABEL)) {
            $modifiedColumns[':p' . $index++]  = 'label';
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_SIZE)) {
            $modifiedColumns[':p' . $index++]  = 'size';
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_ICON_ID)) {
            $modifiedColumns[':p' . $index++]  = 'icon_id';
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_FORM_FIELD_TYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'form_field_type_id';
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_FORM_FIELD_LOOKUPS)) {
            $modifiedColumns[':p' . $index++]  = 'form_field_lookups';
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_DATA_TYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'data_type_id';
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_REQUIRED)) {
            $modifiedColumns[':p' . $index++]  = 'required';
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_IS_PRIMARY_KEY)) {
            $modifiedColumns[':p' . $index++]  = 'is_primary_key';
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_AUTO_INCREMENT)) {
            $modifiedColumns[':p' . $index++]  = 'auto_increment';
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_DEFAULT_VALUE)) {
            $modifiedColumns[':p' . $index++]  = 'default_value';
        }

        $sql = sprintf(
            'INSERT INTO data_field (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'data_model_id':
                        $stmt->bindValue($identifier, $this->data_model_id, PDO::PARAM_INT);
                        break;
                    case 'name':
                        $stmt->bindValue($identifier, $this->name, PDO::PARAM_STR);
                        break;
                    case 'php_name':
                        $stmt->bindValue($identifier, $this->php_name, PDO::PARAM_STR);
                        break;
                    case 'label':
                        $stmt->bindValue($identifier, $this->label, PDO::PARAM_STR);
                        break;
                    case 'size':
                        $stmt->bindValue($identifier, $this->size, PDO::PARAM_STR);
                        break;
                    case 'icon_id':
                        $stmt->bindValue($identifier, $this->icon_id, PDO::PARAM_INT);
                        break;
                    case 'form_field_type_id':
                        $stmt->bindValue($identifier, $this->form_field_type_id, PDO::PARAM_INT);
                        break;
                    case 'form_field_lookups':
                        $stmt->bindValue($identifier, $this->form_field_lookups, PDO::PARAM_STR);
                        break;
                    case 'data_type_id':
                        $stmt->bindValue($identifier, $this->data_type_id, PDO::PARAM_INT);
                        break;
                    case 'required':
                        $stmt->bindValue($identifier, (int) $this->required, PDO::PARAM_INT);
                        break;
                    case 'is_primary_key':
                        $stmt->bindValue($identifier, (int) $this->is_primary_key, PDO::PARAM_INT);
                        break;
                    case 'auto_increment':
                        $stmt->bindValue($identifier, (int) $this->auto_increment, PDO::PARAM_INT);
                        break;
                    case 'default_value':
                        $stmt->bindValue($identifier, $this->default_value, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = DataFieldTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getDataModelId();
                break;
            case 2:
                return $this->getName();
                break;
            case 3:
                return $this->getPhpName();
                break;
            case 4:
                return $this->getLabel();
                break;
            case 5:
                return $this->getSize();
                break;
            case 6:
                return $this->getIconId();
                break;
            case 7:
                return $this->getFormFieldTypeId();
                break;
            case 8:
                return $this->getFormFieldLookups();
                break;
            case 9:
                return $this->getDataTypeId();
                break;
            case 10:
                return $this->getRequired();
                break;
            case 11:
                return $this->getIsPrimaryKey();
                break;
            case 12:
                return $this->getAutoIncrement();
                break;
            case 13:
                return $this->getDefaultValue();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['DataField'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['DataField'][$this->hashCode()] = true;
        $keys = DataFieldTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getDataModelId(),
            $keys[2] => $this->getName(),
            $keys[3] => $this->getPhpName(),
            $keys[4] => $this->getLabel(),
            $keys[5] => $this->getSize(),
            $keys[6] => $this->getIconId(),
            $keys[7] => $this->getFormFieldTypeId(),
            $keys[8] => $this->getFormFieldLookups(),
            $keys[9] => $this->getDataTypeId(),
            $keys[10] => $this->getRequired(),
            $keys[11] => $this->getIsPrimaryKey(),
            $keys[12] => $this->getAutoIncrement(),
            $keys[13] => $this->getDefaultValue(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aFkDataModel) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dataModel';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'data_model';
                        break;
                    default:
                        $key = 'FkDataModel';
                }

                $result[$key] = $this->aFkDataModel->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aFkDataType) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'dataType';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'data_type';
                        break;
                    default:
                        $key = 'FkDataType';
                }

                $result[$key] = $this->aFkDataType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collUniqueFieldGroupFields) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'uniqueFieldGroupFields';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'unique_field_group_fields';
                        break;
                    default:
                        $key = 'UniqueFieldGroupFields';
                }

                $result[$key] = $this->collUniqueFieldGroupFields->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collModelConstraintsRelatedByLocalFieldId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'modelConstraints';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'model_constraints';
                        break;
                    default:
                        $key = 'ModelConstraints';
                }

                $result[$key] = $this->collModelConstraintsRelatedByLocalFieldId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collModelConstraintsRelatedByForeignFieldId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'modelConstraints';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'model_constraints';
                        break;
                    default:
                        $key = 'ModelConstraints';
                }

                $result[$key] = $this->collModelConstraintsRelatedByForeignFieldId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\System\DataModel\Field\DataField
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = DataFieldTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\System\DataModel\Field\DataField
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setDataModelId($value);
                break;
            case 2:
                $this->setName($value);
                break;
            case 3:
                $this->setPhpName($value);
                break;
            case 4:
                $this->setLabel($value);
                break;
            case 5:
                $this->setSize($value);
                break;
            case 6:
                $this->setIconId($value);
                break;
            case 7:
                $this->setFormFieldTypeId($value);
                break;
            case 8:
                $this->setFormFieldLookups($value);
                break;
            case 9:
                $this->setDataTypeId($value);
                break;
            case 10:
                $this->setRequired($value);
                break;
            case 11:
                $this->setIsPrimaryKey($value);
                break;
            case 12:
                $this->setAutoIncrement($value);
                break;
            case 13:
                $this->setDefaultValue($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = DataFieldTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setDataModelId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setName($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setPhpName($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setLabel($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setSize($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setIconId($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setFormFieldTypeId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setFormFieldLookups($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setDataTypeId($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setRequired($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setIsPrimaryKey($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setAutoIncrement($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setDefaultValue($arr[$keys[13]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\System\DataModel\Field\DataField The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(DataFieldTableMap::DATABASE_NAME);

        if ($this->isColumnModified(DataFieldTableMap::COL_ID)) {
            $criteria->add(DataFieldTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_DATA_MODEL_ID)) {
            $criteria->add(DataFieldTableMap::COL_DATA_MODEL_ID, $this->data_model_id);
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_NAME)) {
            $criteria->add(DataFieldTableMap::COL_NAME, $this->name);
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_PHP_NAME)) {
            $criteria->add(DataFieldTableMap::COL_PHP_NAME, $this->php_name);
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_LABEL)) {
            $criteria->add(DataFieldTableMap::COL_LABEL, $this->label);
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_SIZE)) {
            $criteria->add(DataFieldTableMap::COL_SIZE, $this->size);
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_ICON_ID)) {
            $criteria->add(DataFieldTableMap::COL_ICON_ID, $this->icon_id);
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_FORM_FIELD_TYPE_ID)) {
            $criteria->add(DataFieldTableMap::COL_FORM_FIELD_TYPE_ID, $this->form_field_type_id);
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_FORM_FIELD_LOOKUPS)) {
            $criteria->add(DataFieldTableMap::COL_FORM_FIELD_LOOKUPS, $this->form_field_lookups);
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_DATA_TYPE_ID)) {
            $criteria->add(DataFieldTableMap::COL_DATA_TYPE_ID, $this->data_type_id);
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_REQUIRED)) {
            $criteria->add(DataFieldTableMap::COL_REQUIRED, $this->required);
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_IS_PRIMARY_KEY)) {
            $criteria->add(DataFieldTableMap::COL_IS_PRIMARY_KEY, $this->is_primary_key);
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_AUTO_INCREMENT)) {
            $criteria->add(DataFieldTableMap::COL_AUTO_INCREMENT, $this->auto_increment);
        }
        if ($this->isColumnModified(DataFieldTableMap::COL_DEFAULT_VALUE)) {
            $criteria->add(DataFieldTableMap::COL_DEFAULT_VALUE, $this->default_value);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildDataFieldQuery::create();
        $criteria->add(DataFieldTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\System\DataModel\Field\DataField (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setDataModelId($this->getDataModelId());
        $copyObj->setName($this->getName());
        $copyObj->setPhpName($this->getPhpName());
        $copyObj->setLabel($this->getLabel());
        $copyObj->setSize($this->getSize());
        $copyObj->setIconId($this->getIconId());
        $copyObj->setFormFieldTypeId($this->getFormFieldTypeId());
        $copyObj->setFormFieldLookups($this->getFormFieldLookups());
        $copyObj->setDataTypeId($this->getDataTypeId());
        $copyObj->setRequired($this->getRequired());
        $copyObj->setIsPrimaryKey($this->getIsPrimaryKey());
        $copyObj->setAutoIncrement($this->getAutoIncrement());
        $copyObj->setDefaultValue($this->getDefaultValue());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getUniqueFieldGroupFields() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUniqueFieldGroupField($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getModelConstraintsRelatedByLocalFieldId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addModelConstraintRelatedByLocalFieldId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getModelConstraintsRelatedByForeignFieldId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addModelConstraintRelatedByForeignFieldId($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\System\DataModel\Field\DataField Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a DataModel object.
     *
     * @param  DataModel $v
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     * @throws PropelException
     */
    public function setFkDataModel(DataModel $v = null)
    {
        if ($v === null) {
            $this->setDataModelId(NULL);
        } else {
            $this->setDataModelId($v->getId());
        }

        $this->aFkDataModel = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the DataModel object, it will not be re-added.
        if ($v !== null) {
            $v->addDataField($this);
        }


        return $this;
    }


    /**
     * Get the associated DataModel object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return DataModel The associated DataModel object.
     * @throws PropelException
     */
    public function getFkDataModel(ConnectionInterface $con = null)
    {
        if ($this->aFkDataModel === null && ($this->data_model_id != 0)) {
            $this->aFkDataModel = DataModelQuery::create()->findPk($this->data_model_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aFkDataModel->addDataFields($this);
             */
        }

        return $this->aFkDataModel;
    }

    /**
     * Declares an association between this object and a DataType object.
     *
     * @param  DataType $v
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     * @throws PropelException
     */
    public function setFkDataType(DataType $v = null)
    {
        if ($v === null) {
            $this->setDataTypeId(NULL);
        } else {
            $this->setDataTypeId($v->getId());
        }

        $this->aFkDataType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the DataType object, it will not be re-added.
        if ($v !== null) {
            $v->addDataField($this);
        }


        return $this;
    }


    /**
     * Get the associated DataType object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return DataType The associated DataType object.
     * @throws PropelException
     */
    public function getFkDataType(ConnectionInterface $con = null)
    {
        if ($this->aFkDataType === null && ($this->data_type_id != 0)) {
            $this->aFkDataType = DataTypeQuery::create()->findPk($this->data_type_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aFkDataType->addDataFields($this);
             */
        }

        return $this->aFkDataType;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('UniqueFieldGroupField' === $relationName) {
            $this->initUniqueFieldGroupFields();
            return;
        }
        if ('ModelConstraintRelatedByLocalFieldId' === $relationName) {
            $this->initModelConstraintsRelatedByLocalFieldId();
            return;
        }
        if ('ModelConstraintRelatedByForeignFieldId' === $relationName) {
            $this->initModelConstraintsRelatedByForeignFieldId();
            return;
        }
    }

    /**
     * Clears out the collUniqueFieldGroupFields collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUniqueFieldGroupFields()
     */
    public function clearUniqueFieldGroupFields()
    {
        $this->collUniqueFieldGroupFields = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUniqueFieldGroupFields collection loaded partially.
     */
    public function resetPartialUniqueFieldGroupFields($v = true)
    {
        $this->collUniqueFieldGroupFieldsPartial = $v;
    }

    /**
     * Initializes the collUniqueFieldGroupFields collection.
     *
     * By default this just sets the collUniqueFieldGroupFields collection to an empty array (like clearcollUniqueFieldGroupFields());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUniqueFieldGroupFields($overrideExisting = true)
    {
        if (null !== $this->collUniqueFieldGroupFields && !$overrideExisting) {
            return;
        }

        $collectionClassName = UniqueFieldGroupFieldTableMap::getTableMap()->getCollectionClassName();

        $this->collUniqueFieldGroupFields = new $collectionClassName;
        $this->collUniqueFieldGroupFields->setModel('\Model\System\DataModel\Model\UniqueFieldGroupField');
    }

    /**
     * Gets an array of UniqueFieldGroupField objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDataField is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|UniqueFieldGroupField[] List of UniqueFieldGroupField objects
     * @throws PropelException
     */
    public function getUniqueFieldGroupFields(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUniqueFieldGroupFieldsPartial && !$this->isNew();
        if (null === $this->collUniqueFieldGroupFields || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUniqueFieldGroupFields) {
                    $this->initUniqueFieldGroupFields();
                } else {
                    $collectionClassName = UniqueFieldGroupFieldTableMap::getTableMap()->getCollectionClassName();

                    $collUniqueFieldGroupFields = new $collectionClassName;
                    $collUniqueFieldGroupFields->setModel('\Model\System\DataModel\Model\UniqueFieldGroupField');

                    return $collUniqueFieldGroupFields;
                }
            } else {
                $collUniqueFieldGroupFields = UniqueFieldGroupFieldQuery::create(null, $criteria)
                    ->filterByDataField($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUniqueFieldGroupFieldsPartial && count($collUniqueFieldGroupFields)) {
                        $this->initUniqueFieldGroupFields(false);

                        foreach ($collUniqueFieldGroupFields as $obj) {
                            if (false == $this->collUniqueFieldGroupFields->contains($obj)) {
                                $this->collUniqueFieldGroupFields->append($obj);
                            }
                        }

                        $this->collUniqueFieldGroupFieldsPartial = true;
                    }

                    return $collUniqueFieldGroupFields;
                }

                if ($partial && $this->collUniqueFieldGroupFields) {
                    foreach ($this->collUniqueFieldGroupFields as $obj) {
                        if ($obj->isNew()) {
                            $collUniqueFieldGroupFields[] = $obj;
                        }
                    }
                }

                $this->collUniqueFieldGroupFields = $collUniqueFieldGroupFields;
                $this->collUniqueFieldGroupFieldsPartial = false;
            }
        }

        return $this->collUniqueFieldGroupFields;
    }

    /**
     * Sets a collection of UniqueFieldGroupField objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $uniqueFieldGroupFields A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDataField The current object (for fluent API support)
     */
    public function setUniqueFieldGroupFields(Collection $uniqueFieldGroupFields, ConnectionInterface $con = null)
    {
        /** @var UniqueFieldGroupField[] $uniqueFieldGroupFieldsToDelete */
        $uniqueFieldGroupFieldsToDelete = $this->getUniqueFieldGroupFields(new Criteria(), $con)->diff($uniqueFieldGroupFields);


        $this->uniqueFieldGroupFieldsScheduledForDeletion = $uniqueFieldGroupFieldsToDelete;

        foreach ($uniqueFieldGroupFieldsToDelete as $uniqueFieldGroupFieldRemoved) {
            $uniqueFieldGroupFieldRemoved->setDataField(null);
        }

        $this->collUniqueFieldGroupFields = null;
        foreach ($uniqueFieldGroupFields as $uniqueFieldGroupField) {
            $this->addUniqueFieldGroupField($uniqueFieldGroupField);
        }

        $this->collUniqueFieldGroupFields = $uniqueFieldGroupFields;
        $this->collUniqueFieldGroupFieldsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseUniqueFieldGroupField objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseUniqueFieldGroupField objects.
     * @throws PropelException
     */
    public function countUniqueFieldGroupFields(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUniqueFieldGroupFieldsPartial && !$this->isNew();
        if (null === $this->collUniqueFieldGroupFields || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUniqueFieldGroupFields) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUniqueFieldGroupFields());
            }

            $query = UniqueFieldGroupFieldQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByDataField($this)
                ->count($con);
        }

        return count($this->collUniqueFieldGroupFields);
    }

    /**
     * Method called to associate a UniqueFieldGroupField object to this object
     * through the UniqueFieldGroupField foreign key attribute.
     *
     * @param  UniqueFieldGroupField $l UniqueFieldGroupField
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function addUniqueFieldGroupField(UniqueFieldGroupField $l)
    {
        if ($this->collUniqueFieldGroupFields === null) {
            $this->initUniqueFieldGroupFields();
            $this->collUniqueFieldGroupFieldsPartial = true;
        }

        if (!$this->collUniqueFieldGroupFields->contains($l)) {
            $this->doAddUniqueFieldGroupField($l);

            if ($this->uniqueFieldGroupFieldsScheduledForDeletion and $this->uniqueFieldGroupFieldsScheduledForDeletion->contains($l)) {
                $this->uniqueFieldGroupFieldsScheduledForDeletion->remove($this->uniqueFieldGroupFieldsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param UniqueFieldGroupField $uniqueFieldGroupField The UniqueFieldGroupField object to add.
     */
    protected function doAddUniqueFieldGroupField(UniqueFieldGroupField $uniqueFieldGroupField)
    {
        $this->collUniqueFieldGroupFields[]= $uniqueFieldGroupField;
        $uniqueFieldGroupField->setDataField($this);
    }

    /**
     * @param  UniqueFieldGroupField $uniqueFieldGroupField The UniqueFieldGroupField object to remove.
     * @return $this|ChildDataField The current object (for fluent API support)
     */
    public function removeUniqueFieldGroupField(UniqueFieldGroupField $uniqueFieldGroupField)
    {
        if ($this->getUniqueFieldGroupFields()->contains($uniqueFieldGroupField)) {
            $pos = $this->collUniqueFieldGroupFields->search($uniqueFieldGroupField);
            $this->collUniqueFieldGroupFields->remove($pos);
            if (null === $this->uniqueFieldGroupFieldsScheduledForDeletion) {
                $this->uniqueFieldGroupFieldsScheduledForDeletion = clone $this->collUniqueFieldGroupFields;
                $this->uniqueFieldGroupFieldsScheduledForDeletion->clear();
            }
            $this->uniqueFieldGroupFieldsScheduledForDeletion[]= clone $uniqueFieldGroupField;
            $uniqueFieldGroupField->setDataField(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this DataField is new, it will return
     * an empty collection; or if this DataField has previously
     * been saved, it will retrieve related UniqueFieldGroupFields from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in DataField.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|UniqueFieldGroupField[] List of UniqueFieldGroupField objects
     */
    public function getUniqueFieldGroupFieldsJoinModelUnique(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = UniqueFieldGroupFieldQuery::create(null, $criteria);
        $query->joinWith('ModelUnique', $joinBehavior);

        return $this->getUniqueFieldGroupFields($query, $con);
    }

    /**
     * Clears out the collModelConstraintsRelatedByLocalFieldId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addModelConstraintsRelatedByLocalFieldId()
     */
    public function clearModelConstraintsRelatedByLocalFieldId()
    {
        $this->collModelConstraintsRelatedByLocalFieldId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collModelConstraintsRelatedByLocalFieldId collection loaded partially.
     */
    public function resetPartialModelConstraintsRelatedByLocalFieldId($v = true)
    {
        $this->collModelConstraintsRelatedByLocalFieldIdPartial = $v;
    }

    /**
     * Initializes the collModelConstraintsRelatedByLocalFieldId collection.
     *
     * By default this just sets the collModelConstraintsRelatedByLocalFieldId collection to an empty array (like clearcollModelConstraintsRelatedByLocalFieldId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initModelConstraintsRelatedByLocalFieldId($overrideExisting = true)
    {
        if (null !== $this->collModelConstraintsRelatedByLocalFieldId && !$overrideExisting) {
            return;
        }

        $collectionClassName = ModelConstraintTableMap::getTableMap()->getCollectionClassName();

        $this->collModelConstraintsRelatedByLocalFieldId = new $collectionClassName;
        $this->collModelConstraintsRelatedByLocalFieldId->setModel('\Model\System\DataModel\Model\ModelConstraint');
    }

    /**
     * Gets an array of ModelConstraint objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDataField is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ModelConstraint[] List of ModelConstraint objects
     * @throws PropelException
     */
    public function getModelConstraintsRelatedByLocalFieldId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collModelConstraintsRelatedByLocalFieldIdPartial && !$this->isNew();
        if (null === $this->collModelConstraintsRelatedByLocalFieldId || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collModelConstraintsRelatedByLocalFieldId) {
                    $this->initModelConstraintsRelatedByLocalFieldId();
                } else {
                    $collectionClassName = ModelConstraintTableMap::getTableMap()->getCollectionClassName();

                    $collModelConstraintsRelatedByLocalFieldId = new $collectionClassName;
                    $collModelConstraintsRelatedByLocalFieldId->setModel('\Model\System\DataModel\Model\ModelConstraint');

                    return $collModelConstraintsRelatedByLocalFieldId;
                }
            } else {
                $collModelConstraintsRelatedByLocalFieldId = ModelConstraintQuery::create(null, $criteria)
                    ->filterByFkLocalField($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collModelConstraintsRelatedByLocalFieldIdPartial && count($collModelConstraintsRelatedByLocalFieldId)) {
                        $this->initModelConstraintsRelatedByLocalFieldId(false);

                        foreach ($collModelConstraintsRelatedByLocalFieldId as $obj) {
                            if (false == $this->collModelConstraintsRelatedByLocalFieldId->contains($obj)) {
                                $this->collModelConstraintsRelatedByLocalFieldId->append($obj);
                            }
                        }

                        $this->collModelConstraintsRelatedByLocalFieldIdPartial = true;
                    }

                    return $collModelConstraintsRelatedByLocalFieldId;
                }

                if ($partial && $this->collModelConstraintsRelatedByLocalFieldId) {
                    foreach ($this->collModelConstraintsRelatedByLocalFieldId as $obj) {
                        if ($obj->isNew()) {
                            $collModelConstraintsRelatedByLocalFieldId[] = $obj;
                        }
                    }
                }

                $this->collModelConstraintsRelatedByLocalFieldId = $collModelConstraintsRelatedByLocalFieldId;
                $this->collModelConstraintsRelatedByLocalFieldIdPartial = false;
            }
        }

        return $this->collModelConstraintsRelatedByLocalFieldId;
    }

    /**
     * Sets a collection of ModelConstraint objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $modelConstraintsRelatedByLocalFieldId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDataField The current object (for fluent API support)
     */
    public function setModelConstraintsRelatedByLocalFieldId(Collection $modelConstraintsRelatedByLocalFieldId, ConnectionInterface $con = null)
    {
        /** @var ModelConstraint[] $modelConstraintsRelatedByLocalFieldIdToDelete */
        $modelConstraintsRelatedByLocalFieldIdToDelete = $this->getModelConstraintsRelatedByLocalFieldId(new Criteria(), $con)->diff($modelConstraintsRelatedByLocalFieldId);


        $this->modelConstraintsRelatedByLocalFieldIdScheduledForDeletion = $modelConstraintsRelatedByLocalFieldIdToDelete;

        foreach ($modelConstraintsRelatedByLocalFieldIdToDelete as $modelConstraintRelatedByLocalFieldIdRemoved) {
            $modelConstraintRelatedByLocalFieldIdRemoved->setFkLocalField(null);
        }

        $this->collModelConstraintsRelatedByLocalFieldId = null;
        foreach ($modelConstraintsRelatedByLocalFieldId as $modelConstraintRelatedByLocalFieldId) {
            $this->addModelConstraintRelatedByLocalFieldId($modelConstraintRelatedByLocalFieldId);
        }

        $this->collModelConstraintsRelatedByLocalFieldId = $modelConstraintsRelatedByLocalFieldId;
        $this->collModelConstraintsRelatedByLocalFieldIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseModelConstraint objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseModelConstraint objects.
     * @throws PropelException
     */
    public function countModelConstraintsRelatedByLocalFieldId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collModelConstraintsRelatedByLocalFieldIdPartial && !$this->isNew();
        if (null === $this->collModelConstraintsRelatedByLocalFieldId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collModelConstraintsRelatedByLocalFieldId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getModelConstraintsRelatedByLocalFieldId());
            }

            $query = ModelConstraintQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFkLocalField($this)
                ->count($con);
        }

        return count($this->collModelConstraintsRelatedByLocalFieldId);
    }

    /**
     * Method called to associate a ModelConstraint object to this object
     * through the ModelConstraint foreign key attribute.
     *
     * @param  ModelConstraint $l ModelConstraint
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function addModelConstraintRelatedByLocalFieldId(ModelConstraint $l)
    {
        if ($this->collModelConstraintsRelatedByLocalFieldId === null) {
            $this->initModelConstraintsRelatedByLocalFieldId();
            $this->collModelConstraintsRelatedByLocalFieldIdPartial = true;
        }

        if (!$this->collModelConstraintsRelatedByLocalFieldId->contains($l)) {
            $this->doAddModelConstraintRelatedByLocalFieldId($l);

            if ($this->modelConstraintsRelatedByLocalFieldIdScheduledForDeletion and $this->modelConstraintsRelatedByLocalFieldIdScheduledForDeletion->contains($l)) {
                $this->modelConstraintsRelatedByLocalFieldIdScheduledForDeletion->remove($this->modelConstraintsRelatedByLocalFieldIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ModelConstraint $modelConstraintRelatedByLocalFieldId The ModelConstraint object to add.
     */
    protected function doAddModelConstraintRelatedByLocalFieldId(ModelConstraint $modelConstraintRelatedByLocalFieldId)
    {
        $this->collModelConstraintsRelatedByLocalFieldId[]= $modelConstraintRelatedByLocalFieldId;
        $modelConstraintRelatedByLocalFieldId->setFkLocalField($this);
    }

    /**
     * @param  ModelConstraint $modelConstraintRelatedByLocalFieldId The ModelConstraint object to remove.
     * @return $this|ChildDataField The current object (for fluent API support)
     */
    public function removeModelConstraintRelatedByLocalFieldId(ModelConstraint $modelConstraintRelatedByLocalFieldId)
    {
        if ($this->getModelConstraintsRelatedByLocalFieldId()->contains($modelConstraintRelatedByLocalFieldId)) {
            $pos = $this->collModelConstraintsRelatedByLocalFieldId->search($modelConstraintRelatedByLocalFieldId);
            $this->collModelConstraintsRelatedByLocalFieldId->remove($pos);
            if (null === $this->modelConstraintsRelatedByLocalFieldIdScheduledForDeletion) {
                $this->modelConstraintsRelatedByLocalFieldIdScheduledForDeletion = clone $this->collModelConstraintsRelatedByLocalFieldId;
                $this->modelConstraintsRelatedByLocalFieldIdScheduledForDeletion->clear();
            }
            $this->modelConstraintsRelatedByLocalFieldIdScheduledForDeletion[]= clone $modelConstraintRelatedByLocalFieldId;
            $modelConstraintRelatedByLocalFieldId->setFkLocalField(null);
        }

        return $this;
    }

    /**
     * Clears out the collModelConstraintsRelatedByForeignFieldId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addModelConstraintsRelatedByForeignFieldId()
     */
    public function clearModelConstraintsRelatedByForeignFieldId()
    {
        $this->collModelConstraintsRelatedByForeignFieldId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collModelConstraintsRelatedByForeignFieldId collection loaded partially.
     */
    public function resetPartialModelConstraintsRelatedByForeignFieldId($v = true)
    {
        $this->collModelConstraintsRelatedByForeignFieldIdPartial = $v;
    }

    /**
     * Initializes the collModelConstraintsRelatedByForeignFieldId collection.
     *
     * By default this just sets the collModelConstraintsRelatedByForeignFieldId collection to an empty array (like clearcollModelConstraintsRelatedByForeignFieldId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initModelConstraintsRelatedByForeignFieldId($overrideExisting = true)
    {
        if (null !== $this->collModelConstraintsRelatedByForeignFieldId && !$overrideExisting) {
            return;
        }

        $collectionClassName = ModelConstraintTableMap::getTableMap()->getCollectionClassName();

        $this->collModelConstraintsRelatedByForeignFieldId = new $collectionClassName;
        $this->collModelConstraintsRelatedByForeignFieldId->setModel('\Model\System\DataModel\Model\ModelConstraint');
    }

    /**
     * Gets an array of ModelConstraint objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildDataField is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ModelConstraint[] List of ModelConstraint objects
     * @throws PropelException
     */
    public function getModelConstraintsRelatedByForeignFieldId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collModelConstraintsRelatedByForeignFieldIdPartial && !$this->isNew();
        if (null === $this->collModelConstraintsRelatedByForeignFieldId || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collModelConstraintsRelatedByForeignFieldId) {
                    $this->initModelConstraintsRelatedByForeignFieldId();
                } else {
                    $collectionClassName = ModelConstraintTableMap::getTableMap()->getCollectionClassName();

                    $collModelConstraintsRelatedByForeignFieldId = new $collectionClassName;
                    $collModelConstraintsRelatedByForeignFieldId->setModel('\Model\System\DataModel\Model\ModelConstraint');

                    return $collModelConstraintsRelatedByForeignFieldId;
                }
            } else {
                $collModelConstraintsRelatedByForeignFieldId = ModelConstraintQuery::create(null, $criteria)
                    ->filterByFkForeignField($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collModelConstraintsRelatedByForeignFieldIdPartial && count($collModelConstraintsRelatedByForeignFieldId)) {
                        $this->initModelConstraintsRelatedByForeignFieldId(false);

                        foreach ($collModelConstraintsRelatedByForeignFieldId as $obj) {
                            if (false == $this->collModelConstraintsRelatedByForeignFieldId->contains($obj)) {
                                $this->collModelConstraintsRelatedByForeignFieldId->append($obj);
                            }
                        }

                        $this->collModelConstraintsRelatedByForeignFieldIdPartial = true;
                    }

                    return $collModelConstraintsRelatedByForeignFieldId;
                }

                if ($partial && $this->collModelConstraintsRelatedByForeignFieldId) {
                    foreach ($this->collModelConstraintsRelatedByForeignFieldId as $obj) {
                        if ($obj->isNew()) {
                            $collModelConstraintsRelatedByForeignFieldId[] = $obj;
                        }
                    }
                }

                $this->collModelConstraintsRelatedByForeignFieldId = $collModelConstraintsRelatedByForeignFieldId;
                $this->collModelConstraintsRelatedByForeignFieldIdPartial = false;
            }
        }

        return $this->collModelConstraintsRelatedByForeignFieldId;
    }

    /**
     * Sets a collection of ModelConstraint objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $modelConstraintsRelatedByForeignFieldId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildDataField The current object (for fluent API support)
     */
    public function setModelConstraintsRelatedByForeignFieldId(Collection $modelConstraintsRelatedByForeignFieldId, ConnectionInterface $con = null)
    {
        /** @var ModelConstraint[] $modelConstraintsRelatedByForeignFieldIdToDelete */
        $modelConstraintsRelatedByForeignFieldIdToDelete = $this->getModelConstraintsRelatedByForeignFieldId(new Criteria(), $con)->diff($modelConstraintsRelatedByForeignFieldId);


        $this->modelConstraintsRelatedByForeignFieldIdScheduledForDeletion = $modelConstraintsRelatedByForeignFieldIdToDelete;

        foreach ($modelConstraintsRelatedByForeignFieldIdToDelete as $modelConstraintRelatedByForeignFieldIdRemoved) {
            $modelConstraintRelatedByForeignFieldIdRemoved->setFkForeignField(null);
        }

        $this->collModelConstraintsRelatedByForeignFieldId = null;
        foreach ($modelConstraintsRelatedByForeignFieldId as $modelConstraintRelatedByForeignFieldId) {
            $this->addModelConstraintRelatedByForeignFieldId($modelConstraintRelatedByForeignFieldId);
        }

        $this->collModelConstraintsRelatedByForeignFieldId = $modelConstraintsRelatedByForeignFieldId;
        $this->collModelConstraintsRelatedByForeignFieldIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related BaseModelConstraint objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related BaseModelConstraint objects.
     * @throws PropelException
     */
    public function countModelConstraintsRelatedByForeignFieldId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collModelConstraintsRelatedByForeignFieldIdPartial && !$this->isNew();
        if (null === $this->collModelConstraintsRelatedByForeignFieldId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collModelConstraintsRelatedByForeignFieldId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getModelConstraintsRelatedByForeignFieldId());
            }

            $query = ModelConstraintQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByFkForeignField($this)
                ->count($con);
        }

        return count($this->collModelConstraintsRelatedByForeignFieldId);
    }

    /**
     * Method called to associate a ModelConstraint object to this object
     * through the ModelConstraint foreign key attribute.
     *
     * @param  ModelConstraint $l ModelConstraint
     * @return $this|\Model\System\DataModel\Field\DataField The current object (for fluent API support)
     */
    public function addModelConstraintRelatedByForeignFieldId(ModelConstraint $l)
    {
        if ($this->collModelConstraintsRelatedByForeignFieldId === null) {
            $this->initModelConstraintsRelatedByForeignFieldId();
            $this->collModelConstraintsRelatedByForeignFieldIdPartial = true;
        }

        if (!$this->collModelConstraintsRelatedByForeignFieldId->contains($l)) {
            $this->doAddModelConstraintRelatedByForeignFieldId($l);

            if ($this->modelConstraintsRelatedByForeignFieldIdScheduledForDeletion and $this->modelConstraintsRelatedByForeignFieldIdScheduledForDeletion->contains($l)) {
                $this->modelConstraintsRelatedByForeignFieldIdScheduledForDeletion->remove($this->modelConstraintsRelatedByForeignFieldIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ModelConstraint $modelConstraintRelatedByForeignFieldId The ModelConstraint object to add.
     */
    protected function doAddModelConstraintRelatedByForeignFieldId(ModelConstraint $modelConstraintRelatedByForeignFieldId)
    {
        $this->collModelConstraintsRelatedByForeignFieldId[]= $modelConstraintRelatedByForeignFieldId;
        $modelConstraintRelatedByForeignFieldId->setFkForeignField($this);
    }

    /**
     * @param  ModelConstraint $modelConstraintRelatedByForeignFieldId The ModelConstraint object to remove.
     * @return $this|ChildDataField The current object (for fluent API support)
     */
    public function removeModelConstraintRelatedByForeignFieldId(ModelConstraint $modelConstraintRelatedByForeignFieldId)
    {
        if ($this->getModelConstraintsRelatedByForeignFieldId()->contains($modelConstraintRelatedByForeignFieldId)) {
            $pos = $this->collModelConstraintsRelatedByForeignFieldId->search($modelConstraintRelatedByForeignFieldId);
            $this->collModelConstraintsRelatedByForeignFieldId->remove($pos);
            if (null === $this->modelConstraintsRelatedByForeignFieldIdScheduledForDeletion) {
                $this->modelConstraintsRelatedByForeignFieldIdScheduledForDeletion = clone $this->collModelConstraintsRelatedByForeignFieldId;
                $this->modelConstraintsRelatedByForeignFieldIdScheduledForDeletion->clear();
            }
            $this->modelConstraintsRelatedByForeignFieldIdScheduledForDeletion[]= clone $modelConstraintRelatedByForeignFieldId;
            $modelConstraintRelatedByForeignFieldId->setFkForeignField(null);
        }

        return $this;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aFkDataModel) {
            $this->aFkDataModel->removeDataField($this);
        }
        if (null !== $this->aFkDataType) {
            $this->aFkDataType->removeDataField($this);
        }
        $this->id = null;
        $this->data_model_id = null;
        $this->name = null;
        $this->php_name = null;
        $this->label = null;
        $this->size = null;
        $this->icon_id = null;
        $this->form_field_type_id = null;
        $this->form_field_lookups = null;
        $this->data_type_id = null;
        $this->required = null;
        $this->is_primary_key = null;
        $this->auto_increment = null;
        $this->default_value = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collUniqueFieldGroupFields) {
                foreach ($this->collUniqueFieldGroupFields as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collModelConstraintsRelatedByLocalFieldId) {
                foreach ($this->collModelConstraintsRelatedByLocalFieldId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collModelConstraintsRelatedByForeignFieldId) {
                foreach ($this->collModelConstraintsRelatedByForeignFieldId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collUniqueFieldGroupFields = null;
        $this->collModelConstraintsRelatedByLocalFieldId = null;
        $this->collModelConstraintsRelatedByForeignFieldId = null;
        $this->aFkDataModel = null;
        $this->aFkDataType = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(DataFieldTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
