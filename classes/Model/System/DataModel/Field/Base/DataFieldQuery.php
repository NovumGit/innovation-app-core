<?php

namespace Model\System\DataModel\Field\Base;

use \Exception;
use \PDO;
use Model\System\DataModel\DataType;
use Model\System\DataModel\Field\DataField as ChildDataField;
use Model\System\DataModel\Field\DataFieldQuery as ChildDataFieldQuery;
use Model\System\DataModel\Field\Map\DataFieldTableMap;
use Model\System\DataModel\Model\DataModel;
use Model\System\DataModel\Model\ModelConstraint;
use Model\System\DataModel\Model\UniqueFieldGroupField;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'data_field' table.
 *
 *
 *
 * @method     ChildDataFieldQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildDataFieldQuery orderByDataModelId($order = Criteria::ASC) Order by the data_model_id column
 * @method     ChildDataFieldQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildDataFieldQuery orderByPhpName($order = Criteria::ASC) Order by the php_name column
 * @method     ChildDataFieldQuery orderByLabel($order = Criteria::ASC) Order by the label column
 * @method     ChildDataFieldQuery orderBySize($order = Criteria::ASC) Order by the size column
 * @method     ChildDataFieldQuery orderByIconId($order = Criteria::ASC) Order by the icon_id column
 * @method     ChildDataFieldQuery orderByFormFieldTypeId($order = Criteria::ASC) Order by the form_field_type_id column
 * @method     ChildDataFieldQuery orderByFormFieldLookups($order = Criteria::ASC) Order by the form_field_lookups column
 * @method     ChildDataFieldQuery orderByDataTypeId($order = Criteria::ASC) Order by the data_type_id column
 * @method     ChildDataFieldQuery orderByRequired($order = Criteria::ASC) Order by the required column
 * @method     ChildDataFieldQuery orderByIsPrimaryKey($order = Criteria::ASC) Order by the is_primary_key column
 * @method     ChildDataFieldQuery orderByAutoIncrement($order = Criteria::ASC) Order by the auto_increment column
 * @method     ChildDataFieldQuery orderByDefaultValue($order = Criteria::ASC) Order by the default_value column
 *
 * @method     ChildDataFieldQuery groupById() Group by the id column
 * @method     ChildDataFieldQuery groupByDataModelId() Group by the data_model_id column
 * @method     ChildDataFieldQuery groupByName() Group by the name column
 * @method     ChildDataFieldQuery groupByPhpName() Group by the php_name column
 * @method     ChildDataFieldQuery groupByLabel() Group by the label column
 * @method     ChildDataFieldQuery groupBySize() Group by the size column
 * @method     ChildDataFieldQuery groupByIconId() Group by the icon_id column
 * @method     ChildDataFieldQuery groupByFormFieldTypeId() Group by the form_field_type_id column
 * @method     ChildDataFieldQuery groupByFormFieldLookups() Group by the form_field_lookups column
 * @method     ChildDataFieldQuery groupByDataTypeId() Group by the data_type_id column
 * @method     ChildDataFieldQuery groupByRequired() Group by the required column
 * @method     ChildDataFieldQuery groupByIsPrimaryKey() Group by the is_primary_key column
 * @method     ChildDataFieldQuery groupByAutoIncrement() Group by the auto_increment column
 * @method     ChildDataFieldQuery groupByDefaultValue() Group by the default_value column
 *
 * @method     ChildDataFieldQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildDataFieldQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildDataFieldQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildDataFieldQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildDataFieldQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildDataFieldQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildDataFieldQuery leftJoinFkDataModel($relationAlias = null) Adds a LEFT JOIN clause to the query using the FkDataModel relation
 * @method     ChildDataFieldQuery rightJoinFkDataModel($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FkDataModel relation
 * @method     ChildDataFieldQuery innerJoinFkDataModel($relationAlias = null) Adds a INNER JOIN clause to the query using the FkDataModel relation
 *
 * @method     ChildDataFieldQuery joinWithFkDataModel($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FkDataModel relation
 *
 * @method     ChildDataFieldQuery leftJoinWithFkDataModel() Adds a LEFT JOIN clause and with to the query using the FkDataModel relation
 * @method     ChildDataFieldQuery rightJoinWithFkDataModel() Adds a RIGHT JOIN clause and with to the query using the FkDataModel relation
 * @method     ChildDataFieldQuery innerJoinWithFkDataModel() Adds a INNER JOIN clause and with to the query using the FkDataModel relation
 *
 * @method     ChildDataFieldQuery leftJoinFkDataType($relationAlias = null) Adds a LEFT JOIN clause to the query using the FkDataType relation
 * @method     ChildDataFieldQuery rightJoinFkDataType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the FkDataType relation
 * @method     ChildDataFieldQuery innerJoinFkDataType($relationAlias = null) Adds a INNER JOIN clause to the query using the FkDataType relation
 *
 * @method     ChildDataFieldQuery joinWithFkDataType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the FkDataType relation
 *
 * @method     ChildDataFieldQuery leftJoinWithFkDataType() Adds a LEFT JOIN clause and with to the query using the FkDataType relation
 * @method     ChildDataFieldQuery rightJoinWithFkDataType() Adds a RIGHT JOIN clause and with to the query using the FkDataType relation
 * @method     ChildDataFieldQuery innerJoinWithFkDataType() Adds a INNER JOIN clause and with to the query using the FkDataType relation
 *
 * @method     ChildDataFieldQuery leftJoinUniqueFieldGroupField($relationAlias = null) Adds a LEFT JOIN clause to the query using the UniqueFieldGroupField relation
 * @method     ChildDataFieldQuery rightJoinUniqueFieldGroupField($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UniqueFieldGroupField relation
 * @method     ChildDataFieldQuery innerJoinUniqueFieldGroupField($relationAlias = null) Adds a INNER JOIN clause to the query using the UniqueFieldGroupField relation
 *
 * @method     ChildDataFieldQuery joinWithUniqueFieldGroupField($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UniqueFieldGroupField relation
 *
 * @method     ChildDataFieldQuery leftJoinWithUniqueFieldGroupField() Adds a LEFT JOIN clause and with to the query using the UniqueFieldGroupField relation
 * @method     ChildDataFieldQuery rightJoinWithUniqueFieldGroupField() Adds a RIGHT JOIN clause and with to the query using the UniqueFieldGroupField relation
 * @method     ChildDataFieldQuery innerJoinWithUniqueFieldGroupField() Adds a INNER JOIN clause and with to the query using the UniqueFieldGroupField relation
 *
 * @method     ChildDataFieldQuery leftJoinModelConstraintRelatedByLocalFieldId($relationAlias = null) Adds a LEFT JOIN clause to the query using the ModelConstraintRelatedByLocalFieldId relation
 * @method     ChildDataFieldQuery rightJoinModelConstraintRelatedByLocalFieldId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ModelConstraintRelatedByLocalFieldId relation
 * @method     ChildDataFieldQuery innerJoinModelConstraintRelatedByLocalFieldId($relationAlias = null) Adds a INNER JOIN clause to the query using the ModelConstraintRelatedByLocalFieldId relation
 *
 * @method     ChildDataFieldQuery joinWithModelConstraintRelatedByLocalFieldId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ModelConstraintRelatedByLocalFieldId relation
 *
 * @method     ChildDataFieldQuery leftJoinWithModelConstraintRelatedByLocalFieldId() Adds a LEFT JOIN clause and with to the query using the ModelConstraintRelatedByLocalFieldId relation
 * @method     ChildDataFieldQuery rightJoinWithModelConstraintRelatedByLocalFieldId() Adds a RIGHT JOIN clause and with to the query using the ModelConstraintRelatedByLocalFieldId relation
 * @method     ChildDataFieldQuery innerJoinWithModelConstraintRelatedByLocalFieldId() Adds a INNER JOIN clause and with to the query using the ModelConstraintRelatedByLocalFieldId relation
 *
 * @method     ChildDataFieldQuery leftJoinModelConstraintRelatedByForeignFieldId($relationAlias = null) Adds a LEFT JOIN clause to the query using the ModelConstraintRelatedByForeignFieldId relation
 * @method     ChildDataFieldQuery rightJoinModelConstraintRelatedByForeignFieldId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ModelConstraintRelatedByForeignFieldId relation
 * @method     ChildDataFieldQuery innerJoinModelConstraintRelatedByForeignFieldId($relationAlias = null) Adds a INNER JOIN clause to the query using the ModelConstraintRelatedByForeignFieldId relation
 *
 * @method     ChildDataFieldQuery joinWithModelConstraintRelatedByForeignFieldId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ModelConstraintRelatedByForeignFieldId relation
 *
 * @method     ChildDataFieldQuery leftJoinWithModelConstraintRelatedByForeignFieldId() Adds a LEFT JOIN clause and with to the query using the ModelConstraintRelatedByForeignFieldId relation
 * @method     ChildDataFieldQuery rightJoinWithModelConstraintRelatedByForeignFieldId() Adds a RIGHT JOIN clause and with to the query using the ModelConstraintRelatedByForeignFieldId relation
 * @method     ChildDataFieldQuery innerJoinWithModelConstraintRelatedByForeignFieldId() Adds a INNER JOIN clause and with to the query using the ModelConstraintRelatedByForeignFieldId relation
 *
 * @method     \Model\System\DataModel\Model\DataModelQuery|\Model\System\DataModel\DataTypeQuery|\Model\System\DataModel\Model\UniqueFieldGroupFieldQuery|\Model\System\DataModel\Model\ModelConstraintQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildDataField findOne(ConnectionInterface $con = null) Return the first ChildDataField matching the query
 * @method     ChildDataField findOneOrCreate(ConnectionInterface $con = null) Return the first ChildDataField matching the query, or a new ChildDataField object populated from the query conditions when no match is found
 *
 * @method     ChildDataField findOneById(int $id) Return the first ChildDataField filtered by the id column
 * @method     ChildDataField findOneByDataModelId(int $data_model_id) Return the first ChildDataField filtered by the data_model_id column
 * @method     ChildDataField findOneByName(string $name) Return the first ChildDataField filtered by the name column
 * @method     ChildDataField findOneByPhpName(string $php_name) Return the first ChildDataField filtered by the php_name column
 * @method     ChildDataField findOneByLabel(string $label) Return the first ChildDataField filtered by the label column
 * @method     ChildDataField findOneBySize(string $size) Return the first ChildDataField filtered by the size column
 * @method     ChildDataField findOneByIconId(int $icon_id) Return the first ChildDataField filtered by the icon_id column
 * @method     ChildDataField findOneByFormFieldTypeId(int $form_field_type_id) Return the first ChildDataField filtered by the form_field_type_id column
 * @method     ChildDataField findOneByFormFieldLookups(string $form_field_lookups) Return the first ChildDataField filtered by the form_field_lookups column
 * @method     ChildDataField findOneByDataTypeId(int $data_type_id) Return the first ChildDataField filtered by the data_type_id column
 * @method     ChildDataField findOneByRequired(boolean $required) Return the first ChildDataField filtered by the required column
 * @method     ChildDataField findOneByIsPrimaryKey(boolean $is_primary_key) Return the first ChildDataField filtered by the is_primary_key column
 * @method     ChildDataField findOneByAutoIncrement(boolean $auto_increment) Return the first ChildDataField filtered by the auto_increment column
 * @method     ChildDataField findOneByDefaultValue(string $default_value) Return the first ChildDataField filtered by the default_value column *

 * @method     ChildDataField requirePk($key, ConnectionInterface $con = null) Return the ChildDataField by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOne(ConnectionInterface $con = null) Return the first ChildDataField matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDataField requireOneById(int $id) Return the first ChildDataField filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOneByDataModelId(int $data_model_id) Return the first ChildDataField filtered by the data_model_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOneByName(string $name) Return the first ChildDataField filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOneByPhpName(string $php_name) Return the first ChildDataField filtered by the php_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOneByLabel(string $label) Return the first ChildDataField filtered by the label column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOneBySize(string $size) Return the first ChildDataField filtered by the size column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOneByIconId(int $icon_id) Return the first ChildDataField filtered by the icon_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOneByFormFieldTypeId(int $form_field_type_id) Return the first ChildDataField filtered by the form_field_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOneByFormFieldLookups(string $form_field_lookups) Return the first ChildDataField filtered by the form_field_lookups column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOneByDataTypeId(int $data_type_id) Return the first ChildDataField filtered by the data_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOneByRequired(boolean $required) Return the first ChildDataField filtered by the required column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOneByIsPrimaryKey(boolean $is_primary_key) Return the first ChildDataField filtered by the is_primary_key column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOneByAutoIncrement(boolean $auto_increment) Return the first ChildDataField filtered by the auto_increment column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildDataField requireOneByDefaultValue(string $default_value) Return the first ChildDataField filtered by the default_value column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildDataField[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildDataField objects based on current ModelCriteria
 * @method     ChildDataField[]|ObjectCollection findById(int $id) Return ChildDataField objects filtered by the id column
 * @method     ChildDataField[]|ObjectCollection findByDataModelId(int $data_model_id) Return ChildDataField objects filtered by the data_model_id column
 * @method     ChildDataField[]|ObjectCollection findByName(string $name) Return ChildDataField objects filtered by the name column
 * @method     ChildDataField[]|ObjectCollection findByPhpName(string $php_name) Return ChildDataField objects filtered by the php_name column
 * @method     ChildDataField[]|ObjectCollection findByLabel(string $label) Return ChildDataField objects filtered by the label column
 * @method     ChildDataField[]|ObjectCollection findBySize(string $size) Return ChildDataField objects filtered by the size column
 * @method     ChildDataField[]|ObjectCollection findByIconId(int $icon_id) Return ChildDataField objects filtered by the icon_id column
 * @method     ChildDataField[]|ObjectCollection findByFormFieldTypeId(int $form_field_type_id) Return ChildDataField objects filtered by the form_field_type_id column
 * @method     ChildDataField[]|ObjectCollection findByFormFieldLookups(string $form_field_lookups) Return ChildDataField objects filtered by the form_field_lookups column
 * @method     ChildDataField[]|ObjectCollection findByDataTypeId(int $data_type_id) Return ChildDataField objects filtered by the data_type_id column
 * @method     ChildDataField[]|ObjectCollection findByRequired(boolean $required) Return ChildDataField objects filtered by the required column
 * @method     ChildDataField[]|ObjectCollection findByIsPrimaryKey(boolean $is_primary_key) Return ChildDataField objects filtered by the is_primary_key column
 * @method     ChildDataField[]|ObjectCollection findByAutoIncrement(boolean $auto_increment) Return ChildDataField objects filtered by the auto_increment column
 * @method     ChildDataField[]|ObjectCollection findByDefaultValue(string $default_value) Return ChildDataField objects filtered by the default_value column
 * @method     ChildDataField[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class DataFieldQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\DataModel\Field\Base\DataFieldQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\DataModel\\Field\\DataField', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildDataFieldQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildDataFieldQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildDataFieldQuery) {
            return $criteria;
        }
        $query = new ChildDataFieldQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildDataField|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(DataFieldTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = DataFieldTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDataField A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, data_model_id, name, php_name, label, size, icon_id, form_field_type_id, form_field_lookups, data_type_id, required, is_primary_key, auto_increment, default_value FROM data_field WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildDataField $obj */
            $obj = new ChildDataField();
            $obj->hydrate($row);
            DataFieldTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildDataField|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(DataFieldTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(DataFieldTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(DataFieldTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(DataFieldTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the data_model_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDataModelId(1234); // WHERE data_model_id = 1234
     * $query->filterByDataModelId(array(12, 34)); // WHERE data_model_id IN (12, 34)
     * $query->filterByDataModelId(array('min' => 12)); // WHERE data_model_id > 12
     * </code>
     *
     * @see       filterByFkDataModel()
     *
     * @param     mixed $dataModelId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByDataModelId($dataModelId = null, $comparison = null)
    {
        if (is_array($dataModelId)) {
            $useMinMax = false;
            if (isset($dataModelId['min'])) {
                $this->addUsingAlias(DataFieldTableMap::COL_DATA_MODEL_ID, $dataModelId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataModelId['max'])) {
                $this->addUsingAlias(DataFieldTableMap::COL_DATA_MODEL_ID, $dataModelId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_DATA_MODEL_ID, $dataModelId, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the php_name column
     *
     * Example usage:
     * <code>
     * $query->filterByPhpName('fooValue');   // WHERE php_name = 'fooValue'
     * $query->filterByPhpName('%fooValue%', Criteria::LIKE); // WHERE php_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $phpName The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByPhpName($phpName = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($phpName)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_PHP_NAME, $phpName, $comparison);
    }

    /**
     * Filter the query on the label column
     *
     * Example usage:
     * <code>
     * $query->filterByLabel('fooValue');   // WHERE label = 'fooValue'
     * $query->filterByLabel('%fooValue%', Criteria::LIKE); // WHERE label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $label The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByLabel($label = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($label)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_LABEL, $label, $comparison);
    }

    /**
     * Filter the query on the size column
     *
     * Example usage:
     * <code>
     * $query->filterBySize('fooValue');   // WHERE size = 'fooValue'
     * $query->filterBySize('%fooValue%', Criteria::LIKE); // WHERE size LIKE '%fooValue%'
     * </code>
     *
     * @param     string $size The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterBySize($size = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($size)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_SIZE, $size, $comparison);
    }

    /**
     * Filter the query on the icon_id column
     *
     * Example usage:
     * <code>
     * $query->filterByIconId(1234); // WHERE icon_id = 1234
     * $query->filterByIconId(array(12, 34)); // WHERE icon_id IN (12, 34)
     * $query->filterByIconId(array('min' => 12)); // WHERE icon_id > 12
     * </code>
     *
     * @param     mixed $iconId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByIconId($iconId = null, $comparison = null)
    {
        if (is_array($iconId)) {
            $useMinMax = false;
            if (isset($iconId['min'])) {
                $this->addUsingAlias(DataFieldTableMap::COL_ICON_ID, $iconId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($iconId['max'])) {
                $this->addUsingAlias(DataFieldTableMap::COL_ICON_ID, $iconId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_ICON_ID, $iconId, $comparison);
    }

    /**
     * Filter the query on the form_field_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByFormFieldTypeId(1234); // WHERE form_field_type_id = 1234
     * $query->filterByFormFieldTypeId(array(12, 34)); // WHERE form_field_type_id IN (12, 34)
     * $query->filterByFormFieldTypeId(array('min' => 12)); // WHERE form_field_type_id > 12
     * </code>
     *
     * @param     mixed $formFieldTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByFormFieldTypeId($formFieldTypeId = null, $comparison = null)
    {
        if (is_array($formFieldTypeId)) {
            $useMinMax = false;
            if (isset($formFieldTypeId['min'])) {
                $this->addUsingAlias(DataFieldTableMap::COL_FORM_FIELD_TYPE_ID, $formFieldTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($formFieldTypeId['max'])) {
                $this->addUsingAlias(DataFieldTableMap::COL_FORM_FIELD_TYPE_ID, $formFieldTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_FORM_FIELD_TYPE_ID, $formFieldTypeId, $comparison);
    }

    /**
     * Filter the query on the form_field_lookups column
     *
     * Example usage:
     * <code>
     * $query->filterByFormFieldLookups('fooValue');   // WHERE form_field_lookups = 'fooValue'
     * $query->filterByFormFieldLookups('%fooValue%', Criteria::LIKE); // WHERE form_field_lookups LIKE '%fooValue%'
     * </code>
     *
     * @param     string $formFieldLookups The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByFormFieldLookups($formFieldLookups = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($formFieldLookups)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_FORM_FIELD_LOOKUPS, $formFieldLookups, $comparison);
    }

    /**
     * Filter the query on the data_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByDataTypeId(1234); // WHERE data_type_id = 1234
     * $query->filterByDataTypeId(array(12, 34)); // WHERE data_type_id IN (12, 34)
     * $query->filterByDataTypeId(array('min' => 12)); // WHERE data_type_id > 12
     * </code>
     *
     * @see       filterByFkDataType()
     *
     * @param     mixed $dataTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByDataTypeId($dataTypeId = null, $comparison = null)
    {
        if (is_array($dataTypeId)) {
            $useMinMax = false;
            if (isset($dataTypeId['min'])) {
                $this->addUsingAlias(DataFieldTableMap::COL_DATA_TYPE_ID, $dataTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dataTypeId['max'])) {
                $this->addUsingAlias(DataFieldTableMap::COL_DATA_TYPE_ID, $dataTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_DATA_TYPE_ID, $dataTypeId, $comparison);
    }

    /**
     * Filter the query on the required column
     *
     * Example usage:
     * <code>
     * $query->filterByRequired(true); // WHERE required = true
     * $query->filterByRequired('yes'); // WHERE required = true
     * </code>
     *
     * @param     boolean|string $required The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByRequired($required = null, $comparison = null)
    {
        if (is_string($required)) {
            $required = in_array(strtolower($required), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_REQUIRED, $required, $comparison);
    }

    /**
     * Filter the query on the is_primary_key column
     *
     * Example usage:
     * <code>
     * $query->filterByIsPrimaryKey(true); // WHERE is_primary_key = true
     * $query->filterByIsPrimaryKey('yes'); // WHERE is_primary_key = true
     * </code>
     *
     * @param     boolean|string $isPrimaryKey The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByIsPrimaryKey($isPrimaryKey = null, $comparison = null)
    {
        if (is_string($isPrimaryKey)) {
            $isPrimaryKey = in_array(strtolower($isPrimaryKey), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_IS_PRIMARY_KEY, $isPrimaryKey, $comparison);
    }

    /**
     * Filter the query on the auto_increment column
     *
     * Example usage:
     * <code>
     * $query->filterByAutoIncrement(true); // WHERE auto_increment = true
     * $query->filterByAutoIncrement('yes'); // WHERE auto_increment = true
     * </code>
     *
     * @param     boolean|string $autoIncrement The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByAutoIncrement($autoIncrement = null, $comparison = null)
    {
        if (is_string($autoIncrement)) {
            $autoIncrement = in_array(strtolower($autoIncrement), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_AUTO_INCREMENT, $autoIncrement, $comparison);
    }

    /**
     * Filter the query on the default_value column
     *
     * Example usage:
     * <code>
     * $query->filterByDefaultValue('fooValue');   // WHERE default_value = 'fooValue'
     * $query->filterByDefaultValue('%fooValue%', Criteria::LIKE); // WHERE default_value LIKE '%fooValue%'
     * </code>
     *
     * @param     string $defaultValue The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByDefaultValue($defaultValue = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($defaultValue)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(DataFieldTableMap::COL_DEFAULT_VALUE, $defaultValue, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\DataModel\Model\DataModel object
     *
     * @param \Model\System\DataModel\Model\DataModel|ObjectCollection $dataModel The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByFkDataModel($dataModel, $comparison = null)
    {
        if ($dataModel instanceof \Model\System\DataModel\Model\DataModel) {
            return $this
                ->addUsingAlias(DataFieldTableMap::COL_DATA_MODEL_ID, $dataModel->getId(), $comparison);
        } elseif ($dataModel instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DataFieldTableMap::COL_DATA_MODEL_ID, $dataModel->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFkDataModel() only accepts arguments of type \Model\System\DataModel\Model\DataModel or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FkDataModel relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function joinFkDataModel($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FkDataModel');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FkDataModel');
        }

        return $this;
    }

    /**
     * Use the FkDataModel relation DataModel object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\Model\DataModelQuery A secondary query class using the current class as primary query
     */
    public function useFkDataModelQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFkDataModel($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FkDataModel', '\Model\System\DataModel\Model\DataModelQuery');
    }

    /**
     * Filter the query by a related \Model\System\DataModel\DataType object
     *
     * @param \Model\System\DataModel\DataType|ObjectCollection $dataType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByFkDataType($dataType, $comparison = null)
    {
        if ($dataType instanceof \Model\System\DataModel\DataType) {
            return $this
                ->addUsingAlias(DataFieldTableMap::COL_DATA_TYPE_ID, $dataType->getId(), $comparison);
        } elseif ($dataType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(DataFieldTableMap::COL_DATA_TYPE_ID, $dataType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByFkDataType() only accepts arguments of type \Model\System\DataModel\DataType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the FkDataType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function joinFkDataType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('FkDataType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'FkDataType');
        }

        return $this;
    }

    /**
     * Use the FkDataType relation DataType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\DataTypeQuery A secondary query class using the current class as primary query
     */
    public function useFkDataTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinFkDataType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'FkDataType', '\Model\System\DataModel\DataTypeQuery');
    }

    /**
     * Filter the query by a related \Model\System\DataModel\Model\UniqueFieldGroupField object
     *
     * @param \Model\System\DataModel\Model\UniqueFieldGroupField|ObjectCollection $uniqueFieldGroupField the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByUniqueFieldGroupField($uniqueFieldGroupField, $comparison = null)
    {
        if ($uniqueFieldGroupField instanceof \Model\System\DataModel\Model\UniqueFieldGroupField) {
            return $this
                ->addUsingAlias(DataFieldTableMap::COL_ID, $uniqueFieldGroupField->getDataFieldId(), $comparison);
        } elseif ($uniqueFieldGroupField instanceof ObjectCollection) {
            return $this
                ->useUniqueFieldGroupFieldQuery()
                ->filterByPrimaryKeys($uniqueFieldGroupField->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUniqueFieldGroupField() only accepts arguments of type \Model\System\DataModel\Model\UniqueFieldGroupField or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UniqueFieldGroupField relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function joinUniqueFieldGroupField($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UniqueFieldGroupField');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UniqueFieldGroupField');
        }

        return $this;
    }

    /**
     * Use the UniqueFieldGroupField relation UniqueFieldGroupField object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\Model\UniqueFieldGroupFieldQuery A secondary query class using the current class as primary query
     */
    public function useUniqueFieldGroupFieldQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUniqueFieldGroupField($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UniqueFieldGroupField', '\Model\System\DataModel\Model\UniqueFieldGroupFieldQuery');
    }

    /**
     * Filter the query by a related \Model\System\DataModel\Model\ModelConstraint object
     *
     * @param \Model\System\DataModel\Model\ModelConstraint|ObjectCollection $modelConstraint the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByModelConstraintRelatedByLocalFieldId($modelConstraint, $comparison = null)
    {
        if ($modelConstraint instanceof \Model\System\DataModel\Model\ModelConstraint) {
            return $this
                ->addUsingAlias(DataFieldTableMap::COL_ID, $modelConstraint->getLocalFieldId(), $comparison);
        } elseif ($modelConstraint instanceof ObjectCollection) {
            return $this
                ->useModelConstraintRelatedByLocalFieldIdQuery()
                ->filterByPrimaryKeys($modelConstraint->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByModelConstraintRelatedByLocalFieldId() only accepts arguments of type \Model\System\DataModel\Model\ModelConstraint or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ModelConstraintRelatedByLocalFieldId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function joinModelConstraintRelatedByLocalFieldId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ModelConstraintRelatedByLocalFieldId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ModelConstraintRelatedByLocalFieldId');
        }

        return $this;
    }

    /**
     * Use the ModelConstraintRelatedByLocalFieldId relation ModelConstraint object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\Model\ModelConstraintQuery A secondary query class using the current class as primary query
     */
    public function useModelConstraintRelatedByLocalFieldIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinModelConstraintRelatedByLocalFieldId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ModelConstraintRelatedByLocalFieldId', '\Model\System\DataModel\Model\ModelConstraintQuery');
    }

    /**
     * Filter the query by a related \Model\System\DataModel\Model\ModelConstraint object
     *
     * @param \Model\System\DataModel\Model\ModelConstraint|ObjectCollection $modelConstraint the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildDataFieldQuery The current query, for fluid interface
     */
    public function filterByModelConstraintRelatedByForeignFieldId($modelConstraint, $comparison = null)
    {
        if ($modelConstraint instanceof \Model\System\DataModel\Model\ModelConstraint) {
            return $this
                ->addUsingAlias(DataFieldTableMap::COL_ID, $modelConstraint->getForeignFieldId(), $comparison);
        } elseif ($modelConstraint instanceof ObjectCollection) {
            return $this
                ->useModelConstraintRelatedByForeignFieldIdQuery()
                ->filterByPrimaryKeys($modelConstraint->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByModelConstraintRelatedByForeignFieldId() only accepts arguments of type \Model\System\DataModel\Model\ModelConstraint or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ModelConstraintRelatedByForeignFieldId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function joinModelConstraintRelatedByForeignFieldId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ModelConstraintRelatedByForeignFieldId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ModelConstraintRelatedByForeignFieldId');
        }

        return $this;
    }

    /**
     * Use the ModelConstraintRelatedByForeignFieldId relation ModelConstraint object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\DataModel\Model\ModelConstraintQuery A secondary query class using the current class as primary query
     */
    public function useModelConstraintRelatedByForeignFieldIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinModelConstraintRelatedByForeignFieldId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ModelConstraintRelatedByForeignFieldId', '\Model\System\DataModel\Model\ModelConstraintQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildDataField $dataField Object to remove from the list of results
     *
     * @return $this|ChildDataFieldQuery The current query, for fluid interface
     */
    public function prune($dataField = null)
    {
        if ($dataField) {
            $this->addUsingAlias(DataFieldTableMap::COL_ID, $dataField->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the data_field table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DataFieldTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            DataFieldTableMap::clearInstancePool();
            DataFieldTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(DataFieldTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(DataFieldTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            DataFieldTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            DataFieldTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // DataFieldQuery
