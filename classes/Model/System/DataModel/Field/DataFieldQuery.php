<?php

namespace Model\System\DataModel\Field;

use Model\System\DataModel\Field\Base\DataFieldQuery as BaseDataFieldQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'data_field' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DataFieldQuery extends BaseDataFieldQuery
{

}
