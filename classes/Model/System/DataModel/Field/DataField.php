<?php

namespace Model\System\DataModel\Field;

use Exception\LogicException;
use Model\System\DataModel\DataTypeQuery;
use Model\System\DataModel\Field\Base\DataField as BaseDataField;
use Model\System\DataModel\DataType;
use Model\System\DataModel\Model\DataModelQuery;
use Model\System\FormFieldType;
use Model\System\FormFieldTypeQuery;
use Model\System\UI\Icon;
use Model\System\UI\IconQuery;

/**
 * Skeleton subclass for representing a row from the 'data_field' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DataField extends BaseDataField
{
    public static function findOneByName($sTableName, $sColumnName):?DataField
    {
        echo "Getting datamodel by name $sTableName " . PHP_EOL;
        $oDataModel = DataModelQuery::create()->findOneByName($sTableName);
        $oDataFieldQuery = DataFieldQuery::create();
        $oDataFieldQuery->filterByName($sColumnName);
        $oDataFieldQuery->filterByDataModelId($oDataModel->getId());
        $oDataField = $oDataFieldQuery->findOne();
        return $oDataField;
    }
    public function getIcon():Icon
    {
        if($this->getIconId())
        {
            return IconQuery::create()->findOneById($this->getIconId());
        }
        return new Icon();
    }
    public function getFormFieldType():FormFieldType
    {
        if($this->getFormFieldTypeId())
        {
            return FormFieldTypeQuery::create()->findOneById($this->getFormFieldTypeId());
        }
        return new FormFieldType();
    }
    public function getDataType():DataType
    {
        if($this->getDataTypeId())
        {
            return DataTypeQuery::create()->findOneById($this->getDataTypeId());
        }
        return new DataType();
    }

    public function setFormFieldTypeByName(string $sFormFieldType)
    {
        $oFormFieldType = FormFieldTypeQuery::create()->findOneByName($sFormFieldType);
        if(!$oFormFieldType)
        {
            throw new LogicException("Form field type $sFormFieldType missing");
        }
        $this->setFormFieldTypeId($oFormFieldType->getId());
    }

    public function setDataTypeByName(string $sDataType)
    {
        $oDataType = DataTypeQuery::create()->findOneByName($sDataType);

        if(is_null($oDataType))
        {
            echo "Datatype $sDataType missing in table data_type" . PHP_EOL;
            $oDataType = DataTypeQuery::create()->findOneByName('VARCHAR');
        }
        $this->setDataTypeId($oDataType->getId());
    }

    public function setIconByName(string $string)
    {
        $oIcon = IconQuery::create()->findOneByName($string);
        if($oIcon)
        {
            $this->setIconId($oIcon->getId());
        }
    }

}
