<?php

namespace Model\System\DataModel;

use Model\System\DataModel\Base\DataType as BaseDataType;

/**
 * Skeleton subclass for representing a row from the 'data_type' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class DataType extends BaseDataType
{
    const VARCHAR = 'VARCHAR';
    const BOOLEAN = 'BOOLEAN';
    const DATE = 'DATE';
    const DECIMAL = 'DECIMAL';
    const DOUBLE = 'DOUBLE';
    const ENUM = 'ENUM';
    const FLOAT = 'FLOAT';
    const INTEGER = 'INTEGER';
    const NUMERIC = 'NUMERIC';
    const TIME = 'TIME';
    const TIMESTAMP = 'TIMESTAMP';
    const TINYINT = 'TINYINT';

    static function get(string $sType):DataType
    {
        return DataTypeQuery::create()->findOneByName($sType);

    }
}
