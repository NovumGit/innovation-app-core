<?php

namespace Model\System\DataModel\Behavior;

use Model\System\DataModel\Behavior\Base\Behavior as BaseBehavior;

/**
 * Skeleton subclass for representing a row from the 'behavior' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class Behavior extends BaseBehavior
{

}
