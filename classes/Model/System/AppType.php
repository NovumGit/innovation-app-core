<?php

namespace Model\System;

use Core\Utils;
use Model\System\Base\AppType as BaseAppType;

/**
 * Skeleton subclass for representing a row from the 'app_type' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class AppType extends BaseAppType
{

    function getCode()
    {
        return Utils::snake_case($this->getName());
    }
}
