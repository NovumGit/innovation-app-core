<?php

namespace Model\System;

use Model\System\Base\SystemRegistryQuery as BaseSystemRegistryQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'system_registry' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SystemRegistryQuery extends BaseSystemRegistryQuery
{
    static $aAllSettingsAssoc = [];

    private static function loadAllSettingsAssoc()
    {
        if(empty(self::$aAllSettingsAssoc))
        {
            $oQuery = self::create();
            $aAllSettings = $oQuery->find();

            if($aAllSettings->isEmpty())
            {
                return null;
            }
            self::$aAllSettingsAssoc = [];
            foreach($aAllSettings as $oSetting)
            {
                self::$aAllSettingsAssoc[$oSetting->getItemKey()] = $oSetting->getItemValue();
            }
        }
    }

    static function getVal($sKey):string
    {
        self::loadAllSettingsAssoc();
        if(isset(self::$aAllSettingsAssoc[$sKey]))
        {
            return self::$aAllSettingsAssoc[$sKey];
        }
        $oSystemRegistry = self::create()->findOneByItemKey($sKey);
        if($oSystemRegistry instanceof SystemRegistry)
        {
            return $oSystemRegistry->getItemValue();
        }
        return '';
    }

    static function getAllSettingsAsssoc()
    {
        self::loadAllSettingsAssoc();
        return self::$aAllSettingsAssoc;
    }
    public function createOrOverwrite($sKey, $sValue)
    {
        $oSystemRegistry = SystemRegistryQuery::create()->findOneByItemKey($sKey);

        if(!$oSystemRegistry)
        {
            $oSystemRegistry = new SystemRegistry();
            $oSystemRegistry->setItemKey($sKey);
        }
        $oSystemRegistry->setItemValue($sValue);
        $oSystemRegistry->save();
    }

    public function getNewOrEmpty($sKey)
    {
        $oSystemRegistry = SystemRegistryQuery::create()->findOneByItemKey($sKey);

        if(!$oSystemRegistry)
        {
            $oSystemRegistry = new SystemRegistry();
            $oSystemRegistry->setItemKey($sKey);
            $oSystemRegistry->setItemValue('');
        }

        $oSystemRegistry->save();
        return $oSystemRegistry->getItemValue();
    }
}
