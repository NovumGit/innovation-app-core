<?php

namespace Model\System\UI;

use Model\System\UI\Base\FormUIComponent as BaseFormUIComponent;

/**
 * Skeleton subclass for representing a row from the 'form_ui_component' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class FormUIComponent extends BaseFormUIComponent
{

}
