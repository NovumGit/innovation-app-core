<?php

namespace Model\System\UI\Base;

use \Exception;
use \PDO;
use Model\System\App;
use Model\System\AppQuery;
use Model\System\UI\UIComponent as ChildUIComponent;
use Model\System\UI\UIComponentQuery as ChildUIComponentQuery;
use Model\System\UI\UIComponentType as ChildUIComponentType;
use Model\System\UI\UIComponentTypeQuery as ChildUIComponentTypeQuery;
use Model\System\UI\Map\UIComponentTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'ui_component' table.
 *
 *
 *
 * @package    propel.generator.Model.System.UI.Base
 */
abstract class UIComponent implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\System\\UI\\Map\\UIComponentTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the app_id field.
     *
     * @var        int|null
     */
    protected $app_id;

    /**
     * The value for the parent_id field.
     *
     * @var        int|null
     */
    protected $parent_id;

    /**
     * The value for the parent_location field.
     *
     * @var        string|null
     */
    protected $parent_location;

    /**
     * The value for the ui_component_type_id field.
     *
     * @var        int|null
     */
    protected $ui_component_type_id;

    /**
     * The value for the label field.
     *
     * @var        string
     */
    protected $label;

    /**
     * @var        ChildUIComponent
     */
    protected $aUIComponentRelatedByParentId;

    /**
     * @var        App
     */
    protected $aApp;

    /**
     * @var        ChildUIComponentType
     */
    protected $aUIComponentType;

    /**
     * @var        ObjectCollection|ChildUIComponent[] Collection to store aggregation of ChildUIComponent objects.
     */
    protected $collUIComponentsRelatedById;
    protected $collUIComponentsRelatedByIdPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUIComponent[]
     */
    protected $uIComponentsRelatedByIdScheduledForDeletion = null;

    /**
     * Initializes internal state of Model\System\UI\Base\UIComponent object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>UIComponent</code> instance.  If
     * <code>obj</code> is an instance of <code>UIComponent</code>, delegates to
     * <code>equals(UIComponent)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [app_id] column value.
     *
     * @return int|null
     */
    public function getAppId()
    {
        return $this->app_id;
    }

    /**
     * Get the [parent_id] column value.
     *
     * @return int|null
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Get the [parent_location] column value.
     *
     * @return string|null
     */
    public function getParentLocation()
    {
        return $this->parent_location;
    }

    /**
     * Get the [ui_component_type_id] column value.
     *
     * @return int|null
     */
    public function getUiComponentTypeId()
    {
        return $this->ui_component_type_id;
    }

    /**
     * Get the [label] column value.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\UI\UIComponent The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[UIComponentTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [app_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\System\UI\UIComponent The current object (for fluent API support)
     */
    public function setAppId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->app_id !== $v) {
            $this->app_id = $v;
            $this->modifiedColumns[UIComponentTableMap::COL_APP_ID] = true;
        }

        if ($this->aApp !== null && $this->aApp->getId() !== $v) {
            $this->aApp = null;
        }

        return $this;
    } // setAppId()

    /**
     * Set the value of [parent_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\System\UI\UIComponent The current object (for fluent API support)
     */
    public function setParentId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->parent_id !== $v) {
            $this->parent_id = $v;
            $this->modifiedColumns[UIComponentTableMap::COL_PARENT_ID] = true;
        }

        if ($this->aUIComponentRelatedByParentId !== null && $this->aUIComponentRelatedByParentId->getId() !== $v) {
            $this->aUIComponentRelatedByParentId = null;
        }

        return $this;
    } // setParentId()

    /**
     * Set the value of [parent_location] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\System\UI\UIComponent The current object (for fluent API support)
     */
    public function setParentLocation($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->parent_location !== $v) {
            $this->parent_location = $v;
            $this->modifiedColumns[UIComponentTableMap::COL_PARENT_LOCATION] = true;
        }

        return $this;
    } // setParentLocation()

    /**
     * Set the value of [ui_component_type_id] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\System\UI\UIComponent The current object (for fluent API support)
     */
    public function setUiComponentTypeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ui_component_type_id !== $v) {
            $this->ui_component_type_id = $v;
            $this->modifiedColumns[UIComponentTableMap::COL_UI_COMPONENT_TYPE_ID] = true;
        }

        if ($this->aUIComponentType !== null && $this->aUIComponentType->getId() !== $v) {
            $this->aUIComponentType = null;
        }

        return $this;
    } // setUiComponentTypeId()

    /**
     * Set the value of [label] column.
     *
     * @param string $v New value
     * @return $this|\Model\System\UI\UIComponent The current object (for fluent API support)
     */
    public function setLabel($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->label !== $v) {
            $this->label = $v;
            $this->modifiedColumns[UIComponentTableMap::COL_LABEL] = true;
        }

        return $this;
    } // setLabel()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UIComponentTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UIComponentTableMap::translateFieldName('AppId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->app_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UIComponentTableMap::translateFieldName('ParentId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->parent_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UIComponentTableMap::translateFieldName('ParentLocation', TableMap::TYPE_PHPNAME, $indexType)];
            $this->parent_location = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UIComponentTableMap::translateFieldName('UiComponentTypeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ui_component_type_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UIComponentTableMap::translateFieldName('Label', TableMap::TYPE_PHPNAME, $indexType)];
            $this->label = (null !== $col) ? (string) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 6; // 6 = UIComponentTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\System\\UI\\UIComponent'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aApp !== null && $this->app_id !== $this->aApp->getId()) {
            $this->aApp = null;
        }
        if ($this->aUIComponentRelatedByParentId !== null && $this->parent_id !== $this->aUIComponentRelatedByParentId->getId()) {
            $this->aUIComponentRelatedByParentId = null;
        }
        if ($this->aUIComponentType !== null && $this->ui_component_type_id !== $this->aUIComponentType->getId()) {
            $this->aUIComponentType = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UIComponentTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUIComponentQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aUIComponentRelatedByParentId = null;
            $this->aApp = null;
            $this->aUIComponentType = null;
            $this->collUIComponentsRelatedById = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see UIComponent::setDeleted()
     * @see UIComponent::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UIComponentTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUIComponentQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UIComponentTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UIComponentTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aUIComponentRelatedByParentId !== null) {
                if ($this->aUIComponentRelatedByParentId->isModified() || $this->aUIComponentRelatedByParentId->isNew()) {
                    $affectedRows += $this->aUIComponentRelatedByParentId->save($con);
                }
                $this->setUIComponentRelatedByParentId($this->aUIComponentRelatedByParentId);
            }

            if ($this->aApp !== null) {
                if ($this->aApp->isModified() || $this->aApp->isNew()) {
                    $affectedRows += $this->aApp->save($con);
                }
                $this->setApp($this->aApp);
            }

            if ($this->aUIComponentType !== null) {
                if ($this->aUIComponentType->isModified() || $this->aUIComponentType->isNew()) {
                    $affectedRows += $this->aUIComponentType->save($con);
                }
                $this->setUIComponentType($this->aUIComponentType);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->uIComponentsRelatedByIdScheduledForDeletion !== null) {
                if (!$this->uIComponentsRelatedByIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->uIComponentsRelatedByIdScheduledForDeletion as $uIComponentRelatedById) {
                        // need to save related object because we set the relation to null
                        $uIComponentRelatedById->save($con);
                    }
                    $this->uIComponentsRelatedByIdScheduledForDeletion = null;
                }
            }

            if ($this->collUIComponentsRelatedById !== null) {
                foreach ($this->collUIComponentsRelatedById as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UIComponentTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UIComponentTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UIComponentTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(UIComponentTableMap::COL_APP_ID)) {
            $modifiedColumns[':p' . $index++]  = 'app_id';
        }
        if ($this->isColumnModified(UIComponentTableMap::COL_PARENT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'parent_id';
        }
        if ($this->isColumnModified(UIComponentTableMap::COL_PARENT_LOCATION)) {
            $modifiedColumns[':p' . $index++]  = 'parent_location';
        }
        if ($this->isColumnModified(UIComponentTableMap::COL_UI_COMPONENT_TYPE_ID)) {
            $modifiedColumns[':p' . $index++]  = 'ui_component_type_id';
        }
        if ($this->isColumnModified(UIComponentTableMap::COL_LABEL)) {
            $modifiedColumns[':p' . $index++]  = 'label';
        }

        $sql = sprintf(
            'INSERT INTO ui_component (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'app_id':
                        $stmt->bindValue($identifier, $this->app_id, PDO::PARAM_INT);
                        break;
                    case 'parent_id':
                        $stmt->bindValue($identifier, $this->parent_id, PDO::PARAM_INT);
                        break;
                    case 'parent_location':
                        $stmt->bindValue($identifier, $this->parent_location, PDO::PARAM_STR);
                        break;
                    case 'ui_component_type_id':
                        $stmt->bindValue($identifier, $this->ui_component_type_id, PDO::PARAM_INT);
                        break;
                    case 'label':
                        $stmt->bindValue($identifier, $this->label, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UIComponentTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getAppId();
                break;
            case 2:
                return $this->getParentId();
                break;
            case 3:
                return $this->getParentLocation();
                break;
            case 4:
                return $this->getUiComponentTypeId();
                break;
            case 5:
                return $this->getLabel();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['UIComponent'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['UIComponent'][$this->hashCode()] = true;
        $keys = UIComponentTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getAppId(),
            $keys[2] => $this->getParentId(),
            $keys[3] => $this->getParentLocation(),
            $keys[4] => $this->getUiComponentTypeId(),
            $keys[5] => $this->getLabel(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aUIComponentRelatedByParentId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'uIComponent';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'ui_component';
                        break;
                    default:
                        $key = 'UIComponent';
                }

                $result[$key] = $this->aUIComponentRelatedByParentId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aApp) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'app';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'app';
                        break;
                    default:
                        $key = 'App';
                }

                $result[$key] = $this->aApp->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUIComponentType) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'uIComponentType';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'ui_component_type';
                        break;
                    default:
                        $key = 'UIComponentType';
                }

                $result[$key] = $this->aUIComponentType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collUIComponentsRelatedById) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'uIComponents';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'ui_components';
                        break;
                    default:
                        $key = 'UIComponents';
                }

                $result[$key] = $this->collUIComponentsRelatedById->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\System\UI\UIComponent
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UIComponentTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\System\UI\UIComponent
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setAppId($value);
                break;
            case 2:
                $this->setParentId($value);
                break;
            case 3:
                $this->setParentLocation($value);
                break;
            case 4:
                $this->setUiComponentTypeId($value);
                break;
            case 5:
                $this->setLabel($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UIComponentTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setAppId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setParentId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setParentLocation($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setUiComponentTypeId($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setLabel($arr[$keys[5]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\System\UI\UIComponent The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UIComponentTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UIComponentTableMap::COL_ID)) {
            $criteria->add(UIComponentTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(UIComponentTableMap::COL_APP_ID)) {
            $criteria->add(UIComponentTableMap::COL_APP_ID, $this->app_id);
        }
        if ($this->isColumnModified(UIComponentTableMap::COL_PARENT_ID)) {
            $criteria->add(UIComponentTableMap::COL_PARENT_ID, $this->parent_id);
        }
        if ($this->isColumnModified(UIComponentTableMap::COL_PARENT_LOCATION)) {
            $criteria->add(UIComponentTableMap::COL_PARENT_LOCATION, $this->parent_location);
        }
        if ($this->isColumnModified(UIComponentTableMap::COL_UI_COMPONENT_TYPE_ID)) {
            $criteria->add(UIComponentTableMap::COL_UI_COMPONENT_TYPE_ID, $this->ui_component_type_id);
        }
        if ($this->isColumnModified(UIComponentTableMap::COL_LABEL)) {
            $criteria->add(UIComponentTableMap::COL_LABEL, $this->label);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUIComponentQuery::create();
        $criteria->add(UIComponentTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\System\UI\UIComponent (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setAppId($this->getAppId());
        $copyObj->setParentId($this->getParentId());
        $copyObj->setParentLocation($this->getParentLocation());
        $copyObj->setUiComponentTypeId($this->getUiComponentTypeId());
        $copyObj->setLabel($this->getLabel());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getUIComponentsRelatedById() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUIComponentRelatedById($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\System\UI\UIComponent Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildUIComponent object.
     *
     * @param  ChildUIComponent|null $v
     * @return $this|\Model\System\UI\UIComponent The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUIComponentRelatedByParentId(ChildUIComponent $v = null)
    {
        if ($v === null) {
            $this->setParentId(NULL);
        } else {
            $this->setParentId($v->getId());
        }

        $this->aUIComponentRelatedByParentId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUIComponent object, it will not be re-added.
        if ($v !== null) {
            $v->addUIComponentRelatedById($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUIComponent object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUIComponent|null The associated ChildUIComponent object.
     * @throws PropelException
     */
    public function getUIComponentRelatedByParentId(ConnectionInterface $con = null)
    {
        if ($this->aUIComponentRelatedByParentId === null && ($this->parent_id != 0)) {
            $this->aUIComponentRelatedByParentId = ChildUIComponentQuery::create()->findPk($this->parent_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUIComponentRelatedByParentId->addUIComponentsRelatedById($this);
             */
        }

        return $this->aUIComponentRelatedByParentId;
    }

    /**
     * Declares an association between this object and a App object.
     *
     * @param  App|null $v
     * @return $this|\Model\System\UI\UIComponent The current object (for fluent API support)
     * @throws PropelException
     */
    public function setApp(App $v = null)
    {
        if ($v === null) {
            $this->setAppId(NULL);
        } else {
            $this->setAppId($v->getId());
        }

        $this->aApp = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the App object, it will not be re-added.
        if ($v !== null) {
            $v->addUIComponent($this);
        }


        return $this;
    }


    /**
     * Get the associated App object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return App|null The associated App object.
     * @throws PropelException
     */
    public function getApp(ConnectionInterface $con = null)
    {
        if ($this->aApp === null && ($this->app_id != 0)) {
            $this->aApp = AppQuery::create()->findPk($this->app_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aApp->addUIComponents($this);
             */
        }

        return $this->aApp;
    }

    /**
     * Declares an association between this object and a ChildUIComponentType object.
     *
     * @param  ChildUIComponentType|null $v
     * @return $this|\Model\System\UI\UIComponent The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUIComponentType(ChildUIComponentType $v = null)
    {
        if ($v === null) {
            $this->setUiComponentTypeId(NULL);
        } else {
            $this->setUiComponentTypeId($v->getId());
        }

        $this->aUIComponentType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildUIComponentType object, it will not be re-added.
        if ($v !== null) {
            $v->addUIComponent($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildUIComponentType object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildUIComponentType|null The associated ChildUIComponentType object.
     * @throws PropelException
     */
    public function getUIComponentType(ConnectionInterface $con = null)
    {
        if ($this->aUIComponentType === null && ($this->ui_component_type_id != 0)) {
            $this->aUIComponentType = ChildUIComponentTypeQuery::create()->findPk($this->ui_component_type_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUIComponentType->addUIComponents($this);
             */
        }

        return $this->aUIComponentType;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('UIComponentRelatedById' === $relationName) {
            $this->initUIComponentsRelatedById();
            return;
        }
    }

    /**
     * Clears out the collUIComponentsRelatedById collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUIComponentsRelatedById()
     */
    public function clearUIComponentsRelatedById()
    {
        $this->collUIComponentsRelatedById = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUIComponentsRelatedById collection loaded partially.
     */
    public function resetPartialUIComponentsRelatedById($v = true)
    {
        $this->collUIComponentsRelatedByIdPartial = $v;
    }

    /**
     * Initializes the collUIComponentsRelatedById collection.
     *
     * By default this just sets the collUIComponentsRelatedById collection to an empty array (like clearcollUIComponentsRelatedById());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUIComponentsRelatedById($overrideExisting = true)
    {
        if (null !== $this->collUIComponentsRelatedById && !$overrideExisting) {
            return;
        }

        $collectionClassName = UIComponentTableMap::getTableMap()->getCollectionClassName();

        $this->collUIComponentsRelatedById = new $collectionClassName;
        $this->collUIComponentsRelatedById->setModel('\Model\System\UI\UIComponent');
    }

    /**
     * Gets an array of ChildUIComponent objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUIComponent is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUIComponent[] List of ChildUIComponent objects
     * @throws PropelException
     */
    public function getUIComponentsRelatedById(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUIComponentsRelatedByIdPartial && !$this->isNew();
        if (null === $this->collUIComponentsRelatedById || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUIComponentsRelatedById) {
                    $this->initUIComponentsRelatedById();
                } else {
                    $collectionClassName = UIComponentTableMap::getTableMap()->getCollectionClassName();

                    $collUIComponentsRelatedById = new $collectionClassName;
                    $collUIComponentsRelatedById->setModel('\Model\System\UI\UIComponent');

                    return $collUIComponentsRelatedById;
                }
            } else {
                $collUIComponentsRelatedById = ChildUIComponentQuery::create(null, $criteria)
                    ->filterByUIComponentRelatedByParentId($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUIComponentsRelatedByIdPartial && count($collUIComponentsRelatedById)) {
                        $this->initUIComponentsRelatedById(false);

                        foreach ($collUIComponentsRelatedById as $obj) {
                            if (false == $this->collUIComponentsRelatedById->contains($obj)) {
                                $this->collUIComponentsRelatedById->append($obj);
                            }
                        }

                        $this->collUIComponentsRelatedByIdPartial = true;
                    }

                    return $collUIComponentsRelatedById;
                }

                if ($partial && $this->collUIComponentsRelatedById) {
                    foreach ($this->collUIComponentsRelatedById as $obj) {
                        if ($obj->isNew()) {
                            $collUIComponentsRelatedById[] = $obj;
                        }
                    }
                }

                $this->collUIComponentsRelatedById = $collUIComponentsRelatedById;
                $this->collUIComponentsRelatedByIdPartial = false;
            }
        }

        return $this->collUIComponentsRelatedById;
    }

    /**
     * Sets a collection of ChildUIComponent objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $uIComponentsRelatedById A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUIComponent The current object (for fluent API support)
     */
    public function setUIComponentsRelatedById(Collection $uIComponentsRelatedById, ConnectionInterface $con = null)
    {
        /** @var ChildUIComponent[] $uIComponentsRelatedByIdToDelete */
        $uIComponentsRelatedByIdToDelete = $this->getUIComponentsRelatedById(new Criteria(), $con)->diff($uIComponentsRelatedById);


        $this->uIComponentsRelatedByIdScheduledForDeletion = $uIComponentsRelatedByIdToDelete;

        foreach ($uIComponentsRelatedByIdToDelete as $uIComponentRelatedByIdRemoved) {
            $uIComponentRelatedByIdRemoved->setUIComponentRelatedByParentId(null);
        }

        $this->collUIComponentsRelatedById = null;
        foreach ($uIComponentsRelatedById as $uIComponentRelatedById) {
            $this->addUIComponentRelatedById($uIComponentRelatedById);
        }

        $this->collUIComponentsRelatedById = $uIComponentsRelatedById;
        $this->collUIComponentsRelatedByIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UIComponent objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UIComponent objects.
     * @throws PropelException
     */
    public function countUIComponentsRelatedById(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUIComponentsRelatedByIdPartial && !$this->isNew();
        if (null === $this->collUIComponentsRelatedById || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUIComponentsRelatedById) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUIComponentsRelatedById());
            }

            $query = ChildUIComponentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUIComponentRelatedByParentId($this)
                ->count($con);
        }

        return count($this->collUIComponentsRelatedById);
    }

    /**
     * Method called to associate a ChildUIComponent object to this object
     * through the ChildUIComponent foreign key attribute.
     *
     * @param  ChildUIComponent $l ChildUIComponent
     * @return $this|\Model\System\UI\UIComponent The current object (for fluent API support)
     */
    public function addUIComponentRelatedById(ChildUIComponent $l)
    {
        if ($this->collUIComponentsRelatedById === null) {
            $this->initUIComponentsRelatedById();
            $this->collUIComponentsRelatedByIdPartial = true;
        }

        if (!$this->collUIComponentsRelatedById->contains($l)) {
            $this->doAddUIComponentRelatedById($l);

            if ($this->uIComponentsRelatedByIdScheduledForDeletion and $this->uIComponentsRelatedByIdScheduledForDeletion->contains($l)) {
                $this->uIComponentsRelatedByIdScheduledForDeletion->remove($this->uIComponentsRelatedByIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUIComponent $uIComponentRelatedById The ChildUIComponent object to add.
     */
    protected function doAddUIComponentRelatedById(ChildUIComponent $uIComponentRelatedById)
    {
        $this->collUIComponentsRelatedById[]= $uIComponentRelatedById;
        $uIComponentRelatedById->setUIComponentRelatedByParentId($this);
    }

    /**
     * @param  ChildUIComponent $uIComponentRelatedById The ChildUIComponent object to remove.
     * @return $this|ChildUIComponent The current object (for fluent API support)
     */
    public function removeUIComponentRelatedById(ChildUIComponent $uIComponentRelatedById)
    {
        if ($this->getUIComponentsRelatedById()->contains($uIComponentRelatedById)) {
            $pos = $this->collUIComponentsRelatedById->search($uIComponentRelatedById);
            $this->collUIComponentsRelatedById->remove($pos);
            if (null === $this->uIComponentsRelatedByIdScheduledForDeletion) {
                $this->uIComponentsRelatedByIdScheduledForDeletion = clone $this->collUIComponentsRelatedById;
                $this->uIComponentsRelatedByIdScheduledForDeletion->clear();
            }
            $this->uIComponentsRelatedByIdScheduledForDeletion[]= $uIComponentRelatedById;
            $uIComponentRelatedById->setUIComponentRelatedByParentId(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UIComponent is new, it will return
     * an empty collection; or if this UIComponent has previously
     * been saved, it will retrieve related UIComponentsRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UIComponent.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUIComponent[] List of ChildUIComponent objects
     */
    public function getUIComponentsRelatedByIdJoinApp(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUIComponentQuery::create(null, $criteria);
        $query->joinWith('App', $joinBehavior);

        return $this->getUIComponentsRelatedById($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UIComponent is new, it will return
     * an empty collection; or if this UIComponent has previously
     * been saved, it will retrieve related UIComponentsRelatedById from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UIComponent.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUIComponent[] List of ChildUIComponent objects
     */
    public function getUIComponentsRelatedByIdJoinUIComponentType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUIComponentQuery::create(null, $criteria);
        $query->joinWith('UIComponentType', $joinBehavior);

        return $this->getUIComponentsRelatedById($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aUIComponentRelatedByParentId) {
            $this->aUIComponentRelatedByParentId->removeUIComponentRelatedById($this);
        }
        if (null !== $this->aApp) {
            $this->aApp->removeUIComponent($this);
        }
        if (null !== $this->aUIComponentType) {
            $this->aUIComponentType->removeUIComponent($this);
        }
        $this->id = null;
        $this->app_id = null;
        $this->parent_id = null;
        $this->parent_location = null;
        $this->ui_component_type_id = null;
        $this->label = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collUIComponentsRelatedById) {
                foreach ($this->collUIComponentsRelatedById as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collUIComponentsRelatedById = null;
        $this->aUIComponentRelatedByParentId = null;
        $this->aApp = null;
        $this->aUIComponentType = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UIComponentTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
