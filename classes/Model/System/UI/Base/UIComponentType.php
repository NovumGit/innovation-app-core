<?php

namespace Model\System\UI\Base;

use \Exception;
use \PDO;
use Model\System\UI\UIComponent as ChildUIComponent;
use Model\System\UI\UIComponentQuery as ChildUIComponentQuery;
use Model\System\UI\UIComponentType as ChildUIComponentType;
use Model\System\UI\UIComponentTypeAccepts as ChildUIComponentTypeAccepts;
use Model\System\UI\UIComponentTypeAcceptsQuery as ChildUIComponentTypeAcceptsQuery;
use Model\System\UI\UIComponentTypeQuery as ChildUIComponentTypeQuery;
use Model\System\UI\Map\UIComponentTableMap;
use Model\System\UI\Map\UIComponentTypeAcceptsTableMap;
use Model\System\UI\Map\UIComponentTypeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'ui_component_type' table.
 *
 *
 *
 * @package    propel.generator.Model.System.UI.Base
 */
abstract class UIComponentType implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\System\\UI\\Map\\UIComponentTypeTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the title field.
     *
     * @var        string|null
     */
    protected $title;

    /**
     * The value for the is_web field.
     *
     * @var        boolean|null
     */
    protected $is_web;

    /**
     * The value for the is_app field.
     *
     * @var        boolean|null
     */
    protected $is_app;

    /**
     * The value for the component_class field.
     *
     * @var        string|null
     */
    protected $component_class;

    /**
     * The value for the accept_any field.
     *
     * @var        boolean|null
     */
    protected $accept_any;

    /**
     * @var        ObjectCollection|ChildUIComponentTypeAccepts[] Collection to store aggregation of ChildUIComponentTypeAccepts objects.
     */
    protected $collUIComponentTypeAcceptssRelatedByContainerComponentTypeId;
    protected $collUIComponentTypeAcceptssRelatedByContainerComponentTypeIdPartial;

    /**
     * @var        ObjectCollection|ChildUIComponentTypeAccepts[] Collection to store aggregation of ChildUIComponentTypeAccepts objects.
     */
    protected $collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId;
    protected $collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdPartial;

    /**
     * @var        ObjectCollection|ChildUIComponent[] Collection to store aggregation of ChildUIComponent objects.
     */
    protected $collUIComponents;
    protected $collUIComponentsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUIComponentTypeAccepts[]
     */
    protected $uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUIComponentTypeAccepts[]
     */
    protected $uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildUIComponent[]
     */
    protected $uIComponentsScheduledForDeletion = null;

    /**
     * Initializes internal state of Model\System\UI\Base\UIComponentType object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>UIComponentType</code> instance.  If
     * <code>obj</code> is an instance of <code>UIComponentType</code>, delegates to
     * <code>equals(UIComponentType)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [title] column value.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [is_web] column value.
     *
     * @return boolean|null
     */
    public function getIsWeb()
    {
        return $this->is_web;
    }

    /**
     * Get the [is_web] column value.
     *
     * @return boolean|null
     */
    public function isWeb()
    {
        return $this->getIsWeb();
    }

    /**
     * Get the [is_app] column value.
     *
     * @return boolean|null
     */
    public function getIsApp()
    {
        return $this->is_app;
    }

    /**
     * Get the [is_app] column value.
     *
     * @return boolean|null
     */
    public function isApp()
    {
        return $this->getIsApp();
    }

    /**
     * Get the [component_class] column value.
     *
     * @return string|null
     */
    public function getComponentClass()
    {
        return $this->component_class;
    }

    /**
     * Get the [accept_any] column value.
     *
     * @return boolean|null
     */
    public function getAcceptAny()
    {
        return $this->accept_any;
    }

    /**
     * Get the [accept_any] column value.
     *
     * @return boolean|null
     */
    public function isAcceptAny()
    {
        return $this->getAcceptAny();
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\UI\UIComponentType The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[UIComponentTypeTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [title] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\System\UI\UIComponentType The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[UIComponentTypeTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Sets the value of the [is_web] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\System\UI\UIComponentType The current object (for fluent API support)
     */
    public function setIsWeb($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_web !== $v) {
            $this->is_web = $v;
            $this->modifiedColumns[UIComponentTypeTableMap::COL_IS_WEB] = true;
        }

        return $this;
    } // setIsWeb()

    /**
     * Sets the value of the [is_app] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\System\UI\UIComponentType The current object (for fluent API support)
     */
    public function setIsApp($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_app !== $v) {
            $this->is_app = $v;
            $this->modifiedColumns[UIComponentTypeTableMap::COL_IS_APP] = true;
        }

        return $this;
    } // setIsApp()

    /**
     * Set the value of [component_class] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\System\UI\UIComponentType The current object (for fluent API support)
     */
    public function setComponentClass($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->component_class !== $v) {
            $this->component_class = $v;
            $this->modifiedColumns[UIComponentTypeTableMap::COL_COMPONENT_CLASS] = true;
        }

        return $this;
    } // setComponentClass()

    /**
     * Sets the value of the [accept_any] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\System\UI\UIComponentType The current object (for fluent API support)
     */
    public function setAcceptAny($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->accept_any !== $v) {
            $this->accept_any = $v;
            $this->modifiedColumns[UIComponentTypeTableMap::COL_ACCEPT_ANY] = true;
        }

        return $this;
    } // setAcceptAny()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : UIComponentTypeTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : UIComponentTypeTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : UIComponentTypeTableMap::translateFieldName('IsWeb', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_web = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : UIComponentTypeTableMap::translateFieldName('IsApp', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_app = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : UIComponentTypeTableMap::translateFieldName('ComponentClass', TableMap::TYPE_PHPNAME, $indexType)];
            $this->component_class = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : UIComponentTypeTableMap::translateFieldName('AcceptAny', TableMap::TYPE_PHPNAME, $indexType)];
            $this->accept_any = (null !== $col) ? (boolean) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 6; // 6 = UIComponentTypeTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\System\\UI\\UIComponentType'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UIComponentTypeTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildUIComponentTypeQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId = null;

            $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId = null;

            $this->collUIComponents = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see UIComponentType::setDeleted()
     * @see UIComponentType::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UIComponentTypeTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildUIComponentTypeQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(UIComponentTypeTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UIComponentTypeTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion !== null) {
                if (!$this->uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion as $uIComponentTypeAcceptsRelatedByContainerComponentTypeId) {
                        // need to save related object because we set the relation to null
                        $uIComponentTypeAcceptsRelatedByContainerComponentTypeId->save($con);
                    }
                    $this->uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion = null;
                }
            }

            if ($this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId !== null) {
                foreach ($this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion !== null) {
                if (!$this->uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion->isEmpty()) {
                    foreach ($this->uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion as $uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId) {
                        // need to save related object because we set the relation to null
                        $uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId->save($con);
                    }
                    $this->uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion = null;
                }
            }

            if ($this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId !== null) {
                foreach ($this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->uIComponentsScheduledForDeletion !== null) {
                if (!$this->uIComponentsScheduledForDeletion->isEmpty()) {
                    foreach ($this->uIComponentsScheduledForDeletion as $uIComponent) {
                        // need to save related object because we set the relation to null
                        $uIComponent->save($con);
                    }
                    $this->uIComponentsScheduledForDeletion = null;
                }
            }

            if ($this->collUIComponents !== null) {
                foreach ($this->collUIComponents as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[UIComponentTypeTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UIComponentTypeTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UIComponentTypeTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(UIComponentTypeTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(UIComponentTypeTableMap::COL_IS_WEB)) {
            $modifiedColumns[':p' . $index++]  = 'is_web';
        }
        if ($this->isColumnModified(UIComponentTypeTableMap::COL_IS_APP)) {
            $modifiedColumns[':p' . $index++]  = 'is_app';
        }
        if ($this->isColumnModified(UIComponentTypeTableMap::COL_COMPONENT_CLASS)) {
            $modifiedColumns[':p' . $index++]  = 'component_class';
        }
        if ($this->isColumnModified(UIComponentTypeTableMap::COL_ACCEPT_ANY)) {
            $modifiedColumns[':p' . $index++]  = 'accept_any';
        }

        $sql = sprintf(
            'INSERT INTO ui_component_type (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'is_web':
                        $stmt->bindValue($identifier, (int) $this->is_web, PDO::PARAM_INT);
                        break;
                    case 'is_app':
                        $stmt->bindValue($identifier, (int) $this->is_app, PDO::PARAM_INT);
                        break;
                    case 'component_class':
                        $stmt->bindValue($identifier, $this->component_class, PDO::PARAM_STR);
                        break;
                    case 'accept_any':
                        $stmt->bindValue($identifier, (int) $this->accept_any, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UIComponentTypeTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getTitle();
                break;
            case 2:
                return $this->getIsWeb();
                break;
            case 3:
                return $this->getIsApp();
                break;
            case 4:
                return $this->getComponentClass();
                break;
            case 5:
                return $this->getAcceptAny();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['UIComponentType'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['UIComponentType'][$this->hashCode()] = true;
        $keys = UIComponentTypeTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getTitle(),
            $keys[2] => $this->getIsWeb(),
            $keys[3] => $this->getIsApp(),
            $keys[4] => $this->getComponentClass(),
            $keys[5] => $this->getAcceptAny(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'uIComponentTypeAcceptss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'ui_component_type_acceptss';
                        break;
                    default:
                        $key = 'UIComponentTypeAcceptss';
                }

                $result[$key] = $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'uIComponentTypeAcceptss';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'ui_component_type_acceptss';
                        break;
                    default:
                        $key = 'UIComponentTypeAcceptss';
                }

                $result[$key] = $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collUIComponents) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'uIComponents';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'ui_components';
                        break;
                    default:
                        $key = 'UIComponents';
                }

                $result[$key] = $this->collUIComponents->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\System\UI\UIComponentType
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = UIComponentTypeTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\System\UI\UIComponentType
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setTitle($value);
                break;
            case 2:
                $this->setIsWeb($value);
                break;
            case 3:
                $this->setIsApp($value);
                break;
            case 4:
                $this->setComponentClass($value);
                break;
            case 5:
                $this->setAcceptAny($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = UIComponentTypeTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setTitle($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setIsWeb($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setIsApp($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setComponentClass($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setAcceptAny($arr[$keys[5]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\System\UI\UIComponentType The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UIComponentTypeTableMap::DATABASE_NAME);

        if ($this->isColumnModified(UIComponentTypeTableMap::COL_ID)) {
            $criteria->add(UIComponentTypeTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(UIComponentTypeTableMap::COL_TITLE)) {
            $criteria->add(UIComponentTypeTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(UIComponentTypeTableMap::COL_IS_WEB)) {
            $criteria->add(UIComponentTypeTableMap::COL_IS_WEB, $this->is_web);
        }
        if ($this->isColumnModified(UIComponentTypeTableMap::COL_IS_APP)) {
            $criteria->add(UIComponentTypeTableMap::COL_IS_APP, $this->is_app);
        }
        if ($this->isColumnModified(UIComponentTypeTableMap::COL_COMPONENT_CLASS)) {
            $criteria->add(UIComponentTypeTableMap::COL_COMPONENT_CLASS, $this->component_class);
        }
        if ($this->isColumnModified(UIComponentTypeTableMap::COL_ACCEPT_ANY)) {
            $criteria->add(UIComponentTypeTableMap::COL_ACCEPT_ANY, $this->accept_any);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildUIComponentTypeQuery::create();
        $criteria->add(UIComponentTypeTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\System\UI\UIComponentType (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setTitle($this->getTitle());
        $copyObj->setIsWeb($this->getIsWeb());
        $copyObj->setIsApp($this->getIsApp());
        $copyObj->setComponentClass($this->getComponentClass());
        $copyObj->setAcceptAny($this->getAcceptAny());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getUIComponentTypeAcceptssRelatedByContainerComponentTypeId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUIComponentTypeAcceptsRelatedByContainerComponentTypeId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getUIComponents() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addUIComponent($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\System\UI\UIComponentType Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('UIComponentTypeAcceptsRelatedByContainerComponentTypeId' === $relationName) {
            $this->initUIComponentTypeAcceptssRelatedByContainerComponentTypeId();
            return;
        }
        if ('UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId' === $relationName) {
            $this->initUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId();
            return;
        }
        if ('UIComponent' === $relationName) {
            $this->initUIComponents();
            return;
        }
    }

    /**
     * Clears out the collUIComponentTypeAcceptssRelatedByContainerComponentTypeId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUIComponentTypeAcceptssRelatedByContainerComponentTypeId()
     */
    public function clearUIComponentTypeAcceptssRelatedByContainerComponentTypeId()
    {
        $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUIComponentTypeAcceptssRelatedByContainerComponentTypeId collection loaded partially.
     */
    public function resetPartialUIComponentTypeAcceptssRelatedByContainerComponentTypeId($v = true)
    {
        $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeIdPartial = $v;
    }

    /**
     * Initializes the collUIComponentTypeAcceptssRelatedByContainerComponentTypeId collection.
     *
     * By default this just sets the collUIComponentTypeAcceptssRelatedByContainerComponentTypeId collection to an empty array (like clearcollUIComponentTypeAcceptssRelatedByContainerComponentTypeId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUIComponentTypeAcceptssRelatedByContainerComponentTypeId($overrideExisting = true)
    {
        if (null !== $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId && !$overrideExisting) {
            return;
        }

        $collectionClassName = UIComponentTypeAcceptsTableMap::getTableMap()->getCollectionClassName();

        $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId = new $collectionClassName;
        $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId->setModel('\Model\System\UI\UIComponentTypeAccepts');
    }

    /**
     * Gets an array of ChildUIComponentTypeAccepts objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUIComponentType is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUIComponentTypeAccepts[] List of ChildUIComponentTypeAccepts objects
     * @throws PropelException
     */
    public function getUIComponentTypeAcceptssRelatedByContainerComponentTypeId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeIdPartial && !$this->isNew();
        if (null === $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId) {
                    $this->initUIComponentTypeAcceptssRelatedByContainerComponentTypeId();
                } else {
                    $collectionClassName = UIComponentTypeAcceptsTableMap::getTableMap()->getCollectionClassName();

                    $collUIComponentTypeAcceptssRelatedByContainerComponentTypeId = new $collectionClassName;
                    $collUIComponentTypeAcceptssRelatedByContainerComponentTypeId->setModel('\Model\System\UI\UIComponentTypeAccepts');

                    return $collUIComponentTypeAcceptssRelatedByContainerComponentTypeId;
                }
            } else {
                $collUIComponentTypeAcceptssRelatedByContainerComponentTypeId = ChildUIComponentTypeAcceptsQuery::create(null, $criteria)
                    ->filterByContainerComponentType($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeIdPartial && count($collUIComponentTypeAcceptssRelatedByContainerComponentTypeId)) {
                        $this->initUIComponentTypeAcceptssRelatedByContainerComponentTypeId(false);

                        foreach ($collUIComponentTypeAcceptssRelatedByContainerComponentTypeId as $obj) {
                            if (false == $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId->contains($obj)) {
                                $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId->append($obj);
                            }
                        }

                        $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeIdPartial = true;
                    }

                    return $collUIComponentTypeAcceptssRelatedByContainerComponentTypeId;
                }

                if ($partial && $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId) {
                    foreach ($this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId as $obj) {
                        if ($obj->isNew()) {
                            $collUIComponentTypeAcceptssRelatedByContainerComponentTypeId[] = $obj;
                        }
                    }
                }

                $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId = $collUIComponentTypeAcceptssRelatedByContainerComponentTypeId;
                $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeIdPartial = false;
            }
        }

        return $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId;
    }

    /**
     * Sets a collection of ChildUIComponentTypeAccepts objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $uIComponentTypeAcceptssRelatedByContainerComponentTypeId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUIComponentType The current object (for fluent API support)
     */
    public function setUIComponentTypeAcceptssRelatedByContainerComponentTypeId(Collection $uIComponentTypeAcceptssRelatedByContainerComponentTypeId, ConnectionInterface $con = null)
    {
        /** @var ChildUIComponentTypeAccepts[] $uIComponentTypeAcceptssRelatedByContainerComponentTypeIdToDelete */
        $uIComponentTypeAcceptssRelatedByContainerComponentTypeIdToDelete = $this->getUIComponentTypeAcceptssRelatedByContainerComponentTypeId(new Criteria(), $con)->diff($uIComponentTypeAcceptssRelatedByContainerComponentTypeId);


        $this->uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion = $uIComponentTypeAcceptssRelatedByContainerComponentTypeIdToDelete;

        foreach ($uIComponentTypeAcceptssRelatedByContainerComponentTypeIdToDelete as $uIComponentTypeAcceptsRelatedByContainerComponentTypeIdRemoved) {
            $uIComponentTypeAcceptsRelatedByContainerComponentTypeIdRemoved->setContainerComponentType(null);
        }

        $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId = null;
        foreach ($uIComponentTypeAcceptssRelatedByContainerComponentTypeId as $uIComponentTypeAcceptsRelatedByContainerComponentTypeId) {
            $this->addUIComponentTypeAcceptsRelatedByContainerComponentTypeId($uIComponentTypeAcceptsRelatedByContainerComponentTypeId);
        }

        $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId = $uIComponentTypeAcceptssRelatedByContainerComponentTypeId;
        $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UIComponentTypeAccepts objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UIComponentTypeAccepts objects.
     * @throws PropelException
     */
    public function countUIComponentTypeAcceptssRelatedByContainerComponentTypeId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeIdPartial && !$this->isNew();
        if (null === $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUIComponentTypeAcceptssRelatedByContainerComponentTypeId());
            }

            $query = ChildUIComponentTypeAcceptsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByContainerComponentType($this)
                ->count($con);
        }

        return count($this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId);
    }

    /**
     * Method called to associate a ChildUIComponentTypeAccepts object to this object
     * through the ChildUIComponentTypeAccepts foreign key attribute.
     *
     * @param  ChildUIComponentTypeAccepts $l ChildUIComponentTypeAccepts
     * @return $this|\Model\System\UI\UIComponentType The current object (for fluent API support)
     */
    public function addUIComponentTypeAcceptsRelatedByContainerComponentTypeId(ChildUIComponentTypeAccepts $l)
    {
        if ($this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId === null) {
            $this->initUIComponentTypeAcceptssRelatedByContainerComponentTypeId();
            $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeIdPartial = true;
        }

        if (!$this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId->contains($l)) {
            $this->doAddUIComponentTypeAcceptsRelatedByContainerComponentTypeId($l);

            if ($this->uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion and $this->uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion->contains($l)) {
                $this->uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion->remove($this->uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUIComponentTypeAccepts $uIComponentTypeAcceptsRelatedByContainerComponentTypeId The ChildUIComponentTypeAccepts object to add.
     */
    protected function doAddUIComponentTypeAcceptsRelatedByContainerComponentTypeId(ChildUIComponentTypeAccepts $uIComponentTypeAcceptsRelatedByContainerComponentTypeId)
    {
        $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId[]= $uIComponentTypeAcceptsRelatedByContainerComponentTypeId;
        $uIComponentTypeAcceptsRelatedByContainerComponentTypeId->setContainerComponentType($this);
    }

    /**
     * @param  ChildUIComponentTypeAccepts $uIComponentTypeAcceptsRelatedByContainerComponentTypeId The ChildUIComponentTypeAccepts object to remove.
     * @return $this|ChildUIComponentType The current object (for fluent API support)
     */
    public function removeUIComponentTypeAcceptsRelatedByContainerComponentTypeId(ChildUIComponentTypeAccepts $uIComponentTypeAcceptsRelatedByContainerComponentTypeId)
    {
        if ($this->getUIComponentTypeAcceptssRelatedByContainerComponentTypeId()->contains($uIComponentTypeAcceptsRelatedByContainerComponentTypeId)) {
            $pos = $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId->search($uIComponentTypeAcceptsRelatedByContainerComponentTypeId);
            $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId->remove($pos);
            if (null === $this->uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion) {
                $this->uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion = clone $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId;
                $this->uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion->clear();
            }
            $this->uIComponentTypeAcceptssRelatedByContainerComponentTypeIdScheduledForDeletion[]= $uIComponentTypeAcceptsRelatedByContainerComponentTypeId;
            $uIComponentTypeAcceptsRelatedByContainerComponentTypeId->setContainerComponentType(null);
        }

        return $this;
    }

    /**
     * Clears out the collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId()
     */
    public function clearUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId()
    {
        $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId collection loaded partially.
     */
    public function resetPartialUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId($v = true)
    {
        $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdPartial = $v;
    }

    /**
     * Initializes the collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId collection.
     *
     * By default this just sets the collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId collection to an empty array (like clearcollUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId($overrideExisting = true)
    {
        if (null !== $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId && !$overrideExisting) {
            return;
        }

        $collectionClassName = UIComponentTypeAcceptsTableMap::getTableMap()->getCollectionClassName();

        $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId = new $collectionClassName;
        $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId->setModel('\Model\System\UI\UIComponentTypeAccepts');
    }

    /**
     * Gets an array of ChildUIComponentTypeAccepts objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUIComponentType is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUIComponentTypeAccepts[] List of ChildUIComponentTypeAccepts objects
     * @throws PropelException
     */
    public function getUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdPartial && !$this->isNew();
        if (null === $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId) {
                    $this->initUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId();
                } else {
                    $collectionClassName = UIComponentTypeAcceptsTableMap::getTableMap()->getCollectionClassName();

                    $collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId = new $collectionClassName;
                    $collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId->setModel('\Model\System\UI\UIComponentTypeAccepts');

                    return $collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId;
                }
            } else {
                $collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId = ChildUIComponentTypeAcceptsQuery::create(null, $criteria)
                    ->filterByChildComponentType($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdPartial && count($collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId)) {
                        $this->initUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId(false);

                        foreach ($collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId as $obj) {
                            if (false == $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId->contains($obj)) {
                                $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId->append($obj);
                            }
                        }

                        $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdPartial = true;
                    }

                    return $collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId;
                }

                if ($partial && $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId) {
                    foreach ($this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId as $obj) {
                        if ($obj->isNew()) {
                            $collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId[] = $obj;
                        }
                    }
                }

                $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId = $collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId;
                $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdPartial = false;
            }
        }

        return $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId;
    }

    /**
     * Sets a collection of ChildUIComponentTypeAccepts objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $uIComponentTypeAcceptssRelatedByAcceptsComponentTypeId A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUIComponentType The current object (for fluent API support)
     */
    public function setUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId(Collection $uIComponentTypeAcceptssRelatedByAcceptsComponentTypeId, ConnectionInterface $con = null)
    {
        /** @var ChildUIComponentTypeAccepts[] $uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdToDelete */
        $uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdToDelete = $this->getUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId(new Criteria(), $con)->diff($uIComponentTypeAcceptssRelatedByAcceptsComponentTypeId);


        $this->uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion = $uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdToDelete;

        foreach ($uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdToDelete as $uIComponentTypeAcceptsRelatedByAcceptsComponentTypeIdRemoved) {
            $uIComponentTypeAcceptsRelatedByAcceptsComponentTypeIdRemoved->setChildComponentType(null);
        }

        $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId = null;
        foreach ($uIComponentTypeAcceptssRelatedByAcceptsComponentTypeId as $uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId) {
            $this->addUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId($uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId);
        }

        $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId = $uIComponentTypeAcceptssRelatedByAcceptsComponentTypeId;
        $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UIComponentTypeAccepts objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UIComponentTypeAccepts objects.
     * @throws PropelException
     */
    public function countUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdPartial && !$this->isNew();
        if (null === $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId());
            }

            $query = ChildUIComponentTypeAcceptsQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByChildComponentType($this)
                ->count($con);
        }

        return count($this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId);
    }

    /**
     * Method called to associate a ChildUIComponentTypeAccepts object to this object
     * through the ChildUIComponentTypeAccepts foreign key attribute.
     *
     * @param  ChildUIComponentTypeAccepts $l ChildUIComponentTypeAccepts
     * @return $this|\Model\System\UI\UIComponentType The current object (for fluent API support)
     */
    public function addUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId(ChildUIComponentTypeAccepts $l)
    {
        if ($this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId === null) {
            $this->initUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId();
            $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdPartial = true;
        }

        if (!$this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId->contains($l)) {
            $this->doAddUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId($l);

            if ($this->uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion and $this->uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion->contains($l)) {
                $this->uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion->remove($this->uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUIComponentTypeAccepts $uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId The ChildUIComponentTypeAccepts object to add.
     */
    protected function doAddUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId(ChildUIComponentTypeAccepts $uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId)
    {
        $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId[]= $uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId;
        $uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId->setChildComponentType($this);
    }

    /**
     * @param  ChildUIComponentTypeAccepts $uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId The ChildUIComponentTypeAccepts object to remove.
     * @return $this|ChildUIComponentType The current object (for fluent API support)
     */
    public function removeUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId(ChildUIComponentTypeAccepts $uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId)
    {
        if ($this->getUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId()->contains($uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId)) {
            $pos = $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId->search($uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId);
            $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId->remove($pos);
            if (null === $this->uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion) {
                $this->uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion = clone $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId;
                $this->uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion->clear();
            }
            $this->uIComponentTypeAcceptssRelatedByAcceptsComponentTypeIdScheduledForDeletion[]= $uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId;
            $uIComponentTypeAcceptsRelatedByAcceptsComponentTypeId->setChildComponentType(null);
        }

        return $this;
    }

    /**
     * Clears out the collUIComponents collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addUIComponents()
     */
    public function clearUIComponents()
    {
        $this->collUIComponents = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collUIComponents collection loaded partially.
     */
    public function resetPartialUIComponents($v = true)
    {
        $this->collUIComponentsPartial = $v;
    }

    /**
     * Initializes the collUIComponents collection.
     *
     * By default this just sets the collUIComponents collection to an empty array (like clearcollUIComponents());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initUIComponents($overrideExisting = true)
    {
        if (null !== $this->collUIComponents && !$overrideExisting) {
            return;
        }

        $collectionClassName = UIComponentTableMap::getTableMap()->getCollectionClassName();

        $this->collUIComponents = new $collectionClassName;
        $this->collUIComponents->setModel('\Model\System\UI\UIComponent');
    }

    /**
     * Gets an array of ChildUIComponent objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildUIComponentType is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildUIComponent[] List of ChildUIComponent objects
     * @throws PropelException
     */
    public function getUIComponents(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collUIComponentsPartial && !$this->isNew();
        if (null === $this->collUIComponents || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collUIComponents) {
                    $this->initUIComponents();
                } else {
                    $collectionClassName = UIComponentTableMap::getTableMap()->getCollectionClassName();

                    $collUIComponents = new $collectionClassName;
                    $collUIComponents->setModel('\Model\System\UI\UIComponent');

                    return $collUIComponents;
                }
            } else {
                $collUIComponents = ChildUIComponentQuery::create(null, $criteria)
                    ->filterByUIComponentType($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collUIComponentsPartial && count($collUIComponents)) {
                        $this->initUIComponents(false);

                        foreach ($collUIComponents as $obj) {
                            if (false == $this->collUIComponents->contains($obj)) {
                                $this->collUIComponents->append($obj);
                            }
                        }

                        $this->collUIComponentsPartial = true;
                    }

                    return $collUIComponents;
                }

                if ($partial && $this->collUIComponents) {
                    foreach ($this->collUIComponents as $obj) {
                        if ($obj->isNew()) {
                            $collUIComponents[] = $obj;
                        }
                    }
                }

                $this->collUIComponents = $collUIComponents;
                $this->collUIComponentsPartial = false;
            }
        }

        return $this->collUIComponents;
    }

    /**
     * Sets a collection of ChildUIComponent objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $uIComponents A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildUIComponentType The current object (for fluent API support)
     */
    public function setUIComponents(Collection $uIComponents, ConnectionInterface $con = null)
    {
        /** @var ChildUIComponent[] $uIComponentsToDelete */
        $uIComponentsToDelete = $this->getUIComponents(new Criteria(), $con)->diff($uIComponents);


        $this->uIComponentsScheduledForDeletion = $uIComponentsToDelete;

        foreach ($uIComponentsToDelete as $uIComponentRemoved) {
            $uIComponentRemoved->setUIComponentType(null);
        }

        $this->collUIComponents = null;
        foreach ($uIComponents as $uIComponent) {
            $this->addUIComponent($uIComponent);
        }

        $this->collUIComponents = $uIComponents;
        $this->collUIComponentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related UIComponent objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related UIComponent objects.
     * @throws PropelException
     */
    public function countUIComponents(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collUIComponentsPartial && !$this->isNew();
        if (null === $this->collUIComponents || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collUIComponents) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getUIComponents());
            }

            $query = ChildUIComponentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUIComponentType($this)
                ->count($con);
        }

        return count($this->collUIComponents);
    }

    /**
     * Method called to associate a ChildUIComponent object to this object
     * through the ChildUIComponent foreign key attribute.
     *
     * @param  ChildUIComponent $l ChildUIComponent
     * @return $this|\Model\System\UI\UIComponentType The current object (for fluent API support)
     */
    public function addUIComponent(ChildUIComponent $l)
    {
        if ($this->collUIComponents === null) {
            $this->initUIComponents();
            $this->collUIComponentsPartial = true;
        }

        if (!$this->collUIComponents->contains($l)) {
            $this->doAddUIComponent($l);

            if ($this->uIComponentsScheduledForDeletion and $this->uIComponentsScheduledForDeletion->contains($l)) {
                $this->uIComponentsScheduledForDeletion->remove($this->uIComponentsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildUIComponent $uIComponent The ChildUIComponent object to add.
     */
    protected function doAddUIComponent(ChildUIComponent $uIComponent)
    {
        $this->collUIComponents[]= $uIComponent;
        $uIComponent->setUIComponentType($this);
    }

    /**
     * @param  ChildUIComponent $uIComponent The ChildUIComponent object to remove.
     * @return $this|ChildUIComponentType The current object (for fluent API support)
     */
    public function removeUIComponent(ChildUIComponent $uIComponent)
    {
        if ($this->getUIComponents()->contains($uIComponent)) {
            $pos = $this->collUIComponents->search($uIComponent);
            $this->collUIComponents->remove($pos);
            if (null === $this->uIComponentsScheduledForDeletion) {
                $this->uIComponentsScheduledForDeletion = clone $this->collUIComponents;
                $this->uIComponentsScheduledForDeletion->clear();
            }
            $this->uIComponentsScheduledForDeletion[]= $uIComponent;
            $uIComponent->setUIComponentType(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UIComponentType is new, it will return
     * an empty collection; or if this UIComponentType has previously
     * been saved, it will retrieve related UIComponents from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UIComponentType.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUIComponent[] List of ChildUIComponent objects
     */
    public function getUIComponentsJoinUIComponentRelatedByParentId(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUIComponentQuery::create(null, $criteria);
        $query->joinWith('UIComponentRelatedByParentId', $joinBehavior);

        return $this->getUIComponents($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this UIComponentType is new, it will return
     * an empty collection; or if this UIComponentType has previously
     * been saved, it will retrieve related UIComponents from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in UIComponentType.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildUIComponent[] List of ChildUIComponent objects
     */
    public function getUIComponentsJoinApp(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildUIComponentQuery::create(null, $criteria);
        $query->joinWith('App', $joinBehavior);

        return $this->getUIComponents($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->title = null;
        $this->is_web = null;
        $this->is_app = null;
        $this->component_class = null;
        $this->accept_any = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId) {
                foreach ($this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId) {
                foreach ($this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collUIComponents) {
                foreach ($this->collUIComponents as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collUIComponentTypeAcceptssRelatedByContainerComponentTypeId = null;
        $this->collUIComponentTypeAcceptssRelatedByAcceptsComponentTypeId = null;
        $this->collUIComponents = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UIComponentTypeTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
