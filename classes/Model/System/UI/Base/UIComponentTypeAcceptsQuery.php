<?php

namespace Model\System\UI\Base;

use \Exception;
use \PDO;
use Model\System\UI\UIComponentTypeAccepts as ChildUIComponentTypeAccepts;
use Model\System\UI\UIComponentTypeAcceptsQuery as ChildUIComponentTypeAcceptsQuery;
use Model\System\UI\Map\UIComponentTypeAcceptsTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'ui_component_type_accepts' table.
 *
 *
 *
 * @method     ChildUIComponentTypeAcceptsQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUIComponentTypeAcceptsQuery orderByContainerComponentTypeId($order = Criteria::ASC) Order by the container_component_type_id column
 * @method     ChildUIComponentTypeAcceptsQuery orderByAcceptsComponentTypeId($order = Criteria::ASC) Order by the accepts_component_type_id column
 *
 * @method     ChildUIComponentTypeAcceptsQuery groupById() Group by the id column
 * @method     ChildUIComponentTypeAcceptsQuery groupByContainerComponentTypeId() Group by the container_component_type_id column
 * @method     ChildUIComponentTypeAcceptsQuery groupByAcceptsComponentTypeId() Group by the accepts_component_type_id column
 *
 * @method     ChildUIComponentTypeAcceptsQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUIComponentTypeAcceptsQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUIComponentTypeAcceptsQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUIComponentTypeAcceptsQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUIComponentTypeAcceptsQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUIComponentTypeAcceptsQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUIComponentTypeAcceptsQuery leftJoinContainerComponentType($relationAlias = null) Adds a LEFT JOIN clause to the query using the ContainerComponentType relation
 * @method     ChildUIComponentTypeAcceptsQuery rightJoinContainerComponentType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ContainerComponentType relation
 * @method     ChildUIComponentTypeAcceptsQuery innerJoinContainerComponentType($relationAlias = null) Adds a INNER JOIN clause to the query using the ContainerComponentType relation
 *
 * @method     ChildUIComponentTypeAcceptsQuery joinWithContainerComponentType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ContainerComponentType relation
 *
 * @method     ChildUIComponentTypeAcceptsQuery leftJoinWithContainerComponentType() Adds a LEFT JOIN clause and with to the query using the ContainerComponentType relation
 * @method     ChildUIComponentTypeAcceptsQuery rightJoinWithContainerComponentType() Adds a RIGHT JOIN clause and with to the query using the ContainerComponentType relation
 * @method     ChildUIComponentTypeAcceptsQuery innerJoinWithContainerComponentType() Adds a INNER JOIN clause and with to the query using the ContainerComponentType relation
 *
 * @method     ChildUIComponentTypeAcceptsQuery leftJoinChildComponentType($relationAlias = null) Adds a LEFT JOIN clause to the query using the ChildComponentType relation
 * @method     ChildUIComponentTypeAcceptsQuery rightJoinChildComponentType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ChildComponentType relation
 * @method     ChildUIComponentTypeAcceptsQuery innerJoinChildComponentType($relationAlias = null) Adds a INNER JOIN clause to the query using the ChildComponentType relation
 *
 * @method     ChildUIComponentTypeAcceptsQuery joinWithChildComponentType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ChildComponentType relation
 *
 * @method     ChildUIComponentTypeAcceptsQuery leftJoinWithChildComponentType() Adds a LEFT JOIN clause and with to the query using the ChildComponentType relation
 * @method     ChildUIComponentTypeAcceptsQuery rightJoinWithChildComponentType() Adds a RIGHT JOIN clause and with to the query using the ChildComponentType relation
 * @method     ChildUIComponentTypeAcceptsQuery innerJoinWithChildComponentType() Adds a INNER JOIN clause and with to the query using the ChildComponentType relation
 *
 * @method     \Model\System\UI\UIComponentTypeQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUIComponentTypeAccepts findOne(ConnectionInterface $con = null) Return the first ChildUIComponentTypeAccepts matching the query
 * @method     ChildUIComponentTypeAccepts findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUIComponentTypeAccepts matching the query, or a new ChildUIComponentTypeAccepts object populated from the query conditions when no match is found
 *
 * @method     ChildUIComponentTypeAccepts findOneById(int $id) Return the first ChildUIComponentTypeAccepts filtered by the id column
 * @method     ChildUIComponentTypeAccepts findOneByContainerComponentTypeId(int $container_component_type_id) Return the first ChildUIComponentTypeAccepts filtered by the container_component_type_id column
 * @method     ChildUIComponentTypeAccepts findOneByAcceptsComponentTypeId(int $accepts_component_type_id) Return the first ChildUIComponentTypeAccepts filtered by the accepts_component_type_id column *

 * @method     ChildUIComponentTypeAccepts requirePk($key, ConnectionInterface $con = null) Return the ChildUIComponentTypeAccepts by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponentTypeAccepts requireOne(ConnectionInterface $con = null) Return the first ChildUIComponentTypeAccepts matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUIComponentTypeAccepts requireOneById(int $id) Return the first ChildUIComponentTypeAccepts filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponentTypeAccepts requireOneByContainerComponentTypeId(int $container_component_type_id) Return the first ChildUIComponentTypeAccepts filtered by the container_component_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponentTypeAccepts requireOneByAcceptsComponentTypeId(int $accepts_component_type_id) Return the first ChildUIComponentTypeAccepts filtered by the accepts_component_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUIComponentTypeAccepts[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUIComponentTypeAccepts objects based on current ModelCriteria
 * @method     ChildUIComponentTypeAccepts[]|ObjectCollection findById(int $id) Return ChildUIComponentTypeAccepts objects filtered by the id column
 * @method     ChildUIComponentTypeAccepts[]|ObjectCollection findByContainerComponentTypeId(int $container_component_type_id) Return ChildUIComponentTypeAccepts objects filtered by the container_component_type_id column
 * @method     ChildUIComponentTypeAccepts[]|ObjectCollection findByAcceptsComponentTypeId(int $accepts_component_type_id) Return ChildUIComponentTypeAccepts objects filtered by the accepts_component_type_id column
 * @method     ChildUIComponentTypeAccepts[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UIComponentTypeAcceptsQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\UI\Base\UIComponentTypeAcceptsQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\UI\\UIComponentTypeAccepts', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUIComponentTypeAcceptsQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUIComponentTypeAcceptsQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUIComponentTypeAcceptsQuery) {
            return $criteria;
        }
        $query = new ChildUIComponentTypeAcceptsQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUIComponentTypeAccepts|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UIComponentTypeAcceptsTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UIComponentTypeAcceptsTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUIComponentTypeAccepts A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, container_component_type_id, accepts_component_type_id FROM ui_component_type_accepts WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUIComponentTypeAccepts $obj */
            $obj = new ChildUIComponentTypeAccepts();
            $obj->hydrate($row);
            UIComponentTypeAcceptsTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUIComponentTypeAccepts|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUIComponentTypeAcceptsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUIComponentTypeAcceptsQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentTypeAcceptsQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the container_component_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByContainerComponentTypeId(1234); // WHERE container_component_type_id = 1234
     * $query->filterByContainerComponentTypeId(array(12, 34)); // WHERE container_component_type_id IN (12, 34)
     * $query->filterByContainerComponentTypeId(array('min' => 12)); // WHERE container_component_type_id > 12
     * </code>
     *
     * @see       filterByContainerComponentType()
     *
     * @param     mixed $containerComponentTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentTypeAcceptsQuery The current query, for fluid interface
     */
    public function filterByContainerComponentTypeId($containerComponentTypeId = null, $comparison = null)
    {
        if (is_array($containerComponentTypeId)) {
            $useMinMax = false;
            if (isset($containerComponentTypeId['min'])) {
                $this->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_CONTAINER_COMPONENT_TYPE_ID, $containerComponentTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($containerComponentTypeId['max'])) {
                $this->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_CONTAINER_COMPONENT_TYPE_ID, $containerComponentTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_CONTAINER_COMPONENT_TYPE_ID, $containerComponentTypeId, $comparison);
    }

    /**
     * Filter the query on the accepts_component_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAcceptsComponentTypeId(1234); // WHERE accepts_component_type_id = 1234
     * $query->filterByAcceptsComponentTypeId(array(12, 34)); // WHERE accepts_component_type_id IN (12, 34)
     * $query->filterByAcceptsComponentTypeId(array('min' => 12)); // WHERE accepts_component_type_id > 12
     * </code>
     *
     * @see       filterByChildComponentType()
     *
     * @param     mixed $acceptsComponentTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentTypeAcceptsQuery The current query, for fluid interface
     */
    public function filterByAcceptsComponentTypeId($acceptsComponentTypeId = null, $comparison = null)
    {
        if (is_array($acceptsComponentTypeId)) {
            $useMinMax = false;
            if (isset($acceptsComponentTypeId['min'])) {
                $this->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_ACCEPTS_COMPONENT_TYPE_ID, $acceptsComponentTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($acceptsComponentTypeId['max'])) {
                $this->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_ACCEPTS_COMPONENT_TYPE_ID, $acceptsComponentTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_ACCEPTS_COMPONENT_TYPE_ID, $acceptsComponentTypeId, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\UI\UIComponentType object
     *
     * @param \Model\System\UI\UIComponentType|ObjectCollection $uIComponentType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUIComponentTypeAcceptsQuery The current query, for fluid interface
     */
    public function filterByContainerComponentType($uIComponentType, $comparison = null)
    {
        if ($uIComponentType instanceof \Model\System\UI\UIComponentType) {
            return $this
                ->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_CONTAINER_COMPONENT_TYPE_ID, $uIComponentType->getId(), $comparison);
        } elseif ($uIComponentType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_CONTAINER_COMPONENT_TYPE_ID, $uIComponentType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByContainerComponentType() only accepts arguments of type \Model\System\UI\UIComponentType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ContainerComponentType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUIComponentTypeAcceptsQuery The current query, for fluid interface
     */
    public function joinContainerComponentType($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ContainerComponentType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ContainerComponentType');
        }

        return $this;
    }

    /**
     * Use the ContainerComponentType relation UIComponentType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\UI\UIComponentTypeQuery A secondary query class using the current class as primary query
     */
    public function useContainerComponentTypeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinContainerComponentType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ContainerComponentType', '\Model\System\UI\UIComponentTypeQuery');
    }

    /**
     * Filter the query by a related \Model\System\UI\UIComponentType object
     *
     * @param \Model\System\UI\UIComponentType|ObjectCollection $uIComponentType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUIComponentTypeAcceptsQuery The current query, for fluid interface
     */
    public function filterByChildComponentType($uIComponentType, $comparison = null)
    {
        if ($uIComponentType instanceof \Model\System\UI\UIComponentType) {
            return $this
                ->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_ACCEPTS_COMPONENT_TYPE_ID, $uIComponentType->getId(), $comparison);
        } elseif ($uIComponentType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_ACCEPTS_COMPONENT_TYPE_ID, $uIComponentType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByChildComponentType() only accepts arguments of type \Model\System\UI\UIComponentType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ChildComponentType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUIComponentTypeAcceptsQuery The current query, for fluid interface
     */
    public function joinChildComponentType($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ChildComponentType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ChildComponentType');
        }

        return $this;
    }

    /**
     * Use the ChildComponentType relation UIComponentType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\UI\UIComponentTypeQuery A secondary query class using the current class as primary query
     */
    public function useChildComponentTypeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinChildComponentType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ChildComponentType', '\Model\System\UI\UIComponentTypeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUIComponentTypeAccepts $uIComponentTypeAccepts Object to remove from the list of results
     *
     * @return $this|ChildUIComponentTypeAcceptsQuery The current query, for fluid interface
     */
    public function prune($uIComponentTypeAccepts = null)
    {
        if ($uIComponentTypeAccepts) {
            $this->addUsingAlias(UIComponentTypeAcceptsTableMap::COL_ID, $uIComponentTypeAccepts->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the ui_component_type_accepts table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UIComponentTypeAcceptsTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UIComponentTypeAcceptsTableMap::clearInstancePool();
            UIComponentTypeAcceptsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UIComponentTypeAcceptsTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UIComponentTypeAcceptsTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UIComponentTypeAcceptsTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UIComponentTypeAcceptsTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UIComponentTypeAcceptsQuery
