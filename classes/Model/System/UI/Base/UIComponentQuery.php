<?php

namespace Model\System\UI\Base;

use \Exception;
use \PDO;
use Model\System\App;
use Model\System\UI\UIComponent as ChildUIComponent;
use Model\System\UI\UIComponentQuery as ChildUIComponentQuery;
use Model\System\UI\Map\UIComponentTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'ui_component' table.
 *
 *
 *
 * @method     ChildUIComponentQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUIComponentQuery orderByAppId($order = Criteria::ASC) Order by the app_id column
 * @method     ChildUIComponentQuery orderByParentId($order = Criteria::ASC) Order by the parent_id column
 * @method     ChildUIComponentQuery orderByParentLocation($order = Criteria::ASC) Order by the parent_location column
 * @method     ChildUIComponentQuery orderByUiComponentTypeId($order = Criteria::ASC) Order by the ui_component_type_id column
 * @method     ChildUIComponentQuery orderByLabel($order = Criteria::ASC) Order by the label column
 *
 * @method     ChildUIComponentQuery groupById() Group by the id column
 * @method     ChildUIComponentQuery groupByAppId() Group by the app_id column
 * @method     ChildUIComponentQuery groupByParentId() Group by the parent_id column
 * @method     ChildUIComponentQuery groupByParentLocation() Group by the parent_location column
 * @method     ChildUIComponentQuery groupByUiComponentTypeId() Group by the ui_component_type_id column
 * @method     ChildUIComponentQuery groupByLabel() Group by the label column
 *
 * @method     ChildUIComponentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUIComponentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUIComponentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUIComponentQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUIComponentQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUIComponentQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUIComponentQuery leftJoinUIComponentRelatedByParentId($relationAlias = null) Adds a LEFT JOIN clause to the query using the UIComponentRelatedByParentId relation
 * @method     ChildUIComponentQuery rightJoinUIComponentRelatedByParentId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UIComponentRelatedByParentId relation
 * @method     ChildUIComponentQuery innerJoinUIComponentRelatedByParentId($relationAlias = null) Adds a INNER JOIN clause to the query using the UIComponentRelatedByParentId relation
 *
 * @method     ChildUIComponentQuery joinWithUIComponentRelatedByParentId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UIComponentRelatedByParentId relation
 *
 * @method     ChildUIComponentQuery leftJoinWithUIComponentRelatedByParentId() Adds a LEFT JOIN clause and with to the query using the UIComponentRelatedByParentId relation
 * @method     ChildUIComponentQuery rightJoinWithUIComponentRelatedByParentId() Adds a RIGHT JOIN clause and with to the query using the UIComponentRelatedByParentId relation
 * @method     ChildUIComponentQuery innerJoinWithUIComponentRelatedByParentId() Adds a INNER JOIN clause and with to the query using the UIComponentRelatedByParentId relation
 *
 * @method     ChildUIComponentQuery leftJoinApp($relationAlias = null) Adds a LEFT JOIN clause to the query using the App relation
 * @method     ChildUIComponentQuery rightJoinApp($relationAlias = null) Adds a RIGHT JOIN clause to the query using the App relation
 * @method     ChildUIComponentQuery innerJoinApp($relationAlias = null) Adds a INNER JOIN clause to the query using the App relation
 *
 * @method     ChildUIComponentQuery joinWithApp($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the App relation
 *
 * @method     ChildUIComponentQuery leftJoinWithApp() Adds a LEFT JOIN clause and with to the query using the App relation
 * @method     ChildUIComponentQuery rightJoinWithApp() Adds a RIGHT JOIN clause and with to the query using the App relation
 * @method     ChildUIComponentQuery innerJoinWithApp() Adds a INNER JOIN clause and with to the query using the App relation
 *
 * @method     ChildUIComponentQuery leftJoinUIComponentType($relationAlias = null) Adds a LEFT JOIN clause to the query using the UIComponentType relation
 * @method     ChildUIComponentQuery rightJoinUIComponentType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UIComponentType relation
 * @method     ChildUIComponentQuery innerJoinUIComponentType($relationAlias = null) Adds a INNER JOIN clause to the query using the UIComponentType relation
 *
 * @method     ChildUIComponentQuery joinWithUIComponentType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UIComponentType relation
 *
 * @method     ChildUIComponentQuery leftJoinWithUIComponentType() Adds a LEFT JOIN clause and with to the query using the UIComponentType relation
 * @method     ChildUIComponentQuery rightJoinWithUIComponentType() Adds a RIGHT JOIN clause and with to the query using the UIComponentType relation
 * @method     ChildUIComponentQuery innerJoinWithUIComponentType() Adds a INNER JOIN clause and with to the query using the UIComponentType relation
 *
 * @method     ChildUIComponentQuery leftJoinUIComponentRelatedById($relationAlias = null) Adds a LEFT JOIN clause to the query using the UIComponentRelatedById relation
 * @method     ChildUIComponentQuery rightJoinUIComponentRelatedById($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UIComponentRelatedById relation
 * @method     ChildUIComponentQuery innerJoinUIComponentRelatedById($relationAlias = null) Adds a INNER JOIN clause to the query using the UIComponentRelatedById relation
 *
 * @method     ChildUIComponentQuery joinWithUIComponentRelatedById($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UIComponentRelatedById relation
 *
 * @method     ChildUIComponentQuery leftJoinWithUIComponentRelatedById() Adds a LEFT JOIN clause and with to the query using the UIComponentRelatedById relation
 * @method     ChildUIComponentQuery rightJoinWithUIComponentRelatedById() Adds a RIGHT JOIN clause and with to the query using the UIComponentRelatedById relation
 * @method     ChildUIComponentQuery innerJoinWithUIComponentRelatedById() Adds a INNER JOIN clause and with to the query using the UIComponentRelatedById relation
 *
 * @method     \Model\System\UI\UIComponentQuery|\Model\System\AppQuery|\Model\System\UI\UIComponentTypeQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUIComponent findOne(ConnectionInterface $con = null) Return the first ChildUIComponent matching the query
 * @method     ChildUIComponent findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUIComponent matching the query, or a new ChildUIComponent object populated from the query conditions when no match is found
 *
 * @method     ChildUIComponent findOneById(int $id) Return the first ChildUIComponent filtered by the id column
 * @method     ChildUIComponent findOneByAppId(int $app_id) Return the first ChildUIComponent filtered by the app_id column
 * @method     ChildUIComponent findOneByParentId(int $parent_id) Return the first ChildUIComponent filtered by the parent_id column
 * @method     ChildUIComponent findOneByParentLocation(string $parent_location) Return the first ChildUIComponent filtered by the parent_location column
 * @method     ChildUIComponent findOneByUiComponentTypeId(int $ui_component_type_id) Return the first ChildUIComponent filtered by the ui_component_type_id column
 * @method     ChildUIComponent findOneByLabel(string $label) Return the first ChildUIComponent filtered by the label column *

 * @method     ChildUIComponent requirePk($key, ConnectionInterface $con = null) Return the ChildUIComponent by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponent requireOne(ConnectionInterface $con = null) Return the first ChildUIComponent matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUIComponent requireOneById(int $id) Return the first ChildUIComponent filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponent requireOneByAppId(int $app_id) Return the first ChildUIComponent filtered by the app_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponent requireOneByParentId(int $parent_id) Return the first ChildUIComponent filtered by the parent_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponent requireOneByParentLocation(string $parent_location) Return the first ChildUIComponent filtered by the parent_location column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponent requireOneByUiComponentTypeId(int $ui_component_type_id) Return the first ChildUIComponent filtered by the ui_component_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponent requireOneByLabel(string $label) Return the first ChildUIComponent filtered by the label column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUIComponent[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUIComponent objects based on current ModelCriteria
 * @method     ChildUIComponent[]|ObjectCollection findById(int $id) Return ChildUIComponent objects filtered by the id column
 * @method     ChildUIComponent[]|ObjectCollection findByAppId(int $app_id) Return ChildUIComponent objects filtered by the app_id column
 * @method     ChildUIComponent[]|ObjectCollection findByParentId(int $parent_id) Return ChildUIComponent objects filtered by the parent_id column
 * @method     ChildUIComponent[]|ObjectCollection findByParentLocation(string $parent_location) Return ChildUIComponent objects filtered by the parent_location column
 * @method     ChildUIComponent[]|ObjectCollection findByUiComponentTypeId(int $ui_component_type_id) Return ChildUIComponent objects filtered by the ui_component_type_id column
 * @method     ChildUIComponent[]|ObjectCollection findByLabel(string $label) Return ChildUIComponent objects filtered by the label column
 * @method     ChildUIComponent[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UIComponentQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\UI\Base\UIComponentQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\UI\\UIComponent', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUIComponentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUIComponentQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUIComponentQuery) {
            return $criteria;
        }
        $query = new ChildUIComponentQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUIComponent|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UIComponentTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UIComponentTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUIComponent A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, app_id, parent_id, parent_location, ui_component_type_id, label FROM ui_component WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUIComponent $obj */
            $obj = new ChildUIComponent();
            $obj->hydrate($row);
            UIComponentTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUIComponent|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUIComponentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UIComponentTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUIComponentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UIComponentTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UIComponentTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UIComponentTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UIComponentTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the app_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAppId(1234); // WHERE app_id = 1234
     * $query->filterByAppId(array(12, 34)); // WHERE app_id IN (12, 34)
     * $query->filterByAppId(array('min' => 12)); // WHERE app_id > 12
     * </code>
     *
     * @see       filterByApp()
     *
     * @param     mixed $appId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentQuery The current query, for fluid interface
     */
    public function filterByAppId($appId = null, $comparison = null)
    {
        if (is_array($appId)) {
            $useMinMax = false;
            if (isset($appId['min'])) {
                $this->addUsingAlias(UIComponentTableMap::COL_APP_ID, $appId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($appId['max'])) {
                $this->addUsingAlias(UIComponentTableMap::COL_APP_ID, $appId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UIComponentTableMap::COL_APP_ID, $appId, $comparison);
    }

    /**
     * Filter the query on the parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE parent_id > 12
     * </code>
     *
     * @see       filterByUIComponentRelatedByParentId()
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(UIComponentTableMap::COL_PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(UIComponentTableMap::COL_PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UIComponentTableMap::COL_PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the parent_location column
     *
     * Example usage:
     * <code>
     * $query->filterByParentLocation('fooValue');   // WHERE parent_location = 'fooValue'
     * $query->filterByParentLocation('%fooValue%', Criteria::LIKE); // WHERE parent_location LIKE '%fooValue%'
     * </code>
     *
     * @param     string $parentLocation The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentQuery The current query, for fluid interface
     */
    public function filterByParentLocation($parentLocation = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($parentLocation)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UIComponentTableMap::COL_PARENT_LOCATION, $parentLocation, $comparison);
    }

    /**
     * Filter the query on the ui_component_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUiComponentTypeId(1234); // WHERE ui_component_type_id = 1234
     * $query->filterByUiComponentTypeId(array(12, 34)); // WHERE ui_component_type_id IN (12, 34)
     * $query->filterByUiComponentTypeId(array('min' => 12)); // WHERE ui_component_type_id > 12
     * </code>
     *
     * @see       filterByUIComponentType()
     *
     * @param     mixed $uiComponentTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentQuery The current query, for fluid interface
     */
    public function filterByUiComponentTypeId($uiComponentTypeId = null, $comparison = null)
    {
        if (is_array($uiComponentTypeId)) {
            $useMinMax = false;
            if (isset($uiComponentTypeId['min'])) {
                $this->addUsingAlias(UIComponentTableMap::COL_UI_COMPONENT_TYPE_ID, $uiComponentTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uiComponentTypeId['max'])) {
                $this->addUsingAlias(UIComponentTableMap::COL_UI_COMPONENT_TYPE_ID, $uiComponentTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UIComponentTableMap::COL_UI_COMPONENT_TYPE_ID, $uiComponentTypeId, $comparison);
    }

    /**
     * Filter the query on the label column
     *
     * Example usage:
     * <code>
     * $query->filterByLabel('fooValue');   // WHERE label = 'fooValue'
     * $query->filterByLabel('%fooValue%', Criteria::LIKE); // WHERE label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $label The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentQuery The current query, for fluid interface
     */
    public function filterByLabel($label = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($label)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UIComponentTableMap::COL_LABEL, $label, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\UI\UIComponent object
     *
     * @param \Model\System\UI\UIComponent|ObjectCollection $uIComponent The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUIComponentQuery The current query, for fluid interface
     */
    public function filterByUIComponentRelatedByParentId($uIComponent, $comparison = null)
    {
        if ($uIComponent instanceof \Model\System\UI\UIComponent) {
            return $this
                ->addUsingAlias(UIComponentTableMap::COL_PARENT_ID, $uIComponent->getId(), $comparison);
        } elseif ($uIComponent instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UIComponentTableMap::COL_PARENT_ID, $uIComponent->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUIComponentRelatedByParentId() only accepts arguments of type \Model\System\UI\UIComponent or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UIComponentRelatedByParentId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUIComponentQuery The current query, for fluid interface
     */
    public function joinUIComponentRelatedByParentId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UIComponentRelatedByParentId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UIComponentRelatedByParentId');
        }

        return $this;
    }

    /**
     * Use the UIComponentRelatedByParentId relation UIComponent object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\UI\UIComponentQuery A secondary query class using the current class as primary query
     */
    public function useUIComponentRelatedByParentIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUIComponentRelatedByParentId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UIComponentRelatedByParentId', '\Model\System\UI\UIComponentQuery');
    }

    /**
     * Filter the query by a related \Model\System\App object
     *
     * @param \Model\System\App|ObjectCollection $app The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUIComponentQuery The current query, for fluid interface
     */
    public function filterByApp($app, $comparison = null)
    {
        if ($app instanceof \Model\System\App) {
            return $this
                ->addUsingAlias(UIComponentTableMap::COL_APP_ID, $app->getId(), $comparison);
        } elseif ($app instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UIComponentTableMap::COL_APP_ID, $app->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByApp() only accepts arguments of type \Model\System\App or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the App relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUIComponentQuery The current query, for fluid interface
     */
    public function joinApp($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('App');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'App');
        }

        return $this;
    }

    /**
     * Use the App relation App object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\AppQuery A secondary query class using the current class as primary query
     */
    public function useAppQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinApp($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'App', '\Model\System\AppQuery');
    }

    /**
     * Filter the query by a related \Model\System\UI\UIComponentType object
     *
     * @param \Model\System\UI\UIComponentType|ObjectCollection $uIComponentType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUIComponentQuery The current query, for fluid interface
     */
    public function filterByUIComponentType($uIComponentType, $comparison = null)
    {
        if ($uIComponentType instanceof \Model\System\UI\UIComponentType) {
            return $this
                ->addUsingAlias(UIComponentTableMap::COL_UI_COMPONENT_TYPE_ID, $uIComponentType->getId(), $comparison);
        } elseif ($uIComponentType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(UIComponentTableMap::COL_UI_COMPONENT_TYPE_ID, $uIComponentType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUIComponentType() only accepts arguments of type \Model\System\UI\UIComponentType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UIComponentType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUIComponentQuery The current query, for fluid interface
     */
    public function joinUIComponentType($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UIComponentType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UIComponentType');
        }

        return $this;
    }

    /**
     * Use the UIComponentType relation UIComponentType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\UI\UIComponentTypeQuery A secondary query class using the current class as primary query
     */
    public function useUIComponentTypeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUIComponentType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UIComponentType', '\Model\System\UI\UIComponentTypeQuery');
    }

    /**
     * Filter the query by a related \Model\System\UI\UIComponent object
     *
     * @param \Model\System\UI\UIComponent|ObjectCollection $uIComponent the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUIComponentQuery The current query, for fluid interface
     */
    public function filterByUIComponentRelatedById($uIComponent, $comparison = null)
    {
        if ($uIComponent instanceof \Model\System\UI\UIComponent) {
            return $this
                ->addUsingAlias(UIComponentTableMap::COL_ID, $uIComponent->getParentId(), $comparison);
        } elseif ($uIComponent instanceof ObjectCollection) {
            return $this
                ->useUIComponentRelatedByIdQuery()
                ->filterByPrimaryKeys($uIComponent->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUIComponentRelatedById() only accepts arguments of type \Model\System\UI\UIComponent or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UIComponentRelatedById relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUIComponentQuery The current query, for fluid interface
     */
    public function joinUIComponentRelatedById($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UIComponentRelatedById');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UIComponentRelatedById');
        }

        return $this;
    }

    /**
     * Use the UIComponentRelatedById relation UIComponent object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\UI\UIComponentQuery A secondary query class using the current class as primary query
     */
    public function useUIComponentRelatedByIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUIComponentRelatedById($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UIComponentRelatedById', '\Model\System\UI\UIComponentQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUIComponent $uIComponent Object to remove from the list of results
     *
     * @return $this|ChildUIComponentQuery The current query, for fluid interface
     */
    public function prune($uIComponent = null)
    {
        if ($uIComponent) {
            $this->addUsingAlias(UIComponentTableMap::COL_ID, $uIComponent->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the ui_component table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UIComponentTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UIComponentTableMap::clearInstancePool();
            UIComponentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UIComponentTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UIComponentTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UIComponentTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UIComponentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UIComponentQuery
