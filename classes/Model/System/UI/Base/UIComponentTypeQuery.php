<?php

namespace Model\System\UI\Base;

use \Exception;
use \PDO;
use Model\System\UI\UIComponentType as ChildUIComponentType;
use Model\System\UI\UIComponentTypeQuery as ChildUIComponentTypeQuery;
use Model\System\UI\Map\UIComponentTypeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'ui_component_type' table.
 *
 *
 *
 * @method     ChildUIComponentTypeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildUIComponentTypeQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildUIComponentTypeQuery orderByIsWeb($order = Criteria::ASC) Order by the is_web column
 * @method     ChildUIComponentTypeQuery orderByIsApp($order = Criteria::ASC) Order by the is_app column
 * @method     ChildUIComponentTypeQuery orderByComponentClass($order = Criteria::ASC) Order by the component_class column
 * @method     ChildUIComponentTypeQuery orderByAcceptAny($order = Criteria::ASC) Order by the accept_any column
 *
 * @method     ChildUIComponentTypeQuery groupById() Group by the id column
 * @method     ChildUIComponentTypeQuery groupByTitle() Group by the title column
 * @method     ChildUIComponentTypeQuery groupByIsWeb() Group by the is_web column
 * @method     ChildUIComponentTypeQuery groupByIsApp() Group by the is_app column
 * @method     ChildUIComponentTypeQuery groupByComponentClass() Group by the component_class column
 * @method     ChildUIComponentTypeQuery groupByAcceptAny() Group by the accept_any column
 *
 * @method     ChildUIComponentTypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildUIComponentTypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildUIComponentTypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildUIComponentTypeQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildUIComponentTypeQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildUIComponentTypeQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildUIComponentTypeQuery leftJoinUIComponentTypeAcceptsRelatedByContainerComponentTypeId($relationAlias = null) Adds a LEFT JOIN clause to the query using the UIComponentTypeAcceptsRelatedByContainerComponentTypeId relation
 * @method     ChildUIComponentTypeQuery rightJoinUIComponentTypeAcceptsRelatedByContainerComponentTypeId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UIComponentTypeAcceptsRelatedByContainerComponentTypeId relation
 * @method     ChildUIComponentTypeQuery innerJoinUIComponentTypeAcceptsRelatedByContainerComponentTypeId($relationAlias = null) Adds a INNER JOIN clause to the query using the UIComponentTypeAcceptsRelatedByContainerComponentTypeId relation
 *
 * @method     ChildUIComponentTypeQuery joinWithUIComponentTypeAcceptsRelatedByContainerComponentTypeId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UIComponentTypeAcceptsRelatedByContainerComponentTypeId relation
 *
 * @method     ChildUIComponentTypeQuery leftJoinWithUIComponentTypeAcceptsRelatedByContainerComponentTypeId() Adds a LEFT JOIN clause and with to the query using the UIComponentTypeAcceptsRelatedByContainerComponentTypeId relation
 * @method     ChildUIComponentTypeQuery rightJoinWithUIComponentTypeAcceptsRelatedByContainerComponentTypeId() Adds a RIGHT JOIN clause and with to the query using the UIComponentTypeAcceptsRelatedByContainerComponentTypeId relation
 * @method     ChildUIComponentTypeQuery innerJoinWithUIComponentTypeAcceptsRelatedByContainerComponentTypeId() Adds a INNER JOIN clause and with to the query using the UIComponentTypeAcceptsRelatedByContainerComponentTypeId relation
 *
 * @method     ChildUIComponentTypeQuery leftJoinUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId($relationAlias = null) Adds a LEFT JOIN clause to the query using the UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId relation
 * @method     ChildUIComponentTypeQuery rightJoinUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId relation
 * @method     ChildUIComponentTypeQuery innerJoinUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId($relationAlias = null) Adds a INNER JOIN clause to the query using the UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId relation
 *
 * @method     ChildUIComponentTypeQuery joinWithUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId relation
 *
 * @method     ChildUIComponentTypeQuery leftJoinWithUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId() Adds a LEFT JOIN clause and with to the query using the UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId relation
 * @method     ChildUIComponentTypeQuery rightJoinWithUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId() Adds a RIGHT JOIN clause and with to the query using the UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId relation
 * @method     ChildUIComponentTypeQuery innerJoinWithUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId() Adds a INNER JOIN clause and with to the query using the UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId relation
 *
 * @method     ChildUIComponentTypeQuery leftJoinUIComponent($relationAlias = null) Adds a LEFT JOIN clause to the query using the UIComponent relation
 * @method     ChildUIComponentTypeQuery rightJoinUIComponent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UIComponent relation
 * @method     ChildUIComponentTypeQuery innerJoinUIComponent($relationAlias = null) Adds a INNER JOIN clause to the query using the UIComponent relation
 *
 * @method     ChildUIComponentTypeQuery joinWithUIComponent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UIComponent relation
 *
 * @method     ChildUIComponentTypeQuery leftJoinWithUIComponent() Adds a LEFT JOIN clause and with to the query using the UIComponent relation
 * @method     ChildUIComponentTypeQuery rightJoinWithUIComponent() Adds a RIGHT JOIN clause and with to the query using the UIComponent relation
 * @method     ChildUIComponentTypeQuery innerJoinWithUIComponent() Adds a INNER JOIN clause and with to the query using the UIComponent relation
 *
 * @method     \Model\System\UI\UIComponentTypeAcceptsQuery|\Model\System\UI\UIComponentQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildUIComponentType findOne(ConnectionInterface $con = null) Return the first ChildUIComponentType matching the query
 * @method     ChildUIComponentType findOneOrCreate(ConnectionInterface $con = null) Return the first ChildUIComponentType matching the query, or a new ChildUIComponentType object populated from the query conditions when no match is found
 *
 * @method     ChildUIComponentType findOneById(int $id) Return the first ChildUIComponentType filtered by the id column
 * @method     ChildUIComponentType findOneByTitle(string $title) Return the first ChildUIComponentType filtered by the title column
 * @method     ChildUIComponentType findOneByIsWeb(boolean $is_web) Return the first ChildUIComponentType filtered by the is_web column
 * @method     ChildUIComponentType findOneByIsApp(boolean $is_app) Return the first ChildUIComponentType filtered by the is_app column
 * @method     ChildUIComponentType findOneByComponentClass(string $component_class) Return the first ChildUIComponentType filtered by the component_class column
 * @method     ChildUIComponentType findOneByAcceptAny(boolean $accept_any) Return the first ChildUIComponentType filtered by the accept_any column *

 * @method     ChildUIComponentType requirePk($key, ConnectionInterface $con = null) Return the ChildUIComponentType by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponentType requireOne(ConnectionInterface $con = null) Return the first ChildUIComponentType matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUIComponentType requireOneById(int $id) Return the first ChildUIComponentType filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponentType requireOneByTitle(string $title) Return the first ChildUIComponentType filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponentType requireOneByIsWeb(boolean $is_web) Return the first ChildUIComponentType filtered by the is_web column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponentType requireOneByIsApp(boolean $is_app) Return the first ChildUIComponentType filtered by the is_app column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponentType requireOneByComponentClass(string $component_class) Return the first ChildUIComponentType filtered by the component_class column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildUIComponentType requireOneByAcceptAny(boolean $accept_any) Return the first ChildUIComponentType filtered by the accept_any column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildUIComponentType[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildUIComponentType objects based on current ModelCriteria
 * @method     ChildUIComponentType[]|ObjectCollection findById(int $id) Return ChildUIComponentType objects filtered by the id column
 * @method     ChildUIComponentType[]|ObjectCollection findByTitle(string $title) Return ChildUIComponentType objects filtered by the title column
 * @method     ChildUIComponentType[]|ObjectCollection findByIsWeb(boolean $is_web) Return ChildUIComponentType objects filtered by the is_web column
 * @method     ChildUIComponentType[]|ObjectCollection findByIsApp(boolean $is_app) Return ChildUIComponentType objects filtered by the is_app column
 * @method     ChildUIComponentType[]|ObjectCollection findByComponentClass(string $component_class) Return ChildUIComponentType objects filtered by the component_class column
 * @method     ChildUIComponentType[]|ObjectCollection findByAcceptAny(boolean $accept_any) Return ChildUIComponentType objects filtered by the accept_any column
 * @method     ChildUIComponentType[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class UIComponentTypeQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\UI\Base\UIComponentTypeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\UI\\UIComponentType', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildUIComponentTypeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildUIComponentTypeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildUIComponentTypeQuery) {
            return $criteria;
        }
        $query = new ChildUIComponentTypeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildUIComponentType|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(UIComponentTypeTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = UIComponentTypeTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildUIComponentType A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, title, is_web, is_app, component_class, accept_any FROM ui_component_type WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildUIComponentType $obj */
            $obj = new ChildUIComponentType();
            $obj->hydrate($row);
            UIComponentTypeTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildUIComponentType|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UIComponentTypeTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UIComponentTypeTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UIComponentTypeTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UIComponentTypeTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UIComponentTypeTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UIComponentTypeTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the is_web column
     *
     * Example usage:
     * <code>
     * $query->filterByIsWeb(true); // WHERE is_web = true
     * $query->filterByIsWeb('yes'); // WHERE is_web = true
     * </code>
     *
     * @param     boolean|string $isWeb The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function filterByIsWeb($isWeb = null, $comparison = null)
    {
        if (is_string($isWeb)) {
            $isWeb = in_array(strtolower($isWeb), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UIComponentTypeTableMap::COL_IS_WEB, $isWeb, $comparison);
    }

    /**
     * Filter the query on the is_app column
     *
     * Example usage:
     * <code>
     * $query->filterByIsApp(true); // WHERE is_app = true
     * $query->filterByIsApp('yes'); // WHERE is_app = true
     * </code>
     *
     * @param     boolean|string $isApp The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function filterByIsApp($isApp = null, $comparison = null)
    {
        if (is_string($isApp)) {
            $isApp = in_array(strtolower($isApp), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UIComponentTypeTableMap::COL_IS_APP, $isApp, $comparison);
    }

    /**
     * Filter the query on the component_class column
     *
     * Example usage:
     * <code>
     * $query->filterByComponentClass('fooValue');   // WHERE component_class = 'fooValue'
     * $query->filterByComponentClass('%fooValue%', Criteria::LIKE); // WHERE component_class LIKE '%fooValue%'
     * </code>
     *
     * @param     string $componentClass The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function filterByComponentClass($componentClass = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($componentClass)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UIComponentTypeTableMap::COL_COMPONENT_CLASS, $componentClass, $comparison);
    }

    /**
     * Filter the query on the accept_any column
     *
     * Example usage:
     * <code>
     * $query->filterByAcceptAny(true); // WHERE accept_any = true
     * $query->filterByAcceptAny('yes'); // WHERE accept_any = true
     * </code>
     *
     * @param     boolean|string $acceptAny The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function filterByAcceptAny($acceptAny = null, $comparison = null)
    {
        if (is_string($acceptAny)) {
            $acceptAny = in_array(strtolower($acceptAny), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(UIComponentTypeTableMap::COL_ACCEPT_ANY, $acceptAny, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\UI\UIComponentTypeAccepts object
     *
     * @param \Model\System\UI\UIComponentTypeAccepts|ObjectCollection $uIComponentTypeAccepts the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function filterByUIComponentTypeAcceptsRelatedByContainerComponentTypeId($uIComponentTypeAccepts, $comparison = null)
    {
        if ($uIComponentTypeAccepts instanceof \Model\System\UI\UIComponentTypeAccepts) {
            return $this
                ->addUsingAlias(UIComponentTypeTableMap::COL_ID, $uIComponentTypeAccepts->getContainerComponentTypeId(), $comparison);
        } elseif ($uIComponentTypeAccepts instanceof ObjectCollection) {
            return $this
                ->useUIComponentTypeAcceptsRelatedByContainerComponentTypeIdQuery()
                ->filterByPrimaryKeys($uIComponentTypeAccepts->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUIComponentTypeAcceptsRelatedByContainerComponentTypeId() only accepts arguments of type \Model\System\UI\UIComponentTypeAccepts or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UIComponentTypeAcceptsRelatedByContainerComponentTypeId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function joinUIComponentTypeAcceptsRelatedByContainerComponentTypeId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UIComponentTypeAcceptsRelatedByContainerComponentTypeId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UIComponentTypeAcceptsRelatedByContainerComponentTypeId');
        }

        return $this;
    }

    /**
     * Use the UIComponentTypeAcceptsRelatedByContainerComponentTypeId relation UIComponentTypeAccepts object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\UI\UIComponentTypeAcceptsQuery A secondary query class using the current class as primary query
     */
    public function useUIComponentTypeAcceptsRelatedByContainerComponentTypeIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUIComponentTypeAcceptsRelatedByContainerComponentTypeId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UIComponentTypeAcceptsRelatedByContainerComponentTypeId', '\Model\System\UI\UIComponentTypeAcceptsQuery');
    }

    /**
     * Filter the query by a related \Model\System\UI\UIComponentTypeAccepts object
     *
     * @param \Model\System\UI\UIComponentTypeAccepts|ObjectCollection $uIComponentTypeAccepts the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function filterByUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId($uIComponentTypeAccepts, $comparison = null)
    {
        if ($uIComponentTypeAccepts instanceof \Model\System\UI\UIComponentTypeAccepts) {
            return $this
                ->addUsingAlias(UIComponentTypeTableMap::COL_ID, $uIComponentTypeAccepts->getAcceptsComponentTypeId(), $comparison);
        } elseif ($uIComponentTypeAccepts instanceof ObjectCollection) {
            return $this
                ->useUIComponentTypeAcceptsRelatedByAcceptsComponentTypeIdQuery()
                ->filterByPrimaryKeys($uIComponentTypeAccepts->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId() only accepts arguments of type \Model\System\UI\UIComponentTypeAccepts or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function joinUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId');
        }

        return $this;
    }

    /**
     * Use the UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId relation UIComponentTypeAccepts object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\UI\UIComponentTypeAcceptsQuery A secondary query class using the current class as primary query
     */
    public function useUIComponentTypeAcceptsRelatedByAcceptsComponentTypeIdQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUIComponentTypeAcceptsRelatedByAcceptsComponentTypeId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId', '\Model\System\UI\UIComponentTypeAcceptsQuery');
    }

    /**
     * Filter the query by a related \Model\System\UI\UIComponent object
     *
     * @param \Model\System\UI\UIComponent|ObjectCollection $uIComponent the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function filterByUIComponent($uIComponent, $comparison = null)
    {
        if ($uIComponent instanceof \Model\System\UI\UIComponent) {
            return $this
                ->addUsingAlias(UIComponentTypeTableMap::COL_ID, $uIComponent->getUiComponentTypeId(), $comparison);
        } elseif ($uIComponent instanceof ObjectCollection) {
            return $this
                ->useUIComponentQuery()
                ->filterByPrimaryKeys($uIComponent->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByUIComponent() only accepts arguments of type \Model\System\UI\UIComponent or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UIComponent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function joinUIComponent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UIComponent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UIComponent');
        }

        return $this;
    }

    /**
     * Use the UIComponent relation UIComponent object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\UI\UIComponentQuery A secondary query class using the current class as primary query
     */
    public function useUIComponentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUIComponent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UIComponent', '\Model\System\UI\UIComponentQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildUIComponentType $uIComponentType Object to remove from the list of results
     *
     * @return $this|ChildUIComponentTypeQuery The current query, for fluid interface
     */
    public function prune($uIComponentType = null)
    {
        if ($uIComponentType) {
            $this->addUsingAlias(UIComponentTypeTableMap::COL_ID, $uIComponentType->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the ui_component_type table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UIComponentTypeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            UIComponentTypeTableMap::clearInstancePool();
            UIComponentTypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UIComponentTypeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(UIComponentTypeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            UIComponentTypeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            UIComponentTypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // UIComponentTypeQuery
