<?php

namespace Model\System\UI;

use Model\System\UI\Base\UIComponentPropertyValuesQuery as BaseUIComponentPropertyValuesQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'ui_component_property_values' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UIComponentPropertyValuesQuery extends BaseUIComponentPropertyValuesQuery
{

}
