<?php

namespace Model\System\UI;

use Core\Reflector;
use Core\Utils;
use Crud\FormManager;
use Exception\LogicException;
use LowCode\ComponentFactory;
use Model\System\DataSourceQuery;
use Model\System\UI\Base\UIComponent as BaseUIComponent;
use Propel\Runtime\Map\TableMap;

/**
 * Skeleton subclass for representing a row from the 'ui_component' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UIComponent extends BaseUIComponent
{

    /**
     * @return array
     * @throws \Propel\Runtime\Exception\PropelException
     */
    function getForm():array
    {
        $sComponentFqn = $this->getUiComponentType()->getComponentClass();

        $oComponent = ComponentFactory::produce($sComponentFqn, [], []);
        $sComponentId = $oComponent->getComponentXml()->getId();

        $sManagerFqn = '\\Crud\\Component_' . $sComponentId . '\\CrudComponent_' . $sComponentId . 'Manager';
        $oManager = new $sManagerFqn;
        if(!$oManager instanceof FormManager)
        {
            throw new LogicException("Manager not found $sManagerFqn");
        }

        $aProperties = [
            "id" => $this->getId(),
            "ui_component_id" => $this->getId(),
            "type"=>  "\\LowCode\\Component\\IForm\\Component",
            "render_style" =>  "AllCp",
            "title"=>  $this->getUIComponentType()->getTitle() .  " configureren",
            "title_locked" => true,
            "icon"=>  "image",
            "layout_key"=>  "ui_component",
            "defaults" => [
                "layout_key" => "ui_component_" . $this->getId(),
                "component_key" => "ui_component_" . $this->getId(),
                "ui_component_id" => $this->getId(),
            ],
            "manager" => $sManagerFqn,

            "heading_components" =>  [
                [
                    "props" => [

                        "id" => "save_ui_component_" . $this->getId(),
                        "type" =>  "\\LowCode\\Component\\Button\\Component",
                        "title" =>   "Save",
                        "align" => "right",
                        "icon" => "floppy-o",
                        "click" =>  [
                            [
                                "id" => "save_component_" . $this->getId(),
                                "type" => "\\LowCode\\Event\\Store\\Event",
                                "datasource" => DataSourceQuery::create()->findOneByCode('EENO')->toArray(TableMap::TYPE_FIELDNAME),
                                "endpoint" => $this->getUIComponentType()->getComponent()->getApiManager()->getApiEndpoint()
                            ],
                            [
                                "id" => "z_reload_page_" . $this->getId(),
                                "type" => "\\LowCode\\Event\\Reload\\Event",
                                "location" =>   Utils::getRequestUri(),
                            ]
                        ],
                        "color_type" => "success"
                    ]
                ],
                [
                    "props" => [
                        "id" => "delete_ui_component_" . $this->getId(),
                        "type" =>  "\\LowCode\\Component\\Button\\Component",
                        "title" =>   "Remove",
                        "align" => "right",
                        "icon" => "trash",
                        "click" =>  [
                            [
                                "id" => "delete_component_" . $this->getId(),
                                "type" => "\\LowCode\\Event\\Delete\\Event",
                                "datasource" => DataSourceQuery::create()->findOneByCode('EENO')->toArray(TableMap::TYPE_FIELDNAME),
                                "endpoint" => "/v2/rest/uicomponent/" . $this->getId(),
                            ],
                            [
                                "id" => "reload_page_" . $this->getId(),
                                "type" => "\\LowCode\\Event\\Reload\\Event",
                                "location" =>   Utils::getRequestUri(),
                            ]
                        ],
                        "color_type" => "danger"
                    ]
                ],
                [
                    "props" => [
                        "id" => "redirect_edit_form_" . $this->getId(),
                        "type" =>  "\\LowCode\\Component\\Button\\Component",
                        "title" =>   "IForm",
                        "align" => "right",
                        "icon" => "cogs",
                        "click" =>  [
                            [
                                "id" => "redirect_to_form_editor_" . $this->getId(),
                                "type" => "\\LowCode\\Event\\Redirect\\Event",
                                "location" =>   (string) $oManager->getFormEditUrl("ui_component", $this->getUIComponentType()->getTitle()),
                            ]
                        ],
                        "color_type" => "system",
                    ]

                ]
            ],
            "panel_footer_components" => [
                [
                    "props" => [
                        "id" => "validate_button_" . $this->getId(),
                        "title" =>   "Validate",
                        "align" => "right",
                        "icon" => "floppy",
                        "click" =>  [[
                             "id" => "edit_form_" . $this->getId(),
                             "location" => $oManager->getFormEditUrl("ui_component", "Component configureren"),
                             "type" => "\\LowCode\\Event\\Validate\\Event",
                        ]],
                        "color_type" => "system",
                        "type" =>  "\\LowCode\\Component\\Button\\Component"
                    ]
                ],
                [
                    "props" => [
                        "id" => "store_button_" . $this->getId(),
                        "title" =>   "Save",
                        "align" => "right",
                        "icon" => "check",
                        "click" =>  [[
                             "id" => "save_" .  $this->getId(),
                             "type" => "\\LowCode\\Event\\Store\\Event",
                             "location" => $oManager->getFormEditUrl("ui_component", "Component configureren"),

                        ]],
                        "color_type" => "system",
                        "type" =>  "\\LowCode\\Component\\Button\\Component"
                    ]
                ]
            ]

        ];

        $aState = [];
        if($this->isConfigured())
        {
            $aState = $this->getUIComponentType()->getComponent()->getConfig($this->getId());
        }
        // $aState["ui_component_model_form"] = ['id' => $this->getId()];

        $oApp = new \LowCode\App($aProperties, $aState);

        return $oApp->render();

    }
    /**
     * Returns the configuration form if not configured, of configured returns the actual preview.
     */
    function preview():string
    {
        if($this->isConfigured())
        {
            return $this->previewRender()['result'];
        }
        return $this->getForm()['result'];
    }
    function isConfigured():bool
    {
        $aConfig = $this->getUIComponentType()->getComponent()->getConfig($this->getId());
        if($aConfig)
        {
            return true;
        }
        return false;
    }
    function previewRender()
    {
        $sType = $this->getUIComponentType()->getComponentClass();

        $aProperties = $this->getUIComponentType()->getComponent()->getConfig($this->getId());

        $aProperties['id'] = 'rendered_' . $aProperties['id'];
        $aProperties['ui_component_id'] = $this->getId();

        if(isset($aProperties['manager']))
        {
            $sManager = $aProperties['manager'];
            $oManager = new $sManager();

            $oQueryClass = $oManager->getQueryObject();
            $aProperties['query_object'] = (new Reflector($oQueryClass))->getName();
        }

        $aProperties['type'] = $sType;


        $aChildComponents = UIComponentQuery::create()->findByParentId($this->getId());
        foreach ($aChildComponents as $oChildComponent)
        {
            if(!isset($aProperties[$oChildComponent->getParentLocation()]))
            {
                $aProperties[$oChildComponent->getParentLocation()] = [];
            }
            $aProperties[$oChildComponent->getParentLocation()][] = $oChildComponent->previewRender();

        }


        $aState = [];
        $oApp = new \LowCode\App($aProperties, $aState);

        return $oApp->render();
    }



}
