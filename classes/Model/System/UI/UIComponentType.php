<?php

namespace Model\System\UI;

use Crud\IFormManager;
use LowCode\Component\IComponent;
use Model\System\UI\Base\UIComponentType as BaseUIComponentType;

/**
 * Skeleton subclass for representing a row from the 'ui_component_type' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UIComponentType extends BaseUIComponentType
{

    function getComponent(array $aProps = [], array $aState = []):IComponent
    {
        $sClassName = $this->getComponentClass();
        return new $sClassName($aProps, $aState);

    }

}
