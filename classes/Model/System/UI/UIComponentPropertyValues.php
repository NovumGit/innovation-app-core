<?php

namespace Model\System\UI;

use Model\System\UI\Base\UIComponentPropertyValues as BaseUIComponentPropertyValues;

/**
 * Skeleton subclass for representing a row from the 'ui_component_property_values' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class UIComponentPropertyValues extends BaseUIComponentPropertyValues
{

}
