<?php

namespace Model\System\UI\Map;

use Model\System\UI\UiComponentType;
use Model\System\UI\UiComponentTypeQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'ui_component_type' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class UiComponentTypeTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.System.UI.Map.UiComponentTypeTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'ui_component_type';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\System\\UI\\UiComponentType';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.System.UI.UiComponentType';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 6;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 6;

    /**
     * the column name for the id field
     */
    const COL_ID = 'ui_component_type.id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'ui_component_type.title';

    /**
     * the column name for the is_web field
     */
    const COL_IS_WEB = 'ui_component_type.is_web';

    /**
     * the column name for the is_app field
     */
    const COL_IS_APP = 'ui_component_type.is_app';

    /**
     * the column name for the component_class field
     */
    const COL_COMPONENT_CLASS = 'ui_component_type.component_class';

    /**
     * the column name for the accept_any field
     */
    const COL_ACCEPT_ANY = 'ui_component_type.accept_any';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Title', 'IsWeb', 'IsApp', 'ComponentClass', 'AcceptAny', ),
        self::TYPE_CAMELNAME     => array('id', 'title', 'isWeb', 'isApp', 'componentClass', 'acceptAny', ),
        self::TYPE_COLNAME       => array(UiComponentTypeTableMap::COL_ID, UiComponentTypeTableMap::COL_TITLE, UiComponentTypeTableMap::COL_IS_WEB, UiComponentTypeTableMap::COL_IS_APP, UiComponentTypeTableMap::COL_COMPONENT_CLASS, UiComponentTypeTableMap::COL_ACCEPT_ANY, ),
        self::TYPE_FIELDNAME     => array('id', 'title', 'is_web', 'is_app', 'component_class', 'accept_any', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Title' => 1, 'IsWeb' => 2, 'IsApp' => 3, 'ComponentClass' => 4, 'AcceptAny' => 5, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'title' => 1, 'isWeb' => 2, 'isApp' => 3, 'componentClass' => 4, 'acceptAny' => 5, ),
        self::TYPE_COLNAME       => array(UiComponentTypeTableMap::COL_ID => 0, UiComponentTypeTableMap::COL_TITLE => 1, UiComponentTypeTableMap::COL_IS_WEB => 2, UiComponentTypeTableMap::COL_IS_APP => 3, UiComponentTypeTableMap::COL_COMPONENT_CLASS => 4, UiComponentTypeTableMap::COL_ACCEPT_ANY => 5, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'title' => 1, 'is_web' => 2, 'is_app' => 3, 'component_class' => 4, 'accept_any' => 5, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ui_component_type');
        $this->setPhpName('UiComponentType');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\System\\UI\\UiComponentType');
        $this->setPackage('Model.System.UI');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 255, null);
        $this->addColumn('is_web', 'IsWeb', 'BOOLEAN', false, 1, null);
        $this->addColumn('is_app', 'IsApp', 'BOOLEAN', false, 1, null);
        $this->addColumn('component_class', 'ComponentClass', 'VARCHAR', false, 255, null);
        $this->addColumn('accept_any', 'AcceptAny', 'BOOLEAN', false, 1, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('UIComponentTypeAcceptsRelatedByContainerComponentTypeId', '\\Model\\System\\UI\\UIComponentTypeAccepts', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':container_component_type_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'UIComponentTypeAcceptssRelatedByContainerComponentTypeId', false);
        $this->addRelation('UIComponentTypeAcceptsRelatedByAcceptsComponentTypeId', '\\Model\\System\\UI\\UIComponentTypeAccepts', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':accepts_component_type_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'UIComponentTypeAcceptssRelatedByAcceptsComponentTypeId', false);
        $this->addRelation('UIComponent', '\\Model\\System\\UI\\UIComponent', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':ui_component_type_id',
    1 => ':id',
  ),
), 'RESTRICT', null, 'UIComponents', false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? UiComponentTypeTableMap::CLASS_DEFAULT : UiComponentTypeTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (UiComponentType object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = UiComponentTypeTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = UiComponentTypeTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + UiComponentTypeTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = UiComponentTypeTableMap::OM_CLASS;
            /** @var UiComponentType $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            UiComponentTypeTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = UiComponentTypeTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = UiComponentTypeTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var UiComponentType $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                UiComponentTypeTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(UiComponentTypeTableMap::COL_ID);
            $criteria->addSelectColumn(UiComponentTypeTableMap::COL_TITLE);
            $criteria->addSelectColumn(UiComponentTypeTableMap::COL_IS_WEB);
            $criteria->addSelectColumn(UiComponentTypeTableMap::COL_IS_APP);
            $criteria->addSelectColumn(UiComponentTypeTableMap::COL_COMPONENT_CLASS);
            $criteria->addSelectColumn(UiComponentTypeTableMap::COL_ACCEPT_ANY);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.is_web');
            $criteria->addSelectColumn($alias . '.is_app');
            $criteria->addSelectColumn($alias . '.component_class');
            $criteria->addSelectColumn($alias . '.accept_any');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(UiComponentTypeTableMap::DATABASE_NAME)->getTable(UiComponentTypeTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(UiComponentTypeTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(UiComponentTypeTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new UiComponentTypeTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a UiComponentType or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or UiComponentType object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UiComponentTypeTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\System\UI\UiComponentType) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(UiComponentTypeTableMap::DATABASE_NAME);
            $criteria->add(UiComponentTypeTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = UiComponentTypeQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            UiComponentTypeTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                UiComponentTypeTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the ui_component_type table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return UiComponentTypeQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a UiComponentType or Criteria object.
     *
     * @param mixed               $criteria Criteria or UiComponentType object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(UiComponentTypeTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from UiComponentType object
        }

        if ($criteria->containsKey(UiComponentTypeTableMap::COL_ID) && $criteria->keyContainsValue(UiComponentTypeTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.UiComponentTypeTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = UiComponentTypeQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // UiComponentTypeTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
UiComponentTypeTableMap::buildTableMap();
