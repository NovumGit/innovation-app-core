<?php

namespace Model\System\Event;

use Model\System\Event\Base\EventTrigger as BaseEventTrigger;

/**
 * Skeleton subclass for representing a row from the 'event_trigger' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class EventTrigger extends BaseEventTrigger
{

}
