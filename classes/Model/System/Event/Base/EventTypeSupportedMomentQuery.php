<?php

namespace Model\System\Event\Base;

use \Exception;
use \PDO;
use Model\System\Event\EventTypeSupportedMoment as ChildEventTypeSupportedMoment;
use Model\System\Event\EventTypeSupportedMomentQuery as ChildEventTypeSupportedMomentQuery;
use Model\System\Event\Map\EventTypeSupportedMomentTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'event_type_supported_trigger' table.
 *
 *
 *
 * @method     ChildEventTypeSupportedMomentQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildEventTypeSupportedMomentQuery orderByEventTypeId($order = Criteria::ASC) Order by the event_type_id column
 * @method     ChildEventTypeSupportedMomentQuery orderByEventTriggerId($order = Criteria::ASC) Order by the event_trigger_id column
 *
 * @method     ChildEventTypeSupportedMomentQuery groupById() Group by the id column
 * @method     ChildEventTypeSupportedMomentQuery groupByEventTypeId() Group by the event_type_id column
 * @method     ChildEventTypeSupportedMomentQuery groupByEventTriggerId() Group by the event_trigger_id column
 *
 * @method     ChildEventTypeSupportedMomentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEventTypeSupportedMomentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEventTypeSupportedMomentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEventTypeSupportedMomentQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEventTypeSupportedMomentQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEventTypeSupportedMomentQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEventTypeSupportedMomentQuery leftJoinEventTriggerTypeEventType($relationAlias = null) Adds a LEFT JOIN clause to the query using the EventTriggerTypeEventType relation
 * @method     ChildEventTypeSupportedMomentQuery rightJoinEventTriggerTypeEventType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EventTriggerTypeEventType relation
 * @method     ChildEventTypeSupportedMomentQuery innerJoinEventTriggerTypeEventType($relationAlias = null) Adds a INNER JOIN clause to the query using the EventTriggerTypeEventType relation
 *
 * @method     ChildEventTypeSupportedMomentQuery joinWithEventTriggerTypeEventType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EventTriggerTypeEventType relation
 *
 * @method     ChildEventTypeSupportedMomentQuery leftJoinWithEventTriggerTypeEventType() Adds a LEFT JOIN clause and with to the query using the EventTriggerTypeEventType relation
 * @method     ChildEventTypeSupportedMomentQuery rightJoinWithEventTriggerTypeEventType() Adds a RIGHT JOIN clause and with to the query using the EventTriggerTypeEventType relation
 * @method     ChildEventTypeSupportedMomentQuery innerJoinWithEventTriggerTypeEventType() Adds a INNER JOIN clause and with to the query using the EventTriggerTypeEventType relation
 *
 * @method     ChildEventTypeSupportedMomentQuery leftJoinEventTriggerTypeEvent($relationAlias = null) Adds a LEFT JOIN clause to the query using the EventTriggerTypeEvent relation
 * @method     ChildEventTypeSupportedMomentQuery rightJoinEventTriggerTypeEvent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EventTriggerTypeEvent relation
 * @method     ChildEventTypeSupportedMomentQuery innerJoinEventTriggerTypeEvent($relationAlias = null) Adds a INNER JOIN clause to the query using the EventTriggerTypeEvent relation
 *
 * @method     ChildEventTypeSupportedMomentQuery joinWithEventTriggerTypeEvent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EventTriggerTypeEvent relation
 *
 * @method     ChildEventTypeSupportedMomentQuery leftJoinWithEventTriggerTypeEvent() Adds a LEFT JOIN clause and with to the query using the EventTriggerTypeEvent relation
 * @method     ChildEventTypeSupportedMomentQuery rightJoinWithEventTriggerTypeEvent() Adds a RIGHT JOIN clause and with to the query using the EventTriggerTypeEvent relation
 * @method     ChildEventTypeSupportedMomentQuery innerJoinWithEventTriggerTypeEvent() Adds a INNER JOIN clause and with to the query using the EventTriggerTypeEvent relation
 *
 * @method     \Model\System\Event\EventTypeQuery|\Model\System\Event\EventTriggerQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEventTypeSupportedMoment findOne(ConnectionInterface $con = null) Return the first ChildEventTypeSupportedMoment matching the query
 * @method     ChildEventTypeSupportedMoment findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEventTypeSupportedMoment matching the query, or a new ChildEventTypeSupportedMoment object populated from the query conditions when no match is found
 *
 * @method     ChildEventTypeSupportedMoment findOneById(int $id) Return the first ChildEventTypeSupportedMoment filtered by the id column
 * @method     ChildEventTypeSupportedMoment findOneByEventTypeId(int $event_type_id) Return the first ChildEventTypeSupportedMoment filtered by the event_type_id column
 * @method     ChildEventTypeSupportedMoment findOneByEventTriggerId(int $event_trigger_id) Return the first ChildEventTypeSupportedMoment filtered by the event_trigger_id column *

 * @method     ChildEventTypeSupportedMoment requirePk($key, ConnectionInterface $con = null) Return the ChildEventTypeSupportedMoment by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEventTypeSupportedMoment requireOne(ConnectionInterface $con = null) Return the first ChildEventTypeSupportedMoment matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEventTypeSupportedMoment requireOneById(int $id) Return the first ChildEventTypeSupportedMoment filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEventTypeSupportedMoment requireOneByEventTypeId(int $event_type_id) Return the first ChildEventTypeSupportedMoment filtered by the event_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEventTypeSupportedMoment requireOneByEventTriggerId(int $event_trigger_id) Return the first ChildEventTypeSupportedMoment filtered by the event_trigger_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEventTypeSupportedMoment[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEventTypeSupportedMoment objects based on current ModelCriteria
 * @method     ChildEventTypeSupportedMoment[]|ObjectCollection findById(int $id) Return ChildEventTypeSupportedMoment objects filtered by the id column
 * @method     ChildEventTypeSupportedMoment[]|ObjectCollection findByEventTypeId(int $event_type_id) Return ChildEventTypeSupportedMoment objects filtered by the event_type_id column
 * @method     ChildEventTypeSupportedMoment[]|ObjectCollection findByEventTriggerId(int $event_trigger_id) Return ChildEventTypeSupportedMoment objects filtered by the event_trigger_id column
 * @method     ChildEventTypeSupportedMoment[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EventTypeSupportedMomentQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\Event\Base\EventTypeSupportedMomentQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\Event\\EventTypeSupportedMoment', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEventTypeSupportedMomentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEventTypeSupportedMomentQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEventTypeSupportedMomentQuery) {
            return $criteria;
        }
        $query = new ChildEventTypeSupportedMomentQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEventTypeSupportedMoment|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EventTypeSupportedMomentTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = EventTypeSupportedMomentTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEventTypeSupportedMoment A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, event_type_id, event_trigger_id FROM event_type_supported_trigger WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEventTypeSupportedMoment $obj */
            $obj = new ChildEventTypeSupportedMoment();
            $obj->hydrate($row);
            EventTypeSupportedMomentTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEventTypeSupportedMoment|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEventTypeSupportedMomentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EventTypeSupportedMomentTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEventTypeSupportedMomentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EventTypeSupportedMomentTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEventTypeSupportedMomentQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(EventTypeSupportedMomentTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(EventTypeSupportedMomentTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventTypeSupportedMomentTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the event_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEventTypeId(1234); // WHERE event_type_id = 1234
     * $query->filterByEventTypeId(array(12, 34)); // WHERE event_type_id IN (12, 34)
     * $query->filterByEventTypeId(array('min' => 12)); // WHERE event_type_id > 12
     * </code>
     *
     * @see       filterByEventTriggerTypeEventType()
     *
     * @param     mixed $eventTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEventTypeSupportedMomentQuery The current query, for fluid interface
     */
    public function filterByEventTypeId($eventTypeId = null, $comparison = null)
    {
        if (is_array($eventTypeId)) {
            $useMinMax = false;
            if (isset($eventTypeId['min'])) {
                $this->addUsingAlias(EventTypeSupportedMomentTableMap::COL_EVENT_TYPE_ID, $eventTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($eventTypeId['max'])) {
                $this->addUsingAlias(EventTypeSupportedMomentTableMap::COL_EVENT_TYPE_ID, $eventTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventTypeSupportedMomentTableMap::COL_EVENT_TYPE_ID, $eventTypeId, $comparison);
    }

    /**
     * Filter the query on the event_trigger_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEventTriggerId(1234); // WHERE event_trigger_id = 1234
     * $query->filterByEventTriggerId(array(12, 34)); // WHERE event_trigger_id IN (12, 34)
     * $query->filterByEventTriggerId(array('min' => 12)); // WHERE event_trigger_id > 12
     * </code>
     *
     * @see       filterByEventTriggerTypeEvent()
     *
     * @param     mixed $eventTriggerId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEventTypeSupportedMomentQuery The current query, for fluid interface
     */
    public function filterByEventTriggerId($eventTriggerId = null, $comparison = null)
    {
        if (is_array($eventTriggerId)) {
            $useMinMax = false;
            if (isset($eventTriggerId['min'])) {
                $this->addUsingAlias(EventTypeSupportedMomentTableMap::COL_EVENT_TRIGGER_ID, $eventTriggerId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($eventTriggerId['max'])) {
                $this->addUsingAlias(EventTypeSupportedMomentTableMap::COL_EVENT_TRIGGER_ID, $eventTriggerId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventTypeSupportedMomentTableMap::COL_EVENT_TRIGGER_ID, $eventTriggerId, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\Event\EventType object
     *
     * @param \Model\System\Event\EventType|ObjectCollection $eventType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEventTypeSupportedMomentQuery The current query, for fluid interface
     */
    public function filterByEventTriggerTypeEventType($eventType, $comparison = null)
    {
        if ($eventType instanceof \Model\System\Event\EventType) {
            return $this
                ->addUsingAlias(EventTypeSupportedMomentTableMap::COL_EVENT_TYPE_ID, $eventType->getId(), $comparison);
        } elseif ($eventType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EventTypeSupportedMomentTableMap::COL_EVENT_TYPE_ID, $eventType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByEventTriggerTypeEventType() only accepts arguments of type \Model\System\Event\EventType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EventTriggerTypeEventType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEventTypeSupportedMomentQuery The current query, for fluid interface
     */
    public function joinEventTriggerTypeEventType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EventTriggerTypeEventType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EventTriggerTypeEventType');
        }

        return $this;
    }

    /**
     * Use the EventTriggerTypeEventType relation EventType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\Event\EventTypeQuery A secondary query class using the current class as primary query
     */
    public function useEventTriggerTypeEventTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEventTriggerTypeEventType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EventTriggerTypeEventType', '\Model\System\Event\EventTypeQuery');
    }

    /**
     * Filter the query by a related \Model\System\Event\EventTrigger object
     *
     * @param \Model\System\Event\EventTrigger|ObjectCollection $eventTrigger The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEventTypeSupportedMomentQuery The current query, for fluid interface
     */
    public function filterByEventTriggerTypeEvent($eventTrigger, $comparison = null)
    {
        if ($eventTrigger instanceof \Model\System\Event\EventTrigger) {
            return $this
                ->addUsingAlias(EventTypeSupportedMomentTableMap::COL_EVENT_TRIGGER_ID, $eventTrigger->getId(), $comparison);
        } elseif ($eventTrigger instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EventTypeSupportedMomentTableMap::COL_EVENT_TRIGGER_ID, $eventTrigger->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByEventTriggerTypeEvent() only accepts arguments of type \Model\System\Event\EventTrigger or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EventTriggerTypeEvent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEventTypeSupportedMomentQuery The current query, for fluid interface
     */
    public function joinEventTriggerTypeEvent($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EventTriggerTypeEvent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EventTriggerTypeEvent');
        }

        return $this;
    }

    /**
     * Use the EventTriggerTypeEvent relation EventTrigger object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\Event\EventTriggerQuery A secondary query class using the current class as primary query
     */
    public function useEventTriggerTypeEventQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEventTriggerTypeEvent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EventTriggerTypeEvent', '\Model\System\Event\EventTriggerQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEventTypeSupportedMoment $eventTypeSupportedMoment Object to remove from the list of results
     *
     * @return $this|ChildEventTypeSupportedMomentQuery The current query, for fluid interface
     */
    public function prune($eventTypeSupportedMoment = null)
    {
        if ($eventTypeSupportedMoment) {
            $this->addUsingAlias(EventTypeSupportedMomentTableMap::COL_ID, $eventTypeSupportedMoment->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the event_type_supported_trigger table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EventTypeSupportedMomentTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EventTypeSupportedMomentTableMap::clearInstancePool();
            EventTypeSupportedMomentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EventTypeSupportedMomentTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EventTypeSupportedMomentTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EventTypeSupportedMomentTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EventTypeSupportedMomentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // EventTypeSupportedMomentQuery
