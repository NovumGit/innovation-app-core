<?php

namespace Model\System\Event\Base;

use \Exception;
use \PDO;
use Model\System\MimeType;
use Model\System\Event\EventType as ChildEventType;
use Model\System\Event\EventTypeQuery as ChildEventTypeQuery;
use Model\System\Event\Map\EventTypeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'event_type' table.
 *
 *
 *
 * @method     ChildEventTypeQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildEventTypeQuery orderByExecutable($order = Criteria::ASC) Order by the executable column
 * @method     ChildEventTypeQuery orderByIsGeneric($order = Criteria::ASC) Order by the is_generic column
 * @method     ChildEventTypeQuery orderByInputMimeId($order = Criteria::ASC) Order by the input_mime_id column
 * @method     ChildEventTypeQuery orderByOutputMimeId($order = Criteria::ASC) Order by the output_mime_id column
 *
 * @method     ChildEventTypeQuery groupById() Group by the id column
 * @method     ChildEventTypeQuery groupByExecutable() Group by the executable column
 * @method     ChildEventTypeQuery groupByIsGeneric() Group by the is_generic column
 * @method     ChildEventTypeQuery groupByInputMimeId() Group by the input_mime_id column
 * @method     ChildEventTypeQuery groupByOutputMimeId() Group by the output_mime_id column
 *
 * @method     ChildEventTypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEventTypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEventTypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEventTypeQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEventTypeQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEventTypeQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEventTypeQuery leftJoinEventInputMimeType($relationAlias = null) Adds a LEFT JOIN clause to the query using the EventInputMimeType relation
 * @method     ChildEventTypeQuery rightJoinEventInputMimeType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EventInputMimeType relation
 * @method     ChildEventTypeQuery innerJoinEventInputMimeType($relationAlias = null) Adds a INNER JOIN clause to the query using the EventInputMimeType relation
 *
 * @method     ChildEventTypeQuery joinWithEventInputMimeType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EventInputMimeType relation
 *
 * @method     ChildEventTypeQuery leftJoinWithEventInputMimeType() Adds a LEFT JOIN clause and with to the query using the EventInputMimeType relation
 * @method     ChildEventTypeQuery rightJoinWithEventInputMimeType() Adds a RIGHT JOIN clause and with to the query using the EventInputMimeType relation
 * @method     ChildEventTypeQuery innerJoinWithEventInputMimeType() Adds a INNER JOIN clause and with to the query using the EventInputMimeType relation
 *
 * @method     ChildEventTypeQuery leftJoinEventOutputMimeType($relationAlias = null) Adds a LEFT JOIN clause to the query using the EventOutputMimeType relation
 * @method     ChildEventTypeQuery rightJoinEventOutputMimeType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EventOutputMimeType relation
 * @method     ChildEventTypeQuery innerJoinEventOutputMimeType($relationAlias = null) Adds a INNER JOIN clause to the query using the EventOutputMimeType relation
 *
 * @method     ChildEventTypeQuery joinWithEventOutputMimeType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EventOutputMimeType relation
 *
 * @method     ChildEventTypeQuery leftJoinWithEventOutputMimeType() Adds a LEFT JOIN clause and with to the query using the EventOutputMimeType relation
 * @method     ChildEventTypeQuery rightJoinWithEventOutputMimeType() Adds a RIGHT JOIN clause and with to the query using the EventOutputMimeType relation
 * @method     ChildEventTypeQuery innerJoinWithEventOutputMimeType() Adds a INNER JOIN clause and with to the query using the EventOutputMimeType relation
 *
 * @method     ChildEventTypeQuery leftJoinEventTypeSupportedMoment($relationAlias = null) Adds a LEFT JOIN clause to the query using the EventTypeSupportedMoment relation
 * @method     ChildEventTypeQuery rightJoinEventTypeSupportedMoment($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EventTypeSupportedMoment relation
 * @method     ChildEventTypeQuery innerJoinEventTypeSupportedMoment($relationAlias = null) Adds a INNER JOIN clause to the query using the EventTypeSupportedMoment relation
 *
 * @method     ChildEventTypeQuery joinWithEventTypeSupportedMoment($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EventTypeSupportedMoment relation
 *
 * @method     ChildEventTypeQuery leftJoinWithEventTypeSupportedMoment() Adds a LEFT JOIN clause and with to the query using the EventTypeSupportedMoment relation
 * @method     ChildEventTypeQuery rightJoinWithEventTypeSupportedMoment() Adds a RIGHT JOIN clause and with to the query using the EventTypeSupportedMoment relation
 * @method     ChildEventTypeQuery innerJoinWithEventTypeSupportedMoment() Adds a INNER JOIN clause and with to the query using the EventTypeSupportedMoment relation
 *
 * @method     ChildEventTypeQuery leftJoinEventHandler($relationAlias = null) Adds a LEFT JOIN clause to the query using the EventHandler relation
 * @method     ChildEventTypeQuery rightJoinEventHandler($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EventHandler relation
 * @method     ChildEventTypeQuery innerJoinEventHandler($relationAlias = null) Adds a INNER JOIN clause to the query using the EventHandler relation
 *
 * @method     ChildEventTypeQuery joinWithEventHandler($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EventHandler relation
 *
 * @method     ChildEventTypeQuery leftJoinWithEventHandler() Adds a LEFT JOIN clause and with to the query using the EventHandler relation
 * @method     ChildEventTypeQuery rightJoinWithEventHandler() Adds a RIGHT JOIN clause and with to the query using the EventHandler relation
 * @method     ChildEventTypeQuery innerJoinWithEventHandler() Adds a INNER JOIN clause and with to the query using the EventHandler relation
 *
 * @method     \Model\System\MimeTypeQuery|\Model\System\Event\EventTypeSupportedMomentQuery|\Model\System\Event\EventHandlerQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEventType findOne(ConnectionInterface $con = null) Return the first ChildEventType matching the query
 * @method     ChildEventType findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEventType matching the query, or a new ChildEventType object populated from the query conditions when no match is found
 *
 * @method     ChildEventType findOneById(int $id) Return the first ChildEventType filtered by the id column
 * @method     ChildEventType findOneByExecutable(string $executable) Return the first ChildEventType filtered by the executable column
 * @method     ChildEventType findOneByIsGeneric(boolean $is_generic) Return the first ChildEventType filtered by the is_generic column
 * @method     ChildEventType findOneByInputMimeId(int $input_mime_id) Return the first ChildEventType filtered by the input_mime_id column
 * @method     ChildEventType findOneByOutputMimeId(int $output_mime_id) Return the first ChildEventType filtered by the output_mime_id column *

 * @method     ChildEventType requirePk($key, ConnectionInterface $con = null) Return the ChildEventType by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEventType requireOne(ConnectionInterface $con = null) Return the first ChildEventType matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEventType requireOneById(int $id) Return the first ChildEventType filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEventType requireOneByExecutable(string $executable) Return the first ChildEventType filtered by the executable column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEventType requireOneByIsGeneric(boolean $is_generic) Return the first ChildEventType filtered by the is_generic column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEventType requireOneByInputMimeId(int $input_mime_id) Return the first ChildEventType filtered by the input_mime_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEventType requireOneByOutputMimeId(int $output_mime_id) Return the first ChildEventType filtered by the output_mime_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEventType[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEventType objects based on current ModelCriteria
 * @method     ChildEventType[]|ObjectCollection findById(int $id) Return ChildEventType objects filtered by the id column
 * @method     ChildEventType[]|ObjectCollection findByExecutable(string $executable) Return ChildEventType objects filtered by the executable column
 * @method     ChildEventType[]|ObjectCollection findByIsGeneric(boolean $is_generic) Return ChildEventType objects filtered by the is_generic column
 * @method     ChildEventType[]|ObjectCollection findByInputMimeId(int $input_mime_id) Return ChildEventType objects filtered by the input_mime_id column
 * @method     ChildEventType[]|ObjectCollection findByOutputMimeId(int $output_mime_id) Return ChildEventType objects filtered by the output_mime_id column
 * @method     ChildEventType[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EventTypeQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\Event\Base\EventTypeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\Event\\EventType', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEventTypeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEventTypeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEventTypeQuery) {
            return $criteria;
        }
        $query = new ChildEventTypeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEventType|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EventTypeTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = EventTypeTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEventType A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, executable, is_generic, input_mime_id, output_mime_id FROM event_type WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEventType $obj */
            $obj = new ChildEventType();
            $obj->hydrate($row);
            EventTypeTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEventType|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEventTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EventTypeTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEventTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EventTypeTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEventTypeQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(EventTypeTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(EventTypeTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventTypeTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the executable column
     *
     * Example usage:
     * <code>
     * $query->filterByExecutable('fooValue');   // WHERE executable = 'fooValue'
     * $query->filterByExecutable('%fooValue%', Criteria::LIKE); // WHERE executable LIKE '%fooValue%'
     * </code>
     *
     * @param     string $executable The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEventTypeQuery The current query, for fluid interface
     */
    public function filterByExecutable($executable = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($executable)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventTypeTableMap::COL_EXECUTABLE, $executable, $comparison);
    }

    /**
     * Filter the query on the is_generic column
     *
     * Example usage:
     * <code>
     * $query->filterByIsGeneric(true); // WHERE is_generic = true
     * $query->filterByIsGeneric('yes'); // WHERE is_generic = true
     * </code>
     *
     * @param     boolean|string $isGeneric The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEventTypeQuery The current query, for fluid interface
     */
    public function filterByIsGeneric($isGeneric = null, $comparison = null)
    {
        if (is_string($isGeneric)) {
            $isGeneric = in_array(strtolower($isGeneric), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(EventTypeTableMap::COL_IS_GENERIC, $isGeneric, $comparison);
    }

    /**
     * Filter the query on the input_mime_id column
     *
     * Example usage:
     * <code>
     * $query->filterByInputMimeId(1234); // WHERE input_mime_id = 1234
     * $query->filterByInputMimeId(array(12, 34)); // WHERE input_mime_id IN (12, 34)
     * $query->filterByInputMimeId(array('min' => 12)); // WHERE input_mime_id > 12
     * </code>
     *
     * @see       filterByEventInputMimeType()
     *
     * @param     mixed $inputMimeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEventTypeQuery The current query, for fluid interface
     */
    public function filterByInputMimeId($inputMimeId = null, $comparison = null)
    {
        if (is_array($inputMimeId)) {
            $useMinMax = false;
            if (isset($inputMimeId['min'])) {
                $this->addUsingAlias(EventTypeTableMap::COL_INPUT_MIME_ID, $inputMimeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($inputMimeId['max'])) {
                $this->addUsingAlias(EventTypeTableMap::COL_INPUT_MIME_ID, $inputMimeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventTypeTableMap::COL_INPUT_MIME_ID, $inputMimeId, $comparison);
    }

    /**
     * Filter the query on the output_mime_id column
     *
     * Example usage:
     * <code>
     * $query->filterByOutputMimeId(1234); // WHERE output_mime_id = 1234
     * $query->filterByOutputMimeId(array(12, 34)); // WHERE output_mime_id IN (12, 34)
     * $query->filterByOutputMimeId(array('min' => 12)); // WHERE output_mime_id > 12
     * </code>
     *
     * @see       filterByEventOutputMimeType()
     *
     * @param     mixed $outputMimeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEventTypeQuery The current query, for fluid interface
     */
    public function filterByOutputMimeId($outputMimeId = null, $comparison = null)
    {
        if (is_array($outputMimeId)) {
            $useMinMax = false;
            if (isset($outputMimeId['min'])) {
                $this->addUsingAlias(EventTypeTableMap::COL_OUTPUT_MIME_ID, $outputMimeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($outputMimeId['max'])) {
                $this->addUsingAlias(EventTypeTableMap::COL_OUTPUT_MIME_ID, $outputMimeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventTypeTableMap::COL_OUTPUT_MIME_ID, $outputMimeId, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\MimeType object
     *
     * @param \Model\System\MimeType|ObjectCollection $mimeType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEventTypeQuery The current query, for fluid interface
     */
    public function filterByEventInputMimeType($mimeType, $comparison = null)
    {
        if ($mimeType instanceof \Model\System\MimeType) {
            return $this
                ->addUsingAlias(EventTypeTableMap::COL_INPUT_MIME_ID, $mimeType->getId(), $comparison);
        } elseif ($mimeType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EventTypeTableMap::COL_INPUT_MIME_ID, $mimeType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByEventInputMimeType() only accepts arguments of type \Model\System\MimeType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EventInputMimeType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEventTypeQuery The current query, for fluid interface
     */
    public function joinEventInputMimeType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EventInputMimeType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EventInputMimeType');
        }

        return $this;
    }

    /**
     * Use the EventInputMimeType relation MimeType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\MimeTypeQuery A secondary query class using the current class as primary query
     */
    public function useEventInputMimeTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEventInputMimeType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EventInputMimeType', '\Model\System\MimeTypeQuery');
    }

    /**
     * Filter the query by a related \Model\System\MimeType object
     *
     * @param \Model\System\MimeType|ObjectCollection $mimeType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEventTypeQuery The current query, for fluid interface
     */
    public function filterByEventOutputMimeType($mimeType, $comparison = null)
    {
        if ($mimeType instanceof \Model\System\MimeType) {
            return $this
                ->addUsingAlias(EventTypeTableMap::COL_OUTPUT_MIME_ID, $mimeType->getId(), $comparison);
        } elseif ($mimeType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EventTypeTableMap::COL_OUTPUT_MIME_ID, $mimeType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByEventOutputMimeType() only accepts arguments of type \Model\System\MimeType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EventOutputMimeType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEventTypeQuery The current query, for fluid interface
     */
    public function joinEventOutputMimeType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EventOutputMimeType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EventOutputMimeType');
        }

        return $this;
    }

    /**
     * Use the EventOutputMimeType relation MimeType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\MimeTypeQuery A secondary query class using the current class as primary query
     */
    public function useEventOutputMimeTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEventOutputMimeType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EventOutputMimeType', '\Model\System\MimeTypeQuery');
    }

    /**
     * Filter the query by a related \Model\System\Event\EventTypeSupportedMoment object
     *
     * @param \Model\System\Event\EventTypeSupportedMoment|ObjectCollection $eventTypeSupportedMoment the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEventTypeQuery The current query, for fluid interface
     */
    public function filterByEventTypeSupportedMoment($eventTypeSupportedMoment, $comparison = null)
    {
        if ($eventTypeSupportedMoment instanceof \Model\System\Event\EventTypeSupportedMoment) {
            return $this
                ->addUsingAlias(EventTypeTableMap::COL_ID, $eventTypeSupportedMoment->getEventTypeId(), $comparison);
        } elseif ($eventTypeSupportedMoment instanceof ObjectCollection) {
            return $this
                ->useEventTypeSupportedMomentQuery()
                ->filterByPrimaryKeys($eventTypeSupportedMoment->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEventTypeSupportedMoment() only accepts arguments of type \Model\System\Event\EventTypeSupportedMoment or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EventTypeSupportedMoment relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEventTypeQuery The current query, for fluid interface
     */
    public function joinEventTypeSupportedMoment($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EventTypeSupportedMoment');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EventTypeSupportedMoment');
        }

        return $this;
    }

    /**
     * Use the EventTypeSupportedMoment relation EventTypeSupportedMoment object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\Event\EventTypeSupportedMomentQuery A secondary query class using the current class as primary query
     */
    public function useEventTypeSupportedMomentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEventTypeSupportedMoment($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EventTypeSupportedMoment', '\Model\System\Event\EventTypeSupportedMomentQuery');
    }

    /**
     * Filter the query by a related \Model\System\Event\EventHandler object
     *
     * @param \Model\System\Event\EventHandler|ObjectCollection $eventHandler the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildEventTypeQuery The current query, for fluid interface
     */
    public function filterByEventHandler($eventHandler, $comparison = null)
    {
        if ($eventHandler instanceof \Model\System\Event\EventHandler) {
            return $this
                ->addUsingAlias(EventTypeTableMap::COL_ID, $eventHandler->getEventTypeId(), $comparison);
        } elseif ($eventHandler instanceof ObjectCollection) {
            return $this
                ->useEventHandlerQuery()
                ->filterByPrimaryKeys($eventHandler->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByEventHandler() only accepts arguments of type \Model\System\Event\EventHandler or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EventHandler relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEventTypeQuery The current query, for fluid interface
     */
    public function joinEventHandler($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EventHandler');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EventHandler');
        }

        return $this;
    }

    /**
     * Use the EventHandler relation EventHandler object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\Event\EventHandlerQuery A secondary query class using the current class as primary query
     */
    public function useEventHandlerQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEventHandler($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EventHandler', '\Model\System\Event\EventHandlerQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEventType $eventType Object to remove from the list of results
     *
     * @return $this|ChildEventTypeQuery The current query, for fluid interface
     */
    public function prune($eventType = null)
    {
        if ($eventType) {
            $this->addUsingAlias(EventTypeTableMap::COL_ID, $eventType->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the event_type table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EventTypeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EventTypeTableMap::clearInstancePool();
            EventTypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EventTypeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EventTypeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EventTypeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EventTypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // EventTypeQuery
