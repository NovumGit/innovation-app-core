<?php

namespace Model\System\Event\Base;

use \Exception;
use \PDO;
use Model\Setting\CrudManager\CrudConfig;
use Model\System\Event\EventHandler as ChildEventHandler;
use Model\System\Event\EventHandlerQuery as ChildEventHandlerQuery;
use Model\System\Event\Map\EventHandlerTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'event_handler' table.
 *
 *
 *
 * @method     ChildEventHandlerQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildEventHandlerQuery orderByName($order = Criteria::ASC) Order by the name column
 * @method     ChildEventHandlerQuery orderByEventId($order = Criteria::ASC) Order by the event_id column
 * @method     ChildEventHandlerQuery orderByEventTypeId($order = Criteria::ASC) Order by the event_type_id column
 * @method     ChildEventHandlerQuery orderByCrudConfigId($order = Criteria::ASC) Order by the crud_config_id column
 *
 * @method     ChildEventHandlerQuery groupById() Group by the id column
 * @method     ChildEventHandlerQuery groupByName() Group by the name column
 * @method     ChildEventHandlerQuery groupByEventId() Group by the event_id column
 * @method     ChildEventHandlerQuery groupByEventTypeId() Group by the event_type_id column
 * @method     ChildEventHandlerQuery groupByCrudConfigId() Group by the crud_config_id column
 *
 * @method     ChildEventHandlerQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildEventHandlerQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildEventHandlerQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildEventHandlerQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildEventHandlerQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildEventHandlerQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildEventHandlerQuery leftJoinEventHandlerEventType($relationAlias = null) Adds a LEFT JOIN clause to the query using the EventHandlerEventType relation
 * @method     ChildEventHandlerQuery rightJoinEventHandlerEventType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the EventHandlerEventType relation
 * @method     ChildEventHandlerQuery innerJoinEventHandlerEventType($relationAlias = null) Adds a INNER JOIN clause to the query using the EventHandlerEventType relation
 *
 * @method     ChildEventHandlerQuery joinWithEventHandlerEventType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the EventHandlerEventType relation
 *
 * @method     ChildEventHandlerQuery leftJoinWithEventHandlerEventType() Adds a LEFT JOIN clause and with to the query using the EventHandlerEventType relation
 * @method     ChildEventHandlerQuery rightJoinWithEventHandlerEventType() Adds a RIGHT JOIN clause and with to the query using the EventHandlerEventType relation
 * @method     ChildEventHandlerQuery innerJoinWithEventHandlerEventType() Adds a INNER JOIN clause and with to the query using the EventHandlerEventType relation
 *
 * @method     ChildEventHandlerQuery leftJoinCrudConfig($relationAlias = null) Adds a LEFT JOIN clause to the query using the CrudConfig relation
 * @method     ChildEventHandlerQuery rightJoinCrudConfig($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CrudConfig relation
 * @method     ChildEventHandlerQuery innerJoinCrudConfig($relationAlias = null) Adds a INNER JOIN clause to the query using the CrudConfig relation
 *
 * @method     ChildEventHandlerQuery joinWithCrudConfig($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CrudConfig relation
 *
 * @method     ChildEventHandlerQuery leftJoinWithCrudConfig() Adds a LEFT JOIN clause and with to the query using the CrudConfig relation
 * @method     ChildEventHandlerQuery rightJoinWithCrudConfig() Adds a RIGHT JOIN clause and with to the query using the CrudConfig relation
 * @method     ChildEventHandlerQuery innerJoinWithCrudConfig() Adds a INNER JOIN clause and with to the query using the CrudConfig relation
 *
 * @method     \Model\System\Event\EventTypeQuery|\Model\Setting\CrudManager\CrudConfigQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildEventHandler findOne(ConnectionInterface $con = null) Return the first ChildEventHandler matching the query
 * @method     ChildEventHandler findOneOrCreate(ConnectionInterface $con = null) Return the first ChildEventHandler matching the query, or a new ChildEventHandler object populated from the query conditions when no match is found
 *
 * @method     ChildEventHandler findOneById(int $id) Return the first ChildEventHandler filtered by the id column
 * @method     ChildEventHandler findOneByName(string $name) Return the first ChildEventHandler filtered by the name column
 * @method     ChildEventHandler findOneByEventId(int $event_id) Return the first ChildEventHandler filtered by the event_id column
 * @method     ChildEventHandler findOneByEventTypeId(int $event_type_id) Return the first ChildEventHandler filtered by the event_type_id column
 * @method     ChildEventHandler findOneByCrudConfigId(int $crud_config_id) Return the first ChildEventHandler filtered by the crud_config_id column *

 * @method     ChildEventHandler requirePk($key, ConnectionInterface $con = null) Return the ChildEventHandler by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEventHandler requireOne(ConnectionInterface $con = null) Return the first ChildEventHandler matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEventHandler requireOneById(int $id) Return the first ChildEventHandler filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEventHandler requireOneByName(string $name) Return the first ChildEventHandler filtered by the name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEventHandler requireOneByEventId(int $event_id) Return the first ChildEventHandler filtered by the event_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEventHandler requireOneByEventTypeId(int $event_type_id) Return the first ChildEventHandler filtered by the event_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildEventHandler requireOneByCrudConfigId(int $crud_config_id) Return the first ChildEventHandler filtered by the crud_config_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildEventHandler[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildEventHandler objects based on current ModelCriteria
 * @method     ChildEventHandler[]|ObjectCollection findById(int $id) Return ChildEventHandler objects filtered by the id column
 * @method     ChildEventHandler[]|ObjectCollection findByName(string $name) Return ChildEventHandler objects filtered by the name column
 * @method     ChildEventHandler[]|ObjectCollection findByEventId(int $event_id) Return ChildEventHandler objects filtered by the event_id column
 * @method     ChildEventHandler[]|ObjectCollection findByEventTypeId(int $event_type_id) Return ChildEventHandler objects filtered by the event_type_id column
 * @method     ChildEventHandler[]|ObjectCollection findByCrudConfigId(int $crud_config_id) Return ChildEventHandler objects filtered by the crud_config_id column
 * @method     ChildEventHandler[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class EventHandlerQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\Event\Base\EventHandlerQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\Event\\EventHandler', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildEventHandlerQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildEventHandlerQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildEventHandlerQuery) {
            return $criteria;
        }
        $query = new ChildEventHandlerQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildEventHandler|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EventHandlerTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = EventHandlerTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEventHandler A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, name, event_id, event_type_id, crud_config_id FROM event_handler WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildEventHandler $obj */
            $obj = new ChildEventHandler();
            $obj->hydrate($row);
            EventHandlerTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildEventHandler|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildEventHandlerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(EventHandlerTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildEventHandlerQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(EventHandlerTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEventHandlerQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(EventHandlerTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(EventHandlerTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventHandlerTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE name = 'fooValue'
     * $query->filterByName('%fooValue%', Criteria::LIKE); // WHERE name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEventHandlerQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventHandlerTableMap::COL_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the event_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEventId(1234); // WHERE event_id = 1234
     * $query->filterByEventId(array(12, 34)); // WHERE event_id IN (12, 34)
     * $query->filterByEventId(array('min' => 12)); // WHERE event_id > 12
     * </code>
     *
     * @param     mixed $eventId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEventHandlerQuery The current query, for fluid interface
     */
    public function filterByEventId($eventId = null, $comparison = null)
    {
        if (is_array($eventId)) {
            $useMinMax = false;
            if (isset($eventId['min'])) {
                $this->addUsingAlias(EventHandlerTableMap::COL_EVENT_ID, $eventId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($eventId['max'])) {
                $this->addUsingAlias(EventHandlerTableMap::COL_EVENT_ID, $eventId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventHandlerTableMap::COL_EVENT_ID, $eventId, $comparison);
    }

    /**
     * Filter the query on the event_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEventTypeId(1234); // WHERE event_type_id = 1234
     * $query->filterByEventTypeId(array(12, 34)); // WHERE event_type_id IN (12, 34)
     * $query->filterByEventTypeId(array('min' => 12)); // WHERE event_type_id > 12
     * </code>
     *
     * @see       filterByEventHandlerEventType()
     *
     * @param     mixed $eventTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEventHandlerQuery The current query, for fluid interface
     */
    public function filterByEventTypeId($eventTypeId = null, $comparison = null)
    {
        if (is_array($eventTypeId)) {
            $useMinMax = false;
            if (isset($eventTypeId['min'])) {
                $this->addUsingAlias(EventHandlerTableMap::COL_EVENT_TYPE_ID, $eventTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($eventTypeId['max'])) {
                $this->addUsingAlias(EventHandlerTableMap::COL_EVENT_TYPE_ID, $eventTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventHandlerTableMap::COL_EVENT_TYPE_ID, $eventTypeId, $comparison);
    }

    /**
     * Filter the query on the crud_config_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCrudConfigId(1234); // WHERE crud_config_id = 1234
     * $query->filterByCrudConfigId(array(12, 34)); // WHERE crud_config_id IN (12, 34)
     * $query->filterByCrudConfigId(array('min' => 12)); // WHERE crud_config_id > 12
     * </code>
     *
     * @see       filterByCrudConfig()
     *
     * @param     mixed $crudConfigId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildEventHandlerQuery The current query, for fluid interface
     */
    public function filterByCrudConfigId($crudConfigId = null, $comparison = null)
    {
        if (is_array($crudConfigId)) {
            $useMinMax = false;
            if (isset($crudConfigId['min'])) {
                $this->addUsingAlias(EventHandlerTableMap::COL_CRUD_CONFIG_ID, $crudConfigId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($crudConfigId['max'])) {
                $this->addUsingAlias(EventHandlerTableMap::COL_CRUD_CONFIG_ID, $crudConfigId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(EventHandlerTableMap::COL_CRUD_CONFIG_ID, $crudConfigId, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\Event\EventType object
     *
     * @param \Model\System\Event\EventType|ObjectCollection $eventType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEventHandlerQuery The current query, for fluid interface
     */
    public function filterByEventHandlerEventType($eventType, $comparison = null)
    {
        if ($eventType instanceof \Model\System\Event\EventType) {
            return $this
                ->addUsingAlias(EventHandlerTableMap::COL_EVENT_TYPE_ID, $eventType->getId(), $comparison);
        } elseif ($eventType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EventHandlerTableMap::COL_EVENT_TYPE_ID, $eventType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByEventHandlerEventType() only accepts arguments of type \Model\System\Event\EventType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the EventHandlerEventType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEventHandlerQuery The current query, for fluid interface
     */
    public function joinEventHandlerEventType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('EventHandlerEventType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'EventHandlerEventType');
        }

        return $this;
    }

    /**
     * Use the EventHandlerEventType relation EventType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\Event\EventTypeQuery A secondary query class using the current class as primary query
     */
    public function useEventHandlerEventTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEventHandlerEventType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'EventHandlerEventType', '\Model\System\Event\EventTypeQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\CrudManager\CrudConfig object
     *
     * @param \Model\Setting\CrudManager\CrudConfig|ObjectCollection $crudConfig The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildEventHandlerQuery The current query, for fluid interface
     */
    public function filterByCrudConfig($crudConfig, $comparison = null)
    {
        if ($crudConfig instanceof \Model\Setting\CrudManager\CrudConfig) {
            return $this
                ->addUsingAlias(EventHandlerTableMap::COL_CRUD_CONFIG_ID, $crudConfig->getId(), $comparison);
        } elseif ($crudConfig instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(EventHandlerTableMap::COL_CRUD_CONFIG_ID, $crudConfig->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCrudConfig() only accepts arguments of type \Model\Setting\CrudManager\CrudConfig or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CrudConfig relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildEventHandlerQuery The current query, for fluid interface
     */
    public function joinCrudConfig($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CrudConfig');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CrudConfig');
        }

        return $this;
    }

    /**
     * Use the CrudConfig relation CrudConfig object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\CrudManager\CrudConfigQuery A secondary query class using the current class as primary query
     */
    public function useCrudConfigQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCrudConfig($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CrudConfig', '\Model\Setting\CrudManager\CrudConfigQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildEventHandler $eventHandler Object to remove from the list of results
     *
     * @return $this|ChildEventHandlerQuery The current query, for fluid interface
     */
    public function prune($eventHandler = null)
    {
        if ($eventHandler) {
            $this->addUsingAlias(EventHandlerTableMap::COL_ID, $eventHandler->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the event_handler table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EventHandlerTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            EventHandlerTableMap::clearInstancePool();
            EventHandlerTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EventHandlerTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(EventHandlerTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            EventHandlerTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            EventHandlerTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // EventHandlerQuery
