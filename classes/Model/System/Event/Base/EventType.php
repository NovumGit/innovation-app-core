<?php

namespace Model\System\Event\Base;

use \Exception;
use \PDO;
use Model\System\MimeType;
use Model\System\MimeTypeQuery;
use Model\System\Event\EventHandler as ChildEventHandler;
use Model\System\Event\EventHandlerQuery as ChildEventHandlerQuery;
use Model\System\Event\EventType as ChildEventType;
use Model\System\Event\EventTypeQuery as ChildEventTypeQuery;
use Model\System\Event\EventTypeSupportedMoment as ChildEventTypeSupportedMoment;
use Model\System\Event\EventTypeSupportedMomentQuery as ChildEventTypeSupportedMomentQuery;
use Model\System\Event\Map\EventHandlerTableMap;
use Model\System\Event\Map\EventTypeSupportedMomentTableMap;
use Model\System\Event\Map\EventTypeTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;

/**
 * Base class that represents a row from the 'event_type' table.
 *
 *
 *
 * @package    propel.generator.Model.System.Event.Base
 */
abstract class EventType implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\System\\Event\\Map\\EventTypeTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the executable field.
     *
     * @var        string
     */
    protected $executable;

    /**
     * The value for the is_generic field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $is_generic;

    /**
     * The value for the input_mime_id field.
     *
     * @var        int
     */
    protected $input_mime_id;

    /**
     * The value for the output_mime_id field.
     *
     * @var        int
     */
    protected $output_mime_id;

    /**
     * @var        MimeType
     */
    protected $aEventInputMimeType;

    /**
     * @var        MimeType
     */
    protected $aEventOutputMimeType;

    /**
     * @var        ObjectCollection|ChildEventTypeSupportedMoment[] Collection to store aggregation of ChildEventTypeSupportedMoment objects.
     */
    protected $collEventTypeSupportedMoments;
    protected $collEventTypeSupportedMomentsPartial;

    /**
     * @var        ObjectCollection|ChildEventHandler[] Collection to store aggregation of ChildEventHandler objects.
     */
    protected $collEventHandlers;
    protected $collEventHandlersPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEventTypeSupportedMoment[]
     */
    protected $eventTypeSupportedMomentsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildEventHandler[]
     */
    protected $eventHandlersScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->is_generic = false;
    }

    /**
     * Initializes internal state of Model\System\Event\Base\EventType object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>EventType</code> instance.  If
     * <code>obj</code> is an instance of <code>EventType</code>, delegates to
     * <code>equals(EventType)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [executable] column value.
     *
     * @return string
     */
    public function getExecutable()
    {
        return $this->executable;
    }

    /**
     * Get the [is_generic] column value.
     *
     * @return boolean
     */
    public function getIsGeneric()
    {
        return $this->is_generic;
    }

    /**
     * Get the [is_generic] column value.
     *
     * @return boolean
     */
    public function isGeneric()
    {
        return $this->getIsGeneric();
    }

    /**
     * Get the [input_mime_id] column value.
     *
     * @return int
     */
    public function getInputMimeId()
    {
        return $this->input_mime_id;
    }

    /**
     * Get the [output_mime_id] column value.
     *
     * @return int
     */
    public function getOutputMimeId()
    {
        return $this->output_mime_id;
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\Event\EventType The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[EventTypeTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [executable] column.
     *
     * @param string $v New value
     * @return $this|\Model\System\Event\EventType The current object (for fluent API support)
     */
    public function setExecutable($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->executable !== $v) {
            $this->executable = $v;
            $this->modifiedColumns[EventTypeTableMap::COL_EXECUTABLE] = true;
        }

        return $this;
    } // setExecutable()

    /**
     * Sets the value of the [is_generic] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\Model\System\Event\EventType The current object (for fluent API support)
     */
    public function setIsGeneric($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->is_generic !== $v) {
            $this->is_generic = $v;
            $this->modifiedColumns[EventTypeTableMap::COL_IS_GENERIC] = true;
        }

        return $this;
    } // setIsGeneric()

    /**
     * Set the value of [input_mime_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\Event\EventType The current object (for fluent API support)
     */
    public function setInputMimeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->input_mime_id !== $v) {
            $this->input_mime_id = $v;
            $this->modifiedColumns[EventTypeTableMap::COL_INPUT_MIME_ID] = true;
        }

        if ($this->aEventInputMimeType !== null && $this->aEventInputMimeType->getId() !== $v) {
            $this->aEventInputMimeType = null;
        }

        return $this;
    } // setInputMimeId()

    /**
     * Set the value of [output_mime_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\Event\EventType The current object (for fluent API support)
     */
    public function setOutputMimeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->output_mime_id !== $v) {
            $this->output_mime_id = $v;
            $this->modifiedColumns[EventTypeTableMap::COL_OUTPUT_MIME_ID] = true;
        }

        if ($this->aEventOutputMimeType !== null && $this->aEventOutputMimeType->getId() !== $v) {
            $this->aEventOutputMimeType = null;
        }

        return $this;
    } // setOutputMimeId()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->is_generic !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : EventTypeTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : EventTypeTableMap::translateFieldName('Executable', TableMap::TYPE_PHPNAME, $indexType)];
            $this->executable = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : EventTypeTableMap::translateFieldName('IsGeneric', TableMap::TYPE_PHPNAME, $indexType)];
            $this->is_generic = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : EventTypeTableMap::translateFieldName('InputMimeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->input_mime_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : EventTypeTableMap::translateFieldName('OutputMimeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->output_mime_id = (null !== $col) ? (int) $col : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 5; // 5 = EventTypeTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\System\\Event\\EventType'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aEventInputMimeType !== null && $this->input_mime_id !== $this->aEventInputMimeType->getId()) {
            $this->aEventInputMimeType = null;
        }
        if ($this->aEventOutputMimeType !== null && $this->output_mime_id !== $this->aEventOutputMimeType->getId()) {
            $this->aEventOutputMimeType = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(EventTypeTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildEventTypeQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aEventInputMimeType = null;
            $this->aEventOutputMimeType = null;
            $this->collEventTypeSupportedMoments = null;

            $this->collEventHandlers = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see EventType::setDeleted()
     * @see EventType::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(EventTypeTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildEventTypeQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(EventTypeTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                EventTypeTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aEventInputMimeType !== null) {
                if ($this->aEventInputMimeType->isModified() || $this->aEventInputMimeType->isNew()) {
                    $affectedRows += $this->aEventInputMimeType->save($con);
                }
                $this->setEventInputMimeType($this->aEventInputMimeType);
            }

            if ($this->aEventOutputMimeType !== null) {
                if ($this->aEventOutputMimeType->isModified() || $this->aEventOutputMimeType->isNew()) {
                    $affectedRows += $this->aEventOutputMimeType->save($con);
                }
                $this->setEventOutputMimeType($this->aEventOutputMimeType);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->eventTypeSupportedMomentsScheduledForDeletion !== null) {
                if (!$this->eventTypeSupportedMomentsScheduledForDeletion->isEmpty()) {
                    \Model\System\Event\EventTypeSupportedMomentQuery::create()
                        ->filterByPrimaryKeys($this->eventTypeSupportedMomentsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->eventTypeSupportedMomentsScheduledForDeletion = null;
                }
            }

            if ($this->collEventTypeSupportedMoments !== null) {
                foreach ($this->collEventTypeSupportedMoments as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->eventHandlersScheduledForDeletion !== null) {
                if (!$this->eventHandlersScheduledForDeletion->isEmpty()) {
                    \Model\System\Event\EventHandlerQuery::create()
                        ->filterByPrimaryKeys($this->eventHandlersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->eventHandlersScheduledForDeletion = null;
                }
            }

            if ($this->collEventHandlers !== null) {
                foreach ($this->collEventHandlers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[EventTypeTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . EventTypeTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(EventTypeTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(EventTypeTableMap::COL_EXECUTABLE)) {
            $modifiedColumns[':p' . $index++]  = 'executable';
        }
        if ($this->isColumnModified(EventTypeTableMap::COL_IS_GENERIC)) {
            $modifiedColumns[':p' . $index++]  = 'is_generic';
        }
        if ($this->isColumnModified(EventTypeTableMap::COL_INPUT_MIME_ID)) {
            $modifiedColumns[':p' . $index++]  = 'input_mime_id';
        }
        if ($this->isColumnModified(EventTypeTableMap::COL_OUTPUT_MIME_ID)) {
            $modifiedColumns[':p' . $index++]  = 'output_mime_id';
        }

        $sql = sprintf(
            'INSERT INTO event_type (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'executable':
                        $stmt->bindValue($identifier, $this->executable, PDO::PARAM_STR);
                        break;
                    case 'is_generic':
                        $stmt->bindValue($identifier, (int) $this->is_generic, PDO::PARAM_INT);
                        break;
                    case 'input_mime_id':
                        $stmt->bindValue($identifier, $this->input_mime_id, PDO::PARAM_INT);
                        break;
                    case 'output_mime_id':
                        $stmt->bindValue($identifier, $this->output_mime_id, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = EventTypeTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getExecutable();
                break;
            case 2:
                return $this->getIsGeneric();
                break;
            case 3:
                return $this->getInputMimeId();
                break;
            case 4:
                return $this->getOutputMimeId();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['EventType'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['EventType'][$this->hashCode()] = true;
        $keys = EventTypeTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getExecutable(),
            $keys[2] => $this->getIsGeneric(),
            $keys[3] => $this->getInputMimeId(),
            $keys[4] => $this->getOutputMimeId(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aEventInputMimeType) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'mimeType';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mime_type';
                        break;
                    default:
                        $key = 'EventInputMimeType';
                }

                $result[$key] = $this->aEventInputMimeType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aEventOutputMimeType) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'mimeType';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'mime_type';
                        break;
                    default:
                        $key = 'EventOutputMimeType';
                }

                $result[$key] = $this->aEventOutputMimeType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collEventTypeSupportedMoments) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'eventTypeSupportedMoments';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'event_type_supported_triggers';
                        break;
                    default:
                        $key = 'EventTypeSupportedMoments';
                }

                $result[$key] = $this->collEventTypeSupportedMoments->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collEventHandlers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'eventHandlers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'event_handlers';
                        break;
                    default:
                        $key = 'EventHandlers';
                }

                $result[$key] = $this->collEventHandlers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\System\Event\EventType
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = EventTypeTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\System\Event\EventType
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setExecutable($value);
                break;
            case 2:
                $this->setIsGeneric($value);
                break;
            case 3:
                $this->setInputMimeId($value);
                break;
            case 4:
                $this->setOutputMimeId($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = EventTypeTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setExecutable($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setIsGeneric($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setInputMimeId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setOutputMimeId($arr[$keys[4]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\System\Event\EventType The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(EventTypeTableMap::DATABASE_NAME);

        if ($this->isColumnModified(EventTypeTableMap::COL_ID)) {
            $criteria->add(EventTypeTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(EventTypeTableMap::COL_EXECUTABLE)) {
            $criteria->add(EventTypeTableMap::COL_EXECUTABLE, $this->executable);
        }
        if ($this->isColumnModified(EventTypeTableMap::COL_IS_GENERIC)) {
            $criteria->add(EventTypeTableMap::COL_IS_GENERIC, $this->is_generic);
        }
        if ($this->isColumnModified(EventTypeTableMap::COL_INPUT_MIME_ID)) {
            $criteria->add(EventTypeTableMap::COL_INPUT_MIME_ID, $this->input_mime_id);
        }
        if ($this->isColumnModified(EventTypeTableMap::COL_OUTPUT_MIME_ID)) {
            $criteria->add(EventTypeTableMap::COL_OUTPUT_MIME_ID, $this->output_mime_id);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildEventTypeQuery::create();
        $criteria->add(EventTypeTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\System\Event\EventType (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setExecutable($this->getExecutable());
        $copyObj->setIsGeneric($this->getIsGeneric());
        $copyObj->setInputMimeId($this->getInputMimeId());
        $copyObj->setOutputMimeId($this->getOutputMimeId());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getEventTypeSupportedMoments() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEventTypeSupportedMoment($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getEventHandlers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addEventHandler($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\System\Event\EventType Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a MimeType object.
     *
     * @param  MimeType $v
     * @return $this|\Model\System\Event\EventType The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEventInputMimeType(MimeType $v = null)
    {
        if ($v === null) {
            $this->setInputMimeId(NULL);
        } else {
            $this->setInputMimeId($v->getId());
        }

        $this->aEventInputMimeType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MimeType object, it will not be re-added.
        if ($v !== null) {
            $v->addEventTypeRelatedByInputMimeId($this);
        }


        return $this;
    }


    /**
     * Get the associated MimeType object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return MimeType The associated MimeType object.
     * @throws PropelException
     */
    public function getEventInputMimeType(ConnectionInterface $con = null)
    {
        if ($this->aEventInputMimeType === null && ($this->input_mime_id != 0)) {
            $this->aEventInputMimeType = MimeTypeQuery::create()->findPk($this->input_mime_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEventInputMimeType->addEventTypesRelatedByInputMimeId($this);
             */
        }

        return $this->aEventInputMimeType;
    }

    /**
     * Declares an association between this object and a MimeType object.
     *
     * @param  MimeType $v
     * @return $this|\Model\System\Event\EventType The current object (for fluent API support)
     * @throws PropelException
     */
    public function setEventOutputMimeType(MimeType $v = null)
    {
        if ($v === null) {
            $this->setOutputMimeId(NULL);
        } else {
            $this->setOutputMimeId($v->getId());
        }

        $this->aEventOutputMimeType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the MimeType object, it will not be re-added.
        if ($v !== null) {
            $v->addEventTypeRelatedByOutputMimeId($this);
        }


        return $this;
    }


    /**
     * Get the associated MimeType object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return MimeType The associated MimeType object.
     * @throws PropelException
     */
    public function getEventOutputMimeType(ConnectionInterface $con = null)
    {
        if ($this->aEventOutputMimeType === null && ($this->output_mime_id != 0)) {
            $this->aEventOutputMimeType = MimeTypeQuery::create()->findPk($this->output_mime_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aEventOutputMimeType->addEventTypesRelatedByOutputMimeId($this);
             */
        }

        return $this->aEventOutputMimeType;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('EventTypeSupportedMoment' === $relationName) {
            $this->initEventTypeSupportedMoments();
            return;
        }
        if ('EventHandler' === $relationName) {
            $this->initEventHandlers();
            return;
        }
    }

    /**
     * Clears out the collEventTypeSupportedMoments collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEventTypeSupportedMoments()
     */
    public function clearEventTypeSupportedMoments()
    {
        $this->collEventTypeSupportedMoments = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEventTypeSupportedMoments collection loaded partially.
     */
    public function resetPartialEventTypeSupportedMoments($v = true)
    {
        $this->collEventTypeSupportedMomentsPartial = $v;
    }

    /**
     * Initializes the collEventTypeSupportedMoments collection.
     *
     * By default this just sets the collEventTypeSupportedMoments collection to an empty array (like clearcollEventTypeSupportedMoments());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEventTypeSupportedMoments($overrideExisting = true)
    {
        if (null !== $this->collEventTypeSupportedMoments && !$overrideExisting) {
            return;
        }

        $collectionClassName = EventTypeSupportedMomentTableMap::getTableMap()->getCollectionClassName();

        $this->collEventTypeSupportedMoments = new $collectionClassName;
        $this->collEventTypeSupportedMoments->setModel('\Model\System\Event\EventTypeSupportedMoment');
    }

    /**
     * Gets an array of ChildEventTypeSupportedMoment objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEventType is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEventTypeSupportedMoment[] List of ChildEventTypeSupportedMoment objects
     * @throws PropelException
     */
    public function getEventTypeSupportedMoments(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEventTypeSupportedMomentsPartial && !$this->isNew();
        if (null === $this->collEventTypeSupportedMoments || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collEventTypeSupportedMoments) {
                    $this->initEventTypeSupportedMoments();
                } else {
                    $collectionClassName = EventTypeSupportedMomentTableMap::getTableMap()->getCollectionClassName();

                    $collEventTypeSupportedMoments = new $collectionClassName;
                    $collEventTypeSupportedMoments->setModel('\Model\System\Event\EventTypeSupportedMoment');

                    return $collEventTypeSupportedMoments;
                }
            } else {
                $collEventTypeSupportedMoments = ChildEventTypeSupportedMomentQuery::create(null, $criteria)
                    ->filterByEventTriggerTypeEventType($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEventTypeSupportedMomentsPartial && count($collEventTypeSupportedMoments)) {
                        $this->initEventTypeSupportedMoments(false);

                        foreach ($collEventTypeSupportedMoments as $obj) {
                            if (false == $this->collEventTypeSupportedMoments->contains($obj)) {
                                $this->collEventTypeSupportedMoments->append($obj);
                            }
                        }

                        $this->collEventTypeSupportedMomentsPartial = true;
                    }

                    return $collEventTypeSupportedMoments;
                }

                if ($partial && $this->collEventTypeSupportedMoments) {
                    foreach ($this->collEventTypeSupportedMoments as $obj) {
                        if ($obj->isNew()) {
                            $collEventTypeSupportedMoments[] = $obj;
                        }
                    }
                }

                $this->collEventTypeSupportedMoments = $collEventTypeSupportedMoments;
                $this->collEventTypeSupportedMomentsPartial = false;
            }
        }

        return $this->collEventTypeSupportedMoments;
    }

    /**
     * Sets a collection of ChildEventTypeSupportedMoment objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $eventTypeSupportedMoments A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEventType The current object (for fluent API support)
     */
    public function setEventTypeSupportedMoments(Collection $eventTypeSupportedMoments, ConnectionInterface $con = null)
    {
        /** @var ChildEventTypeSupportedMoment[] $eventTypeSupportedMomentsToDelete */
        $eventTypeSupportedMomentsToDelete = $this->getEventTypeSupportedMoments(new Criteria(), $con)->diff($eventTypeSupportedMoments);


        $this->eventTypeSupportedMomentsScheduledForDeletion = $eventTypeSupportedMomentsToDelete;

        foreach ($eventTypeSupportedMomentsToDelete as $eventTypeSupportedMomentRemoved) {
            $eventTypeSupportedMomentRemoved->setEventTriggerTypeEventType(null);
        }

        $this->collEventTypeSupportedMoments = null;
        foreach ($eventTypeSupportedMoments as $eventTypeSupportedMoment) {
            $this->addEventTypeSupportedMoment($eventTypeSupportedMoment);
        }

        $this->collEventTypeSupportedMoments = $eventTypeSupportedMoments;
        $this->collEventTypeSupportedMomentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related EventTypeSupportedMoment objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related EventTypeSupportedMoment objects.
     * @throws PropelException
     */
    public function countEventTypeSupportedMoments(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEventTypeSupportedMomentsPartial && !$this->isNew();
        if (null === $this->collEventTypeSupportedMoments || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEventTypeSupportedMoments) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEventTypeSupportedMoments());
            }

            $query = ChildEventTypeSupportedMomentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEventTriggerTypeEventType($this)
                ->count($con);
        }

        return count($this->collEventTypeSupportedMoments);
    }

    /**
     * Method called to associate a ChildEventTypeSupportedMoment object to this object
     * through the ChildEventTypeSupportedMoment foreign key attribute.
     *
     * @param  ChildEventTypeSupportedMoment $l ChildEventTypeSupportedMoment
     * @return $this|\Model\System\Event\EventType The current object (for fluent API support)
     */
    public function addEventTypeSupportedMoment(ChildEventTypeSupportedMoment $l)
    {
        if ($this->collEventTypeSupportedMoments === null) {
            $this->initEventTypeSupportedMoments();
            $this->collEventTypeSupportedMomentsPartial = true;
        }

        if (!$this->collEventTypeSupportedMoments->contains($l)) {
            $this->doAddEventTypeSupportedMoment($l);

            if ($this->eventTypeSupportedMomentsScheduledForDeletion and $this->eventTypeSupportedMomentsScheduledForDeletion->contains($l)) {
                $this->eventTypeSupportedMomentsScheduledForDeletion->remove($this->eventTypeSupportedMomentsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEventTypeSupportedMoment $eventTypeSupportedMoment The ChildEventTypeSupportedMoment object to add.
     */
    protected function doAddEventTypeSupportedMoment(ChildEventTypeSupportedMoment $eventTypeSupportedMoment)
    {
        $this->collEventTypeSupportedMoments[]= $eventTypeSupportedMoment;
        $eventTypeSupportedMoment->setEventTriggerTypeEventType($this);
    }

    /**
     * @param  ChildEventTypeSupportedMoment $eventTypeSupportedMoment The ChildEventTypeSupportedMoment object to remove.
     * @return $this|ChildEventType The current object (for fluent API support)
     */
    public function removeEventTypeSupportedMoment(ChildEventTypeSupportedMoment $eventTypeSupportedMoment)
    {
        if ($this->getEventTypeSupportedMoments()->contains($eventTypeSupportedMoment)) {
            $pos = $this->collEventTypeSupportedMoments->search($eventTypeSupportedMoment);
            $this->collEventTypeSupportedMoments->remove($pos);
            if (null === $this->eventTypeSupportedMomentsScheduledForDeletion) {
                $this->eventTypeSupportedMomentsScheduledForDeletion = clone $this->collEventTypeSupportedMoments;
                $this->eventTypeSupportedMomentsScheduledForDeletion->clear();
            }
            $this->eventTypeSupportedMomentsScheduledForDeletion[]= clone $eventTypeSupportedMoment;
            $eventTypeSupportedMoment->setEventTriggerTypeEventType(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this EventType is new, it will return
     * an empty collection; or if this EventType has previously
     * been saved, it will retrieve related EventTypeSupportedMoments from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in EventType.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEventTypeSupportedMoment[] List of ChildEventTypeSupportedMoment objects
     */
    public function getEventTypeSupportedMomentsJoinEventTriggerTypeEvent(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEventTypeSupportedMomentQuery::create(null, $criteria);
        $query->joinWith('EventTriggerTypeEvent', $joinBehavior);

        return $this->getEventTypeSupportedMoments($query, $con);
    }

    /**
     * Clears out the collEventHandlers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addEventHandlers()
     */
    public function clearEventHandlers()
    {
        $this->collEventHandlers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collEventHandlers collection loaded partially.
     */
    public function resetPartialEventHandlers($v = true)
    {
        $this->collEventHandlersPartial = $v;
    }

    /**
     * Initializes the collEventHandlers collection.
     *
     * By default this just sets the collEventHandlers collection to an empty array (like clearcollEventHandlers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initEventHandlers($overrideExisting = true)
    {
        if (null !== $this->collEventHandlers && !$overrideExisting) {
            return;
        }

        $collectionClassName = EventHandlerTableMap::getTableMap()->getCollectionClassName();

        $this->collEventHandlers = new $collectionClassName;
        $this->collEventHandlers->setModel('\Model\System\Event\EventHandler');
    }

    /**
     * Gets an array of ChildEventHandler objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildEventType is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildEventHandler[] List of ChildEventHandler objects
     * @throws PropelException
     */
    public function getEventHandlers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collEventHandlersPartial && !$this->isNew();
        if (null === $this->collEventHandlers || null !== $criteria || $partial) {
            if ($this->isNew()) {
                // return empty collection
                if (null === $this->collEventHandlers) {
                    $this->initEventHandlers();
                } else {
                    $collectionClassName = EventHandlerTableMap::getTableMap()->getCollectionClassName();

                    $collEventHandlers = new $collectionClassName;
                    $collEventHandlers->setModel('\Model\System\Event\EventHandler');

                    return $collEventHandlers;
                }
            } else {
                $collEventHandlers = ChildEventHandlerQuery::create(null, $criteria)
                    ->filterByEventHandlerEventType($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collEventHandlersPartial && count($collEventHandlers)) {
                        $this->initEventHandlers(false);

                        foreach ($collEventHandlers as $obj) {
                            if (false == $this->collEventHandlers->contains($obj)) {
                                $this->collEventHandlers->append($obj);
                            }
                        }

                        $this->collEventHandlersPartial = true;
                    }

                    return $collEventHandlers;
                }

                if ($partial && $this->collEventHandlers) {
                    foreach ($this->collEventHandlers as $obj) {
                        if ($obj->isNew()) {
                            $collEventHandlers[] = $obj;
                        }
                    }
                }

                $this->collEventHandlers = $collEventHandlers;
                $this->collEventHandlersPartial = false;
            }
        }

        return $this->collEventHandlers;
    }

    /**
     * Sets a collection of ChildEventHandler objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $eventHandlers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildEventType The current object (for fluent API support)
     */
    public function setEventHandlers(Collection $eventHandlers, ConnectionInterface $con = null)
    {
        /** @var ChildEventHandler[] $eventHandlersToDelete */
        $eventHandlersToDelete = $this->getEventHandlers(new Criteria(), $con)->diff($eventHandlers);


        $this->eventHandlersScheduledForDeletion = $eventHandlersToDelete;

        foreach ($eventHandlersToDelete as $eventHandlerRemoved) {
            $eventHandlerRemoved->setEventHandlerEventType(null);
        }

        $this->collEventHandlers = null;
        foreach ($eventHandlers as $eventHandler) {
            $this->addEventHandler($eventHandler);
        }

        $this->collEventHandlers = $eventHandlers;
        $this->collEventHandlersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related EventHandler objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related EventHandler objects.
     * @throws PropelException
     */
    public function countEventHandlers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collEventHandlersPartial && !$this->isNew();
        if (null === $this->collEventHandlers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collEventHandlers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getEventHandlers());
            }

            $query = ChildEventHandlerQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByEventHandlerEventType($this)
                ->count($con);
        }

        return count($this->collEventHandlers);
    }

    /**
     * Method called to associate a ChildEventHandler object to this object
     * through the ChildEventHandler foreign key attribute.
     *
     * @param  ChildEventHandler $l ChildEventHandler
     * @return $this|\Model\System\Event\EventType The current object (for fluent API support)
     */
    public function addEventHandler(ChildEventHandler $l)
    {
        if ($this->collEventHandlers === null) {
            $this->initEventHandlers();
            $this->collEventHandlersPartial = true;
        }

        if (!$this->collEventHandlers->contains($l)) {
            $this->doAddEventHandler($l);

            if ($this->eventHandlersScheduledForDeletion and $this->eventHandlersScheduledForDeletion->contains($l)) {
                $this->eventHandlersScheduledForDeletion->remove($this->eventHandlersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildEventHandler $eventHandler The ChildEventHandler object to add.
     */
    protected function doAddEventHandler(ChildEventHandler $eventHandler)
    {
        $this->collEventHandlers[]= $eventHandler;
        $eventHandler->setEventHandlerEventType($this);
    }

    /**
     * @param  ChildEventHandler $eventHandler The ChildEventHandler object to remove.
     * @return $this|ChildEventType The current object (for fluent API support)
     */
    public function removeEventHandler(ChildEventHandler $eventHandler)
    {
        if ($this->getEventHandlers()->contains($eventHandler)) {
            $pos = $this->collEventHandlers->search($eventHandler);
            $this->collEventHandlers->remove($pos);
            if (null === $this->eventHandlersScheduledForDeletion) {
                $this->eventHandlersScheduledForDeletion = clone $this->collEventHandlers;
                $this->eventHandlersScheduledForDeletion->clear();
            }
            $this->eventHandlersScheduledForDeletion[]= clone $eventHandler;
            $eventHandler->setEventHandlerEventType(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this EventType is new, it will return
     * an empty collection; or if this EventType has previously
     * been saved, it will retrieve related EventHandlers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in EventType.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildEventHandler[] List of ChildEventHandler objects
     */
    public function getEventHandlersJoinCrudConfig(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildEventHandlerQuery::create(null, $criteria);
        $query->joinWith('CrudConfig', $joinBehavior);

        return $this->getEventHandlers($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aEventInputMimeType) {
            $this->aEventInputMimeType->removeEventTypeRelatedByInputMimeId($this);
        }
        if (null !== $this->aEventOutputMimeType) {
            $this->aEventOutputMimeType->removeEventTypeRelatedByOutputMimeId($this);
        }
        $this->id = null;
        $this->executable = null;
        $this->is_generic = null;
        $this->input_mime_id = null;
        $this->output_mime_id = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collEventTypeSupportedMoments) {
                foreach ($this->collEventTypeSupportedMoments as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collEventHandlers) {
                foreach ($this->collEventHandlers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collEventTypeSupportedMoments = null;
        $this->collEventHandlers = null;
        $this->aEventInputMimeType = null;
        $this->aEventOutputMimeType = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(EventTypeTableMap::DEFAULT_STRING_FORMAT);
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
