<?php

namespace Model\System\Event\Map;

use Model\System\Event\EventTypeSupportedMoment;
use Model\System\Event\EventTypeSupportedMomentQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'event_type_supported_trigger' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class EventTypeSupportedMomentTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.System.Event.Map.EventTypeSupportedMomentTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'event_type_supported_trigger';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\System\\Event\\EventTypeSupportedMoment';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.System.Event.EventTypeSupportedMoment';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 3;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 3;

    /**
     * the column name for the id field
     */
    const COL_ID = 'event_type_supported_trigger.id';

    /**
     * the column name for the event_type_id field
     */
    const COL_EVENT_TYPE_ID = 'event_type_supported_trigger.event_type_id';

    /**
     * the column name for the event_trigger_id field
     */
    const COL_EVENT_TRIGGER_ID = 'event_type_supported_trigger.event_trigger_id';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'EventTypeId', 'EventTriggerId', ),
        self::TYPE_CAMELNAME     => array('id', 'eventTypeId', 'eventTriggerId', ),
        self::TYPE_COLNAME       => array(EventTypeSupportedMomentTableMap::COL_ID, EventTypeSupportedMomentTableMap::COL_EVENT_TYPE_ID, EventTypeSupportedMomentTableMap::COL_EVENT_TRIGGER_ID, ),
        self::TYPE_FIELDNAME     => array('id', 'event_type_id', 'event_trigger_id', ),
        self::TYPE_NUM           => array(0, 1, 2, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'EventTypeId' => 1, 'EventTriggerId' => 2, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'eventTypeId' => 1, 'eventTriggerId' => 2, ),
        self::TYPE_COLNAME       => array(EventTypeSupportedMomentTableMap::COL_ID => 0, EventTypeSupportedMomentTableMap::COL_EVENT_TYPE_ID => 1, EventTypeSupportedMomentTableMap::COL_EVENT_TRIGGER_ID => 2, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'event_type_id' => 1, 'event_trigger_id' => 2, ),
        self::TYPE_NUM           => array(0, 1, 2, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('event_type_supported_trigger');
        $this->setPhpName('EventTypeSupportedMoment');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\System\\Event\\EventTypeSupportedMoment');
        $this->setPackage('Model.System.Event');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('event_type_id', 'EventTypeId', 'INTEGER', 'event_type', 'id', true, null, null);
        $this->addForeignKey('event_trigger_id', 'EventTriggerId', 'INTEGER', 'event_trigger', 'id', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('EventTriggerTypeEventType', '\\Model\\System\\Event\\EventType', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':event_type_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
        $this->addRelation('EventTriggerTypeEvent', '\\Model\\System\\Event\\EventTrigger', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':event_trigger_id',
    1 => ':id',
  ),
), 'RESTRICT', null, null, false);
    } // buildRelations()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? EventTypeSupportedMomentTableMap::CLASS_DEFAULT : EventTypeSupportedMomentTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (EventTypeSupportedMoment object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = EventTypeSupportedMomentTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = EventTypeSupportedMomentTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + EventTypeSupportedMomentTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = EventTypeSupportedMomentTableMap::OM_CLASS;
            /** @var EventTypeSupportedMoment $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            EventTypeSupportedMomentTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = EventTypeSupportedMomentTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = EventTypeSupportedMomentTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var EventTypeSupportedMoment $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                EventTypeSupportedMomentTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(EventTypeSupportedMomentTableMap::COL_ID);
            $criteria->addSelectColumn(EventTypeSupportedMomentTableMap::COL_EVENT_TYPE_ID);
            $criteria->addSelectColumn(EventTypeSupportedMomentTableMap::COL_EVENT_TRIGGER_ID);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.event_type_id');
            $criteria->addSelectColumn($alias . '.event_trigger_id');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(EventTypeSupportedMomentTableMap::DATABASE_NAME)->getTable(EventTypeSupportedMomentTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(EventTypeSupportedMomentTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(EventTypeSupportedMomentTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new EventTypeSupportedMomentTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a EventTypeSupportedMoment or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or EventTypeSupportedMoment object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EventTypeSupportedMomentTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\System\Event\EventTypeSupportedMoment) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(EventTypeSupportedMomentTableMap::DATABASE_NAME);
            $criteria->add(EventTypeSupportedMomentTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = EventTypeSupportedMomentQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            EventTypeSupportedMomentTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                EventTypeSupportedMomentTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the event_type_supported_trigger table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return EventTypeSupportedMomentQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a EventTypeSupportedMoment or Criteria object.
     *
     * @param mixed               $criteria Criteria or EventTypeSupportedMoment object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(EventTypeSupportedMomentTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from EventTypeSupportedMoment object
        }

        if ($criteria->containsKey(EventTypeSupportedMomentTableMap::COL_ID) && $criteria->keyContainsValue(EventTypeSupportedMomentTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.EventTypeSupportedMomentTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = EventTypeSupportedMomentQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // EventTypeSupportedMomentTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
EventTypeSupportedMomentTableMap::buildTableMap();
