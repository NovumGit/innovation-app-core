<?php

namespace Model\System\LowCode\View;

use Model\System\LowCode\View\Base\Component_viewQuery as BaseComponent_viewQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'component_view' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_viewQuery extends BaseComponent_viewQuery
{

}
