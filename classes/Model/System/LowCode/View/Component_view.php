<?php

namespace Model\System\LowCode\View;

use Model\System\LowCode\View\Base\Component_view as BaseComponent_view;

/**
 * Skeleton subclass for representing a row from the 'component_view' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_view extends BaseComponent_view
{

}
