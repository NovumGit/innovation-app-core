<?php

namespace Model\System\LowCode\TableFormRow;

use Model\System\LowCode\TableFormRow\Base\Component_table_form_rowQuery as BaseComponent_table_form_rowQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'component_table_form_row' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_table_form_rowQuery extends BaseComponent_table_form_rowQuery
{

}
