<?php

namespace Model\System\LowCode\TableFormRow\Base;

use \Exception;
use \PDO;
use Model\System\LowCode\TableFormRow\Component_table_form_row as ChildComponent_table_form_row;
use Model\System\LowCode\TableFormRow\Component_table_form_rowQuery as ChildComponent_table_form_rowQuery;
use Model\System\LowCode\TableFormRow\Map\Component_table_form_rowTableMap;
use Model\System\UI\UIComponent;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'component_table_form_row' table.
 *
 *
 *
 * @method     ChildComponent_table_form_rowQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method     ChildComponent_table_form_rowQuery orderByLayoutKey($order = Criteria::ASC) Order by the layout_key column
 * @method     ChildComponent_table_form_rowQuery orderByRenderStyle($order = Criteria::ASC) Order by the render_style column
 * @method     ChildComponent_table_form_rowQuery orderByUiComponentId($order = Criteria::ASC) Order by the ui_component_id column
 * @method     ChildComponent_table_form_rowQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildComponent_table_form_rowQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildComponent_table_form_rowQuery groupByid() Group by the id column
 * @method     ChildComponent_table_form_rowQuery groupByLayoutKey() Group by the layout_key column
 * @method     ChildComponent_table_form_rowQuery groupByRenderStyle() Group by the render_style column
 * @method     ChildComponent_table_form_rowQuery groupByUiComponentId() Group by the ui_component_id column
 * @method     ChildComponent_table_form_rowQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildComponent_table_form_rowQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildComponent_table_form_rowQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildComponent_table_form_rowQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildComponent_table_form_rowQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildComponent_table_form_rowQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildComponent_table_form_rowQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildComponent_table_form_rowQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildComponent_table_form_rowQuery leftJoinId($relationAlias = null) Adds a LEFT JOIN clause to the query using the Id relation
 * @method     ChildComponent_table_form_rowQuery rightJoinId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Id relation
 * @method     ChildComponent_table_form_rowQuery innerJoinId($relationAlias = null) Adds a INNER JOIN clause to the query using the Id relation
 *
 * @method     ChildComponent_table_form_rowQuery joinWithId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Id relation
 *
 * @method     ChildComponent_table_form_rowQuery leftJoinWithId() Adds a LEFT JOIN clause and with to the query using the Id relation
 * @method     ChildComponent_table_form_rowQuery rightJoinWithId() Adds a RIGHT JOIN clause and with to the query using the Id relation
 * @method     ChildComponent_table_form_rowQuery innerJoinWithId() Adds a INNER JOIN clause and with to the query using the Id relation
 *
 * @method     \Model\System\UI\UIComponentQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildComponent_table_form_row findOne(ConnectionInterface $con = null) Return the first ChildComponent_table_form_row matching the query
 * @method     ChildComponent_table_form_row findOneOrCreate(ConnectionInterface $con = null) Return the first ChildComponent_table_form_row matching the query, or a new ChildComponent_table_form_row object populated from the query conditions when no match is found
 *
 * @method     ChildComponent_table_form_row findOneByid(int $id) Return the first ChildComponent_table_form_row filtered by the id column
 * @method     ChildComponent_table_form_row findOneByLayoutKey(string $layout_key) Return the first ChildComponent_table_form_row filtered by the layout_key column
 * @method     ChildComponent_table_form_row findOneByRenderStyle(boolean $render_style) Return the first ChildComponent_table_form_row filtered by the render_style column
 * @method     ChildComponent_table_form_row findOneByUiComponentId(int $ui_component_id) Return the first ChildComponent_table_form_row filtered by the ui_component_id column
 * @method     ChildComponent_table_form_row findOneByCreatedAt(string $created_at) Return the first ChildComponent_table_form_row filtered by the created_at column
 * @method     ChildComponent_table_form_row findOneByUpdatedAt(string $updated_at) Return the first ChildComponent_table_form_row filtered by the updated_at column *

 * @method     ChildComponent_table_form_row requirePk($key, ConnectionInterface $con = null) Return the ChildComponent_table_form_row by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_table_form_row requireOne(ConnectionInterface $con = null) Return the first ChildComponent_table_form_row matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComponent_table_form_row requireOneByid(int $id) Return the first ChildComponent_table_form_row filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_table_form_row requireOneByLayoutKey(string $layout_key) Return the first ChildComponent_table_form_row filtered by the layout_key column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_table_form_row requireOneByRenderStyle(boolean $render_style) Return the first ChildComponent_table_form_row filtered by the render_style column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_table_form_row requireOneByUiComponentId(int $ui_component_id) Return the first ChildComponent_table_form_row filtered by the ui_component_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_table_form_row requireOneByCreatedAt(string $created_at) Return the first ChildComponent_table_form_row filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_table_form_row requireOneByUpdatedAt(string $updated_at) Return the first ChildComponent_table_form_row filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComponent_table_form_row[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildComponent_table_form_row objects based on current ModelCriteria
 * @method     ChildComponent_table_form_row[]|ObjectCollection findByid(int $id) Return ChildComponent_table_form_row objects filtered by the id column
 * @method     ChildComponent_table_form_row[]|ObjectCollection findByLayoutKey(string $layout_key) Return ChildComponent_table_form_row objects filtered by the layout_key column
 * @method     ChildComponent_table_form_row[]|ObjectCollection findByRenderStyle(boolean $render_style) Return ChildComponent_table_form_row objects filtered by the render_style column
 * @method     ChildComponent_table_form_row[]|ObjectCollection findByUiComponentId(int $ui_component_id) Return ChildComponent_table_form_row objects filtered by the ui_component_id column
 * @method     ChildComponent_table_form_row[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildComponent_table_form_row objects filtered by the created_at column
 * @method     ChildComponent_table_form_row[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildComponent_table_form_row objects filtered by the updated_at column
 * @method     ChildComponent_table_form_row[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class Component_table_form_rowQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\LowCode\TableFormRow\Base\Component_table_form_rowQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\LowCode\\TableFormRow\\Component_table_form_row', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildComponent_table_form_rowQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildComponent_table_form_rowQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildComponent_table_form_rowQuery) {
            return $criteria;
        }
        $query = new ChildComponent_table_form_rowQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildComponent_table_form_row|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(Component_table_form_rowTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = Component_table_form_rowTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_table_form_row A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, layout_key, render_style, ui_component_id, created_at, updated_at FROM component_table_form_row WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildComponent_table_form_row $obj */
            $obj = new ChildComponent_table_form_row();
            $obj->hydrate($row);
            Component_table_form_rowTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildComponent_table_form_row|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(Component_table_form_rowTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(Component_table_form_rowTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(Component_table_form_rowTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(Component_table_form_rowTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_table_form_rowTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the layout_key column
     *
     * Example usage:
     * <code>
     * $query->filterByLayoutKey('fooValue');   // WHERE layout_key = 'fooValue'
     * $query->filterByLayoutKey('%fooValue%', Criteria::LIKE); // WHERE layout_key LIKE '%fooValue%'
     * </code>
     *
     * @param     string $layoutKey The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function filterByLayoutKey($layoutKey = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($layoutKey)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_table_form_rowTableMap::COL_LAYOUT_KEY, $layoutKey, $comparison);
    }

    /**
     * Filter the query on the render_style column
     *
     * Example usage:
     * <code>
     * $query->filterByRenderStyle(true); // WHERE render_style = true
     * $query->filterByRenderStyle('yes'); // WHERE render_style = true
     * </code>
     *
     * @param     boolean|string $renderStyle The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function filterByRenderStyle($renderStyle = null, $comparison = null)
    {
        if (is_string($renderStyle)) {
            $renderStyle = in_array(strtolower($renderStyle), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(Component_table_form_rowTableMap::COL_RENDER_STYLE, $renderStyle, $comparison);
    }

    /**
     * Filter the query on the ui_component_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUiComponentId(1234); // WHERE ui_component_id = 1234
     * $query->filterByUiComponentId(array(12, 34)); // WHERE ui_component_id IN (12, 34)
     * $query->filterByUiComponentId(array('min' => 12)); // WHERE ui_component_id > 12
     * </code>
     *
     * @see       filterById()
     *
     * @param     mixed $uiComponentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function filterByUiComponentId($uiComponentId = null, $comparison = null)
    {
        if (is_array($uiComponentId)) {
            $useMinMax = false;
            if (isset($uiComponentId['min'])) {
                $this->addUsingAlias(Component_table_form_rowTableMap::COL_UI_COMPONENT_ID, $uiComponentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uiComponentId['max'])) {
                $this->addUsingAlias(Component_table_form_rowTableMap::COL_UI_COMPONENT_ID, $uiComponentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_table_form_rowTableMap::COL_UI_COMPONENT_ID, $uiComponentId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(Component_table_form_rowTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(Component_table_form_rowTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_table_form_rowTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(Component_table_form_rowTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(Component_table_form_rowTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_table_form_rowTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\UI\UIComponent object
     *
     * @param \Model\System\UI\UIComponent|ObjectCollection $uIComponent The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function filterById($uIComponent, $comparison = null)
    {
        if ($uIComponent instanceof \Model\System\UI\UIComponent) {
            return $this
                ->addUsingAlias(Component_table_form_rowTableMap::COL_UI_COMPONENT_ID, $uIComponent->getId(), $comparison);
        } elseif ($uIComponent instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Component_table_form_rowTableMap::COL_UI_COMPONENT_ID, $uIComponent->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterById() only accepts arguments of type \Model\System\UI\UIComponent or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Id relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function joinId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Id');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Id');
        }

        return $this;
    }

    /**
     * Use the Id relation UIComponent object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\UI\UIComponentQuery A secondary query class using the current class as primary query
     */
    public function useIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Id', '\Model\System\UI\UIComponentQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildComponent_table_form_row $component_table_form_row Object to remove from the list of results
     *
     * @return $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function prune($component_table_form_row = null)
    {
        if ($component_table_form_row) {
            $this->addUsingAlias(Component_table_form_rowTableMap::COL_ID, $component_table_form_row->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the component_table_form_row table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_table_form_rowTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            Component_table_form_rowTableMap::clearInstancePool();
            Component_table_form_rowTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_table_form_rowTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(Component_table_form_rowTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            Component_table_form_rowTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            Component_table_form_rowTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(Component_table_form_rowTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(Component_table_form_rowTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(Component_table_form_rowTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(Component_table_form_rowTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(Component_table_form_rowTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildComponent_table_form_rowQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(Component_table_form_rowTableMap::COL_CREATED_AT);
    }

} // Component_table_form_rowQuery
