<?php

namespace Model\System\LowCode\Table;

use Model\System\LowCode\Table\Base\Component_table_default_sorting_columnQuery as BaseComponent_table_default_sorting_columnQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'component_table_default_sorting_column' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_table_default_sorting_columnQuery extends BaseComponent_table_default_sorting_columnQuery
{

}
