<?php

namespace Model\System\LowCode\MindMap\Map;

use Model\System\LowCode\MindMap\Component_mind_map;
use Model\System\LowCode\MindMap\Component_mind_mapQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'component_mind_map' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class Component_mind_mapTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.System.LowCode.MindMap.Map.Component_mind_mapTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'component_mind_map';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\System\\LowCode\\MindMap\\Component_mind_map';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.System.LowCode.MindMap.Component_mind_map';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 8;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 8;

    /**
     * the column name for the id field
     */
    const COL_ID = 'component_mind_map.id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'component_mind_map.title';

    /**
     * the column name for the root_label field
     */
    const COL_ROOT_LABEL = 'component_mind_map.root_label';

    /**
     * the column name for the query_object field
     */
    const COL_QUERY_OBJECT = 'component_mind_map.query_object';

    /**
     * the column name for the label_value field
     */
    const COL_LABEL_VALUE = 'component_mind_map.label_value';

    /**
     * the column name for the ui_component_id field
     */
    const COL_UI_COMPONENT_ID = 'component_mind_map.ui_component_id';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'component_mind_map.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'component_mind_map.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('id', 'Title', 'RootLabel', 'QueryObject', 'LabelValue', 'UiComponentId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'title', 'rootLabel', 'queryObject', 'labelValue', 'uiComponentId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(Component_mind_mapTableMap::COL_ID, Component_mind_mapTableMap::COL_TITLE, Component_mind_mapTableMap::COL_ROOT_LABEL, Component_mind_mapTableMap::COL_QUERY_OBJECT, Component_mind_mapTableMap::COL_LABEL_VALUE, Component_mind_mapTableMap::COL_UI_COMPONENT_ID, Component_mind_mapTableMap::COL_CREATED_AT, Component_mind_mapTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'title', 'root_label', 'query_object', 'label_value', 'ui_component_id', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('id' => 0, 'Title' => 1, 'RootLabel' => 2, 'QueryObject' => 3, 'LabelValue' => 4, 'UiComponentId' => 5, 'CreatedAt' => 6, 'UpdatedAt' => 7, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'title' => 1, 'rootLabel' => 2, 'queryObject' => 3, 'labelValue' => 4, 'uiComponentId' => 5, 'createdAt' => 6, 'updatedAt' => 7, ),
        self::TYPE_COLNAME       => array(Component_mind_mapTableMap::COL_ID => 0, Component_mind_mapTableMap::COL_TITLE => 1, Component_mind_mapTableMap::COL_ROOT_LABEL => 2, Component_mind_mapTableMap::COL_QUERY_OBJECT => 3, Component_mind_mapTableMap::COL_LABEL_VALUE => 4, Component_mind_mapTableMap::COL_UI_COMPONENT_ID => 5, Component_mind_mapTableMap::COL_CREATED_AT => 6, Component_mind_mapTableMap::COL_UPDATED_AT => 7, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'title' => 1, 'root_label' => 2, 'query_object' => 3, 'label_value' => 4, 'ui_component_id' => 5, 'created_at' => 6, 'updated_at' => 7, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('component_mind_map');
        $this->setPhpName('Component_mind_map');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\System\\LowCode\\MindMap\\Component_mind_map');
        $this->setPackage('Model.System.LowCode.MindMap');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'id', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 255, null);
        $this->addColumn('root_label', 'RootLabel', 'VARCHAR', false, 255, null);
        $this->addColumn('query_object', 'QueryObject', 'VARCHAR', false, 255, null);
        $this->addColumn('label_value', 'LabelValue', 'VARCHAR', false, 255, null);
        $this->addForeignKey('ui_component_id', 'UiComponentId', 'INTEGER', 'ui_component', 'id', true, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Id', '\\Model\\System\\UI\\UIComponent', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':ui_component_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? Component_mind_mapTableMap::CLASS_DEFAULT : Component_mind_mapTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Component_mind_map object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = Component_mind_mapTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = Component_mind_mapTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + Component_mind_mapTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = Component_mind_mapTableMap::OM_CLASS;
            /** @var Component_mind_map $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            Component_mind_mapTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = Component_mind_mapTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = Component_mind_mapTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Component_mind_map $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                Component_mind_mapTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(Component_mind_mapTableMap::COL_ID);
            $criteria->addSelectColumn(Component_mind_mapTableMap::COL_TITLE);
            $criteria->addSelectColumn(Component_mind_mapTableMap::COL_ROOT_LABEL);
            $criteria->addSelectColumn(Component_mind_mapTableMap::COL_QUERY_OBJECT);
            $criteria->addSelectColumn(Component_mind_mapTableMap::COL_LABEL_VALUE);
            $criteria->addSelectColumn(Component_mind_mapTableMap::COL_UI_COMPONENT_ID);
            $criteria->addSelectColumn(Component_mind_mapTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(Component_mind_mapTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.root_label');
            $criteria->addSelectColumn($alias . '.query_object');
            $criteria->addSelectColumn($alias . '.label_value');
            $criteria->addSelectColumn($alias . '.ui_component_id');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(Component_mind_mapTableMap::DATABASE_NAME)->getTable(Component_mind_mapTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(Component_mind_mapTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(Component_mind_mapTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new Component_mind_mapTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Component_mind_map or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Component_mind_map object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_mind_mapTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\System\LowCode\MindMap\Component_mind_map) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(Component_mind_mapTableMap::DATABASE_NAME);
            $criteria->add(Component_mind_mapTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = Component_mind_mapQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            Component_mind_mapTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                Component_mind_mapTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the component_mind_map table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return Component_mind_mapQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Component_mind_map or Criteria object.
     *
     * @param mixed               $criteria Criteria or Component_mind_map object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_mind_mapTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Component_mind_map object
        }

        if ($criteria->containsKey(Component_mind_mapTableMap::COL_ID) && $criteria->keyContainsValue(Component_mind_mapTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.Component_mind_mapTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = Component_mind_mapQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // Component_mind_mapTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
Component_mind_mapTableMap::buildTableMap();
