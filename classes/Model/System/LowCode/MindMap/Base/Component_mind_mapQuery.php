<?php

namespace Model\System\LowCode\MindMap\Base;

use \Exception;
use \PDO;
use Model\System\LowCode\MindMap\Component_mind_map as ChildComponent_mind_map;
use Model\System\LowCode\MindMap\Component_mind_mapQuery as ChildComponent_mind_mapQuery;
use Model\System\LowCode\MindMap\Map\Component_mind_mapTableMap;
use Model\System\UI\UIComponent;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'component_mind_map' table.
 *
 *
 *
 * @method     ChildComponent_mind_mapQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method     ChildComponent_mind_mapQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildComponent_mind_mapQuery orderByRootLabel($order = Criteria::ASC) Order by the root_label column
 * @method     ChildComponent_mind_mapQuery orderByQueryObject($order = Criteria::ASC) Order by the query_object column
 * @method     ChildComponent_mind_mapQuery orderByLabelValue($order = Criteria::ASC) Order by the label_value column
 * @method     ChildComponent_mind_mapQuery orderByUiComponentId($order = Criteria::ASC) Order by the ui_component_id column
 * @method     ChildComponent_mind_mapQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildComponent_mind_mapQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildComponent_mind_mapQuery groupByid() Group by the id column
 * @method     ChildComponent_mind_mapQuery groupByTitle() Group by the title column
 * @method     ChildComponent_mind_mapQuery groupByRootLabel() Group by the root_label column
 * @method     ChildComponent_mind_mapQuery groupByQueryObject() Group by the query_object column
 * @method     ChildComponent_mind_mapQuery groupByLabelValue() Group by the label_value column
 * @method     ChildComponent_mind_mapQuery groupByUiComponentId() Group by the ui_component_id column
 * @method     ChildComponent_mind_mapQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildComponent_mind_mapQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildComponent_mind_mapQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildComponent_mind_mapQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildComponent_mind_mapQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildComponent_mind_mapQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildComponent_mind_mapQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildComponent_mind_mapQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildComponent_mind_mapQuery leftJoinId($relationAlias = null) Adds a LEFT JOIN clause to the query using the Id relation
 * @method     ChildComponent_mind_mapQuery rightJoinId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Id relation
 * @method     ChildComponent_mind_mapQuery innerJoinId($relationAlias = null) Adds a INNER JOIN clause to the query using the Id relation
 *
 * @method     ChildComponent_mind_mapQuery joinWithId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Id relation
 *
 * @method     ChildComponent_mind_mapQuery leftJoinWithId() Adds a LEFT JOIN clause and with to the query using the Id relation
 * @method     ChildComponent_mind_mapQuery rightJoinWithId() Adds a RIGHT JOIN clause and with to the query using the Id relation
 * @method     ChildComponent_mind_mapQuery innerJoinWithId() Adds a INNER JOIN clause and with to the query using the Id relation
 *
 * @method     \Model\System\UI\UIComponentQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildComponent_mind_map findOne(ConnectionInterface $con = null) Return the first ChildComponent_mind_map matching the query
 * @method     ChildComponent_mind_map findOneOrCreate(ConnectionInterface $con = null) Return the first ChildComponent_mind_map matching the query, or a new ChildComponent_mind_map object populated from the query conditions when no match is found
 *
 * @method     ChildComponent_mind_map findOneByid(int $id) Return the first ChildComponent_mind_map filtered by the id column
 * @method     ChildComponent_mind_map findOneByTitle(string $title) Return the first ChildComponent_mind_map filtered by the title column
 * @method     ChildComponent_mind_map findOneByRootLabel(string $root_label) Return the first ChildComponent_mind_map filtered by the root_label column
 * @method     ChildComponent_mind_map findOneByQueryObject(string $query_object) Return the first ChildComponent_mind_map filtered by the query_object column
 * @method     ChildComponent_mind_map findOneByLabelValue(string $label_value) Return the first ChildComponent_mind_map filtered by the label_value column
 * @method     ChildComponent_mind_map findOneByUiComponentId(int $ui_component_id) Return the first ChildComponent_mind_map filtered by the ui_component_id column
 * @method     ChildComponent_mind_map findOneByCreatedAt(string $created_at) Return the first ChildComponent_mind_map filtered by the created_at column
 * @method     ChildComponent_mind_map findOneByUpdatedAt(string $updated_at) Return the first ChildComponent_mind_map filtered by the updated_at column *

 * @method     ChildComponent_mind_map requirePk($key, ConnectionInterface $con = null) Return the ChildComponent_mind_map by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_mind_map requireOne(ConnectionInterface $con = null) Return the first ChildComponent_mind_map matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComponent_mind_map requireOneByid(int $id) Return the first ChildComponent_mind_map filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_mind_map requireOneByTitle(string $title) Return the first ChildComponent_mind_map filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_mind_map requireOneByRootLabel(string $root_label) Return the first ChildComponent_mind_map filtered by the root_label column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_mind_map requireOneByQueryObject(string $query_object) Return the first ChildComponent_mind_map filtered by the query_object column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_mind_map requireOneByLabelValue(string $label_value) Return the first ChildComponent_mind_map filtered by the label_value column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_mind_map requireOneByUiComponentId(int $ui_component_id) Return the first ChildComponent_mind_map filtered by the ui_component_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_mind_map requireOneByCreatedAt(string $created_at) Return the first ChildComponent_mind_map filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_mind_map requireOneByUpdatedAt(string $updated_at) Return the first ChildComponent_mind_map filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComponent_mind_map[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildComponent_mind_map objects based on current ModelCriteria
 * @method     ChildComponent_mind_map[]|ObjectCollection findByid(int $id) Return ChildComponent_mind_map objects filtered by the id column
 * @method     ChildComponent_mind_map[]|ObjectCollection findByTitle(string $title) Return ChildComponent_mind_map objects filtered by the title column
 * @method     ChildComponent_mind_map[]|ObjectCollection findByRootLabel(string $root_label) Return ChildComponent_mind_map objects filtered by the root_label column
 * @method     ChildComponent_mind_map[]|ObjectCollection findByQueryObject(string $query_object) Return ChildComponent_mind_map objects filtered by the query_object column
 * @method     ChildComponent_mind_map[]|ObjectCollection findByLabelValue(string $label_value) Return ChildComponent_mind_map objects filtered by the label_value column
 * @method     ChildComponent_mind_map[]|ObjectCollection findByUiComponentId(int $ui_component_id) Return ChildComponent_mind_map objects filtered by the ui_component_id column
 * @method     ChildComponent_mind_map[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildComponent_mind_map objects filtered by the created_at column
 * @method     ChildComponent_mind_map[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildComponent_mind_map objects filtered by the updated_at column
 * @method     ChildComponent_mind_map[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class Component_mind_mapQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\LowCode\MindMap\Base\Component_mind_mapQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\LowCode\\MindMap\\Component_mind_map', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildComponent_mind_mapQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildComponent_mind_mapQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildComponent_mind_mapQuery) {
            return $criteria;
        }
        $query = new ChildComponent_mind_mapQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildComponent_mind_map|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(Component_mind_mapTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = Component_mind_mapTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_mind_map A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, title, root_label, query_object, label_value, ui_component_id, created_at, updated_at FROM component_mind_map WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildComponent_mind_map $obj */
            $obj = new ChildComponent_mind_map();
            $obj->hydrate($row);
            Component_mind_mapTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildComponent_mind_map|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(Component_mind_mapTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(Component_mind_mapTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(Component_mind_mapTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(Component_mind_mapTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_mind_mapTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_mind_mapTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the root_label column
     *
     * Example usage:
     * <code>
     * $query->filterByRootLabel('fooValue');   // WHERE root_label = 'fooValue'
     * $query->filterByRootLabel('%fooValue%', Criteria::LIKE); // WHERE root_label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $rootLabel The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function filterByRootLabel($rootLabel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($rootLabel)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_mind_mapTableMap::COL_ROOT_LABEL, $rootLabel, $comparison);
    }

    /**
     * Filter the query on the query_object column
     *
     * Example usage:
     * <code>
     * $query->filterByQueryObject('fooValue');   // WHERE query_object = 'fooValue'
     * $query->filterByQueryObject('%fooValue%', Criteria::LIKE); // WHERE query_object LIKE '%fooValue%'
     * </code>
     *
     * @param     string $queryObject The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function filterByQueryObject($queryObject = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($queryObject)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_mind_mapTableMap::COL_QUERY_OBJECT, $queryObject, $comparison);
    }

    /**
     * Filter the query on the label_value column
     *
     * Example usage:
     * <code>
     * $query->filterByLabelValue('fooValue');   // WHERE label_value = 'fooValue'
     * $query->filterByLabelValue('%fooValue%', Criteria::LIKE); // WHERE label_value LIKE '%fooValue%'
     * </code>
     *
     * @param     string $labelValue The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function filterByLabelValue($labelValue = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($labelValue)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_mind_mapTableMap::COL_LABEL_VALUE, $labelValue, $comparison);
    }

    /**
     * Filter the query on the ui_component_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUiComponentId(1234); // WHERE ui_component_id = 1234
     * $query->filterByUiComponentId(array(12, 34)); // WHERE ui_component_id IN (12, 34)
     * $query->filterByUiComponentId(array('min' => 12)); // WHERE ui_component_id > 12
     * </code>
     *
     * @see       filterById()
     *
     * @param     mixed $uiComponentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function filterByUiComponentId($uiComponentId = null, $comparison = null)
    {
        if (is_array($uiComponentId)) {
            $useMinMax = false;
            if (isset($uiComponentId['min'])) {
                $this->addUsingAlias(Component_mind_mapTableMap::COL_UI_COMPONENT_ID, $uiComponentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uiComponentId['max'])) {
                $this->addUsingAlias(Component_mind_mapTableMap::COL_UI_COMPONENT_ID, $uiComponentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_mind_mapTableMap::COL_UI_COMPONENT_ID, $uiComponentId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(Component_mind_mapTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(Component_mind_mapTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_mind_mapTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(Component_mind_mapTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(Component_mind_mapTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_mind_mapTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\UI\UIComponent object
     *
     * @param \Model\System\UI\UIComponent|ObjectCollection $uIComponent The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function filterById($uIComponent, $comparison = null)
    {
        if ($uIComponent instanceof \Model\System\UI\UIComponent) {
            return $this
                ->addUsingAlias(Component_mind_mapTableMap::COL_UI_COMPONENT_ID, $uIComponent->getId(), $comparison);
        } elseif ($uIComponent instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Component_mind_mapTableMap::COL_UI_COMPONENT_ID, $uIComponent->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterById() only accepts arguments of type \Model\System\UI\UIComponent or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Id relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function joinId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Id');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Id');
        }

        return $this;
    }

    /**
     * Use the Id relation UIComponent object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\UI\UIComponentQuery A secondary query class using the current class as primary query
     */
    public function useIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Id', '\Model\System\UI\UIComponentQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildComponent_mind_map $component_mind_map Object to remove from the list of results
     *
     * @return $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function prune($component_mind_map = null)
    {
        if ($component_mind_map) {
            $this->addUsingAlias(Component_mind_mapTableMap::COL_ID, $component_mind_map->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the component_mind_map table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_mind_mapTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            Component_mind_mapTableMap::clearInstancePool();
            Component_mind_mapTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_mind_mapTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(Component_mind_mapTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            Component_mind_mapTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            Component_mind_mapTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(Component_mind_mapTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(Component_mind_mapTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(Component_mind_mapTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(Component_mind_mapTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(Component_mind_mapTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildComponent_mind_mapQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(Component_mind_mapTableMap::COL_CREATED_AT);
    }

} // Component_mind_mapQuery
