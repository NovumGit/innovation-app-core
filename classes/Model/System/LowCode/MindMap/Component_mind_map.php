<?php

namespace Model\System\LowCode\MindMap;

use Model\System\LowCode\MindMap\Base\Component_mind_map as BaseComponent_mind_map;

/**
 * Skeleton subclass for representing a row from the 'component_mind_map' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_mind_map extends BaseComponent_mind_map
{

}
