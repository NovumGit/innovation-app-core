<?php

namespace Model\System\LowCode\Button;

use Model\System\LowCode\Button\Base\Component_button_pageQuery as BaseComponent_button_pageQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'component_button_page' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_button_pageQuery extends BaseComponent_button_pageQuery
{

}
