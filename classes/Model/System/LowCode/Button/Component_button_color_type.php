<?php

namespace Model\System\LowCode\Button;

use Model\System\LowCode\Button\Base\Component_button_color_type as BaseComponent_button_color_type;

/**
 * Skeleton subclass for representing a row from the 'component_button_color_type' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_button_color_type extends BaseComponent_button_color_type
{

}
