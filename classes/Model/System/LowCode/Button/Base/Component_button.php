<?php

namespace Model\System\LowCode\Button\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\System\LowCode\Button\Component_button as ChildComponent_button;
use Model\System\LowCode\Button\Component_buttonQuery as ChildComponent_buttonQuery;
use Model\System\LowCode\Button\Component_button_align as ChildComponent_button_align;
use Model\System\LowCode\Button\Component_button_alignQuery as ChildComponent_button_alignQuery;
use Model\System\LowCode\Button\Component_button_color_type as ChildComponent_button_color_type;
use Model\System\LowCode\Button\Component_button_color_typeQuery as ChildComponent_button_color_typeQuery;
use Model\System\LowCode\Button\Component_button_event as ChildComponent_button_event;
use Model\System\LowCode\Button\Component_button_eventQuery as ChildComponent_button_eventQuery;
use Model\System\LowCode\Button\Component_button_page as ChildComponent_button_page;
use Model\System\LowCode\Button\Component_button_pageQuery as ChildComponent_button_pageQuery;
use Model\System\LowCode\Button\Component_button_size as ChildComponent_button_size;
use Model\System\LowCode\Button\Component_button_sizeQuery as ChildComponent_button_sizeQuery;
use Model\System\LowCode\Button\Map\Component_buttonTableMap;
use Model\System\UI\UIComponent;
use Model\System\UI\UIComponentQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'component_button' table.
 *
 *
 *
 * @package    propel.generator.Model.System.LowCode.Button.Base
 */
abstract class Component_button implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\System\\LowCode\\Button\\Map\\Component_buttonTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the info field.
     *
     * @var        string|null
     */
    protected $info;

    /**
     * The value for the title field.
     *
     * @var        string|null
     */
    protected $title;

    /**
     * The value for the label field.
     *
     * @var        string|null
     */
    protected $label;

    /**
     * The value for the fk_component_button_size field.
     *
     * @var        int|null
     */
    protected $fk_component_button_size;

    /**
     * The value for the fk_component_button_align field.
     *
     * @var        int|null
     */
    protected $fk_component_button_align;

    /**
     * The value for the fk_component_button_color_type field.
     *
     * @var        int|null
     */
    protected $fk_component_button_color_type;

    /**
     * The value for the icon field.
     *
     * @var        string|null
     */
    protected $icon;

    /**
     * The value for the fk_component_button_event field.
     *
     * @var        int|null
     */
    protected $fk_component_button_event;

    /**
     * The value for the fk_component_button_page field.
     *
     * @var        int|null
     */
    protected $fk_component_button_page;

    /**
     * The value for the flow field.
     *
     * @var        string|null
     */
    protected $flow;

    /**
     * The value for the ui_component_id field.
     *
     * @var        int
     */
    protected $ui_component_id;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * @var        ChildComponent_button_size
     */
    protected $aComponentButtonSize;

    /**
     * @var        ChildComponent_button_align
     */
    protected $aComponentButtonAlign;

    /**
     * @var        ChildComponent_button_color_type
     */
    protected $aComponentButtonColorType;

    /**
     * @var        ChildComponent_button_event
     */
    protected $aComponentButtonEvent;

    /**
     * @var        ChildComponent_button_page
     */
    protected $aComponentButtonPage;

    /**
     * @var        UIComponent
     */
    protected $aId;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Model\System\LowCode\Button\Base\Component_button object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Component_button</code> instance.  If
     * <code>obj</code> is an instance of <code>Component_button</code>, delegates to
     * <code>equals(Component_button)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getid()
    {
        return $this->id;
    }

    /**
     * Get the [info] column value.
     *
     * @return string|null
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Get the [title] column value.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get the [label] column value.
     *
     * @return string|null
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Get the [fk_component_button_size] column value.
     *
     * @return int|null
     */
    public function getFkComponentButtonSize()
    {
        return $this->fk_component_button_size;
    }

    /**
     * Get the [fk_component_button_align] column value.
     *
     * @return int|null
     */
    public function getFkComponentButtonAlign()
    {
        return $this->fk_component_button_align;
    }

    /**
     * Get the [fk_component_button_color_type] column value.
     *
     * @return int|null
     */
    public function getFkComponentButtonColorType()
    {
        return $this->fk_component_button_color_type;
    }

    /**
     * Get the [icon] column value.
     *
     * @return string|null
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Get the [fk_component_button_event] column value.
     *
     * @return int|null
     */
    public function getFkComponentButtonEvent()
    {
        return $this->fk_component_button_event;
    }

    /**
     * Get the [fk_component_button_page] column value.
     *
     * @return int|null
     */
    public function getFkComponentButtonPage()
    {
        return $this->fk_component_button_page;
    }

    /**
     * Get the [flow] column value.
     *
     * @return string|null
     */
    public function getFlow()
    {
        return $this->flow;
    }

    /**
     * Get the [ui_component_id] column value.
     *
     * @return int
     */
    public function getUiComponentId()
    {
        return $this->ui_component_id;
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setid($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[Component_buttonTableMap::COL_ID] = true;
        }

        return $this;
    } // setid()

    /**
     * Set the value of [info] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setInfo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->info !== $v) {
            $this->info = $v;
            $this->modifiedColumns[Component_buttonTableMap::COL_INFO] = true;
        }

        return $this;
    } // setInfo()

    /**
     * Set the value of [title] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setTitle($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->title !== $v) {
            $this->title = $v;
            $this->modifiedColumns[Component_buttonTableMap::COL_TITLE] = true;
        }

        return $this;
    } // setTitle()

    /**
     * Set the value of [label] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setLabel($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->label !== $v) {
            $this->label = $v;
            $this->modifiedColumns[Component_buttonTableMap::COL_LABEL] = true;
        }

        return $this;
    } // setLabel()

    /**
     * Set the value of [fk_component_button_size] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setFkComponentButtonSize($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->fk_component_button_size !== $v) {
            $this->fk_component_button_size = $v;
            $this->modifiedColumns[Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_SIZE] = true;
        }

        if ($this->aComponentButtonSize !== null && $this->aComponentButtonSize->getid() !== $v) {
            $this->aComponentButtonSize = null;
        }

        return $this;
    } // setFkComponentButtonSize()

    /**
     * Set the value of [fk_component_button_align] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setFkComponentButtonAlign($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->fk_component_button_align !== $v) {
            $this->fk_component_button_align = $v;
            $this->modifiedColumns[Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_ALIGN] = true;
        }

        if ($this->aComponentButtonAlign !== null && $this->aComponentButtonAlign->getid() !== $v) {
            $this->aComponentButtonAlign = null;
        }

        return $this;
    } // setFkComponentButtonAlign()

    /**
     * Set the value of [fk_component_button_color_type] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setFkComponentButtonColorType($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->fk_component_button_color_type !== $v) {
            $this->fk_component_button_color_type = $v;
            $this->modifiedColumns[Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_COLOR_TYPE] = true;
        }

        if ($this->aComponentButtonColorType !== null && $this->aComponentButtonColorType->getid() !== $v) {
            $this->aComponentButtonColorType = null;
        }

        return $this;
    } // setFkComponentButtonColorType()

    /**
     * Set the value of [icon] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setIcon($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->icon !== $v) {
            $this->icon = $v;
            $this->modifiedColumns[Component_buttonTableMap::COL_ICON] = true;
        }

        return $this;
    } // setIcon()

    /**
     * Set the value of [fk_component_button_event] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setFkComponentButtonEvent($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->fk_component_button_event !== $v) {
            $this->fk_component_button_event = $v;
            $this->modifiedColumns[Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_EVENT] = true;
        }

        if ($this->aComponentButtonEvent !== null && $this->aComponentButtonEvent->getid() !== $v) {
            $this->aComponentButtonEvent = null;
        }

        return $this;
    } // setFkComponentButtonEvent()

    /**
     * Set the value of [fk_component_button_page] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setFkComponentButtonPage($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->fk_component_button_page !== $v) {
            $this->fk_component_button_page = $v;
            $this->modifiedColumns[Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_PAGE] = true;
        }

        if ($this->aComponentButtonPage !== null && $this->aComponentButtonPage->getid() !== $v) {
            $this->aComponentButtonPage = null;
        }

        return $this;
    } // setFkComponentButtonPage()

    /**
     * Set the value of [flow] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setFlow($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->flow !== $v) {
            $this->flow = $v;
            $this->modifiedColumns[Component_buttonTableMap::COL_FLOW] = true;
        }

        return $this;
    } // setFlow()

    /**
     * Set the value of [ui_component_id] column.
     *
     * @param int $v New value
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setUiComponentId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->ui_component_id !== $v) {
            $this->ui_component_id = $v;
            $this->modifiedColumns[Component_buttonTableMap::COL_UI_COMPONENT_ID] = true;
        }

        if ($this->aId !== null && $this->aId->getId() !== $v) {
            $this->aId = null;
        }

        return $this;
    } // setUiComponentId()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[Component_buttonTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[Component_buttonTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : Component_buttonTableMap::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : Component_buttonTableMap::translateFieldName('Info', TableMap::TYPE_PHPNAME, $indexType)];
            $this->info = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : Component_buttonTableMap::translateFieldName('Title', TableMap::TYPE_PHPNAME, $indexType)];
            $this->title = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : Component_buttonTableMap::translateFieldName('Label', TableMap::TYPE_PHPNAME, $indexType)];
            $this->label = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : Component_buttonTableMap::translateFieldName('FkComponentButtonSize', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fk_component_button_size = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : Component_buttonTableMap::translateFieldName('FkComponentButtonAlign', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fk_component_button_align = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : Component_buttonTableMap::translateFieldName('FkComponentButtonColorType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fk_component_button_color_type = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : Component_buttonTableMap::translateFieldName('Icon', TableMap::TYPE_PHPNAME, $indexType)];
            $this->icon = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : Component_buttonTableMap::translateFieldName('FkComponentButtonEvent', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fk_component_button_event = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : Component_buttonTableMap::translateFieldName('FkComponentButtonPage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fk_component_button_page = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : Component_buttonTableMap::translateFieldName('Flow', TableMap::TYPE_PHPNAME, $indexType)];
            $this->flow = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : Component_buttonTableMap::translateFieldName('UiComponentId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ui_component_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : Component_buttonTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : Component_buttonTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 14; // 14 = Component_buttonTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\System\\LowCode\\Button\\Component_button'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aComponentButtonSize !== null && $this->fk_component_button_size !== $this->aComponentButtonSize->getid()) {
            $this->aComponentButtonSize = null;
        }
        if ($this->aComponentButtonAlign !== null && $this->fk_component_button_align !== $this->aComponentButtonAlign->getid()) {
            $this->aComponentButtonAlign = null;
        }
        if ($this->aComponentButtonColorType !== null && $this->fk_component_button_color_type !== $this->aComponentButtonColorType->getid()) {
            $this->aComponentButtonColorType = null;
        }
        if ($this->aComponentButtonEvent !== null && $this->fk_component_button_event !== $this->aComponentButtonEvent->getid()) {
            $this->aComponentButtonEvent = null;
        }
        if ($this->aComponentButtonPage !== null && $this->fk_component_button_page !== $this->aComponentButtonPage->getid()) {
            $this->aComponentButtonPage = null;
        }
        if ($this->aId !== null && $this->ui_component_id !== $this->aId->getId()) {
            $this->aId = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(Component_buttonTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildComponent_buttonQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aComponentButtonSize = null;
            $this->aComponentButtonAlign = null;
            $this->aComponentButtonColorType = null;
            $this->aComponentButtonEvent = null;
            $this->aComponentButtonPage = null;
            $this->aId = null;
        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Component_button::setDeleted()
     * @see Component_button::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_buttonTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildComponent_buttonQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_buttonTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(Component_buttonTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(Component_buttonTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(Component_buttonTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                Component_buttonTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aComponentButtonSize !== null) {
                if ($this->aComponentButtonSize->isModified() || $this->aComponentButtonSize->isNew()) {
                    $affectedRows += $this->aComponentButtonSize->save($con);
                }
                $this->setComponentButtonSize($this->aComponentButtonSize);
            }

            if ($this->aComponentButtonAlign !== null) {
                if ($this->aComponentButtonAlign->isModified() || $this->aComponentButtonAlign->isNew()) {
                    $affectedRows += $this->aComponentButtonAlign->save($con);
                }
                $this->setComponentButtonAlign($this->aComponentButtonAlign);
            }

            if ($this->aComponentButtonColorType !== null) {
                if ($this->aComponentButtonColorType->isModified() || $this->aComponentButtonColorType->isNew()) {
                    $affectedRows += $this->aComponentButtonColorType->save($con);
                }
                $this->setComponentButtonColorType($this->aComponentButtonColorType);
            }

            if ($this->aComponentButtonEvent !== null) {
                if ($this->aComponentButtonEvent->isModified() || $this->aComponentButtonEvent->isNew()) {
                    $affectedRows += $this->aComponentButtonEvent->save($con);
                }
                $this->setComponentButtonEvent($this->aComponentButtonEvent);
            }

            if ($this->aComponentButtonPage !== null) {
                if ($this->aComponentButtonPage->isModified() || $this->aComponentButtonPage->isNew()) {
                    $affectedRows += $this->aComponentButtonPage->save($con);
                }
                $this->setComponentButtonPage($this->aComponentButtonPage);
            }

            if ($this->aId !== null) {
                if ($this->aId->isModified() || $this->aId->isNew()) {
                    $affectedRows += $this->aId->save($con);
                }
                $this->setId($this->aId);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[Component_buttonTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . Component_buttonTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(Component_buttonTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_INFO)) {
            $modifiedColumns[':p' . $index++]  = 'info';
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_TITLE)) {
            $modifiedColumns[':p' . $index++]  = 'title';
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_LABEL)) {
            $modifiedColumns[':p' . $index++]  = 'label';
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_SIZE)) {
            $modifiedColumns[':p' . $index++]  = 'fk_component_button_size';
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_ALIGN)) {
            $modifiedColumns[':p' . $index++]  = 'fk_component_button_align';
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_COLOR_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'fk_component_button_color_type';
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_ICON)) {
            $modifiedColumns[':p' . $index++]  = 'icon';
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_EVENT)) {
            $modifiedColumns[':p' . $index++]  = 'fk_component_button_event';
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_PAGE)) {
            $modifiedColumns[':p' . $index++]  = 'fk_component_button_page';
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_FLOW)) {
            $modifiedColumns[':p' . $index++]  = 'flow';
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_UI_COMPONENT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'ui_component_id';
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO component_button (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'info':
                        $stmt->bindValue($identifier, $this->info, PDO::PARAM_STR);
                        break;
                    case 'title':
                        $stmt->bindValue($identifier, $this->title, PDO::PARAM_STR);
                        break;
                    case 'label':
                        $stmt->bindValue($identifier, $this->label, PDO::PARAM_STR);
                        break;
                    case 'fk_component_button_size':
                        $stmt->bindValue($identifier, $this->fk_component_button_size, PDO::PARAM_INT);
                        break;
                    case 'fk_component_button_align':
                        $stmt->bindValue($identifier, $this->fk_component_button_align, PDO::PARAM_INT);
                        break;
                    case 'fk_component_button_color_type':
                        $stmt->bindValue($identifier, $this->fk_component_button_color_type, PDO::PARAM_INT);
                        break;
                    case 'icon':
                        $stmt->bindValue($identifier, $this->icon, PDO::PARAM_STR);
                        break;
                    case 'fk_component_button_event':
                        $stmt->bindValue($identifier, $this->fk_component_button_event, PDO::PARAM_INT);
                        break;
                    case 'fk_component_button_page':
                        $stmt->bindValue($identifier, $this->fk_component_button_page, PDO::PARAM_INT);
                        break;
                    case 'flow':
                        $stmt->bindValue($identifier, $this->flow, PDO::PARAM_STR);
                        break;
                    case 'ui_component_id':
                        $stmt->bindValue($identifier, $this->ui_component_id, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setid($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = Component_buttonTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getid();
                break;
            case 1:
                return $this->getInfo();
                break;
            case 2:
                return $this->getTitle();
                break;
            case 3:
                return $this->getLabel();
                break;
            case 4:
                return $this->getFkComponentButtonSize();
                break;
            case 5:
                return $this->getFkComponentButtonAlign();
                break;
            case 6:
                return $this->getFkComponentButtonColorType();
                break;
            case 7:
                return $this->getIcon();
                break;
            case 8:
                return $this->getFkComponentButtonEvent();
                break;
            case 9:
                return $this->getFkComponentButtonPage();
                break;
            case 10:
                return $this->getFlow();
                break;
            case 11:
                return $this->getUiComponentId();
                break;
            case 12:
                return $this->getCreatedAt();
                break;
            case 13:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Component_button'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Component_button'][$this->hashCode()] = true;
        $keys = Component_buttonTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getid(),
            $keys[1] => $this->getInfo(),
            $keys[2] => $this->getTitle(),
            $keys[3] => $this->getLabel(),
            $keys[4] => $this->getFkComponentButtonSize(),
            $keys[5] => $this->getFkComponentButtonAlign(),
            $keys[6] => $this->getFkComponentButtonColorType(),
            $keys[7] => $this->getIcon(),
            $keys[8] => $this->getFkComponentButtonEvent(),
            $keys[9] => $this->getFkComponentButtonPage(),
            $keys[10] => $this->getFlow(),
            $keys[11] => $this->getUiComponentId(),
            $keys[12] => $this->getCreatedAt(),
            $keys[13] => $this->getUpdatedAt(),
        );
        if ($result[$keys[12]] instanceof \DateTimeInterface) {
            $result[$keys[12]] = $result[$keys[12]]->format('c');
        }

        if ($result[$keys[13]] instanceof \DateTimeInterface) {
            $result[$keys[13]] = $result[$keys[13]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aComponentButtonSize) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'component_button_size';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'component_button_size';
                        break;
                    default:
                        $key = 'ComponentButtonSize';
                }

                $result[$key] = $this->aComponentButtonSize->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aComponentButtonAlign) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'component_button_align';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'component_button_align';
                        break;
                    default:
                        $key = 'ComponentButtonAlign';
                }

                $result[$key] = $this->aComponentButtonAlign->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aComponentButtonColorType) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'component_button_color_type';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'component_button_color_type';
                        break;
                    default:
                        $key = 'ComponentButtonColorType';
                }

                $result[$key] = $this->aComponentButtonColorType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aComponentButtonEvent) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'component_button_event';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'component_button_event';
                        break;
                    default:
                        $key = 'ComponentButtonEvent';
                }

                $result[$key] = $this->aComponentButtonEvent->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aComponentButtonPage) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'component_button_page';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'component_button_page';
                        break;
                    default:
                        $key = 'ComponentButtonPage';
                }

                $result[$key] = $this->aComponentButtonPage->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aId) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'uIComponent';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'ui_component';
                        break;
                    default:
                        $key = 'Id';
                }

                $result[$key] = $this->aId->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\System\LowCode\Button\Component_button
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = Component_buttonTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\System\LowCode\Button\Component_button
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setid($value);
                break;
            case 1:
                $this->setInfo($value);
                break;
            case 2:
                $this->setTitle($value);
                break;
            case 3:
                $this->setLabel($value);
                break;
            case 4:
                $this->setFkComponentButtonSize($value);
                break;
            case 5:
                $this->setFkComponentButtonAlign($value);
                break;
            case 6:
                $this->setFkComponentButtonColorType($value);
                break;
            case 7:
                $this->setIcon($value);
                break;
            case 8:
                $this->setFkComponentButtonEvent($value);
                break;
            case 9:
                $this->setFkComponentButtonPage($value);
                break;
            case 10:
                $this->setFlow($value);
                break;
            case 11:
                $this->setUiComponentId($value);
                break;
            case 12:
                $this->setCreatedAt($value);
                break;
            case 13:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = Component_buttonTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setid($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setInfo($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setTitle($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setLabel($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setFkComponentButtonSize($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setFkComponentButtonAlign($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setFkComponentButtonColorType($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setIcon($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setFkComponentButtonEvent($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setFkComponentButtonPage($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setFlow($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUiComponentId($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setCreatedAt($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setUpdatedAt($arr[$keys[13]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\System\LowCode\Button\Component_button The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(Component_buttonTableMap::DATABASE_NAME);

        if ($this->isColumnModified(Component_buttonTableMap::COL_ID)) {
            $criteria->add(Component_buttonTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_INFO)) {
            $criteria->add(Component_buttonTableMap::COL_INFO, $this->info);
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_TITLE)) {
            $criteria->add(Component_buttonTableMap::COL_TITLE, $this->title);
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_LABEL)) {
            $criteria->add(Component_buttonTableMap::COL_LABEL, $this->label);
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_SIZE)) {
            $criteria->add(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_SIZE, $this->fk_component_button_size);
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_ALIGN)) {
            $criteria->add(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_ALIGN, $this->fk_component_button_align);
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_COLOR_TYPE)) {
            $criteria->add(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_COLOR_TYPE, $this->fk_component_button_color_type);
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_ICON)) {
            $criteria->add(Component_buttonTableMap::COL_ICON, $this->icon);
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_EVENT)) {
            $criteria->add(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_EVENT, $this->fk_component_button_event);
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_PAGE)) {
            $criteria->add(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_PAGE, $this->fk_component_button_page);
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_FLOW)) {
            $criteria->add(Component_buttonTableMap::COL_FLOW, $this->flow);
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_UI_COMPONENT_ID)) {
            $criteria->add(Component_buttonTableMap::COL_UI_COMPONENT_ID, $this->ui_component_id);
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_CREATED_AT)) {
            $criteria->add(Component_buttonTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(Component_buttonTableMap::COL_UPDATED_AT)) {
            $criteria->add(Component_buttonTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildComponent_buttonQuery::create();
        $criteria->add(Component_buttonTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getid();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getid();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setid($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getid();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\System\LowCode\Button\Component_button (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setInfo($this->getInfo());
        $copyObj->setTitle($this->getTitle());
        $copyObj->setLabel($this->getLabel());
        $copyObj->setFkComponentButtonSize($this->getFkComponentButtonSize());
        $copyObj->setFkComponentButtonAlign($this->getFkComponentButtonAlign());
        $copyObj->setFkComponentButtonColorType($this->getFkComponentButtonColorType());
        $copyObj->setIcon($this->getIcon());
        $copyObj->setFkComponentButtonEvent($this->getFkComponentButtonEvent());
        $copyObj->setFkComponentButtonPage($this->getFkComponentButtonPage());
        $copyObj->setFlow($this->getFlow());
        $copyObj->setUiComponentId($this->getUiComponentId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setid(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\System\LowCode\Button\Component_button Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildComponent_button_size object.
     *
     * @param  ChildComponent_button_size|null $v
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     * @throws PropelException
     */
    public function setComponentButtonSize(ChildComponent_button_size $v = null)
    {
        if ($v === null) {
            $this->setFkComponentButtonSize(NULL);
        } else {
            $this->setFkComponentButtonSize($v->getid());
        }

        $this->aComponentButtonSize = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildComponent_button_size object, it will not be re-added.
        if ($v !== null) {
            $v->addComponent_button($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildComponent_button_size object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildComponent_button_size|null The associated ChildComponent_button_size object.
     * @throws PropelException
     */
    public function getComponentButtonSize(ConnectionInterface $con = null)
    {
        if ($this->aComponentButtonSize === null && ($this->fk_component_button_size != 0)) {
            $this->aComponentButtonSize = ChildComponent_button_sizeQuery::create()->findPk($this->fk_component_button_size, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aComponentButtonSize->addComponent_buttons($this);
             */
        }

        return $this->aComponentButtonSize;
    }

    /**
     * Declares an association between this object and a ChildComponent_button_align object.
     *
     * @param  ChildComponent_button_align|null $v
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     * @throws PropelException
     */
    public function setComponentButtonAlign(ChildComponent_button_align $v = null)
    {
        if ($v === null) {
            $this->setFkComponentButtonAlign(NULL);
        } else {
            $this->setFkComponentButtonAlign($v->getid());
        }

        $this->aComponentButtonAlign = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildComponent_button_align object, it will not be re-added.
        if ($v !== null) {
            $v->addComponent_button($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildComponent_button_align object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildComponent_button_align|null The associated ChildComponent_button_align object.
     * @throws PropelException
     */
    public function getComponentButtonAlign(ConnectionInterface $con = null)
    {
        if ($this->aComponentButtonAlign === null && ($this->fk_component_button_align != 0)) {
            $this->aComponentButtonAlign = ChildComponent_button_alignQuery::create()->findPk($this->fk_component_button_align, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aComponentButtonAlign->addComponent_buttons($this);
             */
        }

        return $this->aComponentButtonAlign;
    }

    /**
     * Declares an association between this object and a ChildComponent_button_color_type object.
     *
     * @param  ChildComponent_button_color_type|null $v
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     * @throws PropelException
     */
    public function setComponentButtonColorType(ChildComponent_button_color_type $v = null)
    {
        if ($v === null) {
            $this->setFkComponentButtonColorType(NULL);
        } else {
            $this->setFkComponentButtonColorType($v->getid());
        }

        $this->aComponentButtonColorType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildComponent_button_color_type object, it will not be re-added.
        if ($v !== null) {
            $v->addComponent_button($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildComponent_button_color_type object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildComponent_button_color_type|null The associated ChildComponent_button_color_type object.
     * @throws PropelException
     */
    public function getComponentButtonColorType(ConnectionInterface $con = null)
    {
        if ($this->aComponentButtonColorType === null && ($this->fk_component_button_color_type != 0)) {
            $this->aComponentButtonColorType = ChildComponent_button_color_typeQuery::create()->findPk($this->fk_component_button_color_type, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aComponentButtonColorType->addComponent_buttons($this);
             */
        }

        return $this->aComponentButtonColorType;
    }

    /**
     * Declares an association between this object and a ChildComponent_button_event object.
     *
     * @param  ChildComponent_button_event|null $v
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     * @throws PropelException
     */
    public function setComponentButtonEvent(ChildComponent_button_event $v = null)
    {
        if ($v === null) {
            $this->setFkComponentButtonEvent(NULL);
        } else {
            $this->setFkComponentButtonEvent($v->getid());
        }

        $this->aComponentButtonEvent = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildComponent_button_event object, it will not be re-added.
        if ($v !== null) {
            $v->addComponent_button($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildComponent_button_event object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildComponent_button_event|null The associated ChildComponent_button_event object.
     * @throws PropelException
     */
    public function getComponentButtonEvent(ConnectionInterface $con = null)
    {
        if ($this->aComponentButtonEvent === null && ($this->fk_component_button_event != 0)) {
            $this->aComponentButtonEvent = ChildComponent_button_eventQuery::create()->findPk($this->fk_component_button_event, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aComponentButtonEvent->addComponent_buttons($this);
             */
        }

        return $this->aComponentButtonEvent;
    }

    /**
     * Declares an association between this object and a ChildComponent_button_page object.
     *
     * @param  ChildComponent_button_page|null $v
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     * @throws PropelException
     */
    public function setComponentButtonPage(ChildComponent_button_page $v = null)
    {
        if ($v === null) {
            $this->setFkComponentButtonPage(NULL);
        } else {
            $this->setFkComponentButtonPage($v->getid());
        }

        $this->aComponentButtonPage = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildComponent_button_page object, it will not be re-added.
        if ($v !== null) {
            $v->addComponent_button($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildComponent_button_page object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildComponent_button_page|null The associated ChildComponent_button_page object.
     * @throws PropelException
     */
    public function getComponentButtonPage(ConnectionInterface $con = null)
    {
        if ($this->aComponentButtonPage === null && ($this->fk_component_button_page != 0)) {
            $this->aComponentButtonPage = ChildComponent_button_pageQuery::create()->findPk($this->fk_component_button_page, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aComponentButtonPage->addComponent_buttons($this);
             */
        }

        return $this->aComponentButtonPage;
    }

    /**
     * Declares an association between this object and a UIComponent object.
     *
     * @param  UIComponent $v
     * @return $this|\Model\System\LowCode\Button\Component_button The current object (for fluent API support)
     * @throws PropelException
     */
    public function setId(UIComponent $v = null)
    {
        if ($v === null) {
            $this->setUiComponentId(NULL);
        } else {
            $this->setUiComponentId($v->getId());
        }

        $this->aId = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the UIComponent object, it will not be re-added.
        if ($v !== null) {
            $v->addComponent_button($this);
        }


        return $this;
    }


    /**
     * Get the associated UIComponent object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return UIComponent The associated UIComponent object.
     * @throws PropelException
     */
    public function getId(ConnectionInterface $con = null)
    {
        if ($this->aId === null && ($this->ui_component_id != 0)) {
            $this->aId = UIComponentQuery::create()->findPk($this->ui_component_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aId->addComponent_buttons($this);
             */
        }

        return $this->aId;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aComponentButtonSize) {
            $this->aComponentButtonSize->removeComponent_button($this);
        }
        if (null !== $this->aComponentButtonAlign) {
            $this->aComponentButtonAlign->removeComponent_button($this);
        }
        if (null !== $this->aComponentButtonColorType) {
            $this->aComponentButtonColorType->removeComponent_button($this);
        }
        if (null !== $this->aComponentButtonEvent) {
            $this->aComponentButtonEvent->removeComponent_button($this);
        }
        if (null !== $this->aComponentButtonPage) {
            $this->aComponentButtonPage->removeComponent_button($this);
        }
        if (null !== $this->aId) {
            $this->aId->removeComponent_button($this);
        }
        $this->id = null;
        $this->info = null;
        $this->title = null;
        $this->label = null;
        $this->fk_component_button_size = null;
        $this->fk_component_button_align = null;
        $this->fk_component_button_color_type = null;
        $this->icon = null;
        $this->fk_component_button_event = null;
        $this->fk_component_button_page = null;
        $this->flow = null;
        $this->ui_component_id = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

        $this->aComponentButtonSize = null;
        $this->aComponentButtonAlign = null;
        $this->aComponentButtonColorType = null;
        $this->aComponentButtonEvent = null;
        $this->aComponentButtonPage = null;
        $this->aId = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(Component_buttonTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildComponent_button The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[Component_buttonTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
