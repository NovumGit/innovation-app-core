<?php

namespace Model\System\LowCode\Button\Base;

use \Exception;
use \PDO;
use Model\System\LowCode\Button\Component_button as ChildComponent_button;
use Model\System\LowCode\Button\Component_buttonQuery as ChildComponent_buttonQuery;
use Model\System\LowCode\Button\Map\Component_buttonTableMap;
use Model\System\UI\UIComponent;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'component_button' table.
 *
 *
 *
 * @method     ChildComponent_buttonQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method     ChildComponent_buttonQuery orderByInfo($order = Criteria::ASC) Order by the info column
 * @method     ChildComponent_buttonQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildComponent_buttonQuery orderByLabel($order = Criteria::ASC) Order by the label column
 * @method     ChildComponent_buttonQuery orderByFkComponentButtonSize($order = Criteria::ASC) Order by the fk_component_button_size column
 * @method     ChildComponent_buttonQuery orderByFkComponentButtonAlign($order = Criteria::ASC) Order by the fk_component_button_align column
 * @method     ChildComponent_buttonQuery orderByFkComponentButtonColorType($order = Criteria::ASC) Order by the fk_component_button_color_type column
 * @method     ChildComponent_buttonQuery orderByIcon($order = Criteria::ASC) Order by the icon column
 * @method     ChildComponent_buttonQuery orderByFkComponentButtonEvent($order = Criteria::ASC) Order by the fk_component_button_event column
 * @method     ChildComponent_buttonQuery orderByFkComponentButtonPage($order = Criteria::ASC) Order by the fk_component_button_page column
 * @method     ChildComponent_buttonQuery orderByFlow($order = Criteria::ASC) Order by the flow column
 * @method     ChildComponent_buttonQuery orderByUiComponentId($order = Criteria::ASC) Order by the ui_component_id column
 * @method     ChildComponent_buttonQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildComponent_buttonQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildComponent_buttonQuery groupByid() Group by the id column
 * @method     ChildComponent_buttonQuery groupByInfo() Group by the info column
 * @method     ChildComponent_buttonQuery groupByTitle() Group by the title column
 * @method     ChildComponent_buttonQuery groupByLabel() Group by the label column
 * @method     ChildComponent_buttonQuery groupByFkComponentButtonSize() Group by the fk_component_button_size column
 * @method     ChildComponent_buttonQuery groupByFkComponentButtonAlign() Group by the fk_component_button_align column
 * @method     ChildComponent_buttonQuery groupByFkComponentButtonColorType() Group by the fk_component_button_color_type column
 * @method     ChildComponent_buttonQuery groupByIcon() Group by the icon column
 * @method     ChildComponent_buttonQuery groupByFkComponentButtonEvent() Group by the fk_component_button_event column
 * @method     ChildComponent_buttonQuery groupByFkComponentButtonPage() Group by the fk_component_button_page column
 * @method     ChildComponent_buttonQuery groupByFlow() Group by the flow column
 * @method     ChildComponent_buttonQuery groupByUiComponentId() Group by the ui_component_id column
 * @method     ChildComponent_buttonQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildComponent_buttonQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildComponent_buttonQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildComponent_buttonQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildComponent_buttonQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildComponent_buttonQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildComponent_buttonQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildComponent_buttonQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildComponent_buttonQuery leftJoinComponentButtonSize($relationAlias = null) Adds a LEFT JOIN clause to the query using the ComponentButtonSize relation
 * @method     ChildComponent_buttonQuery rightJoinComponentButtonSize($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ComponentButtonSize relation
 * @method     ChildComponent_buttonQuery innerJoinComponentButtonSize($relationAlias = null) Adds a INNER JOIN clause to the query using the ComponentButtonSize relation
 *
 * @method     ChildComponent_buttonQuery joinWithComponentButtonSize($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ComponentButtonSize relation
 *
 * @method     ChildComponent_buttonQuery leftJoinWithComponentButtonSize() Adds a LEFT JOIN clause and with to the query using the ComponentButtonSize relation
 * @method     ChildComponent_buttonQuery rightJoinWithComponentButtonSize() Adds a RIGHT JOIN clause and with to the query using the ComponentButtonSize relation
 * @method     ChildComponent_buttonQuery innerJoinWithComponentButtonSize() Adds a INNER JOIN clause and with to the query using the ComponentButtonSize relation
 *
 * @method     ChildComponent_buttonQuery leftJoinComponentButtonAlign($relationAlias = null) Adds a LEFT JOIN clause to the query using the ComponentButtonAlign relation
 * @method     ChildComponent_buttonQuery rightJoinComponentButtonAlign($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ComponentButtonAlign relation
 * @method     ChildComponent_buttonQuery innerJoinComponentButtonAlign($relationAlias = null) Adds a INNER JOIN clause to the query using the ComponentButtonAlign relation
 *
 * @method     ChildComponent_buttonQuery joinWithComponentButtonAlign($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ComponentButtonAlign relation
 *
 * @method     ChildComponent_buttonQuery leftJoinWithComponentButtonAlign() Adds a LEFT JOIN clause and with to the query using the ComponentButtonAlign relation
 * @method     ChildComponent_buttonQuery rightJoinWithComponentButtonAlign() Adds a RIGHT JOIN clause and with to the query using the ComponentButtonAlign relation
 * @method     ChildComponent_buttonQuery innerJoinWithComponentButtonAlign() Adds a INNER JOIN clause and with to the query using the ComponentButtonAlign relation
 *
 * @method     ChildComponent_buttonQuery leftJoinComponentButtonColorType($relationAlias = null) Adds a LEFT JOIN clause to the query using the ComponentButtonColorType relation
 * @method     ChildComponent_buttonQuery rightJoinComponentButtonColorType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ComponentButtonColorType relation
 * @method     ChildComponent_buttonQuery innerJoinComponentButtonColorType($relationAlias = null) Adds a INNER JOIN clause to the query using the ComponentButtonColorType relation
 *
 * @method     ChildComponent_buttonQuery joinWithComponentButtonColorType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ComponentButtonColorType relation
 *
 * @method     ChildComponent_buttonQuery leftJoinWithComponentButtonColorType() Adds a LEFT JOIN clause and with to the query using the ComponentButtonColorType relation
 * @method     ChildComponent_buttonQuery rightJoinWithComponentButtonColorType() Adds a RIGHT JOIN clause and with to the query using the ComponentButtonColorType relation
 * @method     ChildComponent_buttonQuery innerJoinWithComponentButtonColorType() Adds a INNER JOIN clause and with to the query using the ComponentButtonColorType relation
 *
 * @method     ChildComponent_buttonQuery leftJoinComponentButtonEvent($relationAlias = null) Adds a LEFT JOIN clause to the query using the ComponentButtonEvent relation
 * @method     ChildComponent_buttonQuery rightJoinComponentButtonEvent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ComponentButtonEvent relation
 * @method     ChildComponent_buttonQuery innerJoinComponentButtonEvent($relationAlias = null) Adds a INNER JOIN clause to the query using the ComponentButtonEvent relation
 *
 * @method     ChildComponent_buttonQuery joinWithComponentButtonEvent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ComponentButtonEvent relation
 *
 * @method     ChildComponent_buttonQuery leftJoinWithComponentButtonEvent() Adds a LEFT JOIN clause and with to the query using the ComponentButtonEvent relation
 * @method     ChildComponent_buttonQuery rightJoinWithComponentButtonEvent() Adds a RIGHT JOIN clause and with to the query using the ComponentButtonEvent relation
 * @method     ChildComponent_buttonQuery innerJoinWithComponentButtonEvent() Adds a INNER JOIN clause and with to the query using the ComponentButtonEvent relation
 *
 * @method     ChildComponent_buttonQuery leftJoinComponentButtonPage($relationAlias = null) Adds a LEFT JOIN clause to the query using the ComponentButtonPage relation
 * @method     ChildComponent_buttonQuery rightJoinComponentButtonPage($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ComponentButtonPage relation
 * @method     ChildComponent_buttonQuery innerJoinComponentButtonPage($relationAlias = null) Adds a INNER JOIN clause to the query using the ComponentButtonPage relation
 *
 * @method     ChildComponent_buttonQuery joinWithComponentButtonPage($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ComponentButtonPage relation
 *
 * @method     ChildComponent_buttonQuery leftJoinWithComponentButtonPage() Adds a LEFT JOIN clause and with to the query using the ComponentButtonPage relation
 * @method     ChildComponent_buttonQuery rightJoinWithComponentButtonPage() Adds a RIGHT JOIN clause and with to the query using the ComponentButtonPage relation
 * @method     ChildComponent_buttonQuery innerJoinWithComponentButtonPage() Adds a INNER JOIN clause and with to the query using the ComponentButtonPage relation
 *
 * @method     ChildComponent_buttonQuery leftJoinId($relationAlias = null) Adds a LEFT JOIN clause to the query using the Id relation
 * @method     ChildComponent_buttonQuery rightJoinId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Id relation
 * @method     ChildComponent_buttonQuery innerJoinId($relationAlias = null) Adds a INNER JOIN clause to the query using the Id relation
 *
 * @method     ChildComponent_buttonQuery joinWithId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Id relation
 *
 * @method     ChildComponent_buttonQuery leftJoinWithId() Adds a LEFT JOIN clause and with to the query using the Id relation
 * @method     ChildComponent_buttonQuery rightJoinWithId() Adds a RIGHT JOIN clause and with to the query using the Id relation
 * @method     ChildComponent_buttonQuery innerJoinWithId() Adds a INNER JOIN clause and with to the query using the Id relation
 *
 * @method     \Model\System\LowCode\Button\Component_button_sizeQuery|\Model\System\LowCode\Button\Component_button_alignQuery|\Model\System\LowCode\Button\Component_button_color_typeQuery|\Model\System\LowCode\Button\Component_button_eventQuery|\Model\System\LowCode\Button\Component_button_pageQuery|\Model\System\UI\UIComponentQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildComponent_button findOne(ConnectionInterface $con = null) Return the first ChildComponent_button matching the query
 * @method     ChildComponent_button findOneOrCreate(ConnectionInterface $con = null) Return the first ChildComponent_button matching the query, or a new ChildComponent_button object populated from the query conditions when no match is found
 *
 * @method     ChildComponent_button findOneByid(int $id) Return the first ChildComponent_button filtered by the id column
 * @method     ChildComponent_button findOneByInfo(string $info) Return the first ChildComponent_button filtered by the info column
 * @method     ChildComponent_button findOneByTitle(string $title) Return the first ChildComponent_button filtered by the title column
 * @method     ChildComponent_button findOneByLabel(string $label) Return the first ChildComponent_button filtered by the label column
 * @method     ChildComponent_button findOneByFkComponentButtonSize(int $fk_component_button_size) Return the first ChildComponent_button filtered by the fk_component_button_size column
 * @method     ChildComponent_button findOneByFkComponentButtonAlign(int $fk_component_button_align) Return the first ChildComponent_button filtered by the fk_component_button_align column
 * @method     ChildComponent_button findOneByFkComponentButtonColorType(int $fk_component_button_color_type) Return the first ChildComponent_button filtered by the fk_component_button_color_type column
 * @method     ChildComponent_button findOneByIcon(string $icon) Return the first ChildComponent_button filtered by the icon column
 * @method     ChildComponent_button findOneByFkComponentButtonEvent(int $fk_component_button_event) Return the first ChildComponent_button filtered by the fk_component_button_event column
 * @method     ChildComponent_button findOneByFkComponentButtonPage(int $fk_component_button_page) Return the first ChildComponent_button filtered by the fk_component_button_page column
 * @method     ChildComponent_button findOneByFlow(string $flow) Return the first ChildComponent_button filtered by the flow column
 * @method     ChildComponent_button findOneByUiComponentId(int $ui_component_id) Return the first ChildComponent_button filtered by the ui_component_id column
 * @method     ChildComponent_button findOneByCreatedAt(string $created_at) Return the first ChildComponent_button filtered by the created_at column
 * @method     ChildComponent_button findOneByUpdatedAt(string $updated_at) Return the first ChildComponent_button filtered by the updated_at column *

 * @method     ChildComponent_button requirePk($key, ConnectionInterface $con = null) Return the ChildComponent_button by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOne(ConnectionInterface $con = null) Return the first ChildComponent_button matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComponent_button requireOneByid(int $id) Return the first ChildComponent_button filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOneByInfo(string $info) Return the first ChildComponent_button filtered by the info column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOneByTitle(string $title) Return the first ChildComponent_button filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOneByLabel(string $label) Return the first ChildComponent_button filtered by the label column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOneByFkComponentButtonSize(int $fk_component_button_size) Return the first ChildComponent_button filtered by the fk_component_button_size column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOneByFkComponentButtonAlign(int $fk_component_button_align) Return the first ChildComponent_button filtered by the fk_component_button_align column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOneByFkComponentButtonColorType(int $fk_component_button_color_type) Return the first ChildComponent_button filtered by the fk_component_button_color_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOneByIcon(string $icon) Return the first ChildComponent_button filtered by the icon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOneByFkComponentButtonEvent(int $fk_component_button_event) Return the first ChildComponent_button filtered by the fk_component_button_event column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOneByFkComponentButtonPage(int $fk_component_button_page) Return the first ChildComponent_button filtered by the fk_component_button_page column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOneByFlow(string $flow) Return the first ChildComponent_button filtered by the flow column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOneByUiComponentId(int $ui_component_id) Return the first ChildComponent_button filtered by the ui_component_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOneByCreatedAt(string $created_at) Return the first ChildComponent_button filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button requireOneByUpdatedAt(string $updated_at) Return the first ChildComponent_button filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComponent_button[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildComponent_button objects based on current ModelCriteria
 * @method     ChildComponent_button[]|ObjectCollection findByid(int $id) Return ChildComponent_button objects filtered by the id column
 * @method     ChildComponent_button[]|ObjectCollection findByInfo(string $info) Return ChildComponent_button objects filtered by the info column
 * @method     ChildComponent_button[]|ObjectCollection findByTitle(string $title) Return ChildComponent_button objects filtered by the title column
 * @method     ChildComponent_button[]|ObjectCollection findByLabel(string $label) Return ChildComponent_button objects filtered by the label column
 * @method     ChildComponent_button[]|ObjectCollection findByFkComponentButtonSize(int $fk_component_button_size) Return ChildComponent_button objects filtered by the fk_component_button_size column
 * @method     ChildComponent_button[]|ObjectCollection findByFkComponentButtonAlign(int $fk_component_button_align) Return ChildComponent_button objects filtered by the fk_component_button_align column
 * @method     ChildComponent_button[]|ObjectCollection findByFkComponentButtonColorType(int $fk_component_button_color_type) Return ChildComponent_button objects filtered by the fk_component_button_color_type column
 * @method     ChildComponent_button[]|ObjectCollection findByIcon(string $icon) Return ChildComponent_button objects filtered by the icon column
 * @method     ChildComponent_button[]|ObjectCollection findByFkComponentButtonEvent(int $fk_component_button_event) Return ChildComponent_button objects filtered by the fk_component_button_event column
 * @method     ChildComponent_button[]|ObjectCollection findByFkComponentButtonPage(int $fk_component_button_page) Return ChildComponent_button objects filtered by the fk_component_button_page column
 * @method     ChildComponent_button[]|ObjectCollection findByFlow(string $flow) Return ChildComponent_button objects filtered by the flow column
 * @method     ChildComponent_button[]|ObjectCollection findByUiComponentId(int $ui_component_id) Return ChildComponent_button objects filtered by the ui_component_id column
 * @method     ChildComponent_button[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildComponent_button objects filtered by the created_at column
 * @method     ChildComponent_button[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildComponent_button objects filtered by the updated_at column
 * @method     ChildComponent_button[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class Component_buttonQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\LowCode\Button\Base\Component_buttonQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\LowCode\\Button\\Component_button', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildComponent_buttonQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildComponent_buttonQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildComponent_buttonQuery) {
            return $criteria;
        }
        $query = new ChildComponent_buttonQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildComponent_button|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(Component_buttonTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = Component_buttonTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_button A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, info, title, label, fk_component_button_size, fk_component_button_align, fk_component_button_color_type, icon, fk_component_button_event, fk_component_button_page, flow, ui_component_id, created_at, updated_at FROM component_button WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildComponent_button $obj */
            $obj = new ChildComponent_button();
            $obj->hydrate($row);
            Component_buttonTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildComponent_button|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(Component_buttonTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(Component_buttonTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the info column
     *
     * Example usage:
     * <code>
     * $query->filterByInfo('fooValue');   // WHERE info = 'fooValue'
     * $query->filterByInfo('%fooValue%', Criteria::LIKE); // WHERE info LIKE '%fooValue%'
     * </code>
     *
     * @param     string $info The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByInfo($info = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($info)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_INFO, $info, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the label column
     *
     * Example usage:
     * <code>
     * $query->filterByLabel('fooValue');   // WHERE label = 'fooValue'
     * $query->filterByLabel('%fooValue%', Criteria::LIKE); // WHERE label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $label The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByLabel($label = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($label)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_LABEL, $label, $comparison);
    }

    /**
     * Filter the query on the fk_component_button_size column
     *
     * Example usage:
     * <code>
     * $query->filterByFkComponentButtonSize(1234); // WHERE fk_component_button_size = 1234
     * $query->filterByFkComponentButtonSize(array(12, 34)); // WHERE fk_component_button_size IN (12, 34)
     * $query->filterByFkComponentButtonSize(array('min' => 12)); // WHERE fk_component_button_size > 12
     * </code>
     *
     * @see       filterByComponentButtonSize()
     *
     * @param     mixed $fkComponentButtonSize The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByFkComponentButtonSize($fkComponentButtonSize = null, $comparison = null)
    {
        if (is_array($fkComponentButtonSize)) {
            $useMinMax = false;
            if (isset($fkComponentButtonSize['min'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_SIZE, $fkComponentButtonSize['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fkComponentButtonSize['max'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_SIZE, $fkComponentButtonSize['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_SIZE, $fkComponentButtonSize, $comparison);
    }

    /**
     * Filter the query on the fk_component_button_align column
     *
     * Example usage:
     * <code>
     * $query->filterByFkComponentButtonAlign(1234); // WHERE fk_component_button_align = 1234
     * $query->filterByFkComponentButtonAlign(array(12, 34)); // WHERE fk_component_button_align IN (12, 34)
     * $query->filterByFkComponentButtonAlign(array('min' => 12)); // WHERE fk_component_button_align > 12
     * </code>
     *
     * @see       filterByComponentButtonAlign()
     *
     * @param     mixed $fkComponentButtonAlign The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByFkComponentButtonAlign($fkComponentButtonAlign = null, $comparison = null)
    {
        if (is_array($fkComponentButtonAlign)) {
            $useMinMax = false;
            if (isset($fkComponentButtonAlign['min'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_ALIGN, $fkComponentButtonAlign['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fkComponentButtonAlign['max'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_ALIGN, $fkComponentButtonAlign['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_ALIGN, $fkComponentButtonAlign, $comparison);
    }

    /**
     * Filter the query on the fk_component_button_color_type column
     *
     * Example usage:
     * <code>
     * $query->filterByFkComponentButtonColorType(1234); // WHERE fk_component_button_color_type = 1234
     * $query->filterByFkComponentButtonColorType(array(12, 34)); // WHERE fk_component_button_color_type IN (12, 34)
     * $query->filterByFkComponentButtonColorType(array('min' => 12)); // WHERE fk_component_button_color_type > 12
     * </code>
     *
     * @see       filterByComponentButtonColorType()
     *
     * @param     mixed $fkComponentButtonColorType The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByFkComponentButtonColorType($fkComponentButtonColorType = null, $comparison = null)
    {
        if (is_array($fkComponentButtonColorType)) {
            $useMinMax = false;
            if (isset($fkComponentButtonColorType['min'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_COLOR_TYPE, $fkComponentButtonColorType['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fkComponentButtonColorType['max'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_COLOR_TYPE, $fkComponentButtonColorType['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_COLOR_TYPE, $fkComponentButtonColorType, $comparison);
    }

    /**
     * Filter the query on the icon column
     *
     * Example usage:
     * <code>
     * $query->filterByIcon('fooValue');   // WHERE icon = 'fooValue'
     * $query->filterByIcon('%fooValue%', Criteria::LIKE); // WHERE icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $icon The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByIcon($icon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($icon)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_ICON, $icon, $comparison);
    }

    /**
     * Filter the query on the fk_component_button_event column
     *
     * Example usage:
     * <code>
     * $query->filterByFkComponentButtonEvent(1234); // WHERE fk_component_button_event = 1234
     * $query->filterByFkComponentButtonEvent(array(12, 34)); // WHERE fk_component_button_event IN (12, 34)
     * $query->filterByFkComponentButtonEvent(array('min' => 12)); // WHERE fk_component_button_event > 12
     * </code>
     *
     * @see       filterByComponentButtonEvent()
     *
     * @param     mixed $fkComponentButtonEvent The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByFkComponentButtonEvent($fkComponentButtonEvent = null, $comparison = null)
    {
        if (is_array($fkComponentButtonEvent)) {
            $useMinMax = false;
            if (isset($fkComponentButtonEvent['min'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_EVENT, $fkComponentButtonEvent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fkComponentButtonEvent['max'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_EVENT, $fkComponentButtonEvent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_EVENT, $fkComponentButtonEvent, $comparison);
    }

    /**
     * Filter the query on the fk_component_button_page column
     *
     * Example usage:
     * <code>
     * $query->filterByFkComponentButtonPage(1234); // WHERE fk_component_button_page = 1234
     * $query->filterByFkComponentButtonPage(array(12, 34)); // WHERE fk_component_button_page IN (12, 34)
     * $query->filterByFkComponentButtonPage(array('min' => 12)); // WHERE fk_component_button_page > 12
     * </code>
     *
     * @see       filterByComponentButtonPage()
     *
     * @param     mixed $fkComponentButtonPage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByFkComponentButtonPage($fkComponentButtonPage = null, $comparison = null)
    {
        if (is_array($fkComponentButtonPage)) {
            $useMinMax = false;
            if (isset($fkComponentButtonPage['min'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_PAGE, $fkComponentButtonPage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fkComponentButtonPage['max'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_PAGE, $fkComponentButtonPage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_PAGE, $fkComponentButtonPage, $comparison);
    }

    /**
     * Filter the query on the flow column
     *
     * Example usage:
     * <code>
     * $query->filterByFlow('fooValue');   // WHERE flow = 'fooValue'
     * $query->filterByFlow('%fooValue%', Criteria::LIKE); // WHERE flow LIKE '%fooValue%'
     * </code>
     *
     * @param     string $flow The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByFlow($flow = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($flow)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_FLOW, $flow, $comparison);
    }

    /**
     * Filter the query on the ui_component_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUiComponentId(1234); // WHERE ui_component_id = 1234
     * $query->filterByUiComponentId(array(12, 34)); // WHERE ui_component_id IN (12, 34)
     * $query->filterByUiComponentId(array('min' => 12)); // WHERE ui_component_id > 12
     * </code>
     *
     * @see       filterById()
     *
     * @param     mixed $uiComponentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByUiComponentId($uiComponentId = null, $comparison = null)
    {
        if (is_array($uiComponentId)) {
            $useMinMax = false;
            if (isset($uiComponentId['min'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_UI_COMPONENT_ID, $uiComponentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uiComponentId['max'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_UI_COMPONENT_ID, $uiComponentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_UI_COMPONENT_ID, $uiComponentId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(Component_buttonTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_buttonTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\LowCode\Button\Component_button_size object
     *
     * @param \Model\System\LowCode\Button\Component_button_size|ObjectCollection $component_button_size The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByComponentButtonSize($component_button_size, $comparison = null)
    {
        if ($component_button_size instanceof \Model\System\LowCode\Button\Component_button_size) {
            return $this
                ->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_SIZE, $component_button_size->getid(), $comparison);
        } elseif ($component_button_size instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_SIZE, $component_button_size->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByComponentButtonSize() only accepts arguments of type \Model\System\LowCode\Button\Component_button_size or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ComponentButtonSize relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function joinComponentButtonSize($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ComponentButtonSize');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ComponentButtonSize');
        }

        return $this;
    }

    /**
     * Use the ComponentButtonSize relation Component_button_size object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\LowCode\Button\Component_button_sizeQuery A secondary query class using the current class as primary query
     */
    public function useComponentButtonSizeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComponentButtonSize($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ComponentButtonSize', '\Model\System\LowCode\Button\Component_button_sizeQuery');
    }

    /**
     * Filter the query by a related \Model\System\LowCode\Button\Component_button_align object
     *
     * @param \Model\System\LowCode\Button\Component_button_align|ObjectCollection $component_button_align The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByComponentButtonAlign($component_button_align, $comparison = null)
    {
        if ($component_button_align instanceof \Model\System\LowCode\Button\Component_button_align) {
            return $this
                ->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_ALIGN, $component_button_align->getid(), $comparison);
        } elseif ($component_button_align instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_ALIGN, $component_button_align->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByComponentButtonAlign() only accepts arguments of type \Model\System\LowCode\Button\Component_button_align or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ComponentButtonAlign relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function joinComponentButtonAlign($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ComponentButtonAlign');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ComponentButtonAlign');
        }

        return $this;
    }

    /**
     * Use the ComponentButtonAlign relation Component_button_align object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\LowCode\Button\Component_button_alignQuery A secondary query class using the current class as primary query
     */
    public function useComponentButtonAlignQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComponentButtonAlign($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ComponentButtonAlign', '\Model\System\LowCode\Button\Component_button_alignQuery');
    }

    /**
     * Filter the query by a related \Model\System\LowCode\Button\Component_button_color_type object
     *
     * @param \Model\System\LowCode\Button\Component_button_color_type|ObjectCollection $component_button_color_type The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByComponentButtonColorType($component_button_color_type, $comparison = null)
    {
        if ($component_button_color_type instanceof \Model\System\LowCode\Button\Component_button_color_type) {
            return $this
                ->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_COLOR_TYPE, $component_button_color_type->getid(), $comparison);
        } elseif ($component_button_color_type instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_COLOR_TYPE, $component_button_color_type->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByComponentButtonColorType() only accepts arguments of type \Model\System\LowCode\Button\Component_button_color_type or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ComponentButtonColorType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function joinComponentButtonColorType($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ComponentButtonColorType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ComponentButtonColorType');
        }

        return $this;
    }

    /**
     * Use the ComponentButtonColorType relation Component_button_color_type object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\LowCode\Button\Component_button_color_typeQuery A secondary query class using the current class as primary query
     */
    public function useComponentButtonColorTypeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComponentButtonColorType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ComponentButtonColorType', '\Model\System\LowCode\Button\Component_button_color_typeQuery');
    }

    /**
     * Filter the query by a related \Model\System\LowCode\Button\Component_button_event object
     *
     * @param \Model\System\LowCode\Button\Component_button_event|ObjectCollection $component_button_event The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByComponentButtonEvent($component_button_event, $comparison = null)
    {
        if ($component_button_event instanceof \Model\System\LowCode\Button\Component_button_event) {
            return $this
                ->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_EVENT, $component_button_event->getid(), $comparison);
        } elseif ($component_button_event instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_EVENT, $component_button_event->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByComponentButtonEvent() only accepts arguments of type \Model\System\LowCode\Button\Component_button_event or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ComponentButtonEvent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function joinComponentButtonEvent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ComponentButtonEvent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ComponentButtonEvent');
        }

        return $this;
    }

    /**
     * Use the ComponentButtonEvent relation Component_button_event object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\LowCode\Button\Component_button_eventQuery A secondary query class using the current class as primary query
     */
    public function useComponentButtonEventQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComponentButtonEvent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ComponentButtonEvent', '\Model\System\LowCode\Button\Component_button_eventQuery');
    }

    /**
     * Filter the query by a related \Model\System\LowCode\Button\Component_button_page object
     *
     * @param \Model\System\LowCode\Button\Component_button_page|ObjectCollection $component_button_page The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterByComponentButtonPage($component_button_page, $comparison = null)
    {
        if ($component_button_page instanceof \Model\System\LowCode\Button\Component_button_page) {
            return $this
                ->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_PAGE, $component_button_page->getid(), $comparison);
        } elseif ($component_button_page instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Component_buttonTableMap::COL_FK_COMPONENT_BUTTON_PAGE, $component_button_page->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByComponentButtonPage() only accepts arguments of type \Model\System\LowCode\Button\Component_button_page or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ComponentButtonPage relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function joinComponentButtonPage($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ComponentButtonPage');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ComponentButtonPage');
        }

        return $this;
    }

    /**
     * Use the ComponentButtonPage relation Component_button_page object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\LowCode\Button\Component_button_pageQuery A secondary query class using the current class as primary query
     */
    public function useComponentButtonPageQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComponentButtonPage($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ComponentButtonPage', '\Model\System\LowCode\Button\Component_button_pageQuery');
    }

    /**
     * Filter the query by a related \Model\System\UI\UIComponent object
     *
     * @param \Model\System\UI\UIComponent|ObjectCollection $uIComponent The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function filterById($uIComponent, $comparison = null)
    {
        if ($uIComponent instanceof \Model\System\UI\UIComponent) {
            return $this
                ->addUsingAlias(Component_buttonTableMap::COL_UI_COMPONENT_ID, $uIComponent->getId(), $comparison);
        } elseif ($uIComponent instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Component_buttonTableMap::COL_UI_COMPONENT_ID, $uIComponent->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterById() only accepts arguments of type \Model\System\UI\UIComponent or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Id relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function joinId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Id');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Id');
        }

        return $this;
    }

    /**
     * Use the Id relation UIComponent object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\UI\UIComponentQuery A secondary query class using the current class as primary query
     */
    public function useIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Id', '\Model\System\UI\UIComponentQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildComponent_button $component_button Object to remove from the list of results
     *
     * @return $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function prune($component_button = null)
    {
        if ($component_button) {
            $this->addUsingAlias(Component_buttonTableMap::COL_ID, $component_button->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the component_button table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_buttonTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            Component_buttonTableMap::clearInstancePool();
            Component_buttonTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_buttonTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(Component_buttonTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            Component_buttonTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            Component_buttonTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(Component_buttonTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(Component_buttonTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(Component_buttonTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(Component_buttonTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(Component_buttonTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildComponent_buttonQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(Component_buttonTableMap::COL_CREATED_AT);
    }

} // Component_buttonQuery
