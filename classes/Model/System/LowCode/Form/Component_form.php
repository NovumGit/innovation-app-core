<?php

namespace Model\System\LowCode\Form;

use Model\System\LowCode\Form\Base\Component_form as BaseComponent_form;

/**
 * Skeleton subclass for representing a row from the 'component_form' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_form extends BaseComponent_form
{

}
