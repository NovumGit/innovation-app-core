<?php

namespace Model\System\LowCode\Form;

use Model\System\LowCode\Form\Base\Component_form_render_style as BaseComponent_form_render_style;

/**
 * Skeleton subclass for representing a row from the 'component_form_render_style' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_form_render_style extends BaseComponent_form_render_style
{

}
