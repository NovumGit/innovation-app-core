<?php

namespace Model\System\LowCode\Form;

use Model\System\LowCode\Form\Base\Component_form_button_locationQuery as BaseComponent_form_button_locationQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'component_form_button_location' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_form_button_locationQuery extends BaseComponent_form_button_locationQuery
{

}
