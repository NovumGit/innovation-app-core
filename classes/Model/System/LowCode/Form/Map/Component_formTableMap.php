<?php

namespace Model\System\LowCode\Form\Map;

use Model\System\LowCode\Form\Component_form;
use Model\System\LowCode\Form\Component_formQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'component_form' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class Component_formTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.System.LowCode.Form.Map.Component_formTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'component_form';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\System\\LowCode\\Form\\Component_form';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.System.LowCode.Form.Component_form';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'component_form.id';

    /**
     * the column name for the manager field
     */
    const COL_MANAGER = 'component_form.manager';

    /**
     * the column name for the layout_key field
     */
    const COL_LAYOUT_KEY = 'component_form.layout_key';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'component_form.title';

    /**
     * the column name for the fk_component_form_render_style field
     */
    const COL_FK_COMPONENT_FORM_RENDER_STYLE = 'component_form.fk_component_form_render_style';

    /**
     * the column name for the fk_component_form_button_location field
     */
    const COL_FK_COMPONENT_FORM_BUTTON_LOCATION = 'component_form.fk_component_form_button_location';

    /**
     * the column name for the ui_component_id field
     */
    const COL_UI_COMPONENT_ID = 'component_form.ui_component_id';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'component_form.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'component_form.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('id', 'Manager', 'LayoutKey', 'Title', 'FkComponentFormRenderStyle', 'FkComponentFormButtonLocation', 'UiComponentId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'manager', 'layoutKey', 'title', 'fkComponentFormRenderStyle', 'fkComponentFormButtonLocation', 'uiComponentId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(Component_formTableMap::COL_ID, Component_formTableMap::COL_MANAGER, Component_formTableMap::COL_LAYOUT_KEY, Component_formTableMap::COL_TITLE, Component_formTableMap::COL_FK_COMPONENT_FORM_RENDER_STYLE, Component_formTableMap::COL_FK_COMPONENT_FORM_BUTTON_LOCATION, Component_formTableMap::COL_UI_COMPONENT_ID, Component_formTableMap::COL_CREATED_AT, Component_formTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'manager', 'layout_key', 'title', 'fk_component_form_render_style', 'fk_component_form_button_location', 'ui_component_id', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('id' => 0, 'Manager' => 1, 'LayoutKey' => 2, 'Title' => 3, 'FkComponentFormRenderStyle' => 4, 'FkComponentFormButtonLocation' => 5, 'UiComponentId' => 6, 'CreatedAt' => 7, 'UpdatedAt' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'manager' => 1, 'layoutKey' => 2, 'title' => 3, 'fkComponentFormRenderStyle' => 4, 'fkComponentFormButtonLocation' => 5, 'uiComponentId' => 6, 'createdAt' => 7, 'updatedAt' => 8, ),
        self::TYPE_COLNAME       => array(Component_formTableMap::COL_ID => 0, Component_formTableMap::COL_MANAGER => 1, Component_formTableMap::COL_LAYOUT_KEY => 2, Component_formTableMap::COL_TITLE => 3, Component_formTableMap::COL_FK_COMPONENT_FORM_RENDER_STYLE => 4, Component_formTableMap::COL_FK_COMPONENT_FORM_BUTTON_LOCATION => 5, Component_formTableMap::COL_UI_COMPONENT_ID => 6, Component_formTableMap::COL_CREATED_AT => 7, Component_formTableMap::COL_UPDATED_AT => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'manager' => 1, 'layout_key' => 2, 'title' => 3, 'fk_component_form_render_style' => 4, 'fk_component_form_button_location' => 5, 'ui_component_id' => 6, 'created_at' => 7, 'updated_at' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('component_form');
        $this->setPhpName('Component_form');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\System\\LowCode\\Form\\Component_form');
        $this->setPackage('Model.System.LowCode.Form');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'id', 'INTEGER', true, null, null);
        $this->addColumn('manager', 'Manager', 'VARCHAR', false, 255, null);
        $this->addColumn('layout_key', 'LayoutKey', 'VARCHAR', false, 255, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 255, null);
        $this->addForeignKey('fk_component_form_render_style', 'FkComponentFormRenderStyle', 'INTEGER', 'component_form_render_style', 'id', false, null, null);
        $this->addForeignKey('fk_component_form_button_location', 'FkComponentFormButtonLocation', 'INTEGER', 'component_form_button_location', 'id', false, null, null);
        $this->addForeignKey('ui_component_id', 'UiComponentId', 'INTEGER', 'ui_component', 'id', true, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ComponentFormRenderStyle', '\\Model\\System\\LowCode\\Form\\Component_form_render_style', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':fk_component_form_render_style',
    1 => ':id',
  ),
), 'RESTRICT', 'RESTRICT', null, false);
        $this->addRelation('ComponentFormButtonLocation', '\\Model\\System\\LowCode\\Form\\Component_form_button_location', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':fk_component_form_button_location',
    1 => ':id',
  ),
), 'RESTRICT', 'RESTRICT', null, false);
        $this->addRelation('Id', '\\Model\\System\\UI\\UIComponent', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':ui_component_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? Component_formTableMap::CLASS_DEFAULT : Component_formTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Component_form object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = Component_formTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = Component_formTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + Component_formTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = Component_formTableMap::OM_CLASS;
            /** @var Component_form $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            Component_formTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = Component_formTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = Component_formTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Component_form $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                Component_formTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(Component_formTableMap::COL_ID);
            $criteria->addSelectColumn(Component_formTableMap::COL_MANAGER);
            $criteria->addSelectColumn(Component_formTableMap::COL_LAYOUT_KEY);
            $criteria->addSelectColumn(Component_formTableMap::COL_TITLE);
            $criteria->addSelectColumn(Component_formTableMap::COL_FK_COMPONENT_FORM_RENDER_STYLE);
            $criteria->addSelectColumn(Component_formTableMap::COL_FK_COMPONENT_FORM_BUTTON_LOCATION);
            $criteria->addSelectColumn(Component_formTableMap::COL_UI_COMPONENT_ID);
            $criteria->addSelectColumn(Component_formTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(Component_formTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.manager');
            $criteria->addSelectColumn($alias . '.layout_key');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.fk_component_form_render_style');
            $criteria->addSelectColumn($alias . '.fk_component_form_button_location');
            $criteria->addSelectColumn($alias . '.ui_component_id');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(Component_formTableMap::DATABASE_NAME)->getTable(Component_formTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(Component_formTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(Component_formTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new Component_formTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Component_form or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Component_form object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_formTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\System\LowCode\Form\Component_form) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(Component_formTableMap::DATABASE_NAME);
            $criteria->add(Component_formTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = Component_formQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            Component_formTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                Component_formTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the component_form table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return Component_formQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Component_form or Criteria object.
     *
     * @param mixed               $criteria Criteria or Component_form object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_formTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Component_form object
        }

        if ($criteria->containsKey(Component_formTableMap::COL_ID) && $criteria->keyContainsValue(Component_formTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.Component_formTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = Component_formQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // Component_formTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
Component_formTableMap::buildTableMap();
