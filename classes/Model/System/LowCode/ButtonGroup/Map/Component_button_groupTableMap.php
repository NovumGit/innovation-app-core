<?php

namespace Model\System\LowCode\ButtonGroup\Map;

use Model\System\LowCode\ButtonGroup\Component_button_group;
use Model\System\LowCode\ButtonGroup\Component_button_groupQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'component_button_group' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class Component_button_groupTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.System.LowCode.ButtonGroup.Map.Component_button_groupTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'component_button_group';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\System\\LowCode\\ButtonGroup\\Component_button_group';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.System.LowCode.ButtonGroup.Component_button_group';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 12;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 12;

    /**
     * the column name for the id field
     */
    const COL_ID = 'component_button_group.id';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'component_button_group.title';

    /**
     * the column name for the fk_component_button_group_size field
     */
    const COL_FK_COMPONENT_BUTTON_GROUP_SIZE = 'component_button_group.fk_component_button_group_size';

    /**
     * the column name for the fk_component_button_group_role field
     */
    const COL_FK_COMPONENT_BUTTON_GROUP_ROLE = 'component_button_group.fk_component_button_group_role';

    /**
     * the column name for the icon field
     */
    const COL_ICON = 'component_button_group.icon';

    /**
     * the column name for the label field
     */
    const COL_LABEL = 'component_button_group.label';

    /**
     * the column name for the tooltip field
     */
    const COL_TOOLTIP = 'component_button_group.tooltip';

    /**
     * the column name for the as_dropdown field
     */
    const COL_AS_DROPDOWN = 'component_button_group.as_dropdown';

    /**
     * the column name for the default_active field
     */
    const COL_DEFAULT_ACTIVE = 'component_button_group.default_active';

    /**
     * the column name for the ui_component_id field
     */
    const COL_UI_COMPONENT_ID = 'component_button_group.ui_component_id';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'component_button_group.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'component_button_group.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('id', 'Title', 'FkComponentButtonGroupSize', 'FkComponentButtonGroupRole', 'Icon', 'Label', 'Tooltip', 'AsDropdown', 'DefaultActive', 'UiComponentId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'title', 'fkComponentButtonGroupSize', 'fkComponentButtonGroupRole', 'icon', 'label', 'tooltip', 'asDropdown', 'defaultActive', 'uiComponentId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(Component_button_groupTableMap::COL_ID, Component_button_groupTableMap::COL_TITLE, Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_SIZE, Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_ROLE, Component_button_groupTableMap::COL_ICON, Component_button_groupTableMap::COL_LABEL, Component_button_groupTableMap::COL_TOOLTIP, Component_button_groupTableMap::COL_AS_DROPDOWN, Component_button_groupTableMap::COL_DEFAULT_ACTIVE, Component_button_groupTableMap::COL_UI_COMPONENT_ID, Component_button_groupTableMap::COL_CREATED_AT, Component_button_groupTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'title', 'fk_component_button_group_size', 'fk_component_button_group_role', 'icon', 'label', 'tooltip', 'as_dropdown', 'default_active', 'ui_component_id', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('id' => 0, 'Title' => 1, 'FkComponentButtonGroupSize' => 2, 'FkComponentButtonGroupRole' => 3, 'Icon' => 4, 'Label' => 5, 'Tooltip' => 6, 'AsDropdown' => 7, 'DefaultActive' => 8, 'UiComponentId' => 9, 'CreatedAt' => 10, 'UpdatedAt' => 11, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'title' => 1, 'fkComponentButtonGroupSize' => 2, 'fkComponentButtonGroupRole' => 3, 'icon' => 4, 'label' => 5, 'tooltip' => 6, 'asDropdown' => 7, 'defaultActive' => 8, 'uiComponentId' => 9, 'createdAt' => 10, 'updatedAt' => 11, ),
        self::TYPE_COLNAME       => array(Component_button_groupTableMap::COL_ID => 0, Component_button_groupTableMap::COL_TITLE => 1, Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_SIZE => 2, Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_ROLE => 3, Component_button_groupTableMap::COL_ICON => 4, Component_button_groupTableMap::COL_LABEL => 5, Component_button_groupTableMap::COL_TOOLTIP => 6, Component_button_groupTableMap::COL_AS_DROPDOWN => 7, Component_button_groupTableMap::COL_DEFAULT_ACTIVE => 8, Component_button_groupTableMap::COL_UI_COMPONENT_ID => 9, Component_button_groupTableMap::COL_CREATED_AT => 10, Component_button_groupTableMap::COL_UPDATED_AT => 11, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'title' => 1, 'fk_component_button_group_size' => 2, 'fk_component_button_group_role' => 3, 'icon' => 4, 'label' => 5, 'tooltip' => 6, 'as_dropdown' => 7, 'default_active' => 8, 'ui_component_id' => 9, 'created_at' => 10, 'updated_at' => 11, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('component_button_group');
        $this->setPhpName('Component_button_group');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\System\\LowCode\\ButtonGroup\\Component_button_group');
        $this->setPackage('Model.System.LowCode.ButtonGroup');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'id', 'INTEGER', true, null, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 255, null);
        $this->addForeignKey('fk_component_button_group_size', 'FkComponentButtonGroupSize', 'INTEGER', 'component_button_group_size', 'id', false, null, null);
        $this->addForeignKey('fk_component_button_group_role', 'FkComponentButtonGroupRole', 'INTEGER', 'component_button_group_role', 'id', false, null, null);
        $this->addColumn('icon', 'Icon', 'VARCHAR', false, 255, null);
        $this->addColumn('label', 'Label', 'VARCHAR', false, 255, null);
        $this->addColumn('tooltip', 'Tooltip', 'VARCHAR', false, 255, null);
        $this->addColumn('as_dropdown', 'AsDropdown', 'VARCHAR', false, 255, null);
        $this->addColumn('default_active', 'DefaultActive', 'VARCHAR', false, 255, null);
        $this->addForeignKey('ui_component_id', 'UiComponentId', 'INTEGER', 'ui_component', 'id', true, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ComponentButtonGroupSize', '\\Model\\System\\LowCode\\ButtonGroup\\Component_button_group_size', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':fk_component_button_group_size',
    1 => ':id',
  ),
), 'RESTRICT', 'RESTRICT', null, false);
        $this->addRelation('ComponentButtonGroupRole', '\\Model\\System\\LowCode\\ButtonGroup\\Component_button_group_role', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':fk_component_button_group_role',
    1 => ':id',
  ),
), 'RESTRICT', 'RESTRICT', null, false);
        $this->addRelation('Id', '\\Model\\System\\UI\\UIComponent', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':ui_component_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? Component_button_groupTableMap::CLASS_DEFAULT : Component_button_groupTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Component_button_group object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = Component_button_groupTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = Component_button_groupTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + Component_button_groupTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = Component_button_groupTableMap::OM_CLASS;
            /** @var Component_button_group $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            Component_button_groupTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = Component_button_groupTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = Component_button_groupTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Component_button_group $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                Component_button_groupTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(Component_button_groupTableMap::COL_ID);
            $criteria->addSelectColumn(Component_button_groupTableMap::COL_TITLE);
            $criteria->addSelectColumn(Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_SIZE);
            $criteria->addSelectColumn(Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_ROLE);
            $criteria->addSelectColumn(Component_button_groupTableMap::COL_ICON);
            $criteria->addSelectColumn(Component_button_groupTableMap::COL_LABEL);
            $criteria->addSelectColumn(Component_button_groupTableMap::COL_TOOLTIP);
            $criteria->addSelectColumn(Component_button_groupTableMap::COL_AS_DROPDOWN);
            $criteria->addSelectColumn(Component_button_groupTableMap::COL_DEFAULT_ACTIVE);
            $criteria->addSelectColumn(Component_button_groupTableMap::COL_UI_COMPONENT_ID);
            $criteria->addSelectColumn(Component_button_groupTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(Component_button_groupTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.fk_component_button_group_size');
            $criteria->addSelectColumn($alias . '.fk_component_button_group_role');
            $criteria->addSelectColumn($alias . '.icon');
            $criteria->addSelectColumn($alias . '.label');
            $criteria->addSelectColumn($alias . '.tooltip');
            $criteria->addSelectColumn($alias . '.as_dropdown');
            $criteria->addSelectColumn($alias . '.default_active');
            $criteria->addSelectColumn($alias . '.ui_component_id');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(Component_button_groupTableMap::DATABASE_NAME)->getTable(Component_button_groupTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(Component_button_groupTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(Component_button_groupTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new Component_button_groupTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Component_button_group or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Component_button_group object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_button_groupTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\System\LowCode\ButtonGroup\Component_button_group) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(Component_button_groupTableMap::DATABASE_NAME);
            $criteria->add(Component_button_groupTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = Component_button_groupQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            Component_button_groupTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                Component_button_groupTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the component_button_group table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return Component_button_groupQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Component_button_group or Criteria object.
     *
     * @param mixed               $criteria Criteria or Component_button_group object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_button_groupTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Component_button_group object
        }

        if ($criteria->containsKey(Component_button_groupTableMap::COL_ID) && $criteria->keyContainsValue(Component_button_groupTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.Component_button_groupTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = Component_button_groupQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // Component_button_groupTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
Component_button_groupTableMap::buildTableMap();
