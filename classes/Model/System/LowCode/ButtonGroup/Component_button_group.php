<?php

namespace Model\System\LowCode\ButtonGroup;

use Model\System\LowCode\ButtonGroup\Base\Component_button_group as BaseComponent_button_group;

/**
 * Skeleton subclass for representing a row from the 'component_button_group' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_button_group extends BaseComponent_button_group
{

}
