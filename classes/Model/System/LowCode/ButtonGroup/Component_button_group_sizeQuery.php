<?php

namespace Model\System\LowCode\ButtonGroup;

use Model\System\LowCode\ButtonGroup\Base\Component_button_group_sizeQuery as BaseComponent_button_group_sizeQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'component_button_group_size' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_button_group_sizeQuery extends BaseComponent_button_group_sizeQuery
{

}
