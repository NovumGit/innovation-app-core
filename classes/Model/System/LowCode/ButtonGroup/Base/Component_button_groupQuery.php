<?php

namespace Model\System\LowCode\ButtonGroup\Base;

use \Exception;
use \PDO;
use Model\System\LowCode\ButtonGroup\Component_button_group as ChildComponent_button_group;
use Model\System\LowCode\ButtonGroup\Component_button_groupQuery as ChildComponent_button_groupQuery;
use Model\System\LowCode\ButtonGroup\Map\Component_button_groupTableMap;
use Model\System\UI\UIComponent;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'component_button_group' table.
 *
 *
 *
 * @method     ChildComponent_button_groupQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method     ChildComponent_button_groupQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildComponent_button_groupQuery orderByFkComponentButtonGroupSize($order = Criteria::ASC) Order by the fk_component_button_group_size column
 * @method     ChildComponent_button_groupQuery orderByFkComponentButtonGroupRole($order = Criteria::ASC) Order by the fk_component_button_group_role column
 * @method     ChildComponent_button_groupQuery orderByIcon($order = Criteria::ASC) Order by the icon column
 * @method     ChildComponent_button_groupQuery orderByLabel($order = Criteria::ASC) Order by the label column
 * @method     ChildComponent_button_groupQuery orderByTooltip($order = Criteria::ASC) Order by the tooltip column
 * @method     ChildComponent_button_groupQuery orderByAsDropdown($order = Criteria::ASC) Order by the as_dropdown column
 * @method     ChildComponent_button_groupQuery orderByDefaultActive($order = Criteria::ASC) Order by the default_active column
 * @method     ChildComponent_button_groupQuery orderByUiComponentId($order = Criteria::ASC) Order by the ui_component_id column
 * @method     ChildComponent_button_groupQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildComponent_button_groupQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildComponent_button_groupQuery groupByid() Group by the id column
 * @method     ChildComponent_button_groupQuery groupByTitle() Group by the title column
 * @method     ChildComponent_button_groupQuery groupByFkComponentButtonGroupSize() Group by the fk_component_button_group_size column
 * @method     ChildComponent_button_groupQuery groupByFkComponentButtonGroupRole() Group by the fk_component_button_group_role column
 * @method     ChildComponent_button_groupQuery groupByIcon() Group by the icon column
 * @method     ChildComponent_button_groupQuery groupByLabel() Group by the label column
 * @method     ChildComponent_button_groupQuery groupByTooltip() Group by the tooltip column
 * @method     ChildComponent_button_groupQuery groupByAsDropdown() Group by the as_dropdown column
 * @method     ChildComponent_button_groupQuery groupByDefaultActive() Group by the default_active column
 * @method     ChildComponent_button_groupQuery groupByUiComponentId() Group by the ui_component_id column
 * @method     ChildComponent_button_groupQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildComponent_button_groupQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildComponent_button_groupQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildComponent_button_groupQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildComponent_button_groupQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildComponent_button_groupQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildComponent_button_groupQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildComponent_button_groupQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildComponent_button_groupQuery leftJoinComponentButtonGroupSize($relationAlias = null) Adds a LEFT JOIN clause to the query using the ComponentButtonGroupSize relation
 * @method     ChildComponent_button_groupQuery rightJoinComponentButtonGroupSize($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ComponentButtonGroupSize relation
 * @method     ChildComponent_button_groupQuery innerJoinComponentButtonGroupSize($relationAlias = null) Adds a INNER JOIN clause to the query using the ComponentButtonGroupSize relation
 *
 * @method     ChildComponent_button_groupQuery joinWithComponentButtonGroupSize($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ComponentButtonGroupSize relation
 *
 * @method     ChildComponent_button_groupQuery leftJoinWithComponentButtonGroupSize() Adds a LEFT JOIN clause and with to the query using the ComponentButtonGroupSize relation
 * @method     ChildComponent_button_groupQuery rightJoinWithComponentButtonGroupSize() Adds a RIGHT JOIN clause and with to the query using the ComponentButtonGroupSize relation
 * @method     ChildComponent_button_groupQuery innerJoinWithComponentButtonGroupSize() Adds a INNER JOIN clause and with to the query using the ComponentButtonGroupSize relation
 *
 * @method     ChildComponent_button_groupQuery leftJoinComponentButtonGroupRole($relationAlias = null) Adds a LEFT JOIN clause to the query using the ComponentButtonGroupRole relation
 * @method     ChildComponent_button_groupQuery rightJoinComponentButtonGroupRole($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ComponentButtonGroupRole relation
 * @method     ChildComponent_button_groupQuery innerJoinComponentButtonGroupRole($relationAlias = null) Adds a INNER JOIN clause to the query using the ComponentButtonGroupRole relation
 *
 * @method     ChildComponent_button_groupQuery joinWithComponentButtonGroupRole($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ComponentButtonGroupRole relation
 *
 * @method     ChildComponent_button_groupQuery leftJoinWithComponentButtonGroupRole() Adds a LEFT JOIN clause and with to the query using the ComponentButtonGroupRole relation
 * @method     ChildComponent_button_groupQuery rightJoinWithComponentButtonGroupRole() Adds a RIGHT JOIN clause and with to the query using the ComponentButtonGroupRole relation
 * @method     ChildComponent_button_groupQuery innerJoinWithComponentButtonGroupRole() Adds a INNER JOIN clause and with to the query using the ComponentButtonGroupRole relation
 *
 * @method     ChildComponent_button_groupQuery leftJoinId($relationAlias = null) Adds a LEFT JOIN clause to the query using the Id relation
 * @method     ChildComponent_button_groupQuery rightJoinId($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Id relation
 * @method     ChildComponent_button_groupQuery innerJoinId($relationAlias = null) Adds a INNER JOIN clause to the query using the Id relation
 *
 * @method     ChildComponent_button_groupQuery joinWithId($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Id relation
 *
 * @method     ChildComponent_button_groupQuery leftJoinWithId() Adds a LEFT JOIN clause and with to the query using the Id relation
 * @method     ChildComponent_button_groupQuery rightJoinWithId() Adds a RIGHT JOIN clause and with to the query using the Id relation
 * @method     ChildComponent_button_groupQuery innerJoinWithId() Adds a INNER JOIN clause and with to the query using the Id relation
 *
 * @method     \Model\System\LowCode\ButtonGroup\Component_button_group_sizeQuery|\Model\System\LowCode\ButtonGroup\Component_button_group_roleQuery|\Model\System\UI\UIComponentQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildComponent_button_group findOne(ConnectionInterface $con = null) Return the first ChildComponent_button_group matching the query
 * @method     ChildComponent_button_group findOneOrCreate(ConnectionInterface $con = null) Return the first ChildComponent_button_group matching the query, or a new ChildComponent_button_group object populated from the query conditions when no match is found
 *
 * @method     ChildComponent_button_group findOneByid(int $id) Return the first ChildComponent_button_group filtered by the id column
 * @method     ChildComponent_button_group findOneByTitle(string $title) Return the first ChildComponent_button_group filtered by the title column
 * @method     ChildComponent_button_group findOneByFkComponentButtonGroupSize(int $fk_component_button_group_size) Return the first ChildComponent_button_group filtered by the fk_component_button_group_size column
 * @method     ChildComponent_button_group findOneByFkComponentButtonGroupRole(int $fk_component_button_group_role) Return the first ChildComponent_button_group filtered by the fk_component_button_group_role column
 * @method     ChildComponent_button_group findOneByIcon(string $icon) Return the first ChildComponent_button_group filtered by the icon column
 * @method     ChildComponent_button_group findOneByLabel(string $label) Return the first ChildComponent_button_group filtered by the label column
 * @method     ChildComponent_button_group findOneByTooltip(string $tooltip) Return the first ChildComponent_button_group filtered by the tooltip column
 * @method     ChildComponent_button_group findOneByAsDropdown(string $as_dropdown) Return the first ChildComponent_button_group filtered by the as_dropdown column
 * @method     ChildComponent_button_group findOneByDefaultActive(string $default_active) Return the first ChildComponent_button_group filtered by the default_active column
 * @method     ChildComponent_button_group findOneByUiComponentId(int $ui_component_id) Return the first ChildComponent_button_group filtered by the ui_component_id column
 * @method     ChildComponent_button_group findOneByCreatedAt(string $created_at) Return the first ChildComponent_button_group filtered by the created_at column
 * @method     ChildComponent_button_group findOneByUpdatedAt(string $updated_at) Return the first ChildComponent_button_group filtered by the updated_at column *

 * @method     ChildComponent_button_group requirePk($key, ConnectionInterface $con = null) Return the ChildComponent_button_group by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group requireOne(ConnectionInterface $con = null) Return the first ChildComponent_button_group matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComponent_button_group requireOneByid(int $id) Return the first ChildComponent_button_group filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group requireOneByTitle(string $title) Return the first ChildComponent_button_group filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group requireOneByFkComponentButtonGroupSize(int $fk_component_button_group_size) Return the first ChildComponent_button_group filtered by the fk_component_button_group_size column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group requireOneByFkComponentButtonGroupRole(int $fk_component_button_group_role) Return the first ChildComponent_button_group filtered by the fk_component_button_group_role column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group requireOneByIcon(string $icon) Return the first ChildComponent_button_group filtered by the icon column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group requireOneByLabel(string $label) Return the first ChildComponent_button_group filtered by the label column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group requireOneByTooltip(string $tooltip) Return the first ChildComponent_button_group filtered by the tooltip column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group requireOneByAsDropdown(string $as_dropdown) Return the first ChildComponent_button_group filtered by the as_dropdown column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group requireOneByDefaultActive(string $default_active) Return the first ChildComponent_button_group filtered by the default_active column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group requireOneByUiComponentId(int $ui_component_id) Return the first ChildComponent_button_group filtered by the ui_component_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group requireOneByCreatedAt(string $created_at) Return the first ChildComponent_button_group filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group requireOneByUpdatedAt(string $updated_at) Return the first ChildComponent_button_group filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComponent_button_group[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildComponent_button_group objects based on current ModelCriteria
 * @method     ChildComponent_button_group[]|ObjectCollection findByid(int $id) Return ChildComponent_button_group objects filtered by the id column
 * @method     ChildComponent_button_group[]|ObjectCollection findByTitle(string $title) Return ChildComponent_button_group objects filtered by the title column
 * @method     ChildComponent_button_group[]|ObjectCollection findByFkComponentButtonGroupSize(int $fk_component_button_group_size) Return ChildComponent_button_group objects filtered by the fk_component_button_group_size column
 * @method     ChildComponent_button_group[]|ObjectCollection findByFkComponentButtonGroupRole(int $fk_component_button_group_role) Return ChildComponent_button_group objects filtered by the fk_component_button_group_role column
 * @method     ChildComponent_button_group[]|ObjectCollection findByIcon(string $icon) Return ChildComponent_button_group objects filtered by the icon column
 * @method     ChildComponent_button_group[]|ObjectCollection findByLabel(string $label) Return ChildComponent_button_group objects filtered by the label column
 * @method     ChildComponent_button_group[]|ObjectCollection findByTooltip(string $tooltip) Return ChildComponent_button_group objects filtered by the tooltip column
 * @method     ChildComponent_button_group[]|ObjectCollection findByAsDropdown(string $as_dropdown) Return ChildComponent_button_group objects filtered by the as_dropdown column
 * @method     ChildComponent_button_group[]|ObjectCollection findByDefaultActive(string $default_active) Return ChildComponent_button_group objects filtered by the default_active column
 * @method     ChildComponent_button_group[]|ObjectCollection findByUiComponentId(int $ui_component_id) Return ChildComponent_button_group objects filtered by the ui_component_id column
 * @method     ChildComponent_button_group[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildComponent_button_group objects filtered by the created_at column
 * @method     ChildComponent_button_group[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildComponent_button_group objects filtered by the updated_at column
 * @method     ChildComponent_button_group[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class Component_button_groupQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\LowCode\ButtonGroup\Base\Component_button_groupQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\LowCode\\ButtonGroup\\Component_button_group', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildComponent_button_groupQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildComponent_button_groupQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildComponent_button_groupQuery) {
            return $criteria;
        }
        $query = new ChildComponent_button_groupQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildComponent_button_group|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(Component_button_groupTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = Component_button_groupTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_button_group A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, title, fk_component_button_group_size, fk_component_button_group_role, icon, label, tooltip, as_dropdown, default_active, ui_component_id, created_at, updated_at FROM component_button_group WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildComponent_button_group $obj */
            $obj = new ChildComponent_button_group();
            $obj->hydrate($row);
            Component_button_groupTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildComponent_button_group|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(Component_button_groupTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(Component_button_groupTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(Component_button_groupTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(Component_button_groupTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_groupTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_groupTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the fk_component_button_group_size column
     *
     * Example usage:
     * <code>
     * $query->filterByFkComponentButtonGroupSize(1234); // WHERE fk_component_button_group_size = 1234
     * $query->filterByFkComponentButtonGroupSize(array(12, 34)); // WHERE fk_component_button_group_size IN (12, 34)
     * $query->filterByFkComponentButtonGroupSize(array('min' => 12)); // WHERE fk_component_button_group_size > 12
     * </code>
     *
     * @see       filterByComponentButtonGroupSize()
     *
     * @param     mixed $fkComponentButtonGroupSize The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByFkComponentButtonGroupSize($fkComponentButtonGroupSize = null, $comparison = null)
    {
        if (is_array($fkComponentButtonGroupSize)) {
            $useMinMax = false;
            if (isset($fkComponentButtonGroupSize['min'])) {
                $this->addUsingAlias(Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_SIZE, $fkComponentButtonGroupSize['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fkComponentButtonGroupSize['max'])) {
                $this->addUsingAlias(Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_SIZE, $fkComponentButtonGroupSize['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_SIZE, $fkComponentButtonGroupSize, $comparison);
    }

    /**
     * Filter the query on the fk_component_button_group_role column
     *
     * Example usage:
     * <code>
     * $query->filterByFkComponentButtonGroupRole(1234); // WHERE fk_component_button_group_role = 1234
     * $query->filterByFkComponentButtonGroupRole(array(12, 34)); // WHERE fk_component_button_group_role IN (12, 34)
     * $query->filterByFkComponentButtonGroupRole(array('min' => 12)); // WHERE fk_component_button_group_role > 12
     * </code>
     *
     * @see       filterByComponentButtonGroupRole()
     *
     * @param     mixed $fkComponentButtonGroupRole The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByFkComponentButtonGroupRole($fkComponentButtonGroupRole = null, $comparison = null)
    {
        if (is_array($fkComponentButtonGroupRole)) {
            $useMinMax = false;
            if (isset($fkComponentButtonGroupRole['min'])) {
                $this->addUsingAlias(Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_ROLE, $fkComponentButtonGroupRole['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fkComponentButtonGroupRole['max'])) {
                $this->addUsingAlias(Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_ROLE, $fkComponentButtonGroupRole['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_ROLE, $fkComponentButtonGroupRole, $comparison);
    }

    /**
     * Filter the query on the icon column
     *
     * Example usage:
     * <code>
     * $query->filterByIcon('fooValue');   // WHERE icon = 'fooValue'
     * $query->filterByIcon('%fooValue%', Criteria::LIKE); // WHERE icon LIKE '%fooValue%'
     * </code>
     *
     * @param     string $icon The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByIcon($icon = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($icon)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_groupTableMap::COL_ICON, $icon, $comparison);
    }

    /**
     * Filter the query on the label column
     *
     * Example usage:
     * <code>
     * $query->filterByLabel('fooValue');   // WHERE label = 'fooValue'
     * $query->filterByLabel('%fooValue%', Criteria::LIKE); // WHERE label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $label The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByLabel($label = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($label)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_groupTableMap::COL_LABEL, $label, $comparison);
    }

    /**
     * Filter the query on the tooltip column
     *
     * Example usage:
     * <code>
     * $query->filterByTooltip('fooValue');   // WHERE tooltip = 'fooValue'
     * $query->filterByTooltip('%fooValue%', Criteria::LIKE); // WHERE tooltip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tooltip The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByTooltip($tooltip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tooltip)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_groupTableMap::COL_TOOLTIP, $tooltip, $comparison);
    }

    /**
     * Filter the query on the as_dropdown column
     *
     * Example usage:
     * <code>
     * $query->filterByAsDropdown('fooValue');   // WHERE as_dropdown = 'fooValue'
     * $query->filterByAsDropdown('%fooValue%', Criteria::LIKE); // WHERE as_dropdown LIKE '%fooValue%'
     * </code>
     *
     * @param     string $asDropdown The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByAsDropdown($asDropdown = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($asDropdown)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_groupTableMap::COL_AS_DROPDOWN, $asDropdown, $comparison);
    }

    /**
     * Filter the query on the default_active column
     *
     * Example usage:
     * <code>
     * $query->filterByDefaultActive('fooValue');   // WHERE default_active = 'fooValue'
     * $query->filterByDefaultActive('%fooValue%', Criteria::LIKE); // WHERE default_active LIKE '%fooValue%'
     * </code>
     *
     * @param     string $defaultActive The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByDefaultActive($defaultActive = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($defaultActive)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_groupTableMap::COL_DEFAULT_ACTIVE, $defaultActive, $comparison);
    }

    /**
     * Filter the query on the ui_component_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUiComponentId(1234); // WHERE ui_component_id = 1234
     * $query->filterByUiComponentId(array(12, 34)); // WHERE ui_component_id IN (12, 34)
     * $query->filterByUiComponentId(array('min' => 12)); // WHERE ui_component_id > 12
     * </code>
     *
     * @see       filterById()
     *
     * @param     mixed $uiComponentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByUiComponentId($uiComponentId = null, $comparison = null)
    {
        if (is_array($uiComponentId)) {
            $useMinMax = false;
            if (isset($uiComponentId['min'])) {
                $this->addUsingAlias(Component_button_groupTableMap::COL_UI_COMPONENT_ID, $uiComponentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($uiComponentId['max'])) {
                $this->addUsingAlias(Component_button_groupTableMap::COL_UI_COMPONENT_ID, $uiComponentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_groupTableMap::COL_UI_COMPONENT_ID, $uiComponentId, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(Component_button_groupTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(Component_button_groupTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_groupTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(Component_button_groupTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(Component_button_groupTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_groupTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\LowCode\ButtonGroup\Component_button_group_size object
     *
     * @param \Model\System\LowCode\ButtonGroup\Component_button_group_size|ObjectCollection $component_button_group_size The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByComponentButtonGroupSize($component_button_group_size, $comparison = null)
    {
        if ($component_button_group_size instanceof \Model\System\LowCode\ButtonGroup\Component_button_group_size) {
            return $this
                ->addUsingAlias(Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_SIZE, $component_button_group_size->getid(), $comparison);
        } elseif ($component_button_group_size instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_SIZE, $component_button_group_size->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByComponentButtonGroupSize() only accepts arguments of type \Model\System\LowCode\ButtonGroup\Component_button_group_size or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ComponentButtonGroupSize relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function joinComponentButtonGroupSize($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ComponentButtonGroupSize');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ComponentButtonGroupSize');
        }

        return $this;
    }

    /**
     * Use the ComponentButtonGroupSize relation Component_button_group_size object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\LowCode\ButtonGroup\Component_button_group_sizeQuery A secondary query class using the current class as primary query
     */
    public function useComponentButtonGroupSizeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComponentButtonGroupSize($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ComponentButtonGroupSize', '\Model\System\LowCode\ButtonGroup\Component_button_group_sizeQuery');
    }

    /**
     * Filter the query by a related \Model\System\LowCode\ButtonGroup\Component_button_group_role object
     *
     * @param \Model\System\LowCode\ButtonGroup\Component_button_group_role|ObjectCollection $component_button_group_role The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterByComponentButtonGroupRole($component_button_group_role, $comparison = null)
    {
        if ($component_button_group_role instanceof \Model\System\LowCode\ButtonGroup\Component_button_group_role) {
            return $this
                ->addUsingAlias(Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_ROLE, $component_button_group_role->getid(), $comparison);
        } elseif ($component_button_group_role instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Component_button_groupTableMap::COL_FK_COMPONENT_BUTTON_GROUP_ROLE, $component_button_group_role->toKeyValue('PrimaryKey', 'id'), $comparison);
        } else {
            throw new PropelException('filterByComponentButtonGroupRole() only accepts arguments of type \Model\System\LowCode\ButtonGroup\Component_button_group_role or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ComponentButtonGroupRole relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function joinComponentButtonGroupRole($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ComponentButtonGroupRole');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ComponentButtonGroupRole');
        }

        return $this;
    }

    /**
     * Use the ComponentButtonGroupRole relation Component_button_group_role object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\LowCode\ButtonGroup\Component_button_group_roleQuery A secondary query class using the current class as primary query
     */
    public function useComponentButtonGroupRoleQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComponentButtonGroupRole($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ComponentButtonGroupRole', '\Model\System\LowCode\ButtonGroup\Component_button_group_roleQuery');
    }

    /**
     * Filter the query by a related \Model\System\UI\UIComponent object
     *
     * @param \Model\System\UI\UIComponent|ObjectCollection $uIComponent The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function filterById($uIComponent, $comparison = null)
    {
        if ($uIComponent instanceof \Model\System\UI\UIComponent) {
            return $this
                ->addUsingAlias(Component_button_groupTableMap::COL_UI_COMPONENT_ID, $uIComponent->getId(), $comparison);
        } elseif ($uIComponent instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(Component_button_groupTableMap::COL_UI_COMPONENT_ID, $uIComponent->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterById() only accepts arguments of type \Model\System\UI\UIComponent or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Id relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function joinId($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Id');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Id');
        }

        return $this;
    }

    /**
     * Use the Id relation UIComponent object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\UI\UIComponentQuery A secondary query class using the current class as primary query
     */
    public function useIdQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinId($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Id', '\Model\System\UI\UIComponentQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildComponent_button_group $component_button_group Object to remove from the list of results
     *
     * @return $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function prune($component_button_group = null)
    {
        if ($component_button_group) {
            $this->addUsingAlias(Component_button_groupTableMap::COL_ID, $component_button_group->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the component_button_group table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_button_groupTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            Component_button_groupTableMap::clearInstancePool();
            Component_button_groupTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_button_groupTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(Component_button_groupTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            Component_button_groupTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            Component_button_groupTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(Component_button_groupTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(Component_button_groupTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(Component_button_groupTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(Component_button_groupTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(Component_button_groupTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildComponent_button_groupQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(Component_button_groupTableMap::COL_CREATED_AT);
    }

} // Component_button_groupQuery
