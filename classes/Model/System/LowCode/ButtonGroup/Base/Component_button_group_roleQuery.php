<?php

namespace Model\System\LowCode\ButtonGroup\Base;

use \Exception;
use \PDO;
use Model\System\LowCode\ButtonGroup\Component_button_group_role as ChildComponent_button_group_role;
use Model\System\LowCode\ButtonGroup\Component_button_group_roleQuery as ChildComponent_button_group_roleQuery;
use Model\System\LowCode\ButtonGroup\Map\Component_button_group_roleTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'component_button_group_role' table.
 *
 *
 *
 * @method     ChildComponent_button_group_roleQuery orderByid($order = Criteria::ASC) Order by the id column
 * @method     ChildComponent_button_group_roleQuery orderByItemKey($order = Criteria::ASC) Order by the item_key column
 * @method     ChildComponent_button_group_roleQuery orderByItemLabel($order = Criteria::ASC) Order by the item_label column
 * @method     ChildComponent_button_group_roleQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildComponent_button_group_roleQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildComponent_button_group_roleQuery groupByid() Group by the id column
 * @method     ChildComponent_button_group_roleQuery groupByItemKey() Group by the item_key column
 * @method     ChildComponent_button_group_roleQuery groupByItemLabel() Group by the item_label column
 * @method     ChildComponent_button_group_roleQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildComponent_button_group_roleQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildComponent_button_group_roleQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildComponent_button_group_roleQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildComponent_button_group_roleQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildComponent_button_group_roleQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildComponent_button_group_roleQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildComponent_button_group_roleQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildComponent_button_group_roleQuery leftJoinComponent_button_group($relationAlias = null) Adds a LEFT JOIN clause to the query using the Component_button_group relation
 * @method     ChildComponent_button_group_roleQuery rightJoinComponent_button_group($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Component_button_group relation
 * @method     ChildComponent_button_group_roleQuery innerJoinComponent_button_group($relationAlias = null) Adds a INNER JOIN clause to the query using the Component_button_group relation
 *
 * @method     ChildComponent_button_group_roleQuery joinWithComponent_button_group($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Component_button_group relation
 *
 * @method     ChildComponent_button_group_roleQuery leftJoinWithComponent_button_group() Adds a LEFT JOIN clause and with to the query using the Component_button_group relation
 * @method     ChildComponent_button_group_roleQuery rightJoinWithComponent_button_group() Adds a RIGHT JOIN clause and with to the query using the Component_button_group relation
 * @method     ChildComponent_button_group_roleQuery innerJoinWithComponent_button_group() Adds a INNER JOIN clause and with to the query using the Component_button_group relation
 *
 * @method     \Model\System\LowCode\ButtonGroup\Component_button_groupQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildComponent_button_group_role findOne(ConnectionInterface $con = null) Return the first ChildComponent_button_group_role matching the query
 * @method     ChildComponent_button_group_role findOneOrCreate(ConnectionInterface $con = null) Return the first ChildComponent_button_group_role matching the query, or a new ChildComponent_button_group_role object populated from the query conditions when no match is found
 *
 * @method     ChildComponent_button_group_role findOneByid(int $id) Return the first ChildComponent_button_group_role filtered by the id column
 * @method     ChildComponent_button_group_role findOneByItemKey(string $item_key) Return the first ChildComponent_button_group_role filtered by the item_key column
 * @method     ChildComponent_button_group_role findOneByItemLabel(string $item_label) Return the first ChildComponent_button_group_role filtered by the item_label column
 * @method     ChildComponent_button_group_role findOneByCreatedAt(string $created_at) Return the first ChildComponent_button_group_role filtered by the created_at column
 * @method     ChildComponent_button_group_role findOneByUpdatedAt(string $updated_at) Return the first ChildComponent_button_group_role filtered by the updated_at column *

 * @method     ChildComponent_button_group_role requirePk($key, ConnectionInterface $con = null) Return the ChildComponent_button_group_role by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group_role requireOne(ConnectionInterface $con = null) Return the first ChildComponent_button_group_role matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComponent_button_group_role requireOneByid(int $id) Return the first ChildComponent_button_group_role filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group_role requireOneByItemKey(string $item_key) Return the first ChildComponent_button_group_role filtered by the item_key column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group_role requireOneByItemLabel(string $item_label) Return the first ChildComponent_button_group_role filtered by the item_label column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group_role requireOneByCreatedAt(string $created_at) Return the first ChildComponent_button_group_role filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildComponent_button_group_role requireOneByUpdatedAt(string $updated_at) Return the first ChildComponent_button_group_role filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildComponent_button_group_role[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildComponent_button_group_role objects based on current ModelCriteria
 * @method     ChildComponent_button_group_role[]|ObjectCollection findByid(int $id) Return ChildComponent_button_group_role objects filtered by the id column
 * @method     ChildComponent_button_group_role[]|ObjectCollection findByItemKey(string $item_key) Return ChildComponent_button_group_role objects filtered by the item_key column
 * @method     ChildComponent_button_group_role[]|ObjectCollection findByItemLabel(string $item_label) Return ChildComponent_button_group_role objects filtered by the item_label column
 * @method     ChildComponent_button_group_role[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildComponent_button_group_role objects filtered by the created_at column
 * @method     ChildComponent_button_group_role[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildComponent_button_group_role objects filtered by the updated_at column
 * @method     ChildComponent_button_group_role[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class Component_button_group_roleQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\System\LowCode\ButtonGroup\Base\Component_button_group_roleQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\System\\LowCode\\ButtonGroup\\Component_button_group_role', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildComponent_button_group_roleQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildComponent_button_group_roleQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildComponent_button_group_roleQuery) {
            return $criteria;
        }
        $query = new ChildComponent_button_group_roleQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildComponent_button_group_role|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(Component_button_group_roleTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = Component_button_group_roleTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildComponent_button_group_role A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, item_key, item_label, created_at, updated_at FROM component_button_group_role WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildComponent_button_group_role $obj */
            $obj = new ChildComponent_button_group_role();
            $obj->hydrate($row);
            Component_button_group_roleTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildComponent_button_group_role|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(Component_button_group_roleTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(Component_button_group_roleTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterByid(1234); // WHERE id = 1234
     * $query->filterByid(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterByid(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function filterByid($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(Component_button_group_roleTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(Component_button_group_roleTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_group_roleTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the item_key column
     *
     * Example usage:
     * <code>
     * $query->filterByItemKey('fooValue');   // WHERE item_key = 'fooValue'
     * $query->filterByItemKey('%fooValue%', Criteria::LIKE); // WHERE item_key LIKE '%fooValue%'
     * </code>
     *
     * @param     string $itemKey The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function filterByItemKey($itemKey = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($itemKey)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_group_roleTableMap::COL_ITEM_KEY, $itemKey, $comparison);
    }

    /**
     * Filter the query on the item_label column
     *
     * Example usage:
     * <code>
     * $query->filterByItemLabel('fooValue');   // WHERE item_label = 'fooValue'
     * $query->filterByItemLabel('%fooValue%', Criteria::LIKE); // WHERE item_label LIKE '%fooValue%'
     * </code>
     *
     * @param     string $itemLabel The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function filterByItemLabel($itemLabel = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($itemLabel)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_group_roleTableMap::COL_ITEM_LABEL, $itemLabel, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(Component_button_group_roleTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(Component_button_group_roleTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_group_roleTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(Component_button_group_roleTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(Component_button_group_roleTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Component_button_group_roleTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \Model\System\LowCode\ButtonGroup\Component_button_group object
     *
     * @param \Model\System\LowCode\ButtonGroup\Component_button_group|ObjectCollection $component_button_group the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function filterByComponent_button_group($component_button_group, $comparison = null)
    {
        if ($component_button_group instanceof \Model\System\LowCode\ButtonGroup\Component_button_group) {
            return $this
                ->addUsingAlias(Component_button_group_roleTableMap::COL_ID, $component_button_group->getFkComponentButtonGroupRole(), $comparison);
        } elseif ($component_button_group instanceof ObjectCollection) {
            return $this
                ->useComponent_button_groupQuery()
                ->filterByPrimaryKeys($component_button_group->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByComponent_button_group() only accepts arguments of type \Model\System\LowCode\ButtonGroup\Component_button_group or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Component_button_group relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function joinComponent_button_group($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Component_button_group');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Component_button_group');
        }

        return $this;
    }

    /**
     * Use the Component_button_group relation Component_button_group object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\System\LowCode\ButtonGroup\Component_button_groupQuery A secondary query class using the current class as primary query
     */
    public function useComponent_button_groupQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinComponent_button_group($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Component_button_group', '\Model\System\LowCode\ButtonGroup\Component_button_groupQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildComponent_button_group_role $component_button_group_role Object to remove from the list of results
     *
     * @return $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function prune($component_button_group_role = null)
    {
        if ($component_button_group_role) {
            $this->addUsingAlias(Component_button_group_roleTableMap::COL_ID, $component_button_group_role->getid(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the component_button_group_role table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_button_group_roleTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            Component_button_group_roleTableMap::clearInstancePool();
            Component_button_group_roleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_button_group_roleTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(Component_button_group_roleTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            Component_button_group_roleTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            Component_button_group_roleTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(Component_button_group_roleTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(Component_button_group_roleTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(Component_button_group_roleTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(Component_button_group_roleTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(Component_button_group_roleTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildComponent_button_group_roleQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(Component_button_group_roleTableMap::COL_CREATED_AT);
    }

} // Component_button_group_roleQuery
