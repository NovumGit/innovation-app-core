<?php

namespace Model\System\LowCode\ListView;

use Model\System\LowCode\ListView\Base\Component_list_viewQuery as BaseComponent_list_viewQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'component_list_view' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_list_viewQuery extends BaseComponent_list_viewQuery
{

}
