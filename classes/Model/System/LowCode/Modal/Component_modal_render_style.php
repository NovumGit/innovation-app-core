<?php

namespace Model\System\LowCode\Modal;

use Model\System\LowCode\Modal\Base\Component_modal_render_style as BaseComponent_modal_render_style;

/**
 * Skeleton subclass for representing a row from the 'component_modal_render_style' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_modal_render_style extends BaseComponent_modal_render_style
{

}
