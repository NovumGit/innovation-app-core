<?php

namespace Model\System\LowCode\Dialog\Map;

use Model\System\LowCode\Dialog\Component_dialog;
use Model\System\LowCode\Dialog\Component_dialogQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'component_dialog' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class Component_dialogTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.System.LowCode.Dialog.Map.Component_dialogTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'component_dialog';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\System\\LowCode\\Dialog\\Component_dialog';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.System.LowCode.Dialog.Component_dialog';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 9;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 9;

    /**
     * the column name for the id field
     */
    const COL_ID = 'component_dialog.id';

    /**
     * the column name for the component_key field
     */
    const COL_COMPONENT_KEY = 'component_dialog.component_key';

    /**
     * the column name for the title field
     */
    const COL_TITLE = 'component_dialog.title';

    /**
     * the column name for the message field
     */
    const COL_MESSAGE = 'component_dialog.message';

    /**
     * the column name for the fk_component_dialog_display field
     */
    const COL_FK_COMPONENT_DIALOG_DISPLAY = 'component_dialog.fk_component_dialog_display';

    /**
     * the column name for the fk_component_dialog_color_type field
     */
    const COL_FK_COMPONENT_DIALOG_COLOR_TYPE = 'component_dialog.fk_component_dialog_color_type';

    /**
     * the column name for the ui_component_id field
     */
    const COL_UI_COMPONENT_ID = 'component_dialog.ui_component_id';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'component_dialog.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'component_dialog.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('id', 'ComponentKey', 'Title', 'Message', 'FkComponentDialogDisplay', 'FkComponentDialogColorType', 'UiComponentId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'componentKey', 'title', 'message', 'fkComponentDialogDisplay', 'fkComponentDialogColorType', 'uiComponentId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(Component_dialogTableMap::COL_ID, Component_dialogTableMap::COL_COMPONENT_KEY, Component_dialogTableMap::COL_TITLE, Component_dialogTableMap::COL_MESSAGE, Component_dialogTableMap::COL_FK_COMPONENT_DIALOG_DISPLAY, Component_dialogTableMap::COL_FK_COMPONENT_DIALOG_COLOR_TYPE, Component_dialogTableMap::COL_UI_COMPONENT_ID, Component_dialogTableMap::COL_CREATED_AT, Component_dialogTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'component_key', 'title', 'message', 'fk_component_dialog_display', 'fk_component_dialog_color_type', 'ui_component_id', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('id' => 0, 'ComponentKey' => 1, 'Title' => 2, 'Message' => 3, 'FkComponentDialogDisplay' => 4, 'FkComponentDialogColorType' => 5, 'UiComponentId' => 6, 'CreatedAt' => 7, 'UpdatedAt' => 8, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'componentKey' => 1, 'title' => 2, 'message' => 3, 'fkComponentDialogDisplay' => 4, 'fkComponentDialogColorType' => 5, 'uiComponentId' => 6, 'createdAt' => 7, 'updatedAt' => 8, ),
        self::TYPE_COLNAME       => array(Component_dialogTableMap::COL_ID => 0, Component_dialogTableMap::COL_COMPONENT_KEY => 1, Component_dialogTableMap::COL_TITLE => 2, Component_dialogTableMap::COL_MESSAGE => 3, Component_dialogTableMap::COL_FK_COMPONENT_DIALOG_DISPLAY => 4, Component_dialogTableMap::COL_FK_COMPONENT_DIALOG_COLOR_TYPE => 5, Component_dialogTableMap::COL_UI_COMPONENT_ID => 6, Component_dialogTableMap::COL_CREATED_AT => 7, Component_dialogTableMap::COL_UPDATED_AT => 8, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'component_key' => 1, 'title' => 2, 'message' => 3, 'fk_component_dialog_display' => 4, 'fk_component_dialog_color_type' => 5, 'ui_component_id' => 6, 'created_at' => 7, 'updated_at' => 8, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('component_dialog');
        $this->setPhpName('Component_dialog');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\System\\LowCode\\Dialog\\Component_dialog');
        $this->setPackage('Model.System.LowCode.Dialog');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'id', 'INTEGER', true, null, null);
        $this->addColumn('component_key', 'ComponentKey', 'VARCHAR', false, 255, null);
        $this->addColumn('title', 'Title', 'VARCHAR', false, 255, null);
        $this->addColumn('message', 'Message', 'VARCHAR', false, 255, null);
        $this->addForeignKey('fk_component_dialog_display', 'FkComponentDialogDisplay', 'INTEGER', 'component_dialog_display', 'id', false, null, null);
        $this->addForeignKey('fk_component_dialog_color_type', 'FkComponentDialogColorType', 'INTEGER', 'component_dialog_color_type', 'id', false, null, null);
        $this->addForeignKey('ui_component_id', 'UiComponentId', 'INTEGER', 'ui_component', 'id', true, null, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ComponentDialogDisplay', '\\Model\\System\\LowCode\\Dialog\\Component_dialog_display', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':fk_component_dialog_display',
    1 => ':id',
  ),
), 'RESTRICT', 'RESTRICT', null, false);
        $this->addRelation('ComponentDialogColorType', '\\Model\\System\\LowCode\\Dialog\\Component_dialog_color_type', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':fk_component_dialog_color_type',
    1 => ':id',
  ),
), 'RESTRICT', 'RESTRICT', null, false);
        $this->addRelation('Id', '\\Model\\System\\UI\\UIComponent', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':ui_component_id',
    1 => ':id',
  ),
), 'CASCADE', 'CASCADE', null, false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? Component_dialogTableMap::CLASS_DEFAULT : Component_dialogTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Component_dialog object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = Component_dialogTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = Component_dialogTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + Component_dialogTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = Component_dialogTableMap::OM_CLASS;
            /** @var Component_dialog $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            Component_dialogTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = Component_dialogTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = Component_dialogTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Component_dialog $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                Component_dialogTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(Component_dialogTableMap::COL_ID);
            $criteria->addSelectColumn(Component_dialogTableMap::COL_COMPONENT_KEY);
            $criteria->addSelectColumn(Component_dialogTableMap::COL_TITLE);
            $criteria->addSelectColumn(Component_dialogTableMap::COL_MESSAGE);
            $criteria->addSelectColumn(Component_dialogTableMap::COL_FK_COMPONENT_DIALOG_DISPLAY);
            $criteria->addSelectColumn(Component_dialogTableMap::COL_FK_COMPONENT_DIALOG_COLOR_TYPE);
            $criteria->addSelectColumn(Component_dialogTableMap::COL_UI_COMPONENT_ID);
            $criteria->addSelectColumn(Component_dialogTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(Component_dialogTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.component_key');
            $criteria->addSelectColumn($alias . '.title');
            $criteria->addSelectColumn($alias . '.message');
            $criteria->addSelectColumn($alias . '.fk_component_dialog_display');
            $criteria->addSelectColumn($alias . '.fk_component_dialog_color_type');
            $criteria->addSelectColumn($alias . '.ui_component_id');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(Component_dialogTableMap::DATABASE_NAME)->getTable(Component_dialogTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(Component_dialogTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(Component_dialogTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new Component_dialogTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Component_dialog or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Component_dialog object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_dialogTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\System\LowCode\Dialog\Component_dialog) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(Component_dialogTableMap::DATABASE_NAME);
            $criteria->add(Component_dialogTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = Component_dialogQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            Component_dialogTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                Component_dialogTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the component_dialog table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return Component_dialogQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Component_dialog or Criteria object.
     *
     * @param mixed               $criteria Criteria or Component_dialog object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Component_dialogTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Component_dialog object
        }

        if ($criteria->containsKey(Component_dialogTableMap::COL_ID) && $criteria->keyContainsValue(Component_dialogTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.Component_dialogTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = Component_dialogQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // Component_dialogTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
Component_dialogTableMap::buildTableMap();
