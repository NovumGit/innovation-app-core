<?php

namespace Model\System\LowCode\Dialog;

use Model\System\LowCode\Dialog\Base\Component_dialog as BaseComponent_dialog;

/**
 * Skeleton subclass for representing a row from the 'component_dialog' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Component_dialog extends BaseComponent_dialog
{

}
