<?php

namespace Model\Api;

use Model\Api\Base\ApiMailchimpListQuery as BaseApiMailchimpListQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'api_mailchimp_list' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ApiMailchimpListQuery extends BaseApiMailchimpListQuery
{

}
