<?php

namespace Model\Logging\Base;

use \DateTime;
use \Exception;
use \PDO;
use Model\Logging\Except_log as ChildExcept_log;
use Model\Logging\Except_logQuery as ChildExcept_logQuery;
use Model\Logging\Map\Except_logTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'except_log' table.
 *
 *
 *
 * @package    propel.generator.Model.Logging.Base
 */
abstract class Except_log implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\Model\\Logging\\Map\\Except_logTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the id field.
     *
     * @var        int
     */
    protected $id;

    /**
     * The value for the exception_type field.
     *
     * @var        string|null
     */
    protected $exception_type;

    /**
     * The value for the fingerprint field.
     *
     * @var        string|null
     */
    protected $fingerprint;

    /**
     * The value for the message field.
     *
     * @var        string|null
     */
    protected $message;

    /**
     * The value for the file field.
     *
     * @var        string|null
     */
    protected $file;

    /**
     * The value for the line field.
     *
     * @var        int|null
     */
    protected $line;

    /**
     * The value for the backtrace field.
     *
     * @var        string|null
     */
    protected $backtrace;

    /**
     * The value for the first_occurence field.
     *
     * @var        DateTime|null
     */
    protected $first_occurence;

    /**
     * The value for the last_occurence field.
     *
     * @var        DateTime|null
     */
    protected $last_occurence;

    /**
     * The value for the occurence_count field.
     *
     * @var        int|null
     */
    protected $occurence_count;

    /**
     * The value for the domain field.
     *
     * @var        string|null
     */
    protected $domain;

    /**
     * The value for the url field.
     *
     * @var        string|null
     */
    protected $url;

    /**
     * The value for the referrer field.
     *
     * @var        string|null
     */
    protected $referrer;

    /**
     * The value for the serialized_session field.
     *
     * @var        string|null
     */
    protected $serialized_session;

    /**
     * The value for the serialized_get field.
     *
     * @var        string|null
     */
    protected $serialized_get;

    /**
     * The value for the serialized_post field.
     *
     * @var        string|null
     */
    protected $serialized_post;

    /**
     * The value for the cookie_string field.
     *
     * @var        string|null
     */
    protected $cookie_string;

    /**
     * The value for the user_agent field.
     *
     * @var        string|null
     */
    protected $user_agent;

    /**
     * The value for the ip field.
     *
     * @var        string|null
     */
    protected $ip;

    /**
     * The value for the fatal field.
     *
     * @var        boolean|null
     */
    protected $fatal;

    /**
     * The value for the created_at field.
     *
     * @var        DateTime|null
     */
    protected $created_at;

    /**
     * The value for the updated_at field.
     *
     * @var        DateTime|null
     */
    protected $updated_at;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * Initializes internal state of Model\Logging\Base\Except_log object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Except_log</code> instance.  If
     * <code>obj</code> is an instance of <code>Except_log</code>, delegates to
     * <code>equals(Except_log)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return void
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [exception_type] column value.
     *
     * @return string|null
     */
    public function getExceptionType()
    {
        return $this->exception_type;
    }

    /**
     * Get the [fingerprint] column value.
     *
     * @return string|null
     */
    public function getFingerprint()
    {
        return $this->fingerprint;
    }

    /**
     * Get the [message] column value.
     *
     * @return string|null
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Get the [file] column value.
     *
     * @return string|null
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Get the [line] column value.
     *
     * @return int|null
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * Get the [backtrace] column value.
     *
     * @return string|null
     */
    public function getBacktrace()
    {
        return $this->backtrace;
    }

    /**
     * Get the [optionally formatted] temporal [first_occurence] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFirstOccurence($format = null)
    {
        if ($format === null) {
            return $this->first_occurence;
        } else {
            return $this->first_occurence instanceof \DateTimeInterface ? $this->first_occurence->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [last_occurence] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getLastOccurence($format = null)
    {
        if ($format === null) {
            return $this->last_occurence;
        } else {
            return $this->last_occurence instanceof \DateTimeInterface ? $this->last_occurence->format($format) : null;
        }
    }

    /**
     * Get the [occurence_count] column value.
     *
     * @return int|null
     */
    public function getOccurenceCount()
    {
        return $this->occurence_count;
    }

    /**
     * Get the [domain] column value.
     *
     * @return string|null
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Get the [url] column value.
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the [referrer] column value.
     *
     * @return string|null
     */
    public function getReferrer()
    {
        return $this->referrer;
    }

    /**
     * Get the [serialized_session] column value.
     *
     * @return string|null
     */
    public function getSerializedSession()
    {
        return $this->serialized_session;
    }

    /**
     * Get the [serialized_get] column value.
     *
     * @return string|null
     */
    public function getSerializedGet()
    {
        return $this->serialized_get;
    }

    /**
     * Get the [serialized_post] column value.
     *
     * @return string|null
     */
    public function getSerializedPost()
    {
        return $this->serialized_post;
    }

    /**
     * Get the [cookie_string] column value.
     *
     * @return string|null
     */
    public function getCookieString()
    {
        return $this->cookie_string;
    }

    /**
     * Get the [user_agent] column value.
     *
     * @return string|null
     */
    public function getUserAgent()
    {
        return $this->user_agent;
    }

    /**
     * Get the [ip] column value.
     *
     * @return string|null
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Get the [fatal] column value.
     *
     * @return boolean|null
     */
    public function getFatal()
    {
        return $this->fatal;
    }

    /**
     * Get the [fatal] column value.
     *
     * @return boolean|null
     */
    public function isFatal()
    {
        return $this->getFatal();
    }

    /**
     * Get the [optionally formatted] temporal [created_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = null)
    {
        if ($format === null) {
            return $this->created_at;
        } else {
            return $this->created_at instanceof \DateTimeInterface ? $this->created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [updated_at] column value.
     *
     *
     * @param string|null $format The date/time format string (either date()-style or strftime()-style).
     *   If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime|null Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = null)
    {
        if ($format === null) {
            return $this->updated_at;
        } else {
            return $this->updated_at instanceof \DateTimeInterface ? $this->updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [id] column.
     *
     * @param int $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[Except_logTableMap::COL_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [exception_type] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setExceptionType($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->exception_type !== $v) {
            $this->exception_type = $v;
            $this->modifiedColumns[Except_logTableMap::COL_EXCEPTION_TYPE] = true;
        }

        return $this;
    } // setExceptionType()

    /**
     * Set the value of [fingerprint] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setFingerprint($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->fingerprint !== $v) {
            $this->fingerprint = $v;
            $this->modifiedColumns[Except_logTableMap::COL_FINGERPRINT] = true;
        }

        return $this;
    } // setFingerprint()

    /**
     * Set the value of [message] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setMessage($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->message !== $v) {
            $this->message = $v;
            $this->modifiedColumns[Except_logTableMap::COL_MESSAGE] = true;
        }

        return $this;
    } // setMessage()

    /**
     * Set the value of [file] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setFile($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->file !== $v) {
            $this->file = $v;
            $this->modifiedColumns[Except_logTableMap::COL_FILE] = true;
        }

        return $this;
    } // setFile()

    /**
     * Set the value of [line] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setLine($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->line !== $v) {
            $this->line = $v;
            $this->modifiedColumns[Except_logTableMap::COL_LINE] = true;
        }

        return $this;
    } // setLine()

    /**
     * Set the value of [backtrace] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setBacktrace($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->backtrace !== $v) {
            $this->backtrace = $v;
            $this->modifiedColumns[Except_logTableMap::COL_BACKTRACE] = true;
        }

        return $this;
    } // setBacktrace()

    /**
     * Sets the value of [first_occurence] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setFirstOccurence($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->first_occurence !== null || $dt !== null) {
            if ($this->first_occurence === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->first_occurence->format("Y-m-d H:i:s.u")) {
                $this->first_occurence = $dt === null ? null : clone $dt;
                $this->modifiedColumns[Except_logTableMap::COL_FIRST_OCCURENCE] = true;
            }
        } // if either are not null

        return $this;
    } // setFirstOccurence()

    /**
     * Sets the value of [last_occurence] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setLastOccurence($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->last_occurence !== null || $dt !== null) {
            if ($this->last_occurence === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->last_occurence->format("Y-m-d H:i:s.u")) {
                $this->last_occurence = $dt === null ? null : clone $dt;
                $this->modifiedColumns[Except_logTableMap::COL_LAST_OCCURENCE] = true;
            }
        } // if either are not null

        return $this;
    } // setLastOccurence()

    /**
     * Set the value of [occurence_count] column.
     *
     * @param int|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setOccurenceCount($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->occurence_count !== $v) {
            $this->occurence_count = $v;
            $this->modifiedColumns[Except_logTableMap::COL_OCCURENCE_COUNT] = true;
        }

        return $this;
    } // setOccurenceCount()

    /**
     * Set the value of [domain] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setDomain($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->domain !== $v) {
            $this->domain = $v;
            $this->modifiedColumns[Except_logTableMap::COL_DOMAIN] = true;
        }

        return $this;
    } // setDomain()

    /**
     * Set the value of [url] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setUrl($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->url !== $v) {
            $this->url = $v;
            $this->modifiedColumns[Except_logTableMap::COL_URL] = true;
        }

        return $this;
    } // setUrl()

    /**
     * Set the value of [referrer] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setReferrer($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->referrer !== $v) {
            $this->referrer = $v;
            $this->modifiedColumns[Except_logTableMap::COL_REFERRER] = true;
        }

        return $this;
    } // setReferrer()

    /**
     * Set the value of [serialized_session] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setSerializedSession($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->serialized_session !== $v) {
            $this->serialized_session = $v;
            $this->modifiedColumns[Except_logTableMap::COL_SERIALIZED_SESSION] = true;
        }

        return $this;
    } // setSerializedSession()

    /**
     * Set the value of [serialized_get] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setSerializedGet($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->serialized_get !== $v) {
            $this->serialized_get = $v;
            $this->modifiedColumns[Except_logTableMap::COL_SERIALIZED_GET] = true;
        }

        return $this;
    } // setSerializedGet()

    /**
     * Set the value of [serialized_post] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setSerializedPost($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->serialized_post !== $v) {
            $this->serialized_post = $v;
            $this->modifiedColumns[Except_logTableMap::COL_SERIALIZED_POST] = true;
        }

        return $this;
    } // setSerializedPost()

    /**
     * Set the value of [cookie_string] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setCookieString($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cookie_string !== $v) {
            $this->cookie_string = $v;
            $this->modifiedColumns[Except_logTableMap::COL_COOKIE_STRING] = true;
        }

        return $this;
    } // setCookieString()

    /**
     * Set the value of [user_agent] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setUserAgent($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->user_agent !== $v) {
            $this->user_agent = $v;
            $this->modifiedColumns[Except_logTableMap::COL_USER_AGENT] = true;
        }

        return $this;
    } // setUserAgent()

    /**
     * Set the value of [ip] column.
     *
     * @param string|null $v New value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setIp($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->ip !== $v) {
            $this->ip = $v;
            $this->modifiedColumns[Except_logTableMap::COL_IP] = true;
        }

        return $this;
    } // setIp()

    /**
     * Sets the value of the [fatal] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string|null $v The new value
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setFatal($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->fatal !== $v) {
            $this->fatal = $v;
            $this->modifiedColumns[Except_logTableMap::COL_FATAL] = true;
        }

        return $this;
    } // setFatal()

    /**
     * Sets the value of [created_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->created_at !== null || $dt !== null) {
            if ($this->created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->created_at->format("Y-m-d H:i:s.u")) {
                $this->created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[Except_logTableMap::COL_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  string|integer|\DateTimeInterface|null $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\Model\Logging\Except_log The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->updated_at !== null || $dt !== null) {
            if ($this->updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->updated_at->format("Y-m-d H:i:s.u")) {
                $this->updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[Except_logTableMap::COL_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : Except_logTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : Except_logTableMap::translateFieldName('ExceptionType', TableMap::TYPE_PHPNAME, $indexType)];
            $this->exception_type = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : Except_logTableMap::translateFieldName('Fingerprint', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fingerprint = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : Except_logTableMap::translateFieldName('Message', TableMap::TYPE_PHPNAME, $indexType)];
            $this->message = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : Except_logTableMap::translateFieldName('File', TableMap::TYPE_PHPNAME, $indexType)];
            $this->file = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : Except_logTableMap::translateFieldName('Line', TableMap::TYPE_PHPNAME, $indexType)];
            $this->line = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : Except_logTableMap::translateFieldName('Backtrace', TableMap::TYPE_PHPNAME, $indexType)];
            $this->backtrace = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : Except_logTableMap::translateFieldName('FirstOccurence', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->first_occurence = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : Except_logTableMap::translateFieldName('LastOccurence', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->last_occurence = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : Except_logTableMap::translateFieldName('OccurenceCount', TableMap::TYPE_PHPNAME, $indexType)];
            $this->occurence_count = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : Except_logTableMap::translateFieldName('Domain', TableMap::TYPE_PHPNAME, $indexType)];
            $this->domain = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : Except_logTableMap::translateFieldName('Url', TableMap::TYPE_PHPNAME, $indexType)];
            $this->url = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : Except_logTableMap::translateFieldName('Referrer', TableMap::TYPE_PHPNAME, $indexType)];
            $this->referrer = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : Except_logTableMap::translateFieldName('SerializedSession', TableMap::TYPE_PHPNAME, $indexType)];
            $this->serialized_session = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : Except_logTableMap::translateFieldName('SerializedGet', TableMap::TYPE_PHPNAME, $indexType)];
            $this->serialized_get = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : Except_logTableMap::translateFieldName('SerializedPost', TableMap::TYPE_PHPNAME, $indexType)];
            $this->serialized_post = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : Except_logTableMap::translateFieldName('CookieString', TableMap::TYPE_PHPNAME, $indexType)];
            $this->cookie_string = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : Except_logTableMap::translateFieldName('UserAgent', TableMap::TYPE_PHPNAME, $indexType)];
            $this->user_agent = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 18 + $startcol : Except_logTableMap::translateFieldName('Ip', TableMap::TYPE_PHPNAME, $indexType)];
            $this->ip = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 19 + $startcol : Except_logTableMap::translateFieldName('Fatal', TableMap::TYPE_PHPNAME, $indexType)];
            $this->fatal = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 20 + $startcol : Except_logTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 21 + $startcol : Except_logTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 22; // 22 = Except_logTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\Model\\Logging\\Except_log'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(Except_logTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildExcept_logQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Except_log::setDeleted()
     * @see Except_log::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(Except_logTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildExcept_logQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($this->alreadyInSave) {
            return 0;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(Except_logTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior
                $time = time();
                $highPrecision = \Propel\Runtime\Util\PropelDateTime::createHighPrecision();
                if (!$this->isColumnModified(Except_logTableMap::COL_CREATED_AT)) {
                    $this->setCreatedAt($highPrecision);
                }
                if (!$this->isColumnModified(Except_logTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt($highPrecision);
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(Except_logTableMap::COL_UPDATED_AT)) {
                    $this->setUpdatedAt(\Propel\Runtime\Util\PropelDateTime::createHighPrecision());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                Except_logTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[Except_logTableMap::COL_ID] = true;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . Except_logTableMap::COL_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(Except_logTableMap::COL_ID)) {
            $modifiedColumns[':p' . $index++]  = 'id';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_EXCEPTION_TYPE)) {
            $modifiedColumns[':p' . $index++]  = 'exception_type';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_FINGERPRINT)) {
            $modifiedColumns[':p' . $index++]  = 'fingerprint';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_MESSAGE)) {
            $modifiedColumns[':p' . $index++]  = 'message';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_FILE)) {
            $modifiedColumns[':p' . $index++]  = 'file';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_LINE)) {
            $modifiedColumns[':p' . $index++]  = 'line';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_BACKTRACE)) {
            $modifiedColumns[':p' . $index++]  = 'backtrace';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_FIRST_OCCURENCE)) {
            $modifiedColumns[':p' . $index++]  = 'first_occurence';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_LAST_OCCURENCE)) {
            $modifiedColumns[':p' . $index++]  = 'last_occurence';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_OCCURENCE_COUNT)) {
            $modifiedColumns[':p' . $index++]  = 'occurence_count';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_DOMAIN)) {
            $modifiedColumns[':p' . $index++]  = 'domain';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_URL)) {
            $modifiedColumns[':p' . $index++]  = 'url';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_REFERRER)) {
            $modifiedColumns[':p' . $index++]  = 'referrer';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_SERIALIZED_SESSION)) {
            $modifiedColumns[':p' . $index++]  = 'serialized_session';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_SERIALIZED_GET)) {
            $modifiedColumns[':p' . $index++]  = 'serialized_get';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_SERIALIZED_POST)) {
            $modifiedColumns[':p' . $index++]  = 'serialized_post';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_COOKIE_STRING)) {
            $modifiedColumns[':p' . $index++]  = 'cookie_string';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_USER_AGENT)) {
            $modifiedColumns[':p' . $index++]  = 'user_agent';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_IP)) {
            $modifiedColumns[':p' . $index++]  = 'ip';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_FATAL)) {
            $modifiedColumns[':p' . $index++]  = 'fatal';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'created_at';
        }
        if ($this->isColumnModified(Except_logTableMap::COL_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'updated_at';
        }

        $sql = sprintf(
            'INSERT INTO except_log (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'id':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case 'exception_type':
                        $stmt->bindValue($identifier, $this->exception_type, PDO::PARAM_STR);
                        break;
                    case 'fingerprint':
                        $stmt->bindValue($identifier, $this->fingerprint, PDO::PARAM_STR);
                        break;
                    case 'message':
                        $stmt->bindValue($identifier, $this->message, PDO::PARAM_STR);
                        break;
                    case 'file':
                        $stmt->bindValue($identifier, $this->file, PDO::PARAM_STR);
                        break;
                    case 'line':
                        $stmt->bindValue($identifier, $this->line, PDO::PARAM_INT);
                        break;
                    case 'backtrace':
                        $stmt->bindValue($identifier, $this->backtrace, PDO::PARAM_STR);
                        break;
                    case 'first_occurence':
                        $stmt->bindValue($identifier, $this->first_occurence ? $this->first_occurence->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'last_occurence':
                        $stmt->bindValue($identifier, $this->last_occurence ? $this->last_occurence->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'occurence_count':
                        $stmt->bindValue($identifier, $this->occurence_count, PDO::PARAM_INT);
                        break;
                    case 'domain':
                        $stmt->bindValue($identifier, $this->domain, PDO::PARAM_STR);
                        break;
                    case 'url':
                        $stmt->bindValue($identifier, $this->url, PDO::PARAM_STR);
                        break;
                    case 'referrer':
                        $stmt->bindValue($identifier, $this->referrer, PDO::PARAM_STR);
                        break;
                    case 'serialized_session':
                        $stmt->bindValue($identifier, $this->serialized_session, PDO::PARAM_STR);
                        break;
                    case 'serialized_get':
                        $stmt->bindValue($identifier, $this->serialized_get, PDO::PARAM_STR);
                        break;
                    case 'serialized_post':
                        $stmt->bindValue($identifier, $this->serialized_post, PDO::PARAM_STR);
                        break;
                    case 'cookie_string':
                        $stmt->bindValue($identifier, $this->cookie_string, PDO::PARAM_STR);
                        break;
                    case 'user_agent':
                        $stmt->bindValue($identifier, $this->user_agent, PDO::PARAM_STR);
                        break;
                    case 'ip':
                        $stmt->bindValue($identifier, $this->ip, PDO::PARAM_STR);
                        break;
                    case 'fatal':
                        $stmt->bindValue($identifier, (int) $this->fatal, PDO::PARAM_INT);
                        break;
                    case 'created_at':
                        $stmt->bindValue($identifier, $this->created_at ? $this->created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'updated_at':
                        $stmt->bindValue($identifier, $this->updated_at ? $this->updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = Except_logTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getExceptionType();
                break;
            case 2:
                return $this->getFingerprint();
                break;
            case 3:
                return $this->getMessage();
                break;
            case 4:
                return $this->getFile();
                break;
            case 5:
                return $this->getLine();
                break;
            case 6:
                return $this->getBacktrace();
                break;
            case 7:
                return $this->getFirstOccurence();
                break;
            case 8:
                return $this->getLastOccurence();
                break;
            case 9:
                return $this->getOccurenceCount();
                break;
            case 10:
                return $this->getDomain();
                break;
            case 11:
                return $this->getUrl();
                break;
            case 12:
                return $this->getReferrer();
                break;
            case 13:
                return $this->getSerializedSession();
                break;
            case 14:
                return $this->getSerializedGet();
                break;
            case 15:
                return $this->getSerializedPost();
                break;
            case 16:
                return $this->getCookieString();
                break;
            case 17:
                return $this->getUserAgent();
                break;
            case 18:
                return $this->getIp();
                break;
            case 19:
                return $this->getFatal();
                break;
            case 20:
                return $this->getCreatedAt();
                break;
            case 21:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {

        if (isset($alreadyDumpedObjects['Except_log'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Except_log'][$this->hashCode()] = true;
        $keys = Except_logTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getExceptionType(),
            $keys[2] => $this->getFingerprint(),
            $keys[3] => $this->getMessage(),
            $keys[4] => $this->getFile(),
            $keys[5] => $this->getLine(),
            $keys[6] => $this->getBacktrace(),
            $keys[7] => $this->getFirstOccurence(),
            $keys[8] => $this->getLastOccurence(),
            $keys[9] => $this->getOccurenceCount(),
            $keys[10] => $this->getDomain(),
            $keys[11] => $this->getUrl(),
            $keys[12] => $this->getReferrer(),
            $keys[13] => $this->getSerializedSession(),
            $keys[14] => $this->getSerializedGet(),
            $keys[15] => $this->getSerializedPost(),
            $keys[16] => $this->getCookieString(),
            $keys[17] => $this->getUserAgent(),
            $keys[18] => $this->getIp(),
            $keys[19] => $this->getFatal(),
            $keys[20] => $this->getCreatedAt(),
            $keys[21] => $this->getUpdatedAt(),
        );
        if ($result[$keys[7]] instanceof \DateTimeInterface) {
            $result[$keys[7]] = $result[$keys[7]]->format('c');
        }

        if ($result[$keys[8]] instanceof \DateTimeInterface) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        if ($result[$keys[20]] instanceof \DateTimeInterface) {
            $result[$keys[20]] = $result[$keys[20]]->format('c');
        }

        if ($result[$keys[21]] instanceof \DateTimeInterface) {
            $result[$keys[21]] = $result[$keys[21]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }


        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\Model\Logging\Except_log
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = Except_logTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\Model\Logging\Except_log
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setExceptionType($value);
                break;
            case 2:
                $this->setFingerprint($value);
                break;
            case 3:
                $this->setMessage($value);
                break;
            case 4:
                $this->setFile($value);
                break;
            case 5:
                $this->setLine($value);
                break;
            case 6:
                $this->setBacktrace($value);
                break;
            case 7:
                $this->setFirstOccurence($value);
                break;
            case 8:
                $this->setLastOccurence($value);
                break;
            case 9:
                $this->setOccurenceCount($value);
                break;
            case 10:
                $this->setDomain($value);
                break;
            case 11:
                $this->setUrl($value);
                break;
            case 12:
                $this->setReferrer($value);
                break;
            case 13:
                $this->setSerializedSession($value);
                break;
            case 14:
                $this->setSerializedGet($value);
                break;
            case 15:
                $this->setSerializedPost($value);
                break;
            case 16:
                $this->setCookieString($value);
                break;
            case 17:
                $this->setUserAgent($value);
                break;
            case 18:
                $this->setIp($value);
                break;
            case 19:
                $this->setFatal($value);
                break;
            case 20:
                $this->setCreatedAt($value);
                break;
            case 21:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = Except_logTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setExceptionType($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setFingerprint($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setMessage($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setFile($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setLine($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setBacktrace($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setFirstOccurence($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setLastOccurence($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setOccurenceCount($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setDomain($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setUrl($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setReferrer($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setSerializedSession($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setSerializedGet($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setSerializedPost($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setCookieString($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setUserAgent($arr[$keys[17]]);
        }
        if (array_key_exists($keys[18], $arr)) {
            $this->setIp($arr[$keys[18]]);
        }
        if (array_key_exists($keys[19], $arr)) {
            $this->setFatal($arr[$keys[19]]);
        }
        if (array_key_exists($keys[20], $arr)) {
            $this->setCreatedAt($arr[$keys[20]]);
        }
        if (array_key_exists($keys[21], $arr)) {
            $this->setUpdatedAt($arr[$keys[21]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\Model\Logging\Except_log The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(Except_logTableMap::DATABASE_NAME);

        if ($this->isColumnModified(Except_logTableMap::COL_ID)) {
            $criteria->add(Except_logTableMap::COL_ID, $this->id);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_EXCEPTION_TYPE)) {
            $criteria->add(Except_logTableMap::COL_EXCEPTION_TYPE, $this->exception_type);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_FINGERPRINT)) {
            $criteria->add(Except_logTableMap::COL_FINGERPRINT, $this->fingerprint);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_MESSAGE)) {
            $criteria->add(Except_logTableMap::COL_MESSAGE, $this->message);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_FILE)) {
            $criteria->add(Except_logTableMap::COL_FILE, $this->file);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_LINE)) {
            $criteria->add(Except_logTableMap::COL_LINE, $this->line);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_BACKTRACE)) {
            $criteria->add(Except_logTableMap::COL_BACKTRACE, $this->backtrace);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_FIRST_OCCURENCE)) {
            $criteria->add(Except_logTableMap::COL_FIRST_OCCURENCE, $this->first_occurence);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_LAST_OCCURENCE)) {
            $criteria->add(Except_logTableMap::COL_LAST_OCCURENCE, $this->last_occurence);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_OCCURENCE_COUNT)) {
            $criteria->add(Except_logTableMap::COL_OCCURENCE_COUNT, $this->occurence_count);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_DOMAIN)) {
            $criteria->add(Except_logTableMap::COL_DOMAIN, $this->domain);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_URL)) {
            $criteria->add(Except_logTableMap::COL_URL, $this->url);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_REFERRER)) {
            $criteria->add(Except_logTableMap::COL_REFERRER, $this->referrer);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_SERIALIZED_SESSION)) {
            $criteria->add(Except_logTableMap::COL_SERIALIZED_SESSION, $this->serialized_session);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_SERIALIZED_GET)) {
            $criteria->add(Except_logTableMap::COL_SERIALIZED_GET, $this->serialized_get);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_SERIALIZED_POST)) {
            $criteria->add(Except_logTableMap::COL_SERIALIZED_POST, $this->serialized_post);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_COOKIE_STRING)) {
            $criteria->add(Except_logTableMap::COL_COOKIE_STRING, $this->cookie_string);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_USER_AGENT)) {
            $criteria->add(Except_logTableMap::COL_USER_AGENT, $this->user_agent);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_IP)) {
            $criteria->add(Except_logTableMap::COL_IP, $this->ip);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_FATAL)) {
            $criteria->add(Except_logTableMap::COL_FATAL, $this->fatal);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_CREATED_AT)) {
            $criteria->add(Except_logTableMap::COL_CREATED_AT, $this->created_at);
        }
        if ($this->isColumnModified(Except_logTableMap::COL_UPDATED_AT)) {
            $criteria->add(Except_logTableMap::COL_UPDATED_AT, $this->updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildExcept_logQuery::create();
        $criteria->add(Except_logTableMap::COL_ID, $this->id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \Model\Logging\Except_log (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setExceptionType($this->getExceptionType());
        $copyObj->setFingerprint($this->getFingerprint());
        $copyObj->setMessage($this->getMessage());
        $copyObj->setFile($this->getFile());
        $copyObj->setLine($this->getLine());
        $copyObj->setBacktrace($this->getBacktrace());
        $copyObj->setFirstOccurence($this->getFirstOccurence());
        $copyObj->setLastOccurence($this->getLastOccurence());
        $copyObj->setOccurenceCount($this->getOccurenceCount());
        $copyObj->setDomain($this->getDomain());
        $copyObj->setUrl($this->getUrl());
        $copyObj->setReferrer($this->getReferrer());
        $copyObj->setSerializedSession($this->getSerializedSession());
        $copyObj->setSerializedGet($this->getSerializedGet());
        $copyObj->setSerializedPost($this->getSerializedPost());
        $copyObj->setCookieString($this->getCookieString());
        $copyObj->setUserAgent($this->getUserAgent());
        $copyObj->setIp($this->getIp());
        $copyObj->setFatal($this->getFatal());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \Model\Logging\Except_log Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        $this->id = null;
        $this->exception_type = null;
        $this->fingerprint = null;
        $this->message = null;
        $this->file = null;
        $this->line = null;
        $this->backtrace = null;
        $this->first_occurence = null;
        $this->last_occurence = null;
        $this->occurence_count = null;
        $this->domain = null;
        $this->url = null;
        $this->referrer = null;
        $this->serialized_session = null;
        $this->serialized_get = null;
        $this->serialized_post = null;
        $this->cookie_string = null;
        $this->user_agent = null;
        $this->ip = null;
        $this->fatal = null;
        $this->created_at = null;
        $this->updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
        } // if ($deep)

    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(Except_logTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildExcept_log The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[Except_logTableMap::COL_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
            }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
                return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
            }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}
