<?php

namespace Model\Logging\Base;

use \Exception;
use \PDO;
use Model\Logging\Except_log as ChildExcept_log;
use Model\Logging\Except_logQuery as ChildExcept_logQuery;
use Model\Logging\Map\Except_logTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'except_log' table.
 *
 *
 *
 * @method     ChildExcept_logQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildExcept_logQuery orderByExceptionType($order = Criteria::ASC) Order by the exception_type column
 * @method     ChildExcept_logQuery orderByFingerprint($order = Criteria::ASC) Order by the fingerprint column
 * @method     ChildExcept_logQuery orderByMessage($order = Criteria::ASC) Order by the message column
 * @method     ChildExcept_logQuery orderByFile($order = Criteria::ASC) Order by the file column
 * @method     ChildExcept_logQuery orderByLine($order = Criteria::ASC) Order by the line column
 * @method     ChildExcept_logQuery orderByBacktrace($order = Criteria::ASC) Order by the backtrace column
 * @method     ChildExcept_logQuery orderByFirstOccurence($order = Criteria::ASC) Order by the first_occurence column
 * @method     ChildExcept_logQuery orderByLastOccurence($order = Criteria::ASC) Order by the last_occurence column
 * @method     ChildExcept_logQuery orderByOccurenceCount($order = Criteria::ASC) Order by the occurence_count column
 * @method     ChildExcept_logQuery orderByDomain($order = Criteria::ASC) Order by the domain column
 * @method     ChildExcept_logQuery orderByUrl($order = Criteria::ASC) Order by the url column
 * @method     ChildExcept_logQuery orderByReferrer($order = Criteria::ASC) Order by the referrer column
 * @method     ChildExcept_logQuery orderBySerializedSession($order = Criteria::ASC) Order by the serialized_session column
 * @method     ChildExcept_logQuery orderBySerializedGet($order = Criteria::ASC) Order by the serialized_get column
 * @method     ChildExcept_logQuery orderBySerializedPost($order = Criteria::ASC) Order by the serialized_post column
 * @method     ChildExcept_logQuery orderByCookieString($order = Criteria::ASC) Order by the cookie_string column
 * @method     ChildExcept_logQuery orderByUserAgent($order = Criteria::ASC) Order by the user_agent column
 * @method     ChildExcept_logQuery orderByIp($order = Criteria::ASC) Order by the ip column
 * @method     ChildExcept_logQuery orderByFatal($order = Criteria::ASC) Order by the fatal column
 * @method     ChildExcept_logQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildExcept_logQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildExcept_logQuery groupById() Group by the id column
 * @method     ChildExcept_logQuery groupByExceptionType() Group by the exception_type column
 * @method     ChildExcept_logQuery groupByFingerprint() Group by the fingerprint column
 * @method     ChildExcept_logQuery groupByMessage() Group by the message column
 * @method     ChildExcept_logQuery groupByFile() Group by the file column
 * @method     ChildExcept_logQuery groupByLine() Group by the line column
 * @method     ChildExcept_logQuery groupByBacktrace() Group by the backtrace column
 * @method     ChildExcept_logQuery groupByFirstOccurence() Group by the first_occurence column
 * @method     ChildExcept_logQuery groupByLastOccurence() Group by the last_occurence column
 * @method     ChildExcept_logQuery groupByOccurenceCount() Group by the occurence_count column
 * @method     ChildExcept_logQuery groupByDomain() Group by the domain column
 * @method     ChildExcept_logQuery groupByUrl() Group by the url column
 * @method     ChildExcept_logQuery groupByReferrer() Group by the referrer column
 * @method     ChildExcept_logQuery groupBySerializedSession() Group by the serialized_session column
 * @method     ChildExcept_logQuery groupBySerializedGet() Group by the serialized_get column
 * @method     ChildExcept_logQuery groupBySerializedPost() Group by the serialized_post column
 * @method     ChildExcept_logQuery groupByCookieString() Group by the cookie_string column
 * @method     ChildExcept_logQuery groupByUserAgent() Group by the user_agent column
 * @method     ChildExcept_logQuery groupByIp() Group by the ip column
 * @method     ChildExcept_logQuery groupByFatal() Group by the fatal column
 * @method     ChildExcept_logQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildExcept_logQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildExcept_logQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildExcept_logQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildExcept_logQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildExcept_logQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildExcept_logQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildExcept_logQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildExcept_log findOne(ConnectionInterface $con = null) Return the first ChildExcept_log matching the query
 * @method     ChildExcept_log findOneOrCreate(ConnectionInterface $con = null) Return the first ChildExcept_log matching the query, or a new ChildExcept_log object populated from the query conditions when no match is found
 *
 * @method     ChildExcept_log findOneById(int $id) Return the first ChildExcept_log filtered by the id column
 * @method     ChildExcept_log findOneByExceptionType(string $exception_type) Return the first ChildExcept_log filtered by the exception_type column
 * @method     ChildExcept_log findOneByFingerprint(string $fingerprint) Return the first ChildExcept_log filtered by the fingerprint column
 * @method     ChildExcept_log findOneByMessage(string $message) Return the first ChildExcept_log filtered by the message column
 * @method     ChildExcept_log findOneByFile(string $file) Return the first ChildExcept_log filtered by the file column
 * @method     ChildExcept_log findOneByLine(int $line) Return the first ChildExcept_log filtered by the line column
 * @method     ChildExcept_log findOneByBacktrace(string $backtrace) Return the first ChildExcept_log filtered by the backtrace column
 * @method     ChildExcept_log findOneByFirstOccurence(string $first_occurence) Return the first ChildExcept_log filtered by the first_occurence column
 * @method     ChildExcept_log findOneByLastOccurence(string $last_occurence) Return the first ChildExcept_log filtered by the last_occurence column
 * @method     ChildExcept_log findOneByOccurenceCount(int $occurence_count) Return the first ChildExcept_log filtered by the occurence_count column
 * @method     ChildExcept_log findOneByDomain(string $domain) Return the first ChildExcept_log filtered by the domain column
 * @method     ChildExcept_log findOneByUrl(string $url) Return the first ChildExcept_log filtered by the url column
 * @method     ChildExcept_log findOneByReferrer(string $referrer) Return the first ChildExcept_log filtered by the referrer column
 * @method     ChildExcept_log findOneBySerializedSession(string $serialized_session) Return the first ChildExcept_log filtered by the serialized_session column
 * @method     ChildExcept_log findOneBySerializedGet(string $serialized_get) Return the first ChildExcept_log filtered by the serialized_get column
 * @method     ChildExcept_log findOneBySerializedPost(string $serialized_post) Return the first ChildExcept_log filtered by the serialized_post column
 * @method     ChildExcept_log findOneByCookieString(string $cookie_string) Return the first ChildExcept_log filtered by the cookie_string column
 * @method     ChildExcept_log findOneByUserAgent(string $user_agent) Return the first ChildExcept_log filtered by the user_agent column
 * @method     ChildExcept_log findOneByIp(string $ip) Return the first ChildExcept_log filtered by the ip column
 * @method     ChildExcept_log findOneByFatal(boolean $fatal) Return the first ChildExcept_log filtered by the fatal column
 * @method     ChildExcept_log findOneByCreatedAt(string $created_at) Return the first ChildExcept_log filtered by the created_at column
 * @method     ChildExcept_log findOneByUpdatedAt(string $updated_at) Return the first ChildExcept_log filtered by the updated_at column *

 * @method     ChildExcept_log requirePk($key, ConnectionInterface $con = null) Return the ChildExcept_log by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOne(ConnectionInterface $con = null) Return the first ChildExcept_log matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildExcept_log requireOneById(int $id) Return the first ChildExcept_log filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByExceptionType(string $exception_type) Return the first ChildExcept_log filtered by the exception_type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByFingerprint(string $fingerprint) Return the first ChildExcept_log filtered by the fingerprint column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByMessage(string $message) Return the first ChildExcept_log filtered by the message column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByFile(string $file) Return the first ChildExcept_log filtered by the file column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByLine(int $line) Return the first ChildExcept_log filtered by the line column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByBacktrace(string $backtrace) Return the first ChildExcept_log filtered by the backtrace column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByFirstOccurence(string $first_occurence) Return the first ChildExcept_log filtered by the first_occurence column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByLastOccurence(string $last_occurence) Return the first ChildExcept_log filtered by the last_occurence column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByOccurenceCount(int $occurence_count) Return the first ChildExcept_log filtered by the occurence_count column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByDomain(string $domain) Return the first ChildExcept_log filtered by the domain column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByUrl(string $url) Return the first ChildExcept_log filtered by the url column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByReferrer(string $referrer) Return the first ChildExcept_log filtered by the referrer column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneBySerializedSession(string $serialized_session) Return the first ChildExcept_log filtered by the serialized_session column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneBySerializedGet(string $serialized_get) Return the first ChildExcept_log filtered by the serialized_get column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneBySerializedPost(string $serialized_post) Return the first ChildExcept_log filtered by the serialized_post column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByCookieString(string $cookie_string) Return the first ChildExcept_log filtered by the cookie_string column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByUserAgent(string $user_agent) Return the first ChildExcept_log filtered by the user_agent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByIp(string $ip) Return the first ChildExcept_log filtered by the ip column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByFatal(boolean $fatal) Return the first ChildExcept_log filtered by the fatal column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByCreatedAt(string $created_at) Return the first ChildExcept_log filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildExcept_log requireOneByUpdatedAt(string $updated_at) Return the first ChildExcept_log filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildExcept_log[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildExcept_log objects based on current ModelCriteria
 * @method     ChildExcept_log[]|ObjectCollection findById(int $id) Return ChildExcept_log objects filtered by the id column
 * @method     ChildExcept_log[]|ObjectCollection findByExceptionType(string $exception_type) Return ChildExcept_log objects filtered by the exception_type column
 * @method     ChildExcept_log[]|ObjectCollection findByFingerprint(string $fingerprint) Return ChildExcept_log objects filtered by the fingerprint column
 * @method     ChildExcept_log[]|ObjectCollection findByMessage(string $message) Return ChildExcept_log objects filtered by the message column
 * @method     ChildExcept_log[]|ObjectCollection findByFile(string $file) Return ChildExcept_log objects filtered by the file column
 * @method     ChildExcept_log[]|ObjectCollection findByLine(int $line) Return ChildExcept_log objects filtered by the line column
 * @method     ChildExcept_log[]|ObjectCollection findByBacktrace(string $backtrace) Return ChildExcept_log objects filtered by the backtrace column
 * @method     ChildExcept_log[]|ObjectCollection findByFirstOccurence(string $first_occurence) Return ChildExcept_log objects filtered by the first_occurence column
 * @method     ChildExcept_log[]|ObjectCollection findByLastOccurence(string $last_occurence) Return ChildExcept_log objects filtered by the last_occurence column
 * @method     ChildExcept_log[]|ObjectCollection findByOccurenceCount(int $occurence_count) Return ChildExcept_log objects filtered by the occurence_count column
 * @method     ChildExcept_log[]|ObjectCollection findByDomain(string $domain) Return ChildExcept_log objects filtered by the domain column
 * @method     ChildExcept_log[]|ObjectCollection findByUrl(string $url) Return ChildExcept_log objects filtered by the url column
 * @method     ChildExcept_log[]|ObjectCollection findByReferrer(string $referrer) Return ChildExcept_log objects filtered by the referrer column
 * @method     ChildExcept_log[]|ObjectCollection findBySerializedSession(string $serialized_session) Return ChildExcept_log objects filtered by the serialized_session column
 * @method     ChildExcept_log[]|ObjectCollection findBySerializedGet(string $serialized_get) Return ChildExcept_log objects filtered by the serialized_get column
 * @method     ChildExcept_log[]|ObjectCollection findBySerializedPost(string $serialized_post) Return ChildExcept_log objects filtered by the serialized_post column
 * @method     ChildExcept_log[]|ObjectCollection findByCookieString(string $cookie_string) Return ChildExcept_log objects filtered by the cookie_string column
 * @method     ChildExcept_log[]|ObjectCollection findByUserAgent(string $user_agent) Return ChildExcept_log objects filtered by the user_agent column
 * @method     ChildExcept_log[]|ObjectCollection findByIp(string $ip) Return ChildExcept_log objects filtered by the ip column
 * @method     ChildExcept_log[]|ObjectCollection findByFatal(boolean $fatal) Return ChildExcept_log objects filtered by the fatal column
 * @method     ChildExcept_log[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildExcept_log objects filtered by the created_at column
 * @method     ChildExcept_log[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildExcept_log objects filtered by the updated_at column
 * @method     ChildExcept_log[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class Except_logQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\Logging\Base\Except_logQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\Logging\\Except_log', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildExcept_logQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildExcept_logQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildExcept_logQuery) {
            return $criteria;
        }
        $query = new ChildExcept_logQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildExcept_log|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(Except_logTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = Except_logTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildExcept_log A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, exception_type, fingerprint, message, file, line, backtrace, first_occurence, last_occurence, occurence_count, domain, url, referrer, serialized_session, serialized_get, serialized_post, cookie_string, user_agent, ip, fatal, created_at, updated_at FROM except_log WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildExcept_log $obj */
            $obj = new ChildExcept_log();
            $obj->hydrate($row);
            Except_logTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildExcept_log|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(Except_logTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(Except_logTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(Except_logTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(Except_logTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the exception_type column
     *
     * Example usage:
     * <code>
     * $query->filterByExceptionType('fooValue');   // WHERE exception_type = 'fooValue'
     * $query->filterByExceptionType('%fooValue%', Criteria::LIKE); // WHERE exception_type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $exceptionType The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByExceptionType($exceptionType = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($exceptionType)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_EXCEPTION_TYPE, $exceptionType, $comparison);
    }

    /**
     * Filter the query on the fingerprint column
     *
     * Example usage:
     * <code>
     * $query->filterByFingerprint('fooValue');   // WHERE fingerprint = 'fooValue'
     * $query->filterByFingerprint('%fooValue%', Criteria::LIKE); // WHERE fingerprint LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fingerprint The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByFingerprint($fingerprint = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fingerprint)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_FINGERPRINT, $fingerprint, $comparison);
    }

    /**
     * Filter the query on the message column
     *
     * Example usage:
     * <code>
     * $query->filterByMessage('fooValue');   // WHERE message = 'fooValue'
     * $query->filterByMessage('%fooValue%', Criteria::LIKE); // WHERE message LIKE '%fooValue%'
     * </code>
     *
     * @param     string $message The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByMessage($message = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($message)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_MESSAGE, $message, $comparison);
    }

    /**
     * Filter the query on the file column
     *
     * Example usage:
     * <code>
     * $query->filterByFile('fooValue');   // WHERE file = 'fooValue'
     * $query->filterByFile('%fooValue%', Criteria::LIKE); // WHERE file LIKE '%fooValue%'
     * </code>
     *
     * @param     string $file The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByFile($file = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($file)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_FILE, $file, $comparison);
    }

    /**
     * Filter the query on the line column
     *
     * Example usage:
     * <code>
     * $query->filterByLine(1234); // WHERE line = 1234
     * $query->filterByLine(array(12, 34)); // WHERE line IN (12, 34)
     * $query->filterByLine(array('min' => 12)); // WHERE line > 12
     * </code>
     *
     * @param     mixed $line The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByLine($line = null, $comparison = null)
    {
        if (is_array($line)) {
            $useMinMax = false;
            if (isset($line['min'])) {
                $this->addUsingAlias(Except_logTableMap::COL_LINE, $line['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($line['max'])) {
                $this->addUsingAlias(Except_logTableMap::COL_LINE, $line['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_LINE, $line, $comparison);
    }

    /**
     * Filter the query on the backtrace column
     *
     * Example usage:
     * <code>
     * $query->filterByBacktrace('fooValue');   // WHERE backtrace = 'fooValue'
     * $query->filterByBacktrace('%fooValue%', Criteria::LIKE); // WHERE backtrace LIKE '%fooValue%'
     * </code>
     *
     * @param     string $backtrace The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByBacktrace($backtrace = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($backtrace)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_BACKTRACE, $backtrace, $comparison);
    }

    /**
     * Filter the query on the first_occurence column
     *
     * Example usage:
     * <code>
     * $query->filterByFirstOccurence('2011-03-14'); // WHERE first_occurence = '2011-03-14'
     * $query->filterByFirstOccurence('now'); // WHERE first_occurence = '2011-03-14'
     * $query->filterByFirstOccurence(array('max' => 'yesterday')); // WHERE first_occurence > '2011-03-13'
     * </code>
     *
     * @param     mixed $firstOccurence The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByFirstOccurence($firstOccurence = null, $comparison = null)
    {
        if (is_array($firstOccurence)) {
            $useMinMax = false;
            if (isset($firstOccurence['min'])) {
                $this->addUsingAlias(Except_logTableMap::COL_FIRST_OCCURENCE, $firstOccurence['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($firstOccurence['max'])) {
                $this->addUsingAlias(Except_logTableMap::COL_FIRST_OCCURENCE, $firstOccurence['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_FIRST_OCCURENCE, $firstOccurence, $comparison);
    }

    /**
     * Filter the query on the last_occurence column
     *
     * Example usage:
     * <code>
     * $query->filterByLastOccurence('2011-03-14'); // WHERE last_occurence = '2011-03-14'
     * $query->filterByLastOccurence('now'); // WHERE last_occurence = '2011-03-14'
     * $query->filterByLastOccurence(array('max' => 'yesterday')); // WHERE last_occurence > '2011-03-13'
     * </code>
     *
     * @param     mixed $lastOccurence The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByLastOccurence($lastOccurence = null, $comparison = null)
    {
        if (is_array($lastOccurence)) {
            $useMinMax = false;
            if (isset($lastOccurence['min'])) {
                $this->addUsingAlias(Except_logTableMap::COL_LAST_OCCURENCE, $lastOccurence['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lastOccurence['max'])) {
                $this->addUsingAlias(Except_logTableMap::COL_LAST_OCCURENCE, $lastOccurence['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_LAST_OCCURENCE, $lastOccurence, $comparison);
    }

    /**
     * Filter the query on the occurence_count column
     *
     * Example usage:
     * <code>
     * $query->filterByOccurenceCount(1234); // WHERE occurence_count = 1234
     * $query->filterByOccurenceCount(array(12, 34)); // WHERE occurence_count IN (12, 34)
     * $query->filterByOccurenceCount(array('min' => 12)); // WHERE occurence_count > 12
     * </code>
     *
     * @param     mixed $occurenceCount The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByOccurenceCount($occurenceCount = null, $comparison = null)
    {
        if (is_array($occurenceCount)) {
            $useMinMax = false;
            if (isset($occurenceCount['min'])) {
                $this->addUsingAlias(Except_logTableMap::COL_OCCURENCE_COUNT, $occurenceCount['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($occurenceCount['max'])) {
                $this->addUsingAlias(Except_logTableMap::COL_OCCURENCE_COUNT, $occurenceCount['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_OCCURENCE_COUNT, $occurenceCount, $comparison);
    }

    /**
     * Filter the query on the domain column
     *
     * Example usage:
     * <code>
     * $query->filterByDomain('fooValue');   // WHERE domain = 'fooValue'
     * $query->filterByDomain('%fooValue%', Criteria::LIKE); // WHERE domain LIKE '%fooValue%'
     * </code>
     *
     * @param     string $domain The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByDomain($domain = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($domain)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_DOMAIN, $domain, $comparison);
    }

    /**
     * Filter the query on the url column
     *
     * Example usage:
     * <code>
     * $query->filterByUrl('fooValue');   // WHERE url = 'fooValue'
     * $query->filterByUrl('%fooValue%', Criteria::LIKE); // WHERE url LIKE '%fooValue%'
     * </code>
     *
     * @param     string $url The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByUrl($url = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($url)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_URL, $url, $comparison);
    }

    /**
     * Filter the query on the referrer column
     *
     * Example usage:
     * <code>
     * $query->filterByReferrer('fooValue');   // WHERE referrer = 'fooValue'
     * $query->filterByReferrer('%fooValue%', Criteria::LIKE); // WHERE referrer LIKE '%fooValue%'
     * </code>
     *
     * @param     string $referrer The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByReferrer($referrer = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($referrer)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_REFERRER, $referrer, $comparison);
    }

    /**
     * Filter the query on the serialized_session column
     *
     * Example usage:
     * <code>
     * $query->filterBySerializedSession('fooValue');   // WHERE serialized_session = 'fooValue'
     * $query->filterBySerializedSession('%fooValue%', Criteria::LIKE); // WHERE serialized_session LIKE '%fooValue%'
     * </code>
     *
     * @param     string $serializedSession The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterBySerializedSession($serializedSession = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($serializedSession)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_SERIALIZED_SESSION, $serializedSession, $comparison);
    }

    /**
     * Filter the query on the serialized_get column
     *
     * Example usage:
     * <code>
     * $query->filterBySerializedGet('fooValue');   // WHERE serialized_get = 'fooValue'
     * $query->filterBySerializedGet('%fooValue%', Criteria::LIKE); // WHERE serialized_get LIKE '%fooValue%'
     * </code>
     *
     * @param     string $serializedGet The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterBySerializedGet($serializedGet = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($serializedGet)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_SERIALIZED_GET, $serializedGet, $comparison);
    }

    /**
     * Filter the query on the serialized_post column
     *
     * Example usage:
     * <code>
     * $query->filterBySerializedPost('fooValue');   // WHERE serialized_post = 'fooValue'
     * $query->filterBySerializedPost('%fooValue%', Criteria::LIKE); // WHERE serialized_post LIKE '%fooValue%'
     * </code>
     *
     * @param     string $serializedPost The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterBySerializedPost($serializedPost = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($serializedPost)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_SERIALIZED_POST, $serializedPost, $comparison);
    }

    /**
     * Filter the query on the cookie_string column
     *
     * Example usage:
     * <code>
     * $query->filterByCookieString('fooValue');   // WHERE cookie_string = 'fooValue'
     * $query->filterByCookieString('%fooValue%', Criteria::LIKE); // WHERE cookie_string LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cookieString The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByCookieString($cookieString = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cookieString)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_COOKIE_STRING, $cookieString, $comparison);
    }

    /**
     * Filter the query on the user_agent column
     *
     * Example usage:
     * <code>
     * $query->filterByUserAgent('fooValue');   // WHERE user_agent = 'fooValue'
     * $query->filterByUserAgent('%fooValue%', Criteria::LIKE); // WHERE user_agent LIKE '%fooValue%'
     * </code>
     *
     * @param     string $userAgent The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByUserAgent($userAgent = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($userAgent)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_USER_AGENT, $userAgent, $comparison);
    }

    /**
     * Filter the query on the ip column
     *
     * Example usage:
     * <code>
     * $query->filterByIp('fooValue');   // WHERE ip = 'fooValue'
     * $query->filterByIp('%fooValue%', Criteria::LIKE); // WHERE ip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $ip The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByIp($ip = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($ip)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_IP, $ip, $comparison);
    }

    /**
     * Filter the query on the fatal column
     *
     * Example usage:
     * <code>
     * $query->filterByFatal(true); // WHERE fatal = true
     * $query->filterByFatal('yes'); // WHERE fatal = true
     * </code>
     *
     * @param     boolean|string $fatal The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByFatal($fatal = null, $comparison = null)
    {
        if (is_string($fatal)) {
            $fatal = in_array(strtolower($fatal), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(Except_logTableMap::COL_FATAL, $fatal, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(Except_logTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(Except_logTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(Except_logTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(Except_logTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(Except_logTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ChildExcept_log $except_log Object to remove from the list of results
     *
     * @return $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function prune($except_log = null)
    {
        if ($except_log) {
            $this->addUsingAlias(Except_logTableMap::COL_ID, $except_log->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the except_log table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Except_logTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            Except_logTableMap::clearInstancePool();
            Except_logTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Except_logTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(Except_logTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            Except_logTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            Except_logTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(Except_logTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(Except_logTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(Except_logTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(Except_logTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(Except_logTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildExcept_logQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(Except_logTableMap::COL_CREATED_AT);
    }

} // Except_logQuery
