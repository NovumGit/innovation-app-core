<?php

namespace Model\Logging\Map;

use Model\Logging\Except_log;
use Model\Logging\Except_logQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'except_log' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 */
class Except_logTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'Model.Logging.Map.Except_logTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'hurah';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'except_log';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\Model\\Logging\\Except_log';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'Model.Logging.Except_log';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 22;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 22;

    /**
     * the column name for the id field
     */
    const COL_ID = 'except_log.id';

    /**
     * the column name for the exception_type field
     */
    const COL_EXCEPTION_TYPE = 'except_log.exception_type';

    /**
     * the column name for the fingerprint field
     */
    const COL_FINGERPRINT = 'except_log.fingerprint';

    /**
     * the column name for the message field
     */
    const COL_MESSAGE = 'except_log.message';

    /**
     * the column name for the file field
     */
    const COL_FILE = 'except_log.file';

    /**
     * the column name for the line field
     */
    const COL_LINE = 'except_log.line';

    /**
     * the column name for the backtrace field
     */
    const COL_BACKTRACE = 'except_log.backtrace';

    /**
     * the column name for the first_occurence field
     */
    const COL_FIRST_OCCURENCE = 'except_log.first_occurence';

    /**
     * the column name for the last_occurence field
     */
    const COL_LAST_OCCURENCE = 'except_log.last_occurence';

    /**
     * the column name for the occurence_count field
     */
    const COL_OCCURENCE_COUNT = 'except_log.occurence_count';

    /**
     * the column name for the domain field
     */
    const COL_DOMAIN = 'except_log.domain';

    /**
     * the column name for the url field
     */
    const COL_URL = 'except_log.url';

    /**
     * the column name for the referrer field
     */
    const COL_REFERRER = 'except_log.referrer';

    /**
     * the column name for the serialized_session field
     */
    const COL_SERIALIZED_SESSION = 'except_log.serialized_session';

    /**
     * the column name for the serialized_get field
     */
    const COL_SERIALIZED_GET = 'except_log.serialized_get';

    /**
     * the column name for the serialized_post field
     */
    const COL_SERIALIZED_POST = 'except_log.serialized_post';

    /**
     * the column name for the cookie_string field
     */
    const COL_COOKIE_STRING = 'except_log.cookie_string';

    /**
     * the column name for the user_agent field
     */
    const COL_USER_AGENT = 'except_log.user_agent';

    /**
     * the column name for the ip field
     */
    const COL_IP = 'except_log.ip';

    /**
     * the column name for the fatal field
     */
    const COL_FATAL = 'except_log.fatal';

    /**
     * the column name for the created_at field
     */
    const COL_CREATED_AT = 'except_log.created_at';

    /**
     * the column name for the updated_at field
     */
    const COL_UPDATED_AT = 'except_log.updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ExceptionType', 'Fingerprint', 'Message', 'File', 'Line', 'Backtrace', 'FirstOccurence', 'LastOccurence', 'OccurenceCount', 'Domain', 'Url', 'Referrer', 'SerializedSession', 'SerializedGet', 'SerializedPost', 'CookieString', 'UserAgent', 'Ip', 'Fatal', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'exceptionType', 'fingerprint', 'message', 'file', 'line', 'backtrace', 'firstOccurence', 'lastOccurence', 'occurenceCount', 'domain', 'url', 'referrer', 'serializedSession', 'serializedGet', 'serializedPost', 'cookieString', 'userAgent', 'ip', 'fatal', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(Except_logTableMap::COL_ID, Except_logTableMap::COL_EXCEPTION_TYPE, Except_logTableMap::COL_FINGERPRINT, Except_logTableMap::COL_MESSAGE, Except_logTableMap::COL_FILE, Except_logTableMap::COL_LINE, Except_logTableMap::COL_BACKTRACE, Except_logTableMap::COL_FIRST_OCCURENCE, Except_logTableMap::COL_LAST_OCCURENCE, Except_logTableMap::COL_OCCURENCE_COUNT, Except_logTableMap::COL_DOMAIN, Except_logTableMap::COL_URL, Except_logTableMap::COL_REFERRER, Except_logTableMap::COL_SERIALIZED_SESSION, Except_logTableMap::COL_SERIALIZED_GET, Except_logTableMap::COL_SERIALIZED_POST, Except_logTableMap::COL_COOKIE_STRING, Except_logTableMap::COL_USER_AGENT, Except_logTableMap::COL_IP, Except_logTableMap::COL_FATAL, Except_logTableMap::COL_CREATED_AT, Except_logTableMap::COL_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('id', 'exception_type', 'fingerprint', 'message', 'file', 'line', 'backtrace', 'first_occurence', 'last_occurence', 'occurence_count', 'domain', 'url', 'referrer', 'serialized_session', 'serialized_get', 'serialized_post', 'cookie_string', 'user_agent', 'ip', 'fatal', 'created_at', 'updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ExceptionType' => 1, 'Fingerprint' => 2, 'Message' => 3, 'File' => 4, 'Line' => 5, 'Backtrace' => 6, 'FirstOccurence' => 7, 'LastOccurence' => 8, 'OccurenceCount' => 9, 'Domain' => 10, 'Url' => 11, 'Referrer' => 12, 'SerializedSession' => 13, 'SerializedGet' => 14, 'SerializedPost' => 15, 'CookieString' => 16, 'UserAgent' => 17, 'Ip' => 18, 'Fatal' => 19, 'CreatedAt' => 20, 'UpdatedAt' => 21, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'exceptionType' => 1, 'fingerprint' => 2, 'message' => 3, 'file' => 4, 'line' => 5, 'backtrace' => 6, 'firstOccurence' => 7, 'lastOccurence' => 8, 'occurenceCount' => 9, 'domain' => 10, 'url' => 11, 'referrer' => 12, 'serializedSession' => 13, 'serializedGet' => 14, 'serializedPost' => 15, 'cookieString' => 16, 'userAgent' => 17, 'ip' => 18, 'fatal' => 19, 'createdAt' => 20, 'updatedAt' => 21, ),
        self::TYPE_COLNAME       => array(Except_logTableMap::COL_ID => 0, Except_logTableMap::COL_EXCEPTION_TYPE => 1, Except_logTableMap::COL_FINGERPRINT => 2, Except_logTableMap::COL_MESSAGE => 3, Except_logTableMap::COL_FILE => 4, Except_logTableMap::COL_LINE => 5, Except_logTableMap::COL_BACKTRACE => 6, Except_logTableMap::COL_FIRST_OCCURENCE => 7, Except_logTableMap::COL_LAST_OCCURENCE => 8, Except_logTableMap::COL_OCCURENCE_COUNT => 9, Except_logTableMap::COL_DOMAIN => 10, Except_logTableMap::COL_URL => 11, Except_logTableMap::COL_REFERRER => 12, Except_logTableMap::COL_SERIALIZED_SESSION => 13, Except_logTableMap::COL_SERIALIZED_GET => 14, Except_logTableMap::COL_SERIALIZED_POST => 15, Except_logTableMap::COL_COOKIE_STRING => 16, Except_logTableMap::COL_USER_AGENT => 17, Except_logTableMap::COL_IP => 18, Except_logTableMap::COL_FATAL => 19, Except_logTableMap::COL_CREATED_AT => 20, Except_logTableMap::COL_UPDATED_AT => 21, ),
        self::TYPE_FIELDNAME     => array('id' => 0, 'exception_type' => 1, 'fingerprint' => 2, 'message' => 3, 'file' => 4, 'line' => 5, 'backtrace' => 6, 'first_occurence' => 7, 'last_occurence' => 8, 'occurence_count' => 9, 'domain' => 10, 'url' => 11, 'referrer' => 12, 'serialized_session' => 13, 'serialized_get' => 14, 'serialized_post' => 15, 'cookie_string' => 16, 'user_agent' => 17, 'ip' => 18, 'fatal' => 19, 'created_at' => 20, 'updated_at' => 21, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('except_log');
        $this->setPhpName('Except_log');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\Model\\Logging\\Except_log');
        $this->setPackage('Model.Logging');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('exception_type', 'ExceptionType', 'VARCHAR', false, 255, null);
        $this->addColumn('fingerprint', 'Fingerprint', 'VARCHAR', false, 255, null);
        $this->addColumn('message', 'Message', 'LONGVARCHAR', false, null, null);
        $this->addColumn('file', 'File', 'VARCHAR', false, 255, null);
        $this->addColumn('line', 'Line', 'INTEGER', false, null, null);
        $this->addColumn('backtrace', 'Backtrace', 'LONGVARCHAR', false, null, null);
        $this->addColumn('first_occurence', 'FirstOccurence', 'TIMESTAMP', false, null, null);
        $this->addColumn('last_occurence', 'LastOccurence', 'TIMESTAMP', false, null, null);
        $this->addColumn('occurence_count', 'OccurenceCount', 'INTEGER', false, null, null);
        $this->addColumn('domain', 'Domain', 'VARCHAR', false, 255, null);
        $this->addColumn('url', 'Url', 'LONGVARCHAR', false, null, null);
        $this->addColumn('referrer', 'Referrer', 'VARCHAR', false, 255, null);
        $this->addColumn('serialized_session', 'SerializedSession', 'LONGVARCHAR', false, null, null);
        $this->addColumn('serialized_get', 'SerializedGet', 'LONGVARCHAR', false, null, null);
        $this->addColumn('serialized_post', 'SerializedPost', 'LONGVARCHAR', false, null, null);
        $this->addColumn('cookie_string', 'CookieString', 'LONGVARCHAR', false, null, null);
        $this->addColumn('user_agent', 'UserAgent', 'VARCHAR', false, 255, null);
        $this->addColumn('ip', 'Ip', 'VARCHAR', false, 255, null);
        $this->addColumn('fatal', 'Fatal', 'BOOLEAN', false, 1, null);
        $this->addColumn('created_at', 'CreatedAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('updated_at', 'UpdatedAt', 'TIMESTAMP', false, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'created_at', 'update_column' => 'updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? Except_logTableMap::CLASS_DEFAULT : Except_logTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (Except_log object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = Except_logTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = Except_logTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + Except_logTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = Except_logTableMap::OM_CLASS;
            /** @var Except_log $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            Except_logTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = Except_logTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = Except_logTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var Except_log $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                Except_logTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(Except_logTableMap::COL_ID);
            $criteria->addSelectColumn(Except_logTableMap::COL_EXCEPTION_TYPE);
            $criteria->addSelectColumn(Except_logTableMap::COL_FINGERPRINT);
            $criteria->addSelectColumn(Except_logTableMap::COL_MESSAGE);
            $criteria->addSelectColumn(Except_logTableMap::COL_FILE);
            $criteria->addSelectColumn(Except_logTableMap::COL_LINE);
            $criteria->addSelectColumn(Except_logTableMap::COL_BACKTRACE);
            $criteria->addSelectColumn(Except_logTableMap::COL_FIRST_OCCURENCE);
            $criteria->addSelectColumn(Except_logTableMap::COL_LAST_OCCURENCE);
            $criteria->addSelectColumn(Except_logTableMap::COL_OCCURENCE_COUNT);
            $criteria->addSelectColumn(Except_logTableMap::COL_DOMAIN);
            $criteria->addSelectColumn(Except_logTableMap::COL_URL);
            $criteria->addSelectColumn(Except_logTableMap::COL_REFERRER);
            $criteria->addSelectColumn(Except_logTableMap::COL_SERIALIZED_SESSION);
            $criteria->addSelectColumn(Except_logTableMap::COL_SERIALIZED_GET);
            $criteria->addSelectColumn(Except_logTableMap::COL_SERIALIZED_POST);
            $criteria->addSelectColumn(Except_logTableMap::COL_COOKIE_STRING);
            $criteria->addSelectColumn(Except_logTableMap::COL_USER_AGENT);
            $criteria->addSelectColumn(Except_logTableMap::COL_IP);
            $criteria->addSelectColumn(Except_logTableMap::COL_FATAL);
            $criteria->addSelectColumn(Except_logTableMap::COL_CREATED_AT);
            $criteria->addSelectColumn(Except_logTableMap::COL_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.id');
            $criteria->addSelectColumn($alias . '.exception_type');
            $criteria->addSelectColumn($alias . '.fingerprint');
            $criteria->addSelectColumn($alias . '.message');
            $criteria->addSelectColumn($alias . '.file');
            $criteria->addSelectColumn($alias . '.line');
            $criteria->addSelectColumn($alias . '.backtrace');
            $criteria->addSelectColumn($alias . '.first_occurence');
            $criteria->addSelectColumn($alias . '.last_occurence');
            $criteria->addSelectColumn($alias . '.occurence_count');
            $criteria->addSelectColumn($alias . '.domain');
            $criteria->addSelectColumn($alias . '.url');
            $criteria->addSelectColumn($alias . '.referrer');
            $criteria->addSelectColumn($alias . '.serialized_session');
            $criteria->addSelectColumn($alias . '.serialized_get');
            $criteria->addSelectColumn($alias . '.serialized_post');
            $criteria->addSelectColumn($alias . '.cookie_string');
            $criteria->addSelectColumn($alias . '.user_agent');
            $criteria->addSelectColumn($alias . '.ip');
            $criteria->addSelectColumn($alias . '.fatal');
            $criteria->addSelectColumn($alias . '.created_at');
            $criteria->addSelectColumn($alias . '.updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(Except_logTableMap::DATABASE_NAME)->getTable(Except_logTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(Except_logTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(Except_logTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new Except_logTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a Except_log or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or Except_log object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Except_logTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \Model\Logging\Except_log) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(Except_logTableMap::DATABASE_NAME);
            $criteria->add(Except_logTableMap::COL_ID, (array) $values, Criteria::IN);
        }

        $query = Except_logQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            Except_logTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                Except_logTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the except_log table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return Except_logQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a Except_log or Criteria object.
     *
     * @param mixed               $criteria Criteria or Except_log object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(Except_logTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from Except_log object
        }

        if ($criteria->containsKey(Except_logTableMap::COL_ID) && $criteria->keyContainsValue(Except_logTableMap::COL_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.Except_logTableMap::COL_ID.')');
        }


        // Set the correct dbName
        $query = Except_logQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // Except_logTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
Except_logTableMap::buildTableMap();
