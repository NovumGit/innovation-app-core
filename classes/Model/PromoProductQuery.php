<?php

namespace Model;

use Model\Base\PromoProductQuery as BasePromoProductQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'promo_product' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PromoProductQuery extends BasePromoProductQuery
{

}
