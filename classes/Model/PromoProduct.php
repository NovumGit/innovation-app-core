<?php

namespace Model;

use Model\Base\PromoProduct as BasePromoProduct;

/**
 * Skeleton subclass for representing a row from the 'promo_product' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class PromoProduct extends BasePromoProduct
{

}
