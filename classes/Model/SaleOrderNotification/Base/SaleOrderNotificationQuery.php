<?php

namespace Model\SaleOrderNotification\Base;

use \Exception;
use \PDO;
use Model\Sale\SaleOrder;
use Model\SaleOrderNotification\SaleOrderNotification as ChildSaleOrderNotification;
use Model\SaleOrderNotification\SaleOrderNotificationQuery as ChildSaleOrderNotificationQuery;
use Model\SaleOrderNotification\Map\SaleOrderNotificationTableMap;
use Model\Setting\MasterTable\Sale_order_notification_type;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'sale_order_notification' table.
 *
 *
 *
 * @method     ChildSaleOrderNotificationQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildSaleOrderNotificationQuery orderBySaleOrderId($order = Criteria::ASC) Order by the sale_order_id column
 * @method     ChildSaleOrderNotificationQuery orderBySaleOrderNotificationTypeId($order = Criteria::ASC) Order by the sale_order_notification_type_id column
 * @method     ChildSaleOrderNotificationQuery orderByFromEmail($order = Criteria::ASC) Order by the from_email column
 * @method     ChildSaleOrderNotificationQuery orderByToEmail($order = Criteria::ASC) Order by the to_email column
 * @method     ChildSaleOrderNotificationQuery orderByContent($order = Criteria::ASC) Order by the content column
 * @method     ChildSaleOrderNotificationQuery orderByContentCompressed($order = Criteria::ASC) Order by the content_compressed column
 * @method     ChildSaleOrderNotificationQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildSaleOrderNotificationQuery orderByDateSent($order = Criteria::ASC) Order by the date_sent column
 * @method     ChildSaleOrderNotificationQuery orderBySenderIp($order = Criteria::ASC) Order by the sender_ip column
 *
 * @method     ChildSaleOrderNotificationQuery groupById() Group by the id column
 * @method     ChildSaleOrderNotificationQuery groupBySaleOrderId() Group by the sale_order_id column
 * @method     ChildSaleOrderNotificationQuery groupBySaleOrderNotificationTypeId() Group by the sale_order_notification_type_id column
 * @method     ChildSaleOrderNotificationQuery groupByFromEmail() Group by the from_email column
 * @method     ChildSaleOrderNotificationQuery groupByToEmail() Group by the to_email column
 * @method     ChildSaleOrderNotificationQuery groupByContent() Group by the content column
 * @method     ChildSaleOrderNotificationQuery groupByContentCompressed() Group by the content_compressed column
 * @method     ChildSaleOrderNotificationQuery groupByTitle() Group by the title column
 * @method     ChildSaleOrderNotificationQuery groupByDateSent() Group by the date_sent column
 * @method     ChildSaleOrderNotificationQuery groupBySenderIp() Group by the sender_ip column
 *
 * @method     ChildSaleOrderNotificationQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildSaleOrderNotificationQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildSaleOrderNotificationQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildSaleOrderNotificationQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildSaleOrderNotificationQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildSaleOrderNotificationQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildSaleOrderNotificationQuery leftJoinSaleOrder($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrder relation
 * @method     ChildSaleOrderNotificationQuery rightJoinSaleOrder($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrder relation
 * @method     ChildSaleOrderNotificationQuery innerJoinSaleOrder($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrder relation
 *
 * @method     ChildSaleOrderNotificationQuery joinWithSaleOrder($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrder relation
 *
 * @method     ChildSaleOrderNotificationQuery leftJoinWithSaleOrder() Adds a LEFT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildSaleOrderNotificationQuery rightJoinWithSaleOrder() Adds a RIGHT JOIN clause and with to the query using the SaleOrder relation
 * @method     ChildSaleOrderNotificationQuery innerJoinWithSaleOrder() Adds a INNER JOIN clause and with to the query using the SaleOrder relation
 *
 * @method     ChildSaleOrderNotificationQuery leftJoinSaleOrderNotificationType($relationAlias = null) Adds a LEFT JOIN clause to the query using the SaleOrderNotificationType relation
 * @method     ChildSaleOrderNotificationQuery rightJoinSaleOrderNotificationType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the SaleOrderNotificationType relation
 * @method     ChildSaleOrderNotificationQuery innerJoinSaleOrderNotificationType($relationAlias = null) Adds a INNER JOIN clause to the query using the SaleOrderNotificationType relation
 *
 * @method     ChildSaleOrderNotificationQuery joinWithSaleOrderNotificationType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the SaleOrderNotificationType relation
 *
 * @method     ChildSaleOrderNotificationQuery leftJoinWithSaleOrderNotificationType() Adds a LEFT JOIN clause and with to the query using the SaleOrderNotificationType relation
 * @method     ChildSaleOrderNotificationQuery rightJoinWithSaleOrderNotificationType() Adds a RIGHT JOIN clause and with to the query using the SaleOrderNotificationType relation
 * @method     ChildSaleOrderNotificationQuery innerJoinWithSaleOrderNotificationType() Adds a INNER JOIN clause and with to the query using the SaleOrderNotificationType relation
 *
 * @method     \Model\Sale\SaleOrderQuery|\Model\Setting\MasterTable\Sale_order_notification_typeQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildSaleOrderNotification findOne(ConnectionInterface $con = null) Return the first ChildSaleOrderNotification matching the query
 * @method     ChildSaleOrderNotification findOneOrCreate(ConnectionInterface $con = null) Return the first ChildSaleOrderNotification matching the query, or a new ChildSaleOrderNotification object populated from the query conditions when no match is found
 *
 * @method     ChildSaleOrderNotification findOneById(int $id) Return the first ChildSaleOrderNotification filtered by the id column
 * @method     ChildSaleOrderNotification findOneBySaleOrderId(int $sale_order_id) Return the first ChildSaleOrderNotification filtered by the sale_order_id column
 * @method     ChildSaleOrderNotification findOneBySaleOrderNotificationTypeId(int $sale_order_notification_type_id) Return the first ChildSaleOrderNotification filtered by the sale_order_notification_type_id column
 * @method     ChildSaleOrderNotification findOneByFromEmail(string $from_email) Return the first ChildSaleOrderNotification filtered by the from_email column
 * @method     ChildSaleOrderNotification findOneByToEmail(string $to_email) Return the first ChildSaleOrderNotification filtered by the to_email column
 * @method     ChildSaleOrderNotification findOneByContent(string $content) Return the first ChildSaleOrderNotification filtered by the content column
 * @method     ChildSaleOrderNotification findOneByContentCompressed(boolean $content_compressed) Return the first ChildSaleOrderNotification filtered by the content_compressed column
 * @method     ChildSaleOrderNotification findOneByTitle(string $title) Return the first ChildSaleOrderNotification filtered by the title column
 * @method     ChildSaleOrderNotification findOneByDateSent(string $date_sent) Return the first ChildSaleOrderNotification filtered by the date_sent column
 * @method     ChildSaleOrderNotification findOneBySenderIp(string $sender_ip) Return the first ChildSaleOrderNotification filtered by the sender_ip column *

 * @method     ChildSaleOrderNotification requirePk($key, ConnectionInterface $con = null) Return the ChildSaleOrderNotification by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderNotification requireOne(ConnectionInterface $con = null) Return the first ChildSaleOrderNotification matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleOrderNotification requireOneById(int $id) Return the first ChildSaleOrderNotification filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderNotification requireOneBySaleOrderId(int $sale_order_id) Return the first ChildSaleOrderNotification filtered by the sale_order_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderNotification requireOneBySaleOrderNotificationTypeId(int $sale_order_notification_type_id) Return the first ChildSaleOrderNotification filtered by the sale_order_notification_type_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderNotification requireOneByFromEmail(string $from_email) Return the first ChildSaleOrderNotification filtered by the from_email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderNotification requireOneByToEmail(string $to_email) Return the first ChildSaleOrderNotification filtered by the to_email column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderNotification requireOneByContent(string $content) Return the first ChildSaleOrderNotification filtered by the content column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderNotification requireOneByContentCompressed(boolean $content_compressed) Return the first ChildSaleOrderNotification filtered by the content_compressed column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderNotification requireOneByTitle(string $title) Return the first ChildSaleOrderNotification filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderNotification requireOneByDateSent(string $date_sent) Return the first ChildSaleOrderNotification filtered by the date_sent column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildSaleOrderNotification requireOneBySenderIp(string $sender_ip) Return the first ChildSaleOrderNotification filtered by the sender_ip column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildSaleOrderNotification[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildSaleOrderNotification objects based on current ModelCriteria
 * @method     ChildSaleOrderNotification[]|ObjectCollection findById(int $id) Return ChildSaleOrderNotification objects filtered by the id column
 * @method     ChildSaleOrderNotification[]|ObjectCollection findBySaleOrderId(int $sale_order_id) Return ChildSaleOrderNotification objects filtered by the sale_order_id column
 * @method     ChildSaleOrderNotification[]|ObjectCollection findBySaleOrderNotificationTypeId(int $sale_order_notification_type_id) Return ChildSaleOrderNotification objects filtered by the sale_order_notification_type_id column
 * @method     ChildSaleOrderNotification[]|ObjectCollection findByFromEmail(string $from_email) Return ChildSaleOrderNotification objects filtered by the from_email column
 * @method     ChildSaleOrderNotification[]|ObjectCollection findByToEmail(string $to_email) Return ChildSaleOrderNotification objects filtered by the to_email column
 * @method     ChildSaleOrderNotification[]|ObjectCollection findByContent(string $content) Return ChildSaleOrderNotification objects filtered by the content column
 * @method     ChildSaleOrderNotification[]|ObjectCollection findByContentCompressed(boolean $content_compressed) Return ChildSaleOrderNotification objects filtered by the content_compressed column
 * @method     ChildSaleOrderNotification[]|ObjectCollection findByTitle(string $title) Return ChildSaleOrderNotification objects filtered by the title column
 * @method     ChildSaleOrderNotification[]|ObjectCollection findByDateSent(string $date_sent) Return ChildSaleOrderNotification objects filtered by the date_sent column
 * @method     ChildSaleOrderNotification[]|ObjectCollection findBySenderIp(string $sender_ip) Return ChildSaleOrderNotification objects filtered by the sender_ip column
 * @method     ChildSaleOrderNotification[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class SaleOrderNotificationQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \Model\SaleOrderNotification\Base\SaleOrderNotificationQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'hurah', $modelName = '\\Model\\SaleOrderNotification\\SaleOrderNotification', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildSaleOrderNotificationQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildSaleOrderNotificationQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildSaleOrderNotificationQuery) {
            return $criteria;
        }
        $query = new ChildSaleOrderNotificationQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildSaleOrderNotification|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(SaleOrderNotificationTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = SaleOrderNotificationTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderNotification A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, sale_order_id, sale_order_notification_type_id, from_email, to_email, content, content_compressed, title, date_sent, sender_ip FROM sale_order_notification WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildSaleOrderNotification $obj */
            $obj = new ChildSaleOrderNotification();
            $obj->hydrate($row);
            SaleOrderNotificationTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildSaleOrderNotification|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SaleOrderNotificationTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SaleOrderNotificationTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(SaleOrderNotificationTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(SaleOrderNotificationTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderNotificationTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the sale_order_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySaleOrderId(1234); // WHERE sale_order_id = 1234
     * $query->filterBySaleOrderId(array(12, 34)); // WHERE sale_order_id IN (12, 34)
     * $query->filterBySaleOrderId(array('min' => 12)); // WHERE sale_order_id > 12
     * </code>
     *
     * @see       filterBySaleOrder()
     *
     * @param     mixed $saleOrderId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterBySaleOrderId($saleOrderId = null, $comparison = null)
    {
        if (is_array($saleOrderId)) {
            $useMinMax = false;
            if (isset($saleOrderId['min'])) {
                $this->addUsingAlias(SaleOrderNotificationTableMap::COL_SALE_ORDER_ID, $saleOrderId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($saleOrderId['max'])) {
                $this->addUsingAlias(SaleOrderNotificationTableMap::COL_SALE_ORDER_ID, $saleOrderId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderNotificationTableMap::COL_SALE_ORDER_ID, $saleOrderId, $comparison);
    }

    /**
     * Filter the query on the sale_order_notification_type_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySaleOrderNotificationTypeId(1234); // WHERE sale_order_notification_type_id = 1234
     * $query->filterBySaleOrderNotificationTypeId(array(12, 34)); // WHERE sale_order_notification_type_id IN (12, 34)
     * $query->filterBySaleOrderNotificationTypeId(array('min' => 12)); // WHERE sale_order_notification_type_id > 12
     * </code>
     *
     * @see       filterBySaleOrderNotificationType()
     *
     * @param     mixed $saleOrderNotificationTypeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterBySaleOrderNotificationTypeId($saleOrderNotificationTypeId = null, $comparison = null)
    {
        if (is_array($saleOrderNotificationTypeId)) {
            $useMinMax = false;
            if (isset($saleOrderNotificationTypeId['min'])) {
                $this->addUsingAlias(SaleOrderNotificationTableMap::COL_SALE_ORDER_NOTIFICATION_TYPE_ID, $saleOrderNotificationTypeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($saleOrderNotificationTypeId['max'])) {
                $this->addUsingAlias(SaleOrderNotificationTableMap::COL_SALE_ORDER_NOTIFICATION_TYPE_ID, $saleOrderNotificationTypeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderNotificationTableMap::COL_SALE_ORDER_NOTIFICATION_TYPE_ID, $saleOrderNotificationTypeId, $comparison);
    }

    /**
     * Filter the query on the from_email column
     *
     * Example usage:
     * <code>
     * $query->filterByFromEmail('fooValue');   // WHERE from_email = 'fooValue'
     * $query->filterByFromEmail('%fooValue%', Criteria::LIKE); // WHERE from_email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $fromEmail The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterByFromEmail($fromEmail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($fromEmail)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderNotificationTableMap::COL_FROM_EMAIL, $fromEmail, $comparison);
    }

    /**
     * Filter the query on the to_email column
     *
     * Example usage:
     * <code>
     * $query->filterByToEmail('fooValue');   // WHERE to_email = 'fooValue'
     * $query->filterByToEmail('%fooValue%', Criteria::LIKE); // WHERE to_email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $toEmail The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterByToEmail($toEmail = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($toEmail)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderNotificationTableMap::COL_TO_EMAIL, $toEmail, $comparison);
    }

    /**
     * Filter the query on the content column
     *
     * Example usage:
     * <code>
     * $query->filterByContent('fooValue');   // WHERE content = 'fooValue'
     * $query->filterByContent('%fooValue%', Criteria::LIKE); // WHERE content LIKE '%fooValue%'
     * </code>
     *
     * @param     string $content The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterByContent($content = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($content)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderNotificationTableMap::COL_CONTENT, $content, $comparison);
    }

    /**
     * Filter the query on the content_compressed column
     *
     * Example usage:
     * <code>
     * $query->filterByContentCompressed(true); // WHERE content_compressed = true
     * $query->filterByContentCompressed('yes'); // WHERE content_compressed = true
     * </code>
     *
     * @param     boolean|string $contentCompressed The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterByContentCompressed($contentCompressed = null, $comparison = null)
    {
        if (is_string($contentCompressed)) {
            $contentCompressed = in_array(strtolower($contentCompressed), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(SaleOrderNotificationTableMap::COL_CONTENT_COMPRESSED, $contentCompressed, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%', Criteria::LIKE); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderNotificationTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the date_sent column
     *
     * Example usage:
     * <code>
     * $query->filterByDateSent('2011-03-14'); // WHERE date_sent = '2011-03-14'
     * $query->filterByDateSent('now'); // WHERE date_sent = '2011-03-14'
     * $query->filterByDateSent(array('max' => 'yesterday')); // WHERE date_sent > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateSent The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterByDateSent($dateSent = null, $comparison = null)
    {
        if (is_array($dateSent)) {
            $useMinMax = false;
            if (isset($dateSent['min'])) {
                $this->addUsingAlias(SaleOrderNotificationTableMap::COL_DATE_SENT, $dateSent['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateSent['max'])) {
                $this->addUsingAlias(SaleOrderNotificationTableMap::COL_DATE_SENT, $dateSent['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderNotificationTableMap::COL_DATE_SENT, $dateSent, $comparison);
    }

    /**
     * Filter the query on the sender_ip column
     *
     * Example usage:
     * <code>
     * $query->filterBySenderIp('fooValue');   // WHERE sender_ip = 'fooValue'
     * $query->filterBySenderIp('%fooValue%', Criteria::LIKE); // WHERE sender_ip LIKE '%fooValue%'
     * </code>
     *
     * @param     string $senderIp The value to use as filter.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterBySenderIp($senderIp = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($senderIp)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SaleOrderNotificationTableMap::COL_SENDER_IP, $senderIp, $comparison);
    }

    /**
     * Filter the query by a related \Model\Sale\SaleOrder object
     *
     * @param \Model\Sale\SaleOrder|ObjectCollection $saleOrder The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterBySaleOrder($saleOrder, $comparison = null)
    {
        if ($saleOrder instanceof \Model\Sale\SaleOrder) {
            return $this
                ->addUsingAlias(SaleOrderNotificationTableMap::COL_SALE_ORDER_ID, $saleOrder->getId(), $comparison);
        } elseif ($saleOrder instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaleOrderNotificationTableMap::COL_SALE_ORDER_ID, $saleOrder->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySaleOrder() only accepts arguments of type \Model\Sale\SaleOrder or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrder relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function joinSaleOrder($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrder');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrder');
        }

        return $this;
    }

    /**
     * Use the SaleOrder relation SaleOrder object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Sale\SaleOrderQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaleOrder($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrder', '\Model\Sale\SaleOrderQuery');
    }

    /**
     * Filter the query by a related \Model\Setting\MasterTable\Sale_order_notification_type object
     *
     * @param \Model\Setting\MasterTable\Sale_order_notification_type|ObjectCollection $sale_order_notification_type The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function filterBySaleOrderNotificationType($sale_order_notification_type, $comparison = null)
    {
        if ($sale_order_notification_type instanceof \Model\Setting\MasterTable\Sale_order_notification_type) {
            return $this
                ->addUsingAlias(SaleOrderNotificationTableMap::COL_SALE_ORDER_NOTIFICATION_TYPE_ID, $sale_order_notification_type->getId(), $comparison);
        } elseif ($sale_order_notification_type instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(SaleOrderNotificationTableMap::COL_SALE_ORDER_NOTIFICATION_TYPE_ID, $sale_order_notification_type->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterBySaleOrderNotificationType() only accepts arguments of type \Model\Setting\MasterTable\Sale_order_notification_type or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the SaleOrderNotificationType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function joinSaleOrderNotificationType($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('SaleOrderNotificationType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'SaleOrderNotificationType');
        }

        return $this;
    }

    /**
     * Use the SaleOrderNotificationType relation Sale_order_notification_type object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \Model\Setting\MasterTable\Sale_order_notification_typeQuery A secondary query class using the current class as primary query
     */
    public function useSaleOrderNotificationTypeQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinSaleOrderNotificationType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'SaleOrderNotificationType', '\Model\Setting\MasterTable\Sale_order_notification_typeQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildSaleOrderNotification $saleOrderNotification Object to remove from the list of results
     *
     * @return $this|ChildSaleOrderNotificationQuery The current query, for fluid interface
     */
    public function prune($saleOrderNotification = null)
    {
        if ($saleOrderNotification) {
            $this->addUsingAlias(SaleOrderNotificationTableMap::COL_ID, $saleOrderNotification->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the sale_order_notification table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderNotificationTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            SaleOrderNotificationTableMap::clearInstancePool();
            SaleOrderNotificationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(SaleOrderNotificationTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(SaleOrderNotificationTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            SaleOrderNotificationTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            SaleOrderNotificationTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // SaleOrderNotificationQuery
