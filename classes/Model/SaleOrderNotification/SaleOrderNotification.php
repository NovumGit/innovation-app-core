<?php

namespace Model\SaleOrderNotification;

use Model\SaleOrderNotification\Base\SaleOrderNotification as BaseSaleOrderNotification;
use Core\Config;

/**
 * Skeleton subclass for representing a row from the 'sale_order_notification' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SaleOrderNotification extends BaseSaleOrderNotification
{

}
