<?php

namespace Model\SaleOrderNotification;

use Model\SaleOrderNotification\Base\SaleOrderNotificationQuery as BaseSaleOrderNotificationQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'sale_order_notification' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SaleOrderNotificationQuery extends BaseSaleOrderNotificationQuery
{

}
