<?php

namespace Model\Finance;

use Cassandra\Exception\LogicException;
use Core\QueryMapper;
use Model\Finance\Base\BankTransactionCategoryQuery as BaseBankTransactionCategoryQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'bank_transaction_category' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class BankTransactionCategoryQuery extends BaseBankTransactionCategoryQuery
{
	/**
	 * Returns the full transaction tree as an array
	 *
	 * @param null $iParentId
	 * @param array $aItems
	 * @param string $sDescriptionPrefix
	 *
	 * @return array
	 */
	static function getTree($iParentId = null, array &$aItems = array(), string $sDescriptionPrefix = ''):array
	{
		$oBankTransactionCategoryQuery = BankTransactionCategoryQuery::create();
		$iCriteria = $iParentId === null ? Criteria::ISNULL : Criteria::EQUAL;
		$oBankTransactionCategoryQuery->filterByParentId($iParentId, $iCriteria);
		$oBankTransactionCategoryQuery->orderByDescription();
		$aBankTransactionCategories = $oBankTransactionCategoryQuery->find();

		foreach($aBankTransactionCategories as $oCategory)
		{
			if(!$oCategory instanceof BankTransactionCategory)
			{
				throw new LogicException("Expected an instance of Category");
			}
			if($sDescriptionPrefix)
			{
				$sDescription = $sDescriptionPrefix . ' -> ' .  $oCategory->getDescription();
			}
			else
			{
				$sDescription = $oCategory->getDescription();
			}

			$aItems[$oCategory->getId()] = $sDescription;
			self::getTree($oCategory->getId(), $aItems, $sDescription);
		}
		return $aItems;
	}


	function getTotals(int $iBankTransactionCategoryQueryId = null, $iYear, $iMonth, $fAmount = 0)
	{
		$aWhere = [];
		if($iBankTransactionCategoryQueryId == null)
		{
			$aWhere[] = "bt.bank_transaction_category_id IS NULL";
		}
		else
		{
			$aWhere[] = "bt.bank_transaction_category_id = $iBankTransactionCategoryQueryId";
		}
		$aWhere[] = '';

		$sQuery = "SELECT SUM(bt.amount) amount 
					FROM 
						bank_transaction bt 
					WHERE 
						" . join(' AND ', $aWhere);


		$fAmount = $fAmount + QueryMapper::fetchVal($sQuery);

		$oQuery = self::create();
		$oQuery->filterByParentId($iBankTransactionCategoryQueryId);
		$oQuery->orderByDescription();
		$aItems = $oQuery->find();

		if(!empty($aItems))
		{
			foreach ($aItems as $oItem)
			{
				$fAmount = $fAmount + $this->getTotals($oItem->getId());
			}
		}
		return $fAmount;
	}
	private function getMonths()
	{
		$aMonthsMap = [
			1 => 'Januari', 2 => 'Februari', 3 => 'Maart', 4 => 'April', 5 => 'Mei', 6 => 'Juni', 7 => 'Juli',
			8 => 'Augustus', 9 => 'September', 10 => 'Oktober', 11 => 'November', 12 => 'December',
		];
		$iYear = date('Y') - 1;
		$iCurrentMonth = date('n');
		$aMonths = [];
		for($i = $iCurrentMonth; $i <= $iCurrentMonth+12; $i++)
		{
			if($i > 12)
			{
				$aMonths[] = ['month' => $i - 12, 'year' => $iYear + 1, 'label' => $aMonthsMap[$i - 12]];
			}
			else
			{
				$aMonths[] = ['month' => $i, 'year' => $iYear, 'label' => $aMonthsMap[$i]];
			}
		}
		return $aMonths;
	}

	function getTreeWithTotals($iParentId = null, $aTree = [])
	{
		$oQuery = self::create();
		$oQuery->filterByParentId($iParentId);
		$oQuery->orderByDescription();

		$aItems = $oQuery->find();

		if(!empty($aItems))
		{
			foreach($aItems as $oItem)
			{
				$aTree['description'] = $oItem->getDescription();
				$aTree['children'] = $this->getTreeWithTotals($oItem->getId());
				$aMonths = $this->getMonths();
				foreach($aMonths as $aMonth)
				{
					$aMonth['totals'] = $this->getTotals($oItem->getId(), $aMonth['year'], $aMonth['month']);
					$aTree['totals'] = $aMonth;
				}

			}
		}
		return $aTree;
	}
}
